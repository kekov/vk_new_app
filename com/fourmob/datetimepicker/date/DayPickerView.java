package com.fourmob.datetimepicker.date;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.AbsListView;
import android.widget.AbsListView.LayoutParams;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import com.fourmob.datetimepicker.date.SimpleMonthAdapter.CalendarDay;

public class DayPickerView extends ListView implements OnScrollListener, OnDateChangedListener {
    public static int LIST_TOP_OFFSET;
    protected SimpleMonthAdapter mAdapter;
    protected Context mContext;
    private final DatePickerController mController;
    protected int mCurrentMonthDisplayed;
    protected int mCurrentScrollState;
    protected int mDaysPerWeek;
    protected float mFriction;
    protected Handler mHandler;
    protected int mNumWeeks;
    private boolean mPerformingScroll;
    protected long mPreviousScrollPosition;
    protected int mPreviousScrollState;
    protected ScrollStateRunnable mScrollStateChangedRunnable;
    protected CalendarDay mSelectedDay;
    protected boolean mShowWeekNumber;
    protected CalendarDay mTempDay;

    /* renamed from: com.fourmob.datetimepicker.date.DayPickerView.1 */
    class C00851 implements Runnable {
        private final /* synthetic */ int val$position;

        C00851(int i) {
            this.val$position = i;
        }

        public void run() {
            DayPickerView.this.setSelection(this.val$position);
        }
    }

    protected class ScrollStateRunnable implements Runnable {
        private int mNewState;

        protected ScrollStateRunnable() {
        }

        public void doScrollStateChange(AbsListView absListView, int newState) {
            DayPickerView.this.mHandler.removeCallbacks(this);
            this.mNewState = newState;
            DayPickerView.this.mHandler.postDelayed(this, 40);
        }

        public void run() {
        }
    }

    static {
        LIST_TOP_OFFSET = -1;
    }

    public DayPickerView(Context context, DatePickerController datePickerController) {
        super(context);
        this.mCurrentScrollState = 0;
        this.mDaysPerWeek = 7;
        this.mFriction = 1.0f;
        this.mHandler = new Handler();
        this.mNumWeeks = 6;
        this.mPreviousScrollState = 0;
        this.mScrollStateChangedRunnable = new ScrollStateRunnable();
        this.mSelectedDay = new CalendarDay();
        this.mShowWeekNumber = false;
        this.mTempDay = new CalendarDay();
        this.mController = datePickerController;
        this.mController.registerOnDateChangedListener(this);
        setLayoutParams(new LayoutParams(-1, -1));
        setDrawSelectorOnTop(false);
        init(context);
        onDateChanged();
    }

    public int getMostVisiblePosition() {
        int firstVisiblePosition = getFirstVisiblePosition();
        int height = getHeight();
        int maxGap = 0;
        int mostVisiblePosition = 0;
        int childIndex = 0;
        int bottom = 0;
        while (true) {
            View childView = getChildAt(childIndex);
            if (childView == null) {
                return firstVisiblePosition + mostVisiblePosition;
            }
            if (bottom >= height) {
                return firstVisiblePosition + mostVisiblePosition;
            }
            bottom = childView.getBottom();
            int gap = Math.min(bottom, height) - Math.max(0, childView.getTop());
            if (gap > maxGap) {
                mostVisiblePosition = childIndex;
                maxGap = gap;
            }
            childIndex++;
        }
    }

    public boolean goTo(CalendarDay calendarDay, boolean scrollToTop, boolean selectDay, boolean displayMonth) {
        if (selectDay) {
            this.mSelectedDay.set(calendarDay);
        }
        this.mTempDay.set(calendarDay);
        postSetSelection(((calendarDay.year - this.mController.getMinYear()) * 12) + calendarDay.month);
        return true;
    }

    public void init(Context paramContext) {
        this.mContext = paramContext;
        setUpListView();
        setUpAdapter();
        setAdapter(this.mAdapter);
    }

    protected void layoutChildren() {
        super.layoutChildren();
        if (this.mPerformingScroll) {
            this.mPerformingScroll = false;
        }
    }

    public void onChange() {
        setUpAdapter();
        setAdapter(this.mAdapter);
    }

    public void onDateChanged() {
        goTo(this.mController.getSelectedDay(), false, true, true);
    }

    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        SimpleMonthView simpleMonthView = (SimpleMonthView) absListView.getChildAt(0);
        if (simpleMonthView != null) {
            this.mPreviousScrollPosition = (long) ((absListView.getFirstVisiblePosition() * simpleMonthView.getHeight()) - simpleMonthView.getBottom());
            this.mPreviousScrollState = this.mCurrentScrollState;
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int scroll) {
        this.mScrollStateChangedRunnable.doScrollStateChange(absListView, scroll);
    }

    public void postSetSelection(int position) {
        clearFocus();
        post(new C00851(position));
        onScrollStateChanged(this, 0);
    }

    protected void setMonthDisplayed(CalendarDay calendarDay) {
        this.mCurrentMonthDisplayed = calendarDay.month;
        invalidateViews();
    }

    protected void setUpAdapter() {
        if (this.mAdapter == null) {
            this.mAdapter = new SimpleMonthAdapter(getContext(), this.mController);
        }
        this.mAdapter.setSelectedDay(this.mSelectedDay);
        this.mAdapter.setMinDay(this.mController.getMinDay());
        this.mAdapter.setMaxDay(this.mController.getMaxDay());
        this.mAdapter.notifyDataSetChanged();
    }

    protected void setUpListView() {
        setCacheColorHint(0);
        setDivider(null);
        setItemsCanFocus(true);
        setFastScrollEnabled(false);
        setVerticalScrollBarEnabled(false);
        setOnScrollListener(this);
        setFadingEdgeLength(0);
        setFrictionIfSupported(ViewConfiguration.getScrollFriction() * this.mFriction);
    }

    @TargetApi(11)
    void setFrictionIfSupported(float friction) {
        if (VERSION.SDK_INT >= 11) {
            setFriction(friction);
        }
    }
}
