package com.fourmob.datetimepicker.date;

import com.fourmob.datetimepicker.date.SimpleMonthAdapter.CalendarDay;

interface DatePickerController {
    int getFirstDayOfWeek();

    int getMaxDay();

    int getMaxYear();

    int getMinDay();

    int getMinYear();

    CalendarDay getSelectedDay();

    void onDayOfMonthSelected(int i, int i2, int i3);

    void onYearSelected(int i);

    void registerOnDateChangedListener(OnDateChangedListener onDateChangedListener);

    void tryVibrate();
}
