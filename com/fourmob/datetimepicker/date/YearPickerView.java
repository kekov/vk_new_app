package com.fourmob.datetimepicker.date;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.fourmob.datetimepicker.C0083R;
import java.util.ArrayList;
import java.util.List;

public class YearPickerView extends ListView implements OnItemClickListener, OnDateChangedListener {
    private YearAdapter mAdapter;
    private int mChildSize;
    private final DatePickerController mController;
    private TextViewWithCircularIndicator mSelectedView;
    private int mViewSize;

    /* renamed from: com.fourmob.datetimepicker.date.YearPickerView.1 */
    class C00861 implements Runnable {
        private final /* synthetic */ int val$position;
        private final /* synthetic */ int val$y;

        C00861(int i, int i2) {
            this.val$position = i;
            this.val$y = i2;
        }

        public void run() {
            YearPickerView.this.setSelectionFromTop(this.val$position, this.val$y);
            YearPickerView.this.requestLayout();
        }
    }

    private class YearAdapter extends ArrayAdapter<String> {
        public YearAdapter(Context context, int resourceId, List<String> years) {
            super(context, resourceId, years);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            TextViewWithCircularIndicator textViewWithCircularIndicator = (TextViewWithCircularIndicator) super.getView(position, convertView, parent);
            textViewWithCircularIndicator.requestLayout();
            textViewWithCircularIndicator.drawIndicator(YearPickerView.this.mController.getSelectedDay().year == YearPickerView.this.getYearFromTextView(textViewWithCircularIndicator));
            return textViewWithCircularIndicator;
        }
    }

    public YearPickerView(Context context, DatePickerController datePickerController) {
        super(context);
        this.mController = datePickerController;
        this.mController.registerOnDateChangedListener(this);
        setLayoutParams(new LayoutParams(-1, -2));
        Resources resources = context.getResources();
        this.mViewSize = resources.getDimensionPixelOffset(C0083R.dimen.date_picker_view_animator_height);
        this.mChildSize = resources.getDimensionPixelOffset(C0083R.dimen.year_label_height);
        setVerticalFadingEdgeEnabled(true);
        setFadingEdgeLength(this.mChildSize / 3);
        init(context);
        setOnItemClickListener(this);
        setSelector(new StateListDrawable());
        setDividerHeight(0);
        onDateChanged();
    }

    private int getYearFromTextView(TextView textView) {
        return Integer.valueOf(textView.getText().toString()).intValue();
    }

    private void init(Context context) {
        ArrayList<String> years = new ArrayList();
        for (int year = this.mController.getMinYear(); year <= this.mController.getMaxYear(); year++) {
            years.add(String.format("%d", new Object[]{Integer.valueOf(year)}));
        }
        this.mAdapter = new YearAdapter(context, C0083R.layout.year_label_text_view, years);
        setAdapter(this.mAdapter);
    }

    public int getFirstPositionOffset() {
        View view = getChildAt(0);
        if (view == null) {
            return 0;
        }
        return view.getTop();
    }

    public void onDateChanged() {
        this.mAdapter.notifyDataSetChanged();
        postSetSelectionCentered(this.mController.getSelectedDay().year - this.mController.getMinYear());
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        this.mController.tryVibrate();
        TextViewWithCircularIndicator textViewWithCircularIndicator = (TextViewWithCircularIndicator) view;
        if (textViewWithCircularIndicator != null) {
            if (textViewWithCircularIndicator != this.mSelectedView) {
                if (this.mSelectedView != null) {
                    this.mSelectedView.drawIndicator(false);
                    this.mSelectedView.requestLayout();
                }
                textViewWithCircularIndicator.drawIndicator(true);
                textViewWithCircularIndicator.requestLayout();
                this.mSelectedView = textViewWithCircularIndicator;
            }
            this.mController.onYearSelected(getYearFromTextView(textViewWithCircularIndicator));
            this.mAdapter.notifyDataSetChanged();
        }
    }

    public void postSetSelectionCentered(int position) {
        postSetSelectionFromTop(position, (this.mViewSize / 2) - (this.mChildSize / 2));
    }

    public void postSetSelectionFromTop(int position, int y) {
        post(new C00861(position, y));
    }
}
