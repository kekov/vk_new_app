package com.fourmob.datetimepicker.date;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import com.fourmob.datetimepicker.C0083R;
import com.fourmob.datetimepicker.Utils;
import com.fourmob.datetimepicker.date.SimpleMonthAdapter.CalendarDay;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

public class CalendarDatePickerDialog extends DialogFragment implements OnClickListener, DatePickerController {
    private static SimpleDateFormat DAY_FORMAT = null;
    private static final int MAX_YEAR = 2037;
    private static final int MIN_YEAR = 1902;
    private static final int VIEW_DATE_PICKER_MONTH_DAY = 0;
    private static final int VIEW_DATE_PICKER_YEAR = 1;
    private static SimpleDateFormat YEAR_FORMAT;
    private DateFormatSymbols dateformartsymbols;
    private String doneButtonText;
    private ViewAnimator mAnimator;
    private final Calendar mCalendar;
    private OnDateSetListener mCallBack;
    private int mCurrentView;
    private TextView mDayOfWeekView;
    private String mDayPickerDescription;
    private DayPickerView mDayPickerView;
    private boolean mDelayAnimation;
    private Button mDoneButton;
    private long mLastVibrate;
    private HashSet<OnDateChangedListener> mListeners;
    private int mMaxYear;
    private int mMinYear;
    private LinearLayout mMonthAndDayView;
    private String mSelectDay;
    private String mSelectYear;
    private TextView mSelectedDayTextView;
    private TextView mSelectedMonthTextView;
    private boolean mVibrate;
    private Vibrator mVibrator;
    private int mWeekStart;
    private String mYearPickerDescription;
    private YearPickerView mYearPickerView;
    private TextView mYearView;
    private int maxDay;
    private int minDay;

    /* renamed from: com.fourmob.datetimepicker.date.CalendarDatePickerDialog.1 */
    class C00841 implements OnClickListener {
        C00841() {
        }

        public void onClick(View view) {
            CalendarDatePickerDialog.this.tryVibrate();
            if (CalendarDatePickerDialog.this.mCallBack != null) {
                CalendarDatePickerDialog.this.mCallBack.onDateSet(CalendarDatePickerDialog.this, CalendarDatePickerDialog.this.mCalendar.get(CalendarDatePickerDialog.VIEW_DATE_PICKER_YEAR), CalendarDatePickerDialog.this.mCalendar.get(2), CalendarDatePickerDialog.this.mCalendar.get(5));
            }
            CalendarDatePickerDialog.this.dismiss();
        }
    }

    interface OnDateChangedListener {
        void onDateChanged();
    }

    public interface OnDateSetListener {
        void onDateSet(CalendarDatePickerDialog calendarDatePickerDialog, int i, int i2, int i3);
    }

    static {
        DAY_FORMAT = new SimpleDateFormat("dd", Locale.getDefault());
        YEAR_FORMAT = new SimpleDateFormat("yyyy", Locale.getDefault());
    }

    private void adjustDayInMonthIfNeeded(int month, int year) {
        int currentDay = this.mCalendar.get(5);
        int day = Utils.getDaysInMonth(month, year);
        if (currentDay > day) {
            this.mCalendar.set(5, day);
        }
    }

    public CalendarDatePickerDialog() {
        this.mCalendar = Calendar.getInstance();
        this.mCurrentView = -1;
        this.mDelayAnimation = true;
        this.mListeners = new HashSet();
        this.mMaxYear = MAX_YEAR;
        this.mMinYear = MIN_YEAR;
        this.mWeekStart = this.mCalendar.getFirstDayOfWeek();
        this.dateformartsymbols = new DateFormatSymbols();
        this.mVibrate = true;
    }

    public static CalendarDatePickerDialog newInstance(OnDateSetListener onDateSetListener, int year, int month, int day) {
        return newInstance(onDateSetListener, year, month, day, true);
    }

    public static CalendarDatePickerDialog newInstance(OnDateSetListener onDateSetListener, int year, int month, int day, boolean vibrate) {
        CalendarDatePickerDialog datePickerDialog = new CalendarDatePickerDialog();
        datePickerDialog.initialize(onDateSetListener, year, month, day, vibrate);
        return datePickerDialog;
    }

    public void setVibrate(boolean vibrate) {
        this.mVibrate = vibrate;
    }

    private void setCurrentView(int currentView) {
        setCurrentView(currentView, false);
    }

    private void setCurrentView(int currentView, boolean forceRefresh) {
        long timeInMillis = this.mCalendar.getTimeInMillis();
        switch (currentView) {
            case VIEW_DATE_PICKER_MONTH_DAY /*0*/:
                ObjectAnimator monthDayAnim = Utils.getPulseAnimator(this.mMonthAndDayView, 0.9f, 1.05f);
                if (this.mDelayAnimation) {
                    monthDayAnim.setStartDelay(500);
                    this.mDelayAnimation = false;
                }
                this.mDayPickerView.onDateChanged();
                if (this.mCurrentView != currentView || forceRefresh) {
                    this.mMonthAndDayView.setSelected(true);
                    this.mYearView.setSelected(false);
                    this.mAnimator.setDisplayedChild(VIEW_DATE_PICKER_MONTH_DAY);
                    this.mCurrentView = currentView;
                }
                monthDayAnim.start();
                this.mAnimator.setContentDescription(this.mDayPickerDescription + ": " + DateUtils.formatDateTime(getActivity(), timeInMillis, 16));
            case VIEW_DATE_PICKER_YEAR /*1*/:
                ObjectAnimator yearAnim = Utils.getPulseAnimator(this.mYearView, 0.85f, 1.1f);
                if (this.mDelayAnimation) {
                    yearAnim.setStartDelay(500);
                    this.mDelayAnimation = false;
                }
                this.mYearPickerView.onDateChanged();
                if (this.mCurrentView != currentView || forceRefresh) {
                    this.mMonthAndDayView.setSelected(false);
                    this.mYearView.setSelected(true);
                    this.mAnimator.setDisplayedChild(VIEW_DATE_PICKER_YEAR);
                    this.mCurrentView = currentView;
                }
                yearAnim.start();
                this.mAnimator.setContentDescription(this.mYearPickerDescription + ": " + YEAR_FORMAT.format(Long.valueOf(timeInMillis)));
            default:
        }
    }

    private void updateDisplay() {
        if (this.mDayOfWeekView != null) {
            this.mCalendar.setFirstDayOfWeek(this.mWeekStart);
            this.mDayOfWeekView.setText(this.dateformartsymbols.getWeekdays()[this.mCalendar.get(7)].toUpperCase(Locale.getDefault()));
        }
        this.mSelectedMonthTextView.setText(this.dateformartsymbols.getMonths()[this.mCalendar.get(2)].toUpperCase(Locale.getDefault()));
        this.mSelectedDayTextView.setText(DAY_FORMAT.format(this.mCalendar.getTime()));
        this.mYearView.setText(YEAR_FORMAT.format(this.mCalendar.getTime()));
        this.mMonthAndDayView.setContentDescription(DateUtils.formatDateTime(getActivity(), this.mCalendar.getTimeInMillis(), 24));
    }

    private void updatePickers() {
        Iterator<OnDateChangedListener> it = this.mListeners.iterator();
        while (it.hasNext()) {
            ((OnDateChangedListener) it.next()).onDateChanged();
        }
    }

    public int getFirstDayOfWeek() {
        return this.mWeekStart;
    }

    public int getMaxYear() {
        return this.mMaxYear;
    }

    public int getMinYear() {
        return this.mMinYear;
    }

    public CalendarDay getSelectedDay() {
        return new CalendarDay(this.mCalendar);
    }

    public void initialize(OnDateSetListener onDateSetListener, int year, int month, int day, boolean vibrate) {
        if (year > MAX_YEAR) {
            throw new IllegalArgumentException("year end must < 2037");
        } else if (year < MIN_YEAR) {
            throw new IllegalArgumentException("year end must > 1902");
        } else {
            this.mCallBack = onDateSetListener;
            this.mCalendar.set(VIEW_DATE_PICKER_YEAR, year);
            this.mCalendar.set(2, month);
            this.mCalendar.set(5, day);
            this.mVibrate = vibrate;
        }
    }

    public void onClick(View view) {
        tryVibrate();
        if (view.getId() == C0083R.id.date_picker_year) {
            setCurrentView(VIEW_DATE_PICKER_YEAR);
        } else if (view.getId() == C0083R.id.date_picker_month_and_day) {
            setCurrentView(VIEW_DATE_PICKER_MONTH_DAY);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Activity activity = getActivity();
        activity.getWindow().setSoftInputMode(3);
        this.mVibrator = (Vibrator) activity.getSystemService("vibrator");
        if (bundle != null) {
            this.mCalendar.set(VIEW_DATE_PICKER_YEAR, bundle.getInt("year"));
            this.mCalendar.set(2, bundle.getInt("month"));
            this.mCalendar.set(5, bundle.getInt("day"));
            this.mVibrate = bundle.getBoolean("vibrate");
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup parent, Bundle bundle) {
        Log.d("DatePickerDialog", "onCreateView: ");
        getDialog().getWindow().requestFeature(VIEW_DATE_PICKER_YEAR);
        View view = layoutInflater.inflate(C0083R.layout.date_picker_dialog, null);
        this.mDayOfWeekView = (TextView) view.findViewById(C0083R.id.date_picker_header);
        this.mMonthAndDayView = (LinearLayout) view.findViewById(C0083R.id.date_picker_month_and_day);
        this.mMonthAndDayView.setOnClickListener(this);
        this.mSelectedMonthTextView = (TextView) view.findViewById(C0083R.id.date_picker_month);
        this.mSelectedDayTextView = (TextView) view.findViewById(C0083R.id.date_picker_day);
        this.mYearView = (TextView) view.findViewById(C0083R.id.date_picker_year);
        this.mYearView.setOnClickListener(this);
        int listPosition = -1;
        int currentView = VIEW_DATE_PICKER_MONTH_DAY;
        int listPositionOffset = VIEW_DATE_PICKER_MONTH_DAY;
        if (bundle != null) {
            this.mWeekStart = bundle.getInt("week_start");
            this.mMinYear = bundle.getInt("year_start");
            this.mMaxYear = bundle.getInt("year_end");
            currentView = bundle.getInt("current_view");
            listPosition = bundle.getInt("list_position");
            listPositionOffset = bundle.getInt("list_position_offset");
        }
        Activity activity = getActivity();
        this.mDayPickerView = new DayPickerView(activity, this);
        this.mYearPickerView = new YearPickerView(activity, this);
        Resources resources = getResources();
        this.mDayPickerDescription = resources.getString(C0083R.string.day_picker_description);
        this.mSelectDay = resources.getString(C0083R.string.select_day);
        this.mYearPickerDescription = resources.getString(C0083R.string.year_picker_description);
        this.mSelectYear = resources.getString(C0083R.string.select_year);
        this.mAnimator = (ViewAnimator) view.findViewById(C0083R.id.animator);
        this.mAnimator.addView(this.mDayPickerView);
        this.mAnimator.addView(this.mYearPickerView);
        AlphaAnimation inAlphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        inAlphaAnimation.setDuration(300);
        this.mAnimator.setInAnimation(inAlphaAnimation);
        AlphaAnimation outAlphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        outAlphaAnimation.setDuration(300);
        this.mAnimator.setOutAnimation(outAlphaAnimation);
        this.mDoneButton = (Button) view.findViewById(C0083R.id.done);
        this.mDoneButton.setOnClickListener(new C00841());
        if (this.doneButtonText != null) {
            this.mDoneButton.setText(this.doneButtonText);
        }
        updateDisplay();
        setCurrentView(currentView, true);
        if (listPosition != -1) {
            if (currentView == 0) {
                this.mDayPickerView.postSetSelection(listPosition);
            }
            if (currentView == VIEW_DATE_PICKER_YEAR) {
                this.mYearPickerView.postSetSelectionFromTop(listPosition, listPositionOffset);
            }
        }
        return view;
    }

    public void setDoneButtonText(String text) {
        this.doneButtonText = text;
        if (this.mDoneButton != null) {
            this.mDoneButton.setText(text);
        }
    }

    public void onDayOfMonthSelected(int year, int month, int day) {
        this.mCalendar.set(VIEW_DATE_PICKER_YEAR, year);
        this.mCalendar.set(2, month);
        this.mCalendar.set(5, day);
        updatePickers();
        updateDisplay();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("year", this.mCalendar.get(VIEW_DATE_PICKER_YEAR));
        bundle.putInt("month", this.mCalendar.get(2));
        bundle.putInt("day", this.mCalendar.get(5));
        bundle.putInt("week_start", this.mWeekStart);
        bundle.putInt("year_start", this.mMinYear);
        bundle.putInt("year_end", this.mMaxYear);
        bundle.putInt("current_view", this.mCurrentView);
        int mostVisiblePosition = -1;
        if (this.mCurrentView == 0) {
            mostVisiblePosition = this.mDayPickerView.getMostVisiblePosition();
        }
        bundle.putInt("list_position", mostVisiblePosition);
        if (this.mCurrentView == VIEW_DATE_PICKER_YEAR) {
            mostVisiblePosition = this.mYearPickerView.getFirstVisiblePosition();
            bundle.putInt("list_position_offset", this.mYearPickerView.getFirstPositionOffset());
        }
        bundle.putBoolean("vibrate", this.mVibrate);
    }

    public void onYearSelected(int year) {
        adjustDayInMonthIfNeeded(this.mCalendar.get(2), year);
        this.mCalendar.set(VIEW_DATE_PICKER_YEAR, year);
        updatePickers();
        setCurrentView(VIEW_DATE_PICKER_MONTH_DAY);
        updateDisplay();
    }

    public void registerOnDateChangedListener(OnDateChangedListener onDateChangedListener) {
        this.mListeners.add(onDateChangedListener);
    }

    public void setFirstDayOfWeek(int weekStart) {
        if (weekStart < VIEW_DATE_PICKER_YEAR || weekStart > 7) {
            throw new IllegalArgumentException("Value must be between Calendar.SUNDAY and Calendar.SATURDAY");
        }
        this.mWeekStart = weekStart;
        if (this.mDayPickerView != null) {
            this.mDayPickerView.onChange();
        }
    }

    public void setOnDateSetListener(OnDateSetListener onDateSetListener) {
        this.mCallBack = onDateSetListener;
    }

    public void setYearRange(int minYear, int maxYear) {
        if (maxYear <= minYear) {
            throw new IllegalArgumentException("Year end must be larger than year start");
        } else if (maxYear > MAX_YEAR) {
            throw new IllegalArgumentException("max year end must < 2037");
        } else if (minYear < MIN_YEAR) {
            throw new IllegalArgumentException("min year end must > 1902");
        } else {
            this.mMinYear = minYear;
            this.mMaxYear = maxYear;
            if (this.mDayPickerView != null) {
                this.mDayPickerView.onChange();
            }
        }
    }

    public void tryVibrate() {
        if (this.mVibrator != null && this.mVibrate) {
            long timeInMillis = SystemClock.uptimeMillis();
            if (timeInMillis - this.mLastVibrate >= 125) {
                this.mVibrator.vibrate(5);
                this.mLastVibrate = timeInMillis;
            }
        }
    }

    public int getMinDay() {
        return this.minDay;
    }

    public int getMaxDay() {
        return this.maxDay;
    }

    public void setMinDay(int d, int m, int y) {
        if (y == 0) {
            this.minDay = -1;
        } else {
            this.minDay = ((m * 10) + d) + (y * 10000);
        }
        Log.i("vk", "min day " + this.minDay);
    }

    public void setMaxDay(int d, int m, int y) {
        if (y == 0) {
            this.maxDay = -1;
        } else {
            this.maxDay = ((m * 10) + d) + (y * 10000);
        }
    }
}
