package com.fourmob.datetimepicker.time;

import android.animation.ObjectAnimator;
import android.app.DialogFragment;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.text.method.TransformationMethod;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.fourmob.datetimepicker.C0083R;
import com.fourmob.datetimepicker.Utils;
import com.fourmob.datetimepicker.time.RadialPickerLayout.OnValueSelectedListener;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import net.hockeyapp.android.Strings;

public class RadialTimePickerDialog extends DialogFragment implements OnValueSelectedListener {
    public static final int AM = 0;
    public static final int AMPM_INDEX = 2;
    public static final int ENABLE_PICKER_INDEX = 3;
    public static final int HOUR_INDEX = 0;
    private static final String KEY_CURRENT_ITEM_SHOWING = "current_item_showing";
    private static final String KEY_HOUR_OF_DAY = "hour_of_day";
    private static final String KEY_IN_KB_MODE = "in_kb_mode";
    private static final String KEY_IS_24_HOUR_VIEW = "is_24_hour_view";
    private static final String KEY_MINUTE = "minute";
    private static final String KEY_TYPED_TIMES = "typed_times";
    private static final String KEY_VIBRATE = "vibrate";
    public static final int MINUTE_INDEX = 1;
    public static final int PM = 1;
    private static final int PULSE_ANIMATOR_DELAY = 300;
    private static final String TAG = "TimePickerDialog";
    private String doneButtonText;
    private boolean mAllowAutoAdvance;
    private int mAmKeyCode;
    private View mAmPmHitspace;
    private TextView mAmPmTextView;
    private String mAmText;
    private int mBlack;
    private int mBlue;
    private OnTimeSetListener mCallback;
    private String mDeletedKeyFormat;
    private TextView mDoneButton;
    private String mDoublePlaceholderText;
    private String mHourPickerDescription;
    private TextView mHourSpaceView;
    private TextView mHourView;
    private boolean mInKbMode;
    private int mInitialHourOfDay;
    private int mInitialMinute;
    private boolean mIs24HourMode;
    private Node mLegalTimesTree;
    private String mMinutePickerDescription;
    private TextView mMinuteSpaceView;
    private TextView mMinuteView;
    private char mPlaceholderText;
    private int mPmKeyCode;
    private String mPmText;
    private String mSelectHours;
    private String mSelectMinutes;
    private RadialPickerLayout mTimePicker;
    private ArrayList<Integer> mTypedTimes;
    private boolean mVibrate;

    /* renamed from: com.fourmob.datetimepicker.time.RadialTimePickerDialog.1 */
    class C00891 implements TransformationMethod {
        private final Locale locale;

        C00891() {
            this.locale = RadialTimePickerDialog.this.getResources().getConfiguration().locale;
        }

        public CharSequence getTransformation(CharSequence source, View view) {
            return source != null ? source.toString().toUpperCase(this.locale) : null;
        }

        public void onFocusChanged(View view, CharSequence sourceText, boolean focused, int direction, Rect previouslyFocusedRect) {
        }
    }

    /* renamed from: com.fourmob.datetimepicker.time.RadialTimePickerDialog.2 */
    class C00902 implements OnClickListener {
        C00902() {
        }

        public void onClick(View v) {
            RadialTimePickerDialog.this.setCurrentItemShowing(RadialTimePickerDialog.HOUR_INDEX, true, false, true);
            RadialTimePickerDialog.this.mTimePicker.tryVibrate();
        }
    }

    /* renamed from: com.fourmob.datetimepicker.time.RadialTimePickerDialog.3 */
    class C00913 implements OnClickListener {
        C00913() {
        }

        public void onClick(View v) {
            RadialTimePickerDialog.this.setCurrentItemShowing(RadialTimePickerDialog.PM, true, false, true);
            RadialTimePickerDialog.this.mTimePicker.tryVibrate();
        }
    }

    /* renamed from: com.fourmob.datetimepicker.time.RadialTimePickerDialog.4 */
    class C00924 implements OnClickListener {
        C00924() {
        }

        public void onClick(View v) {
            if (RadialTimePickerDialog.this.mInKbMode && RadialTimePickerDialog.this.isTypedTimeFullyLegal()) {
                RadialTimePickerDialog.this.finishKbMode(false);
            } else {
                RadialTimePickerDialog.this.mTimePicker.tryVibrate();
            }
            if (RadialTimePickerDialog.this.mCallback != null) {
                RadialTimePickerDialog.this.mCallback.onTimeSet(RadialTimePickerDialog.this.mTimePicker, RadialTimePickerDialog.this.mTimePicker.getHours(), RadialTimePickerDialog.this.mTimePicker.getMinutes());
            }
            RadialTimePickerDialog.this.dismiss();
        }
    }

    /* renamed from: com.fourmob.datetimepicker.time.RadialTimePickerDialog.5 */
    class C00935 implements OnClickListener {
        C00935() {
        }

        public void onClick(View v) {
            RadialTimePickerDialog.this.mTimePicker.tryVibrate();
            int amOrPm = RadialTimePickerDialog.this.mTimePicker.getIsCurrentlyAmOrPm();
            if (amOrPm == 0) {
                amOrPm = RadialTimePickerDialog.PM;
            } else if (amOrPm == RadialTimePickerDialog.PM) {
                amOrPm = RadialTimePickerDialog.HOUR_INDEX;
            }
            RadialTimePickerDialog.this.updateAmPmDisplay(amOrPm);
            RadialTimePickerDialog.this.mTimePicker.setAmOrPm(amOrPm);
        }
    }

    private class KeyboardListener implements OnKeyListener {
        private KeyboardListener() {
        }

        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == RadialTimePickerDialog.PM) {
                return RadialTimePickerDialog.this.processKeyUp(keyCode);
            }
            return false;
        }
    }

    private class Node {
        private ArrayList<Node> mChildren;
        private int[] mLegalKeys;

        public Node(int... legalKeys) {
            this.mLegalKeys = legalKeys;
            this.mChildren = new ArrayList();
        }

        public void addChild(Node child) {
            this.mChildren.add(child);
        }

        public boolean containsKey(int key) {
            for (int i = RadialTimePickerDialog.HOUR_INDEX; i < this.mLegalKeys.length; i += RadialTimePickerDialog.PM) {
                if (this.mLegalKeys[i] == key) {
                    return true;
                }
            }
            return false;
        }

        public Node canReach(int key) {
            if (this.mChildren == null) {
                return null;
            }
            Iterator it = this.mChildren.iterator();
            while (it.hasNext()) {
                Node child = (Node) it.next();
                if (child.containsKey(key)) {
                    return child;
                }
            }
            return null;
        }
    }

    public interface OnTimeSetListener {
        void onTimeSet(RadialPickerLayout radialPickerLayout, int i, int i2);
    }

    public RadialTimePickerDialog() {
        this.mVibrate = true;
    }

    public static RadialTimePickerDialog newInstance(OnTimeSetListener callback, int hourOfDay, int minute, boolean is24HourMode) {
        return newInstance(callback, hourOfDay, minute, is24HourMode, true);
    }

    public static RadialTimePickerDialog newInstance(OnTimeSetListener callback, int hourOfDay, int minute, boolean is24HourMode, boolean vibrate) {
        RadialTimePickerDialog ret = new RadialTimePickerDialog();
        ret.initialize(callback, hourOfDay, minute, is24HourMode, vibrate);
        return ret;
    }

    public void initialize(OnTimeSetListener callback, int hourOfDay, int minute, boolean is24HourMode, boolean vibrate) {
        this.mCallback = callback;
        this.mInitialHourOfDay = hourOfDay;
        this.mInitialMinute = minute;
        this.mIs24HourMode = is24HourMode;
        this.mInKbMode = false;
        this.mVibrate = vibrate;
    }

    public void setOnTimeSetListener(OnTimeSetListener callback) {
        this.mCallback = callback;
    }

    public void setStartTime(int hourOfDay, int minute) {
        this.mInitialHourOfDay = hourOfDay;
        this.mInitialMinute = minute;
        this.mInKbMode = false;
    }

    public void setVibrate(boolean vibrate) {
        this.mVibrate = vibrate;
        if (this.mTimePicker != null) {
            this.mTimePicker.setVibrate(vibrate);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_HOUR_OF_DAY) && savedInstanceState.containsKey(KEY_MINUTE) && savedInstanceState.containsKey(KEY_IS_24_HOUR_VIEW)) {
            this.mInitialHourOfDay = savedInstanceState.getInt(KEY_HOUR_OF_DAY);
            this.mInitialMinute = savedInstanceState.getInt(KEY_MINUTE);
            this.mIs24HourMode = savedInstanceState.getBoolean(KEY_IS_24_HOUR_VIEW);
            this.mInKbMode = savedInstanceState.getBoolean(KEY_IN_KB_MODE);
            this.mVibrate = savedInstanceState.getBoolean(KEY_VIBRATE);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(PM);
        View view = inflater.inflate(C0083R.layout.time_picker_dialog, null);
        KeyboardListener keyboardListener = new KeyboardListener();
        view.findViewById(C0083R.id.time_picker_dialog).setOnKeyListener(keyboardListener);
        Resources res = getResources();
        this.mHourPickerDescription = res.getString(C0083R.string.hour_picker_description);
        this.mSelectHours = res.getString(C0083R.string.select_hours);
        this.mMinutePickerDescription = res.getString(C0083R.string.minute_picker_description);
        this.mSelectMinutes = res.getString(C0083R.string.select_minutes);
        this.mBlue = res.getColor(C0083R.color.blue);
        this.mBlack = res.getColor(C0083R.color.numbers_text_color);
        this.mHourView = (TextView) view.findViewById(C0083R.id.hours);
        this.mHourView.setOnKeyListener(keyboardListener);
        this.mHourSpaceView = (TextView) view.findViewById(C0083R.id.hour_space);
        this.mMinuteSpaceView = (TextView) view.findViewById(C0083R.id.minutes_space);
        this.mMinuteView = (TextView) view.findViewById(C0083R.id.minutes);
        this.mMinuteView.setOnKeyListener(keyboardListener);
        this.mAmPmTextView = (TextView) view.findViewById(C0083R.id.ampm_label);
        this.mAmPmTextView.setOnKeyListener(keyboardListener);
        if (VERSION.SDK_INT <= 14) {
            this.mAmPmTextView.setTransformationMethod(new C00891());
        }
        String[] amPmTexts = new DateFormatSymbols().getAmPmStrings();
        this.mAmText = amPmTexts[HOUR_INDEX];
        this.mPmText = amPmTexts[PM];
        this.mTimePicker = (RadialPickerLayout) view.findViewById(C0083R.id.time_picker);
        this.mTimePicker.setOnValueSelectedListener(this);
        this.mTimePicker.setOnKeyListener(keyboardListener);
        this.mTimePicker.initialize(getActivity(), this.mInitialHourOfDay, this.mInitialMinute, this.mIs24HourMode, this.mVibrate);
        int currentItemShowing = HOUR_INDEX;
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY_CURRENT_ITEM_SHOWING)) {
                currentItemShowing = savedInstanceState.getInt(KEY_CURRENT_ITEM_SHOWING);
            }
        }
        setCurrentItemShowing(currentItemShowing, false, true, true);
        this.mTimePicker.invalidate();
        this.mHourView.setOnClickListener(new C00902());
        this.mMinuteView.setOnClickListener(new C00913());
        this.mDoneButton = (TextView) view.findViewById(C0083R.id.done_button);
        this.mDoneButton.setOnClickListener(new C00924());
        if (this.doneButtonText != null) {
            this.mDoneButton.setText(this.doneButtonText);
        }
        this.mDoneButton.setOnKeyListener(keyboardListener);
        this.mAmPmHitspace = view.findViewById(C0083R.id.ampm_hitspace);
        if (this.mIs24HourMode) {
            this.mAmPmTextView.setVisibility(8);
            LayoutParams paramsSeparator = new LayoutParams(-2, -2);
            paramsSeparator.addRule(13);
            ((TextView) view.findViewById(C0083R.id.separator)).setLayoutParams(paramsSeparator);
        } else {
            this.mAmPmTextView.setVisibility(HOUR_INDEX);
            updateAmPmDisplay(this.mInitialHourOfDay < 12 ? HOUR_INDEX : PM);
            this.mAmPmHitspace.setOnClickListener(new C00935());
        }
        this.mAllowAutoAdvance = true;
        setHour(this.mInitialHourOfDay, true);
        setMinute(this.mInitialMinute);
        this.mDoublePlaceholderText = res.getString(C0083R.string.time_placeholder);
        this.mDeletedKeyFormat = res.getString(C0083R.string.deleted_key);
        this.mPlaceholderText = this.mDoublePlaceholderText.charAt(HOUR_INDEX);
        this.mPmKeyCode = -1;
        this.mAmKeyCode = -1;
        generateLegalTimesTree();
        if (this.mInKbMode) {
            this.mTypedTimes = savedInstanceState.getIntegerArrayList(KEY_TYPED_TIMES);
            tryStartingKbMode(-1);
            this.mHourView.invalidate();
        } else if (this.mTypedTimes == null) {
            this.mTypedTimes = new ArrayList();
        }
        return view;
    }

    private void updateAmPmDisplay(int amOrPm) {
        if (amOrPm == 0) {
            this.mAmPmTextView.setText(this.mAmText);
            Utils.tryAccessibilityAnnounce(this.mTimePicker, this.mAmText);
            this.mAmPmHitspace.setContentDescription(this.mAmText);
        } else if (amOrPm == PM) {
            this.mAmPmTextView.setText(this.mPmText);
            Utils.tryAccessibilityAnnounce(this.mTimePicker, this.mPmText);
            this.mAmPmHitspace.setContentDescription(this.mPmText);
        } else {
            this.mAmPmTextView.setText(this.mDoublePlaceholderText);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (this.mTimePicker != null) {
            outState.putInt(KEY_HOUR_OF_DAY, this.mTimePicker.getHours());
            outState.putInt(KEY_MINUTE, this.mTimePicker.getMinutes());
            outState.putBoolean(KEY_IS_24_HOUR_VIEW, this.mIs24HourMode);
            outState.putInt(KEY_CURRENT_ITEM_SHOWING, this.mTimePicker.getCurrentItemShowing());
            outState.putBoolean(KEY_IN_KB_MODE, this.mInKbMode);
            if (this.mInKbMode) {
                outState.putIntegerArrayList(KEY_TYPED_TIMES, this.mTypedTimes);
            }
            outState.putBoolean(KEY_VIBRATE, this.mVibrate);
        }
    }

    public void onValueSelected(int pickerIndex, int newValue, boolean autoAdvance) {
        if (pickerIndex == 0) {
            setHour(newValue, false);
            Object[] objArr = new Object[PM];
            objArr[HOUR_INDEX] = Integer.valueOf(newValue);
            String announcement = String.format("%d", objArr);
            if (this.mAllowAutoAdvance && autoAdvance) {
                setCurrentItemShowing(PM, true, true, false);
                announcement = new StringBuilder(String.valueOf(announcement)).append(". ").append(this.mSelectMinutes).toString();
            }
            Utils.tryAccessibilityAnnounce(this.mTimePicker, announcement);
        } else if (pickerIndex == PM) {
            setMinute(newValue);
        } else if (pickerIndex == AMPM_INDEX) {
            updateAmPmDisplay(newValue);
        } else if (pickerIndex == ENABLE_PICKER_INDEX) {
            if (!isTypedTimeFullyLegal()) {
                this.mTypedTimes.clear();
            }
            finishKbMode(true);
        }
    }

    private void setHour(int value, boolean announce) {
        String format;
        if (this.mIs24HourMode) {
            format = "%02d";
        } else {
            format = "%d";
            value %= 12;
            if (value == 0) {
                value = 12;
            }
        }
        Object[] objArr = new Object[PM];
        objArr[HOUR_INDEX] = Integer.valueOf(value);
        CharSequence text = String.format(format, objArr);
        this.mHourView.setText(text);
        this.mHourSpaceView.setText(text);
        if (announce) {
            Utils.tryAccessibilityAnnounce(this.mTimePicker, text);
        }
    }

    private void setMinute(int value) {
        if (value == 60) {
            value = HOUR_INDEX;
        }
        Object[] objArr = new Object[PM];
        objArr[HOUR_INDEX] = Integer.valueOf(value);
        CharSequence text = String.format(Locale.getDefault(), "%02d", objArr);
        Utils.tryAccessibilityAnnounce(this.mTimePicker, text);
        this.mMinuteView.setText(text);
        this.mMinuteSpaceView.setText(text);
    }

    private void setCurrentItemShowing(int index, boolean animateCircle, boolean delayLabelAnimate, boolean announce) {
        TextView labelToAnimate;
        this.mTimePicker.setCurrentItemShowing(index, animateCircle);
        if (index == 0) {
            int hours = this.mTimePicker.getHours();
            if (!this.mIs24HourMode) {
                hours %= 12;
            }
            this.mTimePicker.setContentDescription(this.mHourPickerDescription + ": " + hours);
            if (announce) {
                Utils.tryAccessibilityAnnounce(this.mTimePicker, this.mSelectHours);
            }
            labelToAnimate = this.mHourView;
        } else {
            this.mTimePicker.setContentDescription(this.mMinutePickerDescription + ": " + this.mTimePicker.getMinutes());
            if (announce) {
                Utils.tryAccessibilityAnnounce(this.mTimePicker, this.mSelectMinutes);
            }
            labelToAnimate = this.mMinuteView;
        }
        int hourColor = index == 0 ? this.mBlue : this.mBlack;
        int minuteColor = index == PM ? this.mBlue : this.mBlack;
        this.mHourView.setTextColor(hourColor);
        this.mMinuteView.setTextColor(minuteColor);
        ObjectAnimator pulseAnimator = Utils.getPulseAnimator(labelToAnimate, 0.85f, 1.1f);
        if (delayLabelAnimate) {
            pulseAnimator.setStartDelay(300);
        }
        pulseAnimator.start();
    }

    private boolean processKeyUp(int keyCode) {
        if (keyCode == 111 || keyCode == 4) {
            dismiss();
            return true;
        }
        if (keyCode == 61) {
            if (this.mInKbMode) {
                if (!isTypedTimeFullyLegal()) {
                    return true;
                }
                finishKbMode(true);
                return true;
            }
        } else if (keyCode == 66) {
            if (this.mInKbMode) {
                if (!isTypedTimeFullyLegal()) {
                    return true;
                }
                finishKbMode(false);
            }
            if (this.mCallback != null) {
                this.mCallback.onTimeSet(this.mTimePicker, this.mTimePicker.getHours(), this.mTimePicker.getMinutes());
            }
            dismiss();
            return true;
        } else if (keyCode == 67) {
            if (this.mInKbMode && !this.mTypedTimes.isEmpty()) {
                String deletedKeyStr;
                int deleted = deleteLastTypedKey();
                if (deleted == getAmOrPmKeyCode(HOUR_INDEX)) {
                    deletedKeyStr = this.mAmText;
                } else if (deleted == getAmOrPmKeyCode(PM)) {
                    deletedKeyStr = this.mPmText;
                } else {
                    Object[] objArr = new Object[PM];
                    objArr[HOUR_INDEX] = Integer.valueOf(getValFromKeyCode(deleted));
                    deletedKeyStr = String.format("%d", objArr);
                }
                RadialPickerLayout radialPickerLayout = this.mTimePicker;
                String str = this.mDeletedKeyFormat;
                Object[] objArr2 = new Object[PM];
                objArr2[HOUR_INDEX] = deletedKeyStr;
                Utils.tryAccessibilityAnnounce(radialPickerLayout, String.format(str, objArr2));
                updateDisplay(true);
            }
        } else if (keyCode == 7 || keyCode == 8 || keyCode == 9 || keyCode == 10 || keyCode == 11 || keyCode == 12 || keyCode == 13 || keyCode == 14 || keyCode == 15 || keyCode == 16 || (!this.mIs24HourMode && (keyCode == getAmOrPmKeyCode(HOUR_INDEX) || keyCode == getAmOrPmKeyCode(PM)))) {
            if (this.mInKbMode) {
                if (!addKeyIfLegal(keyCode)) {
                    return true;
                }
                updateDisplay(false);
                return true;
            } else if (this.mTimePicker == null) {
                Log.e(TAG, "Unable to initiate keyboard mode, TimePicker was null.");
                return true;
            } else {
                this.mTypedTimes.clear();
                tryStartingKbMode(keyCode);
                return true;
            }
        }
        return false;
    }

    private void tryStartingKbMode(int keyCode) {
        if (!this.mTimePicker.trySettingInputEnabled(false)) {
            return;
        }
        if (keyCode == -1 || addKeyIfLegal(keyCode)) {
            this.mInKbMode = true;
            this.mDoneButton.setEnabled(false);
            updateDisplay(false);
        }
    }

    private boolean addKeyIfLegal(int keyCode) {
        if (this.mIs24HourMode && this.mTypedTimes.size() == 4) {
            return false;
        }
        if (!this.mIs24HourMode && isTypedTimeFullyLegal()) {
            return false;
        }
        this.mTypedTimes.add(Integer.valueOf(keyCode));
        if (isTypedTimeLegalSoFar()) {
            int val = getValFromKeyCode(keyCode);
            RadialPickerLayout radialPickerLayout = this.mTimePicker;
            Object[] objArr = new Object[PM];
            objArr[HOUR_INDEX] = Integer.valueOf(val);
            Utils.tryAccessibilityAnnounce(radialPickerLayout, String.format("%d", objArr));
            if (isTypedTimeFullyLegal()) {
                if (!this.mIs24HourMode && this.mTypedTimes.size() <= ENABLE_PICKER_INDEX) {
                    this.mTypedTimes.add(this.mTypedTimes.size() - 1, Integer.valueOf(7));
                    this.mTypedTimes.add(this.mTypedTimes.size() - 1, Integer.valueOf(7));
                }
                this.mDoneButton.setEnabled(true);
            }
            return true;
        }
        deleteLastTypedKey();
        return false;
    }

    private boolean isTypedTimeLegalSoFar() {
        Node node = this.mLegalTimesTree;
        Iterator it = this.mTypedTimes.iterator();
        while (it.hasNext()) {
            node = node.canReach(((Integer) it.next()).intValue());
            if (node == null) {
                return false;
            }
        }
        return true;
    }

    private boolean isTypedTimeFullyLegal() {
        if (this.mIs24HourMode) {
            int[] values = getEnteredTime(null);
            if (values[HOUR_INDEX] < 0 || values[PM] < 0 || values[PM] >= 60) {
                return false;
            }
            return true;
        } else if (this.mTypedTimes.contains(Integer.valueOf(getAmOrPmKeyCode(HOUR_INDEX))) || this.mTypedTimes.contains(Integer.valueOf(getAmOrPmKeyCode(PM)))) {
            return true;
        } else {
            return false;
        }
    }

    private int deleteLastTypedKey() {
        int deleted = ((Integer) this.mTypedTimes.remove(this.mTypedTimes.size() - 1)).intValue();
        if (!isTypedTimeFullyLegal()) {
            this.mDoneButton.setEnabled(false);
        }
        return deleted;
    }

    private void finishKbMode(boolean updateDisplays) {
        this.mInKbMode = false;
        if (!this.mTypedTimes.isEmpty()) {
            int[] values = getEnteredTime(null);
            this.mTimePicker.setTime(values[HOUR_INDEX], values[PM]);
            if (!this.mIs24HourMode) {
                this.mTimePicker.setAmOrPm(values[AMPM_INDEX]);
            }
            this.mTypedTimes.clear();
        }
        if (updateDisplays) {
            updateDisplay(false);
            this.mTimePicker.trySettingInputEnabled(true);
        }
    }

    private void updateDisplay(boolean allowEmptyDisplay) {
        if (allowEmptyDisplay || !this.mTypedTimes.isEmpty()) {
            String hourStr;
            Object[] objArr;
            String minuteStr;
            Boolean[] enteredZeros = new Boolean[AMPM_INDEX];
            enteredZeros[HOUR_INDEX] = Boolean.valueOf(false);
            enteredZeros[PM] = Boolean.valueOf(false);
            int[] values = getEnteredTime(enteredZeros);
            String hourFormat = enteredZeros[HOUR_INDEX].booleanValue() ? "%02d" : "%2d";
            String minuteFormat = enteredZeros[PM].booleanValue() ? "%02d" : "%2d";
            if (values[HOUR_INDEX] == -1) {
                hourStr = this.mDoublePlaceholderText;
            } else {
                objArr = new Object[PM];
                objArr[HOUR_INDEX] = Integer.valueOf(values[HOUR_INDEX]);
                hourStr = String.format(hourFormat, objArr).replace(' ', this.mPlaceholderText);
            }
            if (values[PM] == -1) {
                minuteStr = this.mDoublePlaceholderText;
            } else {
                objArr = new Object[PM];
                objArr[HOUR_INDEX] = Integer.valueOf(values[PM]);
                minuteStr = String.format(minuteFormat, objArr).replace(' ', this.mPlaceholderText);
            }
            this.mHourView.setText(hourStr);
            this.mHourSpaceView.setText(hourStr);
            this.mHourView.setTextColor(this.mBlack);
            this.mMinuteView.setText(minuteStr);
            this.mMinuteSpaceView.setText(minuteStr);
            this.mMinuteView.setTextColor(this.mBlack);
            if (!this.mIs24HourMode) {
                updateAmPmDisplay(values[AMPM_INDEX]);
                return;
            }
            return;
        }
        int hour = this.mTimePicker.getHours();
        int minute = this.mTimePicker.getMinutes();
        setHour(hour, true);
        setMinute(minute);
        if (!this.mIs24HourMode) {
            updateAmPmDisplay(hour < 12 ? HOUR_INDEX : PM);
        }
        setCurrentItemShowing(this.mTimePicker.getCurrentItemShowing(), true, true, true);
        this.mDoneButton.setEnabled(true);
    }

    private int getValFromKeyCode(int keyCode) {
        switch (keyCode) {
            case UserListView.TYPE_FAVE_LINKS /*7*/:
                return HOUR_INDEX;
            case UserListView.TYPE_BLACKLIST /*8*/:
                return PM;
            case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                return AMPM_INDEX;
            case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                return ENABLE_PICKER_INDEX;
            case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                return 4;
            case Strings.UPDATE_DIALOG_POSITIVE_BUTTON_ID /*12*/:
                return 5;
            case Strings.EXPIRY_INFO_TITLE_ID /*13*/:
                return 6;
            case Strings.EXPIRY_INFO_TEXT_ID /*14*/:
                return 7;
            case Strings.FEEDBACK_FAILED_TITLE_ID /*15*/:
                return 8;
            case TransportMediator.FLAG_KEY_MEDIA_PAUSE /*16*/:
                return 9;
            default:
                return -1;
        }
    }

    private int[] getEnteredTime(Boolean[] enteredZeros) {
        int amOrPm = -1;
        int startIndex = PM;
        if (!this.mIs24HourMode && isTypedTimeFullyLegal()) {
            int keyCode = ((Integer) this.mTypedTimes.get(this.mTypedTimes.size() - 1)).intValue();
            if (keyCode == getAmOrPmKeyCode(HOUR_INDEX)) {
                amOrPm = HOUR_INDEX;
            } else if (keyCode == getAmOrPmKeyCode(PM)) {
                amOrPm = PM;
            }
            startIndex = AMPM_INDEX;
        }
        int minute = -1;
        int hour = -1;
        for (int i = startIndex; i <= this.mTypedTimes.size(); i += PM) {
            int val = getValFromKeyCode(((Integer) this.mTypedTimes.get(this.mTypedTimes.size() - i)).intValue());
            if (i == startIndex) {
                minute = val;
            } else if (i == startIndex + PM) {
                minute += val * 10;
                if (enteredZeros != null && val == 0) {
                    enteredZeros[PM] = Boolean.valueOf(true);
                }
            } else if (i == startIndex + AMPM_INDEX) {
                hour = val;
            } else if (i == startIndex + ENABLE_PICKER_INDEX) {
                hour += val * 10;
                if (enteredZeros != null && val == 0) {
                    enteredZeros[HOUR_INDEX] = Boolean.valueOf(true);
                }
            }
        }
        int[] ret = new int[ENABLE_PICKER_INDEX];
        ret[HOUR_INDEX] = hour;
        ret[PM] = minute;
        ret[AMPM_INDEX] = amOrPm;
        return ret;
    }

    private int getAmOrPmKeyCode(int amOrPm) {
        if (this.mAmKeyCode == -1 || this.mPmKeyCode == -1) {
            KeyCharacterMap kcm = KeyCharacterMap.load(-1);
            int i = HOUR_INDEX;
            while (i < Math.max(this.mAmText.length(), this.mPmText.length())) {
                char amChar = this.mAmText.toLowerCase(Locale.getDefault()).charAt(i);
                char pmChar = this.mPmText.toLowerCase(Locale.getDefault()).charAt(i);
                if (amChar != pmChar) {
                    char[] cArr = new char[AMPM_INDEX];
                    cArr[HOUR_INDEX] = amChar;
                    cArr[PM] = pmChar;
                    KeyEvent[] events = kcm.getEvents(cArr);
                    if (events == null || events.length != 4) {
                        Log.e(TAG, "Unable to find keycodes for AM and PM.");
                    } else {
                        this.mAmKeyCode = events[HOUR_INDEX].getKeyCode();
                        this.mPmKeyCode = events[AMPM_INDEX].getKeyCode();
                    }
                } else {
                    i += PM;
                }
            }
        }
        if (amOrPm == 0) {
            return this.mAmKeyCode;
        }
        if (amOrPm == PM) {
            return this.mPmKeyCode;
        }
        return -1;
    }

    public void setDoneButtonText(String text) {
        this.doneButtonText = text;
        if (this.mDoneButton != null) {
            this.mDoneButton.setText(text);
        }
    }

    private void generateLegalTimesTree() {
        this.mLegalTimesTree = new Node(new int[HOUR_INDEX]);
        if (this.mIs24HourMode) {
            Node node = new Node(7, 8, 9, 10, 11, 12);
            node = new Node(7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            node.addChild(node);
            int[] iArr = new int[AMPM_INDEX];
            iArr[HOUR_INDEX] = 7;
            iArr[PM] = 8;
            Node firstDigit = new Node(iArr);
            this.mLegalTimesTree.addChild(firstDigit);
            node = new Node(7, 8, 9, 10, 11, 12);
            firstDigit.addChild(node);
            node.addChild(node);
            node.addChild(new Node(13, 14, 15, 16));
            node = new Node(13, 14, 15, 16);
            firstDigit.addChild(node);
            node.addChild(node);
            iArr = new int[PM];
            iArr[HOUR_INDEX] = 9;
            firstDigit = new Node(iArr);
            this.mLegalTimesTree.addChild(firstDigit);
            node = new Node(7, 8, 9, 10);
            firstDigit.addChild(node);
            node.addChild(node);
            iArr = new int[AMPM_INDEX];
            iArr[HOUR_INDEX] = 11;
            iArr[PM] = 12;
            node = new Node(iArr);
            firstDigit.addChild(node);
            node.addChild(node);
            firstDigit = new Node(10, 11, 12, 13, 14, 15, 16);
            this.mLegalTimesTree.addChild(firstDigit);
            firstDigit.addChild(node);
            return;
        }
        iArr = new int[AMPM_INDEX];
        iArr[HOUR_INDEX] = getAmOrPmKeyCode(HOUR_INDEX);
        iArr[PM] = getAmOrPmKeyCode(PM);
        Node ampm = new Node(iArr);
        iArr = new int[PM];
        iArr[HOUR_INDEX] = 8;
        firstDigit = new Node(iArr);
        this.mLegalTimesTree.addChild(firstDigit);
        firstDigit.addChild(ampm);
        iArr = new int[ENABLE_PICKER_INDEX];
        iArr[HOUR_INDEX] = 7;
        iArr[PM] = 8;
        iArr[AMPM_INDEX] = 9;
        node = new Node(iArr);
        firstDigit.addChild(node);
        node.addChild(ampm);
        node = new Node(7, 8, 9, 10, 11, 12);
        node.addChild(node);
        node.addChild(ampm);
        Node fourthDigit = new Node(7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
        node.addChild(fourthDigit);
        fourthDigit.addChild(ampm);
        node = new Node(13, 14, 15, 16);
        node.addChild(node);
        node.addChild(ampm);
        iArr = new int[ENABLE_PICKER_INDEX];
        iArr[HOUR_INDEX] = 10;
        iArr[PM] = 11;
        iArr[AMPM_INDEX] = 12;
        node = new Node(iArr);
        firstDigit.addChild(node);
        node = new Node(7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
        node.addChild(node);
        node.addChild(ampm);
        firstDigit = new Node(9, 10, 11, 12, 13, 14, 15, 16);
        this.mLegalTimesTree.addChild(firstDigit);
        firstDigit.addChild(ampm);
        node = new Node(7, 8, 9, 10, 11, 12);
        firstDigit.addChild(node);
        node = new Node(7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
        node.addChild(node);
        node.addChild(ampm);
    }
}
