package com.fourmob.datetimepicker;

/* renamed from: com.fourmob.datetimepicker.R */
public final class C0083R {

    /* renamed from: com.fourmob.datetimepicker.R.color */
    public static final class color {
        public static final int ampm_text_color = 2131099658;
        public static final int blue = 2131099653;
        public static final int calendar_header = 2131099656;
        public static final int circle_background = 2131099649;
        public static final int darker_blue = 2131099654;
        public static final int date_picker_selector = 2131099681;
        public static final int date_picker_text_normal = 2131099655;
        public static final int date_picker_view_animator = 2131099657;
        public static final int date_picker_year_selector = 2131099682;
        public static final int done_text_color = 2131099685;
        public static final int done_text_color_disabled = 2131099652;
        public static final int done_text_color_normal = 2131099651;
        public static final int line_background = 2131099650;
        public static final int numbers_text_color = 2131099659;
        public static final int transparent_black = 2131099660;
        public static final int white = 2131099648;
    }

    /* renamed from: com.fourmob.datetimepicker.R.dimen */
    public static final class dimen {
        public static final int ampm_label_size = 2131165203;
        public static final int ampm_left_padding = 2131165204;
        public static final int date_picker_component_width = 2131165185;
        public static final int date_picker_header_height = 2131165186;
        public static final int date_picker_header_text_size = 2131165196;
        public static final int date_picker_view_animator_height = 2131165188;
        public static final int day_number_select_circle_radius = 2131165191;
        public static final int day_number_size = 2131165198;
        public static final int dialog_height = 2131165210;
        public static final int done_label_size = 2131165184;
        public static final int extra_time_label_margin = 2131165202;
        public static final int footer_height = 2131165214;
        public static final int header_height = 2131165206;
        public static final int left_side_width = 2131165211;
        public static final int minimum_margin_sides = 2131165208;
        public static final int minimum_margin_top_bottom = 2131165209;
        public static final int month_day_label_text_size = 2131165190;
        public static final int month_label_size = 2131165197;
        public static final int month_list_item_header_height = 2131165189;
        public static final int month_list_item_padding = 2131165212;
        public static final int month_list_item_size = 2131165213;
        public static final int month_select_circle_radius = 2131165192;
        public static final int picker_dimen = 2131165207;
        public static final int selected_calendar_layout_height = 2131165187;
        public static final int selected_date_day_size = 2131165194;
        public static final int selected_date_month_size = 2131165195;
        public static final int selected_date_year_size = 2131165193;
        public static final int separator_padding = 2131165205;
        public static final int time_label_right_padding = 2131165215;
        public static final int time_label_size = 2131165201;
        public static final int year_label_height = 2131165199;
        public static final int year_label_text_size = 2131165200;
    }

    /* renamed from: com.fourmob.datetimepicker.R.id */
    public static final class id {
        public static final int ampm_hitspace = 2131296695;
        public static final int ampm_label = 2131296696;
        public static final int animator = 2131296263;
        public static final int center_view = 2131296689;
        public static final int date_picker_day = 2131296261;
        public static final int date_picker_header = 2131296257;
        public static final int date_picker_month = 2131296260;
        public static final int date_picker_month_and_day = 2131296259;
        public static final int date_picker_year = 2131296262;
        public static final int day_picker_selected_date_layout = 2131296258;
        public static final int done = 2131296256;
        public static final int done_button = 2131296699;
        public static final int hour_space = 2131296690;
        public static final int hours = 2131296692;
        public static final int minutes = 2131296694;
        public static final int minutes_space = 2131296693;
        public static final int month_text_view = 2131296264;
        public static final int separator = 2131296691;
        public static final int time_picker = 2131296698;
        public static final int time_picker_dialog = 2131296697;
    }

    /* renamed from: com.fourmob.datetimepicker.R.layout */
    public static final class layout {
        public static final int date_picker_dialog = 2130903093;
        public static final int date_picker_done_button = 2130903094;
        public static final int date_picker_header_view = 2130903095;
        public static final int date_picker_selected_date = 2130903096;
        public static final int date_picker_view_animator = 2130903097;
        public static final int time_header_label = 2130903180;
        public static final int time_picker_dialog = 2130903181;
        public static final int year_label_text_view = 2130903192;
    }

    /* renamed from: com.fourmob.datetimepicker.R.string */
    public static final class string {
        public static final int ampm_circle_radius_multiplier = 2131230723;
        public static final int circle_radius_multiplier = 2131230720;
        public static final int circle_radius_multiplier_24HourMode = 2131230721;
        public static final int day_of_week_label_typeface = 2131230737;
        public static final int day_picker_description = 2131230731;
        public static final int deleted_key = 2131230740;
        public static final int done_label = 2131230730;
        public static final int hour_picker_description = 2131230741;
        public static final int item_is_selected = 2131230735;
        public static final int minute_picker_description = 2131230742;
        public static final int numbers_radius_multiplier_inner = 2131230725;
        public static final int numbers_radius_multiplier_normal = 2131230724;
        public static final int numbers_radius_multiplier_outer = 2131230726;
        public static final int radial_numbers_typeface = 2131230743;
        public static final int sans_serif = 2131230736;
        public static final int select_day = 2131230733;
        public static final int select_hours = 2131230744;
        public static final int select_minutes = 2131230745;
        public static final int select_year = 2131230734;
        public static final int selection_radius_multiplier = 2131230722;
        public static final int text_size_multiplier_inner = 2131230728;
        public static final int text_size_multiplier_normal = 2131230727;
        public static final int text_size_multiplier_outer = 2131230729;
        public static final int time_placeholder = 2131230738;
        public static final int time_separator = 2131230739;
        public static final int year_picker_description = 2131230732;
    }

    /* renamed from: com.fourmob.datetimepicker.R.style */
    public static final class style {
        public static final int AppBaseTheme = 2131361792;
        public static final int AppTheme = 2131361793;
        public static final int ampm_label = 2131361795;
        public static final int day_of_week_label_condensed = 2131361796;
        public static final int done_button_light = 2131361798;
        public static final int time_label = 2131361794;
        public static final int time_label_thin = 2131361797;
    }
}
