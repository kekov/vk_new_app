package com.fourmob.datetimepicker;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.os.Build.VERSION;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import net.hockeyapp.android.Strings;

public class Utils {
    public static int getDaysInMonth(int month, int year) {
        switch (month) {
            case ValidationActivity.VRESULT_NONE /*0*/:
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
            case UserListView.TYPE_FAVE /*4*/:
            case UserListView.TYPE_POLL_VOTERS /*6*/:
            case UserListView.TYPE_FAVE_LINKS /*7*/:
            case NewPostActivity.POLL_EDIT_RESULT /*9*/:
            case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                return 31;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                if (year % 4 == 0) {
                    return 29;
                }
                return 28;
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
            case UserListView.TYPE_FOLLOWERS /*5*/:
            case UserListView.TYPE_BLACKLIST /*8*/:
            case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                return 30;
            default:
                throw new IllegalArgumentException("Invalid Month");
        }
    }

    public static ObjectAnimator getPulseAnimator(View view, float animVal1, float animVal2) {
        Keyframe keyframe1 = Keyframe.ofFloat(0.0f, 1.0f);
        Keyframe keyframe2 = Keyframe.ofFloat(0.275f, animVal1);
        Keyframe keyframe3 = Keyframe.ofFloat(0.69f, animVal2);
        Keyframe keyframe4 = Keyframe.ofFloat(1.0f, 1.0f);
        r5 = new PropertyValuesHolder[2];
        r5[0] = PropertyValuesHolder.ofKeyframe("scaleX", new Keyframe[]{keyframe1, keyframe2, keyframe3, keyframe4});
        r5[1] = PropertyValuesHolder.ofKeyframe("scaleY", new Keyframe[]{keyframe1, keyframe2, keyframe3, keyframe4});
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(view, r5);
        animator.setDuration(544);
        return animator;
    }

    public static boolean isJellybeanOrLater() {
        return VERSION.SDK_INT >= 16;
    }

    public static void tryAccessibilityAnnounce(Object obj, Object announcement) {
    }

    public static boolean isTouchExplorationEnabled(AccessibilityManager accessibilityManager) {
        if (VERSION.SDK_INT >= 14) {
            return accessibilityManager.isTouchExplorationEnabled();
        }
        return false;
    }
}
