package com.vkontakte.android;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.preference.PreferenceManager;
import com.facebook.WebDialog;
import com.google.android.gcm.GCMConstants;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.background.WorkerThread;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Semaphore;
import java.util.zip.GZIPInputStream;
import org.acra.ACRAConstants;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class APIController {
    public static final boolean API_DEBUG;
    public static final String API_URL;
    private static final long COUNTER_RESET_TIME = 2000;
    private static final long MAX_REQUESTS_PER_COUNT = 10;
    public static final String USER_AGENT;
    private static WorkerThread bgThread;
    private static long counterResetTime;
    private static HttpClient httpclient;
    private static LinkedList<APIRequest> queue;
    private static WorkerThread reqThread;
    private static int requestCounter;
    private static Semaphore validation;

    private static class RequestRunner implements Runnable {
        APIRequest f41r;

        /* renamed from: com.vkontakte.android.APIController.RequestRunner.1 */
        class C01571 implements Runnable {
            private final /* synthetic */ boolean val$networkFail;
            private final /* synthetic */ Object val$result;

            C01571(Object obj, boolean z) {
                this.val$result = obj;
                this.val$networkFail = z;
            }

            public void run() {
                if (this.val$result != null) {
                    try {
                        if (!((this.val$result instanceof ErrorResponse) && ((ErrorResponse) this.val$result).errorCode == 14)) {
                            RequestRunner.this.f41r.invokeCallback(this.val$result);
                        }
                    } catch (Exception x) {
                        Log.m531w("vk", "Callback exception", x);
                        RequestRunner.this.f41r.invokeCallback(new ErrorResponse(-3, "Callback invocation failed (parse error?)"));
                    }
                } else if (this.val$networkFail) {
                    RequestRunner.this.f41r.invokeCallback(new ErrorResponse(-1, "I/O Error"));
                } else {
                    RequestRunner.this.f41r.invokeCallback(new ErrorResponse(-2, "Response parse failed"));
                }
                if (RequestRunner.this.f41r.progressDialog != null && RequestRunner.this.f41r.progressDialog.isShowing()) {
                    RequestRunner.this.f41r.progressDialog.dismiss();
                }
            }
        }

        public RequestRunner(APIRequest req) {
            this.f41r = req;
        }

        public void run() {
            if (!this.f41r.isCanceled()) {
                try {
                    JSONObject res = this.f41r.doExec();
                    if (res == null) {
                        res = APIController.runRequest(this.f41r);
                    }
                    boolean networkFail = res == null ? true : APIController.API_DEBUG;
                    Object result = res != null ? this.f41r.parseResponse(res) : null;
                    if (!this.f41r.isCanceled()) {
                        Runnable rs = new C01571(result, networkFail);
                        if (this.f41r.uiHandler != null) {
                            this.f41r.uiHandler.post(rs);
                        } else {
                            rs.run();
                        }
                    }
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                }
            }
        }
    }

    static {
        boolean z;
        queue = new LinkedList();
        reqThread = new WorkerThread("API Main Thread");
        bgThread = new WorkerThread("API Background Thread");
        if (PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getBoolean("__dbg_api", API_DEBUG) || Global.uid == 25719408) {
            z = true;
        } else {
            z = API_DEBUG;
        }
        API_DEBUG = z;
        API_URL = new StringBuilder(String.valueOf(PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getString("apiHost", "api.vk.com"))).append("/method/").toString();
        validation = new Semaphore(1, true);
        requestCounter = 0;
        counterResetTime = System.currentTimeMillis();
        String str = ACRAConstants.DEFAULT_STRING_VALUE;
        int appbuild = 0;
        try {
            PackageInfo pkgInfo = VKApplication.context.getPackageManager().getPackageInfo(VKApplication.context.getPackageName(), 0);
            str = pkgInfo.versionName;
            appbuild = pkgInfo.versionCode;
        } catch (Exception e) {
        }
        USER_AGENT = String.format(Locale.US, "VKAndroidApp/%s-%d (Android %s; SDK %d; %s; %s %s; %s)", new Object[]{str, Integer.valueOf(appbuild), VERSION.RELEASE, Integer.valueOf(VERSION.SDK_INT), Build.CPU_ABI, Build.MANUFACTURER, Build.MODEL, System.getProperty("user.language")});
        reqThread.start();
        bgThread.start();
    }

    public static void executeRequest(APIRequest req) {
        (req.background ? bgThread : reqThread).postRunnable(new RequestRunner(req), 0);
    }

    static JSONObject runRequest(APIRequest req) {
        JSONObject runRequest;
        try {
            validation.acquire();
            validation.release();
        } catch (Exception e) {
        }
        while (true) {
            if (((long) requestCounter) < MAX_REQUESTS_PER_COUNT) {
                break;
            }
            if (System.currentTimeMillis() - counterResetTime > COUNTER_RESET_TIME) {
                requestCounter = 0;
                counterResetTime = System.currentTimeMillis();
            }
            try {
                Thread.sleep(COUNTER_RESET_TIME);
            } catch (Exception e2) {
            }
        }
        requestCounter++;
        long time = System.currentTimeMillis();
        req.initTime = time;
        if (httpclient == null) {
            HttpParams hParams = new BasicHttpParams();
            HttpProtocolParams.setUseExpectContinue(hParams, API_DEBUG);
            HttpProtocolParams.setUserAgent(hParams, USER_AGENT);
            HttpConnectionParams.setSocketBufferSize(hParams, ACRAConstants.DEFAULT_BUFFER_SIZE_IN_BYTES);
            HttpConnectionParams.setConnectionTimeout(hParams, 30000);
            HttpConnectionParams.setSoTimeout(hParams, 30000);
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            httpclient = new DefaultHttpClient(new ThreadSafeClientConnManager(hParams, registry), hParams);
        }
        Object obj = (PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getBoolean("useHTTPS", API_DEBUG) || req.forceHTTPS || Global.accessToken == null) ? "https" : "http";
        HttpPost httppost = new HttpPost(new StringBuilder(String.valueOf(obj)).append("://").append(API_URL).append((String) req.params.get("method")).toString());
        req.httppost = httppost;
        httppost.addHeader("Accept-Encoding", "gzip");
        JSONObject obj2 = null;
        HttpResponse response = null;
        InputStream is = null;
        try {
            if (Global.accessToken != null) {
                req.params.put(WebDialog.DIALOG_PARAM_ACCESS_TOKEN, Global.accessToken);
            }
            List<NameValuePair> arrayList = new ArrayList(2);
            Enumeration<String> e3 = req.params.keys();
            if (API_DEBUG) {
                Log.m525d("vk", "=====" + ((String) req.params.get("method")));
            }
            while (e3.hasMoreElements()) {
                String key = (String) e3.nextElement();
                if (!key.equals("method")) {
                    if (API_DEBUG) {
                        Log.m525d("vk", new StringBuilder(String.valueOf(key)).append("=").append((String) req.params.get(key)).toString());
                    }
                    arrayList.add(new BasicNameValuePair(key, (String) req.params.get(key)));
                }
            }
            if (API_DEBUG) {
                Log.m525d("vk", "=====");
            }
            if (!(Global.accessToken == null || Global.secret == null)) {
                arrayList.add(new BasicNameValuePair("sig", req.getSig()));
            }
            httppost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            if (API_DEBUG) {
                Log.m529v("vk", "[" + ((String) req.params.get("method")) + "] Prepare: " + (System.currentTimeMillis() - time));
                time = System.currentTimeMillis();
            }
            response = httpclient.execute(httppost);
            if (req.isCanceled()) {
                if (response != null) {
                    try {
                        response.getEntity().consumeContent();
                    } catch (Exception e4) {
                    }
                }
                if (is != null) {
                    try {
                        is.close();
                    } catch (Exception e5) {
                    }
                }
                return null;
            }
            int code;
            if (API_DEBUG) {
                Log.m529v("vk", "[" + ((String) req.params.get("method")) + "] Execute: " + (System.currentTimeMillis() - time));
                time = System.currentTimeMillis();
            }
            is = response.getEntity().getContent();
            Header contentEncoding = response.getFirstHeader("Content-Encoding");
            if (contentEncoding != null && "gzip".equalsIgnoreCase(contentEncoding.getValue())) {
                is = new GZIPInputStream(is);
            }
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            byte[] rd = new byte[GLRenderBuffer.EGL_SURFACE_SIZE];
            while (true) {
                int l = is.read(rd);
                if (l <= 0) {
                    break;
                }
                buf.write(rd, 0, l);
            }
            String str = new String(buf.toByteArray(), "UTF-8");
            is.close();
            if (API_DEBUG) {
                Log.m529v("vk", "[" + ((String) req.params.get("method")) + "] Receive: " + (System.currentTimeMillis() - time));
                time = System.currentTimeMillis();
            }
            if (API_DEBUG) {
                Log.m529v("vk", "Resp status=" + response.getStatusLine().toString());
                Log.m529v("vk", "Response=" + str);
            }
            req.httppost = null;
            response = null;
            is = null;
            obj2 = (JSONObject) new JSONTokener(str).nextValue();
            JSONObject error = obj2.optJSONObject(GCMConstants.EXTRA_ERROR);
            if (obj2.has("execute_errors")) {
                JSONArray errs = obj2.getJSONArray("execute_errors");
                for (int i = 0; i < errs.length(); i++) {
                    JSONObject err = errs.getJSONObject(i);
                    code = err.getInt("error_code");
                    if (code == 14 || code == 17) {
                        error = err;
                        break;
                    }
                }
            }
            if (error != null) {
                code = error.getInt("error_code");
                if (code == 5) {
                    if ("account.unregisterDevice".equals(req.params.get("method"))) {
                        throw new APIException(0, "already unregistered");
                    }
                    if (!"account.unregisterDevice".equals(req.params.get("method"))) {
                        LongPollService.onReauthError();
                    }
                    throw new APIException(0, "reauth error");
                } else if (code == 7) {
                    if (response != null) {
                        try {
                            response.getEntity().consumeContent();
                        } catch (Exception e6) {
                        }
                    }
                    if (is != null) {
                        try {
                            is.close();
                        } catch (Exception e7) {
                        }
                    }
                    return obj2;
                } else if (code == 14) {
                    intent = new Intent(VKApplication.context, CaptchaActivity.class);
                    intent.addFlags(268435456);
                    intent.putExtra(PlusShare.KEY_CALL_TO_ACTION_URL, error.getString("captcha_img"));
                    VKApplication.context.startActivity(intent);
                    while (!CaptchaActivity.isReady) {
                        Thread.sleep(100);
                    }
                    CaptchaActivity.isReady = API_DEBUG;
                    if (CaptchaActivity.lastKey != null) {
                        req.params.put("captcha_sid", error.getString("captcha_sid"));
                        req.params.put("captcha_key", CaptchaActivity.lastKey);
                        if (API_DEBUG) {
                            Log.m525d("vk", "Resending request " + ((String) req.params.get("method")));
                        }
                        runRequest = runRequest(req);
                        if (response != null) {
                            try {
                                response.getEntity().consumeContent();
                            } catch (Exception e8) {
                            }
                        }
                        if (is == null) {
                            return runRequest;
                        }
                        try {
                            is.close();
                            return runRequest;
                        } catch (Exception e9) {
                            return runRequest;
                        }
                    }
                    if (response != null) {
                        try {
                            response.getEntity().consumeContent();
                        } catch (Exception e10) {
                        }
                    }
                    if (is != null) {
                        try {
                            is.close();
                        } catch (Exception e11) {
                        }
                    }
                    return obj2;
                } else if (code == 16) {
                    if (API_DEBUG) {
                        Log.m530w("vk", "Force HTTPS");
                    }
                    PreferenceManager.getDefaultSharedPreferences(VKApplication.context).edit().putBoolean("useHTTPS", true).commit();
                    VKApplication.context.getSharedPreferences(null, 0).edit().putBoolean("forceHTTPS", true).commit();
                    runRequest = runRequest(req);
                    if (response != null) {
                        try {
                            response.getEntity().consumeContent();
                        } catch (Exception e12) {
                        }
                    }
                    if (is == null) {
                        return runRequest;
                    }
                    try {
                        is.close();
                        return runRequest;
                    } catch (Exception e13) {
                        return runRequest;
                    }
                } else if (code == 17) {
                    if (API_DEBUG) {
                        Log.m530w("vk", "Need validation");
                    }
                    try {
                        validation.acquire();
                    } catch (Exception e14) {
                    }
                    intent = new Intent(VKApplication.context, ValidationActivity.class);
                    intent.addFlags(268435456);
                    intent.putExtra(PlusShare.KEY_CALL_TO_ACTION_URL, error.getString(WebDialog.DIALOG_PARAM_REDIRECT_URI));
                    ValidationActivity.result = 0;
                    VKApplication.context.startActivity(intent);
                    while (ValidationActivity.result == 0) {
                        Thread.sleep(100);
                    }
                    if (ValidationActivity.result == 2) {
                        ValidationActivity.result = 0;
                        if (API_DEBUG) {
                            Log.m528i("vk", "Repeating request " + ((String) req.params.get("method")));
                        }
                        validation.release();
                        runRequest = runRequest(req);
                        if (response != null) {
                            try {
                                response.getEntity().consumeContent();
                            } catch (Exception e15) {
                            }
                        }
                        if (is == null) {
                            return runRequest;
                        }
                        try {
                            is.close();
                            return runRequest;
                        } catch (Exception e16) {
                            return runRequest;
                        }
                    }
                    ValidationActivity.result = 0;
                    validation.release();
                    if (response != null) {
                        try {
                            response.getEntity().consumeContent();
                        } catch (Exception e17) {
                        }
                    }
                    if (is != null) {
                        try {
                            is.close();
                        } catch (Exception e18) {
                        }
                    }
                    return obj2;
                } else {
                    throw new APIException(code, error.getString("error_msg"));
                }
            }
            if (API_DEBUG) {
                Log.m529v("vk", "[" + ((String) req.params.get("method")) + "] Parse JSON: " + (System.currentTimeMillis() - time));
                time = System.currentTimeMillis();
            }
            if (response != null) {
                try {
                    response.getEntity().consumeContent();
                } catch (Exception e19) {
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e20) {
                }
            }
            return obj2;
        } catch (Throwable e21) {
            Log.m532w("vk", e21);
            if (response != null) {
                try {
                    response.getEntity().consumeContent();
                } catch (Exception e22) {
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e23) {
                }
            }
            return null;
        } catch (APIException e24) {
            if (response != null) {
                try {
                    response.getEntity().consumeContent();
                } catch (Exception e25) {
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e26) {
                }
            }
            return obj2;
        } catch (Throwable th) {
            if (response != null) {
                try {
                    response.getEntity().consumeContent();
                } catch (Exception e27) {
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e28) {
                }
            }
        }
    }

    public static void runInBg(Runnable runnable) {
        bgThread.postRunnable(runnable, 0);
    }

    public static void runInApi(Runnable runnable) {
        reqThread.postRunnable(runnable, 0);
    }
}
