package com.vkontakte.android;

import android.app.Activity;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.view.MotionEvent;
import android.widget.TextView;

public class FixedLinkMovementMethod extends LinkMovementMethod {
    private static FixedLinkMovementMethod sInstance;

    private FixedLinkMovementMethod() {
    }

    public static MovementMethod getInstance() {
        if (sInstance != null) {
            return sInstance;
        }
        MovementMethod fixedLinkMovementMethod = new FixedLinkMovementMethod();
        sInstance = fixedLinkMovementMethod;
        return fixedLinkMovementMethod;
    }

    public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
        int action = event.getAction();
        if (action == 1 || action == 0) {
            int x = (((int) event.getX()) - widget.getTotalPaddingLeft()) + widget.getScrollX();
            int y = (((int) event.getY()) - widget.getTotalPaddingTop()) + widget.getScrollY();
            Layout layout = widget.getLayout();
            int off = layout.getOffsetForHorizontal(layout.getLineForVertical(y), (float) x);
            ClickableSpan[] link = (ClickableSpan[]) buffer.getSpans(off, off, ClickableSpan.class);
            if (link.length == 0) {
                event.setLocation(0.0f, event.getY());
                return ((Activity) widget.getContext()).getWindow().getDecorView().onTouchEvent(event);
            } else if (action == 1) {
                link[0].onClick(widget);
                return true;
            } else if (action != 0) {
                return true;
            } else {
                Selection.setSelection(buffer, buffer.getSpanStart(link[0]), buffer.getSpanEnd(link[0]));
                return true;
            }
        }
        event.setLocation(0.0f, event.getY());
        return ((Activity) widget.getContext()).getWindow().getDecorView().onTouchEvent(event);
    }

    public boolean canSelectArbitrarily() {
        return false;
    }
}
