package com.vkontakte.android;

public final class Manifest {

    public static final class permission {
        public static final String ACCESS_DATA = "com.vkontakte.android.permission.ACCESS_DATA";
        public static final String C2D_MESSAGE = "com.vkontakte.android.permission.C2D_MESSAGE";
        public static final String MAPS_RECEIVE = "com.vkontakte.android.permission.MAPS_RECEIVE";
    }
}
