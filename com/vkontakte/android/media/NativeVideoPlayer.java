package com.vkontakte.android.media;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioTrack;
import android.os.Build.VERSION;
import android.view.SurfaceHolder;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.Log;
import com.vkontakte.android.NewsEntry;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.TimerTask;

public class NativeVideoPlayer implements Runnable {
    private float audioDelay;
    private AudioTrack audioTrack;
    private Bitmap bmp;
    private int bufReadPos;
    private int bufWritePos;
    private byte[] buffer;
    private Object bufferLock;
    private boolean buffering;
    private int bufferingOffset;
    private int bufsize;
    public Callback callback;
    private URLConnection conn;
    private Context context;
    private String file;
    private int fps;
    private TimerTask fpsTimerTask;
    private Object frameLock;
    private int f52h;
    private SurfaceHolder holder;
    private boolean isCompleted;
    private long lastDelayUpdate;
    private int len;
    private int minAudioBufferSize;
    private boolean mono;
    private boolean needStopBuffering;
    private int tfps;
    private boolean useGL;
    private int f53w;

    /* renamed from: com.vkontakte.android.media.NativeVideoPlayer.1 */
    class C08501 extends TimerTask {
        C08501() {
        }

        public void run() {
            NativeVideoPlayer.this.fps = NativeVideoPlayer.this.tfps;
            NativeVideoPlayer.this.tfps = 0;
        }
    }

    /* renamed from: com.vkontakte.android.media.NativeVideoPlayer.2 */
    class C08512 implements android.view.SurfaceHolder.Callback {
        C08512() {
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
        }

        public void surfaceCreated(SurfaceHolder holder) {
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            if (NativeVideoPlayer.this.bmp != null) {
                Paint bpaint = new Paint();
                bpaint.setFilterBitmap(true);
                Canvas c = holder.lockCanvas();
                if (c != null) {
                    c.drawBitmap(NativeVideoPlayer.this.bmp, new Rect(0, 0, NativeVideoPlayer.this.f53w, NativeVideoPlayer.this.f52h), new Rect(0, 0, c.getWidth(), c.getHeight()), bpaint);
                    holder.unlockCanvasAndPost(c);
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.media.NativeVideoPlayer.3 */
    class C08523 implements Runnable {
        C08523() {
        }

        public void run() {
            if (NativeVideoPlayer.this.f53w >= 0 && NativeVideoPlayer.this.f52h >= 0) {
                try {
                    int bpp = Integer.parseInt(VERSION.SDK) > 9 ? 4 : 2;
                    IntBuffer ibuf = ByteBuffer.allocateDirect((NativeVideoPlayer.this.f53w * NativeVideoPlayer.this.f52h) * bpp).asIntBuffer();
                    NativeVideoPlayer.this.bmp = Bitmap.createBitmap(NativeVideoPlayer.this.f53w, NativeVideoPlayer.this.f52h, bpp == 4 ? Config.ARGB_8888 : Config.RGB_565);
                    Log.m525d("vk", "scale=" + (((float) NativeVideoPlayer.this.context.getResources().getDisplayMetrics().heightPixels) / ((float) NativeVideoPlayer.this.f52h)));
                    Paint bpaint = new Paint();
                    bpaint.setFilterBitmap(true);
                    Rect srcRect = new Rect(0, 0, NativeVideoPlayer.this.f53w, NativeVideoPlayer.this.f52h);
                    Paint fpaint = new Paint();
                    fpaint.setTextSize(15.0f);
                    fpaint.setColor(-65281);
                    while (!NativeVideoPlayer.this.isCompleted) {
                        try {
                            synchronized (NativeVideoPlayer.this.frameLock) {
                                NativeVideoPlayer.this.frameLock.wait();
                            }
                        } catch (Exception e) {
                        }
                        if (NativeVideoPlayer.this.holder != null) {
                            Canvas c = NativeVideoPlayer.this.holder.lockCanvas();
                            if (c != null) {
                                ibuf.rewind();
                                NativeVideoPlayer.this.renderToBuffer(ibuf, bpp);
                                ibuf.rewind();
                                NativeVideoPlayer.this.bmp.copyPixelsFromBuffer(ibuf);
                                c.drawBitmap(NativeVideoPlayer.this.bmp, srcRect, new Rect(0, 0, c.getWidth(), c.getHeight()), bpaint);
                                NativeVideoPlayer.this.holder.unlockCanvasAndPost(c);
                                NativeVideoPlayer nativeVideoPlayer = NativeVideoPlayer.this;
                                nativeVideoPlayer.tfps = nativeVideoPlayer.tfps + 1;
                            }
                        }
                    }
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                    if (NativeVideoPlayer.this.callback != null) {
                        NativeVideoPlayer.this.callback.nativePlayerError(1);
                    }
                }
            } else if (NativeVideoPlayer.this.callback != null) {
                NativeVideoPlayer.this.callback.nativePlayerError(1);
            }
        }
    }

    /* renamed from: com.vkontakte.android.media.NativeVideoPlayer.4 */
    class C08534 implements Runnable {
        C08534() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r11 = this;
            r10 = 0;
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = 1;
            r6.buffering = r7;	 Catch:{ Exception -> 0x00b1 }
            r3 = 0;
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r6 = r6.conn;	 Catch:{ Exception -> 0x00b1 }
            r2 = r6.getInputStream();	 Catch:{ Exception -> 0x00b1 }
            r6 = "vk";
            r7 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x0104 }
            r7 = r7.conn;	 Catch:{ Exception -> 0x0104 }
            r8 = "content-range";
            r7 = r7.getHeaderField(r8);	 Catch:{ Exception -> 0x0104 }
            com.vkontakte.android.Log.m525d(r6, r7);	 Catch:{ Exception -> 0x0104 }
        L_0x0023:
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r6 = r6.len;	 Catch:{ Exception -> 0x00b1 }
            if (r6 != 0) goto L_0x0045;
        L_0x002b:
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = r7.conn;	 Catch:{ Exception -> 0x00b1 }
            r7 = r7.getContentLength();	 Catch:{ Exception -> 0x00b1 }
            r6.len = r7;	 Catch:{ Exception -> 0x00b1 }
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = r7.len;	 Catch:{ Exception -> 0x00b1 }
            r6.setContentLength(r7);	 Catch:{ Exception -> 0x00b1 }
        L_0x0045:
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = r7.len;	 Catch:{ Exception -> 0x00b1 }
            r8 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r8 = r8.getRamSize();	 Catch:{ Exception -> 0x00b1 }
            r8 = (float) r8;	 Catch:{ Exception -> 0x00b1 }
            r9 = 1040522936; // 0x3e051eb8 float:0.13 double:5.140866364E-315;
            r8 = r8 * r9;
            r8 = (int) r8;	 Catch:{ Exception -> 0x00b1 }
            r8 = r8 * 1024;
            r7 = java.lang.Math.min(r7, r8);	 Catch:{ Exception -> 0x00b1 }
            r6.initInputBuffer(r7);	 Catch:{ Exception -> 0x00b1 }
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = r6.bufferLock;	 Catch:{ Exception -> 0x00b1 }
            monitor-enter(r7);	 Catch:{ Exception -> 0x00b1 }
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ all -> 0x00ae }
            r6 = r6.bufferLock;	 Catch:{ all -> 0x00ae }
            r6.notify();	 Catch:{ all -> 0x00ae }
            monitor-exit(r7);	 Catch:{ all -> 0x00ae }
            r6 = 4096; // 0x1000 float:5.74E-42 double:2.0237E-320;
            r0 = new byte[r6];	 Catch:{ Exception -> 0x00b1 }
            r4 = 0;
        L_0x0078:
            r4 = r2.read(r0);	 Catch:{ Exception -> 0x00b1 }
            if (r4 <= 0) goto L_0x0086;
        L_0x007e:
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r6 = r6.needStopBuffering;	 Catch:{ Exception -> 0x00b1 }
            if (r6 == 0) goto L_0x00c6;
        L_0x0086:
            r2.close();	 Catch:{ Exception -> 0x00b1 }
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r6 = r6.needStopBuffering;	 Catch:{ Exception -> 0x00b1 }
            if (r6 == 0) goto L_0x00a8;
        L_0x0091:
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = 0;
            r6.needStopBuffering = r7;	 Catch:{ Exception -> 0x00b1 }
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = r6.bufferLock;	 Catch:{ Exception -> 0x00b1 }
            monitor-enter(r7);	 Catch:{ Exception -> 0x00b1 }
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ all -> 0x0101 }
            r6 = r6.bufferLock;	 Catch:{ all -> 0x0101 }
            r6.notify();	 Catch:{ all -> 0x0101 }
            monitor-exit(r7);	 Catch:{ all -> 0x0101 }
        L_0x00a8:
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;
            r6.buffering = r10;
            return;
        L_0x00ae:
            r6 = move-exception;
            monitor-exit(r7);	 Catch:{ all -> 0x00ae }
            throw r6;	 Catch:{ Exception -> 0x00b1 }
        L_0x00b1:
            r5 = move-exception;
            r6 = "VK_FFPlayer";
            com.vkontakte.android.Log.m532w(r6, r5);
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;
            r6 = r6.callback;
            if (r6 == 0) goto L_0x00a8;
        L_0x00bd:
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;
            r6 = r6.callback;
            r7 = -1;
            r6.nativePlayerError(r7);
            goto L_0x00a8;
        L_0x00c6:
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r6.writeInput(r0, r4);	 Catch:{ Exception -> 0x00b1 }
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = r6.bufsize;	 Catch:{ Exception -> 0x00b1 }
            r7 = r7 + r4;
            r6.bufsize = r7;	 Catch:{ Exception -> 0x00b1 }
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r6 = r6.bufferingOffset;	 Catch:{ Exception -> 0x00b1 }
            r7 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = r7.bufsize;	 Catch:{ Exception -> 0x00b1 }
            r6 = r6 + r7;
            r6 = (float) r6;	 Catch:{ Exception -> 0x00b1 }
            r7 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r7 = r7.len;	 Catch:{ Exception -> 0x00b1 }
            r7 = (float) r7;	 Catch:{ Exception -> 0x00b1 }
            r6 = r6 / r7;
            r7 = 1120403456; // 0x42c80000 float:100.0 double:5.53552857E-315;
            r6 = r6 * r7;
            r1 = (int) r6;	 Catch:{ Exception -> 0x00b1 }
            if (r1 == r3) goto L_0x0078;
        L_0x00f1:
            r3 = r1;
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r6 = r6.callback;	 Catch:{ Exception -> 0x00b1 }
            if (r6 == 0) goto L_0x0078;
        L_0x00f8:
            r6 = com.vkontakte.android.media.NativeVideoPlayer.this;	 Catch:{ Exception -> 0x00b1 }
            r6 = r6.callback;	 Catch:{ Exception -> 0x00b1 }
            r6.nativePlayerBufferingUpdate(r1);	 Catch:{ Exception -> 0x00b1 }
            goto L_0x0078;
        L_0x0101:
            r6 = move-exception;
            monitor-exit(r7);	 Catch:{ all -> 0x0101 }
            throw r6;	 Catch:{ Exception -> 0x00b1 }
        L_0x0104:
            r6 = move-exception;
            goto L_0x0023;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.media.NativeVideoPlayer.4.run():void");
        }
    }

    /* renamed from: com.vkontakte.android.media.NativeVideoPlayer.5 */
    class C08545 implements Runnable {
        private final /* synthetic */ int val$bufsize;
        private final /* synthetic */ int val$ch;
        private final /* synthetic */ int val$rate;

        C08545(int i, int i2, int i3) {
            this.val$bufsize = i;
            this.val$ch = i2;
            this.val$rate = i3;
        }

        public void run() {
            byte[] b = new byte[this.val$bufsize];
            while (!NativeVideoPlayer.this.isCompleted) {
                try {
                    synchronized (NativeVideoPlayer.this.audioTrack) {
                        NativeVideoPlayer.this.audioTrack.wait();
                    }
                } catch (Exception e) {
                }
                try {
                    NativeVideoPlayer nativeVideoPlayer;
                    if (NativeVideoPlayer.this.bufWritePos > NativeVideoPlayer.this.bufReadPos) {
                        NativeVideoPlayer.this.audioDelay = ((float) ((NativeVideoPlayer.this.bufWritePos - NativeVideoPlayer.this.bufReadPos) + (this.val$bufsize / this.val$ch))) / ((float) (this.val$rate * 2));
                    } else {
                        NativeVideoPlayer.this.audioDelay = ((float) (((NativeVideoPlayer.this.buffer.length - NativeVideoPlayer.this.bufReadPos) + NativeVideoPlayer.this.bufWritePos) + (this.val$bufsize / this.val$ch))) / ((float) (this.val$rate * 2));
                    }
                    if (((double) NativeVideoPlayer.this.audioDelay) > 0.7d) {
                        NativeVideoPlayer.this.bufReadPos = NativeVideoPlayer.this.bufWritePos - this.val$bufsize;
                        if (NativeVideoPlayer.this.bufReadPos < 0) {
                            nativeVideoPlayer = NativeVideoPlayer.this;
                            nativeVideoPlayer.bufReadPos = nativeVideoPlayer.bufReadPos + NativeVideoPlayer.this.buffer.length;
                        }
                    }
                    if (System.currentTimeMillis() - NativeVideoPlayer.this.lastDelayUpdate > 10000 && ((int) (((float) NativeVideoPlayer.this.fps) * NativeVideoPlayer.this.audioDelay)) > 0) {
                        NativeVideoPlayer.this.setDelay((int) (((float) NativeVideoPlayer.this.fps) * NativeVideoPlayer.this.audioDelay));
                        NativeVideoPlayer.this.lastDelayUpdate = System.currentTimeMillis();
                    }
                    if (NativeVideoPlayer.this.bufReadPos + this.val$bufsize < NativeVideoPlayer.this.buffer.length) {
                        System.arraycopy(NativeVideoPlayer.this.buffer, NativeVideoPlayer.this.bufReadPos, b, 0, this.val$bufsize);
                        nativeVideoPlayer = NativeVideoPlayer.this;
                        nativeVideoPlayer.bufReadPos = nativeVideoPlayer.bufReadPos + this.val$bufsize;
                    } else {
                        int r1 = NativeVideoPlayer.this.buffer.length - NativeVideoPlayer.this.bufReadPos;
                        int r2 = this.val$bufsize - r1;
                        System.arraycopy(NativeVideoPlayer.this.buffer, NativeVideoPlayer.this.bufReadPos, b, 0, r1);
                        System.arraycopy(NativeVideoPlayer.this.buffer, 0, b, r1, r2);
                        NativeVideoPlayer.this.bufReadPos = r2;
                    }
                    NativeVideoPlayer.this.audioTrack.write(b, 0, this.val$bufsize);
                } catch (Exception e2) {
                }
            }
            try {
                NativeVideoPlayer.this.audioTrack.stop();
                NativeVideoPlayer.this.audioTrack.release();
            } catch (Exception e3) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.media.NativeVideoPlayer.6 */
    class C08556 implements Runnable {
        C08556() {
        }

        public void run() {
            while (!NativeVideoPlayer.this.isCompleted) {
                if (NativeVideoPlayer.this.callback != null && NativeVideoPlayer.this.isPlaying()) {
                    NativeVideoPlayer.this.callback.nativePlayerPositionUpdate((int) NativeVideoPlayer.this.getPosition());
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                }
            }
        }
    }

    public interface Callback {
        void nativePlayerBufferingUpdate(int i);

        void nativePlayerCompleted();

        void nativePlayerError(int i);

        void nativePlayerPositionUpdate(int i);

        void nativePlayerReady(int i, int i2);
    }

    private native void doPlay(String str, int i, boolean z);

    private native void initInputBuffer(int i);

    private native void nativePause();

    private native void nativeResume();

    private native void renderFrame();

    private native void renderToBuffer(IntBuffer intBuffer, int i);

    private native void setContentLength(int i);

    private native void surfaceResized(int i, int i2);

    private native void writeInput(byte[] bArr, int i);

    public native double getPosition();

    public native boolean isPlaying();

    public native void seek(int i);

    public native void setDelay(int i);

    public native void stop();

    static {
        System.loadLibrary("ffmpeg");
        System.loadLibrary("vkplayer");
    }

    public NativeVideoPlayer(Context ctx) {
        this.useGL = false;
        this.minAudioBufferSize = 0;
        this.needStopBuffering = false;
        this.bufferingOffset = 0;
        this.audioDelay = 0.0f;
        this.lastDelayUpdate = 0;
        this.buffering = false;
        this.isCompleted = false;
        this.fpsTimerTask = new C08501();
        this.context = ctx;
        this.frameLock = new Object();
        this.bufferLock = new Object();
    }

    public void setSurfaceHolder(SurfaceHolder sh) {
        this.holder = sh;
        if (Integer.parseInt(VERSION.SDK) > 9) {
            sh.setFormat(1);
        }
        sh.addCallback(new C08512());
    }

    public void setDataSource(String ds) {
        this.file = ds;
    }

    public void start() {
        new Thread(this).start();
    }

    private void startRenderThread() {
        new Thread(new C08523()).start();
    }

    private void frameReady() {
        synchronized (this.frameLock) {
            this.frameLock.notify();
        }
    }

    private void openInput() {
        try {
            this.conn = new URL(this.file).openConnection();
            if (this.bufferingOffset > 0) {
                this.conn.setRequestProperty("Range", "bytes=" + this.bufferingOffset + "-");
            }
            this.conn.setUseCaches(false);
            this.conn.connect();
            new Thread(new C08534()).start();
        } catch (Throwable x) {
            Log.m532w("VK_FFPlayer", x);
            if (this.callback != null) {
                this.callback.nativePlayerError(-1);
            }
        }
    }

    private void initFormat(int rate, int ch, int _w, int _h) {
        int i;
        int i2 = 4;
        Log.m525d("VK_FFPlayer", "initaudio " + rate);
        this.f53w = _w;
        this.f52h = _h;
        startRenderThread();
        if (ch == 2) {
            i = 12;
        } else {
            i = 4;
        }
        int bs = (int) (((float) AudioTrack.getMinBufferSize(rate, i, 2)) * 1.5f);
        this.minAudioBufferSize = bs;
        int bufsize = bs > 0 ? bs : NewsEntry.FLAG_POSTPONED;
        try {
            Log.m525d("VK_FFPlayer", "bufsize=" + bufsize);
            this.buffer = new byte[(bufsize * 50)];
            if (ch == 2) {
                i2 = 12;
            }
            this.audioTrack = new AudioTrack(3, rate, i2, 2, bufsize, 1);
            this.audioTrack.play();
            new Thread(new C08545(bufsize, ch, rate)).start();
            if (this.callback != null) {
                this.callback.nativePlayerReady(_w, _h);
            }
            startUpdatingPosition();
        } catch (Exception e) {
            if (this.audioTrack != null) {
                try {
                    this.audioTrack.stop();
                    this.audioTrack.release();
                } catch (Exception e2) {
                }
            }
            if (this.callback != null) {
                this.callback.nativePlayerError(1);
            }
        }
    }

    protected void writeAudio(byte[] data, int num) {
        if (this.bufWritePos + num < this.buffer.length) {
            System.arraycopy(data, 0, this.buffer, this.bufWritePos, num);
            this.bufWritePos += num;
        } else {
            int r1 = this.buffer.length - this.bufWritePos;
            int r2 = num - r1;
            System.arraycopy(data, 0, this.buffer, this.bufWritePos, r1);
            System.arraycopy(data, r1, this.buffer, 0, r2);
            this.bufWritePos = r2;
        }
        if ((this.bufWritePos > this.bufReadPos && this.bufWritePos - this.bufReadPos > this.minAudioBufferSize) || (this.bufWritePos <= this.bufReadPos && (this.bufWritePos + this.buffer.length) - this.bufReadPos > this.minAudioBufferSize)) {
            synchronized (this.audioTrack) {
                this.audioTrack.notify();
            }
        }
    }

    private int getBufferedSize() {
        return this.bufsize;
    }

    private void startUpdatingPosition() {
        new Thread(new C08556()).start();
    }

    public void pause() {
        this.audioTrack.pause();
        nativePause();
    }

    public void resume() {
        this.audioTrack.play();
        nativeResume();
    }

    private int getRamSize() {
        int tm = LocationStatusCodes.GEOFENCE_NOT_AVAILABLE;
        try {
            String[] trm = new RandomAccessFile("/proc/meminfo", "r").readLine().split(" kB")[0].split(" ");
            tm = Integer.parseInt(trm[trm.length - 1]);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return tm;
    }

    public void run() {
        int i = 1;
        try {
            this.isCompleted = false;
            openInput();
            String str = this.file;
            if (Integer.parseInt(VERSION.SDK) <= 9) {
                i = 0;
            }
            doPlay(str, i, false);
            Log.m528i("VK_FFplayer", "Completed!!!");
            if (this.callback != null) {
                this.callback.nativePlayerCompleted();
            }
            this.isCompleted = true;
            this.audioTrack.stop();
            this.audioTrack.release();
        } catch (Exception e) {
        }
    }

    public boolean isCompleted() {
        return this.isCompleted;
    }

    private void seekHTTP(int offset) {
        Log.m528i("vk", "SEEK HTTP -> " + offset);
        if (offset < this.len && offset >= 0) {
            if (this.buffering) {
                this.needStopBuffering = true;
                synchronized (this.bufferLock) {
                    try {
                        this.bufferLock.wait();
                    } catch (Exception e) {
                    }
                }
            }
            this.bufferingOffset = offset;
            this.bufsize = 0;
            openInput();
            synchronized (this.bufferLock) {
                try {
                    this.bufferLock.wait();
                } catch (Exception e2) {
                }
            }
        }
    }
}
