package com.vkontakte.android.media;

import android.support.v4.view.MotionEventCompat;
import com.vkontakte.android.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import net.hockeyapp.android.views.UpdateView;

public class LZW {
    protected static final int MAX_STACK_SIZE = 4096;

    public static byte[] decodeGifBitmapData(InputStream in, int npix, int initialSize) throws IOException {
        int code;
        int top;
        ByteArrayOutputStream pixels = new ByteArrayOutputStream();
        short[] prefix = new short[MAX_STACK_SIZE];
        byte[] suffix = new byte[MAX_STACK_SIZE];
        byte[] pixelStack = new byte[UpdateView.HEADER_VIEW_ID];
        byte[] block = new byte[MotionEventCompat.ACTION_MASK];
        int data_size = initialSize;
        int clear = 1 << data_size;
        int end_of_information = clear + 1;
        int available = clear + 2;
        int old_code = -1;
        int code_size = data_size + 1;
        int code_mask = (1 << code_size) - 1;
        for (code = 0; code < clear; code++) {
            prefix[code] = (short) 0;
            suffix[code] = (byte) code;
        }
        int bi = 0;
        int pi = 0;
        int first = 0;
        int count = 0;
        int bits = 0;
        int datum = 0;
        int i = 0;
        int top2 = 0;
        while (i < npix) {
            if (top2 != 0) {
                top = top2;
            } else if (bits < code_size) {
                if (count == 0) {
                    count = in.read(block, 0, MotionEventCompat.ACTION_MASK);
                    if (count <= 0) {
                        Log.m530w("vk", "end of data");
                        top = top2;
                        break;
                    }
                    bi = 0;
                }
                datum += (block[bi] & MotionEventCompat.ACTION_MASK) << bits;
                bits += 8;
                bi++;
                count--;
            } else {
                code = datum & code_mask;
                datum >>= code_size;
                bits -= code_size;
                if (code > available || code == end_of_information) {
                    Log.m530w("vk", "lzw exit " + (code > available) + ", " + (code == end_of_information));
                    top = top2;
                } else if (code == clear) {
                    Log.m528i("vk", "clear " + available);
                    code_size = data_size + 1;
                    code_mask = (1 << code_size) - 1;
                    available = clear + 2;
                    old_code = -1;
                    Log.m528i("vk", "after clear " + available);
                } else if (old_code == -1) {
                    top = top2 + 1;
                    pixelStack[top2] = suffix[code];
                    old_code = code;
                    first = code;
                    top2 = top;
                } else {
                    int in_code = code;
                    if (code == available) {
                        top = top2 + 1;
                        pixelStack[top2] = (byte) first;
                        code = old_code;
                        top2 = top;
                    }
                    while (code > clear) {
                        top = top2 + 1;
                        pixelStack[top2] = suffix[code];
                        code = prefix[code];
                        top2 = top;
                    }
                    first = suffix[code] & MotionEventCompat.ACTION_MASK;
                    if (available >= MAX_STACK_SIZE) {
                        Log.m530w("vk", "available >= MAX_STACK_SIZE " + available);
                        top = top2;
                        break;
                    }
                    top = top2 + 1;
                    pixelStack[top2] = (byte) first;
                    prefix[available] = (short) old_code;
                    suffix[available] = (byte) first;
                    available++;
                    if ((available & code_mask) == 0 && available < MAX_STACK_SIZE) {
                        code_size++;
                        code_mask += available;
                    }
                    old_code = in_code;
                }
            }
            top--;
            pixels.write(pixelStack[top]);
            i++;
            top2 = top;
        }
        top = top2;
        for (i = pi; i < npix; i++) {
            pixels.write(0);
        }
        return pixels.toByteArray();
    }
}
