package com.vkontakte.android.media;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build.VERSION;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.Log;

public class PlayerWrapper implements Callback {
    public static final int ERROR_CANT_DECODE = 1;
    public static final int ERROR_CODEC_NOT_FOUND = 2;
    public static final int ERROR_FILE_NOT_FOUND = 3;
    public static final int ERROR_NETWORK = -1;
    public static final int ERROR_UNSUPPORTED_OS = 0;
    private Context context;
    private String dataSource;
    private SurfaceHolder holder;
    private MediaPlayer hwPlayer;
    private boolean hwSurfaceSet;
    private PlayerStateListener listener;
    private NativeVideoPlayer nativePlayer;
    private int prevBufPercent;
    private int seekTo;
    private boolean surfaceReady;
    private Object surfaceTexture;
    private Object textureView;
    private boolean useHWPlayer;

    /* renamed from: com.vkontakte.android.media.PlayerWrapper.1 */
    class C08561 implements SurfaceTextureListener {
        C08561() {
        }

        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            PlayerWrapper.this.surfaceTexture = surface;
            PlayerWrapper.this.hwPlayer.setSurface(new Surface(surface));
        }

        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            if (PlayerWrapper.this.hwPlayer != null) {
                PlayerWrapper.this.hwPlayer.setSurface(null);
            }
            return true;
        }

        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        }

        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    }

    /* renamed from: com.vkontakte.android.media.PlayerWrapper.2 */
    class C08572 implements OnPreparedListener {
        C08572() {
        }

        public void onPrepared(MediaPlayer mp) {
            PlayerWrapper.this.hwPlayerPrepared();
        }
    }

    /* renamed from: com.vkontakte.android.media.PlayerWrapper.3 */
    class C08583 implements OnBufferingUpdateListener {
        C08583() {
        }

        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            if (percent - PlayerWrapper.this.prevBufPercent <= 80) {
                if (PlayerWrapper.this.listener != null) {
                    PlayerWrapper.this.listener.onUpdateBuffered(percent);
                }
                PlayerWrapper.this.prevBufPercent = percent;
            }
        }
    }

    /* renamed from: com.vkontakte.android.media.PlayerWrapper.4 */
    class C08594 implements OnCompletionListener {
        C08594() {
        }

        public void onCompletion(MediaPlayer mp) {
            if (PlayerWrapper.this.listener != null) {
                PlayerWrapper.this.listener.onPlaybackCompleted();
            }
        }
    }

    /* renamed from: com.vkontakte.android.media.PlayerWrapper.5 */
    class C08605 implements OnErrorListener {
        C08605() {
        }

        public boolean onError(MediaPlayer mp, int what, int extra) {
            Log.m526e("vk", "VPLAYER ERROR " + what + "; " + extra);
            if (what == PlayerWrapper.ERROR_CANT_DECODE && PlayerWrapper.this.listener != null) {
                PlayerWrapper.this.listener.onError(PlayerWrapper.ERROR_CANT_DECODE);
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.media.PlayerWrapper.6 */
    class C08616 implements OnInfoListener {
        C08616() {
        }

        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            if (what == 701 && PlayerWrapper.this.listener != null) {
                PlayerWrapper.this.listener.onEndOfBuffer();
            }
            if (what == 702 && PlayerWrapper.this.listener != null) {
                PlayerWrapper.this.listener.onPlaybackResumed();
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.media.PlayerWrapper.8 */
    class C08628 implements Runnable {
        C08628() {
        }

        public void run() {
            while (PlayerWrapper.this.hwPlayer != null) {
                try {
                    if (PlayerWrapper.this.listener != null) {
                        PlayerWrapper.this.listener.onUpdatePlaybackPosition(PlayerWrapper.this.hwPlayer.getCurrentPosition() / LocationStatusCodes.GEOFENCE_NOT_AVAILABLE);
                    }
                    Thread.sleep(1000);
                } catch (Exception e) {
                }
            }
        }
    }

    public interface PlayerStateListener {
        void onEndOfBuffer();

        void onError(int i);

        void onPlaybackCompleted();

        void onPlaybackResumed();

        void onPlayerReady(int i, int i2);

        void onUpdateBuffered(int i);

        void onUpdatePlaybackPosition(int i);
    }

    /* renamed from: com.vkontakte.android.media.PlayerWrapper.7 */
    class C15207 implements NativeVideoPlayer.Callback {
        C15207() {
        }

        public void nativePlayerReady(int w, int h) {
            if (PlayerWrapper.this.listener != null) {
                PlayerWrapper.this.listener.onPlayerReady(w, h);
            }
        }

        public void nativePlayerPositionUpdate(int sec) {
            if (PlayerWrapper.this.listener != null) {
                PlayerWrapper.this.listener.onUpdatePlaybackPosition(sec);
            }
        }

        public void nativePlayerError(int errCode) {
            if (PlayerWrapper.this.listener != null) {
                PlayerWrapper.this.listener.onError(errCode);
            }
        }

        public void nativePlayerCompleted() {
            if (PlayerWrapper.this.listener != null) {
                PlayerWrapper.this.listener.onPlaybackCompleted();
            }
        }

        public void nativePlayerBufferingUpdate(int percent) {
            if (PlayerWrapper.this.listener != null) {
                PlayerWrapper.this.listener.onUpdateBuffered(percent);
            }
        }
    }

    public PlayerWrapper(Context _context, SurfaceHolder _holder) {
        this.useHWPlayer = false;
        this.surfaceReady = false;
        this.hwSurfaceSet = false;
        this.prevBufPercent = 0;
        this.seekTo = 0;
        this.context = _context;
        this.holder = _holder;
        this.holder.addCallback(this);
    }

    public PlayerWrapper(Context _context, TextureView tv) {
        this.useHWPlayer = false;
        this.surfaceReady = false;
        this.hwSurfaceSet = false;
        this.prevBufPercent = 0;
        this.seekTo = 0;
        this.context = _context;
        this.textureView = tv;
        tv.setSurfaceTextureListener(new C08561());
    }

    public void init(String ds) {
        int apiLevel = Integer.parseInt(VERSION.SDK);
        if (apiLevel < 8 || ds.endsWith(".flv")) {
            this.useHWPlayer = false;
            if (apiLevel < 11) {
                this.holder.setType(0);
            }
            this.dataSource = ds;
            initNativePlayer(ds);
            return;
        }
        this.useHWPlayer = true;
        if (apiLevel < 11) {
            this.holder.setType(ERROR_FILE_NOT_FOUND);
        }
        initHWPlayer(ds);
        this.dataSource = ds;
    }

    private void initHWPlayer(String ds) {
        try {
            this.hwPlayer = new MediaPlayer();
            this.hwPlayer.setDataSource(this.context, Uri.parse(ds));
            this.hwPlayer.setOnPreparedListener(new C08572());
            this.hwPlayer.setOnBufferingUpdateListener(new C08583());
            this.hwPlayer.setOnCompletionListener(new C08594());
            this.hwPlayer.setOnErrorListener(new C08605());
            this.hwPlayer.setOnInfoListener(new C08616());
            this.hwPlayer.prepareAsync();
        } catch (Exception e) {
        }
    }

    private void initNativePlayer(String ds) {
        this.nativePlayer = new NativeVideoPlayer(this.context);
        this.nativePlayer.setSurfaceHolder(this.holder);
        this.nativePlayer.setDataSource(ds);
        this.nativePlayer.callback = new C15207();
        this.nativePlayer.start();
    }

    public boolean isHardwareAccelerated() {
        return this.useHWPlayer;
    }

    public String getDataSource() {
        return this.dataSource;
    }

    private void hwPlayerPrepared() {
        if (this.listener != null) {
            this.listener.onPlayerReady(this.hwPlayer.getVideoWidth(), this.hwPlayer.getVideoHeight());
        }
        new Thread(new C08628()).start();
        if (this.seekTo > 0) {
            this.hwPlayer.seekTo(this.seekTo);
            this.seekTo = 0;
        }
    }

    public void play() {
        if (this.useHWPlayer) {
            this.hwPlayer.start();
            if (this.surfaceReady) {
                this.hwPlayer.setDisplay(this.holder);
            }
        } else if (this.nativePlayer.isCompleted()) {
            this.nativePlayer.start();
        } else {
            this.nativePlayer.resume();
        }
    }

    public void pause() {
        if (this.useHWPlayer) {
            this.hwPlayer.pause();
        } else {
            this.nativePlayer.pause();
        }
    }

    public void seek(int sec) {
        if (this.useHWPlayer) {
            this.hwPlayer.seekTo(sec * LocationStatusCodes.GEOFENCE_NOT_AVAILABLE);
        } else {
            this.nativePlayer.seek(sec);
        }
    }

    public void stopAndRelease() {
        if (!this.useHWPlayer || this.hwPlayer == null) {
            this.nativePlayer.stop();
            return;
        }
        try {
            this.hwPlayer.stop();
        } catch (Exception e) {
        }
        try {
            this.hwPlayer.release();
        } catch (Exception e2) {
        }
        this.hwPlayer = null;
    }

    public void setListener(PlayerStateListener l) {
        this.listener = l;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.m525d("vk", "==== SURFACE CHANGED!!");
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.surfaceReady = true;
        Log.m525d("vk", "==== SURFACE CREATED!!");
        this.holder = holder;
        if (this.hwPlayer != null) {
            boolean p = false;
            try {
                p = this.hwPlayer.isPlaying();
            } catch (Exception e) {
            }
            if (p) {
                this.seekTo = this.hwPlayer.getCurrentPosition();
                this.hwPlayer.reset();
                initHWPlayer(this.dataSource);
                return;
            }
            this.hwPlayer.setDisplay(holder);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.m525d("vk", "==== SURFACE DESTROYED!!");
        this.surfaceReady = false;
        this.holder = null;
        if (this.hwPlayer != null) {
            this.hwPlayer.setDisplay(null);
        }
    }
}
