package com.vkontakte.android.media.rtmp;

import android.hardware.Camera;
import android.support.v4.media.TransportMediator;
import com.google.android.gcm.GCMConstants;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.Log;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.media.rtmp.StreamRecorder.StreamPacket;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Semaphore;

public class RTMPClient {
    private static long timestampOffset;
    public static boolean useSpeaker;
    private String appName;
    private Camera camera;
    private HashMap<Integer, StreamInfo> chunkStreams;
    private boolean connectSuccessReceived;
    private String connectURL;
    private boolean disconnected;
    private XDataInputStream in;
    private HashMap<Integer, Object> invokeLocks;
    private HashMap<Integer, Object[]> invokeResults;
    public long ipcount;
    public long ivpsize;
    private int lastCsID;
    private int lastOutLen;
    private int lastStreamID;
    private int lastTransactionID;
    public long opcount;
    private XDataOutputStream out;
    public long ovpsize;
    private int playStreamID;
    private int publishStreamID;
    private RPCListener rpcListener;
    private int serverChunkSize;
    private ArrayBlockingQueue<byte[]> servicePackets;
    private Socket socket;
    private Semaphore ss;
    private StreamPlayer streamPlayer;
    private StreamRecorder streamRecorder;
    private boolean videoStarted;

    /* renamed from: com.vkontakte.android.media.rtmp.RTMPClient.1 */
    class C08631 implements Runnable {
        C08631() {
        }

        public void run() {
            try {
                RTMPClient.this.loop();
            } catch (Exception x) {
                if (!RTMPClient.this.disconnected) {
                    RTMPClient.log("OH SHI~", new Object[0]);
                    Log.m527e("vk_RTMP", "Connection error", x);
                    RTMPClient.this.rpcListener.onConnectionSuddenlyClosed();
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.media.rtmp.RTMPClient.2 */
    class C08642 implements Runnable {
        private final /* synthetic */ int val$streamID;

        C08642(int i) {
            this.val$streamID = i;
        }

        public void run() {
            Thread.currentThread().setName("RTMP_Publisher");
            RTMPClient.log("publish start", new Object[0]);
            int lastType = 0;
            long ts = 0;
            boolean videoStarted = false;
            boolean audioStarted = false;
            while (!RTMPClient.this.disconnected) {
                byte[] sp = (byte[]) RTMPClient.this.servicePackets.poll();
                while (sp != null) {
                    try {
                        RTMPClient.this.out.write(sp);
                        RTMPClient.this.out.flush();
                        sp = (byte[]) RTMPClient.this.servicePackets.poll();
                        RTMPClient.log("Sent a service packet", new Object[0]);
                        lastType = 0;
                    } catch (Throwable x) {
                        Log.m532w("vk_RTMP", x);
                        return;
                    }
                }
                StreamPacket pkt = RTMPClient.this.streamRecorder.getNextPacket();
                if (ts == 0) {
                    ts = (long) RTMPClient.time();
                }
                if (!pkt.first) {
                    int i;
                    RTMPClient rTMPClient = RTMPClient.this;
                    if (pkt.type == 8) {
                        i = 4;
                    } else {
                        i = 5;
                    }
                    rTMPClient.sendType3Header(i);
                } else if (RTMPClient.this.lastOutLen == -1 || RTMPClient.this.lastStreamID != this.val$streamID || lastType == 0 || ((pkt.type == 8 && !audioStarted) || (pkt.type == 9 && !videoStarted))) {
                    RTMPClient.this.sendType0Header(pkt.type == 8 ? 4 : 5, pkt.type, pkt.fullSize, this.val$streamID, (long) RTMPClient.time());
                    RTMPClient.this.lastStreamID = this.val$streamID;
                    Log.m530w("vk", "Sent type 0 header!!!");
                    if (pkt.type == 8) {
                        audioStarted = true;
                    }
                    if (pkt.type == 9) {
                        videoStarted = true;
                    }
                } else {
                    RTMPClient.this.sendType1Header(pkt.type == 8 ? 4 : 5, pkt.type, pkt.fullSize, (long) (pkt.type == 8 ? 20 : 0));
                }
                if (pkt.first) {
                    ts = pkt.timestamp;
                }
                lastType = pkt.type;
                RTMPClient.this.out.writeChunked(pkt.data, pkt.type == 8 ? 4 : 5);
                RTMPClient.this.out.flush();
            }
        }
    }

    public interface RPCListener {
        void onCommand(String str, Object[] objArr);

        void onConnectionSuddenlyClosed();

        void onInVideoStarted();
    }

    private class StreamInfo {
        int lastMessageLength;
        int lastMessageType;
        int lastTimestamp;
        ByteArrayOutputStream messageBuffer;
        int readLength;
        int timestamp;
        long timestampStart;

        public StreamInfo(int ts) {
            this.timestampStart = System.currentTimeMillis() - ((long) ts);
        }

        public int getCurrentTimestamp() {
            return (int) (System.currentTimeMillis() - this.timestampStart);
        }

        public int getTimestampDelta() {
            int d = getCurrentTimestamp() - this.lastTimestamp;
            this.lastTimestamp = getCurrentTimestamp();
            return d;
        }
    }

    static {
        timestampOffset = 0;
        useSpeaker = true;
    }

    public RTMPClient() {
        this.serverChunkSize = TransportMediator.FLAG_KEY_MEDIA_NEXT;
        this.chunkStreams = new HashMap();
        this.connectSuccessReceived = false;
        this.lastTransactionID = 0;
        this.invokeLocks = new HashMap();
        this.invokeResults = new HashMap();
        this.lastStreamID = 0;
        this.lastCsID = 0;
        this.lastOutLen = 0;
        this.ovpsize = 0;
        this.ivpsize = 0;
        this.opcount = 0;
        this.ipcount = 0;
        this.rpcListener = null;
        this.servicePackets = new ArrayBlockingQueue(20);
        this.ss = new Semaphore(1, true);
        this.disconnected = false;
        this.videoStarted = false;
    }

    public void connect(String url, Object... _params) throws IOException, URISyntaxException {
        this.disconnected = false;
        URI _url = new URI(url);
        if (_url.getScheme().equals("rtmp")) {
            String host = _url.getHost();
            int port = _url.getPort();
            if (port == -1) {
                port = 1935;
            }
            this.socket = new Socket(InetAddress.getByName(host), port);
            this.in = new XDataInputStream(this.socket.getInputStream());
            this.out = new XDataOutputStream(new BufferedOutputStream(this.socket.getOutputStream(), NewsEntry.FLAG_SUGGESTED));
            performHandshake();
            HashMap<String, Object> params = new HashMap();
            params.put(GCMConstants.EXTRA_APPLICATION_PENDING_INTENT, _url.getPath().substring(1));
            params.put("flashVer", "ANDROID_VK");
            params.put("swfUrl", null);
            params.put("tcUrl", url);
            params.put("fpad", Boolean.valueOf(false));
            params.put("audioCodecs", Integer.valueOf(2052));
            params.put("videoCodecs", Integer.valueOf(TransportMediator.FLAG_KEY_MEDIA_NEXT));
            params.put("pageUrl", null);
            params.put("objectEncoding", Integer.valueOf(0));
            params.put("capabilities", Integer.valueOf(15));
            params.put("videoFunction", Integer.valueOf(1));
            new Thread(new C08631()).start();
            invoke("connect", 0, 3, params, _params[0], _params[1]);
            return;
        }
        throw new IllegalArgumentException("URL must have rtmp:// scheme");
    }

    private void loop() throws IOException {
        while (true) {
            receiveNextChunk();
        }
    }

    static int time() {
        return (int) (System.currentTimeMillis() - timestampOffset);
    }

    private int time(long ts) {
        return (int) (ts - timestampOffset);
    }

    public static void log(String format, Object... args) {
        Log.m525d("vk_RTMP", String.format(format, args));
    }

    private void performHandshake() throws IOException {
        int i;
        this.out.write(3);
        this.out.flush();
        log("sent C0", new Object[0]);
        long startTime = System.currentTimeMillis();
        Random rand = new Random();
        timestampOffset = System.currentTimeMillis();
        this.out.writeInt(0);
        this.out.writeInt(0);
        for (i = 0; i < 382; i++) {
            this.out.writeInt(rand.nextInt());
        }
        this.out.flush();
        log("sent C1", new Object[0]);
        int sVer = this.in.read();
        log("server ver=%d", Integer.valueOf(sVer));
        log("server ts=%d", Integer.valueOf(this.in.readInt()));
        this.in.readInt();
        byte[] sRandom = new byte[1528];
        this.in.readFull(sRandom);
        long t = System.nanoTime();
        this.out.writeInt(sTimestamp);
        this.out.writeInt((int) (System.currentTimeMillis() - startTime));
        this.out.write(sRandom);
        this.out.flush();
        double upSpeed = 6.0E10d / ((double) ((System.nanoTime() - t) / ((long) (sRandom.length + 8))));
        log("UP %f byte/s", Double.valueOf((double) ((System.nanoTime() - t) / ((long) (sRandom.length + 8)))), Double.valueOf(upSpeed));
        long tr = System.nanoTime();
        this.in.readInt();
        this.in.readInt();
        for (i = 0; i < 382; i++) {
            this.in.readInt();
        }
        double downSpeed = 6.0E10d / ((double) ((System.nanoTime() - tr) / 1536));
        log("DOWN %f byte/s", Double.valueOf(s), Double.valueOf(downSpeed));
        Object[] objArr = new Object[1];
        objArr[0] = Integer.valueOf(this.in.available());
        log("handshake done! %d", objArr);
    }

    private void receiveNextChunk() throws IOException {
        int timestamp;
        StreamInfo si;
        int csId = this.in.read();
        int type = csId >> 6;
        csId &= 63;
        if (csId == 0) {
            csId = this.in.read() + 64;
        } else if (csId == 1) {
            csId = (this.in.read() + 64) + (this.in.read() << 8);
        }
        if (type == 0) {
            timestamp = this.in.readInt24();
            int length = this.in.readInt24();
            int msgType = this.in.read();
            int streamId = this.in.readInt();
            if (!this.chunkStreams.containsKey(Integer.valueOf(csId))) {
                this.chunkStreams.put(Integer.valueOf(csId), new StreamInfo(timestamp));
            }
            si = (StreamInfo) this.chunkStreams.get(Integer.valueOf(csId));
            si.lastMessageLength = length;
            si.lastMessageType = msgType;
            si.messageBuffer = new ByteArrayOutputStream();
            si.readLength = 0;
            si.timestamp = timestamp;
            this.lastStreamID = streamId;
        }
        if (type == 1) {
            timestamp = this.in.readInt24();
            length = this.in.readInt24();
            msgType = this.in.read();
            if (!this.chunkStreams.containsKey(Integer.valueOf(csId))) {
                this.chunkStreams.put(Integer.valueOf(csId), new StreamInfo(timestamp));
            }
            si = (StreamInfo) this.chunkStreams.get(Integer.valueOf(csId));
            si.lastMessageLength = length;
            si.lastMessageType = msgType;
            si.messageBuffer = new ByteArrayOutputStream();
            si.timestamp += timestamp;
            si.readLength = 0;
        }
        if (type == 2) {
            int tsDelta = this.in.readInt24();
            si = (StreamInfo) this.chunkStreams.get(Integer.valueOf(csId));
            si.timestamp += tsDelta;
        }
        si = (StreamInfo) this.chunkStreams.get(Integer.valueOf(csId));
        if (si == null) {
            throw new IOException("Unknown chunk stream ID " + csId);
        }
        byte[] buf = new byte[Math.min(this.serverChunkSize, si.lastMessageLength - si.readLength)];
        int numRead = this.in.readFull(buf);
        si.readLength += numRead;
        si.messageBuffer.write(buf);
        this.lastCsID = csId;
        if (si.readLength >= si.lastMessageLength) {
            int i;
            XDataInputStream is;
            Vector<Object> args;
            String method;
            byte[] msg = si.messageBuffer.toByteArray();
            si.readLength = 0;
            si.messageBuffer.reset();
            if (csId == 2) {
                i = si.lastMessageType;
                if (r0 == 4) {
                    DataInputStream is2 = new DataInputStream(new ByteArrayInputStream(msg));
                    int ptype = is2.readShort();
                    int param1 = is2.readShort();
                    int param2 = is2.readShort();
                    log("PING (type=%d, param1=%d, param2=%d)", Integer.valueOf(ptype), Integer.valueOf(param1), Integer.valueOf(param2));
                    if (ptype == 6) {
                        sendPing(7, param1, si.getCurrentTimestamp());
                    }
                }
            }
            if (csId == 2) {
                i = si.lastMessageType;
                if (r0 == 1) {
                    this.serverChunkSize = new DataInputStream(new ByteArrayInputStream(msg)).readInt();
                    Object[] objArr = new Object[1];
                    objArr[0] = Integer.valueOf(this.serverChunkSize);
                    log("Server chunk size set to %d", objArr);
                }
            }
            if (csId == 2) {
                i = si.lastMessageType;
                if (r0 == 5) {
                    log("Server BW %d", Integer.valueOf(new DataInputStream(new ByteArrayInputStream(msg)).readInt()));
                    sendType0Header(2, 5, 4, 0);
                    this.out.writeInt(bw);
                    this.out.flush();
                }
            }
            i = si.lastMessageType;
            if (r0 == 20) {
                is = new XDataInputStream(new ByteArrayInputStream(msg));
                log("%s - %s", (String) is.readNextAmf(), is.readNextAmf());
                int transaction = (int) ((Double) obj).doubleValue();
                args = new Vector();
                while (is.available() > 0) {
                    args.add(is.readNextAmf());
                }
                onCommand(transaction, method, args.toArray());
            }
            i = si.lastMessageType;
            if (r0 == 15) {
                try {
                    is = new XDataInputStream(new ByteArrayInputStream(msg));
                    is.read();
                    method = (String) is.readNextAmf();
                    args = new Vector();
                    while (is.available() > 1) {
                        args.add(is.readNextAmf());
                    }
                    if (this.rpcListener != null) {
                        this.rpcListener.onCommand(method, args.toArray());
                    }
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                }
            }
            i = si.lastMessageType;
            if (r0 == 8) {
                this.streamPlayer.onAudioData(msg);
            }
            i = si.lastMessageType;
            if (r0 == 9) {
                if (!(this.videoStarted || this.rpcListener == null)) {
                    this.rpcListener.onInVideoStarted();
                    this.videoStarted = true;
                }
                this.ivpsize += (long) msg.length;
                this.ipcount++;
                this.streamPlayer.onVideoData(msg);
            }
        }
    }

    public Object[] invoke(String method, int streamID, int csID, Object... args) throws IOException {
        try {
            this.ss.acquire();
        } catch (Exception e) {
        }
        ByteArrayOutputStream _buf = new ByteArrayOutputStream();
        XDataOutputStream buf = new XDataOutputStream(_buf);
        buf.writeAmfString(method);
        buf.writeAmfNumber((double) this.lastTransactionID);
        for (Object obj : args) {
            buf.writeAMF(obj);
        }
        byte[] data = _buf.toByteArray();
        this.out.write(csID);
        if (!this.chunkStreams.containsKey(Integer.valueOf(csID))) {
            this.chunkStreams.put(Integer.valueOf(csID), new StreamInfo());
        }
        this.out.writeInt24(((StreamInfo) this.chunkStreams.get(Integer.valueOf(csID))).getCurrentTimestamp());
        this.out.writeInt24(data.length);
        this.out.write(20);
        this.out.writeIntFlipped(streamID);
        this.out.writeChunked(data, csID);
        this.out.flush();
        this.ss.release();
        Object lock = new Object();
        int transactionID = this.lastTransactionID;
        this.lastTransactionID++;
        log("invoked %s", method);
        while (!this.invokeResults.containsKey(Integer.valueOf(transactionID))) {
            try {
                Thread.sleep(10);
            } catch (Exception e2) {
            }
        }
        return (Object[]) this.invokeResults.remove(Integer.valueOf(transactionID));
    }

    public void invokeNoWait(String method, int streamID, int csID, Object... args) throws IOException {
        try {
            this.ss.acquire();
        } catch (Exception e) {
        }
        ByteArrayOutputStream _buf = new ByteArrayOutputStream();
        XDataOutputStream buf = new XDataOutputStream(_buf);
        buf.writeAmfString(method);
        buf.writeAmfNumber((double) this.lastTransactionID);
        for (Object obj : args) {
            buf.writeAMF(obj);
        }
        byte[] data = _buf.toByteArray();
        this.out.write(csID);
        if (!this.chunkStreams.containsKey(Integer.valueOf(csID))) {
            this.chunkStreams.put(Integer.valueOf(csID), new StreamInfo());
        }
        this.out.writeInt24(((StreamInfo) this.chunkStreams.get(Integer.valueOf(csID))).getCurrentTimestamp());
        this.out.writeInt24(data.length);
        this.out.write(20);
        this.out.writeIntFlipped(streamID);
        this.out.writeChunked(data, csID);
        this.out.flush();
        this.invokeLocks.put(Integer.valueOf(this.lastTransactionID), new Object());
        this.lastTransactionID++;
        log("invoked %s on stream %d", method, Integer.valueOf(streamID));
        this.ss.release();
    }

    public void send(String method, int streamID, Object... args) throws IOException {
        ByteArrayOutputStream _buf = new ByteArrayOutputStream();
        XDataOutputStream buf = new XDataOutputStream(_buf);
        buf.write(0);
        buf.writeAmfString(method);
        for (Object obj : args) {
            buf.writeAMF(obj);
        }
        byte[] data = _buf.toByteArray();
        _buf = new ByteArrayOutputStream();
        buf = new XDataOutputStream(_buf);
        buf.write(3);
        if (!this.chunkStreams.containsKey(Integer.valueOf(3))) {
            this.chunkStreams.put(Integer.valueOf(3), new StreamInfo());
        }
        buf.writeInt24(((StreamInfo) this.chunkStreams.get(Integer.valueOf(3))).getCurrentTimestamp());
        buf.writeInt24(data.length);
        buf.write(15);
        buf.writeIntFlipped(streamID);
        buf.writeChunked(data, 3);
        this.servicePackets.offer(_buf.toByteArray());
        log("invoked %s on stream %d", method, Integer.valueOf(streamID));
    }

    public void setRPCListener(RPCListener l) {
        this.rpcListener = l;
    }

    private void sendType0Header(int streamID, int msgType, int dataLength, int msgStreamID) throws IOException {
        this.out.write(streamID);
        if (!this.chunkStreams.containsKey(Integer.valueOf(streamID))) {
            this.chunkStreams.put(Integer.valueOf(streamID), new StreamInfo());
        }
        this.out.writeInt24(((StreamInfo) this.chunkStreams.get(Integer.valueOf(streamID))).getCurrentTimestamp());
        this.out.writeInt24(dataLength);
        this.out.write(msgType);
        this.out.writeIntFlipped(msgStreamID);
    }

    private void sendType1Header(int streamID, int msgType, int dataLength) throws IOException {
        this.out.write(streamID | 64);
        this.out.writeInt24(((StreamInfo) this.chunkStreams.get(Integer.valueOf(streamID))).getTimestampDelta());
        this.out.writeInt24(dataLength);
        this.out.write(msgType);
    }

    private void sendType2Header(int streamID, int prevTime) throws IOException {
        this.out.write(streamID | 64);
        this.out.writeInt24(((StreamInfo) this.chunkStreams.get(Integer.valueOf(streamID))).getCurrentTimestamp() - prevTime);
    }

    private void sendType3Header(int streamID) throws IOException {
        this.out.write(streamID | 192);
    }

    private void sendType0Header(int streamID, int msgType, int dataLength, int msgStreamID, long ts) throws IOException {
        this.out.write(streamID);
        this.out.writeInt24((int) ts);
        this.out.writeInt24(dataLength);
        this.out.write(msgType);
        this.out.writeIntFlipped(msgStreamID);
    }

    private void sendType1Header(int streamID, int msgType, int dataLength, long ts) throws IOException {
        this.out.write(streamID | 64);
        this.out.writeInt24((int) ts);
        this.out.writeInt24(dataLength);
        this.out.write(msgType);
    }

    private void sendType0Header(XDataOutputStream out, int streamID, int msgType, int dataLength, int msgStreamID, long ts) throws IOException {
        out.write(streamID);
        out.writeInt24((int) ts);
        out.writeInt24(dataLength);
        out.write(msgType);
        out.writeIntFlipped(msgStreamID);
    }

    private void sendType1Header(XDataOutputStream out, int streamID, int msgType, int dataLength, long ts) throws IOException {
        out.write(streamID | 64);
        out.writeInt24((int) ts);
        out.writeInt24(dataLength);
        out.write(msgType);
    }

    private void sendType2Header(int streamID, int prevTime, long ts) throws IOException {
        this.out.write(streamID | 64);
        this.out.writeInt24(prevTime);
    }

    private void sendPing(int type, int param1, int param2) throws IOException {
        log("send ping, %d, %d, %d", Integer.valueOf(type), Integer.valueOf(param1), Integer.valueOf(param2));
        ByteArrayOutputStream aos = new ByteArrayOutputStream();
        DataOutputStream o = new DataOutputStream(aos);
        o.writeShort(7);
        o.writeShort(param1);
        o.writeShort(param2);
        sendType0Header(2, 4, 6, 0);
        this.out.write(aos.toByteArray());
        this.out.flush();
    }

    private void setServerBufferSize(int streamID, int size) throws IOException {
        sendType0Header(2, 4, 10, 0);
        this.out.writeShort(3);
        this.out.writeInt(streamID);
        this.out.writeInt(size);
        this.out.flush();
    }

    private void onCommand(int transactionID, String name, Object[] args) throws IOException {
        if (args[1] instanceof HashMap) {
            HashMap<String, Object> params = args[1];
            if ("status".equals(params.get("level")) && "NetConnection.Connect.Success".equals(params.get("code"))) {
                this.connectSuccessReceived = true;
                log("Connect success!", new Object[0]);
            }
        }
        log("onCommand " + name, new Object[0]);
        for (Object o : args) {
            log(" - " + o, new Object[0]);
        }
        if (name.equals("_result")) {
            this.invokeResults.put(Integer.valueOf(transactionID), args);
        }
    }

    public void play(String stream, int streamID) throws IOException {
        this.streamPlayer = new StreamPlayer(useSpeaker);
        sendPing(3, LocationStatusCodes.GEOFENCE_NOT_AVAILABLE, LocationStatusCodes.GEOFENCE_NOT_AVAILABLE);
        log("INVOKING PLAY ON STREAM %d !!", Integer.valueOf(streamID));
        invoke("play", streamID, 8, null, stream);
        this.playStreamID = streamID;
    }

    public void publish(String stream, int streamID) throws IOException {
        sendType0Header(2, 1, 4, 0);
        this.out.writeInt(GLRenderBuffer.EGL_SURFACE_SIZE);
        this.out.flush();
        XDataOutputStream.chunkSize = GLRenderBuffer.EGL_SURFACE_SIZE;
        this.publishStreamID = streamID;
        invokeNoWait("publish", streamID, 8, null, stream, "live");
        this.streamRecorder = new StreamRecorder();
        if (this.camera != null) {
            this.streamRecorder.setCamera(this.camera);
        }
        new Thread(new C08642(streamID)).start();
    }

    public void setCamera(Camera cam) {
        this.camera = cam;
        if (this.streamRecorder != null) {
            this.streamRecorder.setCamera(cam);
        }
    }

    public void disableCamera() {
        this.camera = null;
    }

    public void disconnect() {
        this.disconnected = true;
        releaseStreams();
        try {
            if (!this.socket.isClosed()) {
                invokeNoWait("deleteStream", 0, 3, null, Integer.valueOf(this.playStreamID));
                invokeNoWait("deleteStream", 0, 3, null, Integer.valueOf(this.publishStreamID));
            }
        } catch (Exception e) {
        }
        try {
            this.socket.close();
        } catch (Exception e2) {
        }
        log("RTMP disconnected.", new Object[0]);
    }

    public void releaseStreams() {
        if (this.streamPlayer != null) {
            this.streamPlayer.stop();
            this.streamPlayer = null;
        }
        if (this.streamRecorder != null) {
            this.streamRecorder.stop();
            this.streamRecorder = null;
        }
        this.invokeLocks.clear();
        this.lastTransactionID = 0;
        this.invokeResults.clear();
        try {
            Thread.sleep(500);
        } catch (Exception e) {
        }
    }
}
