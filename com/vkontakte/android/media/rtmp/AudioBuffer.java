package com.vkontakte.android.media.rtmp;

public class AudioBuffer {
    private short[] buffer;
    private int needed;
    private int neededDone;
    private Object readLock;
    private int readLoops;
    private int readPos;
    private int writeLoops;
    private int writePos;

    public AudioBuffer(int size) {
        this.readLock = new Object();
        this.needed = 0;
        this.neededDone = 0;
        this.buffer = new short[size];
    }

    public void write(short[] data, int num) {
        if (this.writePos + num < this.buffer.length) {
            System.arraycopy(data, 0, this.buffer, this.writePos, num);
            this.writePos += num;
        } else {
            int r1 = this.buffer.length - this.writePos;
            int r2 = num - r1;
            System.arraycopy(data, 0, this.buffer, this.writePos, r1);
            System.arraycopy(data, r1, this.buffer, 0, r2);
            this.writePos = r2;
        }
        if (this.needed > 0) {
            this.neededDone += num;
        }
        if (this.neededDone >= this.needed) {
            synchronized (this.readLock) {
                this.readLock.notify();
            }
            this.needed = 0;
            this.neededDone = 0;
        }
    }

    public void read(short[] b) {
        if (this.readPos + b.length < this.buffer.length) {
            if (this.readPos + b.length < this.writePos) {
                waitForData(this.writePos - (this.readPos + b.length));
            }
            System.arraycopy(this.buffer, this.readPos, b, 0, b.length);
            this.readPos += b.length;
            return;
        }
        int r1 = this.buffer.length - this.readPos;
        int r2 = b.length - r1;
        System.arraycopy(this.buffer, this.readPos, b, 0, r1);
        System.arraycopy(this.buffer, 0, b, r1, r2);
        this.readPos = r2;
    }

    private void waitForData(int len) {
        this.needed = len;
        try {
            synchronized (this.readLock) {
                this.readLock.wait();
            }
        } catch (Exception e) {
        }
        this.needed = 0;
    }
}
