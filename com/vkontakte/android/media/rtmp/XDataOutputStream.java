package com.vkontakte.android.media.rtmp;

import android.support.v4.media.TransportMediator;
import android.support.v4.view.MotionEventCompat;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

public class XDataOutputStream extends DataOutputStream {
    private static final int AMF3_ARRAY = 9;
    private static final int AMF3_BYTE_ARRAY = 12;
    private static final int AMF3_DATE = 8;
    private static final int AMF3_DOUBLE = 5;
    private static final int AMF3_FALSE = 2;
    private static final int AMF3_INT = 4;
    private static final int AMF3_NULL = 1;
    private static final int AMF3_OBJECT = 10;
    private static final int AMF3_STRING = 6;
    private static final int AMF3_TRUE = 3;
    private static final int AMF3_UNDEFINED = 0;
    private static final int AMF3_XML = 11;
    private static final int AMF3_XML_DOC = 7;
    private static final int AMF_BOOLEAN = 1;
    private static final int AMF_DATE = 11;
    private static final int AMF_ECMA_ARRAY = 8;
    private static final int AMF_NULL = 5;
    private static final int AMF_NUMBER = 0;
    private static final int AMF_OBJECT = 3;
    private static final int AMF_OBJECT_END = 9;
    private static final int AMF_REFERENCE = 7;
    private static final int AMF_STRICT_ARRAY = 10;
    private static final int AMF_STRING = 2;
    private static final int AMF_UNDEFINED = 6;
    public static int chunkSize;

    static {
        chunkSize = TransportMediator.FLAG_KEY_MEDIA_NEXT;
    }

    public XDataOutputStream(OutputStream arg0) {
        super(arg0);
    }

    public void writeInt24(int i) throws IOException {
        write(i >> 16);
        write((i >> AMF_ECMA_ARRAY) & MotionEventCompat.ACTION_MASK);
        write(i & MotionEventCompat.ACTION_MASK);
    }

    public void writeAmfNumber(double n) throws IOException {
        write(AMF_NUMBER);
        writeDouble(n);
    }

    public void writeAmfBoolean(boolean b) throws IOException {
        int i = AMF_BOOLEAN;
        write(AMF_BOOLEAN);
        if (!b) {
            i = AMF_NUMBER;
        }
        write(i);
    }

    public void writeAmfString(String s) throws IOException {
        write(AMF_STRING);
        writeUTF(s);
    }

    public void writeAMF(Object o) throws IOException {
        if (o == null) {
            write(AMF_NULL);
        } else if (o instanceof Integer) {
            writeAmfNumber((double) ((Integer) o).intValue());
        } else if (o instanceof Double) {
            writeAmfNumber(((Double) o).doubleValue());
        } else if (o instanceof String) {
            writeAmfString((String) o);
        } else if (o instanceof Boolean) {
            writeAmfBoolean(((Boolean) o).booleanValue());
        } else if (o instanceof HashMap) {
            writeAmfObject((HashMap) o);
        } else {
            write(AMF_UNDEFINED);
        }
    }

    public void writeAmfObject(HashMap<String, Object> obj) throws IOException {
        if (obj == null) {
            write(AMF_NULL);
            return;
        }
        write(AMF_OBJECT);
        for (String k : obj.keySet()) {
            writeUTF(k);
            Object o = obj.get(k);
            Object[] objArr = new Object[AMF_STRING];
            objArr[AMF_NUMBER] = k;
            objArr[AMF_BOOLEAN] = o;
            RTMPClient.log("%s = %s", objArr);
            if (o == null) {
                write(AMF_NULL);
            } else if (o instanceof Integer) {
                writeAmfNumber((double) ((Integer) o).intValue());
            } else if (o instanceof Double) {
                writeAmfNumber(((Double) o).doubleValue());
            } else if (o instanceof String) {
                writeAmfString((String) o);
            } else if (o instanceof Boolean) {
                writeAmfBoolean(((Boolean) o).booleanValue());
            } else if (o instanceof HashMap) {
                writeAmfObject((HashMap) o);
            } else {
                write(AMF_UNDEFINED);
            }
        }
        writeShort(AMF_NUMBER);
        write(AMF_OBJECT_END);
    }

    public void writeChunked(byte[] data, int csID) throws IOException {
        int i = AMF_NUMBER;
        do {
            write(data, i, Math.min(chunkSize, data.length - i));
            i += chunkSize;
            if (i < data.length) {
                write((csID & 63) | 192);
            }
        } while (i < data.length);
    }

    public void writeIntFlipped(int i) throws IOException {
        writeInt((((i >> 24) | ((i >> AMF_ECMA_ARRAY) & MotionEventCompat.ACTION_POINTER_INDEX_MASK)) | ((i << AMF_ECMA_ARRAY) & 16711680)) | ((i & MotionEventCompat.ACTION_MASK) << 24));
    }
}
