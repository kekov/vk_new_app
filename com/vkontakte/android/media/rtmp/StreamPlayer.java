package com.vkontakte.android.media.rtmp;

import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Build.VERSION;
import android.support.v4.view.MotionEventCompat;
import com.vkontakte.android.Log;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.mediapicker.providers.ResourceProvider.Views;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.concurrent.ArrayBlockingQueue;

public class StreamPlayer {
    public static final int CODEC_AVC = 1;
    public static final int CODEC_FLV = 0;
    private Thread ad;
    private Thread ap;
    private PipedInputStream audioIn;
    private PipedOutputStream audioOut;
    private ArrayBlockingQueue<byte[]> audioPackets;
    private AudioTrack audioTrack;
    private AudioBuffer buffer;
    private int playBufSize;
    private boolean running;
    private Thread vd;
    private boolean videoInited;
    private ArrayBlockingQueue<byte[]> videoPackets;

    /* renamed from: com.vkontakte.android.media.rtmp.StreamPlayer.1 */
    class C08651 implements Runnable {
        C08651() {
        }

        public void run() {
            StreamPlayer.this.audioDecoderLoop();
            RTMPClient.log("audio decoder exiting", new Object[0]);
        }
    }

    /* renamed from: com.vkontakte.android.media.rtmp.StreamPlayer.2 */
    class C08662 implements Runnable {
        C08662() {
        }

        public void run() {
            StreamPlayer.this.audioPlayerLoop();
            StreamPlayer.this.releaseSpeexDecoder();
            RTMPClient.log("audio player exiting", new Object[0]);
        }
    }

    /* renamed from: com.vkontakte.android.media.rtmp.StreamPlayer.3 */
    class C08673 implements Runnable {
        C08673() {
        }

        public void run() {
            StreamPlayer.this.videoDecoderLoop();
            StreamPlayer.this.releaseVideoDecoder();
            RTMPClient.log("video decoder exiting", new Object[0]);
        }
    }

    private native int decodeSpeexAudio(byte[] bArr, int i, short[] sArr);

    private native void decodeVideo(byte[] bArr);

    private native void initVideoDecoder(int i, int i2, int i3);

    private native void releaseSpeexDecoder();

    private native void releaseVideoDecoder();

    static {
        System.loadLibrary("ffmpeg");
        System.loadLibrary("rtmpcodecs");
    }

    public StreamPlayer(boolean useSpeaker) {
        this.audioPackets = new ArrayBlockingQueue(15);
        this.videoPackets = new ArrayBlockingQueue(15);
        this.audioOut = new PipedOutputStream();
        this.videoInited = false;
        this.running = true;
        this.playBufSize = AudioTrack.getMinBufferSize(16000, 2, 2);
        Log.m528i("vk", "play buf size = " + this.playBufSize);
        this.audioTrack = new AudioTrack(0, 16000, 2, 2, this.playBufSize * 2, CODEC_AVC);
        this.audioTrack.play();
        AudioManager aMgr = (AudioManager) VKApplication.context.getSystemService("audio");
        if (VERSION.SDK_INT >= 11) {
            aMgr.setMode(3);
        }
        if (!aMgr.isWiredHeadsetOn() && useSpeaker) {
            aMgr.setSpeakerphoneOn(true);
        }
        try {
            this.audioIn = new PipedInputStream();
            this.audioIn.connect(this.audioOut);
        } catch (Exception e) {
        }
        startAudioThreads();
        startVideoThreads();
    }

    public void onAudioData(byte[] d) {
        if (d != null && d.length != 0 && !this.audioPackets.offer(d)) {
            Log.m526e("vk_RTMP", "Dropped audio packet :(");
        }
    }

    public void onVideoData(byte[] d) {
        if (d != null && d.length != 0 && !this.videoPackets.offer(d)) {
            Log.m526e("vk_RTMP", "Dropped video packet :(");
            this.videoPackets.clear();
            this.videoPackets.offer(d);
        }
    }

    private void startAudioThreads() {
        Thread thread = new Thread(new C08651(), "RTMP_AD");
        this.ad = thread;
        thread.start();
        thread = new Thread(new C08662(), "RTMP_AP");
        this.ap = thread;
        thread.start();
    }

    private void startVideoThreads() {
        Thread thread = new Thread(new C08673(), "RTMP_VD");
        this.vd = thread;
        thread.start();
    }

    private void audioDecoderLoop() {
        short[] out = new short[10240];
        DataOutputStream os = new DataOutputStream(this.audioOut);
        while (this.running) {
            try {
                byte[] pkt = (byte[]) this.audioPackets.take();
                this.audioTrack.write(out, 0, decodeSpeexAudio(pkt, pkt.length, out));
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    private void audioPlayerLoop() {
        byte[] buf = new byte[this.playBufSize];
        while (this.running) {
            try {
                if (this.audioIn.read(buf) == 0) {
                    Log.m526e("vk_RTMP", "not read buffer");
                }
                this.audioTrack.write(buf, 0, buf.length);
            } catch (Exception e) {
            }
        }
    }

    private void videoDecoderLoop() {
        ByteArrayOutputStream cfgBuffer = new ByteArrayOutputStream();
        while (this.running) {
            try {
                byte[] pkt = (byte[]) this.videoPackets.take();
                if (!this.videoInited) {
                    initVideoDecoder((pkt[0] & 15) == 2 ? 0 : CODEC_AVC, Views.ACTION_FILTER, 240);
                    this.videoInited = true;
                }
                if ((pkt[0] & 15) == 7) {
                    XDataInputStream xDataInputStream = new XDataInputStream(new ByteArrayInputStream(pkt));
                    xDataInputStream.skip(1);
                    int avcPacketType = xDataInputStream.read();
                    Object[] objArr = new Object[CODEC_AVC];
                    objArr[0] = Integer.valueOf(avcPacketType);
                    RTMPClient.log("AVCPacketType = %d", objArr);
                    int compTime = xDataInputStream.readInt24();
                    byte[] bArr;
                    if (avcPacketType == 0) {
                        int i;
                        if (cfgBuffer.size() == 0) {
                            cfgBuffer.write(0);
                        }
                        int cfgVersion = xDataInputStream.read();
                        int avcProfile = xDataInputStream.read();
                        int profileCompat = xDataInputStream.read();
                        int avcLevel = xDataInputStream.read();
                        int nalLength = xDataInputStream.read() & 3;
                        int numSps = xDataInputStream.read() & 31;
                        byte[] spsData = new byte[0];
                        byte[] ppsData = new byte[0];
                        for (i = 0; i < numSps; i += CODEC_AVC) {
                            int spsLen = xDataInputStream.readShort();
                            objArr = new Object[CODEC_AVC];
                            objArr[0] = Integer.valueOf(spsLen);
                            RTMPClient.log("Reading SPS [%d bytes]", objArr);
                            spsData = new byte[spsLen];
                            xDataInputStream.read(spsData);
                        }
                        int numPps = xDataInputStream.read();
                        for (i = 0; i < numPps; i += CODEC_AVC) {
                            int ppsLen = xDataInputStream.readShort();
                            objArr = new Object[CODEC_AVC];
                            objArr[0] = Integer.valueOf(ppsLen);
                            RTMPClient.log("Reading PPS [%d bytes]", objArr);
                            ppsData = new byte[ppsLen];
                            xDataInputStream.read(ppsData);
                        }
                        ByteArrayOutputStream dcr = new ByteArrayOutputStream();
                        dcr.write(CODEC_AVC);
                        dcr.write(avcProfile);
                        dcr.write(profileCompat);
                        dcr.write(avcLevel);
                        dcr.write(MotionEventCompat.ACTION_MASK);
                        dcr.write(225);
                        dcr.write(0);
                        dcr.write(spsData.length + 4);
                        bArr = new byte[4];
                        bArr[3] = (byte) 1;
                        dcr.write(bArr);
                        dcr.write(spsData);
                        dcr.write(CODEC_AVC);
                        dcr.write(0);
                        dcr.write(ppsData.length + 4);
                        bArr = new byte[4];
                        bArr[3] = (byte) 1;
                        dcr.write(bArr);
                        dcr.write(ppsData);
                        RTMPClient.log("Decoding DCR [%02X %02X %02X]", Integer.valueOf(avcProfile), Integer.valueOf(profileCompat), Integer.valueOf(avcLevel));
                        cfgBuffer.write(dcr.toByteArray());
                    } else if (avcPacketType == CODEC_AVC) {
                        byte[] nal;
                        if (cfgBuffer.size() > 0) {
                            nal = new byte[xDataInputStream.available()];
                            xDataInputStream.read(nal, 0, xDataInputStream.available());
                            RTMPClient.log("NAL type is %d, %02X", Byte.valueOf(nal[3]), Byte.valueOf(nal[4]));
                            bArr = new byte[4];
                            bArr[3] = (byte) 1;
                            cfgBuffer.write(bArr);
                            cfgBuffer.write(nal, 4, nal.length - 4);
                            decodeVideo(cfgBuffer.toByteArray());
                            cfgBuffer.reset();
                        } else {
                            nal = new byte[(xDataInputStream.available() + CODEC_AVC)];
                            xDataInputStream.read(nal, CODEC_AVC, xDataInputStream.available());
                            RTMPClient.log("NAL type is %d, %02X", Byte.valueOf(nal[4]), Byte.valueOf(nal[5]));
                            decodeVideo(nal);
                        }
                    }
                }
                if ((pkt[0] & 15) == 2) {
                    decodeVideo(pkt);
                }
            } catch (Exception e) {
            }
        }
    }

    public void stop() {
        this.running = false;
        this.vd.interrupt();
        this.ad.interrupt();
        this.ap.interrupt();
    }
}
