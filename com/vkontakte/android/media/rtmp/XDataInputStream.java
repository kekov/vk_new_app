package com.vkontakte.android.media.rtmp;

import com.vkontakte.android.Log;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class XDataInputStream extends DataInputStream {
    private static final int AMF_BOOLEAN = 1;
    private static final int AMF_DATE = 11;
    private static final int AMF_ECMA_ARRAY = 8;
    private static final int AMF_NULL = 5;
    private static final int AMF_NUMBER = 0;
    private static final int AMF_OBJECT = 3;
    private static final int AMF_OBJECT_END = 9;
    private static final int AMF_REFERENCE = 7;
    private static final int AMF_STRICT_ARRAY = 10;
    private static final int AMF_STRING = 2;
    private static final int AMF_UNDEFINED = 6;

    public XDataInputStream(InputStream arg0) {
        super(arg0);
    }

    public int readInt24() throws IOException {
        return ((read() << 16) | (read() << AMF_ECMA_ARRAY)) | read();
    }

    public Object readNextAmf() throws IOException {
        int type = read();
        switch (type) {
            case AMF_NUMBER /*0*/:
                return Double.valueOf(readDouble());
            case AMF_BOOLEAN /*1*/:
                return readInt() > 0 ? Boolean.valueOf(true) : Boolean.valueOf(false);
            case AMF_STRING /*2*/:
                return readUTF();
            case AMF_OBJECT /*3*/:
                break;
            case AMF_REFERENCE /*7*/:
            case AMF_OBJECT_END /*9*/:
            case AMF_STRICT_ARRAY /*10*/:
            case AMF_DATE /*11*/:
                Log.m526e("vk_RTMP", "OH SHI~ " + type);
                return null;
            case AMF_ECMA_ARRAY /*8*/:
                readInt();
                break;
            default:
                return null;
        }
        return readAmfObject();
    }

    public HashMap<String, Object> readAmfObject() throws IOException {
        HashMap<String, Object> result = new HashMap();
        String dbg = "{";
        while (true) {
            String key = readUTF();
            if (key.length() == 0) {
                Log.m528i("vk_RTMP", new StringBuilder(String.valueOf(dbg)).append("}").toString());
                read();
                return result;
            }
            Object o = readNextAmf();
            result.put(key, o);
            dbg = new StringBuilder(String.valueOf(dbg)).append(key).append(" => ").append(o).append("\n").toString();
        }
    }

    public int readFull(byte[] b) throws IOException {
        int numRead = AMF_NUMBER;
        while (numRead < b.length) {
            numRead += read(b, numRead, b.length - numRead);
        }
        return b.length;
    }
}
