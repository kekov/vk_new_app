package com.vkontakte.android.media.rtmp;

import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class VideoOutput implements Renderer {
    private native void nativeDraw();

    private native void nativeInit();

    private native void nativeSetRotation(int i);

    private native void nativeSetViewport(int i, int i2);

    static {
        System.loadLibrary("ffmpeg");
        System.loadLibrary("rtmpcodecs");
    }

    public VideoOutput(GLSurfaceView surface) {
        surface.setEGLContextClientVersion(2);
        surface.setRenderer(this);
    }

    public void onDrawFrame(GL10 gl) {
        nativeDraw();
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        nativeSetViewport(width, height);
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        nativeInit();
    }

    public void setFrameOrientation(int o) {
        if (o == 0 || o == 90 || o == 180 || o == 270) {
            nativeSetRotation(o);
            return;
        }
        throw new IllegalArgumentException("WTF is orientation " + o);
    }
}
