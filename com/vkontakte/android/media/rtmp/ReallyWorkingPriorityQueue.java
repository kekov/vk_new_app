package com.vkontakte.android.media.rtmp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class ReallyWorkingPriorityQueue<E> implements BlockingQueue<E> {
    private ArrayList<E> entries;
    private Object lock;
    private Semaphore sp;

    public interface PriorityElement {
        int getPriority();
    }

    public ReallyWorkingPriorityQueue() {
        this.entries = new ArrayList();
        this.lock = new Object();
        this.sp = new Semaphore(1, true);
    }

    public E element() {
        if (this.entries.size() != 0) {
            return this.entries.get(0);
        }
        throw new NoSuchElementException();
    }

    public E peek() {
        if (this.entries.size() == 0) {
            return null;
        }
        return this.entries.get(0);
    }

    public E poll() {
        if (this.entries.size() == 0) {
            return null;
        }
        return this.entries.remove(0);
    }

    public E remove() {
        if (this.entries.size() != 0) {
            return this.entries.remove(0);
        }
        throw new NoSuchElementException();
    }

    public boolean addAll(Collection<? extends E> collection) {
        return false;
    }

    public void clear() {
        this.entries.clear();
    }

    public boolean containsAll(Collection<?> c) {
        return this.entries.containsAll(c);
    }

    public boolean isEmpty() {
        return this.entries.isEmpty();
    }

    public Iterator<E> iterator() {
        return this.entries.iterator();
    }

    public boolean removeAll(Collection<?> c) {
        return this.entries.removeAll(c);
    }

    public boolean retainAll(Collection<?> c) {
        return this.entries.retainAll(c);
    }

    public int size() {
        return this.entries.size();
    }

    public Object[] toArray() {
        return this.entries.toArray();
    }

    public <T> T[] toArray(T[] array) {
        return this.entries.toArray(array);
    }

    public boolean add(E e) {
        offer(e);
        return true;
    }

    public boolean contains(Object o) {
        return this.entries.contains(o);
    }

    public int drainTo(Collection<? super E> collection) {
        return 0;
    }

    public int drainTo(Collection<? super E> collection, int arg1) {
        return 0;
    }

    public boolean offer(E e) {
        try {
            this.sp.acquire();
        } catch (Exception e2) {
        }
        int priority = ((PriorityElement) e).getPriority();
        boolean added = false;
        int i = this.entries.size() - 1;
        while (i >= 0) {
            int ep = ((PriorityElement) this.entries.get(i)).getPriority();
            if (ep != priority) {
                if (ep <= priority) {
                    if (i == 0 && ep < priority) {
                        this.entries.add(0, e);
                        added = true;
                        break;
                    }
                    i--;
                } else {
                    this.entries.add(i + 1, e);
                    added = true;
                    break;
                }
            }
            this.entries.add(i + 1, e);
            added = true;
            break;
        }
        if (!added) {
            this.entries.add(e);
        }
        synchronized (this.lock) {
            this.lock.notifyAll();
        }
        this.sp.release();
        return true;
    }

    public boolean offer(E e, long timeout, TimeUnit unit) throws InterruptedException {
        return false;
    }

    public E poll(long timeout, TimeUnit unit) throws InterruptedException {
        return null;
    }

    public void put(E e) throws InterruptedException {
    }

    public int remainingCapacity() {
        return 0;
    }

    public boolean remove(Object o) {
        return false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public E take() throws java.lang.InterruptedException {
        /*
        r4 = this;
        r3 = 0;
        r0 = 0;
        r2 = r4.entries;
        r2 = r2.size();
        if (r2 <= 0) goto L_0x0022;
    L_0x000a:
        r2 = r4.sp;	 Catch:{ Exception -> 0x0041 }
        r2.acquire();	 Catch:{ Exception -> 0x0041 }
    L_0x000f:
        r2 = r4.entries;
        r0 = r2.remove(r3);
        r2 = r4.sp;
        r2.release();
        r1 = r0;
    L_0x001b:
        return r1;
    L_0x001c:
        r2 = move-exception;
        r2 = r4.sp;
        r2.release();
    L_0x0022:
        r3 = r4.lock;
        monitor-enter(r3);
        r2 = r4.lock;	 Catch:{ all -> 0x003e }
        r2.wait();	 Catch:{ all -> 0x003e }
        monitor-exit(r3);	 Catch:{ all -> 0x003e }
        r2 = r4.sp;	 Catch:{ Exception -> 0x001c }
        r2.acquire();	 Catch:{ Exception -> 0x001c }
        r2 = r4.entries;	 Catch:{ Exception -> 0x001c }
        r3 = 0;
        r0 = r2.remove(r3);	 Catch:{ Exception -> 0x001c }
        r2 = r4.sp;	 Catch:{ Exception -> 0x001c }
        r2.release();	 Catch:{ Exception -> 0x001c }
        r1 = r0;
        goto L_0x001b;
    L_0x003e:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x003e }
        throw r2;
    L_0x0041:
        r2 = move-exception;
        goto L_0x000f;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.media.rtmp.ReallyWorkingPriorityQueue.take():E");
    }
}
