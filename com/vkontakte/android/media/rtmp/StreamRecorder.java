package com.vkontakte.android.media.rtmp;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.media.AudioRecord;
import com.vkontakte.android.Log;
import com.vkontakte.android.media.rtmp.ReallyWorkingPriorityQueue.PriorityElement;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class StreamRecorder {
    private Thread ah;
    private Thread ar;
    private ArrayBlockingQueue<byte[]> cameraFrames;
    private ArrayBlockingQueue<StreamPacket> encodedAudio;
    private ReallyWorkingPriorityQueue<StreamPacket> encodedPackets;
    private ArrayBlockingQueue<StreamPacket> encodedVideo;
    private int lastAudioTimestamp;
    private long lastCamFrameTime;
    private long lastPktTakeTime;
    private AudioRecord record;
    private int recordBufferSize;
    private boolean running;
    private int singleFrameTime;
    private long streamStartTime;
    private Thread ve;
    private Thread vh;
    private boolean videoEncoderInited;
    private int videoHeight;
    private int videoWidth;

    /* renamed from: com.vkontakte.android.media.rtmp.StreamRecorder.1 */
    class C08681 implements Runnable {
        C08681() {
        }

        public void run() {
            Log.m525d("vk_RTMP", "Starting record thread");
            StreamRecorder.this.record.startRecording();
            Log.m525d("vk_RTMP", "AudioRecord start ok");
            StreamRecorder.this.audioRecordLoop();
            StreamRecorder.this.record.stop();
            StreamRecorder.this.record.release();
            StreamRecorder.this.record = null;
            StreamRecorder.this.releaseSpeexEncoder();
            RTMPClient.log("audio recorder/encoder stopped", new Object[0]);
        }
    }

    /* renamed from: com.vkontakte.android.media.rtmp.StreamRecorder.2 */
    class C08692 implements Runnable {
        C08692() {
        }

        public void run() {
            StreamRecorder.this.videoEncodeLoop();
            StreamRecorder.this.releaseVideoEncoder();
            RTMPClient.log("video encoder stopped", new Object[0]);
        }
    }

    /* renamed from: com.vkontakte.android.media.rtmp.StreamRecorder.3 */
    class C08703 implements Runnable {
        C08703() {
        }

        public void run() {
            Thread.currentThread().setPriority(9);
            StreamRecorder.this.videoHelperLoop();
            RTMPClient.log("video helper stopped", new Object[0]);
        }
    }

    /* renamed from: com.vkontakte.android.media.rtmp.StreamRecorder.4 */
    class C08714 implements Runnable {
        C08714() {
        }

        public void run() {
            Thread.currentThread().setPriority(9);
            StreamRecorder.this.audioHelperLoop();
            RTMPClient.log("audio helper stopped", new Object[0]);
        }
    }

    /* renamed from: com.vkontakte.android.media.rtmp.StreamRecorder.5 */
    class C08725 implements PreviewCallback {
        C08725() {
        }

        public void onPreviewFrame(byte[] data, Camera camera) {
            if (data != null && System.currentTimeMillis() - StreamRecorder.this.lastCamFrameTime >= 50) {
                if (!StreamRecorder.this.cameraFrames.offer(data)) {
                    Log.m528i("vk", "frame queue overflow");
                    StreamRecorder.this.cameraFrames.clear();
                    StreamRecorder.this.cameraFrames.offer(data);
                }
                StreamRecorder.this.lastCamFrameTime = System.currentTimeMillis();
            }
            camera.addCallbackBuffer(new byte[(((StreamRecorder.this.videoWidth * StreamRecorder.this.videoHeight) * 3) / 2)]);
        }
    }

    public static class StreamPacket implements PriorityElement, Delayed {
        byte[] data;
        boolean first;
        int fullSize;
        long timestamp;
        int type;

        public StreamPacket(byte[] d, int t, long ts, boolean f, int s) {
            this.data = d;
            this.type = t;
            this.timestamp = ts;
            this.first = f;
            this.fullSize = s;
        }

        public int getPriority() {
            return ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED - ((int) this.timestamp);
        }

        public int compareTo(Delayed arg0) {
            return 0;
        }

        public long getDelay(TimeUnit unit) {
            long delay = this.timestamp - ((long) RTMPClient.time());
            if (unit == TimeUnit.MILLISECONDS) {
                return delay;
            }
            if (unit == TimeUnit.NANOSECONDS) {
                return delay * 1000;
            }
            Log.m530w("vk", "unknown unit " + unit);
            return delay;
        }
    }

    private native int encodeSpeexAudio(byte[] bArr, byte[] bArr2);

    private native byte[] encodeVideo(byte[] bArr);

    private native void initVideoEncoder(int i, int i2);

    private native void releaseSpeexEncoder();

    private native void releaseVideoEncoder();

    public static native void setEchoParam(int i);

    static {
        System.loadLibrary("ffmpeg");
        System.loadLibrary("rtmpcodecs");
    }

    public StreamRecorder() {
        this.encodedPackets = new ReallyWorkingPriorityQueue();
        this.cameraFrames = new ArrayBlockingQueue(7);
        this.encodedVideo = new ArrayBlockingQueue(20);
        this.encodedAudio = new ArrayBlockingQueue(40);
        this.lastPktTakeTime = 0;
        this.videoEncoderInited = false;
        this.singleFrameTime = 67;
        this.streamStartTime = 0;
        this.running = true;
        this.recordBufferSize = Math.max(AudioRecord.getMinBufferSize(16000, 2, 2), 640);
        if (this.recordBufferSize % 640 > 0) {
            this.recordBufferSize += 640 - (this.recordBufferSize % 640);
        }
        RTMPClient.log("Record buffer size=%d", Integer.valueOf(this.recordBufferSize));
        this.record = new AudioRecord(1, 16000, 2, 2, this.recordBufferSize);
        startRecordThread();
    }

    private void startRecordThread() {
        Thread thread = new Thread(new C08681(), "RTMP_AR");
        this.ar = thread;
        thread.start();
        thread = new Thread(new C08692(), "RTMP_VE");
        this.ve = thread;
        thread.start();
        thread = new Thread(new C08703(), "RTMP_VH");
        this.vh = thread;
        thread.start();
        thread = new Thread(new C08714(), "RTMP_AH");
        this.ah = thread;
        thread.start();
    }

    public void setCamera(Camera cam) {
        if (cam != null) {
            Size previewSize = cam.getParameters().getPreviewSize();
            this.videoWidth = previewSize.width;
            this.videoHeight = previewSize.height;
            for (int i = 0; i < 3; i++) {
                cam.addCallbackBuffer(new byte[(((this.videoWidth * this.videoHeight) * 3) / 2)]);
            }
            Parameters params = cam.getParameters();
            List<Integer> sfps = params.getSupportedPreviewFrameRates();
            if (sfps.contains(Integer.valueOf(15))) {
                params.setPreviewFrameRate(15);
            } else if (sfps.contains(Integer.valueOf(10))) {
                params.setPreviewFrameRate(10);
            } else {
                Log.m526e("vk", "using camera default FPS!");
            }
            cam.setParameters(params);
            cam.setPreviewCallbackWithBuffer(new C08725());
        }
    }

    private void audioRecordLoop() {
        byte[] raw = new byte[this.recordBufferSize];
        byte[] buf = new byte[GLRenderBuffer.EGL_SURFACE_SIZE];
        byte[] buf2 = new byte[640];
        long ts = System.currentTimeMillis();
        while (this.running) {
            try {
                int nRead = this.record.read(raw, 0, raw.length);
                for (int offset = 0; offset < raw.length; offset += 640) {
                    System.arraycopy(raw, offset, buf2, 0, 640);
                    int len = encodeSpeexAudio(buf2, buf);
                    if (ts == 0) {
                        ts = (long) RTMPClient.time();
                    }
                    byte[] pkt = new byte[(len + 1)];
                    System.arraycopy(buf, 0, pkt, 1, len);
                    pkt[0] = (byte) -78;
                    this.encodedAudio.offer(new StreamPacket(pkt, 8, ts, true, pkt.length));
                }
                ts = System.currentTimeMillis();
            } catch (Throwable x) {
                Log.m532w("vk_RTMP", x);
            }
        }
    }

    private void videoEncodeLoop() {
        long st = System.currentTimeMillis();
        while (this.running) {
            try {
                if (!this.videoEncoderInited) {
                    initVideoEncoder(this.videoWidth, this.videoHeight);
                    this.videoEncoderInited = true;
                }
                byte[] frame = encodeVideo((byte[]) this.cameraFrames.take());
                long ts = (long) RTMPClient.time();
                this.encodedVideo.offer(new StreamPacket(frame, 9, ts, true, frame.length));
                ts++;
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void videoHelperLoop() {
        /*
        r3 = this;
    L_0x0000:
        r1 = r3.running;
        if (r1 != 0) goto L_0x0005;
    L_0x0004:
        return;
    L_0x0005:
        r1 = 66;
        java.lang.Thread.sleep(r1);	 Catch:{ Exception -> 0x0043 }
        r1 = r3.encodedVideo;	 Catch:{ Exception -> 0x0043 }
        r1 = r1.size();	 Catch:{ Exception -> 0x0043 }
        r2 = 2;
        if (r1 < r2) goto L_0x0000;
    L_0x0013:
        r1 = r3.encodedVideo;	 Catch:{ Exception -> 0x0043 }
        r0 = r1.take();	 Catch:{ Exception -> 0x0043 }
        r0 = (com.vkontakte.android.media.rtmp.StreamRecorder.StreamPacket) r0;	 Catch:{ Exception -> 0x0043 }
        r1 = com.vkontakte.android.media.rtmp.RTMPClient.time();	 Catch:{ Exception -> 0x0043 }
        r1 = (long) r1;	 Catch:{ Exception -> 0x0043 }
        r0.timestamp = r1;	 Catch:{ Exception -> 0x0043 }
        r1 = r3.encodedPackets;	 Catch:{ Exception -> 0x0043 }
        r1.offer(r0);	 Catch:{ Exception -> 0x0043 }
    L_0x0027:
        r1 = r3.encodedVideo;	 Catch:{ Exception -> 0x0043 }
        r0 = r1.peek();	 Catch:{ Exception -> 0x0043 }
        r0 = (com.vkontakte.android.media.rtmp.StreamRecorder.StreamPacket) r0;	 Catch:{ Exception -> 0x0043 }
        if (r0 == 0) goto L_0x0000;
    L_0x0031:
        r1 = r0.first;	 Catch:{ Exception -> 0x0043 }
        if (r1 != 0) goto L_0x0000;
    L_0x0035:
        r2 = r3.encodedPackets;	 Catch:{ Exception -> 0x0043 }
        r1 = r3.encodedVideo;	 Catch:{ Exception -> 0x0043 }
        r1 = r1.poll();	 Catch:{ Exception -> 0x0043 }
        r1 = (com.vkontakte.android.media.rtmp.StreamRecorder.StreamPacket) r1;	 Catch:{ Exception -> 0x0043 }
        r2.offer(r1);	 Catch:{ Exception -> 0x0043 }
        goto L_0x0027;
    L_0x0043:
        r1 = move-exception;
        goto L_0x0000;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.media.rtmp.StreamRecorder.videoHelperLoop():void");
    }

    private void audioHelperLoop() {
        long delay = 20;
        while (this.running) {
            if (delay > 0) {
                try {
                    Thread.sleep(delay);
                } catch (Exception e) {
                }
            }
            if (this.encodedAudio.size() < 2) {
                delay = 20;
            } else {
                long t = System.currentTimeMillis();
                StreamPacket p = (StreamPacket) this.encodedAudio.take();
                p.timestamp = (long) RTMPClient.time();
                this.encodedPackets.offer(p);
                delay = 20 - (System.currentTimeMillis() - t);
            }
        }
    }

    public StreamPacket getNextPacket() {
        while (true) {
            try {
                break;
            } catch (Exception e) {
            }
        }
        return (StreamPacket) this.encodedPackets.take();
    }

    public void stop() {
        this.running = false;
        this.ar.interrupt();
        this.ve.interrupt();
        this.ah.interrupt();
        this.vh.interrupt();
    }
}
