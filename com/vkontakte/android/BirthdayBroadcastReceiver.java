package com.vkontakte.android;

import android.app.Notification;
import android.app.Notification.BigTextStyle;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.cache.Cache;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;

public class BirthdayBroadcastReceiver extends BroadcastReceiver {
    private static final int ID_BIRTHDAY_NOTIFICATION = 500;

    public void onReceive(Context context, Intent intent) {
        int lastDate = context.getSharedPreferences(null, 0).getInt("last_birthday_notify", 0);
        if (!intent.getBooleanExtra("force", false)) {
            if (lastDate != getCurrentDate(System.currentTimeMillis())) {
                context.getSharedPreferences(null, 0).edit().putInt("last_birthday_notify", getCurrentDate(System.currentTimeMillis())).commit();
            } else {
                return;
            }
        }
        long time = intent.getLongExtra("date", System.currentTimeMillis());
        ArrayList<UserProfile> bdays = Cache.getBirthdays(time);
        if (bdays.size() > 0) {
            int psize = Global.scale(40.0f);
            ArrayList<String> names = new ArrayList();
            ArrayList<UserProfile> users = new ArrayList();
            Date date = new Date(time);
            String today = date.getDate() + "." + (date.getMonth() + 1) + ".";
            Date date2 = new Date(86400000 + time);
            String yesterday = date2.getDate() + "." + (date2.getMonth() + 1) + ".";
            boolean hasToday = false;
            Iterator it = bdays.iterator();
            while (it.hasNext()) {
                UserProfile p = (UserProfile) it.next();
                if (p.bdate.startsWith(today)) {
                    hasToday = true;
                }
                if (p.bdate.startsWith(today)) {
                    names.add(p.university);
                }
                String[] ss = p.bdate.split("\\.");
                int year = Integer.parseInt(ss[2]);
                if (p.bdate.startsWith(today)) {
                    p.firstName = context.getResources().getString(C0436R.string.today);
                } else {
                    if (p.bdate.startsWith(yesterday)) {
                        p.firstName = context.getResources().getString(C0436R.string.tomorrow);
                    } else {
                        p.firstName = ss[0] + " " + context.getResources().getStringArray(C0436R.array.months_full)[Integer.parseInt(ss[1]) - 1];
                    }
                }
                if (year > 0) {
                    int age = (date.getYear() + 1900) - year;
                    p.firstName += ", " + Global.langPlural(C0436R.array.birthday_age, age, context.getResources());
                }
                users.add(p);
            }
            if (hasToday) {
                Intent nIntent;
                Notification n;
                Resources resources = context.getResources();
                Object[] objArr = new Object[1];
                objArr[0] = TextUtils.join(", ", names);
                String notifyText = resources.getString(C0436R.string.birthday_today_short, objArr);
                Bundle args;
                if (users.size() == 1) {
                    args = new Bundle();
                    String str = "id";
                    args.putInt(r27, ((UserProfile) users.get(0)).uid);
                    nIntent = new Intent(context, FragmentWrapperActivity.class);
                    nIntent.putExtra("class", "ProfileFragment");
                    nIntent.putExtra("args", args);
                } else {
                    args = new Bundle();
                    args.putInt(WebDialog.DIALOG_PARAM_TYPE, 3);
                    args.putParcelableArrayList("users", users);
                    args.putBoolean("extended", true);
                    args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, context.getResources().getString(C0436R.string.birthdays_title));
                    nIntent = new Intent(context, FragmentWrapperActivity.class);
                    nIntent.putExtra("class", "UserListFragment");
                    nIntent.putExtra("args", args);
                }
                nIntent.setAction("birthday" + new Random().nextInt());
                PendingIntent cIntent = PendingIntent.getActivity(context, 0, nIntent, 268435456);
                if (VERSION.SDK_INT < 16) {
                    n = new Notification(C0436R.drawable.ic_stat_notify_birthday, null, 0);
                    n.setLatestEventInfo(context, context.getResources().getString(C0436R.string.app_name), notifyText, cIntent);
                } else {
                    n = new BigTextStyle(new Builder(context).setPriority(-2).setContentTitle(context.getString(C0436R.string.reminder)).setContentText(notifyText).setSmallIcon(C0436R.drawable.ic_stat_notify_birthday).setContentIntent(cIntent)).setBigContentTitle(context.getString(C0436R.string.reminder)).bigText(notifyText).build();
                }
                n.flags |= 16;
                ((NotificationManager) context.getSystemService("notification")).notify(ID_BIRTHDAY_NOTIFICATION, n);
            } else {
                return;
            }
        }
        if (MenuListView.lastInstance != null) {
            MenuListView.lastInstance.updateBirthdays();
        }
    }

    private int getCurrentDate(long t) {
        Date d = new Date(t);
        return (d.getDate() + (d.getMonth() * 100)) + (d.getYear() * 10000);
    }
}
