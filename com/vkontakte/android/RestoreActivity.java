package com.vkontakte.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.Auth.AuthResultReceiver;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.AccountChangePassword;
import com.vkontakte.android.api.AuthRestore;
import com.vkontakte.android.api.AuthRestore.Callback;
import com.vkontakte.android.fragments.SignupCodeFragment;
import com.vkontakte.android.fragments.SignupCodeFragment.OnResendListener;
import com.vkontakte.android.fragments.SignupPasswordFragment;
import com.vkontakte.android.fragments.SignupPhoneFragment;
import com.vkontakte.android.ui.ActionBarHacks;
import com.vkontakte.android.ui.ActionBarProgressDrawable;
import java.util.HashMap;

public class RestoreActivity extends VKFragmentActivity {
    private static final int[] titles;
    private String code;
    private SignupCodeFragment codeFragment;
    private int curStep;
    private String number;
    private String passHash;
    private String password;
    private SignupPasswordFragment passwordFragment;
    private SignupPhoneFragment phoneFragment;
    private ActionBarProgressDrawable progress;
    private ProgressDialog progressDialog;
    private boolean restoreDone;
    private String sid;
    private FrameLayout wrap;

    /* renamed from: com.vkontakte.android.RestoreActivity.2 */
    class C04492 implements OnClickListener {

        /* renamed from: com.vkontakte.android.RestoreActivity.2.1 */
        class C04481 implements Runnable {
            C04481() {
            }

            public void run() {
                RestoreActivity.this.setStep(1);
            }
        }

        C04492() {
        }

        public void onClick(View v) {
            if (RestoreActivity.this.phoneFragment.isFilled()) {
                RestoreActivity.this.number = RestoreActivity.this.phoneFragment.getNumber();
                RestoreActivity.this.requestCode(RestoreActivity.this.sid, false, new C04481());
            }
        }
    }

    /* renamed from: com.vkontakte.android.RestoreActivity.3 */
    class C04503 implements Runnable {
        C04503() {
        }

        public void run() {
            RestoreActivity.this.phoneFragment.setNumber(RestoreActivity.this.getIntent().getStringExtra("phone"));
        }
    }

    /* renamed from: com.vkontakte.android.RestoreActivity.4 */
    class C04514 implements Runnable {
        C04514() {
        }

        public void run() {
            RestoreActivity.this.progress.setStepAnimated(RestoreActivity.this.curStep);
        }
    }

    /* renamed from: com.vkontakte.android.RestoreActivity.6 */
    class C04526 implements OnClickListener {
        C04526() {
        }

        public void onClick(View v) {
            RestoreActivity.this.verifyCode(RestoreActivity.this.codeFragment.getCode());
        }
    }

    /* renamed from: com.vkontakte.android.RestoreActivity.7 */
    class C04537 implements OnClickListener {
        C04537() {
        }

        public void onClick(View v) {
            RestoreActivity.this.password = RestoreActivity.this.passwordFragment.getPassword();
            if (RestoreActivity.this.password.length() < 6) {
                RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.signup_pass_too_short));
            } else {
                RestoreActivity.this.completeRestore();
            }
        }
    }

    /* renamed from: com.vkontakte.android.RestoreActivity.1 */
    class C13441 extends ActionBarProgressDrawable {
        C13441() {
        }

        public void invalidateSelf() {
            super.invalidateSelf();
            View abv = ActionBarHacks.getActionBar(RestoreActivity.this);
            if (abv != null) {
                abv.postInvalidate();
            }
        }
    }

    /* renamed from: com.vkontakte.android.RestoreActivity.5 */
    class C13455 implements OnResendListener {
        C13455() {
        }

        public void resendCode(boolean voice, Runnable action) {
            RestoreActivity.this.requestCode(RestoreActivity.this.sid, voice, action);
        }
    }

    /* renamed from: com.vkontakte.android.RestoreActivity.8 */
    class C13468 implements Callback {
        private final /* synthetic */ Runnable val$runAfter;

        C13468(Runnable runnable) {
            this.val$runAfter = runnable;
        }

        public void success(String sid) {
            RestoreActivity.this.sid = sid;
            if (this.val$runAfter != null) {
                this.val$runAfter.run();
            }
        }

        public void fail(int ecode, String emsg) {
            if (ecode == -1) {
                RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.err_text));
            } else if (ecode == 9 || ecode == 1112) {
                RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.signup_flood));
            } else if (ecode == LocationStatusCodes.GEOFENCE_NOT_AVAILABLE) {
                RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.signup_invalid_phone));
            } else if (ecode == 100) {
                if (emsg.contains("first_name") || emsg.contains("last_name")) {
                    RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.signup_invalid_name));
                    RestoreActivity.this.setStep(0);
                } else if (emsg.contains("phone")) {
                    RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.signup_invalid_phone_format));
                } else {
                    RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.unknown_error, new Object[]{emsg, Integer.valueOf(ecode)}));
                }
            } else if (ecode != 15) {
                RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.unknown_error, new Object[]{emsg, Integer.valueOf(ecode)}));
            } else if (emsg.contains("user not found")) {
                RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.error_user_not_found));
            } else if (emsg.contains("not available")) {
                RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.error_not_avail_for_user));
            } else {
                RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.unknown_error, new Object[]{emsg, Integer.valueOf(ecode)}));
            }
        }
    }

    /* renamed from: com.vkontakte.android.RestoreActivity.9 */
    class C13479 implements AuthResultReceiver {

        /* renamed from: com.vkontakte.android.RestoreActivity.9.1 */
        class C04541 implements Runnable {
            private final /* synthetic */ HashMap val$extras;
            private final /* synthetic */ int val$result;

            C04541(HashMap hashMap, int i) {
                this.val$extras = hashMap;
                this.val$result = i;
            }

            public void run() {
                RestoreActivity.this.progressDialog.dismiss();
                RestoreActivity.this.passHash = (String) this.val$extras.get("change_password_hash");
                if (this.val$result == Auth.REAUTH_SUCCESS) {
                    RestoreActivity.this.setStep(2);
                } else if (Auth.lastError.contains("code")) {
                    RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.signup_code_incorrect));
                } else {
                    RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.unknown_error, new Object[]{Auth.lastError, Integer.valueOf(0)}));
                }
            }
        }

        C13479() {
        }

        public void authDone(int result, HashMap<String, String> extras) {
            RestoreActivity.this.runOnUiThread(new C04541(extras, result));
        }
    }

    public RestoreActivity() {
        this.curStep = 0;
        this.restoreDone = false;
    }

    static {
        titles = new int[]{C0436R.string.forgot_pass, C0436R.string.signup_code_title, C0436R.string.restore_password_title};
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage(getString(C0436R.string.loading));
        this.progress = new C13441();
        getSupportActionBar().setBackgroundDrawable(this.progress);
        this.progress.setStepCount(3);
        this.wrap = new FrameLayout(this);
        this.wrap.setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        this.wrap.setId(C0436R.id.fragment_wrapper);
        this.phoneFragment = new SignupPhoneFragment();
        this.phoneFragment.setOnNextClickListener(new C04492());
        if (getIntent().hasExtra("phone")) {
            this.wrap.postDelayed(new C04503(), 200);
        }
        setContentView(this.wrap);
        getSupportFragmentManager().beginTransaction().add((int) C0436R.id.fragment_wrapper, this.phoneFragment).commit();
        this.phoneFragment.setButtonText(getString(C0436R.string.welcome_next));
        this.phoneFragment.setExplainText(getString(C0436R.string.restore_phone_explain));
        this.phoneFragment.setShowForgitButton(true);
    }

    public void onDestroy() {
        super.onDestroy();
        if (!this.restoreDone) {
            Global.uid = 0;
            Global.accessToken = null;
            Global.secret = null;
        }
    }

    private void setStep(int step) {
        if (step > this.curStep) {
            boolean animateForward = true;
        }
        this.curStep = step;
        this.wrap.postDelayed(new C04514(), 100);
        setTitle(titles[step]);
        if (step == 0) {
            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, this.phoneFragment).commit();
        }
        if (step == 1) {
            if (this.codeFragment == null) {
                this.codeFragment = new SignupCodeFragment();
                this.codeFragment.setOnResendListener(new C13455());
                this.codeFragment.setOnNextClickListener(new C04526());
            }
            this.codeFragment.setNumber(this.phoneFragment.getNumber());
            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, this.codeFragment).commit();
        }
        if (step == 2) {
            if (this.passwordFragment == null) {
                this.passwordFragment = new SignupPasswordFragment();
                this.passwordFragment.setOnNextClickListener(new C04537());
                this.passwordFragment.setExplainText(getString(C0436R.string.restore_password_explain));
            }
            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, this.passwordFragment).commitAllowingStateLoss();
        }
    }

    private void requestCode(String sid, boolean voice, Runnable runAfter) {
        new AuthRestore(this.number, sid, voice).setCallback(new C13468(runAfter)).wrapProgress(this).exec((Activity) this);
    }

    private void verifyCode(String code) {
        if (code.length() != 0) {
            this.progressDialog.show();
            Auth.authorizeRestoreAsync(this.sid, code, new C13479());
        }
    }

    private void completeRestore() {
        new AccountChangePassword(this.sid, this.passHash, this.password).setCallback(new AccountChangePassword.Callback() {

            /* renamed from: com.vkontakte.android.RestoreActivity.10.1 */
            class C04471 implements Runnable {
                private final /* synthetic */ int val$ecode;
                private final /* synthetic */ String val$emsg;

                C04471(int i, String str) {
                    this.val$ecode = i;
                    this.val$emsg = str;
                }

                public void run() {
                    if (this.val$ecode == 1110) {
                        RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.signup_code_incorrect));
                    } else if (this.val$ecode == 1111) {
                    } else {
                        if (this.val$ecode == -1) {
                            RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.err_text));
                        } else if (this.val$ecode == 15) {
                            if (this.val$emsg.contains("change_password_hash")) {
                                RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.error_cphash_obsolete));
                                return;
                            }
                            RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.unknown_error, new Object[]{this.val$emsg, Integer.valueOf(this.val$ecode)}));
                        } else if (this.val$ecode != 100) {
                            RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.unknown_error, new Object[]{this.val$emsg, Integer.valueOf(this.val$ecode)}));
                        } else if (this.val$emsg.contains("compromised")) {
                            RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.error_pass_compromised));
                        } else {
                            RestoreActivity.this.showError(RestoreActivity.this.getString(C0436R.string.unknown_error, new Object[]{this.val$emsg, Integer.valueOf(this.val$ecode)}));
                        }
                    }
                }
            }

            public void success(String token, String secret) {
                Auth.setData(token, secret, Global.uid, true);
                RestoreActivity.this.restoreDone = true;
                RestoreActivity.this.setResult(-1);
                RestoreActivity.this.finish();
            }

            public void fail(int ecode, String emsg) {
                RestoreActivity.this.runOnUiThread(new C04471(ecode, emsg));
            }
        }).wrapProgress(this).exec();
    }

    private void showError(String err) {
        new Builder(this).setTitle(C0436R.string.error).setMessage(err).setPositiveButton(C0436R.string.ok, null).show();
    }

    public void onBackPressed() {
        if (this.curStep == 0) {
            super.onBackPressed();
        } else {
            setStep(this.curStep - 1);
        }
    }
}
