package com.vkontakte.android;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import com.vkontakte.android.EmojiView.StickerListener;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.data.Stickers;

public class EmojiPopup {
    public static final String ACTION_HIDE_POPUP = "com.vkontakte.andoroid.HIDE_EMOJI_POPUP";
    private int btnRes;
    private View contentView;
    private Context context;
    private PopupWindow emojiPopup;
    private EmojiView emojiView;
    private int keyboardHeight;
    private boolean keyboardVisible;
    private BroadcastReceiver receiver;
    private boolean showStickers;
    private StickerClickListener stickerListener;

    /* renamed from: com.vkontakte.android.EmojiPopup.1 */
    class C02451 extends BroadcastReceiver {
        C02451() {
        }

        public void onReceive(Context context, Intent intent) {
            if (EmojiPopup.ACTION_HIDE_POPUP.equals(intent.getAction())) {
                EmojiPopup.this.hide();
            }
            if (Stickers.ACTION_STICKERS_UPDATED.equals(intent.getAction()) && EmojiPopup.this.showStickers && EmojiPopup.this.emojiView != null) {
                EmojiPopup.this.emojiView.tempFailedPacks.clear();
                EmojiPopup.this.emojiView.updateStickers();
            }
        }
    }

    /* renamed from: com.vkontakte.android.EmojiPopup.2 */
    class C02462 implements OnDismissListener {
        C02462() {
        }

        public void onDismiss() {
            try {
                VKApplication.context.unregisterReceiver(EmojiPopup.this.receiver);
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.EmojiPopup.3 */
    class C02473 implements Runnable {
        C02473() {
        }

        public void run() {
            EmojiPopup.this.contentView.setPadding(0, 0, 0, 0);
        }
    }

    /* renamed from: com.vkontakte.android.EmojiPopup.4 */
    class C02484 implements OnPreDrawListener {
        private final /* synthetic */ int[] val$aloc;
        private final /* synthetic */ View val$anchor;
        private final /* synthetic */ BackgroundDrawable val$bd;
        private final /* synthetic */ int[] val$eloc;

        C02484(int[] iArr, BackgroundDrawable backgroundDrawable, int[] iArr2, View view) {
            this.val$eloc = iArr;
            this.val$bd = backgroundDrawable;
            this.val$aloc = iArr2;
            this.val$anchor = view;
        }

        public boolean onPreDraw() {
            EmojiPopup.this.emojiView.getViewTreeObserver().removeOnPreDrawListener(this);
            EmojiPopup.this.emojiView.getLocationOnScreen(this.val$eloc);
            this.val$bd.setArrowX((this.val$aloc[0] - this.val$eloc[0]) + (this.val$anchor.getWidth() / 2));
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.EmojiPopup.5 */
    class C02495 implements OnDismissListener {
        C02495() {
        }

        public void onDismiss() {
            try {
                VKApplication.context.unregisterReceiver(EmojiPopup.this.receiver);
            } catch (Exception e) {
            }
        }
    }

    private static class BackgroundDrawable extends Drawable {
        private static final int ARROW_SIZE;
        private static final int PADDING;
        private int arrowX;
        private Bitmap bitmap;
        private Paint paint;

        static {
            PADDING = Global.scale(5.0f);
            ARROW_SIZE = Global.scale(7.0f);
        }

        public BackgroundDrawable(int color) {
            this.bitmap = null;
            this.arrowX = 200;
            this.paint = new Paint();
            this.paint.setColor(color);
            this.paint.setShadowLayer((float) Global.scale(4.0f), 0.0f, (float) Global.scale(1.0f), 1426063360);
            this.paint.setAntiAlias(true);
        }

        public void draw(Canvas canvas) {
            Rect rect = copyBounds();
            if (!(this.bitmap != null && rect.width() == this.bitmap.getWidth() && rect.height() == this.bitmap.getHeight())) {
                this.bitmap = Bitmap.createBitmap(rect.width(), rect.height(), Config.ARGB_8888);
                Canvas c = new Canvas(this.bitmap);
                Rect r = new Rect(rect);
                r.offsetTo(0, 0);
                r.inset(PADDING, PADDING);
                r.bottom -= ARROW_SIZE;
                Path path = new Path();
                path.addRect(new RectF(r), Direction.CW);
                path.moveTo((float) (this.arrowX - ARROW_SIZE), (float) r.bottom);
                path.lineTo((float) this.arrowX, (float) (r.bottom + ARROW_SIZE));
                path.lineTo((float) (this.arrowX + ARROW_SIZE), (float) r.bottom);
                path.close();
                c.drawPath(path, this.paint);
            }
            canvas.drawBitmap(this.bitmap, new Rect(0, 0, rect.width(), PADDING), new Rect(rect.left, rect.top, rect.right, rect.top + PADDING), this.paint);
            canvas.drawBitmap(this.bitmap, new Rect(0, (rect.height() - PADDING) - ARROW_SIZE, rect.width(), rect.height()), new Rect(rect.left, (rect.bottom - PADDING) - ARROW_SIZE, rect.right, rect.bottom), this.paint);
            canvas.drawBitmap(this.bitmap, new Rect(0, PADDING, PADDING, (rect.bottom - PADDING) - ARROW_SIZE), new Rect(rect.left, rect.top + PADDING, rect.left + PADDING, (rect.bottom - PADDING) - ARROW_SIZE), this.paint);
            canvas.drawBitmap(this.bitmap, new Rect(rect.width() - PADDING, PADDING, rect.width(), (rect.bottom - PADDING) - ARROW_SIZE), new Rect(rect.right - PADDING, rect.top + PADDING, rect.right, (rect.bottom - PADDING) - ARROW_SIZE), this.paint);
        }

        public void setArrowX(int x) {
            this.arrowX = Global.scale(5.0f) + x;
            this.bitmap = null;
        }

        public int getOpacity() {
            return -3;
        }

        public void setAlpha(int alpha) {
        }

        public void setColorFilter(ColorFilter cf) {
        }

        public boolean getPadding(Rect out) {
            out.set(PADDING, PADDING, PADDING, PADDING + ARROW_SIZE);
            return true;
        }
    }

    public interface StickerClickListener {
        void onStickerSelected(StickerAttachment stickerAttachment);
    }

    /* renamed from: com.vkontakte.android.EmojiPopup.6 */
    class C16186 implements StickerListener {
        C16186() {
        }

        public void onEmojiSelected(String s) {
            EditText edit = (EditText) EmojiPopup.this.contentView.findViewById(C0436R.id.writebar_edit);
            int pos = edit.getSelectionEnd();
            CharSequence em = Global.replaceEmoji(s);
            edit.setText(edit.getText().insert(pos, em));
            pos += em.length();
            edit.setSelection(pos, pos);
        }

        public void onBackspace() {
            ((EditText) EmojiPopup.this.contentView.findViewById(C0436R.id.writebar_edit)).dispatchKeyEvent(new KeyEvent(0, 67));
        }

        public void onStickerSelected(StickerAttachment sticker) {
            if (EmojiPopup.this.stickerListener != null) {
                EmojiPopup.this.stickerListener.onStickerSelected(sticker);
            }
        }
    }

    public EmojiPopup(Context c, View content, int icon, boolean stickers) {
        this.receiver = new C02451();
        this.context = c;
        this.contentView = content;
        this.btnRes = icon;
        this.showStickers = stickers;
    }

    public void onKeyboardStateChanged(boolean visible, int h) {
        if (!Global.isTablet) {
            this.keyboardVisible = visible;
            this.keyboardHeight = h;
            if (this.keyboardHeight > Global.scale(100.0f) && visible) {
                this.context.getSharedPreferences("emoji", 0).edit().putInt("kbd_height" + this.context.getResources().getDisplayMetrics().widthPixels + "_" + this.context.getResources().getDisplayMetrics().heightPixels, this.keyboardHeight).commit();
                Log.m530w("vk", "SAVED: kbd_height" + this.context.getResources().getDisplayMetrics().widthPixels + "_" + this.context.getResources().getDisplayMetrics().heightPixels + " = " + this.keyboardHeight);
            }
            Log.m528i("vk", "ST Keyboard height = " + this.keyboardHeight + ", visible = " + visible);
            if (visible && this.contentView.getPaddingBottom() == 0 && this.emojiPopup != null && this.emojiPopup.isShowing()) {
                this.emojiPopup.setHeight(h);
                this.emojiPopup.dismiss();
                this.emojiPopup.showAtLocation(((Activity) this.context).getWindow().getDecorView(), 83, 0, 0);
            }
            if (visible && this.contentView.getPaddingBottom() > 0) {
                showEmojiPopup(false);
            }
            if (!visible && this.emojiPopup != null && this.emojiPopup.isShowing()) {
                showEmojiPopup(false);
            }
        }
    }

    public void showEmojiPopup(boolean show) {
        Log.m528i("vk", "show emoji popup " + show + ", ime fullscreen=" + ((InputMethodManager) this.context.getSystemService("input_method")).isFullscreenMode());
        IntentFilter filter;
        if (Global.isTablet) {
            if (show) {
                if (this.emojiPopup == null) {
                    createEmojiPopup();
                }
                filter = new IntentFilter();
                filter.addAction(ACTION_HIDE_POPUP);
                if (this.showStickers) {
                    filter.addAction(Stickers.ACTION_STICKERS_UPDATED);
                }
                VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
                this.emojiPopup.setWidth(Global.scale(350.0f));
                this.emojiPopup.setHeight(Global.scale(260.0f));
                BackgroundDrawable bd = new BackgroundDrawable(this.showStickers ? -1 : -1315086);
                this.emojiPopup.setBackgroundDrawable(bd);
                this.emojiPopup.setOutsideTouchable(true);
                View anchor = this.contentView.findViewById(C0436R.id.writebar_attach);
                this.emojiPopup.showAsDropDown(anchor, -((this.emojiPopup.getWidth() / 2) - (anchor.getWidth() / 2)), 0);
                int[] eloc = new int[2];
                int[] aloc = new int[2];
                anchor.getLocationOnScreen(aloc);
                this.emojiView.getViewTreeObserver().addOnPreDrawListener(new C02484(eloc, bd, aloc, anchor));
                this.emojiPopup.setOnDismissListener(new C02495());
            } else {
                this.emojiPopup.dismiss();
            }
        } else if (show) {
            filter = new IntentFilter();
            filter.addAction(ACTION_HIDE_POPUP);
            if (this.showStickers) {
                filter.addAction(Stickers.ACTION_STICKERS_UPDATED);
            }
            VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
            if (this.emojiPopup == null) {
                createEmojiPopup();
            }
            if (this.keyboardHeight <= Global.scale(100.0f)) {
                this.keyboardHeight = this.context.getSharedPreferences("emoji", 0).getInt("kbd_height" + this.context.getResources().getDisplayMetrics().widthPixels + "_" + this.context.getResources().getDisplayMetrics().heightPixels, Global.scale(200.0f));
            }
            Log.m528i("vk", "PP Keyboard height = " + this.keyboardHeight);
            if (this.keyboardHeight < Global.scale(200.0f)) {
                this.keyboardHeight = Global.scale(200.0f);
            }
            if (!this.keyboardVisible && this.keyboardHeight > this.contentView.getHeight() / 2) {
                this.keyboardHeight = this.contentView.getHeight() / 2;
            }
            this.emojiPopup.setHeight(MeasureSpec.makeMeasureSpec(this.keyboardHeight, 1073741824));
            this.emojiPopup.setWidth(MeasureSpec.makeMeasureSpec(this.context.getResources().getDisplayMetrics().widthPixels, 1073741824));
            this.emojiPopup.showAtLocation(((Activity) this.context).getWindow().getDecorView(), 83, 0, 0);
            if (this.keyboardVisible) {
                ((ImageView) this.contentView.findViewById(C0436R.id.writebar_attach)).setImageResource(C0436R.drawable.ic_msg_panel_kb);
            } else {
                this.contentView.setPadding(0, 0, 0, this.keyboardHeight);
                ((ImageView) this.contentView.findViewById(C0436R.id.writebar_attach)).setImageResource(C0436R.drawable.ic_msg_panel_hide);
            }
            this.emojiPopup.setOnDismissListener(new C02462());
        } else {
            ((ImageView) this.contentView.findViewById(C0436R.id.writebar_attach)).setImageResource(this.btnRes);
            this.emojiPopup.dismiss();
            this.contentView.post(new C02473());
        }
        if (show && this.showStickers) {
            this.emojiView.updateStickers();
        }
    }

    public void hide() {
        if (this.emojiPopup != null && this.emojiPopup.isShowing()) {
            try {
                showEmojiPopup(false);
            } catch (Exception e) {
                try {
                    VKApplication.context.unregisterReceiver(this.receiver);
                } catch (Exception e2) {
                }
            }
        }
    }

    public boolean isShowing() {
        return this.emojiPopup != null && this.emojiPopup.isShowing();
    }

    public void loadRecents() {
        if (this.emojiView != null) {
            this.emojiView.loadRecents();
        }
    }

    private void createEmojiPopup() {
        this.emojiView = new EmojiView(this.context, this.showStickers);
        this.emojiView.setListener(new C16186());
        this.emojiPopup = new PopupWindow(this.emojiView);
    }

    public void setStickerClickListener(StickerClickListener l) {
        this.stickerListener = l;
    }
}
