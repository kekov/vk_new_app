package com.vkontakte.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import java.io.DataOutputStream;
import java.io.IOException;
import org.acra.ACRAConstants;

public class RepostAttachment extends Attachment implements ImageAttachment {
    public static final Creator<RepostAttachment> CREATOR;
    public String name;
    public int ownerID;
    public String photo;
    public int postID;
    public int time;
    public int type;

    /* renamed from: com.vkontakte.android.RepostAttachment.1 */
    class C04451 implements Creator<RepostAttachment> {
        C04451() {
        }

        public RepostAttachment createFromParcel(Parcel in) {
            return new RepostAttachment(in);
        }

        public RepostAttachment[] newArray(int size) {
            return new RepostAttachment[size];
        }
    }

    /* renamed from: com.vkontakte.android.RepostAttachment.2 */
    class C04462 implements OnClickListener {
        C04462() {
        }

        public void onClick(View v) {
            v.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("vkontakte://vk.com/wall" + RepostAttachment.this.ownerID + "_" + RepostAttachment.this.postID)));
        }
    }

    public RepostAttachment(int _ownerID, int _postID, int _time, String _name, String _photo, int _type) {
        this.ownerID = _ownerID;
        this.time = _time;
        this.name = _name;
        this.photo = _photo;
        this.postID = _postID;
        this.type = _type;
    }

    public RepostAttachment(Parcel p) {
        this.ownerID = p.readInt();
        this.postID = p.readInt();
        this.time = p.readInt();
        this.type = p.readInt();
        this.name = p.readString();
        this.photo = p.readString();
    }

    static {
        CREATOR = new C04451();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel p, int arg1) {
        p.writeInt(this.ownerID);
        p.writeInt(this.postID);
        p.writeInt(this.time);
        p.writeInt(this.type);
        p.writeString(this.name);
        p.writeString(this.photo);
    }

    public String getImageURL() {
        return this.photo;
    }

    public void setImage(View view, Bitmap img, boolean fromCache) {
        ((ImageView) view.findViewById(C0436R.id.wall_retweet_photo)).setImageBitmap(img);
    }

    public void clearImage(View view) {
        ((ImageView) view.findViewById(C0436R.id.wall_retweet_photo)).setImageResource(this.ownerID < 0 ? C0436R.drawable.group_placeholder : C0436R.drawable.user_placeholder);
    }

    public View getFullView(Context context) {
        return getViewForList(context, null);
    }

    public View getViewForList(Context context, View reuse) {
        View v = Attachment.getReusableView(context, "repost");
        ((TextView) v.findViewById(C0436R.id.wall_retweet_name)).setText(this.name);
        ((TextView) v.findViewById(C0436R.id.wall_retweet_time)).setText(new StringBuilder(String.valueOf(Global.langDate(context.getResources(), this.time))).append(this.type == 0 ? ACRAConstants.DEFAULT_STRING_VALUE : " " + context.getResources().getString(C0436R.string.ntf_to_post)).toString());
        clearImage(v);
        v.setBackgroundDrawable(null);
        v.setOnClickListener(new C04462());
        v.setFocusable(false);
        v.setFocusableInTouchMode(false);
        return v;
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(17);
        os.writeInt(this.ownerID);
        os.writeInt(this.postID);
        os.writeInt(this.time);
        os.writeUTF(this.name);
        os.writeUTF(this.photo);
        os.writeInt(this.type);
    }

    public LayoutParams getViewLayoutParams() {
        LayoutParams lp = new LayoutParams();
        lp.width = -1;
        lp.height = Global.scale(53.0f);
        return lp;
    }
}
