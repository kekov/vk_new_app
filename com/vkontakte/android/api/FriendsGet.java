package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FriendsGet extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<UserProfile> arrayList);
    }

    public FriendsGet(int uid, boolean mutual) {
        super("friends.get");
        String str = "fields";
        StringBuilder append = new StringBuilder(String.valueOf(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec")).append(",online");
        String str2 = (uid == Global.uid || uid == 0) ? ",bdate,first_name_gen,last_name_gen,sex" : ACRAConstants.DEFAULT_STRING_VALUE;
        param(str, append.append(str2).toString());
        if (uid == 0 || uid == Global.uid) {
            param("order", "hints");
        }
        param("user_id", uid);
    }

    private UserProfile parseProfile(JSONObject p) throws JSONException {
        UserProfile profile = new UserProfile();
        profile.firstName = p.getString("first_name");
        profile.lastName = p.getString("last_name");
        profile.fullName = profile.firstName + " " + profile.lastName;
        profile.photo = p.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
        profile.uid = p.getInt("id");
        profile.online = Global.getUserOnlineStatus(p);
        profile.bdate = p.optString("bdate");
        profile.f151f = p.optInt("sex") == 1;
        if (p.has("first_name_gen")) {
            profile.university = p.getString("first_name_gen") + " " + p.getString("last_name_gen");
        }
        return profile;
    }

    public Object parse(JSONObject o) {
        try {
            ArrayList<UserProfile> result = new ArrayList();
            JSONArray a = o.getJSONObject("response").getJSONArray("items");
            if (a == null) {
                return result;
            }
            for (int i = 0; i < a.length(); i++) {
                result.add(parseProfile(a.getJSONObject(i)));
            }
            return result;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
