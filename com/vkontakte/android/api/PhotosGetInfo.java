package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class PhotosGetInfo extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, int i2, int i3, boolean z, boolean z2);
    }

    public PhotosGetInfo(int oid, int pid, String akey) {
        super("photos.getById");
        String str = "photos";
        StringBuilder append = new StringBuilder(String.valueOf(oid)).append("_").append(pid);
        String str2 = (akey == null || akey.length() <= 0) ? ACRAConstants.DEFAULT_STRING_VALUE : "_" + akey;
        param(str, append.append(str2).toString());
        param("extended", 1);
    }

    public Object parse(JSONObject o) {
        boolean z = true;
        try {
            boolean z2;
            JSONObject p = o.getJSONArray("response").getJSONObject(0);
            Object obj = new Object[5];
            obj[0] = Integer.valueOf(p.getJSONObject("likes").getInt("count"));
            obj[1] = Integer.valueOf(p.getJSONObject("comments").getInt("count"));
            obj[2] = Integer.valueOf(p.getJSONObject("tags").getInt("count"));
            if (p.getJSONObject("likes").getInt("user_likes") == 1) {
                z2 = true;
            } else {
                z2 = false;
            }
            obj[3] = Boolean.valueOf(z2);
            if (p.optInt("can_comment", 1) != 1) {
                z = false;
            }
            obj[4] = Boolean.valueOf(z);
            return obj;
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), ((Integer) r[1]).intValue(), ((Integer) r[2]).intValue(), ((Boolean) r[3]).booleanValue(), ((Boolean) r[4]).booleanValue());
        }
    }
}
