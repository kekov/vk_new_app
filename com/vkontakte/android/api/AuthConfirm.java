package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Auth;
import org.json.JSONObject;

public class AuthConfirm extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i);
    }

    public AuthConfirm(String phone, String code, String pass) {
        super("auth.confirm");
        param("phone", phone);
        param("code", code);
        if (pass != null) {
            param("password", pass);
        }
        param(WebDialog.DIALOG_PARAM_CLIENT_ID, (int) Auth.API_ID);
        param("client_secret", Auth.API_SECRET);
        param("intro", 3);
    }

    public Object parse(JSONObject o) {
        try {
            return Integer.valueOf(o.getJSONObject("response").optInt("user_id"));
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success(((Integer) result).intValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
