package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.data.StickerPack;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class StoreGetProducts extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(List<StickerPack> list);
    }

    public StoreGetProducts() {
        super("isAppUser");
    }

    public Object parse(JSONObject o) {
        try {
            Object packs = new ArrayList();
            for (int i = 0; i < 5; i++) {
                StickerPack p = new StickerPack();
                p.id = i + 1;
                p.title = "Pack #" + i;
                p.thumb = "http://vk.com/images/question_a.gif";
                packs.add(p);
            }
            return packs;
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((List) result);
        }
    }
}
