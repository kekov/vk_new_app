package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class C2DMUnregisterDevice extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    /* renamed from: com.vkontakte.android.api.C2DMUnregisterDevice.1 */
    class C14011 extends APIHandler {
        C14011() {
        }

        public void success(JSONObject o) {
            C2DMUnregisterDevice.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (C2DMUnregisterDevice.this.callback != null) {
                C2DMUnregisterDevice.this.callback.fail(ecode, emsg);
            }
        }
    }

    public C2DMUnregisterDevice(String token) {
        super("account.unregisterDevice");
        param("token", token);
        handler(new C14011());
    }

    public Object parse(JSONObject o) {
        try {
            return Boolean.valueOf(true);
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
