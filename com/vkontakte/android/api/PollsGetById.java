package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class PollsGetById extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(String str, int i, PollOption[] pollOptionArr, boolean z);
    }

    public interface ExCallback extends Callback {
        void canceled();
    }

    public PollsGetById(int ownerID, int pollID) {
        super("polls.getById");
        param("owner_id", ownerID).param("poll_id", pollID);
    }

    public Object parse(JSONObject o) {
        boolean z = true;
        try {
            String q = o.getJSONObject("response").getString("question");
            int ua = o.getJSONObject("response").getInt("answer_id");
            JSONArray op = o.getJSONObject("response").getJSONArray("answers");
            PollOption[] opts = new PollOption[op.length()];
            for (int i = 0; i < op.length(); i++) {
                PollOption po = new PollOption();
                po.id = op.getJSONObject(i).getInt("id");
                po.title = op.getJSONObject(i).getString("text");
                po.numVotes = op.getJSONObject(i).getInt("votes");
                po.percent = (float) op.getJSONObject(i).getDouble("rate");
                opts[i] = po;
            }
            Object obj = new Object[4];
            obj[0] = q;
            obj[1] = Integer.valueOf(ua);
            obj[2] = opts;
            if (o.getJSONObject("response").getInt("anonymous") != 0) {
                z = false;
            }
            obj[3] = Boolean.valueOf(z);
            return obj;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((String) r[0], ((Integer) r[1]).intValue(), (PollOption[]) r[2], ((Boolean) r[3]).booleanValue());
        }
    }

    public void cancel() {
        super.cancel();
        if (this.callback != null && (this.callback instanceof ExCallback)) {
            ((ExCallback) this.callback).canceled();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
