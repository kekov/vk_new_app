package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import com.vkontakte.android.LongPollService;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class WallAddComment extends APIRequest {
    private static final String[] methods;
    Callback callback;
    int type;

    public interface Callback {
        void fail(int i, String str);

        void success(int i);
    }

    static {
        String[] strArr = new String[5];
        strArr[0] = "wall.addComment";
        strArr[1] = "photos.createComment";
        strArr[2] = "video.createComment";
        strArr[4] = "board.addComment";
        methods = strArr;
    }

    public WallAddComment(int ownerID, int postID, String text, int replyTo, int type) {
        this(ownerID, postID, text, replyTo, type, ACRAConstants.DEFAULT_STRING_VALUE, null);
    }

    public WallAddComment(int ownerID, int postID, String text, int replyTo, int type, String attachments, String accessKey) {
        super(methods[type]);
        this.type = type;
        if (replyTo == -1) {
            replyTo = 0;
        }
        if (type == 0) {
            param("owner_id", ownerID).param("post_id", postID).param("text", text).param("reply_to_comment", replyTo).param("attachments", attachments);
        }
        if (type == 1) {
            param("owner_id", ownerID).param("photo_id", postID).param(LongPollService.EXTRA_MESSAGE, text).param("reply_to_comment", replyTo).param("attachments", attachments);
        }
        if (type == 2) {
            param("owner_id", ownerID).param("video_id", postID).param(LongPollService.EXTRA_MESSAGE, text).param("reply_to_comment", replyTo).param("attachments", attachments);
        }
        if (type == 4) {
            param("group_id", -ownerID).param("topic_id", postID).param("text", text.replaceAll("\\[id(\\d+)\\|([^\\]]+)\\]", "[post" + replyTo + "|$2]")).param("attachments", attachments);
        }
        if (accessKey != null) {
            param("access_key", accessKey);
        }
    }

    public Object parse(JSONObject o) {
        try {
            if (this.callback != null) {
                if (this.type == 0) {
                    return Integer.valueOf(o.getJSONObject("response").getInt("comment_id"));
                }
                if (this.type == 1) {
                    return Integer.valueOf(o.getInt("response"));
                }
                if (this.type == 2) {
                    return Integer.valueOf(o.getInt("response"));
                }
                if (this.type == 4) {
                    return Integer.valueOf(o.getInt("response"));
                }
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        return null;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success(((Integer) result).intValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
