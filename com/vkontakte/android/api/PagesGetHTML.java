package com.vkontakte.android.api;

import android.os.Bundle;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import java.util.Locale;
import org.json.JSONObject;

public class PagesGetHTML extends APIRequest {
    Callback callback;
    private boolean needMeta;

    public interface Callback {
        void fail(int i, String str);

        void success(String str, String str2, Bundle bundle);
    }

    public PagesGetHTML(int oid, int pid, boolean isSite) {
        int i = 1;
        super("execute");
        this.needMeta = false;
        this.needMeta = isSite;
        String str = "code";
        Locale locale = Locale.US;
        String str2 = "var p=API.pages.get({owner_id:%1$d,page_id:%2$d,need_html:1,site_preview:%3$d}); p=p+{source: \"\"}; if(%3$d==1){if(p.group_id>0){var g=API.groups.getById({group_id: p.group_id})[0]; p=p+{group_photo: g.photo_100, group_name: g.name};}else{var g=API.users.get({user_ids: -p.group_id, fields:\"photo_100,photo_50,first_name_gen,last_name_gen\"})[0]; p=p+{group_photo: g.photo_100, group_name: g.first_name+\" \"+g.last_name, name_gen: g.first_name_gen+\" \"+g.last_name_gen};}} return p;";
        Object[] objArr = new Object[3];
        objArr[0] = Integer.valueOf(oid);
        objArr[1] = Integer.valueOf(pid);
        if (!isSite) {
            i = 0;
        }
        objArr[2] = Integer.valueOf(i);
        param(str, String.format(locale, str2, objArr));
    }

    public PagesGetHTML(String title) {
        super("execute");
        this.needMeta = false;
        param("code", String.format(Locale.US, "var p=API.pages.get({title:\"%s\",global:1,need_html:1}); p=p+{source: \"\"}; return p;", new Object[]{title.replace("\"", "\\\"")}));
    }

    public PagesGetHTML(int oid, String title) {
        super("execute");
        this.needMeta = false;
        param("code", String.format(Locale.US, "var p=API.pages.get({title:\"%1$s\",owner_id:%2$d,need_html:1}); p=p+{source: \"\"}; return p;", new Object[]{title, Integer.valueOf(oid)}));
    }

    public Object parse(JSONObject o) {
        try {
            if (o.has("execute_errors")) {
                JSONObject je = o.getJSONArray("execute_errors").getJSONObject(0);
                return new ErrorResponse(je.getInt("error_code"), je.getString("error_msg"));
            }
            Bundle meta = new Bundle();
            JSONObject r = o.getJSONObject("response");
            if (this.needMeta) {
                meta.putString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, r.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
                meta.putString(PlusShare.KEY_CALL_TO_ACTION_URL, r.getString(PlusShare.KEY_CALL_TO_ACTION_URL));
                meta.putInt("date", r.getInt("created"));
                meta.putInt("views", r.getInt("views"));
                meta.putInt("group_id", r.getInt("group_id"));
                meta.putString("group_name", r.getString("group_name"));
                meta.putString("group_photo", r.getString("group_photo"));
                if (r.has("name_gen")) {
                    meta.putString("name_gen", r.getString("name_gen"));
                }
            }
            return new Object[]{r.getString("html"), r.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE), meta};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((String) r[0], (String) r[1], (Bundle) r[2]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
