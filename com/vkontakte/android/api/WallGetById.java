package com.vkontakte.android.api;

import android.text.TextUtils;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.NewsEntry;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

public class WallGetById extends APIRequest {
    Callback callback;
    public String gphoto;
    public String gtitle;

    public interface Callback {
        void fail(int i, String str);

        void success(NewsEntry[] newsEntryArr);
    }

    public WallGetById(String[] idsArray) {
        super("wall.getById");
        param("posts", TextUtils.join(",", idsArray));
        param("extended", 1).param("fields", "photo_rec,photo_medium_rec");
        param("photo_sizes", 1);
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray items = o.getJSONObject("response").optJSONArray("items");
            JSONArray profiles = o.getJSONObject("response").optJSONArray("profiles");
            JSONArray groups = o.getJSONObject("response").optJSONArray("groups");
            if (items == null) {
                return new NewsEntry[0];
            }
            int i;
            int uid;
            HashMap<Integer, String> names = new HashMap();
            HashMap<Integer, String> photos = new HashMap();
            if (profiles != null) {
                for (i = 0; i < profiles.length(); i++) {
                    Log.m528i("vk", profiles.getJSONObject(i));
                    uid = profiles.getJSONObject(i).getInt("id");
                    names.put(Integer.valueOf(uid), new StringBuilder(String.valueOf(profiles.getJSONObject(i).getString("first_name"))).append(" ").append(profiles.getJSONObject(i).getString("last_name")).toString());
                    photos.put(Integer.valueOf(uid), profiles.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50"));
                }
            }
            if (groups != null) {
                for (i = 0; i < groups.length(); i++) {
                    uid = -groups.getJSONObject(i).getInt("id");
                    names.put(Integer.valueOf(uid), groups.getJSONObject(i).getString("name"));
                    photos.put(Integer.valueOf(uid), groups.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50"));
                }
            }
            return doParse(names, photos, items);
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    private Object doParse(HashMap<Integer, String> names, HashMap<Integer, String> photos, JSONArray items) throws Exception {
        NewsEntry[] result = new NewsEntry[items.length()];
        for (int i = 0; i < items.length(); i++) {
            result[i] = new NewsEntry(items.getJSONObject(i), names, photos);
        }
        return result;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((NewsEntry[]) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
