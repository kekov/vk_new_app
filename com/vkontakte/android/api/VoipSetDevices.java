package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class VoipSetDevices extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    /* renamed from: com.vkontakte.android.api.VoipSetDevices.1 */
    class C14101 extends APIHandler {
        C14101() {
        }

        public void success(JSONObject o) {
            VoipSetDevices.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (VoipSetDevices.this.callback != null) {
                VoipSetDevices.this.callback.fail(ecode, emsg);
            }
        }
    }

    public VoipSetDevices(int callID, int fromID, boolean camera) {
        super("voip.setDevices");
        param("camera", camera ? 1 : 0);
        param("call_id", callID);
        param("from_id", fromID);
        handler(new C14101());
    }

    public Object parse(JSONObject o) {
        return Boolean.valueOf(true);
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
