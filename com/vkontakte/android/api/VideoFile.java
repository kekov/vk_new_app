package com.vkontakte.android.api;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class VideoFile implements Parcelable {
    public static final Creator<VideoFile> CREATOR;
    public String accessKey;
    public int comments;
    public int date;
    public String descr;
    public int duration;
    public boolean liked;
    public int likes;
    public int oid;
    public String ownerName;
    public String ownerPhoto;
    public boolean repeat;
    public String title;
    public String url240;
    public String url360;
    public String url480;
    public String url720;
    public String urlBigThumb;
    public String urlEmbed;
    public String urlExternal;
    public String urlThumb;
    public int vid;
    public int views;

    /* renamed from: com.vkontakte.android.api.VideoFile.1 */
    class C05521 implements Creator<VideoFile> {
        C05521() {
        }

        public VideoFile createFromParcel(Parcel in) {
            return new VideoFile(in);
        }

        public VideoFile[] newArray(int size) {
            return new VideoFile[size];
        }
    }

    public VideoFile(Parcel p) {
        boolean z;
        boolean z2 = true;
        this.oid = p.readInt();
        this.vid = p.readInt();
        this.duration = p.readInt();
        this.url240 = p.readString();
        this.url360 = p.readString();
        this.url480 = p.readString();
        this.url720 = p.readString();
        this.urlExternal = p.readString();
        this.urlEmbed = p.readString();
        this.urlThumb = p.readString();
        this.urlBigThumb = p.readString();
        this.title = p.readString();
        this.descr = p.readString();
        this.date = p.readInt();
        this.views = p.readInt();
        this.ownerName = p.readString();
        this.ownerPhoto = p.readString();
        this.accessKey = p.readString();
        this.likes = p.readInt();
        this.comments = p.readInt();
        if (p.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.liked = z;
        if (p.readInt() != 1) {
            z2 = false;
        }
        this.repeat = z2;
    }

    public VideoFile(JSONObject jv) {
        boolean z = true;
        try {
            this.vid = jv.optInt("video_id", jv.optInt("id", jv.optInt("vid")));
            this.oid = jv.getInt("owner_id");
            this.title = jv.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            this.descr = jv.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION);
            this.duration = jv.getInt("duration");
            this.urlThumb = jv.optString("photo_130", jv.optString("thumb", jv.optString("image")));
            this.urlBigThumb = jv.optString("photo_640", jv.optString("photo_320", jv.optString("image_medium")));
            this.date = Global.timeDiff + jv.getInt("date");
            this.views = jv.optInt("views");
            JSONObject files = jv.optJSONObject("files");
            if (files != null) {
                if (files.has("flv_320")) {
                    this.url240 = files.getString("flv_320");
                }
                if (files.has("mp4_240")) {
                    this.url240 = files.getString("mp4_240");
                }
                if (files.has("mp4_360")) {
                    this.url360 = files.getString("mp4_360");
                }
                if (files.has("mp4_480")) {
                    this.url480 = files.getString("mp4_480");
                }
                if (files.has("mp4_720")) {
                    this.url720 = files.getString("mp4_720");
                }
                if (files.has("external")) {
                    this.urlExternal = files.getString("external");
                }
            } else {
                this.urlExternal = jv.optString("player");
            }
            if (this.urlExternal != null && this.urlExternal.endsWith(".mp4")) {
                this.url240 = this.urlExternal;
                this.urlExternal = null;
            }
            this.urlEmbed = jv.optString("player");
            if (jv.has("likes")) {
                boolean z2;
                this.likes = jv.getJSONObject("likes").getInt("count");
                if (jv.getJSONObject("likes").getInt("user_likes") == 1) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                this.liked = z2;
            }
            this.comments = jv.optInt("comments");
            if (jv.optInt("repeat") != 1) {
                z = false;
            }
            this.repeat = z;
            this.accessKey = jv.optString("access_key");
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel p, int flags) {
        int i;
        int i2 = 1;
        p.writeInt(this.oid);
        p.writeInt(this.vid);
        p.writeInt(this.duration);
        p.writeString(this.url240);
        p.writeString(this.url360);
        p.writeString(this.url480);
        p.writeString(this.url720);
        p.writeString(this.urlExternal);
        p.writeString(this.urlEmbed);
        p.writeString(this.urlThumb);
        p.writeString(this.urlBigThumb);
        p.writeString(this.title);
        p.writeString(this.descr);
        p.writeInt(this.date);
        p.writeInt(this.views);
        p.writeString(this.ownerName);
        p.writeString(this.ownerPhoto);
        p.writeString(this.accessKey);
        p.writeInt(this.likes);
        p.writeInt(this.comments);
        if (this.liked) {
            i = 1;
        } else {
            i = 0;
        }
        p.writeInt(i);
        if (!this.repeat) {
            i2 = 0;
        }
        p.writeInt(i2);
    }

    public void writeToStream(DataOutputStream p) throws IOException {
        int i;
        int i2 = 1;
        p.writeInt(this.oid);
        p.writeInt(this.vid);
        p.writeInt(this.duration);
        p.writeUTF(this.url240 == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.url240);
        p.writeUTF(this.url360 == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.url360);
        p.writeUTF(this.url480 == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.url480);
        p.writeUTF(this.url720 == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.url720);
        p.writeUTF(this.urlExternal == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.urlExternal);
        p.writeUTF(this.urlEmbed == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.urlEmbed);
        p.writeUTF(this.urlThumb);
        p.writeUTF(this.urlBigThumb == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.urlBigThumb);
        p.writeUTF(this.title);
        p.writeUTF(this.descr);
        p.writeInt(this.date);
        p.writeInt(this.views);
        p.writeUTF(this.accessKey == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.accessKey);
        p.writeInt(this.likes);
        p.writeInt(this.comments);
        if (this.liked) {
            i = 1;
        } else {
            i = 0;
        }
        p.writeInt(i);
        if (!this.repeat) {
            i2 = 0;
        }
        p.writeInt(i2);
    }

    public void readFromStream(DataInputStream p) throws IOException {
        boolean z;
        boolean z2 = true;
        this.oid = p.readInt();
        this.vid = p.readInt();
        this.duration = p.readInt();
        this.url240 = p.readUTF();
        this.url360 = p.readUTF();
        this.url480 = p.readUTF();
        this.url720 = p.readUTF();
        this.urlExternal = p.readUTF();
        this.urlEmbed = p.readUTF();
        this.urlThumb = p.readUTF();
        this.urlBigThumb = p.readUTF();
        this.title = p.readUTF();
        this.descr = p.readUTF();
        this.date = p.readInt();
        this.views = p.readInt();
        if (p.available() > 0) {
            this.accessKey = p.readUTF();
        }
        if (ACRAConstants.DEFAULT_STRING_VALUE.equals(this.url240)) {
            this.url240 = null;
        }
        if (ACRAConstants.DEFAULT_STRING_VALUE.equals(this.url360)) {
            this.url360 = null;
        }
        if (ACRAConstants.DEFAULT_STRING_VALUE.equals(this.url480)) {
            this.url480 = null;
        }
        if (ACRAConstants.DEFAULT_STRING_VALUE.equals(this.url720)) {
            this.url720 = null;
        }
        if (ACRAConstants.DEFAULT_STRING_VALUE.equals(this.urlExternal)) {
            this.urlExternal = null;
        }
        if (ACRAConstants.DEFAULT_STRING_VALUE.equals(this.urlEmbed)) {
            this.urlEmbed = null;
        }
        if (ACRAConstants.DEFAULT_STRING_VALUE.equals(this.urlBigThumb)) {
            this.urlBigThumb = null;
        }
        if (ACRAConstants.DEFAULT_STRING_VALUE.equals(this.accessKey)) {
            this.accessKey = null;
        }
        this.likes = p.readInt();
        this.comments = p.readInt();
        if (p.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.liked = z;
        if (p.readInt() != 1) {
            z2 = false;
        }
        this.repeat = z2;
    }

    public static VideoFile createFromStream(DataInputStream p) throws IOException {
        VideoFile v = new VideoFile();
        v.readFromStream(p);
        return v;
    }

    static {
        CREATOR = new C05521();
    }
}
