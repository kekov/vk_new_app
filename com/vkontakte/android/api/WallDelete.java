package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class WallDelete extends APIRequest {
    private static final String[] methods;
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    static {
        methods = new String[]{"wall.delete", "photos.delete", "video.delete"};
    }

    public WallDelete(int ownerID, int postID, int type) {
        super(methods[type]);
        if (type == 0) {
            param("owner_id", ownerID).param("post_id", postID);
        }
        if (type == 1) {
            param("owner_id", ownerID).param("photo_id", postID);
        }
        if (type == 2) {
            param("owner_id", ownerID).param("video_id", postID);
        }
    }

    public Object parse(JSONObject o) {
        return Boolean.valueOf(true);
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
