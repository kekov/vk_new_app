package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import com.vkontakte.android.Message;
import java.util.HashMap;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class MessagesGetHistory extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, Vector<Message> vector);
    }

    public MessagesGetHistory(int uid, int offset, int count) {
        super("messages.getHistory");
        param("user_id", uid).param("offset", offset).param("count", count).param("photo_sizes", 1);
        param("fields", "first_name,last_name,photo_100,photo_50");
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray a = APIUtils.unwrapArray(o, "response").array;
            if (a != null || this.callback == null) {
                Vector<Message> msgs = new Vector();
                for (int i = 0; i < a.length(); i++) {
                    msgs.add(0, new Message(a.getJSONObject(i), new HashMap(), new HashMap()));
                }
                return new Object[]{Integer.valueOf(APIUtils.unwrapArray(o, "response").count), msgs};
            }
            return new Object[]{Integer.valueOf(0), new Vector()};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (Vector) r[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
