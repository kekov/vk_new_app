package com.vkontakte.android.api;

import android.os.Build;
import android.text.TextUtils;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.GeoAttachment;
import com.vkontakte.android.Global;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.StickerAttachment;
import com.vkontakte.android.VKApplication;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;

public class MessagesSend extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i);
    }

    public MessagesSend(int uid, String msg, ArrayList<?> attachments, ArrayList<Integer> fwdMessages, GeoAttachment location, int tmpId) {
        super("messages.send");
        param("device", Build.BRAND + ":" + Build.MANUFACTURER + ":" + Build.MODEL + ":" + Build.PRODUCT);
        if (uid < 2000000000) {
            param("user_id", uid);
        } else {
            param("chat_id", uid - 2000000000);
        }
        if (msg != null && msg.length() > 0) {
            param(LongPollService.EXTRA_MESSAGE, msg);
        }
        if (location != null) {
            param("lat", new StringBuilder(String.valueOf(location.lat)).toString()).param("long", new StringBuilder(String.valueOf(location.lon)).toString());
        }
        if (attachments != null && attachments.size() > 0) {
            Iterator it = attachments.iterator();
            while (it.hasNext()) {
                Object att = it.next();
                if (att instanceof StickerAttachment) {
                    param("sticker_id", ((StickerAttachment) att).id);
                    param("method", "messages.sendSticker");
                    attachments.remove(att);
                    break;
                }
            }
        }
        if (attachments != null && attachments.size() > 0) {
            param("attachment", TextUtils.join(",", attachments));
        }
        if (fwdMessages != null && fwdMessages.size() > 0) {
            param("forward_messages", TextUtils.join(",", fwdMessages));
        }
        tmpId ^= (int) (VKApplication.deviceID & -1);
        try {
            tmpId ^= Integer.parseInt(Global.accessToken.substring(0, 8), 16);
        } catch (Exception e) {
        }
        param("guid", tmpId);
    }

    public Object parse(JSONObject o) {
        try {
            return Integer.valueOf(o.getInt("response"));
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success(((Integer) result).intValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
