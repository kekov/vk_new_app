package com.vkontakte.android.api;

import android.text.TextUtils;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.AudioFile;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class AudioGetById extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<AudioFile> arrayList);
    }

    public AudioGetById(List<String> audios) {
        super("audio.getById");
        param("audios", TextUtils.join(",", audios));
    }

    public Object parse(JSONObject o) {
        try {
            ArrayList<AudioFile> files = new ArrayList();
            JSONArray a = o.optJSONArray("response");
            if (a == null) {
                return files;
            }
            for (int i = 0; i < a.length(); i++) {
                files.add(new AudioFile(a.getJSONObject(i)));
            }
            return files;
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
