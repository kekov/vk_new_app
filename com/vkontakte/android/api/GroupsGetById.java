package com.vkontakte.android.api;

import android.text.TextUtils;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class GroupsGetById extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(Group[] groupArr);
    }

    public GroupsGetById(int[] gids) {
        super("groups.getById");
        ArrayList<Integer> ids = new ArrayList();
        for (int g : gids) {
            ids.add(Integer.valueOf(g));
        }
        param("group_ids", TextUtils.join(",", ids));
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray a = o.getJSONArray("response");
            Object result = new Group[a.length()];
            for (int i = 0; i < a.length(); i++) {
                result[i] = new Group(a.getJSONObject(i));
            }
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((Group[]) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
