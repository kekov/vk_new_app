package com.vkontakte.android.api;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.NewsfeedList;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class NewsfeedGetLists extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<NewsfeedList> arrayList);
    }

    public NewsfeedGetLists() {
        super("newsfeed.getLists");
    }

    public Object parse(JSONObject o) {
        ArrayList<NewsfeedList> result = new ArrayList();
        try {
            JSONArray ja = APIUtils.unwrapArray(o, "response").array;
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jl = ja.getJSONObject(i);
                result.add(new NewsfeedList(jl.getInt("id"), jl.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)));
            }
        } catch (Exception e) {
        }
        return result;
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }
}
