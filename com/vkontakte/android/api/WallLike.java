package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class WallLike extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, int i2, int i3);
    }

    public WallLike(boolean add, int oid, int pid, boolean pub, int type, int parentType, String akey) {
        String str = type == 0 ? add ? "wall.addLike" : "wall.deleteLike" : add ? "likes.add" : "likes.delete";
        super(str);
        if (type == 0) {
            param("owner_id", oid).param("post_id", pid);
            if (add && pub) {
                param("need_publish", 1);
            }
        }
        if (type == 1) {
            param(WebDialog.DIALOG_PARAM_TYPE, "photo").param("owner_id", oid).param("item_id", pid);
            if (akey != null && akey.length() > 0) {
                param("access_key", akey);
            }
        }
        if (type == 2) {
            param(WebDialog.DIALOG_PARAM_TYPE, "video").param("owner_id", oid).param("item_id", pid);
            if (akey != null && akey.length() > 0) {
                param("access_key", akey);
            }
        }
        if (type == 5) {
            String t = ACRAConstants.DEFAULT_STRING_VALUE;
            switch (parentType) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    t = "photo_";
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    t = "video_";
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    t = "topic_";
                    break;
            }
            param(WebDialog.DIALOG_PARAM_TYPE, new StringBuilder(String.valueOf(t)).append("comment").toString()).param("owner_id", oid).param("item_id", pid);
        }
        if (type == 4) {
            param(WebDialog.DIALOG_PARAM_TYPE, "topic").param("owner_id", oid).param("item_id", pid);
        }
    }

    public Object parse(JSONObject o) {
        try {
            int likes = o.getJSONObject("response").getInt("likes");
            return new int[]{likes, o.getJSONObject("response").optInt("reposts", -1), o.getJSONObject("response").optInt("reposted_post_id", -1)};
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            int[] r = (int[]) result;
            this.callback.success(r[0], r[1], r[2]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
