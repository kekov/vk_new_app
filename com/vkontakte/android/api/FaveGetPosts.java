package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.NewsEntry;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

public class FaveGetPosts extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<NewsEntry> arrayList, int i);
    }

    public FaveGetPosts(int offset, int count) {
        super("fave.getPosts");
        param("photo_sizes", 1);
        param("offset", offset).param("count", count);
        param("extended", 1);
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray items = o.getJSONObject("response").optJSONArray("items");
            JSONArray profiles1 = o.getJSONObject("response").optJSONArray("profiles");
            JSONArray profiles2 = o.getJSONObject("response").optJSONArray("groups");
            ArrayList<NewsEntry> result = new ArrayList();
            if (items == null) {
                return new Object[]{result, Integer.valueOf(0)};
            }
            int i;
            int uid;
            HashMap<Integer, String> names = new HashMap();
            HashMap<Integer, String> photos = new HashMap();
            HashMap<Integer, Boolean> f = new HashMap();
            if (profiles1 != null) {
                for (i = 0; i < profiles1.length(); i++) {
                    uid = profiles1.getJSONObject(i).getInt("id");
                    names.put(Integer.valueOf(uid), new StringBuilder(String.valueOf(profiles1.getJSONObject(i).getString("first_name"))).append(" ").append(profiles1.getJSONObject(i).getString("last_name")).toString());
                    photos.put(Integer.valueOf(uid), profiles1.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50"));
                    f.put(Integer.valueOf(uid), Boolean.valueOf(profiles1.getJSONObject(i).getInt("sex") == 1));
                }
            }
            if (profiles2 != null) {
                for (i = 0; i < profiles2.length(); i++) {
                    uid = -profiles2.getJSONObject(i).getInt("id");
                    if (!names.containsKey(Integer.valueOf(uid))) {
                        names.put(Integer.valueOf(uid), profiles2.getJSONObject(i).getString("name"));
                        photos.put(Integer.valueOf(uid), profiles2.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50"));
                    }
                }
            }
            for (i = 0; i < items.length(); i++) {
                result.add(new NewsEntry(items.getJSONObject(i), names, photos, f));
            }
            return new Object[]{result, Integer.valueOf(o.getJSONObject("response").getInt("count"))};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((ArrayList) r[0], ((Integer) r[1]).intValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
