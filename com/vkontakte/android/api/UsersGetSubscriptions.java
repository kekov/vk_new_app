package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import org.json.JSONObject;

public class UsersGetSubscriptions extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<UserProfile> arrayList, int i);
    }

    public UsersGetSubscriptions(int uid, int offset, int count) {
        super("users.getSubscriptions");
        param("user_id", uid).param("offset", offset).param("count", count);
        param("extended", 1);
        param("fields", "photo_100,photo_50,online");
    }

    public Object parse(JSONObject o) {
        try {
            JSONArrayWithCount aa = APIUtils.unwrapArray(o, "response");
            ArrayList<UserProfile> result = new ArrayList();
            for (int i = 0; i < aa.array.length(); i++) {
                JSONObject ju = aa.array.getJSONObject(i);
                UserProfile u = new UserProfile();
                if (ju.has(WebDialog.DIALOG_PARAM_TYPE) && ("page".equals(ju.getString(WebDialog.DIALOG_PARAM_TYPE)) || "group".equals(ju.getString(WebDialog.DIALOG_PARAM_TYPE)) || "event".equals(ju.getString(WebDialog.DIALOG_PARAM_TYPE)))) {
                    u.uid = -ju.getInt("id");
                    u.fullName = ju.getString("name");
                } else {
                    u.uid = ju.getInt("id");
                    u.firstName = ju.getString("first_name");
                    u.lastName = ju.getString("last_name");
                    u.fullName = u.firstName + " " + u.lastName;
                    u.online = Global.getUserOnlineStatus(ju);
                }
                u.photo = ju.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                result.add(u);
            }
            return new Object[]{result, Integer.valueOf(aa.count)};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((ArrayList) r[0], ((Integer) r[1]).intValue());
        }
    }
}
