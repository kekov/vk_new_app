package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.AudioFile;
import com.vkontakte.android.Log;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class AudioSearch extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<AudioFile> arrayList);
    }

    public AudioSearch(String q) {
        super("audio.search");
        param("q", q);
        param("count", 200);
    }

    public Object parse(JSONObject o) {
        try {
            Object result = new ArrayList();
            JSONArray list = APIUtils.unwrapArray(o, "response").array;
            for (int i = 0; i < list.length(); i++) {
                result.add(new AudioFile(list.getJSONObject(i)));
            }
            return result;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }
}
