package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Auth;
import org.json.JSONObject;

public class AuthCheckPhone extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    /* renamed from: com.vkontakte.android.api.AuthCheckPhone.1 */
    class C13941 extends APIHandler {
        C13941() {
        }

        public void success(JSONObject o) {
            AuthCheckPhone.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (AuthCheckPhone.this.callback != null) {
                AuthCheckPhone.this.callback.fail(ecode, emsg);
            }
        }
    }

    public AuthCheckPhone(String phone) {
        super("auth.checkPhone");
        param("phone", phone);
        param(WebDialog.DIALOG_PARAM_CLIENT_ID, (int) Auth.API_ID);
        param("client_secret", Auth.API_SECRET);
        handler(new C13941());
    }

    public Object parse(JSONObject o) {
        try {
            return Boolean.valueOf(true);
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
