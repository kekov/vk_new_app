package com.vkontakte.android.api;

import android.os.Bundle;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import org.json.JSONObject;

public class AccountGetProfileInfo extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(Bundle bundle);
    }

    public AccountGetProfileInfo() {
        super("account.getProfileInfo");
        param("fields", "photo_50");
    }

    public Object parse(JSONObject o) {
        int status = 0;
        try {
            JSONObject jc;
            JSONObject r = o.getJSONObject("response");
            Bundle res = new Bundle();
            res.putString("first_name", r.getString("first_name"));
            res.putString("last_name", r.getString("last_name"));
            res.putInt("gender", r.getInt("sex"));
            res.putInt("relation", r.getInt("relation"));
            if (r.has("relation_partner")) {
                JSONObject ju = r.getJSONObject("relation_partner");
                UserProfile user = new UserProfile();
                user.uid = ju.getInt("id");
                user.firstName = ju.getString("first_name");
                user.lastName = ju.getString("last_name");
                user.fullName = user.firstName + " " + user.lastName;
                user.photo = ju.getString("photo_50");
                res.putParcelable("relation_partner", user);
            }
            try {
                String[] bd = r.getString("bdate").split("\\.");
                res.putInt("bday", Integer.parseInt(bd[0]));
                res.putInt("bmonth", Integer.parseInt(bd[1]));
                res.putInt("byear", Math.max(Integer.parseInt(bd[2]), 1905));
            } catch (NumberFormatException e) {
            }
            res.putInt("bdate_vis", r.getInt("bdate_visibility"));
            if (r.has("country")) {
                jc = r.getJSONObject("country");
                res.putInt("country_id", jc.getInt("id"));
                res.putString("country_name", jc.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
            }
            if (r.has("city")) {
                jc = r.getJSONObject("city");
                res.putInt("city_id", jc.getInt("id"));
                res.putString("city_name", jc.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
            }
            if (!r.has("name_request")) {
                return res;
            }
            JSONObject nr = r.getJSONObject("name_request");
            if (nr.getString("status").equals("processing")) {
                status = 1;
            }
            res.putInt("name_req_status", status);
            if (status == 1) {
                res.putInt("name_req_id", nr.getInt("id"));
            }
            res.putString("name_req_name", nr.getString("first_name") + " " + nr.getString("last_name"));
            return res;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((Bundle) result);
        }
    }
}
