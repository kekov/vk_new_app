package com.vkontakte.android.api;

import android.text.TextUtils;
import com.facebook.WebDialog;
import com.google.android.gms.games.GamesClient;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.AudioFile;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.Photo;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.ExtendedUserProfile.Relative;
import com.vkontakte.android.api.ExtendedUserProfile.School;
import com.vkontakte.android.api.ExtendedUserProfile.University;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetFullProfile extends APIRequest {
    Callback callback;
    int uid;

    public interface Callback {
        void fail(int i, String str);

        void success(ExtendedUserProfile extendedUserProfile, ArrayList<Photo> arrayList);
    }

    public GetFullProfile(int uid, int photoCount) {
        super(uid > 0 ? "execute.getFullProfileNew" : "execute.getFullGroupNew");
        if (uid > 0) {
            param("user_id", uid);
        } else {
            param("group_id", -uid);
        }
        this.uid = uid;
        param("photo_count", photoCount);
    }

    public Object parse(JSONObject o) {
        try {
            o = o.optJSONObject("response");
            if (o == null) {
                return new Object[2];
            }
            int i;
            ExtendedUserProfile ep = new ExtendedUserProfile();
            ep.profile = new UserProfile();
            ep.screenName = o.optString("screen_name", "id" + this.uid);
            JSONObject jp;
            if (this.uid > 0) {
                ep.profile.uid = o.getInt("id");
                ep.profile.firstName = o.getString("first_name");
                ep.profile.lastName = o.getString("last_name");
                ep.profile.fullName = new StringBuilder(String.valueOf(ep.profile.firstName)).append(" ").append(ep.profile.lastName).toString();
                ep.profile.photo = o.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                ep.profile.isFriend = o.getInt("friend_status") == 3;
                ep.bigPhoto = o.optString("photo_max", o.getString("photo_medium_rec"));
                ep.activity = Global.replaceEmoji(o.optString("activity", ACRAConstants.DEFAULT_STRING_VALUE));
                ep.profile.f151f = o.getInt("sex") == 1;
                ep.profile.online = Global.getUserOnlineStatus(o);
                HashMap<Integer, String> cities = new HashMap();
                JSONArray jcities = o.getJSONArray("cities");
                for (i = 0; i < jcities.length(); i++) {
                    JSONObject city = jcities.getJSONObject(i);
                    cities.put(Integer.valueOf(city.getInt("id")), city.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
                }
                HashMap<Integer, String> countries = new HashMap();
                JSONArray jcountries = o.getJSONArray("cities");
                for (i = 0; i < jcountries.length(); i++) {
                    JSONObject country = jcountries.getJSONObject(i);
                    countries.put(Integer.valueOf(country.getInt("id")), country.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
                }
                ep.canWrite = o.getInt("can_write_private_message") == 1;
                ep.canPost = o.getInt("can_post") == 1;
                ep.canSeeAllPosts = o.optInt("can_see_all_posts") == 1;
                ep.showAllPosts = "all".equals(o.optString("wall_default"));
                ep.canCall = o.optInt("can_call") == 1;
                ep.blacklisted = o.optInt("blacklisted_by_me") == 1;
                ep.relation = o.optInt("relation");
                if (o.has("relation_partner")) {
                    ep.relationPartner = o.getJSONObject("relation_partner").getInt("id");
                    ep.relationPartnerName = new StringBuilder(String.valueOf(o.getJSONObject("relation_partner").getString("first_name"))).append(" ").append(o.getJSONObject("relation_partner").getString("last_name")).toString();
                }
                if (o.has("bdate")) {
                    String[] bd = o.getString("bdate").split("\\.");
                    ep.bDay = Integer.parseInt(bd[0]);
                    ep.bMonth = Integer.parseInt(bd[1]);
                    int length = bd.length;
                    if (r0 > 2) {
                        ep.bYear = Integer.parseInt(bd[2]);
                    }
                }
                ep.firstNameDat = o.optString("first_name_dat", ep.profile.firstName);
                ep.firstNameGen = o.optString("first_name_gen", ep.profile.firstName);
                ep.firstNameIns = o.optString("first_name_ins", ep.profile.firstName);
                ep.firstNameAcc = o.optString("first_name_acc", ep.profile.firstName);
                ep.lastNameDat = o.optString("last_name_dat", ep.profile.lastName);
                ep.lastNameGen = o.optString("last_name_gen", ep.profile.lastName);
                ep.lastNameIns = o.optString("last_name_ins", ep.profile.lastName);
                ep.lastNameAcc = o.optString("last_name_acc", ep.profile.lastName);
                if (o.has("city")) {
                    if (o.has("country")) {
                        ep.city = o.getJSONObject("city").getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                        ep.country = o.getJSONObject("country").getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                    }
                }
                if (o.has("mobile_phone")) {
                    if (o.getString("mobile_phone").length() > 0) {
                        ep.mobilePhone = o.getString("mobile_phone");
                    }
                }
                if (o.has("home_phone")) {
                    if (o.getString("home_phone").length() > 0) {
                        ep.homePhone = o.getString("home_phone");
                    }
                }
                if (o.has("skype")) {
                    ep.skype = o.getString("skype");
                }
                if (o.has("twitter")) {
                    ep.twitter = o.getString("twitter");
                }
                if (o.has("livejournal")) {
                    ep.livejournal = o.getString("livejournal");
                }
                if (o.has("facebook")) {
                    if (o.optLong("facebook", -1) != -1) {
                        ep.facebookId = o.getLong("facebook");
                        ep.facebookName = o.getString("facebook_name");
                    }
                }
                if (o.has("instagram")) {
                    ep.instagram = o.getString("instagram");
                }
                ep.friendStatus = o.getInt("friend_status");
                JSONArray schools = o.optJSONArray("schools");
                ep.schools = new ArrayList();
                if (schools != null) {
                    for (i = 0; i < schools.length(); i++) {
                        School s = new School();
                        JSONObject js = schools.getJSONObject(i);
                        s.city = (String) cities.get(Integer.valueOf(js.optInt("city")));
                        s.name = js.optString("name", "???");
                        s.from = js.optInt("year_from");
                        s.to = js.optInt("year_to");
                        s.graduation = js.optInt("year_graduated");
                        s.className = js.optString("class", null);
                        s.speciality = js.optString("speciality", null);
                        s.type = js.optString("type_str", VKApplication.context.getResources().getString(C0436R.string.profile_school));
                        ep.schools.add(s);
                    }
                }
                ep.universities = new ArrayList();
                JSONArray univers = o.optJSONArray("universities");
                if (univers != null) {
                    for (i = 0; i < univers.length(); i++) {
                        University u = new University();
                        JSONObject ju = univers.getJSONObject(i);
                        u.city = (String) cities.get(Integer.valueOf(ju.getInt("city")));
                        u.name = ju.getString("name").trim();
                        if (ju.has("faculty_name")) {
                            u.faculty = ju.getString("faculty_name").trim();
                        }
                        if (ju.has("chair_name")) {
                            u.chair = ju.getString("chair_name").trim();
                        }
                        u.graduation = ju.optInt("graduation");
                        ep.universities.add(u);
                    }
                }
                if (o.has("interests")) {
                    if (o.getString("interests").length() > 0) {
                        ep.interests = o.getString("interests");
                    }
                }
                if (o.has("movies")) {
                    if (o.getString("movies").length() > 0) {
                        ep.movies = o.getString("movies");
                    }
                }
                if (o.has("music")) {
                    if (o.getString("music").length() > 0) {
                        ep.music = o.getString("music");
                    }
                }
                if (o.has("tv")) {
                    if (o.getString("tv").length() > 0) {
                        ep.tv = o.getString("tv");
                    }
                }
                if (o.has("books")) {
                    if (o.getString("books").length() > 0) {
                        ep.books = o.getString("books");
                    }
                }
                if (o.has("games")) {
                    if (o.getString("games").length() > 0) {
                        ep.games = o.getString("games");
                    }
                }
                if (o.has("about")) {
                    if (o.getString("about").length() > 0) {
                        ep.about = o.getString("about");
                    }
                }
                if (o.has("quotes")) {
                    if (o.getString("quotes").length() > 0) {
                        ep.quotations = o.getString("quotes");
                    }
                }
                if (o.has("activities")) {
                    if (o.getString("activities").length() > 0) {
                        ep.activities = o.getString("activities");
                    }
                }
                if (o.has("home_town")) {
                    if (o.getString("home_town").length() > 0) {
                        ep.hometown = o.getString("home_town");
                    }
                }
                if (o.has("site")) {
                    if (o.getString("site").length() > 0) {
                        ep.website = o.getString("site");
                    }
                }
                ep.verified = o.optInt("verified") == 1;
                JSONObject personal = o.optJSONObject("personal");
                if (personal != null) {
                    JSONArray langs = personal.optJSONArray("langs");
                    if (langs != null) {
                        ArrayList<String> sl = new ArrayList();
                        for (i = 0; i < langs.length(); i++) {
                            sl.add(langs.getString(i));
                        }
                        ep.langs = TextUtils.join(", ", sl);
                    }
                    ep.political = personal.optInt("political");
                    ep.religion = personal.optString("religion");
                    ep.lifeMain = personal.optInt("life_main");
                    ep.peopleMain = personal.optInt("people_main");
                    ep.inspiredBy = personal.optString("inspired_by");
                    ep.smoking = personal.optInt("smoking");
                    ep.alcohol = personal.optInt("alcohol");
                }
                HashMap<Integer, UserProfile> relatives = new HashMap();
                JSONArray jrelprofiles = o.optJSONArray("relatives_profiles");
                if (jrelprofiles != null) {
                    for (i = 0; i < jrelprofiles.length(); i++) {
                        jp = jrelprofiles.getJSONObject(i);
                        UserProfile p = new UserProfile();
                        p.uid = jp.getInt("id");
                        p.firstName = jp.getString("first_name");
                        p.lastName = jp.getString("last_name");
                        p.photo = jp.optString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50", ACRAConstants.DEFAULT_STRING_VALUE);
                        p.f151f = jp.getInt("sex") == 1;
                        relatives.put(Integer.valueOf(p.uid), p);
                    }
                }
                ep.relatives = new ArrayList();
                JSONArray rels = o.optJSONArray("relatives");
                if (rels != null) {
                    for (i = 0; i < rels.length(); i++) {
                        JSONObject jr = rels.getJSONObject(i);
                        int uid = jr.getInt("id");
                        if (relatives.containsKey(Integer.valueOf(uid))) {
                            String type = jr.getString(WebDialog.DIALOG_PARAM_TYPE);
                            Relative rel = new Relative();
                            rel.user = (UserProfile) relatives.get(Integer.valueOf(uid));
                            if ("grandchild".equals(type)) {
                                rel.type = 4;
                            } else {
                                if ("grandparent".equals(type)) {
                                    rel.type = 3;
                                } else {
                                    if ("child".equals(type)) {
                                        rel.type = 2;
                                    } else {
                                        if ("sibling".equals(type)) {
                                            rel.type = 1;
                                        } else {
                                            if ("parent".equals(type)) {
                                                rel.type = 0;
                                            }
                                        }
                                    }
                                }
                            }
                            ep.relatives.add(rel);
                        }
                    }
                }
                if (o.has("last_seen")) {
                    ep.lastSeen = o.getJSONObject("last_seen").getInt("time");
                    int platform = o.getJSONObject("last_seen").optInt("platform");
                    boolean z = platform == 1 || platform == 2 || platform == 3 || platform == 4 || platform == 5;
                    ep.lastSeenMobile = z;
                }
                if (o.has("deactivated")) {
                    String dt = o.getString("deactivated");
                    if ("banned".equals(dt)) {
                        ep.lastSeen = -1;
                    } else {
                        if ("deleted".equals(dt)) {
                            ep.lastSeen = -2;
                        }
                    }
                }
            } else {
                ep.profile.uid = this.uid;
                ep.profile.fullName = o.getString("name");
                ep.profile.photo = o.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                ep.bigPhoto = o.getString("photo_100");
                ep.infoLine = o.optString("activity");
                ep.activity = o.getJSONObject("status").optString("text");
                ep.about = o.optString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION);
                ep.eventStartTime = o.optInt("start_date");
                ep.eventEndTime = o.optInt("end_date");
                ep.website = o.optString("site");
                ep.verified = o.optInt("verified") == 1;
                if (!o.isNull("country_name")) {
                    if (!o.isNull("city_name")) {
                        ArrayList<String> ss = new ArrayList();
                        if (o.has("country_name")) {
                            ss.add(o.getString("country_name"));
                        }
                        if (o.has("city_name")) {
                            ss.add(0, o.getString("city_name"));
                        }
                        if (o.has("place")) {
                            jp = o.getJSONObject("place");
                            if (jp.has("address")) {
                                ss.add(0, jp.getString("address"));
                            }
                            ep.lat = jp.optDouble("latitude", -9000.0d);
                            ep.lon = jp.optDouble("longitude", -9000.0d);
                        } else {
                            ep.lon = -9000.0d;
                            ep.lat = -9000.0d;
                        }
                        ep.city = TextUtils.join(", ", ss);
                    }
                }
                ep.friendStatus = o.optInt("is_member");
                ep.groupAccess = o.getInt("is_closed");
                ep.canSeeAllPosts = o.optInt("can_see_all_posts") == 1;
                if (o.getJSONObject("membership").optInt(GamesClient.EXTRA_INVITATION) > 0) {
                    ep.friendStatus = 2;
                }
                if (o.getJSONObject("membership").optInt("request") > 0) {
                    ep.friendStatus = 3;
                }
                if ("group".equals(o.getString(WebDialog.DIALOG_PARAM_TYPE))) {
                    ep.groupType = 0;
                }
                if ("event".equals(o.getString(WebDialog.DIALOG_PARAM_TYPE))) {
                    ep.groupType = 1;
                }
                if ("page".equals(o.getString(WebDialog.DIALOG_PARAM_TYPE))) {
                    ep.groupType = 2;
                }
                ep.canPost = o.optInt("can_post") == 1;
                if (o.has("wiki_page")) {
                    ep.mobilePhone = o.getString("wiki_page");
                }
            }
            ep.counters = new HashMap();
            JSONObject counters = o.optJSONObject("counters");
            if (counters != null) {
                Iterator<String> keys = counters.keys();
                while (keys.hasNext()) {
                    String k = (String) keys.next();
                    ep.counters.put(k, Integer.valueOf(counters.getInt(k)));
                }
            }
            if (this.uid < 0) {
                ep.counters.put("members", Integer.valueOf(o.optInt("members_count")));
            }
            ArrayList<Photo> photos = new ArrayList();
            if (o.has("photos")) {
                if (o.optJSONObject("photos") != null) {
                    JSONArray jphotos = APIUtils.unwrapArray(o, "photos").array;
                    if (jphotos != null) {
                        for (i = 0; i < jphotos.length(); i++) {
                            photos.add(new Photo(jphotos.getJSONObject(i)));
                        }
                    }
                }
            }
            if (o.getJSONObject("status").has("audio")) {
                AudioFile file = new AudioFile(o.getJSONObject("status").getJSONObject("audio"));
                ep.audioStatus = file;
                ep.activity = file.artist + " - " + file.title;
            }
            return new Object[]{ep, photos};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] res = (Object[]) result;
            this.callback.success((ExtendedUserProfile) res[0], (ArrayList) res[1]);
        }
    }
}
