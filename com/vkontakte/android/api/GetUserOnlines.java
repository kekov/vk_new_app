package com.vkontakte.android.api;

import android.text.TextUtils;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetUserOnlines extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(HashMap<Integer, Integer> hashMap);
    }

    public GetUserOnlines(List<Integer> uids) {
        super("execute.getUserOnlines");
        param("user_ids", TextUtils.join(",", uids));
    }

    public Object parse(JSONObject o) {
        try {
            Object result = new HashMap();
            JSONArray a = o.getJSONArray("response");
            for (int i = 0; i < a.length(); i++) {
                JSONObject oo = a.getJSONObject(i);
                result.put(Integer.valueOf(oo.getInt("id")), Integer.valueOf(Global.getUserOnlineStatus(oo)));
            }
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((HashMap) result);
        }
    }
}
