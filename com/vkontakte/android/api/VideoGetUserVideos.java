package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import java.util.HashMap;
import java.util.Locale;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class VideoGetUserVideos extends APIRequest {
    private static final String CODE = "var v=API.video.getUserVideos({user_id:%d,offset:%d,count:%d});return {v:v,u:API.getProfiles({user_ids:v@.owner_id,fields:\"%s\"})};";
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, Vector<VideoFile> vector);
    }

    public VideoGetUserVideos(int uid, int offset, int count) {
        super("execute");
        String str = "code";
        Locale locale = Locale.US;
        String str2 = CODE;
        Object[] objArr = new Object[4];
        objArr[0] = Integer.valueOf(uid);
        objArr[1] = Integer.valueOf(offset);
        objArr[2] = Integer.valueOf(count);
        objArr[3] = Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec";
        param(str, String.format(locale, str2, objArr));
    }

    public Object parse(JSONObject o) {
        try {
            int i;
            Vector<VideoFile> vf = new Vector();
            int total = 0;
            JSONArray arr = APIUtils.unwrapArray(o.getJSONObject("response"), "v").array;
            JSONArray u = o.getJSONObject("response").optJSONArray("u");
            HashMap<Integer, String> names = new HashMap();
            HashMap<Integer, String> photos = new HashMap();
            if (u != null) {
                for (i = 0; i < u.length(); i++) {
                    int uid = u.getJSONObject(i).getInt("id");
                    names.put(Integer.valueOf(uid), new StringBuilder(String.valueOf(u.getJSONObject(i).getString("first_name"))).append(" ").append(u.getJSONObject(i).getString("last_name")).toString());
                    photos.put(Integer.valueOf(uid), u.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec"));
                }
            }
            if (arr != null) {
                total = APIUtils.unwrapArray(o.getJSONObject("response"), "v").count;
                for (i = 0; i < arr.length(); i++) {
                    vf.add(new VideoFile(arr.getJSONObject(i)));
                }
            }
            return new Object[]{Integer.valueOf(total), vf};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (Vector) r[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
