package com.vkontakte.android.api;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class BoardAddTopic extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i);
    }

    /* renamed from: com.vkontakte.android.api.BoardAddTopic.1 */
    class C13951 extends APIHandler {
        C13951() {
        }

        public void success(JSONObject o) {
            BoardAddTopic.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (BoardAddTopic.this.callback != null) {
                BoardAddTopic.this.callback.fail(ecode, emsg);
            }
        }
    }

    public BoardAddTopic(int gid, String title, String message) {
        super("board.addTopic");
        param("group_id", gid).param(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, title).param("text", message);
        handler(new C13951());
    }

    public Object parse(JSONObject o) {
        try {
            return Integer.valueOf(o.getInt("response"));
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success(((Integer) result).intValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
