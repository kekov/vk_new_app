package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class NewsfeedGetBanned extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<UserProfile> arrayList, ArrayList<UserProfile> arrayList2);
    }

    public NewsfeedGetBanned() {
        super("newsfeed.getBanned");
        param("extended", 1).param("fields", "photo_rec,photo_medium_rec");
    }

    public Object parse(JSONObject o) {
        try {
            int i;
            JSONObject u;
            UserProfile p;
            ArrayList<UserProfile> users = new ArrayList();
            ArrayList<UserProfile> groups = new ArrayList();
            JSONArray ju = o.getJSONObject("response").optJSONArray("profiles");
            JSONArray jg = o.getJSONObject("response").optJSONArray("groups");
            if (ju != null) {
                for (i = 0; i < ju.length(); i++) {
                    u = ju.getJSONObject(i);
                    p = new UserProfile();
                    p.uid = u.getInt("id");
                    p.fullName = u.getString("first_name") + " " + u.getString("last_name");
                    p.photo = u.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                    users.add(p);
                }
            }
            if (jg != null) {
                for (i = 0; i < jg.length(); i++) {
                    u = jg.getJSONObject(i);
                    p = new UserProfile();
                    p.uid = -u.getInt("id");
                    p.fullName = u.getString("name");
                    p.photo = u.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                    groups.add(p);
                }
            }
            return new Object[]{users, groups};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((ArrayList) r[0], (ArrayList) r[1]);
        }
    }
}
