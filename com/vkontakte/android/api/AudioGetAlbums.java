package com.vkontakte.android.api;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.AudioPlaylist;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class AudioGetAlbums extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<AudioPlaylist> arrayList);
    }

    public AudioGetAlbums(int oid) {
        super("audio.getAlbums");
        param("owner_id", oid);
        param("count", 100);
    }

    public Object parse(JSONObject o) {
        try {
            Object result = new ArrayList();
            JSONArray a = APIUtils.unwrapArray(o, "response").array;
            for (int i = 0; i < a.length(); i++) {
                JSONObject j = a.getJSONObject(i);
                result.add(new AudioPlaylist(j.getInt("id"), j.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)));
            }
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }
}
