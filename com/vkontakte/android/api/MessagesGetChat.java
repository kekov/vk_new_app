package com.vkontakte.android.api;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.ChatUser;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Messages;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class MessagesGetChat extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<ChatUser> arrayList, String str, String str2, int i);
    }

    public MessagesGetChat(int chatID) {
        super("messages.getChat");
        param("chat_id", chatID).param("fields", "photo_rec,photo_medium_rec,sex");
    }

    public Object parse(JSONObject o) {
        try {
            int i;
            JSONObject jp;
            int userID;
            int invID;
            JSONObject r = o.getJSONObject("response");
            JSONArray arr = r.getJSONArray("users");
            ArrayList<ChatUser> profiles = new ArrayList();
            HashMap<Integer, UserProfile> users = new HashMap();
            ArrayList<Integer> missingUsers = new ArrayList();
            for (i = 0; i < arr.length(); i++) {
                jp = arr.getJSONObject(i);
                UserProfile p = new UserProfile();
                p.uid = jp.getInt("id");
                p.firstName = jp.getString("first_name");
                p.lastName = jp.getString("last_name");
                p.fullName = p.firstName + " " + p.lastName;
                p.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                p.f151f = jp.optInt("sex") == 1;
                users.put(Integer.valueOf(p.uid), p);
            }
            for (i = 0; i < arr.length(); i++) {
                jp = arr.getJSONObject(i);
                userID = jp.getInt("id");
                invID = jp.getInt("invited_by");
                if (!users.containsKey(Integer.valueOf(invID))) {
                    if (!missingUsers.contains(Integer.valueOf(invID))) {
                        missingUsers.add(Integer.valueOf(invID));
                    }
                }
            }
            if (missingUsers.size() > 0) {
                Iterator it = Friends.getUsersBlocking(missingUsers).iterator();
                while (it.hasNext()) {
                    UserProfile u = (UserProfile) it.next();
                    users.put(Integer.valueOf(u.uid), u);
                }
            }
            for (i = 0; i < arr.length(); i++) {
                jp = arr.getJSONObject(i);
                userID = jp.getInt("id");
                invID = jp.getInt("invited_by");
                ChatUser cu = new ChatUser();
                cu.user = (UserProfile) users.get(Integer.valueOf(userID));
                cu.inviter = (UserProfile) users.get(Integer.valueOf(invID));
                profiles.add(cu);
            }
            ArrayList<ChatUser>[] arrayListArr = new Object[4];
            arrayListArr[0] = profiles;
            arrayListArr[1] = r.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            arrayListArr[2] = r.optString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50", Messages.createChatPhoto(profiles));
            arrayListArr[3] = Integer.valueOf(r.getInt("admin_id"));
            return arrayListArr;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((ArrayList) r[0], (String) r[1], (String) r[2], ((Integer) r[3]).intValue());
        }
    }
}
