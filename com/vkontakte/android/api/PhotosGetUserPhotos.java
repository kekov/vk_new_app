package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import com.vkontakte.android.Photo;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class PhotosGetUserPhotos extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, Vector<Photo> vector);
    }

    public PhotosGetUserPhotos(int oid, int offset, int count) {
        super("photos.getUserPhotos");
        param("user_id", oid).param("offset", offset).param("count", count).param("extended", 1).param("sort", "desc").param("photo_sizes", 1);
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray arr = APIUtils.unwrapArray(o, "response").array;
            Vector<Photo> photos = new Vector();
            for (int i = 0; i < arr.length(); i++) {
                photos.add(new Photo(arr.getJSONObject(i)));
            }
            return new Object[]{Integer.valueOf(APIUtils.unwrapArray(o, "response").count), photos};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (Vector) r[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
