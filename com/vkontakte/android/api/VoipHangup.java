package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import org.json.JSONObject;

public class VoipHangup extends APIRequest {
    public static int REASON_BUSY;
    public static int REASON_NETWORK_ERROR;
    public static int REASON_TIMEOUT;
    public static int REASON_USER_NOT_REPLIED;
    public static int REASON_USER_REPLIED;
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    static {
        REASON_USER_REPLIED = 0;
        REASON_USER_NOT_REPLIED = 3;
        REASON_BUSY = 1;
        REASON_NETWORK_ERROR = -1;
        REASON_TIMEOUT = 2;
    }

    public VoipHangup(int callID, int fromID, int reason) {
        super("voip.hangup");
        param("call_id", callID);
        param("from_id", fromID);
        param("reason", new StringBuilder(String.valueOf(reason)).toString());
        Log.m525d("vk", "r " + reason);
    }

    public Object parse(JSONObject o) {
        try {
            return Boolean.valueOf(true);
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }
}
