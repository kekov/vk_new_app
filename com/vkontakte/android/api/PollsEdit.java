package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class PollsEdit extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    public PollsEdit(int oid, int pid, String question, List<String> add, List<Integer> remove, Map<String, String> edit) {
        super("polls.edit");
        param("owner_id", oid);
        param("poll_id", pid);
        if (question != null) {
            param("question", question);
        }
        if (add != null && add.size() > 0) {
            param("add_answers", new JSONArray(add).toString());
        }
        if (remove != null && remove.size() > 0) {
            param("delete_answers", new JSONArray(remove).toString());
        }
        if (edit != null && edit.size() > 0) {
            param("edit_answers", new JSONObject(edit).toString());
        }
    }

    public Object parse(JSONObject o) {
        return Boolean.valueOf(true);
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
