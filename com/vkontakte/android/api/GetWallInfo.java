package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class GetWallInfo extends APIRequest {
    Callback callback;
    private int id;

    public interface Callback {
        void fail(int i, String str);

        void success(String str, String str2, boolean z, boolean z2, int i, int i2, int i3, boolean z3);
    }

    public GetWallInfo(int id) {
        super("execute.getUserInfo");
        this.id = id;
    }

    public Object parse(JSONObject o) {
        boolean z = true;
        try {
            o = o.getJSONObject("response");
            JSONObject jp = o.getJSONObject("profile");
            int intro = o.getJSONObject("info").getInt("intro");
            JSONObject jx = jp.optJSONObject("exports");
            int country = 0;
            if (jp.has("country")) {
                country = jp.getJSONObject("country").getInt("id");
            }
            Object obj = new Object[8];
            obj[0] = jp.getString("first_name") + " " + jp.getString("last_name");
            obj[1] = jp.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
            boolean z2 = jx != null && jx.optInt("twitter") == 1;
            obj[2] = Boolean.valueOf(z2);
            z2 = jx != null && jx.optInt("facebook") == 1;
            obj[3] = Boolean.valueOf(z2);
            obj[4] = Integer.valueOf(o.getInt("time"));
            obj[5] = Integer.valueOf(intro);
            obj[6] = Integer.valueOf(country);
            if (o.optInt("has_new_items") != 1) {
                z = false;
            }
            obj[7] = Boolean.valueOf(z);
            return obj;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            try {
                Object[] r = (Object[]) result;
                this.callback.success((String) r[0], (String) r[1], ((Boolean) r[2]).booleanValue(), ((Boolean) r[3]).booleanValue(), ((Integer) r[4]).intValue(), ((Integer) r[5]).intValue(), ((Integer) r[6]).intValue(), ((Boolean) r[7]).booleanValue());
            } catch (Throwable x) {
                Log.m532w("vk", x);
                this.callback.fail(-1, ACRAConstants.DEFAULT_STRING_VALUE);
            }
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
