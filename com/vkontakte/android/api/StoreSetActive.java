package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class StoreSetActive extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    public StoreSetActive(int item, boolean active) {
        super(active ? "store.activateProduct" : "store.deactivateProduct");
        param(WebDialog.DIALOG_PARAM_TYPE, "stickers");
        param("product_id", item);
    }

    public Object parse(JSONObject o) {
        try {
            return o.getInt("response") == 1 ? Boolean.valueOf(true) : Boolean.valueOf(false);
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }
}
