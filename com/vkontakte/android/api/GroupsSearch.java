package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import java.util.List;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class GroupsSearch extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(List<Group> list, int i);
    }

    public GroupsSearch(String q, int offset, int count) {
        super("groups.search");
        param("q", q).param("offset", offset).param("count", count);
        param("fields", "start_date");
    }

    public Object parse(JSONObject o) {
        try {
            Vector<Group> result = new Vector();
            JSONArray a = APIUtils.unwrapArray(o, "response").array;
            if (a == null) {
                return new Object[]{result, Integer.valueOf(0)};
            }
            for (int i = 0; i < a.length(); i++) {
                result.add(new Group(a.getJSONObject(i)));
            }
            return new Object[]{result, Integer.valueOf(APIUtils.unwrapArray(o, "response").count)};
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((Vector) r[0], ((Integer) r[1]).intValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
