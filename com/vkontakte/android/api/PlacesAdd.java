package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class PlacesAdd extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i);
    }

    public PlacesAdd(String title, String address, double lat, double lon) {
        super("places.add");
        param(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, title);
        param("address", address);
        param("latitude", new StringBuilder(String.valueOf(lat)).toString());
        param("longitude", new StringBuilder(String.valueOf(lon)).toString());
        param(WebDialog.DIALOG_PARAM_TYPE, 21);
    }

    public Object parse(JSONObject o) {
        try {
            return Integer.valueOf(o.getJSONObject("response").getInt("id"));
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success(((Integer) result).intValue());
        }
    }
}
