package com.vkontakte.android.api;

import android.util.Log;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.Photo;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetFullPhotoList extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<Photo> arrayList);
    }

    /* renamed from: com.vkontakte.android.api.GetFullPhotoList.1 */
    class C14041 extends APIHandler {
        C14041() {
        }

        public void success(JSONObject o) {
            GetFullPhotoList.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (GetFullPhotoList.this.callback != null) {
                GetFullPhotoList.this.callback.fail(ecode, emsg);
            }
        }
    }

    public GetFullPhotoList(NewsEntry e) {
        super("photos.get");
        param("feed", e.time);
        String str = "feed_type";
        String str2 = e.type == 7 ? "photo_tag" : e.type == 9 ? "wall_photo" : "photo";
        param(str, str2);
        param(e.userID > 0 ? "user_id" : "group_id", Math.abs(e.userID));
        param("extended", 1);
        param("photo_sizes", 1);
        handler(new C14041());
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray r = APIUtils.unwrapArray(o, "response").array;
            Object photos = new ArrayList();
            for (int i = 0; i < r.length(); i++) {
                photos.add(new Photo(r.getJSONObject(i)));
            }
            return photos;
        } catch (Exception x) {
            Log.w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
