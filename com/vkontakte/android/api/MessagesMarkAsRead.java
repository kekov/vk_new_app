package com.vkontakte.android.api;

import android.text.TextUtils;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import java.util.List;
import org.json.JSONObject;

public class MessagesMarkAsRead extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    public MessagesMarkAsRead(List<Integer> mids) {
        super("messages.markAsRead");
        param("message_ids", TextUtils.join(",", mids));
    }

    public Object parse(JSONObject o) {
        return Boolean.valueOf(true);
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
