package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.data.StickerPack;
import com.vkontakte.android.fragments.SuggestionsFriendsFragment;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class StoreGetPurchases extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(List<StickerPack> list);
    }

    public StoreGetPurchases() {
        super("store.getProducts");
        param(WebDialog.DIALOG_PARAM_TYPE, "stickers");
        param("extended", 1);
        param("filters", "purchased,promoted");
    }

    public Object parse(JSONObject o) {
        try {
            Object res = new ArrayList();
            JSONArray arr = APIUtils.unwrapArray(o, "response").array;
            for (int i = 0; i < arr.length(); i++) {
                StickerPack pack = new StickerPack(arr.getJSONObject(i));
                pack.thumb = pack.baseURL + "thumb_" + (Global.displayDensity > 1.0f ? SuggestionsFriendsFragment.GPLUS_ERROR_RESULT : 51) + ".png";
                res.add(pack);
            }
            return res;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((List) result);
        }
    }
}
