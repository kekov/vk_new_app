package com.vkontakte.android.api;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Log;
import com.vkontakte.android.VKApplication;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class PhotoAlbum implements Parcelable {
    public static final Creator<PhotoAlbum> CREATOR;
    public static final int PRIVACY_ALL = 0;
    public static final int PRIVACY_EXTENDED = 4;
    public static final int PRIVACY_FRIENDS = 1;
    public static final int PRIVACY_ME = 3;
    public static final int PRIVACY_UNKNOWN = -1;
    public static final int PRIVACY_XFRIENDS = 2;
    public boolean canUpload;
    public int created;
    public String descr;
    public int id;
    public int numPhotos;
    public int oid;
    public int privacy;
    public String thumbURL;
    public String title;
    public int updated;

    /* renamed from: com.vkontakte.android.api.PhotoAlbum.1 */
    class C05511 implements Creator<PhotoAlbum> {
        C05511() {
        }

        public PhotoAlbum createFromParcel(Parcel in) {
            return new PhotoAlbum(in);
        }

        public PhotoAlbum[] newArray(int size) {
            return new PhotoAlbum[size];
        }
    }

    public PhotoAlbum(JSONObject ja) {
        try {
            this.created = ja.optInt("created");
            this.updated = ja.optInt("updated");
            this.title = ja.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            if (this.title.length() == 0) {
                this.title = VKApplication.context.getResources().getString(C0436R.string.album_unnamed);
            }
            this.descr = ja.optString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, ACRAConstants.DEFAULT_STRING_VALUE);
            this.id = ja.getInt("id");
            this.oid = ja.getInt("owner_id");
            this.numPhotos = ja.getInt("size");
            JSONObject jpriv = ja.optJSONObject("privacy_view");
            if (jpriv != null) {
                if (jpriv.has("lists") || jpriv.has("except_lists") || jpriv.has("users") || jpriv.has("except_users")) {
                    this.privacy = PRIVACY_EXTENDED;
                } else {
                    String type = jpriv.getString(WebDialog.DIALOG_PARAM_TYPE);
                    if ("all".equals(type)) {
                        this.privacy = PRIVACY_ALL;
                    } else if ("nobody".equals(type)) {
                        this.privacy = PRIVACY_ME;
                    } else if ("friends".equals(type)) {
                        this.privacy = PRIVACY_FRIENDS;
                    } else if ("friends_of_friends".equals(type)) {
                        this.privacy = PRIVACY_XFRIENDS;
                    } else {
                        this.privacy = PRIVACY_UNKNOWN;
                    }
                }
            }
            this.thumbURL = ja.optString("thumb_src");
        } catch (Exception x) {
            Log.m531w("vk", "Error parsing photo album", x);
        }
    }

    public PhotoAlbum(Parcel p) {
        boolean z = true;
        this.id = p.readInt();
        this.oid = p.readInt();
        this.updated = p.readInt();
        this.created = p.readInt();
        this.numPhotos = p.readInt();
        this.title = p.readString();
        this.descr = p.readString();
        this.privacy = p.readInt();
        this.thumbURL = p.readString();
        if (p.readInt() != PRIVACY_FRIENDS) {
            z = false;
        }
        this.canUpload = z;
    }

    static {
        CREATOR = new C05511();
    }

    public int describeContents() {
        return PRIVACY_ALL;
    }

    public void writeToParcel(Parcel p, int arg1) {
        p.writeInt(this.id);
        p.writeInt(this.oid);
        p.writeInt(this.updated);
        p.writeInt(this.created);
        p.writeInt(this.numPhotos);
        p.writeString(this.title);
        p.writeString(this.descr);
        p.writeInt(this.privacy);
        p.writeString(this.thumbURL);
        p.writeInt(this.canUpload ? PRIVACY_FRIENDS : PRIVACY_ALL);
    }
}
