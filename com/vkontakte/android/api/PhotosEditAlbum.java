package com.vkontakte.android.api;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class PhotosEditAlbum extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    public PhotosEditAlbum(int aid, String title, String descr, int privacy, int gid) {
        super("photos.editAlbum");
        param("album_id", aid).param(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, title).param("descr", descr);
        if (privacy != 4) {
            param("privacy", privacy);
        }
        if (gid > 0) {
            param("group_id", gid);
        }
    }

    public Object parse(JSONObject o) {
        return Boolean.valueOf(true);
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
