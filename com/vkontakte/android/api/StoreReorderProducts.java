package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class StoreReorderProducts extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    public StoreReorderProducts(int id, int before, int after) {
        super("store.reorderProducts");
        param("product_id", id);
        param(WebDialog.DIALOG_PARAM_TYPE, "stickers");
        if (before != -1) {
            param("before", before);
        }
        if (after != -1) {
            param("after", after);
        }
    }

    public Object parse(JSONObject o) {
        try {
            return o.getInt("response") == 1 ? Boolean.valueOf(true) : Boolean.valueOf(false);
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }
}
