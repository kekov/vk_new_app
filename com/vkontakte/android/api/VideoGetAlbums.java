package com.vkontakte.android.api;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import java.util.List;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class VideoGetAlbums extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(List<VideoAlbum> list);
    }

    public VideoGetAlbums(int oid) {
        super("video.getAlbums");
        param("owner_id", oid).param("count", 100);
    }

    public Object parse(JSONObject o) {
        try {
            Vector<VideoAlbum> albums = new Vector();
            JSONArray a = APIUtils.unwrapArray(o, "response").array;
            if (a == null) {
                return albums;
            }
            for (int i = 0; i < a.length(); i++) {
                JSONObject va = a.getJSONObject(i);
                VideoAlbum v = new VideoAlbum();
                v.id = va.getInt("id");
                v.title = va.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                albums.add(v);
            }
            return albums;
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((Vector) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
