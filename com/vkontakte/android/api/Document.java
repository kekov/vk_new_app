package com.vkontakte.android.api;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Log;
import com.vkontakte.android.VKApplication;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class Document implements Parcelable {
    public static final Creator<Document> CREATOR;
    public int did;
    public String ext;
    public int oid;
    public int size;
    public String thumb;
    public String title;
    public String url;

    /* renamed from: com.vkontakte.android.api.Document.1 */
    class C05501 implements Creator<Document> {
        C05501() {
        }

        public Document createFromParcel(Parcel in) {
            return new Document(in);
        }

        public Document[] newArray(int size) {
            return new Document[size];
        }
    }

    public Document(Parcel s) {
        this.did = s.readInt();
        this.oid = s.readInt();
        this.size = s.readInt();
        this.url = s.readString();
        this.title = s.readString();
        this.ext = s.readString();
        this.thumb = s.readString();
    }

    public Document(JSONObject j) {
        try {
            this.did = j.optInt("id", j.optInt("did"));
            this.oid = j.getInt("owner_id");
            this.title = j.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            this.size = j.getInt("size");
            this.ext = j.getString("ext");
            this.url = j.getString(PlusShare.KEY_CALL_TO_ACTION_URL);
            this.thumb = j.optString("photo_130", j.optString("thumb", ACRAConstants.DEFAULT_STRING_VALUE));
            if (j.has("gift")) {
                this.thumb = this.url;
                this.url = null;
                this.title = VKApplication.context.getResources().getString(C0436R.string.gift);
            }
        } catch (Exception x) {
            Log.m531w("vk", "Error parsing doc", x);
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.did);
        dest.writeInt(this.oid);
        dest.writeInt(this.size);
        dest.writeString(this.url);
        dest.writeString(this.title);
        dest.writeString(this.ext);
        dest.writeString(this.thumb);
    }

    static {
        CREATOR = new C05501();
    }
}
