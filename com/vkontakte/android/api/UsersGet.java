package com.vkontakte.android.api;

import android.text.TextUtils;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class UsersGet extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<UserProfile> arrayList);
    }

    public UsersGet(int[] uids, String[] fields) {
        super("execute.getUsersAndGroups");
        ArrayList<Integer> users = new ArrayList();
        ArrayList<Integer> groups = new ArrayList();
        for (int uid : uids) {
            if (uid > 0 || uid < -2000000000) {
                users.add(Integer.valueOf(uid));
            } else {
                groups.add(Integer.valueOf(-uid));
            }
        }
        param("fields", TextUtils.join(",", fields)).param("user_ids", TextUtils.join(",", users));
        param("group_ids", TextUtils.join(",", groups));
    }

    public UsersGet(List<Integer> uids, String[] fields) {
        super("execute.getUsersAndGroups");
        ArrayList<Integer> users = new ArrayList();
        ArrayList<Integer> groups = new ArrayList();
        for (Integer intValue : uids) {
            int uid = intValue.intValue();
            if (uid > 0 || uid < -2000000000) {
                users.add(Integer.valueOf(uid));
            } else {
                groups.add(Integer.valueOf(-uid));
            }
        }
        param("fields", TextUtils.join(",", fields)).param("user_ids", TextUtils.join(",", users));
        param("group_ids", TextUtils.join(",", groups));
    }

    public UsersGet(List<Integer> uids, int nameCase) {
        String[] strArr = new String[3];
        strArr[0] = Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec";
        strArr[1] = "is_friend";
        strArr[2] = "sex";
        this((List) uids, strArr);
        switch (nameCase) {
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                param("name_case", "gen");
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                param("name_case", "dat");
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                param("name_case", "acc");
            case UserListView.TYPE_FAVE /*4*/:
                param("name_case", "ins");
            case UserListView.TYPE_FOLLOWERS /*5*/:
                param("name_case", "abl");
            default:
        }
    }

    public Object parse(JSONObject o) {
        try {
            int i;
            JSONObject jp;
            UserProfile p;
            JSONArray r = o.getJSONObject("response").getJSONArray("users");
            JSONArray gr = o.getJSONObject("response").getJSONArray("groups");
            Object result = new ArrayList();
            for (i = 0; i < r.length(); i++) {
                boolean z;
                jp = r.getJSONObject(i);
                p = new UserProfile();
                p.firstName = jp.getString("first_name");
                p.lastName = jp.getString("last_name");
                p.fullName = p.firstName + " " + p.lastName;
                p.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                if (jp.has("is_friend")) {
                    if (jp.getInt("is_friend") == 1) {
                        z = true;
                    } else {
                        z = false;
                    }
                    p.isFriend = z;
                }
                if (jp.has("sex")) {
                    if (jp.getInt("sex") == 1) {
                        z = true;
                    } else {
                        z = false;
                    }
                    p.f151f = z;
                }
                p.uid = jp.getInt("id");
                result.add(p);
            }
            for (i = 0; i < gr.length(); i++) {
                jp = gr.getJSONObject(i);
                p = new UserProfile();
                p.fullName = jp.getString("name");
                p.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                p.uid = -jp.getInt("id");
                result.add(p);
            }
            return result;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
