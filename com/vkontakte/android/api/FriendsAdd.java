package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class FriendsAdd extends APIRequest {
    Callback callback;
    int uid;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, int i2);
    }

    public FriendsAdd(int uid, String message) {
        super("friends.add");
        this.uid = uid;
        param("user_id", uid);
        if (message != null) {
            param("text", message);
        }
    }

    public Object parse(JSONObject o) {
        try {
            return Integer.valueOf(o.getInt("response"));
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success(this.uid, ((Integer) result).intValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
