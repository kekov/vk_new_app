package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public class BoardGetComments extends APIRequest {
    Callback callback;
    Pattern ptn1;
    Pattern ptn2;
    Pattern ptn3;
    Pattern ptn4;
    Pattern ptn5;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<BoardComment> arrayList, int i, int i2, String str, int i3, ArrayList<PollOption> arrayList2);
    }

    public BoardGetComments(int gid, int tid, int offset, int count) {
        super("board.getComments");
        this.ptn1 = Pattern.compile("((?:(?:http|https)://)?[a-zA-Z\u0430-\u044f\u0410-\u042f0-9-]+\\.[a-zA-Z\u0430-\u044f\u0410-\u042f]{2,4}[a-zA-Z/?\\.=#%&-_]+)");
        this.ptn2 = Pattern.compile("\\[id(\\d+):bp\\-(\\d+)_(\\d+)\\|([^\\]]+)\\]");
        this.ptn3 = Pattern.compile("\\[club(\\d+):bp\\-(\\d+)_(\\d+)\\|([^\\]]+)\\]");
        this.ptn4 = Pattern.compile("\\[id(\\d+)\\|([^\\]]+)\\]");
        this.ptn5 = Pattern.compile("\\[club(\\d+)\\|([^\\]]+)\\]");
        param("group_id", gid).param("topic_id", tid).param("offset", offset).param("count", count);
        param("extended", 1).param("photo_sizes", 1);
    }

    public Object parse(JSONObject o) {
        try {
            int i;
            JSONObject u;
            ArrayList<BoardComment> comments = new ArrayList();
            HashMap<Integer, String> names = new HashMap();
            HashMap<Integer, String> photos = new HashMap();
            JSONArray profiles = o.getJSONObject("response").getJSONArray("profiles");
            JSONArray groups = o.getJSONObject("response").getJSONArray("groups");
            for (i = 0; i < profiles.length(); i++) {
                u = profiles.getJSONObject(i);
                names.put(Integer.valueOf(u.getInt("id")), u.getString("first_name") + " " + u.getString("last_name"));
                photos.put(Integer.valueOf(u.getInt("id")), u.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50"));
            }
            for (i = 0; i < groups.length(); i++) {
                u = groups.getJSONObject(i);
                names.put(Integer.valueOf(-u.getInt("id")), u.getString("name"));
                photos.put(Integer.valueOf(-u.getInt("id")), u.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50"));
            }
            JSONArray comms = o.getJSONObject("response").getJSONArray("items");
            for (i = 0; i < comms.length(); i++) {
                JSONObject jc = comms.getJSONObject(i);
                BoardComment c = new BoardComment();
                c.id = jc.getInt("id");
                c.uid = jc.getInt("from_id");
                c.text = jc.getString("text");
                c.time = Global.timeDiff + jc.getInt("date");
                c.userName = (String) names.get(Integer.valueOf(c.uid));
                c.userPhoto = (String) photos.get(Integer.valueOf(c.uid));
                c.attachments = new ArrayList();
                if (jc.has("attachments")) {
                    JSONArray atts = jc.getJSONArray("attachments");
                    for (int j = 0; j < atts.length(); j++) {
                        c.attachments.add(Attachment.parse(atts.getJSONObject(j)));
                    }
                }
                Attachment.sort(c.attachments);
                c.linkUrls = new ArrayList();
                c.linkTitles = new ArrayList();
                Matcher matcher = this.ptn2.matcher(c.text);
                while (matcher.find()) {
                    c.linkUrls.add("vkontakte://profile/" + matcher.group(1));
                    c.linkTitles.add(matcher.group(4));
                }
                matcher = this.ptn3.matcher(c.text);
                while (matcher.find()) {
                    c.linkUrls.add("vkontakte://profile/-" + matcher.group(1));
                    c.linkTitles.add(matcher.group(4));
                }
                matcher = this.ptn4.matcher(c.text);
                while (matcher.find()) {
                    c.linkUrls.add("vkontakte://profile/" + matcher.group(1));
                    c.linkTitles.add(matcher.group(2));
                }
                matcher = this.ptn5.matcher(c.text);
                while (matcher.find()) {
                    c.linkUrls.add("vkontakte://profile/-" + matcher.group(1));
                    c.linkTitles.add(matcher.group(2));
                }
                matcher = this.ptn1.matcher(c.text);
                while (matcher.find()) {
                    c.linkUrls.add("vklink://view/?" + matcher.group());
                    c.linkTitles.add(matcher.group());
                }
                c.text = c.text.replaceAll("\\[(id|club)[0-9]+(?::bp[-_0-9]+)?\\|([^\\]]+)\\]", "$2");
                comments.add(c);
            }
            int pollID = 0;
            String pollQuestion = null;
            int pollUserAnswer = 0;
            ArrayList<PollOption> pollOptions = new ArrayList();
            if (o.getJSONObject("response").has("poll")) {
                JSONObject poll = o.getJSONObject("response").getJSONObject("poll");
                pollID = poll.getInt("id");
                pollQuestion = poll.getString("question");
                pollUserAnswer = poll.getInt("answer_id");
                if (poll.optInt("is_closed") == 1) {
                    pollUserAnswer = 1;
                }
                JSONArray options = poll.getJSONArray("answers");
                for (i = 0; i < options.length(); i++) {
                    JSONObject jopt = options.getJSONObject(i);
                    PollOption opt = new PollOption();
                    opt.id = jopt.getInt("id");
                    opt.title = jopt.getString("text");
                    opt.numVotes = jopt.getInt("votes");
                    opt.percent = (float) jopt.getDouble("rate");
                    pollOptions.add(opt);
                }
            }
            r24 = new Object[6];
            r24[1] = Integer.valueOf(o.getJSONObject("response").getInt("count"));
            r24[2] = Integer.valueOf(pollID);
            r24[3] = pollQuestion;
            r24[4] = Integer.valueOf(pollUserAnswer);
            r24[5] = pollOptions;
            return r24;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((ArrayList) r[0], ((Integer) r[1]).intValue(), ((Integer) r[2]).intValue(), (String) r[3], ((Integer) r[4]).intValue(), (ArrayList) r[5]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
