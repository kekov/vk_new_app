package com.vkontakte.android.api;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.AudioFile;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.NewsEntry;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

public class WallGet extends APIRequest {
    Callback callback;
    public String gphoto;
    public String gtitle;
    private int uid;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<NewsEntry> arrayList, int i, Object obj, int i2, int i3);
    }

    public WallGet(int uid, int offset, int count, boolean owner, boolean needStatus) {
        super("execute.wallGetWrapNew");
        param("photo_sizes", 1);
        param("owner_id", uid).param("offset", offset).param("count", count).param("extended", 1);
        if (owner) {
            param("filter", "owner");
        }
        this.uid = uid;
    }

    public WallGet(int uid, int offset, int count, String filter) {
        super("wall.get");
        param("owner_id", uid).param("offset", offset).param("count", count).param("extended", 1).param("filter", filter);
        param("photo_sizes", 1);
    }

    private Object parseStatus(JSONObject o) throws Exception {
        if (o == null) {
            return null;
        }
        if (o.has("audio")) {
            Object af = new AudioFile();
            af.aid = o.getJSONObject("audio").getInt("aid");
            af.oid = o.getJSONObject("audio").getInt("owner_id");
            af.duration = o.getJSONObject("audio").getInt("duration");
            af.artist = o.getJSONObject("audio").getString("artist");
            af.title = o.getJSONObject("audio").getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            af.url = o.getJSONObject("audio").getString(PlusShare.KEY_CALL_TO_ACTION_URL);
            af.durationS = String.format("%d:%02d", new Object[]{Integer.valueOf(af.duration / 60), Integer.valueOf(af.duration % 60)});
            return af;
        } else if (o.has("text")) {
            return o.getString("text");
        } else {
            return null;
        }
    }

    public Object parse(JSONObject o) {
        try {
            if (o.optJSONObject("response") == null) {
                JSONObject e = o.getJSONArray("execute_errors").getJSONObject(0);
                return new ErrorResponse(e.getInt("error_code"), e.getString("error_msg"));
            }
            JSONArrayWithCount items = APIUtils.unwrapArray(o, "response");
            JSONArray profiles1 = o.getJSONObject("response").optJSONArray("profiles");
            JSONArray profiles2 = o.getJSONObject("response").optJSONArray("groups");
            Object obj;
            if (items == null) {
                obj = new Object[3];
                obj[0] = new ArrayList();
                obj[1] = Integer.valueOf(0);
                obj[2] = parseStatus(o.getJSONObject("response").optJSONObject("status"));
                return obj;
            }
            int i;
            int uid;
            HashMap<Integer, String> names = new HashMap();
            HashMap<Integer, String> photos = new HashMap();
            HashMap<Integer, Boolean> fs = new HashMap();
            if (profiles1 != null) {
                for (i = 0; i < profiles1.length(); i++) {
                    uid = profiles1.getJSONObject(i).getInt("id");
                    names.put(Integer.valueOf(uid), new StringBuilder(String.valueOf(profiles1.getJSONObject(i).getString("first_name"))).append(" ").append(profiles1.getJSONObject(i).getString("last_name")).toString());
                    photos.put(Integer.valueOf(uid), profiles1.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50"));
                    fs.put(Integer.valueOf(uid), Boolean.valueOf(profiles1.getJSONObject(i).optInt("sex") == 1));
                }
            }
            if (profiles2 != null) {
                for (i = 0; i < profiles2.length(); i++) {
                    uid = -profiles2.getJSONObject(i).getInt("id");
                    if (!names.containsKey(Integer.valueOf(uid))) {
                        names.put(Integer.valueOf(uid), profiles2.getJSONObject(i).getString("name"));
                        photos.put(Integer.valueOf(uid), profiles2.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50"));
                    }
                }
            }
            ArrayList<NewsEntry> result = new ArrayList();
            JSONObject fixed = o.getJSONObject("response").optJSONObject("fixed");
            if (fixed != null) {
                result.add(new NewsEntry(fixed, names, photos));
            }
            i = 0;
            while (true) {
                if (i >= items.array.length()) {
                    JSONObject r = o.getJSONObject("response");
                    obj = new Object[5];
                    obj[0] = result;
                    obj[1] = Integer.valueOf(items.count);
                    obj[2] = parseStatus(o.getJSONObject("response").optJSONObject("status"));
                    obj[3] = Integer.valueOf(r.optInt("postponed_count"));
                    obj[4] = Integer.valueOf(r.optInt("suggested_count"));
                    return obj;
                }
                NewsEntry ne = new NewsEntry(items.array.getJSONObject(i), names, photos, fs);
                if (this.uid != 0) {
                    ne.ownerID = this.uid;
                }
                result.add(ne);
                i++;
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((ArrayList) r[0], ((Integer) r[1]).intValue(), r[2], ((Integer) r[3]).intValue(), ((Integer) r[4]).intValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
