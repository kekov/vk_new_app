package com.vkontakte.android.api;

import com.vkontakte.android.UserProfile;

public class FriendRequest {
    public static final int STATE_ACCEPTED = 2;
    public static final int STATE_DECLINED = 3;
    public static final int STATE_LOADING = 1;
    public static final int STATE_NEW = 0;
    public String info;
    public String message;
    public UserProfile[] mutualFriends;
    public int numMutualFriends;
    public UserProfile profile;
    public int state;

    public FriendRequest() {
        this.state = 0;
    }
}
