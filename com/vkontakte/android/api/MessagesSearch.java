package com.vkontakte.android.api;

import android.text.TextUtils;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Message;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class MessagesSearch extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(List<Message> list, int i, ArrayList<UserProfile> arrayList);
    }

    public MessagesSearch(String q, int offset, int count) {
        super("messages.search");
        param("q", q);
        param("offset", offset).param("count", count).param("photo_sizes", 1);
        param("fields", "first_name,last_name,photo_100,photo_50");
    }

    public Object parse(JSONObject o) {
        try {
            ArrayList<Message> msgs = new ArrayList();
            ArrayList<UserProfile> chats = new ArrayList();
            JSONArray r = APIUtils.unwrapArray(o, "response").array;
            for (int i = 0; i < r.length(); i++) {
                JSONObject m = r.getJSONObject(i);
                Message msg = new Message(m);
                msgs.add(msg);
                if (msg.peer > 2000000000) {
                    UserProfile p = new UserProfile();
                    p.uid = msg.peer;
                    p.fullName = msg.title;
                    if (m.has("photo_50")) {
                        p.photo = m.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                    } else {
                        ArrayList<String> ph = new ArrayList();
                        ph.add("M");
                        JSONArray act = m.getJSONArray("chat_active");
                        for (int j = 0; j < Math.min(4, act.length()); j++) {
                            ph.add(act.getString(j));
                        }
                        p.photo = TextUtils.join(",", ph);
                    }
                    chats.add(p);
                }
            }
            return new Object[]{msgs, Integer.valueOf(APIUtils.unwrapArray(o, "response").count), chats};
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] o = (Object[]) result;
            this.callback.success((List) o[0], ((Integer) o[1]).intValue(), (ArrayList) o[2]);
        }
    }
}
