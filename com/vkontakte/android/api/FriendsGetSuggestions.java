package com.vkontakte.android.api;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKApplication;
import java.util.ArrayList;
import java.util.Locale;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class FriendsGetSuggestions extends APIRequest {
    Callback callback;
    boolean mutual;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<UserProfile> arrayList);
    }

    public FriendsGetSuggestions(boolean onlyMutual) {
        super("execute");
        this.mutual = false;
        this.mutual = onlyMutual;
        String str = "code";
        Locale locale = Locale.US;
        String str2 = "var f=API.friends.getSuggestions({fields:\"%s,education,city,country,common_count\",count:50%s}).items;return {f:f,mc:API.getProfiles({user_ids:%d,fields:\"country\"})[0].country.id};";
        Object[] objArr = new Object[3];
        objArr[0] = Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec";
        objArr[1] = onlyMutual ? ",filter:\"mutual\"" : ACRAConstants.DEFAULT_STRING_VALUE;
        objArr[2] = Integer.valueOf(Global.uid);
        param(str, String.format(locale, str2, objArr));
    }

    public Object parse(JSONObject o) {
        try {
            int myCountry = o.getJSONObject("response").optInt("mc");
            Object users = new ArrayList();
            JSONArray a = o.getJSONObject("response").getJSONArray("f");
            for (int i = 0; i < a.length(); i++) {
                JSONObject p = a.getJSONObject(i);
                UserProfile profile = new UserProfile();
                profile.firstName = p.getString("first_name");
                profile.lastName = p.getString("last_name");
                profile.fullName = profile.firstName + " " + profile.lastName;
                profile.photo = p.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                profile.uid = p.getInt("id");
                profile.university = ACRAConstants.DEFAULT_STRING_VALUE;
                if (this.mutual && p.has("common_count") && p.getInt("common_count") > 0) {
                    profile.university = Global.langPlural(C0436R.array.num_mutual_friends_req, p.optInt("common_count"), VKApplication.context.getResources());
                } else if ((myCountry == 0 || myCountry == p.optInt("country")) && p.optString("university_name", ACRAConstants.DEFAULT_STRING_VALUE).length() > 0) {
                    profile.university = p.getString("university_name").replace("\r\n", ACRAConstants.DEFAULT_STRING_VALUE);
                    if (p.optInt("graduation", 0) > 0) {
                        profile.university += " '" + String.format("%02d", new Object[]{Integer.valueOf(p.getInt("graduation") % 100)});
                    }
                } else if (p.has("country")) {
                    profile.university = p.getJSONObject("country").getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                    if (p.has("city")) {
                        profile.university += ", " + p.getJSONObject("city").getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                    }
                }
                users.add(profile);
            }
            return users;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
