package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class BoardDeleteTopic extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    /* renamed from: com.vkontakte.android.api.BoardDeleteTopic.1 */
    class C13981 extends APIHandler {
        C13981() {
        }

        public void success(JSONObject o) {
            BoardDeleteTopic.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (BoardDeleteTopic.this.callback != null) {
                BoardDeleteTopic.this.callback.fail(ecode, emsg);
            }
        }
    }

    public BoardDeleteTopic(int gid, int tid) {
        super("board.deleteTopic");
        param("group_id", gid).param("topic_id", tid);
        handler(new C13981());
    }

    public Object parse(JSONObject o) {
        try {
            return Boolean.valueOf(true);
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
