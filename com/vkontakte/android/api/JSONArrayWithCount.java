package com.vkontakte.android.api;

import org.json.JSONArray;

public class JSONArrayWithCount {
    public JSONArray array;
    public int count;

    public String toString() {
        return this.count + "; " + this.array;
    }
}
