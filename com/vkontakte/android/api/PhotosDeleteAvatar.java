package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import org.json.JSONObject;

public class PhotosDeleteAvatar extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(String str);
    }

    public PhotosDeleteAvatar() {
        super("execute.deleteAvatarNew");
    }

    public Object parse(JSONObject o) {
        try {
            return o.getJSONObject("response").getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((String) result);
        }
    }
}
