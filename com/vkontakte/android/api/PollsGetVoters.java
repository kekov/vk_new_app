package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.UserProfile;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class PollsGetVoters extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, Vector<UserProfile> vector);
    }

    /* renamed from: com.vkontakte.android.api.PollsGetVoters.1 */
    class C14081 extends APIHandler {
        C14081() {
        }

        public void success(JSONObject o) {
            PollsGetVoters.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (PollsGetVoters.this.callback != null) {
                PollsGetVoters.this.callback.fail(ecode, emsg);
            }
        }
    }

    public PollsGetVoters(int oid, int pollID, int answerID, int offset, int count) {
        super("polls.getVoters");
        param("fields", "first_name,last_name,online," + (Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec"));
        param("owner_id", oid);
        param("poll_id", pollID);
        param("answer_ids", answerID);
        param("offset", offset);
        param("count", count);
        handler(new C14081());
    }

    public Object parse(JSONObject o) {
        try {
            Vector<UserProfile> r = new Vector();
            JSONArray a = o.getJSONArray("response").getJSONObject(0).getJSONObject("users").optJSONArray("items");
            if (a != null) {
                for (int i = 0; i < a.length(); i++) {
                    JSONObject jp = a.getJSONObject(i);
                    UserProfile p = new UserProfile();
                    p.firstName = jp.getString("first_name");
                    p.lastName = jp.getString("last_name");
                    p.fullName = p.firstName + " " + p.lastName;
                    p.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                    p.uid = jp.getInt("id");
                    p.online = Global.getUserOnlineStatus(jp);
                    r.add(p);
                }
            }
            return new Object[]{Integer.valueOf(o.getJSONArray("response").getJSONObject(0).getJSONObject("users").getInt("count")), r};
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (Vector) r[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
