package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FaveGetUsers extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(List<UserProfile> list);
    }

    /* renamed from: com.vkontakte.android.api.FaveGetUsers.1 */
    class C14031 extends APIHandler {
        C14031() {
        }

        public void success(JSONObject o) {
            FaveGetUsers.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (FaveGetUsers.this.callback != null) {
                FaveGetUsers.this.callback.fail(ecode, emsg);
            }
        }
    }

    public FaveGetUsers() {
        super("fave.getUsers");
        param("fields", new StringBuilder(String.valueOf(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec")).append(",online").toString());
        param("count", 10000);
        handler(new C14031());
    }

    private UserProfile parseProfile(JSONObject p) throws JSONException {
        UserProfile profile = new UserProfile();
        profile.firstName = p.getString("first_name");
        profile.lastName = p.getString("last_name");
        profile.fullName = profile.firstName + " " + profile.lastName;
        profile.photo = p.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
        profile.uid = p.getInt("id");
        profile.online = Global.getUserOnlineStatus(p);
        return profile;
    }

    public Object parse(JSONObject o) {
        try {
            Object users = new ArrayList();
            JSONArray a = APIUtils.unwrapArray(o, "response").array;
            for (int i = 0; i < a.length(); i++) {
                users.add(parseProfile(a.getJSONObject(i)));
            }
            return users;
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((List) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
