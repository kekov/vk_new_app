package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class GroupsGet extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<Group> arrayList);
    }

    /* renamed from: com.vkontakte.android.api.GroupsGet.1 */
    class C14061 extends APIHandler {
        C14061() {
        }

        public void success(JSONObject o) {
            GroupsGet.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (GroupsGet.this.callback != null) {
                GroupsGet.this.callback.fail(ecode, emsg);
            }
        }
    }

    public GroupsGet(int uid) {
        super("groups.get");
        param("user_id", uid);
        param("extended", 1);
        param("fields", "start_date");
        handler(new C14061());
    }

    public Object parse(JSONObject o) {
        try {
            ArrayList<Group> result = new ArrayList();
            JSONArray a = o.getJSONObject("response").getJSONArray("items");
            if (a == null) {
                return result;
            }
            for (int i = 0; i < a.length(); i++) {
                result.add(new Group(a.getJSONObject(i)));
            }
            return result;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
