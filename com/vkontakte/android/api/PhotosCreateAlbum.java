package com.vkontakte.android.api;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import org.json.JSONObject;

public class PhotosCreateAlbum extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(PhotoAlbum photoAlbum);
    }

    public PhotosCreateAlbum(String title, String descr, int privacy, int gid) {
        super("photos.createAlbum");
        param(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, title).param(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, descr).param("privacy", privacy);
        if (gid != 0) {
            param("group_id", Math.abs(gid));
        }
    }

    public Object parse(JSONObject o) {
        try {
            return new PhotoAlbum(o.getJSONObject("response"));
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((PhotoAlbum) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
