package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class MessagesDeleteChatPhoto extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i);
    }

    public MessagesDeleteChatPhoto(int cid) {
        super("messages.deleteChatPhoto");
        param("chat_id", cid);
    }

    public Object parse(JSONObject o) {
        try {
            return Integer.valueOf(o.getJSONObject("response").getInt("message_id"));
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success(((Integer) result).intValue());
        }
    }
}
