package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public class VideoGetInfo extends APIRequest {
    private static final String CODE = "return {t:API.video.getTags({video_id:%1$d,owner_id:%2$d}),l:API.likes.getList({type:\"video\",item_id:%1$d,owner_id:%2$d,count:1}).count,il:API.likes.isLiked({type:\"video\",item_id:%1$d,owner_id:%2$d})};";
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<UserProfile> arrayList, int i, boolean z, int i2);
    }

    public VideoGetInfo(int oid, int vid) {
        super("execute");
        param("code", String.format(Locale.US, CODE, new Object[]{Integer.valueOf(vid), Integer.valueOf(oid)}));
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray tags = o.getJSONObject("response").getJSONArray("t");
            int myTagID = -1;
            ArrayList<UserProfile> t = new ArrayList();
            for (int i = 0; i < tags.length(); i++) {
                UserProfile p = new UserProfile();
                JSONObject tag = tags.getJSONObject(i);
                p.uid = tag.getInt("id");
                p.fullName = tag.getString("tagged_name");
                t.add(p);
                if (p.uid == Global.uid) {
                    myTagID = tag.getInt("tag_id");
                }
            }
            return new Object[]{t, Integer.valueOf(o.getJSONObject("response").getInt("l")), Integer.valueOf(o.getJSONObject("response").getJSONObject("il").getInt("liked")), Integer.valueOf(myTagID)};
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        boolean z = true;
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] d = (Object[]) result;
            Callback callback = this.callback;
            ArrayList arrayList = (ArrayList) d[0];
            int intValue = ((Integer) d[1]).intValue();
            if (((Integer) d[2]).intValue() != 1) {
                z = false;
            }
            callback.success(arrayList, intValue, z, ((Integer) d[3]).intValue());
        }
    }
}
