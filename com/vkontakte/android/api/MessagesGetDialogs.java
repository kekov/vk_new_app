package com.vkontakte.android.api;

import android.text.TextUtils;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.DialogEntry;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.Message;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKApplication;
import java.util.ArrayList;
import java.util.HashMap;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class MessagesGetDialogs extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, ArrayList<DialogEntry> arrayList);
    }

    public MessagesGetDialogs(int offset, int count) {
        super("execute.getDialogsWithProfilesNew");
        param("offset", offset).param("count", count);
        Log.m526e("vk", "MESSAGES GET DIALOGS");
    }

    public Object parse(JSONObject o) {
        try {
            int i;
            String myPhoto = VKApplication.context.getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE);
            ArrayList<DialogEntry> results = new ArrayList();
            HashMap<Integer, UserProfile> profiles = new HashMap();
            JSONArray p = o.optJSONObject("response").optJSONArray("p");
            if (p != null) {
                for (i = 0; i < p.length(); i++) {
                    UserProfile up = new UserProfile();
                    up.firstName = p.optJSONObject(i).optString("first_name");
                    up.lastName = p.optJSONObject(i).optString("last_name");
                    up.fullName = new StringBuilder(String.valueOf(p.optJSONObject(i).optString("first_name"))).append(" ").append(p.optJSONObject(i).optString("last_name")).toString();
                    up.photo = p.optJSONObject(i).optString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                    up.uid = p.optJSONObject(i).optInt("id");
                    up.online = Global.getUserOnlineStatus(p.optJSONObject(i));
                    profiles.put(Integer.valueOf(up.uid), up);
                }
            }
            profiles.put(Integer.valueOf(0), new UserProfile());
            JSONArrayWithCount aa = APIUtils.unwrapArray(o.optJSONObject("response"), "a");
            if (aa == null) {
                return new Object[]{Integer.valueOf(0), results};
            }
            JSONArray a = aa.array;
            int total = APIUtils.unwrapArray(o.optJSONObject("response"), "a").count;
            for (i = 0; i < a.length(); i++) {
                DialogEntry entry = new DialogEntry();
                JSONObject msg = a.getJSONObject(i).getJSONObject(LongPollService.EXTRA_MESSAGE);
                if (profiles.containsKey(Integer.valueOf(msg.getInt("user_id")))) {
                    entry.profile = (UserProfile) profiles.get(Integer.valueOf(msg.getInt("user_id")));
                }
                if (entry.profile == null) {
                    entry.profile = new UserProfile();
                    entry.profile.uid = msg.getInt("user_id");
                }
                Message m = new Message(msg, new HashMap(), new HashMap());
                entry.lastMessage = m;
                if (profiles.containsKey(Integer.valueOf(m.sender))) {
                    entry.lastMessagePhoto = ((UserProfile) profiles.get(Integer.valueOf(m.sender))).photo;
                } else {
                    Log.m526e("vk", "Profile for " + m.sender + " not found!!!");
                }
                int i2 = m.peer;
                if (r0 > 2000000000) {
                    entry.profile = new UserProfile();
                    entry.profile.uid = m.peer;
                    entry.profile.fullName = msg.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                    entry.profile.online = msg.getInt("admin_id");
                    if (msg.has("photo_50")) {
                        String str;
                        UserProfile userProfile = entry.profile;
                        if (Global.displayDensity > 1.0f) {
                            str = "photo_100";
                        } else {
                            str = "photo_50";
                        }
                        userProfile.photo = msg.getString(str);
                    } else {
                        ArrayList<String> ph = new ArrayList();
                        ph.add("M");
                        JSONArray act = msg.getJSONArray("chat_active");
                        Log.m528i("vk", "chat active " + act);
                        for (int j = 0; j < Math.min(act.length(), 4); j++) {
                            if (profiles.containsKey(Integer.valueOf(act.getInt(j)))) {
                                ph.add(((UserProfile) profiles.get(Integer.valueOf(act.getInt(j)))).photo);
                            }
                        }
                        entry.profile.photo = TextUtils.join("|", ph);
                        Log.m528i("vk", "Set photo " + entry.profile.photo);
                    }
                }
                results.add(entry);
            }
            return new Object[]{Integer.valueOf(total), results};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (ArrayList) r[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
