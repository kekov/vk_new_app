package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class VoipReply extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(String str, String str2, String str3, String str4);
    }

    public VoipReply(int callID, int fromID) {
        super("voip.reply");
        param("call_id", callID).param("from_id", fromID);
    }

    public Object parse(JSONObject o) {
        try {
            o = o.getJSONObject("response");
            return new String[]{o.getString("rtmp_server"), o.getString("near_stream"), o.getString("far_stream"), o.getString("hash")};
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            String[] r = (String[]) result;
            this.callback.success(r[0], r[1], r[2], r[3]);
        }
    }
}
