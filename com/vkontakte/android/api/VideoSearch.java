package com.vkontakte.android.api;

import android.preference.PreferenceManager;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.VKApplication;
import java.util.HashMap;
import java.util.Locale;
import java.util.Vector;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class VideoSearch extends APIRequest {
    private static final String CODE = "var v=API.video.search({q:\"%s\",offset:%d,count:%d%s%s,adult:%d});return {v:v,u:API.getProfiles({user_ids:v@.owner_id,fields:\"%s\"})};";
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(Vector<VideoFile> vector);
    }

    public VideoSearch(String q, int offset, int count, boolean hd, int length) {
        String str;
        super("execute");
        boolean safe = PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getBoolean("safeSearch", true);
        String str2 = "code";
        Locale locale = Locale.US;
        String str3 = CODE;
        Object[] objArr = new Object[7];
        objArr[0] = q.replace("\"", "\\\"");
        objArr[1] = Integer.valueOf(offset);
        objArr[2] = Integer.valueOf(count);
        objArr[3] = hd ? ",hd:1" : ACRAConstants.DEFAULT_STRING_VALUE;
        if (length > 0) {
            str = ",filters:" + (length == 1 ? "\"short\"" : "\"long\"");
        } else {
            str = ACRAConstants.DEFAULT_STRING_VALUE;
        }
        objArr[4] = str;
        objArr[5] = Integer.valueOf(!safe ? 1 : 0);
        objArr[6] = Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec";
        param(str2, String.format(locale, str3, objArr));
    }

    public Object parse(JSONObject o) {
        try {
            int i;
            Vector<VideoFile> vf = new Vector();
            JSONArray arr = APIUtils.unwrapArray(o.getJSONObject("response"), "v").array;
            JSONArray u = o.getJSONObject("response").optJSONArray("u");
            HashMap<Integer, String> names = new HashMap();
            HashMap<Integer, String> photos = new HashMap();
            if (u != null) {
                for (i = 0; i < u.length(); i++) {
                    int uid = u.getJSONObject(i).getInt("id");
                    names.put(Integer.valueOf(uid), new StringBuilder(String.valueOf(u.getJSONObject(i).getString("first_name"))).append(" ").append(u.getJSONObject(i).getString("last_name")).toString());
                    photos.put(Integer.valueOf(uid), u.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec"));
                }
            }
            if (arr == null) {
                return vf;
            }
            for (i = 0; i < arr.length(); i++) {
                vf.add(new VideoFile(arr.getJSONObject(i)));
            }
            return vf;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((Vector) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
