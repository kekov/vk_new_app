package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.LongPollService;
import org.json.JSONObject;

public class WallRepost extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, int i2, int i3);
    }

    public WallRepost(String object, int gid, String msg) {
        super("wall.repost");
        param("object", object);
        param(LongPollService.EXTRA_MESSAGE, msg);
        if (gid > 0) {
            param("group_id", gid);
        }
    }

    public Object parse(JSONObject o) {
        try {
            JSONObject r = o.getJSONObject("response");
            return new int[]{r.optInt("post_id"), r.getInt("reposts_count"), r.getInt("likes_count")};
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            int[] r = (int[]) result;
            this.callback.success(r[0], r[1], r[2]);
        }
    }
}
