package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class SearchGetHints extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(List<UserProfile> list);
    }

    public SearchGetHints(String q) {
        super("execute.searchHints");
        param("filters", "mutual_friends,correspondents");
        param("q", q);
        param("limit", 30);
        param("fields", "photo_rec,photo_medium_rec,online,verified");
    }

    public Object parse(JSONObject o) {
        ArrayList<UserProfile> result = new ArrayList();
        try {
            JSONArray r = o.getJSONArray("response");
            for (int i = 0; i < r.length(); i++) {
                JSONObject item = r.getJSONObject(i);
                UserProfile profile = new UserProfile();
                String type = item.getString(WebDialog.DIALOG_PARAM_TYPE);
                if ("profile".equals(type)) {
                    JSONObject jp = item.getJSONObject("profile");
                    profile.uid = jp.getInt("id");
                    profile.firstName = jp.getString("first_name");
                    profile.lastName = jp.getString("last_name");
                    profile.fullName = profile.firstName + " " + profile.lastName;
                    profile.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                    profile.online = Global.getUserOnlineStatus(jp);
                    profile.f151f = jp.optInt("verified") == 1;
                } else if ("group".equals(type)) {
                    JSONObject jg = item.getJSONObject("group");
                    profile.uid = -jg.getInt("id");
                    profile.fullName = jg.getString("name");
                    profile.photo = jg.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                }
                profile.university = item.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION);
                result.add(profile);
            }
        } catch (Exception e) {
        }
        return result;
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((List) result);
        }
    }
}
