package com.vkontakte.android.api;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.AnimationDuration;
import java.util.ArrayList;
import java.util.HashMap;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class BoardGetTopics extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, ArrayList<BoardTopic> arrayList, boolean z, int i2);
    }

    public BoardGetTopics(int gid, int offset, int count) {
        super("board.getTopics");
        param("group_id", gid).param("offset", offset).param("count", count).param("extended", 1);
        param("preview", 2).param("preview_length", (int) AnimationDuration.SELECTION_EXIT_FADE);
    }

    public BoardGetTopics(int gid, int tid) {
        super("board.getTopics");
        param("group_id", gid).param("topic_ids", tid).param("extended", 1);
        param("preview", 2).param("preview_length", (int) AnimationDuration.SELECTION_EXIT_FADE);
    }

    public Object parse(JSONObject o) {
        try {
            int i;
            JSONObject p;
            UserProfile up;
            ArrayList<BoardTopic> topics = new ArrayList();
            HashMap<Integer, UserProfile> users = new HashMap();
            JSONArray u = o.getJSONObject("response").optJSONArray("profiles");
            JSONArray g = o.getJSONObject("response").optJSONArray("groups");
            if (u != null) {
                for (i = 0; i < u.length(); i++) {
                    p = u.getJSONObject(i);
                    up = new UserProfile();
                    up.uid = p.getInt("id");
                    up.firstName = p.getString("first_name");
                    up.lastName = p.getString("last_name");
                    up.fullName = up.firstName + " " + up.lastName;
                    up.photo = p.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                    users.put(Integer.valueOf(up.uid), up);
                }
            }
            if (g != null) {
                for (i = 0; i < g.length(); i++) {
                    p = g.getJSONObject(i);
                    up = new UserProfile();
                    up.uid = -p.getInt("id");
                    up.firstName = p.getString("name");
                    up.lastName = ACRAConstants.DEFAULT_STRING_VALUE;
                    up.fullName = up.firstName + " " + up.lastName;
                    up.photo = p.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                    users.put(Integer.valueOf(up.uid), up);
                }
            }
            JSONArray a = o.getJSONObject("response").getJSONArray("items");
            for (i = 0; i < a.length(); i++) {
                JSONObject jt = a.getJSONObject(i);
                BoardTopic t = new BoardTopic();
                t.id = jt.getInt("id");
                t.title = jt.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                t.created = jt.getInt("created") + Global.timeDiff;
                t.creator = jt.getInt("created_by");
                t.updated = jt.getInt("updated") + Global.timeDiff;
                if (jt.getInt("is_closed") != 0) {
                    t.flags |= 1;
                }
                if (jt.getInt("is_fixed") != 0) {
                    t.flags |= 2;
                }
                t.numComments = jt.getInt("comments");
                t.lastComment = jt.getString("last_comment").replaceAll("\\[(id|club)[0-9]+(?::bp[-_0-9]+)?\\|([^\\]]+)\\]", "$2");
                if (t.lastComment.length() == 0) {
                    t.lastComment = VKApplication.context.getResources().getString(C0436R.string.attachment);
                }
                t.updatedBy = (UserProfile) users.get(Integer.valueOf(jt.getInt("updated_by")));
                topics.add(t);
            }
            Object obj = new Object[4];
            obj[0] = Integer.valueOf(o.getJSONObject("response").getInt("count"));
            obj[1] = topics;
            obj[2] = Boolean.valueOf(o.getJSONObject("response").getInt("can_add_topics") == 1);
            obj[3] = Integer.valueOf(o.getJSONObject("response").getInt("default_order"));
            return obj;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (ArrayList) r[1], ((Boolean) r[2]).booleanValue(), ((Integer) r[3]).intValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
