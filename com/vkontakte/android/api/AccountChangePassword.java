package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import org.json.JSONObject;

public class AccountChangePassword extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(String str, String str2);
    }

    public AccountChangePassword(String sid, String hash, String pass) {
        super("account.changePassword");
        param("restore_sid", sid);
        param("change_password_hash", hash);
        param("password", pass);
    }

    public AccountChangePassword(String oldPass, String pass) {
        super("account.changePassword");
        param("old_password", oldPass);
        param("new_password", pass);
    }

    public Object parse(JSONObject o) {
        try {
            o = o.getJSONObject("response");
            return new String[]{o.getString("token"), o.optString("secret")};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            String[] r = (String[]) result;
            this.callback.success(r[0], r[1]);
        }
    }
}
