package com.vkontakte.android.api;

import android.text.TextUtils;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Attachment;
import java.util.ArrayList;
import org.json.JSONObject;

public class BoardAddComment extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i);
    }

    public BoardAddComment(int gid, int tid, String text, ArrayList<Attachment> atts, String title) {
        super(tid == -1 ? "board.addTopic" : "board.addComment");
        param("group_id", gid).param("topic_id", tid).param("text", text);
        if (atts != null && atts.size() > 0) {
            param("attachments", TextUtils.join(",", atts));
        }
        if (tid == -1) {
            param(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, title);
        }
    }

    public Object parse(JSONObject o) {
        try {
            return Integer.valueOf(o.getInt("response"));
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success(((Integer) result).intValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
