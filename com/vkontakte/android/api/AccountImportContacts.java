package com.vkontakte.android.api;

import android.text.TextUtils;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import java.util.List;
import org.json.JSONObject;

public class AccountImportContacts extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    public AccountImportContacts(List<String> ids, String myId) {
        super("account.importContacts");
        param("contacts", TextUtils.join(",", ids));
        if (myId != null) {
            param("my_contact", myId);
        }
    }

    public Object parse(JSONObject o) {
        try {
            return o.getInt("response") == 1 ? Boolean.valueOf(true) : Boolean.valueOf(false);
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }
}
