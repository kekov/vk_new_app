package com.vkontakte.android.api;

import android.text.TextUtils;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.UserProfile;
import java.util.Locale;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class FriendsGetByPhones extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(Vector<UserProfile> vector);
    }

    public FriendsGetByPhones(Vector<String> phones) {
        super("execute");
        forceHTTPS(true);
        Locale locale = Locale.US;
        String str = "API.account.importContacts({contacts:\"%s\"}); return API.friends.getSuggestions({fields:\"%s\"});";
        Object[] objArr = new Object[2];
        objArr[0] = TextUtils.join(",", phones).replace('\"', ' ');
        objArr[1] = "education,city," + (Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
        param("code", String.format(locale, str, objArr));
    }

    public Object parse(JSONObject o) {
        try {
            Vector<UserProfile> profiles = new Vector();
            JSONArray a = APIUtils.unwrapArray(o, "response").array;
            if (a == null || a.length() <= 0) {
                return profiles;
            }
            for (int i = 0; i < a.length(); i++) {
                JSONObject oo = a.getJSONObject(i);
                UserProfile u = new UserProfile();
                u.uid = oo.getInt("id");
                u.firstName = oo.getString("first_name");
                u.lastName = oo.getString("last_name");
                u.fullName = u.firstName + " " + u.lastName;
                u.photo = oo.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                if (oo.has("university_name")) {
                    u.university = oo.getString("university_name");
                    if (oo.has("graduation") && oo.getString("graduation").length() >= 2) {
                        u.university += " '" + oo.getString("graduation").substring(oo.getString("graduation").length() - 2, oo.getString("graduation").length());
                    }
                }
                profiles.add(u);
            }
            return profiles;
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((Vector) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
