package com.vkontakte.android.api;

import android.text.TextUtils;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.Message;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetHistoryBatch extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(HashMap<Integer, ArrayList<Message>> hashMap);
    }

    /* renamed from: com.vkontakte.android.api.GetHistoryBatch.1 */
    class C14051 extends APIHandler {
        C14051() {
        }

        public void success(JSONObject o) {
            GetHistoryBatch.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (GetHistoryBatch.this.callback != null) {
                GetHistoryBatch.this.callback.fail(ecode, emsg);
            }
        }
    }

    public GetHistoryBatch(ArrayList<Integer> ids) {
        super("execute");
        ArrayList<String> ss = new ArrayList();
        Iterator it = ids.iterator();
        while (it.hasNext()) {
            int pid = ((Integer) it.next()).intValue();
            ss.add(String.format(Locale.US, "{peer:%1$d,messages:API.messages.getHistory({uid:%1$d,count:20})}", new Object[]{Integer.valueOf(pid)}));
        }
        param("code", "return [" + TextUtils.join(",", ss) + "];");
        handler(new C14051());
    }

    private Message parseOneMessage(JSONObject jm, int peer) throws Exception {
        boolean z = true;
        Message msg = new Message();
        msg.id = jm.getInt("mid");
        msg.out = jm.getInt("from_id") == Global.uid;
        msg.sender = jm.getInt("from_id");
        msg.time = Global.timeDiff + jm.getInt("date");
        msg.setText(jm.getString("body"));
        if (jm.getInt(LongPollService.EXTRA_READ_STATE) != 1) {
            z = false;
        }
        msg.readState = z;
        msg.peer = peer;
        if (jm.has("attachments")) {
            JSONArray ja = jm.getJSONArray("attachments");
            ArrayList<Attachment> atts = new ArrayList();
            for (int j = 0; j < ja.length(); j++) {
                atts.add(Attachment.parse(ja.getJSONObject(j)));
            }
            Attachment.sort(atts);
            msg.attachments = atts;
        }
        return msg;
    }

    public Object parse(JSONObject o) {
        try {
            Object result = new HashMap();
            JSONArray a = o.getJSONArray("response");
            for (int i = 0; i < a.length(); i++) {
                JSONObject obj = a.getJSONObject(i);
                int peer = obj.getInt("peer");
                JSONArray jmsgs = obj.getJSONArray("messages");
                ArrayList<Message> msgs = new ArrayList();
                for (int j = 1; j < jmsgs.length(); j++) {
                    msgs.add(parseOneMessage(jmsgs.getJSONObject(j), peer));
                }
                result.put(Integer.valueOf(peer), msgs);
            }
            return result;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((HashMap) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
