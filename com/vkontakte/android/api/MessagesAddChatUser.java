package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class MessagesAddChatUser extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    public MessagesAddChatUser(int chatID, int userID) {
        super("messages.addChatUser");
        param("chat_id", chatID).param("user_id", userID);
    }

    public Object parse(JSONObject o) {
        try {
            return Boolean.valueOf(true);
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }
}
