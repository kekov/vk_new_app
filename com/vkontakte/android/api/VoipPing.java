package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class VoipPing extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void onCamera(boolean z);

        void onHangup();

        void onReceived();

        void onReply(String str);

        void success();
    }

    public VoipPing(int callID) {
        super("voip.ping");
        param("call_id", callID);
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray a = o.getJSONArray("response");
            for (int i = 0; i < a.length(); i++) {
                JSONObject ev = a.getJSONObject(i);
                if (ev.has("custom")) {
                    String code = ev.getJSONObject("custom").optString("code");
                    if ("received".equals(code)) {
                        if (this.callback != null) {
                            this.callback.onReceived();
                        }
                    } else if ("reply".equals(code)) {
                        if (this.callback != null) {
                            this.callback.onReply(ev.getJSONObject("custom").optString("stream"));
                        }
                    } else if ("hangup".equals(code)) {
                        if (this.callback != null) {
                            this.callback.onHangup();
                        }
                    } else if ("camera".equals(code)) {
                        if (this.callback != null) {
                            this.callback.onCamera(ev.getJSONObject("custom").optInt("available") == 1);
                        }
                    } else if (!"replied".equals(code)) {
                        Log.m526e("vk", "Unknown event: " + code);
                    } else if (this.callback != null) {
                        this.callback.onReply(null);
                    }
                }
            }
            if (this.callback != null) {
                this.callback.success();
            }
        } catch (Exception e) {
        }
        return o;
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
