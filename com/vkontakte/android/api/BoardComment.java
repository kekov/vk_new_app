package com.vkontakte.android.api;

import com.vkontakte.android.Attachment;
import java.util.ArrayList;

public class BoardComment {
    public ArrayList<Attachment> attachments;
    public int id;
    public ArrayList<String> linkTitles;
    public ArrayList<String> linkUrls;
    public String text;
    public int time;
    public int uid;
    public String userName;
    public String userPhoto;
}
