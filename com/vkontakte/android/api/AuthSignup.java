package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Auth;
import org.json.JSONObject;

public class AuthSignup extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(String str);
    }

    public AuthSignup(String firstName, String lastName, int gender, String phone, String sid, boolean voice) {
        super("auth.signup");
        param("first_name", firstName);
        param("last_name", lastName);
        param("sex", gender);
        param("phone", phone);
        param(WebDialog.DIALOG_PARAM_CLIENT_ID, (int) Auth.API_ID);
        param("client_secret", Auth.API_SECRET);
        if (voice) {
            param("voice", 1);
        }
        if (sid != null) {
            param("sid", sid);
        }
    }

    public Object parse(JSONObject o) {
        try {
            return o.getJSONObject("response").optString("sid");
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((String) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
