package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class NotesGetById extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(String str);
    }

    public NotesGetById(int oid, int nid) {
        super("notes.getById");
        param("owner_id", oid).param("note_id", nid);
    }

    public Object parse(JSONObject o) {
        try {
            return o.getJSONObject("response").optString("text");
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((String) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
