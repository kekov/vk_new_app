package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class VideoGetById extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(VideoFile videoFile);
    }

    public VideoGetById(int oid, int vid, String accessKey) {
        super("video.get");
        param("videos", new StringBuilder(String.valueOf(oid)).append("_").append(vid).toString());
        param("extended", 1);
        if (accessKey != null) {
            param("access_key", accessKey);
        }
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray a = APIUtils.unwrapArray(o, "response").array;
            Object videoFile = new VideoFile();
            if (a.length() > 0) {
                return new VideoFile(a.getJSONObject(0));
            }
            return videoFile;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((VideoFile) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
