package com.vkontakte.android.api;

public class PollOption {
    public int id;
    public int numVotes;
    public float percent;
    public String title;
}
