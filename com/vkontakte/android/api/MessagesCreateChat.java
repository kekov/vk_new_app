package com.vkontakte.android.api;

import android.text.TextUtils;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;

public class MessagesCreateChat extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i);
    }

    public MessagesCreateChat(ArrayList<UserProfile> users, String title) {
        super("messages.createChat");
        ArrayList<Integer> uids = new ArrayList();
        Iterator it = users.iterator();
        while (it.hasNext()) {
            uids.add(Integer.valueOf(((UserProfile) it.next()).uid));
        }
        param("user_ids", TextUtils.join(",", uids));
        if (title != null) {
            param(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, title);
        }
    }

    public Object parse(JSONObject o) {
        try {
            return Integer.valueOf(o.getInt("response"));
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success(((Integer) result).intValue());
        }
    }
}
