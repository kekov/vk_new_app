package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import org.json.JSONObject;

public class AudioGetLyrics extends APIRequest {
    Callback callback;
    int lid;

    public interface Callback {
        void fail(int i, String str);

        void success(String str, int i);
    }

    /* renamed from: com.vkontakte.android.api.AudioGetLyrics.1 */
    class C13931 extends APIHandler {
        C13931() {
        }

        public void success(JSONObject o) {
            AudioGetLyrics.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (AudioGetLyrics.this.callback != null) {
                AudioGetLyrics.this.callback.fail(ecode, emsg);
            }
        }
    }

    public AudioGetLyrics(int id) {
        super("audio.getLyrics");
        param("lyrics_id", id);
        this.lid = id;
        handler(new C13931());
    }

    public Object parse(JSONObject o) {
        try {
            return o.getJSONObject("response").getString("text");
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((String) result, this.lid);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
