package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class AuthRestore extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(String str);
    }

    public AuthRestore(String number, String sid, boolean voice) {
        super("auth.restore");
        param("phone", number);
        param("sid", sid);
        if (voice) {
            param("voice", 1);
        }
    }

    public Object parse(JSONObject o) {
        try {
            return o.getJSONObject("response").getString("sid");
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((String) result);
        }
    }
}
