package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class DocsGet extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, Vector<Document> vector, boolean z);
    }

    public DocsGet(int oid, int offset, int count) {
        super("execute.getDocs");
        param("owner_id", oid).param("offset", offset).param("count", count);
    }

    public Object parse(JSONObject o) {
        boolean z = true;
        try {
            Vector<Document> docs = new Vector();
            int total = 0;
            JSONArray a = APIUtils.unwrapArray(o, "response").array;
            if (a != null) {
                total = APIUtils.unwrapArray(o, "response").count;
                for (int i = 0; i < a.length(); i++) {
                    docs.add(new Document(a.getJSONObject(i)));
                }
            }
            Object obj = new Object[3];
            obj[0] = Integer.valueOf(total);
            obj[1] = docs;
            if (o.getJSONObject("response").optInt("can_add", 1) != 1) {
                z = false;
            }
            obj[2] = Boolean.valueOf(z);
            return obj;
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (Vector) r[1], ((Boolean) r[2]).booleanValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
