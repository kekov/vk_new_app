package com.vkontakte.android.api;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class MessagesGetChatUsers extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, Vector<UserProfile> vector, int i2, String str);
    }

    public MessagesGetChatUsers(int chatID) {
        super("messages.getChat");
        param("chat_id", chatID).param("fields", "online,first_name,last_name,sex," + (Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec"));
    }

    public Object parse(JSONObject o) {
        try {
            Vector<UserProfile> r = new Vector();
            JSONArray a = o.getJSONArray("response");
            if (a != null) {
                for (int i = 0; i < a.length(); i++) {
                    boolean z;
                    JSONObject jp = a.getJSONObject(i);
                    UserProfile p = new UserProfile();
                    p.firstName = jp.getString("first_name");
                    p.lastName = jp.getString("last_name");
                    p.fullName = p.firstName + " " + p.lastName;
                    p.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                    p.uid = jp.getInt("id");
                    p.online = Global.getUserOnlineStatus(jp);
                    if (jp.optInt("sex") == 1) {
                        z = true;
                    } else {
                        z = false;
                    }
                    p.f151f = z;
                    r.add(p);
                }
            }
            return new Object[]{Integer.valueOf(r.size()), r, Integer.valueOf(o.getJSONObject("response").getInt("admin_id")), o.getJSONObject("response").getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (Vector) r[1], ((Integer) r[2]).intValue(), (String) r[3]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
