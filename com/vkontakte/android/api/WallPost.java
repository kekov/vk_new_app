package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import org.json.JSONObject;

public class WallPost extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i);
    }

    public WallPost(int ownerID, int postID) {
        super("wall.post");
        param("post_id", postID).param("owner_id", ownerID);
    }

    public Object parse(JSONObject o) {
        try {
            return Integer.valueOf(o.getJSONObject("response").getInt("post_id"));
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success(((Integer) result).intValue());
        }
    }
}
