package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class BoardDeleteComment extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    /* renamed from: com.vkontakte.android.api.BoardDeleteComment.1 */
    class C13971 extends APIHandler {
        C13971() {
        }

        public void success(JSONObject o) {
            BoardDeleteComment.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (BoardDeleteComment.this.callback != null) {
                BoardDeleteComment.this.callback.fail(ecode, emsg);
            }
        }
    }

    public BoardDeleteComment(int gid, int tid, int cid) {
        super("board.deleteComment");
        param("group_id", gid).param("topic_id", tid).param("comment_id", cid);
        handler(new C13971());
    }

    public Object parse(JSONObject o) {
        try {
            return Boolean.valueOf(true);
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
