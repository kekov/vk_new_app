package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.PhotoTag;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class PhotosGetTags extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<PhotoTag> arrayList);
    }

    public PhotosGetTags(int oid, int pid, String accessKey) {
        super("photos.getTags");
        param("owner_id", oid).param("photo_id", pid);
        if (accessKey != null) {
            param("access_key", accessKey);
        }
    }

    public Object parse(JSONObject o) {
        try {
            Object tags = new ArrayList();
            JSONArray r = o.getJSONArray("response");
            for (int i = 0; i < r.length(); i++) {
                JSONObject j = r.getJSONObject(i);
                PhotoTag t = new PhotoTag();
                t.id = j.getInt("id");
                t.userID = j.getInt("user_id");
                t.x1 = j.getDouble("x");
                t.y1 = j.getDouble("y");
                t.x2 = j.getDouble("x2");
                t.y2 = j.getDouble("y2");
                t.userName = j.getString("tagged_name");
                tags.add(t);
            }
            return tags;
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }
}
