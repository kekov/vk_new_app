package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

public class GroupsGetInvites extends APIRequest {
    private static final String CODE = "var g=API.groups.getInvites({fields:\"members_count\"}).items; return {g:g,u:API.getProfiles({user_ids:g@.invited_by})};";
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<GroupInvitation> arrayList);
    }

    public GroupsGetInvites() {
        super("execute");
        param("code", CODE);
    }

    public Object parse(JSONObject o) {
        ArrayList<GroupInvitation> groups = new ArrayList();
        try {
            JSONArray a = o.getJSONObject("response").optJSONArray("g");
            if (a.length() != 0) {
                int i;
                JSONArray u = o.getJSONObject("response").optJSONArray("u");
                HashMap<Integer, UserProfile> users = new HashMap();
                for (i = 0; i < u.length(); i++) {
                    JSONObject jp = u.getJSONObject(i);
                    UserProfile p = new UserProfile();
                    p.firstName = jp.getString("first_name");
                    p.lastName = jp.getString("last_name");
                    p.fullName = p.firstName + " " + p.lastName;
                    p.uid = jp.getInt("id");
                    users.put(Integer.valueOf(p.uid), p);
                }
                for (i = 0; i < a.length(); i++) {
                    JSONObject jg = a.getJSONObject(i);
                    Group g = new Group(jg);
                    GroupInvitation inv = new GroupInvitation();
                    inv.group = g;
                    inv.inviter = (UserProfile) users.get(Integer.valueOf(jg.getInt("invited_by")));
                    inv.size = jg.getInt("members_count");
                    groups.add(inv);
                }
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        return groups;
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }
}
