package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class PhotosDeleteAlbum extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    public PhotosDeleteAlbum(int aid, int gid) {
        super("photos.deleteAlbum");
        param("album_id", aid);
        if (gid > 0) {
            param("group_id", gid);
        }
    }

    public Object parse(JSONObject o) {
        return Boolean.valueOf(true);
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
