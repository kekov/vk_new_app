package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.Global;
import com.vkontakte.android.Indexable;
import com.vkontakte.android.Log;
import org.json.JSONObject;

public class Group implements Indexable {
    public static final int ADMIN_LEVEL_ADMIN = 3;
    public static final int ADMIN_LEVEL_EDITOR = 2;
    public static final int ADMIN_LEVEL_MODERATOR = 1;
    public static final int ADMIN_LEVEL_NONE = 0;
    public static final int TYPE_EVENT = 1;
    public static final int TYPE_GROUP = 0;
    public static final int TYPE_PUBLIC = 2;
    public int adminLevel;
    public int id;
    public boolean isAdmin;
    public int isClosed;
    public String name;
    public String photo;
    public int startTime;
    public int type;

    public Group(JSONObject gr) {
        boolean z = true;
        try {
            this.id = gr.getInt("id");
            this.name = gr.getString("name");
            if (gr.optInt("is_admin", TYPE_GROUP) <= 0) {
                z = false;
            }
            this.isAdmin = z;
            this.adminLevel = gr.optInt("admin_level");
            this.isClosed = gr.getInt("is_closed");
            this.photo = gr.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
            this.type = TYPE_GROUP;
            this.startTime = gr.optInt("start_date");
            this.adminLevel = gr.optInt("admin_level");
            if ("event".equals(gr.optString(WebDialog.DIALOG_PARAM_TYPE))) {
                this.type = TYPE_EVENT;
            }
            if ("page".equals(gr.optString(WebDialog.DIALOG_PARAM_TYPE))) {
                this.type = TYPE_PUBLIC;
            }
        } catch (Exception x) {
            Log.m531w("vk", "Error parsing group", x);
        }
    }

    public char[] getIndexChars() {
        String[] words = this.name.split(" ");
        char[] result = new char[words.length];
        for (int i = TYPE_GROUP; i < words.length; i += TYPE_EVENT) {
            if (words[i].length() != 0) {
                result[i] = Character.toLowerCase(words[i].charAt(TYPE_GROUP));
            }
        }
        return result;
    }

    public boolean matches(String q) {
        return this.name.toLowerCase().indexOf(q) > -1;
    }
}
