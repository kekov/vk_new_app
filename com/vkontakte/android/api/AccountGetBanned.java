package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class AccountGetBanned extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, ArrayList<UserProfile> arrayList);
    }

    public AccountGetBanned(int offset, int count) {
        super("account.getBanned");
        param("offset", offset).param("count", count);
        param("fields", "photo_rec,photo_medium_rec");
    }

    public Object parse(JSONObject o) {
        try {
            ArrayList<UserProfile> r = new ArrayList();
            JSONArray a = APIUtils.unwrapArray(o, "response").array;
            if (a != null) {
                for (int i = 0; i < a.length(); i++) {
                    JSONObject jp = a.getJSONObject(i);
                    UserProfile p = new UserProfile();
                    p.firstName = jp.getString("first_name");
                    p.lastName = jp.getString("last_name");
                    p.fullName = p.firstName + " " + p.lastName;
                    p.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                    p.uid = jp.getInt("id");
                    p.online = Global.getUserOnlineStatus(jp);
                    r.add(p);
                }
            }
            return new Object[]{Integer.valueOf(o.getJSONObject("response").getInt("count")), r};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (ArrayList) r[1]);
        }
    }
}
