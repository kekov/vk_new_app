package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.NewsEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class NewsfeedGetComments extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<NewsEntry> arrayList, String str);
    }

    public NewsfeedGetComments(String from, int count) {
        super("newsfeed.getComments");
        if (from != null) {
            param("start_from", from);
        }
        param("count", count).param("filters", "post,photo,topic,video").param("last_comments", 1);
        param("photo_sizes", 1);
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray items = o.getJSONObject("response").optJSONArray("items");
            JSONArray profiles = o.getJSONObject("response").optJSONArray("profiles");
            JSONArray groups = o.getJSONObject("response").optJSONArray("groups");
            Vector<Integer> adminGroups = new Vector();
            ArrayList<NewsEntry> result = new ArrayList();
            if (items == null) {
                return new Object[]{result, "0"};
            }
            int i;
            HashMap<Integer, String> names = new HashMap();
            HashMap<Integer, String> photos = new HashMap();
            if (profiles != null) {
                for (i = 0; i < profiles.length(); i++) {
                    int uid = profiles.getJSONObject(i).getInt("id");
                    names.put(Integer.valueOf(uid), new StringBuilder(String.valueOf(profiles.getJSONObject(i).getString("first_name"))).append(" ").append(profiles.getJSONObject(i).getString("last_name")).toString());
                    photos.put(Integer.valueOf(uid), profiles.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50"));
                }
            }
            if (groups != null) {
                for (i = 0; i < groups.length(); i++) {
                    int gid = groups.getJSONObject(i).getInt("id");
                    names.put(Integer.valueOf(-gid), groups.getJSONObject(i).getString("name"));
                    photos.put(Integer.valueOf(-gid), groups.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50"));
                    if (groups.getJSONObject(i).optInt("is_admin", 0) == 1) {
                        adminGroups.add(Integer.valueOf(gid));
                    }
                }
            }
            for (i = 0; i < items.length(); i++) {
                NewsEntry e = new NewsEntry(items.getJSONObject(i), names, photos);
                if (e.ownerID < 0 && adminGroups.contains(Integer.valueOf(-e.ownerID))) {
                    e.flags |= 64;
                }
                result.add(e);
            }
            String from = o.getJSONObject("response").getString("next_from");
            return new Object[]{result, from};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((ArrayList) r[0], (String) r[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
