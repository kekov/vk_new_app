package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import java.util.ArrayList;

public class PhotosGetAlbums extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<PhotoAlbum> arrayList, ArrayList<PhotoAlbum> arrayList2);
    }

    public PhotosGetAlbums(int oid, boolean needSystem) {
        super("execute.getPhotoAlbumsNew");
        param("owner_id", oid);
        param("need_covers", 1);
        if (needSystem) {
            param("need_system", 1);
        }
        param("photo_sizes", 1);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object parse(org.json.JSONObject r23) {
        /*
        r22 = this;
        r7 = "p";
        r17 = com.vkontakte.android.Global.displayDensity;	 Catch:{ Exception -> 0x01fe }
        r18 = 1069547520; // 0x3fc00000 float:1.5 double:5.28426686E-315;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 < 0) goto L_0x000c;
    L_0x000a:
        r7 = "q";
    L_0x000c:
        r17 = com.vkontakte.android.Global.displayDensity;	 Catch:{ Exception -> 0x01fe }
        r18 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 < 0) goto L_0x0016;
    L_0x0014:
        r7 = "r";
    L_0x0016:
        r10 = new java.util.ArrayList;	 Catch:{ Exception -> 0x01fe }
        r10.<init>();	 Catch:{ Exception -> 0x01fe }
        r8 = 0;
        r17 = "response";
        r0 = r23;
        r1 = r17;
        r17 = com.vkontakte.android.api.APIUtils.unwrapArray(r0, r1);	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        r3 = r0.array;	 Catch:{ Exception -> 0x01fe }
        if (r3 == 0) goto L_0x0035;
    L_0x002c:
        r4 = 0;
    L_0x002d:
        r17 = r3.length();	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        if (r4 < r0) goto L_0x011f;
    L_0x0035:
        r13 = new java.util.ArrayList;	 Catch:{ Exception -> 0x01fe }
        r13.<init>();	 Catch:{ Exception -> 0x01fe }
        r17 = "response";
        r0 = r23;
        r1 = r17;
        r17 = r0.getJSONObject(r1);	 Catch:{ Exception -> 0x01fe }
        r18 = "all_photos";
        r17 = r17.has(r18);	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x009c;
    L_0x004c:
        r17 = "response";
        r0 = r23;
        r1 = r17;
        r17 = r0.getJSONObject(r1);	 Catch:{ Exception -> 0x01fe }
        r18 = "all_photos";
        r6 = r17.getJSONObject(r18);	 Catch:{ Exception -> 0x01fe }
        r15 = new com.vkontakte.android.api.MultiThumbPhotoAlbum;	 Catch:{ Exception -> 0x01fe }
        r15.<init>(r6);	 Catch:{ Exception -> 0x01fe }
        r17 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x01fe }
        r17 = r17.getResources();	 Catch:{ Exception -> 0x01fe }
        r18 = 2131231490; // 0x7f080302 float:1.8079062E38 double:1.0529682625E-314;
        r17 = r17.getString(r18);	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        r15.title = r0;	 Catch:{ Exception -> 0x01fe }
        r17 = "sizes";
        r0 = r17;
        r12 = r6.optJSONArray(r0);	 Catch:{ Exception -> 0x01fe }
        r9 = "";
        if (r12 == 0) goto L_0x0087;
    L_0x007e:
        r5 = 0;
    L_0x007f:
        r17 = r12.length();	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        if (r5 < r0) goto L_0x0217;
    L_0x0087:
        r0 = r15.thumbURL;	 Catch:{ Exception -> 0x01fe }
        r17 = r0;
        if (r17 == 0) goto L_0x0097;
    L_0x008d:
        r0 = r15.thumbURL;	 Catch:{ Exception -> 0x01fe }
        r17 = r0;
        r17 = r17.length();	 Catch:{ Exception -> 0x01fe }
        if (r17 != 0) goto L_0x0099;
    L_0x0097:
        r15.thumbURL = r9;	 Catch:{ Exception -> 0x01fe }
    L_0x0099:
        r13.add(r15);	 Catch:{ Exception -> 0x01fe }
    L_0x009c:
        r17 = "response";
        r0 = r23;
        r1 = r17;
        r17 = r0.getJSONObject(r1);	 Catch:{ Exception -> 0x01fe }
        r18 = "user_photos";
        r17 = r17.has(r18);	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x010e;
    L_0x00ae:
        r17 = "response";
        r0 = r23;
        r1 = r17;
        r17 = r0.getJSONObject(r1);	 Catch:{ Exception -> 0x01fe }
        r18 = "user_photos";
        r6 = r17.getJSONObject(r18);	 Catch:{ Exception -> 0x01fe }
        r15 = new com.vkontakte.android.api.MultiThumbPhotoAlbum;	 Catch:{ Exception -> 0x01fe }
        r15.<init>(r6);	 Catch:{ Exception -> 0x01fe }
        r17 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x01fe }
        r17 = r17.getResources();	 Catch:{ Exception -> 0x01fe }
        r18 = 2131230912; // 0x7f0800c0 float:1.807789E38 double:1.052967977E-314;
        r19 = 1;
        r0 = r19;
        r0 = new java.lang.Object[r0];	 Catch:{ Exception -> 0x01fe }
        r19 = r0;
        r20 = 0;
        r0 = r15.title;	 Catch:{ Exception -> 0x01fe }
        r21 = r0;
        r19[r20] = r21;	 Catch:{ Exception -> 0x01fe }
        r17 = r17.getString(r18, r19);	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        r15.title = r0;	 Catch:{ Exception -> 0x01fe }
        r17 = "sizes";
        r0 = r17;
        r12 = r6.optJSONArray(r0);	 Catch:{ Exception -> 0x01fe }
        r9 = "";
        if (r12 == 0) goto L_0x00f9;
    L_0x00f0:
        r5 = 0;
    L_0x00f1:
        r17 = r12.length();	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        if (r5 < r0) goto L_0x0275;
    L_0x00f9:
        r0 = r15.thumbURL;	 Catch:{ Exception -> 0x01fe }
        r17 = r0;
        if (r17 == 0) goto L_0x0109;
    L_0x00ff:
        r0 = r15.thumbURL;	 Catch:{ Exception -> 0x01fe }
        r17 = r0;
        r17 = r17.length();	 Catch:{ Exception -> 0x01fe }
        if (r17 != 0) goto L_0x010b;
    L_0x0109:
        r15.thumbURL = r9;	 Catch:{ Exception -> 0x01fe }
    L_0x010b:
        r13.add(r15);	 Catch:{ Exception -> 0x01fe }
    L_0x010e:
        r17 = 2;
        r0 = r17;
        r0 = new java.lang.Object[r0];	 Catch:{ Exception -> 0x01fe }
        r17 = r0;
        r18 = 0;
        r17[r18] = r10;	 Catch:{ Exception -> 0x01fe }
        r18 = 1;
        r17[r18] = r13;	 Catch:{ Exception -> 0x01fe }
    L_0x011e:
        return r17;
    L_0x011f:
        r6 = r3.getJSONObject(r4);	 Catch:{ Exception -> 0x01fe }
        r17 = "size";
        r0 = r17;
        r17 = r6.has(r0);	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x0141;
    L_0x012d:
        r17 = "size";
        r18 = -1;
        r0 = r17;
        r1 = r18;
        r17 = r6.optInt(r0, r1);	 Catch:{ Exception -> 0x01fe }
        r18 = -1;
        r0 = r17;
        r1 = r18;
        if (r0 != r1) goto L_0x0145;
    L_0x0141:
        r4 = r4 + 1;
        goto L_0x002d;
    L_0x0145:
        r2 = new com.vkontakte.android.api.PhotoAlbum;	 Catch:{ Exception -> 0x01fe }
        r2.<init>(r6);	 Catch:{ Exception -> 0x01fe }
        r0 = r2.title;	 Catch:{ Exception -> 0x01fe }
        r17 = r0;
        r17 = r17.length();	 Catch:{ Exception -> 0x01fe }
        if (r17 != 0) goto L_0x0165;
    L_0x0154:
        r17 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x01fe }
        r17 = r17.getResources();	 Catch:{ Exception -> 0x01fe }
        r18 = 2131231051; // 0x7f08014b float:1.8078172E38 double:1.0529680456E-314;
        r17 = r17.getString(r18);	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        r2.title = r0;	 Catch:{ Exception -> 0x01fe }
    L_0x0165:
        r17 = "sizes";
        r0 = r17;
        r12 = r6.optJSONArray(r0);	 Catch:{ Exception -> 0x01fe }
        if (r12 == 0) goto L_0x018d;
    L_0x016f:
        r14 = new java.util.HashMap;	 Catch:{ Exception -> 0x01fe }
        r14.<init>();	 Catch:{ Exception -> 0x01fe }
        r5 = 0;
    L_0x0175:
        r17 = r12.length();	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        if (r5 < r0) goto L_0x01b9;
    L_0x017d:
        r17 = r14.containsKey(r7);	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x01d7;
    L_0x0183:
        r17 = r14.get(r7);	 Catch:{ Exception -> 0x01fe }
        r17 = (java.lang.String) r17;	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        r2.thumbURL = r0;	 Catch:{ Exception -> 0x01fe }
    L_0x018d:
        r17 = "can_upload";
        r0 = r17;
        r17 = r6.has(r0);	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x01ad;
    L_0x0197:
        r17 = "can_upload";
        r0 = r17;
        r17 = r6.getInt(r0);	 Catch:{ Exception -> 0x01fe }
        r18 = 1;
        r0 = r17;
        r1 = r18;
        if (r0 != r1) goto L_0x020f;
    L_0x01a7:
        r17 = 1;
    L_0x01a9:
        r0 = r17;
        r2.canUpload = r0;	 Catch:{ Exception -> 0x01fe }
    L_0x01ad:
        r0 = r2.id;	 Catch:{ Exception -> 0x01fe }
        r17 = r0;
        if (r17 >= 0) goto L_0x0212;
    L_0x01b3:
        r10.add(r8, r2);	 Catch:{ Exception -> 0x01fe }
        r8 = r8 + 1;
        goto L_0x0141;
    L_0x01b9:
        r11 = r12.getJSONObject(r5);	 Catch:{ Exception -> 0x01fe }
        r17 = "type";
        r0 = r17;
        r17 = r11.getString(r0);	 Catch:{ Exception -> 0x01fe }
        r18 = "src";
        r0 = r18;
        r18 = r11.getString(r0);	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        r1 = r18;
        r14.put(r0, r1);	 Catch:{ Exception -> 0x01fe }
        r5 = r5 + 1;
        goto L_0x0175;
    L_0x01d7:
        r17 = com.vkontakte.android.Global.displayDensity;	 Catch:{ Exception -> 0x01fe }
        r18 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 < 0) goto L_0x020c;
    L_0x01df:
        r17 = com.vkontakte.android.NetworkStateReceiver.isHighSpeed();	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x020c;
    L_0x01e5:
        r17 = "x";
        r0 = r17;
        r17 = r14.containsKey(r0);	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x020c;
    L_0x01ef:
        r17 = "x";
    L_0x01f1:
        r0 = r17;
        r17 = r14.get(r0);	 Catch:{ Exception -> 0x01fe }
        r17 = (java.lang.String) r17;	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        r2.thumbURL = r0;	 Catch:{ Exception -> 0x01fe }
        goto L_0x018d;
    L_0x01fe:
        r16 = move-exception;
        r17 = "vk";
        r0 = r17;
        r1 = r16;
        com.vkontakte.android.Log.m532w(r0, r1);
        r17 = 0;
        goto L_0x011e;
    L_0x020c:
        r17 = "m";
        goto L_0x01f1;
    L_0x020f:
        r17 = 0;
        goto L_0x01a9;
    L_0x0212:
        r10.add(r2);	 Catch:{ Exception -> 0x01fe }
        goto L_0x0141;
    L_0x0217:
        r11 = r12.getJSONObject(r5);	 Catch:{ Exception -> 0x01fe }
        r17 = "type";
        r0 = r17;
        r17 = r11.getString(r0);	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        r17 = r7.equals(r0);	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x0239;
    L_0x022b:
        r17 = "src";
        r0 = r17;
        r17 = r11.getString(r0);	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        r15.thumbURL = r0;	 Catch:{ Exception -> 0x01fe }
        goto L_0x0087;
    L_0x0239:
        r17 = "m";
        r18 = "type";
        r0 = r18;
        r18 = r11.getString(r0);	 Catch:{ Exception -> 0x01fe }
        r17 = r17.equals(r18);	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x0251;
    L_0x0249:
        r17 = com.vkontakte.android.Global.displayDensity;	 Catch:{ Exception -> 0x01fe }
        r18 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 < 0) goto L_0x0269;
    L_0x0251:
        r17 = "x";
        r18 = "type";
        r0 = r18;
        r18 = r11.getString(r0);	 Catch:{ Exception -> 0x01fe }
        r17 = r17.equals(r18);	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x0271;
    L_0x0261:
        r17 = com.vkontakte.android.Global.displayDensity;	 Catch:{ Exception -> 0x01fe }
        r18 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 > 0) goto L_0x0271;
    L_0x0269:
        r17 = "src";
        r0 = r17;
        r9 = r11.getString(r0);	 Catch:{ Exception -> 0x01fe }
    L_0x0271:
        r5 = r5 + 1;
        goto L_0x007f;
    L_0x0275:
        r11 = r12.getJSONObject(r5);	 Catch:{ Exception -> 0x01fe }
        r17 = "type";
        r0 = r17;
        r17 = r11.getString(r0);	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        r17 = r7.equals(r0);	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x0297;
    L_0x0289:
        r17 = "src";
        r0 = r17;
        r17 = r11.getString(r0);	 Catch:{ Exception -> 0x01fe }
        r0 = r17;
        r15.thumbURL = r0;	 Catch:{ Exception -> 0x01fe }
        goto L_0x00f9;
    L_0x0297:
        r17 = "m";
        r18 = "type";
        r0 = r18;
        r18 = r11.getString(r0);	 Catch:{ Exception -> 0x01fe }
        r17 = r17.equals(r18);	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x02af;
    L_0x02a7:
        r17 = com.vkontakte.android.Global.displayDensity;	 Catch:{ Exception -> 0x01fe }
        r18 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 < 0) goto L_0x02c7;
    L_0x02af:
        r17 = "x";
        r18 = "type";
        r0 = r18;
        r18 = r11.getString(r0);	 Catch:{ Exception -> 0x01fe }
        r17 = r17.equals(r18);	 Catch:{ Exception -> 0x01fe }
        if (r17 == 0) goto L_0x02cf;
    L_0x02bf:
        r17 = com.vkontakte.android.Global.displayDensity;	 Catch:{ Exception -> 0x01fe }
        r18 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r17 = (r17 > r18 ? 1 : (r17 == r18 ? 0 : -1));
        if (r17 > 0) goto L_0x02cf;
    L_0x02c7:
        r17 = "src";
        r0 = r17;
        r9 = r11.getString(r0);	 Catch:{ Exception -> 0x01fe }
    L_0x02cf:
        r5 = r5 + 1;
        goto L_0x00f1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.api.PhotosGetAlbums.parse(org.json.JSONObject):java.lang.Object");
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((ArrayList) r[0], (ArrayList) r[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
