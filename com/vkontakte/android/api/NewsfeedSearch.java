package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.NewsEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class NewsfeedSearch extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<NewsEntry> arrayList, String str);
    }

    /* renamed from: com.vkontakte.android.api.NewsfeedSearch.1 */
    class C14071 extends APIHandler {
        C14071() {
        }

        public void success(JSONObject o) {
            NewsfeedSearch.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (NewsfeedSearch.this.callback != null) {
                NewsfeedSearch.this.callback.fail(ecode, emsg);
            }
        }
    }

    public NewsfeedSearch(String q, String from, int count) {
        super("newsfeed.search");
        param("q", q);
        param("count", count).param("start_from", from);
        param("extended", 1).param("photo_sizes", 1);
        handler(new C14071());
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray items = o.getJSONObject("response").optJSONArray("items");
            JSONArray profiles = o.getJSONObject("response").optJSONArray("profiles");
            JSONArray groups = o.getJSONObject("response").optJSONArray("groups");
            Vector<Integer> adminGroups = new Vector();
            ArrayList<NewsEntry> result = new ArrayList();
            if (items == null) {
                return new Object[]{result};
            }
            int i;
            HashMap<Integer, String> names = new HashMap();
            HashMap<Integer, String> photos = new HashMap();
            HashMap<Integer, Boolean> f = new HashMap();
            if (profiles != null) {
                for (i = 0; i < profiles.length(); i++) {
                    int uid = profiles.getJSONObject(i).getInt("id");
                    names.put(Integer.valueOf(uid), new StringBuilder(String.valueOf(profiles.getJSONObject(i).getString("first_name"))).append(" ").append(profiles.getJSONObject(i).getString("last_name")).toString());
                    photos.put(Integer.valueOf(uid), profiles.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50"));
                    f.put(Integer.valueOf(uid), Boolean.valueOf(profiles.getJSONObject(i).getInt("sex") == 1));
                }
            }
            if (groups != null) {
                for (i = 0; i < groups.length(); i++) {
                    int gid = groups.getJSONObject(i).getInt("id");
                    names.put(Integer.valueOf(-gid), groups.getJSONObject(i).getString("name"));
                    photos.put(Integer.valueOf(-gid), groups.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50"));
                    if (groups.getJSONObject(i).optInt("is_admin", 0) == 1) {
                        adminGroups.add(Integer.valueOf(gid));
                    }
                }
            }
            for (i = 0; i < items.length(); i++) {
                NewsEntry e = new NewsEntry(items.getJSONObject(i), names, photos, f);
                if (e.ownerID < 0 && adminGroups.contains(Integer.valueOf(-e.ownerID))) {
                    e.flags |= 64;
                }
                result.add(e);
            }
            Object obj = new Object[2];
            obj[0] = result;
            obj[1] = o.getJSONObject("response").getString("next_from");
            return obj;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] a = (Object[]) result;
            this.callback.success((ArrayList) a[0], (String) a[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
