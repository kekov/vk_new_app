package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import com.vkontakte.android.PollAttachment;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class PollsCreate extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(PollAttachment pollAttachment);
    }

    public PollsCreate(String question, List<String> options, int oid, boolean anonym) {
        super("polls.create");
        param("question", question);
        param("is_anonymous", anonym ? 1 : 0);
        param("owner_id", oid);
        param("add_answers", new JSONArray(options).toString());
    }

    public Object parse(JSONObject o) {
        try {
            JSONObject r = o.getJSONObject("response");
            return new PollAttachment(r.getString("question"), r.getInt("owner_id"), r.getInt("id"));
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((PollAttachment) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
