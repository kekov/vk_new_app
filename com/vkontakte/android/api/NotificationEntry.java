package com.vkontakte.android.api;

import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.UserProfile;
import java.util.Vector;

public class NotificationEntry {
    public static final int A_FOLLOW = 3;
    public static final int A_FRIEND_ACCEPTED = 6;
    public static final int A_LIKE = 1;
    public static final int A_MENTION = 4;
    public static final int A_RETWEET = 2;
    public static final int A_WALL = 5;
    public static final int F_COMMENT = 3;
    public static final int F_COPY = 4;
    public static final int F_GROUPED = 6;
    public static final int F_POST = 2;
    public static final int F_USER = 1;
    public static final int F_VIEWED_TO_HERE = 5;
    public static final int P_COMMENT = 4;
    public static final int P_PHOTO = 2;
    public static final int P_POST = 1;
    public static final int P_TOPIC = 5;
    public static final int P_VIDEO = 3;
    public int action;
    public int commentID;
    public UserProfile commentUser;
    public boolean displayAsEntry;
    public CharSequence displayableText;
    public Object extra;
    public int feedbackType;
    public boolean isLiked;
    public String nameDat;
    public int numLikes;
    public NewsEntry parent;
    public int parentType;
    public NewsEntry ppost;
    public String reply;
    public int replyID;
    public int replyTime;
    public String text;
    public int time;
    public Vector<UserProfile> users;

    private class URLSpanNoUnderline extends URLSpan {
        public URLSpanNoUnderline(String url) {
            super(url);
        }

        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            ds.setColor(-13936518);
        }
    }

    public NotificationEntry() {
        this.reply = null;
        this.replyTime = -1;
        this.replyID = -1;
        this.users = new Vector();
    }

    private Spannable stripUnderlines(Spannable s) {
        URLSpan[] spans = (URLSpan[]) s.getSpans(0, s.length(), URLSpan.class);
        int length = spans.length;
        for (int i = 0; i < length; i += P_POST) {
            URLSpan span = spans[i];
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            s.setSpan(new URLSpanNoUnderline(span.getURL()), start, end, 0);
        }
        return s;
    }

    public void setText(String t) {
        this.text = t;
        try {
            this.displayableText = Global.replaceEmoji(stripUnderlines((Spannable) Html.fromHtml(t)));
        } catch (Throwable x) {
            Log.m532w("vk", x);
            this.displayableText = t;
        }
    }
}
