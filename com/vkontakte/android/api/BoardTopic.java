package com.vkontakte.android.api;

import com.vkontakte.android.UserProfile;

public class BoardTopic {
    public static final int TOPIC_CLOSED = 1;
    public static final int TOPIC_FIXED = 2;
    public int created;
    public int creator;
    public int flags;
    public int gid;
    public int id;
    public String lastComment;
    public int lastCommentUid;
    public int numComments;
    public String title;
    public int updated;
    public UserProfile updatedBy;
}
