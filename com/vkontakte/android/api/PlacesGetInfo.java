package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.GeoPlace;
import com.vkontakte.android.Log;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class PlacesGetInfo extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(GeoPlace geoPlace, ArrayList<String> arrayList, String str, String str2);
    }

    public PlacesGetInfo(int placeID) {
        super("execute.getPlaceInfo");
        param("place_id", placeID);
    }

    public Object parse(JSONObject o) {
        try {
            String string;
            JSONObject r = o.getJSONObject("response");
            GeoPlace place = new GeoPlace(r.getJSONObject("place"));
            ArrayList<String> photos = new ArrayList();
            JSONArray arr = r.getJSONArray("user_photos");
            for (int i = 0; i < arr.length(); i++) {
                photos.add(arr.getString(i));
            }
            JSONObject g = r.optJSONObject("group");
            Object obj = new Object[4];
            obj[0] = place;
            obj[1] = photos;
            if (g != null) {
                string = g.getString("status");
            } else {
                string = null;
            }
            obj[2] = string;
            obj[3] = g != null ? g.getString("photo_100") : null;
            return obj;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((GeoPlace) r[0], (ArrayList) r[1], (String) r[2], (String) r[3]);
        }
    }
}
