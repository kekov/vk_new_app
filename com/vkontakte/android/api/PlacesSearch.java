package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.GeoPlace;
import com.vkontakte.android.Log;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class PlacesSearch extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<GeoPlace> arrayList);
    }

    public PlacesSearch(double lat, double lon, int radius, String q) {
        super("places.search");
        param("latitude", new StringBuilder(String.valueOf(lat)).toString());
        param("longitude", new StringBuilder(String.valueOf(lon)).toString());
        param("radius", radius);
        if (q != null && q.length() > 0) {
            param("q", q);
        }
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray a = APIUtils.unwrapArray(o, "response").array;
            Object places = new ArrayList();
            for (int i = 0; i < a.length(); i++) {
                places.add(new GeoPlace(a.getJSONObject(i)));
            }
            return places;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }
}
