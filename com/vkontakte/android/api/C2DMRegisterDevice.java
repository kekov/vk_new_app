package com.vkontakte.android.api;

import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import com.facebook.WebDialog;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.VKApplication;
import org.json.JSONObject;

public class C2DMRegisterDevice extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    /* renamed from: com.vkontakte.android.api.C2DMRegisterDevice.1 */
    class C14001 extends APIHandler {
        C14001() {
        }

        public void success(JSONObject o) {
            C2DMRegisterDevice.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (C2DMRegisterDevice.this.callback != null) {
                C2DMRegisterDevice.this.callback.fail(ecode, emsg);
            }
        }
    }

    public C2DMRegisterDevice(String token) {
        super("account.registerDevice");
        param("token", token);
        param("system_version", VERSION.RELEASE);
        param("device_model", new StringBuilder(String.valueOf(Secure.getString(VKApplication.context.getContentResolver(), "android_id"))).append(" ").append(Build.MODEL).toString());
        param(WebDialog.DIALOG_PARAM_TYPE, 4);
        param(GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE, 1);
        param("subscribe", "msg,friend,call,reply,mention,group");
        try {
            param("app_version", VKApplication.context.getPackageManager().getPackageInfo(VKApplication.context.getPackageName(), 0).versionCode);
        } catch (Exception e) {
        }
        handler(new C14001());
    }

    public Object parse(JSONObject o) {
        try {
            return Boolean.valueOf(true);
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
