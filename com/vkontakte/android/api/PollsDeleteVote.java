package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class PollsDeleteVote extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(boolean z);
    }

    public PollsDeleteVote(int ownerID, int pollID, int optID, boolean board) {
        super("polls.deleteVote");
        param("owner_id", ownerID).param("poll_id", pollID).param("answer_id", optID);
        if (board) {
            param("board", 1);
        }
    }

    public Object parse(JSONObject o) {
        try {
            return o.getInt("response") == 1 ? Boolean.valueOf(true) : Boolean.valueOf(false);
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success(((Boolean) result).booleanValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
