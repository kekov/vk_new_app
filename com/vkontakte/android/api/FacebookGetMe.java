package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import org.json.JSONObject;
import org.json.JSONTokener;

public class FacebookGetMe extends APIRequest {
    Callback callback;
    private String token;

    public interface Callback {
        void fail(int i, String str);

        void success(String str);
    }

    public FacebookGetMe(String accessToken) {
        super("__facebookGetMe");
        this.token = accessToken;
    }

    public JSONObject doExec() {
        try {
            return (JSONObject) new JSONTokener(new String(Global.getURL("https://graph.facebook.com/me?fields=id&access_token=" + this.token), "UTF-8")).nextValue();
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public Object parse(JSONObject o) {
        try {
            return o.getString("id");
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((String) result);
        }
    }
}
