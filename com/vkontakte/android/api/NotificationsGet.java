package com.vkontakte.android.api;

import android.text.TextUtils;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.VKApplication;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import org.json.JSONObject;
import org.json.JSONTokener;

public class NotificationsGet extends APIRequest {
    Callback callback;
    private boolean fromCache;
    private boolean updateCache;

    public interface Callback {
        void fail(int i, String str);

        void success(List<NotificationEntry> list, int i, int i2, String str);
    }

    public NotificationsGet(int offset, String from, int count, boolean fromCache) {
        boolean z = true;
        super("execute.getNotifications");
        param("offset", offset).param("count", count).param("from", from);
        param("filters", TextUtils.join(",", new String[]{VKApplication.context.getSharedPreferences(null, 0).getString("notifications_filter", "wall,mentions,comments,likes,reposts,followers,friends").replace('|', ',')}));
        if (VKApplication.context.getSharedPreferences(null, 0).getString("notifications_filter", "wall,mentions,comments,likes,reposts,followers,friends").length() == 0) {
            param("filters", "_none");
        }
        param("photo_sizes", 1);
        if (fromCache || offset != 0) {
            z = false;
        }
        this.updateCache = z;
        this.fromCache = fromCache;
    }

    protected JSONObject doExec() {
        if (this.fromCache) {
            try {
                File f = new File(VKApplication.context.getCacheDir(), "replies");
                if (!f.exists()) {
                    return null;
                }
                FileInputStream s = new FileInputStream(f);
                byte[] d = new byte[((int) f.length())];
                s.read(d);
                s.close();
                JSONObject o = (JSONObject) new JSONTokener(new String(d, "UTF-8")).nextValue();
                if (((String) this.params.get("v")).equals(o.optString("v"))) {
                    return o;
                }
                return null;
            } catch (Exception e) {
            }
        }
        return null;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object parse(org.json.JSONObject r39) {
        /*
        r38 = this;
        r0 = r38;
        r0 = r0.updateCache;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        if (r34 == 0) goto L_0x0057;
    L_0x0008:
        r34 = "v";
        r0 = r38;
        r0 = r0.params;	 Catch:{ Exception -> 0x0bf8 }
        r35 = r0;
        r36 = "v";
        r35 = r35.get(r36);	 Catch:{ Exception -> 0x0bf8 }
        r0 = r39;
        r1 = r34;
        r2 = r35;
        r0.put(r1, r2);	 Catch:{ Exception -> 0x0bf8 }
        r9 = new java.io.File;	 Catch:{ Exception -> 0x0bf8 }
        r34 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x0bf8 }
        r34 = r34.getCacheDir();	 Catch:{ Exception -> 0x0bf8 }
        r35 = "replies";
        r0 = r34;
        r1 = r35;
        r9.<init>(r0, r1);	 Catch:{ Exception -> 0x0bf8 }
        r34 = r9.exists();	 Catch:{ Exception -> 0x0bf8 }
        if (r34 == 0) goto L_0x0039;
    L_0x0036:
        r9.delete();	 Catch:{ Exception -> 0x0bf8 }
    L_0x0039:
        r9.createNewFile();	 Catch:{ Exception -> 0x0bf8 }
        r20 = new java.io.FileOutputStream;	 Catch:{ Exception -> 0x0bf8 }
        r0 = r20;
        r0.<init>(r9);	 Catch:{ Exception -> 0x0bf8 }
        r34 = r39.toString();	 Catch:{ Exception -> 0x0bf8 }
        r35 = "UTF-8";
        r34 = r34.getBytes(r35);	 Catch:{ Exception -> 0x0bf8 }
        r0 = r20;
        r1 = r34;
        r0.write(r1);	 Catch:{ Exception -> 0x0bf8 }
        r20.close();	 Catch:{ Exception -> 0x0bf8 }
    L_0x0057:
        r34 = "response";
        r0 = r39;
        r1 = r34;
        r34 = r0.getJSONObject(r1);	 Catch:{ Exception -> 0x0159 }
        r35 = "items";
        r15 = r34.optJSONArray(r35);	 Catch:{ Exception -> 0x0159 }
        r34 = "response";
        r0 = r39;
        r1 = r34;
        r34 = r0.getJSONObject(r1);	 Catch:{ Exception -> 0x0159 }
        r35 = "profiles";
        r24 = r34.optJSONArray(r35);	 Catch:{ Exception -> 0x0159 }
        r34 = "response";
        r0 = r39;
        r1 = r34;
        r34 = r0.getJSONObject(r1);	 Catch:{ Exception -> 0x0159 }
        r35 = "groups";
        r12 = r34.optJSONArray(r35);	 Catch:{ Exception -> 0x0159 }
        r34 = "response";
        r0 = r39;
        r1 = r34;
        r34 = r0.getJSONObject(r1);	 Catch:{ Exception -> 0x0159 }
        r35 = "last_viewed";
        r17 = r34.optInt(r35);	 Catch:{ Exception -> 0x0159 }
        r4 = new java.util.ArrayList;	 Catch:{ Exception -> 0x0159 }
        r4.<init>();	 Catch:{ Exception -> 0x0159 }
        r25 = new java.util.ArrayList;	 Catch:{ Exception -> 0x0159 }
        r25.<init>();	 Catch:{ Exception -> 0x0159 }
        if (r15 != 0) goto L_0x00ba;
    L_0x00a3:
        r34 = 2;
        r0 = r34;
        r0 = new java.lang.Object[r0];	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r35 = 0;
        r34[r35] = r25;	 Catch:{ Exception -> 0x0159 }
        r35 = 1;
        r36 = 0;
        r36 = java.lang.Integer.valueOf(r36);	 Catch:{ Exception -> 0x0159 }
        r34[r35] = r36;	 Catch:{ Exception -> 0x0159 }
    L_0x00b9:
        return r34;
    L_0x00ba:
        r32 = new java.util.HashMap;	 Catch:{ Exception -> 0x0159 }
        r32.<init>();	 Catch:{ Exception -> 0x0159 }
        r19 = new java.util.HashMap;	 Catch:{ Exception -> 0x0159 }
        r19.<init>();	 Catch:{ Exception -> 0x0159 }
        r22 = new java.util.HashMap;	 Catch:{ Exception -> 0x0159 }
        r22.<init>();	 Catch:{ Exception -> 0x0159 }
        if (r24 == 0) goto L_0x00d4;
    L_0x00cb:
        r13 = 0;
    L_0x00cc:
        r34 = r24.length();	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        if (r13 < r0) goto L_0x0189;
    L_0x00d4:
        if (r12 == 0) goto L_0x00df;
    L_0x00d6:
        r13 = 0;
    L_0x00d7:
        r34 = r12.length();	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        if (r13 < r0) goto L_0x02a3;
    L_0x00df:
        r13 = 0;
    L_0x00e0:
        r34 = r15.length();	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        if (r13 < r0) goto L_0x035e;
    L_0x00e8:
        r13 = 1;
    L_0x00e9:
        r34 = r25.size();	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        if (r13 < r0) goto L_0x0a87;
    L_0x00f1:
        r28 = new java.util.ArrayList;	 Catch:{ Exception -> 0x0159 }
        r28.<init>();	 Catch:{ Exception -> 0x0159 }
        r5 = new java.util.ArrayList;	 Catch:{ Exception -> 0x0159 }
        r5.<init>();	 Catch:{ Exception -> 0x0159 }
        r7 = 0;
        r13 = 0;
    L_0x00fd:
        r34 = r25.size();	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        if (r13 < r0) goto L_0x0ac9;
    L_0x0105:
        r25 = r28;
        r34 = 4;
        r0 = r34;
        r0 = new java.lang.Object[r0];	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r35 = 0;
        r34[r35] = r25;	 Catch:{ Exception -> 0x0159 }
        r35 = 1;
        r36 = "response";
        r0 = r39;
        r1 = r36;
        r36 = r0.getJSONObject(r1);	 Catch:{ Exception -> 0x0159 }
        r37 = "count";
        r36 = r36.getInt(r37);	 Catch:{ Exception -> 0x0159 }
        r36 = java.lang.Integer.valueOf(r36);	 Catch:{ Exception -> 0x0159 }
        r34[r35] = r36;	 Catch:{ Exception -> 0x0159 }
        r35 = 2;
        r36 = "response";
        r0 = r39;
        r1 = r36;
        r36 = r0.getJSONObject(r1);	 Catch:{ Exception -> 0x0159 }
        r37 = "new_offset";
        r36 = r36.getInt(r37);	 Catch:{ Exception -> 0x0159 }
        r36 = java.lang.Integer.valueOf(r36);	 Catch:{ Exception -> 0x0159 }
        r34[r35] = r36;	 Catch:{ Exception -> 0x0159 }
        r35 = 3;
        r36 = "response";
        r0 = r39;
        r1 = r36;
        r36 = r0.getJSONObject(r1);	 Catch:{ Exception -> 0x0159 }
        r37 = "new_from";
        r36 = r36.getString(r37);	 Catch:{ Exception -> 0x0159 }
        r34[r35] = r36;	 Catch:{ Exception -> 0x0159 }
        goto L_0x00b9;
    L_0x0159:
        r33 = move-exception;
        r34 = "vk";
        r0 = r34;
        r1 = r33;
        com.vkontakte.android.Log.m532w(r0, r1);
        r0 = r38;
        r0 = r0.fromCache;
        r34 = r0;
        if (r34 == 0) goto L_0x0bed;
    L_0x016b:
        r34 = 0;
        r0 = r34;
        r1 = r38;
        r1.fromCache = r0;
        r34 = 1;
        r0 = r34;
        r1 = r38;
        r1.updateCache = r0;
        r16 = r38.doExec();
        r0 = r38;
        r1 = r16;
        r34 = r0.parse(r1);
        goto L_0x00b9;
    L_0x0189:
        r0 = r24;
        r34 = r0.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r35 = "id";
        r31 = r34.getInt(r35);	 Catch:{ Exception -> 0x0159 }
        r34 = java.lang.Integer.valueOf(r31);	 Catch:{ Exception -> 0x0159 }
        r35 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0159 }
        r0 = r24;
        r36 = r0.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r37 = "first_name";
        r36 = r36.getString(r37);	 Catch:{ Exception -> 0x0159 }
        r36 = java.lang.String.valueOf(r36);	 Catch:{ Exception -> 0x0159 }
        r35.<init>(r36);	 Catch:{ Exception -> 0x0159 }
        r36 = " ";
        r35 = r35.append(r36);	 Catch:{ Exception -> 0x0159 }
        r0 = r24;
        r36 = r0.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r37 = "last_name";
        r36 = r36.getString(r37);	 Catch:{ Exception -> 0x0159 }
        r35 = r35.append(r36);	 Catch:{ Exception -> 0x0159 }
        r35 = r35.toString();	 Catch:{ Exception -> 0x0159 }
        r0 = r19;
        r1 = r34;
        r2 = r35;
        r0.put(r1, r2);	 Catch:{ Exception -> 0x0159 }
        r35 = java.lang.Integer.valueOf(r31);	 Catch:{ Exception -> 0x0159 }
        r0 = r24;
        r36 = r0.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r34 = com.vkontakte.android.Global.displayDensity;	 Catch:{ Exception -> 0x0159 }
        r37 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r34 = (r34 > r37 ? 1 : (r34 == r37 ? 0 : -1));
        if (r34 <= 0) goto L_0x0299;
    L_0x01e3:
        r34 = "photo_100";
    L_0x01e5:
        r0 = r36;
        r1 = r34;
        r34 = r0.getString(r1);	 Catch:{ Exception -> 0x0159 }
        r0 = r22;
        r1 = r35;
        r2 = r34;
        r0.put(r1, r2);	 Catch:{ Exception -> 0x0159 }
        r21 = new com.vkontakte.android.UserProfile;	 Catch:{ Exception -> 0x0159 }
        r21.<init>();	 Catch:{ Exception -> 0x0159 }
        r0 = r31;
        r1 = r21;
        r1.uid = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r24;
        r34 = r0.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r35 = "first_name";
        r34 = r34.getString(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r21;
        r1.firstName = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r24;
        r34 = r0.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r35 = "last_name";
        r34 = r34.getString(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r21;
        r1.lastName = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0159 }
        r0 = r21;
        r0 = r0.firstName;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r35 = java.lang.String.valueOf(r35);	 Catch:{ Exception -> 0x0159 }
        r34.<init>(r35);	 Catch:{ Exception -> 0x0159 }
        r35 = " ";
        r34 = r34.append(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = r21;
        r0 = r0.lastName;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r34 = r34.append(r35);	 Catch:{ Exception -> 0x0159 }
        r34 = r34.toString();	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r21;
        r1.fullName = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r24;
        r34 = r0.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r35 = "sex";
        r34 = r34.getInt(r35);	 Catch:{ Exception -> 0x0159 }
        r35 = 1;
        r0 = r34;
        r1 = r35;
        if (r0 != r1) goto L_0x029d;
    L_0x0262:
        r34 = 1;
    L_0x0264:
        r0 = r34;
        r1 = r21;
        r1.f151f = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r24;
        r35 = r0.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r34 = com.vkontakte.android.Global.displayDensity;	 Catch:{ Exception -> 0x0159 }
        r36 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r34 = (r34 > r36 ? 1 : (r34 == r36 ? 0 : -1));
        if (r34 <= 0) goto L_0x02a0;
    L_0x0278:
        r34 = "photo_100";
    L_0x027a:
        r0 = r35;
        r1 = r34;
        r34 = r0.getString(r1);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r21;
        r1.photo = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = java.lang.Integer.valueOf(r31);	 Catch:{ Exception -> 0x0159 }
        r0 = r32;
        r1 = r34;
        r2 = r21;
        r0.put(r1, r2);	 Catch:{ Exception -> 0x0159 }
        r13 = r13 + 1;
        goto L_0x00cc;
    L_0x0299:
        r34 = "photo_50";
        goto L_0x01e5;
    L_0x029d:
        r34 = 0;
        goto L_0x0264;
    L_0x02a0:
        r34 = "photo_50";
        goto L_0x027a;
    L_0x02a3:
        r34 = r12.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r35 = "id";
        r11 = r34.getInt(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = -r11;
        r34 = r0;
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r35 = r12.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r36 = "name";
        r35 = r35.getString(r36);	 Catch:{ Exception -> 0x0159 }
        r0 = r19;
        r1 = r34;
        r2 = r35;
        r0.put(r1, r2);	 Catch:{ Exception -> 0x0159 }
        r0 = -r11;
        r34 = r0;
        r35 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r36 = r12.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r34 = com.vkontakte.android.Global.displayDensity;	 Catch:{ Exception -> 0x0159 }
        r37 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r34 = (r34 > r37 ? 1 : (r34 == r37 ? 0 : -1));
        if (r34 <= 0) goto L_0x0358;
    L_0x02da:
        r34 = "photo_100";
    L_0x02dc:
        r0 = r36;
        r1 = r34;
        r34 = r0.getString(r1);	 Catch:{ Exception -> 0x0159 }
        r0 = r22;
        r1 = r35;
        r2 = r34;
        r0.put(r1, r2);	 Catch:{ Exception -> 0x0159 }
        r34 = r12.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r35 = "is_admin";
        r36 = 0;
        r34 = r34.optInt(r35, r36);	 Catch:{ Exception -> 0x0159 }
        r35 = 1;
        r0 = r34;
        r1 = r35;
        if (r0 != r1) goto L_0x030a;
    L_0x0301:
        r34 = java.lang.Integer.valueOf(r11);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r4.add(r0);	 Catch:{ Exception -> 0x0159 }
    L_0x030a:
        r21 = new com.vkontakte.android.UserProfile;	 Catch:{ Exception -> 0x0159 }
        r21.<init>();	 Catch:{ Exception -> 0x0159 }
        r0 = -r11;
        r34 = r0;
        r0 = r34;
        r1 = r21;
        r1.uid = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = r12.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r35 = "name";
        r34 = r34.getString(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r21;
        r1.fullName = r0;	 Catch:{ Exception -> 0x0159 }
        r35 = r12.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r34 = com.vkontakte.android.Global.displayDensity;	 Catch:{ Exception -> 0x0159 }
        r36 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r34 = (r34 > r36 ? 1 : (r34 == r36 ? 0 : -1));
        if (r34 <= 0) goto L_0x035b;
    L_0x0334:
        r34 = "photo_100";
    L_0x0336:
        r0 = r35;
        r1 = r34;
        r34 = r0.getString(r1);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r21;
        r1.photo = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = -r11;
        r34 = r0;
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r32;
        r1 = r34;
        r2 = r21;
        r0.put(r1, r2);	 Catch:{ Exception -> 0x0159 }
        r13 = r13 + 1;
        goto L_0x00d7;
    L_0x0358:
        r34 = "photo_50";
        goto L_0x02dc;
    L_0x035b:
        r34 = "photo_50";
        goto L_0x0336;
    L_0x035e:
        r14 = r15.getJSONObject(r13);	 Catch:{ Exception -> 0x0159 }
        r34 = "type";
        r0 = r34;
        r29 = r14.getString(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = "_";
        r0 = r29;
        r1 = r34;
        r30 = r0.split(r1);	 Catch:{ Exception -> 0x0159 }
        r8 = new com.vkontakte.android.api.NotificationEntry;	 Catch:{ Exception -> 0x0159 }
        r8.<init>();	 Catch:{ Exception -> 0x0159 }
        r34 = com.vkontakte.android.Global.timeDiff;	 Catch:{ Exception -> 0x0159 }
        r35 = "date";
        r0 = r35;
        r35 = r14.getInt(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = r34 + r35;
        r0 = r34;
        r8.time = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 0;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "mention";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x03a2;
    L_0x0395:
        r0 = r30;
        r0 = r0.length;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r35 = 1;
        r0 = r34;
        r1 = r35;
        if (r0 == r1) goto L_0x03ae;
    L_0x03a2:
        r34 = 0;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "wall";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x0419;
    L_0x03ae:
        r34 = new com.vkontakte.android.NewsEntry;	 Catch:{ Exception -> 0x0159 }
        r35 = "feedback";
        r0 = r35;
        r35 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r35;
        r2 = r19;
        r3 = r22;
        r0.<init>(r1, r2, r3);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.parent = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r0 = r8.time;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r0 = r35;
        r1 = r34;
        r1.time = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 0;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "mention";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x03e7;
    L_0x03e1:
        r34 = 4;
        r0 = r34;
        r8.action = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x03e7:
        r34 = 0;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "wall";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x0405;
    L_0x03f3:
        r34 = 5;
        r0 = r34;
        r8.action = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r35 = com.vkontakte.android.Global.uid;	 Catch:{ Exception -> 0x0159 }
        r0 = r35;
        r1 = r34;
        r1.ownerID = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x0405:
        r34 = 2;
        r0 = r34;
        r8.feedbackType = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r35 = 8;
        r34 = r34.flag(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.isLiked = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x0419:
        r34 = 0;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "comment";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 != 0) goto L_0x043d;
    L_0x0425:
        r34 = 0;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "reply";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 != 0) goto L_0x043d;
    L_0x0431:
        r34 = "mention_comments";
        r0 = r34;
        r1 = r29;
        r34 = r0.equals(r1);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x0527;
    L_0x043d:
        r34 = "feedback";
        r0 = r34;
        r10 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = "text";
        r0 = r34;
        r34 = r10.getString(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = com.vkontakte.android.Global.replaceMentions(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.setText(r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r8.displayableText;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        if (r34 == 0) goto L_0x0466;
    L_0x045c:
        r0 = r8.displayableText;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = r34.length();	 Catch:{ Exception -> 0x0159 }
        if (r34 != 0) goto L_0x0478;
    L_0x0466:
        r34 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x0159 }
        r34 = r34.getResources();	 Catch:{ Exception -> 0x0159 }
        r35 = 2131231228; // 0x7f0801fc float:1.8078531E38 double:1.052968133E-314;
        r34 = r34.getString(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.setText(r0);	 Catch:{ Exception -> 0x0159 }
    L_0x0478:
        r34 = "from_id";
        r0 = r34;
        r34 = r10.has(r0);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x08fd;
    L_0x0482:
        r34 = "from_id";
    L_0x0484:
        r0 = r34;
        r34 = r10.getInt(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r32;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (com.vkontakte.android.UserProfile) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.commentUser = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.commentUser;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        if (r34 != 0) goto L_0x04be;
    L_0x04a2:
        r34 = "vk";
        r35 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0159 }
        r36 = "no comment user ";
        r35.<init>(r36);	 Catch:{ Exception -> 0x0159 }
        r36 = "from_id";
        r0 = r36;
        r36 = r10.getInt(r0);	 Catch:{ Exception -> 0x0159 }
        r35 = r35.append(r36);	 Catch:{ Exception -> 0x0159 }
        r35 = r35.toString();	 Catch:{ Exception -> 0x0159 }
        com.vkontakte.android.Log.m526e(r34, r35);	 Catch:{ Exception -> 0x0159 }
    L_0x04be:
        r34 = "id";
        r0 = r34;
        r34 = r10.has(r0);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x0901;
    L_0x04c8:
        r34 = "id";
    L_0x04ca:
        r0 = r34;
        r34 = r10.getInt(r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.commentID = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 3;
        r0 = r34;
        r8.feedbackType = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = "mention_comments";
        r0 = r34;
        r1 = r29;
        r34 = r0.equals(r1);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x0527;
    L_0x04e6:
        r34 = new com.vkontakte.android.NewsEntry;	 Catch:{ Exception -> 0x0159 }
        r35 = "parent";
        r0 = r35;
        r35 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r35;
        r2 = r19;
        r3 = r22;
        r0.<init>(r1, r2, r3);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.parent = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r0 = r35;
        r0 = r0.text;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r36 = "<a href='[^']+'>([^<]+)</a>";
        r37 = "$1";
        r35 = r35.replaceAll(r36, r37);	 Catch:{ Exception -> 0x0159 }
        r0 = r35;
        r1 = r34;
        r1.text = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 1;
        r0 = r34;
        r8.parentType = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 4;
        r0 = r34;
        r8.action = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x0527:
        r34 = 0;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "like";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x0557;
    L_0x0533:
        r34 = "feedback";
        r0 = r34;
        r34 = com.vkontakte.android.api.APIUtils.unwrapArray(r14, r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r10 = r0.array;	 Catch:{ Exception -> 0x0159 }
        r16 = 0;
    L_0x0541:
        r34 = r10.length();	 Catch:{ Exception -> 0x0159 }
        r0 = r16;
        r1 = r34;
        if (r0 < r1) goto L_0x0905;
    L_0x054b:
        r34 = 1;
        r0 = r34;
        r8.feedbackType = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 1;
        r0 = r34;
        r8.action = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x0557:
        r34 = 0;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "copy";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x0587;
    L_0x0563:
        r34 = "feedback";
        r0 = r34;
        r34 = com.vkontakte.android.api.APIUtils.unwrapArray(r14, r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r10 = r0.array;	 Catch:{ Exception -> 0x0159 }
        r16 = 0;
    L_0x0571:
        r34 = r10.length();	 Catch:{ Exception -> 0x0159 }
        r0 = r16;
        r1 = r34;
        if (r0 < r1) goto L_0x0932;
    L_0x057b:
        r34 = 4;
        r0 = r34;
        r8.feedbackType = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 2;
        r0 = r34;
        r8.action = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x0587:
        r34 = 0;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "follow";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x05b7;
    L_0x0593:
        r34 = "feedback";
        r0 = r34;
        r34 = com.vkontakte.android.api.APIUtils.unwrapArray(r14, r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r10 = r0.array;	 Catch:{ Exception -> 0x0159 }
        r16 = 0;
    L_0x05a1:
        r34 = r10.length();	 Catch:{ Exception -> 0x0159 }
        r0 = r16;
        r1 = r34;
        if (r0 < r1) goto L_0x095f;
    L_0x05ab:
        r34 = 1;
        r0 = r34;
        r8.feedbackType = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 3;
        r0 = r34;
        r8.action = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x05b7:
        r34 = "friend_accepted";
        r0 = r29;
        r1 = r34;
        r34 = r0.equals(r1);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x05e7;
    L_0x05c3:
        r34 = "feedback";
        r0 = r34;
        r34 = com.vkontakte.android.api.APIUtils.unwrapArray(r14, r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r10 = r0.array;	 Catch:{ Exception -> 0x0159 }
        r16 = 0;
    L_0x05d1:
        r34 = r10.length();	 Catch:{ Exception -> 0x0159 }
        r0 = r16;
        r1 = r34;
        if (r0 < r1) goto L_0x098c;
    L_0x05db:
        r34 = 1;
        r0 = r34;
        r8.feedbackType = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 6;
        r0 = r34;
        r8.action = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x05e7:
        r34 = "feedback";
        r0 = r34;
        r34 = r14.optJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x063b;
    L_0x05f1:
        r34 = "feedback";
        r0 = r34;
        r34 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r35 = "likes";
        r34 = r34.has(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x063b;
    L_0x0601:
        r34 = "feedback";
        r0 = r34;
        r34 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r35 = "likes";
        r34 = r34.getJSONObject(r35);	 Catch:{ Exception -> 0x0159 }
        r35 = "count";
        r34 = r34.getInt(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.numLikes = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = "feedback";
        r0 = r34;
        r34 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r35 = "likes";
        r34 = r34.getJSONObject(r35);	 Catch:{ Exception -> 0x0159 }
        r35 = "user_likes";
        r34 = r34.getInt(r35);	 Catch:{ Exception -> 0x0159 }
        r35 = 1;
        r0 = r34;
        r1 = r35;
        if (r0 != r1) goto L_0x09b9;
    L_0x0635:
        r34 = 1;
    L_0x0637:
        r0 = r34;
        r8.isLiked = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x063b:
        r0 = r30;
        r0 = r0.length;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r35 = 1;
        r0 = r34;
        r1 = r35;
        if (r0 <= r1) goto L_0x08a8;
    L_0x0648:
        r34 = "friend_accepted";
        r0 = r29;
        r1 = r34;
        r34 = r0.equals(r1);	 Catch:{ Exception -> 0x0159 }
        if (r34 != 0) goto L_0x08a8;
    L_0x0654:
        r34 = 1;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "post";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x067f;
    L_0x0660:
        r34 = new com.vkontakte.android.NewsEntry;	 Catch:{ Exception -> 0x0159 }
        r35 = "parent";
        r0 = r35;
        r35 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r35;
        r2 = r19;
        r3 = r22;
        r0.<init>(r1, r2, r3);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.parent = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 1;
        r0 = r34;
        r8.parentType = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x067f:
        r34 = 1;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "photo";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x06f7;
    L_0x068b:
        r34 = "parent";
        r0 = r34;
        r34 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = com.vkontakte.android.NewsEntry.parsePhoto(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.parent = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r0 = r34;
        r0 = r0.ownerID;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r19;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (java.lang.String) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r35;
        r1.userName = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r0 = r34;
        r0 = r0.ownerID;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r22;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (java.lang.String) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r35;
        r1.userPhotoURL = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r0 = r34;
        r0 = r0.flags;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r35 = r35 | 2;
        r0 = r35;
        r1 = r34;
        r1.flags = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 2;
        r0 = r34;
        r8.parentType = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x06f7:
        r34 = 1;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "video";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x076f;
    L_0x0703:
        r34 = "parent";
        r0 = r34;
        r34 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = com.vkontakte.android.NewsEntry.parseVideo(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.parent = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r0 = r34;
        r0 = r0.ownerID;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r19;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (java.lang.String) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r35;
        r1.userName = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r0 = r34;
        r0 = r0.ownerID;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r22;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (java.lang.String) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r35;
        r1.userPhotoURL = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.parent;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r0 = r34;
        r0 = r0.flags;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r35 = r35 | 2;
        r0 = r35;
        r1 = r34;
        r1.flags = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 3;
        r0 = r34;
        r8.parentType = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x076f:
        r34 = 1;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "topic";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x07a2;
    L_0x077b:
        r34 = "parent";
        r0 = r34;
        r34 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = com.vkontakte.android.NewsEntry.parseTopic(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.parent = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 5;
        r0 = r34;
        r8.parentType = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.text;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r35 = "\\[id(\\d+):bp-(\\d+)_(\\d+)\\|([^\\]]+)\\]";
        r36 = "$4";
        r34 = r34.replaceAll(r35, r36);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.setText(r0);	 Catch:{ Exception -> 0x0159 }
    L_0x07a2:
        r34 = 1;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "comment";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x08a8;
    L_0x07ae:
        r34 = "parent";
        r0 = r34;
        r6 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r18 = new com.vkontakte.android.NewsEntry;	 Catch:{ Exception -> 0x0159 }
        r18.<init>();	 Catch:{ Exception -> 0x0159 }
        r34 = 5;
        r0 = r34;
        r1 = r18;
        r1.type = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = "topic";
        r0 = r34;
        r34 = r6.has(r0);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x09bd;
    L_0x07cd:
        r34 = "text";
        r0 = r34;
        r34 = r6.getString(r0);	 Catch:{ Exception -> 0x0159 }
        r35 = "\\[(id|club)[0-9]+:bp[-_0-9]+\\|([^\\]]+)\\]";
        r36 = "$2";
        r34 = r34.replaceAll(r35, r36);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r18;
        r1.text = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x07e3:
        r0 = r18;
        r0 = r0.text;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = com.vkontakte.android.Global.replaceEmoji(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r18;
        r1.displayablePreviewText = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = "owner_id";
        r0 = r34;
        r34 = r6.getInt(r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r18;
        r1.userID = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r18;
        r1.ownerID = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = "date";
        r0 = r34;
        r34 = r6.getInt(r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r18;
        r1.time = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r18;
        r0 = r0.userID;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r19;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (java.lang.String) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r18;
        r1.userName = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r18;
        r0 = r0.userID;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r22;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (java.lang.String) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r18;
        r1.userPhotoURL = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = "id";
        r0 = r34;
        r34 = r6.getInt(r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r18;
        r1.postID = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r18;
        r8.parent = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 4;
        r0 = r34;
        r8.parentType = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = 0;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "reply";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 != 0) goto L_0x0885;
    L_0x086d:
        r34 = 0;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "like";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 != 0) goto L_0x0885;
    L_0x0879:
        r34 = 0;
        r34 = r30[r34];	 Catch:{ Exception -> 0x0159 }
        r35 = "copy";
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x08a8;
    L_0x0885:
        r34 = "post";
        r0 = r34;
        r34 = r6.has(r0);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x09d5;
    L_0x088f:
        r34 = new com.vkontakte.android.NewsEntry;	 Catch:{ Exception -> 0x0159 }
        r35 = "post";
        r0 = r35;
        r35 = r6.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r35;
        r2 = r19;
        r3 = r22;
        r0.<init>(r1, r2, r3);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.ppost = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x08a8:
        r34 = "reply";
        r0 = r34;
        r34 = r14.has(r0);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x08e8;
    L_0x08b2:
        r34 = "reply";
        r0 = r34;
        r34 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r35 = "text";
        r34 = r34.getString(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.reply = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = "reply";
        r0 = r34;
        r34 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r35 = "id";
        r34 = r34.getInt(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.replyID = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = "reply";
        r0 = r34;
        r34 = r14.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r35 = "date";
        r34 = r34.getInt(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.replyTime = r0;	 Catch:{ Exception -> 0x0159 }
    L_0x08e8:
        r0 = r8.parentType;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        if (r34 != 0) goto L_0x08f4;
    L_0x08ee:
        r0 = r8.feedbackType;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        if (r34 == 0) goto L_0x0a6d;
    L_0x08f4:
        r0 = r25;
        r0.add(r8);	 Catch:{ Exception -> 0x0159 }
    L_0x08f9:
        r13 = r13 + 1;
        goto L_0x00e0;
    L_0x08fd:
        r34 = "from_id";
        goto L_0x0484;
    L_0x0901:
        r34 = "cid";
        goto L_0x04ca;
    L_0x0905:
        r0 = r8.users;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r0 = r16;
        r34 = r10.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r36 = "from_id";
        r0 = r34;
        r1 = r36;
        r34 = r0.getInt(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r32;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (com.vkontakte.android.UserProfile) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r35;
        r1 = r34;
        r0.add(r1);	 Catch:{ Exception -> 0x0159 }
        r16 = r16 + 1;
        goto L_0x0541;
    L_0x0932:
        r0 = r8.users;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r0 = r16;
        r34 = r10.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r36 = "from_id";
        r0 = r34;
        r1 = r36;
        r34 = r0.getInt(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r32;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (com.vkontakte.android.UserProfile) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r35;
        r1 = r34;
        r0.add(r1);	 Catch:{ Exception -> 0x0159 }
        r16 = r16 + 1;
        goto L_0x0571;
    L_0x095f:
        r0 = r8.users;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r0 = r16;
        r34 = r10.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r36 = "from_id";
        r0 = r34;
        r1 = r36;
        r34 = r0.getInt(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r32;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (com.vkontakte.android.UserProfile) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r35;
        r1 = r34;
        r0.add(r1);	 Catch:{ Exception -> 0x0159 }
        r16 = r16 + 1;
        goto L_0x05a1;
    L_0x098c:
        r0 = r8.users;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r0 = r16;
        r34 = r10.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r36 = "from_id";
        r0 = r34;
        r1 = r36;
        r34 = r0.getInt(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r32;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (com.vkontakte.android.UserProfile) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r35;
        r1 = r34;
        r0.add(r1);	 Catch:{ Exception -> 0x0159 }
        r16 = r16 + 1;
        goto L_0x05d1;
    L_0x09b9:
        r34 = 0;
        goto L_0x0637;
    L_0x09bd:
        r34 = "text";
        r0 = r34;
        r34 = r6.getString(r0);	 Catch:{ Exception -> 0x0159 }
        r35 = "\\[(id|club)(\\d+)\\|([^\\]]+)\\]";
        r36 = "$3";
        r34 = r34.replaceAll(r35, r36);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r18;
        r1.text = r0;	 Catch:{ Exception -> 0x0159 }
        goto L_0x07e3;
    L_0x09d5:
        r34 = "photo";
        r0 = r34;
        r34 = r6.has(r0);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x0a35;
    L_0x09df:
        r34 = "photo";
        r0 = r34;
        r34 = r6.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = com.vkontakte.android.NewsEntry.parsePhoto(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.ppost = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.ppost;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r0 = r8.ppost;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r0 = r34;
        r0 = r0.ownerID;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r19;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (java.lang.String) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r35;
        r1.userName = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.ppost;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r0 = r8.ppost;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r0 = r34;
        r0 = r0.ownerID;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = java.lang.Integer.valueOf(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r22;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (java.lang.String) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r1 = r35;
        r1.userPhotoURL = r0;	 Catch:{ Exception -> 0x0159 }
        goto L_0x08a8;
    L_0x0a35:
        r34 = "video";
        r0 = r34;
        r34 = r6.has(r0);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x0a51;
    L_0x0a3f:
        r34 = "video";
        r0 = r34;
        r34 = r6.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = com.vkontakte.android.NewsEntry.parseVideo(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.ppost = r0;	 Catch:{ Exception -> 0x0159 }
        goto L_0x08a8;
    L_0x0a51:
        r34 = "topic";
        r0 = r34;
        r34 = r6.has(r0);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x08a8;
    L_0x0a5b:
        r34 = "topic";
        r0 = r34;
        r34 = r6.getJSONObject(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = com.vkontakte.android.NewsEntry.parseTopic(r34);	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r8.ppost = r0;	 Catch:{ Exception -> 0x0159 }
        goto L_0x08a8;
    L_0x0a6d:
        r34 = "vk";
        r35 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0159 }
        r36 = "Unknown notification type ";
        r35.<init>(r36);	 Catch:{ Exception -> 0x0159 }
        r0 = r35;
        r1 = r29;
        r35 = r0.append(r1);	 Catch:{ Exception -> 0x0159 }
        r35 = r35.toString();	 Catch:{ Exception -> 0x0159 }
        com.vkontakte.android.Log.m526e(r34, r35);	 Catch:{ Exception -> 0x0159 }
        goto L_0x08f9;
    L_0x0a87:
        r34 = r13 + -1;
        r0 = r25;
        r1 = r34;
        r34 = r0.get(r1);	 Catch:{ Exception -> 0x0159 }
        r34 = (com.vkontakte.android.api.NotificationEntry) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r0 = r0.time;	 Catch:{ Exception -> 0x0159 }
        r23 = r0;
        r0 = r25;
        r34 = r0.get(r13);	 Catch:{ Exception -> 0x0159 }
        r34 = (com.vkontakte.android.api.NotificationEntry) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r0 = r0.time;	 Catch:{ Exception -> 0x0159 }
        r27 = r0;
        r0 = r23;
        r1 = r17;
        if (r0 <= r1) goto L_0x0ac5;
    L_0x0aad:
        r0 = r27;
        r1 = r17;
        if (r0 > r1) goto L_0x0ac5;
    L_0x0ab3:
        r8 = new com.vkontakte.android.api.NotificationEntry;	 Catch:{ Exception -> 0x0159 }
        r8.<init>();	 Catch:{ Exception -> 0x0159 }
        r34 = 5;
        r0 = r34;
        r8.feedbackType = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r25;
        r0.add(r13, r8);	 Catch:{ Exception -> 0x0159 }
        goto L_0x00f1;
    L_0x0ac5:
        r13 = r13 + 1;
        goto L_0x00e9;
    L_0x0ac9:
        r0 = r25;
        r8 = r0.get(r13);	 Catch:{ Exception -> 0x0159 }
        r8 = (com.vkontakte.android.api.NotificationEntry) r8;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.feedbackType;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r35 = 4;
        r0 = r34;
        r1 = r35;
        if (r0 == r1) goto L_0x0b06;
    L_0x0add:
        r0 = r8.feedbackType;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r35 = 1;
        r0 = r34;
        r1 = r35;
        if (r0 == r1) goto L_0x0b06;
    L_0x0ae9:
        if (r7 == 0) goto L_0x0afd;
    L_0x0aeb:
        r0 = r7.extra;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = (java.util.ArrayList) r34;	 Catch:{ Exception -> 0x0159 }
        r34 = r34.size();	 Catch:{ Exception -> 0x0159 }
        if (r34 <= 0) goto L_0x0afc;
    L_0x0af7:
        r0 = r28;
        r0.add(r7);	 Catch:{ Exception -> 0x0159 }
    L_0x0afc:
        r7 = 0;
    L_0x0afd:
        r0 = r28;
        r0.add(r8);	 Catch:{ Exception -> 0x0159 }
    L_0x0b02:
        r13 = r13 + 1;
        goto L_0x00fd;
    L_0x0b06:
        r34 = r5.size();	 Catch:{ Exception -> 0x0159 }
        r35 = 2;
        r0 = r34;
        r1 = r35;
        if (r0 != r1) goto L_0x0b19;
    L_0x0b12:
        r34 = 0;
        r0 = r34;
        r5.remove(r0);	 Catch:{ Exception -> 0x0159 }
    L_0x0b19:
        r34 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0159 }
        r0 = r8.feedbackType;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r35 = java.lang.String.valueOf(r35);	 Catch:{ Exception -> 0x0159 }
        r34.<init>(r35);	 Catch:{ Exception -> 0x0159 }
        r35 = ",";
        r34 = r34.append(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = r8.action;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r34 = r34.append(r35);	 Catch:{ Exception -> 0x0159 }
        r35 = ",";
        r34 = r34.append(r35);	 Catch:{ Exception -> 0x0159 }
        r0 = r8.parentType;	 Catch:{ Exception -> 0x0159 }
        r35 = r0;
        r34 = r34.append(r35);	 Catch:{ Exception -> 0x0159 }
        r26 = r34.toString();	 Catch:{ Exception -> 0x0159 }
        r0 = r8.users;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = r34.iterator();	 Catch:{ Exception -> 0x0159 }
    L_0x0b4e:
        r35 = r34.hasNext();	 Catch:{ Exception -> 0x0159 }
        if (r35 != 0) goto L_0x0b9a;
    L_0x0b54:
        r0 = r26;
        r5.add(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = r5.size();	 Catch:{ Exception -> 0x0159 }
        r35 = 2;
        r0 = r34;
        r1 = r35;
        if (r0 != r1) goto L_0x0bcb;
    L_0x0b65:
        r34 = 0;
        r0 = r34;
        r34 = r5.get(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = (java.lang.String) r34;	 Catch:{ Exception -> 0x0159 }
        r35 = 1;
        r0 = r35;
        r35 = r5.get(r0);	 Catch:{ Exception -> 0x0159 }
        r34 = r34.equals(r35);	 Catch:{ Exception -> 0x0159 }
        if (r34 == 0) goto L_0x0bcb;
    L_0x0b7d:
        if (r7 != 0) goto L_0x0bbe;
    L_0x0b7f:
        r7 = new com.vkontakte.android.api.NotificationEntry;	 Catch:{ Exception -> 0x0159 }
        r7.<init>();	 Catch:{ Exception -> 0x0159 }
        r34 = 6;
        r0 = r34;
        r7.feedbackType = r0;	 Catch:{ Exception -> 0x0159 }
        r34 = new java.util.ArrayList;	 Catch:{ Exception -> 0x0159 }
        r34.<init>();	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r7.extra = r0;	 Catch:{ Exception -> 0x0159 }
        r0 = r28;
        r0.add(r8);	 Catch:{ Exception -> 0x0159 }
        goto L_0x0b02;
    L_0x0b9a:
        r21 = r34.next();	 Catch:{ Exception -> 0x0159 }
        r21 = (com.vkontakte.android.UserProfile) r21;	 Catch:{ Exception -> 0x0159 }
        r35 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0159 }
        r36 = java.lang.String.valueOf(r26);	 Catch:{ Exception -> 0x0159 }
        r35.<init>(r36);	 Catch:{ Exception -> 0x0159 }
        r36 = ",";
        r35 = r35.append(r36);	 Catch:{ Exception -> 0x0159 }
        r0 = r21;
        r0 = r0.uid;	 Catch:{ Exception -> 0x0159 }
        r36 = r0;
        r35 = r35.append(r36);	 Catch:{ Exception -> 0x0159 }
        r26 = r35.toString();	 Catch:{ Exception -> 0x0159 }
        goto L_0x0b4e;
    L_0x0bbe:
        r0 = r7.extra;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = (java.util.ArrayList) r34;	 Catch:{ Exception -> 0x0159 }
        r0 = r34;
        r0.add(r8);	 Catch:{ Exception -> 0x0159 }
        goto L_0x0b02;
    L_0x0bcb:
        if (r7 == 0) goto L_0x0be6;
    L_0x0bcd:
        r0 = r7.extra;	 Catch:{ Exception -> 0x0159 }
        r34 = r0;
        r34 = (java.util.ArrayList) r34;	 Catch:{ Exception -> 0x0159 }
        r34 = r34.size();	 Catch:{ Exception -> 0x0159 }
        if (r34 <= 0) goto L_0x0bde;
    L_0x0bd9:
        r0 = r28;
        r0.add(r7);	 Catch:{ Exception -> 0x0159 }
    L_0x0bde:
        r7 = 0;
        r0 = r28;
        r0.add(r8);	 Catch:{ Exception -> 0x0159 }
        goto L_0x0b02;
    L_0x0be6:
        r0 = r28;
        r0.add(r8);	 Catch:{ Exception -> 0x0159 }
        goto L_0x0b02;
    L_0x0bed:
        r34 = new com.vkontakte.android.APIRequest$ErrorResponse;
        r35 = 0;
        r36 = "Parse error";
        r34.<init>(r35, r36);
        goto L_0x00b9;
    L_0x0bf8:
        r34 = move-exception;
        goto L_0x0057;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.api.NotificationsGet.parse(org.json.JSONObject):java.lang.Object");
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((List) r[0], ((Integer) r[1]).intValue(), ((Integer) r[2]).intValue(), (String) r[3]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
