package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class WallRestoreComment extends APIRequest {
    private static final String[] methods;
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    static {
        methods = new String[]{"wall.restoreComment", "photos.restoreComment", "video.restoreComment"};
    }

    public WallRestoreComment(int oid, int pid, int cid, int type, String accessKey) {
        super(methods[type]);
        if (type == 0 || type == 2) {
            param("owner_id", oid).param("comment_id", cid);
        }
        if (type == 1) {
            param("owner_id", oid).param("comment_id", cid).param("photo_id", pid);
        }
        if (accessKey != null) {
            param("access_key", accessKey);
        }
    }

    public Object parse(JSONObject o) {
        return Boolean.valueOf(true);
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
