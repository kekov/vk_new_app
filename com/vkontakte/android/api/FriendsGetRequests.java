package com.vkontakte.android.api;

import android.content.res.Resources;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.data.Friends;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class FriendsGetRequests extends APIRequest {
    private Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<FriendRequest> arrayList, ArrayList<FriendRequest> arrayList2, int i);
    }

    public FriendsGetRequests(int offset, int count, boolean onlySuggests) {
        super("execute.getFullFriendRequestsNew");
        this.callback = null;
        param("offset", offset);
        param("count", count);
        if (onlySuggests) {
            param("only_suggests", 1);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public Object parse(JSONObject r) {
        try {
            int i;
            JSONObject jreq;
            FriendRequest req;
            int nMFriends;
            int j;
            int uid;
            UserProfile up;
            Iterator it;
            UserProfile pp;
            int i2;
            ArrayList<FriendRequest> result = new ArrayList();
            ArrayList<FriendRequest> recoms = new ArrayList();
            ArrayList<UserProfile> myFriends = new ArrayList();
            Friends.getFriends(myFriends);
            JSONArray requests = r.getJSONObject("response").optJSONArray("f");
            JSONArray profiles = r.getJSONObject("response").optJSONArray("p");
            JSONArray recom = r.getJSONObject("response").optJSONArray("s");
            JSONArray fnames = r.getJSONObject("response").optJSONArray("s_from");
            int recomCnt = r.getJSONObject("response").optInt("sc");
            int myCountry = r.getJSONObject("response").getJSONObject("mc").getInt("id");
            if ((((requests == null || requests.length() == 0) && (recom == null || recom.length() == 0)) || profiles == null) && this.callback != null) {
                this.callback.success(result, recoms, recomCnt);
            }
            HashMap<Integer, UserProfile> users = new HashMap();
            HashMap<Integer, String> fromNames = new HashMap();
            if (profiles != null) {
                for (i = 0; i < profiles.length(); i++) {
                    JSONObject jp = profiles.getJSONObject(i);
                    UserProfile p = new UserProfile();
                    p.firstName = jp.getString("first_name");
                    p.lastName = jp.getString("last_name");
                    p.fullName = p.firstName + " " + p.lastName;
                    p.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                    p.uid = jp.getInt("id");
                    p.country = jp.optInt("country", 0);
                    p.city = jp.optInt("city", 0);
                    if (jp.optString("university_name", ACRAConstants.DEFAULT_STRING_VALUE).length() > 0) {
                        p.university = jp.getString("university_name").replace("\r\n", ACRAConstants.DEFAULT_STRING_VALUE).trim();
                        if (jp.optInt("graduation", 0) > 0) {
                            StringBuilder stringBuilder = new StringBuilder(String.valueOf(p.university));
                            Integer[] numArr = new Object[1];
                            numArr[0] = Integer.valueOf(jp.getInt("graduation") % 100);
                            p.university = stringBuilder.append(String.format(" '%02d", numArr)).toString();
                        }
                    } else {
                        if (jp.has("country")) {
                            p.university = jp.getJSONObject("country").getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                            if (jp.has("city")) {
                                p.university += ", " + jp.getJSONObject("city").getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                            }
                        } else {
                            p.university = null;
                        }
                    }
                    users.put(Integer.valueOf(jp.getInt("id")), p);
                }
            }
            if (fnames != null) {
                for (i = 0; i < fnames.length(); i++) {
                    JSONObject oo = fnames.getJSONObject(i);
                    fromNames.put(Integer.valueOf(oo.getInt("id")), oo.getString("first_name") + " " + oo.getString("last_name"));
                }
            }
            for (i = 0; i < requests.length(); i++) {
                jreq = requests.getJSONObject(i);
                req = new FriendRequest();
                req.profile = (UserProfile) users.get(jreq.get("user_id"));
                req.message = jreq.optString(LongPollService.EXTRA_MESSAGE);
                req.info = req.profile.university;
                if (jreq.has("mutual")) {
                    nMFriends = Math.min(5, jreq.getJSONObject("mutual").getJSONArray("users").length());
                    req.mutualFriends = new UserProfile[nMFriends];
                    req.numMutualFriends = jreq.getJSONObject("mutual").getInt("count");
                    for (j = 0; j < nMFriends; j++) {
                        uid = jreq.getJSONObject("mutual").getJSONArray("users").getInt(j);
                        up = new UserProfile();
                        it = myFriends.iterator();
                        while (it.hasNext()) {
                            pp = (UserProfile) it.next();
                            i2 = pp.uid;
                            if (r0 == uid) {
                                up = pp;
                                break;
                            }
                        }
                        req.mutualFriends[j] = up;
                    }
                } else {
                    req.numMutualFriends = 0;
                }
                result.add(req);
            }
            if (recom != null) {
                for (i = 0; i < recom.length(); i++) {
                    jreq = recom.getJSONObject(i);
                    req = new FriendRequest();
                    req.profile = (UserProfile) users.get(jreq.get("user_id"));
                    Resources resources = VKApplication.context.getResources();
                    Object[] objArr = new Object[1];
                    objArr[0] = fromNames.get(Integer.valueOf(jreq.getInt("from")));
                    req.message = resources.getString(C0436R.string.friends_recommend_from, objArr);
                    req.info = req.profile.university;
                    if (jreq.has("mutual")) {
                        nMFriends = Math.min(5, jreq.getJSONObject("mutual").getJSONArray("users").length());
                        req.mutualFriends = new UserProfile[nMFriends];
                        req.numMutualFriends = jreq.getJSONObject("mutual").getInt("count");
                        for (j = 0; j < nMFriends; j++) {
                            uid = jreq.getJSONObject("mutual").getJSONArray("users").getInt(j);
                            up = new UserProfile();
                            it = myFriends.iterator();
                            while (it.hasNext()) {
                                pp = (UserProfile) it.next();
                                i2 = pp.uid;
                                if (r0 == uid) {
                                    up = pp;
                                    break;
                                }
                            }
                            req.mutualFriends[j] = up;
                        }
                    } else {
                        req.numMutualFriends = 0;
                    }
                    recoms.add(req);
                }
            }
            return new Object[]{result, recoms, Integer.valueOf(recomCnt)};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            if (this.callback != null) {
                this.callback.fail(-1, "Parse error");
            }
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((ArrayList) r[0], (ArrayList) r[1], ((Integer) r[2]).intValue());
        }
    }
}
