package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class PlacesGetCheckinProfiles extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<UserProfile> arrayList, int i, int i2);
    }

    public PlacesGetCheckinProfiles(int placeID, int offset, int count) {
        super("execute.getPlaceCheckins");
        param("place_id", placeID);
        param("offset", offset);
        param("count", count);
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray jp = o.getJSONObject("response").optJSONArray("items");
            ArrayList<UserProfile> users = new ArrayList();
            if (jp != null) {
                for (int i = 0; i < jp.length(); i++) {
                    JSONObject ju = jp.getJSONObject(i);
                    UserProfile u = new UserProfile();
                    u.uid = ju.getInt("id");
                    u.firstName = ju.getString("first_name");
                    u.lastName = ju.getString("last_name");
                    u.fullName = u.firstName + " " + u.lastName;
                    u.photo = ju.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                    users.add(u);
                }
            }
            return new Object[]{users, Integer.valueOf(o.getJSONObject("response").getInt("new_offset")), Integer.valueOf(o.getJSONObject("response").getInt("count"))};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((ArrayList) r[0], ((Integer) r[1]).intValue(), ((Integer) r[2]).intValue());
        }
    }
}
