package com.vkontakte.android.api;

import org.json.JSONException;
import org.json.JSONObject;

public class APIUtils {
    public static JSONArrayWithCount unwrapArray(JSONObject parent, String name) throws JSONException {
        if (!parent.has(name) || parent.optJSONObject(name) == null) {
            return null;
        }
        JSONArrayWithCount r = new JSONArrayWithCount();
        r.array = parent.getJSONObject(name).optJSONArray("items");
        if (r.array == null) {
            return null;
        }
        r.count = parent.getJSONObject(name).getInt("count");
        return r;
    }
}
