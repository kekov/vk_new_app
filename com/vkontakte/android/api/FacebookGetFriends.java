package com.vkontakte.android.api;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class FacebookGetFriends extends APIRequest {
    Callback callback;
    private String token;

    public interface Callback {
        void fail(int i, String str);

        void success(List<UserProfile> list);
    }

    public FacebookGetFriends(String accessToken) {
        super("__facebookGetFriends");
        this.token = accessToken;
    }

    public JSONObject doExec() {
        try {
            return (JSONObject) new JSONTokener(new String(Global.getURL("https://graph.facebook.com/me/friends?limit=5000&fields=id,first_name,last_name&access_token=" + this.token), "UTF-8")).nextValue();
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public Object parse(JSONObject o) {
        try {
            Object result = new ArrayList();
            JSONArray r = o.getJSONArray("data");
            for (int i = 0; i < r.length(); i++) {
                JSONObject u = r.getJSONObject(i);
                UserProfile user = new UserProfile();
                user.extra = u.getString("id");
                user.firstName = u.getString("first_name");
                user.lastName = u.getString("last_name");
                user.fullName = user.firstName + " " + user.lastName;
                int photoSize = Global.scale(BitmapDescriptorFactory.HUE_YELLOW);
                user.photo = "https://graph.facebook.com/" + user.extra + "/picture?width=" + photoSize + "&height=" + photoSize;
                result.add(user);
            }
            return result;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((List) result);
        }
    }
}
