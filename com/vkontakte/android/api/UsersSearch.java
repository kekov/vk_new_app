package com.vkontakte.android.api;

import android.os.Bundle;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UsersSearch extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<UserProfile> arrayList, int i);
    }

    public UsersSearch(String query, int offset, int count) {
        super("execute");
        String fields = new StringBuilder(String.valueOf(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec")).append(",online").toString();
        String code = "var s=API.users.search({q:\"" + query.replace("\"", "\\\"") + "\",offset:" + offset + ",count:" + count + ",fields:\"" + fields + "\"});";
        boolean pID = false;
        boolean pDomain = false;
        if (offset == 0) {
            int iv = intval(query);
            if (iv > 0) {
                pID = true;
                code = new StringBuilder(String.valueOf(code)).append("var p_id=API.users.get({user_ids:").append(iv).append(",fields:\"").append(fields).append("\"});").toString();
            }
            if (!(isInt(query) || !isDomain(query) || query.toLowerCase().equals("id" + iv))) {
                pDomain = true;
                code = new StringBuilder(String.valueOf(code)).append("var p_domain=API.users.get({user_ids:\"").append(query).append("\",fields:\"").append(fields).append("\"});").toString();
            }
        }
        code = new StringBuilder(String.valueOf(code)).append("return s").toString();
        if (pID) {
            code = new StringBuilder(String.valueOf(code)).append("+{p_id:p_id[0]}").toString();
        }
        if (pDomain) {
            code = new StringBuilder(String.valueOf(code)).append("+{p_domain:p_domain[0]}").toString();
        }
        param("code", new StringBuilder(String.valueOf(code)).append(";").toString());
    }

    public UsersSearch(String query, Bundle xargs, int offset, int count) {
        super("users.search");
        param("fields", "photo_rec,photo_medium_rec,education,city,country,verified");
        param("q", query);
        param("offset", offset).param("count", count);
        if (xargs != null) {
            for (String key : xargs.keySet()) {
                param(key, xargs.get(key).toString());
            }
        }
    }

    private UserProfile parseProfile(JSONObject p) throws JSONException {
        boolean z;
        UserProfile profile = new UserProfile();
        profile.firstName = p.optString("first_name", "DELETED");
        profile.lastName = p.optString("last_name", ACRAConstants.DEFAULT_STRING_VALUE);
        profile.fullName = profile.firstName + " " + profile.lastName;
        profile.photo = p.optString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec", "http://vkontakte.ru/images/question_c.gif");
        profile.uid = p.getInt("id");
        profile.online = Global.getUserOnlineStatus(p);
        if (p.has("university_name") && p.getString("university_name").length() > 0) {
            profile.university = p.getString("university_name").trim();
            if (p.has("graduation") && p.getInt("graduation") > 0) {
                profile.university += String.format(" '%02d", new Object[]{Integer.valueOf(p.getInt("graduation") % 100)});
            }
        } else if (p.has("country")) {
            profile.university = p.getJSONObject("country").getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            if (p.has("city")) {
                profile.university += ", " + p.getJSONObject("city").getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            }
        }
        if (p.optInt("verified") == 1) {
            z = true;
        } else {
            z = false;
        }
        profile.extra = Boolean.valueOf(z);
        return profile;
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray a = APIUtils.unwrapArray(o, "response").array;
            JSONObject pID = o.getJSONObject("response").optJSONObject("p_id");
            JSONObject pDomain = o.getJSONObject("response").optJSONObject("p_domain");
            int total = 0;
            if (a != null) {
                total = APIUtils.unwrapArray(o, "response").count;
            }
            ArrayList<UserProfile> profiles = new ArrayList();
            if (pID != null) {
                profiles.add(parseProfile(pID));
                total++;
            }
            if (pDomain != null) {
                profiles.add(parseProfile(pDomain));
                total++;
            }
            if (a != null) {
                for (int i = 0; i < a.length(); i++) {
                    profiles.add(parseProfile(a.getJSONObject(i)));
                }
            }
            return new Object[]{profiles, Integer.valueOf(total)};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((ArrayList) r[0], ((Integer) r[1]).intValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    private int intval(String s) {
        int i = 0;
        String is = ACRAConstants.DEFAULT_STRING_VALUE;
        for (int i2 = 0; i2 < s.length(); i2++) {
            if (Character.isDigit(s.charAt(i2))) {
                is = new StringBuilder(String.valueOf(is)).append(s.charAt(i2)).toString();
            }
        }
        if (is.length() != 0) {
            try {
                i = Integer.parseInt(is);
            } catch (Exception e) {
            }
        }
        return i;
    }

    private boolean isInt(String s) {
        return new StringBuilder(String.valueOf(intval(s))).toString().equals(s);
    }

    private boolean isDomain(String s) {
        return s.matches("[A-Za-z0-9_\\.]{3,}");
    }
}
