package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class BoardCloseTopic extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    /* renamed from: com.vkontakte.android.api.BoardCloseTopic.1 */
    class C13961 extends APIHandler {
        C13961() {
        }

        public void success(JSONObject o) {
            BoardCloseTopic.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (BoardCloseTopic.this.callback != null) {
                BoardCloseTopic.this.callback.fail(ecode, emsg);
            }
        }
    }

    public BoardCloseTopic(int gid, int tid, boolean closed) {
        super(closed ? "board.closeTopic" : "board.openTopic");
        param("group_id", gid).param("topic_id", tid);
        handler(new C13961());
    }

    public Object parse(JSONObject o) {
        try {
            return Boolean.valueOf(true);
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
