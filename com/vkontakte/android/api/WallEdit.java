package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.LongPollService;
import org.json.JSONObject;

public class WallEdit extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    public WallEdit(int postID, int oid, String text) {
        super("wall.edit");
        param("owner_id", oid);
        param("post_id", postID);
        param(LongPollService.EXTRA_MESSAGE, text);
    }

    public Object parse(JSONObject o) {
        try {
            return Boolean.valueOf(true);
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }
}
