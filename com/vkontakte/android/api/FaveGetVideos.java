package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import java.util.HashMap;
import java.util.Locale;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class FaveGetVideos extends APIRequest {
    private static final String CODE = "var v=API.fave.getVideos({offset:%d,count:%d});return {v:v,u:API.getProfiles({uids:v@.owner_id,fields:\"%s\"})};";
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, Vector<VideoFile> vector);
    }

    public FaveGetVideos(int offset, int count) {
        super("execute");
        String str = "code";
        Locale locale = Locale.US;
        String str2 = CODE;
        Object[] objArr = new Object[3];
        objArr[0] = Integer.valueOf(offset);
        objArr[1] = Integer.valueOf(count);
        objArr[2] = Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec";
        param(str, String.format(locale, str2, objArr));
    }

    public Object parse(JSONObject o) {
        try {
            int i;
            Vector<VideoFile> vf = new Vector();
            int total = 0;
            JSONArray arr = APIUtils.unwrapArray(o.getJSONObject("response"), "v").array;
            JSONArray u = o.getJSONObject("response").optJSONArray("u");
            HashMap<Integer, String> names = new HashMap();
            HashMap<Integer, String> photos = new HashMap();
            if (u != null) {
                for (i = 0; i < u.length(); i++) {
                    int uid = u.getJSONObject(i).getInt("id");
                    names.put(Integer.valueOf(uid), new StringBuilder(String.valueOf(u.getJSONObject(i).getString("first_name"))).append(" ").append(u.getJSONObject(i).getString("last_name")).toString());
                    photos.put(Integer.valueOf(uid), u.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec"));
                }
            }
            if (arr != null) {
                total = APIUtils.unwrapArray(o.getJSONObject("response"), "v").count;
                for (i = 0; i < arr.length(); i++) {
                    VideoFile v = new VideoFile(arr.getJSONObject(i));
                    v.ownerName = (String) names.get(Integer.valueOf(v.oid));
                    v.ownerPhoto = (String) photos.get(Integer.valueOf(v.oid));
                    vf.add(v);
                }
            }
            return new Object[]{Integer.valueOf(total), vf};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (Vector) r[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
