package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import org.json.JSONObject;

public class BoardFixTopic extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    /* renamed from: com.vkontakte.android.api.BoardFixTopic.1 */
    class C13991 extends APIHandler {
        C13991() {
        }

        public void success(JSONObject o) {
            BoardFixTopic.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (BoardFixTopic.this.callback != null) {
                BoardFixTopic.this.callback.fail(ecode, emsg);
            }
        }
    }

    public BoardFixTopic(int gid, int tid, boolean fixed) {
        super(fixed ? "board.fixTopic" : "board.unfixTopic");
        param("group_id", gid).param("topic_id", tid);
        handler(new C13991());
    }

    public Object parse(JSONObject o) {
        try {
            if (this.callback != null) {
                this.callback.success();
            }
        } catch (Exception e) {
        }
        return null;
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
