package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import org.json.JSONObject;

public class VoipInit extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(String str, String str2, String str3);
    }

    /* renamed from: com.vkontakte.android.api.VoipInit.1 */
    class C14091 extends APIHandler {
        C14091() {
        }

        public void success(JSONObject o) {
            VoipInit.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (VoipInit.this.callback != null) {
                VoipInit.this.callback.fail(ecode, emsg);
            }
        }
    }

    public VoipInit() {
        super("voip.init");
        handler(new C14091());
    }

    public Object parse(JSONObject o) {
        try {
            o = o.getJSONObject("response");
            return new String[]{o.getString("server"), o.getJSONArray("params").getString(0), o.getJSONArray("params").getString(1)};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            String[] r = (String[]) result;
            this.callback.success(r[0], r[1], r[2]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
