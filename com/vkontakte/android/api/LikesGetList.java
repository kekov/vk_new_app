package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.Vector;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class LikesGetList extends APIRequest {
    Callback callback;
    private String[] types;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, Vector<UserProfile> vector);
    }

    public LikesGetList(int type, int parentType, int oid, int itemID, int offset, int count, boolean friends, String filter) {
        super("likes.getList");
        this.types = new String[]{"post", "photo", "video", "note", "topic", "comment"};
        if (type == 5) {
            String t = ACRAConstants.DEFAULT_STRING_VALUE;
            switch (parentType) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    t = "photo_";
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    t = "video_";
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    t = "topic_";
                    break;
            }
            param(WebDialog.DIALOG_PARAM_TYPE, new StringBuilder(String.valueOf(t)).append("comment").toString());
        } else {
            param(WebDialog.DIALOG_PARAM_TYPE, this.types[type]);
        }
        param("owner_id", oid).param("item_id", itemID).param("count", count).param("offset", offset).param("extended", 1).param("fields", "photo_rec,photo_medium_rec");
        if (friends) {
            param("friends_only", 1);
        }
        if (filter != null) {
            param("filter", filter);
        }
    }

    public Object parse(JSONObject o) {
        try {
            Vector<UserProfile> r = new Vector();
            JSONArray a = APIUtils.unwrapArray(o, "response").array;
            if (a != null) {
                for (int i = 0; i < a.length(); i++) {
                    JSONObject jp = a.getJSONObject(i);
                    UserProfile p = new UserProfile();
                    if ("profile".equals(jp.optString(WebDialog.DIALOG_PARAM_TYPE))) {
                        p.firstName = jp.getString("first_name");
                        p.lastName = jp.getString("last_name");
                        p.fullName = p.firstName + " " + p.lastName;
                        p.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                        p.uid = jp.getInt("id");
                        p.online = Global.getUserOnlineStatus(jp);
                    } else {
                        p.fullName = jp.getString("name");
                        p.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                        p.uid = -jp.getInt("id");
                    }
                    r.add(p);
                }
            }
            return new Object[]{Integer.valueOf(o.getJSONObject("response").getInt("count")), r};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (Vector) r[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
