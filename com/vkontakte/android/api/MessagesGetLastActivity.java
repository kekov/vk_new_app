package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import org.json.JSONObject;

public class MessagesGetLastActivity extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, int i2, boolean z, boolean z2);
    }

    public MessagesGetLastActivity(int uid) {
        super("users.get");
        param("user_ids", uid);
        param("fields", "online,sex,last_seen");
    }

    public Object parse(JSONObject o) {
        boolean z = false;
        try {
            int i;
            o = o.getJSONArray("response").getJSONObject(0);
            int platform = o.has("last_seen") ? o.getJSONObject("last_seen").getInt("platform") : 0;
            Object obj = new Object[4];
            if (o.has("last_seen")) {
                i = o.getJSONObject("last_seen").getInt("time") + Global.timeDiff;
            } else {
                i = 0;
            }
            obj[0] = Integer.valueOf(i);
            obj[1] = Integer.valueOf(Global.getUserOnlineStatus(o));
            obj[2] = Boolean.valueOf(o.getInt("sex") == 1);
            if (platform == 1 || platform == 2 || platform == 3 || platform == 4 || platform == 5) {
                z = true;
            }
            obj[3] = Boolean.valueOf(z);
            return obj;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), ((Integer) r[1]).intValue(), ((Boolean) r[2]).booleanValue(), ((Boolean) r[3]).booleanValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
