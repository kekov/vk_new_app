package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class VoipStart extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, String str);
    }

    /* renamed from: com.vkontakte.android.api.VoipStart.1 */
    class C14111 extends APIHandler {
        C14111() {
        }

        public void success(JSONObject o) {
            VoipStart.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (VoipStart.this.callback != null) {
                VoipStart.this.callback.fail(ecode, emsg);
            }
        }
    }

    public VoipStart(int uid) {
        super("voip.start");
        param("user_id", uid);
        handler(new C14111());
    }

    public Object parse(JSONObject o) {
        try {
            o = o.getJSONObject("response");
            return new Object[]{Integer.valueOf(o.getInt("id")), o.getString("near_stream")};
        } catch (Exception e) {
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (String) r[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
