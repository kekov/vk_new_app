package com.vkontakte.android.api;

import android.text.TextUtils;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.data.StickerPack;
import com.vkontakte.android.data.Stickers;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class StoreGetInventory extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(List<StickerPack> list);
    }

    public StoreGetInventory() {
        super("execute.storeGetStockItems");
        param(WebDialog.DIALOG_PARAM_TYPE, "stickers");
        param("merchant", "google");
        if (!Stickers.isPlayStoreInstalled()) {
            param("filters", "free,purchased");
        }
    }

    public StoreGetInventory(int id) {
        super("store.getStockItems");
        param(WebDialog.DIALOG_PARAM_TYPE, "stickers");
        param("merchant", "google");
        param("product_ids", id);
    }

    public StoreGetInventory(List<Integer> ids) {
        super("store.getStockItems");
        param(WebDialog.DIALOG_PARAM_TYPE, "stickers");
        param("merchant", "google");
        param("product_ids", TextUtils.join(",", ids));
    }

    public Object parse(JSONObject o) {
        try {
            Object res = new ArrayList();
            JSONArray arr = APIUtils.unwrapArray(o, "response").array;
            for (int i = 0; i < arr.length(); i++) {
                JSONObject jp = arr.getJSONObject(i);
                StickerPack pack = new StickerPack(jp.getJSONObject("product"));
                pack.description = jp.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION);
                boolean purchased = jp.getJSONObject("product").optInt("purchased") == 1;
                boolean free = jp.optInt("free", 1) == 1;
                if (!(purchased || free)) {
                    pack.storeID = jp.optString("merchant_product_id");
                }
                pack.thumb = jp.getString("photo_" + (Global.displayDensity > 1.0f ? 140 : 70));
                pack.isNew = jp.optInt("new") == 1;
                pack.author = jp.getString("author");
                pack.previews = new ArrayList();
                JSONArray jpa = jp.getJSONArray("demo_photos_560");
                for (int j = 0; j < jpa.length(); j++) {
                    pack.previews.add(jpa.getString(j));
                }
                res.add(pack);
            }
            return res;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((List) result);
        }
    }
}
