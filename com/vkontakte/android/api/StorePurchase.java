package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.data.StickerPack;
import org.json.JSONObject;

public class StorePurchase extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, StickerPack stickerPack, String str, boolean z);
    }

    public StorePurchase(int id, String storeID, String transaction, String token) {
        super("store.purchase");
        param(WebDialog.DIALOG_PARAM_TYPE, "stickers");
        param("product_id", id);
        param("merchant", "google");
        if (storeID != null) {
            param("merchant_product_id", storeID);
            param("merchant_transaction_id", transaction);
            param("receipt", token);
        }
    }

    public Object parse(JSONObject o) {
        boolean z = true;
        try {
            JSONObject r = o.getJSONObject("response");
            int state = r.getInt("state");
            StickerPack pack = null;
            String msg = r.optString(LongPollService.EXTRA_MESSAGE, r.optString("error_message"));
            if (msg != null && msg.length() == 0) {
                msg = null;
            }
            if (r.has("product")) {
                pack = new StickerPack(r.getJSONObject("product"));
            }
            Object obj = new Object[4];
            obj[0] = Integer.valueOf(state);
            obj[1] = pack;
            obj[2] = msg;
            if (r.optInt("error_fatal") != 1) {
                z = false;
            }
            obj[3] = Boolean.valueOf(z);
            return obj;
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (StickerPack) r[1], (String) r[2], ((Boolean) r[3]).booleanValue());
        }
    }
}
