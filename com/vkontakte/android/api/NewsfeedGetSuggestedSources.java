package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class NewsfeedGetSuggestedSources extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(List<UserProfile> list);
    }

    public NewsfeedGetSuggestedSources() {
        super("newsfeed.getSuggestedSources");
        param("count", 200);
        param("fields", "photo_50,photo_100,activity");
    }

    public Object parse(JSONObject o) {
        try {
            Object result = new ArrayList();
            JSONArray r = o.getJSONArray("response");
            for (int i = 0; i < r.length(); i++) {
                JSONObject u = r.getJSONObject(i);
                UserProfile p = new UserProfile();
                if (u.getString(WebDialog.DIALOG_PARAM_TYPE).equals("profile")) {
                    p.uid = u.getInt("id");
                    p.firstName = u.getString("first_name");
                    p.lastName = u.getString("last_name");
                    p.fullName = p.firstName + " " + p.lastName;
                } else {
                    p.uid = -u.getInt("id");
                    p.fullName = u.getString("name");
                }
                p.photo = u.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                p.university = u.getString("activity");
                result.add(p);
            }
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((List) result);
        }
    }
}
