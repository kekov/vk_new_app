package com.vkontakte.android.api;

import com.vkontakte.android.AudioFile;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import java.util.HashMap;

public class ExtendedUserProfile {
    public static final int ACCESS_CLOSED = 1;
    public static final int ACCESS_OPEN = 0;
    public static final int ACCESS_PRIVATE = 2;
    public static final int FRIEND_STATUS_FRIENDS = 3;
    public static final int FRIEND_STATUS_NONE = 0;
    public static final int FRIEND_STATUS_RECV_REQUEST = 2;
    public static final int FRIEND_STATUS_SENT_REQUEST = 1;
    public static final int RELATIVE_CHILD = 2;
    public static final int RELATIVE_GRANDCHILD = 4;
    public static final int RELATIVE_GRANDPARENT = 3;
    public static final int RELATIVE_PARENT = 0;
    public static final int RELATIVE_SIBLING = 1;
    public static final int TYPE_EVENT = 1;
    public static final int TYPE_GROUP = 0;
    public static final int TYPE_PUBLIC = 2;
    public String about;
    public String activities;
    public CharSequence activity;
    public int alcohol;
    public AudioFile audioStatus;
    public int bDay;
    public int bMonth;
    public int bYear;
    public String bigPhoto;
    public boolean blacklisted;
    public String books;
    public boolean canCall;
    public boolean canPost;
    public boolean canSeeAllPosts;
    public boolean canWrite;
    public String city;
    public HashMap<String, Integer> counters;
    public String country;
    public int eventEndTime;
    public int eventStartTime;
    public long facebookId;
    public String facebookName;
    public String firstNameAcc;
    public String firstNameDat;
    public String firstNameGen;
    public String firstNameIns;
    public int friendStatus;
    public String games;
    public int groupAccess;
    public int groupType;
    public String homePhone;
    public String hometown;
    public String infoLine;
    public String inspiredBy;
    public String instagram;
    public String interests;
    public String langs;
    public String lastNameAcc;
    public String lastNameDat;
    public String lastNameGen;
    public String lastNameIns;
    public int lastSeen;
    public boolean lastSeenMobile;
    public double lat;
    public int lifeMain;
    public String livejournal;
    public double lon;
    public String mobilePhone;
    public String movies;
    public String music;
    public int peopleMain;
    public int political;
    public int postponedCount;
    public UserProfile profile;
    public String quotations;
    public int relation;
    public int relationPartner;
    public String relationPartnerName;
    public ArrayList<Relative> relatives;
    public String religion;
    public ArrayList<School> schools;
    public String screenName;
    public boolean showAllPosts;
    public String skype;
    public int smoking;
    public int suggestedCount;
    public String tv;
    public String twitter;
    public ArrayList<University> universities;
    public boolean verified;
    public String website;

    public static class Relative {
        public int type;
        public UserProfile user;
    }

    public static class School {
        public String city;
        public String className;
        public int from;
        public int graduation;
        public String name;
        public String speciality;
        public int to;
        public String type;
    }

    public static class University {
        public String chair;
        public String city;
        public String faculty;
        public int graduation;
        public String name;
    }
}
