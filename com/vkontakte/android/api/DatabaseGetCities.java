package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.data.database.City;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class DatabaseGetCities extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(List<City> list);
    }

    public DatabaseGetCities(int country, String q) {
        super("database.getCities");
        param("country_id", country);
        if (q != null && q.length() > 0) {
            param("q", q);
            param("count", 100);
        }
    }

    public Object parse(JSONObject o) {
        try {
            Object result = new ArrayList();
            JSONArray ja = APIUtils.unwrapArray(o, "response").array;
            for (int i = 0; i < ja.length(); i++) {
                result.add(new City(ja.getJSONObject(i)));
            }
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((List) result);
        }
    }
}
