package com.vkontakte.android.api;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FaveGetLinks extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(List<UserProfile> list);
    }

    /* renamed from: com.vkontakte.android.api.FaveGetLinks.1 */
    class C14021 extends APIHandler {
        C14021() {
        }

        public void success(JSONObject o) {
            FaveGetLinks.this.parse(o);
        }

        public void fail(int ecode, String emsg) {
            if (FaveGetLinks.this.callback != null) {
                FaveGetLinks.this.callback.fail(ecode, emsg);
            }
        }
    }

    public FaveGetLinks() {
        super("fave.getLinks");
        param("count", 10000);
        handler(new C14021());
    }

    private UserProfile parseProfile(JSONObject p) throws JSONException {
        UserProfile profile = new UserProfile();
        profile.firstName = p.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION);
        profile.lastName = p.getString(PlusShare.KEY_CALL_TO_ACTION_URL);
        if (!profile.lastName.startsWith("http")) {
            profile.lastName = "http://vk.com" + profile.lastName;
        }
        profile.fullName = p.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
        profile.photo = p.getString("image_src");
        return profile;
    }

    public Object parse(JSONObject o) {
        try {
            Object users = new ArrayList();
            JSONArray a = o.getJSONObject("response").getJSONArray("items");
            for (int i = 0; i < a.length(); i++) {
                users.add(parseProfile(a.getJSONObject(i)));
            }
            return users;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((List) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
