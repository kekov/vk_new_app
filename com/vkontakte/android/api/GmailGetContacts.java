package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import java.util.List;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class GmailGetContacts extends APIRequest {
    private String account;
    Callback callback;
    private String token;

    public interface Callback {
        void fail(int i, String str);

        void success(List<UserProfile> list);
    }

    public GmailGetContacts(String _token, String _account) {
        super("__gmailGetContacts");
        this.token = _token;
        this.account = _account;
    }

    public JSONObject doExec() {
        try {
            return (JSONObject) new JSONTokener(new String(Global.getURL("https://www.google.com/m8/feeds/contacts/" + this.account + "/full?v=3.0&alt=json&max-results=1000&access_token=" + this.token), "UTF-8")).nextValue();
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public Object parse(JSONObject o) {
        try {
            Object result = new ArrayList();
            JSONArray jusers = o.getJSONObject("feed").getJSONArray("entry");
            for (int i = 0; i < jusers.length(); i++) {
                JSONObject ju = jusers.getJSONObject(i);
                if (ju.has("gd$email")) {
                    UserProfile p = new UserProfile();
                    p.extra = ju.getJSONArray("gd$email").getJSONObject(0).getString("address");
                    if (ju.has("gd$name")) {
                        JSONObject name = ju.getJSONObject("gd$name");
                        p.firstName = name.has("gd$givenName") ? name.getJSONObject("gd$givenName").getString("$t") : ACRAConstants.DEFAULT_STRING_VALUE;
                        p.lastName = name.has("gd$familyName") ? name.getJSONObject("gd$familyName").getString("$t") : ACRAConstants.DEFAULT_STRING_VALUE;
                        p.fullName = name.getJSONObject("gd$fullName").getString("$t");
                    } else {
                        p.fullName = p.extra.toString();
                    }
                    String photo = null;
                    JSONArray links = ju.getJSONArray("link");
                    for (int j = 0; j < links.length(); j++) {
                        JSONObject link = links.getJSONObject(j);
                        if (link.getString(WebDialog.DIALOG_PARAM_TYPE).equals("image/*") && link.getString("rel").endsWith("#photo")) {
                            photo = link.getString("href") + "&access_token=" + this.token;
                            break;
                        }
                    }
                    p.photo = photo;
                    p.university = p.extra.toString();
                    result.add(p);
                }
            }
            return result;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((List) result);
        }
    }
}
