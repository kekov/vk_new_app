package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class VideoGet extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, Vector<VideoFile> vector);
    }

    public VideoGet(int uid, int offset, int count, int album) {
        super("video.get");
        param(uid > 0 ? "user_id" : "group_id", Math.abs(uid)).param("offset", offset).param("count", count).param("album_id", album);
        param("extended", 1);
    }

    public Object parse(JSONObject o) {
        try {
            Vector<VideoFile> vf = new Vector();
            int total = 0;
            JSONArray arr = APIUtils.unwrapArray(o, "response").array;
            if (arr != null) {
                total = APIUtils.unwrapArray(o, "response").count;
                for (int i = 0; i < arr.length(); i++) {
                    vf.add(new VideoFile(arr.getJSONObject(i)));
                }
            }
            return new Object[]{Integer.valueOf(total), vf};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (Vector) r[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
