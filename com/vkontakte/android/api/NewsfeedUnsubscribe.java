package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import org.json.JSONObject;

public class NewsfeedUnsubscribe extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success();
    }

    public NewsfeedUnsubscribe(int oid, int id, int type) {
        super("newsfeed.unsubscribe");
        param("owner_id", oid).param("item_id", id);
        switch (type) {
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                param(WebDialog.DIALOG_PARAM_TYPE, "photo");
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                param(WebDialog.DIALOG_PARAM_TYPE, "video");
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                param(WebDialog.DIALOG_PARAM_TYPE, "note");
            case UserListView.TYPE_FAVE /*4*/:
                param(WebDialog.DIALOG_PARAM_TYPE, "topic");
            default:
                param(WebDialog.DIALOG_PARAM_TYPE, "post");
        }
    }

    public Object parse(JSONObject o) {
        try {
            return Boolean.valueOf(true);
        } catch (Exception e) {
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success();
        }
    }
}
