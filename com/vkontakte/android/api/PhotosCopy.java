package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import org.json.JSONObject;

public class PhotosCopy extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i);
    }

    public PhotosCopy(int oid, int pid, String akey) {
        super("photos.copy");
        param("owner_id", oid);
        param("photo_id", pid);
        if (akey != null) {
            param("access_key", akey);
        }
    }

    public Object parse(JSONObject o) {
        try {
            return Integer.valueOf(o.getInt("response"));
        } catch (Exception e) {
            return Integer.valueOf(-1);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success(((Integer) result).intValue());
        }
    }
}
