package com.vkontakte.android.api;

import android.text.TextUtils;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import java.util.List;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class AccountLookupContacts extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<UserProfile> arrayList, ArrayList<UserProfile> arrayList2);
    }

    public AccountLookupContacts(List<String> contacts, String service, String myContact) {
        super("account.lookupContacts");
        param("contacts", TextUtils.join(",", contacts));
        param("service", service);
        if (myContact != null) {
            param("mycontact", myContact);
        }
        param("fields", "photo_100,photo50,city,country,education");
    }

    public Object parse(JSONObject o) {
        try {
            int i;
            UserProfile profile;
            JSONObject jp;
            ArrayList<UserProfile> found = new ArrayList();
            ArrayList<UserProfile> other = new ArrayList();
            o = o.getJSONObject("response");
            JSONArray jfound = o.getJSONArray("found");
            JSONArray jother = o.getJSONArray("other");
            for (i = 0; i < jfound.length(); i++) {
                profile = new UserProfile();
                jp = jfound.getJSONObject(i);
                profile.uid = jp.getInt("id");
                profile.firstName = jp.getString("first_name");
                profile.lastName = jp.getString("last_name");
                profile.fullName = profile.firstName + " " + profile.lastName;
                profile.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                profile.isFriend = jp.optInt("request_sent") == 1;
                profile.university = ACRAConstants.DEFAULT_STRING_VALUE;
                if (jp.optString("university_name", ACRAConstants.DEFAULT_STRING_VALUE).length() > 0) {
                    profile.university = jp.getString("university_name").replace("\r\n", ACRAConstants.DEFAULT_STRING_VALUE).trim();
                    if (jp.optInt("graduation", 0) > 0) {
                        profile.university += String.format(" '%02d", new Object[]{Integer.valueOf(jp.getInt("graduation") % 100)});
                    }
                } else {
                    if (jp.optJSONObject("country") != null) {
                        profile.university = jp.getJSONObject("country").getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                    }
                    if (!(jp.optJSONObject("country") == null || jp.optJSONObject("city") == null)) {
                        profile.university += ", ";
                    }
                    if (jp.optJSONObject("city") != null) {
                        profile.university += jp.getJSONObject("city").getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                    }
                }
                found.add(profile);
            }
            for (i = 0; i < jother.length(); i++) {
                profile = new UserProfile();
                jp = jother.getJSONObject(i);
                profile.uid = -1;
                profile.extra = jp.getString("contact");
                other.add(profile);
            }
            return new Object[]{found, other};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success((ArrayList) r[0], (ArrayList) r[1]);
        }
    }
}
