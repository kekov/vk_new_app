package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import java.util.ArrayList;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MessagesSearchDialogs extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(ArrayList<UserProfile> arrayList);
    }

    public MessagesSearchDialogs(String q) {
        super("messages.searchDialogs");
        param("q", q).param("fields", new StringBuilder(String.valueOf(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec")).append(",online").toString());
    }

    private UserProfile parseProfile(JSONObject p) throws JSONException {
        UserProfile profile = new UserProfile();
        profile.firstName = p.getString("first_name");
        profile.lastName = p.getString("last_name");
        profile.fullName = profile.firstName + " " + profile.lastName;
        profile.photo = p.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
        profile.uid = p.getInt("id");
        profile.online = Global.getUserOnlineStatus(p);
        return profile;
    }

    public Object parse(JSONObject o) {
        try {
            Object users = new ArrayList();
            JSONArray a = o.getJSONArray("response");
            for (int i = 0; i < a.length(); i++) {
                JSONObject e = a.getJSONObject(i);
                if ("profile".equals(e.optString(WebDialog.DIALOG_PARAM_TYPE, "profile"))) {
                    users.add(parseProfile(e));
                }
                if ("chat".equals(e.optString(WebDialog.DIALOG_PARAM_TYPE))) {
                    UserProfile p = new UserProfile();
                    p.uid = 2000000000 + e.getInt("id");
                    String string = e.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                    p.firstName = string;
                    p.fullName = string;
                    p.lastName = ACRAConstants.DEFAULT_STRING_VALUE;
                    users.add(p);
                }
            }
            return users;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            this.callback.success((ArrayList) result);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
