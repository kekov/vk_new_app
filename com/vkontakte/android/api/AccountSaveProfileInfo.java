package com.vkontakte.android.api;

import android.os.Bundle;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserProfile;
import org.json.JSONObject;

public class AccountSaveProfileInfo extends APIRequest {
    Callback callback;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, int i2, String str, String str2);
    }

    public AccountSaveProfileInfo(Bundle info) {
        super("execute.saveProfileInfo");
        if (info.containsKey("first_name")) {
            param("first_name", info.getString("first_name"));
            param("last_name", info.getString("last_name"));
        }
        if (info.containsKey("gender")) {
            param("sex", info.getInt("gender"));
        }
        if (info.containsKey("relation")) {
            param("relation", new StringBuilder(String.valueOf(info.getInt("relation"))).toString());
        }
        if (info.containsKey("relation_partner")) {
            UserProfile p = (UserProfile) info.getParcelable("relation_partner");
            param("relation_partner_id", p != null ? new StringBuilder(String.valueOf(p.uid)).toString() : "0");
        }
        if (info.containsKey("bday")) {
            param("bdate", info.getInt("bday") + "." + info.getInt("bmonth") + "." + info.getInt("byear"));
        }
        if (info.containsKey("bdate_vis")) {
            param("bdate_visibility", new StringBuilder(String.valueOf(info.getInt("bdate_vis"))).toString());
        }
        if (info.containsKey("country_id")) {
            param("country_id", new StringBuilder(String.valueOf(info.getInt("country_id"))).toString());
        }
        if (info.containsKey("city_id")) {
            param("city_id", new StringBuilder(String.valueOf(info.getInt("city_id"))).toString());
        }
    }

    public AccountSaveProfileInfo(int cancelNameReq) {
        super("account.saveProfileInfo");
        param("cancel_request_id", cancelNameReq);
    }

    public Object parse(JSONObject o) {
        try {
            JSONObject r = o.getJSONObject("response");
            if (r.has("name_request")) {
                JSONObject nreq = r.getJSONObject("name_request");
                String status = nreq.getString("status");
                Object obj;
                if ("processing".equals(status)) {
                    obj = new Object[4];
                    obj[0] = Integer.valueOf(1);
                    obj[1] = Integer.valueOf(0);
                    return obj;
                } else if ("declined".equals(status)) {
                    obj = new Object[4];
                    obj[0] = Integer.valueOf(2);
                    obj[1] = Integer.valueOf(0);
                    return obj;
                } else if ("was_accepted".equals(status)) {
                    obj = new Object[4];
                    obj[0] = Integer.valueOf(3);
                    obj[1] = Integer.valueOf(nreq.getInt("repeat_date"));
                    return obj;
                } else if ("was_declined".equals(status)) {
                    obj = new Object[4];
                    obj[0] = Integer.valueOf(4);
                    obj[1] = Integer.valueOf(nreq.getInt("repeat_date"));
                    return obj;
                }
            }
            return new Object[]{Integer.valueOf(0), Integer.valueOf(0), r.optString("new_first"), r.optString("new_last")};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), ((Integer) r[1]).intValue(), (String) r[2], (String) r[3]);
        }
    }
}
