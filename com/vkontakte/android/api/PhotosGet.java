package com.vkontakte.android.api;

import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Log;
import com.vkontakte.android.Photo;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public class PhotosGet extends APIRequest {
    Callback callback;
    int uid;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, Vector<Photo> vector);
    }

    public PhotosGet(int uid, int aid, int offset, int count) {
        this(uid, aid, offset, count, false);
    }

    public PhotosGet(int uid, int aid, int offset, int count, boolean rev) {
        super(aid != 0 ? "photos.get" : "photos.getAll");
        this.uid = uid;
        param("album_id", aid).param("owner_id", uid).param("extended", 1);
        param("photo_sizes", 1);
        param("offset", offset).param("count", count);
        if (rev) {
            param("rev", 1);
        }
    }

    public Object parse(JSONObject o) {
        try {
            JSONArray arr = APIUtils.unwrapArray(o, "response").array;
            Vector<Photo> photos = new Vector();
            if (arr != null) {
                for (int i = 0; i < arr.length(); i++) {
                    photos.add(new Photo(arr.getJSONObject(i)));
                }
            }
            return new Object[]{Integer.valueOf(APIUtils.unwrapArray(o, "response").count), photos};
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return null;
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (Vector) r[1]);
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
