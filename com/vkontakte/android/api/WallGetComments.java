package com.vkontakte.android.api;

import com.facebook.WebDialog;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.APIRequest.ErrorResponse;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.NewsComment;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public class WallGetComments extends APIRequest {
    private static final String REQUEST_PHOTO = "var c=API.photos.getComments({owner_id:%1$d,pid:%2$d,offset:%3$d,count:%4$d,sort:\"desc\"});var p=API.users.get({uids:c@.from_id,fields:\"%5$s\"});return {c:c,p1:p};";
    private static final String REQUEST_POST = "var c=API.wall.getComments({owner_id:%1$d,post_id:%2$d,offset:%3$d,count:%4$d,need_likes:1,sort:\"desc\",photo_sizes:1});var p1=API.getProfiles({uids:c@.uid,fields:\"%5$s\"});var p2=API.getProfiles({uids:c@.reply_to_uid+c@.uid,name_case:\"dat\"});return {c:c,p1:p1,p2u:p2@.uid,p2n:p2@.first_name};";
    private static final String REQUEST_VIDEO = "var c=API.video.getComments({owner_id:%1$d,vid:%2$d,offset:%3$d,count:%4$d,sort:\"desc\"});var p=API.users.get({uids:c@.from_id,fields:\"%5$s\"});return {c:c,p1:p};";
    private static final String[] requests;
    Callback callback;
    Pattern ptn1;
    Pattern ptn2;
    Pattern ptn3;
    int type;

    public interface Callback {
        void fail(int i, String str);

        void success(int i, ArrayList<NewsComment> arrayList, ArrayList<String> arrayList2, int i2, boolean z);
    }

    static {
        requests = new String[]{REQUEST_POST, REQUEST_PHOTO, REQUEST_VIDEO};
    }

    public WallGetComments(int ownerID, int postID, int offset, int count, int type) {
        this(ownerID, postID, offset, count, type, false, null);
    }

    public WallGetComments(int ownerID, int postID, int offset, int count, int type, boolean needLikes, String accessKey) {
        super("execute.getCommentsNew");
        this.ptn1 = Pattern.compile("((?:(?:http|https)://)?[a-zA-Z\u0430-\u044f\u0410-\u042f0-9-]+\\.[a-zA-Z\u0430-\u044f\u0410-\u042f]{2,4}[a-zA-Z/?\\.=#%&-_]+)");
        this.ptn2 = Pattern.compile("\\[id(\\d+)\\|([^\\]]+)\\]");
        this.ptn3 = Pattern.compile("\\[club(\\d+)\\|([^\\]]+)\\]");
        param("owner_id", ownerID);
        param("item_id", postID);
        param("offset", offset);
        param("count", count);
        switch (type) {
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                param(WebDialog.DIALOG_PARAM_TYPE, "photo");
                break;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                param(WebDialog.DIALOG_PARAM_TYPE, "video");
                break;
            default:
                param(WebDialog.DIALOG_PARAM_TYPE, "post");
                break;
        }
        if (needLikes) {
            param("need_likes", 1);
        }
        if (accessKey != null) {
            param("access_key", accessKey);
        }
        this.type = type;
    }

    private NewsComment parsePostComment(JSONObject jc, HashMap<Integer, String> names, HashMap<Integer, String> photos, HashMap<Integer, String> names_dat) throws Exception {
        NewsComment comm = new NewsComment();
        comm.cid = jc.getInt("id");
        comm.uid = jc.getInt("from_id");
        String txt = jc.getString("text");
        comm.text = txt;
        comm.userPhoto = (String) photos.get(Integer.valueOf(comm.uid));
        comm.userName = (String) names.get(Integer.valueOf(comm.uid));
        comm.userRName = (String) names_dat.get(Integer.valueOf(comm.uid));
        if (jc.has("reply_to_user")) {
            comm.respToName = (String) names_dat.get(Integer.valueOf(jc.getInt("reply_to_user")));
        }
        comm.time = Global.timeDiff + jc.getInt("date");
        comm.links = new Vector();
        comm.linkTitles = new Vector();
        Matcher matcher = this.ptn2.matcher(txt);
        while (matcher.find()) {
            comm.links.add("vkontakte://profile/" + matcher.group(1));
            comm.linkTitles.add(matcher.group(2));
        }
        matcher = this.ptn3.matcher(txt);
        while (matcher.find()) {
            comm.links.add("vkontakte://profile/-" + matcher.group(1));
            comm.linkTitles.add(matcher.group(2));
        }
        matcher = this.ptn1.matcher(txt);
        while (matcher.find()) {
            comm.links.add("vklink://view/?" + matcher.group());
            comm.linkTitles.add(matcher.group());
        }
        if (jc.has("likes")) {
            comm.numLikes = jc.getJSONObject("likes").getInt("count");
            comm.isLiked = jc.getJSONObject("likes").getInt("user_likes") == 1;
        }
        if (jc.has("attachments")) {
            JSONArray atts = jc.getJSONArray("attachments");
            for (int i = 0; i < atts.length(); i++) {
                comm.attachments.add(Attachment.parse(atts.getJSONObject(i)));
            }
            Attachment.sort(comm.attachments);
        }
        return comm;
    }

    public Object parse(JSONObject o) {
        try {
            int i;
            HashMap<Integer, String> names = new HashMap();
            HashMap<Integer, String> names_dat = new HashMap();
            HashMap<Integer, String> photos = new HashMap();
            JSONArrayWithCount jcc = APIUtils.unwrapArray(o.getJSONObject("response"), "c");
            JSONArray jcomments = jcc != null ? jcc.array : null;
            JSONArray jprofiles = o.getJSONObject("response").optJSONArray("p1");
            JSONArray uids_dat = o.getJSONObject("response").optJSONArray("p2u");
            JSONArray ndat = o.getJSONObject("response").optJSONArray("p2n");
            ArrayList<NewsComment> comments = new ArrayList();
            ArrayList<String> likes = new ArrayList();
            if (o.getJSONObject("response").has("likes")) {
                JSONArray lp = o.getJSONObject("response").getJSONArray("likes");
                String myPhoto = o.getJSONObject("response").getString("my_photo");
                for (i = 0; i < lp.length(); i++) {
                    String p = lp.getString(i);
                    if (!myPhoto.equals(p)) {
                        likes.add(p);
                    }
                }
                if (likes.size() == 11) {
                    likes.remove(10);
                }
            }
            if (jcomments == null) {
                r17 = new Object[5];
                r17[3] = Integer.valueOf(o.getJSONObject("response").optInt("likes_count", -1));
                r17[4] = Boolean.valueOf(false);
                return r17;
            }
            if (jprofiles != null) {
                for (i = 0; i < jprofiles.length(); i++) {
                    names.put(Integer.valueOf(jprofiles.getJSONObject(i).getInt("id")), new StringBuilder(String.valueOf(jprofiles.getJSONObject(i).getString("first_name"))).append(" ").append(jprofiles.getJSONObject(i).getString("last_name")).toString());
                    photos.put(Integer.valueOf(jprofiles.getJSONObject(i).getInt("id")), jprofiles.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec"));
                }
            }
            if (ndat != null) {
                for (i = 0; i < ndat.length(); i++) {
                    names_dat.put(Integer.valueOf(uids_dat.getInt(i)), ndat.getString(i));
                }
            }
            for (i = jcomments.length() - 1; i >= 0; i--) {
                comments.add(parsePostComment(jcomments.getJSONObject(i), names, photos, names_dat));
            }
            r17 = new Object[5];
            r17[0] = Integer.valueOf(APIUtils.unwrapArray(o.getJSONObject("response"), "c").count);
            r17[1] = comments;
            r17[2] = likes;
            r17[3] = Integer.valueOf(o.getJSONObject("response").optInt("likes_count", -1));
            r17[4] = Boolean.valueOf(true);
            return r17;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return new ErrorResponse(0, "parse error");
        }
    }

    public void invokeCallback(Object result) {
        if (this.callback != null) {
            if (result instanceof ErrorResponse) {
                ErrorResponse er = (ErrorResponse) result;
                this.callback.fail(er.errorCode, er.errorMessage);
                return;
            }
            Object[] r = (Object[]) result;
            this.callback.success(((Integer) r[0]).intValue(), (ArrayList) r[1], (ArrayList) r[2], ((Integer) r[3]).intValue(), ((Boolean) r[4]).booleanValue());
        }
    }

    public APIRequest setCallback(Callback cb) {
        this.callback = cb;
        return this;
    }
}
