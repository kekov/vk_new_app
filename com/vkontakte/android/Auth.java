package com.vkontakte.android;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.vkontakte.android.api.GetWallInfo;
import com.vkontakte.android.api.GetWallInfo.Callback;
import java.util.HashMap;
import java.util.Random;
import org.acra.ACRAConstants;

public class Auth {
    public static final String ACCOUNT_TYPE = "com.vkontakte.account";
    public static final int API_ID = 2274003;
    public static final String API_SECRET = "hHbZxrka2uZ6jB1inYsH";
    public static int REAUTH_CANCELED;
    public static int REAUTH_ERROR_INCORRECT_PASSWORD;
    public static int REAUTH_ERROR_NETWORK;
    public static int REAUTH_OPEN_BROWSER;
    public static int REAUTH_SUCCESS;
    private static String captchaSid;
    public static String lastError;
    static String nonce;

    /* renamed from: com.vkontakte.android.Auth.1 */
    class C01961 implements Runnable {
        private final /* synthetic */ AuthResultReceiver val$act;
        private final /* synthetic */ String val$login;
        private final /* synthetic */ String val$pass;

        C01961(String str, String str2, AuthResultReceiver authResultReceiver) {
            this.val$login = str;
            this.val$pass = str2;
            this.val$act = authResultReceiver;
        }

        public void run() {
            Auth.createNonce();
            HashMap<String, String> args = new HashMap();
            args.put("username", this.val$login);
            args.put("password", this.val$pass);
            HashMap<String, String> out = new HashMap();
            int r = Auth.doAuth("password", args, true, out);
            if (this.val$act != null) {
                this.val$act.authDone(r, out);
            }
        }
    }

    /* renamed from: com.vkontakte.android.Auth.2 */
    class C01972 implements Runnable {
        private final /* synthetic */ AuthResultReceiver val$act;
        private final /* synthetic */ String val$code;
        private final /* synthetic */ String val$sid;

        C01972(String str, String str2, AuthResultReceiver authResultReceiver) {
            this.val$sid = str;
            this.val$code = str2;
            this.val$act = authResultReceiver;
        }

        public void run() {
            Auth.createNonce();
            HashMap<String, String> args = new HashMap();
            args.put("sid", this.val$sid);
            args.put("code", this.val$code);
            HashMap<String, String> out = new HashMap();
            int r = Auth.doAuth("restore_code", args, false, out);
            if (this.val$act != null) {
                this.val$act.authDone(r, out);
            }
        }
    }

    public interface AuthResultReceiver {
        void authDone(int i, HashMap<String, String> hashMap);
    }

    /* renamed from: com.vkontakte.android.Auth.3 */
    class C12713 implements Callback {
        private final /* synthetic */ boolean[] val$result;

        C12713(boolean[] zArr) {
            this.val$result = zArr;
        }

        public void success(String name, String photo, boolean exportTwi, boolean exportFb, int time, int intro, int country, boolean hasNewItems) {
            VKApplication.context.getSharedPreferences(null, 0).edit().putString("username", name).putString("userphoto", photo).putInt("usercountry", country).putInt("intro", intro).putBoolean("export_twitter_avail", exportTwi).putBoolean("export_facebook_avail", exportFb).commit();
            VKApplication.context.getSharedPreferences("stickers", 0).edit().putBoolean("has_new", hasNewItems).commit();
            try {
                Account account = new Account(name, Auth.ACCOUNT_TYPE);
                boolean accountCreated = AccountManager.get(VKApplication.context).addAccountExplicitly(account, null, null);
                ContentResolver.setIsSyncable(account, "com.android.contacts", 1);
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }

        public void fail(int ecode, String emsg) {
            Log.m530w("vk", "Get user info FAILED!");
            this.val$result[0] = false;
        }
    }

    static {
        REAUTH_OPEN_BROWSER = 4;
        REAUTH_CANCELED = 3;
        REAUTH_ERROR_INCORRECT_PASSWORD = 2;
        REAUTH_ERROR_NETWORK = 1;
        REAUTH_SUCCESS = 0;
        lastError = ACRAConstants.DEFAULT_STRING_VALUE;
    }

    public static void authorizeAsync(String login, String pass, AuthResultReceiver act) {
        new Thread(new C01961(login, pass, act)).start();
    }

    public static void authorizeRestoreAsync(String sid, String code, AuthResultReceiver act) {
        new Thread(new C01972(sid, code, act)).start();
    }

    public static boolean doReauth() {
        if (doAuth("password", null, true, null) == REAUTH_ERROR_INCORRECT_PASSWORD) {
            LongPollService.onReauthError();
        }
        return true;
    }

    private static void createNonce() {
        char[] chars = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
        nonce = ACRAConstants.DEFAULT_STRING_VALUE;
        Random r = new Random();
        for (int i = 0; i < 16; i++) {
            nonce += chars[r.nextInt(chars.length)];
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int doAuth(java.lang.String r15, java.util.HashMap<java.lang.String, java.lang.String> r16, boolean r17, java.util.HashMap<java.lang.String, java.lang.String> r18) {
        /*
        if (r16 == 0) goto L_0x0016;
    L_0x0002:
        r12 = "login";
        r0 = r16;
        r12 = r0.containsKey(r12);	 Catch:{ Exception -> 0x0181 }
        if (r12 == 0) goto L_0x003f;
    L_0x000c:
        r12 = "login";
        r0 = r16;
        r12 = r0.get(r12);	 Catch:{ Exception -> 0x0181 }
        if (r12 != 0) goto L_0x003f;
    L_0x0016:
        r12 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x0181 }
        r13 = 0;
        r14 = 0;
        r7 = r12.getSharedPreferences(r13, r14);	 Catch:{ Exception -> 0x0181 }
        r12 = "vk";
        r13 = "Login/pass not available, starting AuthActivity";
        com.vkontakte.android.Log.m528i(r12, r13);	 Catch:{ Exception -> 0x0181 }
        r12 = com.vkontakte.android.AuthActivity.active;	 Catch:{ Exception -> 0x0181 }
        if (r12 != 0) goto L_0x003c;
    L_0x0029:
        r2 = new android.content.Intent;	 Catch:{ Exception -> 0x0181 }
        r12 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x0181 }
        r13 = com.vkontakte.android.AuthActivity.class;
        r2.<init>(r12, r13);	 Catch:{ Exception -> 0x0181 }
        r12 = 805306368; // 0x30000000 float:4.656613E-10 double:3.97874211E-315;
        r2.addFlags(r12);	 Catch:{ Exception -> 0x0181 }
        r12 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x0181 }
        r12.startActivity(r2);	 Catch:{ Exception -> 0x0181 }
    L_0x003c:
        r12 = REAUTH_ERROR_INCORRECT_PASSWORD;	 Catch:{ Exception -> 0x0181 }
    L_0x003e:
        return r12;
    L_0x003f:
        r12 = new android.net.Uri$Builder;	 Catch:{ Exception -> 0x0181 }
        r12.<init>();	 Catch:{ Exception -> 0x0181 }
        r13 = "https";
        r12 = r12.scheme(r13);	 Catch:{ Exception -> 0x0181 }
        r13 = "oauth.vk.com";
        r12 = r12.authority(r13);	 Catch:{ Exception -> 0x0181 }
        r13 = "/token";
        r12 = r12.path(r13);	 Catch:{ Exception -> 0x0181 }
        r13 = "grant_type";
        r12 = r12.appendQueryParameter(r13, r15);	 Catch:{ Exception -> 0x0181 }
        r13 = "scope";
        r14 = "nohttps,all";
        r12 = r12.appendQueryParameter(r13, r14);	 Catch:{ Exception -> 0x0181 }
        r13 = "client_id";
        r14 = "2274003";
        r12 = r12.appendQueryParameter(r13, r14);	 Catch:{ Exception -> 0x0181 }
        r13 = "client_secret";
        r14 = "hHbZxrka2uZ6jB1inYsH";
        r1 = r12.appendQueryParameter(r13, r14);	 Catch:{ Exception -> 0x0181 }
        r12 = captchaSid;	 Catch:{ Exception -> 0x0181 }
        if (r12 == 0) goto L_0x008a;
    L_0x0078:
        r12 = "captcha_sid";
        r13 = captchaSid;	 Catch:{ Exception -> 0x0181 }
        r12 = r1.appendQueryParameter(r12, r13);	 Catch:{ Exception -> 0x0181 }
        r13 = "captcha_key";
        r14 = com.vkontakte.android.CaptchaActivity.lastKey;	 Catch:{ Exception -> 0x0181 }
        r12.appendQueryParameter(r13, r14);	 Catch:{ Exception -> 0x0181 }
        r12 = 0;
        captchaSid = r12;	 Catch:{ Exception -> 0x0181 }
    L_0x008a:
        r6 = r16.keySet();	 Catch:{ Exception -> 0x0181 }
        r13 = r6.iterator();	 Catch:{ Exception -> 0x0181 }
    L_0x0092:
        r12 = r13.hasNext();	 Catch:{ Exception -> 0x0181 }
        if (r12 != 0) goto L_0x016e;
    L_0x0098:
        r12 = r1.build();	 Catch:{ Exception -> 0x0181 }
        r10 = r12.toString();	 Catch:{ Exception -> 0x0181 }
        r8 = com.vkontakte.android.Global.getURL(r10);	 Catch:{ Exception -> 0x0181 }
        r9 = new java.lang.String;	 Catch:{ Exception -> 0x0181 }
        r12 = "UTF-8";
        r9.<init>(r8, r12);	 Catch:{ Exception -> 0x0181 }
        r12 = com.vkontakte.android.APIController.API_DEBUG;	 Catch:{ Exception -> 0x0181 }
        if (r12 == 0) goto L_0x00c3;
    L_0x00af:
        r12 = "vk";
        r13 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0181 }
        r14 = "Auth Result = ";
        r13.<init>(r14);	 Catch:{ Exception -> 0x0181 }
        r13 = r13.append(r9);	 Catch:{ Exception -> 0x0181 }
        r13 = r13.toString();	 Catch:{ Exception -> 0x0181 }
        com.vkontakte.android.Log.m525d(r12, r13);	 Catch:{ Exception -> 0x0181 }
    L_0x00c3:
        r12 = new org.json.JSONTokener;	 Catch:{ Exception -> 0x0181 }
        r12.<init>(r9);	 Catch:{ Exception -> 0x0181 }
        r3 = r12.nextValue();	 Catch:{ Exception -> 0x0181 }
        r3 = (org.json.JSONObject) r3;	 Catch:{ Exception -> 0x0181 }
        if (r3 == 0) goto L_0x00dc;
    L_0x00d0:
        if (r18 == 0) goto L_0x00dc;
    L_0x00d2:
        r5 = r3.keys();	 Catch:{ Exception -> 0x0181 }
    L_0x00d6:
        r12 = r5.hasNext();	 Catch:{ Exception -> 0x0181 }
        if (r12 != 0) goto L_0x018b;
    L_0x00dc:
        r12 = "error";
        r12 = r3.has(r12);	 Catch:{ Exception -> 0x0181 }
        if (r12 == 0) goto L_0x01c6;
    L_0x00e4:
        r12 = "vk";
        r13 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0181 }
        r14 = "Auth Error: ";
        r13.<init>(r14);	 Catch:{ Exception -> 0x0181 }
        r14 = "error";
        r14 = r3.getString(r14);	 Catch:{ Exception -> 0x0181 }
        r13 = r13.append(r14);	 Catch:{ Exception -> 0x0181 }
        r13 = r13.toString();	 Catch:{ Exception -> 0x0181 }
        com.vkontakte.android.Log.m526e(r12, r13);	 Catch:{ Exception -> 0x0181 }
        r12 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0181 }
        r13 = "error";
        r13 = r3.getString(r13);	 Catch:{ Exception -> 0x0181 }
        r13 = java.lang.String.valueOf(r13);	 Catch:{ Exception -> 0x0181 }
        r12.<init>(r13);	 Catch:{ Exception -> 0x0181 }
        r13 = " / ";
        r12 = r12.append(r13);	 Catch:{ Exception -> 0x0181 }
        r13 = "error_description";
        r13 = r3.optString(r13);	 Catch:{ Exception -> 0x0181 }
        r12 = r12.append(r13);	 Catch:{ Exception -> 0x0181 }
        r12 = r12.toString();	 Catch:{ Exception -> 0x0181 }
        lastError = r12;	 Catch:{ Exception -> 0x0181 }
        r12 = "need_captcha";
        r13 = "error";
        r13 = r3.optString(r13);	 Catch:{ Exception -> 0x0181 }
        r12 = r12.equals(r13);	 Catch:{ Exception -> 0x0181 }
        if (r12 == 0) goto L_0x01a8;
    L_0x0131:
        r12 = "captcha_sid";
        r12 = r3.getString(r12);	 Catch:{ Exception -> 0x0181 }
        captchaSid = r12;	 Catch:{ Exception -> 0x0181 }
        r2 = new android.content.Intent;	 Catch:{ Exception -> 0x0181 }
        r12 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x0181 }
        r13 = com.vkontakte.android.CaptchaActivity.class;
        r2.<init>(r12, r13);	 Catch:{ Exception -> 0x0181 }
        r12 = 268435456; // 0x10000000 float:2.5243549E-29 double:1.32624737E-315;
        r2.addFlags(r12);	 Catch:{ Exception -> 0x0181 }
        r12 = "url";
        r13 = "captcha_img";
        r13 = r3.getString(r13);	 Catch:{ Exception -> 0x0181 }
        r2.putExtra(r12, r13);	 Catch:{ Exception -> 0x0181 }
        r12 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x0181 }
        r12.startActivity(r2);	 Catch:{ Exception -> 0x0181 }
    L_0x0157:
        r12 = com.vkontakte.android.CaptchaActivity.isReady;	 Catch:{ Exception -> 0x0181 }
        if (r12 == 0) goto L_0x019c;
    L_0x015b:
        r12 = 0;
        com.vkontakte.android.CaptchaActivity.isReady = r12;	 Catch:{ Exception -> 0x0181 }
        r12 = com.vkontakte.android.CaptchaActivity.lastKey;	 Catch:{ Exception -> 0x0181 }
        if (r12 == 0) goto L_0x016a;
    L_0x0162:
        r12 = com.vkontakte.android.CaptchaActivity.lastKey;	 Catch:{ Exception -> 0x0181 }
        r12 = r12.length();	 Catch:{ Exception -> 0x0181 }
        if (r12 != 0) goto L_0x01a2;
    L_0x016a:
        r12 = REAUTH_CANCELED;	 Catch:{ Exception -> 0x0181 }
        goto L_0x003e;
    L_0x016e:
        r4 = r13.next();	 Catch:{ Exception -> 0x0181 }
        r4 = (java.lang.String) r4;	 Catch:{ Exception -> 0x0181 }
        r0 = r16;
        r12 = r0.get(r4);	 Catch:{ Exception -> 0x0181 }
        r12 = (java.lang.String) r12;	 Catch:{ Exception -> 0x0181 }
        r1.appendQueryParameter(r4, r12);	 Catch:{ Exception -> 0x0181 }
        goto L_0x0092;
    L_0x0181:
        r11 = move-exception;
        r12 = "vk";
        com.vkontakte.android.Log.m532w(r12, r11);
    L_0x0187:
        r12 = REAUTH_ERROR_NETWORK;
        goto L_0x003e;
    L_0x018b:
        r4 = r5.next();	 Catch:{ Exception -> 0x0181 }
        r4 = (java.lang.String) r4;	 Catch:{ Exception -> 0x0181 }
        r12 = r3.getString(r4);	 Catch:{ Exception -> 0x0181 }
        r0 = r18;
        r0.put(r4, r12);	 Catch:{ Exception -> 0x0181 }
        goto L_0x00d6;
    L_0x019c:
        r12 = 100;
        java.lang.Thread.sleep(r12);	 Catch:{ Exception -> 0x0181 }
        goto L_0x0157;
    L_0x01a2:
        r12 = doAuth(r15, r16, r17, r18);	 Catch:{ Exception -> 0x0181 }
        goto L_0x003e;
    L_0x01a8:
        r12 = "need_validation";
        r13 = "error";
        r13 = r3.optString(r13);	 Catch:{ Exception -> 0x0181 }
        r12 = r12.equals(r13);	 Catch:{ Exception -> 0x0181 }
        if (r12 == 0) goto L_0x01c2;
    L_0x01b6:
        r12 = "redirect_uri";
        r12 = r3.getString(r12);	 Catch:{ Exception -> 0x0181 }
        lastError = r12;	 Catch:{ Exception -> 0x0181 }
        r12 = REAUTH_OPEN_BROWSER;	 Catch:{ Exception -> 0x0181 }
        goto L_0x003e;
    L_0x01c2:
        r12 = REAUTH_ERROR_INCORRECT_PASSWORD;	 Catch:{ Exception -> 0x0181 }
        goto L_0x003e;
    L_0x01c6:
        r12 = "access_token";
        r12 = r3.has(r12);	 Catch:{ Exception -> 0x0181 }
        if (r12 == 0) goto L_0x0187;
    L_0x01ce:
        r12 = "access_token";
        r12 = r3.getString(r12);	 Catch:{ Exception -> 0x0181 }
        r13 = "secret";
        r13 = r3.getString(r13);	 Catch:{ Exception -> 0x0181 }
        r14 = "user_id";
        r14 = r3.getInt(r14);	 Catch:{ Exception -> 0x0181 }
        r0 = r17;
        r12 = setData(r12, r13, r14, r0);	 Catch:{ Exception -> 0x0181 }
        if (r12 == 0) goto L_0x0187;
    L_0x01e8:
        r12 = REAUTH_SUCCESS;	 Catch:{ Exception -> 0x0181 }
        goto L_0x003e;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.Auth.doAuth(java.lang.String, java.util.HashMap, boolean, java.util.HashMap):int");
    }

    public static boolean setData(String accessToken, String secret, int userID, boolean persist) {
        SharedPreferences prefs = VKApplication.context.getSharedPreferences(null, 0);
        boolean isReauth = false;
        if (persist && prefs.contains("reauth_uid")) {
            int ru = prefs.getInt("reauth_uid", 0);
            if (ru != userID) {
                Log.m526e("vk", "UserID should be " + ru + " but is " + userID + ", forcing full reauth");
                LongPollService.logOut(true);
                prefs.edit().remove("reauth_uid").commit();
            } else {
                isReauth = true;
            }
        }
        Global.uid = userID;
        Global.accessToken = accessToken;
        Global.secret = secret;
        Global.authOK = true;
        if (!persist) {
            return true;
        }
        boolean needCreateAccount;
        if (isReauth) {
            needCreateAccount = false;
        } else {
            try {
                AccountManager am = AccountManager.get(VKApplication.context);
                Account[] acc = am.getAccountsByType(ACCOUNT_TYPE);
                if (acc.length > 0) {
                    am.removeAccount(acc[0], null, null);
                }
                needCreateAccount = true;
            } catch (Exception e) {
                needCreateAccount = false;
            }
        }
        SharedPreferences prefs1 = PreferenceManager.getDefaultSharedPreferences(VKApplication.context);
        C2DM.start();
        boolean[] result = new boolean[]{true};
        if (needCreateAccount) {
            new GetWallInfo(Global.uid).setCallback(new C12713(result)).execSync();
        }
        if (result[0]) {
            VKApplication.context.getSharedPreferences(null, 0).edit().putInt("uid", Global.uid).putString("sid", Global.accessToken).putString("secret", Global.secret).putBoolean("new_auth", true).commit();
        } else {
            Global.accessToken = null;
            Global.uid = 0;
            Global.secret = null;
            Global.authOK = false;
        }
        return result[0];
    }

    public static int getCurrentSyncOption(Context context) {
        try {
            AccountManager am = AccountManager.get(VKApplication.context);
            Account[] accounts = am.getAccountsByType(ACCOUNT_TYPE);
            if (accounts.length == 0) {
                am.addAccountExplicitly(new Account(VKApplication.context.getSharedPreferences(null, 0).getString("username", ACRAConstants.DEFAULT_STRING_VALUE), ACCOUNT_TYPE), null, null);
                accounts = new Account[]{account};
            }
            boolean syncEnabled = Boolean.valueOf(ContentResolver.getSyncAutomatically(accounts[0], "com.android.contacts")).booleanValue();
            boolean syncAll = context.getApplicationContext().getSharedPreferences(null, 0).getBoolean("sync_all", false);
            if (!syncEnabled) {
                return 2;
            }
            if (syncEnabled && !syncAll) {
                return 1;
            }
            if (syncEnabled && syncAll) {
                return 0;
            }
            return -1;
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }
}
