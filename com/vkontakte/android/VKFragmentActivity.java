package com.vkontakte.android;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

public class VKFragmentActivity extends SherlockFragmentActivity {

    /* renamed from: com.vkontakte.android.VKFragmentActivity.1 */
    class C05251 implements Runnable {
        C05251() {
        }

        public void run() {
            try {
                int btnId = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
                if (btnId == 0) {
                    btnId = C0436R.id.abs__action_bar_title;
                }
                if (btnId != 0) {
                    VKFragmentActivity.this.setTextViewMarquee((TextView) VKFragmentActivity.this.findViewById(btnId));
                }
                btnId = Resources.getSystem().getIdentifier("action_bar_subtitle", "id", "android");
                if (btnId == 0) {
                    btnId = C0436R.id.abs__action_bar_subtitle;
                }
                if (btnId != 0) {
                    VKFragmentActivity.this.setTextViewMarquee((TextView) VKFragmentActivity.this.findViewById(btnId));
                }
                if (VERSION.SDK_INT < 17) {
                    btnId = Resources.getSystem().getIdentifier("action_bar", "id", "android");
                    if (btnId == 0) {
                        btnId = C0436R.id.abs__action_bar;
                    }
                    if (btnId != 0) {
                        ViewGroup vg = (ViewGroup) VKFragmentActivity.this.findViewById(btnId);
                        for (int i = 0; i < vg.getChildCount(); i++) {
                            if (vg.getChildAt(i) instanceof LinearLayout) {
                                vg.getChildAt(i).setBackgroundDrawable(null);
                                return;
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
        if (!isTaskRoot() && getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        }
        onBackPressed();
        return true;
    }

    public void onPause() {
        super.onPause();
        AppStateTracker.onActivityPaused();
    }

    public void onResume() {
        super.onResume();
        AppStateTracker.onActivityResumed(this);
    }

    public void onConfigurationChanged(Configuration cfg) {
        super.onConfigurationChanged(cfg);
        setTitleMarquee();
    }

    public void setTitle(CharSequence title) {
        super.setTitle(title);
        setTitleMarquee();
    }

    public void setTitle(int res) {
        super.setTitle(res);
        setTitleMarquee();
    }

    private void setTitleMarquee() {
        if (getWindow().getDecorView() != null) {
            getWindow().getDecorView().postDelayed(new C05251(), 100);
        }
    }

    private void setTextViewMarquee(TextView t) {
        t.setEllipsize(TruncateAt.MARQUEE);
        t.setSelected(true);
        t.setHorizontalFadingEdgeEnabled(true);
        t.setFadingEdgeLength(Global.scale(10.0f));
        t.setMarqueeRepeatLimit(2);
    }
}
