package com.vkontakte.android;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class PhotoAttachView extends LinearLayout {
    public PhotoAttachView(Context context, String photoURL, String bigURL) {
        super(context);
        ImageView iv = new ImageView(context);
        iv.setTag(photoURL);
        addView(iv);
    }

    public void load() {
        Log.m525d("vk", "PhotoAttachView load");
        new ImageLoader().add((ImageView) getChildAt(0), false, 0).start();
    }

    public PhotoAttachView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
