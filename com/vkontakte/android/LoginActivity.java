package com.vkontakte.android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Auth.AuthResultReceiver;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.ui.Fonts;
import java.util.HashMap;
import org.acra.ACRAConstants;

public class LoginActivity extends VKActivity implements AuthResultReceiver {
    private static final int RESTORE_RESULT = 203;
    private static final int VALIDATION_RESULT = 202;
    private ProgressDialog progress;

    /* renamed from: com.vkontakte.android.LoginActivity.1 */
    class C02901 implements OnEditorActionListener {
        C02901() {
        }

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            LoginActivity.this.doAuth();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.LoginActivity.2 */
    class C02912 implements OnClickListener {
        C02912() {
        }

        public void onClick(View v) {
            LoginActivity.this.doAuth();
        }
    }

    /* renamed from: com.vkontakte.android.LoginActivity.3 */
    class C02923 implements OnClickListener {
        C02923() {
        }

        public void onClick(View v) {
            LoginActivity.this.startActivityForResult(new Intent(LoginActivity.this, RestoreActivity.class), LoginActivity.RESTORE_RESULT);
        }
    }

    /* renamed from: com.vkontakte.android.LoginActivity.4 */
    class C02934 implements Runnable {
        private final /* synthetic */ int val$result;

        C02934(int i) {
            this.val$result = i;
        }

        public void run() {
            LoginActivity.this.findViewById(C0436R.id.auth_btn).setEnabled(true);
            LoginActivity.this.progress.dismiss();
            if (this.val$result == Auth.REAUTH_CANCELED) {
                return;
            }
            if (this.val$result == Auth.REAUTH_OPEN_BROWSER) {
                LoginActivity.this.openBrowser(Auth.lastError);
                return;
            }
            String msg = LoginActivity.this.getResources().getString(this.val$result == Auth.REAUTH_ERROR_INCORRECT_PASSWORD ? C0436R.string.auth_error : C0436R.string.auth_error_network);
            if (this.val$result == Auth.REAUTH_ERROR_INCORRECT_PASSWORD) {
                msg = new StringBuilder(String.valueOf(msg)).append(" (").append(LoginActivity.this.getResources().getString(C0436R.string.error)).append(": ").append(Auth.lastError).append(")").toString();
            }
            new Builder(LoginActivity.this).setMessage(msg).setTitle(C0436R.string.auth_error_title).setIcon(VERSION.SDK_INT >= 11 ? 0 : ACRAConstants.DEFAULT_DIALOG_ICON).setPositiveButton(C0436R.string.ok, null).show();
        }
    }

    /* renamed from: com.vkontakte.android.LoginActivity.5 */
    class C02945 implements Runnable {
        private final /* synthetic */ Intent val$intent;

        C02945(Intent intent) {
            this.val$intent = intent;
        }

        public void run() {
            Auth.setData(this.val$intent.getStringExtra(WebDialog.DIALOG_PARAM_ACCESS_TOKEN), this.val$intent.getStringExtra("secret"), this.val$intent.getIntExtra("user_id", 0), true);
            LoginActivity.this.authDone(Auth.REAUTH_SUCCESS, null);
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView((int) C0436R.layout.login);
        this.progress = new ProgressDialog(this);
        this.progress.setMessage(getResources().getString(C0436R.string.loading));
        this.progress.setCancelable(false);
        ((EditText) findViewById(C0436R.id.auth_pass)).setOnEditorActionListener(new C02901());
        findViewById(C0436R.id.auth_btn).setOnClickListener(new C02912());
        findViewById(C0436R.id.auth_forgot).setOnClickListener(new C02923());
        ((EditText) findViewById(C0436R.id.auth_pass)).setTypeface(Typeface.SANS_SERIF);
        ((TextView) findViewById(C0436R.id.auth_forgot)).setTypeface(Fonts.getRobotoLight());
    }

    private void doAuth() {
        String login = ((EditText) findViewById(C0436R.id.auth_login)).getText().toString();
        String pass = ((EditText) findViewById(C0436R.id.auth_pass)).getText().toString();
        if (login.length() != 0 && pass.length() != 0) {
            findViewById(C0436R.id.auth_btn).setEnabled(false);
            this.progress.show();
            this.progress.getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
            Auth.authorizeAsync(login, pass, this);
        }
    }

    public void authDone(int result, HashMap<String, String> hashMap) {
        if (result == Auth.REAUTH_SUCCESS) {
            setResult(-1);
            finish();
            return;
        }
        runOnUiThread(new C02934(result));
    }

    private void openBrowser(String url) {
        Intent intent = new Intent(this, ValidationActivity.class);
        intent.putExtra(PlusShare.KEY_CALL_TO_ACTION_URL, url);
        intent.putExtra("return_result", true);
        startActivityForResult(intent, VALIDATION_RESULT);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == VALIDATION_RESULT && resultCode == -1) {
            this.progress.show();
            new Thread(new C02945(intent)).start();
        }
        if (requestCode == RESTORE_RESULT && resultCode == -1) {
            authDone(Auth.REAUTH_SUCCESS, null);
        }
        if (requestCode == RESTORE_RESULT && resultCode == 1) {
            openBrowser("https://oauth.vk.com/restore?scope=nohttps,all&client_id=2274003&client_secret=hHbZxrka2uZ6jB1inYsH");
        }
    }
}
