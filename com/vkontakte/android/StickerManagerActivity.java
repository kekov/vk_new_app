package com.vkontakte.android;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.StoreGetPurchases;
import com.vkontakte.android.api.StoreReorderProducts;
import com.vkontakte.android.api.StoreSetActive;
import com.vkontakte.android.api.StoreSetActive.Callback;
import com.vkontakte.android.data.StickerPack;
import com.vkontakte.android.data.Stickers;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.MultiSectionAdapter;
import com.vkontakte.android.ui.PhotoView;
import com.vkontakte.android.ui.ReorderableListView;
import com.vkontakte.android.ui.ReorderableListView.DragListener;
import com.vkontakte.android.ui.ReorderableListView.DraggableChecker;
import com.vkontakte.android.ui.ReorderableListView.Swappable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.acra.ACRAConstants;

public class StickerManagerActivity extends VKActivity {
    private OnClickListener activationClickListener;
    private ArrayList<StickerPack> active;
    private StickerAdapter adapter;
    private FrameLayout content;
    private APIRequest currentReq;
    private int dragPrevPos;
    private int draggingItem;
    private ErrorView error;
    private ListImageLoaderWrapper imgLoader;
    private ArrayList<StickerPack> inactive;
    private ListView list;
    private FrameLayout listWrap;
    private ProgressBar progress;
    private APIRequest reorderReq;

    /* renamed from: com.vkontakte.android.StickerManagerActivity.1 */
    class C05041 implements OnClickListener {

        /* renamed from: com.vkontakte.android.StickerManagerActivity.1.1 */
        class C05031 implements DialogInterface.OnClickListener {
            private final /* synthetic */ StickerPack val$pack;

            C05031(StickerPack stickerPack) {
                this.val$pack = stickerPack;
            }

            public void onClick(DialogInterface dialog, int which) {
                StickerManagerActivity.this.setActiveState(this.val$pack, false);
            }
        }

        C05041() {
        }

        public void onClick(View v) {
            boolean activate;
            StickerPack pack = (StickerPack) v.getTag();
            if (StickerManagerActivity.this.active.contains(pack)) {
                activate = false;
            } else {
                activate = true;
            }
            if (activate) {
                StickerManagerActivity.this.setActiveState(pack, true);
            } else {
                new Builder(StickerManagerActivity.this).setTitle(C0436R.string.confirm).setMessage(C0436R.string.stickers_deactivate_confirm).setPositiveButton(C0436R.string.yes, new C05031(pack)).setNegativeButton(C0436R.string.no, null).show();
            }
        }
    }

    /* renamed from: com.vkontakte.android.StickerManagerActivity.3 */
    class C05053 implements OnClickListener {
        C05053() {
        }

        public void onClick(View v) {
            StickerManagerActivity.this.error.setVisibility(8);
            StickerManagerActivity.this.progress.setVisibility(0);
            StickerManagerActivity.this.loadData();
        }
    }

    /* renamed from: com.vkontakte.android.StickerManagerActivity.2 */
    class C13592 implements DragListener {
        C13592() {
        }

        public void onDragStart(long id) {
            for (int i = 0; i < StickerManagerActivity.this.active.size(); i++) {
                if (((long) ((StickerPack) StickerManagerActivity.this.active.get(i)).id) == id) {
                    StickerManagerActivity.this.dragPrevPos = i;
                    return;
                }
            }
        }

        public void onDragDrop(long id) {
            int pos = -1;
            for (int i = 0; i < StickerManagerActivity.this.active.size(); i++) {
                if (((long) ((StickerPack) StickerManagerActivity.this.active.get(i)).id) == id) {
                    pos = i;
                    break;
                }
            }
            if (pos == -1) {
                return;
            }
            if (pos == 0) {
                StickerManagerActivity.this.sendReorder((int) id, ((StickerPack) StickerManagerActivity.this.active.get(1)).id, -1);
            } else {
                StickerManagerActivity.this.sendReorder((int) id, -1, ((StickerPack) StickerManagerActivity.this.active.get(pos - 1)).id);
            }
        }
    }

    /* renamed from: com.vkontakte.android.StickerManagerActivity.4 */
    class C13604 implements Callback {
        private final /* synthetic */ boolean val$activate;
        private final /* synthetic */ StickerPack val$pack;

        C13604(boolean z, StickerPack stickerPack) {
            this.val$activate = z;
            this.val$pack = stickerPack;
        }

        public void success() {
            if (this.val$activate) {
                StickerManagerActivity.this.inactive.remove(this.val$pack);
                StickerManagerActivity.this.active.add(this.val$pack);
                ArrayList<Integer> builtIn = new ArrayList();
                try {
                    for (String file : VKApplication.context.getAssets().list("stickers")) {
                        try {
                            builtIn.add(Integer.valueOf(Integer.parseInt(file.split("\\.")[0])));
                        } catch (NumberFormatException e) {
                        }
                    }
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                }
                if (builtIn.contains(Integer.valueOf(this.val$pack.id))) {
                    Intent intent = new Intent(StickerManagerActivity.this, StickerDownloaderService.class);
                    intent.putExtra("id", this.val$pack.id);
                    intent.putExtra(PlusShare.KEY_CALL_TO_ACTION_URL, StickerManagerActivity.this.getSharedPreferences("stickers", 0).getString("content" + this.val$pack.id, ACRAConstants.DEFAULT_STRING_VALUE));
                    intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, this.val$pack.title);
                    intent.putExtra("silent", true);
                    StickerManagerActivity.this.startService(intent);
                }
            } else {
                Stickers.deleteDownloadedPack(this.val$pack.id);
                StickerManagerActivity.this.active.remove(this.val$pack);
                StickerManagerActivity.this.inactive.add(0, this.val$pack);
            }
            StickerManagerActivity.this.updateList();
            StickerManagerActivity.this.applyChanges();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(StickerManagerActivity.this, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.StickerManagerActivity.5 */
    class C13615 implements StoreGetPurchases.Callback {
        C13615() {
        }

        public void success(List<StickerPack> packs) {
            StickerManagerActivity.this.currentReq = null;
            Stickers.doUpdateInfo(packs);
            for (StickerPack pack : packs) {
                if (pack.state == 4 || pack.state == 3) {
                    StickerManagerActivity.this.active.add(pack);
                } else if (pack.state == 5) {
                    StickerManagerActivity.this.inactive.add(pack);
                }
            }
            StickerManagerActivity.this.updateList();
            Global.showViewAnimated(StickerManagerActivity.this.listWrap, true, PhotoView.THUMB_ANIM_DURATION);
            Global.showViewAnimated(StickerManagerActivity.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
        }

        public void fail(int ecode, String emsg) {
            StickerManagerActivity.this.currentReq = null;
            StickerManagerActivity.this.error.setErrorInfo(ecode, emsg);
            Global.showViewAnimated(StickerManagerActivity.this.error, true, PhotoView.THUMB_ANIM_DURATION);
            Global.showViewAnimated(StickerManagerActivity.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
        }
    }

    /* renamed from: com.vkontakte.android.StickerManagerActivity.6 */
    class C13626 implements StoreReorderProducts.Callback {
        private final /* synthetic */ int val$item;

        /* renamed from: com.vkontakte.android.StickerManagerActivity.6.1 */
        class C05061 implements Runnable {
            private final /* synthetic */ int val$ecode;
            private final /* synthetic */ int val$item;

            C05061(int i, int i2) {
                this.val$ecode = i;
                this.val$item = i2;
            }

            public void run() {
                Toast.makeText(StickerManagerActivity.this, this.val$ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
                int pos = -1;
                for (int i = 0; i < StickerManagerActivity.this.active.size(); i++) {
                    if (((StickerPack) StickerManagerActivity.this.active.get(i)).id == this.val$item) {
                        pos = i;
                        break;
                    }
                }
                if (pos != -1) {
                    StickerManagerActivity.this.active.add(StickerManagerActivity.this.dragPrevPos, (StickerPack) StickerManagerActivity.this.active.remove(pos));
                    StickerManagerActivity.this.updateList();
                }
            }
        }

        C13626(int i) {
            this.val$item = i;
        }

        public void success() {
            StickerManagerActivity.this.reorderReq = null;
            StickerManagerActivity.this.applyChanges();
        }

        public void fail(int ecode, String emsg) {
            StickerManagerActivity.this.reorderReq = null;
            StickerManagerActivity.this.runOnUiThread(new C05061(ecode, this.val$item));
        }
    }

    private class StickerAdapter extends MultiSectionAdapter implements Swappable, DraggableChecker {
        private StickerAdapter() {
        }

        public View getView(int section, int item, View convertView) {
            View view = convertView;
            if (view == null) {
                view = View.inflate(StickerManagerActivity.this, C0436R.layout.news_banlist_item, null);
                view.findViewById(C0436R.id.flist_item_btn).setOnClickListener(StickerManagerActivity.this.activationClickListener);
            }
            ((ImageView) view.findViewById(C0436R.id.flist_item_btn)).setImageResource(section == 0 ? C0436R.drawable.ic_list_remove : C0436R.drawable.ic_list_add);
            StickerPack pack = (StickerPack) (section == 0 ? StickerManagerActivity.this.active : StickerManagerActivity.this.inactive).get(item);
            view.findViewById(C0436R.id.flist_item_btn).setTag(pack);
            ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(pack.title);
            if (StickerManagerActivity.this.imgLoader.isAlreadyLoaded(pack.thumb)) {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageBitmap(StickerManagerActivity.this.imgLoader.get(pack.thumb));
            } else {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(C0436R.drawable.sticker_placeholder);
            }
            return view;
        }

        public String getSectionTitle(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return StickerManagerActivity.this.getString(C0436R.string.stickers_active);
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return StickerManagerActivity.this.getString(C0436R.string.stickers_inactive);
                default:
                    return null;
            }
        }

        public int getSectionCount() {
            return 2;
        }

        public int getItemCount(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return StickerManagerActivity.this.active.size();
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return StickerManagerActivity.this.inactive.size();
                default:
                    return 0;
            }
        }

        public long getItemId(int section, int item) {
            return (long) ((StickerPack) (section == 0 ? StickerManagerActivity.this.active : StickerManagerActivity.this.inactive).get(item)).id;
        }

        public boolean isSectionHeaderVisible(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    if (StickerManagerActivity.this.active.size() <= 0) {
                        return false;
                    }
                    return true;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    if (StickerManagerActivity.this.inactive.size() <= 0) {
                        return false;
                    }
                    return true;
                default:
                    return true;
            }
        }

        public void swapItems(int first, int second) {
            first = resolveIndex(first)[1];
            second = resolveIndex(second)[1];
            StickerPack tmp = (StickerPack) StickerManagerActivity.this.active.get(first);
            StickerManagerActivity.this.active.set(first, (StickerPack) StickerManagerActivity.this.active.get(second));
            StickerManagerActivity.this.active.set(second, tmp);
        }

        public boolean canDragItem(int position) {
            int[] p = resolveIndex(position);
            if (p[1] == -1 || p[0] != 0) {
                return false;
            }
            return true;
        }

        public boolean canDragToPosition(int position) {
            if (position != 0 && resolveIndex(position)[0] == 0) {
                return true;
            }
            return false;
        }
    }

    private class StickerThumbAdapter extends MultiSectionImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.StickerManagerActivity.StickerThumbAdapter.1 */
        class C05071 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C05071(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.flist_item_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private StickerThumbAdapter() {
        }

        public int getSectionCount() {
            return StickerManagerActivity.this.adapter.getSectionCount();
        }

        public int getItemCount(int section) {
            return StickerManagerActivity.this.adapter.getItemCount(section);
        }

        public boolean isSectionHeaderVisible(int section) {
            return StickerManagerActivity.this.adapter.isSectionHeaderVisible(section);
        }

        public int getImageCountForItem(int section, int item) {
            return 1;
        }

        public String getImageURL(int section, int item, int image) {
            return ((StickerPack) (section == 0 ? StickerManagerActivity.this.active : StickerManagerActivity.this.inactive).get(item)).thumb;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += StickerManagerActivity.this.list.getHeaderViewsCount();
            if (item >= StickerManagerActivity.this.list.getFirstVisiblePosition() && item <= StickerManagerActivity.this.list.getLastVisiblePosition()) {
                StickerManagerActivity.this.runOnUiThread(new C05071(StickerManagerActivity.this.list.getChildAt(item - StickerManagerActivity.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    public StickerManagerActivity() {
        this.active = new ArrayList();
        this.inactive = new ArrayList();
        this.draggingItem = -1;
        this.dragPrevPos = -1;
        this.activationClickListener = new C05041();
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        this.content = new FrameLayout(this);
        this.content.setBackgroundColor(-1);
        this.listWrap = new FrameLayout(this);
        this.content.addView(this.listWrap);
        if (VERSION.SDK_INT >= 14) {
            this.list = new ReorderableListView(this);
            ((ReorderableListView) this.list).setDragListener(new C13592());
        } else {
            this.list = new ListView(this);
        }
        ListView listView = this.list;
        ListAdapter stickerAdapter = new StickerAdapter();
        this.adapter = stickerAdapter;
        listView.setAdapter(stickerAdapter);
        this.list.setPadding(getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding), 0, getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding), 0);
        this.list.setSelector(getResources().getDrawable(C0436R.drawable.highlight));
        this.listWrap.addView(this.list);
        EmptyView empty = EmptyView.create(this);
        this.list.setEmptyView(empty);
        this.listWrap.addView(empty);
        this.listWrap.setVisibility(8);
        this.progress = new ProgressBar(this);
        this.content.addView(this.progress, new LayoutParams(Global.scale(40.0f), Global.scale(40.0f), 17));
        this.error = (ErrorView) View.inflate(this, C0436R.layout.error, null);
        this.error.setVisibility(8);
        this.content.addView(this.error);
        this.error.setOnRetryListener(new C05053());
        this.imgLoader = new ListImageLoaderWrapper(new StickerThumbAdapter(), this.list, null);
        setContentView(this.content);
        loadData();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.currentReq != null) {
            this.currentReq.cancel();
            this.currentReq = null;
        }
    }

    private void setActiveState(StickerPack pack, boolean activate) {
        new StoreSetActive(pack.id, activate).setCallback(new C13604(activate, pack)).wrapProgress(this).exec((Activity) this);
    }

    private void updateList() {
        this.adapter.notifyDataSetChanged();
        this.imgLoader.updateImages();
    }

    private void loadData() {
        this.currentReq = new StoreGetPurchases().setCallback(new C13615()).exec((Activity) this);
    }

    private void sendReorder(int item, int before, int after) {
        this.reorderReq = new StoreReorderProducts(item, before, after).setCallback(new C13626(item)).exec();
    }

    private void applyChanges() {
        ArrayList<Integer> order = new ArrayList();
        Iterator it = this.active.iterator();
        while (it.hasNext()) {
            order.add(Integer.valueOf(((StickerPack) it.next()).id));
        }
        getSharedPreferences("stickers", 0).edit().putString("order", TextUtils.join(",", order)).commit();
        Stickers.broadcastUpdate();
    }
}
