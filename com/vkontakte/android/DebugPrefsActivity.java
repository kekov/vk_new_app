package com.vkontakte.android;

import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.widget.DatePicker;
import android.widget.Toast;
import com.vkontakte.android.APIRequest.APIHandler;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Date;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class DebugPrefsActivity extends PreferenceActivity {

    /* renamed from: com.vkontakte.android.DebugPrefsActivity.1 */
    class C02321 implements OnPreferenceClickListener {
        C02321() {
        }

        public boolean onPreferenceClick(Preference preference) {
            System.exit(0);
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.DebugPrefsActivity.2 */
    class C02332 implements OnPreferenceClickListener {
        C02332() {
        }

        public boolean onPreferenceClick(Preference pref) {
            Log.logFile = new File(Environment.getExternalStorageDirectory(), "VK.log");
            Toast.makeText(DebugPrefsActivity.this, String.format("\u041e\u0442\u043b\u0430\u0434\u043e\u0447\u043d\u0430\u044f \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f \u0437\u0430\u043f\u0438\u0441\u044b\u0432\u0430\u0435\u0442\u0441\u044f \u0432 \u0444\u0430\u0439\u043b \"%s\"", new Object[]{Log.logFile.getAbsolutePath()}), 1).show();
            pref.setEnabled(false);
            pref.setSummary("\u0423\u0436\u0435 \u0432\u043a\u043b\u044e\u0447\u0435\u043d\u043e");
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.DebugPrefsActivity.3 */
    class C02353 implements OnPreferenceClickListener {

        /* renamed from: com.vkontakte.android.DebugPrefsActivity.3.1 */
        class C02341 implements OnDateSetListener {
            C02341() {
            }

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Date date = new Date(0);
                date.setDate(dayOfMonth);
                date.setMonth(monthOfYear);
                date.setYear(year - 1900);
                Intent intent = new Intent(DebugPrefsActivity.this, BirthdayBroadcastReceiver.class);
                intent.putExtra("force", true);
                intent.putExtra("date", date.getTime());
                DebugPrefsActivity.this.sendBroadcast(intent);
            }
        }

        C02353() {
        }

        public boolean onPreferenceClick(Preference preference) {
            Date date = new Date();
            new DatePickerDialog(DebugPrefsActivity.this, new C02341(), date.getYear() + 1900, date.getMonth(), date.getDate()).show();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.DebugPrefsActivity.4 */
    class C02364 implements OnPreferenceClickListener {

        /* renamed from: com.vkontakte.android.DebugPrefsActivity.4.1 */
        class C12821 extends APIHandler {
            C12821() {
            }

            public void success(JSONObject j) {
                Toast.makeText(DebugPrefsActivity.this, j.toString(), 1).show();
            }

            public void fail(int ecode, String emsg) {
                Toast.makeText(DebugPrefsActivity.this, new StringBuilder(String.valueOf(ecode)).append(" ").append(emsg).toString(), 1).show();
            }
        }

        C02364() {
        }

        public boolean onPreferenceClick(Preference preference) {
            new APIRequest("account.testValidation").handler(new C12821()).wrapProgress(DebugPrefsActivity.this).exec(DebugPrefsActivity.this);
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.DebugPrefsActivity.5 */
    class C02375 implements OnPreferenceClickListener {
        C02375() {
        }

        public boolean onPreferenceClick(Preference preference) {
            DebugPrefsActivity.this.copyDatabases();
            return true;
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        addPreferencesFromResource(C0436R.xml.preferences_debug);
        findPreference("terminate").setOnPreferenceClickListener(new C02321());
        Preference pref = findPreference("logToFile");
        if (Log.logFile != null) {
            pref.setEnabled(false);
            pref.setSummary("\u0423\u0436\u0435 \u0432\u043a\u043b\u044e\u0447\u0435\u043d\u043e");
        } else {
            pref.setOnPreferenceClickListener(new C02332());
        }
        findPreference("bdayTest").setOnPreferenceClickListener(new C02353());
        findPreference("validationTest").setOnPreferenceClickListener(new C02364());
        findPreference("copyDatabases").setOnPreferenceClickListener(new C02375());
        pref = findPreference("invis");
        if (!getSharedPreferences(null, 0).getBoolean("sinv", false)) {
            getPreferenceScreen().removePreference(pref);
        }
    }

    private void copyDatabases() {
        String report = ACRAConstants.DEFAULT_STRING_VALUE;
        try {
            File dbDir = new File(getDatabasePath("qwe").getParent());
            File outDir = new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache_debug");
            if (!outDir.exists()) {
                outDir.mkdirs();
            }
            for (File f : dbDir.listFiles()) {
                if (f.getName().endsWith(".db")) {
                    File outFile = new File(outDir, f.getName());
                    outFile.createNewFile();
                    FileOutputStream out = new FileOutputStream(outFile);
                    FileInputStream in = new FileInputStream(f);
                    int count = 0;
                    byte[] buf = new byte[10240];
                    while (true) {
                        int read = in.read(buf);
                        if (read <= 0) {
                            break;
                        }
                        count += read;
                        out.write(buf, 0, read);
                    }
                    out.close();
                    in.close();
                    report = new StringBuilder(String.valueOf(report)).append(f.getAbsolutePath()).append(" -> ").append(outFile.getAbsolutePath()).append(" [").append(Global.langFileSize((long) count, getResources())).append("]\n").toString();
                }
            }
        } catch (Exception x) {
            report = new StringBuilder(String.valueOf(report)).append(x.getLocalizedMessage()).toString();
        }
        new Builder(this).setMessage(report).setPositiveButton("ok", null).show();
    }
}
