package com.vkontakte.android;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.graphics.Typeface;
import android.os.Build.VERSION;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import java.io.File;
import org.acra.ACRA;
import org.acra.ErrorReporter;
import org.acra.ReportField;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(customReportContent = {ReportField.APP_VERSION_NAME, ReportField.PHONE_MODEL, ReportField.BRAND, ReportField.PRODUCT, ReportField.ANDROID_VERSION, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.APP_VERSION_CODE}, formKey = "", formUri = "http://188.93.18.154/android/report.php")
public class VKApplication extends Application {
    public static Context context;
    public static long deviceID;

    static {
        deviceID = 0;
    }

    public void onCreate() {
        try {
            ACRA.init(this);
        } catch (Exception e) {
            Log.m526e("vk", "OH SHIT, unable to init error reporting");
        }
        ErrorReporter.getInstance().checkReportsOnApplicationStart();
        try {
            deviceID = Long.parseLong(Secure.getString(getApplicationContext().getContentResolver(), "android_id"), 16);
        } catch (Exception e2) {
        }
        if (VERSION.SDK_INT >= 19) {
            try {
                int i;
                ApplicationInfo applicationInfo = getApplicationInfo();
                i = applicationInfo.flags & 2;
                applicationInfo.flags = i;
                if (i != 0) {
                    WebView.setWebContentsDebuggingEnabled(true);
                }
            } catch (Exception e3) {
            }
        }
        File thumbsDir = new File(getApplicationContext().getCacheDir(), "thumbs");
        if (!thumbsDir.exists()) {
            thumbsDir.mkdirs();
        }
        for (File file : thumbsDir.listFiles()) {
            file.delete();
        }
        if (context == null) {
            boolean z;
            context = getApplicationContext();
            if (VERSION.SDK_INT < 14) {
                if (Global.regFont == null) {
                    Global.regFont = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                }
                if (Global.boldFont == null) {
                    Global.boldFont = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");
                }
            } else {
                if (Global.regFont == null) {
                    Global.regFont = Typeface.DEFAULT;
                }
                if (Global.boldFont == null) {
                    Global.boldFont = Typeface.DEFAULT_BOLD;
                }
            }
            if (VERSION.SDK_INT < 11) {
                z = true;
            } else {
                z = false;
            }
            Global.useBitmapHack = z;
            SharedPreferences prefs = getApplicationContext().getSharedPreferences(null, 0);
            if ((getResources().getConfiguration().screenLayout & 15) == 4) {
                z = true;
            } else {
                z = false;
            }
            Global.isTablet = z;
            if (!Global.isTablet) {
                DisplayMetrics dm = getResources().getDisplayMetrics();
                if ((getResources().getConfiguration().screenLayout & 15) == 3 || (dm.density < 1.25f && Math.max(dm.heightPixels, dm.widthPixels) > 640)) {
                    z = true;
                } else {
                    z = false;
                }
                Global.maybeTablet = z;
                SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(this);
                Log.m528i("vk", "MT " + Global.maybeTablet + " pref " + prefs2.getBoolean("forceTabletUI", false));
                if (Global.maybeTablet && prefs2.getBoolean("forceTabletUI", false)) {
                    Global.isTablet = true;
                }
            }
            if (Global.isTablet && VERSION.SDK_INT < 11) {
                Global.isTablet = false;
            }
            Global.timeDiff = prefs.getInt("time_diff", 0);
            Global.displayDensity = getResources().getDisplayMetrics().density;
        }
        super.onCreate();
    }
}
