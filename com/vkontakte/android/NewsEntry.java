package com.vkontakte.android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import com.google.android.gms.plus.PlusShare;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.acra.ACRAConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class NewsEntry implements Parcelable {
    public static final int A_ALBUM = 13;
    public static final int A_APP = 8;
    public static final int A_AUDIO = 3;
    public static final int A_DOCUMENT = 9;
    public static final int A_GEO = 10;
    public static final int A_GRAFFITI = 6;
    public static final int A_LINK = 5;
    public static final int A_NOTE = 7;
    public static final int A_PENDING_PHOTO = 14;
    public static final int A_PHOTO = 1;
    public static final int A_POLL = 4;
    public static final int A_POST = 12;
    public static final int A_REPOST = 17;
    public static final int A_SIGNATURE = 15;
    public static final int A_STICKER = 16;
    public static final int A_VIDEO = 2;
    public static final int A_WIKI = 11;
    public static final Creator<NewsEntry> CREATOR;
    public static final int FLAG_CAN_COMMENT = 2;
    public static final int FLAG_CAN_DELETE = 64;
    public static final int FLAG_CAN_EDIT = 128;
    public static final int FLAG_CAN_RETWEET = 1;
    public static final int FLAG_EXPORT_FACEBOOK = 16384;
    public static final int FLAG_EXPORT_TWITTER = 8192;
    public static final int FLAG_FIXED = 1024;
    public static final int FLAG_FRIENDS_ONLY = 512;
    public static final int FLAG_GRAY_FIRST_LINE = 256;
    public static final int FLAG_GRAY_TEXT = 16;
    public static final int FLAG_IS_RETWEET = 32;
    public static final int FLAG_LIKED = 8;
    public static final int FLAG_POSTPONED = 2048;
    public static final int FLAG_RETWEETED = 4;
    public static final int FLAG_SUGGESTED = 4096;
    public static final int TYPE_ADDED_PHOTO = 6;
    public static final int TYPE_AUDIO = 10;
    public static final int TYPE_BIRTHDAY = 8;
    public static final int TYPE_COMMENT = 5;
    public static final int TYPE_NOTE = 3;
    public static final int TYPE_PHOTO = 1;
    public static final int TYPE_POST = 0;
    public static final int TYPE_TAGGED_PHOTO = 7;
    public static final int TYPE_TOPIC = 4;
    public static final int TYPE_VIDEO = 2;
    public static final int TYPE_WALL_PHOTO = 9;
    public String attachTitle;
    public int attachType;
    public ArrayList<Attachment> attachments;
    public int createdBy;
    public CharSequence displayablePreviewText;
    public CharSequence displayableRetweetText;
    public boolean f44f;
    public int flags;
    public String lastComment;
    public int lastCommentTime;
    public String lastCommentUserName;
    public String lastCommentUserPhoto;
    public int numComments;
    public int numLikes;
    public int numRetweets;
    public int ownerID;
    public int postID;
    public ArrayList<Attachment> repostAttachments;
    public int retweetOrigId;
    public int retweetOrigTime;
    public String retweetText;
    public int retweetType;
    public int retweetUID;
    public String retweetUserName;
    public String retweetUserPhoto;
    public String text;
    public int time;
    public int type;
    public int userID;
    public String userName;
    public String userPhotoURL;

    /* renamed from: com.vkontakte.android.NewsEntry.1 */
    class C03391 implements Creator<NewsEntry> {
        C03391() {
        }

        public NewsEntry createFromParcel(Parcel in) {
            return new NewsEntry(in);
        }

        public NewsEntry[] newArray(int size) {
            return new NewsEntry[size];
        }
    }

    private static class URLSpanNoUnderline extends URLSpan {
        public URLSpanNoUnderline(String url) {
            super(url);
        }

        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            if (ds.drawableState != null) {
                int i = NewsEntry.TYPE_POST;
                while (i < ds.drawableState.length) {
                    if (ds.drawableState[i] == 16842919 || ds.drawableState[i] == 16842913) {
                    }
                    i += NewsEntry.TYPE_PHOTO;
                }
                ds.setColor(-13936518);
            }
        }
    }

    private static class XColorSpan extends ForegroundColorSpan {
        public XColorSpan(int color) {
            super(color);
        }

        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            if (ds.drawableState != null) {
                boolean isPressed = false;
                int i = NewsEntry.TYPE_POST;
                while (i < ds.drawableState.length) {
                    if (ds.drawableState[i] == 16842919 || ds.drawableState[i] == 16842913) {
                        isPressed = true;
                    }
                    i += NewsEntry.TYPE_PHOTO;
                }
                if (isPressed) {
                    ds.setColor(-1);
                } else {
                    ds.setColor(getForegroundColor());
                }
            }
        }
    }

    public NewsEntry() {
        this.userName = "UNKNOWN";
        this.type = TYPE_POST;
        this.text = ACRAConstants.DEFAULT_STRING_VALUE;
        this.retweetText = ACRAConstants.DEFAULT_STRING_VALUE;
        this.attachType = TYPE_POST;
        this.attachments = new ArrayList();
        this.repostAttachments = new ArrayList();
        this.flags = TYPE_POST;
        this.numRetweets = TYPE_POST;
        this.lastComment = null;
    }

    public NewsEntry(NewsEntry e) {
        this.userName = "UNKNOWN";
        this.type = TYPE_POST;
        this.text = ACRAConstants.DEFAULT_STRING_VALUE;
        this.retweetText = ACRAConstants.DEFAULT_STRING_VALUE;
        this.attachType = TYPE_POST;
        this.attachments = new ArrayList();
        this.repostAttachments = new ArrayList();
        this.flags = TYPE_POST;
        this.numRetweets = TYPE_POST;
        this.lastComment = null;
        this.userName = e.userName;
        this.userID = e.userID;
        this.ownerID = e.ownerID;
        this.postID = e.postID;
        this.type = e.type;
        this.text = e.text;
        this.displayablePreviewText = e.displayablePreviewText;
        this.time = e.time;
        this.numComments = e.numComments;
        this.retweetUID = e.retweetUID;
        this.retweetUserName = e.retweetUserName;
        this.retweetUserPhoto = e.retweetUserPhoto;
        this.retweetOrigTime = e.retweetOrigTime;
        this.retweetOrigId = e.retweetOrigId;
        this.retweetType = e.retweetType;
        this.numLikes = e.numLikes;
        this.userPhotoURL = e.userPhotoURL;
        this.attachments.addAll(e.attachments);
        this.flags = e.flags;
        this.type = e.type;
        this.retweetText = e.retweetText;
        this.displayableRetweetText = e.displayableRetweetText;
    }

    public NewsEntry(JSONObject item, HashMap<Integer, String> names, HashMap<Integer, String> photos) throws JSONException {
        this(item, names, photos, null);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public NewsEntry(org.json.JSONObject r30, java.util.HashMap<java.lang.Integer, java.lang.String> r31, java.util.HashMap<java.lang.Integer, java.lang.String> r32, java.util.HashMap<java.lang.Integer, java.lang.Boolean> r33) throws org.json.JSONException {
        /*
        r29 = this;
        r29.<init>();
        r2 = "UNKNOWN";
        r0 = r29;
        r0.userName = r2;
        r2 = 0;
        r0 = r29;
        r0.type = r2;
        r2 = "";
        r0 = r29;
        r0.text = r2;
        r2 = "";
        r0 = r29;
        r0.retweetText = r2;
        r2 = 0;
        r0 = r29;
        r0.attachType = r2;
        r2 = new java.util.ArrayList;
        r2.<init>();
        r0 = r29;
        r0.attachments = r2;
        r2 = new java.util.ArrayList;
        r2.<init>();
        r0 = r29;
        r0.repostAttachments = r2;
        r2 = 0;
        r0 = r29;
        r0.flags = r2;
        r2 = 0;
        r0 = r29;
        r0.numRetweets = r2;
        r2 = 0;
        r0 = r29;
        r0.lastComment = r2;
        r2 = "type";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x00f2;
    L_0x004a:
        r2 = "type";
        r0 = r30;
        r2 = r0.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "topic";
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x005f;
    L_0x005a:
        r2 = 4;
        r0 = r29;
        r0.type = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x005f:
        r2 = "type";
        r0 = r30;
        r2 = r0.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "photo";
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0074;
    L_0x006f:
        r2 = 1;
        r0 = r29;
        r0.type = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0074:
        r2 = "type";
        r0 = r30;
        r2 = r0.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "video";
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0089;
    L_0x0084:
        r2 = 2;
        r0 = r29;
        r0.type = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0089:
        r2 = "type";
        r0 = r30;
        r2 = r0.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "note";
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x009e;
    L_0x0099:
        r2 = 3;
        r0 = r29;
        r0.type = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x009e:
        r2 = "type";
        r0 = r30;
        r2 = r0.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "photo";
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x00bd;
    L_0x00ae:
        r2 = "photos";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x00bd;
    L_0x00b8:
        r2 = 6;
        r0 = r29;
        r0.type = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x00bd:
        r2 = "type";
        r0 = r30;
        r2 = r0.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "wall_photo";
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x00dd;
    L_0x00cd:
        r2 = "photos";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x00dd;
    L_0x00d7:
        r2 = 9;
        r0 = r29;
        r0.type = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x00dd:
        r2 = "type";
        r0 = r30;
        r2 = r0.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "photo_tag";
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x00f2;
    L_0x00ed:
        r2 = 7;
        r0 = r29;
        r0.type = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x00f2:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r4 = r0.type;	 Catch:{ Exception -> 0x09e9 }
        r4 = r4 << 24;
        r2 = r2 | r4;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "from_id";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x09d7;
    L_0x010b:
        r2 = "from_id";
    L_0x010d:
        r4 = "source_id";
        r0 = r30;
        r4 = r0.optInt(r4);	 Catch:{ Exception -> 0x09e9 }
        r0 = r30;
        r25 = r0.optInt(r2, r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = java.lang.Integer.valueOf(r25);	 Catch:{ Exception -> 0x09e9 }
        r0 = r31;
        r2 = r0.get(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (java.lang.String) r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.userName = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = java.lang.Integer.valueOf(r25);	 Catch:{ Exception -> 0x09e9 }
        r0 = r32;
        r2 = r0.get(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (java.lang.String) r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.userPhotoURL = r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r25;
        r1 = r29;
        r1.userID = r0;	 Catch:{ Exception -> 0x09e9 }
        r2 = "owner_id";
        r4 = "to_id";
        r5 = "source_id";
        r0 = r29;
        r6 = r0.userID;	 Catch:{ Exception -> 0x09e9 }
        r0 = r30;
        r5 = r0.optInt(r5, r6);	 Catch:{ Exception -> 0x09e9 }
        r0 = r30;
        r4 = r0.optInt(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r0 = r30;
        r2 = r0.optInt(r2, r4);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.ownerID = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "text";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x01d9;
    L_0x016b:
        r0 = r29;
        r2 = r0.type;	 Catch:{ Exception -> 0x09e9 }
        r4 = 4;
        if (r2 == r4) goto L_0x09db;
    L_0x0172:
        r2 = "text";
        r0 = r30;
        r2 = r0.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = com.vkontakte.android.Global.replaceMentions(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.text = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0182:
        r2 = "copy_comment_id";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x01d9;
    L_0x018c:
        r2 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x09e9 }
        r4 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x09e9 }
        r4 = r4.getResources();	 Catch:{ Exception -> 0x09e9 }
        r5 = 2131231030; // 0x7f080136 float:1.807813E38 double:1.0529680353E-314;
        r6 = 2;
        r6 = new java.lang.Object[r6];	 Catch:{ Exception -> 0x09e9 }
        r7 = 0;
        r8 = "copy_owner_id";
        r0 = r30;
        r8 = r0.getInt(r8);	 Catch:{ Exception -> 0x09e9 }
        r8 = java.lang.Integer.valueOf(r8);	 Catch:{ Exception -> 0x09e9 }
        r6[r7] = r8;	 Catch:{ Exception -> 0x09e9 }
        r7 = 1;
        r8 = "copy_post_id";
        r0 = r30;
        r8 = r0.getInt(r8);	 Catch:{ Exception -> 0x09e9 }
        r8 = java.lang.Integer.valueOf(r8);	 Catch:{ Exception -> 0x09e9 }
        r6[r7] = r8;	 Catch:{ Exception -> 0x09e9 }
        r4 = r4.getString(r5, r6);	 Catch:{ Exception -> 0x09e9 }
        r4 = java.lang.String.valueOf(r4);	 Catch:{ Exception -> 0x09e9 }
        r2.<init>(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "\n\n";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r4 = r0.text;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.toString();	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.text = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x01d9:
        r2 = "attachments";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0207;
    L_0x01e3:
        r2 = "attachments";
        r0 = r30;
        r10 = r0.optJSONArray(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r10 == 0) goto L_0x0207;
    L_0x01ed:
        r2 = r10.length();	 Catch:{ Exception -> 0x09e9 }
        if (r2 <= 0) goto L_0x0207;
    L_0x01f3:
        r14 = 0;
    L_0x01f4:
        r2 = r10.length();	 Catch:{ Exception -> 0x09e9 }
        r4 = 10;
        r2 = java.lang.Math.min(r2, r4);	 Catch:{ Exception -> 0x09e9 }
        if (r14 < r2) goto L_0x0a55;
    L_0x0200:
        r0 = r29;
        r2 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        com.vkontakte.android.Attachment.sort(r2);	 Catch:{ Exception -> 0x09e9 }
    L_0x0207:
        r2 = "id";
        r4 = "post_id";
        r5 = -1;
        r0 = r30;
        r4 = r0.optInt(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r0 = r30;
        r2 = r0.optInt(r2, r4);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.postID = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "comments";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0248;
    L_0x0226:
        r2 = "comments";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "count";
        r2 = r2.isNull(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 != 0) goto L_0x0248;
    L_0x0236:
        r2 = "comments";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "count";
        r2 = r2.getInt(r4);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.numComments = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0248:
        r2 = "comments";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x026e;
    L_0x0252:
        r2 = "comments";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "can_post";
        r5 = 1;
        r2 = r2.optInt(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 != r4) goto L_0x026e;
    L_0x0264:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 2;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x026e:
        r2 = "comments";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 != 0) goto L_0x0282;
    L_0x0278:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 2;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0282:
        r2 = "reposts";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x02ba;
    L_0x028c:
        r2 = "reposts";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "user_reposted";
        r5 = 0;
        r2 = r2.optInt(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 != r4) goto L_0x02ba;
    L_0x029e:
        r0 = r29;
        r2 = r0.ownerID;	 Catch:{ Exception -> 0x09e9 }
        r4 = com.vkontakte.android.Global.uid;	 Catch:{ Exception -> 0x09e9 }
        if (r2 == r4) goto L_0x02ba;
    L_0x02a6:
        r0 = r29;
        r2 = r0.ownerID;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r4 = r0.userID;	 Catch:{ Exception -> 0x09e9 }
        if (r2 != r4) goto L_0x02ba;
    L_0x02b0:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 4;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x02ba:
        r2 = "reposts";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x02d6;
    L_0x02c4:
        r2 = "reposts";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "count";
        r2 = r2.optInt(r4);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.numRetweets = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x02d6:
        r2 = "likes";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x02fc;
    L_0x02e0:
        r2 = "likes";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "can_publish";
        r5 = 0;
        r2 = r2.optInt(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 != r4) goto L_0x02fc;
    L_0x02f2:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 1;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x02fc:
        r2 = "likes";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0328;
    L_0x0306:
        r2 = "likes";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "count";
        r2 = r2.isNull(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 != 0) goto L_0x0328;
    L_0x0316:
        r2 = "likes";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "count";
        r2 = r2.getInt(r4);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.numLikes = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0328:
        r2 = "likes";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x034d;
    L_0x0332:
        r2 = "likes";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "user_likes";
        r2 = r2.optInt(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 != r4) goto L_0x034d;
    L_0x0343:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 8;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x034d:
        r2 = "can_edit";
        r4 = 0;
        r0 = r30;
        r2 = r0.optInt(r2, r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 != r4) goto L_0x0363;
    L_0x0359:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 128;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0363:
        r2 = "can_delete";
        r4 = 0;
        r0 = r30;
        r2 = r0.optInt(r2, r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 != r4) goto L_0x0379;
    L_0x036f:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 64;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0379:
        r2 = "friends_only";
        r0 = r30;
        r2 = r0.optInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 != r4) goto L_0x038e;
    L_0x0384:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 512;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x038e:
        r2 = "fixed";
        r0 = r30;
        r2 = r0.optInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 != r4) goto L_0x03a3;
    L_0x0399:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 1024;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x03a3:
        r0 = r29;
        r2 = r0.type;	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 != r4) goto L_0x03bd;
    L_0x03aa:
        r0 = r29;
        r2 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        r4 = new com.vkontakte.android.PhotoAttachment;	 Catch:{ Exception -> 0x09e9 }
        r5 = new com.vkontakte.android.Photo;	 Catch:{ Exception -> 0x09e9 }
        r0 = r30;
        r5.<init>(r0);	 Catch:{ Exception -> 0x09e9 }
        r4.<init>(r5);	 Catch:{ Exception -> 0x09e9 }
        r2.add(r4);	 Catch:{ Exception -> 0x09e9 }
    L_0x03bd:
        r0 = r29;
        r2 = r0.type;	 Catch:{ Exception -> 0x09e9 }
        r4 = 2;
        if (r2 != r4) goto L_0x03f8;
    L_0x03c4:
        r0 = r29;
        r8 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        r2 = new com.vkontakte.android.VideoAttachment;	 Catch:{ Exception -> 0x09e9 }
        r4 = "text";
        r0 = r30;
        r3 = r0.getString(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "photo_320";
        r5 = "photo_130";
        r0 = r30;
        r5 = r0.optString(r5);	 Catch:{ Exception -> 0x09e9 }
        r0 = r30;
        r4 = r0.optString(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r5 = r0.ownerID;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r6 = r0.postID;	 Catch:{ Exception -> 0x09e9 }
        r7 = "duration";
        r0 = r30;
        r7 = r0.optInt(r7);	 Catch:{ Exception -> 0x09e9 }
        r2.<init>(r3, r4, r5, r6, r7);	 Catch:{ Exception -> 0x09e9 }
        r8.add(r2);	 Catch:{ Exception -> 0x09e9 }
    L_0x03f8:
        r2 = "geo";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0415;
    L_0x0402:
        r2 = "geo";
        r0 = r30;
        r13 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        r4 = com.vkontakte.android.Attachment.parseGeo(r13);	 Catch:{ Exception -> 0x09e9 }
        r2.add(r4);	 Catch:{ Exception -> 0x09e9 }
    L_0x0415:
        if (r33 == 0) goto L_0x0a6c;
    L_0x0417:
        r0 = r29;
        r2 = r0.ownerID;	 Catch:{ Exception -> 0x09e9 }
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r33;
        r2 = r0.containsKey(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0a6c;
    L_0x0427:
        r0 = r29;
        r2 = r0.ownerID;	 Catch:{ Exception -> 0x09e9 }
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r33;
        r2 = r0.get(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (java.lang.Boolean) r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.booleanValue();	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0a6c;
    L_0x043d:
        r12 = 1;
    L_0x043e:
        r2 = "post_source";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0497;
    L_0x0448:
        r2 = "post_source";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "data";
        r2 = r2.has(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0497;
    L_0x0458:
        r2 = "post_source";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "data";
        r2 = r2.getString(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "profile_photo";
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0497;
    L_0x046e:
        r0 = r29;
        r2 = r0.ownerID;	 Catch:{ Exception -> 0x09e9 }
        if (r2 >= 0) goto L_0x0a6f;
    L_0x0474:
        r2 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.getResources();	 Catch:{ Exception -> 0x09e9 }
        r4 = 2131231304; // 0x7f080248 float:1.8078685E38 double:1.0529681706E-314;
        r2 = r2.getString(r4);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.text = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0485:
        r0 = r29;
        r2 = r0.text;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.displayablePreviewText = r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 16;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0497:
        r2 = "photos";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 != 0) goto L_0x04ab;
    L_0x04a1:
        r2 = "photo_tags";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0507;
    L_0x04ab:
        r0 = r29;
        r0.f44f = r12;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.type;	 Catch:{ Exception -> 0x09e9 }
        r4 = 6;
        if (r2 == r4) goto L_0x04be;
    L_0x04b6:
        r0 = r29;
        r2 = r0.type;	 Catch:{ Exception -> 0x09e9 }
        r4 = 9;
        if (r2 != r4) goto L_0x0a88;
    L_0x04be:
        r2 = "photos";
    L_0x04c0:
        r0 = r30;
        r17 = com.vkontakte.android.api.APIUtils.unwrapArray(r0, r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r17;
        r0 = r0.array;	 Catch:{ Exception -> 0x09e9 }
        r23 = r0;
        r0 = r29;
        r2 = r0.type;	 Catch:{ Exception -> 0x09e9 }
        r4 = 6;
        if (r2 != r4) goto L_0x0a91;
    L_0x04d3:
        if (r12 == 0) goto L_0x0a8c;
    L_0x04d5:
        r2 = 2131558450; // 0x7f0d0032 float:1.8742216E38 double:1.0531298022E-314;
    L_0x04d8:
        r0 = r17;
        r4 = r0.count;	 Catch:{ Exception -> 0x09e9 }
        r5 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x09e9 }
        r5 = r5.getResources();	 Catch:{ Exception -> 0x09e9 }
        r2 = com.vkontakte.android.Global.langPlural(r2, r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.text = r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.displayablePreviewText = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x04ee:
        r0 = r17;
        r2 = r0.count;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.postID = r2;	 Catch:{ Exception -> 0x09e9 }
        r14 = 0;
    L_0x04f7:
        r2 = r23.length();	 Catch:{ Exception -> 0x09e9 }
        if (r14 < r2) goto L_0x0ab2;
    L_0x04fd:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 16;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0507:
        r2 = "copy_history";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x078b;
    L_0x0511:
        r2 = "copy_history";
        r0 = r30;
        r2 = r0.optJSONArray(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x078b;
    L_0x051b:
        r2 = "copy_history";
        r0 = r30;
        r2 = r0.getJSONArray(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.length();	 Catch:{ Exception -> 0x09e9 }
        if (r2 <= 0) goto L_0x078b;
    L_0x0529:
        r2 = "copy_history";
        r0 = r30;
        r2 = r0.getJSONArray(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = 0;
        r22 = r2.getJSONObject(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = "text";
        r0 = r30;
        r2 = r0.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = com.vkontakte.android.Global.replaceMentions(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.retweetText = r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.retweetText;	 Catch:{ Exception -> 0x09e9 }
        r4 = "\n";
        r2 = r2.split(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.length;	 Catch:{ Exception -> 0x09e9 }
        r4 = 6;
        if (r2 <= r4) goto L_0x0adf;
    L_0x0554:
        r15 = -1;
        r14 = 0;
    L_0x0556:
        r2 = 6;
        if (r14 < r2) goto L_0x0acf;
    L_0x0559:
        r2 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r4 = r0.retweetText;	 Catch:{ Exception -> 0x09e9 }
        r5 = 0;
        r6 = 280; // 0x118 float:3.92E-43 double:1.383E-321;
        r7 = r15 + -1;
        r6 = java.lang.Math.min(r6, r7);	 Catch:{ Exception -> 0x09e9 }
        r4 = r4.substring(r5, r6);	 Catch:{ Exception -> 0x09e9 }
        r4 = java.lang.String.valueOf(r4);	 Catch:{ Exception -> 0x09e9 }
        r2.<init>(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "...<br/><a href='http://vk.com'>";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x09e9 }
        r4 = r4.getResources();	 Catch:{ Exception -> 0x09e9 }
        r5 = 2131231237; // 0x7f080205 float:1.807855E38 double:1.0529681375E-314;
        r4 = r4.getString(r5);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "</a>";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.toString();	 Catch:{ Exception -> 0x09e9 }
        r4 = "\n";
        r5 = "<br/>";
        r2 = r2.replace(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r2 = android.text.Html.fromHtml(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (android.text.Spannable) r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = stripUnderlines(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.displayableRetweetText = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x05aa:
        r0 = r29;
        r2 = r0.displayableRetweetText;	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x05c6;
    L_0x05b0:
        r0 = r29;
        r2 = r0.displayableRetweetText;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.length();	 Catch:{ Exception -> 0x09e9 }
        if (r2 <= 0) goto L_0x05c6;
    L_0x05ba:
        r0 = r29;
        r2 = r0.displayableRetweetText;	 Catch:{ Exception -> 0x09e9 }
        r2 = com.vkontakte.android.Global.replaceEmoji(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.displayableRetweetText = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x05c6:
        r2 = "text";
        r0 = r22;
        r2 = r0.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = com.vkontakte.android.Global.replaceMentions(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.text = r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.repostAttachments = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = new java.util.ArrayList;	 Catch:{ Exception -> 0x09e9 }
        r2.<init>();	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.attachments = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "attachments";
        r0 = r22;
        r16 = r0.optJSONArray(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r16 == 0) goto L_0x05f8;
    L_0x05f1:
        r14 = 0;
    L_0x05f2:
        r2 = r16.length();	 Catch:{ Exception -> 0x09e9 }
        if (r14 < r2) goto L_0x0b54;
    L_0x05f8:
        r2 = "geo";
        r0 = r22;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0615;
    L_0x0602:
        r2 = "geo";
        r0 = r22;
        r13 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        r4 = com.vkontakte.android.Attachment.parseGeo(r13);	 Catch:{ Exception -> 0x09e9 }
        r2.add(r4);	 Catch:{ Exception -> 0x09e9 }
    L_0x0615:
        r0 = r29;
        r2 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        com.vkontakte.android.Attachment.sort(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = "owner_id";
        r0 = r22;
        r2 = r0.optInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.retweetUID = r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 32;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.retweetUID;	 Catch:{ Exception -> 0x09e9 }
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r31;
        r2 = r0.containsKey(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0b6b;
    L_0x0642:
        r0 = r29;
        r2 = r0.retweetUID;	 Catch:{ Exception -> 0x09e9 }
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r31;
        r2 = r0.get(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (java.lang.String) r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0652:
        r0 = r29;
        r0.retweetUserName = r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.retweetUID;	 Catch:{ Exception -> 0x09e9 }
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r32;
        r2 = r0.get(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (java.lang.String) r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.retweetUserPhoto = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "id";
        r0 = r22;
        r2 = r0.optInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.retweetOrigId = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "date";
        r4 = 0;
        r0 = r22;
        r2 = r0.optInt(r2, r4);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.retweetOrigTime = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "reply";
        r4 = "post_type";
        r0 = r22;
        r4 = r0.optString(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x06cc;
    L_0x0693:
        r2 = 5;
        r0 = r29;
        r0.retweetType = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "reply_post_id";
        r0 = r22;
        r2 = r0.getInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.retweetOrigId = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "from_id";
        r0 = r22;
        r11 = r0.getInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = java.lang.Integer.valueOf(r11);	 Catch:{ Exception -> 0x09e9 }
        r0 = r31;
        r2 = r0.get(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (java.lang.String) r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.retweetUserName = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = java.lang.Integer.valueOf(r11);	 Catch:{ Exception -> 0x09e9 }
        r0 = r32;
        r2 = r0.get(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (java.lang.String) r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.retweetUserPhoto = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x06cc:
        r2 = "photo";
        r4 = "post_type";
        r0 = r22;
        r4 = r0.optString(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x06e1;
    L_0x06dc:
        r2 = 1;
        r0 = r29;
        r0.retweetType = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x06e1:
        r2 = "video";
        r4 = "post_type";
        r0 = r22;
        r4 = r0.optString(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x06f6;
    L_0x06f1:
        r2 = 2;
        r0 = r29;
        r0.retweetType = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x06f6:
        r2 = "copy_history";
        r0 = r30;
        r2 = r0.getJSONArray(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.length();	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 <= r4) goto L_0x078b;
    L_0x0705:
        r2 = "copy_history";
        r0 = r30;
        r2 = r0.getJSONArray(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        r26 = r2.getJSONObject(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = "owner_id";
        r0 = r26;
        r3 = r0.getInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = "reply";
        r4 = "post_type";
        r0 = r26;
        r4 = r0.optString(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x0b77;
    L_0x072a:
        r2 = "from_id";
        r0 = r26;
        r21 = r0.getInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        r28 = r0;
        r2 = new com.vkontakte.android.RepostAttachment;	 Catch:{ Exception -> 0x09e9 }
        r4 = "owner_id";
        r0 = r26;
        r3 = r0.getInt(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "reply_post_id";
        r0 = r26;
        r4 = r0.getInt(r4);	 Catch:{ Exception -> 0x09e9 }
        r5 = "date";
        r0 = r26;
        r5 = r0.getInt(r5);	 Catch:{ Exception -> 0x09e9 }
        r6 = java.lang.Integer.valueOf(r21);	 Catch:{ Exception -> 0x09e9 }
        r0 = r31;
        r6 = r0.containsKey(r6);	 Catch:{ Exception -> 0x09e9 }
        if (r6 == 0) goto L_0x0b6f;
    L_0x075e:
        r6 = java.lang.Integer.valueOf(r21);	 Catch:{ Exception -> 0x09e9 }
        r0 = r31;
        r6 = r0.get(r6);	 Catch:{ Exception -> 0x09e9 }
        r6 = (java.lang.String) r6;	 Catch:{ Exception -> 0x09e9 }
    L_0x076a:
        r7 = java.lang.Integer.valueOf(r21);	 Catch:{ Exception -> 0x09e9 }
        r0 = r32;
        r7 = r0.containsKey(r7);	 Catch:{ Exception -> 0x09e9 }
        if (r7 == 0) goto L_0x0b73;
    L_0x0776:
        r7 = java.lang.Integer.valueOf(r21);	 Catch:{ Exception -> 0x09e9 }
        r0 = r32;
        r7 = r0.get(r7);	 Catch:{ Exception -> 0x09e9 }
        r7 = (java.lang.String) r7;	 Catch:{ Exception -> 0x09e9 }
    L_0x0782:
        r8 = 5;
        r2.<init>(r3, r4, r5, r6, r7, r8);	 Catch:{ Exception -> 0x09e9 }
        r0 = r28;
        r0.add(r2);	 Catch:{ Exception -> 0x09e9 }
    L_0x078b:
        r0 = r29;
        r2 = r0.text;	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x07de;
    L_0x0791:
        r0 = r29;
        r2 = r0.type;	 Catch:{ Exception -> 0x09e9 }
        r4 = 4;
        if (r2 != r4) goto L_0x0bd0;
    L_0x0798:
        r2 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x09e9 }
        r4 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x09e9 }
        r4 = r4.getResources();	 Catch:{ Exception -> 0x09e9 }
        r5 = 2131231050; // 0x7f08014a float:1.807817E38 double:1.052968045E-314;
        r4 = r4.getString(r5);	 Catch:{ Exception -> 0x09e9 }
        r4 = java.lang.String.valueOf(r4);	 Catch:{ Exception -> 0x09e9 }
        r2.<init>(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = " <a href='http://vk.com'>";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r4 = r0.text;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "</a>";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.toString();	 Catch:{ Exception -> 0x09e9 }
        r2 = android.text.Html.fromHtml(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (android.text.Spannable) r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = stripUnderlines(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.displayablePreviewText = r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 16;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x07de:
        r2 = "date";
        r0 = r30;
        r2 = r0.optInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = com.vkontakte.android.Global.timeDiff;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 + r4;
        r0 = r29;
        r0.time = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "comments";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x08a4;
    L_0x07f7:
        r2 = "comments";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "list";
        r2 = r2.has(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x08a4;
    L_0x0807:
        r2 = "comments";
        r0 = r30;
        r2 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = "list";
        r20 = r2.getJSONArray(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r20.length();	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 + -1;
        r0 = r20;
        r19 = r0.getJSONObject(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = "text";
        r0 = r19;
        r2 = r0.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.lastComment = r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.type;	 Catch:{ Exception -> 0x09e9 }
        r4 = 4;
        if (r2 != r4) goto L_0x0cc6;
    L_0x0834:
        r0 = r29;
        r2 = r0.lastComment;	 Catch:{ Exception -> 0x09e9 }
        r4 = "\\[(id|club)(\\d+):bp-(\\d+)_(\\d+)\\|([^\\]]+)\\]";
        r5 = "$5";
        r2 = r2.replaceAll(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.lastComment = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0844:
        r2 = "from_id";
        r0 = r19;
        r2 = r0.getInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r31;
        r2 = r0.get(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (java.lang.String) r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.lastCommentUserName = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "from_id";
        r0 = r19;
        r2 = r0.getInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r32;
        r2 = r0.get(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (java.lang.String) r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.lastCommentUserPhoto = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "date";
        r0 = r19;
        r2 = r0.getInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = com.vkontakte.android.Global.timeDiff;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 + r4;
        r0 = r29;
        r0.lastCommentTime = r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.lastComment;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.length();	 Catch:{ Exception -> 0x09e9 }
        if (r2 != 0) goto L_0x08a4;
    L_0x088d:
        r2 = "attachments";
        r0 = r19;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x08a4;
    L_0x0897:
        r2 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x09e9 }
        r4 = 2131231228; // 0x7f0801fc float:1.8078531E38 double:1.052968133E-314;
        r2 = r2.getString(r4);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.lastComment = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x08a4:
        r29.layoutThumbs();	 Catch:{ Exception -> 0x09e9 }
        r2 = "signer_id";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x08ec;
    L_0x08b1:
        r18 = new com.vkontakte.android.SignatureLinkAttachment;	 Catch:{ Exception -> 0x09e9 }
        r2 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x09e9 }
        r4 = "http://vkontakte.ru/id";
        r2.<init>(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "signer_id";
        r0 = r30;
        r4 = r0.getInt(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = r2.toString();	 Catch:{ Exception -> 0x09e9 }
        r2 = "signer_id";
        r0 = r30;
        r2 = r0.getInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r31;
        r2 = r0.get(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (java.lang.String) r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r18;
        r0.<init>(r4, r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        r0 = r18;
        r2.add(r0);	 Catch:{ Exception -> 0x09e9 }
    L_0x08ec:
        r2 = "reply_owner_id";
        r0 = r30;
        r2 = r0.has(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x093e;
    L_0x08f6:
        r18 = new com.vkontakte.android.LinkAttachment;	 Catch:{ Exception -> 0x09e9 }
        r2 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x09e9 }
        r4 = "http://vkontakte.ru/wall";
        r2.<init>(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "reply_owner_id";
        r0 = r30;
        r4 = r0.getInt(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "_";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "reply_post_id";
        r0 = r30;
        r4 = r0.getInt(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.toString();	 Catch:{ Exception -> 0x09e9 }
        r4 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x09e9 }
        r4 = r4.getResources();	 Catch:{ Exception -> 0x09e9 }
        r5 = 2131231031; // 0x7f080137 float:1.8078132E38 double:1.052968036E-314;
        r4 = r4.getString(r5);	 Catch:{ Exception -> 0x09e9 }
        r5 = "";
        r0 = r18;
        r0.<init>(r2, r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        r0 = r18;
        r2.add(r0);	 Catch:{ Exception -> 0x09e9 }
    L_0x093e:
        r0 = r29;
        r2 = r0.ownerID;	 Catch:{ Exception -> 0x09e9 }
        if (r2 >= 0) goto L_0x0978;
    L_0x0944:
        r0 = r29;
        r2 = r0.ownerID;	 Catch:{ Exception -> 0x09e9 }
        r2 = -r2;
        r2 = com.vkontakte.android.data.Groups.getAdminLevel(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 < r4) goto L_0x0956;
    L_0x0950:
        r0 = r29;
        r2 = r0.userID;	 Catch:{ Exception -> 0x09e9 }
        if (r2 > 0) goto L_0x0962;
    L_0x0956:
        r0 = r29;
        r2 = r0.ownerID;	 Catch:{ Exception -> 0x09e9 }
        r2 = -r2;
        r2 = com.vkontakte.android.data.Groups.getAdminLevel(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = 2;
        if (r2 < r4) goto L_0x096c;
    L_0x0962:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 64;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x096c:
        r2 = "created_by";
        r0 = r30;
        r2 = r0.optInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.createdBy = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x0978:
        r2 = "postpone";
        r4 = "post_type";
        r0 = r30;
        r4 = r0.optString(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x09bc;
    L_0x0988:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 2048;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = "twitter_export";
        r0 = r30;
        r2 = r0.optInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 != r4) goto L_0x09a7;
    L_0x099d:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 8192;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x09a7:
        r2 = "facebook_export";
        r0 = r30;
        r2 = r0.optInt(r2);	 Catch:{ Exception -> 0x09e9 }
        r4 = 1;
        if (r2 != r4) goto L_0x09bc;
    L_0x09b2:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 16384;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x09bc:
        r2 = "suggest";
        r4 = "post_type";
        r0 = r30;
        r4 = r0.optString(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.equals(r4);	 Catch:{ Exception -> 0x09e9 }
        if (r2 == 0) goto L_0x09d6;
    L_0x09cc:
        r0 = r29;
        r2 = r0.flags;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2 | 4096;
        r0 = r29;
        r0.flags = r2;	 Catch:{ Exception -> 0x09e9 }
    L_0x09d6:
        return;
    L_0x09d7:
        r2 = "owner_id";
        goto L_0x010d;
    L_0x09db:
        r2 = "text";
        r0 = r30;
        r2 = r0.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.text = r2;	 Catch:{ Exception -> 0x09e9 }
        goto L_0x0182;
    L_0x09e9:
        r27 = move-exception;
        r2 = "vk";
        r0 = r27;
        com.vkontakte.android.Log.m532w(r2, r0);
        r2 = "vk";
        r4 = r30.toString();
        com.vkontakte.android.Log.m528i(r2, r4);
        r0 = r29;
        r2 = r0.text;
        if (r2 != 0) goto L_0x0a06;
    L_0x0a00:
        r2 = "";
        r0 = r29;
        r0.text = r2;
    L_0x0a06:
        r0 = r29;
        r2 = r0.text;
        r4 = new java.lang.StringBuilder;
        r2 = java.lang.String.valueOf(r2);
        r4.<init>(r2);
        r2 = com.vkontakte.android.VKApplication.context;
        r2 = r2.getResources();
        r5 = 2131230824; // 0x7f080068 float:1.8077712E38 double:1.0529679335E-314;
        r2 = r2.getString(r5);
        r2 = r4.append(r2);
        r4 = "\n";
        r2 = r2.append(r4);
        r4 = r27.getClass();
        r4 = r4.getSimpleName();
        r2 = r2.append(r4);
        r4 = ": ";
        r2 = r2.append(r4);
        r4 = r27.getMessage();
        r2 = r2.append(r4);
        r2 = r2.toString();
        r0 = r29;
        r0.text = r2;
        r0 = r29;
        r2 = r0.text;
        r0 = r29;
        r0.displayablePreviewText = r2;
        goto L_0x09d6;
    L_0x0a55:
        r2 = r10.getJSONObject(r14);	 Catch:{ Exception -> 0x09e9 }
        r9 = com.vkontakte.android.Attachment.parse(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r9 == 0) goto L_0x0a66;
    L_0x0a5f:
        r0 = r29;
        r2 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        r2.add(r9);	 Catch:{ Exception -> 0x09e9 }
    L_0x0a66:
        r2 = r9 instanceof com.vkontakte.android.AlbumAttachment;	 Catch:{ Exception -> 0x09e9 }
        r14 = r14 + 1;
        goto L_0x01f4;
    L_0x0a6c:
        r12 = 0;
        goto L_0x043e;
    L_0x0a6f:
        r2 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x09e9 }
        r4 = r2.getResources();	 Catch:{ Exception -> 0x09e9 }
        if (r12 == 0) goto L_0x0a84;
    L_0x0a77:
        r2 = 2131231303; // 0x7f080247 float:1.8078683E38 double:1.05296817E-314;
    L_0x0a7a:
        r2 = r4.getString(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.text = r2;	 Catch:{ Exception -> 0x09e9 }
        goto L_0x0485;
    L_0x0a84:
        r2 = 2131231302; // 0x7f080246 float:1.8078681E38 double:1.0529681697E-314;
        goto L_0x0a7a;
    L_0x0a88:
        r2 = "photo_tags";
        goto L_0x04c0;
    L_0x0a8c:
        r2 = 2131558449; // 0x7f0d0031 float:1.8742214E38 double:1.053129802E-314;
        goto L_0x04d8;
    L_0x0a91:
        if (r12 == 0) goto L_0x0aae;
    L_0x0a93:
        r2 = 2131558452; // 0x7f0d0034 float:1.874222E38 double:1.053129803E-314;
    L_0x0a96:
        r0 = r17;
        r4 = r0.count;	 Catch:{ Exception -> 0x09e9 }
        r5 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x09e9 }
        r5 = r5.getResources();	 Catch:{ Exception -> 0x09e9 }
        r2 = com.vkontakte.android.Global.langPlural(r2, r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.text = r2;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.displayablePreviewText = r2;	 Catch:{ Exception -> 0x09e9 }
        goto L_0x04ee;
    L_0x0aae:
        r2 = 2131558451; // 0x7f0d0033 float:1.8742218E38 double:1.0531298027E-314;
        goto L_0x0a96;
    L_0x0ab2:
        r0 = r23;
        r24 = r0.getJSONObject(r14);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r2 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        r4 = new com.vkontakte.android.PhotoAttachment;	 Catch:{ Exception -> 0x09e9 }
        r5 = new com.vkontakte.android.Photo;	 Catch:{ Exception -> 0x09e9 }
        r0 = r24;
        r5.<init>(r0);	 Catch:{ Exception -> 0x09e9 }
        r4.<init>(r5);	 Catch:{ Exception -> 0x09e9 }
        r2.add(r4);	 Catch:{ Exception -> 0x09e9 }
        r14 = r14 + 1;
        goto L_0x04f7;
    L_0x0acf:
        r0 = r29;
        r2 = r0.retweetText;	 Catch:{ Exception -> 0x09e9 }
        r4 = 10;
        r5 = r15 + 1;
        r15 = r2.indexOf(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r14 = r14 + 1;
        goto L_0x0556;
    L_0x0adf:
        r0 = r29;
        r2 = r0.retweetText;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.length();	 Catch:{ Exception -> 0x09e9 }
        r4 = 280; // 0x118 float:3.92E-43 double:1.383E-321;
        if (r2 > r4) goto L_0x0b07;
    L_0x0aeb:
        r0 = r29;
        r2 = r0.retweetText;	 Catch:{ Exception -> 0x09e9 }
        r4 = "\n";
        r5 = "<br/>";
        r2 = r2.replace(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r2 = android.text.Html.fromHtml(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (android.text.Spannable) r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = stripUnderlines(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.displayableRetweetText = r2;	 Catch:{ Exception -> 0x09e9 }
        goto L_0x05aa;
    L_0x0b07:
        r2 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r4 = r0.retweetText;	 Catch:{ Exception -> 0x09e9 }
        r5 = 0;
        r6 = 280; // 0x118 float:3.92E-43 double:1.383E-321;
        r4 = r4.substring(r5, r6);	 Catch:{ Exception -> 0x09e9 }
        r4 = java.lang.String.valueOf(r4);	 Catch:{ Exception -> 0x09e9 }
        r2.<init>(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "...<br/><a href='http://vk.com'>";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x09e9 }
        r4 = r4.getResources();	 Catch:{ Exception -> 0x09e9 }
        r5 = 2131231237; // 0x7f080205 float:1.807855E38 double:1.0529681375E-314;
        r4 = r4.getString(r5);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "</a>";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.toString();	 Catch:{ Exception -> 0x09e9 }
        r4 = "\n";
        r5 = "<br/>";
        r2 = r2.replace(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r2 = android.text.Html.fromHtml(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (android.text.Spannable) r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = stripUnderlines(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.displayableRetweetText = r2;	 Catch:{ Exception -> 0x09e9 }
        goto L_0x05aa;
    L_0x0b54:
        r0 = r16;
        r2 = r0.getJSONObject(r14);	 Catch:{ Exception -> 0x09e9 }
        r9 = com.vkontakte.android.Attachment.parse(r2);	 Catch:{ Exception -> 0x09e9 }
        if (r9 == 0) goto L_0x0b67;
    L_0x0b60:
        r0 = r29;
        r2 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        r2.add(r9);	 Catch:{ Exception -> 0x09e9 }
    L_0x0b67:
        r14 = r14 + 1;
        goto L_0x05f2;
    L_0x0b6b:
        r2 = "DELETED";
        goto L_0x0652;
    L_0x0b6f:
        r6 = "DELETED";
        goto L_0x076a;
    L_0x0b73:
        r7 = "http://vk.com/images/question_a.gif";
        goto L_0x0782;
    L_0x0b77:
        r0 = r29;
        r0 = r0.attachments;	 Catch:{ Exception -> 0x09e9 }
        r28 = r0;
        r2 = new com.vkontakte.android.RepostAttachment;	 Catch:{ Exception -> 0x09e9 }
        r4 = "id";
        r0 = r26;
        r4 = r0.getInt(r4);	 Catch:{ Exception -> 0x09e9 }
        r5 = "date";
        r0 = r26;
        r5 = r0.getInt(r5);	 Catch:{ Exception -> 0x09e9 }
        r6 = java.lang.Integer.valueOf(r3);	 Catch:{ Exception -> 0x09e9 }
        r0 = r31;
        r6 = r0.containsKey(r6);	 Catch:{ Exception -> 0x09e9 }
        if (r6 == 0) goto L_0x0bca;
    L_0x0b9b:
        r6 = java.lang.Integer.valueOf(r3);	 Catch:{ Exception -> 0x09e9 }
        r0 = r31;
        r6 = r0.get(r6);	 Catch:{ Exception -> 0x09e9 }
        r6 = (java.lang.String) r6;	 Catch:{ Exception -> 0x09e9 }
    L_0x0ba7:
        r7 = java.lang.Integer.valueOf(r3);	 Catch:{ Exception -> 0x09e9 }
        r0 = r32;
        r7 = r0.containsKey(r7);	 Catch:{ Exception -> 0x09e9 }
        if (r7 == 0) goto L_0x0bcd;
    L_0x0bb3:
        r7 = java.lang.Integer.valueOf(r3);	 Catch:{ Exception -> 0x09e9 }
        r0 = r32;
        r7 = r0.get(r7);	 Catch:{ Exception -> 0x09e9 }
        r7 = (java.lang.String) r7;	 Catch:{ Exception -> 0x09e9 }
    L_0x0bbf:
        r8 = 0;
        r2.<init>(r3, r4, r5, r6, r7, r8);	 Catch:{ Exception -> 0x09e9 }
        r0 = r28;
        r0.add(r2);	 Catch:{ Exception -> 0x09e9 }
        goto L_0x078b;
    L_0x0bca:
        r6 = "DELETED";
        goto L_0x0ba7;
    L_0x0bcd:
        r7 = "http://vk.com/images/question_a.gif";
        goto L_0x0bbf;
    L_0x0bd0:
        r0 = r29;
        r2 = r0.text;	 Catch:{ Exception -> 0x09e9 }
        r4 = "\n";
        r2 = r2.split(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.length;	 Catch:{ Exception -> 0x09e9 }
        r4 = 6;
        if (r2 <= r4) goto L_0x0c49;
    L_0x0bde:
        r15 = -1;
        r14 = 0;
    L_0x0be0:
        r2 = 6;
        if (r14 < r2) goto L_0x0c3a;
    L_0x0be3:
        r2 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r4 = r0.text;	 Catch:{ Exception -> 0x09e9 }
        r5 = 0;
        r6 = 280; // 0x118 float:3.92E-43 double:1.383E-321;
        r7 = r15 + -1;
        r6 = java.lang.Math.min(r6, r7);	 Catch:{ Exception -> 0x09e9 }
        r4 = r4.substring(r5, r6);	 Catch:{ Exception -> 0x09e9 }
        r4 = java.lang.String.valueOf(r4);	 Catch:{ Exception -> 0x09e9 }
        r2.<init>(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "...<br/><a href='http://vk.com'>";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x09e9 }
        r4 = r4.getResources();	 Catch:{ Exception -> 0x09e9 }
        r5 = 2131231237; // 0x7f080205 float:1.807855E38 double:1.0529681375E-314;
        r4 = r4.getString(r5);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "</a>";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.toString();	 Catch:{ Exception -> 0x09e9 }
        r4 = "\n";
        r5 = "<br/>";
        r2 = r2.replace(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r2 = android.text.Html.fromHtml(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = com.vkontakte.android.Global.replaceEmoji(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (android.text.Spannable) r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = stripUnderlines(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.displayablePreviewText = r2;	 Catch:{ Exception -> 0x09e9 }
        goto L_0x07de;
    L_0x0c3a:
        r0 = r29;
        r2 = r0.text;	 Catch:{ Exception -> 0x09e9 }
        r4 = 10;
        r5 = r15 + 1;
        r15 = r2.indexOf(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r14 = r14 + 1;
        goto L_0x0be0;
    L_0x0c49:
        r0 = r29;
        r2 = r0.text;	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.length();	 Catch:{ Exception -> 0x09e9 }
        r4 = 280; // 0x118 float:3.92E-43 double:1.383E-321;
        if (r2 > r4) goto L_0x0c75;
    L_0x0c55:
        r0 = r29;
        r2 = r0.text;	 Catch:{ Exception -> 0x09e9 }
        r4 = "\n";
        r5 = "<br/>";
        r2 = r2.replace(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r2 = android.text.Html.fromHtml(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = com.vkontakte.android.Global.replaceEmoji(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (android.text.Spannable) r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = stripUnderlines(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.displayablePreviewText = r2;	 Catch:{ Exception -> 0x09e9 }
        goto L_0x07de;
    L_0x0c75:
        r2 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r4 = r0.text;	 Catch:{ Exception -> 0x09e9 }
        r5 = 0;
        r6 = 280; // 0x118 float:3.92E-43 double:1.383E-321;
        r4 = r4.substring(r5, r6);	 Catch:{ Exception -> 0x09e9 }
        r4 = java.lang.String.valueOf(r4);	 Catch:{ Exception -> 0x09e9 }
        r2.<init>(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "...<br/><a href='http://vk.com'>";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x09e9 }
        r4 = r4.getResources();	 Catch:{ Exception -> 0x09e9 }
        r5 = 2131231237; // 0x7f080205 float:1.807855E38 double:1.0529681375E-314;
        r4 = r4.getString(r5);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r4 = "</a>";
        r2 = r2.append(r4);	 Catch:{ Exception -> 0x09e9 }
        r2 = r2.toString();	 Catch:{ Exception -> 0x09e9 }
        r4 = "\n";
        r5 = "<br/>";
        r2 = r2.replace(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r2 = android.text.Html.fromHtml(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = com.vkontakte.android.Global.replaceEmoji(r2);	 Catch:{ Exception -> 0x09e9 }
        r2 = (android.text.Spannable) r2;	 Catch:{ Exception -> 0x09e9 }
        r2 = stripUnderlines(r2);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.displayablePreviewText = r2;	 Catch:{ Exception -> 0x09e9 }
        goto L_0x07de;
    L_0x0cc6:
        r0 = r29;
        r2 = r0.lastComment;	 Catch:{ Exception -> 0x09e9 }
        r4 = "\\[(id|club)(\\d+)\\|([^\\]]+)\\]";
        r5 = "$3";
        r2 = r2.replaceAll(r4, r5);	 Catch:{ Exception -> 0x09e9 }
        r0 = r29;
        r0.lastComment = r2;	 Catch:{ Exception -> 0x09e9 }
        goto L_0x0844;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.NewsEntry.<init>(org.json.JSONObject, java.util.HashMap, java.util.HashMap, java.util.HashMap):void");
    }

    public static NewsEntry parsePhoto(JSONObject photo) throws Exception {
        NewsEntry e = new NewsEntry();
        e.type = TYPE_PHOTO;
        e.postID = photo.getInt(photo.has("id") ? "id" : "pid");
        e.ownerID = photo.getInt("owner_id");
        e.text = Global.replaceMentions(photo.optString("text"));
        e.time = photo.getInt("date");
        if (photo.has("likes")) {
            e.numLikes = photo.getJSONObject("likes").getInt("count");
            if (photo.getJSONObject("likes").getInt("user_likes") == TYPE_PHOTO) {
                e.flags |= TYPE_BIRTHDAY;
            }
        }
        e.flags |= TYPE_VIDEO;
        e.attachments.add(new PhotoAttachment(new Photo(photo)));
        return e;
    }

    public static NewsEntry parseVideo(JSONObject video) throws Exception {
        NewsEntry e = new NewsEntry();
        e.type = TYPE_VIDEO;
        e.postID = video.getInt(video.has("id") ? "id" : "vid");
        int i = video.getInt("owner_id");
        e.userID = i;
        e.ownerID = i;
        e.time = video.getInt("date");
        e.attachments.add(new VideoAttachment(video.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE), video.optString("photo_320", video.optString("image")), video.getInt("owner_id"), video.getInt(video.has("id") ? "id" : "vid"), video.getInt("duration")));
        return e;
    }

    public static NewsEntry parseTopic(JSONObject topic) throws Exception {
        NewsEntry e = new NewsEntry();
        e.type = TYPE_TOPIC;
        e.text = topic.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
        e.postID = topic.getInt("id");
        int i = topic.getInt("owner_id");
        e.userID = i;
        e.ownerID = i;
        return e;
    }

    NewsEntry(Parcel in) {
        this.userName = "UNKNOWN";
        this.type = TYPE_POST;
        this.text = ACRAConstants.DEFAULT_STRING_VALUE;
        this.retweetText = ACRAConstants.DEFAULT_STRING_VALUE;
        this.attachType = TYPE_POST;
        this.attachments = new ArrayList();
        this.repostAttachments = new ArrayList();
        this.flags = TYPE_POST;
        this.numRetweets = TYPE_POST;
        this.lastComment = null;
        this.userName = in.readString();
        this.userID = in.readInt();
        this.ownerID = in.readInt();
        this.postID = in.readInt();
        this.type = in.readInt();
        this.text = in.readString();
        this.time = in.readInt();
        this.numComments = in.readInt();
        this.flags = in.readInt();
        this.retweetUID = in.readInt();
        this.retweetUserName = in.readString();
        this.numLikes = in.readInt();
        this.attachType = in.readInt();
        this.attachTitle = in.readString();
        this.userPhotoURL = in.readString();
        this.retweetText = in.readString();
        this.retweetUserPhoto = in.readString();
        this.numRetweets = in.readInt();
        this.retweetOrigId = in.readInt();
        this.retweetOrigTime = in.readInt();
        this.retweetType = in.readInt();
        this.createdBy = in.readInt();
        try {
            int len = in.readInt();
            if (len > 0) {
                byte[] sa = new byte[len];
                in.readByteArray(sa);
                deserializeAttachments(sa);
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        if (this.text.length() <= 280) {
            this.displayablePreviewText = stripUnderlines((Spannable) Global.replaceEmoji(Html.fromHtml(this.text.replace("\n", "<br/>"))));
        } else {
            this.displayablePreviewText = stripUnderlines((Spannable) Global.replaceEmoji(Html.fromHtml(new StringBuilder(String.valueOf(this.text.substring(TYPE_POST, 280))).append("...<br/><a href='http://vk.com'>").append(VKApplication.context.getResources().getString(C0436R.string.post_show_full)).append("</a>").toString().replace("\n", "<br/>"))));
        }
        if (this.retweetText != null) {
            if (this.retweetText.split("\n").length > TYPE_ADDED_PHOTO) {
                int index = -1;
                for (int i = TYPE_POST; i < TYPE_ADDED_PHOTO; i += TYPE_PHOTO) {
                    index = this.retweetText.indexOf(TYPE_AUDIO, index + TYPE_PHOTO);
                }
                this.displayableRetweetText = stripUnderlines((Spannable) Html.fromHtml(new StringBuilder(String.valueOf(this.retweetText.substring(TYPE_POST, Math.min(280, index - 1)))).append("...<br/><a href='http://vk.com'>").append(VKApplication.context.getResources().getString(C0436R.string.post_show_full)).append("</a>").toString().replace("\n", "<br/>")));
            } else if (this.retweetText.length() <= 280) {
                this.displayableRetweetText = stripUnderlines((Spannable) Html.fromHtml(this.retweetText.replace("\n", "<br/>")));
            } else {
                this.displayableRetweetText = stripUnderlines((Spannable) Html.fromHtml(new StringBuilder(String.valueOf(this.retweetText.substring(TYPE_POST, 280))).append("...<br/><a href='http://vk.com'>").append(VKApplication.context.getResources().getString(C0436R.string.post_show_full)).append("</a>").toString().replace("\n", "<br/>")));
            }
        }
        layoutThumbs();
    }

    public NewsEntry(DataInputStream in) throws IOException {
        this.userName = "UNKNOWN";
        this.type = TYPE_POST;
        this.text = ACRAConstants.DEFAULT_STRING_VALUE;
        this.retweetText = ACRAConstants.DEFAULT_STRING_VALUE;
        this.attachType = TYPE_POST;
        this.attachments = new ArrayList();
        this.repostAttachments = new ArrayList();
        this.flags = TYPE_POST;
        this.numRetweets = TYPE_POST;
        this.lastComment = null;
        this.userName = in.readUTF();
        this.userID = in.readInt();
        this.ownerID = in.readInt();
        this.postID = in.readInt();
        this.type = in.readInt();
        this.text = in.readUTF();
        this.time = in.readInt();
        this.numComments = in.readInt();
        this.flags = in.readInt();
        this.retweetUID = in.readInt();
        this.retweetUserName = in.readUTF();
        this.numLikes = in.readInt();
        this.attachType = in.readInt();
        this.attachTitle = in.readUTF();
        this.userPhotoURL = in.readUTF();
        this.retweetText = in.readUTF();
        this.retweetUserPhoto = in.readUTF();
        this.numRetweets = in.readInt();
        this.retweetOrigId = in.readInt();
        this.retweetOrigTime = in.readInt();
        this.retweetType = in.readInt();
        this.createdBy = in.readInt();
        try {
            int len = in.readInt();
            if (len > 0) {
                byte[] atts = new byte[len];
                in.read(atts);
                deserializeAttachments(atts);
            } else {
                this.attachments = new ArrayList();
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        if (this.text.length() <= 280) {
            this.displayablePreviewText = stripUnderlines((Spannable) Global.replaceEmoji(Html.fromHtml(this.text.replace("\n", "<br/>"))));
        } else {
            this.displayablePreviewText = stripUnderlines((Spannable) Global.replaceEmoji(Html.fromHtml(new StringBuilder(String.valueOf(this.text.substring(TYPE_POST, 280))).append("...<br/><a href='http://vk.com'>").append(VKApplication.context.getResources().getString(C0436R.string.post_show_full)).append("</a>").toString().replace("\n", "<br/>"))));
        }
        layoutThumbs();
    }

    public int describeContents() {
        return TYPE_POST;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.userName);
        out.writeInt(this.userID);
        out.writeInt(this.ownerID);
        out.writeInt(this.postID);
        out.writeInt(this.type);
        out.writeString(this.text);
        out.writeInt(this.time);
        out.writeInt(this.numComments);
        out.writeInt(this.flags);
        out.writeInt(this.retweetUID);
        out.writeString(this.retweetUserName);
        out.writeInt(this.numLikes);
        out.writeInt(this.attachType);
        out.writeString(this.attachTitle);
        out.writeString(this.userPhotoURL);
        out.writeString(this.retweetText);
        out.writeString(this.retweetUserPhoto);
        out.writeInt(this.numRetweets);
        out.writeInt(this.retweetOrigId);
        out.writeInt(this.retweetOrigTime);
        out.writeInt(this.retweetType);
        out.writeInt(this.createdBy);
        byte[] sa = serializeAttachments();
        if (sa != null) {
            out.writeInt(sa.length);
            out.writeByteArray(sa);
            return;
        }
        out.writeInt(TYPE_POST);
    }

    public void writeToStream(DataOutputStream out) throws IOException {
        out.writeUTF(this.userName != null ? this.userName : ACRAConstants.DEFAULT_STRING_VALUE);
        out.writeInt(this.userID);
        out.writeInt(this.ownerID);
        out.writeInt(this.postID);
        out.writeInt(this.type);
        out.writeUTF(this.text != null ? this.text : ACRAConstants.DEFAULT_STRING_VALUE);
        out.writeInt(this.time);
        out.writeInt(this.numComments);
        out.writeInt(this.flags);
        out.writeInt(this.retweetUID);
        out.writeUTF(this.retweetUserName != null ? this.retweetUserName : ACRAConstants.DEFAULT_STRING_VALUE);
        out.writeInt(this.numLikes);
        out.writeInt(this.attachType);
        out.writeUTF(this.attachTitle != null ? this.attachTitle : ACRAConstants.DEFAULT_STRING_VALUE);
        out.writeUTF(this.userPhotoURL != null ? this.userPhotoURL : ACRAConstants.DEFAULT_STRING_VALUE);
        out.writeUTF(this.retweetText != null ? this.retweetText : ACRAConstants.DEFAULT_STRING_VALUE);
        out.writeUTF(this.retweetUserPhoto != null ? this.retweetUserPhoto : ACRAConstants.DEFAULT_STRING_VALUE);
        out.writeInt(this.numRetweets);
        out.writeInt(this.retweetOrigId);
        out.writeInt(this.retweetOrigTime);
        out.writeInt(this.retweetType);
        out.writeInt(this.createdBy);
        byte[] atts = serializeAttachments();
        if (atts != null) {
            out.writeInt(atts.length);
            out.write(atts);
            return;
        }
        out.writeInt(TYPE_POST);
    }

    static {
        CREATOR = new C03391();
    }

    private byte[] serializeAttachments() {
        if (this.attachments.size() == 0 && this.repostAttachments.size() == 0) {
            return null;
        }
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        DataOutputStream os = new DataOutputStream(buf);
        try {
            os.writeInt(this.attachments.size());
            Iterator it = this.attachments.iterator();
            while (it.hasNext()) {
                ((Attachment) it.next()).serialize(os);
            }
            os.writeInt(this.repostAttachments.size());
            it = this.repostAttachments.iterator();
            while (it.hasNext()) {
                ((Attachment) it.next()).serialize(os);
            }
            os.flush();
        } catch (Exception e) {
        }
        return buf.toByteArray();
    }

    public void writeToSQLite(SQLiteDatabase db, String table) {
        String str;
        byte[] serializeAttachments;
        int i;
        int i2 = TYPE_POST;
        ContentValues values = new ContentValues();
        values.put("pid", Integer.valueOf(this.postID));
        values.put("uid", Integer.valueOf(this.userID));
        values.put("text", this.text);
        values.put("time", Integer.valueOf(this.time));
        values.put("likes", Integer.valueOf(this.numLikes));
        values.put("comments", Integer.valueOf(this.numComments));
        values.put("username", this.userName);
        values.put("userphoto", this.userPhotoURL);
        values.put("retweet_uid", Integer.valueOf(flag(FLAG_IS_RETWEET) ? this.retweetUID : TYPE_POST));
        String str2 = "retweet_username";
        if (flag(FLAG_IS_RETWEET)) {
            str = this.retweetUserName;
        } else {
            str = null;
        }
        values.put(str2, str);
        str2 = "attachments";
        if (this.attachments.size() > 0 || this.repostAttachments.size() > 0) {
            serializeAttachments = serializeAttachments();
        } else {
            serializeAttachments = null;
        }
        values.put(str2, serializeAttachments);
        values.put("flags", Integer.valueOf(this.flags | (this.type << 24)));
        str2 = "retweet_text";
        if (flag(FLAG_IS_RETWEET)) {
            str = this.retweetText;
        } else {
            str = null;
        }
        values.put(str2, str);
        values.put("retweets", Integer.valueOf(this.numRetweets));
        str2 = "retweet_user_photo";
        if (flag(FLAG_IS_RETWEET)) {
            str = this.retweetUserPhoto;
        } else {
            str = null;
        }
        values.put(str2, str);
        str2 = "retweet_orig_id";
        if (flag(FLAG_IS_RETWEET)) {
            i = this.retweetOrigId;
        } else {
            i = TYPE_POST;
        }
        values.put(str2, Integer.valueOf(i));
        str2 = "retweet_orig_time";
        if (flag(FLAG_IS_RETWEET)) {
            i = this.retweetOrigTime;
        } else {
            i = TYPE_POST;
        }
        values.put(str2, Integer.valueOf(i));
        str = "retweet_type";
        if (flag(FLAG_IS_RETWEET)) {
            i2 = this.retweetType;
        }
        values.put(str, Integer.valueOf(i2));
        values.put("created_by", Integer.valueOf(this.createdBy));
        if (this.lastComment != null) {
            values.put("last_comment_name", this.lastCommentUserName);
            values.put("last_comment_photo", this.lastCommentUserPhoto);
            values.put("last_comment_text", this.lastComment);
            values.put("last_comment_time", Integer.valueOf(this.lastCommentTime));
        }
        db.insert(table, null, values);
    }

    public void readFromSQLite(Cursor cursor, Context context) {
        ContentValues values = new ContentValues();
        DatabaseUtils.cursorRowToContentValues(cursor, values);
        this.postID = values.getAsInteger("pid").intValue();
        int intValue = values.getAsInteger("uid").intValue();
        this.ownerID = intValue;
        this.userID = intValue;
        this.text = values.getAsString("text");
        this.time = values.getAsInteger("time").intValue();
        this.numLikes = values.getAsInteger("likes").intValue();
        this.numRetweets = values.getAsInteger("retweets").intValue();
        this.numComments = values.getAsInteger("comments").intValue();
        this.userName = values.getAsString("username");
        this.userPhotoURL = values.getAsString("userphoto");
        deserializeAttachments(values.getAsByteArray("attachments"));
        this.flags = values.getAsInteger("flags").intValue();
        if ((this.flags & FLAG_IS_RETWEET) > 0) {
            this.retweetUID = values.getAsInteger("retweet_uid").intValue();
            this.retweetUserName = values.getAsString("retweet_username");
            this.retweetText = values.getAsString("retweet_text");
            this.retweetUserPhoto = values.getAsString("retweet_user_photo");
            this.retweetOrigId = values.getAsInteger("retweet_orig_id").intValue();
            this.retweetOrigTime = values.getAsInteger("retweet_orig_time").intValue();
            this.retweetType = values.getAsInteger("retweet_type").intValue();
        }
        this.type = this.flags >> 24;
        if (values.containsKey("last_comment_name")) {
            this.lastCommentUserName = values.getAsString("last_comment_name");
            this.lastCommentUserPhoto = values.getAsString("last_comment_photo");
            this.lastComment = values.getAsString("last_comment_text");
            this.lastCommentTime = values.getAsInteger("last_comment_time").intValue();
        }
        this.createdBy = values.getAsInteger("created_by").intValue();
        layoutThumbs();
        if (this.type == TYPE_TOPIC) {
            this.displayablePreviewText = stripUnderlines((Spannable) Html.fromHtml(new StringBuilder(String.valueOf(VKApplication.context.getResources().getString(C0436R.string.feed_comments_topic))).append(" <a href='http://vk.com'>").append(this.text).append("</a>").toString()));
            this.flags |= FLAG_GRAY_TEXT;
        } else if (this.text.length() <= 280) {
            this.displayablePreviewText = stripUnderlines((Spannable) Global.replaceEmoji(Html.fromHtml(this.text.replace("\n", "<br/>"))));
        } else {
            this.displayablePreviewText = stripUnderlines((Spannable) Global.replaceEmoji(Html.fromHtml(new StringBuilder(String.valueOf(this.text.substring(TYPE_POST, 280))).append("...<br/><a href='http://vk.com'>").append(VKApplication.context.getResources().getString(C0436R.string.post_show_full)).append("</a>").toString().replace("\n", "<br/>"))));
        }
        if (this.retweetText.length() <= 280) {
            this.displayableRetweetText = stripUnderlines((Spannable) Global.replaceEmoji(Html.fromHtml(this.retweetText.replace("\n", "<br/>"))));
        } else {
            this.displayableRetweetText = stripUnderlines((Spannable) Global.replaceEmoji(Html.fromHtml(new StringBuilder(String.valueOf(this.retweetText.substring(TYPE_POST, 280))).append("...<br/><a href='http://vk.com'>").append(VKApplication.context.getResources().getString(C0436R.string.post_show_full)).append("</a>").toString().replace("\n", "<br/>"))));
        }
    }

    private void deserializeAttachments(byte[] b) {
        if (b != null) {
            DataInputStream is = new DataInputStream(new ByteArrayInputStream(b));
            try {
                int i;
                int count = is.readInt();
                for (i = TYPE_POST; i < count; i += TYPE_PHOTO) {
                    this.attachments.add(Attachment.deserialize(is, is.readInt()));
                }
                count = is.readInt();
                for (i = TYPE_POST; i < count; i += TYPE_PHOTO) {
                    this.repostAttachments.add(Attachment.deserialize(is, is.readInt()));
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    public int getImageCount() {
        int cnt = TYPE_POST;
        Iterator it = this.repostAttachments.iterator();
        while (it.hasNext()) {
            if (((Attachment) it.next()) instanceof ImageAttachment) {
                cnt += TYPE_PHOTO;
            }
        }
        it = this.attachments.iterator();
        while (it.hasNext()) {
            if (((Attachment) it.next()) instanceof ImageAttachment) {
                cnt += TYPE_PHOTO;
            }
        }
        return cnt;
    }

    public ImageAttachment getImageAttachment(int idx) {
        int cnt = TYPE_POST;
        Iterator it = this.repostAttachments.iterator();
        while (it.hasNext()) {
            Attachment att = (Attachment) it.next();
            if (att instanceof ImageAttachment) {
                if (cnt == idx) {
                    return (ImageAttachment) att;
                }
                cnt += TYPE_PHOTO;
            }
        }
        it = this.attachments.iterator();
        while (it.hasNext()) {
            att = (Attachment) it.next();
            if (att instanceof ImageAttachment) {
                if (cnt == idx) {
                    return (ImageAttachment) att;
                }
                cnt += TYPE_PHOTO;
            }
        }
        return null;
    }

    public boolean flag(int f) {
        return (this.flags & f) > 0;
    }

    public void flag(int f, boolean v) {
        if (v) {
            this.flags |= f;
        } else {
            this.flags &= f ^ -1;
        }
    }

    public boolean equals(NewsEntry e) {
        if (e != null && e.ownerID == this.ownerID && e.postID == this.postID && e.userID == this.userID && e.type == this.type) {
            return true;
        }
        return false;
    }

    public void updateRetweetText() {
        if (this.retweetText.split("\n").length > TYPE_ADDED_PHOTO) {
            int index = -1;
            for (int i = TYPE_POST; i < TYPE_ADDED_PHOTO; i += TYPE_PHOTO) {
                index = this.retweetText.indexOf(TYPE_AUDIO, index + TYPE_PHOTO);
            }
            this.displayableRetweetText = stripUnderlines((Spannable) Html.fromHtml(new StringBuilder(String.valueOf(this.retweetText.substring(TYPE_POST, Math.min(280, index - 1)))).append("...<br/><a href='http://vk.com'>").append(VKApplication.context.getResources().getString(C0436R.string.post_show_full)).append("</a>").toString().replace("\n", "<br/>")));
        } else if (this.retweetText.length() <= 280) {
            this.displayableRetweetText = stripUnderlines((Spannable) Html.fromHtml(this.retweetText.replace("\n", "<br/>")));
        } else {
            this.displayableRetweetText = stripUnderlines((Spannable) Html.fromHtml(new StringBuilder(String.valueOf(this.retweetText.substring(TYPE_POST, 280))).append("...<br/><a href='http://vk.com'>").append(VKApplication.context.getResources().getString(C0436R.string.post_show_full)).append("</a>").toString().replace("\n", "<br/>")));
        }
    }

    public static Spannable stripUnderlines(Spannable s) {
        int i;
        URLSpan[] spans = (URLSpan[]) s.getSpans(TYPE_POST, s.length(), URLSpan.class);
        int length = spans.length;
        for (i = TYPE_POST; i < length; i += TYPE_PHOTO) {
            URLSpan span = spans[i];
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            s.setSpan(new URLSpanNoUnderline(span.getURL()), start, end, TYPE_POST);
        }
        ForegroundColorSpan[] aspans = (ForegroundColorSpan[]) s.getSpans(TYPE_POST, s.length(), ForegroundColorSpan.class);
        length = aspans.length;
        for (i = TYPE_POST; i < length; i += TYPE_PHOTO) {
            ForegroundColorSpan span2 = aspans[i];
            start = s.getSpanStart(span2);
            end = s.getSpanEnd(span2);
            s.removeSpan(span2);
            s.setSpan(new XColorSpan(span2.getForegroundColor()), start, end, TYPE_POST);
        }
        return s;
    }

    public void layoutThumbs() {
        DisplayMetrics metrics = VKApplication.context.getResources().getDisplayMetrics();
        int tSize = Math.min(metrics.widthPixels, metrics.heightPixels) - Math.round((float) ((Global.scale(9.0f) * TYPE_VIDEO) + (Global.scale(5.0f) * TYPE_VIDEO)));
        ZhukovLayout.processThumbs(tSize, tSize, this.attachments);
        ZhukovLayout.processThumbs(tSize, tSize, this.repostAttachments);
    }

    private static String ell(String s, int lim) {
        if (s.length() > lim) {
            return s.substring(TYPE_POST, lim) + "...";
        }
        return s;
    }

    public String toString() {
        return "NewsEntry {id=" + this.ownerID + "_" + this.postID + ", text=" + ell(this.text, 200) + ", repostText=" + ell(this.retweetText, 200) + ", flags=" + this.flags + ", type=" + this.type + ", attachments=" + this.attachments + ", repostAttachments=" + this.repostAttachments + "}";
    }
}
