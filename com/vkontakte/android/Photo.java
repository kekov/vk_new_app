package com.vkontakte.android;

import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.facebook.WebDialog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public class Photo implements Parcelable {
    public static final Creator<Photo> CREATOR;
    public String accessKey;
    public int albumID;
    public boolean canComment;
    public ArrayList<NewsComment> comments;
    public boolean commentsLoaded;
    public int date;
    public String descr;
    public String fullURL;
    public int id;
    public boolean infoLoaded;
    public boolean isLiked;
    public double lat;
    public ArrayList<String> likePhotos;
    public double lon;
    public int nComments;
    public int nLikes;
    public int nTags;
    public int ownerID;
    public String ownerName;
    public String ownerPhoto;
    public String placeAddress;
    public ArrayList<Image> sizes;
    public ArrayList<PhotoTag> tags;
    public String thumbURL;
    public UserProfile user;
    public int userID;
    public Rect viewBounds;
    public int viewClipTop;

    /* renamed from: com.vkontakte.android.Photo.1 */
    class C03801 implements Creator<Photo> {
        C03801() {
        }

        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    }

    public static class Image {
        public int height;
        public char type;
        public String url;
        public int width;
    }

    public Photo() {
        this.infoLoaded = false;
        this.commentsLoaded = false;
        this.canComment = true;
        this.tags = new ArrayList();
        this.sizes = new ArrayList();
        this.comments = new ArrayList();
        this.lat = -9000.0d;
        this.lon = -9000.0d;
        this.likePhotos = new ArrayList();
    }

    public Photo(Parcel p) {
        boolean z;
        boolean z2 = true;
        this.infoLoaded = false;
        this.commentsLoaded = false;
        this.canComment = true;
        this.tags = new ArrayList();
        this.sizes = new ArrayList();
        this.comments = new ArrayList();
        this.lat = -9000.0d;
        this.lon = -9000.0d;
        this.likePhotos = new ArrayList();
        this.id = p.readInt();
        this.albumID = p.readInt();
        this.ownerID = p.readInt();
        this.userID = p.readInt();
        this.date = p.readInt();
        this.nLikes = p.readInt();
        this.nComments = p.readInt();
        this.nTags = p.readInt();
        this.infoLoaded = p.readInt() == 1;
        if (p.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.isLiked = z;
        if (p.readInt() != 1) {
            z2 = false;
        }
        this.canComment = z2;
        this.thumbURL = p.readString();
        this.fullURL = p.readString();
        this.descr = p.readString();
        this.lat = p.readDouble();
        this.lon = p.readDouble();
        this.accessKey = p.readString();
        this.viewBounds = (Rect) p.readParcelable(ClassLoader.getSystemClassLoader());
        this.viewClipTop = p.readInt();
        deserializeImages(p.readString());
    }

    public Photo(PhotoAttachment att) {
        this.infoLoaded = false;
        this.commentsLoaded = false;
        this.canComment = true;
        this.tags = new ArrayList();
        this.sizes = new ArrayList();
        this.comments = new ArrayList();
        this.lat = -9000.0d;
        this.lon = -9000.0d;
        this.likePhotos = new ArrayList();
        this.fullURL = att.srcBig;
        this.thumbURL = att.getThumbURL();
        this.albumID = att.aid;
        this.id = att.pid;
        this.ownerID = att.oid;
        this.userID = att.uid;
        this.date = att.date;
        this.descr = att.descr;
        this.accessKey = att.accessKey;
        this.sizes = new ArrayList();
        HashMap<String, com.vkontakte.android.PhotoAttachment.Image> sizes = att.images;
        for (String k : sizes.keySet()) {
            com.vkontakte.android.PhotoAttachment.Image im = (com.vkontakte.android.PhotoAttachment.Image) sizes.get(k);
            Image p = new Image();
            p.height = im.height;
            p.width = im.width;
            p.type = im.type;
            p.url = im.url;
            this.sizes.add(p);
        }
    }

    public Photo(JSONObject obj) {
        boolean z = true;
        this.infoLoaded = false;
        this.commentsLoaded = false;
        this.canComment = true;
        this.tags = new ArrayList();
        this.sizes = new ArrayList();
        this.comments = new ArrayList();
        this.lat = -9000.0d;
        this.lon = -9000.0d;
        this.likePhotos = new ArrayList();
        try {
            this.id = obj.optInt("id", obj.optInt("pid"));
            this.ownerID = obj.getInt("owner_id");
            this.albumID = obj.optInt("album_id");
            this.userID = obj.optInt("user_id", this.ownerID);
            if (this.userID == 100) {
                this.userID = this.ownerID;
            }
            this.descr = obj.optString("text", ACRAConstants.DEFAULT_STRING_VALUE);
            this.accessKey = obj.optString("access_key");
            this.date = obj.optInt("date", obj.optInt("created"));
            if (obj.has("comments") && obj.has("tags")) {
                this.infoLoaded = true;
                this.nComments = obj.getJSONObject("comments").getInt("count");
                this.nTags = obj.getJSONObject("tags").getInt("count");
            }
            if (obj.has("likes")) {
                this.nLikes = obj.getJSONObject("likes").getInt("count");
                this.isLiked = obj.getJSONObject("likes").getInt("user_likes") == 1;
            }
            if (obj.optInt("can_comment", 1) != 1) {
                z = false;
            }
            this.canComment = z;
            JSONArray sizes = obj.getJSONArray("sizes");
            for (int i = 0; i < sizes.length(); i++) {
                JSONObject size = sizes.getJSONObject(i);
                Image img = new Image();
                img.height = size.getInt("height");
                img.width = size.getInt("width");
                img.type = size.getString(WebDialog.DIALOG_PARAM_TYPE).charAt(0);
                img.url = size.getString("src");
                this.sizes.add(img);
            }
            if (obj.has("lat") && obj.has("long")) {
                this.lat = obj.getDouble("lat");
                this.lon = obj.getDouble("long");
            }
            this.accessKey = obj.optString("access_key");
        } catch (Exception x) {
            Log.m531w("vk", "Error parsing photo!", x);
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel p, int flags) {
        int i;
        int i2 = 1;
        p.writeInt(this.id);
        p.writeInt(this.albumID);
        p.writeInt(this.ownerID);
        p.writeInt(this.userID);
        p.writeInt(this.date);
        p.writeInt(this.nLikes);
        p.writeInt(this.nComments);
        p.writeInt(this.nTags);
        p.writeInt(this.infoLoaded ? 1 : 0);
        if (this.isLiked) {
            i = 1;
        } else {
            i = 0;
        }
        p.writeInt(i);
        if (!this.canComment) {
            i2 = 0;
        }
        p.writeInt(i2);
        p.writeString(this.thumbURL);
        p.writeString(this.fullURL);
        p.writeString(this.descr);
        p.writeDouble(this.lat);
        p.writeDouble(this.lon);
        p.writeString(this.accessKey);
        p.writeParcelable(this.viewBounds, 0);
        p.writeInt(this.viewClipTop);
        p.writeString(serializeImages());
    }

    static {
        CREATOR = new C03801();
    }

    public Image getImage(char s) {
        Iterator it = this.sizes.iterator();
        while (it.hasNext()) {
            Image im = (Image) it.next();
            if (im.type == s) {
                return im;
            }
        }
        return null;
    }

    public Image getImage(char[] sizes) {
        for (char c : sizes) {
            Image im = getImage(c);
            if (im != null) {
                return im;
            }
        }
        return null;
    }

    public Image getImage(char s, char fallback) {
        Image im = getImage(s);
        return im != null ? im : getImage(fallback);
    }

    public String serializeImages() {
        ArrayList<String> ss = new ArrayList();
        Iterator it = this.sizes.iterator();
        while (it.hasNext()) {
            Image im = (Image) it.next();
            ss.add(im.type + "^" + im.url + "^" + im.width + "^" + im.height);
        }
        return TextUtils.join("$", ss);
    }

    public void deserializeImages(String s) {
        try {
            String srcX = null;
            String srcY = null;
            for (String p : s.split("\\$")) {
                String[] pp = p.split("\\^");
                Image im = new Image();
                im.type = pp[0].charAt(0);
                im.url = pp[1];
                im.width = Integer.parseInt(pp[2]);
                im.height = Integer.parseInt(pp[3]);
                this.sizes.add(im);
                if (im.type == 'm') {
                    this.thumbURL = im.url;
                }
                if (im.type == 'x') {
                    srcX = im.url;
                }
                if (im.type == 'y') {
                    srcY = im.url;
                }
            }
            if (Global.displayDensity > 1.0f || Global.isTablet) {
                if (srcY == null) {
                    srcY = srcX;
                }
                this.fullURL = srcY;
                return;
            }
            this.fullURL = srcX;
        } catch (Exception e) {
        }
    }
}
