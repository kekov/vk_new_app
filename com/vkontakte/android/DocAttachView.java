package com.vkontakte.android;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.vkontakte.android.ui.GifView;
import java.io.File;
import org.acra.ACRAConstants;

public class DocAttachView extends LinearLayout implements OnClickListener, OnLongClickListener {
    private static GifView gifView;
    public String fileName;
    public String thumb;
    String url;

    public DocAttachView(Context context) {
        super(context);
        this.url = ACRAConstants.DEFAULT_STRING_VALUE;
    }

    public DocAttachView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.url = ACRAConstants.DEFAULT_STRING_VALUE;
        setOnClickListener(this);
        setOnLongClickListener(this);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (gifView != null && gifView.getParent() == this) {
            gifView = null;
        }
    }

    public void setData(String docName, String dlLink, int size, String thumb) {
        this.url = dlLink;
        this.fileName = docName;
        this.thumb = thumb;
        ((TextView) findViewById(C0436R.id.att_doc_title)).setText(docName);
        if (this.url == null || this.url.length() == 0) {
            setOnClickListener(null);
            setOnLongClickListener(null);
            setBackgroundColor(-1);
            getChildAt(0).setLayoutParams(new LayoutParams(Global.scale(150.0f), Global.scale(150.0f)));
        }
    }

    public void onMeasure(int w, int h) {
        super.onMeasure(w, h);
        if (this.thumb == null || this.thumb.length() == 0 || (gifView != null && gifView.getParent() == this)) {
            setMeasuredDimension(MeasureSpec.getSize(w), getMeasuredHeight());
        }
    }

    public void onClick(View v) {
        if (this.fileName.endsWith(".gif")) {
            if (gifView == null) {
                gifView = new GifView(getContext());
                getChildAt(0).setVisibility(8);
                addView(gifView);
                gifView.loadURL(this.url);
                return;
            }
            boolean otherView;
            if (gifView.getParent() != this) {
                otherView = true;
            } else {
                otherView = false;
            }
            ((ViewGroup) gifView.getParent()).getChildAt(0).setVisibility(0);
            ((ViewGroup) gifView.getParent()).removeView(gifView);
            gifView = null;
            requestLayout();
            if (otherView) {
                onClick(v);
            }
        } else if (!this.url.startsWith("http")) {
        } else {
            if (this.thumb != null && this.thumb.length() > 0) {
                Bundle args = new Bundle();
                args.putString("doc_url", this.url);
                args.putString("doc_title", this.fileName);
                Navigate.to("PhotoViewerFragment", args, (Activity) getContext(), true, -1, -1);
            } else if (VERSION.SDK_INT >= 14) {
                Uri uri = Uri.parse(this.url);
                Request req = new Request(uri);
                req.setDestinationUri(Uri.fromFile(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), uri.getLastPathSegment())));
                if (VERSION.SDK_INT >= 14) {
                    req.setNotificationVisibility(1);
                    req.allowScanningByMediaScanner();
                }
                ((DownloadManager) getContext().getSystemService("download")).enqueue(req);
            } else {
                getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.url)));
            }
        }
    }

    public void reset() {
        if (gifView != null && gifView.getParent() == this) {
            ((ViewGroup) gifView.getParent()).getChildAt(0).setVisibility(0);
            ((ViewGroup) gifView.getParent()).removeView(gifView);
            gifView = null;
            requestLayout();
        }
    }

    public boolean onLongClick(View v) {
        if (!this.fileName.endsWith(".gif")) {
            return false;
        }
        getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.url)));
        return true;
    }
}
