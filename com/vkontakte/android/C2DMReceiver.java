package com.vkontakte.android;

import android.content.Context;
import android.content.Intent;
import com.google.android.gcm.GCMBaseIntentService;
import com.vkontakte.android.api.C2DMRegisterDevice;
import com.vkontakte.android.api.C2DMRegisterDevice.Callback;
import com.vkontakte.android.api.C2DMUnregisterDevice;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import org.acra.ACRAConstants;

public class C2DMReceiver extends GCMBaseIntentService {

    /* renamed from: com.vkontakte.android.C2DMReceiver.1 */
    class C12751 implements Callback {
        private final /* synthetic */ String val$registrationId;

        C12751(String str) {
            this.val$registrationId = str;
        }

        public void success() {
        }

        public void fail(int ecode, String emsg) {
            VKApplication.context.getSharedPreferences(null, 0).edit().putString("need_update_gcm", this.val$registrationId).commit();
        }
    }

    public C2DMReceiver() {
        super(ACRAConstants.DEFAULT_STRING_VALUE);
    }

    protected void onMessage(Context context, Intent _intent) {
    }

    public void onError(Context context, String errorId) {
        Log.m526e("vk", "GCM error! " + errorId);
        log("Error: " + errorId);
    }

    public void onRegistered(Context context, String registrationId) {
        Log.m528i("vk", "GCM Registered!");
        VKApplication.context.getSharedPreferences(null, 0).edit().putString("c2dm_regID", registrationId).commit();
        new C2DMRegisterDevice(registrationId).setCallback(new C12751(registrationId)).exec();
    }

    protected void onUnregistered(Context arg0, String s) {
        Log.m528i("vk", "GCM Unregistered!");
        log("Unregistered, s=" + s);
        if (VKApplication.context.getSharedPreferences(null, 0).contains("uid")) {
            String oldToken = VKApplication.context.getSharedPreferences(null, 0).getString("c2dm_regID", null);
            if (oldToken != null) {
                new C2DMUnregisterDevice(oldToken).exec();
            }
        }
    }

    static void log(String l) {
        if (Global.uid == 1708231) {
            try {
                FileOutputStream os = new FileOutputStream(new File("/sdcard/vkpush.log"), true);
                Calendar c = Calendar.getInstance();
                os.write(String.format("[%02d:%02d, %02d-%02d-%d] %s\n", new Object[]{Integer.valueOf(c.get(11)), Integer.valueOf(c.get(12)), Integer.valueOf(c.get(5)), Integer.valueOf(c.get(2) + 1), Integer.valueOf(c.get(1)), l}).getBytes("UTF-8"));
                os.close();
            } catch (Exception e) {
            }
        }
    }
}
