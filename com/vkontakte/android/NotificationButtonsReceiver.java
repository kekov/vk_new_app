package com.vkontakte.android;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.vkontakte.android.api.FriendsAdd;
import com.vkontakte.android.api.FriendsAdd.Callback;
import com.vkontakte.android.api.FriendsDelete;
import com.vkontakte.android.cache.Cache;
import com.vkontakte.android.data.Friends;
import java.util.HashMap;
import org.acra.ACRAConstants;

public class NotificationButtonsReceiver extends BroadcastReceiver {
    public static final String ACTION_ACCEPT_FRIEND = "com.vkontakte.android.ACCEPT_FRIEND";
    public static final String ACTION_DECLINE_FRIEND = "com.vkontakte.android.DECLINE_FRIEND";

    /* renamed from: com.vkontakte.android.NotificationButtonsReceiver.1 */
    class C13281 implements Callback {
        private final /* synthetic */ int val$uid;

        C13281(int i) {
            this.val$uid = i;
        }

        public void success(int uid, int result) {
            Friends.reload(true);
        }

        public void fail(int ecode, String emsg) {
            HashMap<String, String> args = new HashMap();
            args.put("uid", new StringBuilder(String.valueOf(this.val$uid)).toString());
            Cache.putApiRequest("friends.add", args);
        }
    }

    /* renamed from: com.vkontakte.android.NotificationButtonsReceiver.2 */
    class C13292 implements FriendsDelete.Callback {
        private final /* synthetic */ int val$uid;

        C13292(int i) {
            this.val$uid = i;
        }

        public void success(int uid, int result) {
            Friends.reload(true);
        }

        public void fail(int ecode, String emsg) {
            HashMap<String, String> args = new HashMap();
            args.put("uid", new StringBuilder(String.valueOf(this.val$uid)).toString());
            Cache.putApiRequest("friends.delete", args);
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (ACTION_ACCEPT_FRIEND.equals(intent.getAction())) {
            ((NotificationManager) context.getSystemService("notification")).cancel(intent.getIntExtra("notifyId", GCMBroadcastReceiver.ID_FRIEND_NOTIFICATION));
            int uid = intent.getIntExtra("uid", 0);
            new FriendsAdd(uid, ACRAConstants.DEFAULT_STRING_VALUE).setCallback(new C13281(uid)).exec();
        }
        if (ACTION_DECLINE_FRIEND.equals(intent.getAction())) {
            ((NotificationManager) context.getSystemService("notification")).cancel(intent.getIntExtra("notifyId", GCMBroadcastReceiver.ID_FRIEND_NOTIFICATION));
            uid = intent.getIntExtra("uid", 0);
            new FriendsDelete(uid).setCallback(new C13292(uid)).exec();
        }
    }
}
