package com.vkontakte.android;

import com.facebook.NativeProtocol;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.vkontakte.android.api.C2DMRegisterDevice;
import com.vkontakte.android.api.C2DMUnregisterDevice;

public class C2DM {

    /* renamed from: com.vkontakte.android.C2DM.1 */
    class C02121 implements Runnable {
        private final /* synthetic */ boolean val$check;

        C02121(boolean z) {
            this.val$check = z;
        }

        public void run() {
            try {
                String token = VKApplication.context.getSharedPreferences(GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE, 0).getString("reg", null);
                int tokenVer = VKApplication.context.getSharedPreferences(GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE, 0).getInt(NativeProtocol.PLATFORM_PROVIDER_VERSION_COLUMN, 0);
                int appVer = VKApplication.context.getPackageManager().getPackageInfo(VKApplication.context.getPackageName(), 0).versionCode;
                boolean reg = false;
                if (token == null || appVer != tokenVer) {
                    token = GoogleCloudMessaging.getInstance(VKApplication.context).register("841415684880");
                    VKApplication.context.getSharedPreferences(GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE, 0).edit().putString("reg", token).putInt(NativeProtocol.PLATFORM_PROVIDER_VERSION_COLUMN, appVer).commit();
                    reg = true;
                }
                if (reg || !this.val$check) {
                    Log.m525d("vk", "GCM register ok, id=" + token);
                    new C2DMRegisterDevice(token).exec();
                }
            } catch (Exception x) {
                C2DMReceiver.log("Registration error " + x);
                Log.m531w("vk", "Error registering GCM", x);
            }
        }
    }

    public static void start(boolean check) {
        new Thread(new C02121(check)).start();
    }

    public static void start() {
        start(false);
    }

    public static void checkForUpdate() {
        start(true);
    }

    public static void stop() {
        try {
            String token = VKApplication.context.getSharedPreferences(GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE, 0).getString("reg", null);
            if (token != null) {
                new C2DMUnregisterDevice(token).exec();
            }
        } catch (Exception e) {
        }
    }
}
