package com.vkontakte.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.WallRepost;
import com.vkontakte.android.api.WallRepost.Callback;
import com.vkontakte.android.cache.NewsfeedCache;
import com.vkontakte.android.cache.UserWallCache;
import com.vkontakte.android.data.Posts;
import java.util.ArrayList;
import org.acra.ACRAConstants;

public class RepostActivity extends Activity {
    private static final int SEL_CHAT_RESULT = 102;
    private static final int SEL_GROUP_RESULT = 101;
    private String groupName;
    private String groupPhoto;
    private NewsEntry post;

    /* renamed from: com.vkontakte.android.RepostActivity.1 */
    class C04391 implements OnClickListener {
        private final /* synthetic */ ArrayList val$acts;

        C04391(ArrayList arrayList) {
            this.val$acts = arrayList;
        }

        public void onClick(DialogInterface dialog, int which) {
            String act = (String) this.val$acts.get(which);
            if ("me".equals(act)) {
                RepostActivity.this.repostWithComment(0);
            } else if ("group".equals(act)) {
                RepostActivity.this.startGroupChooser();
            } else if ("msg".equals(act)) {
                RepostActivity.this.startChatChooser();
            }
        }
    }

    /* renamed from: com.vkontakte.android.RepostActivity.2 */
    class C04402 implements OnCancelListener {
        C04402() {
        }

        public void onCancel(DialogInterface dialog) {
            RepostActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.RepostActivity.3 */
    class C04413 implements OnClickListener {
        private final /* synthetic */ EditText val$ed;
        private final /* synthetic */ int val$gid;

        C04413(int i, EditText editText) {
            this.val$gid = i;
            this.val$ed = editText;
        }

        public void onClick(DialogInterface dialog, int which) {
            RepostActivity.this.doRepostWithComment(this.val$gid, this.val$ed.getText().toString());
        }
    }

    /* renamed from: com.vkontakte.android.RepostActivity.4 */
    class C04424 implements OnClickListener {
        C04424() {
        }

        public void onClick(DialogInterface dialog, int which) {
            RepostActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.RepostActivity.5 */
    class C04435 implements OnCancelListener {
        C04435() {
        }

        public void onCancel(DialogInterface dialog) {
            RepostActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.RepostActivity.6 */
    class C04446 implements OnShowListener {
        private final /* synthetic */ EditText val$ed;

        C04446(EditText editText) {
            this.val$ed = editText;
        }

        public void onShow(DialogInterface dialog) {
            ((InputMethodManager) RepostActivity.this.getSystemService("input_method")).showSoftInput(this.val$ed, 1);
        }
    }

    /* renamed from: com.vkontakte.android.RepostActivity.7 */
    class C13437 implements Callback {
        private final /* synthetic */ String val$comment;
        private final /* synthetic */ int val$gid;

        C13437(int i, String str) {
            this.val$gid = i;
            this.val$comment = str;
        }

        public void success(int repostID, int reposts, int likes) {
            String string;
            Log.m525d("vk", "repost id=" + repostID + ", rp=" + reposts + ", lk=" + likes);
            SharedPreferences prefs = RepostActivity.this.getSharedPreferences(null, 0);
            NewsEntry newPost = new NewsEntry(RepostActivity.this.post);
            if (this.val$gid == 0) {
                RepostActivity.this.post.flag(4, true);
            }
            newPost.flag(32, true);
            newPost.flag(TransportMediator.FLAG_KEY_MEDIA_NEXT, true);
            newPost.postID = repostID;
            newPost.time = (int) (System.currentTimeMillis() / 1000);
            int i = this.val$gid != 0 ? -this.val$gid : Global.uid;
            newPost.userID = i;
            newPost.ownerID = i;
            newPost.userName = this.val$gid == 0 ? prefs.getString("username", ACRAConstants.DEFAULT_STRING_VALUE) : RepostActivity.this.groupName;
            if (this.val$gid == 0) {
                string = prefs.getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE);
            } else {
                string = RepostActivity.this.groupPhoto;
            }
            newPost.userPhotoURL = string;
            newPost.retweetText = this.val$comment;
            if ((RepostActivity.this.post.retweetText != null && RepostActivity.this.post.retweetText.length() > 0) || RepostActivity.this.post.repostAttachments.size() > 0) {
                newPost.text = RepostActivity.this.post.retweetText;
                newPost.attachments = RepostActivity.this.post.repostAttachments;
                if (newPost.attachments.size() > 0 && (newPost.attachments.get(newPost.attachments.size() - 1) instanceof RepostAttachment)) {
                    newPost.attachments.remove(newPost.attachments.size() - 1);
                }
                newPost.attachments.add(new RepostAttachment(RepostActivity.this.post.retweetUID, RepostActivity.this.post.retweetOrigId, RepostActivity.this.post.retweetOrigTime, RepostActivity.this.post.retweetUserName, RepostActivity.this.post.retweetUserPhoto, 0));
                newPost.retweetOrigId = RepostActivity.this.post.postID;
                newPost.retweetOrigTime = RepostActivity.this.post.time;
                newPost.retweetUID = RepostActivity.this.post.ownerID;
                newPost.retweetUserName = RepostActivity.this.post.userName;
                newPost.retweetUserPhoto = RepostActivity.this.post.userPhotoURL;
            } else if (RepostActivity.this.post.flag(32)) {
                newPost.retweetOrigId = RepostActivity.this.post.retweetOrigId;
                newPost.retweetOrigTime = RepostActivity.this.post.retweetOrigTime;
                newPost.retweetUID = RepostActivity.this.post.retweetUID;
                newPost.retweetUserName = RepostActivity.this.post.retweetUserName;
                newPost.retweetUserPhoto = RepostActivity.this.post.retweetUserPhoto;
            } else {
                newPost.retweetOrigId = RepostActivity.this.post.postID;
                newPost.retweetOrigTime = RepostActivity.this.post.time;
                newPost.retweetUID = RepostActivity.this.post.ownerID;
                newPost.retweetUserName = RepostActivity.this.post.userName;
                newPost.retweetUserPhoto = RepostActivity.this.post.userPhotoURL;
            }
            NewsfeedCache.add(newPost, RepostActivity.this.getApplicationContext());
            if (this.val$gid == 0) {
                UserWallCache.add(newPost, RepostActivity.this.getApplicationContext());
            }
            Intent intent = new Intent(Posts.ACTION_NEW_POST_BROADCAST);
            intent.putExtra("entry", newPost);
            RepostActivity.this.sendBroadcast(intent);
            if (this.val$gid == 0) {
                Toast.makeText(RepostActivity.this, C0436R.string.repost_ok_wall, 0).show();
            } else {
                Toast.makeText(RepostActivity.this, C0436R.string.repost_ok_group, 0).show();
            }
            RepostActivity.this.post.numRetweets = reposts;
            RepostActivity.this.post.numLikes = likes;
            RepostActivity.this.broadcastUpdate();
            RepostActivity.this.finish();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(RepostActivity.this, C0436R.string.error, 0).show();
            RepostActivity.this.finish();
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        overridePendingTransition(0, 0);
        this.post = (NewsEntry) getIntent().getParcelableExtra("post");
        ArrayList<String> items = new ArrayList();
        ArrayList<String> acts = new ArrayList();
        if ((this.post.flag(1) || (this.post.ownerID == Global.uid && this.post.ownerID == this.post.userID)) && !this.post.flag(NewsEntry.FLAG_FRIENDS_ONLY)) {
            if (!(this.post.flag(4) || this.post.ownerID == Global.uid)) {
                items.add(getString(C0436R.string.repost_own_wall));
                acts.add("me");
            }
            items.add(getString(C0436R.string.repost_group));
            acts.add("group");
        }
        items.add(getString(C0436R.string.repost_message));
        acts.add("msg");
        setContentView(new View(this));
        if (getIntent().hasExtra("msg")) {
            startChatChooser();
        } else {
            new Builder(this).setTitle(C0436R.string.repost_dlg_title).setItems((CharSequence[]) items.toArray(new String[0]), new C04391(acts)).setOnCancelListener(new C04402()).show();
        }
    }

    private void startGroupChooser() {
        Bundle args = new Bundle();
        args.putBoolean("admin_only", true);
        args.putBoolean("select", true);
        Intent intent = new Intent(this, FragmentWrapperActivity.class);
        intent.putExtra("class", "GroupsFragment");
        intent.putExtra("args", args);
        startActivityForResult(intent, SEL_GROUP_RESULT);
    }

    private void startChatChooser() {
        startActivityForResult(new Intent(this, ForwardMessageActivity.class), SEL_CHAT_RESULT);
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (resCode != -1) {
            finish();
            return;
        }
        if (reqCode == SEL_GROUP_RESULT && resCode == -1) {
            this.groupName = data.getStringExtra("name");
            this.groupPhoto = data.getStringExtra("photo");
            repostWithComment(data.getIntExtra("gid", 0));
        }
        if (reqCode == SEL_CHAT_RESULT && resCode == -1) {
            UserProfile profile = (UserProfile) data.getParcelableExtra("profile");
            Bundle args = new Bundle();
            args.putInt("id", profile.uid);
            args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, profile.fullName);
            if (profile.uid < 2000000000) {
                args.putCharSequence("photo", profile.photo);
            }
            args.putParcelable("post", this.post);
            Navigate.to("ChatFragment", args, this);
            finish();
        }
    }

    private void repostWithComment(int gid) {
        EditText ed = new EditText(this);
        ed.setLines(4);
        ed.setGravity(51);
        AlertDialog dlg = new Builder(this).setTitle(C0436R.string.repost_comment_title).setView(ed).setPositiveButton(C0436R.string.ok, new C04413(gid, ed)).setNegativeButton(C0436R.string.cancel, new C04424()).setOnCancelListener(new C04435()).create();
        dlg.setOnShowListener(new C04446(ed));
        dlg.show();
    }

    private void doRepostWithComment(int gid, String comment) {
        new WallRepost("wall" + this.post.ownerID + "_" + this.post.postID, gid, comment).setCallback(new C13437(gid, comment)).wrapProgress(this).exec((Activity) this);
    }

    private void broadcastUpdate() {
        if (this.post.type == 0) {
            Intent intent = new Intent(Posts.ACTION_POST_UPDATED_BROADCAST);
            intent.putExtra("post_id", this.post.postID);
            intent.putExtra("owner_id", this.post.ownerID);
            intent.putExtra("comments", this.post.numComments);
            intent.putExtra("retweets", this.post.numRetweets);
            intent.putExtra("likes", this.post.numLikes);
            intent.putExtra("liked", this.post.flag(8));
            intent.putExtra("retweeted", this.post.flag(4));
            sendBroadcast(intent);
            NewsfeedCache.update(VKApplication.context, this.post.ownerID, this.post.postID, this.post.numLikes, this.post.numComments, this.post.numRetweets, this.post.flag(8), this.post.flag(4));
        }
    }
}
