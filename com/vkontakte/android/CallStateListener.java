package com.vkontakte.android;

import android.telephony.PhoneStateListener;

public class CallStateListener extends PhoneStateListener {
    boolean wasPlaying;

    public CallStateListener() {
        this.wasPlaying = false;
    }

    public void onCallStateChanged(int state, String incomingNumber) {
        Log.m525d("vk", "CALL STATE = " + state);
        if (AudioPlayerService.sharedInstance == null) {
            return;
        }
        if (state == 0) {
            MediaButtonReceiver.receiveEvents = true;
            AudioPlayerService.sharedInstance.registerRemoteControl();
            if (this.wasPlaying) {
                AudioPlayerService.sharedInstance.togglePlayPause();
                this.wasPlaying = false;
                return;
            }
            return;
        }
        AudioPlayerService.sharedInstance.unregisterRemoteControl();
        MediaButtonReceiver.receiveEvents = false;
        if (AudioPlayerService.sharedInstance.isPlaying()) {
            AudioPlayerService.sharedInstance.togglePlayPause();
            this.wasPlaying = true;
        }
    }
}
