package com.vkontakte.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.api.PhotoAlbum;
import com.vkontakte.android.mediapicker.GalleryPickerActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

public class PostPhotoActivity extends Activity {
    private static final int ALBUM_RESULT = 103;
    private static final int MESSAGE_RESULT = 102;
    private static final int PICKER_RESULT = 101;
    private int aid;
    private ArrayList<String> files;
    private UserProfile msgReceiver;
    private int option;

    public void onCreate(Bundle b) {
        super.onCreate(b);
        if (getIntent().hasExtra("option")) {
            this.option = getIntent().getIntExtra("option", 0);
            processOption();
            return;
        }
        startPicker();
    }

    private void processOption() {
        if (this.option == 0 || this.option == 3) {
            startPicker();
        } else if (this.option != 1 && this.option == 2) {
            Bundle args = new Bundle();
            args.putBoolean("select", true);
            args.putBoolean("select_album", true);
            Intent intent = new Intent(this, FragmentWrapperActivity.class);
            intent.putExtra("class", "PhotoAlbumsListFragment");
            intent.putExtra("args", args);
            startActivityForResult(intent, ALBUM_RESULT);
        }
    }

    private void startPicker() {
        Intent intent = new Intent(this, GalleryPickerActivity.class);
        ArrayList<String> al = new ArrayList();
        al.addAll(Arrays.asList(new String[]{getString(C0436R.string.share_photo_wall), getString(C0436R.string.share_photo_msg), getString(C0436R.string.share_photo_album)}));
        intent.putExtra("complete_options", al);
        al = new ArrayList();
        al.add(getString(C0436R.string.share_photo_profile));
        intent.putExtra("complete_single_options", al);
        intent.putExtra("selection_limit", 10);
        startActivityForResult(intent, PICKER_RESULT);
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        Intent intent;
        Bundle args;
        if (reqCode == PICKER_RESULT && resCode == -1) {
            this.files = data.getStringArrayListExtra("images");
            this.option = data.getIntExtra("chosen_option", 0);
            if (this.option == 0) {
                intent = new Intent(this, NewPostActivity.class);
                intent.putExtra("photos", this.files);
                startActivity(intent);
                finish();
            }
            if (this.option == 1) {
                startActivityForResult(new Intent(this, ForwardMessageActivity.class), MESSAGE_RESULT);
            }
            if (this.option == 2) {
                args = new Bundle();
                args.putBoolean("select", true);
                args.putBoolean("select_album", true);
                intent = new Intent(this, FragmentWrapperActivity.class);
                intent.putExtra("class", "PhotoAlbumsListFragment");
                intent.putExtra("args", args);
                startActivityForResult(intent, ALBUM_RESULT);
            }
            if (this.option == 3) {
                intent = new Intent(this, UploaderService.class);
                intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 6);
                intent.putExtra("file", (String) this.files.get(0));
                startService(intent);
                finish();
            }
        } else if (reqCode == MESSAGE_RESULT && resCode == -1) {
            this.msgReceiver = (UserProfile) data.getParcelableExtra("profile");
            args = new Bundle();
            args.putInt("id", this.msgReceiver.uid);
            args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, this.msgReceiver.fullName);
            if (this.msgReceiver.uid < 2000000000) {
                args.putCharSequence("photo", this.msgReceiver.photo);
            }
            args.putStringArrayList("photos", this.files);
            Navigate.to("ChatFragment", args, this);
            finish();
        } else if (reqCode == ALBUM_RESULT && resCode == -1) {
            this.aid = ((PhotoAlbum) data.getParcelableExtra("album")).id;
            Iterator it = this.files.iterator();
            while (it.hasNext()) {
                String file = (String) it.next();
                intent = new Intent(this, UploaderService.class);
                intent.putExtra("file", file);
                HashMap<String, String> params = new HashMap();
                params.put("aid", new StringBuilder(String.valueOf(this.aid)).toString());
                intent.putExtra("req_params", params);
                intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 1);
                startService(intent);
            }
            finish();
        } else {
            finish();
        }
    }

    public void onPause() {
        super.onPause();
        AppStateTracker.onActivityPaused();
    }

    public void onResume() {
        super.onResume();
        AppStateTracker.onActivityResumed(this);
    }
}
