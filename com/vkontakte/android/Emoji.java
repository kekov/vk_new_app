package com.vkontakte.android;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.ui.DialogEntryView;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Emoji {
    private static final int[] ROW_SIZES;
    private static int bigImgSize;
    private static WeakReference<Bitmap>[] bmps;
    private static int drawImgSize;
    private static ArrayList<WeakReference<EmojiDrawable>> drawables;
    private static int imgSize;
    private static boolean inited;
    private static boolean[] loading;
    private static Paint placeholderPaint;
    private static HashMap<Long, DrawableInfo> rects;

    /* renamed from: com.vkontakte.android.Emoji.1 */
    class C02431 implements Runnable {
        private final /* synthetic */ ArrayList val$toInvalidate;

        C02431(ArrayList arrayList) {
            this.val$toInvalidate = arrayList;
        }

        public void run() {
            Iterator it = this.val$toInvalidate.iterator();
            while (it.hasNext()) {
                EmojiDrawable ed = (EmojiDrawable) it.next();
                if (ed != null) {
                    ed.invalidateSelf();
                }
            }
            try {
                Emoji.invalidateAll(AppStateTracker.getCurrentActivity().getWindow().getDecorView());
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.Emoji.2 */
    class C02442 implements Runnable {
        private final /* synthetic */ int val$page;

        C02442(int i) {
            this.val$page = i;
        }

        public void run() {
            Emoji.loadPage(this.val$page);
            Emoji.loading[this.val$page] = false;
        }
    }

    private static class DrawableInfo {
        int page;
        Rect rect;

        public DrawableInfo(Rect rect, int p) {
            this.rect = rect;
            this.page = p;
        }
    }

    public static class EmojiDrawable extends Drawable {
        private static Paint paint;
        Bitmap bmp;
        int customSize;
        boolean fullSize;
        int page;
        Rect rect;

        static {
            paint = new Paint();
            paint.setFilterBitmap(true);
        }

        public EmojiDrawable(DrawableInfo info) {
            this.fullSize = false;
            this.customSize = 0;
            this.rect = info.rect;
            this.page = info.page;
        }

        public void draw(Canvas canvas) {
            if (Emoji.bmps[this.page].get() == null) {
                canvas.drawRect(getBounds(), Emoji.placeholderPaint);
                return;
            }
            if (this.bmp == null) {
                this.bmp = (Bitmap) Emoji.bmps[this.page].get();
            }
            Rect b = copyBounds();
            int cX = b.centerX();
            int cY = b.centerY();
            if (this.customSize > 0) {
                b.left = cX - (Math.min(this.fullSize ? Emoji.bigImgSize : Emoji.drawImgSize, this.customSize) / 2);
                b.right = (Math.min(this.fullSize ? Emoji.bigImgSize : Emoji.drawImgSize, this.customSize) / 2) + cX;
                b.top = cY - (Math.min(this.fullSize ? Emoji.bigImgSize : Emoji.drawImgSize, this.customSize) / 2);
                b.bottom = (Math.min(this.fullSize ? Emoji.bigImgSize : Emoji.drawImgSize, this.customSize) / 2) + cY;
            } else {
                b.left = cX - ((this.fullSize ? Emoji.bigImgSize : Emoji.drawImgSize) / 2);
                b.right = ((this.fullSize ? Emoji.bigImgSize : Emoji.drawImgSize) / 2) + cX;
                b.top = cY - ((this.fullSize ? Emoji.bigImgSize : Emoji.drawImgSize) / 2);
                b.bottom = ((this.fullSize ? Emoji.bigImgSize : Emoji.drawImgSize) / 2) + cY;
            }
            canvas.drawBitmap(this.bmp, this.rect, b, paint);
        }

        public int getOpacity() {
            return 0;
        }

        public void setAlpha(int alpha) {
        }

        public void setColorFilter(ColorFilter cf) {
        }

        public void setSize(int s) {
            this.customSize = s;
        }
    }

    static {
        int i;
        ROW_SIZES = new int[]{27, 29, 33, 34, 34};
        rects = new HashMap();
        inited = true;
        loading = new boolean[5];
        drawables = new ArrayList();
        bmps = new WeakReference[5];
        for (i = 0; i < bmps.length; i++) {
            bmps[i] = new WeakReference(null);
        }
        imgSize = Math.min(Global.scale(BitmapDescriptorFactory.HUE_ORANGE), VKApplication.context.getResources().getDisplayMetrics().density < 1.5f ? 28 : 56);
        drawImgSize = Global.scale(20.0f);
        bigImgSize = Global.scale(BitmapDescriptorFactory.HUE_ORANGE);
        if (Math.abs(imgSize - bigImgSize) < 5) {
            bigImgSize = imgSize;
        }
        for (int j = 1; j < EmojiView.data.length; j++) {
            int rsize = ROW_SIZES[j - 1];
            for (i = 0; i < EmojiView.data[j].length; i++) {
                rects.put(Long.valueOf(EmojiView.data[j][i]), new DrawableInfo(new Rect((i % rsize) * imgSize, (i / rsize) * imgSize, ((i % rsize) + 1) * imgSize, ((i / rsize) + 1) * imgSize), j - 1));
            }
        }
        placeholderPaint = new Paint();
        placeholderPaint.setColor(1426063360);
    }

    private static Bitmap loadPage(int page) {
        try {
            int rsize = ROW_SIZES[page];
            Options opts = new Options();
            opts.inPreferredConfig = Config.ARGB_8888;
            opts.inDither = false;
            if (VKApplication.context.getResources().getDisplayMetrics().density < 1.5f) {
                opts.inSampleSize = 2;
            }
            Log.m528i("vk", "Load emoji page " + page);
            InputStream is = VKApplication.context.getAssets().open("emoji" + page + "c.jpg");
            Bitmap color = BitmapFactory.decodeStream(is, null, opts);
            is.close();
            is = VKApplication.context.getAssets().open("emoji" + page + "a.jpg");
            Bitmap alpha = BitmapFactory.decodeStream(is, null, opts);
            is.close();
            int iw = color.getWidth();
            int ih = color.getHeight();
            Log.m528i("vk", "EMOJI INIT: c: " + iw + "x" + ih + ", a: " + alpha.getWidth() + "x" + alpha.getHeight());
            int[] cpx = new int[(iw * ih)];
            color.getPixels(cpx, 0, iw, 0, 0, iw, ih);
            color.recycle();
            int[] apx = new int[(iw * ih)];
            alpha.getPixels(apx, 0, iw, 0, 0, iw, ih);
            alpha.recycle();
            for (int i = 0; i < cpx.length; i++) {
                cpx[i] = (cpx[i] & 16777215) | (apx[i] << 24);
            }
            System.gc();
            Bitmap bmp = Bitmap.createBitmap(iw, ih, opts.inPreferredConfig);
            bmp.setPixels(cpx, 0, iw, 0, 0, iw, ih);
            ih = (int) Math.round((((double) imgSize) * ((double) rsize)) * (((double) ih) / ((double) iw)));
            iw = imgSize * rsize;
            Log.m528i("vk", "EMOJI INIT: resizing to " + iw + "x" + ih);
            if (iw < bmp.getWidth() && ih < bmp.getWidth()) {
                bmp = Bitmap.createScaledBitmap(bmp, iw, ih, true);
            }
            Log.m525d("vk", "Emoji init ok");
            ArrayList<EmojiDrawable> toInvalidate = new ArrayList();
            try {
                Iterator<WeakReference<EmojiDrawable>> it = drawables.iterator();
                while (it.hasNext()) {
                    WeakReference<EmojiDrawable> wr = (WeakReference) it.next();
                    if (wr.get() == null) {
                        it.remove();
                    } else {
                        EmojiDrawable ed = (EmojiDrawable) wr.get();
                        toInvalidate.add(ed);
                        if (ed.page == page) {
                            ed.bmp = bmp;
                        }
                    }
                }
            } catch (Exception e) {
            }
            bmps[page] = new WeakReference(bmp);
            if (AppStateTracker.getCurrentActivity() == null) {
                return bmp;
            }
            AppStateTracker.getCurrentActivity().runOnUiThread(new C02431(toInvalidate));
            return bmp;
        } catch (Throwable x) {
            Log.m527e("vk", "Error loading emoji", x);
            return null;
        }
    }

    private static void loadPageAsync(int page) {
        if (!loading[page]) {
            loading[page] = true;
            new Thread(new C02442(page)).start();
        }
    }

    private static void invalidateAll(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup g = (ViewGroup) view;
            for (int i = 0; i < g.getChildCount(); i++) {
                invalidateAll(g.getChildAt(i));
            }
        } else if (view instanceof DialogEntryView) {
            view.invalidate();
        }
    }

    public static Drawable getEmojiDrawable(long code) {
        DrawableInfo info = (DrawableInfo) rects.get(Long.valueOf(code));
        if (info == null) {
            Log.m526e("vk", "No emoji drawable for code " + String.format("%016X", new Object[]{Long.valueOf(code)}));
            return null;
        }
        Drawable ed = new EmojiDrawable(info);
        ed.setBounds(0, 0, drawImgSize, drawImgSize);
        if (bmps[info.page].get() == null) {
            loadPageAsync(info.page);
        }
        drawables.add(new WeakReference(ed));
        try {
            Iterator<WeakReference<EmojiDrawable>> it = drawables.iterator();
            while (it.hasNext()) {
                if (((WeakReference) it.next()).get() == null) {
                    it.remove();
                }
            }
            return ed;
        } catch (Throwable th) {
            return ed;
        }
    }

    public static Drawable getEmojiBigDrawable(long code) {
        EmojiDrawable ed = (EmojiDrawable) getEmojiDrawable(code);
        if (ed == null) {
            return null;
        }
        ed.setBounds(0, 0, bigImgSize, bigImgSize);
        ed.fullSize = true;
        return ed;
    }
}
