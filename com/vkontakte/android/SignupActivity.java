package com.vkontakte.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import com.facebook.WebDialog;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Auth.AuthResultReceiver;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.AuthConfirm;
import com.vkontakte.android.api.AuthSignup;
import com.vkontakte.android.api.AuthSignup.Callback;
import com.vkontakte.android.fragments.SignupCodeFragment;
import com.vkontakte.android.fragments.SignupCodeFragment.OnResendListener;
import com.vkontakte.android.fragments.SignupPasswordFragment;
import com.vkontakte.android.fragments.SignupPhoneFragment;
import com.vkontakte.android.fragments.SignupProfileFragment;
import com.vkontakte.android.ui.ActionBarHacks;
import com.vkontakte.android.ui.ActionBarProgressDrawable;
import java.util.HashMap;
import org.acra.ACRAConstants;

public class SignupActivity extends VKFragmentActivity implements AuthResultReceiver {
    private static final int RESTORE_RESULT = 203;
    private static final int VALIDATION_RESULT = 202;
    private static final int[] titles;
    private String code;
    private SignupCodeFragment codeFragment;
    private int curStep;
    private String firstName;
    private int gender;
    private String lastName;
    private String number;
    private String password;
    private SignupPasswordFragment passwordFragment;
    private SignupPhoneFragment phoneFragment;
    private String photo;
    private SignupProfileFragment profileFragment;
    private ActionBarProgressDrawable progress;
    private ProgressDialog progressDialog;
    private String sid;
    private FrameLayout wrap;

    /* renamed from: com.vkontakte.android.SignupActivity.11 */
    class AnonymousClass11 implements Runnable {
        private final /* synthetic */ int val$result;

        AnonymousClass11(int i) {
            this.val$result = i;
        }

        public void run() {
            SignupActivity.this.progressDialog.dismiss();
            if (this.val$result == Auth.REAUTH_CANCELED) {
                return;
            }
            if (this.val$result == Auth.REAUTH_OPEN_BROWSER) {
                SignupActivity.this.openBrowser(Auth.lastError);
                return;
            }
            String msg = SignupActivity.this.getResources().getString(this.val$result == Auth.REAUTH_ERROR_INCORRECT_PASSWORD ? C0436R.string.auth_error : C0436R.string.auth_error_network);
            if (this.val$result == Auth.REAUTH_ERROR_INCORRECT_PASSWORD) {
                msg = new StringBuilder(String.valueOf(msg)).append(" (").append(SignupActivity.this.getResources().getString(C0436R.string.error)).append(": ").append(Auth.lastError).append(")").toString();
            }
            new Builder(SignupActivity.this).setMessage(msg).setTitle(C0436R.string.auth_error_title).setIcon(VERSION.SDK_INT >= 11 ? 0 : ACRAConstants.DEFAULT_DIALOG_ICON).setPositiveButton(C0436R.string.ok, null).show();
        }
    }

    /* renamed from: com.vkontakte.android.SignupActivity.12 */
    class AnonymousClass12 implements Runnable {
        private final /* synthetic */ Intent val$intent;

        AnonymousClass12(Intent intent) {
            this.val$intent = intent;
        }

        public void run() {
            Auth.setData(this.val$intent.getStringExtra(WebDialog.DIALOG_PARAM_ACCESS_TOKEN), this.val$intent.getStringExtra("secret"), this.val$intent.getIntExtra("user_id", 0), true);
            SignupActivity.this.authDone(Auth.REAUTH_SUCCESS, null);
        }
    }

    /* renamed from: com.vkontakte.android.SignupActivity.2 */
    class C04932 implements OnClickListener {
        C04932() {
        }

        public void onClick(View v) {
            int f = SignupActivity.this.profileFragment.isFilled();
            if (f == 1) {
                SignupActivity.this.firstName = SignupActivity.this.profileFragment.getFirstName();
                SignupActivity.this.lastName = SignupActivity.this.profileFragment.getLastName();
                SignupActivity.this.gender = SignupActivity.this.profileFragment.getGender();
                SignupActivity.this.photo = SignupActivity.this.profileFragment.getPhoto();
                SignupActivity.this.setStep(1);
            } else if (f == 0) {
                SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.signup_invalid_name));
            }
        }
    }

    /* renamed from: com.vkontakte.android.SignupActivity.3 */
    class C04943 implements Runnable {
        C04943() {
        }

        public void run() {
            SignupActivity.this.progress.setStepAnimated(SignupActivity.this.curStep);
        }
    }

    /* renamed from: com.vkontakte.android.SignupActivity.4 */
    class C04964 implements OnClickListener {

        /* renamed from: com.vkontakte.android.SignupActivity.4.1 */
        class C04951 implements Runnable {
            C04951() {
            }

            public void run() {
                SignupActivity.this.setStep(2);
            }
        }

        C04964() {
        }

        public void onClick(View v) {
            if (SignupActivity.this.phoneFragment.isFilled()) {
                SignupActivity.this.number = SignupActivity.this.phoneFragment.getNumber();
                SignupActivity.this.requestCode(SignupActivity.this.sid, false, new C04951());
            }
        }
    }

    /* renamed from: com.vkontakte.android.SignupActivity.6 */
    class C04976 implements OnClickListener {
        C04976() {
        }

        public void onClick(View v) {
            SignupActivity.this.verifyCode(SignupActivity.this.codeFragment.getCode());
        }
    }

    /* renamed from: com.vkontakte.android.SignupActivity.7 */
    class C04987 implements OnClickListener {
        C04987() {
        }

        public void onClick(View v) {
            SignupActivity.this.password = SignupActivity.this.passwordFragment.getPassword();
            if (SignupActivity.this.password.length() < 6) {
                SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.signup_pass_too_short));
            } else {
                SignupActivity.this.completeSignup();
            }
        }
    }

    /* renamed from: com.vkontakte.android.SignupActivity.1 */
    class C13551 extends ActionBarProgressDrawable {
        C13551() {
        }

        public void invalidateSelf() {
            super.invalidateSelf();
            View abv = ActionBarHacks.getActionBar(SignupActivity.this);
            if (abv != null) {
                abv.postInvalidate();
            }
        }
    }

    /* renamed from: com.vkontakte.android.SignupActivity.5 */
    class C13565 implements OnResendListener {
        C13565() {
        }

        public void resendCode(boolean voice, Runnable action) {
            SignupActivity.this.requestCode(SignupActivity.this.sid, voice, action);
        }
    }

    /* renamed from: com.vkontakte.android.SignupActivity.8 */
    class C13578 implements Callback {
        private final /* synthetic */ Runnable val$runAfter;

        /* renamed from: com.vkontakte.android.SignupActivity.8.1 */
        class C04991 implements DialogInterface.OnClickListener {
            C04991() {
            }

            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(SignupActivity.this, RestoreActivity.class);
                intent.putExtra("phone", SignupActivity.this.number);
                SignupActivity.this.startActivityForResult(intent, SignupActivity.RESTORE_RESULT);
            }
        }

        C13578(Runnable runnable) {
            this.val$runAfter = runnable;
        }

        public void success(String sid) {
            SignupActivity.this.sid = sid;
            if (this.val$runAfter != null) {
                this.val$runAfter.run();
            }
        }

        public void fail(int ecode, String emsg) {
            if (ecode == -1) {
                SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.err_text));
            } else if (ecode == 1004) {
                new Builder(SignupActivity.this).setTitle(C0436R.string.error).setMessage(C0436R.string.signup_phone_already_used).setPositiveButton(C0436R.string.signup_btn_restore, new C04991()).setNegativeButton(C0436R.string.ok, null).show();
            } else if (ecode == 9 || ecode == 1112) {
                SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.signup_flood));
            } else if (ecode == LocationStatusCodes.GEOFENCE_NOT_AVAILABLE) {
                SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.signup_invalid_phone));
            } else if (ecode != 100) {
                SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.unknown_error, new Object[]{emsg, Integer.valueOf(ecode)}));
            } else if (emsg.contains("first_name") || emsg.contains("last_name")) {
                SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.signup_invalid_name));
                SignupActivity.this.setStep(0);
            } else if (emsg.contains("phone")) {
                SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.signup_invalid_phone_format));
            } else {
                SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.unknown_error, new Object[]{emsg, Integer.valueOf(ecode)}));
            }
        }
    }

    /* renamed from: com.vkontakte.android.SignupActivity.9 */
    class C13589 implements AuthConfirm.Callback {
        private final /* synthetic */ String val$code;

        C13589(String str) {
            this.val$code = str;
        }

        public void success(int uid) {
        }

        public void fail(int ecode, String emsg) {
            if (ecode == 1110) {
                SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.signup_code_incorrect));
            } else if (ecode == 1111) {
                SignupActivity.this.setStep(3);
                SignupActivity.this.code = this.val$code;
            } else if (ecode == -1) {
                SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.err_text));
            } else {
                SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.unknown_error, new Object[]{emsg, Integer.valueOf(ecode)}));
            }
        }
    }

    public SignupActivity() {
        this.curStep = 0;
    }

    static {
        titles = new int[]{C0436R.string.registration, C0436R.string.signup_phone_title, C0436R.string.signup_code_title, C0436R.string.signup_pass_title};
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage(getString(C0436R.string.loading));
        this.progress = new C13551();
        getSupportActionBar().setBackgroundDrawable(this.progress);
        this.progress.setStepCount(4);
        this.wrap = new FrameLayout(this);
        this.wrap.setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        this.wrap.setId(C0436R.id.fragment_wrapper);
        this.profileFragment = new SignupProfileFragment();
        this.profileFragment.setOnNextClickListener(new C04932());
        setContentView(this.wrap);
        getSupportFragmentManager().beginTransaction().add((int) C0436R.id.fragment_wrapper, this.profileFragment).commit();
    }

    private void setStep(int step) {
        if (step > this.curStep) {
            boolean animateForward = true;
        }
        this.curStep = step;
        this.wrap.postDelayed(new C04943(), 100);
        setTitle(titles[step]);
        if (step == 0) {
            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, this.profileFragment).commit();
        }
        if (step == 1) {
            if (this.phoneFragment == null) {
                this.phoneFragment = new SignupPhoneFragment();
                this.phoneFragment.setOnNextClickListener(new C04964());
            }
            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, this.phoneFragment).commit();
        }
        if (step == 2) {
            if (this.codeFragment == null) {
                this.codeFragment = new SignupCodeFragment();
                this.codeFragment.setOnResendListener(new C13565());
                this.codeFragment.setOnNextClickListener(new C04976());
            }
            this.codeFragment.setNumber(this.phoneFragment.getNumber());
            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, this.codeFragment).commit();
        }
        if (step == 3) {
            if (this.passwordFragment == null) {
                this.passwordFragment = new SignupPasswordFragment();
                this.passwordFragment.setOnNextClickListener(new C04987());
            }
            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, this.passwordFragment).commitAllowingStateLoss();
        }
    }

    private void requestCode(String sid, boolean voice, Runnable runAfter) {
        new AuthSignup(this.firstName, this.lastName, this.gender, this.number, sid, voice).setCallback(new C13578(runAfter)).wrapProgress(this).exec((Activity) this);
    }

    private void verifyCode(String code) {
        if (code.length() != 0) {
            new AuthConfirm(this.number, code, null).setCallback(new C13589(code)).wrapProgress(this).exec((Activity) this);
        }
    }

    private void completeSignup() {
        new AuthConfirm(this.number, this.code, this.password).setCallback(new AuthConfirm.Callback() {
            public void success(int uid) {
                SignupActivity.this.progressDialog.show();
                Auth.authorizeAsync(SignupActivity.this.number, SignupActivity.this.password, SignupActivity.this);
            }

            public void fail(int ecode, String emsg) {
                if (ecode == 1110) {
                    SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.signup_code_incorrect));
                } else if (ecode == 1111) {
                } else {
                    if (ecode == -1) {
                        SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.err_text));
                        return;
                    }
                    SignupActivity.this.showError(SignupActivity.this.getString(C0436R.string.unknown_error, new Object[]{emsg, Integer.valueOf(ecode)}));
                }
            }
        }).wrapProgress(this).exec((Activity) this);
    }

    private void showError(String err) {
        new Builder(this).setTitle(C0436R.string.error).setMessage(err).setPositiveButton(C0436R.string.ok, null).show();
    }

    public void onBackPressed() {
        if (this.curStep == 0) {
            super.onBackPressed();
        } else {
            setStep(this.curStep - 1);
        }
    }

    public void authDone(int result, HashMap<String, String> hashMap) {
        if (result == Auth.REAUTH_SUCCESS) {
            setResult(-1);
            finish();
            if (this.photo != null) {
                Intent intent = new Intent(this, UploaderService.class);
                intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 6);
                intent.putExtra("file", this.photo);
                startService(intent);
                return;
            }
            return;
        }
        runOnUiThread(new AnonymousClass11(result));
    }

    private void openBrowser(String url) {
        Intent intent = new Intent(this, ValidationActivity.class);
        intent.putExtra(PlusShare.KEY_CALL_TO_ACTION_URL, url);
        intent.putExtra("return_result", true);
        startActivityForResult(intent, VALIDATION_RESULT);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == VALIDATION_RESULT && resultCode == -1) {
            this.progressDialog.show();
            new Thread(new AnonymousClass12(intent)).start();
        }
        if (requestCode == RESTORE_RESULT && resultCode == -1) {
            authDone(Auth.REAUTH_SUCCESS, null);
        }
    }
}
