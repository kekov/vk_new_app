package com.vkontakte.android;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListAdapter;

public class EditableUserListView extends UserListView {
    private OnUserRemovedListener listener;
    private OnClickListener removeClickListener;

    /* renamed from: com.vkontakte.android.EditableUserListView.1 */
    class C02421 implements OnClickListener {
        C02421() {
        }

        public void onClick(View v) {
            if (EditableUserListView.this.listener != null) {
                EditableUserListView.this.listener.onUserRemoved((UserProfile) v.getTag());
            }
        }
    }

    public interface OnUserRemovedListener {
        void onUserRemoved(UserProfile userProfile);
    }

    protected class EditableUserAdapter extends UserListAdapter {
        protected EditableUserAdapter() {
            super();
        }

        public View getView(int pos, View view, ViewGroup group) {
            if (view == null) {
                view = EditableUserListView.inflate(EditableUserListView.this.getContext(), C0436R.layout.news_banlist_item, null);
                view.findViewById(C0436R.id.flist_item_btn).setOnClickListener(EditableUserListView.this.removeClickListener);
            }
            view.findViewById(C0436R.id.flist_item_btn).setTag(EditableUserListView.this.users.get(pos));
            return super.getView(pos, view, group);
        }
    }

    public EditableUserListView(Context context, int _type, Bundle args) {
        super(context, _type, args);
        this.removeClickListener = new C02421();
    }

    protected ListAdapter getAdapter() {
        return new EditableUserAdapter();
    }

    public void setOnUserRemovedListener(OnUserRemovedListener l) {
        this.listener = l;
    }

    public void removeUser(UserProfile u) {
        this.users.remove(u);
        updateList();
    }
}
