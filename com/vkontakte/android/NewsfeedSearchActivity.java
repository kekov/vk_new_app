package com.vkontakte.android;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import java.lang.reflect.Field;

public class NewsfeedSearchActivity extends SherlockActivity {
    private SearchView searchView;

    /* renamed from: com.vkontakte.android.NewsfeedSearchActivity.1 */
    class C03631 implements Runnable {
        C03631() {
        }

        public void run() {
            ((InputMethodManager) NewsfeedSearchActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(NewsfeedSearchActivity.this.getCurrentFocus().getWindowToken(), 0);
            if (NewsfeedSearchActivity.this.getCurrentFocus() != null) {
                NewsfeedSearchActivity.this.getCurrentFocus().clearFocus();
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewsfeedSearchActivity.2 */
    class C13272 implements OnQueryTextListener {
        private final /* synthetic */ NewsView val$view;

        C13272(NewsView newsView) {
            this.val$view = newsView;
        }

        public boolean onQueryTextSubmit(String query) {
            this.val$view.setSearchQuery(query);
            this.val$view.loadData(true);
            ((InputMethodManager) NewsfeedSearchActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(NewsfeedSearchActivity.this.getCurrentFocus().getWindowToken(), 0);
            this.val$view.requestFocus();
            return true;
        }

        public boolean onQueryTextChange(String newText) {
            return false;
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String q = getIntent().getStringExtra("q");
        NewsView view = new NewsView((Context) this, true);
        view.initWithSearch();
        if (q != null && q.length() > 0) {
            view.setSearchQuery(q);
            view.loadData(true);
            view.postDelayed(new C03631(), 200);
        }
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        setContentView((View) view);
        this.searchView = new SearchView(getSupportActionBar().getThemedContext());
        this.searchView.setQueryHint(getResources().getString(C0436R.string.search));
        try {
            Field searchField = SearchView.class.getDeclaredField("mSearchButton");
            searchField.setAccessible(true);
            ((ImageView) searchField.get(this.searchView)).setImageResource(C0436R.drawable.ic_ab_search);
            searchField = SearchView.class.getDeclaredField("mSearchPlate");
            searchField.setAccessible(true);
            ((TextView) ((LinearLayout) searchField.get(this.searchView)).getChildAt(0)).setHintTextColor(-2130706433);
        } catch (Exception e) {
        }
        this.searchView.setOnQueryTextListener(new C13272(view));
        this.searchView.setIconified(false);
        this.searchView.setQuery(q, false);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem search = menu.add((int) C0436R.string.search);
        search.setShowAsAction(2);
        search.setActionView(this.searchView);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            onBackPressed();
        }
        return true;
    }

    public void onPause() {
        super.onPause();
        AppStateTracker.onActivityPaused();
    }

    public void onResume() {
        super.onResume();
        AppStateTracker.onActivityResumed(this);
    }
}
