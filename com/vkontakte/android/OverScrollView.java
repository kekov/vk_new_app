package com.vkontakte.android;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.util.Timer;
import java.util.TimerTask;

public class OverScrollView extends ScrollView {
    public static final int ID_BOTTOM_PADDING_VIEW = 6;
    public static final int ID_TOP_PADDING_VIEW = 5;
    private static final int NOCHANGE_DELAY = 300;
    boolean addingContentAtTop;
    AlphaAnimation anim;
    int collapsedBottomH;
    int collapsedTopH;
    boolean draggingBottom;
    boolean draggingTop;
    boolean isAligned;
    public OnScrollEndListener onScrollEndListener;
    private int prevHeight;
    private int prevViewH;
    private Timer scrollEndTimer;
    float startY;
    float vH;

    /* renamed from: com.vkontakte.android.OverScrollView.1 */
    class C03771 implements Runnable {
        C03771() {
        }

        public void run() {
            OverScrollView.this.scrollTo(0, 9999999);
        }
    }

    /* renamed from: com.vkontakte.android.OverScrollView.2 */
    class C03782 implements Runnable {
        C03782() {
        }

        public void run() {
            if (OverScrollView.this.onScrollEndListener != null) {
                OverScrollView.this.onScrollEndListener.onScrollEnd(OverScrollView.this.getScrollY(), OverScrollView.this.getHeight());
            }
        }
    }

    public interface OnScrollEndListener {
        void onScrollEnd(int i, int i2);
    }

    private class ResizingInterpolator extends DecelerateInterpolator {
        public int ch;
        public int viewID;

        public ResizingInterpolator() {
            super(ImageViewHolder.PaddingSize);
            this.ch = 0;
        }

        public float getInterpolation(float input) {
            float f = super.getInterpolation(input);
            OverScrollView.this.findViewById(this.viewID).setLayoutParams(new LayoutParams(-1, (int) (((OverScrollView.this.vH - ((float) this.ch)) * (1.0f - f)) + ((float) this.ch))));
            return f;
        }
    }

    private class ScrollEndTimerTask extends TimerTask {
        private ScrollEndTimerTask() {
        }

        public void run() {
            OverScrollView.this.postScrollEnd();
        }
    }

    public OverScrollView(Context context) {
        super(context);
        this.draggingBottom = false;
        this.draggingTop = false;
        this.startY = 0.0f;
        this.vH = 0.0f;
        this.collapsedTopH = 0;
        this.collapsedBottomH = 0;
        this.anim = new AlphaAnimation(0.999f, 1.0f);
        this.scrollEndTimer = null;
        this.onScrollEndListener = null;
        this.prevHeight = 0;
        this.prevViewH = 0;
        this.addingContentAtTop = false;
        this.isAligned = false;
    }

    public OverScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.draggingBottom = false;
        this.draggingTop = false;
        this.startY = 0.0f;
        this.vH = 0.0f;
        this.collapsedTopH = 0;
        this.collapsedBottomH = 0;
        this.anim = new AlphaAnimation(0.999f, 1.0f);
        this.scrollEndTimer = null;
        this.onScrollEndListener = null;
        this.prevHeight = 0;
        this.prevViewH = 0;
        this.addingContentAtTop = false;
        this.isAligned = false;
    }

    public OverScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.draggingBottom = false;
        this.draggingTop = false;
        this.startY = 0.0f;
        this.vH = 0.0f;
        this.collapsedTopH = 0;
        this.collapsedBottomH = 0;
        this.anim = new AlphaAnimation(0.999f, 1.0f);
        this.scrollEndTimer = null;
        this.onScrollEndListener = null;
        this.prevHeight = 0;
        this.prevViewH = 0;
        this.addingContentAtTop = false;
        this.isAligned = false;
    }

    public void init() {
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 2) {
            float h;
            if (getScrollY() == 0) {
                if (!this.draggingTop) {
                    startDraggingTop(event);
                }
                h = event.getY() - this.startY;
                if (h < 0.0f) {
                    endDraggingTop(false);
                    return super.onTouchEvent(event);
                }
                findViewById(ID_TOP_PADDING_VIEW).setLayoutParams(new LayoutParams(-1, ((int) h) + this.collapsedTopH));
                onDragTop((int) h);
                try {
                    getClass().getMethod("awakenScrollBars", new Class[0]).invoke(this, new Object[0]);
                    return true;
                } catch (Exception e) {
                    return true;
                }
            } else if (getScrollY() == getChildAt(0).getHeight() - getHeight() || (getScrollY() >= (getChildAt(0).getHeight() - getHeight()) - 5 && this.draggingBottom)) {
                if (!this.draggingBottom) {
                    startDraggingBottom(event);
                }
                h = this.startY - event.getY();
                if (h < 0.0f) {
                    endDraggingBottom(false);
                    return super.onTouchEvent(event);
                }
                findViewById(ID_BOTTOM_PADDING_VIEW).setLayoutParams(new LayoutParams(-1, (int) h));
                post(new C03771());
                return true;
            } else if (!this.draggingBottom && this.draggingTop) {
                endDraggingTop(false);
            }
        }
        if (event.getAction() == 1) {
            if (this.draggingTop) {
                endDraggingTop(true);
            }
            if (this.draggingBottom) {
                endDraggingBottom(true);
            }
        }
        return super.onTouchEvent(event);
    }

    protected void startDraggingTop(MotionEvent ev) {
        this.draggingTop = true;
        findViewById(ID_TOP_PADDING_VIEW).clearAnimation();
        this.startY = ev.getY();
    }

    protected void endDraggingTop(boolean animate) {
        if (this.draggingTop) {
            this.vH = (float) findViewById(ID_TOP_PADDING_VIEW).getHeight();
            if (!animate || this.vH <= 0.0f) {
                findViewById(ID_TOP_PADDING_VIEW).setLayoutParams(new LayoutParams(-1, this.collapsedTopH));
            } else {
                this.anim.reset();
                this.anim.setDuration((long) Math.max(NOCHANGE_DELAY, (int) this.vH));
                this.anim.setRepeatCount(0);
                ResizingInterpolator ri = new ResizingInterpolator();
                ri.viewID = ID_TOP_PADDING_VIEW;
                ri.ch = this.collapsedTopH;
                this.anim.setInterpolator(ri);
                findViewById(ID_TOP_PADDING_VIEW).startAnimation(this.anim);
            }
            this.draggingTop = false;
        }
    }

    private void startDraggingBottom(MotionEvent ev) {
        this.draggingBottom = true;
        findViewById(ID_BOTTOM_PADDING_VIEW).clearAnimation();
        this.startY = ev.getY();
    }

    private void endDraggingBottom(boolean animate) {
        if (this.draggingBottom) {
            this.vH = (float) findViewById(ID_BOTTOM_PADDING_VIEW).getHeight();
            if (!animate || this.vH <= 0.0f) {
                findViewById(ID_BOTTOM_PADDING_VIEW).setLayoutParams(new LayoutParams(-1, 0));
            } else {
                this.anim.reset();
                this.anim.setDuration((long) ((int) this.vH));
                this.anim.setRepeatCount(0);
                ResizingInterpolator ri = new ResizingInterpolator();
                ri.viewID = ID_BOTTOM_PADDING_VIEW;
                this.anim.setInterpolator(ri);
                findViewById(ID_BOTTOM_PADDING_VIEW).startAnimation(this.anim);
            }
            this.draggingBottom = false;
        }
    }

    public void onDragTop(int h) {
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (this.isAligned) {
            View v;
            if (changed && this.prevHeight > 0) {
                scrollBy(0, -(getHeight() - this.prevHeight));
            }
            super.onLayout(changed, l, t, r, b);
            if (!changed && this.addingContentAtTop) {
                v = getChildAt(0);
                if (v != null) {
                    scrollTo(0, (v.getHeight() - this.prevViewH) + getScrollY());
                }
                this.addingContentAtTop = false;
            }
            this.prevHeight = getHeight();
            v = getChildAt(0);
            if (v != null) {
                this.prevViewH = v.getHeight();
                return;
            }
            return;
        }
        super.onLayout(changed, l, t, r, b);
    }

    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (this.scrollEndTimer != null) {
            this.scrollEndTimer.cancel();
        }
        this.scrollEndTimer = new Timer();
        this.scrollEndTimer.schedule(new ScrollEndTimerTask(), 300);
    }

    public void postScrollEnd() {
        post(new C03782());
    }
}
