package com.vkontakte.android;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class PendingDocumentAttachment extends DocumentAttachment {
    public static final Creator<PendingDocumentAttachment> CREATOR;

    /* renamed from: com.vkontakte.android.PendingDocumentAttachment.1 */
    class C03791 implements Creator<PendingDocumentAttachment> {
        C03791() {
        }

        public PendingDocumentAttachment createFromParcel(Parcel in) {
            return new PendingDocumentAttachment(in);
        }

        public PendingDocumentAttachment[] newArray(int size) {
            return new PendingDocumentAttachment[size];
        }
    }

    public PendingDocumentAttachment(String _title, String _url, int _size, String _thumb, int _oid, int _did) {
        super(_title, _url, _size, _thumb, _oid, _did);
    }

    public PendingDocumentAttachment(Parcel parcel) {
        super(parcel);
    }

    static {
        CREATOR = new C03791();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel p, int arg1) {
        super.writeToParcel(p, arg1);
    }
}
