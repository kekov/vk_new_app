package com.vkontakte.android;

import android.app.IntentService;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.widget.RemoteViews;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.api.StoreSetActive;
import com.vkontakte.android.data.Stickers;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.acra.ACRAConstants;

public class StickerDownloaderService extends IntentService {
    private static final boolean DEBUG = false;
    private static final int ID_NOTIFICATION_DONE = 2305;
    private static final int ID_NOTIFICATION_FAILED = 2605;
    private static final int ID_NOTIFICATION_PROGRESS = 2005;
    public static StickerDownloaderService currentInstance;
    private String destPath;
    private Builder notifyBuilder;
    private int packId;
    private float percent;
    private Notification progress;
    private ArrayList<Integer> queue;
    private boolean silent;
    private String title;
    private RemoteViews uploadView;
    private String url;

    /* renamed from: com.vkontakte.android.StickerDownloaderService.1 */
    class C05021 implements FilenameFilter {
        Pattern ptn;

        C05021() {
            this.ptn = Pattern.compile("\\d+_256b\\.png");
        }

        public boolean accept(File dir, String filename) {
            return this.ptn.matcher(filename).find();
        }
    }

    public StickerDownloaderService() {
        super("StickerDownloader");
        this.queue = new ArrayList();
        this.silent = DEBUG;
    }

    public void onCreate() {
        super.onCreate();
        currentInstance = this;
    }

    public void onDestroy() {
        super.onDestroy();
        currentInstance = null;
    }

    public int getCurrentPackId() {
        return this.packId;
    }

    public float getCurrentProgress() {
        return this.percent;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.hasExtra("id")) {
            this.queue.add(Integer.valueOf(intent.getIntExtra("id", 0)));
            Stickers.broadcastUpdate();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public void onHandleIntent(Intent intent) {
        this.url = intent.getStringExtra(PlusShare.KEY_CALL_TO_ACTION_URL);
        if (intent.hasExtra("id")) {
            this.packId = intent.getIntExtra("id", 0);
            this.title = intent.getStringExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            this.silent = intent.getBooleanExtra("silent", DEBUG);
            this.destPath = null;
            this.queue.remove(Integer.valueOf(this.packId));
        } else {
            this.destPath = intent.getStringExtra("dest_path");
            this.packId = 0;
            Log.m525d("vk", "Will download " + this.url + " -> " + this.destPath);
        }
        download();
    }

    public boolean isInQueue(int id) {
        return this.queue.contains(Integer.valueOf(id));
    }

    private void updateProgress(int loaded, int total) {
        if (this.destPath == null && !this.silent) {
            this.percent = ((float) loaded) / ((float) total);
            Intent intent1 = new Intent(Stickers.ACTION_STICKERS_DOWNLOAD_PROGRESS);
            intent1.putExtra("id", this.packId);
            intent1.putExtra("progress", this.percent);
            sendBroadcast(intent1);
            Intent intent = new Intent(this, UploaderService.class);
            intent.setAction("NOTHING");
            if (VERSION.SDK_INT < 14) {
                if (this.progress == null) {
                    this.progress = new Notification(17301633, null, System.currentTimeMillis());
                    Notification notification = this.progress;
                    notification.flags |= 34;
                    this.uploadView = new RemoteViews(getPackageName(), C0436R.layout.notify_with_progress);
                    this.uploadView.setTextViewText(C0436R.id.notify_title, getString(C0436R.string.stickers_progress, new Object[]{this.title}));
                    this.uploadView.setTextViewText(C0436R.id.notify_num, ACRAConstants.DEFAULT_STRING_VALUE);
                    this.progress.setLatestEventInfo(getApplicationContext(), null, null, PendingIntent.getService(this, 0, intent, 0));
                    this.progress.contentView = this.uploadView;
                }
                this.uploadView.setProgressBar(C0436R.id.notify_progress, total, loaded, DEBUG);
            } else {
                if (this.notifyBuilder == null) {
                    this.notifyBuilder = new Builder(this).setContentTitle(getString(C0436R.string.stickers_progress, new Object[]{this.title})).setOngoing(true).setProgress(total, loaded, DEBUG).setContentIntent(PendingIntent.getService(this, 0, intent, 0)).setSmallIcon(17301633);
                } else {
                    this.notifyBuilder.setProgress(total, loaded, DEBUG);
                }
                this.progress = this.notifyBuilder.getNotification();
            }
            ((NotificationManager) getSystemService("notification")).notify(this.packId + ID_NOTIFICATION_PROGRESS, this.progress);
        }
    }

    private void showDone() {
        if (this.destPath == null && !this.silent) {
            Notification notification;
            Intent intent = new Intent(this, UploaderService.class);
            intent.setAction("NOTHING");
            if (VERSION.SDK_INT < 14) {
                notification = new Notification(C0436R.drawable.ic_stat_notify_ok, getString(C0436R.string.stickers_ok, new Object[]{this.title}), 0);
                notification.setLatestEventInfo(this, getString(C0436R.string.app_name), getString(C0436R.string.stickers_ok, new Object[]{this.title}), PendingIntent.getService(this, 0, intent, 0));
                notification.flags |= 16;
            } else {
                notification = new Builder(this).setContentTitle(getString(C0436R.string.app_name)).setTicker(getString(C0436R.string.stickers_ok, new Object[]{this.title})).setContentText(getString(C0436R.string.stickers_ok, new Object[]{this.title})).setSmallIcon(C0436R.drawable.ic_stat_notify_ok).setContentIntent(PendingIntent.getService(this, 0, intent, 0)).setAutoCancel(true).getNotification();
            }
            ((NotificationManager) getSystemService("notification")).cancel(this.packId + ID_NOTIFICATION_PROGRESS);
            ((NotificationManager) getSystemService("notification")).notify(this.packId + ID_NOTIFICATION_DONE, notification);
        }
    }

    private void showFailed() {
        if (this.destPath == null && !this.silent) {
            Notification notification;
            Intent intent = new Intent(this, getClass());
            intent.putExtra("id", this.packId);
            intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, this.title);
            intent.putExtra(PlusShare.KEY_CALL_TO_ACTION_URL, this.url);
            if (VERSION.SDK_INT < 14) {
                notification = new Notification(ACRAConstants.DEFAULT_NOTIFICATION_ICON, getString(C0436R.string.stickers_fail, new Object[]{this.title}), 0);
                notification.setLatestEventInfo(this, getString(C0436R.string.app_name), getString(C0436R.string.stickers_fail, new Object[]{this.title}), PendingIntent.getService(this, 0, intent, 0));
                notification.flags |= 16;
            } else {
                notification = new Builder(this).setContentTitle(getString(C0436R.string.app_name)).setTicker(getString(C0436R.string.stickers_fail, new Object[]{this.title})).setContentText(getString(C0436R.string.stickers_fail, new Object[]{this.title})).setSmallIcon(ACRAConstants.DEFAULT_NOTIFICATION_ICON).setContentIntent(PendingIntent.getService(this, 0, intent, 0)).setAutoCancel(true).getNotification();
            }
            ((NotificationManager) getSystemService("notification")).cancel(this.packId + ID_NOTIFICATION_PROGRESS);
            ((NotificationManager) getSystemService("notification")).notify(this.packId + ID_NOTIFICATION_FAILED, notification);
        }
    }

    private void download() {
        File file;
        int i = this.packId + ID_NOTIFICATION_FAILED;
        ((NotificationManager) getSystemService("notification")).cancel(r0);
        updateProgress(0, 10);
        if (this.packId > 0) {
            if (Stickers.getPackState(this.packId) == 5) {
                if (new StoreSetActive(this.packId, true).execSync()) {
                    ArrayList<String> order = new ArrayList();
                    String ordr = getSharedPreferences("stickers", 0).getString("order", ACRAConstants.DEFAULT_STRING_VALUE);
                    if (ordr.length() > 0) {
                        order.addAll(Arrays.asList(ordr.split(",")));
                    }
                    order.add(new StringBuilder(String.valueOf(this.packId)).toString());
                    getSharedPreferences("stickers", 0).edit().putString("order", TextUtils.join(",", order)).commit();
                } else {
                    showFailed();
                    this.packId = 0;
                    Stickers.broadcastUpdate();
                    return;
                }
            }
        }
        if (this.destPath == null) {
            file = new File(getFilesDir(), this.packId + ".zip");
        } else {
            file = new File(this.destPath);
        }
        try {
            InputStream in;
            int total;
            int read;
            if (Arrays.asList(getAssets().list("stickers")).contains(this.packId + ".zip")) {
                in = getAssets().open("stickers/" + this.packId + ".zip");
                total = 0;
            } else {
                URLConnection conn = new URL(this.url).openConnection();
                conn.connect();
                in = conn.getInputStream();
                total = conn.getContentLength();
            }
            file.createNewFile();
            FileOutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[10240];
            int loaded = 0;
            int prevUpdate = 0;
            while (true) {
                read = in.read(buf);
                if (read <= 0) {
                    break;
                }
                out.write(buf, 0, read);
                loaded += read;
                if (loaded - prevUpdate > 100000) {
                    updateProgress(loaded, total);
                    prevUpdate = loaded;
                }
            }
            in.close();
            out.close();
            updateProgress(10, 10);
            if (this.packId > 0) {
                File dir = new File(getFilesDir(), "stickers/" + this.packId);
                ZipFile zipFile = new ZipFile(file);
                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                dir.mkdirs();
                while (entries.hasMoreElements()) {
                    ZipEntry ze = (ZipEntry) entries.nextElement();
                    File f = new File(dir, ze.getName());
                    in = zipFile.getInputStream(ze);
                    out = new FileOutputStream(f);
                    while (true) {
                        read = in.read(buf);
                        if (read <= 0) {
                            break;
                        }
                        out.write(buf, 0, read);
                    }
                    in.close();
                    out.close();
                }
                zipFile.close();
                file.delete();
                generateSizesFile(dir);
                showDone();
                this.packId = 0;
            } else {
                generateSizesFile(new File(file.getParent()));
            }
            Thread.sleep(200);
            Stickers.broadcastUpdate();
        } catch (Throwable x) {
            Log.m532w("vk", x);
            file.delete();
            if (this.packId > 0) {
                Stickers.deleteDownloadedPack(this.packId);
            }
            showFailed();
            this.packId = 0;
            Stickers.broadcastUpdate();
        }
    }

    public static void generateSizesFile(File dir) {
        try {
            String[] files = dir.list(new C05021());
            Arrays.sort(files);
            Options opts = new Options();
            opts.inJustDecodeBounds = true;
            File sizes = new File(dir, "sizes");
            if (sizes.exists()) {
                sizes.delete();
            }
            sizes.createNewFile();
            DataOutputStream os = new DataOutputStream(new FileOutputStream(sizes));
            os.writeInt(1);
            os.writeInt(files.length);
            for (String f : files) {
                BitmapFactory.decodeFile(new File(dir, f).getAbsolutePath(), opts);
                os.writeInt(Integer.parseInt(f.split("_")[0]));
                os.writeInt(opts.outWidth);
                os.writeInt(opts.outHeight);
            }
            os.close();
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }
}
