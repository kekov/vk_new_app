package com.vkontakte.android;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v4.media.TransportMediator;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.ui.FlowLayout;
import com.vkontakte.android.ui.PhotoView;
import java.io.DataOutputStream;
import java.io.IOException;
import org.acra.ACRAConstants;

public class GeoAttachment extends Attachment implements ImageAttachment {
    public static final Creator<GeoAttachment> CREATOR;
    public String address;
    public int id;
    public double lat;
    public double lon;
    public String photo;
    public String title;

    /* renamed from: com.vkontakte.android.GeoAttachment.1 */
    class C02721 implements Creator<GeoAttachment> {
        C02721() {
        }

        public GeoAttachment createFromParcel(Parcel in) {
            return new GeoAttachment(null);
        }

        public GeoAttachment[] newArray(int size) {
            return new GeoAttachment[size];
        }
    }

    /* renamed from: com.vkontakte.android.GeoAttachment.2 */
    class C02732 implements OnClickListener {
        C02732() {
        }

        public void onClick(View v) {
            Bundle args = new Bundle();
            args.putParcelable("point", GeoAttachment.this);
            Navigate.to("GeoPlaceFragment", args, (Activity) v.getContext());
        }
    }

    /* renamed from: com.vkontakte.android.GeoAttachment.3 */
    class C02753 implements OnClickListener {
        private final /* synthetic */ Context val$context;

        /* renamed from: com.vkontakte.android.GeoAttachment.3.1 */
        class C02741 implements DialogInterface.OnClickListener {
            private final /* synthetic */ Context val$context;

            C02741(Context context) {
                this.val$context = context;
            }

            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps"));
                intent.addFlags(268435456);
                this.val$context.startActivity(intent);
            }
        }

        C02753(Context context) {
            this.val$context = context;
        }

        public void onClick(View v) {
            try {
                this.val$context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("geo:" + GeoAttachment.this.lat + "," + GeoAttachment.this.lon + "?z=18&q=" + GeoAttachment.this.lat + "," + GeoAttachment.this.lon)));
            } catch (Throwable th) {
                new Builder(this.val$context).setTitle(C0436R.string.maps_not_available).setMessage(C0436R.string.maps_not_available_descr).setPositiveButton(C0436R.string.open_google_play, new C02741(this.val$context)).setNegativeButton(C0436R.string.close, null).show();
            }
        }
    }

    private class FLinearLayout extends LinearLayout {
        public FLinearLayout(Context context) {
            super(context);
        }

        public void onMeasure(int w, int h) {
            super.onMeasure(w, h);
            setMeasuredDimension(MeasureSpec.getSize(w), getMeasuredHeight());
        }
    }

    public GeoAttachment() {
        this.id = -1;
    }

    public GeoAttachment(double _lat, double _lon, String _title, String _address, int _id, String _photo) {
        this.id = -1;
        this.lat = _lat;
        this.lon = _lon;
        this.id = _id;
        if (_title != null && _title.length() > 0) {
            this.title = _title;
        }
        if (_address != null && _address.length() > 0) {
            this.address = _address;
        }
        if (_photo != null && _photo.length() > 0) {
            this.photo = _photo;
        }
    }

    private GeoAttachment(Parcel p) {
        this.id = -1;
        this.lat = p.readDouble();
        this.lon = p.readDouble();
        this.id = p.readInt();
        this.address = p.readString();
        this.title = p.readString();
        this.photo = p.readString();
    }

    static {
        CREATOR = new C02721();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel p, int arg1) {
        p.writeDouble(this.lat);
        p.writeDouble(this.lon);
        p.writeInt(this.id);
        p.writeString(this.address);
        p.writeString(this.title);
        p.writeString(this.photo);
    }

    public View getFullView(Context context) {
        return getViewForList(context, null);
    }

    public View getViewForList(Context context, View reuse) {
        if (this.id > 0) {
            View v;
            if (reuse != null) {
                v = reuse;
            } else {
                v = Attachment.getReusableView(context, "common");
            }
            ((TextView) v.findViewById(C0436R.id.attach_title)).setText(this.title);
            ((TextView) v.findViewById(C0436R.id.attach_subtitle)).setText(this.address);
            ((ImageView) v.findViewById(C0436R.id.attach_icon)).setImageResource(C0436R.drawable.ic_attach_checkin);
            v.setOnClickListener(new C02732());
            return v;
        }
        View fl = new FrameLayout(context);
        ImageView iv = new ImageView(context);
        iv.setId(1);
        iv.setScaleType(ScaleType.CENTER_CROP);
        iv.setLayoutParams(new LayoutParams(Global.scale(BitmapDescriptorFactory.HUE_MAGENTA), Global.scale(130.0f)));
        fl.addView(iv);
        ImageView marker = new ImageView(context);
        marker.setImageResource(C0436R.drawable.map_marker);
        LayoutParams mparams = new LayoutParams(-2, -1);
        mparams.gravity = 49;
        mparams.topMargin = Global.scale(32.0f);
        marker.setLayoutParams(mparams);
        fl.addView(marker);
        fl.setPadding(0, 0, 0, Global.scale(10.0f));
        fl.setOnClickListener(new C02753(context));
        return fl;
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(10);
        os.writeDouble(this.lat);
        os.writeDouble(this.lon);
        os.writeUTF(this.title == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.title);
        os.writeUTF(this.address == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.address);
        os.writeInt(this.id);
        os.writeUTF(this.photo == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.photo);
    }

    public FlowLayout.LayoutParams getViewLayoutParams() {
        FlowLayout.LayoutParams lp = new FlowLayout.LayoutParams();
        lp.width = Global.scale(BitmapDescriptorFactory.HUE_MAGENTA);
        lp.height = Global.scale(130.0f);
        return lp;
    }

    public String getImageURL() {
        return this.id > 0 ? this.photo : Global.getStaticMapURL(this.lat, this.lon, PhotoView.THUMB_ANIM_DURATION, TransportMediator.KEYCODE_MEDIA_RECORD);
    }

    public void setImage(View view, Bitmap img, boolean fromCache) {
        if (view.findViewById(C0436R.id.attach_icon) == null) {
            ((ImageView) ((ViewGroup) view).findViewById(1)).setImageBitmap(img);
        } else {
            ((ImageView) view.findViewById(C0436R.id.attach_icon)).setImageBitmap(img);
        }
    }

    public void clearImage(View view) {
        if (view.findViewById(C0436R.id.attach_icon) == null) {
            ((ImageView) ((ViewGroup) view).findViewById(1)).setImageBitmap(null);
        } else {
            ((ImageView) view.findViewById(C0436R.id.attach_icon)).setImageResource(C0436R.drawable.ic_attach_checkin);
        }
    }
}
