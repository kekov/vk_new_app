package com.vkontakte.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import com.actionbarsherlock.app.SherlockActivity;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.VKAlertDialog.Builder;

public class NotificationActivity extends SherlockActivity {

    /* renamed from: com.vkontakte.android.NotificationActivity.1 */
    class C03661 implements OnCancelListener {
        C03661() {
        }

        public void onCancel(DialogInterface dialog) {
            NotificationActivity.this.canceled();
            NotificationActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.NotificationActivity.2 */
    class C03672 implements OnClickListener {
        C03672() {
        }

        public void onClick(DialogInterface dialog, int which) {
            NotificationActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(NotificationActivity.this.getIntent().getStringExtra("link"))));
            NotificationActivity.this.opened();
            NotificationActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.NotificationActivity.3 */
    class C03683 implements OnClickListener {
        C03683() {
        }

        public void onClick(DialogInterface dialog, int which) {
            NotificationActivity.this.canceled();
            NotificationActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.NotificationActivity.4 */
    class C03694 implements OnClickListener {
        C03694() {
        }

        public void onClick(DialogInterface dialog, int which) {
            NotificationActivity.this.opened();
            NotificationActivity.this.finish();
        }
    }

    public void onCreate(Bundle b) {
        CharSequence stringExtra;
        super.onCreate(b);
        Intent intent = getIntent();
        Builder builder = new Builder(this);
        if (intent.hasExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)) {
            stringExtra = intent.getStringExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
        } else {
            stringExtra = getResources().getString(C0436R.string.notification);
        }
        AlertDialog.Builder alert = builder.setTitle(stringExtra).setMessage(Html.fromHtml(intent.getStringExtra(LongPollService.EXTRA_MESSAGE))).setIcon(17301659).setOnCancelListener(new C03661());
        if (intent.hasExtra("link")) {
            alert.setPositiveButton(C0436R.string.view, new C03672()).setNegativeButton(C0436R.string.close, new C03683());
        } else {
            alert.setPositiveButton(C0436R.string.close, new C03694());
        }
        alert.show();
    }

    private void opened() {
    }

    private void canceled() {
    }
}
