package com.vkontakte.android;

import android.content.Context;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.TextUtils.TruncateAt;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class AttachInfoView extends LinearLayout {

    private class URLSpanNoUnderline extends URLSpan {
        public URLSpanNoUnderline(String url) {
            super(url);
        }

        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            if (ds.drawableState != null) {
                boolean isPressed = false;
                int i = 0;
                while (i < ds.drawableState.length) {
                    if (ds.drawableState[i] == 16842919 || ds.drawableState[i] == 16842913) {
                        isPressed = true;
                    }
                    i++;
                }
                if (isPressed) {
                    ds.setColor(-1);
                } else {
                    ds.setColor(-13936518);
                }
            }
        }
    }

    public AttachInfoView(Context context, int iconID, String text) {
        super(context);
        LayoutParams lparams = new LayoutParams(-1, -2);
        lparams.topMargin = (int) (3.0f * Global.displayDensity);
        setLayoutParams(lparams);
        setGravity(16);
        ImageView iconView = new ImageView(getContext());
        iconView.setDuplicateParentStateEnabled(true);
        addView(iconView);
        TextView textView = new TextView(getContext());
        textView.setMovementMethod(new LinkMovementMethod());
        textView.setTextColor(getResources().getColorStateList(C0436R.color.hint));
        textView.setLinkTextColor(getResources().getColorStateList(C0436R.color.link));
        textView.setSingleLine(true);
        textView.setEllipsize(TruncateAt.MARQUEE);
        addView(textView);
        iconView.setImageResource(iconID);
    }

    private Spannable stripUnderlines(Spannable s) {
        for (URLSpan span : (URLSpan[]) s.getSpans(0, s.length(), URLSpan.class)) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            s.setSpan(new URLSpanNoUnderline(span.getURL()), start, end, 0);
        }
        return s;
    }
}
