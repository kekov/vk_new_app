package com.vkontakte.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class DebugBroadcastReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent1) {
        Intent intent = new Intent(context, DebugPrefsActivity.class);
        intent.addFlags(268435456);
        context.startActivity(intent);
    }
}
