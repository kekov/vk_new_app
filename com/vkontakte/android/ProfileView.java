package com.vkontakte.android;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.Html;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.text.method.LinkMovementMethod;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.WebDialog;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.AccountBanUser;
import com.vkontakte.android.api.ExtendedUserProfile;
import com.vkontakte.android.api.ExtendedUserProfile.Relative;
import com.vkontakte.android.api.ExtendedUserProfile.School;
import com.vkontakte.android.api.ExtendedUserProfile.University;
import com.vkontakte.android.api.FriendsAdd;
import com.vkontakte.android.api.FriendsDelete;
import com.vkontakte.android.api.GetFullProfile;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.GroupsJoin;
import com.vkontakte.android.api.GroupsLeave;
import com.vkontakte.android.api.PhotosGet;
import com.vkontakte.android.api.StatusSet;
import com.vkontakte.android.api.WallGet;
import com.vkontakte.android.api.WallGet.Callback;
import com.vkontakte.android.cache.Cache;
import com.vkontakte.android.cache.UserWallCache;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.fragments.PhotoViewerFragment;
import com.vkontakte.android.fragments.ProfileFragment;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.ui.Fonts;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.MergeAdapter;
import com.vkontakte.android.ui.MergeImageLoaderAdapter;
import com.vkontakte.android.ui.PhotoFeedView;
import com.vkontakte.android.ui.PhotoView;
import com.vkontakte.android.ui.RefreshableListView;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;
import org.acra.ACRAConstants;

public class ProfileView extends NewsView {
    private OnClickListener btnClickListener;
    private LinearLayout buttons;
    private LinearLayout buttonsWrap;
    private OnClickListener counterClickListener;
    private LinearLayout countersWrap;
    private APIRequest currentReq;
    private TextView emptyText;
    private int fixedPostId;
    private ProfileFragment fragment;
    private ArrayList<View> headerItems;
    private View headerView;
    private ProfileInfoAdapter infoAdapter;
    private OnClickListener infoItemClickListener;
    private ArrayList<InfoItem> infoItems;
    private String infoLine;
    private MergeAdapter lAdapter;
    private boolean ownerOnly;
    private PhotoFeedView photoFeed;
    private View postponedView;
    private ExtendedUserProfile profile;
    private ArrayList<View> relativesViews;
    private View selector;
    private boolean showExtended;
    private View suggestsView;
    private int uid;

    /* renamed from: com.vkontakte.android.ProfileView.18 */
    class AnonymousClass18 implements DialogInterface.OnClickListener {
        private final /* synthetic */ EditText val$ed;

        AnonymousClass18(EditText editText) {
            this.val$ed = editText;
        }

        public void onClick(DialogInterface dialog, int which) {
            ProfileView.this.setStatus(this.val$ed.getText().toString());
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.19 */
    class AnonymousClass19 implements OnShowListener {
        private final /* synthetic */ EditText val$ed;

        AnonymousClass19(EditText editText) {
            this.val$ed = editText;
        }

        public void onShow(DialogInterface dialog) {
            ((InputMethodManager) ProfileView.this.getContext().getSystemService("input_method")).showSoftInput(this.val$ed, 1);
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.1 */
    class C04171 implements OnClickListener {
        C04171() {
        }

        public void onClick(View v) {
            String act = (String) v.getTag();
            Bundle args;
            if ("relation".equals(act)) {
                args = new Bundle();
                args.putInt("id", ProfileView.this.profile.relationPartner);
                Navigate.to("ProfileFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("bdate".equals(act)) {
                intent = new Intent("android.intent.action.EDIT");
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, ProfileView.this.getResources().getString(C0436R.string.profile_bdate_event_title, new Object[]{new StringBuilder(String.valueOf(ProfileView.this.profile.firstNameGen)).append(" ").append(ProfileView.this.profile.lastNameGen).toString()}));
                Calendar cal = Calendar.getInstance();
                cal.set(cal.get(1), ProfileView.this.profile.bMonth - 1, ProfileView.this.profile.bDay);
                if (cal.before(Calendar.getInstance())) {
                    cal.add(1, 1);
                }
                intent.putExtra("beginTime", cal.getTimeInMillis());
                intent.putExtra("allDay", true);
                intent.putExtra("rrule", "FREQ=YEARLY");
                ProfileView.this.getContext().startActivity(intent);
            } else if (act != null && act.startsWith("relative")) {
                args = new Bundle();
                args.putInt("id", Integer.parseInt(act.replace("relative", ACRAConstants.DEFAULT_STRING_VALUE)));
                Navigate.to("ProfileFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("twitter".equals(act)) {
                ProfileView.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://twitter.com/" + ProfileView.this.profile.twitter)));
            } else if ("facebook".equals(act)) {
                ProfileView.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://facebook.com/profile.php?id=" + ProfileView.this.profile.facebookId)));
            } else if ("livejournal".equals(act)) {
                ProfileView.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://" + ProfileView.this.profile.livejournal + ".livejournal.com/")));
            } else if ("instagram".equals(act)) {
                ProfileView.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://instagram.com/" + ProfileView.this.profile.instagram)));
            } else if ("skype".equals(act)) {
                try {
                    ProfileView.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("skype:" + ProfileView.this.profile.skype + "?call")));
                } catch (Exception e) {
                    Toast.makeText(ProfileView.this.getContext(), C0436R.string.skype_not_installed, 0).show();
                }
            } else if ("homePhone".equals(act)) {
                ProfileView.this.getContext().startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + ProfileView.this.profile.homePhone)));
            } else if ("mobilePhone".equals(act)) {
                ProfileView.this.getContext().startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + ProfileView.this.profile.mobilePhone)));
            } else if ("website".equals(act)) {
                String site = ProfileView.this.profile.website;
                if (!(site.startsWith("http://") || site.startsWith("https://"))) {
                    site = "http://" + site;
                }
                ProfileView.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(site)));
            } else if ("place".equals(act)) {
                ProfileView.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("geo:" + ProfileView.this.profile.lat + "," + ProfileView.this.profile.lon + "?q=" + ProfileView.this.profile.lat + "," + ProfileView.this.profile.lon)));
            } else if ("time".equals(act)) {
                intent = new Intent("android.intent.action.EDIT");
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, ProfileView.this.profile.profile.fullName);
                intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, ProfileView.this.profile.about);
                intent.putExtra("beginTime", ((long) ProfileView.this.profile.eventStartTime) * 1000);
                if (ProfileView.this.profile.eventEndTime > 0) {
                    intent.putExtra("endTime", ((long) ProfileView.this.profile.eventEndTime) * 1000);
                }
                ProfileView.this.getContext().startActivity(intent);
            } else if ("wiki".equals(act)) {
                intent = new Intent(ProfileView.this.getContext(), WikiViewActivity.class);
                intent.putExtra("oid", ProfileView.this.uid);
                intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, ProfileView.this.profile.mobilePhone);
                ProfileView.this.getContext().startActivity(intent);
            } else if ("status".equals(act)) {
                if (ProfileView.this.profile.audioStatus != null) {
                    ProfileView.this.openAudioStatus();
                } else if (ProfileView.this.uid == Global.uid) {
                    ProfileView.this.showStatusEditDlg(ProfileView.this.profile.activity);
                }
            } else if ("groups".equals(act)) {
                args = new Bundle();
                args.putInt("uid", ProfileView.this.uid);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, ProfileView.this.getResources().getString(C0436R.string.groups_of_user, new Object[]{ProfileView.this.profile.firstNameGen}));
                Navigate.to("GroupsFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("subscriptions".equals(act)) {
                args = new Bundle();
                args.putInt("uid", ProfileView.this.uid);
                args.putInt(WebDialog.DIALOG_PARAM_TYPE, 10);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, ProfileView.this.getResources().getString(C0436R.string.profile_subscriptions));
                Navigate.to("UserListFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("docs".equals(act)) {
                args = new Bundle();
                args.putInt("oid", ProfileView.this.uid);
                Navigate.to("DocumentsFragment", args, (Activity) ProfileView.this.getContext());
            }
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.21 */
    class AnonymousClass21 implements DialogInterface.OnClickListener {
        private final /* synthetic */ View val$alertView;

        AnonymousClass21(View view) {
            this.val$alertView = view;
        }

        public void onClick(DialogInterface dialog, int which) {
            ProfileView.this.doAddFriend(((TextView) this.val$alertView.findViewById(C0436R.id.add_friend_msg)).getText().toString());
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.2 */
    class C04182 implements OnClickListener {
        C04182() {
        }

        public void onClick(View v) {
            String act = (String) v.getTag();
            Bundle args;
            if ("photos".equals(act)) {
                args = new Bundle();
                args.putInt("uid", ProfileView.this.uid);
                args.putCharSequence("user_name_ins", ProfileView.this.profile.firstNameIns);
                Navigate.to("PhotoAlbumsListFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("friends".equals(act)) {
                args = new Bundle();
                args.putInt("uid", ProfileView.this.uid);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, ProfileView.this.getResources().getString(C0436R.string.friends_of_user, new Object[]{ProfileView.this.profile.firstNameGen}));
                Navigate.to("FriendsFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("mutual_friends".equals(act)) {
                args = new Bundle();
                args.putInt("uid", ProfileView.this.uid);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, ProfileView.this.getResources().getString(C0436R.string.friends_of_user, new Object[]{ProfileView.this.profile.firstNameGen}));
                args.putBoolean("mutual", true);
                Navigate.to("FriendsFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("videos".equals(act)) {
                args = new Bundle();
                args.putInt("uid", ProfileView.this.uid);
                if (ProfileView.this.profile.profile.uid > 0) {
                    args.putCharSequence("username_ins", ProfileView.this.profile.firstNameIns);
                }
                String str = PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE;
                Resources resources = ProfileView.this.getResources();
                Object[] objArr = new Object[1];
                objArr[0] = ProfileView.this.uid > 0 ? ProfileView.this.profile.firstNameGen : ProfileView.this.getResources().getString(C0436R.string.group_s);
                args.putCharSequence(str, resources.getString(C0436R.string.videos_by_user, objArr));
                if (ProfileView.this.uid < 0) {
                    args.putString("groupName", ProfileView.this.profile.profile.fullName);
                    args.putString("groupPhoto", ProfileView.this.profile.profile.photo);
                }
                Navigate.to("VideoListFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("audios".equals(act)) {
                args = new Bundle();
                args.putInt("uid", ProfileView.this.uid);
                args.putCharSequence("username", ProfileView.this.uid > 0 ? ProfileView.this.profile.firstNameGen : ProfileView.this.getResources().getString(C0436R.string.group_s));
                Navigate.to("AudioListFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("groups".equals(act)) {
                args = new Bundle();
                args.putInt("uid", ProfileView.this.uid);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, ProfileView.this.getResources().getString(C0436R.string.groups_of_user, new Object[]{ProfileView.this.profile.firstNameGen}));
                Navigate.to("GroupsFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("topics".equals(act)) {
                args = new Bundle();
                args.putInt("gid", -ProfileView.this.uid);
                Navigate.to("BoardTopicsFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("members".equals(act)) {
                args = new Bundle();
                args.putInt("gid", -ProfileView.this.uid);
                args.putInt(WebDialog.DIALOG_PARAM_TYPE, 1);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, ProfileView.this.getResources().getString(C0436R.string.group_members));
                Navigate.to("UserListFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("followers".equals(act)) {
                args = new Bundle();
                args.putInt("uid", ProfileView.this.uid);
                args.putInt(WebDialog.DIALOG_PARAM_TYPE, 5);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, ProfileView.this.getResources().getString(C0436R.string.followers_of, new Object[]{ProfileView.this.profile.firstNameGen}));
                Navigate.to("UserListFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("docs".equals(act)) {
                args = new Bundle();
                args.putInt("oid", ProfileView.this.uid);
                Navigate.to("DocumentsFragment", args, (Activity) ProfileView.this.getContext());
            }
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.3 */
    class C04193 implements OnClickListener {
        C04193() {
        }

        public void onClick(View v) {
            String act = (String) v.getTag();
            if (LongPollService.EXTRA_MESSAGE.equals(act)) {
                Bundle args = new Bundle();
                args.putInt("id", ProfileView.this.uid);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, new StringBuilder(String.valueOf(ProfileView.this.profile.profile.firstName)).append(" ").append(ProfileView.this.profile.profile.lastName).toString());
                args.putCharSequence("photo", ProfileView.this.profile.profile.photo);
                Navigate.to("ChatFragment", args, (Activity) ProfileView.this.getContext());
            } else if ("post".equals(act)) {
                ProfileView.this.showNewPost();
            } else if ("add".equals(act)) {
                ProfileView.this.addFriend();
            } else if ("accept".equals(act)) {
                ProfileView.this.doAddFriend(ACRAConstants.DEFAULT_STRING_VALUE);
            } else if ("cancel".equals(act)) {
                ProfileView.this.doRemoveFriend();
            } else if ("join".equals(act)) {
                ProfileView.this.joinGroup(true);
            } else if ("join_unsure".equals(act)) {
                ProfileView.this.joinGroup(false);
            } else if ("leave".equals(act)) {
                ProfileView.this.leaveGroup();
            } else if ("photo".equals(act)) {
                Intent intent = new Intent(ProfileView.this.getContext(), PostPhotoActivity.class);
                intent.putExtra("option", 0);
                ProfileView.this.getContext().startActivity(intent);
            }
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.4 */
    class C04204 implements OnClickListener {
        C04204() {
        }

        public void onClick(View v) {
            ProfileView.this.toggleExtendedInfo();
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.5 */
    class C04215 implements OnClickListener {
        C04215() {
        }

        public void onClick(View v) {
            ProfileView.this.switchOwnerOnly(false);
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.6 */
    class C04226 implements OnClickListener {
        C04226() {
        }

        public void onClick(View v) {
            ProfileView.this.switchOwnerOnly(true);
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.7 */
    class C04237 implements OnClickListener {
        C04237() {
        }

        public void onClick(View v) {
            ProfileView.this.showNewPost();
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.8 */
    class C04248 implements OnClickListener {
        C04248() {
        }

        public void onClick(View v) {
            ProfileView.this.errorView.setVisibility(8);
            ProfileView.this.bigProgress.setVisibility(0);
            ProfileView.this.loadInitial();
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.9 */
    class C04259 implements OnClickListener {
        C04259() {
        }

        public void onClick(View v) {
            Bundle args = new Bundle();
            args.putInt("owner_id", ProfileView.this.uid);
            args.putString("mode", "postponed");
            Navigate.to("NewsFragment", args, (Activity) ProfileView.this.getContext());
        }
    }

    private static class InfoItem {
        public static final int TYPE_COUNTER = 4;
        public static final int TYPE_HEADER = 2;
        public static final int TYPE_REGULAR = 0;
        public static final int TYPE_RELATIVE = 1;
        public static final int TYPE_STATUS = 3;
        public CharSequence data;
        public String subData;
        public String tag;
        public String title;
        public int type;

        public InfoItem(int _type, String _title, CharSequence _data, String _subdata, String _tag) {
            this.type = _type;
            this.title = _title;
            this.data = _data;
            this.subData = _subdata;
            this.tag = _tag;
        }
    }

    private class ProfileInfoAdapter extends BaseAdapter {
        private ProfileInfoAdapter() {
        }

        public int getCount() {
            int i = 0;
            int size = (ProfileView.this.showExtended ? ProfileView.this.infoItems.size() : 0) + 1;
            if (ProfileView.this.headerItems != null) {
                i = ProfileView.this.headerItems.size();
            }
            return size + i;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public boolean isEnabled(int item) {
            return false;
        }

        public int getViewTypeCount() {
            return 14;
        }

        public int getItemViewType(int pos) {
            if (pos == 0) {
                return 0;
            }
            int size;
            if (ProfileView.this.showExtended) {
                size = ProfileView.this.infoItems.size();
            } else {
                size = 0;
            }
            int ipos = (pos - size) - 1;
            if (ipos == 0) {
                return 6;
            }
            if (ipos == 1) {
                return 7;
            }
            if (ipos == 2) {
                return 8;
            }
            if (ipos == 3) {
                return 9;
            }
            if (ipos == 4) {
                return 10;
            }
            if (ipos == 5) {
                return 11;
            }
            if (ipos == 6) {
                return 12;
            }
            if (pos - 1 < ProfileView.this.infoItems.size()) {
                return ((InfoItem) ProfileView.this.infoItems.get(pos - 1)).type + 1;
            }
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            int i = 0;
            if (position == 0) {
                if (ProfileView.this.profile == null || !ProfileView.this.imgLoader.isAlreadyLoaded(ProfileView.this.profile.bigPhoto)) {
                    ((ImageView) ProfileView.this.headerView.findViewById(C0436R.id.profile_photo)).setImageResource(ProfileView.this.uid < 0 ? C0436R.drawable.group_placeholder_profile : C0436R.drawable.user_placeholder_profile);
                } else {
                    ((ImageView) ProfileView.this.headerView.findViewById(C0436R.id.profile_photo)).setImageBitmap(ProfileView.this.imgLoader.get(ProfileView.this.profile.bigPhoto));
                }
                return ProfileView.this.headerView;
            }
            int size;
            if (ProfileView.this.showExtended) {
                size = ProfileView.this.infoItems.size();
            } else {
                size = 0;
            }
            int ipos = (position - size) - 1;
            if (ipos >= 0) {
                return (View) ProfileView.this.headerItems.get(ipos);
            }
            if (!ProfileView.this.showExtended) {
                return null;
            }
            boolean z;
            View view = convertView;
            InfoItem item = (InfoItem) ProfileView.this.infoItems.get(position - 1);
            if (view == null) {
                int r = C0436R.layout.profile_info_item;
                switch (item.type) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                    case Group.ADMIN_LEVEL_ADMIN /*3*/:
                        r = C0436R.layout.profile_info_item;
                        break;
                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                        r = C0436R.layout.profile_info_relative;
                        break;
                    case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                        r = C0436R.layout.profile_info_section_header;
                        break;
                    case UserListView.TYPE_FAVE /*4*/:
                        r = C0436R.layout.profile_info_counter;
                        break;
                }
                view = View.inflate(ProfileView.this.getContext(), r, null);
                view.setOnClickListener(ProfileView.this.infoItemClickListener);
            }
            view.setTag(item.tag);
            if (item.tag != null) {
                z = true;
            } else {
                z = false;
            }
            view.setClickable(z);
            if (item.type == 2) {
                z = position + -1 == 0 || (position - 1 == 1 && ((InfoItem) ProfileView.this.infoItems.get(position - 2)).type == 3);
                view.setBackgroundDrawable(new ProfileInfoHeaderBgDrawable(z));
            } else if (item.type == 3) {
                int i2;
                view.setBackgroundDrawable(new ProfileInfoItemBgDrawable(false, true, true));
                TextView textView = (TextView) view.findViewById(C0436R.id.profile_info_text);
                if (ProfileView.this.profile.activity.length() > 0) {
                    i2 = Color.WINDOW_BACKGROUND_COLOR;
                } else {
                    i2 = 1711276032;
                }
                textView.setTextColor(i2);
            } else if (position - 1 == 0 || (position - 1 == 1 && ((InfoItem) ProfileView.this.infoItems.get(position - 2)).type == 3)) {
                view.setBackgroundDrawable(new ProfileInfoItemBgDrawable(false, true, position == ProfileView.this.infoItems.size()));
            } else if (position - 1 == ProfileView.this.infoItems.size() - 1) {
                z = position + -2 < 0 || ((InfoItem) ProfileView.this.infoItems.get(position - 2)).type != 2;
                view.setBackgroundDrawable(new ProfileInfoItemBgDrawable(z, false, true));
            } else {
                z = position + -2 < 0 || ((InfoItem) ProfileView.this.infoItems.get(position - 2)).type != 2;
                view.setBackgroundDrawable(new ProfileInfoItemBgDrawable(z, false, false));
            }
            if (item.title != null) {
                ((TextView) view.findViewById(C0436R.id.profile_info_title)).setText(VERSION.SDK_INT >= 14 ? item.title : item.title.toUpperCase());
            }
            view.findViewById(C0436R.id.profile_info_title).setVisibility(item.title != null ? 0 : 8);
            if (item.data != null) {
                ((TextView) view.findViewById(C0436R.id.profile_info_text)).setText(item.data);
                ((TextView) view.findViewById(C0436R.id.profile_info_text)).setMovementMethod(item.tag == null ? LinkMovementMethod.getInstance() : null);
            }
            if (!(item.subData == null || item.type == 1)) {
                ((TextView) view.findViewById(C0436R.id.profile_info_subtext)).setText(item.subData);
            }
            if (item.type == 0 || item.type == 1 || item.type == 3) {
                view.findViewById(C0436R.id.profile_info_text).setVisibility(item.data != null ? 0 : 8);
            }
            if (item.type == 0 || item.type == 3) {
                View findViewById = view.findViewById(C0436R.id.profile_info_subtext);
                if (item.subData == null) {
                    i = 8;
                }
                findViewById.setVisibility(i);
            }
            if (item.type == 1) {
                if (ProfileView.this.imgLoader.isAlreadyLoaded(item.subData)) {
                    ((ImageView) view.findViewById(C0436R.id.profile_info_photo)).setImageBitmap(ProfileView.this.imgLoader.get(item.subData));
                } else {
                    ((ImageView) view.findViewById(C0436R.id.profile_info_photo)).setImageResource(C0436R.drawable.user_placeholder);
                }
                if (!ProfileView.this.relativesViews.contains(view)) {
                    ProfileView.this.relativesViews.add(view);
                }
            }
            return view;
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.16 */
    class AnonymousClass16 implements Callback {
        private final /* synthetic */ int val$count;
        private final /* synthetic */ boolean val$refresh;

        AnonymousClass16(boolean z, int i) {
            this.val$refresh = z;
            this.val$count = i;
        }

        public void success(ArrayList<NewsEntry> news, int total, Object status, int postponedCount, int suggestedCount) {
            if (ProfileView.this.offset == 0) {
                ProfileView.this.profile.postponedCount = postponedCount;
                ProfileView.this.profile.suggestedCount = suggestedCount;
                ProfileView.this.updatePostsButtons();
            }
            int ii = 1;
            Iterator it = news.iterator();
            while (it.hasNext()) {
                NewsEntry e = (NewsEntry) it.next();
                ii++;
            }
            ProfileView.this.currentReq = null;
            if (this.val$refresh && news.size() > 0 && ((NewsEntry) news.get(0)).flag(GLRenderBuffer.EGL_SURFACE_SIZE)) {
                ProfileView.this.fixedPostId = ((NewsEntry) news.get(0)).postID;
            }
            it = news.iterator();
            while (it.hasNext()) {
                NewsEntry p = (NewsEntry) it.next();
                if (p.postID == ProfileView.this.fixedPostId && !p.flag(GLRenderBuffer.EGL_SURFACE_SIZE)) {
                    news.remove(p);
                    break;
                }
            }
            if (ProfileView.this.list.getVisibility() != 0) {
                Global.showViewAnimated(ProfileView.this.list, true, PhotoView.THUMB_ANIM_DURATION);
            }
            if (total == 0) {
                if (!ProfileView.this.headerItems.contains(ProfileView.this.emptyText)) {
                    ProfileView.this.headerItems.add(ProfileView.this.emptyText);
                }
                ProfileView.this.emptyText.setText(ProfileView.this.uid == Global.uid ? C0436R.string.wall_empty_my : C0436R.string.wall_empty);
            } else {
                ProfileView.this.headerItems.remove(ProfileView.this.emptyText);
            }
            if (this.val$refresh) {
                ProfileView.this.selector.findViewById(C0436R.id.profile_wall_post_btn).setVisibility(ProfileView.this.profile.canPost ? 0 : 4);
                ProfileView.this.selector.findViewById(C0436R.id.profile_wall_progress).setVisibility(8);
            }
            ProfileView profileView = ProfileView.this;
            profileView.offset += this.val$count;
            if ((ProfileView.this.uid == 0 || ProfileView.this.uid == Global.uid) && (this.val$refresh || ProfileView.this.lastUpdateTime == 0)) {
                if (ProfileView.this.ownerOnly == (!ProfileView.this.profile.showAllPosts)) {
                    UserWallCache.replace(news, ProfileView.this.getContext());
                    ProfileView.this.getContext().getSharedPreferences(null, 0).edit().putInt("postponed_count", postponedCount).commit();
                }
            }
            if (!this.val$refresh) {
                Iterator<NewsEntry> itr = news.iterator();
                while (itr.hasNext()) {
                    e = (NewsEntry) itr.next();
                    boolean removed = false;
                    it = ProfileView.this.news.iterator();
                    while (it.hasNext()) {
                        if (e.postID == ((NewsEntry) it.next()).postID) {
                            itr.remove();
                            removed = true;
                            break;
                        }
                    }
                    if (!removed) {
                        it = ProfileView.this.preloadedNews.iterator();
                        while (it.hasNext()) {
                            if (e.postID == ((NewsEntry) it.next()).postID) {
                                itr.remove();
                                removed = true;
                                break;
                            }
                        }
                        if (removed) {
                        }
                    }
                }
            }
            if (news.size() > 10) {
                ProfileView.this.onDataLoaded(news.subList(0, 10), this.val$refresh);
                if (this.val$refresh) {
                    ProfileView.this.preloadedNews.clear();
                }
                ProfileView.this.preloadedNews.addAll(news.subList(10, news.size()));
            } else if (ProfileView.this.preloading) {
                ProfileView.this.preloadedNews.addAll(news);
                ProfileView.this.dataLoading = false;
            } else {
                ProfileView.this.onDataLoaded(news, this.val$refresh);
            }
            ProfileView.this.dataLoading = false;
            ProfileView.this.preloading = false;
            if (ProfileView.this.preloadOnReady) {
                ProfileView.this.preloadOnReady = false;
                ProfileView.this.preloading = true;
                ProfileView.this.loadData(false);
            }
        }

        public void fail(int ecode, String emsg) {
            boolean z = true;
            ProfileView.this.currentReq = null;
            ProfileView.this.dataLoading = false;
            ProfileView profileView = ProfileView.this;
            ProfileView profileView2 = ProfileView.this;
            ProfileView.this.dataLoading = false;
            profileView2.prependNewEntries = false;
            profileView.refreshingOnStart = false;
            if (this.val$refresh) {
                ProfileView.this.list.refreshDone();
            }
            if (ecode == 18 || ecode == 15) {
                if (ecode == 18) {
                    ProfileView.this.emptyText.setText(C0436R.string.page_deleted);
                }
                if (ecode == 15) {
                    ProfileView.this.emptyText.setText(C0436R.string.page_blacklist);
                }
                Global.showViewAnimated(ProfileView.this.list, true, PhotoView.THUMB_ANIM_DURATION);
                if (!ProfileView.this.headerItems.contains(ProfileView.this.emptyText)) {
                    ProfileView.this.headerItems.add(ProfileView.this.emptyText);
                }
                ProfileView.this.onDataLoaded(new ArrayList(), this.val$refresh);
                RefreshableListView refreshableListView = ProfileView.this.list;
                if (ProfileView.this.uid >= 0) {
                    z = false;
                }
                refreshableListView.setDraggingEnabled(z);
                ProfileView.this.headerItems.remove(ProfileView.this.selector);
                if (ProfileView.this.uid > 0) {
                    ProfileView.this.profile.friendStatus = -1;
                }
                ProfileView.this.updateButtons();
                return;
            }
            ProfileView.this.onError(ecode, emsg);
        }
    }

    /* renamed from: com.vkontakte.android.ProfileView.20 */
    class AnonymousClass20 implements StatusSet.Callback {
        private final /* synthetic */ String val$status;

        AnonymousClass20(String str) {
            this.val$status = str;
        }

        public void success() {
            ProfileView.this.profile.activity = this.val$status;
            ((InfoItem) ProfileView.this.infoItems.get(0)).data = this.val$status.length() > 0 ? this.val$status : ProfileView.this.getResources().getString(C0436R.string.change_status);
            ((TextView) ProfileView.this.headerView.findViewById(C0436R.id.profile_activity)).setText(this.val$status.length() > 0 ? ProfileView.this.profile.activity : ProfileView.this.infoLine);
            ProfileView.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(ProfileView.this.getContext(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
            ProfileView.this.showStatusEditDlg(this.val$status);
        }
    }

    private class ProfileImagesAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.ProfileView.ProfileImagesAdapter.1 */
        class C04261 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;

            C04261(Bitmap bitmap) {
                this.val$bitmap = bitmap;
            }

            public void run() {
                ((ImageView) ProfileView.this.headerView.findViewById(C0436R.id.profile_photo)).setImageBitmap(this.val$bitmap);
            }
        }

        /* renamed from: com.vkontakte.android.ProfileView.ProfileImagesAdapter.2 */
        class C04272 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C04272(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.profile_info_photo);
                    if (vv != null) {
                        Log.m525d("vk", "set bitmap");
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private ProfileImagesAdapter() {
        }

        public int getItemCount() {
            return ((ProfileView.this.showExtended ? ProfileView.this.infoItems.size() : 0) + 1) + ProfileView.this.headerItems.size();
        }

        public int getImageCountForItem(int item) {
            if (item == 0) {
                return 1;
            }
            if (!ProfileView.this.showExtended || item <= 0 || item - 1 >= ProfileView.this.infoItems.size() || ((InfoItem) ProfileView.this.infoItems.get(item - 1)).type != 1) {
                return 0;
            }
            return 1;
        }

        public String getImageURL(int item, int image) {
            if (item == 0) {
                return ProfileView.this.profile.bigPhoto;
            }
            if (!ProfileView.this.showExtended || item <= 0 || item - 1 >= ProfileView.this.infoItems.size() || ((InfoItem) ProfileView.this.infoItems.get(item - 1)).type != 1) {
                return null;
            }
            return ((InfoItem) ProfileView.this.infoItems.get(item - 1)).subData;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            if (item == 0) {
                ProfileView.this.post(new C04261(bitmap));
            }
            if (ProfileView.this.showExtended && item > 0 && item - 1 < ProfileView.this.infoItems.size() && ((InfoItem) ProfileView.this.infoItems.get(item - 1)).type == 1) {
                Log.m525d("vk", "loaded: " + item);
                View rv = null;
                Iterator it = ProfileView.this.relativesViews.iterator();
                while (it.hasNext()) {
                    View v = (View) it.next();
                    if (((InfoItem) ProfileView.this.infoItems.get(item - 1)).tag.equals(v.getTag())) {
                        rv = v;
                        Log.m525d("vk", "found view: " + item);
                        break;
                    }
                }
                if (rv != null) {
                    ((Activity) ProfileView.this.getContext()).runOnUiThread(new C04272(rv, bitmap));
                }
            }
        }
    }

    public ProfileView(Context context, int id, ProfileFragment fr) {
        super(context);
        this.showExtended = false;
        this.infoItems = new ArrayList();
        this.relativesViews = new ArrayList();
        this.headerItems = new ArrayList();
        this.ownerOnly = false;
        this.infoLine = ACRAConstants.DEFAULT_STRING_VALUE;
        this.infoItemClickListener = new C04171();
        this.counterClickListener = new C04182();
        this.btnClickListener = new C04193();
        this.uid = id;
        this.fragment = fr;
        this.headerView = inflate(context, C0436R.layout.profile_head, null);
        this.list.setDividerHeight(0);
        this.list.setOnItemLongClickListener(null);
        this.list.setTopColor(-13551805);
        this.list.setProgressResource(C0436R.drawable.p2r_progress_white_anim);
        this.list.setArrowResource(C0436R.drawable.ic_pull_arrow_white, C0436R.drawable.ic_pull_arrow_white);
        this.list.setTextColor(-4276288);
        this.headerView.findViewById(C0436R.id.profile_head_highlight).setOnClickListener(new C04204());
        this.buttons = new LinearLayout(context);
        this.buttons.setOrientation(0);
        this.buttonsWrap = new LinearLayout(context);
        this.buttonsWrap.setOrientation(1);
        this.buttonsWrap.addView(this.buttons);
        this.buttons.setPadding(Global.scale(12.0f), 0, Global.scale(12.0f), 0);
        this.buttonsWrap.setBackgroundColor(-13551805);
        this.countersWrap = new LinearLayout(getContext());
        this.countersWrap.setBackgroundColor(-13551805);
        this.countersWrap.setPadding(Global.scale(12.0f), Global.scale(12.0f), Global.scale(12.0f), Global.scale(12.0f));
        this.photoFeed = new PhotoFeedView(getContext());
        this.photoFeed.setBackgroundColor(-13551805);
        this.selector = inflate(getContext(), C0436R.layout.profile_wall_selector, null);
        this.selector.findViewById(C0436R.id.profile_wall_all_posts).setOnClickListener(new C04215());
        this.selector.findViewById(C0436R.id.profile_wall_owner_posts).setOnClickListener(new C04226());
        this.selector.findViewById(C0436R.id.profile_wall_post_btn).setOnClickListener(new C04237());
        this.selector.findViewById(C0436R.id.profile_wall_all_posts).setSelected(true);
        this.list.setVisibility(8);
        this.errorView.setOnRetryListener(new C04248());
        this.emptyText = new TextView(getContext());
        this.emptyText.setText(C0436R.string.wall_empty);
        this.emptyText.setGravity(17);
        this.emptyText.setPadding(Global.scale(12.0f), Global.scale(12.0f), Global.scale(12.0f), Global.scale(12.0f));
        this.emptyText.setTextSize(GalleryPickerFooterView.BADGE_SIZE);
        this.emptyText.setTextColor(-6052957);
        this.postponedView = View.inflate(getContext(), C0436R.layout.friend_req_show_all, null);
        this.suggestsView = View.inflate(getContext(), C0436R.layout.friend_req_show_all, null);
        this.postponedView.setOnClickListener(new C04259());
        this.suggestsView.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putInt("owner_id", ProfileView.this.uid);
                args.putString("mode", "suggested");
                Navigate.to("NewsFragment", args, (Activity) ProfileView.this.getContext());
            }
        });
        removeView(this.emptyView);
        if (this.uid > 0) {
            this.headerView.findViewById(C0436R.id.profile_photo_wrap).setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (ProfileView.this.uid != Global.uid) {
                        ProfileView.this.openProfilePhotos();
                    } else if (ProfileView.this.fragment != null) {
                        ProfileView.this.fragment.showUpdatePhotoDlg(!ProfileView.this.profile.bigPhoto.endsWith(".gif"));
                    }
                }
            });
            if (this.uid == Global.uid) {
                this.headerView.findViewById(C0436R.id.profile_photo_wrap).setOnLongClickListener(new OnLongClickListener() {
                    public boolean onLongClick(View v) {
                        ProfileView.this.openProfilePhotos();
                        return true;
                    }
                });
            }
        }
    }

    public void onDetachedFromWindow() {
        if (this.currentReq != null) {
            this.currentReq.cancel();
        }
    }

    public void setUserPhoto(String photo) {
        if (this.profile != null) {
            this.profile.profile.photo = photo;
            this.profile.bigPhoto = photo;
            updateList();
            if (this.imgLoader != null) {
                this.imgLoader.updateImages();
            }
        }
    }

    public void openProfilePhotos() {
        new PhotosGet(this.uid, -6, 0, 500, true).setCallback(new PhotosGet.Callback() {
            public void success(int total, Vector<Photo> photos) {
                if (photos.size() == 0) {
                    Toast.makeText(ProfileView.this.getContext(), C0436R.string.no_photos, 0).show();
                    return;
                }
                ArrayList<Photo> ph = new ArrayList();
                ph.addAll(photos);
                Bundle args = new Bundle();
                if (ph.size() > 400) {
                    args.putBoolean("shared_list", true);
                    PhotoViewerFragment.sharedList = ph;
                } else {
                    args.putParcelableArrayList("list", ph);
                }
                if (total > photos.size()) {
                    args.putInt("aid", -6);
                    args.putInt("total", total);
                }
                Navigate.to("PhotoViewerFragment", args, (Activity) ProfileView.this.getContext(), true, -1, -1);
            }

            public void fail(int ecode, String emsg) {
                Toast.makeText(ProfileView.this.getContext(), C0436R.string.error, 0).show();
            }
        }).wrapProgress(getContext()).exec((View) this);
    }

    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (this.profile != null) {
            post(new Runnable() {
                public void run() {
                    ProfileView.this.updateCounters();
                }
            });
        }
    }

    protected ListAdapter createAdapter() {
        if (this.lAdapter == null) {
            this.lAdapter = new MergeAdapter();
            this.infoAdapter = new ProfileInfoAdapter();
            this.lAdapter.addAdapter(this.infoAdapter);
            this.lAdapter.addAdapter(super.createAdapter());
        }
        return this.lAdapter;
    }

    protected ListImageLoaderAdapter createImageLoaderAdapter() {
        MergeImageLoaderAdapter la = new MergeImageLoaderAdapter();
        la.addAdapter(new ProfileImagesAdapter());
        la.addAdapter(super.createImageLoaderAdapter());
        return la;
    }

    public void loadInitial() {
        this.headerItems.clear();
        this.headerItems.add(this.buttonsWrap);
        this.headerItems.add(this.countersWrap);
        this.headerItems.add(this.photoFeed);
        this.headerItems.add(this.selector);
        this.list.setVisibility(8);
        this.bigProgress.setVisibility(0);
        this.currentReq = new GetFullProfile(this.uid, 25).setCallback(new GetFullProfile.Callback() {

            /* renamed from: com.vkontakte.android.ProfileView.15.1 */
            class C04151 implements DialogInterface.OnClickListener {
                C04151() {
                }

                public void onClick(DialogInterface dialog, int which) {
                    ((Activity) ProfileView.this.getContext()).onBackPressed();
                }
            }

            /* renamed from: com.vkontakte.android.ProfileView.15.2 */
            class C04162 implements OnCancelListener {
                C04162() {
                }

                public void onCancel(DialogInterface dialog) {
                    ((Activity) ProfileView.this.getContext()).onBackPressed();
                }
            }

            public void success(ExtendedUserProfile profile, ArrayList<Photo> photos) {
                if (profile == null) {
                    new Builder(ProfileView.this.getContext()).setTitle(C0436R.string.error).setMessage(C0436R.string.page_not_found).setPositiveButton(C0436R.string.ok, new C04151()).setOnCancelListener(new C04162()).show();
                    return;
                }
                int intValue;
                ArrayList<UserProfile> users = new ArrayList();
                users.add(profile.profile);
                Cache.updatePeers(users, true);
                if (profile.bigPhoto.endsWith("gif") && ProfileView.this.uid != Global.uid) {
                    ProfileView.this.headerView.findViewById(C0436R.id.profile_photo_wrap).setEnabled(false);
                }
                ProfileView profileView = ProfileView.this;
                boolean z = !profile.showAllPosts && ProfileView.this.uid > 0;
                profileView.ownerOnly = z;
                View findViewById = ProfileView.this.selector.findViewById(C0436R.id.profile_wall_all_posts);
                if (ProfileView.this.ownerOnly) {
                    z = false;
                } else {
                    z = true;
                }
                findViewById.setSelected(z);
                ProfileView.this.selector.findViewById(C0436R.id.profile_wall_owner_posts).setSelected(ProfileView.this.ownerOnly);
                ProfileView.this.profile = profile;
                ProfileView.this.updateHeaderView();
                ProfileView.this.buildInfoItems();
                ProfileView.this.updateButtons();
                ProfileView.this.updateCounters();
                PhotoFeedView access$27 = ProfileView.this.photoFeed;
                int access$1 = ProfileView.this.uid;
                if (profile.counters.containsKey("photos")) {
                    intValue = ((Integer) profile.counters.get("photos")).intValue();
                } else {
                    intValue = 0;
                }
                access$27.init(photos, access$1, intValue);
                if (ProfileView.this.uid > 0) {
                    ((TextView) ProfileView.this.selector.findViewById(C0436R.id.profile_wall_owner_posts)).setText(ProfileView.this.getResources().getString(C0436R.string.wall_owners_posts, new Object[]{profile.firstNameGen}).toUpperCase());
                } else {
                    ((TextView) ProfileView.this.selector.findViewById(C0436R.id.profile_wall_owner_posts)).setText(ProfileView.this.getResources().getString(C0436R.string.wall_owners_posts, new Object[]{ProfileView.this.getResources().getString(C0436R.string.group_s)}).toUpperCase());
                }
                if (photos.size() == 0) {
                    ProfileView.this.headerItems.remove(ProfileView.this.photoFeed);
                }
                if (!profile.canSeeAllPosts || (ProfileView.this.uid < 0 && profile.groupType == 2)) {
                    ProfileView.this.selector.findViewById(C0436R.id.profile_wall_all_posts).setVisibility(4);
                    ProfileView.this.selector.findViewById(C0436R.id.profile_wall_owner_posts).setVisibility(4);
                }
                findViewById = ProfileView.this.selector.findViewById(C0436R.id.profile_wall_post_btn);
                if (profile.canPost) {
                    intValue = 0;
                } else {
                    intValue = 4;
                }
                findViewById.setVisibility(intValue);
                if (!(profile.canPost || profile.canSeeAllPosts) || ((!profile.canSeeAllPosts || profile.groupType == 2) && profile.canPost && ProfileView.this.uid < 0)) {
                    ProfileView.this.headerItems.remove(ProfileView.this.selector);
                }
                if (ProfileView.this.uid < 0 && profile.groupType == 2) {
                    ProfileView.this.headerItems.remove(ProfileView.this.selector);
                }
                if (((!profile.canSeeAllPosts || profile.groupType == 2) && profile.canPost && ProfileView.this.uid < 0) || ProfileView.this.uid == Global.uid) {
                    ProfileView.this.headerItems.remove(ProfileView.this.buttonsWrap);
                    if (ProfileView.this.uid == Global.uid) {
                        ProfileView.this.headerItems.add(ProfileView.this.headerItems.size() - 1, ProfileView.this.buttonsWrap);
                    } else {
                        ProfileView.this.headerItems.add(ProfileView.this.buttonsWrap);
                    }
                    ProfileView.this.buttonsWrap.setPadding(ProfileView.this.buttonsWrap.getPaddingLeft(), ProfileView.this.buttonsWrap.getPaddingTop(), ProfileView.this.buttonsWrap.getPaddingRight(), ProfileView.this.countersWrap.getPaddingBottom());
                    ProfileView.this.countersWrap.setPadding(ProfileView.this.countersWrap.getPaddingLeft(), 0, ProfileView.this.countersWrap.getPaddingRight(), ProfileView.this.countersWrap.getPaddingBottom());
                }
                if (ProfileView.this.infoItems.size() == 0) {
                    ProfileView.this.headerView.findViewById(C0436R.id.profile_expand).setVisibility(4);
                    ProfileView.this.headerView.findViewById(C0436R.id.profile_head_highlight).setEnabled(false);
                }
                if (profile.lastSeen < 0) {
                    if (profile.lastSeen == -1) {
                        ProfileView.this.emptyText.setText(C0436R.string.profile_inactive_banned);
                    } else {
                        ProfileView.this.emptyText.setText(C0436R.string.profile_inactive_deleted);
                    }
                    ProfileView.this.headerItems.add(ProfileView.this.emptyText);
                    ProfileView.this.onDataLoaded(new ArrayList(), false);
                    Global.showViewAnimated(ProfileView.this.list, true, PhotoView.THUMB_ANIM_DURATION);
                    ProfileView.this.list.setDraggingEnabled(false);
                    ProfileView.this.updateList();
                    return;
                }
                ProfileView.this.currentReq = null;
                ProfileView.this.loadData(true);
                ProfileView.this.updateList();
            }

            public void fail(int ecode, String emsg) {
                ProfileView.this.onError(ecode, emsg);
                ProfileView.this.currentReq = null;
            }
        }).exec((View) this);
    }

    public void loadData(boolean refresh) {
        int count = 10;
        if (!this.dataLoading) {
            this.dataLoading = true;
            if (this.lastUpdateTime > 0 || !((this.uid == 0 || this.uid == Global.uid) && UserWallCache.hasEntries(getContext()) && (System.currentTimeMillis() / 1000) - ((long) UserWallCache.getUpdateTime(getContext())) <= 86400)) {
                if (refresh) {
                    this.commentsFrom = "0";
                    this.offset = 0;
                }
                int i = this.uid;
                int i2 = this.offset;
                if (!this.preloading) {
                    count = 20;
                }
                this.currentReq = new WallGet(i, i2, count, this.ownerOnly, false).setCallback(new AnonymousClass16(refresh, count)).exec((View) this);
                return;
            }
            boolean z;
            if (refresh) {
                this.selector.findViewById(C0436R.id.profile_wall_post_btn).setVisibility(this.profile.canPost ? 0 : 4);
                this.selector.findViewById(C0436R.id.profile_wall_progress).setVisibility(8);
            }
            if (this.news.size() == 0) {
                Global.showViewAnimated(this.list, true, PhotoView.THUMB_ANIM_DURATION);
            }
            View findViewById = this.selector.findViewById(C0436R.id.profile_wall_all_posts);
            if (this.ownerOnly) {
                z = false;
            } else {
                z = true;
            }
            findViewById.setSelected(z);
            this.selector.findViewById(C0436R.id.profile_wall_owner_posts).setSelected(this.ownerOnly);
            ArrayList<NewsEntry> e = UserWallCache.get(getContext());
            this.headerItems.remove(this.postponedView);
            this.headerItems.remove(this.suggestsView);
            int postponedCount = getContext().getSharedPreferences(null, 0).getInt("postponed_count", 0);
            this.profile.postponedCount = postponedCount;
            if (postponedCount > 0) {
                ((TextView) this.postponedView.findViewById(C0436R.id.text)).setText(Global.langPlural(C0436R.array.postponed_posts, postponedCount, getResources()));
                this.headerItems.add(this.postponedView);
            }
            if (e.size() <= 10) {
                onDataLoaded(e, refresh);
            } else {
                onDataLoaded(e.subList(0, 10), refresh);
                for (int i3 = 10; i3 < e.size(); i3++) {
                    this.preloadedNews.add((NewsEntry) e.get(i3));
                }
            }
            this.lastUpdateTime = UserWallCache.getUpdateTime(getContext());
        }
    }

    public void updatePostsButtons(int postponed, int suggested) {
        ExtendedUserProfile extendedUserProfile = this.profile;
        extendedUserProfile.postponedCount += postponed;
        extendedUserProfile = this.profile;
        extendedUserProfile.suggestedCount += suggested;
        updatePostsButtons();
        updateList();
        if (this.uid == Global.uid) {
            getContext().getSharedPreferences(null, 0).edit().putInt("postponed_count", this.profile.postponedCount).commit();
        }
    }

    private void updatePostsButtons() {
        this.headerItems.remove(this.postponedView);
        this.headerItems.remove(this.suggestsView);
        if (this.profile.postponedCount > 0) {
            ((TextView) this.postponedView.findViewById(C0436R.id.text)).setText(Global.langPlural(C0436R.array.postponed_posts, this.profile.postponedCount, getResources()));
            this.headerItems.add(this.postponedView);
        }
        if (this.profile.suggestedCount > 0) {
            ((TextView) this.suggestsView.findViewById(C0436R.id.text)).setText(Global.langPlural(this.profile.canPost ? C0436R.array.suggested_posts : C0436R.array.suggested_posts_by_me, this.profile.suggestedCount, getResources()));
            this.headerItems.add(this.suggestsView);
        }
    }

    private void updateHeaderView() {
        SpannableStringBuilder bldr;
        Spannable sp;
        Drawable d;
        CharSequence name;
        if (this.uid > 0) {
            name = new StringBuilder(String.valueOf(this.profile.profile.firstName)).append(" ").append(this.profile.profile.lastName).toString();
            if (this.profile.verified) {
                bldr = new SpannableStringBuilder(name);
                sp = Factory.getInstance().newSpannable("F");
                d = getResources().getDrawable(C0436R.drawable.ic_verified);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                sp.setSpan(new ImageSpan(d, 1), 0, 1, 0);
                bldr.append("\u00a0");
                bldr.append(sp);
                name = bldr;
            }
            ((TextView) this.headerView.findViewById(C0436R.id.profile_name)).setText(name);
        } else {
            name = this.profile.profile.fullName;
            if (this.profile.verified) {
                bldr = new SpannableStringBuilder(name);
                sp = Factory.getInstance().newSpannable("F");
                d = getResources().getDrawable(C0436R.drawable.ic_verified);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                sp.setSpan(new ImageSpan(d, 1), 0, 1, 0);
                bldr.append("\u00a0");
                bldr.append(sp);
                name = bldr;
            }
            ((TextView) this.headerView.findViewById(C0436R.id.profile_name)).setText(name);
            ((TextView) this.headerView.findViewById(C0436R.id.profile_name)).setSingleLine();
            ((TextView) this.headerView.findViewById(C0436R.id.profile_name)).setEllipsize(TruncateAt.MARQUEE);
            ((TextView) this.headerView.findViewById(C0436R.id.profile_name)).setSelected(true);
            ((TextView) this.headerView.findViewById(C0436R.id.profile_name)).setHorizontalFadingEdgeEnabled(true);
            ((TextView) this.headerView.findViewById(C0436R.id.profile_name)).setFadingEdgeLength(Global.scale(10.0f));
        }
        ((TextView) this.headerView.findViewById(C0436R.id.profile_activity)).setText(this.profile.activity);
        if (this.uid > 0) {
            ArrayList<String> ss = new ArrayList();
            if (this.profile.city != null && this.profile.city.length() > 0) {
                ss.add(this.profile.city);
            }
            if (this.profile.bYear > 0) {
                Calendar c = Calendar.getInstance();
                int now = c.get(1);
                Calendar c2 = Calendar.getInstance();
                c2.set(5, this.profile.bDay);
                c2.set(2, this.profile.bMonth - 1);
                int years = now - this.profile.bYear;
                if (c2.after(c)) {
                    years--;
                }
                ss.add(Global.langPlural(C0436R.array.profile_age_years, years, getResources()));
            }
            this.infoLine = TextUtils.join(", ", ss);
        } else {
            this.infoLine = this.profile.infoLine;
        }
        if (this.profile.activity.length() == 0) {
            ((TextView) this.headerView.findViewById(C0436R.id.profile_activity)).setText(this.infoLine);
        }
        CharSequence online;
        if (this.profile.profile.online > 0) {
            online = getContext().getString(C0436R.string.online);
            if (this.profile.profile.online != 1) {
                bldr = new SpannableStringBuilder(online);
                sp = Factory.getInstance().newSpannable("F");
                d = getResources().getDrawable(C0436R.drawable.ic_left_online_mobile);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                sp.setSpan(new ImageSpan(d, 1), 0, 1, 0);
                bldr.append("\u00a0");
                bldr.append(sp);
                online = bldr;
            }
            ((TextView) this.headerView.findViewById(C0436R.id.profile_last_seen)).setText(online);
        } else if (this.profile.lastSeen > 0) {
            online = getResources().getString(this.profile.profile.f151f ? C0436R.string.last_seen_profile_f : C0436R.string.last_seen_profile_m, new Object[]{Global.langDateRelative(this.profile.lastSeen, getResources())});
            if (this.profile.lastSeenMobile) {
                bldr = new SpannableStringBuilder(online);
                sp = Factory.getInstance().newSpannable("F");
                d = getResources().getDrawable(C0436R.drawable.ic_left_online_mobile);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                sp.setSpan(new ImageSpan(d, 1), 0, 1, 0);
                bldr.append("\u00a0");
                bldr.append(sp);
                online = bldr;
            }
            ((TextView) this.headerView.findViewById(C0436R.id.profile_last_seen)).setText(online);
        } else {
            ((TextView) this.headerView.findViewById(C0436R.id.profile_last_seen)).setText(ACRAConstants.DEFAULT_STRING_VALUE);
        }
        ((TextView) this.headerView.findViewById(C0436R.id.profile_last_seen)).setSelected(true);
    }

    protected boolean canHideFromFeed() {
        return false;
    }

    public void updateList() {
        post(new Runnable() {
            public void run() {
                if (ProfileView.this.news.size() == 0) {
                    if (!ProfileView.this.headerItems.contains(ProfileView.this.emptyText)) {
                        ProfileView.this.headerItems.add(ProfileView.this.emptyText);
                        ProfileView.this.lAdapter.notifyDataSetChanged();
                    }
                } else if (ProfileView.this.headerItems.contains(ProfileView.this.emptyText)) {
                    ProfileView.this.headerItems.remove(ProfileView.this.emptyText);
                    ProfileView.this.lAdapter.notifyDataSetChanged();
                }
            }
        });
        super.updateList();
    }

    private void openAudioStatus() {
        Intent intent = new Intent(getContext(), AudioPlayerService.class);
        intent.putExtra("action", 2);
        intent.putExtra("act_uid", this.uid);
        intent.putExtra("list", new AudioFile[]{this.profile.audioStatus});
        getContext().startService(intent);
        intent = new Intent(getContext(), AudioPlayerService.class);
        intent.putExtra("action", 4);
        getContext().startService(intent);
    }

    private void buildInfoItems() {
        this.infoItems.clear();
        if (this.profile.activity.length() > 0 || this.uid == Global.uid) {
            ArrayList arrayList = this.infoItems;
            CharSequence string = this.profile.activity.length() > 0 ? this.profile.activity : getResources().getString(C0436R.string.change_status);
            String str = (this.uid == Global.uid || this.profile.audioStatus != null) ? "status" : null;
            arrayList.add(new InfoItem(3, null, string, null, str));
        }
        int docs;
        if (this.uid > 0) {
            Iterator it;
            if (this.profile.bDay > 0) {
                String bd;
                if (this.profile.bYear > 0) {
                    bd = String.format("%d %s %d", new Object[]{Integer.valueOf(this.profile.bDay), getResources().getStringArray(C0436R.array.months_full)[this.profile.bMonth - 1], Integer.valueOf(this.profile.bYear)});
                } else {
                    bd = String.format("%d %s", new Object[]{Integer.valueOf(this.profile.bDay), getResources().getStringArray(C0436R.array.months_full)[this.profile.bMonth - 1]});
                }
                this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_bdate), bd, null, "bdate"));
            }
            if (this.profile.hometown != null) {
                this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_hometown), this.profile.hometown, null, null));
            }
            if (this.profile.relation > 0) {
                String rel;
                if (this.profile.relationPartner > 0) {
                    rel = String.format(getResources().getStringArray(this.profile.profile.f151f ? C0436R.array.profile_relation_pf : C0436R.array.profile_relation_pm)[this.profile.relation - 1], new Object[]{this.profile.relationPartnerName});
                } else {
                    rel = getResources().getStringArray(this.profile.profile.f151f ? C0436R.array.profile_relation_f : C0436R.array.profile_relation_m)[this.profile.relation - 1];
                }
                this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_relation), rel, null, this.profile.relationPartner > 0 ? "relation" : null));
            }
            if (this.profile.langs != null) {
                this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.personal_langs), this.profile.langs, null, null));
            }
            if (this.profile.relatives.size() > 0) {
                this.infoItems.add(new InfoItem(2, getResources().getString(C0436R.string.profile_info_relatives), null, null, null));
                it = this.profile.relatives.iterator();
                while (it.hasNext()) {
                    Relative rel2 = (Relative) it.next();
                    int typeRes = 0;
                    switch (rel2.type) {
                        case ValidationActivity.VRESULT_NONE /*0*/:
                            typeRes = rel2.user.f151f ? C0436R.string.profile_relative_parent_f : C0436R.string.profile_relative_parent_m;
                            break;
                        case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                            typeRes = rel2.user.f151f ? C0436R.string.profile_relative_sibling_f : C0436R.string.profile_relative_sibling_m;
                            break;
                        case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                            typeRes = rel2.user.f151f ? C0436R.string.profile_relative_child_f : C0436R.string.profile_relative_child_m;
                            break;
                        case Group.ADMIN_LEVEL_ADMIN /*3*/:
                            typeRes = rel2.user.f151f ? C0436R.string.profile_relative_grandparent_f : C0436R.string.profile_relative_grandparent_m;
                            break;
                        case UserListView.TYPE_FAVE /*4*/:
                            typeRes = rel2.user.f151f ? C0436R.string.profile_relative_grandchild_f : C0436R.string.profile_relative_grandchild_m;
                            break;
                        default:
                            break;
                    }
                    this.infoItems.add(new InfoItem(1, getResources().getString(typeRes), new StringBuilder(String.valueOf(rel2.user.firstName)).append(" ").append(rel2.user.lastName).toString(), rel2.user.photo, "relative" + rel2.user.uid));
                }
            }
            if (!(this.profile.city == null && this.profile.mobilePhone == null && this.profile.homePhone == null && this.profile.skype == null && this.profile.twitter == null && this.profile.facebookName == null && this.profile.livejournal == null && this.profile.website == null)) {
                this.infoItems.add(new InfoItem(2, getResources().getString(C0436R.string.profile_info_contacts), null, null, null));
                if (this.profile.city != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_city), this.profile.city, null, null));
                }
                if (this.profile.mobilePhone != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_mphone), this.profile.mobilePhone, null, "mobilePhone"));
                }
                if (this.profile.homePhone != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_hphone), this.profile.homePhone, null, "homePhone"));
                }
                if (this.profile.skype != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_skype), this.profile.skype, null, "skype"));
                }
                if (this.profile.instagram != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_instagram), this.profile.instagram, null, "instagram"));
                }
                if (this.profile.twitter != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_twitter), this.profile.twitter, null, "twitter"));
                }
                if (this.profile.facebookName != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_facebook), this.profile.facebookName, null, "facebook"));
                }
                if (this.profile.website != null && this.profile.website.length() > 0) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.group_site), this.profile.website, null, "website"));
                }
                if (this.profile.livejournal != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_livejournal), this.profile.livejournal, null, "livejournal"));
                }
            }
            if (this.profile.schools.size() > 0 || this.profile.universities.size() > 0) {
                String title;
                String subtitle;
                this.infoItems.add(new InfoItem(2, getResources().getString(C0436R.string.profile_info_education), null, null, null));
                it = this.profile.schools.iterator();
                while (it.hasNext()) {
                    School s = (School) it.next();
                    if (!(s == null || s.name == null)) {
                        title = s.name;
                        if (s.graduation > 0) {
                            title = new StringBuilder(String.valueOf(title)).append(" '").append(String.format("%02d", new Object[]{Integer.valueOf(s.graduation % 100)})).toString();
                        }
                        subtitle = s.city != null ? s.city : ACRAConstants.DEFAULT_STRING_VALUE;
                        if ((s.from > 0 || s.to > 0) && subtitle.length() > 0) {
                            subtitle = new StringBuilder(String.valueOf(subtitle)).append(", ").toString();
                        }
                        if (s.from > 0) {
                            subtitle = new StringBuilder(String.valueOf(subtitle)).append(s.from).toString();
                        }
                        if (s.from > 0 && s.to > 0) {
                            subtitle = new StringBuilder(String.valueOf(subtitle)).append("-").toString();
                        }
                        if (s.to > 0) {
                            subtitle = new StringBuilder(String.valueOf(subtitle)).append(s.to).toString();
                        }
                        if (s.className != null && s.className.length() > 0) {
                            subtitle = new StringBuilder(String.valueOf(subtitle)).append(" (").append(s.className).append(")").toString();
                        }
                        if (subtitle != null) {
                            subtitle = subtitle.trim();
                        }
                        if (s.speciality != null) {
                            if (subtitle.length() > 0) {
                                subtitle = new StringBuilder(String.valueOf(subtitle)).append("\n").toString();
                            }
                            subtitle = new StringBuilder(String.valueOf(subtitle)).append(s.speciality).toString();
                        }
                        this.infoItems.add(new InfoItem(0, s.type, title, subtitle, null));
                    }
                }
                it = this.profile.universities.iterator();
                while (it.hasNext()) {
                    University u = (University) it.next();
                    title = u.name;
                    if (u.graduation > 0) {
                        title = new StringBuilder(String.valueOf(title)).append(" '").append(String.format("%02d", new Object[]{Integer.valueOf(u.graduation % 100)})).toString();
                    }
                    subtitle = ACRAConstants.DEFAULT_STRING_VALUE;
                    if (u.faculty != null) {
                        subtitle = new StringBuilder(String.valueOf(subtitle)).append(u.faculty).toString();
                    }
                    if (u.chair != null) {
                        if (subtitle.length() > 0) {
                            subtitle = new StringBuilder(String.valueOf(subtitle)).append("\n").toString();
                        }
                        subtitle = new StringBuilder(String.valueOf(subtitle)).append(u.chair).toString();
                    }
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_university), title, subtitle, null));
                }
            }
            if ((this.profile.religion != null && this.profile.religion.length() > 0) || ((this.profile.inspiredBy != null && this.profile.inspiredBy.length() > 0) || this.profile.political > 0 || this.profile.lifeMain > 0 || this.profile.peopleMain > 0 || this.profile.smoking > 0 || this.profile.alcohol > 0)) {
                String[] opts;
                this.infoItems.add(new InfoItem(2, getResources().getString(C0436R.string.personal), null, null, null));
                if (this.profile.political > 0) {
                    opts = getResources().getStringArray(C0436R.array.personal_politics_options);
                    if (this.profile.political - 1 < opts.length) {
                        this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.personal_politics), opts[this.profile.political - 1], null, null));
                    }
                }
                if (this.profile.religion != null && this.profile.religion.length() > 0) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.personal_religion), this.profile.religion, null, null));
                }
                if (this.profile.lifeMain > 0) {
                    opts = getResources().getStringArray(C0436R.array.personal_life_options);
                    if (this.profile.lifeMain - 1 < opts.length) {
                        this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.personal_life), opts[this.profile.lifeMain - 1], null, null));
                    }
                }
                if (this.profile.peopleMain > 0) {
                    opts = getResources().getStringArray(C0436R.array.personal_people_options);
                    if (this.profile.peopleMain - 1 < opts.length) {
                        this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.personal_people), opts[this.profile.peopleMain - 1], null, null));
                    }
                }
                if (this.profile.smoking > 0) {
                    opts = getResources().getStringArray(C0436R.array.personal_views_options);
                    if (this.profile.smoking - 1 < opts.length) {
                        this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.personal_smoking), opts[this.profile.smoking - 1], null, null));
                    }
                }
                if (this.profile.alcohol > 0) {
                    opts = getResources().getStringArray(C0436R.array.personal_views_options);
                    if (this.profile.alcohol - 1 < opts.length) {
                        this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.personal_alcohol), opts[this.profile.alcohol - 1], null, null));
                    }
                }
                if (this.profile.inspiredBy != null && this.profile.inspiredBy.length() > 0) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.personal_inspiration), this.profile.inspiredBy, null, null));
                }
            }
            if (!(this.profile.activities == null && this.profile.interests == null && this.profile.music == null && this.profile.movies == null && this.profile.tv == null && this.profile.books == null && this.profile.games == null && this.profile.quotations == null && this.profile.about == null)) {
                this.infoItems.add(new InfoItem(2, getResources().getString(C0436R.string.profile_info_personal), null, null, null));
                if (this.profile.activities != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_occupation), this.profile.activities, null, null));
                }
                if (this.profile.interests != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_interests), this.profile.interests, null, null));
                }
                if (this.profile.music != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_music), this.profile.music, null, null));
                }
                if (this.profile.movies != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_movies), this.profile.movies, null, null));
                }
                if (this.profile.tv != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_tv), this.profile.tv, null, null));
                }
                if (this.profile.books != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_books), this.profile.books, null, null));
                }
                if (this.profile.games != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_games), this.profile.games, null, null));
                }
                if (this.profile.quotations != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_quotations), this.profile.quotations, null, null));
                }
                if (this.profile.about != null) {
                    this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.profile_about), this.profile.about, null, null));
                }
            }
            int subscriptions = (this.profile.counters.containsKey("pages") ? ((Integer) this.profile.counters.get("pages")).intValue() : 0) + (this.profile.counters.containsKey("subscriptions") ? ((Integer) this.profile.counters.get("subscriptions")).intValue() : 0);
            int groups = this.profile.counters.containsKey("groups") ? ((Integer) this.profile.counters.get("groups")).intValue() : 0;
            docs = this.profile.counters.containsKey("docs") ? ((Integer) this.profile.counters.get("docs")).intValue() : 0;
            if (subscriptions > 0 || groups > 0 || docs > 0) {
                this.infoItems.add(new InfoItem(2, getResources().getString(C0436R.string.sett_other), null, null, null));
                if (subscriptions > 0) {
                    this.infoItems.add(new InfoItem(4, getResources().getString(C0436R.string.profile_subscriptions), new StringBuilder(String.valueOf(subscriptions)).toString(), null, "subscriptions"));
                }
                if (groups > 0) {
                    this.infoItems.add(new InfoItem(4, getResources().getString(C0436R.string.groups), new StringBuilder(String.valueOf(groups)).toString(), null, "groups"));
                }
                if (docs > 0) {
                    this.infoItems.add(new InfoItem(4, getResources().getString(C0436R.string.docs), new StringBuilder(String.valueOf(docs)).toString(), null, "docs"));
                    return;
                }
                return;
            }
            return;
        }
        if (this.profile.about != null && this.profile.about.length() > 0) {
            this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.group_descr), NewsEntry.stripUnderlines((Spannable) Html.fromHtml(Global.replaceMentions(this.profile.about).replace("\n", "<br/>"))), null, null));
        }
        if (this.profile.website != null && this.profile.website.length() > 0) {
            this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.group_site), this.profile.website, null, "website"));
        }
        if (this.profile.eventStartTime > 0) {
            this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.group_start_date), Global.langDate(getResources(), this.profile.eventStartTime), null, "time"));
        }
        if (this.profile.eventEndTime > 0) {
            this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.group_end_date), Global.langDate(getResources(), this.profile.eventEndTime), null, "time"));
        }
        if (this.profile.city != null && this.profile.city.length() > 0) {
            this.infoItems.add(new InfoItem(0, getResources().getString(C0436R.string.group_place), this.profile.city, null, this.profile.lat != -9000.0d ? "place" : null));
        }
        if (this.profile.mobilePhone != null && this.profile.mobilePhone.length() > 0) {
            this.infoItems.add(new InfoItem(0, null, this.profile.mobilePhone, null, "wiki"));
        }
        docs = this.profile.counters.containsKey("docs") ? ((Integer) this.profile.counters.get("docs")).intValue() : 0;
        if (docs > 0) {
            this.infoItems.add(new InfoItem(2, getResources().getString(C0436R.string.sett_other), null, null, null));
            if (docs > 0) {
                this.infoItems.add(new InfoItem(4, getResources().getString(C0436R.string.docs), new StringBuilder(String.valueOf(docs)).toString(), null, "docs"));
            }
        }
    }

    private void toggleExtendedInfo() {
        boolean z;
        int i = -180;
        if (this.showExtended) {
            z = false;
        } else {
            z = true;
        }
        this.showExtended = z;
        View arrow = this.headerView.findViewById(C0436R.id.profile_expand);
        int i2;
        if (VERSION.SDK_INT >= 11) {
            String str = "rotation";
            float[] fArr = new float[2];
            if (this.showExtended) {
                i2 = 0;
            } else {
                i2 = -180;
            }
            fArr[0] = (float) i2;
            if (!this.showExtended) {
                i = 0;
            }
            fArr[1] = (float) i;
            ObjectAnimator.ofFloat(arrow, str, fArr).setDuration(300).start();
        } else {
            if (this.showExtended) {
                i2 = 0;
            } else {
                i2 = -180;
            }
            float f = (float) i2;
            if (!this.showExtended) {
                i = 0;
            }
            RotateAnimation anim = new RotateAnimation(f, (float) i, 1, 0.5f, 1, 0.5f);
            anim.setFillAfter(true);
            anim.setDuration(300);
            arrow.startAnimation(anim);
        }
        this.infoAdapter.notifyDataSetChanged();
        this.imgLoader.updateImages();
    }

    private void updateButtons() {
        this.buttons.removeAllViews();
        if (this.profile.friendStatus != -1) {
            Button firstButton;
            LayoutParams lp;
            Button secondButton;
            if (this.buttonsWrap.getChildCount() > 1) {
                this.buttonsWrap.removeAllViews();
                this.buttonsWrap.addView(this.buttons);
            }
            if (this.uid > 0) {
                firstButton = null;
                String friendStatus = null;
                if (this.uid == 0 || this.uid == Global.uid) {
                    firstButton = (Button) inflate(getContext(), C0436R.layout.blue_btn, null);
                    firstButton.setText(C0436R.string.profile_add_post);
                    firstButton.setTag("post");
                    ImageView secondButton2 = new ImageView(getContext());
                    secondButton2.setImageResource(C0436R.drawable.ic_profile_add_photo);
                    secondButton2.setBackgroundResource(C0436R.drawable.btn_blue);
                    secondButton2.setScaleType(ScaleType.CENTER);
                    secondButton2.setTag("photo");
                    lp = new LayoutParams(Global.scale(56.0f), Global.scale(42.0f));
                    if (firstButton != null) {
                        lp.leftMargin = Global.scale(5.0f);
                    }
                    this.buttons.addView(secondButton2, lp);
                    secondButton2.setOnClickListener(this.btnClickListener);
                } else if (this.profile.friendStatus == 0) {
                    firstButton = (Button) inflate(getContext(), C0436R.layout.blue_btn, null);
                    firstButton.setText(C0436R.string.profile_add_friend);
                    firstButton.setTag("add");
                } else if (this.profile.friendStatus == 1) {
                    firstButton = (Button) inflate(getContext(), C0436R.layout.blue_btn, null);
                    firstButton.setText(C0436R.string.profile_friend_cancel);
                    firstButton.setTag("cancel");
                    friendStatus = getResources().getString(C0436R.string.friend_status_req_sent, new Object[]{this.profile.firstNameDat});
                } else if (this.profile.friendStatus == 2) {
                    firstButton = (Button) inflate(getContext(), C0436R.layout.blue_btn, null);
                    firstButton.setText(C0436R.string.profile_friend_accept);
                    firstButton.setTag("accept");
                    friendStatus = getResources().getString(this.profile.profile.f151f ? C0436R.string.friend_status_req_recv_f : C0436R.string.friend_status_req_recv_m, new Object[]{this.profile.profile.firstName});
                } else if (this.profile.friendStatus == 3) {
                    friendStatus = getResources().getString(C0436R.string.friend_status_friend, new Object[]{this.profile.profile.firstName});
                }
                if (firstButton != null) {
                    this.buttons.addView(firstButton, 0, new LayoutParams(-1, Global.scale(42.0f), 1.0f));
                    firstButton.setOnClickListener(this.btnClickListener);
                }
                if (!(!this.profile.canWrite || this.uid == 0 || this.uid == Global.uid)) {
                    secondButton = (Button) inflate(getContext(), C0436R.layout.blue_btn, null);
                    secondButton.setText(C0436R.string.profile_write_msg);
                    secondButton.setTag(LongPollService.EXTRA_MESSAGE);
                    secondButton.setSingleLine();
                    secondButton.setEllipsize(TruncateAt.END);
                    lp = new LayoutParams(-1, Global.scale(42.0f), 1.0f);
                    if (firstButton != null) {
                        lp.leftMargin = Global.scale(5.0f);
                    }
                    this.buttons.addView(secondButton, lp);
                    secondButton.setOnClickListener(this.btnClickListener);
                }
                if (friendStatus != null) {
                    TextView fs = new TextView(getContext());
                    fs.setText(friendStatus);
                    fs.setTextColor(-4276288);
                    int pad = Global.scale(5.0f);
                    fs.setPadding(pad, pad, pad, 0);
                    fs.setTextSize(15.0f);
                    fs.setGravity(17);
                    this.buttonsWrap.addView(fs, new LayoutParams(-1, -2));
                }
            }
            if (this.uid < 0) {
                if ((!this.profile.canSeeAllPosts || (this.profile.groupType == 2 && this.profile.friendStatus == 1)) && this.profile.canPost) {
                    firstButton = (Button) inflate(getContext(), C0436R.layout.blue_btn, null);
                    firstButton.setText(this.profile.canPost ? C0436R.string.profile_add_post : C0436R.string.profile_suggest_post);
                    firstButton.setTag("post");
                    firstButton.setOnClickListener(this.btnClickListener);
                    this.buttons.addView(firstButton, new LayoutParams(-1, Global.scale(42.0f), 1.0f));
                }
                if (this.profile.friendStatus == 0 || this.profile.friendStatus == 2) {
                    if (this.profile.groupType == 0 && (this.profile.groupAccess != 2 || this.profile.friendStatus == 2)) {
                        firstButton = (Button) inflate(getContext(), C0436R.layout.blue_btn, null);
                        int i = (this.profile.groupAccess == 0 || this.profile.friendStatus == 2) ? C0436R.string.join_group : C0436R.string.join_group_closed;
                        firstButton.setText(i);
                        firstButton.setTag("join");
                        firstButton.setOnClickListener(this.btnClickListener);
                        this.buttons.addView(firstButton, new LayoutParams(-1, Global.scale(42.0f), 1.0f));
                    } else if (this.profile.groupType == 1 && (this.profile.groupAccess == 0 || this.profile.friendStatus == 2)) {
                        firstButton = (Button) inflate(getContext(), C0436R.layout.blue_btn, null);
                        firstButton.setText(C0436R.string.group_event_join);
                        firstButton.setTag("join");
                        firstButton.setOnClickListener(this.btnClickListener);
                        this.buttons.addView(firstButton, new LayoutParams(-1, Global.scale(42.0f), 1.0f));
                        secondButton = (Button) inflate(getContext(), C0436R.layout.blue_btn, null);
                        secondButton.setText(C0436R.string.group_event_join_unsure);
                        secondButton.setTag("join_unsure");
                        secondButton.setOnClickListener(this.btnClickListener);
                        lp = new LayoutParams(-1, Global.scale(42.0f), 1.0f);
                        if (firstButton != null) {
                            lp.leftMargin = Global.scale(5.0f);
                        }
                        this.buttons.addView(secondButton, lp);
                    } else if (this.profile.groupType == 2) {
                        firstButton = (Button) inflate(getContext(), C0436R.layout.blue_btn, null);
                        firstButton.setText(C0436R.string.join_page);
                        firstButton.setTag("join");
                        firstButton.setOnClickListener(this.btnClickListener);
                        this.buttons.addView(firstButton, new LayoutParams(-1, Global.scale(42.0f), 1.0f));
                    }
                } else if (this.profile.friendStatus == 3) {
                    firstButton = (Button) inflate(getContext(), C0436R.layout.blue_btn, null);
                    firstButton.setText(C0436R.string.profile_friend_cancel);
                    firstButton.setTag("leave");
                    firstButton.setOnClickListener(this.btnClickListener);
                    this.buttons.addView(firstButton, new LayoutParams(-1, Global.scale(42.0f), 1.0f));
                }
            }
            if (getContext() instanceof SherlockActivity) {
                ((SherlockActivity) getContext()).invalidateOptionsMenu();
            } else if (getContext() instanceof SherlockFragmentActivity) {
                ((SherlockFragmentActivity) getContext()).invalidateOptionsMenu();
            }
        }
    }

    private void updateCounters() {
        String[] cntrs;
        int[] titleRes;
        this.countersWrap.removeAllViews();
        if (this.uid == 0 || this.uid == Global.uid) {
            cntrs = new String[]{"friends", "followers", "groups", "photos", "videos", "audios"};
            titleRes = new int[]{C0436R.array.profile_friends, C0436R.array.profile_followers, C0436R.array.profile_groups, C0436R.array.profile_photos, C0436R.array.profile_videos, C0436R.array.profile_audios};
        } else if (this.uid < 0) {
            cntrs = new String[]{"members", "topics", "docs", "photos", "videos", "audios"};
            titleRes = new int[]{C0436R.array.profile_members, C0436R.array.profile_topics, C0436R.array.profile_docs, C0436R.array.profile_photos, C0436R.array.profile_videos, C0436R.array.profile_audios};
        } else {
            cntrs = new String[]{"friends", "mutual_friends", "followers", "photos", "videos", "audios", "groups"};
            titleRes = new int[]{C0436R.array.profile_friends, C0436R.array.profile_mutual_friends, C0436R.array.profile_followers, C0436R.array.profile_photos, C0436R.array.profile_videos, C0436R.array.profile_audios, C0436R.array.profile_groups};
        }
        int n = 0;
        boolean twoRow = (getResources().getConfiguration().screenLayout & 15) < 3 || this.list.getWidth() <= this.list.getHeight();
        LinearLayout linearLayout = this.countersWrap;
        if (twoRow) {
            this.countersWrap.setOrientation(1);
            linearLayout = new LinearLayout(getContext());
            linearLayout.setOrientation(0);
            LayoutParams lp = new LayoutParams(-1, -2);
            lp.bottomMargin = Global.scale(8.0f);
            this.countersWrap.addView(linearLayout, lp);
        } else {
            this.countersWrap.setOrientation(0);
        }
        int i = 0;
        while (n < 6 && i < cntrs.length) {
            if (this.profile.counters.containsKey(cntrs[i]) && ((Integer) this.profile.counters.get(cntrs[i])).intValue() > 0) {
                int i2;
                CharSequence charSequence;
                View cntr = inflate(getContext(), C0436R.layout.profile_counter, null);
                ((TextView) cntr.findViewById(C0436R.id.profile_counter_title)).setTypeface(Fonts.getRobotoLight());
                int c = ((Integer) this.profile.counters.get(cntrs[i])).intValue();
                TextView textView = (TextView) cntr.findViewById(C0436R.id.profile_counter_title);
                int i3 = titleRes[i];
                if (c > 99999) {
                    i2 = 7;
                } else {
                    i2 = c;
                }
                textView.setText(Global.langPlural(i3, i2, getResources()));
                textView = (TextView) cntr.findViewById(C0436R.id.profile_counter_value);
                if (c > 99999) {
                    charSequence = (c / LocationStatusCodes.GEOFENCE_NOT_AVAILABLE) + "K";
                } else {
                    charSequence = new StringBuilder(String.valueOf(c)).toString();
                }
                textView.setText(charSequence);
                lp = new LayoutParams(-1, Global.scale(BitmapDescriptorFactory.HUE_YELLOW), 1.0f);
                if (n > 0 && !(twoRow && n == 3)) {
                    lp.leftMargin = Global.scale(8.0f);
                }
                if (twoRow && n == 3) {
                    linearLayout = new LinearLayout(getContext());
                    linearLayout.setOrientation(0);
                    this.countersWrap.addView(linearLayout);
                }
                cntr.setTag(cntrs[i]);
                cntr.setOnClickListener(this.counterClickListener);
                linearLayout.addView(cntr, lp);
                n++;
            }
            i++;
        }
        if (n % 3 != 0) {
            int nn = 3 - (n % 3);
            i = 0;
            while (i < nn) {
                lp = new LayoutParams(-1, Global.scale(BitmapDescriptorFactory.HUE_YELLOW), 1.0f);
                if (i > 0 && !(twoRow && i == 3)) {
                    lp.leftMargin = Global.scale(8.0f);
                }
                linearLayout.addView(new View(getContext()), lp);
                i++;
            }
        }
    }

    private void switchOwnerOnly(boolean owner) {
        boolean z = true;
        if (owner != this.ownerOnly && !this.dataLoading) {
            this.ownerOnly = owner;
            loadData(true);
            View findViewById = this.selector.findViewById(C0436R.id.profile_wall_all_posts);
            if (owner) {
                z = false;
            }
            findViewById.setSelected(z);
            this.selector.findViewById(C0436R.id.profile_wall_owner_posts).setSelected(owner);
            this.selector.findViewById(C0436R.id.profile_wall_post_btn).setVisibility(4);
            this.selector.findViewById(C0436R.id.profile_wall_progress).setVisibility(0);
        }
    }

    private void showNewPost() {
        Intent intent = new Intent(getContext(), NewPostActivity.class);
        intent.putExtra("uid", this.uid);
        if (this.uid < 0) {
            intent.putExtra("group_title", this.profile.profile.fullName);
            intent.putExtra("group_photo", this.profile.profile.photo);
            if (this.profile.groupType == 2 || !this.profile.canSeeAllPosts) {
                intent.putExtra("public", true);
            }
        }
        if (!this.profile.canPost) {
            intent.putExtra("suggest", true);
        }
        getContext().startActivity(intent);
    }

    private void showStatusEditDlg(CharSequence text) {
        EditText ed = new EditText(getContext());
        ed.setLines(4);
        ed.setGravity(51);
        ed.setText(text);
        ed.setSelection(text.length());
        AlertDialog dlg = new Builder(getContext()).setTitle(C0436R.string.status).setView(ed).setPositiveButton(C0436R.string.save, new AnonymousClass18(ed)).setNegativeButton(C0436R.string.cancel, null).create();
        dlg.setOnShowListener(new AnonymousClass19(ed));
        dlg.show();
    }

    private void setStatus(String status) {
        new StatusSet(status).setCallback(new AnonymousClass20(status)).wrapProgress(getContext()).exec((View) this);
    }

    private void addFriend() {
        View alertView = inflate(getContext(), C0436R.layout.add_friend_alert, null);
        ((TextView) alertView.findViewById(C0436R.id.add_friend_text)).setText(getResources().getString(this.profile.profile.f151f ? C0436R.string.add_friend_explain_f : C0436R.string.add_friend_explain_m, new Object[]{new StringBuilder(String.valueOf(this.profile.profile.firstName)).append(" ").append(this.profile.profile.lastName).toString()}));
        new Builder(getContext()).setTitle(C0436R.string.profile_add_friend).setView(alertView).setPositiveButton(C0436R.string.ok, new AnonymousClass21(alertView)).setNegativeButton(C0436R.string.cancel, null).show();
    }

    private void doAddFriend(String msg) {
        new FriendsAdd(this.uid, msg).setCallback(new FriendsAdd.Callback() {
            public void success(int uid, int result) {
                String msg = null;
                if (result == 1) {
                    msg = ProfileView.this.getResources().getString(ProfileView.this.profile.profile.f151f ? C0436R.string.add_friend_sent_f : C0436R.string.add_friend_sent_m, new Object[]{ProfileView.this.profile.profile.firstName});
                    ProfileView.this.profile.friendStatus = 1;
                }
                if (result == 2) {
                    msg = ProfileView.this.getResources().getString(C0436R.string.add_friend_accepted);
                    ProfileView.this.profile.friendStatus = 3;
                    Friends.add(ProfileView.this.profile.profile);
                }
                if (result == 4) {
                    msg = ProfileView.this.getResources().getString(C0436R.string.add_friend_already_sent);
                    ProfileView.this.profile.friendStatus = 1;
                }
                if (msg != null) {
                    Toast.makeText(ProfileView.this.getContext(), msg, 1).show();
                }
                ProfileView.this.updateButtons();
            }

            public void fail(int ecode, String emsg) {
                new Builder(ProfileView.this.getContext()).setTitle(C0436R.string.error).setMessage(C0436R.string.err_text).setPositiveButton(C0436R.string.ok, null).show();
            }
        }).wrapProgress(getContext()).exec((View) this);
    }

    private void joinGroup(boolean sure) {
        new GroupsJoin(-this.uid, !sure).setCallback(new GroupsJoin.Callback() {
            public void success() {
                ProfileView.this.loadInitial();
                ProfileView.this.updateButtons();
            }

            public void fail(int ecode, String emsg) {
                new Builder(ProfileView.this.getContext()).setTitle(C0436R.string.error).setMessage(ecode == 15 ? C0436R.string.page_blacklist : C0436R.string.err_text).setPositiveButton(C0436R.string.ok, null).show();
            }
        }).wrapProgress(getContext()).exec((View) this);
    }

    private void removeFriend() {
        new Builder(getContext()).setTitle(C0436R.string.delete_friend).setMessage(getResources().getString(C0436R.string.delete_friend_confirm, new Object[]{new StringBuilder(String.valueOf(this.profile.firstNameAcc)).append(" ").append(this.profile.lastNameAcc).toString()})).setPositiveButton(C0436R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ProfileView.this.doRemoveFriend();
            }
        }).setNegativeButton(C0436R.string.no, null).show();
    }

    private void doRemoveFriend() {
        new FriendsDelete(this.uid).setCallback(new FriendsDelete.Callback() {
            public void success(int uid, int result) {
                if (ProfileView.this.profile.friendStatus == 3) {
                    ProfileView.this.profile.friendStatus = 2;
                    Toast.makeText(ProfileView.this.getContext(), ProfileView.this.getResources().getString(ProfileView.this.profile.profile.f151f ? C0436R.string.friend_deleted_f : C0436R.string.friend_deleted_m, new Object[]{new StringBuilder(String.valueOf(ProfileView.this.profile.profile.firstName)).append(" ").append(ProfileView.this.profile.profile.lastName).toString()}), 1).show();
                }
                if (ProfileView.this.profile.friendStatus == 1) {
                    ProfileView.this.profile.friendStatus = 0;
                    Toast.makeText(ProfileView.this.getContext(), ProfileView.this.getResources().getString(C0436R.string.friend_request_canceled, new Object[]{new StringBuilder(String.valueOf(ProfileView.this.profile.profile.firstName)).append(" ").append(ProfileView.this.profile.profile.lastName).toString()}), 1).show();
                }
                ProfileView.this.updateButtons();
                Friends.remove(uid);
            }

            public void fail(int ecode, String emsg) {
                new Builder(ProfileView.this.getContext()).setTitle(C0436R.string.error).setMessage(C0436R.string.err_text).setPositiveButton(C0436R.string.ok, null).show();
            }
        }).wrapProgress(getContext()).exec((View) this);
    }

    private void leaveGroup() {
        if (this.profile.groupAccess == 0 || this.profile.friendStatus == 3) {
            doLeaveGroup();
        } else {
            new Builder(getContext()).setTitle(C0436R.string.leave_group).setMessage(C0436R.string.leave_closed_group_confirm).setPositiveButton(C0436R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    ProfileView.this.doLeaveGroup();
                }
            }).setNegativeButton(C0436R.string.no, null).show();
        }
    }

    private void doLeaveGroup() {
        new GroupsLeave(-this.uid).setCallback(new GroupsLeave.Callback() {
            public void success() {
                ProfileView.this.profile.friendStatus = 0;
                ProfileView.this.loadInitial();
            }

            public void fail(int ecode, String emsg) {
                new Builder(ProfileView.this.getContext()).setTitle(C0436R.string.error).setMessage(C0436R.string.err_text).setPositiveButton(C0436R.string.ok, null).show();
            }
        }).wrapProgress(getContext()).exec((View) this);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        boolean z = true;
        if (this.profile != null) {
            boolean z2;
            inflater.inflate(C0436R.menu.profile, menu);
            menu.findItem(C0436R.id.edit_profile).setVisible(this.uid == Global.uid);
            MenuItem findItem = menu.findItem(C0436R.id.suggest_post);
            if (this.uid >= 0 || this.profile.groupType != 2 || this.profile.canPost) {
                z2 = false;
            } else {
                z2 = true;
            }
            findItem.setVisible(z2);
            MenuItem findItem2;
            if (this.uid > 0) {
                int i;
                menu.findItem(C0436R.id.leave_group).setVisible(false);
                findItem2 = menu.findItem(C0436R.id.remove_friend);
                if (this.profile.friendStatus != 3) {
                    z = false;
                }
                findItem2.setVisible(z);
                if (this.uid == Global.uid) {
                    menu.findItem(C0436R.id.block).setVisible(false);
                }
                MenuItem findItem3 = menu.findItem(C0436R.id.block);
                if (this.profile.blacklisted) {
                    i = C0436R.string.unblock_user;
                } else {
                    i = C0436R.string.block_user;
                }
                findItem3.setTitle(i);
                return;
            }
            menu.findItem(C0436R.id.remove_friend).setVisible(false);
            findItem2 = menu.findItem(C0436R.id.leave_group);
            if (this.profile.friendStatus != 1) {
                z = false;
            }
            findItem2.setVisible(z);
            menu.findItem(C0436R.id.block).setVisible(false);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case C0436R.id.copy_link:
                copyLink();
                break;
            case C0436R.id.suggest_post:
                showNewPost();
                break;
            case C0436R.id.edit_profile:
                this.fragment.editProfile();
                break;
            case C0436R.id.remove_friend:
                removeFriend();
                break;
            case C0436R.id.leave_group:
                leaveGroup();
                break;
            case C0436R.id.open_in_browser:
                openInBrowser();
                break;
            case C0436R.id.block:
                toggleBlacklist();
                break;
        }
        return true;
    }

    private void copyLink() {
        if (this.profile != null) {
            ((ClipboardManager) getContext().getSystemService("clipboard")).setText("http://vk.com/" + this.profile.screenName);
            Toast.makeText(getContext(), C0436R.string.link_copied, 0).show();
        }
    }

    private void openInBrowser() {
        getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://m.vk.com/" + this.profile.screenName)));
    }

    private void toggleBlacklist() {
        new AccountBanUser(this.uid, !this.profile.blacklisted).setCallback(new AccountBanUser.Callback() {
            public void success() {
                boolean z;
                ExtendedUserProfile access$0 = ProfileView.this.profile;
                if (ProfileView.this.profile.blacklisted) {
                    z = false;
                } else {
                    z = true;
                }
                access$0.blacklisted = z;
                ((SherlockFragmentActivity) ProfileView.this.getContext()).invalidateOptionsMenu();
                Context context = ProfileView.this.getContext();
                Resources resources = ProfileView.this.getResources();
                int i = ProfileView.this.profile.blacklisted ? ProfileView.this.profile.profile.f151f ? C0436R.string.user_blocked_f : C0436R.string.user_blocked_m : ProfileView.this.profile.profile.f151f ? C0436R.string.user_unblocked_f : C0436R.string.user_unblocked_m;
                Toast.makeText(context, resources.getString(i, new Object[]{ProfileView.this.profile.profile.fullName}), 0).show();
            }

            public void fail(int ecode, String emsg) {
                Toast.makeText(ProfileView.this.getContext(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
            }
        }).wrapProgress((Activity) getContext()).exec((View) this);
    }

    public void onPause() {
        super.onPause();
        this.photoFeed.onPause();
    }

    public void onResume() {
        super.onResume();
        this.photoFeed.onResume();
    }

    protected int getPostsOffset() {
        return this.infoAdapter.getCount();
    }
}
