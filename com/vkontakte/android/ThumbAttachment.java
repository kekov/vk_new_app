package com.vkontakte.android;

public interface ThumbAttachment {
    int getHeight();

    float getRatio();

    String getThumbURL();

    int getWidth();

    int getWidth(char c);

    void setPaddingAfter(boolean z);

    void setViewSize(float f, float f2, boolean z, boolean z2);
}
