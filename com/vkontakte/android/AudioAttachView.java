package com.vkontakte.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build.VERSION;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.AudioPlayerService.AttachViewCallback;
import com.vkontakte.android.AudioPlayerService.ProgressCallback;
import com.vkontakte.android.mediapicker.gl.GLFilterContext;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.ui.IndeterminateHorizontalDrawable;
import java.lang.reflect.Field;

public class AudioAttachView extends ViewGroup implements OnClickListener, AttachViewCallback, ProgressCallback, OnLongClickListener {
    public static final int AUDIO_ATTACH_VIEW_ID = 10;
    String artist;
    private boolean canUpdateProgress;
    int duration;
    int id;
    boolean isPlaying;
    boolean isPressed;
    int oid;
    private ImageView playButton;
    public AudioFile[] playlist;
    public int playlistPos;
    private SeekBar seekbar;
    private TextView text1;
    private TextView text2;
    private TextView timeText;
    String title;

    /* renamed from: com.vkontakte.android.AudioAttachView.1 */
    class C01611 implements OnSeekBarChangeListener {
        C01611() {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            AudioPlayerService.sharedInstance.seek(seekBar.getProgress());
            AudioAttachView.this.canUpdateProgress = true;
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            AudioAttachView.this.canUpdateProgress = false;
        }

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        }
    }

    /* renamed from: com.vkontakte.android.AudioAttachView.2 */
    class C01622 implements Runnable {
        private final /* synthetic */ boolean val$p;

        C01622(boolean z) {
            this.val$p = z;
        }

        public void run() {
            AudioAttachView.this.isPlaying = this.val$p;
            AudioAttachView.this.replaceIcon(AudioAttachView.this.isPlaying ? C0436R.drawable.attach_audio_pause : C0436R.drawable.attach_audio_play);
        }
    }

    /* renamed from: com.vkontakte.android.AudioAttachView.3 */
    class C01633 implements Runnable {
        private final /* synthetic */ int val$aid;
        private final /* synthetic */ int val$oid;
        private final /* synthetic */ int val$state;

        C01633(int i, int i2, int i3) {
            this.val$oid = i;
            this.val$aid = i2;
            this.val$state = i3;
        }

        public void run() {
            boolean z = true;
            if (this.val$oid == AudioAttachView.this.oid && this.val$aid == AudioAttachView.this.id) {
                AudioAttachView audioAttachView = AudioAttachView.this;
                boolean z2 = this.val$state == 1 || this.val$state == 3;
                audioAttachView.setPlaying(z2);
                if (AudioAttachView.this.seekbar.getVisibility() != 0) {
                    AudioAttachView.this.seekbar.setVisibility(0);
                }
                AudioAttachView.this.text1.setSelected(true);
                AudioAttachView.this.text2.setSelected(true);
                SeekBar access$2 = AudioAttachView.this.seekbar;
                if (this.val$state == 3) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                access$2.setIndeterminate(z2);
                SeekBar access$22 = AudioAttachView.this.seekbar;
                if (this.val$state == 3) {
                    z = false;
                }
                access$22.setEnabled(z);
            } else if (AudioAttachView.this.seekbar.getVisibility() == 0) {
                AudioAttachView.this.seekbar.setVisibility(4);
                AudioAttachView.this.text1.setSelected(false);
                AudioAttachView.this.text2.setSelected(false);
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioAttachView.4 */
    class C01644 implements Runnable {
        private final /* synthetic */ int val$played;

        C01644(int i) {
            this.val$played = i;
        }

        public void run() {
            AudioAttachView.this.seekbar.setProgress(this.val$played);
        }
    }

    /* renamed from: com.vkontakte.android.AudioAttachView.5 */
    class C01655 implements Runnable {
        private final /* synthetic */ int val$buffered;

        C01655(int i) {
            this.val$buffered = i;
        }

        public void run() {
            AudioAttachView.this.seekbar.setSecondaryProgress(this.val$buffered);
        }
    }

    public AudioAttachView(Context context) {
        super(context);
        this.isPlaying = false;
        this.isPressed = false;
        this.playlist = null;
        this.playlistPos = 0;
        this.canUpdateProgress = true;
        init();
    }

    public AudioAttachView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.isPlaying = false;
        this.isPressed = false;
        this.playlist = null;
        this.playlistPos = 0;
        this.canUpdateProgress = true;
        init();
    }

    private void init() {
        this.text1 = new TextView(getContext());
        this.text1.setEllipsize(TruncateAt.MARQUEE);
        this.text1.setSingleLine();
        this.text1.setTextColor(Color.WINDOW_BACKGROUND_COLOR);
        this.text1.setTextSize(1, GalleryPickerFooterView.BADGE_SIZE);
        this.text1.setLines(1);
        addView(this.text1);
        this.text2 = new TextView(getContext());
        this.text2.setEllipsize(TruncateAt.MARQUEE);
        this.text2.setSingleLine();
        this.text2.setTextColor(getResources().getColorStateList(C0436R.color.hint));
        this.text2.setTextSize(1, 15.0f);
        this.text2.setLines(1);
        addView(this.text2);
        this.timeText = new TextView(getContext());
        this.timeText.setTextColor(getResources().getColorStateList(C0436R.color.hint));
        this.timeText.setTextSize(1, 15.0f);
        this.timeText.setSingleLine();
        addView(this.timeText);
        this.playButton = new ImageView(getContext());
        this.playButton.setImageResource(C0436R.drawable.attach_audio_play);
        addView(this.playButton);
        this.seekbar = new SeekBar(getContext());
        this.seekbar.setMax(LocationStatusCodes.GEOFENCE_NOT_AVAILABLE);
        this.seekbar.setProgressDrawable(getResources().getDrawable(C0436R.drawable.progress_audio_attach));
        this.seekbar.setThumb(getResources().getDrawable(C0436R.drawable.audio_slider));
        this.seekbar.setPadding(Global.scale(11.0f), 0, Global.scale(11.0f), 0);
        this.seekbar.setMinimumHeight(Global.scale(5.0f));
        this.seekbar.setThumbOffset(Global.scale(11.0f));
        try {
            Field fld = ProgressBar.class.getDeclaredField("mMaxHeight");
            fld.setAccessible(true);
            fld.set(this.seekbar, Integer.valueOf(Global.scale(3.0f)));
            fld = ProgressBar.class.getDeclaredField("mMinHeight");
            fld.setAccessible(true);
            fld.set(this.seekbar, Integer.valueOf(Global.scale(3.0f)));
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        addView(this.seekbar);
        this.seekbar.setIndeterminateDrawable(new IndeterminateHorizontalDrawable());
        this.seekbar.setVisibility(4);
        this.timeText.setGravity(53);
        if (VERSION.SDK_INT < 11) {
            this.seekbar.setPadding(this.seekbar.getPaddingLeft(), Global.scale(9.0f), this.seekbar.getPaddingRight(), Global.scale(10.0f));
        }
    }

    public void onFinishInflate() {
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        AudioPlayerService.addAttachViewCallback(this);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        AudioPlayerService.removeAttachViewCallback(this);
        AudioPlayerService.removeProgressCallback(this);
    }

    public void onMeasure(int w, int h) {
        int pad = Global.scale(5.0f);
        Rect pbRect = new Rect(Global.scale(43.0f) + pad, MeasureSpec.getSize(h) - Global.scale(22.0f), (MeasureSpec.getSize(w) + Global.scale(11.0f)) - pad, MeasureSpec.getSize(h));
        setMeasuredDimension(MeasureSpec.getSize(w), Global.scale(54.0f));
    }

    public void setData(String artist, String title, int _oid, int _aid, int duration) {
        this.text1.setText(artist);
        this.text2.setText(title);
        this.timeText.setText(String.format("%d:%02d", new Object[]{Integer.valueOf(duration / 60), Integer.valueOf(duration % 60)}));
        this.seekbar.setVisibility(4);
        AudioPlayerService.removeProgressCallback(this);
        setOnClickListener(this);
        this.playButton.setOnClickListener(this);
        setOnLongClickListener(this);
        this.playButton.setOnLongClickListener(this);
        this.oid = _oid;
        this.id = _aid;
        this.artist = artist;
        this.title = title;
        this.duration = duration;
        if (AudioPlayerService.sharedInstance != null && AudioPlayerService.sharedInstance.getOid() == this.oid && AudioPlayerService.sharedInstance.getAid() == this.id) {
            if (AudioPlayerService.sharedInstance.isPlaying()) {
                this.isPlaying = true;
                replaceIcon(C0436R.drawable.attach_audio_pause);
            }
            AudioPlayerService.addProgressCallback(this);
            this.seekbar.setVisibility(0);
        } else {
            this.isPlaying = false;
            replaceIcon(C0436R.drawable.attach_audio_play);
        }
        this.seekbar.setOnSeekBarChangeListener(new C01611());
    }

    public void onClick(View v) {
        Intent intent;
        if (AudioPlayerService.sharedInstance != null && AudioPlayerService.sharedInstance.getOid() == this.oid && AudioPlayerService.sharedInstance.getAid() == this.id) {
            intent = new Intent(getContext(), AudioPlayerService.class);
            intent.putExtra("action", 3);
            getContext().startService(intent);
        } else if (AudioPlayerService.sharedInstance == null || AudioPlayerService.sharedInstance.getOid() != this.oid || AudioPlayerService.sharedInstance.getAid() != this.id || !AudioPlayerService.sharedInstance.initing) {
            if (this.playlist == null) {
                intent = new Intent(getContext(), AudioPlayerService.class);
                intent.putExtra("action", 1);
                intent.putExtra("file", new AudioFile(this.id, this.oid, this.artist, this.title, this.duration, null));
                getContext().startService(intent);
                return;
            }
            for (AudioFile file : this.playlist) {
                file.fromAttachment = true;
            }
            intent = new Intent(getContext(), AudioPlayerService.class);
            intent.putExtra("action", 2);
            intent.putExtra("list", this.playlist);
            intent.putExtra(GLFilterContext.AttributePosition, this.playlistPos);
            getContext().startService(intent);
        }
    }

    private void replaceIcon(int resID) {
        this.playButton.setImageResource(resID);
    }

    public void setPlaying(boolean p) {
        if (getParent() != null) {
            post(new C01622(p));
            if (p) {
                AudioPlayerService.addProgressCallback(this);
            } else {
                AudioPlayerService.removeProgressCallback(this);
            }
        }
    }

    public void onPlayStateChanged(int oid, int aid, int state) {
        post(new C01633(oid, aid, state));
    }

    public void onPlayProgressChanged(int oid, int aid, int played) {
        if (this.canUpdateProgress) {
            post(new C01644(played));
        }
    }

    public void onBufferProgressChanged(int oid, int aid, int buffered) {
        post(new C01655(buffered));
    }

    public void onLayout(boolean changed, int l, int t, int r, int b) {
        int pad = Global.scale(5.0f);
        this.playButton.layout(pad, 0, Global.scale(42.0f) + pad, Global.scale(42.0f));
        this.text1.layout(Global.scale(54.0f) + pad, 0, r - l, Global.scale(23.0f));
        this.text2.layout(Global.scale(54.0f) + pad, Global.scale(20.0f), ((r - l) - Global.scale(50.0f)) - pad, Global.scale(42.0f));
        this.timeText.measure(MeasureSpec.makeMeasureSpec(Global.scale(50.0f), 1073741824), MeasureSpec.makeMeasureSpec(Global.scale(22.0f), 1073741824));
        this.timeText.layout((r - l) - Global.scale(50.0f), Global.scale(20.0f), (r - l) - pad, Global.scale(42.0f));
        this.seekbar.layout(Global.scale(43.0f) + pad, (b - t) - Global.scale(22.0f), ((r - l) + Global.scale(11.0f)) - pad, b - t);
    }

    public boolean onLongClick(View v) {
        if (AudioPlayerService.sharedInstance == null) {
            return false;
        }
        if (!AudioPlayerService.sharedInstance.enqueue(new AudioFile(this.id, this.oid, this.artist, this.title, this.duration, null))) {
            return false;
        }
        Toast.makeText(getContext(), C0436R.string.audio_added_to_queue, 0).show();
        return true;
    }
}
