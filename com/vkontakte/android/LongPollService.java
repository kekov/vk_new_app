package com.vkontakte.android;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Notification;
import android.app.Notification.BigTextStyle;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings.System;
import android.support.v4.view.MotionEventCompat;
import android.text.TextUtils;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.api.APIUtils;
import com.vkontakte.android.api.C2DMUnregisterDevice;
import com.vkontakte.android.api.MessagesGetChatUsers;
import com.vkontakte.android.api.MessagesGetHistory;
import com.vkontakte.android.api.MessagesGetHistory.Callback;
import com.vkontakte.android.cache.AddMessageAction;
import com.vkontakte.android.cache.Cache;
import com.vkontakte.android.cache.ChatCache;
import com.vkontakte.android.cache.DeleteMessageAction;
import com.vkontakte.android.cache.MessagesAction;
import com.vkontakte.android.cache.ModifyMessageFlagsAction;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.data.Messages;
import com.vkontakte.android.data.Messages.GetChatUsersCallback;
import com.vkontakte.android.data.Posts;
import com.vkontakte.android.fragments.ChatFragment;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.AnimationDuration;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import org.acra.ACRAConstants;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class LongPollService extends Service implements Runnable {
    public static final String ACTION_CHAT_CHANGED = "com.vkontakte.android.CHAT_CHANGED";
    public static final String ACTION_COUNTERS_UPDATED = "com.vkontakte.android.COUNTERS_UPDATED";
    public static final String ACTION_MESSAGE_DELETED = "com.vkontakte.android.MESSAGE_DELETED";
    public static final String ACTION_MESSAGE_RSTATE_CHANGED = "com.vkontakte.android.MESSAGE_RSTATE_CHANGED";
    public static final String ACTION_NEW_MESSAGE = "com.vkontakte.android.NEW_MESSAGE";
    public static final String ACTION_REFRESH_DIALOGS_LIST = "com.vkontakte.android.REFRESH_DIALOGS";
    public static final String ACTION_TYPING = "com.vkontakte.android.TYPING";
    public static final String ACTION_USER_PRESENCE = "com.vkontakte.android.USER_PRESENCE";
    public static final String ACTION_VOIP_HANGUP = "com.vkontakte.android.VOIP_HANGUP";
    private static final boolean DEBUG = false;
    private static final int EVENT_CHAT_CHANGED = 51;
    private static final int EVENT_CHAT_TYPING = 62;
    private static final int EVENT_FRIEND_OFFLINE = 9;
    private static final int EVENT_FRIEND_ONLINE = 8;
    private static final int EVENT_MSG_ADD = 4;
    private static final int EVENT_MSG_ADD_EXTENDED = 101;
    private static final int EVENT_MSG_DELETE = 0;
    private static final int EVENT_MSG_FLAG_ADD = 2;
    private static final int EVENT_MSG_FLAG_CLEAR = 3;
    private static final int EVENT_MSG_FLAG_REPLACE = 1;
    private static final int EVENT_MSG_IN_READ_UPTO = 6;
    private static final int EVENT_MSG_OUT_READ_UPTO = 7;
    private static final int EVENT_UPDATE_COUNTER = 80;
    private static final int EVENT_USER_TYPING = 61;
    private static final int EVENT_VOIP_HANGUP = 112;
    private static final int EVENT_VOIP_INCOMING = 110;
    private static final int EVENT_VOIP_REPLIED = 111;
    public static final String EXTRA_MESSAGE = "message";
    public static final String EXTRA_MSG_ID = "msg_id";
    public static final String EXTRA_PEER_ID = "peer_id";
    public static final String EXTRA_READ_STATE = "read_state";
    public static final int INTRO_IMPORT_FRIENDS = 1;
    public static final int INTRO_SUGGEST_GROUPS = 2;
    public static final int[] MOBILE_ONLINE_TYPES;
    public static final int MSG_CHAT = 16;
    public static final int MSG_DELETED = 128;
    public static final int MSG_FIXED = 256;
    public static final int MSG_FRIENDS = 32;
    public static final int MSG_IMPORTANT = 8;
    public static final int MSG_MEDIA = 512;
    public static final int MSG_OUTBOX = 2;
    public static final int MSG_REPLIED = 4;
    public static final int MSG_SPAM = 64;
    public static final int MSG_UNREAD = 1;
    public static final int NOTIFY_ID_MESSAGE = 10;
    public static final int ONLINE_TYPE_ANDROID = 4;
    public static final int ONLINE_TYPE_DEFAULT = 7;
    public static final int ONLINE_TYPE_IPAD = 3;
    public static final int ONLINE_TYPE_IPHONE = 2;
    public static final int ONLINE_TYPE_MOBILE = 1;
    public static final int ONLINE_TYPE_WINDOWS8 = 6;
    public static final int ONLINE_TYPE_WINPHONE = 5;
    public static final String PERMISSION = "com.vkontakte.android.permission.ACCESS_DATA";
    public static Semaphore addMessage;
    public static Semaphore addOwnMessage;
    public static ConcurrentHashMap<Integer, Vector<Message>> cachedDialogs;
    public static ConcurrentHashMap<Integer, HashMap<Integer, String>> cachedKnownUsernames;
    public static ConcurrentHashMap<Integer, HashMap<Integer, String>> cachedKnownUserphotos;
    public static ConcurrentHashMap<Integer, String> chatTitles;
    public static ConcurrentHashMap<Integer, ArrayList<UserProfile>> chatUsers;
    public static ConcurrentHashMap<Integer, Boolean> dialogsMoreAvailable;
    public static UserProfile lastMessageProfile;
    public static boolean longPollActive;
    private static HttpClient longPollClient;
    public static boolean longPollRunning;
    public static boolean needFinishAllActivities;
    public static boolean needReloadDialogs;
    static Notification notification1;
    public static int numFriendRequests;
    public static int numGroupInvitations;
    public static int numNewMessages;
    public static int numNotifications;
    public static ArrayList<Message> pendingReceivedMessages;
    public static int prevNumNewMessages;
    public static UserProfile realLastProfile;
    public static ArrayList<Integer> receivedCalls;
    public static ArrayList<Message> sendingMessages;
    private boolean currentIsHttps;
    private String key;
    long lastReqTime;
    Thread lpThread;
    private boolean needResendOnline;
    private boolean needStop;
    private boolean newSystem;
    Timer onlineTimer;
    private String server;
    private boolean started;
    Timer stopDelayTimer;
    private int ts;

    /* renamed from: com.vkontakte.android.LongPollService.7 */
    class C02957 extends TimerTask {
        C02957() {
        }

        public void run() {
            LongPollService.this.sendOnline();
        }
    }

    /* renamed from: com.vkontakte.android.LongPollService.1 */
    class C13011 extends APIHandler {
        C13011() {
        }

        public void success(JSONObject r) {
            try {
                int i;
                JSONObject o = r.getJSONObject("response").getJSONObject("s");
                LongPollService.this.ts = o.getInt("ts");
                LongPollService.this.key = o.getString("key");
                LongPollService.this.server = o.getString("server");
                LongPollService.this.currentIsHttps = PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getBoolean("useHTTPS", LongPollService.DEBUG);
                JSONObject o1 = r.getJSONObject("response").optJSONObject("c");
                MenuListView.counters.clear();
                if (o1 != null) {
                    LongPollService.this.setNumUnread(o1.optInt("messages", LongPollService.EVENT_MSG_DELETE));
                    LongPollService.numFriendRequests = o1.optInt("friends");
                    LongPollService.numNotifications = o1.optInt("notifications");
                    LongPollService.numGroupInvitations = o1.optInt("groups");
                    Iterator<String> keys = o1.keys();
                    while (keys.hasNext()) {
                        String k = (String) keys.next();
                        MenuListView.counters.put(k, Integer.valueOf(o1.optInt(k)));
                    }
                } else {
                    LongPollService.this.setNumUnread(LongPollService.EVENT_MSG_DELETE);
                    LongPollService.numFriendRequests = LongPollService.EVENT_MSG_DELETE;
                    LongPollService.numNotifications = LongPollService.EVENT_MSG_DELETE;
                    LongPollService.numGroupInvitations = LongPollService.EVENT_MSG_DELETE;
                }
                if (MenuListView.lastInstance != null) {
                    MenuListView.lastInstance.updateList();
                }
                LongPollService.this.sendBroadcast(new Intent(LongPollService.ACTION_COUNTERS_UPDATED), LongPollService.PERMISSION);
                LongPollService.this.sendBroadcast(new Intent(Friends.ACTION_FRIEND_REQUESTS_CHANGED), LongPollService.PERMISSION);
                JSONObject fo = r.getJSONObject("response").getJSONObject("fo");
                JSONArray fdo = fo.getJSONArray("online");
                JSONArray fmo = fo.getJSONArray("online_mobile");
                ArrayList<Integer> onlines = new ArrayList();
                ArrayList<Integer> mobileOnlines = new ArrayList();
                for (i = LongPollService.EVENT_MSG_DELETE; i < fdo.length(); i += LongPollService.ONLINE_TYPE_MOBILE) {
                    onlines.add(Integer.valueOf(fdo.getInt(i)));
                }
                for (i = LongPollService.EVENT_MSG_DELETE; i < fmo.length(); i += LongPollService.ONLINE_TYPE_MOBILE) {
                    mobileOnlines.add(Integer.valueOf(fmo.getInt(i)));
                }
                Friends.setOnlines(onlines, mobileOnlines);
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    /* renamed from: com.vkontakte.android.LongPollService.2 */
    class C13022 extends APIHandler {
        private final /* synthetic */ int[] val$maxMid;

        C13022(int[] iArr) {
            this.val$maxMid = iArr;
        }

        public void success(JSONObject o) {
            try {
                this.val$maxMid[LongPollService.EVENT_MSG_DELETE] = o.getInt("response");
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.LongPollService.3 */
    class C13033 extends APIHandler {
        private final /* synthetic */ boolean[] val$sendNextRequest;

        C13033(boolean[] zArr) {
            this.val$sendNextRequest = zArr;
        }

        public void success(JSONObject o) {
            try {
                int i;
                UserProfile p;
                Intent intent;
                ArrayList<Integer> updatedIds = new ArrayList();
                ArrayList<Integer> markedAsRead = new ArrayList();
                ArrayList<Integer> markedAsUnread = new ArrayList();
                ArrayList<Integer> deleted = new ArrayList();
                o = o.getJSONObject("response");
                HashMap<Integer, String> names = new HashMap();
                HashMap<Integer, String> photos = new HashMap();
                JSONArray profiles = o.getJSONArray("profiles");
                ArrayList<UserProfile> users = new ArrayList();
                for (i = LongPollService.EVENT_MSG_DELETE; i < profiles.length(); i += LongPollService.ONLINE_TYPE_MOBILE) {
                    JSONObject jp = profiles.getJSONObject(i);
                    names.put(Integer.valueOf(jp.getInt("id")), jp.getString("first_name") + " " + jp.getString("last_name"));
                    photos.put(Integer.valueOf(jp.getInt("id")), jp.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo"));
                    p = new UserProfile();
                    p.uid = jp.getInt("id");
                    p.firstName = jp.getString("first_name");
                    p.lastName = jp.getString("last_name");
                    p.fullName = p.firstName + " " + p.lastName;
                    p.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo");
                    users.add(p);
                }
                Cache.updatePeers(users, LongPollService.DEBUG);
                users.clear();
                JSONArray jchats = o.optJSONArray("chats");
                if (jchats != null) {
                    for (i = LongPollService.EVENT_MSG_DELETE; i < jchats.length(); i += LongPollService.ONLINE_TYPE_MOBILE) {
                        JSONObject jc = jchats.getJSONObject(i);
                        UserProfile c = new UserProfile();
                        c.uid = 2000000000 + jc.getInt("id");
                        c.fullName = jc.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                        c.online = jc.getInt("admin_id");
                        if (jc.has("photo_50")) {
                            String str;
                            if (Global.displayDensity > 1.0f) {
                                str = "photo_100";
                            } else {
                                str = "photo_50";
                            }
                            c.photo = jc.getString(str);
                        } else {
                            ArrayList<String> ph = new ArrayList();
                            ph.add("M");
                            JSONArray act = jc.getJSONArray("users");
                            ArrayList<Integer> needUsers = new ArrayList();
                            for (int j = LongPollService.EVENT_MSG_DELETE; j < Math.min(act.length(), LongPollService.ONLINE_TYPE_ANDROID); j += LongPollService.ONLINE_TYPE_MOBILE) {
                                needUsers.add(Integer.valueOf(act.getInt(j)));
                            }
                            Iterator it = Friends.getUsersBlocking(needUsers).iterator();
                            while (it.hasNext()) {
                                p = (UserProfile) it.next();
                                if (p.uid != Global.uid) {
                                    ph.add(p.photo);
                                }
                            }
                            c.photo = TextUtils.join("|", ph);
                        }
                        users.add(c);
                    }
                }
                JSONArray jmsgs = APIUtils.unwrapArray(o, "messages").array;
                HashMap<Integer, Message> msgs = new HashMap();
                for (i = LongPollService.EVENT_MSG_DELETE; i < jmsgs.length(); i += LongPollService.ONLINE_TYPE_MOBILE) {
                    Message message = new Message(jmsgs.getJSONObject(i), names, photos);
                    msgs.put(Integer.valueOf(message.id), message);
                    if (message.isServiceMessage && message.extras.containsKey("action")) {
                        Cache.setNeedUpdateChat(message.peer - 2000000000);
                        intent = new Intent(LongPollService.ACTION_CHAT_CHANGED);
                        intent.putExtra("id", message.peer - 2000000000);
                        VKApplication.context.sendBroadcast(intent);
                        if (message.extras.containsKey("action_mid")) {
                            Integer[] numArr = new Integer[LongPollService.ONLINE_TYPE_MOBILE];
                            numArr[LongPollService.EVENT_MSG_DELETE] = Integer.valueOf(message.extras.getInt("action_mid"));
                            message.extras.putString("action_user_name_acc", ((UserProfile) Friends.getUsersBlocking(Arrays.asList(numArr), LongPollService.ONLINE_TYPE_IPAD).get(LongPollService.EVENT_MSG_DELETE)).fullName);
                        }
                    }
                }
                JSONArray events = o.getJSONArray("history");
                ArrayList<MessagesAction> actions = new ArrayList();
                for (i = LongPollService.EVENT_MSG_DELETE; i < events.length(); i += LongPollService.ONLINE_TYPE_MOBILE) {
                    JSONArray event = events.getJSONArray(i);
                    int ev = event.getInt(LongPollService.EVENT_MSG_DELETE);
                    int flag;
                    int mid;
                    switch (ev) {
                        case LongPollService.ONLINE_TYPE_IPHONE /*2*/:
                            flag = event.getInt(LongPollService.ONLINE_TYPE_IPHONE);
                            mid = event.getInt(LongPollService.ONLINE_TYPE_MOBILE);
                            if ((flag & LongPollService.ONLINE_TYPE_MOBILE) <= 0) {
                                if ((flag & LongPollService.MSG_DELETED) <= 0) {
                                    break;
                                }
                                actions.add(new DeleteMessageAction(mid));
                                deleted.add(Integer.valueOf(mid));
                                break;
                            }
                            actions.add(new ModifyMessageFlagsAction(mid, LongPollService.ONLINE_TYPE_MOBILE, LongPollService.EVENT_MSG_DELETE));
                            if (markedAsRead.contains(Integer.valueOf(mid))) {
                                markedAsRead.remove(Integer.valueOf(mid));
                            }
                            markedAsUnread.add(Integer.valueOf(mid));
                            break;
                        case LongPollService.ONLINE_TYPE_IPAD /*3*/:
                            flag = event.getInt(LongPollService.ONLINE_TYPE_IPHONE);
                            mid = event.getInt(LongPollService.ONLINE_TYPE_MOBILE);
                            if ((flag & LongPollService.ONLINE_TYPE_MOBILE) <= 0) {
                                break;
                            }
                            actions.add(new ModifyMessageFlagsAction(mid, LongPollService.ONLINE_TYPE_MOBILE, LongPollService.ONLINE_TYPE_MOBILE));
                            if (markedAsUnread.contains(Integer.valueOf(mid))) {
                                markedAsUnread.remove(Integer.valueOf(mid));
                            }
                            markedAsRead.add(Integer.valueOf(mid));
                            break;
                        case LongPollService.ONLINE_TYPE_ANDROID /*4*/:
                            if ((event.getInt(LongPollService.ONLINE_TYPE_IPHONE) & LongPollService.MSG_DELETED) != 0) {
                                break;
                            }
                            Message msg = (Message) msgs.get(Integer.valueOf(event.getInt(LongPollService.ONLINE_TYPE_MOBILE)));
                            actions.add(new AddMessageAction(msg));
                            if (!updatedIds.contains(Integer.valueOf(msg.peer))) {
                                updatedIds.add(Integer.valueOf(msg.peer));
                                break;
                            }
                            break;
                        case LongPollService.ONLINE_TYPE_WINDOWS8 /*6*/:
                        case LongPollService.ONLINE_TYPE_DEFAULT /*7*/:
                            actions.add(new ModifyMessageFlagsAction(event.getInt(LongPollService.ONLINE_TYPE_IPHONE), LongPollService.ONLINE_TYPE_MOBILE, LongPollService.ONLINE_TYPE_MOBILE, ev == LongPollService.ONLINE_TYPE_WINDOWS8 ? true : LongPollService.DEBUG, event.getInt(LongPollService.ONLINE_TYPE_MOBILE)));
                            break;
                        default:
                            break;
                    }
                }
                if (actions.size() > 0) {
                    Messages.applyActions(actions);
                }
                if (users.size() > 0) {
                    Cache.updatePeers(users, LongPollService.DEBUG);
                }
                int pts = o.getInt("new_pts");
                if (VKApplication.context.getSharedPreferences("longpoll", LongPollService.EVENT_MSG_DELETE).getInt("pts", LongPollService.EVENT_MSG_DELETE) == 0) {
                    this.val$sendNextRequest[LongPollService.EVENT_MSG_DELETE] = LongPollService.DEBUG;
                    return;
                }
                VKApplication.context.getSharedPreferences("longpoll", LongPollService.EVENT_MSG_DELETE).edit().putInt("pts", pts).commit();
                this.val$sendNextRequest[LongPollService.EVENT_MSG_DELETE] = o.optInt("more") == LongPollService.ONLINE_TYPE_MOBILE ? true : LongPollService.DEBUG;
                Log.m525d("vk_longpoll", "More=" + this.val$sendNextRequest[LongPollService.EVENT_MSG_DELETE]);
                intent = new Intent(LongPollService.ACTION_REFRESH_DIALOGS_LIST);
                intent.putExtra("reload_chats", updatedIds);
                intent.putExtra("marked_as_read", markedAsRead);
                intent.putExtra("marked_as_unread", markedAsUnread);
                intent.putExtra("deleted", deleted);
                VKApplication.context.sendBroadcast(intent);
            } catch (Throwable x) {
                Log.m531w("vk_longpoll", "get long poll history failed!", x);
            }
        }

        public void fail(int ecode, String emsg) {
            if (ecode == LongPollService.ONLINE_TYPE_WINDOWS8) {
                try {
                    Thread.sleep(2000);
                } catch (Exception e) {
                }
                this.val$sendNextRequest[LongPollService.EVENT_MSG_DELETE] = true;
            }
            if (ecode != -1) {
                Log.m526e("vk_longpoll", "GET LONG POLL HISTORY FAILED " + ecode + "/" + emsg);
            }
        }
    }

    /* renamed from: com.vkontakte.android.LongPollService.4 */
    class C13044 extends APIHandler {
        C13044() {
        }

        public void success(JSONObject o) {
            boolean z = LongPollService.DEBUG;
            try {
                JSONObject r = o.optJSONObject("response");
                if (r == null) {
                    r = new JSONObject();
                }
                int numReqs = r.optInt("friends", LongPollService.EVENT_MSG_DELETE);
                if (numReqs != LongPollService.numFriendRequests) {
                    LongPollService.numFriendRequests = numReqs;
                    VKApplication.context.sendBroadcast(new Intent(Friends.ACTION_FRIEND_REQUESTS_CHANGED), LongPollService.PERMISSION);
                }
                LongPollService.numNotifications = r.optInt("notifications", LongPollService.EVENT_MSG_DELETE);
                LongPollService.numGroupInvitations = r.optInt("groups", LongPollService.EVENT_MSG_DELETE);
                int numMsgs = r.optInt("messages", LongPollService.EVENT_MSG_DELETE);
                if (numMsgs != LongPollService.numNewMessages) {
                    LongPollService.needReloadDialogs = true;
                    VKApplication.context.sendBroadcast(new Intent(LongPollService.ACTION_REFRESH_DIALOGS_LIST), LongPollService.PERMISSION);
                    LongPollService.numNewMessages = numMsgs;
                }
                MenuListView.counters.clear();
                Iterator<String> keys = r.keys();
                while (keys.hasNext()) {
                    String k = (String) keys.next();
                    MenuListView.counters.put(k, Integer.valueOf(r.optInt(k)));
                }
                if (MenuListView.lastInstance != null) {
                    MenuListView.lastInstance.updateList();
                }
                Editor edit = VKApplication.context.getSharedPreferences(null, LongPollService.EVENT_MSG_DELETE).edit();
                String str = "forceHTTPS";
                if (r.optInt("https_required") == LongPollService.ONLINE_TYPE_MOBILE) {
                    z = true;
                }
                edit.putBoolean(str, z).commit();
                if (r.optInt("https_required") == LongPollService.ONLINE_TYPE_MOBILE) {
                    PreferenceManager.getDefaultSharedPreferences(VKApplication.context).edit().putBoolean("useHTTPS", true).commit();
                }
                VKApplication.context.getSharedPreferences(null, LongPollService.EVENT_MSG_DELETE).edit().putInt("intro", r.getInt("intro")).commit();
                VKApplication.context.sendBroadcast(new Intent(LongPollService.ACTION_COUNTERS_UPDATED), LongPollService.PERMISSION);
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    /* renamed from: com.vkontakte.android.LongPollService.5 */
    class C13065 implements Callback {
        private final /* synthetic */ int val$peerID;

        /* renamed from: com.vkontakte.android.LongPollService.5.1 */
        class C13051 implements MessagesGetChatUsers.Callback {
            private final /* synthetic */ int val$peerID;

            C13051(int i) {
                this.val$peerID = i;
            }

            public void success(int total, Vector<UserProfile> users, int adminID, String title) {
                Iterator it = users.iterator();
                while (it.hasNext()) {
                    UserProfile u = (UserProfile) it.next();
                    if (!LongPollService.cachedKnownUsernames.containsKey(Integer.valueOf(this.val$peerID))) {
                        LongPollService.cachedKnownUsernames.put(Integer.valueOf(this.val$peerID), new HashMap());
                    }
                    if (!LongPollService.cachedKnownUserphotos.containsKey(Integer.valueOf(this.val$peerID))) {
                        LongPollService.cachedKnownUserphotos.put(Integer.valueOf(this.val$peerID), new HashMap());
                    }
                    ((HashMap) LongPollService.cachedKnownUsernames.get(Integer.valueOf(this.val$peerID))).put(Integer.valueOf(u.uid), u.fullName);
                    ((HashMap) LongPollService.cachedKnownUserphotos.get(Integer.valueOf(this.val$peerID))).put(Integer.valueOf(u.uid), u.photo);
                }
            }

            public void fail(int ecode, String emsg) {
            }
        }

        C13065(int i) {
            this.val$peerID = i;
        }

        public void success(int total, Vector<Message> msgs) {
            LongPollService.cachedDialogs.put(Integer.valueOf(this.val$peerID), msgs);
            LongPollService.dialogsMoreAvailable.put(Integer.valueOf(this.val$peerID), Boolean.valueOf(msgs.size() < total ? true : LongPollService.DEBUG));
            if (this.val$peerID > 2000000000) {
                new MessagesGetChatUsers(this.val$peerID - 2000000000).setCallback(new C13051(this.val$peerID)).exec();
            }
            ChatCache.add(VKApplication.context, this.val$peerID, msgs, true);
        }

        public void fail(int ecode, String emsg) {
        }
    }

    /* renamed from: com.vkontakte.android.LongPollService.6 */
    class C13076 implements GetChatUsersCallback {
        private final /* synthetic */ Message val$msg;

        C13076(Message message) {
            this.val$msg = message;
        }

        public void onUsersLoaded(ArrayList<ChatUser> users, String title, String photo) {
            String ph = Messages.createChatPhoto(users);
            Log.m528i("vk", "New chat photo = " + ph);
            Cache.updateChat(this.val$msg.peer - 2000000000, null, null, ph);
            Intent intent = new Intent(LongPollService.ACTION_CHAT_CHANGED);
            intent.putExtra("id", this.val$msg.peer - 2000000000);
            intent.putExtra("photo", ph);
            LongPollService.this.sendBroadcast(intent);
        }
    }

    /* renamed from: com.vkontakte.android.LongPollService.8 */
    class C13088 extends APIHandler {
        C13088() {
        }

        public void success(JSONObject j) {
            LongPollService.this.needResendOnline = LongPollService.DEBUG;
            VKApplication.context.getSharedPreferences(null, LongPollService.EVENT_MSG_DELETE).edit().remove("push_counter").commit();
            Posts.clearViewedPosts();
        }

        public void fail(int ecode, String emsg) {
            LongPollService.this.needResendOnline = true;
        }
    }

    /* renamed from: com.vkontakte.android.LongPollService.9 */
    class C13099 extends APIHandler {
        C13099() {
        }

        public void success(JSONObject j) {
            Posts.clearViewedPosts();
        }

        public void fail(int ecode, String emsg) {
        }
    }

    public LongPollService() {
        this.started = DEBUG;
        this.needResendOnline = DEBUG;
        this.newSystem = DEBUG;
        this.needStop = DEBUG;
    }

    static {
        MOBILE_ONLINE_TYPES = new int[]{ONLINE_TYPE_MOBILE, ONLINE_TYPE_IPHONE, ONLINE_TYPE_IPAD, ONLINE_TYPE_ANDROID, ONLINE_TYPE_WINPHONE};
        needFinishAllActivities = DEBUG;
        longPollRunning = DEBUG;
        needReloadDialogs = DEBUG;
        longPollActive = DEBUG;
        numNewMessages = EVENT_MSG_DELETE;
        prevNumNewMessages = EVENT_MSG_DELETE;
        numFriendRequests = EVENT_MSG_DELETE;
        numNotifications = EVENT_MSG_DELETE;
        numGroupInvitations = EVENT_MSG_DELETE;
        notification1 = null;
        addOwnMessage = new Semaphore(ONLINE_TYPE_MOBILE, true);
        addMessage = new Semaphore(ONLINE_TYPE_MOBILE, true);
        cachedDialogs = new ConcurrentHashMap();
        dialogsMoreAvailable = new ConcurrentHashMap();
        cachedKnownUsernames = new ConcurrentHashMap();
        cachedKnownUserphotos = new ConcurrentHashMap();
        chatUsers = new ConcurrentHashMap();
        chatTitles = new ConcurrentHashMap();
        lastMessageProfile = null;
        realLastProfile = null;
        receivedCalls = new ArrayList();
        sendingMessages = new ArrayList();
        pendingReceivedMessages = new ArrayList();
    }

    private void updateServerInfo() {
        int i = ONLINE_TYPE_MOBILE;
        try {
            if (!Global.inited) {
                Global.inited = true;
                VKApplication.context = getApplicationContext();
                SharedPreferences prefs = getSharedPreferences(null, EVENT_MSG_DELETE);
                if (prefs.contains("sid")) {
                    Global.uid = prefs.getInt("uid", EVENT_MSG_DELETE);
                    Global.accessToken = prefs.getString("sid", null);
                    Global.secret = prefs.getString("secret", null);
                }
            }
            this.server = null;
            APIRequest aPIRequest = new APIRequest("execute");
            String str = "code";
            StringBuilder stringBuilder = new StringBuilder("return {c:API.getCounters(),s:API.messages.getLongPollServer({use_ssl:");
            if (!PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getBoolean("useHTTPS", DEBUG)) {
                i = EVENT_MSG_DELETE;
            }
            aPIRequest.param(str, stringBuilder.append(i).append("}),fo:API.friends.getOnline({online_mobile:1})};").toString()).handler(new C13011()).execSync();
            syncStateWithServer();
        } catch (Exception e) {
        }
    }

    public static void syncStateWithServer() {
        int pts = VKApplication.context.getSharedPreferences("longpoll", EVENT_MSG_DELETE).getInt("pts", EVENT_MSG_DELETE);
        Log.m528i("vk_longpoll", "Before get long poll history, pts=" + pts);
        if (pts > 0) {
            int[] maxMid = new int[ONLINE_TYPE_MOBILE];
            new APIRequest("execute.getMaxMsgIdNew").handler(new C13022(maxMid)).execSync();
            int localMaxMid = Cache.getMaxMsgId();
            int diff = maxMid[EVENT_MSG_DELETE] - localMaxMid;
            Log.m525d("vk_longpoll", "Msg diff=" + diff + " [server=" + maxMid[EVENT_MSG_DELETE] + ", client=" + localMaxMid + "]");
            if (diff < AnimationDuration.SELECTION_EXIT_FADE || Global.uid == 1708231) {
                boolean[] sendNextRequest = new boolean[ONLINE_TYPE_MOBILE];
                do {
                    pts = VKApplication.context.getSharedPreferences("longpoll", EVENT_MSG_DELETE).getInt("pts", EVENT_MSG_DELETE);
                    Log.m528i("vk_longpoll", "Get long poll history, old ts=" + pts);
                    new APIRequest("messages.getLongPollHistory").param("pts", pts).param("photo_sizes", (int) ONLINE_TYPE_MOBILE).param("fields", "first_name,last_name,photo_100,photo_50").handler(new C13033(sendNextRequest)).execSync();
                } while (sendNextRequest[EVENT_MSG_DELETE]);
                Log.m528i("vk_longpoll", "Get long poll hisrory done.");
                return;
            }
            Log.m525d("vk_longpoll", "Too many new messages, resetting cache");
            Messages.reset();
            Messages.resetCache();
            VKApplication.context.sendBroadcast(new Intent(ACTION_REFRESH_DIALOGS_LIST));
        }
    }

    public void setNumUnread(int n) {
        numNewMessages = n;
        updateTabBadge();
        if (numNewMessages == 0) {
            ((NotificationManager) getSystemService("notification")).cancel(NOTIFY_ID_MESSAGE);
        }
    }

    private void start() {
        Log.m525d("vk_longpoll", "started = " + this.started);
        if (!this.started) {
            Thread thread = new Thread(this);
            this.lpThread = thread;
            thread.start();
            this.started = true;
        }
    }

    private void stop() {
        this.needStop = true;
        if (this.onlineTimer != null) {
            this.onlineTimer.cancel();
        }
        stopSelf();
        Global.longPoll = null;
    }

    public static void updateCounters() {
        if (VKApplication.context.getSharedPreferences(null, EVENT_MSG_DELETE).contains("uid")) {
            new APIRequest("execute.getCountersAndInfo").handler(new C13044()).exec();
        }
    }

    private void processClearFlags(int mid, int uid, int mask) {
        if ((mask & ONLINE_TYPE_MOBILE) > 0 && !this.newSystem) {
            Intent intent = new Intent(ACTION_MESSAGE_RSTATE_CHANGED);
            intent.putExtra(EXTRA_MSG_ID, mid);
            intent.putExtra(EXTRA_READ_STATE, true);
            sendBroadcast(intent, PERMISSION);
            boolean isOut = DEBUG;
            if (cachedDialogs.containsKey(Integer.valueOf(uid))) {
                Iterator it = ((Vector) cachedDialogs.get(Integer.valueOf(uid))).iterator();
                while (it.hasNext()) {
                    Message msg = (Message) it.next();
                    if (msg.id == mid) {
                        msg.readState = true;
                        isOut = msg.out;
                    }
                }
            }
            Messages.setReadState(mid, true);
            if (!isOut && uid != Global.uid && !this.newSystem) {
                numNewMessages--;
                if (numNewMessages < 0) {
                    numNewMessages = EVENT_MSG_DELETE;
                }
                if (numNewMessages == 0) {
                    ((NotificationManager) getSystemService("notification")).cancel(NOTIFY_ID_MESSAGE);
                    sendBroadcast(new Intent(ACTION_COUNTERS_UPDATED), PERMISSION);
                }
            }
        }
    }

    private void processAddFlags(int mid, int uid, int mask) {
        if ((mask & ONLINE_TYPE_MOBILE) > 0 && !this.newSystem) {
            Intent intent = new Intent(ACTION_MESSAGE_RSTATE_CHANGED);
            intent.putExtra(EXTRA_MSG_ID, mid);
            intent.putExtra(EXTRA_READ_STATE, DEBUG);
            sendBroadcast(intent, PERMISSION);
            if (cachedDialogs.containsKey(Integer.valueOf(uid))) {
                Iterator it = ((Vector) cachedDialogs.get(Integer.valueOf(uid))).iterator();
                while (it.hasNext()) {
                    Message msg = (Message) it.next();
                    if (msg.id == mid) {
                        msg.readState = DEBUG;
                    }
                }
            }
            Messages.setReadState(mid, DEBUG);
            if (!(uid == Global.uid || this.newSystem)) {
                numNewMessages += ONLINE_TYPE_MOBILE;
                sendBroadcast(new Intent(ACTION_COUNTERS_UPDATED), PERMISSION);
            }
        }
        if ((mask & MSG_DELETED) > 0) {
            intent = new Intent(ACTION_MESSAGE_DELETED);
            intent.putExtra(EXTRA_MSG_ID, mid);
            sendBroadcast(intent, PERMISSION);
        }
    }

    private void updateTabBadge() {
        MenuListView.counters.put("messages", Integer.valueOf(numNewMessages));
        sendBroadcast(new Intent(ACTION_COUNTERS_UPDATED), PERMISSION);
    }

    private void processOnlineChange(int uid, int online) {
        Friends.setOnlineStatus(uid, online);
        Intent intent = new Intent(ACTION_USER_PRESENCE);
        intent.putExtra("uid", uid);
        intent.putExtra("online", online);
        sendBroadcast(intent, PERMISSION);
    }

    private void processTyping(int peerID, int userID) {
        Intent intent = new Intent(ACTION_TYPING);
        intent.putExtra("uid", peerID);
        intent.putExtra("user", userID);
        sendBroadcast(intent, PERMISSION);
    }

    public void run() {
        Thread.currentThread().setName("LongPoll");
        if (!this.needStop) {
            longPollRunning = true;
            runLongPoll();
            longPollRunning = DEBUG;
            longPollActive = DEBUG;
        }
        this.lpThread = null;
        this.started = DEBUG;
    }

    public static void checkAndPreload(int peerID) {
        if (!cachedDialogs.containsKey(Integer.valueOf(peerID))) {
            new MessagesGetHistory(peerID, EVENT_MSG_DELETE, 20).setCallback(new C13065(peerID)).exec();
        }
    }

    private byte[] getURL(String url) {
        if (longPollClient == null) {
            HttpParams hParams = new BasicHttpParams();
            HttpProtocolParams.setUseExpectContinue(hParams, DEBUG);
            HttpProtocolParams.setUserAgent(hParams, null);
            HttpConnectionParams.setSocketBufferSize(hParams, ACRAConstants.DEFAULT_BUFFER_SIZE_IN_BYTES);
            HttpConnectionParams.setConnectionTimeout(hParams, 60000);
            HttpConnectionParams.setSoTimeout(hParams, 60000);
            HttpConnectionParams.setStaleCheckingEnabled(hParams, DEBUG);
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), EVENT_UPDATE_COUNTER));
            longPollClient = new DefaultHttpClient(new ThreadSafeClientConnManager(hParams, registry), hParams);
        }
        try {
            InputStream is = longPollClient.execute(new HttpGet(url)).getEntity().getContent();
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            byte[] rd = new byte[GLRenderBuffer.EGL_SURFACE_SIZE];
            while (true) {
                int l = is.read(rd);
                if (l <= 0) {
                    is.close();
                    return buf.toByteArray();
                }
                buf.write(rd, EVENT_MSG_DELETE, l);
            }
        } catch (Throwable th) {
            return null;
        }
    }

    private void processChatChanged(int chatID) {
        Cache.setNeedUpdateChat(chatID);
        Intent intent = new Intent(ACTION_CHAT_CHANGED);
        intent.putExtra("id", chatID);
        sendBroadcast(intent);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processMessage(int r28, int r29, int r30, int r31, java.lang.String r32, java.lang.String r33, org.json.JSONObject r34) {
        /*
        r27 = this;
        r3 = r30 & 2;
        if (r3 <= 0) goto L_0x00ae;
    L_0x0004:
        r15 = 1;
    L_0x0005:
        if (r15 == 0) goto L_0x0011;
    L_0x0007:
        r3 = addOwnMessage;	 Catch:{ Exception -> 0x03b6 }
        r3.acquire();	 Catch:{ Exception -> 0x03b6 }
        r3 = addOwnMessage;	 Catch:{ Exception -> 0x03b6 }
        r3.release();	 Catch:{ Exception -> 0x03b6 }
    L_0x0011:
        r3 = addMessage;	 Catch:{ Exception -> 0x03b3 }
        r3.acquire();	 Catch:{ Exception -> 0x03b3 }
        r3 = addMessage;	 Catch:{ Exception -> 0x03b3 }
        r3.release();	 Catch:{ Exception -> 0x03b3 }
    L_0x001b:
        r3 = 2000000000; // 0x77359400 float:3.682842E33 double:9.881312917E-315;
        r0 = r29;
        if (r0 >= r3) goto L_0x00b5;
    L_0x0022:
        if (r15 == 0) goto L_0x00b1;
    L_0x0024:
        r22 = com.vkontakte.android.Global.uid;	 Catch:{ Exception -> 0x00c7 }
    L_0x0026:
        if (r34 == 0) goto L_0x00d2;
    L_0x0028:
        r3 = "attach1_type";
        r0 = r34;
        r3 = r0.has(r3);	 Catch:{ Exception -> 0x00c7 }
        if (r3 != 0) goto L_0x003c;
    L_0x0032:
        r3 = "fwd";
        r0 = r34;
        r3 = r0.has(r3);	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x00d2;
    L_0x003c:
        r12 = new com.vkontakte.android.APIRequest;	 Catch:{ Exception -> 0x00c7 }
        r3 = "messages.getById";
        r12.<init>(r3);	 Catch:{ Exception -> 0x00c7 }
        r3 = "message_ids";
        r0 = r28;
        r3 = r12.param(r3, r0);	 Catch:{ Exception -> 0x00c7 }
        r4 = "photo_sizes";
        r5 = 1;
        r3 = r3.param(r4, r5);	 Catch:{ Exception -> 0x00c7 }
        r4 = "fields";
        r5 = "first_name,last_name,photo_100,photo_50";
        r3.param(r4, r5);	 Catch:{ Exception -> 0x00c7 }
        r21 = com.vkontakte.android.APIController.runRequest(r12);	 Catch:{ Exception -> 0x00c7 }
        if (r21 == 0) goto L_0x00bf;
    L_0x005f:
        r3 = "response";
        r0 = r21;
        r3 = r0.has(r3);	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x00bf;
    L_0x0069:
        r3 = "response";
        r0 = r21;
        r3 = com.vkontakte.android.api.APIUtils.unwrapArray(r0, r3);	 Catch:{ Exception -> 0x00c7 }
        r3 = r3.array;	 Catch:{ Exception -> 0x00c7 }
        r4 = 0;
        r16 = r3.getJSONObject(r4);	 Catch:{ Exception -> 0x00c7 }
        r17 = new com.vkontakte.android.Message;	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r1 = r16;
        r0.<init>(r1);	 Catch:{ Exception -> 0x00c7 }
    L_0x0081:
        r0 = r17;
        r3 = r0.extras;	 Catch:{ Exception -> 0x00c7 }
        r4 = "action";
        r3 = r3.containsKey(r4);	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x0092;
    L_0x008d:
        r3 = 1;
        r0 = r17;
        r0.isServiceMessage = r3;	 Catch:{ Exception -> 0x00c7 }
    L_0x0092:
        r0 = r17;
        r3 = r0.out;	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x0175;
    L_0x0098:
        r0 = r17;
        r3 = r0.isServiceMessage;	 Catch:{ Exception -> 0x00c7 }
        if (r3 != 0) goto L_0x0175;
    L_0x009e:
        r3 = sendingMessages;	 Catch:{ Exception -> 0x00c7 }
        r3 = r3.size();	 Catch:{ Exception -> 0x00c7 }
        if (r3 <= 0) goto L_0x0175;
    L_0x00a6:
        r3 = pendingReceivedMessages;	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r3.add(r0);	 Catch:{ Exception -> 0x00c7 }
    L_0x00ad:
        return;
    L_0x00ae:
        r15 = 0;
        goto L_0x0005;
    L_0x00b1:
        r22 = r29;
        goto L_0x0026;
    L_0x00b5:
        r3 = "from";
        r0 = r34;
        r22 = r0.getInt(r3);	 Catch:{ Exception -> 0x00c7 }
        goto L_0x0026;
    L_0x00bf:
        r3 = "vk_longpoll";
        r4 = "Error getting full message";
        com.vkontakte.android.Log.m530w(r3, r4);	 Catch:{ Exception -> 0x00c7 }
        goto L_0x00ad;
    L_0x00c7:
        r26 = move-exception;
        r3 = "vk_longpoll";
        r4 = "Error processing message";
        r0 = r26;
        com.vkontakte.android.Log.m531w(r3, r4, r0);
        goto L_0x00ad;
    L_0x00d2:
        r17 = new com.vkontakte.android.Message;	 Catch:{ Exception -> 0x00c7 }
        r17.<init>();	 Catch:{ Exception -> 0x00c7 }
        r0 = r29;
        r1 = r17;
        r1.peer = r0;	 Catch:{ Exception -> 0x00c7 }
        r0 = r22;
        r1 = r17;
        r1.sender = r0;	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r1 = r32;
        r0.setText(r1);	 Catch:{ Exception -> 0x00c7 }
        r0 = r31;
        r1 = r17;
        r1.time = r0;	 Catch:{ Exception -> 0x00c7 }
        r0 = r28;
        r1 = r17;
        r1.id = r0;	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r0.out = r15;	 Catch:{ Exception -> 0x00c7 }
        r3 = 0;
        r0 = r17;
        r0.readState = r3;	 Catch:{ Exception -> 0x00c7 }
        if (r34 == 0) goto L_0x0081;
    L_0x0101:
        r3 = "source_act";
        r0 = r34;
        r3 = r0.has(r3);	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x0081;
    L_0x010b:
        r3 = "source_act";
        r0 = r34;
        r8 = r0.getString(r3);	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r3 = r0.extras;	 Catch:{ Exception -> 0x00c7 }
        r4 = "action";
        r3.putString(r4, r8);	 Catch:{ Exception -> 0x00c7 }
        r3 = "chat_invite_user";
        r3 = r3.equals(r8);	 Catch:{ Exception -> 0x00c7 }
        if (r3 != 0) goto L_0x012c;
    L_0x0124:
        r3 = "chat_kick_user";
        r3 = r3.equals(r8);	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x013f;
    L_0x012c:
        r3 = "source_mid";
        r0 = r34;
        r24 = r0.getInt(r3);	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r3 = r0.extras;	 Catch:{ Exception -> 0x00c7 }
        r4 = "action_mid";
        r0 = r24;
        r3.putInt(r4, r0);	 Catch:{ Exception -> 0x00c7 }
    L_0x013f:
        r3 = "chat_title_update";
        r3 = r3.equals(r8);	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x0158;
    L_0x0147:
        r0 = r17;
        r3 = r0.extras;	 Catch:{ Exception -> 0x00c7 }
        r4 = "action_text";
        r5 = "source_text";
        r0 = r34;
        r5 = r0.getString(r5);	 Catch:{ Exception -> 0x00c7 }
        r3.putString(r4, r5);	 Catch:{ Exception -> 0x00c7 }
    L_0x0158:
        r3 = "source_email";
        r0 = r34;
        r3 = r0.has(r3);	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x0081;
    L_0x0162:
        r0 = r17;
        r3 = r0.extras;	 Catch:{ Exception -> 0x00c7 }
        r4 = "action_email";
        r5 = "source_email";
        r0 = r34;
        r5 = r0.getString(r5);	 Catch:{ Exception -> 0x00c7 }
        r3.putString(r4, r5);	 Catch:{ Exception -> 0x00c7 }
        goto L_0x0081;
    L_0x0175:
        r13 = new android.content.Intent;	 Catch:{ Exception -> 0x00c7 }
        r3 = "com.vkontakte.android.NEW_MESSAGE";
        r13.<init>(r3);	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r3 = r0.extras;	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x01d0;
    L_0x0182:
        r0 = r17;
        r3 = r0.extras;	 Catch:{ Exception -> 0x00c7 }
        r4 = "action_mid";
        r3 = r3.containsKey(r4);	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x01d0;
    L_0x018e:
        r0 = r17;
        r3 = r0.extras;	 Catch:{ Exception -> 0x00c7 }
        r4 = "action_mid";
        r5 = 0;
        r9 = r3.getInt(r4, r5);	 Catch:{ Exception -> 0x00c7 }
        r3 = 1;
        r3 = new java.lang.Integer[r3];	 Catch:{ Exception -> 0x00c7 }
        r4 = 0;
        r5 = java.lang.Integer.valueOf(r9);	 Catch:{ Exception -> 0x00c7 }
        r3[r4] = r5;	 Catch:{ Exception -> 0x00c7 }
        r3 = java.util.Arrays.asList(r3);	 Catch:{ Exception -> 0x00c7 }
        r4 = 3;
        r25 = com.vkontakte.android.data.Friends.getUsersBlocking(r3, r4);	 Catch:{ Exception -> 0x00c7 }
        r4 = "action_user_name_acc";
        r3 = 0;
        r0 = r25;
        r3 = r0.get(r3);	 Catch:{ Exception -> 0x00c7 }
        r3 = (com.vkontakte.android.UserProfile) r3;	 Catch:{ Exception -> 0x00c7 }
        r3 = r3.fullName;	 Catch:{ Exception -> 0x00c7 }
        r13.putExtra(r4, r3);	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r4 = r0.extras;	 Catch:{ Exception -> 0x00c7 }
        r5 = "action_user_name_acc";
        r3 = 0;
        r0 = r25;
        r3 = r0.get(r3);	 Catch:{ Exception -> 0x00c7 }
        r3 = (com.vkontakte.android.UserProfile) r3;	 Catch:{ Exception -> 0x00c7 }
        r3 = r3.fullName;	 Catch:{ Exception -> 0x00c7 }
        r4.putString(r5, r3);	 Catch:{ Exception -> 0x00c7 }
    L_0x01d0:
        r18 = new java.util.ArrayList;	 Catch:{ Exception -> 0x00c7 }
        r18.<init>();	 Catch:{ Exception -> 0x00c7 }
        r3 = java.lang.Integer.valueOf(r22);	 Catch:{ Exception -> 0x00c7 }
        r0 = r18;
        r0.add(r3);	 Catch:{ Exception -> 0x00c7 }
        r3 = com.vkontakte.android.data.Friends.getUsersBlocking(r18);	 Catch:{ Exception -> 0x00c7 }
        r4 = 0;
        r23 = r3.get(r4);	 Catch:{ Exception -> 0x00c7 }
        r23 = (com.vkontakte.android.UserProfile) r23;	 Catch:{ Exception -> 0x00c7 }
        r3 = "sender_profile";
        r0 = r23;
        r13.putExtra(r3, r0);	 Catch:{ Exception -> 0x00c7 }
        r3 = "peer_id";
        r0 = r29;
        r13.putExtra(r3, r0);	 Catch:{ Exception -> 0x00c7 }
        r3 = "message";
        r0 = r17;
        r13.putExtra(r3, r0);	 Catch:{ Exception -> 0x00c7 }
        r3 = "is_out";
        r13.putExtra(r3, r15);	 Catch:{ Exception -> 0x00c7 }
        r3 = "sender_photo";
        r0 = r23;
        r4 = r0.photo;	 Catch:{ Exception -> 0x00c7 }
        r13.putExtra(r3, r4);	 Catch:{ Exception -> 0x00c7 }
        r19 = 0;
        r3 = 2000000000; // 0x77359400 float:3.682842E33 double:9.881312917E-315;
        r0 = r29;
        if (r0 >= r3) goto L_0x0237;
    L_0x0215:
        r3 = 1;
        r3 = new java.lang.Integer[r3];	 Catch:{ Exception -> 0x00c7 }
        r4 = 0;
        r5 = java.lang.Integer.valueOf(r29);	 Catch:{ Exception -> 0x00c7 }
        r3[r4] = r5;	 Catch:{ Exception -> 0x00c7 }
        r3 = java.util.Arrays.asList(r3);	 Catch:{ Exception -> 0x00c7 }
        r20 = com.vkontakte.android.data.Friends.getUsersBlocking(r3);	 Catch:{ Exception -> 0x00c7 }
        r3 = "peer_profile";
        r4 = 0;
        r0 = r20;
        r19 = r0.get(r4);	 Catch:{ Exception -> 0x00c7 }
        r19 = (com.vkontakte.android.UserProfile) r19;	 Catch:{ Exception -> 0x00c7 }
        r0 = r19;
        r13.putExtra(r3, r0);	 Catch:{ Exception -> 0x00c7 }
    L_0x0237:
        r0 = r27;
        r0.sendBroadcast(r13);	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r3 = r0.extras;	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x026f;
    L_0x0242:
        r0 = r17;
        r3 = r0.extras;	 Catch:{ Exception -> 0x00c7 }
        r4 = "action";
        r3 = r3.containsKey(r4);	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x026f;
    L_0x024e:
        r3 = 2000000000; // 0x77359400 float:3.682842E33 double:9.881312917E-315;
        r3 = r29 - r3;
        com.vkontakte.android.cache.Cache.setNeedUpdateChat(r3);	 Catch:{ Exception -> 0x00c7 }
        r14 = new android.content.Intent;	 Catch:{ Exception -> 0x00c7 }
        r3 = "com.vkontakte.android.CHAT_CHANGED";
        r14.<init>(r3);	 Catch:{ Exception -> 0x00c7 }
        r3 = "id";
        r0 = r17;
        r4 = r0.peer;	 Catch:{ Exception -> 0x00c7 }
        r5 = 2000000000; // 0x77359400 float:3.682842E33 double:9.881312917E-315;
        r4 = r4 - r5;
        r14.putExtra(r3, r4);	 Catch:{ Exception -> 0x00c7 }
        r0 = r27;
        r0.sendBroadcast(r14);	 Catch:{ Exception -> 0x00c7 }
    L_0x026f:
        if (r15 == 0) goto L_0x0288;
    L_0x0271:
        r0 = r17;
        r3 = r0.id;	 Catch:{ Exception -> 0x00c7 }
        r3 = com.vkontakte.android.cache.Cache.containsMessage(r3);	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x0288;
    L_0x027b:
        r0 = r23;
        r3 = r0.photo;	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r1 = r19;
        com.vkontakte.android.data.Messages.add(r0, r1, r3);	 Catch:{ Exception -> 0x00c7 }
        goto L_0x00ad;
    L_0x0288:
        r0 = r23;
        r3 = r0.photo;	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r1 = r19;
        com.vkontakte.android.data.Messages.add(r0, r1, r3);	 Catch:{ Exception -> 0x00c7 }
        if (r15 != 0) goto L_0x00ad;
    L_0x0295:
        r0 = r27;
        r3 = r0.newSystem;	 Catch:{ Exception -> 0x00c7 }
        if (r3 != 0) goto L_0x02af;
    L_0x029b:
        r3 = numNewMessages;	 Catch:{ Exception -> 0x00c7 }
        r3 = r3 + 1;
        numNewMessages = r3;	 Catch:{ Exception -> 0x00c7 }
        r3 = new android.content.Intent;	 Catch:{ Exception -> 0x00c7 }
        r4 = "com.vkontakte.android.COUNTERS_UPDATED";
        r3.<init>(r4);	 Catch:{ Exception -> 0x00c7 }
        r4 = "com.vkontakte.android.permission.ACCESS_DATA";
        r0 = r27;
        r0.sendBroadcast(r3, r4);	 Catch:{ Exception -> 0x00c7 }
    L_0x02af:
        realLastProfile = r23;	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r2 = r0.text;	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r3 = r0.isServiceMessage;	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x02d5;
    L_0x02bb:
        r0 = r17;
        r3 = r0.extras;	 Catch:{ Exception -> 0x00c7 }
        r4 = "action";
        r3 = r3.containsKey(r4);	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x02d5;
    L_0x02c7:
        r3 = "action_user_name_acc";
        r3 = r13.getStringExtra(r3);	 Catch:{ Exception -> 0x00c7 }
        r0 = r17;
        r1 = r23;
        r2 = r0.getServiceMessageText(r1, r3);	 Catch:{ Exception -> 0x00c7 }
    L_0x02d5:
        if (r2 == 0) goto L_0x02dd;
    L_0x02d7:
        r3 = r2.length();	 Catch:{ Exception -> 0x00c7 }
        if (r3 != 0) goto L_0x0301;
    L_0x02dd:
        r0 = r17;
        r3 = r0.attachments;	 Catch:{ Exception -> 0x00c7 }
        r3 = r3.size();	 Catch:{ Exception -> 0x00c7 }
        if (r3 <= 0) goto L_0x038b;
    L_0x02e7:
        r0 = r17;
        r3 = r0.attachments;	 Catch:{ Exception -> 0x00c7 }
        r4 = 0;
        r10 = r3.get(r4);	 Catch:{ Exception -> 0x00c7 }
        r10 = (com.vkontakte.android.Attachment) r10;	 Catch:{ Exception -> 0x00c7 }
        r3 = r10 instanceof com.vkontakte.android.PhotoAttachment;	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x031e;
    L_0x02f6:
        r3 = r27.getResources();	 Catch:{ Exception -> 0x00c7 }
        r4 = 2131230766; // 0x7f08002e float:1.8077594E38 double:1.052967905E-314;
        r2 = r3.getString(r4);	 Catch:{ Exception -> 0x00c7 }
    L_0x0301:
        r0 = r17;
        r3 = r0.peer;	 Catch:{ Exception -> 0x00c7 }
        r4 = 2000000000; // 0x77359400 float:3.682842E33 double:9.881312917E-315;
        if (r3 <= r4) goto L_0x03b0;
    L_0x030a:
        r3 = r33;
    L_0x030c:
        r0 = r23;
        r4 = r0.fullName;	 Catch:{ Exception -> 0x00c7 }
        r0 = r23;
        r5 = r0.photo;	 Catch:{ Exception -> 0x00c7 }
        r6 = 1;
        r0 = r17;
        r7 = r0.peer;	 Catch:{ Exception -> 0x00c7 }
        updateNotification(r2, r3, r4, r5, r6, r7);	 Catch:{ Exception -> 0x00c7 }
        goto L_0x00ad;
    L_0x031e:
        r3 = r10 instanceof com.vkontakte.android.VideoAttachment;	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x032e;
    L_0x0322:
        r3 = r27.getResources();	 Catch:{ Exception -> 0x00c7 }
        r4 = 2131230768; // 0x7f080030 float:1.8077598E38 double:1.052967906E-314;
        r2 = r3.getString(r4);	 Catch:{ Exception -> 0x00c7 }
        goto L_0x0301;
    L_0x032e:
        r3 = r10 instanceof com.vkontakte.android.AudioAttachment;	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x033e;
    L_0x0332:
        r3 = r27.getResources();	 Catch:{ Exception -> 0x00c7 }
        r4 = 2131230767; // 0x7f08002f float:1.8077596E38 double:1.0529679053E-314;
        r2 = r3.getString(r4);	 Catch:{ Exception -> 0x00c7 }
        goto L_0x0301;
    L_0x033e:
        r3 = r10 instanceof com.vkontakte.android.DocumentAttachment;	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x035a;
    L_0x0342:
        r0 = r10;
        r0 = (com.vkontakte.android.DocumentAttachment) r0;	 Catch:{ Exception -> 0x00c7 }
        r11 = r0;
        r4 = r27.getResources();	 Catch:{ Exception -> 0x00c7 }
        r3 = r11.url;	 Catch:{ Exception -> 0x00c7 }
        if (r3 != 0) goto L_0x0356;
    L_0x034e:
        r3 = 2131231187; // 0x7f0801d3 float:1.8078448E38 double:1.052968113E-314;
    L_0x0351:
        r2 = r4.getString(r3);	 Catch:{ Exception -> 0x00c7 }
        goto L_0x0301;
    L_0x0356:
        r3 = 2131230769; // 0x7f080031 float:1.80776E38 double:1.0529679063E-314;
        goto L_0x0351;
    L_0x035a:
        r3 = r10 instanceof com.vkontakte.android.PostAttachment;	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x036a;
    L_0x035e:
        r3 = r27.getResources();	 Catch:{ Exception -> 0x00c7 }
        r4 = 2131231056; // 0x7f080150 float:1.8078182E38 double:1.052968048E-314;
        r2 = r3.getString(r4);	 Catch:{ Exception -> 0x00c7 }
        goto L_0x0301;
    L_0x036a:
        r3 = r10 instanceof com.vkontakte.android.GeoAttachment;	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x037a;
    L_0x036e:
        r3 = r27.getResources();	 Catch:{ Exception -> 0x00c7 }
        r4 = 2131230971; // 0x7f0800fb float:1.807801E38 double:1.052968006E-314;
        r2 = r3.getString(r4);	 Catch:{ Exception -> 0x00c7 }
        goto L_0x0301;
    L_0x037a:
        r3 = r10 instanceof com.vkontakte.android.StickerAttachment;	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x0301;
    L_0x037e:
        r3 = r27.getResources();	 Catch:{ Exception -> 0x00c7 }
        r4 = 2131231548; // 0x7f08033c float:1.807918E38 double:1.052968291E-314;
        r2 = r3.getString(r4);	 Catch:{ Exception -> 0x00c7 }
        goto L_0x0301;
    L_0x038b:
        r0 = r17;
        r3 = r0.fwdMessages;	 Catch:{ Exception -> 0x00c7 }
        if (r3 == 0) goto L_0x0301;
    L_0x0391:
        r0 = r17;
        r3 = r0.fwdMessages;	 Catch:{ Exception -> 0x00c7 }
        r3 = r3.size();	 Catch:{ Exception -> 0x00c7 }
        if (r3 <= 0) goto L_0x0301;
    L_0x039b:
        r3 = 2131558429; // 0x7f0d001d float:1.8742174E38 double:1.053129792E-314;
        r0 = r17;
        r4 = r0.fwdMessages;	 Catch:{ Exception -> 0x00c7 }
        r4 = r4.size();	 Catch:{ Exception -> 0x00c7 }
        r5 = r27.getResources();	 Catch:{ Exception -> 0x00c7 }
        r2 = com.vkontakte.android.Global.langPlural(r3, r4, r5);	 Catch:{ Exception -> 0x00c7 }
        goto L_0x0301;
    L_0x03b0:
        r3 = 0;
        goto L_0x030c;
    L_0x03b3:
        r3 = move-exception;
        goto L_0x001b;
    L_0x03b6:
        r3 = move-exception;
        goto L_0x0011;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.LongPollService.processMessage(int, int, int, int, java.lang.String, java.lang.String, org.json.JSONObject):void");
    }

    private void processExtendedMessage(JSONObject obj) {
        try {
            int i;
            Intent intent;
            UserProfile p;
            JSONObject jmsg = obj.getJSONObject(EXTRA_MESSAGE);
            boolean isOut = jmsg.getInt("out") == ONLINE_TYPE_MOBILE ? true : DEBUG;
            if (isOut) {
                try {
                    addOwnMessage.acquire();
                    addOwnMessage.release();
                } catch (Exception e) {
                }
            }
            try {
                addMessage.acquire();
                addMessage.release();
            } catch (Exception e2) {
            }
            int sender = jmsg.has("chat_id") ? 2000000000 + jmsg.getInt("chat_id") : jmsg.getInt("uid");
            HashMap<Integer, String> names = new HashMap();
            HashMap<Integer, String> photos = new HashMap();
            JSONArray profiles = obj.optJSONArray("profiles");
            if (profiles != null) {
                for (i = EVENT_MSG_DELETE; i < profiles.length(); i += ONLINE_TYPE_MOBILE) {
                    JSONObject p2 = profiles.getJSONObject(i);
                    names.put(Integer.valueOf(p2.getInt("uid")), p2.getString("first_name") + " " + p2.getString("last_name"));
                    String photo = p2.getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo");
                    if (!photo.startsWith("http")) {
                        if (photo.startsWith("/")) {
                            photo = "http://vk.com" + photo;
                        } else {
                            photo = "http://vk.com/" + photo;
                        }
                    }
                    photos.put(Integer.valueOf(p2.getInt("uid")), photo);
                }
            }
            Message message = new Message(jmsg, names, photos);
            if (message.isServiceMessage && "%act:chat_photo_update".equals(message.text)) {
                String ph = jmsg.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                Cache.updateChat(message.peer - 2000000000, null, null, ph);
                intent = new Intent(ACTION_CHAT_CHANGED);
                intent.putExtra("id", message.peer - 2000000000);
                intent.putExtra("photo", ph);
                sendBroadcast(intent);
            }
            if (message.isServiceMessage && "%act:chat_photo_remove".equals(message.text)) {
                Messages.getChatUsers(message.peer - 2000000000, new C13076(message));
            }
            String senderPhoto = ACRAConstants.DEFAULT_STRING_VALUE;
            UserProfile senderProfile = null;
            if (sender > 2000000000) {
                for (i = EVENT_MSG_DELETE; i < profiles.length(); i += ONLINE_TYPE_MOBILE) {
                    if (profiles.getJSONObject(i).getInt("uid") == message.sender) {
                        senderPhoto = profiles.getJSONObject(i).getString(Global.displayDensity > 1.0f ? "photo_medium" : "photo");
                    }
                }
            }
            if (!senderPhoto.startsWith("http")) {
                senderPhoto = "http://vk.com" + senderPhoto;
            }
            ArrayList<UserProfile> users = new ArrayList();
            if (profiles != null) {
                for (i = EVENT_MSG_DELETE; i < profiles.length(); i += ONLINE_TYPE_MOBILE) {
                    JSONObject jp = profiles.getJSONObject(i);
                    p = new UserProfile();
                    p.uid = jp.getInt("uid");
                    p.firstName = jp.getString("first_name");
                    p.lastName = jp.getString("last_name");
                    p.fullName = p.firstName + " " + p.lastName;
                    p.photo = jp.getString(Global.displayDensity > 1.0f ? "photo_medium" : "photo");
                    if (!p.photo.startsWith("http")) {
                        p.photo = "http://vk.com" + p.photo;
                    }
                    p.isFriend = jp.optInt("is_friend") == ONLINE_TYPE_MOBILE ? true : DEBUG;
                    p.f151f = jp.optInt("sex") == ONLINE_TYPE_MOBILE ? true : DEBUG;
                    p.online = Global.getUserOnlineStatus(jp);
                    users.add(p);
                    if (p.uid == message.sender) {
                        senderProfile = p;
                    }
                }
            }
            if (!message.out || sendingMessages.size() <= 0) {
                UserProfile profile;
                int i2;
                intent = new Intent(ACTION_NEW_MESSAGE);
                intent.putExtra(EXTRA_PEER_ID, sender);
                intent.putExtra(EXTRA_MESSAGE, message);
                intent.putExtra("is_out", isOut);
                intent.putExtra("sender_photo", senderPhoto);
                Iterator it;
                if (message.peer > 2000000000) {
                    profile = new UserProfile();
                    profile.uid = message.peer;
                    profile.fullName = jmsg.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                    if (jmsg.has("photo_50")) {
                        String str;
                        if (Global.displayDensity > 1.0f) {
                            str = "photo_100";
                        } else {
                            str = "photo_50";
                        }
                        profile.photo = jmsg.getString(str);
                    } else {
                        ArrayList<String> ph2 = new ArrayList();
                        ph2.add("M");
                        String[] active = jmsg.getString("chat_active").split(",");
                        ArrayList<Integer> uids = new ArrayList();
                        int length = active.length;
                        for (i2 = EVENT_MSG_DELETE; i2 < length; i2 += ONLINE_TYPE_MOBILE) {
                            try {
                                int uid = Integer.parseInt(active[i2]);
                                if (uid != Global.uid) {
                                    uids.add(Integer.valueOf(uid));
                                    if (uids.size() == ONLINE_TYPE_ANDROID) {
                                        break;
                                    }
                                } else {
                                    continue;
                                }
                            } catch (Throwable x) {
                                Log.m532w("vk", x);
                            }
                        }
                        it = Friends.getUsersBlocking(uids).iterator();
                        while (it.hasNext()) {
                            ph2.add(((UserProfile) it.next()).photo);
                        }
                        profile.photo = TextUtils.join("|", ph2);
                    }
                    users.add(profile);
                    intent.putExtra("peer_profile", profile);
                } else {
                    profile = new UserProfile();
                    it = users.iterator();
                    while (it.hasNext()) {
                        p = (UserProfile) it.next();
                        if (p.uid == message.peer) {
                            profile = p;
                            break;
                        }
                    }
                    intent.putExtra("peer_profile", profile);
                }
                intent.putExtra("sender_profile", senderProfile);
                sendBroadcast(intent, PERMISSION);
                if (isOut && Cache.containsMessage(message.id)) {
                    Messages.add(message, profile, senderPhoto);
                    return;
                }
                Messages.add(message, profile, senderPhoto);
                if (!isOut) {
                    if (!this.newSystem) {
                        numNewMessages += ONLINE_TYPE_MOBILE;
                        sendBroadcast(new Intent(ACTION_COUNTERS_UPDATED), PERMISSION);
                    }
                    realLastProfile = senderProfile;
                    String notifyText = message.text;
                    if (message.isServiceMessage && message.text.startsWith("%act")) {
                        Resources resources;
                        Object[] objArr;
                        if ("%act:chat_photo_update".equals(message.text)) {
                            resources = getResources();
                            i2 = senderProfile.f151f ? C0436R.string.chat_photo_updated_f : C0436R.string.chat_photo_updated_m;
                            objArr = new Object[ONLINE_TYPE_MOBILE];
                            objArr[EVENT_MSG_DELETE] = senderProfile.fullName;
                            notifyText = resources.getString(i2, objArr);
                        }
                        if ("%act:chat_photo_remove".equals(message.text)) {
                            resources = getResources();
                            i2 = senderProfile.f151f ? C0436R.string.chat_photo_removed_f : C0436R.string.chat_photo_removed_m;
                            objArr = new Object[ONLINE_TYPE_MOBILE];
                            objArr[EVENT_MSG_DELETE] = senderProfile.fullName;
                            notifyText = resources.getString(i2, objArr);
                        }
                    }
                    if (notifyText == null || notifyText.length() == 0) {
                        if (message.attachments.size() > 0) {
                            Attachment att = (Attachment) message.attachments.get(EVENT_MSG_DELETE);
                            if (att instanceof PhotoAttachment) {
                                notifyText = getResources().getString(C0436R.string.photo);
                            } else if (att instanceof VideoAttachment) {
                                notifyText = getResources().getString(C0436R.string.video);
                            } else if (att instanceof AudioAttachment) {
                                notifyText = getResources().getString(C0436R.string.audio);
                            } else if (att instanceof DocumentAttachment) {
                                notifyText = getResources().getString(((DocumentAttachment) att).url == null ? C0436R.string.gift : C0436R.string.doc);
                            } else if (att instanceof PostAttachment) {
                                notifyText = getResources().getString(C0436R.string.attach_wall_post);
                            } else if (att instanceof GeoAttachment) {
                                notifyText = getResources().getString(C0436R.string.place);
                            } else if (att instanceof StickerAttachment) {
                                notifyText = getResources().getString(C0436R.string.sticker);
                            }
                        } else if (message.fwdMessages != null && message.fwdMessages.size() > 0) {
                            notifyText = Global.langPlural(C0436R.array.num_attach_fwd_message, message.fwdMessages.size(), getResources());
                        }
                    }
                    updateNotification(notifyText, message.peer > 2000000000 ? jmsg.optString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE) : null, senderProfile.fullName, senderProfile.photo, true, message.peer);
                }
                Cache.updatePeers(users, DEBUG);
                return;
            }
            pendingReceivedMessages.add(message);
        } catch (Throwable x2) {
            Log.m532w("vk", x2);
        }
    }

    private void processReadUpto(int peerID, int msgID, boolean in) {
        if (!in) {
            try {
                addOwnMessage.acquire();
                addOwnMessage.release();
            } catch (Exception e) {
            }
        }
        Messages.setReadStateUpto(peerID, msgID, in);
        Intent intent = new Intent(ACTION_MESSAGE_RSTATE_CHANGED);
        intent.putExtra(EXTRA_MSG_ID, msgID);
        intent.putExtra(EXTRA_READ_STATE, true);
        intent.putExtra(EXTRA_PEER_ID, peerID);
        intent.putExtra("in", in);
        intent.putExtra("le", true);
        sendBroadcast(intent, PERMISSION);
    }

    private void processUpdateCounter(int counter) {
        if (!this.newSystem) {
            this.newSystem = true;
            getSharedPreferences("longpoll", EVENT_MSG_DELETE).edit().putBoolean("new_system", true).commit();
        }
        setNumUnread(counter);
    }

    private void runLongPoll() {
        Log.m525d("vk_longpoll", "LongPoll starting");
        if (this.onlineTimer == null) {
            startSendingOnline();
        }
        if (Global.accessToken == null || !getSharedPreferences(null, EVENT_MSG_DELETE).contains("sid")) {
            Log.m525d("vk_longpoll", "No user, stopping");
            stopSelf();
            return;
        }
        updateServerInfo();
        int nAttempts = EVENT_MSG_DELETE;
        int updateRetries = EVENT_MSG_DELETE;
        while (!this.needStop) {
            String url = new StringBuilder(String.valueOf(PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getBoolean("useHTTPS", DEBUG) ? "https" : "http")).append("://").append(this.server).append("?act=a_check&key=").append(this.key).append("&ts=").append(this.ts).append("&wait=25&mode=98").toString();
            if (this.needStop) {
                Log.m525d("vk_longpoll", "LongPoll STOPPED");
                return;
            }
            byte[] rdata = getURL(url);
            if (this.needStop) {
                Log.m525d("vk_longpoll", "LongPoll STOPPED");
                return;
            } else if (this.currentIsHttps != PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getBoolean("useHTTPS", DEBUG)) {
                Log.m528i("vk_longpoll", "HTTPS setting changed, updating server");
                updateServerInfo();
                if (this.server == null) {
                    updateRetries += ONLINE_TYPE_MOBILE;
                } else {
                    updateRetries = EVENT_MSG_DELETE;
                }
                if (!NetworkStateReceiver.isConnected() || updateRetries >= ONLINE_TYPE_IPHONE) {
                    Log.m528i("vk_longpoll", "No network connection, stopping longpoll");
                    stopSelf();
                    return;
                }
            } else if (rdata != null) {
                longPollActive = true;
                try {
                    JSONObject r = (JSONObject) new JSONTokener(Global.replaceHTML(new String(rdata, "UTF-8"))).nextValue();
                    if (r.has("failed")) {
                        Log.m530w("vk_longpoll", "Got 'failed', reloading server info");
                        updateServerInfo();
                    } else {
                        if (this.needResendOnline) {
                            sendOnline();
                        }
                        this.ts = r.getInt("ts");
                        getSharedPreferences("longpoll", EVENT_MSG_DELETE).edit().putInt("pts", r.getInt("pts")).commit();
                        JSONArray u = r.getJSONArray("updates");
                        for (int i = EVENT_MSG_DELETE; i < u.length(); i += ONLINE_TYPE_MOBILE) {
                            JSONArray ev = u.getJSONArray(i);
                            switch (ev.getInt(EVENT_MSG_DELETE)) {
                                case ONLINE_TYPE_IPHONE /*2*/:
                                    processAddFlags(ev.getInt(ONLINE_TYPE_MOBILE), ev.getInt(ONLINE_TYPE_IPAD), ev.getInt(ONLINE_TYPE_IPHONE));
                                    break;
                                case ONLINE_TYPE_IPAD /*3*/:
                                    processClearFlags(ev.getInt(ONLINE_TYPE_MOBILE), ev.getInt(ONLINE_TYPE_IPAD), ev.getInt(ONLINE_TYPE_IPHONE));
                                    break;
                                case ONLINE_TYPE_ANDROID /*4*/:
                                    processMessage(ev.getInt(ONLINE_TYPE_MOBILE), ev.getInt(ONLINE_TYPE_IPAD), ev.getInt(ONLINE_TYPE_IPHONE), ev.getInt(ONLINE_TYPE_ANDROID), ev.getString(ONLINE_TYPE_WINDOWS8), ev.getString(ONLINE_TYPE_WINPHONE), ev.optJSONObject(ONLINE_TYPE_DEFAULT));
                                    break;
                                case ONLINE_TYPE_WINDOWS8 /*6*/:
                                    processReadUpto(ev.getInt(ONLINE_TYPE_MOBILE), ev.getInt(ONLINE_TYPE_IPHONE), true);
                                    break;
                                case ONLINE_TYPE_DEFAULT /*7*/:
                                    processReadUpto(ev.getInt(ONLINE_TYPE_MOBILE), ev.getInt(ONLINE_TYPE_IPHONE), DEBUG);
                                    break;
                                case MSG_IMPORTANT /*8*/:
                                    int onl = ONLINE_TYPE_MOBILE;
                                    int lpo = ev.getInt(ONLINE_TYPE_IPHONE) & MotionEventCompat.ACTION_MASK;
                                    if (lpo == ONLINE_TYPE_MOBILE) {
                                        onl = ONLINE_TYPE_IPHONE;
                                    }
                                    if (lpo == ONLINE_TYPE_ANDROID || lpo == ONLINE_TYPE_IPHONE || lpo == ONLINE_TYPE_IPAD || lpo == ONLINE_TYPE_WINPHONE) {
                                        onl = ONLINE_TYPE_IPAD;
                                    }
                                    processOnlineChange(-ev.getInt(ONLINE_TYPE_MOBILE), onl);
                                    break;
                                case EVENT_FRIEND_OFFLINE /*9*/:
                                    processOnlineChange(-ev.getInt(ONLINE_TYPE_MOBILE), EVENT_MSG_DELETE);
                                    break;
                                case EVENT_CHAT_CHANGED /*51*/:
                                    processChatChanged(ev.getInt(ONLINE_TYPE_MOBILE));
                                    break;
                                case EVENT_USER_TYPING /*61*/:
                                    processTyping(ev.getInt(ONLINE_TYPE_MOBILE), ev.getInt(ONLINE_TYPE_MOBILE));
                                    break;
                                case EVENT_CHAT_TYPING /*62*/:
                                    processTyping(2000000000 + ev.getInt(ONLINE_TYPE_IPHONE), ev.getInt(ONLINE_TYPE_MOBILE));
                                    break;
                                case EVENT_UPDATE_COUNTER /*80*/:
                                    processUpdateCounter(ev.getInt(ONLINE_TYPE_MOBILE));
                                    break;
                                default:
                                    Log.m530w("vk_longpoll", "Unknown event " + ev.toString());
                                    break;
                            }
                        }
                    }
                } catch (Throwable x) {
                    Log.m527e("vk_longpoll", "Exception while parsing", x);
                }
            } else if (nAttempts < ONLINE_TYPE_WINDOWS8) {
                Log.m530w("vk_longpoll", "Connection error. Retry in 5 sec");
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                }
                nAttempts += ONLINE_TYPE_MOBILE;
            } else if (NetworkStateReceiver.isConnected()) {
                updateServerInfo();
                nAttempts = EVENT_MSG_DELETE;
                Log.m530w("vk_longpoll", "6 errors, reloading server info");
            } else {
                Log.m528i("vk_longpoll", "No network connection, stopping longpoll");
                stopSelf();
                return;
            }
        }
        Log.m525d("vk_longpoll", "LongPoll exiting");
        Thread thread = new Thread(this);
        this.lpThread = thread;
        thread.start();
    }

    public static void updateNotification() {
        NotificationManager mNotificationManager = (NotificationManager) VKApplication.context.getSystemService("notification");
        notification1 = new Notification();
        if (PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getBoolean("notifySound", true)) {
            notification1.sound = Uri.parse(PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getString("notifyRingtone", System.DEFAULT_NOTIFICATION_URI.toString()));
        }
        if (PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getBoolean("notifyVibrate", true)) {
            Notification notification = notification1;
            notification.defaults |= ONLINE_TYPE_IPHONE;
        }
        mNotificationManager.notify(NOTIFY_ID_MESSAGE, notification1);
    }

    public static void updateNotification(CharSequence text, String chatTitle, String userName, String userPhoto, boolean forceSound, int userId) {
        SharedPreferences nprefs = VKApplication.context.getSharedPreferences("notify", EVENT_MSG_DELETE);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(VKApplication.context);
        if (nprefs.getInt("dnd" + userId, EVENT_MSG_DELETE) > ((int) (System.currentTimeMillis() / 1000))) {
            Log.m525d("vk", "dnd for peer " + userId + " is active, is " + nprefs.getInt("dnd" + userId, EVENT_MSG_DELETE));
        } else if (prefs.getLong("dnd_end", 0) > System.currentTimeMillis()) {
            Log.m525d("vk", "global dnd is active");
        } else {
            String enabled = prefs.getString("notifyTypes", null);
            if (enabled != null && !Arrays.asList(enabled.split(";")).contains("messages")) {
                Log.m525d("vk", "message notifications disabled");
            } else if (!prefs.getBoolean("notifications", true)) {
                Log.m525d("vk", "all notifications disabled");
            } else if (ChatFragment.activeInstance == null || ChatFragment.activeInstance.getPeerID() != userId) {
                Log.m528i("vk", "NumNewMessages=" + numNewMessages);
                try {
                    NotificationManager mNotificationManager = (NotificationManager) VKApplication.context.getSystemService("notification");
                    MenuListView.counters.put("messages", Integer.valueOf(numNewMessages));
                    if (MenuListView.lastInstance != null) {
                        MenuListView.lastInstance.updateList();
                    }
                    if (numNewMessages == 0) {
                        prevNumNewMessages = EVENT_MSG_DELETE;
                        mNotificationManager.cancel(NOTIFY_ID_MESSAGE);
                        return;
                    }
                    CharSequence charSequence;
                    Object obj;
                    String string;
                    Bundle args = new Bundle();
                    args.putInt("id", userId);
                    String str = PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE;
                    if (chatTitle != null) {
                        charSequence = chatTitle;
                    } else {
                        obj = userName;
                    }
                    args.putCharSequence(str, charSequence);
                    if (userPhoto != null) {
                        args.putCharSequence("photo", userPhoto);
                    }
                    Intent notificationIntent = new Intent(VKApplication.context, FragmentWrapperActivity.class);
                    notificationIntent.putExtra("class", "ChatFragment");
                    notificationIntent.putExtra("args", args);
                    PendingIntent contentIntent = PendingIntent.getActivity(VKApplication.context, EVENT_MSG_DELETE, notificationIntent, 134217728);
                    Intent cancelIntent = new Intent(VKApplication.context, LongPollService.class);
                    cancelIntent.setAction("com.vkontakte.android.CANCEL_NOTIFICATION");
                    PendingIntent pendingCancel = PendingIntent.getService(VKApplication.context, EVENT_MSG_DELETE, cancelIntent, 134217728);
                    if (chatTitle != null) {
                        obj = new StringBuilder(String.valueOf(userName)).append(" ").append(VKApplication.context.getResources().getString(C0436R.string.notification_in_chat)).toString();
                    } else {
                        String str2 = userName;
                    }
                    String ticker = new StringBuilder(String.valueOf(obj)).append(": ").append(text).toString();
                    if (chatTitle != null) {
                        str = new StringBuilder(String.valueOf(userName)).append(" ").append(VKApplication.context.getResources().getString(C0436R.string.notification_in_chat)).toString();
                    } else {
                        str = userName;
                    }
                    if (chatTitle != null) {
                        Resources resources = VKApplication.context.getResources();
                        Object[] objArr = new Object[ONLINE_TYPE_MOBILE];
                        objArr[EVENT_MSG_DELETE] = chatTitle;
                        string = resources.getString(C0436R.string.notification_in_chat_summary, objArr);
                    } else {
                        string = " ";
                    }
                    notification1 = createNotification(text, str, userName, string, userPhoto, contentIntent, ticker);
                    Notification notification = notification1;
                    notification.flags |= MSG_CHAT;
                    Log.m528i("vk_longpoll", prevNumNewMessages + "->" + numNewMessages);
                    if (!nprefs.getBoolean("mute" + userId, DEBUG) && (forceSound || prevNumNewMessages < numNewMessages)) {
                        if (prefs.getString("notifyRingtone", System.DEFAULT_NOTIFICATION_URI.toString()).length() > 0) {
                            notification1.sound = Uri.parse(prefs.getString("notifyRingtone", System.DEFAULT_NOTIFICATION_URI.toString()));
                        }
                        if (prefs.getBoolean("notifyVibrate", true)) {
                            notification = notification1;
                            notification.defaults |= ONLINE_TYPE_IPHONE;
                        }
                        if (prefs.getBoolean("notifyLED", true)) {
                            notification = notification1;
                            notification.defaults |= ONLINE_TYPE_ANDROID;
                            notification = notification1;
                            notification.flags |= ONLINE_TYPE_MOBILE;
                        }
                    }
                    prevNumNewMessages = numNewMessages;
                    notification1.number = numNewMessages;
                    notification1.deleteIntent = pendingCancel;
                    mNotificationManager.cancel(NOTIFY_ID_MESSAGE);
                    mNotificationManager.notify(NOTIFY_ID_MESSAGE, notification1);
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                }
            } else {
                updateNotification();
            }
        }
    }

    private static Notification createNotification(CharSequence text, String title, String expandedTitle, String summary, String photoUrl, PendingIntent contentIntent, String tickerText) {
        String ticker;
        if (tickerText.length() > 50) {
            ticker = tickerText.substring(EVENT_MSG_DELETE, 50) + "...";
        } else {
            ticker = tickerText;
        }
        if (Integer.parseInt(VERSION.SDK) < 11) {
            Notification n = new Notification(C0436R.drawable.ic_stat_notify, ticker, System.currentTimeMillis());
            n.setLatestEventInfo(VKApplication.context, title, text, contentIntent);
            return n;
        }
        Bitmap photo;
        if (photoUrl != null) {
            photo = ImageCache.get(photoUrl);
        } else {
            photo = Global.getResBitmap(VKApplication.context.getResources(), C0436R.drawable.user_placeholder);
        }
        Builder builder = new Builder(VKApplication.context).setSmallIcon(C0436R.drawable.ic_stat_notify).setContentTitle(title).setContentText(text).setNumber(numNewMessages).setLargeIcon(Bitmap.createScaledBitmap(photo, Global.scale(64.0f), Global.scale(64.0f), true)).setContentIntent(contentIntent).setTicker(ticker);
        if (VERSION.SDK_INT >= MSG_CHAT) {
            return new BigTextStyle(builder).bigText(text).setBigContentTitle(expandedTitle).setSummaryText(summary).build();
        }
        return builder.getNotification();
    }

    public static void playNotification() {
        Notification n = new Notification();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(VKApplication.context);
        if (prefs.getLong("dnd_end", 0) < System.currentTimeMillis()) {
            if (prefs.getBoolean("notifySound", true)) {
                n.sound = Uri.parse(PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getString("notifyRingtone", System.DEFAULT_NOTIFICATION_URI.toString()));
            }
            if (prefs.getBoolean("notifyVibrate", true)) {
                n.defaults |= ONLINE_TYPE_IPHONE;
            }
        }
        ((NotificationManager) VKApplication.context.getSystemService("notification")).notify(11, n);
    }

    public void startSendingOnline() {
        TimerTask tt = new C02957();
        if (this.onlineTimer != null) {
            try {
                this.onlineTimer.cancel();
            } catch (Exception e) {
            }
        }
        this.onlineTimer = new Timer();
        this.onlineTimer.schedule(tt, 10000, 120000);
    }

    private void sendOnline() {
        if (longPollRunning && AppStateTracker.getCurrentActivity() != null && !PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getBoolean("invis", DEBUG)) {
            Log.m528i("vk", "Sending online.");
            ArrayList<String> posts = new ArrayList();
            ArrayList<String> reposts = new ArrayList();
            Posts.getViewedPosts(posts, reposts);
            Object[] objArr = new Object[ONLINE_TYPE_IPAD];
            objArr[EVENT_MSG_DELETE] = Integer.valueOf(VKApplication.context.getSharedPreferences(null, EVENT_MSG_DELETE).getInt("push_counter", EVENT_MSG_DELETE));
            objArr[ONLINE_TYPE_MOBILE] = TextUtils.join(",", posts);
            objArr[ONLINE_TYPE_IPHONE] = TextUtils.join(",", reposts);
            new APIRequest("execute").param("code", String.format("API.account.setOnline({push_count: %d});API.stats.viewPosts({post_ids: \"%s\", repost_ids: \"%s\"});", objArr)).handler(new C13088()).execSync();
        }
    }

    private void sendOffline() {
        if (Global.uid > 0) {
            Log.m528i("vk", "Sending offline.");
            ArrayList<String> posts = new ArrayList();
            ArrayList<String> reposts = new ArrayList();
            Posts.getViewedPosts(posts, reposts);
            Object[] objArr = new Object[ONLINE_TYPE_IPHONE];
            objArr[EVENT_MSG_DELETE] = TextUtils.join(",", posts);
            objArr[ONLINE_TYPE_MOBILE] = TextUtils.join(",", reposts);
            new APIRequest("execute").param("code", String.format("API.account.setOffline();API.stats.viewPosts({post_ids: \"%s\", repost_ids: \"%s\"});", objArr)).handler(new C13099()).exec();
        }
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.m528i("LocalService", "Received start id " + startId + ": " + intent);
        if ("com.vkontakte.android.CANCEL_NOTIFICATION".equals(intent.getAction())) {
            notification1 = null;
        }
        return ONLINE_TYPE_IPHONE;
    }

    public static void onNotificationOpened() {
        notification1 = null;
    }

    public void onCreate() {
        Log.m528i("vk_longpoll", "Starting LongPoll service.");
        Global.longPoll = this;
        this.newSystem = getSharedPreferences("longpoll", EVENT_MSG_DELETE).getBoolean("new_system", DEBUG);
        start();
    }

    public void onDestroy() {
        Log.m528i("vk_longpoll", "Stopping LongPoll service.");
        sendOffline();
        stop();
    }

    public static void logOut(boolean full) {
        Log.m528i("vk", "========== LOGOUT! " + full);
        if (Global.longPoll != null) {
            Global.longPoll.sendOffline();
            Global.longPoll.setNumUnread(EVENT_MSG_DELETE);
            Global.longPoll.stop();
        }
        if (Global.uid == 0 || Global.accessToken == null) {
            Log.m530w("vk", "Already logged out");
            return;
        }
        try {
            Messages.updateLock.acquire();
        } catch (Exception e) {
        }
        Messages.updateLock.release();
        String oldToken = VKApplication.context.getSharedPreferences(null, EVENT_MSG_DELETE).getString("c2dm_regID", null);
        if (oldToken != null) {
            VKApplication.context.getSharedPreferences(null, EVENT_MSG_DELETE).edit().remove("c2dm_regID").commit();
            new C2DMUnregisterDevice(oldToken).setBackground(true).exec();
        }
        C2DM.stop();
        SharedPreferences prefs = VKApplication.context.getSharedPreferences(null, EVENT_MSG_DELETE);
        VKApplication.context.stopService(new Intent(VKApplication.context, AudioPlayerService.class));
        if (full) {
            ((NotificationManager) VKApplication.context.getSystemService("notification")).cancelAll();
            prefs.edit().clear().commit();
            VKApplication.context.getSharedPreferences("drafts", EVENT_MSG_DELETE).edit().clear().commit();
            VKApplication.context.getSharedPreferences("news", EVENT_MSG_DELETE).edit().clear().commit();
            VKApplication.context.getSharedPreferences("longpoll", EVENT_MSG_DELETE).edit().clear().commit();
            VKApplication.context.getSharedPreferences("stickers", EVENT_MSG_DELETE).edit().clear().commit();
            VKApplication.context.deleteDatabase("posts.db");
            VKApplication.context.deleteDatabase("friends.db");
            VKApplication.context.deleteDatabase("dialogs.db");
            VKApplication.context.deleteDatabase("chats.db");
            VKApplication.context.deleteDatabase("groups.db");
            VKApplication.context.deleteDatabase("vk.db");
            Friends.reset();
            Messages.reset();
            Groups.reset();
            NotificationsView.reset();
            Global.inited = DEBUG;
            Global.friendHints = null;
            Global.accessToken = null;
            Global.uid = EVENT_MSG_DELETE;
            Global.longPoll = null;
            numNewMessages = EVENT_MSG_DELETE;
            numFriendRequests = EVENT_MSG_DELETE;
            numGroupInvitations = EVENT_MSG_DELETE;
            Posts.feed.clear();
            Posts.feedFrom = "0";
            Posts.feedItem = EVENT_MSG_DELETE;
            Posts.feedOffset = EVENT_MSG_DELETE;
            Posts.feedItemOffset = EVENT_MSG_DELETE;
            Posts.preloadedFeed.clear();
            new File(VKApplication.context.getCacheDir(), "replies").delete();
            if (full) {
                try {
                    AccountManager am = AccountManager.get(VKApplication.context);
                    am.removeAccount(am.getAccountsByType(Auth.ACCOUNT_TYPE)[EVENT_MSG_DELETE], null, null);
                    return;
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                    return;
                }
            }
            return;
        }
        if (Global.uid > 0) {
            prefs.edit().putInt("reauth_uid", Global.uid).commit();
        }
        prefs.edit().remove("uid").remove("sid").remove("secret").commit();
    }

    public static void onReauthError() {
        if (Global.uid != 0 && Global.accessToken != null) {
            logOut(DEBUG);
            try {
                Activity act = AppStateTracker.getCurrentActivity();
                if (act != null) {
                    Intent intent = new Intent(act, MainActivity.class);
                    intent.addFlags(67108864);
                    act.startActivity(intent);
                }
            } catch (Exception e) {
            }
        }
    }

    public void stopDelayed() {
        if (this.stopDelayTimer == null && !this.needStop) {
            try {
                this.stopDelayTimer = new Timer();
                this.stopDelayTimer.schedule(new TimerTask() {
                    public void run() {
                        LongPollService.this.stopDelayTimer = null;
                        LongPollService.this.stopSelf();
                    }
                }, 180000);
            } catch (Exception e) {
            }
        }
    }

    public void cancelDelayedStop() {
        if (this.stopDelayTimer != null) {
            this.stopDelayTimer.cancel();
            this.stopDelayTimer = null;
            return;
        }
        this.needStop = DEBUG;
        if (this.lpThread == null || !this.started) {
            start();
        }
    }
}
