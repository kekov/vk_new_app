package com.vkontakte.android;

public class AudioPlaylist {
    public int id;
    public String title;

    public AudioPlaylist(int _id, String _title) {
        this.id = _id;
        this.title = _title;
    }
}
