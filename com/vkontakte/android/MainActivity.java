package com.vkontakte.android;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.widget.ExploreByTouchHelper;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.text.TextUtils.TruncateAt;
import android.text.method.LinkMovementMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.MenuListView.Listener;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.data.Stickers;
import com.vkontakte.android.fragments.BackListener;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.fragments.NewsFragment;
import com.vkontakte.android.fragments.SettingsFragment;
import com.vkontakte.android.ui.MenuOverlayView;
import java.util.Stack;
import net.hockeyapp.android.Strings;
import org.acra.ACRAConstants;
import org.acra.ErrorReporter;

public class MainActivity extends SherlockFragmentActivity implements OnBackStackChangedListener {
    private Stack<ActionBarState> abStates;
    private FrameLayout contentView;
    private TextView counter;
    private long lastUpdatedCounters;
    private MenuOverlayView menu;
    private boolean needClearSettings;
    private int prevStackSize;
    private BroadcastReceiver receiver;
    private boolean showNewsOnResume;

    /* renamed from: com.vkontakte.android.MainActivity.1 */
    class C02961 extends BroadcastReceiver {
        C02961() {
        }

        public void onReceive(Context context, Intent intent) {
            if (LongPollService.ACTION_COUNTERS_UPDATED.equals(intent.getAction())) {
                MainActivity.this.updateCounter((LongPollService.numFriendRequests + LongPollService.numNewMessages) + LongPollService.numNotifications);
            }
        }
    }

    /* renamed from: com.vkontakte.android.MainActivity.3 */
    class C02973 implements Runnable {
        C02973() {
        }

        public void run() {
            MainActivity.this.updateCounter((LongPollService.numFriendRequests + LongPollService.numNewMessages) + LongPollService.numNotifications);
        }
    }

    /* renamed from: com.vkontakte.android.MainActivity.4 */
    class C02984 extends WebViewClient {
        C02984() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageFinished(WebView webView, String url) {
            CookieSyncManager.getInstance().sync();
            webView.destroy();
        }
    }

    /* renamed from: com.vkontakte.android.MainActivity.5 */
    class C02995 implements ImageGetter {
        private final /* synthetic */ Context val$context;

        C02995(Context context) {
            this.val$context = context;
        }

        public Drawable getDrawable(String arg0) {
            Drawable bmp = this.val$context.getResources().getDrawable(C0436R.drawable.ic_about);
            bmp.setBounds(0, 0, bmp.getIntrinsicWidth(), bmp.getIntrinsicHeight());
            return bmp;
        }
    }

    /* renamed from: com.vkontakte.android.MainActivity.6 */
    class C03006 implements OnTouchListener {
        int ccnt;
        private final /* synthetic */ Context val$context;

        C03006(Context context) {
            this.val$context = context;
            this.ccnt = 0;
        }

        public boolean onTouch(View v, MotionEvent event) {
            this.ccnt++;
            if (this.ccnt == 5) {
                this.val$context.getSharedPreferences(null, 0).edit().putBoolean("sinv", true).commit();
            }
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.MainActivity.7 */
    class C03017 implements Runnable {
        C03017() {
        }

        public void run() {
            try {
                int btnId = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
                if (btnId == 0) {
                    btnId = C0436R.id.abs__action_bar_title;
                }
                if (btnId != 0) {
                    MainActivity.this.setTextViewMarquee((TextView) MainActivity.this.findViewById(btnId));
                }
                btnId = Resources.getSystem().getIdentifier("action_bar_subtitle", "id", "android");
                if (btnId == 0) {
                    btnId = C0436R.id.abs__action_bar_subtitle;
                }
                if (btnId != 0) {
                    MainActivity.this.setTextViewMarquee((TextView) MainActivity.this.findViewById(btnId));
                }
                if (VERSION.SDK_INT < 17) {
                    btnId = Resources.getSystem().getIdentifier("action_bar", "id", "android");
                    if (btnId == 0) {
                        btnId = C0436R.id.abs__action_bar;
                    }
                    if (btnId != 0) {
                        ViewGroup vg = (ViewGroup) MainActivity.this.findViewById(btnId);
                        for (int i = 0; i < vg.getChildCount(); i++) {
                            if (vg.getChildAt(i) instanceof LinearLayout) {
                                vg.getChildAt(i).setBackgroundDrawable(null);
                                return;
                            }
                        }
                    }
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    private static class ActionBarState {
        int displayOpts;
        CharSequence title;

        private ActionBarState() {
        }
    }

    /* renamed from: com.vkontakte.android.MainActivity.2 */
    class C13102 implements Listener {
        C13102() {
        }

        public void onUserSelected(int id, boolean allowBack) {
            Bundle args = new Bundle();
            args.putInt("id", id);
            MainActivity.this.openFromMenu("ProfileFragment", args, allowBack);
        }

        public void onMenuItemSelected(int item, boolean allowBack) {
            String fragment = ACRAConstants.DEFAULT_STRING_VALUE;
            Bundle args = new Bundle();
            switch (item) {
                case -2000000000:
                    try {
                        fragment = MenuListView.reminderInfo.getCharSequence("_class").toString();
                        args = (Bundle) MenuListView.reminderInfo.clone();
                        break;
                    } catch (Throwable x) {
                        Log.m532w("vk", x);
                        break;
                    }
                case ValidationActivity.VRESULT_NONE /*0*/:
                    fragment = "ProfileFragment";
                    args.putInt("id", Global.uid);
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    fragment = "NewsFragment";
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    fragment = "FeedbackFragment";
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    fragment = "DialogsFragment";
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    fragment = "FriendsFragment";
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    fragment = "GroupsFragment";
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    fragment = "PhotoAlbumsListFragment";
                    args.putBoolean("news", true);
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    fragment = "VideoListFragment";
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    fragment = "AudioListFragment";
                    break;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    fragment = "FaveFragment";
                    break;
                case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                    fragment = "BrowseUsersFragment";
                    break;
                case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                    fragment = "_settings";
                    break;
            }
            if (fragment != null && fragment.length() > 0) {
                MainActivity.this.openFromMenu(fragment, args, allowBack);
            }
        }

        public void onCommunitySelected(int id, boolean allowBack) {
            Bundle args = new Bundle();
            args.putInt("id", -id);
            MainActivity.this.openFromMenu("ProfileFragment", args, allowBack);
        }
    }

    public MainActivity() {
        this.showNewsOnResume = false;
        this.lastUpdatedCounters = 0;
        this.prevStackSize = 0;
        this.abStates = new Stack();
        this.needClearSettings = false;
        this.receiver = new C02961();
    }

    static {
        NetworkInfo info = ((ConnectivityManager) VKApplication.context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info != null && info.isAvailable()) {
            NetworkStateReceiver.isConnected = true;
            NetworkStateReceiver.updateInfo(VKApplication.context);
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        C2DM.checkForUpdate();
        this.contentView = new FrameLayout(this);
        this.contentView.setId(C0436R.id.fragment_wrapper);
        setContentView(this.contentView);
        getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
        if (VERSION.SDK_INT < 9) {
            getWindow().setFormat(1);
        }
        if (VERSION.SDK_INT < 8) {
            new Builder(this).setMessage("\u041f\u0440\u0438\u043b\u043e\u0436\u0435\u043d\u0438\u0435 \u043d\u0435 \u0440\u0430\u0431\u043e\u0442\u0430\u0435\u0442 \u043d\u0430 \u0442\u0430\u043a\u043e\u0439 \u0441\u0442\u0430\u0440\u043e\u0439 \u0432\u0435\u0440\u0441\u0438\u0438 Android.").setTitle(C0436R.string.error).setPositiveButton(C0436R.string.ok, null).show();
        }
        this.menu = new MenuOverlayView(this);
        FrameLayout decorView = (FrameLayout) getWindow().getDecorView();
        View vr = decorView.getChildAt(0);
        decorView.removeViewAt(0);
        this.menu.addView(vr);
        decorView.addView(this.menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.menu.getListView().setListener(new C13102());
        getSupportActionBar().setIcon((int) C0436R.drawable.ic_left_menu);
        int btnId = Resources.getSystem().getIdentifier("action_bar_container", "id", "android");
        if (btnId == 0) {
            btnId = C0436R.id.abs__action_bar_container;
        }
        this.counter = new TextView(this);
        this.counter.setId(C0436R.id.badge);
        this.counter.setBackgroundResource(C0436R.drawable.badge_head);
        this.counter.setTextColor(-12621929);
        this.counter.setTypeface(Typeface.DEFAULT_BOLD);
        this.counter.setTextSize(1, 13.0f);
        this.counter.setVisibility(8);
        this.counter.setPadding(this.counter.getPaddingLeft(), Global.scale(GroundOverlayOptions.NO_DIMENSION), this.counter.getPaddingRight(), this.counter.getPaddingBottom());
        try {
            ((FrameLayout) findViewById(btnId)).addView(this.counter, new LayoutParams(-2, Global.scale(19.0f), 51));
        } catch (Exception e) {
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(LongPollService.ACTION_COUNTERS_UPDATED);
        registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
        updateMenuMode();
        SharedPreferences prefs = getApplicationContext().getSharedPreferences(null, 0);
        if (prefs.contains("sid")) {
            if (prefs.contains("uid")) {
                if (prefs.getInt("uid", 0) > 0) {
                    ErrorReporter.getInstance().putCustomData("vk_uid", new StringBuilder(String.valueOf(Global.uid)).toString());
                    if (getIntent().hasExtra("class")) {
                        String fn = getIntent().getStringExtra("class");
                        if (!"_settings".equals(fn)) {
                            try {
                                if (this.needClearSettings) {
                                    Fragment settings = getFragmentManager().findFragmentByTag("settings");
                                    if (settings != null) {
                                        getFragmentManager().beginTransaction().remove(settings).commit();
                                    }
                                    this.needClearSettings = false;
                                }
                                SherlockFragment fragment = (SherlockFragment) Class.forName("com.vkontakte.android.fragments." + fn).newInstance();
                                fragment.setArguments(getIntent().getBundleExtra("args"));
                                getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, fragment, "news").commit();
                            } catch (Throwable x) {
                                Log.m527e("vk", "Error starting fragment! " + fn, x);
                                Toast.makeText(this, C0436R.string.error, 0).show();
                                finish();
                            }
                        } else if (VERSION.SDK_INT >= 11) {
                            SherlockFragment sf = new SherlockFragment();
                            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, sf, "content").commit();
                            Fragment f = new SettingsFragment();
                            getFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, f, "settings").commit();
                            this.needClearSettings = true;
                            return;
                        } else {
                            startActivity(new Intent(this, SettingsActivity.class));
                        }
                    } else {
                        if (b == null) {
                            NewsFragment fragment2 = new NewsFragment();
                            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, fragment2, "news").commit();
                        }
                        setTitleMarquee();
                    }
                    startService(new Intent(this, LongPollService.class));
                    Friends.reload(false);
                    Groups.reload(false);
                    this.contentView.post(new C02973());
                    getSupportFragmentManager().addOnBackStackChangedListener(this);
                    try {
                        WebView webView = new WebView(this);
                        CookieSyncManager.createInstance(VKApplication.context);
                        CookieSyncManager.getInstance().sync();
                        webView.getSettings().setBuiltInZoomControls(false);
                        webView.setWebViewClient(new C02984());
                        webView.loadUrl("http://m.vk.com/counters.php");
                    } catch (Exception e2) {
                    }
                    setTitleMarquee();
                    checkForIntro();
                    return;
                }
            }
        }
        startActivityForResult(new Intent(this, AuthActivity.class), 100);
    }

    private void openFromMenu(String fclass, Bundle args, boolean back) {
        this.menu.closeMenu();
        if (back) {
            Navigate.to(fclass, args, this);
        } else if (!"_settings".equals(fclass)) {
            if (args != null) {
                args.putBoolean("_from_menu", true);
            }
            try {
                if (this.needClearSettings) {
                    Fragment settings = getFragmentManager().findFragmentByTag("settings");
                    if (settings != null) {
                        getFragmentManager().beginTransaction().remove(settings).commit();
                    }
                    this.needClearSettings = false;
                }
                SherlockFragment fragment = (SherlockFragment) Class.forName("com.vkontakte.android.fragments." + fclass).newInstance();
                fragment.setArguments(args);
                getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, fragment, "news").commit();
            } catch (Exception x) {
                Log.m527e("vk", "Error starting fragment! " + fclass, x);
                Toast.makeText(this, C0436R.string.error, 0).show();
                finish();
            }
        } else if (VERSION.SDK_INT >= 11) {
            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, new SherlockFragment(), "content").commit();
            getFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, new SettingsFragment(), "settings").commit();
            this.needClearSettings = true;
        } else {
            startActivity(new Intent(this, SettingsActivity.class));
        }
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
    }

    public void onConfigurationChanged(Configuration cfg) {
        super.onConfigurationChanged(cfg);
        updateMenuMode();
        setTitleMarquee();
    }

    public void onResume() {
        super.onResume();
        AppStateTracker.onActivityResumed(this);
        if (this.showNewsOnResume) {
            this.showNewsOnResume = false;
            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, new NewsFragment(), "news").commit();
        }
        if (Global.longPoll != null && System.currentTimeMillis() - this.lastUpdatedCounters > 10000) {
            LongPollService longPollService = Global.longPoll;
            LongPollService.updateCounters();
            this.lastUpdatedCounters = System.currentTimeMillis();
        }
        NetworkStateReceiver.getNotifications(this);
    }

    private void checkForIntro() {
        int intro = getSharedPreferences(null, 0).getInt("intro", 0);
        if ((intro & 1) > 0 || (intro & 2) > 0) {
            Intent intent = new Intent(this, SuggestionsActivity.class);
            if ((intro & 1) == 0) {
                intent.putExtra("groups", true);
            }
            startActivityForResult(intent, 101);
        }
    }

    public void onPause() {
        super.onPause();
        AppStateTracker.onActivityPaused();
    }

    private void updateMenuMode() {
        if (Global.isTablet) {
            updateCounter((LongPollService.numFriendRequests + LongPollService.numNewMessages) + LongPollService.numNotifications);
            if (getResources().getDisplayMetrics().widthPixels <= getResources().getDisplayMetrics().heightPixels || (getResources().getConfiguration().screenLayout & 15) != 4) {
                this.menu.setMode(0);
                getSupportActionBar().setIcon((int) C0436R.drawable.ic_left_menu);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                return;
            }
            this.menu.setMode(2);
            getSupportActionBar().setIcon((int) C0436R.drawable.ic_ab_app);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    private void updateCounter(int cnt) {
        int i = 0;
        if (this.counter != null && this.counter.getParent() != null) {
            if (Global.isTablet && getResources().getDisplayMetrics().widthPixels > getResources().getDisplayMetrics().heightPixels && (getResources().getConfiguration().screenLayout & 15) == 4) {
                this.counter.setVisibility(8);
                return;
            }
            String text = null;
            if (cnt < LocationStatusCodes.GEOFENCE_NOT_AVAILABLE) {
                text = new StringBuilder(String.valueOf(cnt)).toString();
            } else if (cnt >= LocationStatusCodes.GEOFENCE_NOT_AVAILABLE && cnt < 1000000) {
                text = (cnt / LocationStatusCodes.GEOFENCE_NOT_AVAILABLE) + "K";
            } else if (cnt >= 1000000) {
                text = String.format("%.1fM", new Object[]{Float.valueOf(((float) cnt) / 1000000.0f)});
            }
            if (text == null) {
                text = ACRAConstants.DEFAULT_STRING_VALUE;
                cnt = 0;
            }
            this.counter.setText(text);
            TextView textView = this.counter;
            if (cnt <= 0) {
                i = 8;
            }
            textView.setVisibility(i);
            if (cnt > 0) {
                try {
                    int[] xy = new int[2];
                    LayoutParams lp = new LayoutParams(-2, Global.scale(19.0f), 51);
                    this.counter.measure(MeasureSpec.makeMeasureSpec(Global.scale(100.0f), ExploreByTouchHelper.INVALID_ID), MeasureSpec.makeMeasureSpec(Global.scale(100.0f), ExploreByTouchHelper.INVALID_ID));
                    int cw = this.counter.getMeasuredWidth();
                    Log.m528i("vk", "m width=" + cw);
                    int btnId = Resources.getSystem().getIdentifier("action_bar_container", "id", "android");
                    if (btnId == 0) {
                        btnId = C0436R.id.abs__action_bar_container;
                    }
                    findViewById(btnId).getLocationInWindow(xy);
                    int offsetX = xy[0];
                    btnId = Resources.getSystem().getIdentifier("home", "id", "android");
                    if (btnId == 0) {
                        btnId = C0436R.id.abs__home;
                    }
                    View v = findViewById(btnId);
                    v.getLocationInWindow(xy);
                    Rect rect = new Rect();
                    getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
                    lp.topMargin = xy[1] - rect.top;
                    if (Global.scale(8.0f) + cw > v.getWidth()) {
                        lp.leftMargin = xy[0] + Global.scale(8.0f);
                    } else {
                        lp.leftMargin = (xy[0] + v.getWidth()) - this.counter.getMeasuredWidth();
                    }
                    lp.leftMargin -= offsetX;
                    this.counter.setLayoutParams(lp);
                } catch (Exception e) {
                }
            }
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            this.menu.openMenu();
        }
        return super.onOptionsItemSelected(item);
    }

    public static void showAbout(Context context) {
        try {
            ImageGetter igetter = new C02995(context);
            Builder dlg = new VKAlertDialog.Builder(context);
            if (Integer.parseInt(VERSION.SDK) > 10) {
                dlg = (Builder) dlg.getClass().getConstructor(new Class[]{Context.class, Integer.TYPE}).newInstance(new Object[]{context, Integer.valueOf(2)});
            }
            dlg.setTitle(context.getResources().getString(C0436R.string.menu_about));
            dlg.setMessage(Html.fromHtml("<br/><img src='1'/><br/>" + context.getResources().getString(C0436R.string.about_text, new Object[]{context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName, Integer.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode)}), igetter, null));
            dlg.setPositiveButton("OK", null);
            if (VERSION.SDK_INT < 14) {
                dlg.setIcon(17301659);
            }
            AlertDialog d = dlg.show();
            d.getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
            ((TextView) d.findViewById(16908299)).setLinkTextColor(-6300676);
            ((TextView) d.findViewById(16908299)).setHighlightColor(-2137007108);
            ((TextView) d.findViewById(16908299)).setGravity(1);
            ((TextView) d.findViewById(16908299)).setMovementMethod(LinkMovementMethod.getInstance());
            d.findViewById(16908299).setOnTouchListener(new C03006(context));
        } catch (Exception e) {
        }
    }

    public void restartAfterLogout() {
        finish();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == -1) {
                SharedPreferences prefs = getApplicationContext().getSharedPreferences(null, 0);
                NetworkStateReceiver.updateInfo(this);
                this.menu.updateInfo();
                if (prefs.getBoolean("enableC2DM", true)) {
                    C2DM.start();
                }
                startService(new Intent(this, LongPollService.class));
                this.showNewsOnResume = true;
                Friends.reload(false);
                Groups.reload(false);
                Stickers.updateInfo();
                if (Global.longPoll != null) {
                    LongPollService longPollService = Global.longPoll;
                    LongPollService.updateCounters();
                }
                checkForIntro();
            } else {
                finish();
            }
        }
        if (requestCode == 101 && resultCode != -1) {
            finish();
        }
    }

    public void setTitle(CharSequence title) {
        super.setTitle(title);
        setTitleMarquee();
    }

    public void setTitle(int res) {
        super.setTitle(res);
        setTitleMarquee();
    }

    private void setTitleMarquee() {
        if (this.contentView != null) {
            this.contentView.postDelayed(new C03017(), 200);
        }
    }

    private void setTextViewMarquee(TextView t) {
        if (t != null) {
            t.setEllipsize(TruncateAt.MARQUEE);
            t.setSelected(true);
            t.setHorizontalFadingEdgeEnabled(true);
            t.setFadingEdgeLength(Global.scale(10.0f));
            t.setMarqueeRepeatLimit(2);
        }
    }

    public void onBackPressed() {
        android.support.v4.app.Fragment f = getSupportFragmentManager().findFragmentById(C0436R.id.fragment_wrapper);
        if (f == null || !(f instanceof BackListener) || !((BackListener) f).onBackPressed()) {
            super.onBackPressed();
        }
    }

    public void onBackStackChanged() {
        ActionBarState st;
        if (this.prevStackSize > getSupportFragmentManager().getBackStackEntryCount()) {
            st = (ActionBarState) this.abStates.pop();
            setTitle(st.title);
            getSupportActionBar().setDisplayOptions(st.displayOpts);
        } else {
            st = new ActionBarState();
            st.title = getTitle();
            st.displayOpts = getSupportActionBar().getDisplayOptions();
            this.abStates.push(st);
        }
        this.prevStackSize = getSupportFragmentManager().getBackStackEntryCount();
    }
}
