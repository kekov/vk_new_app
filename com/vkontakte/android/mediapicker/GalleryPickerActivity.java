package com.vkontakte.android.mediapicker;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.mediapicker.entries.ActionCallback;
import com.vkontakte.android.mediapicker.entries.AlbumEntry;
import com.vkontakte.android.mediapicker.entries.AlbumsListCallback;
import com.vkontakte.android.mediapicker.entries.BitmapEntry;
import com.vkontakte.android.mediapicker.entries.GalleryContext;
import com.vkontakte.android.mediapicker.entries.IVCallback;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.entries.ImagesGridCallback;
import com.vkontakte.android.mediapicker.gallery.AlbumsListFragment;
import com.vkontakte.android.mediapicker.gallery.ImageViewerFragment;
import com.vkontakte.android.mediapicker.gallery.ImagesGridFragment;
import com.vkontakte.android.mediapicker.gallery.InlineUtils;
import com.vkontakte.android.mediapicker.gl.ImageProcessor;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.providers.LangProvider;
import com.vkontakte.android.mediapicker.providers.ResourceProvider.Views;
import com.vkontakte.android.mediapicker.ui.FiltersListView;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.CropperCallback;
import com.vkontakte.android.mediapicker.ui.GalleryPickerHeaderView;
import com.vkontakte.android.mediapicker.ui.GalleryPickerHeaderView.Callback;
import com.vkontakte.android.mediapicker.ui.LocalImageView;
import com.vkontakte.android.mediapicker.ui.holders.AlbumViewHolder;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.mediapicker.utils.CameraUtils;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.LocalImageLoader;
import com.vkontakte.android.mediapicker.utils.Loggable;
import com.vkontakte.android.mediapicker.utils.OnClickTouchListener;
import com.vkontakte.android.mediapicker.utils.SelectionContext;
import com.vkontakte.android.mediapicker.utils.StrictCache;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.acra.ACRAConstants;

public class GalleryPickerActivity extends ActivityClassProvider {
    private static final float BACKGROUND_SCALE = 0.965f;
    private static final boolean DEBUG_OLD_SDK = false;
    private static final int TEMP_CAMERA_ALBUM_ID = 0;
    private ImageView checkView;
    private FrameLayout checkWrap;
    private int chosenOption;
    private boolean clearThumbsOnExit;
    private FrameLayout contentView;
    private CropperCallback cropperCallback;
    private int currentOrientation;
    private CropperCallback doneCropperCallback;
    private FiltersListView filtersView;
    public GalleryPickerFooterView footerView;
    private FrameLayout frameView;
    private GalleryContext galleryContext;
    private GalleryPickerHeaderView headerView;
    private IVCallback imageViewerCallback;
    private ImagesGridCallback imagesGridCallback;
    private boolean isBlocked;
    boolean isGoingBackToGrid;
    public boolean noAlbumsList;
    public boolean noImagesGrid;
    private OnTouchListener onCheckTouchListener;
    private FrameLayout photoView;
    int rectUpdates;
    private boolean showingAlbum;
    public boolean showingViewer;

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.10 */
    class AnonymousClass10 implements Runnable {
        private final /* synthetic */ AlbumEntry val$album;

        AnonymousClass10(AlbumEntry albumEntry) {
            this.val$album = albumEntry;
        }

        public void run() {
            View view = GalleryPickerActivity.this.getAlbumView(GalleryPickerActivity.this.galleryContext.albumsFragment.getAlbumIndex(this.val$album));
            if (view != null) {
                AlbumViewHolder holder = (AlbumViewHolder) view.getTag();
                if (holder != null) {
                    holder.updateImage(this.val$album.getPreview());
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.11 */
    class AnonymousClass11 implements Runnable {
        private final /* synthetic */ int val$index;

        AnonymousClass11(int i) {
            this.val$index = i;
        }

        public void run() {
            if (GalleryPickerActivity.this.galleryContext.currentAlbumFragment != null) {
                View view = GalleryPickerActivity.this.getImageView(this.val$index);
                if (view != null) {
                    ImageViewHolder holder = (ImageViewHolder) view.getTag();
                    if (holder != null) {
                        holder.updateImage(GalleryPickerActivity.this.galleryContext.currentAlbumFragment.getImageAt(0));
                    }
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.12 */
    class AnonymousClass12 implements Runnable {
        private final /* synthetic */ int val$albumIndex;
        private final /* synthetic */ ImageEntry val$image;
        private final /* synthetic */ int val$index;

        AnonymousClass12(int i, ImageEntry imageEntry, int i2) {
            this.val$index = i;
            this.val$image = imageEntry;
            this.val$albumIndex = i2;
        }

        public void run() {
            View view = GalleryPickerActivity.this.getImageView(this.val$index);
            if (view != null) {
                ImageViewHolder holder = (ImageViewHolder) view.getTag();
                if (holder != null) {
                    holder.updateImage(this.val$image);
                }
            }
            if (this.val$index == 0) {
                view = GalleryPickerActivity.this.getAlbumView(this.val$albumIndex);
                if (view != null) {
                    AlbumViewHolder holder2 = (AlbumViewHolder) view.getTag();
                    if (holder2 != null) {
                        holder2.updateImage(this.val$image);
                    }
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.13 */
    class AnonymousClass13 implements Runnable {
        private final /* synthetic */ Runnable val$updater;

        AnonymousClass13(Runnable runnable) {
            this.val$updater = runnable;
        }

        public void run() {
            GalleryPickerActivity.this.runOnUiThread(this.val$updater);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.14 */
    class AnonymousClass14 implements Runnable {
        private final /* synthetic */ CropperCallback val$callback;
        private final /* synthetic */ float[] val$coords;
        private final /* synthetic */ ImageEntry val$image;

        AnonymousClass14(float[] fArr, ImageEntry imageEntry, CropperCallback cropperCallback) {
            this.val$coords = fArr;
            this.val$image = imageEntry;
            this.val$callback = cropperCallback;
        }

        public void run() {
            boolean z;
            if (this.val$coords == null) {
                GalleryPickerActivity.this.galleryContext.currentViewerFragment.applyEmptyCropDataToViewer();
            } else {
                GalleryPickerActivity.this.galleryContext.currentViewerFragment.applyCropDataToViewer(this.val$coords, this.val$image);
            }
            ImageViewerFragment imageViewerFragment = GalleryPickerActivity.this.galleryContext.currentViewerFragment;
            if (this.val$coords != null) {
                z = true;
            } else {
                z = false;
            }
            imageViewerFragment.showCropper(z);
            GalleryPickerActivity.this.footerView.displayCropperAction(this.val$callback != null ? this.val$callback : GalleryPickerActivity.this.cropperCallback, this.val$callback != null ? 6 : 7, this.val$callback != null ? 2 : 5);
            GalleryPickerActivity.this.filtersView.updateForImageViewer(false);
            GalleryPickerActivity.this.showBadge(true);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.15 */
    class AnonymousClass15 implements Runnable {
        private final /* synthetic */ Runnable val$block;

        AnonymousClass15(Runnable runnable) {
            this.val$block = runnable;
        }

        public void run() {
            GalleryPickerActivity.this.setIsBlocked(false);
            this.val$block.run();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.17 */
    class AnonymousClass17 implements Runnable {
        private final /* synthetic */ float[] val$coords;
        private final /* synthetic */ ImageEntry val$image;

        /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.17.1 */
        class C08731 implements Runnable {
            private final /* synthetic */ ImageEntry val$image;

            C08731(ImageEntry imageEntry) {
                this.val$image = imageEntry;
            }

            public void run() {
                GalleryPickerActivity.this.footerView.updateEditorActionEnabled(Views.ACTION_CROP, true);
                GalleryPickerActivity.this.galleryContext.currentViewerFragment.setPreventInvalidateViewer(false);
                GalleryPickerActivity.this.galleryContext.currentViewerFragment.animateCrop();
                ImageProcessor.instance().updateStyledThumbAsync((Bitmap) this.val$image.getImageData().get(), this.val$image);
                GalleryPickerActivity.this.prepareCropper(false, true);
            }
        }

        /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.17.2 */
        class C15212 extends ActionCallback<Bitmap> {
            private final /* synthetic */ Runnable val$after;
            private final /* synthetic */ ImageEntry val$image;

            C15212(ImageEntry imageEntry, Runnable runnable) {
                this.val$image = imageEntry;
                this.val$after = runnable;
            }

            public void run(Bitmap result) {
                this.val$image.setImageData(result);
                this.val$after.run();
            }
        }

        AnonymousClass17(ImageEntry imageEntry, float[] fArr) {
            this.val$image = imageEntry;
            this.val$coords = fArr;
        }

        public void run() {
            boolean z = false;
            GalleryPickerActivity.this.footerView.hideCropperAction();
            if (GalleryPickerActivity.this.galleryContext.currentViewerFragment != null) {
                boolean z2;
                GalleryPickerActivity.this.galleryContext.currentViewerFragment.setPreventInvalidateViewer(true);
                ActionCallback<Bitmap> callback = new C15212(this.val$image, new C08731(this.val$image));
                String str = "Crop data set: %b, coords: %b";
                Object[] objArr = new Object[2];
                if (this.val$image.getIsStyled() && this.val$image.getStyle().getIsCropped()) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                objArr[0] = Boolean.valueOf(z2);
                if (this.val$coords == null) {
                    z = true;
                }
                objArr[1] = Boolean.valueOf(z);
                Loggable.Error(str, objArr);
                ImageProcessor.instance().crop((Bitmap) this.val$image.getImageData().get(), this.val$coords, callback);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.18 */
    class AnonymousClass18 implements Runnable {
        private final /* synthetic */ boolean val$crop;
        private final /* synthetic */ boolean val$toShow;

        AnonymousClass18(boolean z, boolean z2) {
            this.val$toShow = z;
            this.val$crop = z2;
        }

        public void run() {
            GalleryPickerActivity.this.galleryContext.currentViewerFragment.setSwitchEnabled(!this.val$toShow);
            GalleryPickerActivity.this.showCheck(this.val$toShow);
            GalleryPickerActivity.this.updateThumbnail(GalleryPickerActivity.this.galleryContext.currentImageIndex);
            if (this.val$toShow) {
                GalleryPickerActivity.this.blockOrientation();
                GalleryPickerActivity.this.showCropper(this.val$crop, !this.val$crop ? GalleryPickerActivity.this.doneCropperCallback : null);
                GalleryPickerActivity.this.getFiltersView().updateForImageViewer(false);
                return;
            }
            GalleryPickerActivity.this.unblockOrientation();
            GalleryPickerActivity.this.hideCropper();
            GalleryPickerActivity.this.getFiltersView().updateForImageViewer(true);
            GalleryPickerActivity.this.showBadge(false);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.19 */
    class AnonymousClass19 implements Runnable {
        private final /* synthetic */ boolean val$doApply;
        private final /* synthetic */ ImageEntry val$image;
        private final /* synthetic */ int val$imageIndex;

        /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.19.1 */
        class C15221 extends ActionCallback<BitmapEntry> {
            private final /* synthetic */ ImageEntry val$image;
            private final /* synthetic */ int val$imageIndex;

            C15221(ImageEntry imageEntry, int i) {
                this.val$image = imageEntry;
                this.val$imageIndex = i;
            }

            public void run(BitmapEntry result) {
                this.val$image.setImageData(result);
                if (GalleryPickerActivity.this.galleryContext.currentViewerFragment != null) {
                    GalleryPickerActivity.this.galleryContext.currentViewerFragment.setPreventInvalidateViewer(false);
                }
                InlineUtils.updateThumbnailForImage(this.val$image.getIsStyled() ? (Bitmap) result.get() : null, this.val$imageIndex, this.val$image, GalleryPickerActivity.this);
            }
        }

        AnonymousClass19(boolean z, ImageEntry imageEntry, int i) {
            this.val$doApply = z;
            this.val$image = imageEntry;
            this.val$imageIndex = i;
        }

        public void run() {
            if (this.val$doApply) {
                InlineUtils.applyEnhance(GalleryPickerActivity.this, this.val$image, this.val$imageIndex);
                return;
            }
            if (this.val$image.getIsStyled()) {
                this.val$image.getStyle(true).setEnhanced(false);
                this.val$image.checkStyleTopicality();
            }
            LocalImageLoader.instance().getImage(this.val$image, false, false, new C15221(this.val$image, this.val$imageIndex), false, -1, 1);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.20 */
    class AnonymousClass20 implements Runnable {
        private final /* synthetic */ String val$rawText;
        private final /* synthetic */ EditText val$textView;

        AnonymousClass20(EditText editText, String str) {
            this.val$textView = editText;
            this.val$rawText = str;
        }

        public void run() {
            boolean z = false;
            String result = this.val$textView.getText().toString();
            boolean isTexted;
            if (result.length() > 0) {
                isTexted = true;
            } else {
                isTexted = false;
            }
            if (GalleryPickerActivity.this.galleryContext.currentImage == null) {
                return;
            }
            if (!GalleryPickerActivity.this.galleryContext.currentImage.getIsStyled() || !GalleryPickerActivity.this.galleryContext.currentImage.getStyle().getIsTexted() || !isTexted || this.val$rawText == null || !this.val$rawText.equals(result)) {
                GalleryPickerActivity galleryPickerActivity = GalleryPickerActivity.this;
                if (!(GalleryPickerActivity.this.galleryContext.currentImage.getIsStyled() && GalleryPickerActivity.this.galleryContext.currentImage.getStyle().getIsTexted())) {
                    z = true;
                }
                InlineUtils.applyTextToImage(galleryPickerActivity, z, result, GalleryPickerActivity.this.galleryContext.currentImage);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.21 */
    class AnonymousClass21 implements TextWatcher {
        boolean blocked;
        private final /* synthetic */ EditText val$textView;

        AnonymousClass21(EditText editText) {
            this.val$textView = editText;
            this.blocked = false;
        }

        public void beforeTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
        }

        public void onTextChanged(CharSequence inputString, int i1, int i2, int i3) {
            if (!this.blocked) {
                this.blocked = true;
                String input = inputString.toString();
                if (input.length() > 0) {
                    GalleryPickerUtils.instance().clearTextInput(this.val$textView, input);
                }
                this.blocked = false;
            }
        }

        public void afterTextChanged(Editable editable) {
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.22 */
    class AnonymousClass22 implements OnGlobalLayoutListener {
        private final /* synthetic */ EditText val$textView;

        AnonymousClass22(EditText editText) {
            this.val$textView = editText;
        }

        public void onGlobalLayout() {
            this.val$textView.requestFocus();
            ((InputMethodManager) ActivityClassProvider.getStaticLocalContext().getSystemService("input_method")).showSoftInput(this.val$textView, 0);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.24 */
    class AnonymousClass24 implements OnClickListener {
        private final /* synthetic */ Runnable val$applier;

        AnonymousClass24(Runnable runnable) {
            this.val$applier = runnable;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            this.val$applier.run();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.25 */
    class AnonymousClass25 implements OnPreDrawListener {
        private final /* synthetic */ Runnable val$applier;
        private final /* synthetic */ AlertDialog[] val$dialog;
        private final /* synthetic */ String val$rawText;
        private final /* synthetic */ EditText val$textView;
        private final /* synthetic */ TextWatcher val$watcher;

        /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.25.1 */
        class C08741 implements OnEditorActionListener {
            private final /* synthetic */ Runnable val$applier;
            private final /* synthetic */ AlertDialog[] val$dialog;

            C08741(Runnable runnable, AlertDialog[] alertDialogArr) {
                this.val$applier = runnable;
                this.val$dialog = alertDialogArr;
            }

            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == 6 || (actionId == 0 && event != null && event.getAction() == 0)) {
                    this.val$applier.run();
                    this.val$dialog[0].dismiss();
                }
                return true;
            }
        }

        AnonymousClass25(EditText editText, String str, TextWatcher textWatcher, Runnable runnable, AlertDialog[] alertDialogArr) {
            this.val$textView = editText;
            this.val$rawText = str;
            this.val$watcher = textWatcher;
            this.val$applier = runnable;
            this.val$dialog = alertDialogArr;
        }

        public boolean onPreDraw() {
            this.val$textView.getViewTreeObserver().removeOnPreDrawListener(this);
            if (GalleryPickerActivity.this.galleryContext.currentImage != null && GalleryPickerActivity.this.galleryContext.currentImage.getIsStyled() && GalleryPickerActivity.this.galleryContext.currentImage.getStyle().getIsTexted()) {
                this.val$textView.setText(this.val$rawText);
                this.val$textView.setSelection(this.val$textView.getText().length());
            }
            this.val$textView.addTextChangedListener(this.val$watcher);
            this.val$textView.setOnEditorActionListener(new C08741(this.val$applier, this.val$dialog));
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.31 */
    class AnonymousClass31 implements Runnable {
        private final /* synthetic */ AlbumEntry val$album;
        private final /* synthetic */ ImageEntry val$image;

        AnonymousClass31(ImageEntry imageEntry, AlbumEntry albumEntry) {
            this.val$image = imageEntry;
            this.val$album = albumEntry;
        }

        public void run() {
            View view = GalleryPickerActivity.this.getImageView(0);
            GalleryPickerActivity.this.galleryContext.currentImage = this.val$image;
            GalleryPickerActivity.this.galleryContext.currentImageIndex = 0;
            GalleryPickerActivity.this.openImage(0, this.val$image, view, this.val$album.getImages(), false, true);
            if (GalleryPickerUtils.getAreFiltersSupported()) {
                GalleryPickerActivity.this.footerView.showFilters();
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.32 */
    class AnonymousClass32 implements Runnable {
        private final /* synthetic */ boolean val$hide;

        AnonymousClass32(boolean z) {
            this.val$hide = z;
        }

        public void run() {
            if (this.val$hide) {
                GalleryPickerActivity.this.checkWrap.setVisibility(8);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.6 */
    class C08796 implements Runnable {

        /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.6.1 */
        class C08781 implements Runnable {
            private final /* synthetic */ ImageEntry val$image;

            C08781(ImageEntry imageEntry) {
                this.val$image = imageEntry;
            }

            public void run() {
                GalleryPickerActivity.this.addNewImageToCameraAlbum(this.val$image);
            }
        }

        C08796() {
        }

        public void run() {
            File file = CameraUtils.getCurrentPhotoFile();
            if (file != null) {
                CameraUtils.addImageToGallery(GalleryPickerActivity.this);
                ImageEntry image = new ImageEntry(GalleryPickerActivity.this.galleryContext.currentCameraBucketId, file.getAbsolutePath(), true, 0);
                LocalImageLoader.instance().setOrientationByPath(image, file);
                GalleryPickerActivity.this.runOnUiThread(new C08781(image));
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.8 */
    class C08808 implements View.OnClickListener {
        C08808() {
        }

        public void onClick(View view) {
            GalleryPickerActivity.this.blockOrientation();
            CameraUtils.launchCamera(GalleryPickerActivity.this);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.16 */
    class AnonymousClass16 extends ActionCallback<BitmapEntry> {
        private final /* synthetic */ Runnable val$after;
        private final /* synthetic */ ImageEntry val$image;
        private final /* synthetic */ int val$index;
        private final /* synthetic */ boolean val$noInvalidateReset;

        AnonymousClass16(ImageEntry imageEntry, boolean z, Runnable runnable, int i) {
            this.val$image = imageEntry;
            this.val$noInvalidateReset = z;
            this.val$after = runnable;
            this.val$index = i;
        }

        public void run(BitmapEntry result) {
            if (!(result == null || result.get() == null)) {
                this.val$image.setImageData(result);
            }
            if (GalleryPickerActivity.this.galleryContext.currentViewerFragment != null) {
                if (!this.val$noInvalidateReset) {
                    GalleryPickerActivity.this.galleryContext.currentViewerFragment.setPreventInvalidateViewer(false);
                }
                if (this.val$after != null) {
                    this.val$after.run();
                }
                if (result != null && this.val$image.getIsStyled() && this.val$image.getUseAlternateThumb()) {
                    ImageProcessor.instance().updateStyledThumbAsync((Bitmap) this.val$image.getImageData().get(), this.val$image);
                } else {
                    StrictCache.instance().remove(this.val$image.getCacheKey(false));
                }
                GalleryPickerActivity.this.updateThumbnail(GalleryPickerActivity.this.galleryContext.currentImageIndex);
                if (result != null) {
                    GalleryPickerActivity.this.galleryContext.currentViewerFragment.loadNeighborImages(this.val$index);
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.1 */
    class C15231 extends IVCallback {
        C15231() {
        }

        public void onPositionChanged(int index, ImageEntry image) {
            GalleryPickerActivity.this.galleryContext.currentImage = image;
            GalleryPickerActivity.this.galleryContext.currentImageIndex = index;
            if (image != null) {
                GalleryPickerActivity.this.updateCheck(GalleryPickerActivity.this.galleryContext.currentImage.getIsChecked(), GalleryPickerActivity.this.checkView);
                GalleryPickerActivity.this.updateEditorActions(image);
                if (GalleryPickerActivity.this.galleryContext.currentViewerFragment != null) {
                    GalleryPickerActivity.this.galleryContext.currentViewerFragment.updateRect(GalleryPickerActivity.this.getImageView(index));
                }
            } else if (GalleryPickerActivity.this.galleryContext.currentAlbumFragment != null) {
                GalleryPickerActivity.this.galleryContext.currentAlbumFragment.updateList();
            }
        }

        public void onImageStyled(ImageEntry entry) {
        }

        public void onDismiss() {
            GalleryPickerActivity.this.backToGrid();
        }

        public void onBackgroundUpdate(float ratio) {
            if (ratio == 1.0f) {
                GalleryPickerActivity.this.frameView.setVisibility(8);
            } else {
                GalleryPickerActivity.this.frameView.setVisibility(0);
            }
            if (VERSION.SDK_INT >= 12 && ratio != 0.999f) {
                GalleryPickerActivity.this.checkView.setAlpha(ratio);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.28 */
    class AnonymousClass28 extends ActionCallback<BitmapEntry> {
        private final /* synthetic */ List val$entries;
        private final /* synthetic */ ImageEntry val$image;
        private final /* synthetic */ int val$index;
        private final /* synthetic */ View val$view;

        /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.28.1 */
        class C08761 implements Runnable {
            private final /* synthetic */ List val$entries;
            private final /* synthetic */ ImageEntry val$image;
            private final /* synthetic */ int val$index;
            private final /* synthetic */ BitmapEntry val$result;
            private final /* synthetic */ View val$view;

            C08761(View view, BitmapEntry bitmapEntry, int i, ImageEntry imageEntry, List list) {
                this.val$view = view;
                this.val$result = bitmapEntry;
                this.val$index = i;
                this.val$image = imageEntry;
                this.val$entries = list;
            }

            public void run() {
                if (this.val$view != null && (this.val$view instanceof LocalImageView)) {
                    ((LocalImageView) this.val$view).hideOverlayView(false);
                }
                if (this.val$result != null && this.val$result.get() != null) {
                    GalleryPickerActivity access$1 = GalleryPickerActivity.this;
                    int i = this.val$index;
                    ImageEntry imageEntry = this.val$image;
                    View view = this.val$view == null ? null : this.val$view instanceof LocalImageView ? (View) this.val$view.getParent() : this.val$view;
                    access$1.openImage(i, imageEntry, view, this.val$entries, true, true);
                }
            }
        }

        AnonymousClass28(ImageEntry imageEntry, View view, int i, List list) {
            this.val$image = imageEntry;
            this.val$view = view;
            this.val$index = i;
            this.val$entries = list;
        }

        public void run(BitmapEntry result) {
            if (!(result == null || result.get() == null)) {
                this.val$image.setImageData(result);
            }
            GalleryPickerActivity.this.runOnUiThread(new C08761(this.val$view, result, this.val$index, this.val$image, this.val$entries));
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.2 */
    class C15242 implements ImagesGridCallback {
        C15242() {
        }

        public void onImageOpened(int index, ImageEntry image, List<ImageEntry> images, LocalImageView view) {
            GalleryPickerActivity.this.loadAndOpenImage(index, image, view, images);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.3 */
    class C15253 extends OnClickTouchListener {

        /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.3.1 */
        class C08771 implements AnimatorListener {
            C08771() {
            }

            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                GalleryPickerActivity.this.checkView.setScaleX(1.0f);
                GalleryPickerActivity.this.checkView.setScaleY(1.0f);
            }
        }

        C15253() {
        }

        @TargetApi(11)
        public void onTapStarted(View view) {
            if (VERSION.SDK_INT >= 11) {
                GalleryPickerActivity.this.checkView.setScaleX(0.83f);
                GalleryPickerActivity.this.checkView.setScaleY(0.83f);
            }
        }

        private void returnScale() {
            if (VERSION.SDK_INT >= 11) {
                try {
                    GalleryPickerActivity.this.checkView.animate().scaleX(1.0f).scaleY(1.0f).setDuration(87).setListener(new C08771());
                } catch (Throwable th) {
                    GalleryPickerActivity.this.checkView.setScaleX(1.0f);
                    GalleryPickerActivity.this.checkView.setScaleY(1.0f);
                }
            }
        }

        @TargetApi(11)
        public void onTapCanceled(View view, boolean beforeCompleted) {
            if (!beforeCompleted) {
                returnScale();
            }
        }

        public void onTapCompleted(View view) {
            boolean checked = true;
            view.playSoundEffect(0);
            if (SelectionContext.performSelection(null, GalleryPickerActivity.this.galleryContext.currentImage) != 1) {
                checked = false;
            }
            GalleryPickerActivity.this.updateCheck(checked, GalleryPickerActivity.this.checkView);
            SelectionContext.updateImageViewSelected(GalleryPickerActivity.this.galleryContext.currentImageIndex, checked);
            if (checked) {
                GalleryPickerUtils.instance().bounce(GalleryPickerActivity.this.checkView, false);
            } else {
                returnScale();
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.4 */
    class C15264 extends CropperCallback {
        C15264() {
        }

        public void onCropApply() {
            GalleryPickerActivity.this.applyCrop();
        }

        public void onCropReset() {
            GalleryPickerActivity.this.resetCrop();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.5 */
    class C15275 extends CropperCallback {
        C15275() {
        }

        public void onCropApply() {
            float[] coords = GalleryPickerActivity.this.galleryContext.currentViewerFragment.getCropData();
            int[] data = new int[6];
            for (int i = 0; i < data.length; i++) {
                data[i] = Math.round(coords[i]);
            }
            GalleryPickerActivity.this.exit(false, GalleryPickerActivity.this.chosenOption, data);
        }

        public void onCropReset() {
            GalleryPickerActivity.this.resetCrop();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.7 */
    class C15287 implements Callback {
        C15287() {
        }

        public void onBackPressed() {
            GalleryPickerActivity.this.goBack();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.9 */
    class C15299 implements GalleryPickerFooterView.Callback {
        C15299() {
        }

        public void onCancelPressed() {
            if (GalleryPickerActivity.this.galleryContext == null || GalleryPickerActivity.this.galleryContext.currentViewerFragment == null) {
                GalleryPickerActivity.this.exit(true);
            } else {
                GalleryPickerActivity.this.goBack();
            }
        }

        public void onBigCancelPressed() {
            GalleryPickerActivity.this.goBack();
        }

        public void onBigCompletePressed() {
            onCompletePressed();
        }

        public void onCompletePressed() {
            GalleryPickerActivity.this.exit(false);
        }

        public void onRetryPressed() {
            GalleryPickerActivity.this.showRetryButton(true);
            GalleryPickerActivity.this.initializeAlbumsFragment();
            GalleryPickerActivity.this.displayAlbums();
        }

        public boolean getEditorActionsAvailable() {
            return (GalleryPickerActivity.this.galleryContext.currentImage == null || !GalleryPickerActivity.this.galleryContext.currentImage.getIsImageLoaded() || GalleryPickerActivity.this.galleryContext.currentImage.getIsImageFailed()) ? false : true;
        }

        public void onCropPressed() {
            GalleryPickerActivity.this.prepareCropper(true, true);
        }

        public boolean onFiltersPressed() {
            return GalleryPickerActivity.this.getFiltersView().toggle(true);
        }

        public void onTextPressed() {
            GalleryPickerActivity.this.showCaptionBox();
        }

        public void onEnhancePressed(boolean doApply) {
            GalleryPickerActivity.this.applyEnhance(doApply);
        }
    }

    public GalleryPickerActivity() {
        this.clearThumbsOnExit = false;
        this.noAlbumsList = false;
        this.noImagesGrid = false;
        this.galleryContext = new GalleryContext();
        this.imageViewerCallback = new C15231();
        this.imagesGridCallback = new C15242();
        this.onCheckTouchListener = new C15253().setRequiredApiVersionForStateListeners(11).toOnTouchListener();
        this.cropperCallback = new C15264();
        this.doneCropperCallback = new C15275();
        this.showingAlbum = false;
        this.showingViewer = false;
        this.isGoingBackToGrid = false;
        this.rectUpdates = -1;
    }

    public GalleryPickerFooterView getFooterView() {
        return this.footerView;
    }

    public GalleryContext getGalleryContext() {
        return this.galleryContext;
    }

    public void onCreate(Bundle savedInstanceState) {
        ArrayList arrayList = null;
        ActivityClassProvider.setStaticLocalContext(this);
        GalleryPickerUtils.sharedInstance = this;
        ImageProcessor.instance().clearForOtherContext(false);
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        SelectionContext.initialize(intent.getIntExtra("selection_limit", -1), intent.getBooleanExtra("single_mode", false), intent.getBooleanExtra("need_square", false));
        ArrayList stringArrayListExtra = intent.hasExtra("complete_options") ? intent.getStringArrayListExtra("complete_options") : null;
        if (intent.hasExtra("complete_single_options")) {
            arrayList = intent.getStringArrayListExtra("complete_single_options");
        }
        SelectionContext.setCompleteOptions(stringArrayListExtra, arrayList);
        SelectionContext.setPreventStyling(intent.getBooleanExtra("prevent_styling", false));
        this.clearThumbsOnExit = intent.getBooleanExtra("no_thumbs", false);
        this.currentOrientation = getResources().getConfiguration().orientation;
        getWindow().requestFeature(1);
        List<ImageEntry> images;
        Iterator it;
        ImageEntry image;
        if ("android.intent.action.SEND_MULTIPLE".equals(intent.getAction())) {
            images = new ArrayList();
            for (Parcelable uri : intent.getParcelableArrayListExtra("android.intent.extra.STREAM")) {
                image = LocalImageLoader.instance().getImageByUri(this, (Uri) uri);
                if (image != null) {
                    image.toggleChecked();
                    images.add(image);
                }
            }
            if (images.size() == 0) {
                exit(true);
            } else if (images.size() == 1) {
                initializeForViewer(0, images, false);
            } else {
                initializeAlbumWithImages(images);
            }
        } else if ("android.intent.action.SEND".equals(intent.getAction())) {
            image = LocalImageLoader.instance().getImageByUri(this, (Uri) intent.getParcelableExtra("android.intent.extra.STREAM"));
            if (image != null) {
                images = new ArrayList();
                images.add(image);
                initializeForViewer(0, images, false);
                return;
            }
            exit(true);
        } else if (intent.hasExtra("edit_images")) {
            ArrayList<String> paths = intent.getStringArrayListExtra("edit_images");
            if (paths.size() == 0) {
                exit(true);
                return;
            }
            images = new ArrayList();
            it = paths.iterator();
            while (it.hasNext()) {
                image = new ImageEntry((String) it.next());
                images.add(image);
                SelectionContext.addImageToSelected(image);
            }
            SelectionContext.setEditModeEnabled(true, paths);
            initializeForViewer(intent.getIntExtra("edit_index", 0), images, true);
        } else {
            initialize(true);
        }
    }

    private void initializeForViewer(int index, List<ImageEntry> images, boolean editMode) {
        Loggable.Error("Initializing " + images.size() + " images", new Object[0]);
        SelectionContext.setSingleImageShowing(true);
        SelectionContext.setSingleModeEnabled(!editMode);
        this.noAlbumsList = true;
        this.noImagesGrid = true;
        initialize(false);
        openImage(index, (ImageEntry) images.get(index), null, images, false, false);
        if (editMode) {
            showCheck();
            updateCheck(true, this.checkView);
            this.footerView.setBadge(images.size(), false);
        }
    }

    private void initializeAlbumWithImages(List<ImageEntry> images) {
        this.noAlbumsList = true;
        this.showingAlbum = true;
        initialize(false);
        this.headerView.setTitle(18);
        this.galleryContext.currentAlbumFragment = new ImagesGridFragment();
        this.galleryContext.currentAlbumFragment.setAlbumData(-1, images);
        this.galleryContext.currentAlbumFragment.setCallback(this.imagesGridCallback);
        SelectionContext.initialize(images);
        this.footerView.setBadge(SelectionContext.getSelectedCount(), false);
        this.footerView.setCompleteButtonType(2);
        navigateToFragment(this.galleryContext.currentAlbumFragment, this.frameView.getId(), false);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        unblockOrientation();
        if (this.galleryContext != null && requestCode == 100) {
            if (resultCode != -1) {
                CameraUtils.deleteImage();
            } else {
                GalleryPickerUtils.instance().invoke(new C08796());
            }
        }
    }

    private void initialize(boolean loadAlbums) {
        int i;
        this.headerView = new GalleryPickerHeaderView(this, new C15287());
        if (CameraUtils.getDeviceHasCamera()) {
            this.headerView.addActionButton(Views.CAMERA_BUTTON, C0436R.drawable.pe_camera, new C08808());
        }
        if (SelectionContext.getSingleMode()) {
            i = 1;
        } else {
            i = 0;
        }
        this.footerView = new GalleryPickerFooterView(this, i, new C15299());
        this.frameView = new FrameLayout(this);
        this.frameView.setLayoutParams(new LayoutParams(-1, -1));
        this.frameView.setPadding(0, 0, 0, ActivityClassProvider.dp(GalleryPickerFooterView.SIZE));
        this.frameView.setId(Views.FRAGMENT_VIEW);
        GalleryPickerUtils.setViewNotFocusable(this.frameView);
        this.photoView = new FrameLayout(this);
        this.photoView.setLayoutParams(new LayoutParams(-1, -1));
        this.photoView.setId(Views.PHOTO_VIEW);
        this.photoView.setVisibility(8);
        this.contentView = new FrameLayout(this);
        this.contentView.setLayoutParams(new LayoutParams(-1, -1));
        this.contentView.setBackgroundColor(Color.WINDOW_BACKGROUND_COLOR);
        GalleryPickerUtils.setViewNotFocusable(this.contentView);
        this.checkWrap = new FrameLayout(this);
        this.checkWrap.setLayoutParams(new FrameLayout.LayoutParams(ActivityClassProvider.dp(57.0f), ActivityClassProvider.dp(57.0f), 5));
        this.checkWrap.setOnTouchListener(this.onCheckTouchListener);
        this.checkWrap.setVisibility(8);
        this.checkView = new ImageView(this);
        this.checkView.setImageResource(C0436R.drawable.pe_big_check);
        this.checkWrap.addView(this.checkView, new FrameLayout.LayoutParams(ActivityClassProvider.dp(37.0f), ActivityClassProvider.dp(37.0f), 17));
        this.contentView.addView(this.frameView);
        this.contentView.addView(this.headerView);
        this.contentView.addView(this.photoView);
        this.contentView.addView(this.footerView);
        if (!SelectionContext.getSingleMode()) {
            this.contentView.addView(this.checkWrap);
        }
        if (loadAlbums) {
            initializeAlbumsFragment();
            displayAlbums();
        } else {
            this.headerView.hideActionButton(Views.CAMERA_BUTTON);
        }
        setContentView(this.contentView);
    }

    public void updateThumbnailForAlbum(AlbumEntry album) {
        runOnUiThread(new AnonymousClass10(album));
    }

    public void updateThumbnailForImage(int index) {
        runOnUiThread(new AnonymousClass11(index));
    }

    public void updateThumbnail(int index) {
        ImageProcessor.instance().dispatch_async(new AnonymousClass13(new AnonymousClass12(index, this.galleryContext.currentImage, this.galleryContext.currentAlbumIndex)));
    }

    public FiltersListView getFiltersView() {
        if (this.filtersView == null) {
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, ActivityClassProvider.dp(116.0f), 80);
            layoutParams.setMargins(0, 0, 0, ActivityClassProvider.dp(GalleryPickerFooterView.SIZE));
            this.filtersView = new FiltersListView(this);
            this.contentView.addView(this.filtersView, this.contentView.getChildCount() - (SelectionContext.getSingleMode() ? 1 : 2), layoutParams);
        }
        return this.filtersView;
    }

    private void showCropper(boolean crop, CropperCallback callback) {
        int index = this.galleryContext.currentImageIndex;
        ImageEntry image = this.galleryContext.currentImage;
        float[] coords = (image.getIsStyled() && image.getStyle().getIsCropped()) ? image.getStyle().removeCropData() : null;
        Runnable block = new AnonymousClass14(coords, image, callback);
        if (coords != null || (crop && image.getIsStyled() && image.getStyle().getIsProcessed())) {
            boolean z;
            this.galleryContext.currentViewerFragment.clearNeighborImages(index);
            image.checkStyleTopicality();
            setIsBlocked(true);
            Runnable anonymousClass15 = new AnonymousClass15(block);
            if (image.getIsStyled() && image.getStyle().getIsCropped()) {
                z = true;
            } else {
                z = false;
            }
            reloadImage(index, image, true, anonymousClass15, true, z);
            return;
        }
        block.run();
    }

    private void reloadImage(int index, ImageEntry image, boolean noProcessing, Runnable after, boolean noInvalidateReset, boolean resetContext) {
        if (resetContext) {
            ImageProcessor.instance().clearCurrentContext();
        }
        LocalImageLoader.instance().getImage(image, noProcessing, noProcessing, new AnonymousClass16(image, noInvalidateReset, after, index), false, -1, 1);
    }

    private void hideCropper() {
        this.galleryContext.currentViewerFragment.hideCropper();
        this.footerView.hideCropperAction();
        if (this.galleryContext.currentImage.getIsStyled() && this.galleryContext.currentImage.getStyle().getIsProcessed()) {
            reloadImage(this.galleryContext.currentImageIndex, this.galleryContext.currentImage, false, null, false, false);
        }
    }

    private void applyCrop() {
        ImageProcessor.instance().clearCurrentContext();
        int index = this.galleryContext.currentImageIndex;
        ImageEntry image = this.galleryContext.currentImage;
        float[] coords = this.galleryContext.currentViewerFragment.getCropData();
        image.getStyle(true).setCropped(coords);
        image.checkStyleTopicality();
        Loggable.Error("Crop image. 0x1: %f, 1y1: %f, 2x2: %f, 3y2: %f, 4w: %f, 5h: %f, scale: %f", Float.valueOf(coords[0]), Float.valueOf(coords[1]), Float.valueOf(coords[2]), Float.valueOf(coords[3]), Float.valueOf(coords[4]), Float.valueOf(coords[5]), Float.valueOf(coords[6]));
        this.galleryContext.currentViewerFragment.dropTouches();
        if (coords[2] - coords[0] == coords[4] && coords[3] - coords[1] == coords[5]) {
            resetCrop();
        } else {
            this.galleryContext.currentViewerFragment.fadeCropper(new AnonymousClass17(image, coords));
        }
    }

    private void resetCrop() {
        this.footerView.updateEditorActionEnabled(Views.ACTION_CROP, false);
        if (SelectionContext.getSelectedCount() > 0) {
            this.footerView.updateBadgeVisible(true);
        }
        this.galleryContext.currentImage.getStyle(true).setCropped(null);
        this.galleryContext.currentImage.checkStyleTopicality();
        StrictCache.instance().remove(this.galleryContext.currentImage.getCacheKey(false));
        prepareCropper(false, true);
    }

    void prepareCropper(boolean toShow, boolean crop) {
        runOnUiThread(new AnonymousClass18(toShow, crop));
    }

    void showBadge(boolean hide) {
        if (SelectionContext.getSelectedCount() > 0) {
            this.footerView.updateBadgeVisible(!hide);
        }
    }

    private void applyEnhance(boolean doApply) {
        ImageEntry image = this.galleryContext.currentImage;
        int imageIndex = this.galleryContext.currentImageIndex;
        if (image != null && this.galleryContext.currentViewerFragment != null) {
            getGalleryContext().currentViewerFragment.setPreventInvalidateViewer(true);
            ImageProcessor.instance().dispatch_async(new AnonymousClass19(doApply, image, imageIndex));
        }
    }

    private void showCaptionBox() {
        if (this.filtersView.getIsShowing()) {
            this.footerView.updateFilterActionButton();
        }
        String rawText = (this.galleryContext.currentImage.getIsStyled() && this.galleryContext.currentImage.getStyle().getIsTexted()) ? this.galleryContext.currentImage.getStyle().getText() : null;
        Builder builder = new Builder(ActivityClassProvider.getStaticLocalContext());
        builder.setTitle(LangProvider.getLocalizedString(8));
        int padding = ActivityClassProvider.dp(10.0f);
        LinearLayout layout = new LinearLayout(ActivityClassProvider.getStaticLocalContext());
        layout.setPadding(padding, padding, padding, padding);
        EditText textView = new EditText(ActivityClassProvider.getStaticLocalContext());
        textView.setHint(LangProvider.getLocalizedString(9));
        textView.setTextSize(1, GalleryPickerFooterView.BADGE_SIZE);
        textView.setInputType(540673);
        textView.setSingleLine(true);
        textView.setMaxLines(1);
        Runnable applier = new AnonymousClass20(textView, rawText);
        layout.addView(textView, new LinearLayout.LayoutParams(-1, -2));
        builder.setView(layout);
        TextWatcher watcher = new AnonymousClass21(textView);
        textView.getViewTreeObserver().addOnGlobalLayoutListener(new AnonymousClass22(textView));
        builder.setNegativeButton(LangProvider.getLocalizedString(10), new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (GalleryPickerActivity.this.galleryContext.currentImage.getIsStyled() && GalleryPickerActivity.this.galleryContext.currentImage.getStyle().getIsTexted()) {
                    InlineUtils.applyTextToImage(GalleryPickerActivity.this, false, ACRAConstants.DEFAULT_STRING_VALUE, GalleryPickerActivity.this.galleryContext.currentImage);
                }
            }
        });
        builder.setPositiveButton(LangProvider.getLocalizedString(6), new AnonymousClass24(applier));
        AlertDialog[] dialog = new AlertDialog[1];
        textView.getViewTreeObserver().addOnPreDrawListener(new AnonymousClass25(textView, rawText, watcher, applier, dialog));
        dialog[0] = builder.show();
        dialog[0].setCanceledOnTouchOutside(false);
    }

    public void updateCheck(boolean isChecked) {
        updateCheck(isChecked, this.checkView);
    }

    private void updateCheck(boolean isChecked, ImageView check) {
        check.setImageResource(isChecked ? C0436R.drawable.pe_big_checked : C0436R.drawable.pe_big_check);
    }

    private void updateEditorActions(ImageEntry image) {
        boolean z = true;
        if (image != null) {
            boolean z2;
            boolean styled = image.getIsStyled();
            GalleryPickerFooterView galleryPickerFooterView = this.footerView;
            if (styled && image.getStyle().getIsFiltered()) {
                z2 = true;
            } else {
                z2 = false;
            }
            boolean z3 = styled && image.getStyle().getIsTexted();
            boolean z4 = styled && image.getStyle().getIsCropped();
            if (!(styled && image.getStyle().getIsEnhanced())) {
                z = false;
            }
            galleryPickerFooterView.updateEditorActionsEnabled(z2, z3, z4, z);
            getFiltersView().update(image.getFilterId());
        }
    }

    private void initializeAlbumsFragment() {
        this.galleryContext.albumsFragment = new AlbumsListFragment();
        this.galleryContext.albumsFragment.setCallback(new AlbumsListCallback() {
            public void onCameraAlbumFound(int bucketId) {
                GalleryPickerActivity.this.galleryContext.currentCameraBucketId = bucketId;
                GalleryPickerActivity.this.galleryContext.currentCameraBucketFound = true;
            }

            public void onVKCameraAlbumFound(int bucketId) {
            }

            public void onAlbumChosen(int index, AlbumEntry album) {
                GalleryPickerActivity.this.openAlbum(index, album, false);
            }
        });
    }

    private void addNewImageToCameraAlbum(ImageEntry image) {
        if (SelectionContext.addImageToSelected(image)) {
            updateCheck(true);
            this.footerView.setBadge(SelectionContext.getSelectedCount(), true);
        }
        AlbumEntry album = !this.galleryContext.currentCameraBucketFound ? null : this.galleryContext.albumsFragment.getAlbumByBucketId(this.galleryContext.currentCameraBucketId);
        if (album == null) {
            album = new AlbumEntry(0, LangProvider.getLocalizedString(20), image.getImageId());
            album.setIsCamera(true);
            this.galleryContext.albumsFragment.addAlbum(album);
            this.galleryContext.currentCameraBucketFound = true;
            this.galleryContext.currentCameraBucketId = 0;
        }
        album.addImageToBeginning(image);
        this.galleryContext.albumsFragment.updateList();
        updateThumbnailForAlbum(album);
        if (this.galleryContext.currentAlbumFragment != null) {
            this.galleryContext.currentAlbumFragment.updateList();
            updateThumbnailForImage(0);
        }
        forceOpen(album, image);
    }

    private void setIsShowingAlbum(boolean isShowing) {
        this.showingAlbum = isShowing;
        if (!isShowing) {
            this.galleryContext.currentAlbumFragment = null;
        }
        updateBackButton(!this.showingAlbum);
    }

    public void updateBackButton(boolean isCancel) {
        this.headerView.setBackVisible(!isCancel);
        GalleryPickerHeaderView galleryPickerHeaderView = this.headerView;
        int i = isCancel ? 17 : SelectionContext.getSingleMode() ? 19 : 18;
        galleryPickerHeaderView.setTitle(i);
    }

    public void showRetryButton(boolean hide) {
        boolean z = true;
        GalleryPickerUtils.sharedInstance.updateBackButton(true);
        GalleryPickerFooterView galleryPickerFooterView = this.footerView;
        if (hide) {
            z = false;
        }
        galleryPickerFooterView.updateIsRetryShowing(z);
    }

    private void setIsBlocked(boolean isBlocked) {
        this.isBlocked = isBlocked;
        this.footerView.setIsBlocked(isBlocked);
        if (this.photoView != null) {
            this.photoView.setOnClickListener(isBlocked ? GalleryPickerUtils.emptyOnClickListener : null);
        }
    }

    public void navigateToFragment(Fragment fragment, int target, boolean animated) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (animated && VERSION.SDK_INT >= 11) {
            transaction.setCustomAnimations(C0436R.anim.scale_in, C0436R.anim.scale_out);
        }
        transaction.replace(target, fragment);
        try {
            transaction.commit();
        } catch (Throwable throwable) {
            Loggable.Error("Cannot navigate to fragment", throwable, new Object[0]);
        }
    }

    private void displayAlbums() {
        navigateToFragment(this.galleryContext.albumsFragment, this.frameView.getId(), false);
    }

    private void openAlbum(int index, AlbumEntry album, boolean setOnly) {
        boolean z = true;
        setIsShowingAlbum(true);
        this.galleryContext.currentAlbumIndex = index;
        if (!album.getIsCamera()) {
            this.headerView.hideActionButton(Views.CAMERA_BUTTON);
        }
        this.galleryContext.currentAlbumFragment = new ImagesGridFragment();
        this.galleryContext.currentAlbumFragment.setAlbumData(album.getBucketId(), album.getImages());
        this.galleryContext.currentAlbumFragment.setCallback(this.imagesGridCallback);
        Fragment fragment = this.galleryContext.currentAlbumFragment;
        int id = this.frameView.getId();
        if (setOnly) {
            z = false;
        }
        navigateToFragment(fragment, id, z);
    }

    private void setIsShowingViewer(boolean isShowing) {
        this.showingViewer = isShowing;
        if (!isShowing) {
            Runnable remover = new Runnable() {

                /* renamed from: com.vkontakte.android.mediapicker.GalleryPickerActivity.27.1 */
                class C08751 implements Runnable {
                    C08751() {
                    }

                    public void run() {
                        GalleryPickerActivity.this.photoView.removeAllViews();
                        GalleryPickerActivity.this.photoView.setVisibility(8);
                        GalleryPickerActivity.this.frameView.setVisibility(0);
                        GalleryPickerActivity.this.clearCurrentImage();
                        GalleryPickerActivity.this.galleryContext.currentViewerFragment = null;
                    }
                }

                public void run() {
                    GalleryPickerActivity.this.galleryContext.currentViewerFragment.showThumb();
                    GalleryPickerActivity.this.photoView.postDelayed(new C08751(), 10);
                }
            };
            if (VERSION.SDK_INT >= 14) {
                this.galleryContext.currentViewerFragment.animateOut(getCurrentImageView(), remover);
                if (this.galleryContext.currentAlbumFragment != null && GalleryPickerUtils.instance().getHardAnimationsEnabled()) {
                    GalleryPickerUtils.instance().scale(this.galleryContext.currentAlbumFragment.getGridView(), 1.0f, GalleryPickerUtils.getThumbnailAnimationDuration());
                }
            } else {
                this.galleryContext.currentViewerFragment.clearImages();
                remover.run();
                clearCurrentImage();
            }
        }
        GalleryPickerFooterView galleryPickerFooterView = this.footerView;
        boolean z = this.showingViewer || SelectionContext.getSelectedCount() > 0;
        galleryPickerFooterView.updateCompleteButtonEnabled(z);
        this.footerView.showEditorAction(this.showingViewer);
        updateEditorActions(this.galleryContext.currentImage);
        getFiltersView().updateForImageViewer(this.showingViewer);
    }

    private View getAlbumView(int position) {
        return this.galleryContext.albumsFragment == null ? null : this.galleryContext.albumsFragment.getViewAt(position);
    }

    private View getCurrentImageView() {
        return getImageView(this.galleryContext.currentViewerFragment.getCurrentIndex());
    }

    private void clearCurrentImage() {
        this.galleryContext.currentImage = null;
        this.galleryContext.currentImageIndex = -1;
    }

    private View getImageView(int position) {
        return this.galleryContext.currentAlbumFragment == null ? null : this.galleryContext.currentAlbumFragment.getGridViewAt(position);
    }

    private void loadAndOpenImage(int index, ImageEntry image, View view, List<ImageEntry> entries) {
        this.photoView.setVisibility(0);
        setIsBlocked(true);
        LocalImageLoader.instance().getImage(image, false, false, new AnonymousClass28(image, view, index, entries), false, -1, 1);
    }

    private void openImage(int index, ImageEntry image, View view, List<ImageEntry> entries, boolean animated, boolean closeEnabled) {
        if (entries.size() != 0) {
            setIsBlocked(false);
            this.galleryContext.currentViewerFragment = new ImageViewerFragment();
            this.galleryContext.currentViewerFragment.setImages(entries);
            this.galleryContext.currentViewerFragment.setCurrentIndex(index);
            this.galleryContext.currentImageIndex = index;
            this.galleryContext.currentImage = (ImageEntry) entries.get(this.galleryContext.currentImageIndex);
            updateCheck(this.galleryContext.currentImage.getIsChecked(), this.checkView);
            this.galleryContext.currentViewerFragment.setCallback(this.imageViewerCallback);
            navigateToFragment(this.galleryContext.currentViewerFragment, this.photoView.getId(), false);
            if (VERSION.SDK_INT >= 14) {
                this.galleryContext.currentViewerFragment.animateIn(image, view, !animated);
                if (this.galleryContext.currentAlbumFragment != null && GalleryPickerUtils.instance().getHardAnimationsEnabled()) {
                    if (animated) {
                        GalleryPickerUtils.instance().scale(this.galleryContext.currentAlbumFragment.getGridView(), BACKGROUND_SCALE, GalleryPickerUtils.getThumbnailAnimationDuration());
                    } else {
                        forceSetScale(BACKGROUND_SCALE);
                    }
                }
            }
            if (!animated) {
                this.frameView.setVisibility(8);
                this.photoView.setVisibility(0);
                this.galleryContext.currentViewerFragment.loadImages(new int[]{this.galleryContext.currentImageIndex, this.galleryContext.currentImageIndex - 1, this.galleryContext.currentImageIndex + 1});
            }
            if (!closeEnabled) {
                this.galleryContext.currentViewerFragment.setCloseEnabled(closeEnabled);
            }
            setIsShowingViewer(true);
            if (!SelectionContext.getSingleMode()) {
                showCheck(false);
            }
        }
    }

    @TargetApi(11)
    private void forceSetScale(float scale) {
        if (VERSION.SDK_INT >= 11) {
            this.galleryContext.currentAlbumFragment.getGridView().setScaleX(scale);
            this.galleryContext.currentAlbumFragment.getGridView().setScaleY(scale);
        }
    }

    private boolean updateCurrentRect(boolean force) {
        if (this.galleryContext.currentViewerFragment.hasRect() && !force) {
            return true;
        }
        View view = getCurrentImageView();
        if (view != null || force) {
            this.rectUpdates = -1;
            this.galleryContext.currentViewerFragment.setRect(this.galleryContext.currentImageIndex, view);
            return true;
        }
        this.imageViewerCallback.onBackgroundUpdate(0.999f);
        if (!this.galleryContext.currentAlbumFragment.checkAvailable()) {
            int i = this.rectUpdates + 1;
            this.rectUpdates = i;
            if (i < 15) {
                this.isGoingBackToGrid = true;
                this.frameView.postDelayed(new Runnable() {
                    public void run() {
                        if (GalleryPickerActivity.this.isGoingBackToGrid) {
                            GalleryPickerActivity.this.backToGrid();
                        }
                    }
                }, 90);
                return false;
            }
        }
        this.rectUpdates = -1;
        this.galleryContext.currentViewerFragment.clearThumb();
        return true;
    }

    private void backToGrid() {
        if (this.isGoingBackToGrid) {
            this.isGoingBackToGrid = false;
            updateCurrentRect(true);
        }
        if (updateCurrentRect(false)) {
            setIsShowingViewer(false);
            showCheck(true);
        }
    }

    private void backToAlbums() {
        setIsShowingAlbum(false);
        navigateToFragment(this.galleryContext.albumsFragment, this.frameView.getId(), true);
        this.headerView.showActionButton(Views.CAMERA_BUTTON);
    }

    private void goBack() {
        if ((this.galleryContext.currentViewerFragment != null && this.galleryContext.currentViewerFragment.isAnimating()) || this.isBlocked) {
            return;
        }
        if (this.footerView.getIsExtraActionShowing()) {
            prepareCropper(false, true);
        } else if (this.showingViewer && !this.noImagesGrid) {
            backToGrid();
        } else if (!this.showingAlbum || this.noAlbumsList) {
            exit(true);
        } else {
            backToAlbums();
        }
    }

    private void showOptions() {
        CharSequence[] options = SelectionContext.getCompleteOptions();
        if (options == null || options.length == 0) {
            exit(false, -1, null);
            return;
        }
        Builder builder = new Builder(this);
        builder.setTitle(LangProvider.getLocalizedString(SelectionContext.getSelectedCount() > 1 ? 53 : 52));
        builder.setItems(options, new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i < SelectionContext.getCompleteOptionsSize() || !SelectionContext.getNeedSquare() || (GalleryPickerActivity.this.galleryContext.currentImage.getIsStyled() && GalleryPickerActivity.this.galleryContext.currentImage.getStyle().getIsCropped())) {
                    GalleryPickerActivity.this.exit(false, i, null);
                } else {
                    GalleryPickerActivity.this.getSquare(i);
                }
            }
        });
        builder.create().show();
    }

    private void getSquare(int option) {
        this.chosenOption = option;
        prepareCropper(true, false);
    }

    private void exit(boolean isCanceled, int option, int[] cropResult) {
        if (!isCanceled) {
            if (this.clearThumbsOnExit) {
                Loggable.Info("Clearing thumbs on exit", new Object[0]);
                StrictCache.instance().clear();
            }
            boolean editMode = SelectionContext.getIsInEditMode();
            ArrayList<String> uris = new ArrayList();
            ArrayList<String> passedItems = SelectionContext.getPassedItems();
            List<ImageEntry> result = SelectionContext.retrieveSelectedImages();
            if (result != null) {
                List<String> cachedKeys = new ArrayList();
                for (ImageEntry image : result) {
                    uris.add(image.getResultPath());
                    cachedKeys.add(image.getCacheKey(false));
                }
                if (uris.size() == 0) {
                    if (this.galleryContext.currentImage != null) {
                        uris.add(this.galleryContext.currentImage.getResultPath());
                        cachedKeys.add(this.galleryContext.currentImage.getCacheKey(false));
                    }
                }
                if (uris.size() == 0) {
                    Loggable.Error("Wow! No URIs passed", new Object[0]);
                    return;
                }
                StrictCache.instance().clearExcept(cachedKeys);
                Intent data = new Intent();
                if (editMode) {
                    List<ImageEntry> images;
                    int i;
                    if (this.galleryContext.currentViewerFragment != null) {
                        images = this.galleryContext.currentViewerFragment.getImages();
                    } else {
                        images = null;
                    }
                    ArrayList<String> currentImages = new ArrayList();
                    if (images != null) {
                        for (ImageEntry image2 : images) {
                            currentImages.add(image2.getResultPath());
                        }
                        ArrayList<Integer> removedIndexes = new ArrayList();
                        i = images.size() - 1;
                        while (i >= 0) {
                            if (!((ImageEntry) images.get(i)).getIsChecked()) {
                                removedIndexes.add(Integer.valueOf(i));
                                if (passedItems != null && i >= 0 && i < passedItems.size()) {
                                    passedItems.remove(i);
                                    currentImages.remove(i);
                                }
                            }
                            i--;
                        }
                        Collections.reverse(removedIndexes);
                        if (removedIndexes.size() > 0) {
                            data.putExtra("removed_indexes", removedIndexes);
                        }
                    }
                    if (passedItems != null && passedItems.size() > 0) {
                        ArrayList<Integer> updatedIndexes = new ArrayList();
                        ArrayList<String> updatedImages = new ArrayList();
                        for (i = 0; i < passedItems.size(); i++) {
                            if (!currentImages.contains((String) passedItems.get(i))) {
                                updatedImages.add((String) currentImages.get(i));
                                updatedIndexes.add(Integer.valueOf(i));
                            }
                        }
                        if (updatedIndexes.size() > 0) {
                            data.putExtra("updated_indexes", updatedIndexes);
                            data.putExtra("updated_images", updatedImages);
                        }
                    }
                } else {
                    data.putExtra("images", uris);
                }
                if (option >= 0) {
                    data.putExtra("chosen_option", option);
                }
                if (SelectionContext.getNeedSquare() && cropResult != null) {
                    int length = cropResult.length;
                    if (r0 >= 6) {
                        int x1 = cropResult[0];
                        int x2 = cropResult[2];
                        int y1 = cropResult[1];
                        int y2 = cropResult[3];
                        int w = cropResult[4];
                        int h = cropResult[5];
                        if (x2 - x1 != y2 - y1 || w <= 0 || h <= 0) {
                            data.putExtra("cropped", false);
                            Object[] objArr = new Object[0];
                            Loggable.Error("What the hell are you doing: x: " + (x2 - x1) + " y: " + (y2 - y1) + " w: " + w + " h: " + h, r0);
                        } else {
                            data.putExtra("cropped", true);
                            data.putExtra("crop_x", x1);
                            data.putExtra("crop_y", y1);
                            data.putExtra("crop_width", x2 - x1);
                            data.putExtra("crop_image_width", w);
                            data.putExtra("crop_image_height", h);
                        }
                    }
                }
                setResult(-1, data);
                finish();
            }
        }
    }

    private void exit(boolean isCanceled) {
        int i = 1;
        if (ImageProcessor.instance().isRendering()) {
            Loggable.Error("It's Rendering. Please, wait!!!", new Object[0]);
            return;
        }
        ImageProcessor.instance().clearForOtherContext(true);
        if (SelectionContext.getIsInEditMode()) {
            i = 0;
        }
        if (isCanceled & i) {
            StrictCache.instance().clear();
            SelectionContext.retrieveSelectedImages();
            setResult(0);
            finish();
            return;
        }
        showOptions();
    }

    private void forceOpen(AlbumEntry album, ImageEntry image) {
        if (this.galleryContext.currentAlbumFragment == null || this.galleryContext.currentAlbumFragment.getBucketId() == album.getBucketId()) {
            Runnable proc = new AnonymousClass31(image, album);
            if (this.galleryContext.currentAlbumFragment == null) {
                openAlbum(this.galleryContext.albumsFragment.getAlbumIndex(album), album, true);
            }
            proc.run();
        }
    }

    public void showCheck() {
        this.checkWrap.setVisibility(0);
    }

    @TargetApi(12)
    public void showCheck(boolean hide) {
        float f = 1.0f;
        if (!hide) {
            this.checkWrap.setVisibility(0);
        }
        float from = VERSION.SDK_INT >= 12 ? this.checkWrap.getAlpha() : hide ? 1.0f : 0.0f;
        GalleryPickerUtils instance = GalleryPickerUtils.instance();
        View view = this.checkWrap;
        if (hide) {
            f = 0.0f;
        }
        instance.fade(view, from, f, GalleryPickerUtils.getThumbnailAnimationDuration(), new AnonymousClass32(hide));
    }

    public void onBackPressed() {
        goBack();
    }

    public void onConfigurationChanged(Configuration config) {
        if (this.galleryContext.currentAlbumFragment != null) {
            this.galleryContext.currentAlbumFragment.rememberPosition();
        }
        if (this.galleryContext.currentViewerFragment != null) {
            this.galleryContext.currentViewerFragment.clearRect();
        }
        boolean needUpdate = false;
        if (this.currentOrientation != config.orientation) {
            needUpdate = true;
            this.currentOrientation = config.orientation;
        }
        super.onConfigurationChanged(config);
        if (needUpdate && this.galleryContext.currentAlbumFragment != null) {
            this.galleryContext.currentAlbumFragment.updateColumnsSize();
        }
        if (this.footerView != null) {
            this.footerView.updateButtonsMaxWidth();
        }
    }
}
