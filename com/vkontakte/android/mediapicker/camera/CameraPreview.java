package com.vkontakte.android.mediapicker.camera;

import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import com.vkontakte.android.mediapicker.utils.Loggable;

public class CameraPreview extends SurfaceView implements Callback {
    private Camera camera;
    private SurfaceHolder holder;

    public CameraPreview(Context context, Camera camera) {
        super(context);
        this.camera = camera;
        this.holder = getHolder();
        this.holder.addCallback(this);
        this.holder.setType(3);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            this.camera.setPreviewDisplay(holder);
            this.camera.startPreview();
        } catch (Throwable t) {
            Loggable.Error("Error setting camera preview: ", t, new Object[0]);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (this.holder.getSurface() != null) {
            try {
                this.camera.stopPreview();
            } catch (Throwable th) {
            }
            try {
                this.camera.setPreviewDisplay(this.holder);
                this.camera.startPreview();
            } catch (Exception t) {
                Loggable.Error("Error starting", t, new Object[0]);
            }
        }
    }
}
