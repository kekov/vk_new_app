package com.vkontakte.android.mediapicker.camera.utils;

import android.view.MotionEvent;

public abstract class CameraModule {
    public abstract boolean onBackPressed();

    public abstract void onPauseAfterSuper();

    public abstract void onPauseBeforeSuper();

    public abstract void onResumeAfterSuper();

    public abstract void onResumeBeforeSuper();

    public abstract void onStop();

    public abstract boolean onTouchEvent(MotionEvent motionEvent);
}
