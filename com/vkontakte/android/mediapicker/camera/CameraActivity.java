package com.vkontakte.android.mediapicker.camera;

import android.hardware.Camera;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.widget.FrameLayout;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.utils.Loggable;

public class CameraActivity extends ActivityClassProvider {
    private Camera camera;
    private FrameLayout contentView;
    private boolean needResume;
    private CameraPreview preview;
    private int rectSize;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(GLRenderBuffer.EGL_SURFACE_SIZE, GLRenderBuffer.EGL_SURFACE_SIZE);
        if (VERSION.SDK_INT >= 14) {
            getWindow().getDecorView().setSystemUiVisibility(1);
        }
        this.camera = CameraHolder.getCamera(0);
        this.contentView = new FrameLayout(this);
        this.contentView.setBackgroundColor(Color.ALBUMS_LIST_BACKGROUND);
        this.preview = new CameraPreview(this, this.camera);
        this.contentView.addView(this.preview);
        updateRectSize();
        setContentView(this.contentView);
    }

    public void onResume() {
        super.onResume();
        try {
            if (this.needResume) {
                Camera.open();
            }
        } catch (Throwable throwable) {
            Loggable.Error("Cannot access camera", throwable, new Object[0]);
        }
    }

    public void onPause() {
        super.onPause();
        try {
            this.camera.release();
            this.needResume = true;
        } catch (Throwable throwable) {
            Loggable.Error("Cannot release camera", throwable, new Object[0]);
            this.needResume = false;
        }
    }

    private void updateRectSize() {
        this.rectSize = ActivityClassProvider.getScreenMinSize();
    }
}
