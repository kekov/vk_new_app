package com.vkontakte.android.mediapicker.gallery;

import android.graphics.Bitmap;
import android.widget.Toast;
import com.vkontakte.android.mediapicker.GalleryPickerActivity;
import com.vkontakte.android.mediapicker.entries.ActionCallback;
import com.vkontakte.android.mediapicker.entries.BitmapEntry;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.entries.StyleEntry;
import com.vkontakte.android.mediapicker.gl.CLAHE;
import com.vkontakte.android.mediapicker.gl.ImageProcessor;
import com.vkontakte.android.mediapicker.providers.LangProvider;
import com.vkontakte.android.mediapicker.providers.ResourceProvider.Views;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.LocalImageLoader;

public class InlineUtils {

    /* renamed from: com.vkontakte.android.mediapicker.gallery.InlineUtils.10 */
    class AnonymousClass10 implements Runnable {
        private final /* synthetic */ GalleryPickerActivity val$activity;
        private final /* synthetic */ ImageEntry val$image;
        private final /* synthetic */ int val$imageIndex;
        private final /* synthetic */ ImageEntry val$inputImage;
        private final /* synthetic */ Runnable val$ondone;

        AnonymousClass10(ImageEntry imageEntry, ImageEntry imageEntry2, Runnable runnable, int i, GalleryPickerActivity galleryPickerActivity) {
            this.val$inputImage = imageEntry;
            this.val$image = imageEntry2;
            this.val$ondone = runnable;
            this.val$imageIndex = i;
            this.val$activity = galleryPickerActivity;
        }

        public void run() {
            this.val$inputImage.setImageData(this.val$image.getImageData());
            this.val$ondone.run();
            InlineUtils.updateThumbnailForImage((Bitmap) this.val$image.getImageData().get(), this.val$imageIndex, this.val$image, this.val$activity);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.InlineUtils.1 */
    class C08971 implements Runnable {
        C08971() {
        }

        public void run() {
            GalleryPickerUtils.showFiltersToast();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.InlineUtils.3 */
    class C08993 implements Runnable {
        private final /* synthetic */ ActionCallback val$callback;
        private final /* synthetic */ ImageEntry val$image;
        private final /* synthetic */ boolean val$wasFiltered;

        C08993(ImageEntry imageEntry, boolean z, ActionCallback actionCallback) {
            this.val$image = imageEntry;
            this.val$wasFiltered = z;
            this.val$callback = actionCallback;
        }

        public void run() {
            ImageProcessor.instance().filter(1, this.val$image, this.val$wasFiltered ? null : (Bitmap) this.val$image.getImageData().get(), this.val$wasFiltered, true, this.val$callback, -1);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.InlineUtils.4 */
    class C09004 implements Runnable {
        private final /* synthetic */ GalleryPickerActivity val$activity;
        private final /* synthetic */ Bitmap val$bitmap;
        private final /* synthetic */ ImageEntry val$image;
        private final /* synthetic */ int val$index;

        C09004(Bitmap bitmap, ImageEntry imageEntry, GalleryPickerActivity galleryPickerActivity, int i) {
            this.val$bitmap = bitmap;
            this.val$image = imageEntry;
            this.val$activity = galleryPickerActivity;
            this.val$index = i;
        }

        public void run() {
            ImageProcessor.instance().updateStyledThumb(this.val$bitmap, this.val$image);
            if (this.val$activity != null) {
                this.val$activity.updateThumbnail(this.val$index);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.InlineUtils.5 */
    class C09015 implements Runnable {
        private final /* synthetic */ GalleryPickerActivity val$activity;

        C09015(GalleryPickerActivity galleryPickerActivity) {
            this.val$activity = galleryPickerActivity;
        }

        public void run() {
            if (this.val$activity.getGalleryContext().currentViewerFragment != null) {
                this.val$activity.getGalleryContext().currentViewerFragment.setPreventInvalidateViewer(false);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.InlineUtils.6 */
    class C09036 implements Runnable {
        private final /* synthetic */ GalleryPickerActivity val$activity;
        private final /* synthetic */ ImageEntry val$image;
        private final /* synthetic */ Runnable val$ondone;

        /* renamed from: com.vkontakte.android.mediapicker.gallery.InlineUtils.6.1 */
        class C09021 implements Runnable {
            private final /* synthetic */ GalleryPickerActivity val$activity;
            private final /* synthetic */ ImageEntry val$image;

            C09021(GalleryPickerActivity galleryPickerActivity, ImageEntry imageEntry) {
                this.val$activity = galleryPickerActivity;
                this.val$image = imageEntry;
            }

            public void run() {
                Toast.makeText(this.val$activity, LangProvider.getLocalizedString(34), 0).show();
                if (!this.val$image.getIsStyled() || !this.val$image.getStyle().getIsEnhanced()) {
                    GalleryPickerUtils.sharedInstance.getFooterView().updateEditorActionEnabled(Views.ACTION_ENHANCE, false);
                }
            }
        }

        C09036(Runnable runnable, GalleryPickerActivity galleryPickerActivity, ImageEntry imageEntry) {
            this.val$ondone = runnable;
            this.val$activity = galleryPickerActivity;
            this.val$image = imageEntry;
        }

        public void run() {
            this.val$ondone.run();
            if (GalleryPickerUtils.sharedInstance != null && GalleryPickerUtils.sharedInstance.getFooterView() != null) {
                GalleryPickerUtils.sharedInstance.getFooterView().postDelayed(new C09021(this.val$activity, this.val$image), 400);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.InlineUtils.8 */
    class C09048 implements Runnable {
        private final /* synthetic */ Runnable val$onerror;

        C09048(Runnable runnable) {
            this.val$onerror = runnable;
        }

        public void run() {
            this.val$onerror.run();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.InlineUtils.2 */
    class C15382 extends ActionCallback<Bitmap> {
        private final /* synthetic */ GalleryPickerActivity val$activity;
        private final /* synthetic */ boolean val$force;
        private final /* synthetic */ ImageEntry val$image;
        private final /* synthetic */ int val$index;
        private final /* synthetic */ int val$lastFilterId;

        /* renamed from: com.vkontakte.android.mediapicker.gallery.InlineUtils.2.1 */
        class C08981 implements Runnable {
            private final /* synthetic */ GalleryPickerActivity val$activity;
            private final /* synthetic */ ImageEntry val$image;
            private final /* synthetic */ int val$lastFilterId;

            C08981(ImageEntry imageEntry, int i, GalleryPickerActivity galleryPickerActivity) {
                this.val$image = imageEntry;
                this.val$lastFilterId = i;
                this.val$activity = galleryPickerActivity;
            }

            public void run() {
                GalleryPickerUtils.instance();
                if (GalleryPickerUtils.getAreFiltersSupported()) {
                    this.val$image.getStyle(true).setFiltered(this.val$lastFilterId);
                } else {
                    GalleryPickerUtils.showFiltersToast();
                    this.val$image.getStyle(true).setFiltered(0);
                }
                this.val$image.checkStyleTopicality();
                this.val$activity.footerView.updateEditorActionEnabled(Views.ACTION_FILTER, false);
                this.val$activity.getFiltersView().update(0);
            }
        }

        C15382(boolean z, GalleryPickerActivity galleryPickerActivity, ImageEntry imageEntry, int i, int i2) {
            this.val$force = z;
            this.val$activity = galleryPickerActivity;
            this.val$image = imageEntry;
            this.val$index = i;
            this.val$lastFilterId = i2;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run(android.graphics.Bitmap r6) {
            /*
            r5 = this;
            r0 = r5.val$force;
            if (r0 != 0) goto L_0x0020;
        L_0x0004:
            if (r6 == 0) goto L_0x000f;
        L_0x0006:
            com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.instance();
            r0 = com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.getAreFiltersSupported();
            if (r0 != 0) goto L_0x0020;
        L_0x000f:
            r0 = r5.val$activity;
            r1 = new com.vkontakte.android.mediapicker.gallery.InlineUtils$2$1;
            r2 = r5.val$image;
            r3 = r5.val$lastFilterId;
            r4 = r5.val$activity;
            r1.<init>(r2, r3, r4);
            r0.runOnUiThread(r1);
        L_0x001f:
            return;
        L_0x0020:
            r0 = r5.val$force;
            if (r0 != 0) goto L_0x0032;
        L_0x0024:
            r0 = com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.sharedInstance;
            if (r0 == 0) goto L_0x0037;
        L_0x0028:
            r0 = com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.sharedInstance;
            r0 = r0.footerView;
            r0 = r0.getIsExtraActionShowing();
            if (r0 != 0) goto L_0x0037;
        L_0x0032:
            r0 = r5.val$image;
            r0.setImageData(r6);
        L_0x0037:
            r0 = r5.val$force;
            if (r0 != 0) goto L_0x001f;
        L_0x003b:
            r0 = r5.val$activity;
            r0 = r0.getGalleryContext();
            r0 = r0.currentViewerFragment;
            if (r0 == 0) goto L_0x0050;
        L_0x0045:
            r0 = r5.val$activity;
            r0 = r0.getGalleryContext();
            r0 = r0.currentViewerFragment;
            r0.invalidateViewer();
        L_0x0050:
            com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.instance();
            r0 = com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.getAreFiltersSupported();
            if (r0 == 0) goto L_0x001f;
        L_0x0059:
            r0 = r5.val$image;
            r0 = r0.getUseAlternateThumb();
            if (r0 == 0) goto L_0x006b;
        L_0x0061:
            r0 = r5.val$index;
            r1 = r5.val$image;
            r2 = r5.val$activity;
            com.vkontakte.android.mediapicker.gallery.InlineUtils.updateThumbnailForImage(r6, r0, r1, r2);
            goto L_0x001f;
        L_0x006b:
            r6 = 0;
            goto L_0x0061;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.mediapicker.gallery.InlineUtils.2.run(android.graphics.Bitmap):void");
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.InlineUtils.7 */
    class C15397 extends ActionCallback<BitmapEntry> {
        private final /* synthetic */ BitmapEntry val$result;

        C15397(BitmapEntry bitmapEntry) {
            this.val$result = bitmapEntry;
        }

        public void run(BitmapEntry origImage) {
            this.val$result.set((Bitmap) origImage.get());
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.InlineUtils.9 */
    class C15409 extends ActionCallback<Bitmap> {
        private final /* synthetic */ ImageEntry val$image;

        C15409(ImageEntry imageEntry) {
            this.val$image = imageEntry;
        }

        public void run(Bitmap result) {
            this.val$image.setImageData(result);
        }
    }

    public static void applyTextToImage(GalleryPickerActivity activity, boolean inline, String text, ImageEntry image) {
        boolean z = true;
        if (image != null) {
            StyleEntry style = image.getStyle(true);
            if (text == null || text.length() == 0) {
                text = null;
            }
            style.setText(text);
            image.checkStyleTopicality();
            GalleryPickerFooterView galleryPickerFooterView = activity.footerView;
            if (!(image.getIsStyled() && image.getStyle().getIsTexted())) {
                z = false;
            }
            galleryPickerFooterView.updateEditorActionEnabled(Views.ACTION_TEXT, z);
            activity.getGalleryContext().currentViewerFragment.invalidateViewer();
        }
    }

    public static void applyFilter(GalleryPickerActivity activity, int filter_id, ImageEntry image, boolean force) {
        boolean z = true;
        GalleryPickerUtils.instance();
        if (GalleryPickerUtils.getAreFiltersSupported()) {
            boolean wasFiltered;
            int lastFilterId;
            if (image.getIsStyled() && image.getStyle().getIsFiltered()) {
                wasFiltered = true;
            } else {
                wasFiltered = false;
            }
            if (image.getIsStyled() && image.getStyle().getIsFiltered()) {
                lastFilterId = image.getStyle().getFilterId();
            } else {
                lastFilterId = 0;
            }
            if (!force) {
                if (!image.getIsStyled() || image.getStyle().getFilterId() != filter_id) {
                    image.getStyle(true).setFiltered(filter_id);
                    image.checkStyleTopicality();
                } else {
                    return;
                }
            }
            int index = activity.getGalleryContext().currentImageIndex;
            if (!force) {
                GalleryPickerFooterView galleryPickerFooterView = activity.footerView;
                if (filter_id <= 0) {
                    z = false;
                }
                galleryPickerFooterView.updateEditorActionEnabled(Views.ACTION_FILTER, z);
                activity.getFiltersView().update(filter_id);
            }
            Runnable runnable = new C08993(image, wasFiltered, new C15382(force, activity, image, index, lastFilterId));
            if (force) {
                ImageProcessor.instance().dispatch_sync(runnable);
                return;
            } else {
                ImageProcessor.instance().dispatch_async(runnable);
                return;
            }
        }
        image.getStyle(true).setFiltered(0);
        image.checkStyleTopicality();
        activity.runOnUiThread(new C08971());
    }

    public static void updateThumbnailForImage(Bitmap bitmap, int index, ImageEntry image, GalleryPickerActivity activity) {
        ImageProcessor.instance().dispatch_async(new C09004(bitmap, image, activity, index));
    }

    public static void applyEnhance(GalleryPickerActivity activity, ImageEntry inputImage, int imageIndex) {
        if (inputImage != null && inputImage.getImageData() != null && inputImage.getImageData().get() != null) {
            ImageEntry image = new ImageEntry(inputImage);
            BitmapEntry result = new BitmapEntry((Bitmap) inputImage.getImageData().get());
            Runnable ondone = new C09015(activity);
            Runnable onerror = new C09036(ondone, activity, image);
            if (inputImage.getStyle(true).getIsFiltered()) {
                LocalImageLoader.instance().getImage(inputImage, true, false, new C15397(result), true, -1, 1);
            }
            Bitmap enhancedImage = CLAHE.process((Bitmap) result.get());
            if (enhancedImage == null) {
                activity.runOnUiThread(new C09048(onerror));
                return;
            }
            result.set(enhancedImage);
            inputImage.getStyle(true).setEnhanced(true);
            image.getStyle(true).setEnhanced(true);
            image.clearImageData();
            image.setImageData(result);
            if (inputImage.getStyle(true).getIsFiltered()) {
                ImageProcessor.instance().filter(1, image, (Bitmap) image.getImageData().get(), false, true, new C15409(image), -1);
                if (image.getImageData() == null) {
                    inputImage.setImageData((Bitmap) result.get());
                    onerror.run();
                    return;
                }
            }
            activity.runOnUiThread(new AnonymousClass10(inputImage, image, ondone, imageIndex, activity));
        }
    }
}
