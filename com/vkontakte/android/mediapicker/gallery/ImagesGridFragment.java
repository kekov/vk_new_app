package com.vkontakte.android.mediapicker.gallery;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.entries.ImagesGridCallback;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.providers.FragmentClassProvider;
import com.vkontakte.android.mediapicker.ui.LocalImageView;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.LocalImageLoader;
import com.vkontakte.android.mediapicker.utils.SelectionContext;
import java.util.ArrayList;
import java.util.List;

public class ImagesGridFragment extends FragmentClassProvider {
    private ImagesListAdapter adapter;
    private int bucketId;
    private ImagesGridCallback callback;
    private GridView contentView;
    private List<ImageEntry> images;
    private int mColumnSize;
    private int mNumColumns;
    private int mSelectedPosition;
    private OnItemClickListener onItemClickListener;

    /* renamed from: com.vkontakte.android.mediapicker.gallery.ImagesGridFragment.1 */
    class C08951 implements OnItemClickListener {
        C08951() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            ImagesGridFragment.this.openImage(i, (LocalImageView) view);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.ImagesGridFragment.2 */
    class C08962 extends GridView {
        C08962(Context $anonymous0) {
            super($anonymous0);
        }

        public boolean onTouchEvent(MotionEvent event) {
            switch (event.getAction()) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return (SelectionContext.getIsInSelectionMode() || !SelectionContext.getScrollEnabled()) ? false : super.onTouchEvent(event);
                default:
                    return super.onTouchEvent(event);
            }
        }

        public boolean onInterceptTouchEvent(MotionEvent event) {
            if (SelectionContext.getIsInSelectionMode() || !SelectionContext.getScrollEnabled()) {
                return false;
            }
            return super.onInterceptTouchEvent(event);
        }
    }

    private class ImagesListAdapter extends BaseAdapter {
        private ImagesListAdapter() {
        }

        public int getCount() {
            return ImagesGridFragment.this.images.size();
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ImageViewHolder holder = (view == null || view.getTag() == null) ? new ImageViewHolder() : (ImageViewHolder) view.getTag();
            return holder.getView(GalleryPickerUtils.sharedInstance, view, i, (ImageEntry) ImagesGridFragment.this.images.get(i));
        }
    }

    public ImagesGridFragment() {
        this.images = new ArrayList();
        this.onItemClickListener = new C08951();
        this.mSelectedPosition = -1;
    }

    public void setCallback(ImagesGridCallback callback) {
        this.callback = callback;
    }

    public View getGridViewAt(int index) {
        return getContentView().getChildAt(index - getContentView().getFirstVisiblePosition());
    }

    public void openImage(int index, LocalImageView view) {
        if (this.callback != null && index >= 0 && index < this.images.size()) {
            this.callback.onImageOpened(index, (ImageEntry) this.images.get(index), this.images, view);
        }
    }

    public void setAlbumData(int bucketId, List<ImageEntry> images) {
        this.bucketId = bucketId;
        this.images = images;
    }

    public ImageEntry getImageAt(int index) {
        return (ImageEntry) this.images.get(index);
    }

    public int getBucketId() {
        return this.bucketId;
    }

    public GridView getGridView() {
        return getContentView();
    }

    private GridView getContentView() {
        ImageViewHolder.setOnItemClickListener(this.onItemClickListener);
        if (this.contentView != null) {
            return this.contentView;
        }
        this.contentView = new C08962(GalleryPickerUtils.sharedInstance);
        int contentPadding = ActivityClassProvider.dp(ImageViewHolder.PaddingSize);
        this.contentView.setCacheColorHint(Color.WINDOW_BACKGROUND_COLOR);
        this.contentView.setBackgroundColor(Color.WINDOW_BACKGROUND_COLOR);
        this.contentView.setLayoutParams(new LayoutParams(-1, -1));
        this.contentView.setColumnWidth(ActivityClassProvider.dp(ImageViewHolder.GridSize));
        this.contentView.setHorizontalSpacing(ActivityClassProvider.dp(ImageViewHolder.PaddingSize));
        this.contentView.setVerticalSpacing(ActivityClassProvider.dp(ImageViewHolder.PaddingSize));
        this.contentView.setPadding(contentPadding, ActivityClassProvider.dp(50.0f), contentPadding, ActivityClassProvider.dp(ImageViewHolder.PaddingSize));
        this.contentView.setSelector(new ColorDrawable(0));
        this.contentView.setClipToPadding(false);
        this.contentView.setClipChildren(false);
        updateColumnsSize();
        this.contentView.setGravity(17);
        GridView gridView = this.contentView;
        ListAdapter imagesListAdapter = new ImagesListAdapter();
        this.adapter = imagesListAdapter;
        gridView.setAdapter(imagesListAdapter);
        return this.contentView;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (this.contentView == null) {
            return getContentView();
        }
        if (this.contentView.getParent() != null) {
            ((FrameLayout) this.contentView.getParent()).removeView(this.contentView);
        }
        return this.contentView;
    }

    public void updateList() {
        if (this.adapter != null) {
            this.adapter.notifyDataSetChanged();
        }
    }

    public void rememberPosition() {
        this.mSelectedPosition = this.contentView.getFirstVisiblePosition();
    }

    public boolean checkAvailable() {
        return this.contentView.getLastVisiblePosition() > 0 && this.contentView.getFirstVisiblePosition() >= 0 && this.contentView.getLastVisiblePosition() - this.contentView.getFirstVisiblePosition() > 0;
    }

    public void updateColumnsSize() {
        int width = ActivityClassProvider.getStaticLocalContext().getResources().getDisplayMetrics().widthPixels;
        int columnSize = ActivityClassProvider.dp(142.0f);
        if (LocalImageLoader.instance().getThumbTasksLimit() <= 0) {
            int maxSize = ActivityClassProvider.getScreenMaxSize();
            LocalImageLoader.instance().setThumbTasksLimit(((int) Math.ceil((double) (((float) maxSize) / ((float) columnSize)))) * (((int) Math.ceil((double) (((float) ActivityClassProvider.getScreenMinSize()) / ((float) columnSize)))) + 1));
        }
        this.mNumColumns = (int) Math.ceil((double) (((float) width) / ((float) columnSize)));
        this.mColumnSize = ((int) (((float) width) / ((float) this.mNumColumns))) - 2;
        this.contentView.setNumColumns(this.mNumColumns);
        this.contentView.setColumnWidth(this.mColumnSize);
        this.contentView.setSelection(this.mSelectedPosition);
    }

    public int getNumColumns() {
        return this.mNumColumns;
    }

    public int getColumnSize() {
        return this.mColumnSize;
    }
}
