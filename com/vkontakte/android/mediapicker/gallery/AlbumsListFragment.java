package com.vkontakte.android.mediapicker.gallery;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.annotation.TargetApi;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.vkontakte.android.mediapicker.entries.ActionCallback;
import com.vkontakte.android.mediapicker.entries.AlbumEntry;
import com.vkontakte.android.mediapicker.entries.AlbumsListCallback;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.providers.FragmentClassProvider;
import com.vkontakte.android.mediapicker.providers.LangProvider;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.mediapicker.ui.holders.AlbumViewHolder;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.mediapicker.utils.CameraUtils;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.Loggable;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class AlbumsListFragment extends FragmentClassProvider {
    private AlbumsListAdapter albumsListAdapter;
    private ListView albumsListView;
    private AlbumsListCallback callback;
    private FrameLayout contentView;
    private TextView errorText;
    private OnItemClickListener onItemClickListener;

    /* renamed from: com.vkontakte.android.mediapicker.gallery.AlbumsListFragment.1 */
    class C08881 implements OnItemClickListener {
        C08881() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            AlbumEntry album = (AlbumEntry) AlbumsListFragment.this.albumsListAdapter.entries.get(i);
            if (AlbumsListFragment.this.callback != null) {
                AlbumsListFragment.this.callback.onAlbumChosen(i, album);
            }
        }
    }

    public class AlbumsListAdapter extends BaseAdapter {
        private List<AlbumEntry> entries;
        private boolean needAnimation;

        /* renamed from: com.vkontakte.android.mediapicker.gallery.AlbumsListFragment.AlbumsListAdapter.1 */
        class C08891 extends ArrayList<AlbumEntry> {
            C08891() {
            }

            public int indexOf(Object object) {
                if (!(object instanceof Integer)) {
                    return super.indexOf(object);
                }
                Integer bucketId = (Integer) object;
                ListIterator<AlbumEntry> e = listIterator();
                while (e.hasNext()) {
                    if (((AlbumEntry) e.next()).getBucketId() == bucketId.intValue()) {
                        return e.previousIndex();
                    }
                }
                return -1;
            }
        }

        /* renamed from: com.vkontakte.android.mediapicker.gallery.AlbumsListFragment.AlbumsListAdapter.2 */
        class C08912 implements Runnable {

            /* renamed from: com.vkontakte.android.mediapicker.gallery.AlbumsListFragment.AlbumsListAdapter.2.1 */
            class C08901 implements AnimatorListener {
                C08901() {
                }

                public void onAnimationStart(Animator animator) {
                }

                public void onAnimationRepeat(Animator animator) {
                }

                public void onAnimationEnd(Animator animator) {
                    AlbumsListFragment.this.albumsListView.setAlpha(1.0f);
                    AlbumsListFragment.this.albumsListView.setScaleX(1.0f);
                    AlbumsListFragment.this.albumsListView.setScaleY(1.0f);
                }

                public void onAnimationCancel(Animator animator) {
                    AlbumsListFragment.this.albumsListView.setAlpha(1.0f);
                }
            }

            C08912() {
            }

            public void run() {
                AlbumsListFragment.this.albumsListView.animate().alpha(1.0f).scaleX(1.0f).scaleY(1.0f).setListener(new C08901()).setDuration(340);
            }
        }

        public AlbumsListAdapter() {
            this.entries = new C08891();
        }

        public void setEntries(List<AlbumEntry> entries) {
            this.entries.clear();
            this.entries.addAll(entries);
            notifyDataSetChanged();
            if (this.needAnimation) {
                animateListView();
            }
        }

        @TargetApi(14)
        private void animateListView() {
            this.needAnimation = false;
            if (AlbumsListFragment.this.contentView != null && AlbumsListFragment.this.albumsListView != null && VERSION.SDK_INT >= 16) {
                AlbumsListFragment.this.contentView.postDelayed(new C08912(), 10);
            }
        }

        public void prepareListViewAnimation() {
            if (AlbumsListFragment.this.albumsListView != null && VERSION.SDK_INT >= 16) {
                this.needAnimation = true;
                AlbumsListFragment.this.albumsListView.setAlpha(0.0f);
                AlbumsListFragment.this.albumsListView.setScaleX(0.9f);
                AlbumsListFragment.this.albumsListView.setScaleY(0.9f);
            }
        }

        public int getCount() {
            return this.entries.size();
        }

        public Object getItem(int i) {
            return null;
        }

        public boolean isEnabled(int i) {
            return true;
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            AlbumViewHolder holder = (view == null || view.getTag() == null) ? new AlbumViewHolder() : (AlbumViewHolder) view.getTag();
            return holder.getView(GalleryPickerUtils.sharedInstance, view, i, (AlbumEntry) this.entries.get(i));
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.AlbumsListFragment.2 */
    class C15312 extends ActionCallback<List<AlbumEntry>> {
        C15312() {
        }

        public void run(List<AlbumEntry> result) {
            if (result == null || result.size() == 0) {
                AlbumsListFragment.this.albumsListView.setVisibility(8);
                AlbumsListFragment.this.errorText.setVisibility(0);
                AlbumsListFragment.this.updateError(result == null);
                if (result == null) {
                    GalleryPickerUtils.sharedInstance.showRetryButton(false);
                    return;
                }
                return;
            }
            AlbumsListFragment.this.albumsListAdapter.setEntries(result);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.AlbumsListFragment.3 */
    class C15323 extends ActionCallback<Integer[]> {
        C15323() {
        }

        public void run(Integer[] result) {
            if (AlbumsListFragment.this.callback != null) {
                if (!(result[0] == null || result[0].intValue() == -1)) {
                    AlbumsListFragment.this.callback.onCameraAlbumFound(result[0].intValue());
                }
                if (result[1] != null && result[1].intValue() != -1) {
                    AlbumsListFragment.this.callback.onVKCameraAlbumFound(result[1].intValue());
                }
            }
        }
    }

    public AlbumsListFragment() {
        this.onItemClickListener = new C08881();
    }

    public void setCallback(AlbumsListCallback callback) {
        this.callback = callback;
    }

    public void updateAlbumSelectedCount(int bucketId, boolean increment) {
        if (this.albumsListAdapter != null) {
            int index = this.albumsListAdapter.entries.indexOf(Integer.valueOf(bucketId));
            AlbumEntry album = getAlbumByIndex(index);
            if (album != null) {
                album.incrementSelectedCount(increment);
                updateList();
                updateItem(index, album);
            }
        }
    }

    public void updateSomethingNotNecessary() {
        try {
            if (this.albumsListView != null) {
                View view = this.albumsListView.getChildAt(0);
                if (view != null) {
                    view.requestLayout();
                }
            }
        } catch (Throwable throwable) {
            Loggable.Error("Something wrong, but who cares", throwable, new Object[0]);
        }
    }

    public void updateItem(int index, AlbumEntry album) {
        if (this.albumsListView != null) {
            View view = this.albumsListView.getChildAt(index - this.albumsListView.getFirstVisiblePosition());
            if (view != null && view != null && view.getTag() != null && (view.getTag() instanceof AlbumViewHolder)) {
                ((AlbumViewHolder) view.getTag()).update(ActivityClassProvider.getStaticLocalContext(), 0, album);
            }
        }
    }

    public void updateList() {
        if (this.albumsListAdapter != null) {
            this.albumsListAdapter.notifyDataSetChanged();
            if (this.albumsListAdapter.getCount() > 0) {
                this.errorText.setVisibility(8);
                this.albumsListView.setVisibility(0);
            }
        }
    }

    public int getAlbumIndex(AlbumEntry album) {
        return this.albumsListAdapter == null ? -1 : this.albumsListAdapter.entries.indexOf(Integer.valueOf(album.getBucketId()));
    }

    public AlbumEntry getAlbumByBucketId(int bucketId) {
        return this.albumsListAdapter == null ? null : getAlbumByIndex(this.albumsListAdapter.entries.indexOf(Integer.valueOf(bucketId)));
    }

    public AlbumEntry getAlbumByIndex(int index) {
        return (this.albumsListAdapter == null || index < 0 || index >= this.albumsListAdapter.entries.size()) ? null : (AlbumEntry) this.albumsListAdapter.entries.get(index);
    }

    public void addAlbum(AlbumEntry album) {
        this.albumsListAdapter.entries.add(0, album);
        updateList();
    }

    public void onItemClick(int position) {
        this.onItemClickListener.onItemClick(null, null, position, 0);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (this.contentView != null) {
            if (this.contentView.getParent() != null) {
                updateSomethingNotNecessary();
                ((ViewGroup) this.contentView.getParent()).removeView(this.contentView);
            }
            return this.contentView;
        }
        this.contentView = new FrameLayout(GalleryPickerUtils.sharedInstance);
        this.contentView.setBackgroundColor(Color.ALBUMS_LIST_BACKGROUND);
        GalleryPickerUtils.setViewNotFocusable(this.contentView);
        int offsetSize = ActivityClassProvider.dp(GalleryPickerFooterView.SIZE);
        int doubledOffset = (int) (((float) GalleryPickerUtils.sharedInstance.getListPadding()) / ImageViewHolder.PaddingSize);
        int listPaddingVert = offsetSize + doubledOffset;
        this.errorText = new TextView(GalleryPickerUtils.sharedInstance);
        this.errorText.setVisibility(8);
        this.errorText.setTextSize(1, 16.0f);
        this.errorText.setTypeface(Typeface.DEFAULT);
        this.errorText.setTextColor(Color.LIGHT_GRAY);
        this.errorText.setPadding(0, offsetSize, 0, offsetSize);
        this.errorText.setGravity(17);
        this.errorText.setBackgroundColor(Color.ALBUMS_LIST_BACKGROUND);
        this.albumsListView = new ListView(GalleryPickerUtils.sharedInstance);
        this.albumsListView.setLayoutParams(new LayoutParams(-1, -1));
        this.albumsListView.setPadding(0, listPaddingVert, 0, doubledOffset);
        GalleryPickerUtils.setViewNotFocusable(this.albumsListView);
        int i = VERSION.SDK_INT;
        this.albumsListView.setClipToPadding(false);
        this.albumsListView.setCacheColorHint(Color.ALBUMS_LIST_BACKGROUND);
        this.albumsListView.setBackgroundColor(Color.ALBUMS_LIST_BACKGROUND);
        this.albumsListView.setDivider(new ColorDrawable(Color.ALBUMS_LIST_BACKGROUND));
        this.albumsListView.setDividerHeight(0);
        this.albumsListView.setSelector(new ColorDrawable(0));
        this.albumsListView.setOnItemClickListener(this.onItemClickListener);
        ListView listView = this.albumsListView;
        ListAdapter albumsListAdapter = new AlbumsListAdapter();
        this.albumsListAdapter = albumsListAdapter;
        listView.setAdapter(albumsListAdapter);
        GalleryPickerUtils.instance().invokeGetAlbums(GalleryPickerUtils.sharedInstance.getContentResolver(), new C15312(), new C15323());
        if (this.albumsListAdapter != null) {
            this.albumsListAdapter.prepareListViewAnimation();
        }
        this.contentView.addView(this.albumsListView);
        this.contentView.addView(this.errorText, new FrameLayout.LayoutParams(-1, -1, 17));
        return this.contentView;
    }

    private void updateError(boolean internal) {
        if (internal) {
            try {
                if (!CameraUtils.isExternalStorageMounted(true)) {
                    this.errorText.setText(LangProvider.getLocalizedString(33));
                    return;
                }
            } catch (Throwable throwable) {
                Loggable.Error("Error reading environment state", throwable, new Object[0]);
            }
            this.errorText.setText(LangProvider.getLocalizedString(34));
            return;
        }
        this.errorText.setText(LangProvider.getLocalizedString(32));
    }

    public View getViewAt(int index) {
        return this.albumsListView.getChildAt(index - this.albumsListView.getFirstVisiblePosition());
    }
}
