package com.vkontakte.android.mediapicker.gallery;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import com.vkontakte.android.mediapicker.entries.IVAdapter;
import com.vkontakte.android.mediapicker.entries.IVBackgroundListener;
import com.vkontakte.android.mediapicker.entries.IVCallback;
import com.vkontakte.android.mediapicker.entries.IVDismissListener;
import com.vkontakte.android.mediapicker.entries.IVNavigationListener;
import com.vkontakte.android.mediapicker.entries.IVZoomListener;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.FragmentClassProvider;
import com.vkontakte.android.mediapicker.ui.CropperView;
import com.vkontakte.android.mediapicker.ui.ImageViewer;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.Loggable;
import java.util.ArrayList;
import java.util.List;

public class ImageViewerFragment extends FragmentClassProvider {
    private IVAdapter adapter;
    private IVCallback callback;
    private boolean closeEnabled;
    private FrameLayout contentView;
    private CropperView cropperView;
    private int currentIndex;
    private Rect currentRect;
    private View currentThumb;
    private ImageViewer imageViewer;
    private List<ImageEntry> images;
    private int[] loadIndexes;
    private boolean needAnimate;
    private boolean setOnly;

    /* renamed from: com.vkontakte.android.mediapicker.gallery.ImageViewerFragment.6 */
    class C08926 implements OnClickListener {
        C08926() {
        }

        public void onClick(View view) {
            if (ImageViewerFragment.this.callback != null) {
                ImageViewerFragment.this.callback.onClick(view);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.ImageViewerFragment.7 */
    class C08937 implements Runnable {
        C08937() {
        }

        public void run() {
            ImageViewerFragment.this.cropperView.setVisibility(8);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.ImageViewerFragment.8 */
    class C08948 implements Runnable {
        C08948() {
        }

        public void run() {
            ImageViewerFragment.this.cropperView.setVisibility(8);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.ImageViewerFragment.1 */
    class C15331 extends IVAdapter {
        C15331() {
        }

        public int getCount() {
            return ImageViewerFragment.this.images == null ? 0 : ImageViewerFragment.this.images.size();
        }

        public ImageEntry getItemAt(int position) {
            return ImageViewerFragment.this.images == null ? null : (ImageEntry) ImageViewerFragment.this.images.get(position);
        }

        public boolean isPositionAvailable(int position) {
            return ImageViewerFragment.this.images != null && position >= 0 && position < ImageViewerFragment.this.images.size();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.ImageViewerFragment.2 */
    class C15342 extends IVNavigationListener {
        C15342() {
        }

        public void onPositionChanged(int position, ImageEntry image) {
            if (ImageViewerFragment.this.callback != null) {
                ImageViewerFragment.this.callback.onPositionChanged(position, image);
            }
            ImageViewerFragment.this.currentIndex = position;
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.ImageViewerFragment.3 */
    class C15353 implements IVDismissListener {
        C15353() {
        }

        public void onDismiss() {
            if (ImageViewerFragment.this.callback != null) {
                ImageViewerFragment.this.callback.onDismiss();
            }
        }

        public void onPrepareDismiss() {
            if (ImageViewerFragment.this.callback != null) {
                ImageViewerFragment.this.callback.onPrepareDismiss();
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.ImageViewerFragment.4 */
    class C15364 implements IVZoomListener {
        C15364() {
        }

        public void onZoomChanged(float factor, float max, float min) {
            if (ImageViewerFragment.this.callback != null) {
                ImageViewerFragment.this.callback.onZoomChanged(factor, max, min);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gallery.ImageViewerFragment.5 */
    class C15375 implements IVBackgroundListener {
        C15375() {
        }

        public void onUpdated(float ratio) {
            if (ImageViewerFragment.this.callback != null) {
                IVCallback access$1 = ImageViewerFragment.this.callback;
                if ((ImageViewerFragment.this.imageViewer.dismissing || ImageViewerFragment.this.imageViewer.dismissed) && ratio >= 1.0f) {
                    ratio = 0.99f;
                }
                access$1.onBackgroundUpdate(ratio);
            }
        }
    }

    public void setCallback(IVCallback callback) {
        this.callback = callback;
    }

    public ImageViewerFragment() {
        this.adapter = new C15331();
        this.images = new ArrayList();
        this.closeEnabled = true;
    }

    public void setImages(List<ImageEntry> images) {
        this.images = images;
    }

    public List<ImageEntry> getImages() {
        return this.images;
    }

    public int getCurrentIndex() {
        return this.currentIndex;
    }

    public void setCurrentIndex(int index) {
        this.currentIndex = index;
    }

    public void invalidateViewer() {
        this.imageViewer.postInvalidate();
    }

    public void setCloseEnabled(boolean enabled) {
        this.closeEnabled = enabled;
        if (this.imageViewer != null) {
            this.imageViewer.setCloseEnabled(enabled);
        }
    }

    public void animateIn(ImageEntry image, View view, boolean noAnimate) {
        if (image.getIsImageLoaded()) {
            onCreateView(null, null, null);
            this.imageViewer.setThumb((Bitmap) image.getImageData().get());
        }
        this.currentRect = getRectForView(view);
        this.currentThumb = view;
        this.needAnimate = true;
        this.setOnly = noAnimate;
    }

    public void animateOut(View view, Runnable after) {
        if (view == null) {
            view = this.currentThumb;
        }
        Rect rect = getRectForView(view);
        ImageViewer imageViewer = this.imageViewer;
        int i = (rect == null || rect.top >= 0) ? 0 : -rect.top;
        imageViewer.animateOut(rect, i, after);
    }

    public boolean isAnimating() {
        return this.imageViewer != null && this.imageViewer.isAnimating();
    }

    public void setViewerEnabled(boolean enabled) {
        this.imageViewer.setTouchEnabled(enabled);
    }

    public void setSwitchEnabled(boolean enabled) {
        this.imageViewer.setSwitchEnabled(enabled);
    }

    public boolean hasRect() {
        return this.currentRect != null;
    }

    public void clearRect() {
        this.currentRect = null;
    }

    public void clearThumb() {
        clearRect();
        this.currentThumb = null;
    }

    public void setRect(int index, View view) {
        setCurrentThumbVisibility(0);
        this.currentThumb = view;
        this.currentRect = getRectForView(view);
        this.currentIndex = index;
        setCurrentThumbVisibility(4);
    }

    public void updateRect(View view) {
        setCurrentThumbVisibility(0);
        this.currentThumb = view;
        setCurrentThumbVisibility(4);
    }

    private void setCurrentThumbVisibility(int visibility) {
        if (this.currentThumb != null) {
            this.currentThumb.setVisibility(visibility);
        }
    }

    public void showThumb() {
        setCurrentThumbVisibility(0);
    }

    public void clearImages() {
        this.imageViewer.clearImages();
    }

    public void clearImage(int index) {
        this.imageViewer.clearImage(index);
    }

    public void clearNeighborImages(int forIndex) {
        this.imageViewer.clearImage(forIndex - 1);
        this.imageViewer.clearImage(forIndex + 1);
    }

    public void loadNeighborImages(int fromIndex) {
        loadImages(new int[]{fromIndex - 1, fromIndex + 1});
    }

    public void loadImages(int[] indexes) {
        if (this.imageViewer != null) {
            this.imageViewer.load(indexes, false);
        } else {
            this.loadIndexes = indexes;
        }
    }

    private Rect getRectForView(View view) {
        if (view == null) {
            return null;
        }
        View wrap = ((ImageViewHolder) view.getTag()).wrap;
        int offset = ActivityClassProvider.dp(25.0f);
        Rect rect = new Rect();
        rect.top = wrap.getTop() + offset;
        rect.bottom = wrap.getBottom() + offset;
        rect.right = wrap.getRight();
        rect.left = wrap.getLeft();
        return rect;
    }

    private void animate() {
        int i = 0;
        if (this.needAnimate) {
            this.needAnimate = false;
            this.contentView.addView(this.imageViewer.getOverlayView());
            ImageViewer imageViewer = this.imageViewer;
            Rect rect = this.currentRect;
            if (this.currentRect != null && this.currentRect.top < 0) {
                i = -this.currentRect.top;
            }
            imageViewer.animateIn(rect, i, this.setOnly);
            setCurrentThumbVisibility(4);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (this.contentView != null) {
            if (this.contentView.getParent() != null) {
                ((FrameLayout) this.contentView.getParent()).removeView(this.contentView);
            }
            animate();
            return this.contentView;
        }
        this.imageViewer = new ImageViewer(ActivityClassProvider.getStaticLocalContext());
        this.imageViewer.setCloseEnabled(this.closeEnabled);
        this.imageViewer.setCurrentIndex(this.currentIndex);
        this.imageViewer.setOnNavigationListener(new C15342());
        this.imageViewer.setOnDismissListener(new C15353());
        this.imageViewer.setOnZoomListener(new C15364());
        this.imageViewer.setOnBackgroundUpdateListener(new C15375());
        this.imageViewer.setOnClickListener(new C08926());
        this.imageViewer.setAdapter(this.adapter);
        if (this.loadIndexes != null) {
            this.imageViewer.load(this.loadIndexes, true);
        } else {
            this.imageViewer.load(new int[]{this.currentIndex + 1, this.currentIndex - 1}, false);
        }
        this.contentView = new FrameLayout(ActivityClassProvider.getStaticLocalContext());
        this.contentView.setLayoutParams(new LayoutParams(-1, -1));
        this.contentView.addView(this.imageViewer);
        animate();
        return this.contentView;
    }

    public float[] getCropData() {
        ImageEntry image = this.imageViewer.getCurrentImage();
        float sc = this.imageViewer.getCurrentScale();
        int w = image.getCropWidth();
        int h = image.getCropHeight();
        float s = (((float) getCropperSize()) / sc) / ImageViewHolder.PaddingSize;
        float tx = this.imageViewer.getTranslateX() / sc;
        float ty = this.imageViewer.getTranslateY() / sc;
        int x = (int) ((((float) w) / ImageViewHolder.PaddingSize) - tx);
        int y = (int) ((((float) h) / ImageViewHolder.PaddingSize) - ty);
        int x1 = Math.round(((float) x) - s);
        int x2 = Math.round(((float) x) + s);
        int y1 = Math.round(((float) y) - s);
        int y2 = Math.round(((float) y) + s);
        float[] result = new float[9];
        result[0] = (float) x1;
        result[1] = (float) y1;
        result[2] = (float) x2;
        result[3] = (float) y2;
        result[4] = (float) w;
        result[5] = (float) h;
        result[6] = sc;
        result[7] = tx;
        result[8] = ty;
        return result;
    }

    public void applyEmptyCropDataToViewer() {
        applyCropDataToViewer(null, null);
    }

    public void applyCropDataToViewer(float[] coords, ImageEntry image) {
        Loggable.Warn("Coords: " + coords, new Object[0]);
        if (coords == null) {
            this.imageViewer.setPseudoTranslate(0.0f, 0.0f);
            this.imageViewer.setPseudoScale(0.0f);
            return;
        }
        this.imageViewer.setPseudoTranslate(coords[7] * coords[6], coords[8] * coords[6]);
        this.imageViewer.setPseudoScale(coords[6]);
        this.imageViewer.setCurrentScale(coords[6]);
    }

    private int getCropperFullSize() {
        return Math.min(this.imageViewer.getViewportWidth(), this.imageViewer.getViewerHeight());
    }

    private int getCropperSize() {
        return Math.min(this.imageViewer.getViewportWidth(), this.imageViewer.getViewerHeight()) - (getCropperMargin() * 2);
    }

    private int getCropperMargin() {
        return ActivityClassProvider.dp(10.0f);
    }

    private float getCropperScale(ImageEntry image) {
        float cropperSize = (float) getCropperSize();
        if (((float) image.getCropWidth()) / ((float) image.getCropHeight()) > 1.0f) {
            return cropperSize / ((float) image.getCropHeight());
        }
        return cropperSize / ((float) image.getCropWidth());
    }

    public void showCropper(boolean noFade) {
        if (this.cropperView == null) {
            this.cropperView = new CropperView(getActivity());
            this.contentView.addView(this.cropperView);
        } else {
            this.cropperView.setVisibility(0);
        }
        this.cropperView.resetAnimation();
        float cropperSize = (float) getCropperSize();
        int cropperPosition = (int) ((((float) Math.max(this.imageViewer.getViewportWidth(), this.imageViewer.getViewerHeight())) / ImageViewHolder.PaddingSize) - (cropperSize / ImageViewHolder.PaddingSize));
        float scale = getCropperScale(this.imageViewer.getCurrentImage());
        if (this.imageViewer.getViewportWidth() > this.imageViewer.getViewerHeight()) {
            this.cropperView.setBounds(cropperPosition, getCropperMargin(), (int) cropperSize, (int) cropperSize);
        } else {
            this.cropperView.setBounds(getCropperMargin(), cropperPosition, (int) cropperSize, (int) cropperSize);
        }
        this.cropperView.setSquareMode((int) cropperSize);
        this.imageViewer.setMinimumScale((int) cropperSize, scale);
        if (noFade) {
            GalleryPickerUtils.instance().setAlpha(this.cropperView, 1.0f);
        } else {
            GalleryPickerUtils.instance().fadeIn(this.cropperView, null, 200);
        }
    }

    public void animateCrop() {
        this.imageViewer.invalidate();
        this.imageViewer.invalidateOverlayView();
        this.imageViewer.clearMinimumScale(true);
        this.imageViewer.post(new C08937());
    }

    public void hideCropper() {
        this.imageViewer.clearMinimumScale(false);
        GalleryPickerUtils.instance().fadeOut(this.cropperView, new C08948(), 200);
    }

    public void fadeCropper(Runnable after) {
        this.cropperView.fadeOut(after);
    }

    public void setPreventInvalidateViewer(boolean prevent) {
        this.imageViewer.setPreventInvalidate(prevent);
    }

    public void dropTouches() {
        this.imageViewer.dropTouches();
    }
}
