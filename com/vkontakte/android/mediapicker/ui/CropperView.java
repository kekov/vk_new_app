package com.vkontakte.android.mediapicker.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;

public class CropperView extends View {
    private static final int BoldStroke;
    private static final int CircleRadius;
    private static final int FadeOutDuration = 140;
    private static final int MinimumSize;
    private static boolean SuggestSquare = false;
    private static final int TapBound;
    private static final int ThinStroke = 1;
    private long animationEnd;
    private long animationStart;
    private Paint bg_paint;
    private byte[] buttons;
    private int currentHeight;
    private int currentWidth;
    private int currentX;
    private int currentY;
    private boolean dragBottom;
    private boolean dragLeft;
    private boolean dragRight;
    private int dragStartX;
    private int dragStartY;
    private boolean dragTop;
    private int dragX;
    private int dragY;
    private boolean dropTouches;
    private DecelerateInterpolator interpolator;
    private boolean isSquare;
    private int maximumHeight;
    private int maximumWidth;
    private int maximumX;
    private int maximumY;
    private int minimumX;
    private int minimumY;
    private boolean noDrag;
    private Paint paint;

    /* renamed from: com.vkontakte.android.mediapicker.ui.CropperView.1 */
    class C09181 implements Runnable {
        private final /* synthetic */ Runnable val$after;

        C09181(Runnable runnable) {
            this.val$after = runnable;
        }

        public void run() {
            CropperView.this.invalidate();
            if (System.currentTimeMillis() <= CropperView.this.animationEnd) {
                CropperView.this.postDelayed(this, 15);
            } else if (this.val$after != null) {
                this.val$after.run();
            }
        }
    }

    static {
        SuggestSquare = false;
        CircleRadius = ActivityClassProvider.dp(6.0f);
        TapBound = ActivityClassProvider.dp(20.0f);
        MinimumSize = TapBound * 2;
        BoldStroke = Math.max(ThinStroke, ActivityClassProvider.dp(1.5f));
    }

    public CropperView(Context context) {
        super(context);
        this.interpolator = new DecelerateInterpolator();
        this.buttons = new byte[]{(byte) 1, (byte) 1, (byte) 1, (byte) 1};
        this.paint = new Paint();
        this.paint.setAntiAlias(true);
        this.paint.setDither(true);
        this.paint.setStyle(Style.FILL);
        this.paint.setStrokeWidth(1.0f);
        this.bg_paint = new Paint();
        this.bg_paint.setAntiAlias(true);
        this.bg_paint.setColor(Color.WINDOW_BACKGROUND_COLOR);
        this.bg_paint.setDither(true);
        this.bg_paint.setStyle(Style.FILL);
    }

    public void setBounds(int x, int y, int width, int height) {
        this.currentX = x;
        this.minimumX = x;
        this.currentY = y;
        this.minimumY = y;
        this.currentWidth = width;
        this.maximumWidth = width;
        this.currentHeight = height;
        this.maximumHeight = height;
        this.maximumX = this.minimumX + this.maximumWidth;
        this.maximumY = this.minimumY + this.maximumHeight;
    }

    public void setCurrentData(int currentX, int currentY, int currentWidth, int currentHeight) {
        this.currentX = this.minimumX + currentX;
        this.currentWidth = currentWidth;
        this.currentY = this.minimumY + currentY;
        this.currentHeight = currentHeight;
    }

    public void setSourceSize(int width, int height) {
        this.maximumWidth = width;
        this.maximumHeight = height;
    }

    private int getCurrentX2() {
        return this.currentX + this.currentWidth;
    }

    private int getCurrentY2() {
        return this.currentY + this.currentHeight;
    }

    public void resetAnimation() {
        this.animationStart = 0;
        this.animationEnd = 0;
        this.dropTouches = false;
    }

    private void setButtonsVisible(boolean left, boolean top, boolean right, boolean bottom) {
        int i;
        int i2 = ThinStroke;
        if (left) {
            i = ThinStroke;
        } else {
            i = TapBound;
        }
        byte cur = (byte) i;
        if (cur != this.buttons[TapBound]) {
            i = ThinStroke;
        } else {
            i = TapBound;
        }
        boolean changed = false | i;
        this.buttons[TapBound] = cur;
        if (top) {
            i = ThinStroke;
        } else {
            i = TapBound;
        }
        cur = (byte) i;
        if (cur != this.buttons[ThinStroke]) {
            i = ThinStroke;
        } else {
            i = TapBound;
        }
        changed |= i;
        this.buttons[ThinStroke] = cur;
        if (right) {
            i = ThinStroke;
        } else {
            i = TapBound;
        }
        cur = (byte) i;
        if (cur != this.buttons[TapBound]) {
            i = ThinStroke;
        } else {
            i = TapBound;
        }
        changed |= i;
        this.buttons[2] = cur;
        if (bottom) {
            i = ThinStroke;
        } else {
            i = TapBound;
        }
        cur = (byte) i;
        if (cur == this.buttons[3]) {
            i2 = TapBound;
        }
        changed |= i2;
        this.buttons[3] = cur;
        if (changed) {
            invalidate();
        }
    }

    public void fadeOut(Runnable after) {
        this.dropTouches = true;
        this.animationStart = System.currentTimeMillis();
        this.animationEnd = this.animationStart + 140;
        invalidate();
        postDelayed(new C09181(after), 15);
    }

    public void setSquareMode(boolean isSquare) {
        setSquareMode(isSquare ? Math.min(this.maximumWidth, this.maximumHeight) : -1);
    }

    public void setSquareMode(int squareSize) {
        boolean isSquare;
        boolean z;
        boolean z2 = false;
        if (squareSize > 0) {
            isSquare = true;
        } else {
            isSquare = false;
        }
        this.isSquare = isSquare;
        if (isSquare) {
            this.currentHeight = squareSize;
            this.currentWidth = squareSize;
            this.currentX = (this.minimumX + ((int) (((float) (this.maximumX - this.minimumX)) / ImageViewHolder.PaddingSize))) - ((int) (((float) this.currentWidth) / ImageViewHolder.PaddingSize));
            this.currentY = (this.minimumY + ((int) (((float) (this.maximumY - this.minimumY)) / ImageViewHolder.PaddingSize))) - ((int) (((float) this.currentHeight) / ImageViewHolder.PaddingSize));
        }
        if (isSquare) {
            z = false;
        } else {
            z = true;
        }
        boolean z3 = !isSquare;
        boolean z4 = !isSquare;
        if (!isSquare) {
            z2 = true;
        }
        setButtonsVisible(z, z3, z4, z2);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!this.dropTouches && !this.isSquare) {
            int x = Math.round(event.getX());
            int y = Math.round(event.getY());
            int x2;
            int y2;
            switch (event.getAction()) {
                case TapBound:
                    boolean z;
                    this.dragStartX = getCurrentX2();
                    this.dragX = x;
                    this.dragStartY = getCurrentY2();
                    this.dragY = y;
                    x2 = getCurrentX2();
                    y2 = getCurrentY2();
                    int minX = this.currentX - TapBound;
                    int minY = this.currentY - TapBound;
                    int maxX = x2 + TapBound;
                    int maxY = y2 + TapBound;
                    if (y > minY && y < maxY) {
                        if (x > this.currentX - TapBound) {
                            if (x < this.currentX + TapBound) {
                                z = true;
                                this.dragLeft = z;
                                if (x > minX && x < maxX) {
                                    if (y > this.currentY - TapBound) {
                                        if (y < this.currentY + TapBound) {
                                            z = true;
                                            this.dragTop = z;
                                            z = y <= minY && y < maxY && x > x2 - TapBound && x < TapBound + x2;
                                            this.dragRight = z;
                                            z = x <= minX && x < maxX && y > y2 - TapBound && y < TapBound + y2;
                                            this.dragBottom = z;
                                            z = (!this.dragLeft || this.dragTop || this.dragRight || this.dragBottom) ? false : true;
                                            this.noDrag = z;
                                            setButtonsVisible(this.dragLeft, this.dragTop, this.dragRight, this.dragBottom);
                                            break;
                                        }
                                    }
                                }
                                z = false;
                                this.dragTop = z;
                                if (y <= minY) {
                                    break;
                                }
                                this.dragRight = z;
                                if (x <= minX) {
                                }
                                this.dragBottom = z;
                                if (!this.dragLeft) {
                                    break;
                                }
                                this.noDrag = z;
                                setButtonsVisible(this.dragLeft, this.dragTop, this.dragRight, this.dragBottom);
                            }
                        }
                    }
                    z = false;
                    this.dragLeft = z;
                    if (y > this.currentY - TapBound) {
                        if (y < this.currentY + TapBound) {
                            z = true;
                            this.dragTop = z;
                            if (y <= minY) {
                            }
                            this.dragRight = z;
                            if (x <= minX) {
                            }
                            this.dragBottom = z;
                            if (!this.dragLeft) {
                            }
                            this.noDrag = z;
                            setButtonsVisible(this.dragLeft, this.dragTop, this.dragRight, this.dragBottom);
                        }
                    }
                    z = false;
                    this.dragTop = z;
                    if (y <= minY) {
                    }
                    this.dragRight = z;
                    if (x <= minX) {
                    }
                    this.dragBottom = z;
                    if (!this.dragLeft) {
                    }
                    this.noDrag = z;
                    setButtonsVisible(this.dragLeft, this.dragTop, this.dragRight, this.dragBottom);
                    break;
                case ThinStroke /*1*/:
                    setButtonsVisible(true, true, true, true);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    int currentX;
                    int currentY;
                    int i;
                    int dx = x - this.dragX;
                    int dy = y - this.dragY;
                    if (this.noDrag) {
                        currentX = this.currentX + dx;
                        currentY = this.currentY + dy;
                        if (this.currentWidth + currentX > this.maximumX) {
                            currentX = this.maximumX - this.currentWidth;
                        }
                        i = this.minimumX;
                        if (currentX < r0) {
                            currentX = this.minimumX;
                        }
                        if (this.currentHeight + currentY > this.maximumY) {
                            currentY = this.maximumY - this.currentHeight;
                        }
                        i = this.minimumY;
                        if (currentY < r0) {
                            currentY = this.minimumY;
                        }
                        this.currentX = currentX;
                        this.currentY = currentY;
                        this.dragX = x;
                        this.dragY = y;
                        invalidate();
                    }
                    x2 = getCurrentX2();
                    y2 = getCurrentY2();
                    if (this.dragLeft) {
                        currentX = this.currentX + dx;
                        int currentWidth = this.dragStartX - currentX;
                        i = this.minimumX;
                        if (currentX >= r0) {
                            if (currentWidth < MinimumSize) {
                                currentX = x2 - MinimumSize;
                                currentWidth = MinimumSize;
                            }
                            if (SuggestSquare && x2 - this.currentHeight >= this.minimumX && currentWidth - TapBound > this.currentHeight && TapBound + currentWidth < this.currentHeight) {
                                currentX = x2 - this.currentHeight;
                                currentWidth = this.currentHeight;
                            }
                            this.currentX = currentX;
                            this.currentWidth = currentWidth;
                        }
                    } else if (this.dragRight) {
                        float currentWidth2 = (float) (this.currentWidth + dx);
                        if (((float) this.currentX) + currentWidth2 <= ((float) this.maximumX)) {
                            if (currentWidth2 < ((float) MinimumSize)) {
                                currentWidth2 = (float) MinimumSize;
                            }
                            if (SuggestSquare && this.currentHeight + this.currentX <= this.maximumX) {
                                if (currentWidth2 > ((float) (this.currentHeight - TapBound))) {
                                    if (currentWidth2 < ((float) (this.currentHeight + TapBound))) {
                                        currentWidth2 = (float) this.currentHeight;
                                    }
                                }
                            }
                            this.currentWidth = (int) currentWidth2;
                        }
                    }
                    int currentHeight;
                    if (this.dragTop) {
                        currentY = this.currentY + dy;
                        currentHeight = this.dragStartY - currentY;
                        i = this.minimumY;
                        if (currentY >= r0) {
                            if (currentHeight < MinimumSize) {
                                currentY = y2 - MinimumSize;
                                currentHeight = MinimumSize;
                            }
                            if (SuggestSquare && y2 - this.currentWidth >= this.minimumY && currentHeight - TapBound > this.currentWidth && TapBound + currentHeight < this.currentWidth) {
                                currentY = y2 - this.currentWidth;
                                currentHeight = this.currentWidth;
                            }
                            this.currentY = currentY;
                            this.currentHeight = currentHeight;
                        }
                    } else if (this.dragBottom) {
                        currentHeight = this.currentHeight + dy;
                        if (this.currentY + currentHeight <= this.maximumY) {
                            if (currentHeight < MinimumSize) {
                                currentHeight = MinimumSize;
                            }
                            if (SuggestSquare && this.currentWidth + this.currentY <= this.maximumY && currentHeight - TapBound > this.currentWidth && TapBound + currentHeight < this.currentWidth) {
                                currentHeight = this.currentWidth;
                            }
                            this.currentHeight = currentHeight;
                        }
                    }
                    this.dragX = x;
                    this.dragY = y;
                    invalidate();
                    break;
            }
            return true;
        } else if (this.isSquare) {
            return false;
        } else {
            return true;
        }
    }

    public void onDraw(Canvas c) {
        int currentX2 = getCurrentX2();
        int currentY2 = getCurrentY2();
        float ratio = 0.0f;
        if (this.animationStart == 0) {
            this.bg_paint.setAlpha(136);
        } else if (this.animationEnd >= System.currentTimeMillis()) {
            ratio = this.interpolator.getInterpolation(((float) (System.currentTimeMillis() - this.animationStart)) / 140.0f);
            this.bg_paint.setAlpha(((int) (119.0f * ratio)) + 136);
        } else {
            this.bg_paint.setAlpha(MotionEventCompat.ACTION_MASK);
            ratio = 1.0f;
        }
        c.drawRect(0.0f, 0.0f, (float) getWidth(), (float) this.currentY, this.bg_paint);
        c.drawRect(0.0f, (float) currentY2, (float) getWidth(), (float) getHeight(), this.bg_paint);
        c.drawRect(0.0f, (float) this.currentY, (float) this.currentX, (float) currentY2, this.bg_paint);
        c.drawRect((float) currentX2, (float) this.currentY, (float) getWidth(), (float) currentY2, this.bg_paint);
        this.paint.setStyle(Style.STROKE);
        this.paint.setStrokeWidth((float) BoldStroke);
        this.paint.setColor(-657931);
        this.paint.setAlpha(170 - ((int) (170.0f * ratio)));
        c.drawRect((float) this.currentX, (float) this.currentY, (float) currentX2, (float) currentY2, this.paint);
        this.paint.setStrokeWidth(1.0f);
        this.paint.setStyle(Style.FILL);
        this.paint.setAlpha(255 - ((int) (255.0f * ratio)));
        int x = this.currentX;
        int y = this.currentY + (this.currentHeight / 2);
        int d = this.currentHeight / 3;
        c.drawLine((float) x, (float) (this.currentY + d), (float) currentX2, (float) (this.currentY + d), this.paint);
        c.drawLine((float) x, (float) (currentY2 - d), (float) currentX2, (float) (currentY2 - d), this.paint);
        if (this.buttons[TapBound] == (byte) 1) {
            c.drawCircle((float) x, (float) y, (float) CircleRadius, this.paint);
        }
        if (this.buttons[2] == (byte) 1) {
            c.drawCircle((float) currentX2, (float) y, (float) CircleRadius, this.paint);
        }
        x = this.currentX + (this.currentWidth / 2);
        y = this.currentY;
        d = this.currentWidth / 3;
        c.drawLine((float) (this.currentX + d), (float) y, (float) (this.currentX + d), (float) currentY2, this.paint);
        c.drawLine((float) (currentX2 - d), (float) y, (float) (currentX2 - d), (float) currentY2, this.paint);
        if (this.buttons[ThinStroke] == (byte) 1) {
            c.drawCircle((float) x, (float) y, (float) CircleRadius, this.paint);
        }
        if (this.buttons[3] == (byte) 1) {
            c.drawCircle((float) x, (float) currentY2, (float) CircleRadius, this.paint);
        }
    }
}
