package com.vkontakte.android.mediapicker.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;

public class ImageCheckView extends View {
    private static Bitmap bitmap_checked;
    private static Bitmap bitmap_unchecked;
    private static Paint paint;
    private boolean isChecked;

    public ImageCheckView(Context context) {
        super(context);
        this.isChecked = false;
        init(context);
    }

    public boolean getIsChecked() {
        return this.isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    private void init(Context context) {
        if (paint == null) {
            paint = new Paint();
            paint.setFilterBitmap(true);
            paint.setDither(true);
            paint.setAntiAlias(true);
        }
        if (bitmap_checked == null) {
            int size = ActivityClassProvider.dp(24.0f);
            bitmap_unchecked = BitmapFactory.decodeResource(context.getResources(), C0436R.drawable.pe_check);
            bitmap_unchecked = Bitmap.createScaledBitmap(bitmap_unchecked, size, size, true);
            bitmap_checked = BitmapFactory.decodeResource(context.getResources(), C0436R.drawable.pe_checked);
            bitmap_checked = Bitmap.createScaledBitmap(bitmap_checked, size, size, true);
        }
    }

    public void onDraw(Canvas c) {
        c.drawBitmap(this.isChecked ? bitmap_checked : bitmap_unchecked, (float) getPaddingLeft(), (float) getPaddingTop(), paint);
    }
}
