package com.vkontakte.android.mediapicker.ui.holders;

import android.content.Context;
import android.view.View;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.ui.ImageCheckView;
import com.vkontakte.android.mediapicker.ui.LocalImageView;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.SelectionContext;

public class ImageViewHolder extends Holder<ImageEntry> {
    public static final float GridSize = 146.0f;
    public static final float PaddingSize = 2.0f;
    private static int checkPaddingSize;
    private static int checkPaddingSizeSmall;
    private static int checkSize;
    static int measuredWidth;
    private static OnItemClickListener onItemClickListener;
    private static boolean sizes_inited;
    public ImageCheckView check;
    public LocalImageView image;
    public View overlay;
    public FrameLayout wrap;

    /* renamed from: com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder.1 */
    class C09541 extends FrameLayout {
        C09541(Context $anonymous0) {
            super($anonymous0);
        }

        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            ImageViewHolder.measuredWidth = widthMeasureSpec;
            super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder.2 */
    class C15502 extends LocalImageView {
        C15502(Context $anonymous0, int $anonymous1, OnItemClickListener $anonymous2) {
            super($anonymous0, $anonymous1, $anonymous2);
        }

        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            if (ImageViewHolder.measuredWidth == -1) {
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            } else {
                super.onMeasure(ImageViewHolder.measuredWidth, ImageViewHolder.measuredWidth);
            }
        }
    }

    static {
        sizes_inited = false;
        measuredWidth = -1;
    }

    public static void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    protected View initialize(Context context, int viewType, ImageEntry data) {
        if (!sizes_inited) {
            sizes_inited = true;
            checkSize = ActivityClassProvider.dp(24.0f);
            checkPaddingSizeSmall = ActivityClassProvider.dp(PaddingSize);
            checkPaddingSize = ActivityClassProvider.dp(24.0f);
            checkSize += checkPaddingSize + checkPaddingSizeSmall;
        }
        this.wrap = new C09541(context);
        this.wrap.setLayoutParams(new LayoutParams(-1, -1));
        this.image = new C15502(context, -1, onItemClickListener);
        this.image.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.overlay = new View(context);
        this.overlay.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.overlay.setBackgroundResource(C0436R.drawable.pe_selected_border);
        this.wrap.addView(this.image);
        if (!SelectionContext.getSingleMode()) {
            this.check = new ImageCheckView(context);
            this.check.setPadding(checkPaddingSize, checkPaddingSizeSmall, checkPaddingSizeSmall, checkPaddingSize);
            this.wrap.addView(this.check, new FrameLayout.LayoutParams(checkSize, checkSize, 5));
            this.wrap.addView(this.overlay);
        }
        return this.wrap;
    }

    protected void clear() {
        this.wrap = null;
        this.check = null;
        this.image = null;
    }

    protected void getViews(View view, int viewType) {
    }

    protected void update(Context context, int viewType, ImageEntry data) {
        FrameLayout frameLayout = this.wrap;
        int i = (GalleryPickerUtils.sharedInstance == null || GalleryPickerUtils.sharedInstance.getGalleryContext().currentImage == null || GalleryPickerUtils.sharedInstance.getGalleryContext().currentImage.getImageId() != data.getImageId()) ? 0 : 4;
        frameLayout.setVisibility(i);
        this.image.display(data, true, false);
        this.image.setTag(data);
        updateCheck(data.getIsChecked(), this.check);
        updateBorder(data.getIsChecked(), this.overlay);
    }

    public void updateImage(ImageEntry data) {
        this.image.display(data, true, true);
    }

    public static void updateCheck(boolean isChecked, ImageCheckView check) {
        if (check != null && isChecked != check.getIsChecked()) {
            check.setIsChecked(isChecked);
            check.invalidate();
        }
    }

    public static void updateBorder(boolean isChecked, View border) {
        border.setVisibility(isChecked ? 0 : 4);
    }

    protected int getViewType(ImageEntry data) {
        return 0;
    }
}
