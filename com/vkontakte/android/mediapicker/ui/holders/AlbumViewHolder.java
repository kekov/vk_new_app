package com.vkontakte.android.mediapicker.ui.holders;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.mediapicker.GalleryPickerActivity;
import com.vkontakte.android.mediapicker.entries.AlbumEntry;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.mediapicker.ui.LocalImageView;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import org.acra.ACRAConstants;

public class AlbumViewHolder extends Holder<AlbumEntry> {
    private static int camera_offset;
    private static int camera_padding;
    private static int image_size;
    private static int name_padding_bottom;
    private static int name_padding_left;
    private static int name_padding_top;
    private static OnClickListener onClickListener;
    private static int row_size;
    private static boolean sizes_inited;
    private ImageView camera_icon;
    private TextView count;
    private DividerView divider;
    private LocalImageView image;
    private TextView name;
    private RelativeLayout wrap;
    private LinearLayout wrapper;

    /* renamed from: com.vkontakte.android.mediapicker.ui.holders.AlbumViewHolder.1 */
    class C09531 implements OnClickListener {
        C09531() {
        }

        public void onClick(View view) {
            if (GalleryPickerUtils.sharedInstance != null && GalleryPickerUtils.sharedInstance.getGalleryContext().albumsFragment != null) {
                AlbumViewHolder holder = (AlbumViewHolder) view.getTag();
                if (holder != null) {
                    GalleryPickerUtils.sharedInstance.getGalleryContext().albumsFragment.onItemClick(holder.getPosition());
                }
            }
        }
    }

    private static class DividerView extends View {
        private boolean hasDivider;

        public DividerView(Context context) {
            super(context);
            this.hasDivider = true;
        }

        public void setHasDivider(boolean hasDivider) {
            this.hasDivider = hasDivider;
        }

        public void onDraw(Canvas c) {
            c.drawColor(this.hasDivider ? Color.LIST_DIVIDER_COLOR : Color.ALBUMS_LIST_BACKGROUND);
        }
    }

    static {
        sizes_inited = false;
        onClickListener = new C09531();
    }

    protected View initialize(Context context, int viewType, AlbumEntry data) {
        if (!sizes_inited) {
            sizes_inited = true;
            row_size = ActivityClassProvider.dp(74.0f);
            name_padding_left = ActivityClassProvider.dp(GalleryPickerFooterView.BADGE_SIZE);
            name_padding_top = ActivityClassProvider.dp(1.0f);
            name_padding_bottom = ActivityClassProvider.dp(ImageViewHolder.PaddingSize);
            camera_padding = -ActivityClassProvider.dp(4.0f);
            camera_offset = ActivityClassProvider.dp(0.5f);
            image_size = row_size;
            row_size += ActivityClassProvider.dp(1.0f);
        }
        int itemPadding = ((GalleryPickerActivity) context).getListPadding();
        this.wrapper = new LinearLayout(context);
        this.wrapper.setLayoutParams(new LayoutParams(-1, row_size));
        this.wrapper.setPadding(itemPadding, 0, itemPadding, 0);
        this.wrap = new RelativeLayout(context);
        this.wrap.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        GalleryPickerUtils.instance().setSelector(this.wrap, true, Color.SELECTION_TRANSPARENT_COLOR);
        this.image = new LocalImageView(context, image_size);
        this.image.setLayoutParams(new RelativeLayout.LayoutParams(image_size, image_size));
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(1);
        layout.setGravity(16);
        layout.setPadding(name_padding_left + image_size, name_padding_top, 0, 0);
        this.name = new TextView(context);
        this.name.setTextColor(-1);
        this.name.setTextSize(1, GalleryPickerFooterView.BADGE_SIZE);
        this.name.setPadding(0, 0, 0, name_padding_bottom);
        this.count = new TextView(context);
        this.count.setTextColor(Color.TEXT_LIGHT_COLOR);
        this.count.setTextSize(1, 14.0f);
        this.camera_icon = new ImageView(context);
        this.camera_icon.setScaleType(ScaleType.CENTER);
        this.camera_icon.setImageResource(C0436R.drawable.pe_camera_small);
        this.camera_icon.setVisibility(8);
        this.camera_icon.setPadding(camera_padding, 0, 0, camera_offset);
        LinearLayout nameWrap = new LinearLayout(context);
        nameWrap.setOrientation(0);
        nameWrap.setGravity(16);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, ActivityClassProvider.dp(1.0f));
        params.addRule(12);
        params.addRule(14);
        this.divider = new DividerView(context);
        this.divider.setLayoutParams(params);
        this.divider.setPadding(itemPadding, 0, itemPadding, 0);
        params = new RelativeLayout.LayoutParams(-1, -1);
        params.addRule(15);
        nameWrap.addView(this.camera_icon);
        nameWrap.addView(this.name);
        layout.addView(nameWrap);
        layout.addView(this.count);
        this.wrap.addView(this.image);
        this.wrap.addView(layout, params);
        this.wrap.addView(this.divider);
        this.wrapper.addView(this.wrap);
        return this.wrapper;
    }

    protected void clear() {
        this.wrapper = null;
        this.wrap = null;
        this.name = null;
        this.camera_icon = null;
        this.image = null;
        this.count = null;
    }

    protected void getViews(View view, int viewType) {
    }

    public void update(Context context, int viewType, AlbumEntry data) {
        this.name.setText(data.getBucketName());
        this.count.setText(data.getCounter() + (data.getSelectedCount() > 0 ? "; " + data.getSelectedCounter() : ACRAConstants.DEFAULT_STRING_VALUE));
        this.image.display(data.getPreview(), false, false);
        this.divider.setHasDivider(data.getHasNext());
        this.camera_icon.setVisibility(data.getIsCamera() ? 0 : 8);
    }

    public void updateImage(ImageEntry data) {
        this.image.display(data, false, true);
    }

    protected int getViewType(AlbumEntry data) {
        return 0;
    }
}
