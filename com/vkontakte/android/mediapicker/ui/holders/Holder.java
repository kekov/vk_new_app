package com.vkontakte.android.mediapicker.ui.holders;

import android.content.Context;
import android.view.View;

public abstract class Holder<T> {
    private boolean isSelected;
    private int position;
    private int type;
    protected View viewWrap;

    protected abstract void clear();

    protected abstract int getViewType(T t);

    protected abstract void getViews(View view, int i);

    protected abstract View initialize(Context context, int i, T t);

    protected abstract void update(Context context, int i, T t);

    protected void setPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return this.position;
    }

    protected void setType(int type) {
        this.type = type;
    }

    protected int getType() {
        return this.type;
    }

    protected void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    protected boolean isSelected() {
        return this.isSelected;
    }

    public View getView(Context context, View convertView, int position, T data) {
        boolean inited = true;
        int type = getViewType(data);
        if (convertView == null || type != getType()) {
            inited = false;
            clear();
            convertView = initialize(context, type, data);
        }
        if (!inited) {
            setType(type);
            this.viewWrap = convertView;
            getViews(convertView, type);
            convertView.setTag(this);
        }
        setPosition(position);
        update(context, type, data);
        return convertView;
    }
}
