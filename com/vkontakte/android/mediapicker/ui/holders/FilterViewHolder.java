package com.vkontakte.android.mediapicker.ui.holders;

import android.content.Context;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.mediapicker.gallery.InlineUtils;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ResourceProvider.Views;
import com.vkontakte.android.mediapicker.ui.LocalImageView;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.OnClickTouchListener;

public class FilterViewHolder extends Holder<String> {
    private static int image_padding;
    private static int name_padding;
    private static OnTouchListener onTouchListener;
    private static int row_height;
    private static int row_padding;
    private static int row_size;
    private static boolean sizes_inited;
    private LocalImageView image;
    private TextView name;
    private View selection;
    private FrameLayout wrap;

    /* renamed from: com.vkontakte.android.mediapicker.ui.holders.FilterViewHolder.1 */
    class C15491 extends OnClickTouchListener {
        C15491() {
        }

        public void onTapStarted(View view) {
        }

        public void onTapCanceled(View view, boolean beforeCompleted) {
        }

        public void onTapCompleted(View view) {
            if (GalleryPickerUtils.sharedInstance != null && view != null) {
                FilterViewHolder holder = (FilterViewHolder) view.getTag();
                if (holder == null) {
                    return;
                }
                if (!GalleryPickerUtils.getAreFiltersSupported()) {
                    GalleryPickerUtils.showFiltersToast();
                } else if (GalleryPickerUtils.sharedInstance.getGalleryContext().currentImage != null && !GalleryPickerUtils.sharedInstance.getGalleryContext().currentImage.getIsImageFailed()) {
                    InlineUtils.applyFilter(GalleryPickerUtils.sharedInstance, holder.getPosition(), GalleryPickerUtils.sharedInstance.getGalleryContext().currentImage, false);
                }
            }
        }
    }

    static {
        sizes_inited = false;
        onTouchListener = new C15491().setCancelOnViewMove(true).toOnTouchListener();
    }

    protected View initialize(Context context, int viewType, String data) {
        float f = ImageViewHolder.PaddingSize;
        if (!sizes_inited) {
            row_size = ActivityClassProvider.dp(85.0f);
            row_padding = ActivityClassProvider.dp(3.0f);
            row_height = ActivityClassProvider.dp(116.0f);
            name_padding = ActivityClassProvider.dp(9.0f);
            if (ActivityClassProvider.getDensity() >= ImageViewHolder.PaddingSize) {
                f = 3.5f;
            }
            image_padding = ActivityClassProvider.dp(f);
            sizes_inited = true;
        }
        this.wrap = new FrameLayout(context);
        this.wrap.setLayoutParams(new LayoutParams(row_size, row_height));
        this.image = new LocalImageView(context, row_size);
        this.image.setPadding(image_padding, image_padding, image_padding, image_padding);
        this.image.setBackgroundColor(0);
        this.selection = new View(context);
        this.selection.setId(Views.FILTER_SELECTION);
        this.selection.setBackgroundResource(C0436R.drawable.pe_filters_border);
        this.selection.setVisibility(4);
        this.name = new TextView(context);
        this.name.setTypeface(ActivityClassProvider.getDefaultTypeface());
        this.name.setTextSize(1, 13.0f);
        this.name.setTextColor(-1);
        this.name.setGravity(81);
        this.name.setPadding(0, 0, 0, name_padding);
        this.wrap.addView(this.selection, new FrameLayout.LayoutParams(row_size, row_size, 48));
        this.wrap.addView(this.image, new FrameLayout.LayoutParams(row_size, row_size, 48));
        this.wrap.addView(this.name, new FrameLayout.LayoutParams(row_size, row_height, 80));
        this.wrap.setOnTouchListener(onTouchListener);
        return this.wrap;
    }

    protected void clear() {
        this.wrap = null;
        this.image = null;
        this.name = null;
    }

    protected void getViews(View view, int viewType) {
    }

    protected void update(Context context, int viewType, String data) {
        this.name.setText(data);
        if (GalleryPickerUtils.sharedInstance == null || GalleryPickerUtils.sharedInstance.getGalleryContext().currentImage == null || GalleryPickerUtils.sharedInstance.getGalleryContext().currentImage.getFilterId() != getPosition()) {
            this.selection.setVisibility(4);
        } else {
            this.selection.setVisibility(0);
        }
        this.image.displayFilterPreview(getPosition());
    }

    protected int getViewType(String data) {
        return 0;
    }
}
