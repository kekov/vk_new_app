package com.vkontakte.android.mediapicker.ui;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.StateListDrawable;
import android.os.Build.VERSION;
import android.util.StateSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.providers.LangProvider;
import com.vkontakte.android.mediapicker.providers.ResourceProvider;
import com.vkontakte.android.mediapicker.providers.ResourceProvider.Views;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.Loggable;
import com.vkontakte.android.mediapicker.utils.SelectionContext;
import org.acra.ACRAConstants;

public class GalleryPickerFooterView extends RelativeLayout {
    public static final float BADGE_SIZE = 17.0f;
    public static final float BADGE_WRAP_SIZE = 21.0f;
    private static final int[] EDITOR_ACTIONS_LIST;
    private static final boolean ENHANCE_DISABLED = true;
    public static final float SIZE = 48.0f;
    private static final int TAP_LIMIT = 400;
    private static final int TAP_LIMIT_LONG = 600;
    private static long last_tap_time;
    private RelativeLayout actionButtons;
    private TextView badge;
    private FrameLayout badgeWrap;
    private Callback callback;
    private RelativeLayout cancelButton;
    private RelativeLayout cancelButtonBig;
    private ImageView cancelIcon;
    private TextView cancelText;
    private RelativeLayout completeButton;
    private RelativeLayout completeButtonBig;
    private int completeButtonType;
    private ImageView completeIcon;
    private TextView completeText;
    private CropperCallback cropperCallback;
    private LinearLayout editorActions;
    private RelativeLayout editorButtons;
    private View filterActionBottom;
    private boolean mIsBlocked;
    private boolean mIsExtraActionShowing;
    private boolean mIsRetryShowing;
    private int mOrientation;
    private int previousBadge;
    private FrameLayout secondBadgeWrap;

    /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.1 */
    class C09221 implements OnClickListener {
        private final /* synthetic */ int val$type;

        C09221(int i) {
            this.val$type = i;
        }

        public void onClick(View view) {
            if (GalleryPickerFooterView.this.callback != null && !GalleryPickerFooterView.this.mIsBlocked) {
                if (GalleryPickerFooterView.this.cropperCallback != null) {
                    if (this.val$type == 0) {
                        GalleryPickerFooterView.this.cropperCallback.cancel();
                    } else {
                        GalleryPickerFooterView.this.cropperCallback.apply();
                    }
                } else if (this.val$type == 0) {
                    if (GalleryPickerFooterView.checkTapLimit()) {
                        GalleryPickerFooterView.this.callback.onCancelPressed();
                    }
                } else if (GalleryPickerFooterView.this.mIsRetryShowing) {
                    GalleryPickerFooterView.this.callback.onRetryPressed();
                } else {
                    GalleryPickerFooterView.this.callback.onCompletePressed();
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.2 */
    class C09232 implements OnClickListener {
        private final /* synthetic */ int val$type;

        C09232(int i) {
            this.val$type = i;
        }

        public void onClick(View view) {
            if (GalleryPickerFooterView.this.callback != null && !GalleryPickerFooterView.this.mIsBlocked) {
                if (this.val$type == 0) {
                    if (GalleryPickerFooterView.checkTapLimit()) {
                        GalleryPickerFooterView.this.callback.onBigCancelPressed();
                    }
                } else if (GalleryPickerFooterView.checkTapLimit()) {
                    GalleryPickerFooterView.this.callback.onBigCompletePressed();
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.3 */
    class C09243 extends FrameLayout {
        private final /* synthetic */ ImageView val$image;

        C09243(Context $anonymous0, ImageView imageView) {
            this.val$image = imageView;
            super($anonymous0);
        }

        public void setEnabled(boolean enabled) {
            this.val$image.setEnabled(enabled);
        }

        public boolean isEnabled() {
            return this.val$image.isEnabled();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.4 */
    class C09254 implements OnClickListener {
        private final /* synthetic */ int val$action;

        C09254(int i) {
            this.val$action = i;
        }

        public void onClick(View view) {
            if (GalleryPickerFooterView.this.callback != null && GalleryPickerFooterView.this.callback.getEditorActionsAvailable()) {
                switch (this.val$action) {
                    case Views.ACTION_FILTER /*320*/:
                        GalleryPickerFooterView.this.updateFilterActionButton();
                    case Views.ACTION_CROP /*321*/:
                        if (GalleryPickerFooterView.checkTapLimit()) {
                            GalleryPickerFooterView.this.callback.onCropPressed();
                        }
                    case Views.ACTION_TEXT /*322*/:
                        GalleryPickerFooterView.this.callback.onTextPressed();
                    case Views.ACTION_ENHANCE /*323*/:
                        if (GalleryPickerFooterView.checkTapLimit(GalleryPickerFooterView.TAP_LIMIT_LONG)) {
                            GalleryPickerFooterView.this.callback.onEnhancePressed(GalleryPickerFooterView.this.toggleEditorActionEnabled(Views.ACTION_ENHANCE));
                        } else {
                            GalleryPickerFooterView.this.callback.onEnhancePressed(GalleryPickerFooterView.this.toggleEditorActionEnabled(Views.ACTION_ENHANCE));
                        }
                    default:
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.5 */
    class C09265 implements AnimatorListener {
        C09265() {
        }

        public void onAnimationStart(Animator animator) {
        }

        public void onAnimationEnd(Animator animator) {
            GalleryPickerFooterView.this.badge.setScaleX(1.0f);
            GalleryPickerFooterView.this.badge.setScaleY(1.0f);
        }

        public void onAnimationCancel(Animator animator) {
        }

        public void onAnimationRepeat(Animator animator) {
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.6 */
    class C09276 implements AnimatorListener {
        C09276() {
        }

        public void onAnimationStart(Animator animator) {
        }

        public void onAnimationEnd(Animator animator) {
            GalleryPickerFooterView.this.badge.setScaleX(0.0f);
            GalleryPickerFooterView.this.badge.setScaleY(0.0f);
        }

        public void onAnimationCancel(Animator animator) {
        }

        public void onAnimationRepeat(Animator animator) {
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.7 */
    class C09287 implements Runnable {
        private final /* synthetic */ int val$count;

        C09287(int i) {
            this.val$count = i;
        }

        public void run() {
            GalleryPickerFooterView.this.setBadge(this.val$count);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.8 */
    class C09338 implements AnimatorListener {
        private final /* synthetic */ float val$fullOffset;
        private final /* synthetic */ float val$partOffset;

        /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.8.1 */
        class C09321 implements AnimatorListener {
            private final /* synthetic */ float val$fullOffset;
            private final /* synthetic */ float val$partOffset;

            /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.8.1.1 */
            class C09311 implements AnimatorListener {
                private final /* synthetic */ float val$fullOffset;

                /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.8.1.1.1 */
                class C09301 implements AnimatorListener {
                    private final /* synthetic */ float val$fullOffset;

                    /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView.8.1.1.1.1 */
                    class C09291 implements AnimatorListener {
                        C09291() {
                        }

                        public void onAnimationStart(Animator animator) {
                        }

                        public void onAnimationCancel(Animator animator) {
                        }

                        public void onAnimationRepeat(Animator animator) {
                        }

                        public void onAnimationEnd(Animator animator) {
                            GalleryPickerFooterView.this.badge.setTranslationX(0.0f);
                        }
                    }

                    C09301(float f) {
                        this.val$fullOffset = f;
                    }

                    public void onAnimationStart(Animator animator) {
                    }

                    public void onAnimationCancel(Animator animator) {
                    }

                    public void onAnimationRepeat(Animator animator) {
                    }

                    public void onAnimationEnd(Animator animator) {
                        GalleryPickerFooterView.this.badge.setTranslationX(this.val$fullOffset);
                        GalleryPickerFooterView.this.badge.animate().translationX(0.0f).setDuration(60).setListener(new C09291());
                    }
                }

                C09311(float f) {
                    this.val$fullOffset = f;
                }

                public void onAnimationStart(Animator animator) {
                }

                public void onAnimationCancel(Animator animator) {
                }

                public void onAnimationRepeat(Animator animator) {
                }

                public void onAnimationEnd(Animator animator) {
                    GalleryPickerFooterView.this.badge.setTranslationX(-this.val$fullOffset);
                    GalleryPickerFooterView.this.badge.animate().translationX(this.val$fullOffset).setDuration(50).setListener(new C09301(this.val$fullOffset));
                }
            }

            C09321(float f, float f2) {
                this.val$partOffset = f;
                this.val$fullOffset = f2;
            }

            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                GalleryPickerFooterView.this.badge.setTranslationX(this.val$partOffset);
                GalleryPickerFooterView.this.badge.animate().translationX(-this.val$fullOffset).setDuration(50).setListener(new C09311(this.val$fullOffset));
            }
        }

        C09338(float f, float f2) {
            this.val$partOffset = f;
            this.val$fullOffset = f2;
        }

        public void onAnimationStart(Animator animator) {
        }

        public void onAnimationCancel(Animator animator) {
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationEnd(Animator animator) {
            GalleryPickerFooterView.this.badge.setTranslationX(-this.val$partOffset);
            GalleryPickerFooterView.this.badge.animate().translationX(this.val$partOffset).setDuration(60).setListener(new C09321(this.val$partOffset, this.val$fullOffset));
        }
    }

    private static final class ButtonType {
        public static final int CANCEL = 0;
        public static final int COMPLETE = 1;

        private ButtonType() {
        }
    }

    public interface Callback {
        boolean getEditorActionsAvailable();

        void onBigCancelPressed();

        void onBigCompletePressed();

        void onCancelPressed();

        void onCompletePressed();

        void onCropPressed();

        void onEnhancePressed(boolean z);

        boolean onFiltersPressed();

        void onRetryPressed();

        void onTextPressed();
    }

    public static abstract class CropperCallback {
        public abstract void onCropApply();

        public abstract void onCropReset();

        public void apply() {
            if (GalleryPickerFooterView.checkTapLimit()) {
                onCropApply();
            }
        }

        public void cancel() {
            if (GalleryPickerFooterView.checkTapLimit()) {
                onCropReset();
            }
        }
    }

    static {
        int[] iArr;
        if (GalleryPickerUtils.getIsDeviceShitty()) {
        }
        if (VERSION.SDK_INT >= 11) {
            iArr = new int[]{Views.ACTION_FILTER, Views.ACTION_TEXT, Views.ACTION_CROP};
        } else {
            iArr = new int[]{Views.ACTION_FILTER, Views.ACTION_CROP};
        }
        EDITOR_ACTIONS_LIST = iArr;
        last_tap_time = 0;
    }

    public GalleryPickerFooterView(Context context, int completeButtonType, Callback callback) {
        this(context, completeButtonType);
        setCallback(callback);
    }

    public GalleryPickerFooterView(Context context, int completeButtonType) {
        super(context);
        this.previousBadge = -1;
        this.mIsExtraActionShowing = false;
        this.mIsBlocked = false;
        this.mOrientation = -1;
        this.completeButtonType = completeButtonType;
        setBackgroundResource(C0436R.drawable.pe_bottom_bg);
        updateSizeByOrientation(context.getResources().getConfiguration().orientation);
        addView(getActionButtonsLayout(context));
    }

    public static boolean checkTapLimit() {
        return checkTapLimit(TAP_LIMIT);
    }

    public static boolean checkTapLimit(int limit) {
        boolean result = (last_tap_time == 0 || System.currentTimeMillis() - last_tap_time > ((long) limit)) ? ENHANCE_DISABLED : false;
        if (result) {
            last_tap_time = System.currentTimeMillis();
        }
        return result;
    }

    public void setCompleteButtonType(int type) {
        this.completeButtonType = type;
        updateCompleteButtonText(getCompleteButtonString());
    }

    private int getCompleteButtonString() {
        if (GalleryPickerUtils.sharedInstance == null || this.completeButtonType == 0) {
            return 1;
        }
        return this.completeButtonType == 1 ? 11 : 6;
    }

    private View getDividerView(Context context, int id) {
        LayoutParams layoutParams = new LayoutParams(ActivityClassProvider.dp(1.0f), -1);
        layoutParams.setMargins(0, ActivityClassProvider.dp(8.0f), 0, ActivityClassProvider.dp(8.0f));
        View divider = new View(context);
        divider.setId(id);
        divider.setBackgroundColor(Color.DIVIDER_COLOR);
        divider.setLayoutParams(layoutParams);
        return divider;
    }

    private FrameLayout getBadgeWrap(Context context) {
        FrameLayout result = new FrameLayout(context);
        result.setClipChildren(false);
        result.setClipToPadding(false);
        result.setPadding(ActivityClassProvider.dp(4.0f), 0, ActivityClassProvider.dp(4.0f), 0);
        result.setLayoutParams(new LayoutParams(-2, ActivityClassProvider.dp(BADGE_WRAP_SIZE)));
        return result;
    }

    private RelativeLayout getActionButtonsLayout(Context context) {
        if (this.actionButtons != null) {
            return this.actionButtons;
        }
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        this.actionButtons = new RelativeLayout(context);
        this.actionButtons.setLayoutParams(layoutParams);
        View dividerView = getDividerView(context, Views.DIVIDER_VIEW);
        layoutParams = (LayoutParams) dividerView.getLayoutParams();
        layoutParams.addRule(13);
        this.actionButtons.addView(dividerView, layoutParams);
        layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(9);
        layoutParams.addRule(0, Views.DIVIDER_VIEW);
        RelativeLayout relativeLayout = new RelativeLayout(context);
        this.cancelButton = relativeLayout;
        fillActionButton(context, 0, relativeLayout);
        this.actionButtons.addView(this.cancelButton, layoutParams);
        layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(11);
        layoutParams.addRule(1, Views.DIVIDER_VIEW);
        relativeLayout = new RelativeLayout(context);
        this.completeButton = relativeLayout;
        fillActionButton(context, 1, relativeLayout);
        this.actionButtons.addView(this.completeButton, layoutParams);
        updateButtonsMaxWidth();
        return this.actionButtons;
    }

    private void fillActionButton(Context context, int type, RelativeLayout layout) {
        LayoutParams layoutParams;
        int stringId = type == 0 ? 2 : getCompleteButtonString();
        int imageEnabledId = type == 0 ? C0436R.drawable.pe_cancel : C0436R.drawable.pe_ok;
        int imageDisabledId = type == 0 ? C0436R.drawable.pe_cancel_disabled : C0436R.drawable.pe_ok_disabled;
        int viewId = type == 0 ? Views.CANCEL_BUTTON : Views.COMPLETE_BUTTON;
        new LayoutParams(-2, -2).addRule(13);
        LinearLayout button = new LinearLayout(context);
        button.setOrientation(0);
        button.setGravity(17);
        button.setId(viewId);
        states = new int[2][];
        states[0] = new int[]{16842910};
        states[1] = new int[]{-16842910};
        int i = 2;
        int[] colors = new int[]{Color.LIGHT_GRAY, Color.DARK_GRAY};
        TextView text = new TextView(context);
        text.setTextSize(0, (float) ActivityClassProvider.dp(15.0f));
        text.setSingleLine(ENHANCE_DISABLED);
        text.setTypeface(ActivityClassProvider.getDefaultTypeface(ENHANCE_DISABLED));
        text.setText(getLocalizedString(stringId).toUpperCase());
        text.setTextColor(new ColorStateList(states, colors));
        text.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        ImageView image = new ImageView(context);
        image.setImageDrawable(getStateListDrawable(imageDisabledId, imageEnabledId));
        image.setLayoutParams(new LinearLayout.LayoutParams(ActivityClassProvider.dp(BADGE_WRAP_SIZE), ActivityClassProvider.dp(BADGE_WRAP_SIZE)));
        int textOffset = ActivityClassProvider.dp(8.0f);
        int vertOffset = ActivityClassProvider.dp(0.25f);
        int buttonOffset = ActivityClassProvider.dp(1.0f);
        if (type == 0) {
            text.setPadding(textOffset, vertOffset, 0, 0);
            button.setPadding(0, 0, 0, buttonOffset);
        } else {
            text.setPadding(textOffset, vertOffset, 0, 0);
            button.setPadding(0, 0, 0, buttonOffset);
        }
        button.addView(image);
        button.addView(text);
        if (type == 0) {
            this.cancelText = text;
            this.cancelIcon = image;
        } else {
            this.completeText = text;
            this.completeIcon = image;
            this.badge = new TextView(context);
            this.badge.setBackgroundResource(C0436R.drawable.pe_badge);
            this.badge.setTextColor(-1);
            this.badge.setGravity(17);
            this.badge.setPadding(ActivityClassProvider.dp(4.0f), 0, ActivityClassProvider.dp(4.0f), ActivityClassProvider.dp(3.0f));
            this.badge.setTextSize(1, 12.0f);
            this.badge.setTypeface(ActivityClassProvider.getDefaultTypeface(ENHANCE_DISABLED));
            this.badge.setLayoutParams(new FrameLayout.LayoutParams(-2, ActivityClassProvider.dp(BADGE_SIZE), 17));
            this.badgeWrap = getBadgeWrap(context);
            this.badgeWrap.addView(this.badge);
            layoutParams = (LayoutParams) this.badgeWrap.getLayoutParams();
            layoutParams.addRule(1, viewId);
            layoutParams.addRule(2, viewId);
            layoutParams.setMargins(-ActivityClassProvider.dp(5.0f), 0, 0, -ActivityClassProvider.dp(15.0f));
            layout.addView(this.badgeWrap, layoutParams);
            int i2 = this.previousBadge;
            if (i == -1) {
                updateBadge(0, ENHANCE_DISABLED);
            }
        }
        layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(13);
        layout.addView(button, layoutParams);
        if (type == 0) {
            updateCancelButtonEnabled(ENHANCE_DISABLED);
        } else {
            updateCompleteButtonEnabled(false);
        }
        layout.setOnClickListener(new C09221(type));
        GalleryPickerUtils.instance().setSelector(layout);
    }

    private RelativeLayout getEditorActionsLayout(Context context) {
        if (this.editorButtons != null) {
            return this.editorButtons;
        }
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        this.editorButtons = new RelativeLayout(context);
        this.editorButtons.setLayoutParams(layoutParams);
        layoutParams = new LayoutParams(ActivityClassProvider.dp(68.0f), -1);
        layoutParams.addRule(9);
        RelativeLayout relativeLayout = new RelativeLayout(context);
        this.cancelButtonBig = relativeLayout;
        fillActionButtonBig(context, 0, relativeLayout);
        this.editorButtons.addView(this.cancelButtonBig, layoutParams);
        layoutParams = new LayoutParams(ActivityClassProvider.dp(68.0f), -1);
        layoutParams.addRule(11);
        relativeLayout = new RelativeLayout(context);
        this.completeButtonBig = relativeLayout;
        fillActionButtonBig(context, 1, relativeLayout);
        this.editorButtons.addView(this.completeButtonBig, layoutParams);
        if (!SelectionContext.getPreventStyling()) {
            View dividerView = getDividerView(context, Views.DIVIDER_VIEW_LEFT);
            layoutParams = (LayoutParams) dividerView.getLayoutParams();
            layoutParams.addRule(1, Views.CANCEL_BUTTON_BIG);
            this.editorButtons.addView(dividerView, layoutParams);
            dividerView = getDividerView(context, Views.DIVIDER_VIEW_RIGHT);
            layoutParams = (LayoutParams) dividerView.getLayoutParams();
            layoutParams.addRule(0, Views.COMPLETE_BUTTON_BIG);
            this.editorButtons.addView(dividerView, layoutParams);
        }
        this.secondBadgeWrap = getBadgeWrap(context);
        layoutParams = (LayoutParams) this.secondBadgeWrap.getLayoutParams();
        layoutParams.addRule(1, Views.COMPLETE_BUTTON_BIG_ICON);
        layoutParams.addRule(2, Views.COMPLETE_BUTTON_BIG_ICON);
        layoutParams.setMargins(-ActivityClassProvider.dp(9.0f), 0, 0, -ActivityClassProvider.dp(9.0f));
        this.completeButtonBig.addView(this.secondBadgeWrap, layoutParams);
        layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(1, Views.DIVIDER_VIEW_LEFT);
        layoutParams.addRule(0, Views.DIVIDER_VIEW_RIGHT);
        LinearLayout linearLayout = new LinearLayout(context);
        this.editorActions = linearLayout;
        fillEdtiorActionsWrap(context, linearLayout);
        this.editorButtons.addView(this.editorActions, layoutParams);
        addView(this.editorButtons);
        return this.editorButtons;
    }

    private void fillActionButtonBig(Context context, int type, RelativeLayout layout) {
        int viewId = type == 0 ? Views.CANCEL_BUTTON_BIG : Views.COMPLETE_BUTTON_BIG;
        int iconId = type == 0 ? SelectionContext.getIsSingleImageShowing() ? C0436R.drawable.pe_big_cancel : C0436R.drawable.pe_mosaic_big : C0436R.drawable.pe_big_ok;
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(13);
        ImageView icon = new ImageView(context);
        icon.setScaleType(ScaleType.CENTER);
        icon.setImageResource(iconId);
        if (type == 1) {
            icon.setId(Views.COMPLETE_BUTTON_BIG_ICON);
        }
        layout.addView(icon, layoutParams);
        layout.setId(viewId);
        layout.setOnClickListener(new C09232(type));
        GalleryPickerUtils.instance().setSelector(layout);
    }

    private void fillEdtiorActionsWrap(Context context, LinearLayout layout) {
        layout.setOrientation(0);
        for (int action : EDITOR_ACTIONS_LIST) {
            if (action == Views.ACTION_FILTER) {
                if (!(((ActivityManager) context.getSystemService("activity")).getDeviceConfigurationInfo().reqGlEsVersion >= Menu.CATEGORY_SYSTEM ? ENHANCE_DISABLED : false)) {
                }
            }
            ImageView image = new ImageView(context);
            image.setScaleType(ScaleType.CENTER);
            FrameLayout actionButton = new C09243(context, image);
            actionButton.setId(action);
            actionButton.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
            actionButton.addView(image, new FrameLayout.LayoutParams(-1, -1, 17));
            actionButton.setEnabled(false);
            GalleryPickerUtils.instance().setSelector(actionButton);
            if (action == Views.ACTION_FILTER) {
                this.filterActionBottom = new View(context);
                this.filterActionBottom.setLayoutParams(new FrameLayout.LayoutParams(-1, ActivityClassProvider.dp(3.0f), 80));
                this.filterActionBottom.setBackgroundColor(Color.SELECTION_COLOR);
                this.filterActionBottom.setVisibility(4);
                actionButton.addView(this.filterActionBottom);
            }
            switch (action) {
                case Views.ACTION_FILTER /*320*/:
                    image.setImageDrawable(getStateListDrawable(C0436R.drawable.pe_icon_filters, C0436R.drawable.pe_icon_filters_selected));
                    break;
                case Views.ACTION_CROP /*321*/:
                    image.setImageDrawable(getStateListDrawable(C0436R.drawable.pe_icon_crop, C0436R.drawable.pe_icon_crop_selected));
                    break;
                case Views.ACTION_TEXT /*322*/:
                    image.setImageDrawable(getStateListDrawable(C0436R.drawable.pe_icon_text, C0436R.drawable.pe_icon_text_selected));
                    break;
                case Views.ACTION_ENHANCE /*323*/:
                    image.setImageDrawable(getStateListDrawable(C0436R.drawable.pe_icon_contrast, C0436R.drawable.pe_icon_contrast_selected));
                    break;
            }
            actionButton.setOnClickListener(new C09254(action));
            if (!SelectionContext.getPreventStyling()) {
                layout.addView(actionButton);
            }
        }
    }

    public void updateButtonsMaxWidth() {
        int maxSize;
        if (ResourceProvider.getApplicationContext().getResources().getConfiguration().orientation == 1) {
            maxSize = ActivityClassProvider.getScreenMinSize();
        } else {
            maxSize = ActivityClassProvider.getScreenMaxSize();
        }
        maxSize = (maxSize / 2) - ActivityClassProvider.dp(65.0f);
        if (this.completeText != null) {
            this.completeText.setMaxWidth(maxSize);
        }
        if (this.cancelText != null) {
            this.cancelText.setMaxWidth(maxSize);
        }
    }

    public void updateFilterActionButton() {
        boolean result = this.callback.onFiltersPressed();
        if (this.filterActionBottom != null) {
            this.filterActionBottom.setVisibility(result ? 0 : 4);
        }
    }

    public void showFilters() {
        if (this.filterActionBottom == null || this.filterActionBottom.getVisibility() != 0) {
            updateFilterActionButton();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        return ENHANCE_DISABLED;
    }

    private String getLocalizedString(int string) {
        String result = LangProvider.getLocalizedString(string);
        return result == null ? ACRAConstants.DEFAULT_STRING_VALUE : result;
    }

    private StateListDrawable getStateListDrawable(int... resources) {
        StateListDrawable drawable = new StateListDrawable();
        if (resources.length > 1) {
            drawable.addState(new int[]{16842910}, getContext().getResources().getDrawable(resources[1]));
        }
        if (resources.length > 0) {
            drawable.addState(StateSet.WILD_CARD, getContext().getResources().getDrawable(resources[0]));
        }
        return drawable;
    }

    private void setBadge(int count) {
        this.badge.setText(new StringBuilder(String.valueOf(count)).toString());
    }

    public void updateBadgeVisible(boolean visible) {
        this.badge.setVisibility(visible ? 0 : 4);
    }

    private void updateBadge(int count, boolean animated) {
        boolean z = false;
        float f = 1.0f;
        if (VERSION.SDK_INT <= 11 || !animated || this.previousBadge == count) {
            if (VERSION.SDK_INT > 11) {
                this.badge.setScaleX(count > 0 ? 1.0f : 0.0f);
                TextView textView = this.badge;
                if (count <= 0) {
                    f = 0.0f;
                }
                textView.setScaleY(f);
            } else {
                this.badge.setVisibility(count > 0 ? 0 : 4);
            }
            this.previousBadge = count;
            setBadge(count);
        } else {
            updateBadgeAnimated(count);
        }
        if (count > 0) {
            z = ENHANCE_DISABLED;
        }
        updateCompleteButtonEnabled(z);
    }

    @TargetApi(12)
    private void updateBadgeAnimated(int count) {
        boolean increased;
        boolean show;
        boolean hide;
        boolean init;
        boolean z = false;
        if (count > this.previousBadge) {
            increased = ENHANCE_DISABLED;
        } else {
            increased = false;
        }
        if (this.previousBadge != 0 || count <= 0) {
            show = false;
        } else {
            show = ENHANCE_DISABLED;
        }
        if (this.previousBadge == 0 || count != 0) {
            hide = false;
        } else {
            hide = ENHANCE_DISABLED;
        }
        if (this.previousBadge == -1) {
            init = ENHANCE_DISABLED;
        } else {
            init = false;
        }
        this.previousBadge = count;
        if (init) {
            this.badge.setScaleX(0.0f);
            this.badge.setScaleY(0.0f);
        } else if (show) {
            setBadge(count);
            try {
                this.badge.animate().scaleX(1.0f).scaleY(1.0f).setDuration(97).setListener(new C09265());
            } catch (Throwable throwable) {
                Loggable.Error("Cannot show badge animated, setting scale", throwable, new Object[0]);
                this.badge.setScaleX(1.0f);
                this.badge.setScaleY(1.0f);
            }
        } else if (hide) {
            try {
                this.badge.animate().scaleX(0.0f).scaleY(0.0f).setDuration(97).setListener(new C09276());
            } catch (Throwable throwable2) {
                Loggable.Error("Cannot hide badge animated, setting scale", throwable2, new Object[0]);
                this.badge.setScaleX(0.0f);
                this.badge.setScaleY(0.0f);
            }
        } else {
            Runnable setter = new C09287(count);
            GalleryPickerUtils instance = GalleryPickerUtils.instance();
            View view = this.badge;
            if (!increased) {
                z = ENHANCE_DISABLED;
            }
            instance.bounce(view, z, setter);
        }
    }

    public void updateCompleteButtonText(int string) {
        if (this.completeText != null) {
            this.completeText.setText(getLocalizedString(string).toUpperCase());
        }
    }

    public void updateCancelButtonText(int string) {
        if (this.cancelText != null) {
            this.cancelText.setText(getLocalizedString(string).toUpperCase());
        }
    }

    public void updateCancelButtonIcon(int drawable) {
        if (this.cancelIcon != null) {
            this.cancelIcon.setImageResource(drawable);
        }
    }

    public void updateCompleteButtonEnabled(boolean enabled) {
        if (enabled != this.completeButton.isEnabled()) {
            this.completeButton.setEnabled(enabled);
            this.completeText.setEnabled(enabled);
            this.completeIcon.setEnabled(enabled);
        }
    }

    public void updateCompleteButtonIcon(int... resources) {
        this.completeIcon.setImageDrawable(getStateListDrawable(resources));
    }

    public void updateCancelButtonEnabled(boolean enabled) {
        if (enabled != this.cancelButton.isEnabled()) {
            this.cancelButton.setEnabled(enabled);
            this.cancelText.setEnabled(enabled);
            this.cancelIcon.setEnabled(enabled);
        }
    }

    public void setBadge(int count, boolean animated) {
        updateBadge(count, animated);
    }

    public void updateEditorActionsEnabled(boolean filter, boolean text, boolean crop, boolean enchance) {
        if (this.editorActions != null) {
            View view = this.editorActions.findViewById(Views.ACTION_FILTER);
            if (view != null) {
                view.setEnabled(filter);
            }
            view = this.editorActions.findViewById(Views.ACTION_TEXT);
            if (view != null) {
                view.setEnabled(text);
            }
            view = this.editorActions.findViewById(Views.ACTION_CROP);
            if (view != null) {
                view.setEnabled(crop);
            }
            view = this.editorActions.findViewById(Views.ACTION_ENHANCE);
            if (view != null) {
                view.setEnabled(enchance);
            }
        }
    }

    public void updateEditorActionEnabled(int action, boolean enabled) {
        if (this.editorActions != null) {
            View view = this.editorActions.findViewById(action);
            if (view != null) {
                view.setEnabled(enabled);
            }
        }
    }

    public boolean toggleEditorActionEnabled(int action) {
        boolean z = false;
        if (this.editorActions == null) {
            return false;
        }
        View view = this.editorActions.findViewById(action);
        if (view == null) {
            return false;
        }
        if (!view.isEnabled()) {
            z = ENHANCE_DISABLED;
        }
        view.setEnabled(z);
        return view.isEnabled();
    }

    public void showEditorAction(boolean show) {
        int i = 8;
        getEditorActionsLayout(getContext()).setVisibility(show ? 0 : 8);
        RelativeLayout actionButtonsLayout = getActionButtonsLayout(getContext());
        if (!show) {
            i = 0;
        }
        actionButtonsLayout.setVisibility(i);
        if (this.badge.getParent() != null) {
            ((FrameLayout) this.badge.getParent()).removeView(this.badge);
        }
        (show ? this.secondBadgeWrap : this.badgeWrap).addView(this.badge);
    }

    public void displayCropperAction(CropperCallback callback, int caption, int caption2) {
        boolean z;
        boolean z2 = false;
        if (callback != null) {
            z = ENHANCE_DISABLED;
        } else {
            z = false;
        }
        this.mIsExtraActionShowing = z;
        this.cropperCallback = callback;
        if (getIsExtraActionShowing()) {
            z = false;
        } else {
            z = ENHANCE_DISABLED;
        }
        showEditorAction(z);
        if (!getIsExtraActionShowing()) {
            caption = getCompleteButtonString();
        }
        updateCompleteButtonText(caption);
        if (!getIsExtraActionShowing()) {
            caption2 = 2;
        }
        updateCancelButtonText(caption2);
        if (getIsExtraActionShowing() || SelectionContext.getSelectedCount() > 0) {
            z2 = ENHANCE_DISABLED;
        }
        updateCompleteButtonEnabled(z2);
    }

    public void hideCropperAction() {
        displayCropperAction(null, 7, 5);
    }

    public boolean getIsExtraActionShowing() {
        return this.mIsExtraActionShowing;
    }

    public void updateIsRetryShowing(boolean isShowing) {
        this.mIsRetryShowing = isShowing;
        updateCompleteButtonEnabled(isShowing);
        updateCompleteButtonText(isShowing ? 4 : getCompleteButtonString());
        updateCompleteButtonIcon(isShowing ? new int[]{C0436R.drawable.pe_refresh} : new int[]{C0436R.drawable.pe_ok_disabled, C0436R.drawable.pe_ok});
    }

    public void setIsBlocked(boolean isBlocked) {
        this.mIsBlocked = isBlocked;
    }

    public void updateSizeByOrientation(int orientation) {
        if (this.mOrientation == -1 || orientation != this.mOrientation) {
            this.mOrientation = orientation;
            if (orientation == 1) {
                setLayoutParams(new FrameLayout.LayoutParams(-1, ActivityClassProvider.dp(SIZE), 80));
            } else {
                setLayoutParams(new FrameLayout.LayoutParams(-1, ActivityClassProvider.dp(SIZE), 80));
            }
        }
    }

    @TargetApi(12)
    public void shakeBadge() {
        if (VERSION.SDK_INT >= 12) {
            setBadge(SelectionContext.getSelectedCount());
            float offset = ((float) this.badge.getMeasuredWidth()) / 6.0f;
            float partOffset = offset / 2.5f;
            this.badge.animate().translationX(-partOffset).setDuration(60).setListener(new C09338(partOffset, offset / 1.75f));
        }
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }
}
