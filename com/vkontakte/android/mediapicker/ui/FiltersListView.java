package com.vkontakte.android.mediapicker.ui;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.mediapicker.gl.ImageProcessor;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.AnimationDuration;
import com.vkontakte.android.mediapicker.providers.ResourceProvider.Views;
import com.vkontakte.android.mediapicker.ui.holders.FilterViewHolder;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.SelectionContext;

public class FiltersListView extends FrameLayout {
    boolean filled;
    private LinearLayout filtersList;
    private HorizontalScrollView filtersScrollView;
    private int last_filter_id;
    public boolean mIsShowing;

    /* renamed from: com.vkontakte.android.mediapicker.ui.FiltersListView.1 */
    class C09191 implements AnimatorListener {
        C09191() {
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationStart(Animator animator) {
            FiltersListView.this.setVisibility(0);
            FiltersListView.this.setTranslationY((float) ActivityClassProvider.dp(116.0f));
            FiltersListView.this.setAlpha(0.0f);
        }

        public void onAnimationEnd(Animator animator) {
            FiltersListView.this.setAlpha(1.0f);
            FiltersListView.this.setTranslationY(0.0f);
        }

        public void onAnimationCancel(Animator animator) {
            FiltersListView.this.setAlpha(1.0f);
            FiltersListView.this.setTranslationY(0.0f);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.FiltersListView.2 */
    class C09202 implements Runnable {
        C09202() {
        }

        public void run() {
            FiltersListView.this.setVisibility(8);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.FiltersListView.3 */
    class C09213 implements AnimatorListener {
        private final /* synthetic */ float val$translation;

        C09213(float f) {
            this.val$translation = f;
        }

        public void onAnimationStart(Animator animator) {
            FiltersListView.this.setVisibility(0);
            FiltersListView.this.setTranslationY(0.0f);
            FiltersListView.this.setAlpha(1.0f);
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationEnd(Animator animator) {
            FiltersListView.this.setAlpha(0.0f);
            FiltersListView.this.setTranslationY(this.val$translation);
            FiltersListView.this.setVisibility(8);
        }

        public void onAnimationCancel(Animator animator) {
            FiltersListView.this.setAlpha(0.0f);
            FiltersListView.this.setTranslationY(this.val$translation);
            FiltersListView.this.setVisibility(8);
        }
    }

    public FiltersListView(Context context) {
        super(context);
        this.last_filter_id = 0;
        this.filled = false;
        this.mIsShowing = false;
        init(context);
    }

    private void init(Context context) {
        int listPadding = ActivityClassProvider.dp(5.0f);
        setBackgroundResource(C0436R.drawable.pe_filters_bg);
        this.filtersList = new LinearLayout(context);
        this.filtersList.setLayoutParams(new LayoutParams(-2, -1));
        this.filtersList.setOrientation(0);
        this.filtersScrollView = new HorizontalScrollView(context);
        this.filtersScrollView.setLayoutParams(new LayoutParams(-1, -1));
        setPadding(0, listPadding, 0, listPadding);
        this.filtersScrollView.addView(this.filtersList);
        if (VERSION.SDK_INT >= 9) {
            this.filtersScrollView.setOverScrollMode(2);
        }
        this.filtersScrollView.setHorizontalScrollBarEnabled(false);
        this.filtersScrollView.setHorizontalFadingEdgeEnabled(false);
        addView(this.filtersScrollView);
        setVisibility(8);
    }

    private void fillFiltersScrollView() {
        if (!this.filled && !SelectionContext.getPreventStyling()) {
            this.filled = true;
            int listPadding = ActivityClassProvider.dp(5.0f);
            View view = new View(GalleryPickerUtils.sharedInstance);
            view.setLayoutParams(new ViewGroup.LayoutParams(listPadding, -1));
            this.filtersList.addView(view);
            for (int i = 0; i < ImageProcessor.Filters.length; i++) {
                view = new FilterViewHolder().getView(GalleryPickerUtils.sharedInstance, null, i, ImageProcessor.Filters[i][0]);
                view.setId(i);
                this.filtersList.addView(view);
            }
            view = new View(GalleryPickerUtils.sharedInstance);
            view.setLayoutParams(new ViewGroup.LayoutParams(listPadding, -1));
            this.filtersList.addView(view);
        }
    }

    public boolean getIsShowing() {
        return this.mIsShowing;
    }

    public void updateForImageViewer(boolean isViewerShowing) {
        if (!getIsShowing()) {
            return;
        }
        if (isViewerShowing) {
            show(false);
        } else {
            hide(false);
        }
    }

    public boolean toggle(boolean animated) {
        boolean z = false;
        if (SelectionContext.getPreventStyling()) {
            return false;
        }
        if (!this.mIsShowing) {
            z = true;
        }
        this.mIsShowing = z;
        if (this.mIsShowing) {
            show(animated);
        } else {
            hide(animated);
        }
        return this.mIsShowing;
    }

    public void showIfHidden(boolean animated) {
        if (!this.mIsShowing) {
            if (animated) {
                this.mIsShowing = true;
            }
            show(animated);
        }
    }

    public void show(boolean animated) {
        if (!SelectionContext.getPreventStyling()) {
            fillFiltersScrollView();
            if (getVisibility() != 0) {
                setVisibility(0);
                if (VERSION.SDK_INT < 12 || !animated) {
                    GalleryPickerUtils.instance().fadeIn(this, null, animated ? AnimationDuration.FILTERS_APPEARING : GalleryPickerUtils.getThumbnailAnimationDuration());
                } else {
                    showAnimated();
                }
            }
        }
    }

    @TargetApi(12)
    private void showAnimated() {
        animate().cancel();
        setVisibility(0);
        setTranslationY((float) ActivityClassProvider.dp(116.0f));
        setAlpha(0.0f);
        animate().translationY(0.0f).alpha(1.0f).setDuration(135).setListener(new C09191());
    }

    public void hideIfShown(boolean animated) {
        if (this.mIsShowing) {
            if (animated) {
                this.mIsShowing = false;
            }
            hide(animated);
        }
    }

    public void hide(boolean animated) {
        if (getVisibility() != 8) {
            if (VERSION.SDK_INT < 12 || !animated) {
                int i;
                GalleryPickerUtils instance = GalleryPickerUtils.instance();
                Runnable c09202 = new C09202();
                if (animated) {
                    i = AnimationDuration.FILTERS_APPEARING;
                } else {
                    i = GalleryPickerUtils.getThumbnailAnimationDuration();
                }
                instance.fadeOut(this, c09202, i);
                return;
            }
            hideAnimated();
        }
    }

    @TargetApi(12)
    private void hideAnimated() {
        float translation = (float) ActivityClassProvider.dp(116.0f);
        animate().cancel();
        animate().translationY(translation).alpha(0.0f).setDuration(135).setListener(new C09213(translation));
    }

    private void updateVisibilityOnPosition(int position, boolean visible) {
        View item = this.filtersList.getChildAt(position + 1);
        if (item != null) {
            View view = item.findViewById(Views.FILTER_SELECTION);
            if (view != null) {
                view.setVisibility(visible ? 0 : 4);
            }
        }
    }

    public void update(int position) {
        updateVisibilityOnPosition(this.last_filter_id, false);
        this.last_filter_id = position;
        updateVisibilityOnPosition(position, true);
        scrollToPosition(position);
    }

    private void scrollToPosition(int position) {
    }

    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }
}
