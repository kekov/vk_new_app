package com.vkontakte.android.mediapicker.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.MotionEventCompat;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.Scroller;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.vkontakte.android.mediapicker.entries.ActionCallback;
import com.vkontakte.android.mediapicker.entries.BitmapEntry;
import com.vkontakte.android.mediapicker.entries.IVAdapter;
import com.vkontakte.android.mediapicker.entries.IVBackgroundListener;
import com.vkontakte.android.mediapicker.entries.IVDismissListener;
import com.vkontakte.android.mediapicker.entries.IVNavigationListener;
import com.vkontakte.android.mediapicker.entries.IVZoomListener;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.gl.TextPainter;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.LocalImageLoader;
import com.vkontakte.android.mediapicker.utils.Loggable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class ImageViewer extends View {
    private static final int DEFAULT_MAXIMUM_SCALE = 2;
    public static final boolean DRAW_RECT_BACKGROUND = false;
    public static final int MOVING_ANIMATION_DURATION = 250;
    public static final int PAGE_SPACING;
    private static Paint bg_paint;
    private static boolean bg_paint_inited;
    private static Paint bitmap_paint;
    private static boolean bitmap_paint_inited;
    private static float density;
    private static boolean density_inited;
    private static ColorDrawable window_bg;
    private static boolean window_bg_inited;
    private IVAdapter adapter;
    private long animationDuration;
    private float animationScale;
    private long animationStartTime;
    private float animationTranslateX;
    private float animationTranslateY;
    private float bg_alpha;
    private boolean closeEnabled;
    private int currentIndex;
    private float currentScale;
    private boolean disallowZoom;
    public boolean dismissed;
    public boolean dismissing;
    private boolean doubleTapped;
    private int dragStartPointerId;
    private float dragStartTranslateX;
    private float dragStartTranslateY;
    private float dragStartX;
    private float dragStartY;
    private boolean dragging;
    private boolean fitBySmallestSide;
    private GestureDetector gestureDetector;
    private boolean inAnimationDone;
    private boolean initialized;
    private Interpolator interpolator;
    private float lastBgAlpha;
    private int limitX;
    private int limitY;
    private float maximumScale;
    private int maximumX;
    private int maximumY;
    private float minimumScale;
    private int minimumX;
    private int minimumY;
    private boolean needDropTouches;
    private int nextIndex;
    private float nextScale;
    private IVBackgroundListener onBackgroundUpdateListener;
    private IVDismissListener onDismissListener;
    private OnDoubleTapListener onDoubleTapListener;
    private OnGestureListener onGestureListener;
    private OnClickListener onImageClickListener;
    private IVNavigationListener onNavigationListener;
    private IVZoomListener onZoomListener;
    private boolean outAnimationDone;
    public FrameLayout overlayView;
    private float pinchCenterX;
    private float pinchCenterY;
    private long pinchEndTime;
    private float pinchStartDist;
    private float pinchStartScale;
    private Runnable postedPhotoSwitch;
    private boolean preventInvalidate;
    private int previousCallbackIndex;
    private int previousIndex;
    private float pseudoScale;
    private float pseudoTranslateX;
    private float pseudoTranslateY;
    private int pseudoViewportSize;
    private float scaleStartTx;
    private float scaleStartTy;
    int[] screenData;
    int[] screenData2;
    int[] screenData3;
    private Scroller scroller;
    private int secondIndex;
    private boolean secondIsNext;
    private boolean switchDisabled;
    private Rect thumbnailRect;
    private ViewWithClipping thumbnailView;
    private boolean touchDisabled;
    private float touchSlopX;
    private float touchSlopY;
    private float translateX;
    private float translateY;
    private int viewerHeight;
    private int viewerWidth;

    /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.10 */
    class AnonymousClass10 implements OnPreDrawListener {
        private final /* synthetic */ Runnable val$after;
        private final /* synthetic */ long val$animationStarted;
        private final /* synthetic */ int val$clipSize;
        private final /* synthetic */ Rect val$rect;

        /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.10.1 */
        class C09391 extends AnimatorListenerAdapter {
            private final /* synthetic */ Runnable val$after;
            private final /* synthetic */ long val$animationStarted;

            /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.10.1.1 */
            class C09381 implements Runnable {
                private final /* synthetic */ Runnable val$after;

                C09381(Runnable runnable) {
                    this.val$after = runnable;
                }

                public void run() {
                    ImageViewer.this.overlayView.setBackgroundDrawable(null);
                    ImageViewer.this.thumbnailView.setVisibility(8);
                    ImageViewer.this.inAnimationDone = true;
                    ImageViewer.this.onPositionChanged(-1);
                    ImageViewer.this.invalidate();
                    ImageViewer.this.clearImages();
                    if (this.val$after != null) {
                        this.val$after.run();
                    }
                }
            }

            C09391(long j, Runnable runnable) {
                this.val$animationStarted = j;
                this.val$after = runnable;
            }

            public void onAnimationEnd(Animator a) {
                int delay = (int) ((System.currentTimeMillis() - this.val$animationStarted) - ImageViewer.this.animationDuration);
                new C09381(this.val$after).run();
            }
        }

        AnonymousClass10(Rect rect, int i, long j, Runnable runnable) {
            this.val$rect = rect;
            this.val$clipSize = i;
            this.val$animationStarted = j;
            this.val$after = runnable;
        }

        public boolean onPreDraw() {
            if (!ImageViewer.this.outAnimationDone) {
                return true;
            }
            ImageViewer.this.thumbnailView.getViewTreeObserver().removeOnPreDrawListener(this);
            ImageViewer.this.overlayView.setBackgroundDrawable(ImageViewer.window_bg);
            ImageViewer.this.outAnimationDone = ImageViewer.DRAW_RECT_BACKGROUND;
            ImageViewer.this.invalidate();
            int[] pos = new int[ImageViewer.DEFAULT_MAXIMUM_SCALE];
            ImageViewer.this.thumbnailView.getLocationOnScreen(pos);
            float scaleX = ((float) this.val$rect.width()) / ((float) ImageViewer.this.thumbnailView.getWidth());
            float scaleY = ((float) this.val$rect.height()) / ((float) ImageViewer.this.thumbnailView.getHeight());
            float transX = (float) (this.val$rect.left - pos[0]);
            float transY = (float) (this.val$rect.top - pos[1]);
            float scale = Math.max(scaleX, scaleY);
            float tRatio = ((float) this.val$rect.width()) / ((float) this.val$rect.height());
            float ivRatio = ((float) ImageViewer.this.thumbnailView.getWidth()) / ((float) ImageViewer.this.thumbnailView.getHeight());
            int clipV = 0;
            int clipH = 0;
            if (tRatio != ivRatio) {
                if (tRatio > ivRatio) {
                    float th = ((float) ImageViewer.this.thumbnailView.getHeight()) * scale;
                    clipV = Math.round(((th - ((float) this.val$rect.height())) / ImageViewHolder.PaddingSize) / scale);
                    transY -= (th - ((float) this.val$rect.height())) / ImageViewHolder.PaddingSize;
                } else {
                    float tw = ((float) ImageViewer.this.thumbnailView.getWidth()) * scale;
                    clipH = Math.round(((tw - ((float) this.val$rect.width())) / ImageViewHolder.PaddingSize) / scale);
                    transX -= (tw - ((float) this.val$rect.width())) / ImageViewHolder.PaddingSize;
                }
            }
            ImageViewer.this.thumbnailView.setPivotX(0.0f);
            ImageViewer.this.thumbnailView.setPivotY(0.0f);
            ImageViewer.this.thumbnailView.setScaleX(1.0f);
            ImageViewer.this.thumbnailView.setScaleY(1.0f);
            ImageViewer.this.thumbnailView.setTranslationX(0.0f);
            ImageViewer.this.thumbnailView.setTranslationY(ImageViewer.this.translateY);
            AnimatorSet set = new AnimatorSet();
            r18 = new Animator[8];
            float[] fArr = new float[]{scale};
            r18[0] = ObjectAnimator.ofFloat(ImageViewer.this.thumbnailView, "scaleX", fArr);
            fArr = new float[]{scale};
            r18[1] = ObjectAnimator.ofFloat(ImageViewer.this.thumbnailView, "scaleY", fArr);
            fArr = new float[]{transX};
            r18[ImageViewer.DEFAULT_MAXIMUM_SCALE] = ObjectAnimator.ofFloat(ImageViewer.this.thumbnailView, "translationX", fArr);
            fArr = new float[]{transY};
            r18[3] = ObjectAnimator.ofFloat(ImageViewer.this.thumbnailView, "translationY", fArr);
            int[] iArr = new int[ImageViewer.DEFAULT_MAXIMUM_SCALE];
            iArr[0] = Math.round(ImageViewer.this.bg_alpha * 255.0f);
            iArr[1] = 0;
            r18[4] = ObjectAnimator.ofInt(ImageViewer.window_bg, "alpha", iArr);
            ImageViewer imageViewer = ImageViewer.this;
            iArr = new int[ImageViewer.DEFAULT_MAXIMUM_SCALE];
            iArr[0] = 0;
            iArr[1] = clipV;
            r18[5] = ObjectAnimator.ofInt(r0.thumbnailView, "clipBottom", iArr);
            imageViewer = ImageViewer.this;
            iArr = new int[ImageViewer.DEFAULT_MAXIMUM_SCALE];
            iArr[0] = 0;
            iArr[1] = Math.round(((float) this.val$clipSize) / scale) + clipV;
            r18[6] = ObjectAnimator.ofInt(r0.thumbnailView, "clipTop", iArr);
            imageViewer = ImageViewer.this;
            iArr = new int[ImageViewer.DEFAULT_MAXIMUM_SCALE];
            iArr[0] = 0;
            iArr[1] = clipH;
            r18[7] = ObjectAnimator.ofInt(r0.thumbnailView, "clipHorizontal", iArr);
            set.playTogether(r18);
            set.setDuration((long) GalleryPickerUtils.getThumbnailAnimationDuration());
            set.addListener(new C09391(this.val$animationStarted, this.val$after));
            set.start();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.12 */
    class AnonymousClass12 implements OnPreDrawListener {
        private final /* synthetic */ int val$clipSize;
        private final /* synthetic */ Runnable val$ender;
        private final /* synthetic */ Rect val$rect;

        /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.12.1 */
        class C09401 extends AnimatorListenerAdapter {
            private final /* synthetic */ Runnable val$ender;

            C09401(Runnable runnable) {
                this.val$ender = runnable;
            }

            public void onAnimationEnd(Animator a) {
                this.val$ender.run();
            }
        }

        AnonymousClass12(Rect rect, int i, Runnable runnable) {
            this.val$rect = rect;
            this.val$clipSize = i;
            this.val$ender = runnable;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onPreDraw() {
            /*
            r20 = this;
            r0 = r20;
            r13 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r13 = r13.thumbnailView;
            r13 = r13.getViewTreeObserver();
            r0 = r20;
            r13.removeOnPreDrawListener(r0);
            r0 = r20;
            r13 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r13 = r13.thumbnailView;
            r0 = r20;
            r14 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r14 = r14.screenData3;
            r13.getLocationOnScreen(r14);
            r0 = r20;
            r13 = r0.val$rect;
            r13 = r13.width();
            r13 = (float) r13;
            r0 = r20;
            r14 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r14 = r14.thumbnailView;
            r14 = r14.getWidth();
            r14 = (float) r14;
            r5 = r13 / r14;
            r0 = r20;
            r13 = r0.val$rect;
            r13 = r13.height();
            r13 = (float) r13;
            r0 = r20;
            r14 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r14 = r14.thumbnailView;
            r14 = r14.getHeight();
            r14 = (float) r14;
            r6 = r13 / r14;
            r0 = r20;
            r13 = r0.val$rect;
            r13 = r13.left;
            r0 = r20;
            r14 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r14 = r14.screenData3;
            r15 = 0;
            r14 = r14[r15];
            r13 = r13 - r14;
            r10 = (float) r13;
            r0 = r20;
            r13 = r0.val$rect;
            r13 = r13.top;
            r0 = r20;
            r14 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r14 = r14.screenData3;
            r15 = 1;
            r14 = r14[r15];
            r13 = r13 - r14;
            r11 = (float) r13;
            r4 = java.lang.Math.max(r5, r6);
            r0 = r20;
            r13 = r0.val$rect;
            r13 = r13.width();
            r13 = (float) r13;
            r0 = r20;
            r14 = r0.val$rect;
            r14 = r14.height();
            r14 = (float) r14;
            r8 = r13 / r14;
            r0 = r20;
            r13 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r13 = r13.thumbnailView;
            r13 = r13.getWidth();
            r13 = (float) r13;
            r0 = r20;
            r14 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r14 = r14.thumbnailView;
            r14 = r14.getHeight();
            r14 = (float) r14;
            r3 = r13 / r14;
            r2 = 0;
            r1 = 0;
            r13 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1));
            if (r13 == 0) goto L_0x00e3;
        L_0x00ae:
            r13 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1));
            if (r13 <= 0) goto L_0x0268;
        L_0x00b2:
            r0 = r20;
            r13 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r13 = r13.thumbnailView;
            r13 = r13.getHeight();
            r13 = (float) r13;
            r9 = r13 * r4;
            r0 = r20;
            r13 = r0.val$rect;
            r13 = r13.height();
            r13 = (float) r13;
            r13 = r9 - r13;
            r14 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
            r13 = r13 / r14;
            r13 = r13 / r4;
            r2 = java.lang.Math.round(r13);
            r0 = r20;
            r13 = r0.val$rect;
            r13 = r13.height();
            r13 = (float) r13;
            r13 = r9 - r13;
            r14 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
            r13 = r13 / r14;
            r11 = r11 - r13;
        L_0x00e3:
            r13 = 0;
            r2 = java.lang.Math.max(r13, r2);
            r13 = 0;
            r1 = java.lang.Math.max(r13, r1);
            r0 = r20;
            r13 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r13 = r13.thumbnailView;
            r14 = 0;
            r13.setPivotX(r14);
            r0 = r20;
            r13 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r13 = r13.thumbnailView;
            r14 = 0;
            r13.setPivotY(r14);
            r0 = r20;
            r13 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r13 = r13.thumbnailView;
            r13.setScaleX(r4);
            r0 = r20;
            r13 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r13 = r13.thumbnailView;
            r13.setScaleY(r4);
            r0 = r20;
            r13 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r13 = r13.thumbnailView;
            r13.setTranslationX(r10);
            r0 = r20;
            r13 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r13 = r13.thumbnailView;
            r13.setTranslationY(r11);
            r7 = new android.animation.AnimatorSet;
            r7.<init>();
            r13 = 8;
            r13 = new android.animation.Animator[r13];
            r14 = 0;
            r0 = r20;
            r15 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r15 = r15.thumbnailView;
            r16 = "scaleX";
            r17 = 1;
            r0 = r17;
            r0 = new float[r0];
            r17 = r0;
            r18 = 0;
            r19 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
            r17[r18] = r19;
            r15 = android.animation.ObjectAnimator.ofFloat(r15, r16, r17);
            r13[r14] = r15;
            r14 = 1;
            r0 = r20;
            r15 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r15 = r15.thumbnailView;
            r16 = "scaleY";
            r17 = 1;
            r0 = r17;
            r0 = new float[r0];
            r17 = r0;
            r18 = 0;
            r19 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
            r17[r18] = r19;
            r15 = android.animation.ObjectAnimator.ofFloat(r15, r16, r17);
            r13[r14] = r15;
            r14 = 2;
            r0 = r20;
            r15 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r15 = r15.thumbnailView;
            r16 = "translationX";
            r17 = 1;
            r0 = r17;
            r0 = new float[r0];
            r17 = r0;
            r18 = 0;
            r19 = 0;
            r17[r18] = r19;
            r15 = android.animation.ObjectAnimator.ofFloat(r15, r16, r17);
            r13[r14] = r15;
            r14 = 3;
            r0 = r20;
            r15 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r15 = r15.thumbnailView;
            r16 = "translationY";
            r17 = 1;
            r0 = r17;
            r0 = new float[r0];
            r17 = r0;
            r18 = 0;
            r19 = 0;
            r17[r18] = r19;
            r15 = android.animation.ObjectAnimator.ofFloat(r15, r16, r17);
            r13[r14] = r15;
            r14 = 4;
            r15 = com.vkontakte.android.mediapicker.ui.ImageViewer.window_bg;
            r16 = "alpha";
            r17 = 2;
            r0 = r17;
            r0 = new int[r0];
            r17 = r0;
            r17 = {0, 255};
            r15 = android.animation.ObjectAnimator.ofInt(r15, r16, r17);
            r13[r14] = r15;
            r14 = 5;
            r0 = r20;
            r15 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r15 = r15.thumbnailView;
            r16 = "clipBottom";
            r17 = 2;
            r0 = r17;
            r0 = new int[r0];
            r17 = r0;
            r18 = 0;
            r17[r18] = r2;
            r18 = 1;
            r19 = 0;
            r17[r18] = r19;
            r15 = android.animation.ObjectAnimator.ofInt(r15, r16, r17);
            r13[r14] = r15;
            r14 = 6;
            r0 = r20;
            r15 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r15 = r15.thumbnailView;
            r16 = "clipTop";
            r17 = 2;
            r0 = r17;
            r0 = new int[r0];
            r17 = r0;
            r18 = 0;
            r0 = r20;
            r0 = r0.val$clipSize;
            r19 = r0;
            r0 = r19;
            r0 = (float) r0;
            r19 = r0;
            r19 = r19 / r4;
            r19 = java.lang.Math.round(r19);
            r19 = r19 + r2;
            r17[r18] = r19;
            r18 = 1;
            r19 = 0;
            r17[r18] = r19;
            r15 = android.animation.ObjectAnimator.ofInt(r15, r16, r17);
            r13[r14] = r15;
            r14 = 7;
            r0 = r20;
            r15 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r15 = r15.thumbnailView;
            r16 = "clipHorizontal";
            r17 = 2;
            r0 = r17;
            r0 = new int[r0];
            r17 = r0;
            r18 = 0;
            r17[r18] = r1;
            r18 = 1;
            r19 = 0;
            r17[r18] = r19;
            r15 = android.animation.ObjectAnimator.ofInt(r15, r16, r17);
            r13[r14] = r15;
            r7.playTogether(r13);
            r13 = com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.getThumbnailAnimationDuration();
            r13 = (long) r13;
            r7.setDuration(r13);
            r13 = new com.vkontakte.android.mediapicker.ui.ImageViewer$12$1;
            r0 = r20;
            r14 = r0.val$ender;
            r0 = r20;
            r13.<init>(r14);
            r7.addListener(r13);
            r7.start();
            r13 = 1;
            return r13;
        L_0x0268:
            r0 = r20;
            r13 = com.vkontakte.android.mediapicker.ui.ImageViewer.this;
            r13 = r13.thumbnailView;
            r13 = r13.getWidth();
            r13 = (float) r13;
            r12 = r13 * r4;
            r0 = r20;
            r13 = r0.val$rect;
            r13 = r13.width();
            r13 = (float) r13;
            r13 = r12 - r13;
            r14 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
            r13 = r13 / r14;
            r13 = r13 / r4;
            r1 = java.lang.Math.round(r13);
            r0 = r20;
            r13 = r0.val$rect;
            r13 = r13.width();
            r13 = (float) r13;
            r13 = r12 - r13;
            r14 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
            r13 = r13 / r14;
            r10 = r10 - r13;
            goto L_0x00e3;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.mediapicker.ui.ImageViewer.12.onPreDraw():boolean");
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.1 */
    class C09411 implements OnGestureListener {
        C09411() {
        }

        public boolean onDown(MotionEvent motionEvent) {
            return ImageViewer.DRAW_RECT_BACKGROUND;
        }

        public void onShowPress(MotionEvent motionEvent) {
        }

        public boolean onSingleTapUp(MotionEvent motionEvent) {
            return ImageViewer.DRAW_RECT_BACKGROUND;
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float v, float v2) {
            return ImageViewer.DRAW_RECT_BACKGROUND;
        }

        public void onLongPress(MotionEvent motionEvent) {
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float velocityX, float velocityY) {
            return ImageViewer.this.doOnFling(motionEvent, motionEvent2, velocityX, velocityY);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.2 */
    class C09422 implements OnDoubleTapListener {
        C09422() {
        }

        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            return ImageViewer.this.doOnClick();
        }

        public boolean onDoubleTapEvent(MotionEvent motionEvent) {
            return ImageViewer.DRAW_RECT_BACKGROUND;
        }

        public boolean onDoubleTap(MotionEvent motionEvent) {
            return ImageViewer.this.doOnDoubleTap(motionEvent);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.3 */
    class C09433 implements Runnable {
        private final /* synthetic */ int[] val$indexes;
        private final /* synthetic */ Semaphore[] val$lock;
        private final /* synthetic */ boolean val$sync;

        C09433(int[] iArr, Semaphore[] semaphoreArr, boolean z) {
            this.val$indexes = iArr;
            this.val$lock = semaphoreArr;
            this.val$sync = z;
        }

        public void run() {
            List<Integer> list = new ArrayList();
            for (int i : this.val$indexes) {
                if (ImageViewer.this.adapter.isPositionAvailable(i) && !list.contains(Integer.valueOf(i))) {
                    list.add(Integer.valueOf(i));
                }
            }
            if (list.size() != 0) {
                for (Integer intValue : list) {
                    int index = intValue.intValue();
                    ImageEntry entry = ImageViewer.this.adapter.getItemAt(index);
                    if (!entry.getIsImageLoaded()) {
                        LocalImageLoader.instance().getImage(entry, ImageViewer.DRAW_RECT_BACKGROUND, ImageViewer.DRAW_RECT_BACKGROUND, ImageViewer.this.getLoadCallback(index, this.val$lock[0]), this.val$sync, -1, 1);
                    }
                    if (this.val$lock[0] != null) {
                        this.val$lock[0] = null;
                    }
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.5 */
    class C09455 implements Runnable {
        private final /* synthetic */ boolean val$forward;

        C09455(boolean z) {
            this.val$forward = z;
        }

        public void run() {
            ImageViewer.this.postedPhotoSwitch = null;
            ImageViewer imageViewer = ImageViewer.this;
            ImageViewer.this.translateY = 0.0f;
            imageViewer.translateX = 0.0f;
            ImageViewer.this.switchPhoto(this.val$forward);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.6 */
    class C09466 extends FrameLayout {
        C09466(Context $anonymous0) {
            super($anonymous0);
        }

        protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
            canvas.save();
            if (ImageViewer.this.inAnimationDone && ImageViewer.this.outAnimationDone) {
                canvas.translate(ImageViewer.this.translateX, ImageViewer.this.translateY);
                if (System.currentTimeMillis() - ImageViewer.this.animationStartTime < ImageViewer.this.animationDuration) {
                    float ai = ImageViewer.this.interpolator.getInterpolation(((float) (System.currentTimeMillis() - ImageViewer.this.animationStartTime)) / ((float) ImageViewer.this.animationDuration));
                    canvas.translate((ImageViewer.this.animationTranslateX - ImageViewer.this.translateX) * ai, (ImageViewer.this.animationTranslateY - ImageViewer.this.translateY) * ai);
                    invalidate();
                }
            }
            boolean result = super.drawChild(canvas, child, drawingTime);
            canvas.restore();
            return result;
        }

        public void draw(Canvas c) {
            super.draw(c);
        }

        public void onLayout(boolean changed, int l, int t, int r, int b) {
            super.onLayout(changed, l, t, r, b);
            Drawable d = ImageViewer.this.thumbnailView.getDrawable();
            if (d != null) {
                int w = r - l;
                int h = (b - t) - ImageViewer.this.getViewerOffset();
                float scale;
                if (((float) w) / ((float) h) < ((float) d.getIntrinsicWidth()) / ((float) d.getIntrinsicHeight())) {
                    scale = ((float) w) / ((float) d.getIntrinsicWidth());
                    ImageViewer.this.thumbnailView.layout(0, Math.round(((float) (h / ImageViewer.DEFAULT_MAXIMUM_SCALE)) - ((((float) d.getIntrinsicHeight()) * scale) / ImageViewHolder.PaddingSize)), w, Math.round(((float) (h / ImageViewer.DEFAULT_MAXIMUM_SCALE)) + ((((float) d.getIntrinsicHeight()) * scale) / ImageViewHolder.PaddingSize)));
                    return;
                }
                scale = ((float) h) / ((float) d.getIntrinsicHeight());
                ImageViewer.this.thumbnailView.layout(Math.round(((float) (w / ImageViewer.DEFAULT_MAXIMUM_SCALE)) - ((((float) d.getIntrinsicWidth()) * scale) / ImageViewHolder.PaddingSize)), 0, Math.round(((float) (w / ImageViewer.DEFAULT_MAXIMUM_SCALE)) + ((((float) d.getIntrinsicWidth()) * scale) / ImageViewHolder.PaddingSize)), h);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.7 */
    class C09477 implements Runnable {
        private final /* synthetic */ int val$pseudoViewportSize;
        private final /* synthetic */ float val$scale;

        C09477(int i, float f) {
            this.val$pseudoViewportSize = i;
            this.val$scale = f;
        }

        public void run() {
            ImageViewer.this.pseudoViewportSize = this.val$pseudoViewportSize;
            ImageViewer imageViewer = ImageViewer.this;
            ImageViewer imageViewer2 = ImageViewer.this;
            float max = Math.max(this.val$scale, ImageViewer.this.pseudoScale);
            imageViewer2.currentScale = max;
            imageViewer.updateMinMax(max);
            ImageViewer.this.minimumScale = this.val$scale;
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.8 */
    class C09488 implements Runnable {
        C09488() {
        }

        public void run() {
            ImageViewer imageViewer = ImageViewer.this;
            ImageViewer imageViewer2 = ImageViewer.this;
            float access$28 = ImageViewer.this.minimumScale;
            imageViewer2.currentScale = access$28;
            imageViewer.updateMinMax(access$28);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.9 */
    class C09499 implements Runnable {
        private final /* synthetic */ Runnable val$after;

        C09499(Runnable runnable) {
            this.val$after = runnable;
        }

        public void run() {
            ImageViewer.this.onPositionChanged(-1);
            if (this.val$after != null) {
                this.val$after.run();
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.4 */
    class C15474 extends ActionCallback<BitmapEntry> {
        private final /* synthetic */ int val$index;
        private final /* synthetic */ Semaphore val$lock;

        /* renamed from: com.vkontakte.android.mediapicker.ui.ImageViewer.4.1 */
        class C09441 implements Runnable {
            C09441() {
            }

            public void run() {
                if (ImageViewer.this.inAnimationDone) {
                    ImageViewer.this.thumbnailView.setVisibility(8);
                    ImageViewer.this.postInvalidate();
                    return;
                }
                ImageViewer.this.postDelayed(this, 20);
            }
        }

        C15474(int i, Semaphore semaphore) {
            this.val$index = i;
            this.val$lock = semaphore;
        }

        public void run(BitmapEntry result) {
            ImageViewer.this.adapter.getItemAt(this.val$index).setImageData(result);
            if (this.val$lock != null) {
                this.val$lock.release();
            }
            ImageViewer.this.updateScale(ImageViewer.this.currentIndex == this.val$index ? true : ImageViewer.DRAW_RECT_BACKGROUND);
            ImageViewer.this.updateMinMax(ImageViewer.this.currentScale);
            ImageViewer.this.post(new C09441());
        }
    }

    static {
        PAGE_SPACING = ActivityClassProvider.dp(10.0f);
        bg_paint_inited = DRAW_RECT_BACKGROUND;
        bitmap_paint_inited = DRAW_RECT_BACKGROUND;
        window_bg_inited = DRAW_RECT_BACKGROUND;
        density_inited = DRAW_RECT_BACKGROUND;
    }

    public ImageViewer(Context context) {
        super(context);
        this.secondIndex = -1;
        this.previousIndex = -1;
        this.nextIndex = -1;
        this.closeEnabled = true;
        this.lastBgAlpha = GroundOverlayOptions.NO_DIMENSION;
        this.screenData = new int[DEFAULT_MAXIMUM_SCALE];
        this.screenData2 = new int[DEFAULT_MAXIMUM_SCALE];
        this.screenData3 = new int[DEFAULT_MAXIMUM_SCALE];
        init();
    }

    private final void init() {
        if (!bg_paint_inited) {
            bg_paint = new Paint();
            bg_paint.setColor(-1);
            bg_paint_inited = true;
        }
        if (!bitmap_paint_inited) {
            bitmap_paint = new Paint();
            bitmap_paint.setFilterBitmap(true);
            bitmap_paint_inited = true;
        }
        if (!window_bg_inited) {
            window_bg = new ColorDrawable(Color.WINDOW_BACKGROUND_COLOR);
            window_bg_inited = true;
        }
        if (!density_inited) {
            density = getResources().getDisplayMetrics().density;
            density_inited = DRAW_RECT_BACKGROUND;
        }
        this.interpolator = new DecelerateInterpolator();
        this.scroller = new Scroller(getContext());
        this.inAnimationDone = true;
        this.outAnimationDone = true;
        this.secondIsNext = true;
        this.nextScale = 1.0f;
        this.minimumScale = 1.0f;
        this.currentScale = 1.0f;
        this.maximumScale = ImageViewHolder.PaddingSize;
        this.touchSlopX = (float) ViewConfiguration.get(getContext()).getScaledTouchSlop();
        this.touchSlopY = this.touchSlopX * 2.5f;
        this.thumbnailView = new ViewWithClipping(getContext());
        this.onGestureListener = new C09411();
        this.onDoubleTapListener = new C09422();
        this.gestureDetector = new GestureDetector(getContext(), this.onGestureListener);
        this.gestureDetector.setOnDoubleTapListener(this.onDoubleTapListener);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
    }

    public int getViewerOffset() {
        return ActivityClassProvider.dp(GalleryPickerFooterView.SIZE);
    }

    public int getViewerHeight() {
        return getHeight() - getViewerOffset();
    }

    public void setViewportHeight(int height) {
        this.viewerHeight = height;
    }

    public int getViewportWidth() {
        return this.viewerWidth;
    }

    public void setViewportWidth(int width) {
        this.viewerWidth = width;
    }

    public float getTranslateX() {
        return this.translateX;
    }

    public float getTranslateY() {
        return this.translateY;
    }

    public float getCurrentScale() {
        return this.currentScale;
    }

    public float getMinimumScale() {
        return this.minimumScale;
    }

    public void setThumb(Bitmap thumb) {
        getOverlayView();
        this.thumbnailView.setImageBitmap(thumb);
        this.thumbnailView.getParent().requestLayout();
    }

    public boolean dropTouches() {
        this.needDropTouches = true;
        return true;
    }

    private void onPrepareDismiss() {
        if (!(this.onDismissListener == null || this.dismissed || this.dismissing)) {
            this.onDismissListener.onPrepareDismiss();
            if (this.translateY > 0.0f) {
                this.dragStartY += this.touchSlopY;
                this.translateY -= this.touchSlopY;
            } else {
                this.dragStartY -= this.touchSlopY;
                this.translateY += this.touchSlopY;
            }
        }
        this.dismissing = true;
    }

    private void onBackgroundUpdate(float bg_alpha) {
        if (this.lastBgAlpha != bg_alpha) {
            this.lastBgAlpha = bg_alpha;
            if (this.onBackgroundUpdateListener != null && !animationNotEnded()) {
                this.onBackgroundUpdateListener.onUpdated(bg_alpha);
            }
        }
    }

    private boolean onDismiss() {
        if (!(this.dismissed || this.onDismissListener == null)) {
            this.onDismissListener.onDismiss();
        }
        this.dismissing = DRAW_RECT_BACKGROUND;
        this.dismissed = true;
        return true;
    }

    private boolean currentIsLoaded() {
        return (this.adapter.isPositionAvailable(this.currentIndex) && this.adapter.getItemAt(this.currentIndex).getIsImageLoaded()) ? true : DRAW_RECT_BACKGROUND;
    }

    private float getAnimationRatio() {
        return ((float) (System.currentTimeMillis() - this.animationStartTime)) / ((float) this.animationDuration);
    }

    private void doAnimate(float ratio) {
        this.translateX += (float) Math.round((this.animationTranslateX - this.translateX) * ratio);
        this.translateY += (float) Math.round((this.animationTranslateY - this.translateY) * ratio);
        this.currentScale += (this.animationScale - this.currentScale) * ratio;
        invalidate();
    }

    private void abortScrollerAnimation() {
        if (!this.scroller.isFinished()) {
            this.scroller.abortAnimation();
        }
    }

    private void updateDragInfo(MotionEvent ev) {
        this.dragStartX = ev.getX();
        this.dragStartY = ev.getY();
        this.dragStartTranslateX = this.translateX;
        this.dragStartTranslateY = this.translateY;
        this.dragStartPointerId = ev.getPointerId(0);
    }

    private void switchPhoto() {
        if (this.previousCallbackIndex == -1) {
        }
    }

    private void updatePinchInfo(MotionEvent ev) {
        this.pinchStartDist = (float) Math.hypot((double) (ev.getX(1) - ev.getX(0)), (double) (ev.getY(1) - ev.getY(0)));
        this.pinchStartScale = this.currentScale;
        this.pinchCenterX = (ev.getX(0) + ev.getX(1)) / ImageViewHolder.PaddingSize;
        this.pinchCenterY = (ev.getY(0) + ev.getY(1)) / ImageViewHolder.PaddingSize;
        this.scaleStartTx = this.translateX;
        this.scaleStartTy = this.translateY;
    }

    public void resetScale(boolean force) {
        if (this.currentScale < this.minimumScale || force) {
            animateTo(this.minimumScale, 0.0f, 0.0f);
            updateMinMax(this.minimumScale);
        } else if (this.currentScale > this.maximumScale) {
            updateMinMax(this.maximumScale);
            animateTo(this.maximumScale, clamp(this.translateX, (float) this.minimumX, (float) this.maximumX), clamp(this.translateY, (float) this.minimumY, (float) this.maximumY));
        }
    }

    public void setSwitchEnabled(boolean enabled) {
        this.switchDisabled = enabled ? DRAW_RECT_BACKGROUND : true;
    }

    public void setTouchEnabled(boolean enabled) {
        boolean z;
        if (enabled) {
            z = DRAW_RECT_BACKGROUND;
        } else {
            z = true;
        }
        this.touchDisabled = z;
        if (this.touchDisabled && this.currentScale != this.minimumScale) {
            resetScale(true);
        }
    }

    private boolean getCanZoom(int index) {
        return (!this.touchDisabled && this.adapter.isPositionAvailable(index) && this.adapter.getItemAt(index).getIsImageLoaded()) ? true : DRAW_RECT_BACKGROUND;
    }

    private void doZoom(MotionEvent ev) {
        this.currentScale = (((float) Math.hypot((double) (ev.getX(1) - ev.getX(0)), (double) (ev.getY(1) - ev.getY(0)))) / this.pinchStartDist) * this.pinchStartScale;
        this.translateX = (this.pinchCenterX - ((float) (getViewportWidth() / DEFAULT_MAXIMUM_SCALE))) - (((this.pinchCenterX - ((float) (getViewportWidth() / DEFAULT_MAXIMUM_SCALE))) - this.scaleStartTx) * (this.currentScale / this.pinchStartScale));
        this.translateY = (this.pinchCenterY - ((float) (getViewerHeight() / DEFAULT_MAXIMUM_SCALE))) - (((this.pinchCenterY - ((float) (getViewerHeight() / DEFAULT_MAXIMUM_SCALE))) - this.scaleStartTy) * (this.currentScale / this.pinchStartScale));
        if (this.onZoomListener != null) {
            this.onZoomListener.onZoomChanged(this.currentScale, this.maximumScale, this.minimumScale);
        }
        invalidate();
    }

    private boolean needDropPinch() {
        return (System.currentTimeMillis() < this.pinchEndTime + 300 || System.currentTimeMillis() - this.animationStartTime < this.animationDuration) ? true : DRAW_RECT_BACKGROUND;
    }

    private void updateHorizontalTranslate(float x) {
        if (this.maximumX > 0 || !this.dismissing) {
            this.translateX = (this.dragStartTranslateX + x) - this.dragStartX;
            updateTextures();
            return;
        }
        this.translateX = 0.0f;
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (this.preventInvalidate) {
            return true;
        }
        if (!this.touchDisabled && this.currentIndex != -1 && this.initialized && !this.needDropTouches && this.inAnimationDone && this.outAnimationDone && isEnabled()) {
            boolean multitouch;
            if (ev.getPointerCount() > 1) {
                multitouch = true;
            } else {
                multitouch = DRAW_RECT_BACKGROUND;
            }
            if (multitouch && !currentIsLoaded()) {
                return dropTouches();
            }
            if (!multitouch && this.gestureDetector.onTouchEvent(ev) && this.doubleTapped) {
                this.doubleTapped = DRAW_RECT_BACKGROUND;
                return true;
            }
            if (ev.getAction() == 1) {
                if (this.dismissing && Math.abs(this.translateY) > ((float) (getViewerHeight() / 4)) && !this.dismissed) {
                    return onDismiss();
                }
                if (this.dismissed) {
                    return true;
                }
                this.dragging = DRAW_RECT_BACKGROUND;
                this.dismissing = DRAW_RECT_BACKGROUND;
                updateMinMax(clamp(this.currentScale, this.minimumScale, this.maximumScale));
                if (animationNotEnded()) {
                    return true;
                }
                float halfedWidth = ((float) getWidth()) / ImageViewHolder.PaddingSize;
                if (this.translateX - ((float) this.maximumX) > halfedWidth && this.currentIndex > 0) {
                    return goToPrev(DRAW_RECT_BACKGROUND);
                }
                if ((-(this.translateX - ((float) this.minimumX))) > halfedWidth && this.currentIndex < this.adapter.getCount() - 1) {
                    return goToNext(DRAW_RECT_BACKGROUND);
                }
                if (this.translateX < ((float) this.minimumX) || this.translateX > ((float) this.maximumX) || this.translateY < ((float) this.minimumY) || this.translateY > ((float) this.maximumY)) {
                    animateTo(clamp(this.currentScale, this.minimumScale, this.maximumScale), clamp(this.translateX, (float) this.minimumX, (float) this.maximumX), clamp(this.translateY, (float) this.minimumY, (float) this.maximumY));
                }
            }
            if (ev.getAction() == 0) {
                abortScrollerAnimation();
                if (animationNotEnded()) {
                    float ratio = this.interpolator.getInterpolation(getAnimationRatio());
                    this.animationStartTime = 0;
                    doAnimate(ratio);
                }
                updateDragInfo(ev);
                if (this.previousCallbackIndex == -1 || this.previousCallbackIndex != this.currentIndex) {
                    switchPhoto();
                    cancelPostSwitch();
                }
            }
            if (ev.getAction() == DEFAULT_MAXIMUM_SCALE && !multitouch) {
                if (needDropPinch()) {
                    return true;
                }
                if (this.dragStartPointerId != ev.getPointerId(0)) {
                    updateDragInfo(ev);
                    return true;
                }
                updateHorizontalTranslate(ev.getX());
                if (this.currentScale == this.minimumScale && this.pseudoViewportSize == 0 && this.closeEnabled && (Math.abs(this.translateY) > this.touchSlopY || (Math.abs(this.translateX) < this.touchSlopX && !this.dragging))) {
                    this.translateX = 0.0f;
                }
                if (this.currentScale == this.minimumScale && this.pseudoViewportSize == 0 && this.closeEnabled && Math.abs(this.translateX) > this.touchSlopX && !this.dragging) {
                    this.dragging = true;
                    if (this.translateX > 0.0f) {
                        this.dragStartX += this.touchSlopX;
                        this.translateX -= this.touchSlopX;
                    } else {
                        this.dragStartX -= this.touchSlopX;
                        this.translateX += this.touchSlopX;
                    }
                }
                this.translateY = (this.dragStartTranslateY + ev.getY()) - this.dragStartY;
                if (this.currentScale != this.minimumScale || this.pseudoViewportSize != 0) {
                    if (this.translateY > ((float) this.maximumY)) {
                        this.translateY = ((float) this.maximumY) + ((this.translateY - ((float) this.maximumY)) / ImageViewHolder.PaddingSize);
                    }
                    if (this.translateY < ((float) this.minimumY)) {
                        this.translateY = ((float) this.minimumY) + ((this.translateY - ((float) this.minimumY)) / ImageViewHolder.PaddingSize);
                    }
                } else if (!this.closeEnabled || Math.abs(this.translateX) > this.touchSlopX || ((Math.abs(this.translateY) < this.touchSlopY && !this.dismissing) || this.dragging || !getIsCurrentImageLoaded())) {
                    this.translateY = 0.0f;
                } else if (Math.abs(this.translateY) > this.touchSlopY && !this.switchDisabled) {
                    onPrepareDismiss();
                }
                if (this.translateX - ((float) this.maximumX) > ((float) (getWidth() / DEFAULT_MAXIMUM_SCALE)) && this.currentIndex > 0) {
                    onPositionChanged(this.currentIndex - 1);
                } else if ((-(this.translateX - ((float) this.minimumX))) <= ((float) (getWidth() / DEFAULT_MAXIMUM_SCALE)) || this.currentIndex >= this.adapter.getCount() - 1) {
                    onPositionChanged(this.currentIndex);
                } else {
                    onPositionChanged(this.currentIndex + 1);
                }
                if (this.currentScale == this.minimumScale && this.touchDisabled && this.pseudoViewportSize == 0 && this.closeEnabled) {
                    if (this.translateX < 0.0f && (-(this.translateX - ((float) this.minimumX))) > ((float) (getViewportWidth() + PAGE_SPACING))) {
                        switchPhoto(true);
                        this.dragStartX -= (float) (getViewportWidth() + PAGE_SPACING);
                        this.translateX += (float) (getViewportWidth() + PAGE_SPACING);
                    } else if (this.translateX > 0.0f && this.translateX - ((float) this.maximumX) > ((float) (getViewportWidth() + PAGE_SPACING))) {
                        switchPhoto(DRAW_RECT_BACKGROUND);
                        this.dragStartX += (float) (getViewportWidth() + PAGE_SPACING);
                        this.translateX -= (float) (getViewportWidth() + PAGE_SPACING);
                    }
                }
                invalidate();
                if (this.thumbnailView.getVisibility() == 0 && this.overlayView != null) {
                    invalidateOverlayView();
                }
            }
            if ((this.dismissing || animationNotEnded()) && multitouch) {
                return true;
            }
            if ((ev.getAction() & MotionEventCompat.ACTION_MASK) == 5) {
                if ((-(this.translateX - ((float) this.minimumX))) > this.touchSlopX || this.translateX - ((float) this.maximumX) > this.touchSlopX) {
                    this.disallowZoom = true;
                    return true;
                }
                updatePinchInfo(ev);
            }
            if ((ev.getAction() & MotionEventCompat.ACTION_MASK) == 6) {
                if (this.disallowZoom) {
                    this.disallowZoom = DRAW_RECT_BACKGROUND;
                    return true;
                }
                this.dragStartPointerId = -1;
                resetScale(DRAW_RECT_BACKGROUND);
                this.pinchEndTime = System.currentTimeMillis();
            }
            if ((ev.getAction() & MotionEventCompat.ACTION_MASK) != DEFAULT_MAXIMUM_SCALE || ev.getPointerCount() != DEFAULT_MAXIMUM_SCALE || !getCanZoom(this.currentIndex) || this.disallowZoom || this.dismissing) {
                return true;
            }
            doZoom(ev);
            return true;
        }
        dropTouches();
        if (ev.getAction() != 1) {
            return true;
        }
        this.needDropTouches = DRAW_RECT_BACKGROUND;
        return true;
    }

    private void onPositionChanged(int position) {
        if (this.onNavigationListener != null) {
            this.onNavigationListener.changePosition(position, this.adapter.isPositionAvailable(position) ? this.adapter.getItemAt(position) : null);
        }
    }

    private boolean doOnFling(MotionEvent event, MotionEvent event2, float velocityX, float velocityY) {
        if (this.switchDisabled) {
            return true;
        }
        if (this.currentScale == this.minimumScale && this.closeEnabled && this.pseudoViewportSize == 0 && this.dismissing && Math.abs(velocityY) > ((float) ActivityClassProvider.dp(1000.0f))) {
            return onDismiss();
        }
        if ((this.translateX - ((float) this.maximumX) > 0.0f || (-(this.translateX - ((float) this.minimumX))) > 0.0f) && Math.abs(velocityX) > 650.0f * density && this.postedPhotoSwitch == null) {
            if (velocityX > 0.0f && this.currentIndex > 0 && !this.secondIsNext) {
                goToPrev(true);
            } else if (velocityX < 0.0f && this.currentIndex < this.adapter.getCount() - 1 && this.secondIsNext) {
                goToNext(true);
            }
            return true;
        }
        if (System.currentTimeMillis() > this.pinchEndTime + 300) {
            this.scroller.fling(Math.round(this.translateX), Math.round(this.translateY), Math.round(velocityX), Math.round(velocityY), this.minimumX, this.maximumX, this.minimumY, this.maximumY);
            postInvalidate();
        }
        return true;
    }

    private boolean doOnClick() {
        boolean doClick = this.onImageClickListener != null ? true : DRAW_RECT_BACKGROUND;
        if (doClick) {
            this.onImageClickListener.onClick(this);
        }
        return doClick;
    }

    private boolean doOnDoubleTap(MotionEvent event) {
        if ((this.currentScale == this.minimumScale && Math.abs(this.translateY) > 0.0f) || this.animationStartTime + this.animationDuration > System.currentTimeMillis() || this.translateX > ((float) this.maximumX) + this.touchSlopX || this.translateX < ((float) this.minimumX) - this.touchSlopX) {
            return DRAW_RECT_BACKGROUND;
        }
        if (this.adapter.isPositionAvailable(this.currentIndex) && !this.adapter.getItemAt(this.currentIndex).getIsLoaded()) {
            return DRAW_RECT_BACKGROUND;
        }
        this.doubleTapped = true;
        if (this.currentScale < this.maximumScale) {
            float cx = event.getX();
            float cy = event.getY();
            float ratio = this.maximumScale / this.currentScale;
            float x = (cx - ((float) (getViewportWidth() / DEFAULT_MAXIMUM_SCALE))) - (((cx - ((float) (getViewportWidth() / DEFAULT_MAXIMUM_SCALE))) - this.translateX) * ratio);
            float y = (cy - ((float) (getViewerHeight() / DEFAULT_MAXIMUM_SCALE))) - (((cy - ((float) (getViewerHeight() / DEFAULT_MAXIMUM_SCALE))) - this.translateY) * ratio);
            updateMinMax(this.maximumScale);
            animateTo(this.maximumScale, clamp(x, (float) this.minimumX, (float) this.maximumX), clamp(y, (float) this.minimumY, (float) this.maximumY));
        } else {
            animateTo(this.minimumScale, 0.0f, 0.0f);
        }
        return true;
    }

    private void onZoomChanged(float newScale, float newTranslateX, float newTranslateY) {
        if (this.onZoomListener != null) {
            this.onZoomListener.onZoomChanged(newScale, newTranslateX, newTranslateY);
        }
    }

    public void load(int[] indexes, boolean sync) {
        Semaphore[] lock = new Semaphore[1];
        Runnable runnable = new C09433(indexes, lock, sync);
        if (sync) {
            lock[0] = new Semaphore(0);
            GalleryPickerUtils.instance().invoke(runnable);
            try {
                Loggable.ThreadBlock("ImageViewer, near 798", new Object[0]);
                lock[0].acquire();
                return;
            } catch (Throwable throwable) {
                Loggable.Error("Error blocking highload thread", throwable, new Object[0]);
                return;
            }
        }
        LocalImageLoader.instance().invokeHighload(runnable);
    }

    private ActionCallback<BitmapEntry> getLoadCallback(int index, Semaphore lock) {
        return new C15474(index, lock);
    }

    private void initDragData(MotionEvent event) {
        this.dragStartX = event.getX();
        this.dragStartY = event.getY();
        this.dragStartTranslateX = this.translateX;
        this.dragStartTranslateY = this.translateY;
        this.dragStartPointerId = event.getPointerId(0);
    }

    private boolean animationNotEnded() {
        return System.currentTimeMillis() - this.animationStartTime < this.animationDuration ? true : DRAW_RECT_BACKGROUND;
    }

    private float clamp(float x, float min, float max) {
        return Math.min(Math.max(x, min), max);
    }

    private boolean goToNext(boolean byVelocity) {
        animateTo(this.currentScale, (float) ((this.minimumX - getViewportWidth()) - PAGE_SPACING), this.translateY);
        postSwitch(true);
        return true;
    }

    private boolean goToPrev(boolean byVelocity) {
        animateTo(this.currentScale, (float) ((this.maximumX + getViewportWidth()) + PAGE_SPACING), this.translateY);
        postSwitch(DRAW_RECT_BACKGROUND);
        return true;
    }

    private boolean cancelPostSwitch() {
        if (this.postedPhotoSwitch != null) {
            removeCallbacks(this.postedPhotoSwitch);
            this.postedPhotoSwitch = null;
        }
        return DRAW_RECT_BACKGROUND;
    }

    private void postSwitch(boolean forward) {
        Runnable c09455 = new C09455(forward);
        this.postedPhotoSwitch = c09455;
        postDelayed(c09455, this.animationDuration + 50);
    }

    public void clearImage(int index) {
        if (this.adapter.isPositionAvailable(index)) {
            this.adapter.getItemAt(index).clearImageData();
        }
    }

    public void clearImages() {
        clearImage(this.currentIndex);
        clearImage(this.currentIndex - 1);
        clearImage(this.currentIndex + 1);
    }

    private void switchPhoto(boolean forward) {
        int i;
        int i2 = -1;
        int i3 = this.currentIndex;
        if (forward) {
            i = -1;
        } else {
            i = 1;
        }
        int clearImageIndex = i3 + i;
        int currentIndex = forward ? this.nextIndex : this.previousIndex;
        if (currentIndex < 0) {
            currentIndex = 0;
        }
        if (currentIndex >= this.adapter.getCount()) {
            currentIndex = this.adapter.getCount() - 1;
        }
        this.previousCallbackIndex = this.currentIndex;
        this.currentIndex = currentIndex;
        this.nextIndex = currentIndex + 1;
        if (this.nextIndex < this.adapter.getCount()) {
            i = this.nextIndex;
        } else {
            i = -1;
        }
        this.nextIndex = i;
        this.previousIndex = currentIndex - 1;
        if (this.previousIndex >= 0) {
            i2 = this.previousIndex;
        }
        this.previousIndex = i2;
        onPositionChanged(currentIndex);
        if (this.adapter.isPositionAvailable(clearImageIndex)) {
            this.adapter.getItemAt(clearImageIndex).clearImageData();
        }
        updateScale(true);
        updateMinMax(this.minimumScale);
        int[] indexes = new int[3];
        indexes[0] = currentIndex;
        indexes[1] = forward ? this.nextIndex : this.previousIndex;
        indexes[DEFAULT_MAXIMUM_SCALE] = forward ? this.previousIndex : this.nextIndex;
        load(indexes, DRAW_RECT_BACKGROUND);
        updateThumbnail();
        invalidate();
        getOverlayView().requestLayout();
    }

    private void updateThumbnail() {
        if (this.adapter.getItemAt(this.currentIndex).getIsThumb()) {
            this.thumbnailView.setVisibility(0);
        } else {
            this.thumbnailView.setImageBitmap(null);
        }
    }

    private void updateTextures() {
        if ((this.translateX > ((float) this.maximumX) && this.currentIndex == 0) || ((this.translateX < ((float) this.minimumX) && this.currentIndex == this.adapter.getCount() - 1) || this.switchDisabled)) {
            if (this.translateX > ((float) this.maximumX)) {
                this.translateX = ((float) this.maximumX) + ((this.translateX - ((float) this.maximumX)) / ImageViewHolder.PaddingSize);
            }
            if (this.translateX < ((float) this.minimumX)) {
                this.translateX = ((float) this.minimumX) + ((this.translateX - ((float) this.minimumX)) / ImageViewHolder.PaddingSize);
            }
            if (this.secondIndex != -1) {
                this.secondIndex = -1;
                updateScale(DRAW_RECT_BACKGROUND);
            }
        } else if (this.translateX > ((float) this.maximumX) && this.currentIndex > 0 && this.secondIndex != this.previousIndex) {
            this.secondIndex = this.previousIndex;
            this.secondIsNext = DRAW_RECT_BACKGROUND;
            updateScale(DRAW_RECT_BACKGROUND);
        } else if (this.translateX < ((float) this.minimumX) && this.currentIndex < this.adapter.getCount() && this.secondIndex != this.previousIndex) {
            this.secondIndex = this.nextIndex;
            this.secondIsNext = true;
            updateScale(DRAW_RECT_BACKGROUND);
        }
    }

    private void updateScale(boolean changed) {
        updateScale(changed, DRAW_RECT_BACKGROUND);
    }

    private void updateScale(boolean changed, boolean computionOnly) {
        ImageEntry entry = getCurrentImage();
        if (entry != null) {
            float width;
            float height;
            if (((float) entry.getCropWidth()) / ((float) entry.getCropHeight()) > ((float) getViewportWidth()) / ((float) getViewerHeight()) || this.fitBySmallestSide) {
                width = (float) getViewportWidth();
                height = (float) getViewerHeight();
                if (this.fitBySmallestSide) {
                    width -= (float) (this.limitX * DEFAULT_MAXIMUM_SCALE);
                    height -= (float) (this.limitY * DEFAULT_MAXIMUM_SCALE);
                }
                if (this.pseudoViewportSize == 0) {
                    this.minimumScale = width / ((float) entry.getCropWidth());
                }
                this.maximumScale = height / ((float) entry.getCropHeight());
            } else {
                if (this.pseudoViewportSize == 0) {
                    this.minimumScale = ((float) getViewerHeight()) / ((float) entry.getCropHeight());
                }
                this.maximumScale = ((float) getViewportWidth()) / ((float) entry.getCropWidth());
            }
            if (this.maximumScale < ImageViewHolder.PaddingSize) {
                this.maximumScale = ImageViewHolder.PaddingSize;
            }
            if (!computionOnly) {
                if (this.currentScale < this.minimumScale || this.currentScale > this.maximumScale || changed) {
                    this.currentScale = this.minimumScale;
                    this.animationScale = this.minimumScale;
                }
                ImageEntry entry2 = this.secondIndex == -1 ? null : this.adapter.getItemAt(this.secondIndex);
                height = (float) entry.getCropHeight();
                float width2 = (float) (entry2 == null ? 1 : entry2.getCropWidth());
                float height2 = (float) (entry2 == null ? 1 : entry2.getCropHeight());
                if (((float) entry.getCropWidth()) == 0.0f || height == 0.0f) {
                    width = 1.0f;
                }
                if (width2 == 0.0f || height2 == 0.0f) {
                    height2 = 0.0f;
                    width2 = 0.0f;
                }
                if (entry2 != null && entry.getIsLoaded()) {
                    if (width2 / height2 > ((float) getViewportWidth()) / ((float) getViewerHeight())) {
                        this.nextScale = ((float) getViewportWidth()) / width2;
                    } else {
                        this.nextScale = ((float) getViewerHeight()) / height2;
                    }
                }
            }
        }
    }

    private void updateMinMax(float scale) {
        if (this.adapter.isPositionAvailable(this.currentIndex)) {
            ImageEntry entry = this.adapter.getItemAt(this.currentIndex);
            float viewportWidth = (float) (this.pseudoViewportSize > 0 ? this.pseudoViewportSize : getViewportWidth());
            float viewportHeight = (float) (this.pseudoViewportSize > 0 ? this.pseudoViewportSize : getViewerHeight());
            if (((float) entry.getCropWidth()) / ((float) entry.getCropHeight()) > ((float) getViewportWidth()) / ((float) getViewerHeight())) {
                this.maximumX = Math.round(((((float) entry.getCropWidth()) * scale) / ImageViewHolder.PaddingSize) - ((viewportWidth / ImageViewHolder.PaddingSize) - ((float) this.limitX)));
                this.minimumX = -this.maximumX;
                float scaledHeight = ((float) entry.getCropHeight()) * scale;
                if (scaledHeight > viewportHeight - ((float) this.limitY)) {
                    this.maximumY = Math.round((scaledHeight / ImageViewHolder.PaddingSize) - ((viewportHeight / ImageViewHolder.PaddingSize) - ((float) this.limitY)));
                    this.minimumY = -this.maximumY;
                    return;
                }
                this.minimumY = 0;
                this.maximumY = 0;
            } else if (scale > this.minimumScale || this.pseudoViewportSize > 0 || this.fitBySmallestSide) {
                this.maximumY = Math.round(((((float) entry.getCropHeight()) * scale) / ImageViewHolder.PaddingSize) - ((viewportHeight / ImageViewHolder.PaddingSize) - ((float) this.limitY)));
                this.minimumY = -this.maximumY;
                if (((float) entry.getCropWidth()) * scale > viewportWidth) {
                    this.maximumX = Math.round(((((float) entry.getCropWidth()) * scale) / ImageViewHolder.PaddingSize) - ((viewportWidth / ImageViewHolder.PaddingSize) - ((float) this.limitX)));
                    this.minimumX = -this.maximumX;
                    return;
                }
                this.minimumX = 0;
                this.maximumX = 0;
            } else {
                this.minimumY = 0;
                this.maximumY = 0;
                this.minimumX = 0;
                this.maximumX = 0;
            }
        }
    }

    public View getOverlayView() {
        if (this.overlayView != null) {
            return this.overlayView;
        }
        this.overlayView = new C09466(getContext());
        this.overlayView.setBackgroundColor(-16733696);
        this.overlayView.addView(this.thumbnailView);
        return this.overlayView;
    }

    public void setCloseEnabled(boolean enabled) {
        this.closeEnabled = enabled;
    }

    public void setThumbnailRect(Rect rect) {
        this.thumbnailRect = rect;
        invalidateOverlayView();
    }

    public void setPreventInvalidate(boolean prevent) {
        this.preventInvalidate = prevent;
        postInvalidate();
    }

    public void setCurrentIndex(int index) {
        this.currentIndex = index;
        this.nextIndex = this.currentIndex + 1;
        this.previousIndex = this.currentIndex - 1;
    }

    public void setMinimumScale(float scale) {
        this.minimumScale = scale;
    }

    public void setCurrentScale(float scale) {
        this.currentScale = scale;
        updateMinMax(scale);
    }

    public void setPseudoTranslate(float tx, float ty) {
        this.pseudoTranslateX = tx;
        this.pseudoTranslateY = ty;
        this.translateX = tx;
        this.translateY = ty;
    }

    public void setPseudoScale(float scale) {
        this.pseudoScale = scale;
    }

    public void setAdapter(IVAdapter adapter) {
        this.adapter = adapter;
    }

    public ImageEntry getCurrentImage() {
        return this.adapter.isPositionAvailable(this.currentIndex) ? this.adapter.getItemAt(this.currentIndex) : null;
    }

    public boolean getIsCurrentImageLoaded() {
        ImageEntry image = getCurrentImage();
        return (image == null || !image.getIsLoaded()) ? DRAW_RECT_BACKGROUND : true;
    }

    public void setOnNavigationListener(IVNavigationListener navigationListener) {
        this.onNavigationListener = navigationListener;
    }

    public void setOnDismissListener(IVDismissListener listener) {
        this.onDismissListener = listener;
    }

    public void setOnBackgroundUpdateListener(IVBackgroundListener listener) {
        this.onBackgroundUpdateListener = listener;
    }

    public void setOnZoomListener(IVZoomListener listener) {
        this.onZoomListener = listener;
    }

    public void setLimitOffsets(int x, int y) {
        this.limitX = x;
        this.limitY = y;
    }

    public int getViewportMaximumSize() {
        return Math.max(getViewportWidth(), getViewerHeight());
    }

    public int getViewportMinimumSize() {
        return Math.min(getViewportWidth(), getViewerHeight());
    }

    public void setMinimumScale(int pseudoViewportSize, float scale) {
        this.minimumScale = scale;
        animateTo(Math.max(this.minimumScale, this.pseudoScale), this.pseudoTranslateX, this.pseudoTranslateY);
        postDelayed(new C09477(pseudoViewportSize, scale), 250);
    }

    public void clearMinimumScale(boolean reset) {
        this.minimumScale = 1.0f;
        this.pseudoViewportSize = 0;
        this.pseudoTranslateY = 0.0f;
        this.pseudoTranslateX = 0.0f;
        this.pseudoScale = 0.0f;
        if (reset) {
            this.translateX = 0.0f;
            this.translateY = 0.0f;
        }
        updateScale(true, true);
        animateTo(this.minimumScale, 0.0f, 0.0f);
        postDelayed(new C09488(), 250);
    }

    public void setMinimumSize(int minX, int minY, int maxX, int maxY, float scale) {
        if (scale != this.currentScale) {
            this.minimumScale = scale;
            this.minimumX = minX;
            this.minimumY = minY;
            this.maximumX = maxX;
            this.maximumY = maxY;
            animateTo(this.minimumScale, 0.0f, 0.0f);
        }
    }

    public float getScaleForSize(int size) {
        return ((float) size) / ((float) getViewportMaximumSize());
    }

    public void setOnClickListener(OnClickListener listener) {
        this.onImageClickListener = listener;
    }

    public void invalidateOverlayView() {
        if (!this.preventInvalidate) {
            getOverlayView().invalidate();
        }
    }

    public void postInvalidate() {
        if (!this.preventInvalidate) {
            super.postInvalidate();
            getOverlayView().postInvalidate();
        }
    }

    public void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed) {
            setViewportWidth(r - l);
            setViewportHeight(b - t);
            updateScale(true);
            updateMinMax(this.currentScale);
            this.translateX = clamp(this.translateX, (float) this.minimumX, (float) this.maximumX);
            this.translateY = clamp(this.translateX, (float) this.minimumY, (float) this.maximumY);
        }
        this.initialized = true;
    }

    public boolean isAnimating() {
        return (this.inAnimationDone && this.outAnimationDone) ? DRAW_RECT_BACKGROUND : true;
    }

    private void animateTo(float newScale, float newTranslateX, float newTranslateY) {
        this.animationScale = newScale;
        this.animationTranslateX = newTranslateX;
        this.animationTranslateY = newTranslateY;
        this.animationStartTime = System.currentTimeMillis();
        this.animationDuration = 250;
        postInvalidate();
        onZoomChanged(newScale, this.maximumScale, this.minimumScale);
    }

    public void animateOut(Rect rect, int clipSize, Runnable after) {
        if (this.inAnimationDone) {
            onBackgroundUpdate(0.999f);
            if (rect == null) {
                GalleryPickerUtils.instance().fadeOut((FrameLayout) getParent(), new C09499(after), GalleryPickerUtils.getThumbnailAnimationDuration());
                return;
            }
            this.thumbnailView.getViewTreeObserver().addOnPreDrawListener(new AnonymousClass10(rect, clipSize, System.currentTimeMillis(), after));
            ImageEntry entry = this.adapter.getItemAt(this.currentIndex);
            if (entry.getImageData() != null) {
                this.thumbnailView.setImageBitmap((Bitmap) entry.getImageData().get());
            }
            this.thumbnailView.setVisibility(0);
            this.thumbnailView.clearAnimation();
            requestLayout();
            this.overlayView.requestLayout();
        }
    }

    public void animateIn(Rect rect, int clipSize, boolean setOnly) {
        if (rect != null) {
            this.thumbnailRect = new Rect(rect);
            Rect rect2 = this.thumbnailRect;
            rect2.top += clipSize;
        }
        this.inAnimationDone = DRAW_RECT_BACKGROUND;
        getOverlayView().setBackgroundDrawable(window_bg);
        Runnable ender = new Runnable() {
            public void run() {
                ImageViewer.this.overlayView.setBackgroundDrawable(null);
                ImageViewer.this.thumbnailView.setImageBitmap(null);
                ImageViewer.this.inAnimationDone = true;
                ImageViewer.this.onBackgroundUpdate(1.0f);
                ImageViewer.this.invalidate();
            }
        };
        if (setOnly) {
            ender.run();
        } else {
            this.thumbnailView.getViewTreeObserver().addOnPreDrawListener(new AnonymousClass12(rect, clipSize, ender));
        }
    }

    public void updateThumbnailRect(Rect rect) {
        this.thumbnailRect = rect;
        this.overlayView.invalidate();
    }

    public void onDraw(Canvas c) {
        if (this.inAnimationDone && this.outAnimationDone) {
            int x;
            int y;
            c.save();
            c.translate((float) (getWidth() / DEFAULT_MAXIMUM_SCALE), (float) (getViewerHeight() / DEFAULT_MAXIMUM_SCALE));
            float aty = this.translateY;
            if (animationNotEnded()) {
                if (!this.scroller.isFinished()) {
                    this.scroller.abortAnimation();
                }
                float ai = this.interpolator.getInterpolation(getAnimationRatio());
                float ts = this.currentScale + ((this.animationScale - this.currentScale) * ai);
                float tx = (this.translateX + ((this.animationTranslateX - this.translateX) * ai)) / ts;
                float ty = (this.translateY + ((this.animationTranslateY - this.translateY) * ai)) / ts;
                aty = ty;
                if (tx - ((float) this.maximumX) > ((float) (getWidth() / DEFAULT_MAXIMUM_SCALE)) && this.currentIndex > 0) {
                    onPositionChanged(this.currentIndex - 1);
                } else if ((-(tx - ((float) this.minimumX))) > ((float) (getWidth() / DEFAULT_MAXIMUM_SCALE)) && this.currentIndex < this.adapter.getCount() - 1) {
                    onPositionChanged(this.currentIndex + 1);
                }
                c.scale(ts, ts);
                c.translate(tx, ty);
                invalidate();
            } else {
                if (this.animationStartTime != 0) {
                    this.translateX = this.animationTranslateX;
                    this.translateY = this.animationTranslateY;
                    this.currentScale = this.animationScale;
                    this.bg_alpha = 1.0f;
                    this.animationStartTime = 0;
                    updateMinMax(this.currentScale);
                }
                if (!this.scroller.isFinished() && this.scroller.computeScrollOffset()) {
                    if (this.scroller.getStartX() < this.maximumX && this.scroller.getStartX() > this.minimumX) {
                        this.translateX = (float) this.scroller.getCurrX();
                    }
                    if (this.scroller.getStartY() < this.maximumX && this.scroller.getStartY() > this.minimumX) {
                        this.translateY = (float) this.scroller.getCurrY();
                    }
                    invalidate();
                }
                c.translate(this.translateX, this.translateY);
                c.scale(this.currentScale, this.currentScale);
                aty = this.translateY;
                if (this.overlayView != null) {
                    this.overlayView.invalidate();
                }
            }
            int ba = MotionEventCompat.ACTION_MASK;
            if (this.currentScale == this.minimumScale && !this.doubleTapped && this.pseudoViewportSize == 0 && this.closeEnabled) {
                if (Float.isNaN(aty)) {
                    aty = 0.0f;
                }
                this.bg_alpha = 1.0f - (Math.abs(aty) / ((float) getViewerHeight()));
                ba = Math.round(this.bg_alpha * 255.0f);
                onBackgroundUpdate(this.bg_alpha);
            }
            c.drawColor(ba << 24);
            ImageEntry item = getCurrentImage();
            if (item != null && item.getIsLoaded()) {
                x = (-item.getCropWidth()) / DEFAULT_MAXIMUM_SCALE;
                y = (-item.getCropHeight()) / DEFAULT_MAXIMUM_SCALE;
                c.drawBitmap((Bitmap) item.getImageData().get(), (float) x, (float) y, bitmap_paint);
                if (!this.switchDisabled && item.getIsStyled() && item.getStyle().getIsTexted()) {
                    TextPainter.drawLobster(c, 0, (-y) + 1, item.getCropWidth(), item.getCropHeight(), x, -x, item.getStyle());
                }
            }
            ImageEntry item2 = this.adapter.isPositionAvailable(this.secondIndex) ? this.adapter.getItemAt(this.secondIndex) : null;
            if (!this.switchDisabled && this.currentScale >= this.minimumScale && item2 != null && item2.getIsLoaded()) {
                c.translate(0.0f, (-this.translateY) / this.currentScale);
                float k = (float) (this.secondIsNext ? 1 : -1);
                c.translate(((((float) (getViewportWidth() + PAGE_SPACING)) * k) / this.minimumScale) / ImageViewHolder.PaddingSize, 0.0f);
                c.scale(this.nextScale / this.currentScale, this.nextScale / this.currentScale);
                c.translate(((((float) (getViewportWidth() + PAGE_SPACING)) * k) / this.nextScale) / ImageViewHolder.PaddingSize, 0.0f);
                x = (-item2.getCropWidth()) / DEFAULT_MAXIMUM_SCALE;
                y = (-item2.getCropHeight()) / DEFAULT_MAXIMUM_SCALE;
                c.drawBitmap((Bitmap) item2.getImageData().get(), (float) x, (float) y, bitmap_paint);
                if (!this.switchDisabled && item2.getIsStyled() && item2.getStyle().getIsTexted()) {
                    TextPainter.drawLobster(c, 0, -y, item2.getCropWidth(), item2.getCropHeight(), x, -x, item2.getStyle());
                }
            }
            c.restore();
        }
    }
}
