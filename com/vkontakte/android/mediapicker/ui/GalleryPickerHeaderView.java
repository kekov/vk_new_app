package com.vkontakte.android.mediapicker.ui;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.LangProvider;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;

public class GalleryPickerHeaderView extends RelativeLayout {
    public static final int SIZE = 48;
    private LinearLayout actionsWrap;
    private ImageView back;
    private LinearLayout backWrap;
    private Callback callback;
    private ImageView logo;
    private OnClickListener onBackClickListener;
    private TextView title;

    /* renamed from: com.vkontakte.android.mediapicker.ui.GalleryPickerHeaderView.1 */
    class C09341 implements OnClickListener {
        C09341() {
        }

        public void onClick(View view) {
            if (GalleryPickerHeaderView.this.callback != null) {
                GalleryPickerHeaderView.this.callback.onBackPressed();
            }
        }
    }

    public interface Callback {
        void onBackPressed();
    }

    public GalleryPickerHeaderView(Context context, Callback callback) {
        this(context);
        setCallback(callback);
    }

    public GalleryPickerHeaderView(Context context) {
        super(context);
        this.onBackClickListener = new C09341();
        setLayoutParams(new LayoutParams(-1, ActivityClassProvider.dp(GalleryPickerFooterView.SIZE)));
        setBackgroundResource(C0436R.drawable.pe_header);
        this.logo = new ImageView(context);
        this.logo.setImageResource(C0436R.drawable.pe_header_icon);
        this.logo.setScaleType(ScaleType.CENTER);
        this.logo.setLayoutParams(new RelativeLayout.LayoutParams(ActivityClassProvider.dp(28.0f), ActivityClassProvider.dp(28.0f)));
        this.back = new ImageView(context);
        this.back.setImageResource(C0436R.drawable.pe_back);
        this.back.setScaleType(ScaleType.CENTER);
        this.back.setLayoutParams(new RelativeLayout.LayoutParams(ActivityClassProvider.dp(16.0f), -1));
        this.back.setVisibility(4);
        this.title = new TextView(context);
        this.title.setText(LangProvider.getLocalizedString(17));
        this.title.setTextColor(-1);
        this.title.setPadding(ActivityClassProvider.dp(7.0f), ActivityClassProvider.dp(ImageViewHolder.PaddingSize), 0, 0);
        this.title.setTypeface(ActivityClassProvider.getDefaultTypeface());
        this.title.setTextSize(0, (float) ActivityClassProvider.dp(16.0f));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-2, -1);
        params.addRule(9);
        this.backWrap = new LinearLayout(context);
        this.backWrap.setLayoutParams(params);
        this.backWrap.setPadding(0, 0, ActivityClassProvider.dp(7.0f), 0);
        this.backWrap.setOrientation(0);
        this.backWrap.setGravity(16);
        this.backWrap.setOnClickListener(this.onBackClickListener);
        this.backWrap.setEnabled(false);
        GalleryPickerUtils.instance().setSelector(this.backWrap);
        this.backWrap.addView(this.back);
        this.backWrap.addView(this.logo);
        this.backWrap.addView(this.title);
        params = new RelativeLayout.LayoutParams(-2, -1);
        params.addRule(11);
        this.actionsWrap = new LinearLayout(context);
        this.actionsWrap.setLayoutParams(params);
        addView(this.backWrap);
        addView(this.actionsWrap);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }

    public void addActionButton(int id, int resource, OnClickListener onClickListener) {
        int padding = ActivityClassProvider.dp(14.0f);
        ImageButton button = new ImageButton(ActivityClassProvider.getStaticLocalContext());
        button.setId(id);
        button.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        button.setImageResource(resource);
        button.setScaleType(ScaleType.CENTER);
        button.setOnClickListener(onClickListener);
        button.setPadding(padding, 0, padding, 0);
        GalleryPickerUtils.instance().setSelector(button);
        this.actionsWrap.addView(button, 0);
    }

    public void hideActionButton(int id) {
        View view = this.actionsWrap.findViewById(id);
        if (view != null) {
            view.setVisibility(8);
        }
    }

    public void showActionButton(int id) {
        View view = this.actionsWrap.findViewById(id);
        if (view != null) {
            view.setVisibility(0);
        }
    }

    public void setBackVisible(boolean visible) {
        this.back.setVisibility(visible ? 0 : 4);
        this.backWrap.setEnabled(visible);
    }

    public void setTitle(String text) {
        this.title.setText(text);
    }

    public void setTitle(int stringId) {
        this.title.setText(LangProvider.getLocalizedString(stringId));
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }
}
