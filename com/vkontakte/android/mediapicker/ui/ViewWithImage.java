package com.vkontakte.android.mediapicker.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public class ViewWithImage extends View {
    private static Paint paint;
    private Bitmap bitmap;
    private float ratio;

    static {
        paint = new Paint(4);
    }

    public ViewWithImage(Context context) {
        super(context);
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
        if (bitmap == null) {
            this.ratio = 0.0f;
        } else {
            float size = (float) getWidth();
            this.ratio = Math.min(size / ((float) bitmap.getWidth()), size / ((float) bitmap.getHeight()));
        }
        postInvalidate();
    }

    public void onDraw(Canvas c) {
        if (this.bitmap != null && !this.bitmap.isRecycled()) {
            c.scale(this.ratio, this.ratio);
            c.drawBitmap(this.bitmap, 0.0f, 0.0f, paint);
        }
    }
}
