package com.vkontakte.android.mediapicker.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.vkontakte.android.mediapicker.utils.Loggable;

public class ViewWithClipping extends View {
    private static Paint paint;
    private static boolean paint_inited;
    private Bitmap bmp;
    private int clipBottom;
    private int clipLeft;
    private int clipRight;
    private int clipTop;

    static {
        paint_inited = false;
    }

    public ViewWithClipping(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!paint_inited) {
            paint = new Paint();
            paint.setFilterBitmap(true);
            paint_inited = true;
        }
    }

    public int getClipTop() {
        return this.clipTop;
    }

    public void setClipTop(int clipTop) {
        this.clipTop = clipTop;
        invalidate();
    }

    public int getClipLeft() {
        return this.clipLeft;
    }

    public void setClipLeft(int clipLeft) {
        this.clipLeft = clipLeft;
        invalidate();
    }

    public int getClipBottom() {
        return this.clipBottom;
    }

    public void setClipBottom(int clipBottom) {
        this.clipBottom = clipBottom;
        invalidate();
    }

    public int getClipRight() {
        return this.clipRight;
    }

    public void setClipRight(int clipRight) {
        this.clipRight = clipRight;
        invalidate();
    }

    public void setClipVertical(int c) {
        this.clipBottom = c;
        this.clipTop = c;
        invalidate();
    }

    public int getClipVertical() {
        return this.clipTop;
    }

    public void setClipHorizontal(int c) {
        this.clipRight = c;
        this.clipLeft = c;
        invalidate();
    }

    public int getClipHorizontal() {
        return this.clipRight;
    }

    public void setImageBitmap(Bitmap b) {
        this.bmp = b;
        invalidate();
    }

    public Drawable getDrawable() {
        return new BitmapDrawable(this.bmp);
    }

    public void onDraw(Canvas c) {
        Rect rect = new Rect(this.clipLeft, this.clipTop, getWidth() - this.clipRight, getHeight() - this.clipBottom);
        c.save();
        c.clipRect(rect);
        if (this.bmp != null) {
            if (this.bmp.isRecycled()) {
                Loggable.Error("U OR NO PASSING RECYCLED THUMB?", new Object[0]);
            } else {
                c.drawBitmap(this.bmp, null, new Rect(0, 0, getWidth(), getHeight()), paint);
            }
        }
        c.restore();
    }
}
