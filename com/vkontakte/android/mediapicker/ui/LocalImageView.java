package com.vkontakte.android.mediapicker.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.view.GestureDetector;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.vkontakte.android.mediapicker.entries.ActionCallback;
import com.vkontakte.android.mediapicker.entries.ImageDataEntry;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.entries.ImageGestureListener;
import com.vkontakte.android.mediapicker.entries.ImageTouchListener;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.LocalImageLoader;
import com.vkontakte.android.mediapicker.utils.Loggable;
import com.vkontakte.android.mediapicker.utils.SelectionContext;

public class LocalImageView extends FrameLayout {
    private static int def_image_kind;
    private static GestureDetector imageGestureDetector;
    private static boolean image_kind_inited;
    private static ImageGestureListener onGestureListener;
    private static ImageTouchListener onImageTouchListener;
    private ActionCallback<Object> callback;
    private ImageView image;
    private int imageId;
    private int image_kind;
    private boolean isTemp;
    private OnItemClickListener onItemClickListener;
    private View overlay;
    private int size;

    /* renamed from: com.vkontakte.android.mediapicker.ui.LocalImageView.2 */
    class C09512 extends ImageView {
        private boolean makeRequest;
        private Runnable releaseRequest;

        /* renamed from: com.vkontakte.android.mediapicker.ui.LocalImageView.2.1 */
        class C09501 implements Runnable {
            C09501() {
            }

            public void run() {
                C09512.this.makeRequest = true;
            }
        }

        C09512(Context $anonymous0) {
            super($anonymous0);
            this.makeRequest = true;
            this.releaseRequest = new C09501();
        }

        public void setImageBitmap(Bitmap bitmap) {
            this.makeRequest = false;
            super.setImageBitmap(bitmap);
            postDelayed(this.releaseRequest, 25);
        }

        public void requestLayout() {
            if (this.makeRequest) {
                super.requestLayout();
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.LocalImageView.3 */
    class C09523 implements Runnable {
        C09523() {
        }

        public void run() {
            LocalImageView.this.getOverlayView().setVisibility(8);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.ui.LocalImageView.1 */
    class C15481 extends ActionCallback<Object> {
        C15481() {
        }

        public void run(Object data) {
            if (data instanceof Bitmap) {
                LocalImageView.this.image.setImageBitmap((Bitmap) data);
                return;
            }
            try {
                ImageDataEntry result = (ImageDataEntry) data;
                if (result != null && LocalImageView.this.imageId == result.getImageId()) {
                    LocalImageView.this.display(result);
                }
            } catch (Throwable t) {
                Loggable.Error("Error in the LocalImageView callback", t, new Object[0]);
            }
        }
    }

    static {
        image_kind_inited = false;
        onGestureListener = new ImageGestureListener();
        imageGestureDetector = new GestureDetector(ActivityClassProvider.getStaticLocalContext(), onGestureListener);
        imageGestureDetector.setIsLongpressEnabled(false);
        onImageTouchListener = new ImageTouchListener();
        onImageTouchListener.setImageGestureListener(imageGestureDetector, onGestureListener);
        onGestureListener.bindTouchListener(onImageTouchListener);
    }

    public static int getDefaultImageKind() {
        if (!image_kind_inited) {
            def_image_kind = 3;
            image_kind_inited = true;
        }
        return def_image_kind;
    }

    public LocalImageView(Context context, int minSize) {
        this(context, minSize, null);
    }

    public LocalImageView(Context context, int minSize, OnItemClickListener onItemClickListener) {
        super(context);
        this.imageId = -1;
        this.isTemp = false;
        this.callback = new C15481();
        this.size = minSize;
        if (minSize > 96) {
            this.image_kind = getDefaultImageKind();
        } else {
            this.image_kind = 3;
        }
        this.onItemClickListener = onItemClickListener;
        this.image = new C09512(context);
        this.image.setScaleType(ScaleType.CENTER_CROP);
        if (this.onItemClickListener != null) {
            onImageTouchListener.setGestureDetector(SelectionContext.getSingleMode() ? null : imageGestureDetector);
            setOnTouchListener(onImageTouchListener.toOnTouchListener());
            this.overlay = new View(context);
            this.overlay.setBackgroundColor(1140850688);
            this.overlay.setVisibility(8);
        }
        addView(this.image, new LayoutParams(-1, -1));
        if (this.onItemClickListener != null) {
            addView(this.overlay, new LayoutParams(-1, -1));
        }
        setBackgroundColor(Color.IMAGE_LOAD_BACKGROUND);
    }

    public void displayFilterPreview(int filter_id) {
        if (filter_id != this.imageId) {
            this.image.setImageBitmap(null);
            this.imageId = filter_id;
            LocalImageLoader.instance().getPreviewForFilter(getContext(), this.imageId, this.size, this.callback);
        }
    }

    private boolean compare(ImageEntry image) {
        return image != null && this.imageId == image.getImageId() && this.isTemp == image.getIsTemp();
    }

    private void set(ImageEntry image) {
        boolean z = false;
        this.imageId = image == null ? 0 : image.getImageId();
        if (image != null && image.getIsTemp()) {
            z = true;
        }
        this.isTemp = z;
    }

    public void display(ImageEntry image, boolean fromGrid, boolean force) {
        if (!compare(image) || force) {
            this.image.setImageBitmap(null);
            set(image);
            if (image != null) {
                LocalImageLoader.instance().getThumbnailForImage(image, fromGrid, this.callback);
            }
        }
    }

    private void display(ImageDataEntry data) {
        if (this.image != null) {
            if (VERSION.SDK_INT < 12 || data.getLoadedIn() < 30) {
                this.image.setImageBitmap(data.getBitmap());
            } else {
                displayWithFade(data);
            }
        }
    }

    @TargetApi(12)
    private void displayWithFade(ImageDataEntry data) {
        this.image.setAlpha(0.0f);
        this.image.setImageBitmap(data.getBitmap());
        try {
            this.image.animate().alpha(1.0f).setDuration((long) (data.getLoadedIn() < 50 ? 97 : 120));
        } catch (Throwable th) {
            Loggable.Error("Cannot animate view, setting alpha", new Object[0]);
            this.image.setAlpha(1.0f);
        }
    }

    public View getOverlayView() {
        return this.overlay;
    }

    public void displayOverlayView() {
        getOverlayView().setVisibility(0);
        if (VERSION.SDK_INT >= 14) {
            GalleryPickerUtils.instance().fadeIn(getOverlayView(), null, 40);
        }
    }

    public void hideOverlayView(boolean animated) {
        Runnable hider = new C09523();
        if (VERSION.SDK_INT < 14 || !animated) {
            hider.run();
        } else {
            GalleryPickerUtils.instance().fadeOut(getOverlayView(), hider, 90);
        }
    }

    public OnItemClickListener getOnItemClickListener() {
        return this.onItemClickListener;
    }
}
