package com.vkontakte.android.mediapicker.entries;

public interface AlbumsListCallback {
    void onAlbumChosen(int i, AlbumEntry albumEntry);

    void onCameraAlbumFound(int i);

    void onVKCameraAlbumFound(int i);
}
