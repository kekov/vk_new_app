package com.vkontakte.android.mediapicker.entries;

import android.graphics.Bitmap;

public class BitmapEntry extends Entry<Bitmap> {
    private boolean failed;

    public BitmapEntry(Bitmap bitmap) {
        set(bitmap);
    }

    public void setFailed() {
        this.failed = true;
    }

    public boolean getFailed() {
        return this.failed;
    }
}
