package com.vkontakte.android.mediapicker.entries;

import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import com.vkontakte.android.mediapicker.gallery.ImagesGridFragment;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.Loggable;
import com.vkontakte.android.mediapicker.utils.OnClickTouchListener;
import com.vkontakte.android.mediapicker.utils.SelectionContext;

public class ImageGestureListener implements OnGestureListener {
    OnClickTouchListener bindedTouchListener;
    int cellSize;
    int checkBound;
    int[] checkCoords;
    int[] coords;
    int index;
    int item;
    int lastIndex;
    boolean mode;
    int numColumns;
    CancellableRunnable postedLongPress;

    /* renamed from: com.vkontakte.android.mediapicker.entries.ImageGestureListener.1 */
    class C15301 extends CancellableRunnable {
        C15301() {
        }

        public void run() {
            ImageGestureListener.this.onLongPress(null);
        }
    }

    public ImageGestureListener() {
        this.mode = false;
        this.coords = new int[2];
        this.checkCoords = new int[2];
        this.checkBound = -1;
        this.index = -1;
        this.item = -1;
        this.lastIndex = -1;
    }

    public boolean onDown(MotionEvent event) {
        ImagesGridFragment fragment;
        ImageViewHolder holder;
        try {
            fragment = GalleryPickerUtils.sharedInstance.getGalleryContext().currentAlbumFragment;
            holder = SelectionContext.getImageHolderFromView((View) this.bindedTouchListener.getCurrentView().getParent());
        } catch (Throwable throwable) {
            Loggable.Error("Error in onDown", throwable, new Object[0]);
            holder = null;
            fragment = null;
        }
        if (holder == null || fragment == null) {
            return false;
        }
        holder.wrap.getLocationInWindow(this.coords);
        this.mode = SelectionContext.getIsCheckedFromView(holder.image);
        this.numColumns = fragment.getNumColumns();
        this.cellSize = fragment.getColumnSize();
        this.index = holder.getPosition();
        this.lastIndex = this.index;
        this.item = this.index % this.numColumns;
        this.checkBound = this.cellSize + ActivityClassProvider.dp(ImageViewHolder.PaddingSize);
        this.checkCoords[0] = holder.check.getLeft();
        this.checkCoords[1] = holder.check.getBottom();
        postLongPress();
        return true;
    }

    public boolean onSingleTapUp(MotionEvent event) {
        onCanceled();
        int x = (int) Math.floor((double) event.getX());
        int y = (int) Math.floor((double) event.getY());
        if (x >= this.checkBound || x < this.checkCoords[0] || y <= 0 || y > this.checkCoords[1]) {
            return false;
        }
        dropBindedTouch();
        select(this.index);
        return true;
    }

    public boolean onScroll(MotionEvent downEvent, MotionEvent event, float v, float v2) {
        cancelLongPress();
        int col = (int) Math.floor((double) (((float) ((int) Math.floor((double) event.getX()))) / ((float) this.cellSize)));
        int result = (this.index + (this.numColumns * ((int) Math.floor((double) (((float) ((int) Math.floor((double) event.getY()))) / ((float) this.cellSize)))))) + col;
        if (!(result == this.index || result == this.lastIndex)) {
            int citem = this.item + col;
            if (citem >= 0 && citem < this.numColumns) {
                this.lastIndex = result;
                if (SelectionContext.enterSelectionMode(this.mode)) {
                    select(this.index);
                    SelectionContext.performSelection(result, Boolean.valueOf(this.mode));
                } else {
                    select(result);
                }
            }
        }
        if (!SelectionContext.getIsInSelectionMode()) {
            SelectionContext.setScrollEnabled(Math.abs(v2) > Math.abs(v) / ImageViewHolder.PaddingSize);
        }
        return SelectionContext.getIsInSelectionMode();
    }

    private void select(int index) {
        SelectionContext.playSoundEffect(this.bindedTouchListener.getCurrentView(), SelectionContext.performSelection(index, Boolean.valueOf(this.mode)), false);
    }

    void postLongPress() {
        cancelLongPress();
        if (getCurrentView() != null) {
            this.postedLongPress = new C15301();
            getCurrentView().postDelayed(this.postedLongPress.toOnceRunnable(), 300);
        }
    }

    void cancelLongPress() {
        if (this.postedLongPress != null) {
            this.postedLongPress.cancel();
            this.postedLongPress = null;
        }
    }

    public void onLongPress(MotionEvent event) {
        onCanceled();
        if (SelectionContext.enterSelectionMode(this.mode)) {
            dropBindedTouch();
            SelectionContext.playSoundEffect(getCurrentView(), SelectionContext.performSelection(getCurrentWrapper(), Boolean.valueOf(SelectionContext.getSelectionMode())), true);
        }
    }

    public void onCanceled() {
        cancelLongPress();
        SelectionContext.setScrollEnabled(true);
    }

    private View getCurrentView() {
        return this.bindedTouchListener == null ? null : this.bindedTouchListener.getCurrentView();
    }

    private View getCurrentWrapper() {
        return getCurrentView() == null ? null : (View) getCurrentView().getParent();
    }

    private void dropBindedTouch() {
        if (this.bindedTouchListener != null) {
            this.bindedTouchListener.dropTouch();
        }
    }

    public ImageGestureListener bindTouchListener(OnClickTouchListener listener) {
        this.bindedTouchListener = listener;
        return this;
    }

    public boolean onFling(MotionEvent event, MotionEvent event2, float v, float v2) {
        return false;
    }

    public void onShowPress(MotionEvent event) {
    }
}
