package com.vkontakte.android.mediapicker.entries;

import android.view.View;

public abstract class AlbumsLoadCallback {
    public View bindedView;

    public abstract void prepareListAnimation();

    public AlbumsLoadCallback(View view) {
        this.bindedView = view;
    }

    public void post(Runnable runnable, int delay) {
        if (this.bindedView != null) {
            this.bindedView.postDelayed(runnable, (long) delay);
        }
    }
}
