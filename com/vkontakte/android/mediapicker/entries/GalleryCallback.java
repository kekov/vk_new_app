package com.vkontakte.android.mediapicker.entries;

import android.view.MotionEvent;
import android.view.View;

public interface GalleryCallback {
    void onItemOvered(View view, int i, View view2);

    void onSelectModeClosed(View view);

    void onSelectModeStarted(View view);

    boolean selectSingleImage(View view, MotionEvent motionEvent);
}
