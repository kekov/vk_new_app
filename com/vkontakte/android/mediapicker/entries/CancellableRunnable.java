package com.vkontakte.android.mediapicker.entries;

public abstract class CancellableRunnable {
    private boolean isCanceled;
    private Runnable runnable;

    /* renamed from: com.vkontakte.android.mediapicker.entries.CancellableRunnable.1 */
    class C08841 implements Runnable {
        C08841() {
        }

        public void run() {
            if (!CancellableRunnable.this.isCanceled) {
                CancellableRunnable.this.isCanceled = true;
                CancellableRunnable.this.run();
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.entries.CancellableRunnable.2 */
    class C08852 implements Runnable {
        C08852() {
        }

        public void run() {
            if (!CancellableRunnable.this.isCanceled) {
                CancellableRunnable.this.run();
            }
        }
    }

    public abstract void run();

    public CancellableRunnable() {
        this.isCanceled = false;
    }

    public void cancel() {
        this.isCanceled = true;
    }

    public boolean getIsCanceled() {
        return this.isCanceled;
    }

    public Runnable toOnceRunnable() {
        if (this.runnable == null) {
            this.runnable = new C08841();
        }
        return this.runnable;
    }

    public Runnable toRunnable() {
        if (this.runnable == null) {
            this.runnable = new C08852();
        }
        return this.runnable;
    }
}
