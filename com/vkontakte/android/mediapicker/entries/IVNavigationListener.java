package com.vkontakte.android.mediapicker.entries;

public abstract class IVNavigationListener {
    private int lastCallbackPosition;

    public abstract void onPositionChanged(int i, ImageEntry imageEntry);

    public IVNavigationListener() {
        this.lastCallbackPosition = -1;
    }

    public void changePosition(int position, ImageEntry image) {
        if (position != this.lastCallbackPosition) {
            this.lastCallbackPosition = position;
            onPositionChanged(position, image);
        }
    }
}
