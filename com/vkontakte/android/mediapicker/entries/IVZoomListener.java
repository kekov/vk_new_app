package com.vkontakte.android.mediapicker.entries;

public interface IVZoomListener {
    void onZoomChanged(float f, float f2, float f3);
}
