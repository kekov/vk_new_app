package com.vkontakte.android.mediapicker.entries;

import android.graphics.Bitmap;

public class ImageDataEntry {
    private Bitmap bitmap;
    private int imageId;
    private int ms;
    private byte temp;

    public ImageDataEntry(int imageId) {
        this.imageId = imageId;
    }

    public void setIsTemp(boolean isTemp) {
        this.temp = isTemp ? (byte) 1 : (byte) 0;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getImageId() {
        return this.imageId;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return this.bitmap;
    }

    public void setLoadedIn(int ms) {
        this.ms = ms;
    }

    public int getLoadedIn() {
        return this.ms;
    }
}
