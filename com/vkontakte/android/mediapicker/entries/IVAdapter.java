package com.vkontakte.android.mediapicker.entries;

public abstract class IVAdapter {
    public abstract int getCount();

    public abstract ImageEntry getItemAt(int i);

    public abstract boolean isPositionAvailable(int i);
}
