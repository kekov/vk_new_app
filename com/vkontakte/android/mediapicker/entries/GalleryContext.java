package com.vkontakte.android.mediapicker.entries;

import com.vkontakte.android.mediapicker.gallery.AlbumsListFragment;
import com.vkontakte.android.mediapicker.gallery.ImageViewerFragment;
import com.vkontakte.android.mediapicker.gallery.ImagesGridFragment;

public class GalleryContext {
    public AlbumsListFragment albumsFragment;
    public ImagesGridFragment currentAlbumFragment;
    public int currentAlbumIndex;
    public boolean currentCameraBucketFound;
    public int currentCameraBucketId;
    public ImageEntry currentImage;
    public int currentImageIndex;
    public boolean currentVKCameraBucketFound;
    public int currentVKCameraBucketId;
    public ImageViewerFragment currentViewerFragment;

    public boolean hasAlbumsFragment() {
        return this.albumsFragment != null;
    }
}
