package com.vkontakte.android.mediapicker.entries;

import com.vkontakte.android.mediapicker.ui.LocalImageView;
import java.util.List;

public interface ImagesGridCallback {
    void onImageOpened(int i, ImageEntry imageEntry, List<ImageEntry> list, LocalImageView localImageView);
}
