package com.vkontakte.android.mediapicker.entries;

import android.view.GestureDetector;
import android.view.View;
import android.widget.AdapterView;
import com.vkontakte.android.mediapicker.ui.LocalImageView;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.mediapicker.utils.OnClickTouchListener;
import com.vkontakte.android.mediapicker.utils.SelectionContext;

public class ImageTouchListener extends OnClickTouchListener {
    private ImageGestureListener gestureListener;

    public ImageTouchListener() {
        setUseDelayBeforeDown(true);
    }

    public void setImageGestureListener(GestureDetector detector, ImageGestureListener listener) {
        setGestureDetector(detector);
        this.gestureListener = listener;
    }

    protected void onCanceled(View view) {
        super.onCanceled(view);
        this.gestureListener.onCanceled();
    }

    public void onTapStarted(View view) {
        if (SelectionContext.getSingleMode()) {
            LocalImageView wrap = (LocalImageView) view;
            if (wrap.getOnItemClickListener() != null) {
                wrap.displayOverlayView();
            }
        }
    }

    public void onTapCanceled(View view, boolean beforeCompleted) {
        this.gestureListener.cancelLongPress();
        if (SelectionContext.getSingleMode()) {
            LocalImageView wrap = (LocalImageView) view;
            if (wrap.getOnItemClickListener() != null && !beforeCompleted) {
                wrap.hideOverlayView(true);
            }
        }
    }

    public void onTapCompleted(View view) {
        LocalImageView wrap = (LocalImageView) view;
        if (wrap.getOnItemClickListener() != null) {
            ImageViewHolder holder = (ImageViewHolder) ((View) wrap.getParent()).getTag();
            int index = holder.getPosition();
            if (!SelectionContext.getSingleMode()) {
                wrap.displayOverlayView();
            }
            wrap.playSoundEffect(0);
            wrap.getOnItemClickListener().onItemClick(holder == null ? null : (AdapterView) holder.wrap.getParent(), wrap, index, (long) wrap.getId());
        }
    }
}
