package com.vkontakte.android.mediapicker.entries;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.vkontakte.android.mediapicker.providers.GalleryPickerProvider;
import com.vkontakte.android.mediapicker.providers.LruCacheProvider;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.Loggable;
import com.vkontakte.android.mediapicker.utils.StrictCache;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import org.acra.ACRAConstants;

public class ImageEntry implements Serializable, Parcelable {
    public static final Creator<ImageEntry> CREATOR;
    private static int LAST_TEMP_IMAGE_ID;
    private static Comparator<ImageEntry> comparator;
    private Integer bucketId;
    private String cacheKey;
    private String cacheKeySmall;
    private int cropHeight;
    private int cropWidth;
    private BitmapEntry data;
    private int dateTaken;
    private Integer imageId;
    private byte isChecked;
    private byte isTemp;
    private int orientation;
    private String path;
    private StyleEntry style;

    /* renamed from: com.vkontakte.android.mediapicker.entries.ImageEntry.1 */
    class C08861 implements Creator<ImageEntry> {
        C08861() {
        }

        public ImageEntry createFromParcel(Parcel in) {
            return new ImageEntry(in);
        }

        public ImageEntry[] newArray(int size) {
            return new ImageEntry[size];
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.entries.ImageEntry.2 */
    class C08872 implements Comparator<ImageEntry> {
        C08872() {
        }

        public int compare(ImageEntry a, ImageEntry b) {
            return a.getDateTaken().compareTo(b.getDateTaken());
        }
    }

    static {
        LAST_TEMP_IMAGE_ID = 0;
        CREATOR = new C08861();
    }

    public ImageEntry(String rawPath) {
        int i = LAST_TEMP_IMAGE_ID;
        LAST_TEMP_IMAGE_ID = i - 1;
        this.imageId = Integer.valueOf(i);
        this.dateTaken = getCurrentDateTaken() + Math.abs(LAST_TEMP_IMAGE_ID);
        if (rawPath.startsWith(GalleryPickerProvider.UNSTYLED_URI_SCHEME)) {
            this.path = rawPath;
        } else {
            initWithValues(parseImagePath(rawPath));
        }
    }

    public ImageEntry(Integer imageId) {
        this.imageId = imageId;
    }

    public ImageEntry(ImageEntry image) {
        this.imageId = image.imageId;
        this.bucketId = image.bucketId;
        this.dateTaken = image.dateTaken;
        this.orientation = image.orientation;
        this.path = image.path;
        this.style = image.style;
        this.data = image.data;
        this.cropWidth = image.cropWidth;
        this.cropHeight = image.cropHeight;
    }

    public ImageEntry(int bucketId, String path, boolean isTemp, int orientation) {
        int i = LAST_TEMP_IMAGE_ID;
        LAST_TEMP_IMAGE_ID = i - 1;
        this(bucketId, i, getCurrentDateTaken() + Math.abs(LAST_TEMP_IMAGE_ID), path, orientation);
        setIsTemp(isTemp);
    }

    public ImageEntry(int bucketId, int imageId, int dateTaken, String path, int orientation) {
        this.bucketId = Integer.valueOf(bucketId);
        this.imageId = Integer.valueOf(imageId);
        this.dateTaken = dateTaken;
        if (!(path == null || path.startsWith(GalleryPickerProvider.UNSTYLED_URI_SCHEME))) {
            path = new StringBuilder(GalleryPickerProvider.UNSTYLED_URI_SCHEME).append(path).toString();
        }
        this.path = path;
        this.orientation = orientation;
    }

    public ImageEntry(int imageId, String path, int orientation) {
        this.imageId = Integer.valueOf(imageId);
        this.dateTaken = getCurrentDateTaken();
        if (!(path == null || path.startsWith(GalleryPickerProvider.UNSTYLED_URI_SCHEME))) {
            path = new StringBuilder(GalleryPickerProvider.UNSTYLED_URI_SCHEME).append(path).toString();
        }
        this.path = path;
        this.orientation = orientation;
    }

    public ImageEntry(HashMap<String, String> values) {
        initWithValues(values);
    }

    private void initWithValues(HashMap<String, String> values) {
        StyleEntry style = new StyleEntry();
        if (values.containsKey("filter")) {
            style.setFiltered(Integer.parseInt((String) values.get("filter")));
        }
        if (values.containsKey("text")) {
            style.setText((String) values.get("text"));
        }
        if (values.containsKey("crop")) {
            String[] vals = ((String) values.get("crop")).split(",");
            float[] coords = new float[vals.length];
            for (int i = 0; i < vals.length; i++) {
                coords[i] = Float.parseFloat(vals[i]);
            }
            style.setCropped(coords);
        }
        if (values.containsKey("enhanced")) {
            style.setEnhanced(true);
        }
        int imageId = Integer.parseInt((String) values.get("image_id"));
        int orientation = Integer.parseInt((String) values.get("orientation"));
        String path = (String) values.get("path");
        this.imageId = Integer.valueOf(imageId);
        this.dateTaken = getCurrentDateTaken();
        if (!(path == null || path.startsWith(GalleryPickerProvider.UNSTYLED_URI_SCHEME))) {
            path = new StringBuilder(GalleryPickerProvider.UNSTYLED_URI_SCHEME).append(path).toString();
        }
        this.path = path;
        this.orientation = orientation;
        setStyle(style);
    }

    private static int getCurrentDateTaken() {
        return (int) (System.currentTimeMillis() / 1000);
    }

    public String getCacheKey(boolean useSmallThumbnail) {
        if (this.cacheKey == null) {
            this.cacheKey = LruCacheProvider.instance().getImageCacheKey(getIsStyled(), getIsTemp() ? this.path : this.imageId);
        }
        if (!useSmallThumbnail || getUseAlternateThumb()) {
            return this.cacheKey;
        }
        if (this.cacheKeySmall == null) {
            this.cacheKeySmall = this.cacheKey + LruCacheProvider.instance().getSmallImageCacheKeySuffix();
        }
        return this.cacheKeySmall;
    }

    public boolean hasBucket() {
        return this.bucketId != null;
    }

    public void setBucketId(int bucket_id) {
        this.bucketId = Integer.valueOf(bucket_id);
    }

    public int getBucketId() {
        return this.bucketId.intValue();
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public int getOrientation() {
        return this.orientation;
    }

    public int getImageId() {
        return this.imageId.intValue();
    }

    public Integer getDateTaken() {
        return Integer.valueOf(this.dateTaken);
    }

    public boolean getIsChecked() {
        return this.isChecked == (byte) 1;
    }

    public void setIsChecked(boolean checked) {
        this.isChecked = checked ? (byte) 1 : (byte) 0;
    }

    public void toggleChecked() {
        setIsChecked(!getIsChecked());
    }

    public void setIsTemp(boolean isTemp) {
        this.isTemp = isTemp ? (byte) 1 : (byte) 0;
    }

    public boolean getIsTemp() {
        return this.isTemp == (byte) 1;
    }

    public String getPath() {
        return this.path;
    }

    public static HashMap<String, String> parseImagePath(String path) {
        String realPath;
        Loggable.Log("Parsing image path %s", path);
        String[] res = path.split("\\?");
        String params = res[res.length - 1];
        if (res.length == 2) {
            realPath = res[0];
        } else {
            List<String> list = new ArrayList();
            for (int i = 0; i < res.length - 1; i++) {
                list.add(res[i]);
            }
            realPath = TextUtils.join("?", list);
        }
        HashMap<String, String> values = new HashMap();
        for (String param : params.split("&")) {
            String[] v = param.split("=");
            values.put(v[0], Uri.decode(v[1]));
        }
        values.put("path", new StringBuilder(GalleryPickerProvider.UNSTYLED_URI_SCHEME).append(realPath.substring(GalleryPickerProvider.STYLED_IMAGE_URI_START.length(), realPath.length())).toString());
        for (String key : values.keySet()) {
            Loggable.Error("Parsed %s -> %s", key, values.get(key));
        }
        return values;
    }

    public String getResultPath() {
        if (!getIsStyled()) {
            return Uri.fromFile(new File(GalleryPickerUtils.getFilePath(this.path))).toString();
        }
        String res = this.path;
        if (res.startsWith(GalleryPickerProvider.UNSTYLED_URI_SCHEME)) {
            res = res.substring(GalleryPickerProvider.UNSTYLED_URI_SCHEME.length(), res.length());
        }
        List<String> options = new ArrayList();
        if (getStyle().getIsFiltered()) {
            options.add("filter=" + getStyle().getFilterId());
        }
        if (getStyle().getIsTexted()) {
            options.add("text=" + Uri.encode(getStyle().getText()));
        }
        if (getStyle().getIsCropped()) {
            String cropString = ACRAConstants.DEFAULT_STRING_VALUE;
            for (float t : getStyle().getCropData()) {
                cropString = new StringBuilder(String.valueOf(cropString)).append(cropString.length() > 0 ? "," : ACRAConstants.DEFAULT_STRING_VALUE).append(t).toString();
            }
            options.add("crop=" + Uri.encode(cropString));
        }
        if (getStyle().getIsEnhanced()) {
            options.add("enhanced=1");
        }
        options.add("image_id=" + this.imageId);
        options.add("orientation=" + this.orientation);
        if (getIsTemp()) {
            options.add("temp=1");
        }
        return new StringBuilder(GalleryPickerProvider.STYLED_IMAGE_URI_START).append(res).append("?").append(TextUtils.join("&", options)).toString();
    }

    public String getCropKeys() {
        if (!getIsStyled()) {
            return "0";
        }
        float[] crop = getStyle().getCropData();
        String keys = ACRAConstants.DEFAULT_STRING_VALUE;
        if (crop == null) {
            return "0";
        }
        for (float key : crop) {
            keys = new StringBuilder(String.valueOf(keys)).append(keys.length() > 0 ? "," : ACRAConstants.DEFAULT_STRING_VALUE).append(key).toString();
        }
        return keys;
    }

    public void checkStyleTopicality() {
        if (this.style != null) {
            if (!getUseAlternateThumb()) {
                StrictCache.instance().remove(getCacheKey(false));
            }
            if (!this.style.getIsStyled()) {
                this.style = null;
            }
        }
    }

    public boolean getIsStyled() {
        return this.style != null;
    }

    public boolean getUseAlternateThumb() {
        return this.style != null && this.style.getAppliedStylesCount() > 0;
    }

    public StyleEntry getStyle() {
        return getStyle(false);
    }

    public void setStyle(StyleEntry style) {
        this.style = style;
    }

    public StyleEntry getStyle(boolean force) {
        if (force && this.style == null) {
            this.style = new StyleEntry();
        }
        return this.style;
    }

    public int getFilterId() {
        return getIsStyled() ? getStyle().getFilterId() : 0;
    }

    public int hashCode() {
        return this.imageId.hashCode();
    }

    public String toString() {
        return getResultPath();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel p, int flags) {
        p.writeInt(this.bucketId.intValue());
        p.writeInt(this.imageId.intValue());
        p.writeInt(this.dateTaken);
        p.writeByte((byte) (getIsChecked() ? 1 : 2));
        p.writeInt(this.cropWidth);
        p.writeInt(this.cropHeight);
        if (this.style == null) {
            p.writeByte((byte) 0);
            return;
        }
        p.writeByte((byte) 1);
        this.style.writeToParcel(p);
    }

    public ImageEntry(Parcel p) {
        this.bucketId = Integer.valueOf(p.readInt());
        this.imageId = Integer.valueOf(p.readInt());
        this.dateTaken = p.readInt();
        this.isChecked = p.readByte();
        this.cropWidth = p.readInt();
        this.cropHeight = p.readInt();
        if (p.readByte() == 1) {
            this.style = StyleEntry.createFromParcel(p);
        }
    }

    public static Comparator<ImageEntry> getComparator() {
        if (comparator == null) {
            comparator = new C08872();
        }
        return comparator;
    }

    public boolean getIsLoaded() {
        return getIsImageLoaded();
    }

    public boolean getIsThumb() {
        return !getIsImageLoaded();
    }

    public void setImageData(Bitmap bitmap) {
        if (this.data != null) {
            this.data.set(bitmap);
        } else {
            this.data = new BitmapEntry(bitmap);
        }
        setImageData(this.data);
    }

    public void setImageData(BitmapEntry data) {
        if (!(data == null || data.get() == null)) {
            this.cropWidth = ((Bitmap) data.get()).getWidth();
            this.cropHeight = ((Bitmap) data.get()).getHeight();
        }
        if (!(data != null || this.data == null || this.data.get() == null)) {
            ((Bitmap) this.data.get()).recycle();
        }
        this.data = data;
    }

    public void clearImageData() {
        setImageData(null);
    }

    public boolean getIsImageLoaded() {
        return (this.data == null || this.data.get() == null || ((Bitmap) this.data.get()).isRecycled()) ? false : true;
    }

    public boolean getIsImageFailed() {
        return (this.data == null || this.data.get() == null || !this.data.getFailed()) ? false : true;
    }

    public BitmapEntry getImageData() {
        return this.data;
    }

    public void setCropWidth(int cropWidth) {
        this.cropWidth = cropWidth;
    }

    public int getCropWidth() {
        return this.cropWidth;
    }

    public void setCropHeight(int cropHeight) {
        this.cropHeight = cropHeight;
    }

    public int getCropHeight() {
        return this.cropHeight;
    }
}
