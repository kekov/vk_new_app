package com.vkontakte.android.mediapicker.entries;

public class Entry<T> {
    private T value;

    public Entry(T value) {
        this.value = value;
    }

    public T get() {
        return this.value;
    }

    public void set(T value) {
        this.value = value;
    }
}
