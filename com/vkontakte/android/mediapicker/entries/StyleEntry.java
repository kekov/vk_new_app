package com.vkontakte.android.mediapicker.entries;

import android.os.Parcel;
import java.util.List;
import org.acra.ACRAConstants;

public class StyleEntry {
    private float[] cropData;
    private int filter_id;
    private byte isEnhanced;
    private int last_words_width;
    private List<String> lines;
    private byte stylesCount;
    private String text;

    public StyleEntry(StyleEntry entry) {
        this.filter_id = entry.filter_id;
        this.cropData = entry.cropData;
        this.text = entry.text;
        this.isEnhanced = entry.isEnhanced;
        this.stylesCount = getStyleCount();
    }

    public int getAppliedStylesCount() {
        return this.stylesCount;
    }

    public int getTextureStylesCount() {
        int i = 1;
        int i2 = getIsCropped() ? 1 : 0;
        if (!getIsEnhanced()) {
            i = 0;
        }
        return i2 + i;
    }

    public boolean getIsStyled() {
        return getIsFiltered() || getIsCropped() || getIsTexted() || getIsEnhanced();
    }

    private byte getStyleCount() {
        byte count = (byte) 0;
        if (getIsEnhanced()) {
            count = (byte) 1;
        }
        if (getIsCropped()) {
            count = (byte) (count + 1);
        }
        if (getIsFiltered()) {
            return (byte) (count + 1);
        }
        return count;
    }

    private void updateStylesCount(boolean increase) {
        this.stylesCount = (byte) ((increase ? 1 : -1) + this.stylesCount);
    }

    public boolean getIsEnhanced() {
        return this.isEnhanced == (byte) 1;
    }

    public void setEnhanced(boolean enhanced) {
        byte isEnhanced = enhanced ? (byte) 1 : (byte) 0;
        if (this.isEnhanced != isEnhanced) {
            this.isEnhanced = isEnhanced;
            updateStylesCount(getIsEnhanced());
        }
    }

    public boolean getIsProcessed() {
        return getIsFiltered() || getIsEnhanced();
    }

    public boolean getIsFiltered() {
        return this.filter_id > 0;
    }

    public int getFilterId() {
        return this.filter_id;
    }

    public void setFiltered(int filter_id) {
        if (this.filter_id != filter_id) {
            this.filter_id = filter_id;
            updateStylesCount(getIsFiltered());
        }
    }

    public String getTextureKey() {
        String cropKey;
        Object obj;
        if (getIsCropped()) {
            cropKey = ACRAConstants.DEFAULT_STRING_VALUE;
            for (float coord : this.cropData) {
                cropKey = new StringBuilder(String.valueOf(cropKey)).append(cropKey.length() > 0 ? "," : ACRAConstants.DEFAULT_STRING_VALUE).append(coord).toString();
            }
        } else {
            cropKey = "0";
        }
        if (getIsEnhanced()) {
            obj = "1";
        } else {
            obj = "0";
        }
        return new StringBuilder(String.valueOf(obj)).append(cropKey).toString();
    }

    public boolean getIsCropped() {
        return this.cropData != null;
    }

    public float[] getCropData() {
        return this.cropData;
    }

    public float[] removeCropData() {
        if (this.cropData == null) {
            return null;
        }
        float[] data = this.cropData;
        this.cropData = null;
        updateStylesCount(getIsCropped());
        return data;
    }

    public void setCropped(float[] data) {
        if (this.cropData != data) {
            this.cropData = data;
            updateStylesCount(getIsCropped());
        }
    }

    public boolean getIsTexted() {
        return this.text != null;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<java.lang.String> splitLinesByWidth(int r11, android.text.TextPaint r12) {
        /*
        r10 = this;
        r7 = 1;
        r8 = 0;
        r6 = r10.getIsTexted();
        if (r6 != 0) goto L_0x000a;
    L_0x0008:
        r6 = 0;
    L_0x0009:
        return r6;
    L_0x000a:
        r6 = r10.lines;
        if (r6 == 0) goto L_0x0015;
    L_0x000e:
        r6 = r10.last_words_width;
        if (r6 != r11) goto L_0x0015;
    L_0x0012:
        r6 = r10.lines;
        goto L_0x0009;
    L_0x0015:
        r10.last_words_width = r11;
        r6 = new java.util.ArrayList;
        r6.<init>();
        r10.lines = r6;
        if (r11 <= 0) goto L_0x002b;
    L_0x0020:
        r6 = r10.text;
        r6 = r12.measureText(r6);
        r9 = (float) r11;
        r6 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1));
        if (r6 > 0) goto L_0x0039;
    L_0x002b:
        r6 = r10.lines;
        r7 = r10.text;
        r7 = r7.trim();
        r6.add(r7);
        r6 = r10.lines;
        goto L_0x0009;
    L_0x0039:
        r6 = r10.text;
        r5 = r6.trim();
        r6 = new java.lang.StringBuilder;
        r9 = java.lang.String.valueOf(r5);
        r6.<init>(r9);
        r4 = r6.toString();
        r3 = 0;
        r1 = 0;
        r0 = 1;
        r2 = r1;
    L_0x0050:
        r1 = r2 + 1;
        r6 = 5000; // 0x1388 float:7.006E-42 double:2.4703E-320;
        if (r2 < r6) goto L_0x0059;
    L_0x0056:
        r6 = r10.lines;
        goto L_0x0009;
    L_0x0059:
        if (r0 == 0) goto L_0x0056;
    L_0x005b:
        r6 = r5.length();
        if (r3 >= r6) goto L_0x0056;
    L_0x0061:
        r6 = r12.measureText(r4);
        r9 = (float) r11;
        r6 = (r6 > r9 ? 1 : (r6 == r9 ? 0 : -1));
        if (r6 > 0) goto L_0x008f;
    L_0x006a:
        r6 = r10.lines;
        r9 = r4.trim();
        r6.add(r9);
        r6 = r4.length();
        r3 = r3 + r6;
        r6 = r5.length();
        if (r3 >= r6) goto L_0x008d;
    L_0x007e:
        r0 = r7;
    L_0x007f:
        if (r0 == 0) goto L_0x0089;
    L_0x0081:
        r6 = r5.length();
        r4 = r5.substring(r3, r6);
    L_0x0089:
        if (r0 == 0) goto L_0x0056;
    L_0x008b:
        r2 = r1;
        goto L_0x0050;
    L_0x008d:
        r0 = r8;
        goto L_0x007f;
    L_0x008f:
        r6 = " ";
        r6 = r4.contains(r6);
        if (r6 == 0) goto L_0x00c0;
    L_0x0097:
        r6 = " ";
        r6 = r4.startsWith(r6);
        if (r6 != 0) goto L_0x00c0;
    L_0x009f:
        r6 = " ";
        r6 = r4.endsWith(r6);
        if (r6 != 0) goto L_0x00c0;
    L_0x00a7:
        r6 = " ";
        r6 = r4.lastIndexOf(r6);
    L_0x00ad:
        r4 = r4.substring(r8, r6);
        r6 = " ";
        r6 = r4.startsWith(r6);
        if (r6 == 0) goto L_0x0089;
    L_0x00b9:
        r4 = r4.substring(r7);
        r3 = r3 + 1;
        goto L_0x0089;
    L_0x00c0:
        r6 = r4.length();
        r6 = r6 + -1;
        goto L_0x00ad;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.mediapicker.entries.StyleEntry.splitLinesByWidth(int, android.text.TextPaint):java.util.List<java.lang.String>");
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        if (this.text != text) {
            this.text = text;
            this.lines = null;
            this.last_words_width = -1;
        }
    }

    public void writeToParcel(Parcel p) {
        byte b;
        byte b2 = (byte) 1;
        p.writeByte(this.stylesCount);
        p.writeByte(this.isEnhanced);
        p.writeByte(getIsFiltered() ? (byte) 1 : (byte) 0);
        if (getIsFiltered()) {
            p.writeInt(getFilterId());
        }
        if (getIsCropped()) {
            b = (byte) 1;
        } else {
            b = (byte) 0;
        }
        p.writeByte(b);
        if (getIsCropped()) {
            p.writeInt(this.cropData.length);
            for (float i : this.cropData) {
                p.writeFloat(i);
            }
        }
        if (!getIsTexted()) {
            b2 = (byte) 0;
        }
        p.writeByte(b2);
        if (getIsTexted()) {
            p.writeString(this.text);
        }
    }

    public static StyleEntry createFromParcel(Parcel p) {
        StyleEntry result = new StyleEntry();
        result.stylesCount = p.readByte();
        result.isEnhanced = p.readByte();
        if (p.readByte() == (byte) 1) {
            result.filter_id = p.readInt();
        }
        if (p.readByte() == (byte) 1) {
            int length = p.readInt();
            result.cropData = new float[length];
            for (int i = 0; i < length; i++) {
                result.cropData[i] = p.readFloat();
            }
        }
        if (p.readByte() == (byte) 1) {
            result.text = p.readString();
        }
        return result;
    }
}
