package com.vkontakte.android.mediapicker.entries;

public interface IVDismissListener {
    void onDismiss();

    void onPrepareDismiss();
}
