package com.vkontakte.android.mediapicker.entries;

import android.view.View;

public abstract class IVCallback {
    public abstract void onImageStyled(ImageEntry imageEntry);

    public abstract void onPositionChanged(int i, ImageEntry imageEntry);

    public void onPrepareDismiss() {
    }

    public void onDismiss() {
    }

    public void onZoomChanged(float factor, float max, float min) {
    }

    public void onClick(View view) {
    }

    public void onBackgroundUpdate(float ratio) {
    }
}
