package com.vkontakte.android.mediapicker.entries;

import android.os.Handler;
import android.os.Looper;

public abstract class ActionCallback<T> {
    private static Handler uiHandler;
    private Handler handler;
    private boolean hasExecuted;

    /* renamed from: com.vkontakte.android.mediapicker.entries.ActionCallback.1 */
    class C08811 implements Runnable {
        private final /* synthetic */ Object val$result;

        C08811(Object obj) {
            this.val$result = obj;
        }

        public void run() {
            ActionCallback.this.run(this.val$result);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.entries.ActionCallback.2 */
    class C08822 implements Runnable {
        private final /* synthetic */ Object val$result;

        C08822(Object obj) {
            this.val$result = obj;
        }

        public void run() {
            ActionCallback.this.run(this.val$result);
        }
    }

    public abstract void run(T t);

    static {
        uiHandler = new Handler(Looper.getMainLooper());
    }

    public void exec(T result) {
        this.hasExecuted = true;
        run(result);
    }

    public void post(T result) {
        this.hasExecuted = true;
        uiHandler.postDelayed(new C08811(result), 1);
    }

    public void postBack(T result) {
        this.hasExecuted = true;
        if (this.handler != null) {
            this.handler.post(new C08822(result));
        } else {
            post(result);
        }
    }

    public boolean getHasExecuted() {
        return this.hasExecuted;
    }
}
