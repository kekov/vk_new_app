package com.vkontakte.android.mediapicker.entries;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.vkontakte.android.mediapicker.providers.LangProvider;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AlbumEntry implements Serializable, Parcelable {
    public static final Creator<AlbumEntry> CREATOR;
    private Integer bucket_id;
    private String bucket_name;
    private String counter;
    private boolean hasNext;
    private List<ImageEntry> images;
    private byte isCamera;
    private ImageEntry preview;
    private int preview_id;
    private int selected_count;
    private String selected_counter;

    /* renamed from: com.vkontakte.android.mediapicker.entries.AlbumEntry.1 */
    class C08831 implements Creator<AlbumEntry> {
        C08831() {
        }

        public AlbumEntry createFromParcel(Parcel in) {
            return new AlbumEntry(null);
        }

        public AlbumEntry[] newArray(int size) {
            return new AlbumEntry[size];
        }
    }

    public AlbumEntry(int bucket_id, String bucketName, int previewId) {
        this.bucket_id = Integer.valueOf(bucket_id);
        this.bucket_name = bucketName;
        this.preview_id = previewId;
        this.images = new ArrayList();
    }

    public void setIsCamera(boolean isCamera) {
        this.isCamera = isCamera ? (byte) 1 : (byte) 0;
    }

    public boolean getIsCamera() {
        return this.isCamera == (byte) 1;
    }

    public void setBucketId(int bucket_id) {
        this.bucket_id = Integer.valueOf(bucket_id);
    }

    public int getBucketId() {
        return this.bucket_id.intValue();
    }

    public void setPreviewId(int preview_id) {
        this.preview_id = preview_id;
    }

    public int getPreviewId() {
        return this.preview_id;
    }

    public ImageEntry getPreview() {
        return this.preview;
    }

    private void updateThumbnailForAlbum(ImageEntry image, boolean force) {
        if (this.images.size() == 0 || force) {
            this.preview_id = image.getImageId();
            this.preview = image;
        }
    }

    public void addImageToBeginning(ImageEntry image) {
        updateThumbnailForAlbum(image, true);
        this.images.add(0, image);
        updateCounter();
        if (image.getIsChecked()) {
            incrementSelectedCount(true);
        }
    }

    public void addImage(ImageEntry image) {
        updateThumbnailForAlbum(image, false);
        this.images.add(image);
        if (image.getIsChecked()) {
            incrementSelectedCount(true);
        }
    }

    public List<ImageEntry> getImages() {
        return this.images;
    }

    public int getCount() {
        return this.images.size();
    }

    public void incrementSelectedCount(boolean increment) {
        this.selected_count = (increment ? 1 : -1) + this.selected_count;
        this.selected_counter = new StringBuilder(String.valueOf(LangProvider.getLocalizedString(16).toLowerCase())).append(": ").append(this.selected_count).toString();
    }

    public int getSelectedCount() {
        return this.selected_count;
    }

    public String getSelectedCounter() {
        return this.selected_counter;
    }

    public void setBucketName(String bucket_name) {
        this.bucket_name = bucket_name;
    }

    public String getBucketName() {
        return this.bucket_name;
    }

    public void setCounter(String string) {
        this.counter = string;
    }

    public void updateCounter() {
        setCounter(LangProvider.getLocalizedPluralString(80, getCount()));
    }

    public String getCounter() {
        return this.counter;
    }

    public void setHasNext() {
        this.hasNext = true;
    }

    public boolean getHasNext() {
        return this.hasNext;
    }

    public int hashCode() {
        return this.bucket_id.hashCode();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel p, int flags) {
        p.writeInt(this.preview_id);
        p.writeString(this.bucket_name);
        p.writeInt(this.images.size());
        for (ImageEntry image : this.images) {
            image.writeToParcel(p, flags);
        }
    }

    static {
        CREATOR = new C08831();
    }

    private AlbumEntry(Parcel p) {
        this.preview_id = p.readInt();
        this.bucket_name = p.readString();
        int size = p.readInt();
        this.images = new ArrayList();
        for (int i = 0; i < size; i++) {
            this.images.add(new ImageEntry(p));
        }
    }
}
