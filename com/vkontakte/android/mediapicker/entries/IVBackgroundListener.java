package com.vkontakte.android.mediapicker.entries;

public interface IVBackgroundListener {
    void onUpdated(float f);
}
