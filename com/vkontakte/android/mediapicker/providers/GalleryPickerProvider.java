package com.vkontakte.android.mediapicker.providers;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import com.vkontakte.android.mediapicker.entries.ActionCallback;
import com.vkontakte.android.mediapicker.entries.BitmapEntry;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.entries.StyleEntry;
import com.vkontakte.android.mediapicker.gl.ImageProcessor;
import com.vkontakte.android.mediapicker.gl.TextPainter;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.LocalImageLoader;
import com.vkontakte.android.mediapicker.utils.Loggable;
import com.vkontakte.android.mediapicker.utils.StrictCache;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.concurrent.Semaphore;

public class GalleryPickerProvider {
    private static final int DEFAULT_BIG_IMAGE_SIZE = 1280;
    private static final int DEFAULT_SMALL_IMAGE_SIZE = 1024;
    public static final String STYLED_IMAGE_URI_SCHEME = "impick";
    public static final String STYLED_IMAGE_URI_START = "impick://";
    public static final String UNSTYLED_URI_SCHEME = "file://";
    private static final GalleryPickerProvider instance;

    /* renamed from: com.vkontakte.android.mediapicker.providers.GalleryPickerProvider.1 */
    class C09171 implements Runnable {
        private final /* synthetic */ int val$compression;
        private final /* synthetic */ Semaphore val$lock;
        private final /* synthetic */ String val$path;
        private final /* synthetic */ String[] val$result;
        private final /* synthetic */ int val$size;
        private final /* synthetic */ String val$toPath;

        /* renamed from: com.vkontakte.android.mediapicker.providers.GalleryPickerProvider.1.1 */
        class C15451 extends ActionCallback<Bitmap> {
            private final /* synthetic */ BitmapEntry val$styledBitmap;

            C15451(BitmapEntry bitmapEntry) {
                this.val$styledBitmap = bitmapEntry;
            }

            public void run(Bitmap result) {
                this.val$styledBitmap.set(result);
            }
        }

        C09171(Semaphore semaphore, String str, int i, String str2, int i2, String[] strArr) {
            this.val$lock = semaphore;
            this.val$path = str;
            this.val$size = i;
            this.val$toPath = str2;
            this.val$compression = i2;
            this.val$result = strArr;
        }

        public void run() {
            try {
                int height;
                ImageEntry image = new ImageEntry(ImageEntry.parseImagePath(this.val$path));
                File file = new File(GalleryPickerUtils.getFilePath(this.val$path));
                if (!file.exists()) {
                    Loggable.Error("GalleryPickerProvider whispers: image doesn't exist on this path %s", image.getPath());
                }
                boolean swapSides = (image.getOrientation() == 0 || image.getOrientation() == 180) ? false : true;
                Options options = new Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                int width = swapSides ? options.outHeight : options.outWidth;
                if (swapSides) {
                    height = options.outWidth;
                } else {
                    height = options.outHeight;
                }
                int sampleSize = LocalImageLoader.instance().getSampleSize(width, height, this.val$size);
                options.inSampleSize = sampleSize;
                options.inJustDecodeBounds = false;
                options.inDither = true;
                options.inPurgeable = true;
                if (VERSION.SDK_INT > 11) {
                    options.inMutable = true;
                }
                Bitmap sourceBitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                if (sourceBitmap == null) {
                    throw new IllegalAccessException("Image doesnt exist or corrupted");
                }
                BitmapEntry bitmapEntry = new BitmapEntry(sourceBitmap);
                width = swapSides ? options.outHeight : options.outWidth;
                if (swapSides) {
                    height = options.outWidth;
                } else {
                    height = options.outHeight;
                }
                image.setCropWidth(width / sampleSize);
                image.setCropHeight(height / sampleSize);
                LocalImageLoader.instance().rotateImage(image, bitmapEntry);
                if (image.getIsStyled() && image.getStyle().getIsCropped()) {
                    int i;
                    float[] cropData = image.getStyle().getCropData();
                    Loggable.Error("width %d (%d) -> %f, height %d (%d) -> %f", Integer.valueOf(options.outWidth), Integer.valueOf(((Bitmap) bitmapEntry.get()).getWidth()), Float.valueOf(cropData[4]), Integer.valueOf(options.outHeight), Integer.valueOf(((Bitmap) bitmapEntry.get()).getHeight()), Float.valueOf(cropData[5]));
                    float ratioX = ((float) (swapSides ? options.outHeight : options.outWidth)) / cropData[4];
                    if (swapSides) {
                        i = options.outWidth;
                    } else {
                        i = options.outHeight;
                    }
                    float ratioY = ((float) i) / cropData[5];
                    cropData[0] = cropData[0] * ratioX;
                    cropData[1] = cropData[1] * ratioY;
                    cropData[2] = cropData[2] * ratioX;
                    cropData[3] = cropData[3] * ratioY;
                    cropData[4] = cropData[4] * ratioX;
                    cropData[5] = cropData[5] * ratioY;
                    image.getStyle().setCropped(cropData);
                }
                ImageProcessor.instance().style(image, (Bitmap) bitmapEntry.get(), false, false, new C15451(bitmapEntry), false, this.val$size, 2);
                if (bitmapEntry.get() == null) {
                    throw new IllegalAccessException("Error processing image (perhaps, there is no enought memory)");
                }
                if (VERSION.SDK_INT > 11 && image.getIsStyled() && image.getStyle().getIsTexted() && image.getStyle().getText().length() > 0) {
                    StyleEntry style = new StyleEntry();
                    style.setText(image.getStyle().getText());
                    Bitmap bitmap = (Bitmap) bitmapEntry.get();
                    TextPainter.drawLobsterForBitmap(new Canvas(bitmap), bitmap.getWidth(), bitmap.getHeight(), style);
                }
                File bmpFile = new File(Uri.parse(this.val$toPath).getPath());
                if (!bmpFile.exists()) {
                    bmpFile.createNewFile();
                }
                OutputStream fileOutputStream = new FileOutputStream(this.val$toPath);
                ((Bitmap) bitmapEntry.get()).compress(CompressFormat.JPEG, this.val$compression, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
                this.val$result[0] = bmpFile.getAbsolutePath();
            } catch (IllegalAccessException access) {
                Loggable.Error("Error getPathForStyledImage", access, new Object[0]);
                this.val$result[0] = null;
            } catch (Throwable throwable) {
                Loggable.Error("Error getPathForStyledImage ", throwable, new Object[0]);
            } finally {
                this.val$lock.release();
            }
        }
    }

    static {
        instance = new GalleryPickerProvider();
    }

    public static GalleryPickerProvider instance() {
        return instance;
    }

    public int getSuggestedSizeLimit(String path, int defaultSize) {
        if (path == null || (path.contains("filter=0") && path.contains("enhanced=0"))) {
            return defaultSize;
        }
        DisplayMetrics metrics = ResourceProvider.getApplicationContext().getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        float density = metrics.density;
        int max = Math.max(width, height);
        int min = Math.min(width, height);
        if (min >= 720 || density >= ImageViewHolder.PaddingSize) {
            return DEFAULT_BIG_IMAGE_SIZE;
        }
        if (density < 1.0f || max <= 480) {
            return 640;
        }
        if (max >= 800 || min > 480) {
            return DEFAULT_SMALL_IMAGE_SIZE;
        }
        return 720;
    }

    public boolean getNeedUseStyledThumb(String path) {
        String params = path == null ? null : path.substring(path.lastIndexOf("?") + 1, path.length());
        return params != null && (params.startsWith("filter=") || params.contains("enhanced=1") || params.contains("crop="));
    }

    public static Uri getOriginalUri(String rawPath) {
        if (!rawPath.startsWith(STYLED_IMAGE_URI_START)) {
            return Uri.parse(rawPath);
        }
        String path = rawPath.substring(STYLED_IMAGE_URI_START.length());
        return Uri.fromFile(new File(path.substring(0, path.lastIndexOf("?"))));
    }

    public Bitmap getThumbnail(String path, String thumbPath) {
        Bitmap thumb;
        HashMap<String, String> values = ImageEntry.parseImagePath(path);
        try {
            ImageEntry image = new ImageEntry(Integer.parseInt((String) values.get("image_id")), (String) values.get("path"), Integer.parseInt((String) values.get("orientation")));
            if (values.containsKey("temp")) {
                image.setIsTemp(true);
            }
            if (StrictCache.instance().isCached(image.getCacheKey(false), true)) {
                thumb = StrictCache.instance().get(image.getCacheKey(false), true);
                File file = new File(thumbPath);
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileOutputStream stream = new FileOutputStream(file);
                thumb.compress(CompressFormat.JPEG, 89, stream);
                stream.flush();
                stream.close();
                return thumb;
            }
            thumb = null;
            try {
                return BitmapFactory.decodeFile(thumbPath);
            } catch (Throwable throwable) {
                Loggable.Error("Cannot read thumbnail from disk", throwable, new Object[0]);
                return thumb;
            }
        } catch (Throwable throwable2) {
            Loggable.Error("Cannot get thumbnail", throwable2, new Object[0]);
            return null;
        }
    }

    public String getPathForStyledImage(String path, int size, int compression, String toPath) {
        try {
            Semaphore lock = new Semaphore(0);
            String[] result = new String[1];
            LocalImageLoader.instance().dispatchHighload(new C09171(lock, path, size, toPath, compression, result));
            Loggable.ThreadBlock("ImagePickerProvider, near 274", new Object[0]);
            lock.acquire();
            return result[0];
        } catch (Throwable throwable) {
            Loggable.Error("Some errors in getStyledImagePath", throwable, new Object[0]);
            return null;
        }
    }
}
