package com.vkontakte.android.mediapicker.providers;

import android.content.res.Resources;
import android.support.v4.media.TransportMediator;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.ui.GalleryPickerHeaderView;
import net.hockeyapp.android.Strings;
import org.acra.ACRAConstants;

public class LangProvider {

    public static final class StringKeys {
        public static final int ATTACH = 1;
        public static final int BACK = 3;
        public static final int CAMERA_FOLDER = 20;
        public static final int CANCEL = 2;
        public static final int CAPTION = 9;
        public static final int CHOOSE = 11;
        public static final int CLEAR = 10;
        public static final int CLOSE = 54;
        public static final int CORRUPTED_IMAGE = 48;
        public static final int CORRUPTED_IMAGE_1 = 49;
        public static final int CORRUPTED_IMAGE_2 = 50;
        public static final int CROP = 7;
        public static final int DONE = 6;
        public static final int ENTER_TEXT = 8;
        public static final int ERROR = 34;
        public static final int FILTERS_UNSUPPORTED = 51;
        public static final int NO_IMAGES = 32;
        public static final int NO_SD_CARD = 33;
        public static final int PLURAL_PHOTOS = 80;
        public static final int RESET = 5;
        public static final int RETRY = 4;
        public static final int SELECTED = 16;
        public static final int SELECT_ALBUM = 17;
        public static final int SELECT_IMAGE = 19;
        public static final int SELECT_IMAGES = 18;
        public static final int SHARE_PHOTO = 52;
        public static final int SHARE_PHOTOS = 53;
    }

    public static String getLocalizedString(int type) {
        Resources r = ResourceProvider.getApplicationContext().getResources();
        switch (type) {
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                return r.getString(C0436R.string.picker_attach);
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return r.getString(C0436R.string.cancel);
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                return r.getString(C0436R.string.back);
            case UserListView.TYPE_FAVE /*4*/:
                return r.getString(C0436R.string.retry);
            case UserListView.TYPE_FOLLOWERS /*5*/:
                return r.getString(C0436R.string.reset);
            case UserListView.TYPE_POLL_VOTERS /*6*/:
                return r.getString(C0436R.string.done);
            case UserListView.TYPE_FAVE_LINKS /*7*/:
                return r.getString(C0436R.string.crop);
            case UserListView.TYPE_BLACKLIST /*8*/:
                return r.getString(C0436R.string.enter_text);
            case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                return r.getString(C0436R.string.caption);
            case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                return r.getString(C0436R.string.clear);
            case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                return r.getString(C0436R.string.choose);
            case TransportMediator.FLAG_KEY_MEDIA_PAUSE /*16*/:
                return r.getString(C0436R.string.selected);
            case StringKeys.SELECT_ALBUM /*17*/:
                return r.getString(C0436R.string.select_album);
            case StringKeys.SELECT_IMAGES /*18*/:
                return r.getString(C0436R.string.select_photos);
            case StringKeys.SELECT_IMAGE /*19*/:
                return r.getString(C0436R.string.select_photo);
            case StringKeys.CAMERA_FOLDER /*20*/:
                return r.getString(C0436R.string.folder_camera);
            case TransportMediator.FLAG_KEY_MEDIA_STOP /*32*/:
                return r.getString(C0436R.string.no_photos);
            case StringKeys.NO_SD_CARD /*33*/:
                return r.getString(C0436R.string.no_sd_card);
            case StringKeys.ERROR /*34*/:
                return r.getString(C0436R.string.error);
            case GalleryPickerHeaderView.SIZE /*48*/:
                return r.getString(C0436R.string.image_corrupted_1) + " " + r.getString(C0436R.string.image_corrupted_2);
            case StringKeys.CORRUPTED_IMAGE_1 /*49*/:
                return r.getString(C0436R.string.image_corrupted_1);
            case StringKeys.CORRUPTED_IMAGE_2 /*50*/:
                return r.getString(C0436R.string.image_corrupted_2);
            case StringKeys.FILTERS_UNSUPPORTED /*51*/:
                return r.getString(C0436R.string.filters_unsupported);
            case StringKeys.SHARE_PHOTO /*52*/:
                return r.getString(C0436R.string.share_photo);
            case StringKeys.SHARE_PHOTOS /*53*/:
                return r.getString(C0436R.string.share_photos);
            case StringKeys.CLOSE /*54*/:
                return r.getString(C0436R.string.close);
            default:
                return ACRAConstants.DEFAULT_STRING_VALUE;
        }
    }

    public static String getLocalizedPluralString(int type, int count) {
        switch (type) {
            case StringKeys.PLURAL_PHOTOS /*80*/:
                return Global.langPlural(C0436R.array.photos, count, VKApplication.context.getResources());
            default:
                return count;
        }
    }
}
