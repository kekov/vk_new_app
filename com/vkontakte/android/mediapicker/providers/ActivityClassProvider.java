package com.vkontakte.android.mediapicker.providers;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import com.vkontakte.android.mediapicker.providers.ResourceProvider.Views;

public class ActivityClassProvider extends FragmentActivity {
    protected static Context context;
    private static float density;
    private static boolean density_inited;
    private static Typeface impact;
    private static int list_padding;
    private static Typeface lobster;

    public static final class AnimationDuration {
        public static final int FILTERS_APPEARING = 135;
        public static final int SELECTION_EXIT_FADE = 150;
    }

    public static final class Color {
        public static final int ALBUMS_LIST_BACKGROUND = -15066598;
        public static final int BADGE_TEXT_COLOR = -1;
        public static final int DARK_GRAY = -9145228;
        public static final int DARK_SELECTION_COLOR = -1724664347;
        public static final int DIVIDER_COLOR = -10855846;
        public static final int HEADER_TEXT_COLOR = -1;
        public static final int IMAGES_GRID_BACKGROUND = -16777216;
        public static final int IMAGE_LOAD_BACKGROUND = -15658735;
        public static final int LIGHT_GRAY = -855310;
        public static final int LIST_DIVIDER_COLOR = -12895429;
        public static final int SELECTION_COLOR = -13388312;
        public static final int SELECTION_TRANSPARENT_COLOR = -1724664347;
        public static final int TEXT_COLOR = -1;
        public static final int TEXT_LIGHT_COLOR = -6250077;
        public static final int WINDOW_BACKGROUND_COLOR = -16777216;
    }

    protected static void setStaticLocalContext(Context context) {
        context = context;
    }

    protected static void setStaticTestApplicationContext(Context context) {
        context = context;
    }

    public static Context getStaticLocalContext() {
        return context;
    }

    static {
        density_inited = false;
        density = 0.0f;
        list_padding = -1;
    }

    public static float getDensity() {
        if (!density_inited) {
            density = ResourceProvider.getApplicationContext().getResources().getDisplayMetrics().density;
            density_inited = true;
        }
        return density;
    }

    public static int dp(float size, float maximumDensity) {
        return (int) ((Math.min(getDensity(), maximumDensity) * size) + 0.5f);
    }

    public static int px(float dp) {
        return (int) ((dp / getDensity()) + 0.5f);
    }

    public static int dp(float size) {
        return (int) ((getDensity() * size) + 0.5f);
    }

    public static int getScreenMaxSize() {
        DisplayMetrics displayMetrics = ResourceProvider.getApplicationContext().getResources().getDisplayMetrics();
        return Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    public static int getScreenMinSize() {
        DisplayMetrics displayMetrics = ResourceProvider.getApplicationContext().getResources().getDisplayMetrics();
        return Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    public int getListPadding() {
        if (list_padding == -1) {
            if (px((float) getScreenMinSize()) > Views.ACTION_FILTER) {
                list_padding = dp(20.0f);
            } else {
                list_padding = 0;
            }
        }
        return list_padding;
    }

    public static Typeface getDefaultTypeface(boolean isBold) {
        return isBold ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT;
    }

    public static Typeface getDefaultTypeface() {
        return getDefaultTypeface(false);
    }

    public static Typeface getImpactTypeface() {
        if (impact == null) {
            impact = Typeface.createFromAsset(ResourceProvider.getApplicationContext().getAssets(), AssetsProvider.getFontPath("impact.ttf"));
        }
        return impact;
    }

    public static Typeface getLobsterTypeface() {
        if (lobster == null) {
            lobster = Typeface.createFromAsset(ResourceProvider.getApplicationContext().getAssets(), AssetsProvider.getFontPath("lobster.ttf"));
        }
        return lobster;
    }

    protected void blockOrientation() {
        if (getResources().getConfiguration().orientation == 2) {
            setRequestedOrientation(6);
        } else {
            setRequestedOrientation(7);
        }
    }

    protected void unblockOrientation() {
        setRequestedOrientation(-1);
    }
}
