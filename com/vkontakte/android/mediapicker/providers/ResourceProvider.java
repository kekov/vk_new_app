package com.vkontakte.android.mediapicker.providers;

import android.content.Context;

public class ResourceProvider {

    public static final class Animations {
        public static final int SCALE_IN = 2130968586;
        public static final int SCALE_OUT = 2130968587;
    }

    public static final class Drawables {
        public static final int ACTION_CROP = 2130838045;
        public static final int ACTION_CROP_ENABLED = 2130838046;
        public static final int ACTION_ENCHANCE = 2130838043;
        public static final int ACTION_ENCHANCE_ENABLED = 2130838044;
        public static final int ACTION_FILTER = 2130838047;
        public static final int ACTION_FILTER_ENABLED = 2130838048;
        public static final int ACTION_TEXT = 2130838049;
        public static final int ACTION_TEXT_ENABLED = 2130838050;
        public static final int ACTION_VIEW_BACKGROUND = 2130838032;
        public static final int BACK_ICON = 2130838026;
        public static final int BADGE_ICON = 2130838027;
        public static final int CAMERA = 2130838033;
        public static final int CAMERA_ICON = 2130838034;
        public static final int CANCEL_ICON_BIG = 2130838028;
        public static final int CANCEL_ICON_DISABLED = 2130838036;
        public static final int CANCEL_ICON_ENABLED = 2130838035;
        public static final int CHECKMARK_BIG_CHECKED = 2130838030;
        public static final int CHECKMARK_BIG_UNCHECKED = 2130838029;
        public static final int CHECKMARK_CHECKED = 2130838038;
        public static final int CHECKMARK_UNCHECKED = 2130838037;
        public static final int COMPLETE_ICON_BIG = 2130838031;
        public static final int COMPLETE_ICON_DISABLED = 2130838054;
        public static final int COMPLETE_ICON_ENABLED = 2130838053;
        public static final int FILTERS_BACKGROUND = 2130838039;
        public static final int FILTER_SELECTION = 2130838040;
        public static final int HEADER_BACKGROUND = 2130838041;
        public static final int HEADER_ICON = 2130838042;
        public static final int IMAGE_SELECTION = 2130838056;
        public static final int MOSAIC_ICON = 2130838051;
        public static final int MOSAIC_ICON_BIG = 2130838052;
        public static final int REFRESH_ICON = 2130838055;
    }

    public static final class Views {
        public static final int ACTION_CROP = 321;
        public static final int ACTION_ENHANCE = 323;
        public static final int ACTION_FILTER = 320;
        public static final int ACTION_TEXT = 322;
        public static final int CAMERA_BUTTON = 272;
        public static final int CANCEL_BUTTON = 288;
        public static final int CANCEL_BUTTON_BIG = 291;
        public static final int COMPLETE_BUTTON = 289;
        public static final int COMPLETE_BUTTON_BIG = 292;
        public static final int COMPLETE_BUTTON_BIG_ICON = 293;
        public static final int DIVIDER_VIEW = 304;
        public static final int DIVIDER_VIEW_LEFT = 305;
        public static final int DIVIDER_VIEW_RIGHT = 306;
        public static final int FILTER_SELECTION = 336;
        public static final int FRAGMENT_VIEW = 257;
        public static final int PHOTO_VIEW = 256;
    }

    public static Context getApplicationContext() {
        return ActivityClassProvider.context;
    }
}
