package com.vkontakte.android.mediapicker.providers;

import com.vkontakte.android.Log;

public class LogProvider {
    public static final boolean LOADER_LOG_ENABLED = false;
    public static final boolean LOG_ENABLED = true;
    public static final boolean OPENGL_LOG_ENABLED = false;
    public static final boolean PROCESSOR_LOG_ENABLED = true;
    public static final boolean THREADBLOCK_LOG_ENABLED = false;
    public static final boolean THUMB_LOG_ENABLED = false;

    public static void m537v(String tag, String message) {
        Log.m529v(tag, message);
    }

    public static void m533d(String tag, String message) {
        Log.m525d(tag, message);
    }

    public static void m536i(String tag, String message) {
        Log.m528i(tag, message);
    }

    public static void m538w(String tag, String message) {
        Log.m530w(tag, message);
    }

    public static void m539w(String tag, String message, Throwable throwable) {
        Log.m531w(tag, message, throwable);
    }

    public static void m534e(String tag, String message) {
        Log.m526e(tag, message);
    }

    public static void m535e(String tag, String message, Throwable throwable) {
        Log.m527e(tag, message, throwable);
    }
}
