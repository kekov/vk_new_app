package com.vkontakte.android.mediapicker.providers;

public class AssetsProvider {
    public static String getBaseFolder() {
        return "imagepicker/";
    }

    public static String getFiltersFolder() {
        return getBaseFolder() + "filters/";
    }

    public static String getPreviewsFolder() {
        return getBaseFolder() + "previews/";
    }

    public static String getFontPath(String fileName) {
        return getBaseFolder() + fileName;
    }
}
