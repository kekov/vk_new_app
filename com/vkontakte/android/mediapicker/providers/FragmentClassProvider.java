package com.vkontakte.android.mediapicker.providers;

import android.support.v4.app.Fragment;
import com.vkontakte.android.mediapicker.utils.Loggable;

public class FragmentClassProvider extends Fragment {
    protected static void Log(String message) {
        Loggable.Log(message);
    }

    protected static void Log(String message, Object... args) {
        Loggable.Log(String.format(message, args));
    }
}
