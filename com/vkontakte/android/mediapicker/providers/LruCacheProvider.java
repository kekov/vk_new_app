package com.vkontakte.android.mediapicker.providers;

import android.app.ActivityManager;
import android.graphics.Bitmap;
import com.vkontakte.android.ImageCache;
import com.vkontakte.android.cache.LruCache;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;

public class LruCacheProvider {
    private static final String SMALL_CACHE_KEY_SUFFIX = "_small";
    private static LruCacheProvider instance;
    private LruCache<String, Bitmap> lruCache;

    /* renamed from: com.vkontakte.android.mediapicker.providers.LruCacheProvider.1 */
    class C15461 extends LruCache<String, Bitmap> {
        C15461(int $anonymous0) {
            super($anonymous0);
        }

        protected int sizeOf(String key, Bitmap value) {
            return value.getRowBytes() * value.getHeight();
        }

        protected void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue) {
            super.entryRemoved(evicted, key, oldValue, newValue);
        }
    }

    public static LruCacheProvider instance() {
        if (instance == null) {
            instance = new LruCacheProvider(new C15461(((((ActivityManager) ResourceProvider.getApplicationContext().getSystemService("activity")).getMemoryClass() / 10) * GLRenderBuffer.EGL_SURFACE_SIZE) * GLRenderBuffer.EGL_SURFACE_SIZE));
        }
        return instance;
    }

    private LruCacheProvider(LruCache<String, Bitmap> lruCache) {
        this.lruCache = lruCache;
    }

    private LruCache<String, Bitmap> getLruCache() {
        return ImageCache.getLruCache();
    }

    public void clear() {
        getLruCache().evictAll();
    }

    public boolean isCached(String key) {
        return getLruCache().contains(key);
    }

    public void put(String key, Bitmap value) {
        if (key == null) {
            return;
        }
        if (value == null) {
            getLruCache().remove(key);
        } else {
            getLruCache().put(key, value);
        }
    }

    public Bitmap get(String key) {
        return key == null ? null : (Bitmap) getLruCache().get(key);
    }

    public String getFilterPreviewCacheKey(int filterId) {
        return "filter" + filterId;
    }

    public String getImageCacheKey(boolean isStyled, String suffix) {
        return new StringBuilder(String.valueOf(isStyled ? "styled" : "local")).append(suffix).toString();
    }

    public String getSmallImageCacheKeySuffix() {
        return SMALL_CACHE_KEY_SUFFIX;
    }
}
