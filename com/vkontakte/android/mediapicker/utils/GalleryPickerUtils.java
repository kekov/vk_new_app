package com.vkontakte.android.mediapicker.utils;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.MediaStore.Images.Media;
import android.util.StateSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.EditText;
import android.widget.Toast;
import com.vkontakte.android.mediapicker.GalleryPickerActivity;
import com.vkontakte.android.mediapicker.entries.ActionCallback;
import com.vkontakte.android.mediapicker.entries.AlbumEntry;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.AnimationDuration;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.providers.GalleryPickerProvider;
import com.vkontakte.android.mediapicker.providers.LangProvider;
import com.vkontakte.android.mediapicker.providers.ResourceProvider;
import com.vkontakte.android.mediapicker.ui.ImageViewer;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.ui.PhotoView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.acra.ACRAConstants;

public class GalleryPickerUtils extends Loggable {
    private static final int MaximumCaptionLength = 176;
    public static final boolean SMALL_SCREEN_FILTERS_ENABLED = false;
    public static OnClickListener emptyOnClickListener;
    private static boolean force_filters_disabled;
    private static boolean hanim_enabled;
    private static boolean hanim_inited;
    private static GalleryPickerUtils instance;
    private static boolean is_device_shitty;
    private static boolean is_device_shitty_inited;
    public static GalleryPickerActivity sharedInstance;
    private static String[] shittyDevices;

    /* renamed from: com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.1 */
    class C09561 implements OnClickListener {
        C09561() {
        }

        public void onClick(View view) {
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.2 */
    class C09572 implements Runnable {
        private final /* synthetic */ ActionCallback val$callback;
        private final /* synthetic */ ActionCallback val$cameraBucketCallback;
        private final /* synthetic */ ContentResolver val$resolver;

        C09572(ContentResolver contentResolver, ActionCallback actionCallback, ActionCallback actionCallback2) {
            this.val$resolver = contentResolver;
            this.val$callback = actionCallback;
            this.val$cameraBucketCallback = actionCallback2;
        }

        public void run() {
            GalleryPickerUtils.this.getAlbums(this.val$resolver, this.val$callback, this.val$cameraBucketCallback);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.3 */
    class C09583 implements AnimatorListener {
        private final /* synthetic */ Runnable val$after;

        C09583(Runnable runnable) {
            this.val$after = runnable;
        }

        public void onAnimationStart(Animator animator) {
        }

        public void onAnimationCancel(Animator animator) {
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationEnd(Animator animator) {
            if (this.val$after != null) {
                this.val$after.run();
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.4 */
    class C09594 implements AnimatorListener {
        private final /* synthetic */ Runnable val$after;
        private final /* synthetic */ float val$to;
        private final /* synthetic */ View val$view;

        C09594(View view, float f, Runnable runnable) {
            this.val$view = view;
            this.val$to = f;
            this.val$after = runnable;
        }

        public void onAnimationStart(Animator animator) {
        }

        public void onAnimationCancel(Animator animator) {
        }

        public void onAnimationRepeat(Animator animator) {
        }

        public void onAnimationEnd(Animator animator) {
            this.val$view.setAlpha(this.val$to);
            if (this.val$after != null) {
                try {
                    this.val$after.run();
                } catch (Throwable throwable) {
                    Loggable.Error("Cannot call after action", throwable, new Object[0]);
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.5 */
    class C09605 implements AnimationListener {
        private final /* synthetic */ Runnable val$after;

        C09605(Runnable runnable) {
            this.val$after = runnable;
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            if (this.val$after != null) {
                this.val$after.run();
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.6 */
    class C09626 implements AnimatorListener {
        private final /* synthetic */ Runnable val$onToggled;
        private final /* synthetic */ float val$ratio;
        private final /* synthetic */ View val$view;

        /* renamed from: com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.6.1 */
        class C09611 implements AnimatorListener {
            private final /* synthetic */ View val$view;

            C09611(View view) {
                this.val$view = view;
            }

            public void onAnimationEnd(Animator animator) {
                this.val$view.setScaleY(1.0f);
                this.val$view.setScaleX(1.0f);
            }

            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }
        }

        C09626(View view, float f, Runnable runnable) {
            this.val$view = view;
            this.val$ratio = f;
            this.val$onToggled = runnable;
        }

        public void onAnimationStart(Animator animator) {
        }

        public void onAnimationEnd(Animator animator) {
            this.val$view.setScaleX(this.val$ratio);
            this.val$view.setScaleY(this.val$ratio);
            if (this.val$onToggled != null) {
                this.val$onToggled.run();
            }
            this.val$view.animate().scaleX(1.0f).scaleY(1.0f).setDuration(90).setListener(new C09611(this.val$view));
        }

        public void onAnimationCancel(Animator animator) {
        }

        public void onAnimationRepeat(Animator animator) {
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.7 */
    class C09647 extends Drawable {
        final Paint paint;

        /* renamed from: com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.7.1 */
        class C09631 extends Paint {
            C09631(int i) {
                setColor(i);
            }
        }

        C09647(int i) {
            this.paint = new C09631(i);
        }

        public void draw(Canvas canvas) {
            canvas.drawRect(getBounds(), this.paint);
        }

        public int getOpacity() {
            return 0;
        }

        public void setAlpha(int alpha) {
        }

        public void setColorFilter(ColorFilter colorFilter) {
        }
    }

    static {
        instance = new GalleryPickerUtils();
        force_filters_disabled = false;
        hanim_inited = false;
        emptyOnClickListener = new C09561();
        shittyDevices = new String[]{"GT-S5360", "GT-S5830M", "GT-S5830i", "GT-S5830C", "GT-S5570I", "GT-S5363", "GT-S5367", "GT-S6102", "GT-S6102B", "GT-S5300", "GT-S5839i", "GT-S6802B", "GT-S5302B", "GT-S6802", "GT-S5369", "GT-B5330", "GT-B5512B", "ZTE V768", "MS1", "PSPR355", "ONE TOUCH 983", "Vodafone Smart II", "TREND", "OMNI", "Multilaser Orion"};
        is_device_shitty = false;
        is_device_shitty_inited = false;
    }

    public static GalleryPickerUtils instance() {
        return instance;
    }

    public static void setForceFiltersDisabled(boolean isDisabled) {
        force_filters_disabled = isDisabled;
    }

    public static void showFiltersToast() {
        Toast.makeText(ResourceProvider.getApplicationContext(), LangProvider.getLocalizedString(51), 1).show();
    }

    public static boolean getAreFiltersSupported() {
        int i;
        int i2 = 0;
        if (ActivityClassProvider.getDensity() >= 1.0f) {
            i = 1;
        } else {
            i = 0;
        }
        boolean supported = true & i;
        if (!force_filters_disabled) {
            i2 = 1;
        }
        return supported & i2;
    }

    public static boolean getIsDeviceShitty() {
        if (is_device_shitty_inited) {
            return is_device_shitty;
        }
        for (String device : shittyDevices) {
            if (Build.MODEL.indexOf(device) != -1) {
                is_device_shitty = true;
                break;
            }
        }
        is_device_shitty_inited = true;
        return is_device_shitty;
    }

    public static void setViewNotFocusable(View view) {
        if (view != null) {
            view.setClickable(false);
            view.setFocusable(false);
            view.setFocusableInTouchMode(false);
        }
    }

    public static String getFilePath(String path) {
        if (path == null) {
            return null;
        }
        if (path.startsWith(GalleryPickerProvider.UNSTYLED_URI_SCHEME)) {
            return path.substring(GalleryPickerProvider.UNSTYLED_URI_SCHEME.length());
        }
        if (path.startsWith(GalleryPickerProvider.STYLED_IMAGE_URI_START)) {
            return path.substring(GalleryPickerProvider.STYLED_IMAGE_URI_START.length(), path.lastIndexOf("?"));
        }
        return path;
    }

    public static boolean isFileStyled(String path) {
        return path != null && path.startsWith(GalleryPickerProvider.STYLED_IMAGE_URI_SCHEME);
    }

    public static int getThumbnailAnimationDuration() {
        return instance().getHardAnimationsEnabled() ? ImageViewer.MOVING_ANIMATION_DURATION : PhotoView.THUMB_ANIM_DURATION;
    }

    public boolean getHardAnimationsEnabled() {
        if (!hanim_inited) {
            boolean z = VERSION.SDK_INT >= 14 && ActivityClassProvider.getDensity() >= ImageViewHolder.PaddingSize;
            hanim_enabled = z;
            hanim_inited = true;
        }
        return hanim_enabled;
    }

    private GalleryPickerUtils() {
        LocalImageLoader.instance();
    }

    public void invoke(Runnable runnable) {
        LocalImageLoader.instance().invoke(runnable);
    }

    public void invokeGetAlbums(ContentResolver resolver, ActionCallback<List<AlbumEntry>> callback, ActionCallback<Integer[]> cameraBucketCallback) {
        invoke(new C09572(resolver, callback, cameraBucketCallback));
    }

    public static void closeCursor(Cursor cursor) {
        if (cursor != null) {
            try {
                cursor.close();
            } catch (Throwable t) {
                Loggable.Error("Cannot close an album cursor", t, new Object[0]);
            }
        }
    }

    private void getAlbums(ContentResolver resolver, ActionCallback<List<AlbumEntry>> callback, ActionCallback<Integer[]> cameraBucketCallback) {
        try {
            Cursor cursor = Media.query(resolver, Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "bucket_id", "bucket_display_name", "_data", "datetaken", "orientation"}, ACRAConstants.DEFAULT_STRING_VALUE, null, "datetaken DESC");
            if (cursor == null || !cursor.moveToFirst()) {
                Object obj;
                closeCursor(cursor);
                if (cursor == null) {
                    obj = null;
                } else {
                    obj = new ArrayList();
                }
                callback.post(obj);
                return;
            }
            AlbumEntry album;
            List<Integer> keys = new ArrayList();
            HashMap<Integer, AlbumEntry> entries = new HashMap();
            int imageIdColumn = cursor.getColumnIndex("_id");
            int bucketIdColumn = cursor.getColumnIndex("bucket_id");
            int bucketNameColumn = cursor.getColumnIndex("bucket_display_name");
            int dataColumn = cursor.getColumnIndex("_data");
            int dateColumn = cursor.getColumnIndex("datetaken");
            int orientationColumn = cursor.getColumnIndex("orientation");
            String cameraFolder = CameraUtils.getCameraDir() + "/";
            String vkCameraFolder = CameraUtils.getVKCameraDir() + "/";
            boolean cameraFound = false;
            boolean vkCameraFound = false;
            int cameraBucketId = -1;
            int vkCameraBucketId = -1;
            do {
                int imageId = cursor.getInt(imageIdColumn);
                int bucketId = cursor.getInt(bucketIdColumn);
                String bucketName = cursor.getString(bucketNameColumn);
                String path = cursor.getString(dataColumn);
                ImageEntry image = new ImageEntry(bucketId, imageId, cursor.getInt(dateColumn), path, cursor.getInt(orientationColumn));
                if (entries.containsKey(Integer.valueOf(bucketId))) {
                    ((AlbumEntry) entries.get(Integer.valueOf(bucketId))).addImage(image);
                } else {
                    album = new AlbumEntry(bucketId, bucketName, imageId);
                    album.addImage(image);
                    if (!cameraFound && path != null && path.startsWith(cameraFolder)) {
                        album.setIsCamera(true);
                        keys.add(0, Integer.valueOf(album.getBucketId()));
                        cameraBucketId = album.getBucketId();
                        cameraFound = true;
                    } else if (vkCameraFound || path == null || !path.startsWith(vkCameraFolder)) {
                        keys.add(Integer.valueOf(album.getBucketId()));
                    } else {
                        album.setIsCamera(true);
                        keys.add(cameraFound ? 1 : 0, Integer.valueOf(album.getBucketId()));
                        vkCameraBucketId = album.getBucketId();
                        vkCameraFound = true;
                    }
                    entries.put(Integer.valueOf(album.getBucketId()), album);
                }
            } while (cursor.moveToNext());
            closeCursor(cursor);
            List<AlbumEntry> result = new ArrayList();
            if (keys.size() > 0) {
                Integer lastKey = (Integer) keys.get(keys.size() - 1);
                for (Integer key : keys) {
                    album = (AlbumEntry) entries.get(key);
                    album.updateCounter();
                    if (key != lastKey) {
                        album.setHasNext();
                    }
                    result.add(album);
                }
            }
            if (cameraFound || vkCameraFound) {
                Object obj2 = new Integer[2];
                obj2[0] = cameraFound ? Integer.valueOf(cameraBucketId) : null;
                obj2[1] = cameraFound ? Integer.valueOf(vkCameraBucketId) : null;
                cameraBucketCallback.post(obj2);
            }
            callback.post(result);
        } catch (Throwable th) {
            if (!callback.getHasExecuted()) {
                callback.post(null);
            }
        }
    }

    public void scale(View view, float to, int duration) {
        scale(view, to, duration, null);
    }

    @TargetApi(12)
    public void scale(View view, float to, int duration, Runnable after) {
        if (VERSION.SDK_INT >= 12) {
            view.animate().scaleX(to).scaleY(to).setDuration((long) duration).setListener(new C09583(after));
        }
    }

    public void fadeOut(View view, Runnable after, int duration) {
        fade(view, after, true, duration);
    }

    public void fadeIn(View view, Runnable after, int duration) {
        fade(view, after, false, duration);
    }

    @TargetApi(12)
    public void fade(View view, Runnable after, boolean out, int duration) {
        int i;
        int i2 = 0;
        if (out) {
            i = 1;
        } else {
            i = 0;
        }
        float from = (float) i;
        if (!out) {
            i2 = 1;
        }
        fade(view, from, (float) i2, duration, after);
    }

    @TargetApi(12)
    public void setAlpha(View view, float alpha) {
        if (VERSION.SDK_INT >= 12 && view != null) {
            view.setAlpha(alpha);
        }
    }

    @TargetApi(12)
    public void fade(View view, float from, float to, int duration, Runnable after) {
        if (view == null) {
            if (after != null) {
                try {
                    after.run();
                } catch (Throwable throwable) {
                    Loggable.Error("Cannot execute action \"after\" in ImagePickerUtils.fade", throwable, new Object[0]);
                }
            }
        } else if (VERSION.SDK_INT >= 12) {
            view.setAlpha(from);
            try {
                view.animate().alpha(to).setDuration((long) duration).setListener(new C09594(view, to, after));
            } catch (Throwable throwable2) {
                Loggable.Error("Cannot call after action", throwable2, new Object[0]);
            }
        } else {
            Animation animation = new AlphaAnimation(from, to);
            animation.setDuration((long) duration);
            animation.setAnimationListener(new C09605(after));
            view.startAnimation(animation);
        }
    }

    @TargetApi(12)
    public void bounce(View view, boolean reverse) {
        bounce(view, reverse, null);
    }

    @TargetApi(12)
    public void bounce(View view, boolean reverse, Runnable onToggled) {
        if (view != null && VERSION.SDK_INT >= 12) {
            float ratio = reverse ? 0.83f : 1.17f;
            try {
                view.animate().scaleX(ratio).scaleY(ratio).setDuration(78).setListener(new C09626(view, ratio, onToggled));
            } catch (Throwable th) {
                Loggable.Error("Cannot bounce badge, setting scale & calling onToggled", new Object[0]);
                view.setScaleX(1.0f);
                view.setScaleY(1.0f);
                if (onToggled != null) {
                    onToggled.run();
                }
            }
        } else if (onToggled != null) {
            try {
                onToggled.run();
            } catch (Throwable throwable) {
                Loggable.Error("Cannot toggle in bouncer", throwable, new Object[0]);
            }
        }
    }

    private Drawable getSelector(int color) {
        Drawable selectionDrawable;
        StateListDrawable drawable = new StateListDrawable();
        if (VERSION.SDK_INT >= 11) {
            selectionDrawable = new ColorDrawable(color);
        } else {
            selectionDrawable = new C09647(color);
        }
        drawable.addState(new int[]{16842919}, selectionDrawable);
        drawable.addState(new int[]{16842913}, selectionDrawable);
        drawable.addState(StateSet.WILD_CARD, new ColorDrawable(0));
        if (VERSION.SDK_INT >= 11) {
            drawable.setExitFadeDuration(AnimationDuration.SELECTION_EXIT_FADE);
        }
        return drawable;
    }

    public void setSelector(View view) {
        setSelector(view, false, Color.SELECTION_TRANSPARENT_COLOR);
    }

    public void setSelector(View view, boolean preventStateSet, int color) {
        if (VERSION.SDK_INT >= 16) {
            view.setBackground(getSelector(color));
        } else {
            view.setBackgroundDrawable(getSelector(color));
        }
        if (!preventStateSet) {
            view.setClickable(true);
            view.setFocusableInTouchMode(false);
            view.setFocusable(false);
        }
    }

    public void clearTextInput(EditText textView, String input) {
        if (input.length() != 0) {
            int selection = textView.getSelectionStart();
            if (input.length() > MaximumCaptionLength) {
                textView.setText(input.substring(0, MaximumCaptionLength));
                textView.setSelection(selection - 1);
            }
        }
    }
}
