package com.vkontakte.android.mediapicker.utils;

import android.view.View;
import android.widget.FrameLayout;
import com.vkontakte.android.mediapicker.GalleryPickerActivity;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class SelectionContext {
    private static ArrayList<String> completeOptions;
    private static ArrayList<String> completeSingleOptions;
    private static boolean editMode;
    private static boolean isInSelectionMode;
    private static boolean needSquare;
    private static ArrayList<String> passedItems;
    private static boolean preventStyling;
    private static boolean scrollDisabled;
    private static HashSet<ImageEntry> selectedImages;
    private static int selectionLimit;
    private static boolean selectionMode;
    private static boolean singleImageShowing;
    private static boolean singleMode;

    public static final class SelectionConstants {
        public static final int CANCELED = -1;
        public static final int CANCELED_WITH_SOUND = 0;
        public static final int DESELECTED = 2;
        public static final int SELECTED = 1;
    }

    static {
        selectionLimit = -1;
        singleMode = false;
        needSquare = false;
        preventStyling = false;
        isInSelectionMode = false;
        selectionMode = false;
        scrollDisabled = false;
        singleImageShowing = false;
        editMode = false;
    }

    public static void initialize(List<ImageEntry> images) {
        initialize(images.size(), false, false);
        selectedImages.addAll(images);
    }

    public static void initialize() {
        initialize(-1, true, false);
    }

    public static void initialize(int selectionLimit, boolean singleMode, boolean needSquare) {
        if (singleMode) {
            selectionLimit = 1;
        }
        singleImageShowing = false;
        isInSelectionMode = false;
        selectedImages = new HashSet();
        selectionLimit = selectionLimit;
        selectionMode = true;
        editMode = false;
        singleMode = singleMode;
        needSquare = needSquare;
    }

    public static void setCompleteOptions(ArrayList<String> options, ArrayList<String> singleOptions) {
        completeOptions = options;
        completeSingleOptions = singleOptions;
    }

    public static void setPreventStyling(boolean preventStyling) {
        preventStyling = preventStyling;
    }

    public static ArrayList<String> getPassedItems() {
        return (passedItems == null || passedItems.size() <= 0) ? null : passedItems;
    }

    public static List<ImageEntry> retrieveSelectedImages() {
        if (selectedImages == null) {
            return null;
        }
        HashSet<ImageEntry> images = selectedImages;
        List<ImageEntry> result = new ArrayList();
        Iterator it = images.iterator();
        while (it.hasNext()) {
            result.add((ImageEntry) it.next());
        }
        images.clear();
        clear();
        Collections.sort(result, ImageEntry.getComparator());
        return result;
    }

    public static void clear() {
        selectedImages = null;
        editMode = false;
        passedItems = null;
        selectionLimit = -1;
        singleMode = false;
        completeOptions = null;
        completeSingleOptions = null;
        preventStyling = false;
    }

    public static boolean chooseImage(ImageEntry image) {
        if (selectedImages == null) {
            return false;
        }
        boolean beChecked;
        GalleryPickerActivity activity = GalleryPickerUtils.sharedInstance;
        GalleryPickerFooterView footerView = activity != null ? activity.getFooterView() : null;
        if (image.getIsChecked()) {
            beChecked = false;
        } else {
            beChecked = true;
        }
        if (!beChecked) {
            selectedImages.remove(image);
        } else if (selectedImages.contains(image)) {
            return false;
        } else {
            if (checkRange()) {
                selectedImages.add(image);
            } else if (footerView == null) {
                return false;
            } else {
                footerView.shakeBadge();
                return false;
            }
        }
        if (image.hasBucket() && activity != null && activity.getGalleryContext() != null && activity.getGalleryContext().hasAlbumsFragment()) {
            activity.getGalleryContext().albumsFragment.updateAlbumSelectedCount(image.getBucketId(), beChecked);
        }
        if (footerView != null) {
            footerView.setBadge(selectedImages.size(), true);
        }
        return true;
    }

    public static boolean addImageToSelected(ImageEntry image) {
        if (singleMode || selectedImages == null || image == null || !checkRange()) {
            return false;
        }
        image.toggleChecked();
        selectedImages.add(image);
        return true;
    }

    public static int getSelectedCount() {
        return selectedImages == null ? -1 : selectedImages.size();
    }

    public static boolean getSingleMode() {
        return singleMode;
    }

    public static boolean getNeedSquare() {
        return needSquare;
    }

    public static boolean getNeedCompleteOption() {
        return (completeOptions == null && completeSingleOptions == null) ? false : true;
    }

    public static boolean getPreventStyling() {
        return preventStyling;
    }

    public static int getCompleteOptionsSize() {
        return (completeOptions == null || getSingleMode()) ? 0 : completeOptions.size();
    }

    public static CharSequence[] getCompleteOptions() {
        int i = 0;
        if (!getNeedCompleteOption()) {
            return null;
        }
        boolean useOptions;
        boolean useSingleOptions;
        int size;
        if (completeOptions == null || getSingleMode()) {
            useOptions = false;
        } else {
            useOptions = true;
        }
        if (completeSingleOptions == null || ((!getSingleMode() && getSelectedCount() > 1) || GalleryPickerUtils.sharedInstance == null || GalleryPickerUtils.sharedInstance.getGalleryContext() == null || GalleryPickerUtils.sharedInstance.getGalleryContext().currentViewerFragment == null)) {
            useSingleOptions = false;
        } else {
            useSingleOptions = true;
        }
        if (useOptions) {
            size = completeOptions.size();
        } else {
            size = 0;
        }
        if (useSingleOptions) {
            i = completeSingleOptions.size();
        }
        CharSequence[] sequences = new CharSequence[(size + i)];
        if (sequences.length == 0) {
            return sequences;
        }
        int i2;
        int j = 0;
        if (useOptions) {
            i2 = 0;
            while (i2 < completeOptions.size()) {
                sequences[j] = (CharSequence) completeOptions.get(i2);
                i2++;
                j++;
            }
        }
        if (!useSingleOptions) {
            return sequences;
        }
        i2 = 0;
        while (i2 < completeSingleOptions.size()) {
            sequences[j] = (CharSequence) completeSingleOptions.get(i2);
            i2++;
            j++;
        }
        return sequences;
    }

    public static boolean checkRange() {
        return selectionLimit == -1 || getSelectedCount() + 1 <= selectionLimit;
    }

    public static int getSelectionLimit() {
        return selectionLimit;
    }

    public static int performSelection(View wrap) {
        return performSelection(wrap, null);
    }

    public static int performSelection(int index) {
        return performSelection(index, null);
    }

    public static int performSelection(View wrap, Boolean checked) {
        ImageEntry image = (ImageEntry) ((FrameLayout) wrap).getChildAt(0).getTag();
        ImageViewHolder holder = (ImageViewHolder) wrap.getTag();
        return checked == null ? performSelection(holder, image) : performSelection(holder, image, checked.booleanValue());
    }

    public static int performSelection(int index, Boolean checked) {
        ImageViewHolder holder = getImageHolderByIndex(index);
        ImageEntry image = getImageDataFromHolder(holder);
        return checked == null ? performSelection(holder, image) : performSelection(holder, image, checked.booleanValue());
    }

    public static int performSelection(ImageViewHolder holder, ImageEntry image) {
        if (image == null || (GalleryPickerUtils.sharedInstance.getGalleryContext().currentAlbumFragment == null && !getIsSingleImageShowing())) {
            return -1;
        }
        if (!chooseImage(image)) {
            return 0;
        }
        image.toggleChecked();
        updateImageViewSelected(holder, image.getIsChecked());
        return image.getIsChecked() ? 1 : 2;
    }

    public static int performSelection(ImageViewHolder holder, ImageEntry image, boolean check) {
        if (image == null) {
            return -1;
        }
        if (check) {
            if (image.getIsChecked() || !chooseImage(image)) {
                return -1;
            }
            image.toggleChecked();
            updateImageViewSelected(holder, true);
            return 1;
        } else if (!image.getIsChecked() || !chooseImage(image)) {
            return -1;
        } else {
            image.toggleChecked();
            updateImageViewSelected(holder, false);
            return 2;
        }
    }

    public static boolean playSoundEffect(View view, int result, boolean vibrate) {
        if (view != null) {
            if (result != -1) {
                view.playSoundEffect(0);
            }
            if (vibrate) {
                view.performHapticFeedback(0);
            }
        }
        if (view != null) {
            return true;
        }
        return false;
    }

    public static boolean updateImageViewSelected(int index, boolean checked) {
        return updateImageViewSelected(getImageHolderByIndex(index), checked);
    }

    public static boolean updateImageViewSelected(ImageViewHolder holder, boolean isChecked) {
        if (holder != null) {
            ImageViewHolder.updateCheck(isChecked, holder.check);
            ImageViewHolder.updateBorder(isChecked, holder.overlay);
        }
        return holder != null;
    }

    private static ImageViewHolder getImageHolderByIndex(int index) {
        if (GalleryPickerUtils.sharedInstance.getGalleryContext().currentAlbumFragment != null) {
            return getImageHolderFromView(GalleryPickerUtils.sharedInstance.getGalleryContext().currentAlbumFragment.getGridViewAt(index));
        }
        return null;
    }

    public static ImageViewHolder getImageHolderFromView(View view) {
        return (view == null || view.getTag() == null || !(view.getTag() instanceof ImageViewHolder)) ? null : (ImageViewHolder) view.getTag();
    }

    private static ImageEntry getImageDataFromHolder(ImageViewHolder holder) {
        return (holder == null || holder.image == null || holder.image.getTag() == null || !(holder.image.getTag() instanceof ImageEntry)) ? null : (ImageEntry) holder.image.getTag();
    }

    private static ImageEntry getImageDataFromView(View view) {
        return (view == null || view.getTag() == null || !(view.getTag() instanceof ImageEntry)) ? null : (ImageEntry) view.getTag();
    }

    public static boolean getIsCheckedFromView(View view) {
        ImageEntry image = getImageDataFromView(view);
        return image == null || !image.getIsChecked();
    }

    public static boolean getIsSingleImageShowing() {
        return singleImageShowing;
    }

    public static void setSingleImageShowing(boolean showing) {
        singleImageShowing = showing;
    }

    public static void setSingleModeEnabled(boolean enabled) {
        singleMode = enabled;
    }

    public static void setEditModeEnabled(boolean enabled, ArrayList<String> images) {
        editMode = enabled;
        passedItems = images;
    }

    public static boolean getIsInSelectionMode() {
        return isInSelectionMode;
    }

    public static boolean getIsInEditMode() {
        return editMode;
    }

    public static boolean enterSelectionMode(boolean mode) {
        selectionMode = mode;
        scrollDisabled = false;
        if (isInSelectionMode) {
            return false;
        }
        isInSelectionMode = true;
        return true;
    }

    public static boolean leaveSelectionMode() {
        scrollDisabled = false;
        if (!isInSelectionMode) {
            return false;
        }
        isInSelectionMode = false;
        return true;
    }

    public static boolean getSelectionMode() {
        return selectionMode;
    }

    public static void setScrollEnabled(boolean isEnabled) {
        scrollDisabled = !isEnabled;
    }

    public static boolean getScrollEnabled() {
        return !scrollDisabled;
    }
}
