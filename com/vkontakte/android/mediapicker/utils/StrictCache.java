package com.vkontakte.android.mediapicker.utils;

import android.graphics.Bitmap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;
import org.acra.ACRAConstants;

public class StrictCache {
    private static StrictCache instance;
    private HashMap<String, Bitmap> values;

    /* renamed from: com.vkontakte.android.mediapicker.utils.StrictCache.1 */
    class C09781 implements Runnable {
        private final /* synthetic */ String val$key;
        private final /* synthetic */ Bitmap val$value;

        C09781(Bitmap bitmap, String str) {
            this.val$value = bitmap;
            this.val$key = str;
        }

        public void run() {
            if (this.val$value != null) {
                StrictCache.this.values.put(StrictCache.this.getRawKey(this.val$key), this.val$value);
            } else {
                StrictCache.this.values.remove(StrictCache.this.getRawKey(this.val$key));
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.StrictCache.2 */
    class C09792 implements Runnable {
        private final /* synthetic */ String val$key;
        private final /* synthetic */ Semaphore val$lock;
        private final /* synthetic */ Bitmap[] val$result;

        C09792(Bitmap[] bitmapArr, String str, Semaphore semaphore) {
            this.val$result = bitmapArr;
            this.val$key = str;
            this.val$lock = semaphore;
        }

        public void run() {
            this.val$result[0] = StrictCache.this.get(this.val$key, false);
            this.val$lock.release();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.StrictCache.3 */
    class C09803 implements Runnable {
        private final /* synthetic */ String val$key;
        private final /* synthetic */ Semaphore val$lock;
        private final /* synthetic */ boolean[] val$result;

        C09803(boolean[] zArr, String str, Semaphore semaphore) {
            this.val$result = zArr;
            this.val$key = str;
            this.val$lock = semaphore;
        }

        public void run() {
            this.val$result[0] = StrictCache.this.isCached(this.val$key, false);
            this.val$lock.release();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.StrictCache.4 */
    class C09814 implements Runnable {
        private final /* synthetic */ String val$key;

        C09814(String str) {
            this.val$key = str;
        }

        public void run() {
            StrictCache.this.values.remove(StrictCache.this.getRawKey(this.val$key));
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.StrictCache.5 */
    class C09825 implements Runnable {
        C09825() {
        }

        public void run() {
            for (String key : StrictCache.this.values.keySet()) {
                Bitmap bmp = (Bitmap) StrictCache.this.values.get(key);
                if (!(bmp == null || bmp.isRecycled())) {
                    bmp.recycle();
                }
            }
            StrictCache.this.values.clear();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.StrictCache.6 */
    class C09836 implements Runnable {
        private final /* synthetic */ List val$exceptionKeys;

        C09836(List list) {
            this.val$exceptionKeys = list;
        }

        public void run() {
            Set<String> keys = StrictCache.this.values.keySet();
            HashSet<String> exceptions = new HashSet();
            for (String key : this.val$exceptionKeys) {
                if (key != null) {
                    exceptions.add(StrictCache.this.getRawKey(key));
                }
            }
            this.val$exceptionKeys.clear();
            List<String> removedKeys = new ArrayList();
            for (String key2 : keys) {
                if (!(key2 == null || exceptions.contains(key2))) {
                    Bitmap bmp = StrictCache.this.get(key2);
                    removedKeys.add(key2);
                }
            }
            for (String key22 : removedKeys) {
                StrictCache.this.remove(key22);
            }
        }
    }

    public static StrictCache instance() {
        if (instance == null) {
            instance = new StrictCache();
        }
        return instance;
    }

    private StrictCache() {
        this.values = new HashMap();
    }

    private DispatchQueue queue() {
        return LocalImageLoader.instance().getQueue();
    }

    private void invoke(Runnable runnable) {
        LocalImageLoader.instance().invoke(runnable);
    }

    public void cache(String key, Bitmap value) {
        if (key != null) {
            invoke(new C09781(value, key));
        }
    }

    public Bitmap get(String key) {
        return get(key, false);
    }

    public Bitmap get(String key, boolean sync) {
        if (sync && Thread.currentThread() != queue()) {
            Bitmap[] result = new Bitmap[1];
            Semaphore lock = new Semaphore(0);
            invoke(new C09792(result, key, lock));
            try {
                Loggable.ThreadBlock("StrictCache.get, near 194", new Object[0]);
                lock.acquire();
            } catch (Throwable throwable) {
                Loggable.Error("Cannot block thread", throwable, new Object[0]);
            }
            return result[0];
        } else if (key == null) {
            return null;
        } else {
            return (Bitmap) this.values.get(getRawKey(key));
        }
    }

    public boolean isCached(String key) {
        return isCached(key, false);
    }

    public boolean isCached(String key, boolean sync) {
        if (sync && Thread.currentThread() != queue()) {
            boolean[] result = new boolean[1];
            Semaphore lock = new Semaphore(0);
            invoke(new C09803(result, key, lock));
            try {
                Loggable.ThreadBlock("LocalImageCache.check, near 225", new Object[0]);
                lock.acquire();
            } catch (Throwable throwable) {
                Loggable.Error("Cannot block thread", throwable, new Object[0]);
            }
            return result[0];
        } else if (key == null || !this.values.containsKey(getRawKey(key))) {
            return false;
        } else {
            return true;
        }
    }

    public void remove(String key) {
        if (key != null) {
            GalleryPickerUtils.instance().invoke(new C09814(key));
        }
    }

    public void clear() {
        GalleryPickerUtils.instance().invoke(new C09825());
    }

    public void clearExcept(List<String> exceptionKeys) {
        GalleryPickerUtils.instance().invoke(new C09836(exceptionKeys));
    }

    public String getRawKey(String key) {
        if (key == null) {
            return null;
        }
        if (key.contains("local")) {
            key = key.replace("local", ACRAConstants.DEFAULT_STRING_VALUE);
        }
        if (key.contains("styled")) {
            key = key.replace("styled", ACRAConstants.DEFAULT_STRING_VALUE);
        }
        return key;
    }
}
