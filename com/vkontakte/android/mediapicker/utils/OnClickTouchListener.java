package com.vkontakte.android.mediapicker.utils;

import android.os.Build.VERSION;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.entries.CancellableRunnable;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;

public abstract class OnClickTouchListener {
    private static final int DELAY_BEFORE_SELECTION = 175;
    private View currentView;
    private boolean delayBeforeDown;
    private boolean dropClick;
    private boolean dropTouch;
    private GestureDetector gestureDetector;
    private boolean isPressed;
    private OnTouchListener onTouchListener;
    private boolean preventAfterPositionChanged;
    private boolean preventCancelBeforeCompleted;
    private int requiredApiVersion;
    private CancellableRunnable touchDowner;
    private boolean touchUpExecuted;
    private int viewBound;
    private int viewHeight;
    private int viewSize;
    private int viewWidth;
    private int viewX;
    private int viewY;

    /* renamed from: com.vkontakte.android.mediapicker.utils.OnClickTouchListener.1 */
    class C09741 implements OnTouchListener {
        C09741() {
        }

        public boolean onTouch(View view, MotionEvent event) {
            if (view.isEnabled()) {
                return OnClickTouchListener.this.onTouched(view, event);
            }
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.OnClickTouchListener.3 */
    class C09753 implements Runnable {
        private final /* synthetic */ View val$view;

        C09753(View view) {
            this.val$view = view;
        }

        public void run() {
            if (OnClickTouchListener.this.touchUpExecuted) {
                OnClickTouchListener.this.touchUpExecuted = false;
            } else if (OnClickTouchListener.this.canExecuteStateListeners()) {
                OnClickTouchListener.this.onTapCanceled(this.val$view, false);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.OnClickTouchListener.2 */
    class C15532 extends CancellableRunnable {
        private final /* synthetic */ View val$view;

        C15532(View view) {
            this.val$view = view;
        }

        public void run() {
            if (OnClickTouchListener.this.canExecuteStateListeners()) {
                OnClickTouchListener.this.onTapStarted(this.val$view);
            }
        }
    }

    public abstract void onTapCompleted(View view);

    public OnClickTouchListener() {
        this.isPressed = false;
        this.preventCancelBeforeCompleted = false;
        this.touchUpExecuted = false;
        this.preventAfterPositionChanged = false;
        this.delayBeforeDown = false;
        this.requiredApiVersion = -1;
        this.dropTouch = false;
    }

    public void onTapStarted(View view) {
    }

    public void onTapCanceled(View view, boolean beforeCompleted) {
    }

    public final OnClickTouchListener setRequiredApiVersionForStateListeners(int apiVersion) {
        this.requiredApiVersion = apiVersion;
        return this;
    }

    public final OnClickTouchListener setUseDelayBeforeDown(boolean use) {
        this.delayBeforeDown = use;
        return this;
    }

    public final OnClickTouchListener setCancelOnViewMove(boolean prevent) {
        this.preventAfterPositionChanged = prevent;
        return this;
    }

    public final OnClickTouchListener setPreventCancelBeforeCompleted(boolean prevent) {
        this.preventCancelBeforeCompleted = prevent;
        return this;
    }

    public final OnClickTouchListener setGestureDetector(GestureDetector gestureDetector) {
        this.gestureDetector = gestureDetector;
        return this;
    }

    private final boolean canExecuteStateListeners() {
        return VERSION.SDK_INT >= this.requiredApiVersion;
    }

    public final OnTouchListener toOnTouchListener() {
        if (this.onTouchListener == null) {
            this.onTouchListener = new C09741();
        }
        return this.onTouchListener;
    }

    protected void onCanceled(View view) {
        cancelSelection(view);
    }

    private void cancelTouchDowner(View view, boolean execute) {
        if (this.touchDowner != null) {
            view.removeCallbacks(this.touchDowner.toOnceRunnable());
            if (execute) {
                this.touchDowner.toOnceRunnable().run();
            }
            this.touchDowner.cancel();
            this.touchDowner = null;
        }
    }

    private void postTouchDowner(View view) {
        if (this.touchDowner != null) {
            this.touchDowner.cancel();
        }
        this.touchDowner = new C15532(view);
        if (this.delayBeforeDown) {
            view.postDelayed(this.touchDowner.toOnceRunnable(), 175);
        } else {
            this.touchDowner.toOnceRunnable().run();
        }
    }

    private void cancelSelection(View view) {
        if (this.isPressed) {
            if (canExecuteStateListeners()) {
                cancelTouchDowner(view, false);
                view.postDelayed(new C09753(view), 100);
            }
            this.isPressed = false;
        }
    }

    public boolean dropTouch() {
        if (!this.dropTouch) {
            onCanceled(this.currentView);
        }
        this.dropTouch = true;
        return true;
    }

    public View getCurrentView() {
        return this.currentView;
    }

    private final boolean onTouched(View view, MotionEvent event) {
        if (event.getPointerCount() > 1) {
            onCanceled(view);
            return false;
        }
        int action = event.getAction();
        if (this.gestureDetector != null) {
            this.currentView = view;
            this.gestureDetector.onTouchEvent(event);
            if (!(this.dropTouch || !SelectionContext.getIsInSelectionMode() || action == 0)) {
                dropTouch();
            }
            if (this.dropTouch) {
                if (action == 1 || action == 3) {
                    this.dropTouch = false;
                    this.currentView = null;
                    SelectionContext.leaveSelectionMode();
                }
                return true;
            }
        }
        if (action == 0) {
            float tap;
            this.viewWidth = view.getMeasuredWidth();
            this.viewHeight = view.getMeasuredHeight();
            this.viewX = view.getLeft();
            this.viewY = view.getTop();
            this.viewSize = Math.max(this.viewWidth, this.viewHeight);
            if (VERSION.SDK_INT >= 9) {
                tap = Math.abs(event.getTouchMinor());
            } else {
                tap = ((float) this.viewSize) / ImageViewHolder.PaddingSize;
            }
            if (tap * ImageViewHolder.PaddingSize >= ((float) this.viewSize)) {
                tap = (float) this.viewSize;
            }
            this.viewBound = (int) tap;
        }
        if (this.preventAfterPositionChanged && !(view.getLeft() == this.viewX && view.getTop() == this.viewY)) {
            this.dropClick = true;
        }
        if (this.dropClick) {
            if (action == 1) {
                this.dropClick = false;
            }
            if (action == 0) {
                return true;
            }
            return false;
        }
        switch (action) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                if (!this.isPressed) {
                    if (canExecuteStateListeners()) {
                        postTouchDowner(view);
                    }
                    this.isPressed = true;
                    break;
                }
                break;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                if (this.isPressed) {
                    if (canExecuteStateListeners()) {
                        cancelTouchDowner(view, true);
                        onTapCanceled(view, true);
                    }
                    onTapCompleted(view);
                    this.isPressed = false;
                    break;
                }
                break;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                int x = (int) event.getX();
                int y = (int) event.getY();
                if (x >= (-this.viewBound) && y >= (-this.viewBound) && x <= this.viewWidth + this.viewBound && y <= this.viewHeight + this.viewBound) {
                    if (!this.isPressed) {
                        if (canExecuteStateListeners()) {
                            onTapStarted(view);
                        }
                        this.isPressed = true;
                        break;
                    }
                } else if (this.isPressed) {
                    if (canExecuteStateListeners()) {
                        onTapCanceled(view, false);
                    }
                    this.isPressed = false;
                    break;
                }
                break;
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                onCanceled(view);
                break;
        }
        if (action == 0) {
            return true;
        }
        return false;
    }
}
