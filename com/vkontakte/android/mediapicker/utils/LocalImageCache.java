package com.vkontakte.android.mediapicker.utils;

import com.vkontakte.android.mediapicker.providers.LruCacheProvider;

public class LocalImageCache {
    private static LocalImageCache instance;

    public static LocalImageCache instance() {
        if (instance == null) {
            instance = new LocalImageCache();
        }
        return instance;
    }

    private LocalImageCache() {
        LruCacheProvider.instance();
    }
}
