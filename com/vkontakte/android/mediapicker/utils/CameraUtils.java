package com.vkontakte.android.mediapicker.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaScannerConnection.OnScanCompletedListener;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.mediapicker.providers.ResourceProvider;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CameraUtils {
    private static final String DeviceCameraFolderName = "Camera";
    private static final String JpegFIleSuffix = ".jpg";
    private static final String JpegFilePrefix = "IMG_";
    public static final int PHOTO_TAKEN_RESULT = 100;
    private static final String VKCameraFolderName = "VK Camera";
    private static boolean hasCamera;
    private static boolean hasCameraInitialized;
    private static File mCurrentFile;
    private static String mCurrentPath;
    private static String mCurrentTitle;
    private static File mStorageDir;

    /* renamed from: com.vkontakte.android.mediapicker.utils.CameraUtils.1 */
    class C09551 implements OnScanCompletedListener {
        C09551() {
        }

        public void onScanCompleted(String s, Uri uri) {
            String str = "Image was successfuly scanned %s";
            Object[] objArr = new Object[1];
            if (s == null) {
                s = "null";
            }
            objArr[0] = s;
            Loggable.Error(str, objArr);
        }
    }

    private static String getTimestamp() {
        return new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    }

    private static File getStorageDir() {
        if (mStorageDir == null) {
            mStorageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            if (mStorageDir != null) {
                mStorageDir = new File(new StringBuilder(String.valueOf(mStorageDir.getAbsolutePath())).append("/").append(DeviceCameraFolderName).toString());
            }
            if (!(mStorageDir == null || mStorageDir.mkdirs() || mStorageDir.exists())) {
                mStorageDir = null;
            }
        }
        return mStorageDir;
    }

    private static File getOutput() throws IOException {
        String path = getStorageDir() + "/" + JpegFilePrefix + getTimestamp() + JpegFIleSuffix;
        File file = new File(path);
        Loggable.Error("Image file output: " + file.getAbsolutePath() + " " + path, new Object[0]);
        file.createNewFile();
        return file;
    }

    private static File prepareOutput() throws IOException {
        File file = getOutput();
        mCurrentPath = file.getAbsolutePath();
        mCurrentTitle = file.getName();
        return file;
    }

    private static File closeOutput() {
        mCurrentPath = null;
        return null;
    }

    public static void deleteImage() {
        if (mCurrentPath != null && mCurrentFile != null) {
            try {
                if (mCurrentFile.length() < 50) {
                    mCurrentFile.delete();
                }
            } catch (Throwable throwable) {
                Loggable.Error("Cannot delete file " + mCurrentPath, throwable, new Object[0]);
            }
        }
    }

    public static void addImageToGallery(Activity context) {
        if (mCurrentPath != null && mCurrentTitle != null) {
            try {
                if (!isExternalStorageMounted(false)) {
                    Loggable.Error("External storage is not mounted, so we are skipping mediascanner stuff", new Object[0]);
                } else if (isMediaScannerScanning()) {
                    Loggable.Error("Media scanner is running, we hope that we don't need to call the scanner", new Object[0]);
                } else {
                    ContentValues values = new ContentValues();
                    ContentResolver resolver = ResourceProvider.getApplicationContext().getContentResolver();
                    long time = System.currentTimeMillis();
                    values.put("_data", mCurrentPath);
                    values.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, mCurrentTitle);
                    values.put("_display_name", mCurrentTitle);
                    values.put("datetaken", Long.valueOf(time));
                    values.put("date_added", Long.valueOf(time));
                    values.put("date_modified", Long.valueOf(time));
                    values.put("mime_type", "image/jpeg");
                    Loggable.Warn("So, added image to gallery? " + resolver.insert(Media.EXTERNAL_CONTENT_URI, values), new Object[0]);
                }
            } catch (Throwable t) {
                Loggable.Error("Cannot add image to gallery", t, new Object[0]);
            }
            closeOutput();
        }
    }

    public static File getCurrentPhotoFile() {
        return mCurrentFile;
    }

    public static String getCurrentPhotoPath() {
        return mCurrentPath;
    }

    public static void launchCamera(Activity context) {
        if (getDeviceHasCamera() && "mounted".equals(Environment.getExternalStorageState()) && getStorageDir() != null) {
            Intent in = new Intent("android.media.action.IMAGE_CAPTURE");
            try {
                mCurrentFile = prepareOutput();
                in.putExtra("output", Uri.fromFile(mCurrentFile));
            } catch (Throwable t) {
                Loggable.Error("Cannot start camera intent", t, new Object[0]);
                mCurrentFile = closeOutput();
            }
            if (mCurrentFile != null && mCurrentPath != null) {
                context.startActivityForResult(in, PHOTO_TAKEN_RESULT);
            }
        }
    }

    static {
        hasCameraInitialized = false;
    }

    public static boolean getDeviceHasCamera() {
        if (hasCameraInitialized) {
            return hasCamera;
        }
        PackageManager manager = ResourceProvider.getApplicationContext().getPackageManager();
        hasCameraInitialized = true;
        hasCamera = manager.hasSystemFeature("android.hardware.camera");
        return hasCamera;
    }

    public static String getCameraDir() {
        return new StringBuilder(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath())).append("/").append(DeviceCameraFolderName).toString();
    }

    public static String getVKCameraDir() {
        return new StringBuilder(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath())).append("/").append(VKCameraFolderName).toString();
    }

    public static boolean isExternalStorageMounted(boolean allowReadonly) {
        String state = Environment.getExternalStorageState();
        return "mounted".equals(state) || (allowReadonly && "mounted_ro".equals(state));
    }

    public static boolean isMediaScannerScanning() {
        if (GalleryPickerUtils.sharedInstance == null) {
            return false;
        }
        boolean isRunning = false;
        Cursor cursor = ResourceProvider.getApplicationContext().getContentResolver().query(MediaStore.getMediaScannerUri(), new String[]{"volume"}, null, null, null);
        if (cursor == null) {
            return false;
        }
        if (cursor.getColumnCount() == 1 && cursor.moveToFirst()) {
            String state = cursor.getString(0);
            isRunning = "external".equals(state);
            if (!isRunning) {
                String str = "State of the mediascanner is %s";
                Object[] objArr = new Object[1];
                if (state == null) {
                    state = "null";
                }
                objArr[0] = state;
                Loggable.Error(str, objArr);
            }
        }
        cursor.close();
        return isRunning;
    }
}
