package com.vkontakte.android.mediapicker.utils;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore.Images.Media;
import android.provider.MediaStore.Images.Thumbnails;
import android.util.DisplayMetrics;
import com.facebook.NativeProtocol;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.entries.ActionCallback;
import com.vkontakte.android.mediapicker.entries.BitmapEntry;
import com.vkontakte.android.mediapicker.entries.CancellableRunnable;
import com.vkontakte.android.mediapicker.entries.ImageDataEntry;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import com.vkontakte.android.mediapicker.gl.ImageProcessor;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.providers.AssetsProvider;
import com.vkontakte.android.mediapicker.providers.LangProvider;
import com.vkontakte.android.mediapicker.providers.LruCacheProvider;
import com.vkontakte.android.mediapicker.providers.ResourceProvider;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.io.File;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Semaphore;
import org.acra.ACRAConstants;

public class LocalImageLoader extends Loggable {
    private static boolean deviceIsSamsung;
    private static boolean deviceIsSamsungInited;
    private static LocalImageLoader instance;
    private ArrayBlockingQueue<Runnable> highloadTasks;
    private DispatchQueue highloadThread;
    private boolean highload_inited;
    private DispatchQueue thread;
    private ArrayBlockingQueue<CancellableRunnable> thumbTasks;
    private boolean thumbload_inited;
    private int thumbnailTasksLimit;
    private boolean use_small_thumb;
    private boolean use_small_thumb_inited;
    private int use_thumb_sample_size;
    private int use_thumb_size;
    private int use_thumb_small_sample_size;

    /* renamed from: com.vkontakte.android.mediapicker.utils.LocalImageLoader.10 */
    class AnonymousClass10 implements Runnable {
        private final /* synthetic */ ActionCallback val$callback;
        private final /* synthetic */ Context val$context;
        private final /* synthetic */ int val$filter_id;
        private final /* synthetic */ String val$key;
        private final /* synthetic */ int val$size;

        AnonymousClass10(String str, ActionCallback actionCallback, Context context, int i, int i2) {
            this.val$key = str;
            this.val$callback = actionCallback;
            this.val$context = context;
            this.val$filter_id = i;
            this.val$size = i2;
        }

        public void run() {
            if (LruCacheProvider.instance().isCached(this.val$key)) {
                this.val$callback.post(LruCacheProvider.instance().get(this.val$key));
                return;
            }
            Bitmap bitmap;
            boolean failed;
            try {
                bitmap = BitmapFactory.decodeStream(this.val$context.getAssets().open(new StringBuilder(String.valueOf(AssetsProvider.getPreviewsFolder() + ImageProcessor.Filters[this.val$filter_id][0].toLowerCase())).append(".jpg").toString()));
                failed = false;
            } catch (Throwable throwable) {
                failed = true;
                bitmap = null;
                Loggable.Error("Cannot get preview for filter %d", throwable, Integer.valueOf(this.val$filter_id));
            }
            if (failed) {
                bitmap = LocalImageLoader.this.getFailedBitmap(this.val$size);
            } else {
                StrictCache.instance().cache(this.val$key, bitmap);
            }
            this.val$callback.post(bitmap);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.LocalImageLoader.1 */
    class C09651 implements Runnable {
        private final /* synthetic */ int val$limit;

        C09651(int i) {
            this.val$limit = i;
        }

        public void run() {
            LocalImageLoader.this.thumbnailTasksLimit = this.val$limit;
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.LocalImageLoader.2 */
    class C09662 implements Runnable {
        C09662() {
        }

        public void run() {
            while (true) {
                try {
                    CancellableRunnable runnable = (CancellableRunnable) LocalImageLoader.this.thumbTasks.take();
                    if (runnable != null) {
                        if (LocalImageLoader.this.thumbnailTasksLimit <= 0 || LocalImageLoader.this.thumbTasks.size() + 1 < LocalImageLoader.this.thumbnailTasksLimit) {
                            runnable.toRunnable().run();
                        } else {
                            runnable.cancel();
                        }
                    } else {
                        return;
                    }
                } catch (Throwable throwable) {
                    Loggable.Error("Error in thumbnail loop", throwable, new Object[0]);
                    return;
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.LocalImageLoader.3 */
    class C09673 implements Runnable {
        private final /* synthetic */ CancellableRunnable val$runnable;

        C09673(CancellableRunnable cancellableRunnable) {
            this.val$runnable = cancellableRunnable;
        }

        public void run() {
            LocalImageLoader.this.initThumbQueue();
            LocalImageLoader.this.thumbTasks.offer(this.val$runnable);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.LocalImageLoader.5 */
    class C09695 implements Runnable {
        private final /* synthetic */ Runnable val$runnable;

        /* renamed from: com.vkontakte.android.mediapicker.utils.LocalImageLoader.5.1 */
        class C09681 implements Runnable {
            private final /* synthetic */ DispatchQueue val$thread;

            C09681(DispatchQueue dispatchQueue) {
                this.val$thread = dispatchQueue;
            }

            public void run() {
                while (true) {
                    try {
                        Runnable runnable = (Runnable) LocalImageLoader.this.highloadTasks.take();
                        if (runnable != null) {
                            runnable.run();
                        } else {
                            return;
                        }
                    } catch (Throwable t) {
                        Loggable.Error(Thread.currentThread() + " error " + t.getClass().getSimpleName() + " " + t.getMessage(), t, new Object[0]);
                        return;
                    }
                }
            }
        }

        C09695(Runnable runnable) {
            this.val$runnable = runnable;
        }

        public void run() {
            if (!LocalImageLoader.this.highload_inited) {
                for (int i = 0; i < 6; i++) {
                    DispatchQueue thread = new DispatchQueue("ImagePickerDiskQueue#" + i);
                    thread.start();
                    thread.postRunnable(new C09681(thread), 0);
                }
                LocalImageLoader.this.highload_inited = true;
            }
            LocalImageLoader.this.highloadTasks.offer(this.val$runnable);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.LocalImageLoader.6 */
    class C09706 implements Runnable {
        private final /* synthetic */ ActionCallback val$callback;
        private final /* synthetic */ int val$context;
        private final /* synthetic */ ImageEntry val$image;
        private final /* synthetic */ boolean val$noEnhance;
        private final /* synthetic */ boolean val$noFilter;
        private final /* synthetic */ int val$size;
        private final /* synthetic */ boolean val$sync;

        /* renamed from: com.vkontakte.android.mediapicker.utils.LocalImageLoader.6.1 */
        class C15521 extends ActionCallback<Bitmap> {
            private final /* synthetic */ BitmapEntry val$bitmap;
            private final /* synthetic */ ActionCallback val$callback;
            private final /* synthetic */ boolean val$sync;

            C15521(BitmapEntry bitmapEntry, boolean z, ActionCallback actionCallback) {
                this.val$bitmap = bitmapEntry;
                this.val$sync = z;
                this.val$callback = actionCallback;
            }

            public void run(Bitmap result) {
                this.val$bitmap.set(result);
                if (this.val$sync) {
                    this.val$callback.exec(this.val$bitmap);
                } else {
                    this.val$callback.post(this.val$bitmap);
                }
            }
        }

        C09706(ImageEntry imageEntry, int i, boolean z, ActionCallback actionCallback, boolean z2, boolean z3, int i2) {
            this.val$image = imageEntry;
            this.val$size = i;
            this.val$sync = z;
            this.val$callback = actionCallback;
            this.val$noFilter = z2;
            this.val$noEnhance = z3;
            this.val$context = i2;
        }

        public void run() {
            try {
                String prefix = new StringBuilder(String.valueOf(this.val$image.toString())).append(": ").toString();
                long totalMs = System.currentTimeMillis();
                BitmapEntry bitmap = LocalImageLoader.this.getBitmapByPath(this.val$image.getPath(), this.val$size, this.val$image);
                if (!bitmap.getFailed()) {
                    if (this.val$image.getIsStyled() && this.val$image.getStyle().getIsCropped()) {
                        this.val$image.getStyle().getCropData();
                    }
                    ImageProcessor.instance().style(this.val$image, (Bitmap) bitmap.get(), this.val$noFilter, this.val$noEnhance, new C15521(bitmap, this.val$sync, this.val$callback), false, this.val$size, this.val$context);
                } else if (this.val$sync) {
                    this.val$callback.exec(bitmap);
                } else {
                    this.val$callback.post(bitmap);
                }
            } catch (Throwable throwable) {
                Loggable.Error("Error.. Fuck :(", throwable, new Object[0]);
                if (this.val$sync) {
                    this.val$callback.exec(null);
                } else {
                    this.val$callback.post(null);
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.LocalImageLoader.7 */
    class C09717 implements Runnable {
        private final /* synthetic */ Semaphore val$lock;
        private final /* synthetic */ boolean[] val$unlock_executed;

        C09717(boolean[] zArr, Semaphore semaphore) {
            this.val$unlock_executed = zArr;
            this.val$lock = semaphore;
        }

        public void run() {
            if (!this.val$unlock_executed[0]) {
                this.val$lock.release();
                this.val$unlock_executed[0] = true;
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.LocalImageLoader.8 */
    class C09728 implements Runnable {
        private final /* synthetic */ Runnable val$getter;
        private final /* synthetic */ Runnable val$unlock;

        C09728(Runnable runnable, Runnable runnable2) {
            this.val$getter = runnable;
            this.val$unlock = runnable2;
        }

        public void run() {
            this.val$getter.run();
            this.val$unlock.run();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.LocalImageLoader.9 */
    class C09739 implements Runnable {
        private final /* synthetic */ String val$threadName;
        private final /* synthetic */ boolean[] val$unlock_executed;

        C09739(boolean[] zArr, String str) {
            this.val$unlock_executed = zArr;
            this.val$threadName = str;
        }

        public void run() {
            if (!this.val$unlock_executed[0]) {
                Loggable.Error("!!!WARNING!!! IMAGE GETTER IS STILL RUNNING IN 10 SECONDS ON %s THREAD", this.val$threadName);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.LocalImageLoader.4 */
    class C15514 extends CancellableRunnable {
        private final /* synthetic */ String val$cacheKey;
        private final /* synthetic */ ActionCallback val$callback;
        private final /* synthetic */ boolean val$gridThumb;
        private final /* synthetic */ ImageEntry val$image;

        C15514(ImageEntry imageEntry, String str, boolean z, ActionCallback actionCallback) {
            this.val$image = imageEntry;
            this.val$cacheKey = str;
            this.val$gridThumb = z;
            this.val$callback = actionCallback;
        }

        public void run() {
            long ms = System.currentTimeMillis();
            ImageDataEntry result = new ImageDataEntry(this.val$image.getImageId());
            if (this.val$image.getIsTemp()) {
                result.setIsTemp(true);
            }
            if (LruCacheProvider.instance().isCached(this.val$cacheKey)) {
                result.setBitmap(LruCacheProvider.instance().get(this.val$cacheKey));
                return;
            }
            if (this.val$image.getIsTemp()) {
                BitmapEntry bitmap;
                if (LocalImageLoader.this.use_small_thumb || !this.val$gridThumb) {
                    bitmap = LocalImageLoader.this.getBitmapByPath(this.val$image.getPath(), 96, this.val$image);
                } else {
                    bitmap = LocalImageLoader.this.getBitmapByPath(this.val$image.getPath(), LocalImageLoader.this.use_thumb_size, this.val$image);
                }
                if (!(bitmap.getFailed() || bitmap.get() == null)) {
                    result.setBitmap((Bitmap) bitmap.get());
                }
            } else {
                boolean smallVariant;
                if (LocalImageLoader.this.use_small_thumb || !this.val$gridThumb) {
                    smallVariant = true;
                } else {
                    smallVariant = false;
                }
                Bitmap bitmap2 = null;
                if (smallVariant) {
                    bitmap2 = Thumbnails.getThumbnail(ResourceProvider.getApplicationContext().getContentResolver(), (long) this.val$image.getImageId(), 3, null);
                }
                if (bitmap2 == null) {
                    int access$6;
                    Options options = new Options();
                    if (smallVariant) {
                        access$6 = LocalImageLoader.this.use_thumb_small_sample_size;
                    } else {
                        access$6 = LocalImageLoader.this.use_thumb_sample_size;
                    }
                    options.inSampleSize = access$6;
                    bitmap2 = Thumbnails.getThumbnail(ResourceProvider.getApplicationContext().getContentResolver(), (long) this.val$image.getImageId(), 1, options);
                }
                BitmapEntry bitmapEntry = new BitmapEntry(bitmap2);
                LocalImageLoader.this.rotateImage(this.val$image, bitmapEntry);
                result.setBitmap((Bitmap) bitmapEntry.get());
            }
            if (result.getBitmap() == null) {
                result.setBitmap(LocalImageLoader.this.getFailedBitmap(-1));
            } else {
                LruCacheProvider.instance().put(this.val$cacheKey, result.getBitmap());
            }
            result.setLoadedIn((int) (System.currentTimeMillis() - ms));
            this.val$callback.post(result);
        }
    }

    public static LocalImageLoader instance() {
        if (instance == null) {
            instance = new LocalImageLoader();
        }
        return instance;
    }

    private LocalImageLoader() {
        this.thumbnailTasksLimit = -1;
        this.thumbload_inited = false;
        this.highload_inited = false;
        this.thumbTasks = new ArrayBlockingQueue(LocationStatusCodes.GEOFENCE_NOT_AVAILABLE);
        this.highloadTasks = new ArrayBlockingQueue(500);
        this.use_small_thumb_inited = false;
        this.thread = new DispatchQueue("ImagePickerThumbQueue");
        this.thread.start();
        this.highloadThread = new DispatchQueue("ImagePickerDiskQueue");
        this.highloadThread.start();
    }

    public void invoke(Runnable runnable) {
        this.thread.postRunnable(runnable, 0);
    }

    public void invoke(Runnable runnable, int delay) {
        this.thread.postRunnable(runnable, delay);
    }

    public DispatchQueue getQueue() {
        return this.thread;
    }

    public void setThumbTasksLimit(int limit) {
        invoke(new C09651(limit));
    }

    public int getThumbTasksLimit() {
        return this.thumbnailTasksLimit;
    }

    private void initThumbQueue() {
        if (!this.thumbload_inited) {
            DispatchQueue queue = new DispatchQueue("ThumbnailLoadQueue");
            queue.start();
            queue.postRunnable(new C09662(), 0);
            this.thumbload_inited = true;
        }
    }

    private void dispatchThumbnail(CancellableRunnable runnable) {
        invoke(new C09673(runnable));
    }

    public void getThumbnailForImage(ImageEntry image, boolean gridThumb, ActionCallback<Object> callback) {
        boolean z = true;
        if (!this.use_small_thumb_inited) {
            boolean z2;
            this.use_thumb_size = ActivityClassProvider.dp(ImageViewHolder.GridSize, 2.5f);
            if (this.use_thumb_size < 192) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.use_small_thumb = z2;
            this.use_thumb_sample_size = (int) Math.max((float) Math.ceil((double) (512.0f / ((float) this.use_thumb_size))), 1.0f);
            this.use_thumb_small_sample_size = (int) Math.max((float) Math.ceil((double) (512.0f / (((float) this.use_thumb_size) / ImageViewHolder.PaddingSize))), 1.0f);
            this.use_small_thumb_inited = true;
        }
        if (image.getUseAlternateThumb() || (gridThumb && !this.use_small_thumb)) {
            z = false;
        }
        String cacheKey = image.getCacheKey(z);
        if (!postThumbnailFromMemcache(cacheKey, image.getUseAlternateThumb(), callback)) {
            dispatchThumbnail(new C15514(image, cacheKey, gridThumb, callback));
        }
    }

    public void invokeHighload(Runnable runnable) {
        this.highloadThread.postRunnable(runnable, 0);
    }

    public void invokeHighload(Runnable runnable, int delay) {
        this.highloadThread.postRunnable(runnable, delay);
    }

    public void dispatchHighload(Runnable runnable) {
        invokeHighload(new C09695(runnable));
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.vkontakte.android.mediapicker.entries.BitmapEntry getBitmapByPath(java.lang.String r13, int r14, com.vkontakte.android.mediapicker.entries.ImageEntry r15) {
        /*
        r12 = this;
        r10 = 0;
        r6 = 1;
        r0 = new com.vkontakte.android.mediapicker.entries.BitmapEntry;
        r0.<init>();
        if (r15 == 0) goto L_0x0080;
    L_0x0009:
        r9 = r15.getOrientation();	 Catch:{ Throwable -> 0x008e }
        if (r9 == 0) goto L_0x0080;
    L_0x000f:
        r9 = r15.getOrientation();	 Catch:{ Throwable -> 0x008e }
        r11 = 180; // 0xb4 float:2.52E-43 double:8.9E-322;
        if (r9 == r11) goto L_0x0080;
    L_0x0017:
        r1 = new java.io.File;	 Catch:{ Throwable -> 0x008e }
        r9 = com.vkontakte.android.mediapicker.utils.GalleryPickerUtils.getFilePath(r13);	 Catch:{ Throwable -> 0x008e }
        r1.<init>(r9);	 Catch:{ Throwable -> 0x008e }
        r3 = new android.graphics.BitmapFactory$Options;	 Catch:{ Throwable -> 0x008e }
        r3.<init>();	 Catch:{ Throwable -> 0x008e }
        r9 = 1;
        r3.inJustDecodeBounds = r9;	 Catch:{ Throwable -> 0x008e }
        r9 = r1.getAbsolutePath();	 Catch:{ Throwable -> 0x008e }
        android.graphics.BitmapFactory.decodeFile(r9, r3);	 Catch:{ Throwable -> 0x008e }
        if (r6 == 0) goto L_0x0082;
    L_0x0031:
        r8 = r3.outHeight;	 Catch:{ Throwable -> 0x008e }
    L_0x0033:
        if (r6 == 0) goto L_0x0085;
    L_0x0035:
        r2 = r3.outWidth;	 Catch:{ Throwable -> 0x008e }
    L_0x0037:
        r4 = r12.getSampleSize(r8, r2, r14);	 Catch:{ Throwable -> 0x008e }
        r8 = r8 / r4;
        r2 = r2 / r4;
        r3.inSampleSize = r4;	 Catch:{ Throwable -> 0x008e }
        r9 = 0;
        r3.inJustDecodeBounds = r9;	 Catch:{ Throwable -> 0x008e }
        r9 = 1;
        r3.inDither = r9;	 Catch:{ Throwable -> 0x008e }
        r9 = 1;
        r3.inPurgeable = r9;	 Catch:{ Throwable -> 0x008e }
        r9 = r1.getAbsolutePath();	 Catch:{ Throwable -> 0x008e }
        r9 = android.graphics.BitmapFactory.decodeFile(r9, r3);	 Catch:{ Throwable -> 0x008e }
        r0.set(r9);	 Catch:{ Throwable -> 0x008e }
        r9 = -1;
        if (r14 != r9) goto L_0x0066;
    L_0x0056:
        if (r15 == 0) goto L_0x0066;
    L_0x0058:
        if (r6 == 0) goto L_0x0088;
    L_0x005a:
        r9 = r3.outHeight;	 Catch:{ Throwable -> 0x008e }
    L_0x005c:
        r15.setCropWidth(r9);	 Catch:{ Throwable -> 0x008e }
        if (r6 == 0) goto L_0x008b;
    L_0x0061:
        r9 = r3.outWidth;	 Catch:{ Throwable -> 0x008e }
    L_0x0063:
        r15.setCropHeight(r9);	 Catch:{ Throwable -> 0x008e }
    L_0x0066:
        r12.rotateImage(r15, r0);	 Catch:{ Throwable -> 0x008e }
    L_0x0069:
        r9 = r0.get();
        if (r9 != 0) goto L_0x007f;
    L_0x006f:
        if (r15 == 0) goto L_0x007f;
    L_0x0071:
        r5 = com.vkontakte.android.mediapicker.providers.ActivityClassProvider.getScreenMinSize();
        r9 = r12.getFailedBitmap(r5);
        r0.set(r9);
        r0.setFailed();
    L_0x007f:
        return r0;
    L_0x0080:
        r6 = r10;
        goto L_0x0017;
    L_0x0082:
        r8 = r3.outWidth;	 Catch:{ Throwable -> 0x008e }
        goto L_0x0033;
    L_0x0085:
        r2 = r3.outHeight;	 Catch:{ Throwable -> 0x008e }
        goto L_0x0037;
    L_0x0088:
        r9 = r3.outWidth;	 Catch:{ Throwable -> 0x008e }
        goto L_0x005c;
    L_0x008b:
        r9 = r3.outHeight;	 Catch:{ Throwable -> 0x008e }
        goto L_0x0063;
    L_0x008e:
        r7 = move-exception;
        r9 = "Cannot get image";
        r10 = new java.lang.Object[r10];
        com.vkontakte.android.mediapicker.utils.Loggable.Error(r9, r7, r10);
        r9 = 0;
        r0.set(r9);
        goto L_0x0069;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.mediapicker.utils.LocalImageLoader.getBitmapByPath(java.lang.String, int, com.vkontakte.android.mediapicker.entries.ImageEntry):com.vkontakte.android.mediapicker.entries.BitmapEntry");
    }

    public void getImage(ImageEntry originalImage, boolean noFilter, boolean noEnhance, ActionCallback<BitmapEntry> callback, boolean sync, int size, int context) {
        Runnable getter = new C09706(new ImageEntry(originalImage), size, sync, callback, noFilter, noEnhance, context);
        if (sync) {
            Semaphore lock = new Semaphore(0);
            try {
                boolean[] unlock_executed = new boolean[1];
                dispatchHighload(new C09728(getter, new C09717(unlock_executed, lock)));
                invokeHighload(new C09739(unlock_executed, Thread.currentThread().getName()), 10000);
                lock.acquire();
                return;
            } catch (Throwable th) {
                Loggable.Error("Cannot get image sync", new Object[0]);
                return;
            }
        }
        dispatchHighload(getter);
    }

    private String tryFixContentPath(String fix) {
        fix = fix.substring(NativeProtocol.CONTENT_SCHEME.length());
        if (!fix.contains("/")) {
            return null;
        }
        fix = fix.substring(fix.indexOf("/"));
        if (new File(fix).exists()) {
            return fix;
        }
        return null;
    }

    public ImageEntry getImageByUri(Activity activity, Uri uri) {
        Cursor cursor;
        String path = null;
        int orientation = 0;
        Loggable.Warn("Getting image by uri %s, scheme: %s file: %s", uri.getPath(), uri.getScheme(), uri.getLastPathSegment());
        if ("content".equals(uri.getScheme())) {
            cursor = ResourceProvider.getApplicationContext().getContentResolver().query(uri, getMediaColumnsProjection(), null, null, null);
            if (cursor == null || cursor.getCount() == 0) {
                path = tryFixContentPath(uri.toString());
            } else {
                int dataIndex = cursor.getColumnIndex("_data");
                int idIndex = cursor.getColumnIndex("_id");
                int orIndex = cursor.getColumnIndex("orientation");
                cursor.moveToFirst();
                int i = cursor.getInt(idIndex);
                path = cursor.getString(dataIndex);
                orientation = cursor.getInt(orIndex);
                if (!new File(path).exists()) {
                    String fix = tryFixContentPath(uri.toString());
                    if (fix != null) {
                        path = fix;
                    } else {
                        String[] splitted = path.split("/");
                        fix = ACRAConstants.DEFAULT_STRING_VALUE;
                        for (String part : splitted) {
                            String str;
                            StringBuilder stringBuilder = new StringBuilder(String.valueOf(fix));
                            if (part.length() == 0) {
                                str = ACRAConstants.DEFAULT_STRING_VALUE;
                            } else {
                                str = "/" + Uri.encode(part);
                            }
                            fix = stringBuilder.append(str).toString();
                        }
                        if (new File(fix).exists()) {
                            path = fix;
                        }
                    }
                }
            }
            GalleryPickerUtils.closeCursor(cursor);
        }
        if (path == null && !"file".equals(uri.getScheme())) {
            return null;
        }
        i = -1;
        if (path == null) {
            path = GalleryPickerUtils.getFilePath(uri.toString());
        }
        boolean temp = true;
        cursor = activity.getContentResolver().query(Media.EXTERNAL_CONTENT_URI, getMediaColumnsProjection(), "_data=? ", new String[]{path}, null);
        Loggable.Error("Getting content image by path result. Path: %s, count: %d", path, Integer.valueOf(cursor.getCount()));
        if (cursor != null && cursor.moveToFirst()) {
            i = cursor.getInt(cursor.getColumnIndex("_id"));
            orientation = cursor.getInt(cursor.getColumnIndex("orientation"));
            temp = false;
        }
        GalleryPickerUtils.closeCursor(cursor);
        ImageEntry image = temp ? new ImageEntry(i, path, true, orientation) : new ImageEntry(i, path, orientation);
        if (null != null) {
            image.setBucketId(0);
        }
        if (orientation != 0) {
            return image;
        }
        setOrientationByPath(image, new File(path));
        return image;
    }

    private String[] getMediaColumnsProjection() {
        return new String[]{"_id", "_data", "orientation", "bucket_id"};
    }

    static {
        deviceIsSamsung = false;
        deviceIsSamsungInited = false;
    }

    public int getSampleSize(int width, int height, int maximumSize) {
        DisplayMetrics display = ResourceProvider.getApplicationContext().getResources().getDisplayMetrics();
        int screenWidth = display.widthPixels;
        int screenHeight = display.heightPixels;
        boolean fitsInScreenSize = deviceIsSamsung;
        if ((width <= screenWidth && height <= screenHeight) || (width <= screenHeight && width <= screenWidth)) {
            if (!deviceIsSamsungInited) {
                boolean z = (Build.DEVICE.toLowerCase().indexOf("samsung") == -1 && Build.MANUFACTURER.toLowerCase().indexOf("samsung") == -1) ? false : true;
                deviceIsSamsung = z;
                deviceIsSamsungInited = true;
            }
            fitsInScreenSize = !deviceIsSamsung && ((width <= 1300 && height <= 780) || (width <= 780 && height <= 1300));
        }
        float ratio;
        if (maximumSize <= 0) {
            int limit = Math.min(GLRenderBuffer.EGL_SURFACE_SIZE, Math.max(display.widthPixels, display.heightPixels));
            if (maximumSize > 0) {
                limit = Math.min(limit, maximumSize);
            }
            if (width <= limit && height <= limit) {
                return 1;
            }
            ratio = ((float) Math.max(width, height)) / ((float) limit);
            return (int) (fitsInScreenSize ? Math.floor((double) ratio) : Math.ceil((double) ratio));
        } else if (width <= maximumSize && height <= maximumSize) {
            return 1;
        } else {
            double floor;
            ratio = ((float) Math.max(width, height)) / ((float) maximumSize);
            if (fitsInScreenSize) {
                floor = Math.floor((double) ratio);
            } else {
                floor = Math.ceil((double) ratio);
            }
            return (int) floor;
        }
    }

    private Bitmap getFailedBitmap(int size) {
        String key = "thumb_error_image" + size;
        if (size == -1) {
            Bitmap bmp;
            if (LruCacheProvider.instance().isCached(key)) {
                bmp = LruCacheProvider.instance().get(key);
            } else {
                bmp = null;
            }
            if (!(bmp == null || bmp.isRecycled())) {
                return LruCacheProvider.instance().get(key);
            }
        }
        boolean split = true;
        int textSize = 16;
        float textOffset = (float) ActivityClassProvider.dp(7.0f);
        if (size == -1) {
            size = ActivityClassProvider.dp(ImageViewHolder.GridSize);
            split = true;
            textSize = 10;
        }
        Bitmap bitmap = Bitmap.createBitmap(size, size, Config.RGB_565);
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(-1);
        p.setTextSize((float) ActivityClassProvider.dp((float) textSize));
        p.setTypeface(ActivityClassProvider.getDefaultTypeface());
        Canvas c = new Canvas(bitmap);
        c.drawColor(Color.IMAGE_LOAD_BACKGROUND);
        if (split) {
            String msg1 = LangProvider.getLocalizedString(49);
            String msg2 = LangProvider.getLocalizedString(50);
            float msg1Size = p.measureText(msg1, 0, msg1.length());
            float msg2Size = p.measureText(msg2, 0, msg2.length());
            float scaledTextSize = (float) ActivityClassProvider.dp((float) textSize);
            int center = (int) (((float) size) / ImageViewHolder.PaddingSize);
            c.drawText(msg1, (float) ((int) (((float) center) - (msg1Size / ImageViewHolder.PaddingSize))), (float) ((int) ((((float) center) - (scaledTextSize / ImageViewHolder.PaddingSize)) + textOffset)), p);
            c.drawText(msg2, (float) ((int) (((float) center) - (msg2Size / ImageViewHolder.PaddingSize))), (float) ((int) ((((float) center) + (scaledTextSize / ImageViewHolder.PaddingSize)) + textOffset)), p);
        } else {
            String msg = LangProvider.getLocalizedString(48);
            float f = (float) size;
            c.drawText(msg, (float) ((int) ((r0 / ImageViewHolder.PaddingSize) - (p.measureText(msg, 0, msg.length()) / ImageViewHolder.PaddingSize))), (float) ((int) ((((float) size) / ImageViewHolder.PaddingSize) + textOffset)), p);
        }
        if (size != -1) {
            return bitmap;
        }
        LruCacheProvider.instance().put(key, bitmap);
        return bitmap;
    }

    private Bitmap scaleThumbnail(int requiredSize, Bitmap bitmap) {
        float ratio = Math.min(((float) requiredSize) / ((float) bitmap.getWidth()), ((float) requiredSize) / ((float) bitmap.getHeight()));
        return Bitmap.createScaledBitmap(bitmap, (int) (((float) bitmap.getWidth()) * ratio), (int) (((float) bitmap.getHeight()) * ratio), true);
    }

    public void getPreviewForFilter(Context context, int filter_id, int size, ActionCallback<Object> callback) {
        String key = LruCacheProvider.instance().getFilterPreviewCacheKey(filter_id);
        if (LruCacheProvider.instance().isCached(key)) {
            callback.exec(LruCacheProvider.instance().get(key));
        } else {
            invoke(new AnonymousClass10(key, callback, context, filter_id, size));
        }
    }

    private boolean postThumbnailFromMemcache(String key, boolean fromStrictCache, ActionCallback<Object> callback) {
        if (fromStrictCache) {
            if (StrictCache.instance().isCached(key)) {
                callback.exec(StrictCache.instance().get(key));
                return true;
            }
            Loggable.Warn("Something went wrong: strict cache doesn't conatin the thumb with key: %s", key);
        }
        if (!LruCacheProvider.instance().isCached(key)) {
            return false;
        }
        callback.exec(LruCacheProvider.instance().get(key));
        return true;
    }

    public void setOrientationByPath(ImageEntry image, File file) {
        try {
            int rotation = 0;
            switch (new ExifInterface(file.getAbsolutePath()).getAttributeInt("Orientation", 0)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    rotation = 0;
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    rotation = 180;
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    rotation = 90;
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    rotation = 270;
                    break;
            }
            image.setOrientation(rotation);
        } catch (Throwable throwable) {
            Loggable.Error("Cannot read orientation from EXIF", throwable, new Object[0]);
        }
    }

    public void rotateImage(ImageEntry image, BitmapEntry bitmap) {
        if (image == null) {
            Loggable.Warn("Image cannot be rotated, because it's null", new Object[0]);
        } else if (bitmap.get() != null && image != null && image.getOrientation() != 0) {
            Matrix matrix = new Matrix();
            matrix.preRotate((float) image.getOrientation());
            try {
                bitmap.set(Bitmap.createBitmap((Bitmap) bitmap.get(), 0, 0, ((Bitmap) bitmap.get()).getWidth(), ((Bitmap) bitmap.get()).getHeight(), matrix, false));
            } catch (Throwable throwable) {
                Loggable.Error("Cannot rotate bitmap", throwable, new Object[0]);
            }
        }
    }
}
