package com.vkontakte.android.mediapicker.utils;

import com.vkontakte.android.mediapicker.providers.LogProvider;

public class Loggable {
    public static final String COMMON_TAG = "photo_picker_cmn";
    public static final String OPENGL_TAG = "photo_picker_gl";
    public static final String PROCESSOR_TAG = "photo_picker_ip";

    public static void Log(String message) {
        LogProvider.m537v(COMMON_TAG, message);
    }

    public static void Log(String tag, String message) {
        LogProvider.m533d(tag, message);
    }

    public static void Log(String message, Object... args) {
        String str = COMMON_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m537v(str, message);
    }

    public static void Debug(String message, Object... args) {
        String str = COMMON_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m533d(str, message);
    }

    public static void Error(String message, Object... args) {
        String str = COMMON_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m534e(str, message);
    }

    public static void Error(String message, Throwable throwable, Object... args) {
        String str = COMMON_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m535e(str, message, throwable);
    }

    public static void Warn(String message, Object... args) {
        String str = COMMON_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m538w(str, message);
    }

    public static void Warn(String message, Throwable throwable, Object... args) {
        String str = COMMON_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m539w(str, message, throwable);
    }

    public static void Info(String message, Object... args) {
        String str = COMMON_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m536i(str, message);
    }

    public static void ThreadBlock(String message, Object... args) {
    }

    public static void GLVerbose(String message, Object... args) {
    }

    public static void GLInfo(String message, Object... args) {
    }

    public static void GLError(String message, Object... args) {
        String str = OPENGL_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m534e(str, message);
    }

    public static void GLError(String message, Throwable throwable, Object... args) {
        String str = OPENGL_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m535e(str, message, throwable);
    }

    public static void GLWarn(String message, Object... args) {
        String str = OPENGL_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m538w(str, message);
    }

    public static void GLWarn(String message, Throwable throwable, Object... args) {
        String str = OPENGL_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m539w(str, message, throwable);
    }

    public static void IPVerbose(String message, Object... args) {
        String str = PROCESSOR_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m537v(str, message);
    }

    public static void IPInfo(String message, Object... args) {
        String str = PROCESSOR_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m536i(str, message);
    }

    public static void IPError(String message, Object... args) {
        String str = PROCESSOR_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m534e(str, message);
    }

    public static void IPError(String message, Throwable throwable, Object... args) {
        String str = PROCESSOR_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m535e(str, message, throwable);
    }

    public static void IPWarn(String message, Object... args) {
        String str = PROCESSOR_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m538w(str, message);
    }

    public static void IPWarn(String message, Throwable throwable, Object... args) {
        String str = PROCESSOR_TAG;
        if (args.length != 0) {
            message = String.format(message, args);
        }
        LogProvider.m539w(str, message, throwable);
    }

    public static String YN(boolean value) {
        return value ? "yes" : "no";
    }
}
