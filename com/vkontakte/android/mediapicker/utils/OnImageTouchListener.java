package com.vkontakte.android.mediapicker.utils;

import android.content.Context;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public abstract class OnImageTouchListener {
    private View currentView;
    private GestureDetector gestureDetector;
    private boolean mDoCheck;
    private boolean mIsInSelectionMode;
    private OnGestureListener onGestureListener;
    private OnTouchListener onTouchListener;

    /* renamed from: com.vkontakte.android.mediapicker.utils.OnImageTouchListener.1 */
    class C09761 implements OnGestureListener {
        C09761() {
        }

        public void onShowPress(MotionEvent event) {
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float velocityX, float velocityY) {
            return false;
        }

        public boolean onDown(MotionEvent event) {
            return true;
        }

        public void onLongPress(MotionEvent motionEvent) {
            boolean z = true;
            Loggable.Error("OnLongPress", new Object[0]);
            OnImageTouchListener onImageTouchListener = OnImageTouchListener.this;
            if (OnImageTouchListener.this.tapCurrentView(false) != 1) {
                z = false;
            }
            onImageTouchListener.startSelectionMode(z);
        }

        public boolean onSingleTapUp(MotionEvent event) {
            Loggable.Error("OnSingleTapUp", new Object[0]);
            if (!OnImageTouchListener.this.mIsInSelectionMode) {
                OnImageTouchListener.this.tapCurrentView(true);
            }
            return true;
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float velocityX, float velocityY) {
            boolean z = true;
            Loggable.Error("OnScroll %f %f", Float.valueOf(velocityX), Float.valueOf(velocityY));
            if (!OnImageTouchListener.this.mIsInSelectionMode) {
                float velX = Math.abs(velocityX);
                float velY = Math.abs(velocityY) * 0.9f;
                if (velX >= 15.0f && velX > velY) {
                    OnImageTouchListener onImageTouchListener = OnImageTouchListener.this;
                    if (OnImageTouchListener.this.tapCurrentView(true) != 1) {
                        z = false;
                    }
                    onImageTouchListener.startSelectionMode(z);
                }
            }
            return OnImageTouchListener.this.mIsInSelectionMode;
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.utils.OnImageTouchListener.2 */
    class C09772 implements OnTouchListener {
        C09772() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            return OnImageTouchListener.this.onTouched(view, motionEvent);
        }
    }

    public static final class CheckConstants {
        public static final int CANCELED = -1;
        public static final int CANCELED_WITH_SOUND = 0;
        public static final int CHECKED = 1;
        public static final int UNCHECKED = 2;
    }

    public abstract int getCurrentCellSize();

    public abstract void onIndexSelected(int i);

    public abstract void onSelectionModeCompleted();

    public abstract void onSelectionModeStarted();

    public abstract int onTap(View view);

    public OnImageTouchListener(Context context) {
        this.mIsInSelectionMode = false;
        this.mDoCheck = false;
        this.onGestureListener = new C09761();
        this.gestureDetector = new GestureDetector(context, this.onGestureListener);
    }

    public boolean getIsInSelectionMode() {
        return this.mIsInSelectionMode;
    }

    private void startSelectionMode(boolean doCheck) {
        this.mDoCheck = doCheck;
        this.mIsInSelectionMode = true;
        onSelectionModeStarted();
    }

    private void cancelSelectionMode() {
        this.mDoCheck = false;
        this.mIsInSelectionMode = false;
        onSelectionModeCompleted();
    }

    private int tapCurrentView(boolean playSoundEffect) {
        if (this.currentView == null) {
            return -1;
        }
        int result = onTap(this.currentView);
        if (!playSoundEffect || result == -1) {
            return result;
        }
        this.currentView.playSoundEffect(0);
        return result;
    }

    private final boolean onTouched(View view, MotionEvent event) {
        if (this.currentView == null || this.currentView != view) {
            this.currentView = view;
        }
        boolean result = this.gestureDetector.onTouchEvent(event);
        if (event.getAction() == 3 || event.getAction() == 1) {
            cancelSelectionMode();
        }
        if (result || this.mIsInSelectionMode) {
            return true;
        }
        return false;
    }

    public OnTouchListener toOnTouchListener() {
        if (this.onTouchListener == null) {
            this.onTouchListener = new C09772();
        }
        return this.onTouchListener;
    }
}
