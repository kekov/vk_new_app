package com.vkontakte.android.mediapicker.gl;

import android.graphics.Bitmap;
import com.vkontakte.android.mediapicker.entries.ActionCallback;
import com.vkontakte.android.mediapicker.entries.BitmapEntry;
import com.vkontakte.android.mediapicker.entries.ImageEntry;
import com.vkontakte.android.mediapicker.entries.StyleEntry;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.utils.DispatchQueue;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import com.vkontakte.android.mediapicker.utils.LocalImageLoader;
import com.vkontakte.android.mediapicker.utils.Loggable;
import com.vkontakte.android.mediapicker.utils.StrictCache;
import java.util.HashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class ImageProcessor {
    public static final int CONTEXT_PRIVATE = 1;
    public static final int CONTEXT_PUBLIC = 2;
    public static final String[][] Filters;
    private static final String LOGGING_TAG = "imagepicker_processor";
    private static ImageProcessor instance;
    private HashMap<Integer, GLFilterContext> filterContexts;
    private String last_image_key;
    private DispatchQueue queue;
    private AtomicInteger renderingCount;
    private HashMap<String, GLTexture> sourceTextures;

    /* renamed from: com.vkontakte.android.mediapicker.gl.ImageProcessor.1 */
    class C09101 implements Runnable {
        private final /* synthetic */ Semaphore val$lock;
        private final /* synthetic */ Runnable val$runnable;

        C09101(Runnable runnable, Semaphore semaphore) {
            this.val$runnable = runnable;
            this.val$lock = semaphore;
        }

        public void run() {
            this.val$runnable.run();
            this.val$lock.release();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gl.ImageProcessor.2 */
    class C09112 implements Runnable {
        private final /* synthetic */ Bitmap val$bitmap;
        private final /* synthetic */ ActionCallback val$callback;
        private final /* synthetic */ int val$context;
        private final /* synthetic */ boolean val$fromUi;
        private final /* synthetic */ ImageEntry val$image;
        private final /* synthetic */ int val$maximumSize;
        private final /* synthetic */ boolean val$noEnhance;
        private final /* synthetic */ boolean val$noFilter;

        /* renamed from: com.vkontakte.android.mediapicker.gl.ImageProcessor.2.1 */
        class C15421 extends ActionCallback<Bitmap> {
            private final /* synthetic */ BitmapEntry val$entry;

            C15421(BitmapEntry bitmapEntry) {
                this.val$entry = bitmapEntry;
            }

            public void run(Bitmap result) {
                this.val$entry.set(result);
            }
        }

        C09112(ImageEntry imageEntry, Bitmap bitmap, boolean z, boolean z2, int i, int i2, boolean z3, ActionCallback actionCallback) {
            this.val$image = imageEntry;
            this.val$bitmap = bitmap;
            this.val$noEnhance = z;
            this.val$noFilter = z2;
            this.val$context = i;
            this.val$maximumSize = i2;
            this.val$fromUi = z3;
            this.val$callback = actionCallback;
        }

        public void run() {
            Object[] objArr = new Object[ImageProcessor.CONTEXT_PRIVATE];
            objArr[0] = this.val$image.toString();
            ImageProcessor.this.log("STYLING STARTED - %s", objArr);
            long totalMs = System.currentTimeMillis();
            BitmapEntry entry = new BitmapEntry(this.val$bitmap);
            if (this.val$image.getIsStyled()) {
                StyleEntry style = this.val$image.getStyle();
                ActionCallback<Bitmap> setterCallback = new C15421(entry);
                if (style.getIsCropped()) {
                    ImageProcessor.this.crop((Bitmap) entry.get(), style.getCropData(), setterCallback);
                }
                if (!this.val$noEnhance && style.getIsEnhanced()) {
                    setterCallback.exec(CLAHE.process((Bitmap) entry.get()));
                }
                if (!this.val$noFilter && style.getIsFiltered()) {
                    ImageProcessor.this.filter(this.val$context, this.val$image, (Bitmap) entry.get(), true, true, setterCallback, this.val$maximumSize);
                }
                ImageProcessor.this.updateStyledThumb((Bitmap) entry.get(), this.val$image);
            } else {
                Loggable.Error("not caching styled thumb", new Object[0]);
            }
            if (this.val$fromUi) {
                this.val$callback.post((Bitmap) entry.get());
            } else {
                this.val$callback.exec((Bitmap) entry.get());
            }
            objArr = new Object[ImageProcessor.CONTEXT_PUBLIC];
            objArr[0] = Long.valueOf(((long) ((int) System.currentTimeMillis())) - totalMs);
            objArr[ImageProcessor.CONTEXT_PRIVATE] = this.val$image.toString();
            ImageProcessor.this.log("Styling completed in %dms for %s", objArr);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gl.ImageProcessor.3 */
    class C09123 implements Runnable {
        private final /* synthetic */ Bitmap val$bitmap;
        private final /* synthetic */ ImageEntry val$image;

        C09123(Bitmap bitmap, ImageEntry imageEntry) {
            this.val$bitmap = bitmap;
            this.val$image = imageEntry;
        }

        public void run() {
            ImageProcessor.this.updateStyledThumb(this.val$bitmap, this.val$image);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gl.ImageProcessor.5 */
    class C09135 implements Runnable {
        private final /* synthetic */ ActionCallback val$bitmapCallback;
        private final /* synthetic */ BitmapEntry val$bitmapEntry;
        private final /* synthetic */ int val$context;
        private final /* synthetic */ ImageEntry val$image;
        private final /* synthetic */ String val$image_key;

        C09135(ActionCallback actionCallback, BitmapEntry bitmapEntry, String str, ImageEntry imageEntry, int i) {
            this.val$bitmapCallback = actionCallback;
            this.val$bitmapEntry = bitmapEntry;
            this.val$image_key = str;
            this.val$image = imageEntry;
            this.val$context = i;
        }

        public void run() {
            GalleryPickerUtils.instance();
            if (GalleryPickerUtils.getAreFiltersSupported()) {
                try {
                    ImageProcessor.this.renderForBitmapWithKey(this.val$image_key, this.val$image.getFilterId(), this.val$bitmapEntry, this.val$bitmapCallback);
                } catch (Throwable throwable) {
                    Loggable.Error("I don't know, how it has happened", throwable, new Object[0]);
                }
                if (this.val$context == ImageProcessor.CONTEXT_PRIVATE) {
                    ImageProcessor.this.renderingCount.decrementAndGet();
                    return;
                }
                return;
            }
            this.val$bitmapCallback.exec((Bitmap) this.val$bitmapEntry.get());
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gl.ImageProcessor.7 */
    class C09147 implements Runnable {
        C09147() {
        }

        public void run() {
            ImageProcessor.this.clearTextures(true);
            ImageProcessor.this.clearFilters(true);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gl.ImageProcessor.8 */
    class C09158 implements Runnable {
        C09158() {
        }

        public void run() {
            ImageProcessor.this.clear();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gl.ImageProcessor.9 */
    class C09169 implements Runnable {
        private final /* synthetic */ boolean val$async;
        private final /* synthetic */ Bitmap val$bitmap;
        private final /* synthetic */ ActionCallback val$callback;
        private final /* synthetic */ StyleEntry val$styleEntry;

        C09169(Bitmap bitmap, StyleEntry styleEntry, boolean z, ActionCallback actionCallback) {
            this.val$bitmap = bitmap;
            this.val$styleEntry = styleEntry;
            this.val$async = z;
            this.val$callback = actionCallback;
        }

        public void run() {
            Bitmap result = this.val$bitmap;
            if (this.val$styleEntry.getIsCropped()) {
                result = ImageProcessor.this.applyCropForBitmap(result, this.val$styleEntry.getCropData());
            }
            if (this.val$styleEntry.getIsEnhanced()) {
                result = ImageProcessor.this.applyClaheForBitmap(result);
            }
            if (this.val$styleEntry.getIsFiltered()) {
                result = ImageProcessor.this.applyFilterForBitmap(result, this.val$styleEntry.getFilterId(), this.val$styleEntry.getTextureKey());
            }
            if (this.val$async) {
                this.val$callback.post(result);
            } else {
                this.val$callback.exec(result);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gl.ImageProcessor.4 */
    class C15434 extends ActionCallback<Bitmap> {
        private final /* synthetic */ ActionCallback val$callback;
        private final /* synthetic */ boolean val$sync;

        C15434(boolean z, ActionCallback actionCallback) {
            this.val$sync = z;
            this.val$callback = actionCallback;
        }

        public void run(Bitmap result) {
            if (this.val$sync) {
                this.val$callback.exec(result);
            } else {
                this.val$callback.post(result);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gl.ImageProcessor.6 */
    class C15446 extends ActionCallback<BitmapEntry> {
        private final /* synthetic */ BitmapEntry val$bitmapEntry;

        C15446(BitmapEntry bitmapEntry) {
            this.val$bitmapEntry = bitmapEntry;
        }

        public void run(BitmapEntry result) {
            Object obj;
            BitmapEntry bitmapEntry = this.val$bitmapEntry;
            if (result == null) {
                obj = null;
            } else {
                Bitmap bitmap = (Bitmap) result.get();
            }
            bitmapEntry.set(obj);
        }
    }

    static {
        String[][] strArr;
        String[] strArr2;
        String[] strArr3;
        if (ActivityClassProvider.getScreenMaxSize() > 480 || !GalleryPickerUtils.getAreFiltersSupported()) {
            strArr = new String[19][];
            strArr2 = new String[CONTEXT_PRIVATE];
            strArr2[0] = "Normal";
            strArr[0] = strArr2;
            strArr[CONTEXT_PRIVATE] = new String[]{"Pro", "filter_blackboard.jpg", "pro_overlaymap.png", "pro_map.png"};
            strArr[CONTEXT_PUBLIC] = new String[]{"Horus", "filter_blackboard.jpg", "horus_overlaymap.png", "horus_map.png"};
            strArr[3] = new String[]{"Latona", "latona_map.png", "latona_softlight.png", "latona_curves.png"};
            strArr[4] = new String[]{"Aurora", "aurora_curves.png", "aurora_overlaymap.png", "aurora_vignettemap.png", "aurora_blowout.png", "aurora_map.png"};
            strArr[5] = new String[]{"Liber", "liber_map.png", "liber_vignettemap.png"};
            strArr3 = new String[CONTEXT_PUBLIC];
            strArr3[0] = "Zaria";
            strArr3[CONTEXT_PRIVATE] = "zaria_map.png";
            strArr[6] = strArr3;
            strArr[7] = new String[]{"Vesta", "vesta_overlay.png", "vesta_softlight.png", "vesta_curves.png", "vesta_overlaymapwarm.png", "vesta_colorshift.png"};
            strArr[8] = new String[]{"Fortuna", "fortuna_map.png", "fortuna_gradientmap.png", "fortuna_softlight.png"};
            strArr3 = new String[CONTEXT_PUBLIC];
            strArr3[0] = "Nox";
            strArr3[CONTEXT_PRIVATE] = "nox_map.png";
            strArr[9] = strArr3;
            strArr[10] = new String[]{"Veles", "veles_background.jpg", "veles_overlay.png", "veles_map.png"};
            strArr[11] = new String[]{"Minerva", "minerva_map.png", "minerva_gradientmap.png", "minerva_softlight.png", "filter_metal.jpg"};
            strArr3 = new String[CONTEXT_PRIVATE];
            strArr3[0] = "Luna";
            strArr[12] = strArr3;
            strArr[13] = new String[]{"Iris", "iris_edgeburn.jpg", "iris_gradientmap.png", "iris_softlight.png", "filter_metal.jpg"};
            strArr3 = new String[CONTEXT_PUBLIC];
            strArr3[0] = "Terra";
            strArr3[CONTEXT_PRIVATE] = "terra_map.png";
            strArr[14] = strArr3;
            strArr[15] = new String[]{"Genius", "genius_map.png", "genius_metal.jpg", "genius_softlight.png", "genius_curves.png"};
            strArr[16] = new String[]{"Vesper", "vesper_map.png", "vesper_vignettemap.png"};
            strArr[17] = new String[]{"Mitra", "filter_blackboard.jpg", "mitra_overlaymap.png", "mitra_map.png", "mitra_bigmap.png"};
            strArr3 = new String[CONTEXT_PRIVATE];
            strArr3[0] = "Diana";
            strArr[18] = strArr3;
        } else {
            strArr = new String[17][];
            strArr2 = new String[CONTEXT_PRIVATE];
            strArr2[0] = "Normal";
            strArr[0] = strArr2;
            strArr[CONTEXT_PRIVATE] = new String[]{"Pro", "filter_blackboard.jpg", "pro_overlaymap.png", "pro_map.png"};
            strArr[CONTEXT_PUBLIC] = new String[]{"Horus", "filter_blackboard.jpg", "horus_overlaymap.png", "horus_map.png"};
            strArr[3] = new String[]{"Latona", "latona_map.png", "latona_softlight.png", "latona_curves.png"};
            strArr[4] = new String[]{"Aurora", "aurora_curves.png", "aurora_overlaymap.png", "aurora_vignettemap.png", "aurora_blowout.png", "aurora_map.png"};
            strArr[5] = new String[]{"Liber", "liber_map.png", "liber_vignettemap.png"};
            strArr3 = new String[CONTEXT_PUBLIC];
            strArr3[0] = "Zaria";
            strArr3[CONTEXT_PRIVATE] = "zaria_map.png";
            strArr[6] = strArr3;
            strArr[7] = new String[]{"Vesta", "vesta_overlay.png", "vesta_softlight.png", "vesta_curves.png", "vesta_overlaymapwarm.png", "vesta_colorshift.png"};
            strArr[8] = new String[]{"Fortuna", "fortuna_map.png", "fortuna_gradientmap.png", "fortuna_softlight.png"};
            strArr3 = new String[CONTEXT_PUBLIC];
            strArr3[0] = "Nox";
            strArr3[CONTEXT_PRIVATE] = "nox_map.png";
            strArr[9] = strArr3;
            strArr[10] = new String[]{"Veles", "veles_background.jpg", "veles_overlay.png", "veles_map.png"};
            strArr[11] = new String[]{"Minerva", "minerva_map.png", "minerva_gradientmap.png", "minerva_softlight.png", "filter_metal.jpg"};
            strArr[12] = new String[]{"Iris", "iris_edgeburn.jpg", "iris_gradientmap.png", "iris_softlight.png", "filter_metal.jpg"};
            strArr3 = new String[CONTEXT_PUBLIC];
            strArr3[0] = "Terra";
            strArr3[CONTEXT_PRIVATE] = "terra_map.png";
            strArr[13] = strArr3;
            strArr[14] = new String[]{"Genius", "genius_map.png", "genius_metal.jpg", "genius_softlight.png", "genius_curves.png"};
            strArr[15] = new String[]{"Vesper", "vesper_map.png", "vesper_vignettemap.png"};
            strArr3 = new String[CONTEXT_PRIVATE];
            strArr3[0] = "Diana";
            strArr[16] = strArr3;
        }
        Filters = strArr;
        instance = new ImageProcessor();
    }

    public static ImageProcessor instance() {
        return instance;
    }

    private ImageProcessor() {
        this.queue = new DispatchQueue("RendererQueue");
        this.queue.start();
    }

    private void log(String msg, Object... args) {
        Loggable.Log(LOGGING_TAG, String.format(msg, args));
    }

    public void dispatch_sync(Runnable runnable) {
        if (Thread.currentThread() == this.queue) {
            dispatch_async(runnable);
            return;
        }
        Semaphore lock = new Semaphore(0);
        dispatch_async(new C09101(runnable, lock));
        try {
            Loggable.ThreadBlock("ImageProcessor, near 98", new Object[0]);
            lock.acquire();
        } catch (Throwable t) {
            Loggable.Error("Error invoking sync renderer", t, new Object[0]);
        }
    }

    public void dispatch_async(Runnable runnable) {
        this.queue.postRunnable(runnable, 0);
    }

    public void style(ImageEntry image, Bitmap bitmap, boolean noFilter, boolean noEnhance, ActionCallback<Bitmap> callback, boolean fromUi, int maximumSize, int context) {
        if (image.getIsStyled()) {
            Runnable runnable = new C09112(image, bitmap, noEnhance, noFilter, context, maximumSize, fromUi, callback);
            if (fromUi) {
                dispatch_async(runnable);
                return;
            } else {
                runnable.run();
                return;
            }
        }
        callback.exec(bitmap);
    }

    public void updateStyledThumbAsync(Bitmap bitmap, ImageEntry image) {
        GalleryPickerUtils.instance().invoke(new C09123(bitmap, image));
    }

    public void updateStyledThumb(Bitmap bitmap, ImageEntry image) {
        if (image != null) {
            if (bitmap == null || bitmap.isRecycled()) {
                StrictCache.instance().remove(image.getCacheKey(false));
                return;
            }
            Bitmap thumb2;
            float width = (float) bitmap.getWidth();
            float height = (float) bitmap.getHeight();
            float ratio = Math.min(219.0f / width, 219.0f / height);
            Bitmap thumb = Bitmap.createScaledBitmap(bitmap, (int) (width * ratio), (int) (height * ratio), true);
            if (thumb.getWidth() >= thumb.getHeight()) {
                thumb2 = Bitmap.createBitmap(thumb, (thumb.getWidth() / CONTEXT_PUBLIC) - (thumb.getHeight() / CONTEXT_PUBLIC), 0, thumb.getHeight(), thumb.getHeight());
            } else {
                thumb2 = Bitmap.createBitmap(thumb, 0, (thumb.getHeight() / CONTEXT_PUBLIC) - (thumb.getWidth() / CONTEXT_PUBLIC), thumb.getWidth(), thumb.getWidth());
            }
            StrictCache.instance().cache(image.getCacheKey(false), thumb2);
        }
    }

    public boolean isRendering() {
        GalleryPickerUtils.instance();
        return GalleryPickerUtils.getAreFiltersSupported() && this.renderingCount != null && this.renderingCount.get() > 0;
    }

    public void filter(int context, ImageEntry sourceImage, Bitmap sourceBitmap, boolean wasFiltered, boolean sync, ActionCallback<Bitmap> callback, int maximumSize) {
        if (context == CONTEXT_PRIVATE) {
            if (this.renderingCount == null) {
                this.renderingCount = new AtomicInteger(CONTEXT_PRIVATE);
            } else {
                this.renderingCount.incrementAndGet();
            }
        }
        ImageEntry image = sync ? sourceImage : new ImageEntry(sourceImage);
        StringBuilder append = new StringBuilder(String.valueOf(context)).append("_").append(image.getImageId()).append("_");
        String str = (image.getIsStyled() && image.getStyle(true).getIsEnhanced()) ? "1" : "0";
        String image_key = append.append(str).append("_").append(image.getCropKeys()).toString();
        BitmapEntry bitmapEntry = new BitmapEntry(sourceBitmap);
        String str2 = "Applying filter for key %s (last key was %s)";
        Object[] objArr = new Object[CONTEXT_PUBLIC];
        objArr[0] = image_key;
        if (this.last_image_key == null) {
            str = "null";
        } else {
            str = this.last_image_key;
        }
        objArr[CONTEXT_PRIVATE] = str;
        Loggable.Error(str2, objArr);
        Runnable runnable = new C09135(new C15434(sync, callback), bitmapEntry, image_key, image, context);
        if (sourceBitmap == null || wasFiltered) {
            if (compareKeys(image_key, this.last_image_key) && image.getIsLoaded()) {
                bitmapEntry.set((Bitmap) image.getImageData().get());
            } else {
                Loggable.Error("Getting image from disk", new Object[0]);
                LocalImageLoader.instance().getImage(image, true, false, new C15446(bitmapEntry), true, maximumSize, context);
            }
        }
        GLRenderer instance = GLRenderer.instance(((Bitmap) bitmapEntry.get()).getWidth(), ((Bitmap) bitmapEntry.get()).getHeight());
        if (sync) {
            instance.invoke_sync(runnable, true);
        } else {
            instance.invoke_async(runnable, true);
        }
    }

    private boolean compareKeys(String a, String b) {
        return (a == null || b == null || !a.equals(b)) ? false : true;
    }

    public void crop(Bitmap bitmap, float[] coords, ActionCallback<Bitmap> callback) {
        int x = Math.max((int) coords[0], 0);
        int y = Math.max((int) coords[CONTEXT_PRIVATE], 0);
        int w = Math.min((int) (coords[CONTEXT_PUBLIC] - coords[0]), (int) (coords[4] - ((float) x)));
        int h = Math.min((int) (coords[3] - coords[CONTEXT_PRIVATE]), (int) (coords[5] - ((float) y)));
        if (w <= 0 || h <= 0) {
            callback.exec(bitmap);
            return;
        }
        callback.exec(Bitmap.createBitmap(bitmap, x, y, w, h));
        bitmap.recycle();
    }

    public void clearForOtherContext(boolean onClose) {
        dispatch_async(new C09147());
    }

    private void clear() {
        clearTextures(true);
        clearFilters(false);
    }

    public void clearCurrentContext() {
        dispatch_async(new C09158());
    }

    private void clearTextures(boolean noUnload) {
        this.last_image_key = null;
        if (this.sourceTextures != null) {
            try {
                for (String key : this.sourceTextures.keySet()) {
                    GLTexture texture = (GLTexture) this.sourceTextures.get(key);
                    if (!(texture == null || noUnload)) {
                        texture.unload();
                    }
                }
                this.sourceTextures.clear();
            } catch (Throwable throwable) {
                Loggable.GLError("Cannot clear source textures", throwable, new Object[0]);
            }
        }
    }

    private void clearFilters(boolean noUnload) {
        if (this.filterContexts != null) {
            try {
                for (Integer fid : this.filterContexts.keySet()) {
                    GLFilterContext context = (GLFilterContext) this.filterContexts.get(fid);
                    if (!(context == null || noUnload)) {
                        context.unload();
                    }
                }
                this.filterContexts.clear();
            } catch (Throwable throwable) {
                Loggable.GLError("Cannot clear filters contexts", throwable, new Object[0]);
            }
        }
    }

    private void renderForBitmapWithKey(String key, int filterId, BitmapEntry bitmap, ActionCallback<Bitmap> callback) {
        if (this.filterContexts == null) {
            this.filterContexts = new HashMap();
        }
        if (this.sourceTextures == null) {
            this.sourceTextures = new HashMap();
        }
        long totalMs = System.currentTimeMillis();
        if (!(this.last_image_key == null || this.last_image_key.equals(key))) {
            long ms = System.currentTimeMillis();
            clear();
        }
        this.last_image_key = key;
        GLFilterContext context = this.filterContexts.containsKey(Integer.valueOf(filterId)) ? (GLFilterContext) this.filterContexts.get(Integer.valueOf(filterId)) : null;
        if (context == null) {
            ms = System.currentTimeMillis();
            context = new GLFilterContext(new GLFilter(Filters[filterId]));
            context.load();
            this.filterContexts.put(Integer.valueOf(filterId), context);
        }
        GLTexture source = this.sourceTextures.containsKey(key) ? (GLTexture) this.sourceTextures.get(key) : null;
        if (source == null) {
            ms = System.currentTimeMillis();
            source = new GLTexture();
            source.loadWithImage((Bitmap) bitmap.get());
        }
        if (source != null) {
            int width = ((Bitmap) bitmap.get()).getWidth();
            int height = ((Bitmap) bitmap.get()).getHeight();
            GLTarget target = new GLTarget();
            ms = System.currentTimeMillis();
            target.loadWithSize(width, height);
            ms = System.currentTimeMillis();
            target.activate();
            ms = System.currentTimeMillis();
            if (context.renderForTexture(source.texture())) {
                ms = System.currentTimeMillis();
                callback.exec(target.targetTexture.readImage());
                ms = System.currentTimeMillis();
                target.unload();
                this.sourceTextures.put(key, source);
                return;
            }
            clear();
            callback.exec(null);
        }
    }

    public void applyStylesForBitmap(Bitmap bitmap, StyleEntry styleEntry, ActionCallback<Bitmap> callback, boolean async) {
        if (bitmap != null && styleEntry != null && styleEntry.getIsStyled() && callback != null) {
            Runnable block = new C09169(bitmap, styleEntry, async, callback);
            if (async) {
                dispatch_async(block);
            } else {
                dispatch_sync(block);
            }
        } else if (callback != null) {
            callback.exec(bitmap);
        }
    }

    private Bitmap applyCropForBitmap(Bitmap bitmap, float[] coords) {
        boolean z = true;
        if (bitmap == null || bitmap.isRecycled() || coords == null) {
            boolean z2;
            String str = "Cannot apply crop for bitmap. Info: {bn: %s, br: %s, cn: %s}";
            Object[] objArr = new Object[3];
            objArr[0] = Loggable.YN(bitmap == null);
            if (bitmap == null || !bitmap.isRecycled()) {
                z2 = false;
            } else {
                z2 = true;
            }
            objArr[CONTEXT_PRIVATE] = Loggable.YN(z2);
            if (coords != null) {
                z = false;
            }
            objArr[CONTEXT_PUBLIC] = Loggable.YN(z);
            Loggable.IPWarn(str, objArr);
            return bitmap;
        }
        int x = Math.max((int) coords[0], 0);
        int y = Math.max((int) coords[CONTEXT_PRIVATE], 0);
        int w = Math.min((int) (coords[CONTEXT_PUBLIC] - coords[0]), (int) (coords[4] - ((float) x)));
        int h = Math.min((int) (coords[3] - coords[CONTEXT_PRIVATE]), (int) (coords[5] - ((float) y)));
        if (w <= 0 || h <= 0) {
            str = "Something went wrong while cropping a bitmap: %s";
            Object[] objArr2 = new Object[CONTEXT_PRIVATE];
            String str2 = (w > 0 || h > 0) ? w <= 0 ? "w <= 0" : "h <= 0" : "w <= 0 && h <= 0";
            objArr2[0] = str2;
            Loggable.IPWarn(str, objArr2);
            return bitmap;
        }
        Bitmap result = Bitmap.createBitmap(bitmap, x, y, w, h);
        bitmap.recycle();
        return result;
    }

    private Bitmap applyClaheForBitmap(Bitmap bitmap) {
        if (bitmap == null || bitmap.isRecycled()) {
            String str = "Cannot apply CLAHE for bitmap: bitmap is %s";
            Object[] objArr = new Object[CONTEXT_PRIVATE];
            objArr[0] = bitmap == null ? "null" : "recycled";
            Loggable.IPWarn(str, objArr);
            return bitmap;
        }
        Bitmap result = CLAHE.process(bitmap);
        bitmap.recycle();
        return result;
    }

    private Bitmap applyFilterForBitmap(Bitmap bitmap, int filter, String key) {
        if (bitmap == null || filter <= 0) {
            String str = "Cannot apply filter for bitmap: %s";
            Object[] objArr = new Object[CONTEXT_PRIVATE];
            objArr[0] = bitmap == null ? "bitmap is null" : "filter id is normal or unknown: " + filter;
            Loggable.IPWarn(str, objArr);
            return bitmap;
        }
        bitmap.recycle();
        return null;
    }
}
