package com.vkontakte.android.mediapicker.gl;

public class GLAttrib {
    int location;
    String name;

    public GLAttrib(int location, String name) {
        this.location = location;
        this.name = name;
    }
}
