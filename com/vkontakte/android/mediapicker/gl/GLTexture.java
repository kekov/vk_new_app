package com.vkontakte.android.mediapicker.gl;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.opengl.GLES20;
import android.support.v4.view.MotionEventCompat;
import com.vkontakte.android.mediapicker.providers.LruCacheProvider;
import com.vkontakte.android.mediapicker.utils.Loggable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class GLTexture extends GLES20 {
    public static final int GLTextureFilter = 9728;
    public static final int GLTextureUnitTemp = 33984;
    boolean failed;
    int[] gl_texture;
    int height;
    int width;

    public GLTexture() {
        this.gl_texture = new int[1];
        this.failed = false;
    }

    public void setFailed() {
        this.failed = true;
    }

    public boolean getFailed() {
        return this.failed;
    }

    public int texture() {
        return this.gl_texture[0];
    }

    public int[] size() {
        return new int[]{this.width, this.height};
    }

    public Bitmap readImage() {
        try {
            String prefix = "-readImage: ";
            long ms = System.currentTimeMillis();
            ByteBuffer buffer = ByteBuffer.allocateDirect((this.width * this.height) * 4);
            buffer.order(ByteOrder.nativeOrder());
            buffer.position(0);
            ms = System.currentTimeMillis();
            glReadPixels(0, 0, this.width, this.height, 6408, 5121, buffer);
            ms = System.currentTimeMillis();
            Bitmap bitmap = Bitmap.createBitmap(this.width, this.height, Config.ARGB_8888);
            ms = System.currentTimeMillis();
            bitmap.copyPixelsFromBuffer(buffer);
            return bitmap;
        } catch (OutOfMemoryError outOfMemoryError) {
            System.gc();
            LruCacheProvider.instance().clear();
            Loggable.GLError("OutOfMemoryError while working with filtered image", outOfMemoryError, new Object[0]);
            return null;
        } catch (Throwable throwable) {
            Loggable.Error("Cannot read image in root", throwable, new Object[0]);
            return null;
        }
    }

    public boolean loadWithImage(Bitmap bitmap) {
        try {
            loadWithSize(bitmap.getWidth(), bitmap.getHeight(), bitmapAsByteBuffer(bitmap));
            return true;
        } catch (Throwable throwable) {
            Loggable.GLError("Cannot load with bitmap", throwable, new Object[0]);
            return false;
        }
    }

    public void loadWithSize(int width, int height) {
        loadWithSize(width, height, null);
    }

    public void loadWithSize(int width, int height, ByteBuffer data) {
        this.width = width;
        this.height = height;
        glActiveTexture(GLTextureUnitTemp);
        glGenTextures(1, this.gl_texture, 0);
        glBindTexture(3553, this.gl_texture[0]);
        glTexParameteri(3553, 10241, GLTextureFilter);
        glTexParameteri(3553, 10240, GLTextureFilter);
        glTexParameteri(3553, 10242, 33071);
        glTexParameteri(3553, 10243, 33071);
        glTexImage2D(3553, 0, 6408, width, height, 0, 6408, 5121, data);
    }

    public void unload() {
        if (this.gl_texture[0] != 0) {
            glDeleteTextures(1, this.gl_texture, 0);
            this.gl_texture = new int[1];
        }
    }

    static ByteBuffer bitmapAsByteBuffer(Bitmap bitmap) {
        int[] pixels = new int[(bitmap.getWidth() * bitmap.getHeight())];
        bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = (pixels[i] << 8) | MotionEventCompat.ACTION_MASK;
        }
        ByteBuffer buffer = ByteBuffer.allocateDirect((bitmap.getWidth() * bitmap.getHeight()) * 4);
        buffer.asIntBuffer().put(pixels);
        buffer.position(0);
        return buffer;
    }
}
