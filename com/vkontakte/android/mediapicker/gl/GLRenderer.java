package com.vkontakte.android.mediapicker.gl;

import android.opengl.GLES20;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.mediapicker.utils.DispatchQueue;
import com.vkontakte.android.mediapicker.utils.Loggable;
import java.util.concurrent.Semaphore;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GLRenderer {
    private static GLRenderer instance;
    private GLRenderBuffer buffer;
    private DispatchQueue queue;

    /* renamed from: com.vkontakte.android.mediapicker.gl.GLRenderer.1 */
    class C09051 implements Runnable {
        private final /* synthetic */ int val$height;
        private final /* synthetic */ int val$width;

        C09051(int i, int i2) {
            this.val$width = i;
            this.val$height = i2;
        }

        public void run() {
            GLRenderer.this.buffer = new GLRenderBuffer(this.val$width, this.val$height);
            GLRenderer.this.buffer.setRenderer(new Renderer());
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gl.GLRenderer.2 */
    class C09062 implements Runnable {
        private final /* synthetic */ int val$height;
        private final /* synthetic */ int val$width;

        C09062(int i, int i2) {
            this.val$width = i;
            this.val$height = i2;
        }

        public void run() {
            if (GLRenderer.this.buffer.width != this.val$width || GLRenderer.this.buffer.height != this.val$height) {
                GLRenderer.this.buffer.updateSurface(this.val$width, this.val$height);
            }
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gl.GLRenderer.3 */
    class C09073 implements Runnable {
        private final /* synthetic */ Runnable val$action;

        C09073(Runnable runnable) {
            this.val$action = runnable;
        }

        public void run() {
            GLRenderer.this.buffer.drawFrame(this.val$action);
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gl.GLRenderer.4 */
    class C09084 implements Runnable {
        private final /* synthetic */ Semaphore val$lock;
        private final /* synthetic */ Runnable val$runnable;

        C09084(Runnable runnable, Semaphore semaphore) {
            this.val$runnable = runnable;
            this.val$lock = semaphore;
        }

        public void run() {
            this.val$runnable.run();
            this.val$lock.release();
        }
    }

    /* renamed from: com.vkontakte.android.mediapicker.gl.GLRenderer.5 */
    class C09095 implements Runnable {
        private final /* synthetic */ Runnable val$action;

        C09095(Runnable runnable) {
            this.val$action = runnable;
        }

        public void run() {
            long ms = System.currentTimeMillis();
            GLRenderer.this.buffer.drawFrame(this.val$action);
        }
    }

    static class Renderer implements android.opengl.GLSurfaceView.Renderer {
        Renderer() {
        }

        public void onSurfaceCreated(GL10 unused, EGLConfig config) {
            GLES20.glClearColor(0.5f, 0.5f, 0.5f, 0.5f);
        }

        public void onDrawFrame(GL10 unused) {
            GLES20.glClear(NewsEntry.FLAG_EXPORT_FACEBOOK);
        }

        public void onSurfaceChanged(GL10 unused, int width, int height) {
            GLES20.glViewport(0, 0, width, height);
        }
    }

    static synchronized GLRenderer instance(int width, int height) {
        GLRenderer gLRenderer;
        synchronized (GLRenderer.class) {
            if (instance == null) {
                instance = new GLRenderer(width, height);
            } else {
                instance.updateSurface(width, height);
            }
            gLRenderer = instance;
        }
        return gLRenderer;
    }

    private GLRenderer(int width, int height) {
        this.queue = new DispatchQueue("ImagePickerGLThread");
        this.queue.start();
        invoke(new C09051(width, height));
    }

    private void updateSurface(int width, int height) {
        invoke(new C09062(width, height));
    }

    void invoke(Runnable runnable) {
        this.queue.postRunnable(runnable, 0);
    }

    void invoke_async(Runnable action, boolean unused) {
        invoke(new C09073(action));
    }

    void invoke_sync(Runnable runnable) {
        if (Thread.currentThread() == this.queue) {
            runnable.run();
            return;
        }
        Semaphore lock = new Semaphore(0);
        invoke(new C09084(runnable, lock));
        try {
            Loggable.ThreadBlock("GLRenderer, near 113", new Object[0]);
            lock.acquire();
        } catch (Throwable throwable) {
            Loggable.GLWarn("Cannot block %s for GLRenderer.invoke_sync, something bad may happen", throwable, Thread.currentThread().getName());
        }
    }

    void invoke_sync(Runnable action, boolean unused) {
        invoke_sync(new C09095(action));
    }
}
