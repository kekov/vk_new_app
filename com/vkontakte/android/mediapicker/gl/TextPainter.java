package com.vkontakte.android.mediapicker.gl;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.text.TextPaint;
import com.vkontakte.android.mediapicker.entries.StyleEntry;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.util.List;

public class TextPainter {
    private static final float TEXT_OFFSET_RATIO = 0.065f;
    private static final float TEXT_RATIO = 0.068f;
    private static final float TEXT_ROW_RATIO = 0.009f;
    private static final float TEXT_WIDTH_RATIO = 0.95f;
    private static GradientDrawable gradientDrawable;
    private static TextPaint lobsterPaint;

    private static TextPaint getLobsterPainter() {
        if (lobsterPaint == null) {
            Paint paint = new Paint(1);
            paint.setColor(-1);
            paint.setTextSize((float) ActivityClassProvider.dp(22.0f));
            paint.setTypeface(ActivityClassProvider.getLobsterTypeface());
            lobsterPaint = new TextPaint(paint);
        }
        return lobsterPaint;
    }

    private static GradientDrawable getGradientDrawable() {
        if (gradientDrawable == null) {
            Orientation orientation = Orientation.BOTTOM_TOP;
            int[] iArr = new int[2];
            iArr[0] = -1728053248;
            gradientDrawable = new GradientDrawable(orientation, iArr);
        }
        return gradientDrawable;
    }

    private static void drawLobsterParts(Canvas c, int width, int height, int x, int y, int left, int right, StyleEntry style) {
        float textSize = ((float) Math.min(width, height)) * TEXT_RATIO;
        float rowHeight = textSize + (((float) Math.min(width, height)) * TEXT_ROW_RATIO);
        getLobsterPainter().setTextSize(textSize);
        getLobsterPainter().setShadowLayer(2.8f, 1.4f, 1.8f, -637534208);
        List<String> words = style.splitLinesByWidth((int) (((float) width) * TEXT_WIDTH_RATIO), getLobsterPainter());
        getGradientDrawable().setBounds(left, (int) (((((float) y) - (((float) height) * TEXT_OFFSET_RATIO)) - (((float) words.size()) * rowHeight)) - (0.75f * rowHeight)), right, y);
        getGradientDrawable().draw(c);
        y -= (int) (((float) height) * TEXT_OFFSET_RATIO);
        for (int i = 0; i < words.size(); i++) {
            String word = (String) words.get(i);
            Canvas canvas = c;
            canvas.drawText(word, (float) (x - ((int) (getLobsterPainter().measureText(word) / ImageViewHolder.PaddingSize))), (float) ((int) (((float) y) - (((float) ((words.size() - 1) - i)) * rowHeight))), getLobsterPainter());
        }
    }

    public static void drawLobsterForBitmap(Canvas c, int width, int height, StyleEntry style) {
        if (style != null && style.getIsTexted()) {
            drawLobsterParts(c, width, height, (int) (((float) width) / ImageViewHolder.PaddingSize), height, 0, width, style);
        }
    }

    public static void drawLobster(Canvas c, int x, int y, int width, int height, int left, int right, StyleEntry style) {
        if (style != null && style.getIsTexted()) {
            drawLobsterParts(c, width, height, x, y, left, right, style);
        }
    }
}
