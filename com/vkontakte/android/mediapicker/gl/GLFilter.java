package com.vkontakte.android.mediapicker.gl;

import java.util.ArrayList;
import java.util.List;

public class GLFilter {
    String name;
    List<String> textures;

    public GLFilter(String[] data) {
        this.name = data[0].toLowerCase();
        this.textures = new ArrayList();
        if (data.length > 1) {
            for (int i = 1; i < data.length; i++) {
                this.textures.add(data[i]);
            }
        }
    }
}
