package com.vkontakte.android.mediapicker.gl;

import android.opengl.GLES20;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.mediapicker.providers.AssetsProvider;
import com.vkontakte.android.mediapicker.utils.Loggable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class GLFilterContext extends GLES20 {
    public static final String AttributeInputTextureCoordinate = "inputTextureCoordinate";
    public static final String AttributePosition = "position";
    public static final int GLAttributeInputTextureCoordinate = 1;
    public static final int GLAttributeInputTextureCoordinate2 = 2;
    public static final int GLAttributePosition = 0;
    public static final int GLFilterInputTextureUnit = 33986;
    public static final int[] GLFilterTextureUnit;
    public static final int GLTexturesMaximum = 5;
    public static final String[] GLUniformName;
    private static final float[] PositionCoordinate;
    private static final float[] TextureCoordinate;
    GLFilter filter;
    int gl_sl_u_input_texture;
    int[] gl_sl_u_texture;
    int[] gl_texture;
    GLProgram program;

    static {
        GLFilterTextureUnit = new int[]{33987, 33988, 33989, 33990, 33991};
        String[] strArr = new String[GLTexturesMaximum];
        strArr[GLAttributePosition] = "inputImageTexture2";
        strArr[GLAttributeInputTextureCoordinate] = "inputImageTexture3";
        strArr[GLAttributeInputTextureCoordinate2] = "inputImageTexture4";
        strArr[3] = "inputImageTexture5";
        strArr[4] = "inputImageTexture6";
        GLUniformName = strArr;
        PositionCoordinate = new float[]{GroundOverlayOptions.NO_DIMENSION, GroundOverlayOptions.NO_DIMENSION, 1.0f, GroundOverlayOptions.NO_DIMENSION, GroundOverlayOptions.NO_DIMENSION, 1.0f, 1.0f, 1.0f};
        TextureCoordinate = new float[]{0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f};
    }

    static int GLTextureUnitNumber(int texture) {
        return texture - GLTexture.GLTextureUnitTemp;
    }

    public GLFilterContext(GLFilter filter) {
        this.gl_texture = new int[GLTexturesMaximum];
        this.gl_sl_u_texture = new int[GLTexturesMaximum];
        this.filter = filter;
        if (filter.textures.size() > GLTexturesMaximum) {
            throw new IllegalArgumentException("Too many filter textures");
        }
    }

    public void unload() {
        if (this.program != null) {
            if (!this.filter.name.equals("diana")) {
                this.program.unload();
            }
            this.program = null;
        }
        for (int i = GLAttributePosition; i < GLTexturesMaximum; i += GLAttributeInputTextureCoordinate) {
            if (this.gl_texture[i] != 0) {
                int[] iArr = new int[GLAttributeInputTextureCoordinate];
                iArr[GLAttributePosition] = this.gl_texture[i];
                glDeleteTextures(GLAttributeInputTextureCoordinate, iArr, GLAttributePosition);
                this.gl_texture[i] = GLAttributePosition;
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.vkontakte.android.mediapicker.gl.GLFilterContext load() {
        /*
        r22 = this;
        r0 = r22;
        r1 = r0.program;
        if (r1 == 0) goto L_0x0007;
    L_0x0006:
        return r22;
    L_0x0007:
        r1 = new java.lang.StringBuilder;	 Catch:{ IOException -> 0x0076 }
        r2 = "Loading shader ";
        r1.<init>(r2);	 Catch:{ IOException -> 0x0076 }
        r0 = r22;
        r2 = r0.filter;	 Catch:{ IOException -> 0x0076 }
        r2 = r2.name;	 Catch:{ IOException -> 0x0076 }
        r1 = r1.append(r2);	 Catch:{ IOException -> 0x0076 }
        r1 = r1.toString();	 Catch:{ IOException -> 0x0076 }
        r2 = 0;
        r2 = new java.lang.Object[r2];	 Catch:{ IOException -> 0x0076 }
        com.vkontakte.android.mediapicker.utils.Loggable.GLInfo(r1, r2);	 Catch:{ IOException -> 0x0076 }
        r1 = "filter.vsh";
        r4 = com.vkontakte.android.mediapicker.gl.ResourceLoader.getCommonShader(r1);	 Catch:{ IOException -> 0x0076 }
        r0 = r22;
        r1 = r0.filter;	 Catch:{ IOException -> 0x0076 }
        r1 = r1.name;	 Catch:{ IOException -> 0x0076 }
        r5 = com.vkontakte.android.mediapicker.gl.ResourceLoader.getFilterShader(r1);	 Catch:{ IOException -> 0x0076 }
        r6 = new java.util.ArrayList;	 Catch:{ IOException -> 0x0076 }
        r6.<init>();	 Catch:{ IOException -> 0x0076 }
        r1 = new com.vkontakte.android.mediapicker.gl.GLAttrib;	 Catch:{ IOException -> 0x0076 }
        r2 = 0;
        r3 = "position";
        r1.<init>(r2, r3);	 Catch:{ IOException -> 0x0076 }
        r6.add(r1);	 Catch:{ IOException -> 0x0076 }
        r1 = new com.vkontakte.android.mediapicker.gl.GLAttrib;	 Catch:{ IOException -> 0x0076 }
        r2 = 1;
        r3 = "inputTextureCoordinate";
        r1.<init>(r2, r3);	 Catch:{ IOException -> 0x0076 }
        r6.add(r1);	 Catch:{ IOException -> 0x0076 }
        r1 = new com.vkontakte.android.mediapicker.gl.GLProgram;	 Catch:{ IOException -> 0x0076 }
        r1.<init>();	 Catch:{ IOException -> 0x0076 }
        r0 = r22;
        r0.program = r1;	 Catch:{ IOException -> 0x0076 }
        r0 = r22;
        r1 = r0.program;	 Catch:{ IOException -> 0x0076 }
        r2 = "filter.vsh";
        r0 = r22;
        r3 = r0.filter;	 Catch:{ IOException -> 0x0076 }
        r3 = r3.name;	 Catch:{ IOException -> 0x0076 }
        r1.loadVertexShader(r2, r3, r4, r5, r6);	 Catch:{ IOException -> 0x0076 }
        r0 = r22;
        r1 = r0.program;	 Catch:{ IOException -> 0x0076 }
        r1 = r1.gl_program;	 Catch:{ IOException -> 0x0076 }
        if (r1 != 0) goto L_0x0082;
    L_0x006d:
        r1 = "Error: gl_program == 0 ";
        r2 = 0;
        r2 = new java.lang.Object[r2];	 Catch:{ IOException -> 0x0076 }
        com.vkontakte.android.mediapicker.utils.Loggable.GLError(r1, r2);	 Catch:{ IOException -> 0x0076 }
        goto L_0x0006;
    L_0x0076:
        r19 = move-exception;
        r1 = "Error in GLFilterContext.load";
        r2 = 0;
        r2 = new java.lang.Object[r2];
        r0 = r19;
        com.vkontakte.android.mediapicker.utils.Loggable.GLError(r1, r0, r2);
        goto L_0x0006;
    L_0x0082:
        r0 = r22;
        r1 = r0.program;	 Catch:{ IOException -> 0x0076 }
        r2 = "inputImageTexture";
        r1 = r1.uniformLocation(r2);	 Catch:{ IOException -> 0x0076 }
        r0 = r22;
        r0.gl_sl_u_input_texture = r1;	 Catch:{ IOException -> 0x0076 }
        r1 = "gl_sl_u_input_texture = %d";
        r2 = 1;
        r2 = new java.lang.Object[r2];	 Catch:{ IOException -> 0x0076 }
        r3 = 0;
        r0 = r22;
        r7 = r0.gl_sl_u_input_texture;	 Catch:{ IOException -> 0x0076 }
        r7 = java.lang.Integer.valueOf(r7);	 Catch:{ IOException -> 0x0076 }
        r2[r3] = r7;	 Catch:{ IOException -> 0x0076 }
        com.vkontakte.android.mediapicker.utils.Loggable.GLInfo(r1, r2);	 Catch:{ IOException -> 0x0076 }
        r0 = r22;
        r1 = r0.filter;	 Catch:{ IOException -> 0x0076 }
        r1 = r1.name;	 Catch:{ IOException -> 0x0076 }
        r2 = "diana";
        r1 = r1.equals(r2);	 Catch:{ IOException -> 0x0076 }
        if (r1 == 0) goto L_0x00db;
    L_0x00b1:
        r1 = 16;
        r0 = new float[r1];	 Catch:{ IOException -> 0x0076 }
        r20 = r0;
        r20 = {1052226722, 1060393871, 1040979277, 0, 1050220167, 1058424226, 1038710997, 0, 1047851224, 1055944553, 1035650833, 0, 0, 0, 0, 1065353216};	 Catch:{ IOException -> 0x0076 }
        r0 = r22;
        r1 = r0.program;	 Catch:{ IOException -> 0x0076 }
        r2 = "intensity";
        r1 = r1.uniformLocation(r2);	 Catch:{ IOException -> 0x0076 }
        r2 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        glUniform1f(r1, r2);	 Catch:{ IOException -> 0x0076 }
        r0 = r22;
        r1 = r0.program;	 Catch:{ IOException -> 0x0076 }
        r2 = "colorMatrix";
        r1 = r1.uniformLocation(r2);	 Catch:{ IOException -> 0x0076 }
        r2 = 1;
        r3 = 0;
        r7 = 0;
        r0 = r20;
        glUniformMatrix4fv(r1, r2, r3, r0, r7);	 Catch:{ IOException -> 0x0076 }
    L_0x00db:
        r0 = r22;
        r1 = r0.filter;	 Catch:{ IOException -> 0x0076 }
        r1 = r1.textures;	 Catch:{ IOException -> 0x0076 }
        r1 = r1.size();	 Catch:{ IOException -> 0x0076 }
        if (r1 <= 0) goto L_0x00f7;
    L_0x00e7:
        r18 = 0;
    L_0x00e9:
        r0 = r22;
        r1 = r0.filter;	 Catch:{ Throwable -> 0x01e8 }
        r1 = r1.textures;	 Catch:{ Throwable -> 0x01e8 }
        r1 = r1.size();	 Catch:{ Throwable -> 0x01e8 }
        r0 = r18;
        if (r0 < r1) goto L_0x0101;
    L_0x00f7:
        r1 = "GLFilterContext.load completed";
        r2 = 0;
        r2 = new java.lang.Object[r2];	 Catch:{ IOException -> 0x0076 }
        com.vkontakte.android.mediapicker.utils.Loggable.GLInfo(r1, r2);	 Catch:{ IOException -> 0x0076 }
        goto L_0x0006;
    L_0x0101:
        r1 = 1;
        r0 = new int[r1];	 Catch:{ Throwable -> 0x01e8 }
        r17 = r0;
        r0 = r22;
        r1 = r0.filter;	 Catch:{ Throwable -> 0x01e8 }
        r1 = r1.textures;	 Catch:{ Throwable -> 0x01e8 }
        r0 = r18;
        r1 = r1.get(r0);	 Catch:{ Throwable -> 0x01e8 }
        r1 = (java.lang.String) r1;	 Catch:{ Throwable -> 0x01e8 }
        r0 = r22;
        r1 = r0.pathForTexture(r1);	 Catch:{ Throwable -> 0x01e8 }
        r16 = com.vkontakte.android.mediapicker.gl.ResourceLoader.getFilterTexture(r1);	 Catch:{ Throwable -> 0x01e8 }
        r1 = GLFilterTextureUnit;	 Catch:{ Throwable -> 0x01e8 }
        r1 = r1[r18];	 Catch:{ Throwable -> 0x01e8 }
        glActiveTexture(r1);	 Catch:{ Throwable -> 0x01e8 }
        r1 = 1;
        r2 = 0;
        r0 = r17;
        glGenTextures(r1, r0, r2);	 Catch:{ Throwable -> 0x01e8 }
        r0 = r22;
        r1 = r0.gl_texture;	 Catch:{ Throwable -> 0x01e8 }
        r2 = 0;
        r2 = r17[r2];	 Catch:{ Throwable -> 0x01e8 }
        r1[r18] = r2;	 Catch:{ Throwable -> 0x01e8 }
        r1 = 3553; // 0xde1 float:4.979E-42 double:1.7554E-320;
        r0 = r22;
        r2 = r0.gl_texture;	 Catch:{ Throwable -> 0x01e8 }
        r2 = r2[r18];	 Catch:{ Throwable -> 0x01e8 }
        glBindTexture(r1, r2);	 Catch:{ Throwable -> 0x01e8 }
        r1 = 3553; // 0xde1 float:4.979E-42 double:1.7554E-320;
        r2 = 10241; // 0x2801 float:1.435E-41 double:5.0597E-320;
        r3 = 9729; // 0x2601 float:1.3633E-41 double:4.807E-320;
        glTexParameteri(r1, r2, r3);	 Catch:{ Throwable -> 0x01e8 }
        r1 = 3553; // 0xde1 float:4.979E-42 double:1.7554E-320;
        r2 = 10240; // 0x2800 float:1.4349E-41 double:5.059E-320;
        r3 = 9729; // 0x2601 float:1.3633E-41 double:4.807E-320;
        glTexParameteri(r1, r2, r3);	 Catch:{ Throwable -> 0x01e8 }
        r1 = 3553; // 0xde1 float:4.979E-42 double:1.7554E-320;
        r2 = 10242; // 0x2802 float:1.4352E-41 double:5.06E-320;
        r3 = 33071; // 0x812f float:4.6342E-41 double:1.6339E-319;
        glTexParameteri(r1, r2, r3);	 Catch:{ Throwable -> 0x01e8 }
        r1 = 3553; // 0xde1 float:4.979E-42 double:1.7554E-320;
        r2 = 10243; // 0x2803 float:1.4354E-41 double:5.0607E-320;
        r3 = 33071; // 0x812f float:4.6342E-41 double:1.6339E-319;
        glTexParameteri(r1, r2, r3);	 Catch:{ Throwable -> 0x01e8 }
        r1 = "glParameteri";
        com.vkontakte.android.mediapicker.gl.ResourceLoader.checkGlError(r1);	 Catch:{ Throwable -> 0x01e8 }
        r15 = com.vkontakte.android.mediapicker.gl.GLTexture.bitmapAsByteBuffer(r16);	 Catch:{ Throwable -> 0x01e8 }
        r10 = r16.getWidth();	 Catch:{ Throwable -> 0x01e8 }
        r11 = r16.getHeight();	 Catch:{ Throwable -> 0x01e8 }
        r7 = 3553; // 0xde1 float:4.979E-42 double:1.7554E-320;
        r8 = 0;
        r9 = 6408; // 0x1908 float:8.98E-42 double:3.166E-320;
        r12 = 0;
        r13 = 6408; // 0x1908 float:8.98E-42 double:3.166E-320;
        r14 = 5121; // 0x1401 float:7.176E-42 double:2.53E-320;
        glTexImage2D(r7, r8, r9, r10, r11, r12, r13, r14, r15);	 Catch:{ Throwable -> 0x01e8 }
        r1 = new java.lang.StringBuilder;	 Catch:{ Throwable -> 0x01e8 }
        r0 = r22;
        r2 = r0.filter;	 Catch:{ Throwable -> 0x01e8 }
        r2 = r2.name;	 Catch:{ Throwable -> 0x01e8 }
        r2 = java.lang.String.valueOf(r2);	 Catch:{ Throwable -> 0x01e8 }
        r1.<init>(r2);	 Catch:{ Throwable -> 0x01e8 }
        r2 = ", glTexImage2d (";
        r1 = r1.append(r2);	 Catch:{ Throwable -> 0x01e8 }
        r0 = r18;
        r1 = r1.append(r0);	 Catch:{ Throwable -> 0x01e8 }
        r2 = ": ";
        r2 = r1.append(r2);	 Catch:{ Throwable -> 0x01e8 }
        r0 = r22;
        r1 = r0.filter;	 Catch:{ Throwable -> 0x01e8 }
        r1 = r1.textures;	 Catch:{ Throwable -> 0x01e8 }
        r0 = r18;
        r1 = r1.get(r0);	 Catch:{ Throwable -> 0x01e8 }
        r1 = (java.lang.String) r1;	 Catch:{ Throwable -> 0x01e8 }
        r1 = r2.append(r1);	 Catch:{ Throwable -> 0x01e8 }
        r2 = ") width: ";
        r1 = r1.append(r2);	 Catch:{ Throwable -> 0x01e8 }
        r1 = r1.append(r10);	 Catch:{ Throwable -> 0x01e8 }
        r2 = ", height: ";
        r1 = r1.append(r2);	 Catch:{ Throwable -> 0x01e8 }
        r1 = r1.append(r11);	 Catch:{ Throwable -> 0x01e8 }
        r1 = r1.toString();	 Catch:{ Throwable -> 0x01e8 }
        com.vkontakte.android.mediapicker.gl.ResourceLoader.checkGlError(r1);	 Catch:{ Throwable -> 0x01e8 }
        r0 = r22;
        r1 = r0.gl_sl_u_texture;	 Catch:{ Throwable -> 0x01e8 }
        r0 = r22;
        r2 = r0.program;	 Catch:{ Throwable -> 0x01e8 }
        r3 = GLUniformName;	 Catch:{ Throwable -> 0x01e8 }
        r3 = r3[r18];	 Catch:{ Throwable -> 0x01e8 }
        r2 = r2.uniformLocation(r3);	 Catch:{ Throwable -> 0x01e8 }
        r1[r18] = r2;	 Catch:{ Throwable -> 0x01e8 }
        r18 = r18 + 1;
        goto L_0x00e9;
    L_0x01e8:
        r21 = move-exception;
        r1 = "Error loading textures";
        r2 = 0;
        r2 = new java.lang.Object[r2];	 Catch:{ IOException -> 0x0076 }
        r0 = r21;
        com.vkontakte.android.mediapicker.utils.Loggable.GLError(r1, r0, r2);	 Catch:{ IOException -> 0x0076 }
        goto L_0x00f7;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.mediapicker.gl.GLFilterContext.load():com.vkontakte.android.mediapicker.gl.GLFilterContext");
    }

    String pathForTexture(String textureName) {
        String folderName = textureName.startsWith("filter_") ? "common" : this.filter.name;
        if (textureName.startsWith("filter_")) {
            textureName = textureName.substring(7, textureName.length());
        } else if (textureName.startsWith(new StringBuilder(String.valueOf(this.filter.name)).append("_").toString())) {
            textureName = textureName.substring(this.filter.name.length() + GLAttributeInputTextureCoordinate, textureName.length());
        }
        return AssetsProvider.getFiltersFolder() + folderName + "/" + textureName;
    }

    boolean renderForTexture(int sourceTexture) {
        if (this.program == null) {
            return false;
        }
        long ms = System.currentTimeMillis();
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(NewsEntry.FLAG_EXPORT_FACEBOOK);
        this.program.use();
        glActiveTexture(GLFilterInputTextureUnit);
        glBindTexture(3553, sourceTexture);
        glUniform1i(this.gl_sl_u_input_texture, GLTextureUnitNumber(GLFilterInputTextureUnit));
        int i = GLAttributePosition;
        while (i < GLTexturesMaximum && this.gl_texture[i] != 0) {
            glActiveTexture(GLFilterTextureUnit[i]);
            glBindTexture(3553, this.gl_texture[i]);
            glUniform1i(this.gl_sl_u_texture[i], GLTextureUnitNumber(GLFilterTextureUnit[i]));
            i += GLAttributeInputTextureCoordinate;
        }
        glVertexAttribPointer(GLAttributePosition, GLAttributeInputTextureCoordinate2, 5126, false, GLAttributePosition, asFloatBuffer(PositionCoordinate));
        glVertexAttribPointer(GLAttributeInputTextureCoordinate, GLAttributeInputTextureCoordinate2, 5126, false, GLAttributePosition, asFloatBuffer(TextureCoordinate));
        ResourceLoader.checkGlError("renderForTexture");
        glDrawArrays(GLTexturesMaximum, GLAttributePosition, 4);
        boolean resultValue = true;
        int error = glGetError();
        if (error == 1286 || error == 1282) {
            Object[] objArr = new Object[GLAttributeInputTextureCoordinate];
            objArr[GLAttributePosition] = Integer.valueOf(error);
            Loggable.GLError("Error operating while renderForTexture. Error %d returned", objArr);
            resultValue = false;
        }
        ResourceLoader.checkGlError("glDrawArrays");
        return resultValue;
    }

    private FloatBuffer asFloatBuffer(float[] floats) {
        FloatBuffer buffer = ByteBuffer.allocateDirect(floats.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        buffer.put(floats);
        buffer.position(GLAttributePosition);
        return buffer;
    }
}
