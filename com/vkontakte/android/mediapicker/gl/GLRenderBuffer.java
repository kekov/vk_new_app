package com.vkontakte.android.mediapicker.gl;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView.Renderer;
import com.vkontakte.android.mediapicker.utils.GalleryPickerUtils;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL10;
import org.acra.ACRAConstants;

public class GLRenderBuffer {
    private static final boolean DumpConfigs = false;
    private static final int EGL_CONTEXT_CLIENT_VERSION = 12440;
    private static final int EGL_OPENGL_ES2_BIT = 4;
    public static final int EGL_SURFACE_SIZE = 1024;
    private static final String LoggingTag = "photo_picker_gl";
    private boolean first_initialization;
    int height;
    private EGL10 mEGL;
    private EGLConfig mEGLConfig;
    private EGLConfig[] mEGLConfigs;
    private EGLContext mEGLContext;
    private EGLDisplay mEGLDisplay;
    private EGLSurface mEGLSurface;
    private GL10 mGL;
    Renderer renderer;
    private String threadOwnerName;
    int width;

    public GLRenderBuffer(int width, int height) {
        this.first_initialization = true;
        this.threadOwnerName = Thread.currentThread().getName();
        initContext(width, height);
    }

    public void initContext(int width, int height) {
        int[] version = new int[2];
        if (this.mEGL == null) {
            this.mEGL = (EGL10) EGLContext.getEGL();
            this.mEGLDisplay = this.mEGL.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            this.mEGL.eglInitialize(this.mEGLDisplay, version);
            this.mEGLConfig = chooseConfig();
        }
        init(width, height);
        ResourceLoader.checkGlError("Step after initialization");
    }

    public void updateSurface(int width, int height) {
        destroy();
        if (width > 0 && height > 0) {
            init(width, height);
        }
    }

    public void destroy() {
        this.mEGL.eglDestroySurface(this.mEGLDisplay, this.mEGLSurface);
        this.mEGL.eglDestroyContext(this.mEGLDisplay, this.mEGLContext);
    }

    private void init(int width, int height) {
        if (this.mEGLConfig == null) {
            GalleryPickerUtils.instance();
            GalleryPickerUtils.setForceFiltersDisabled(true);
            return;
        }
        this.width = width;
        this.height = height;
        int[] attribList = new int[]{12375, width, 12374, height, 12344};
        this.mEGLContext = this.mEGL.eglCreateContext(this.mEGLDisplay, this.mEGLConfig, EGL10.EGL_NO_CONTEXT, new int[]{EGL_CONTEXT_CLIENT_VERSION, 2, 12344});
        this.mEGLSurface = this.mEGL.eglCreatePbufferSurface(this.mEGLDisplay, this.mEGLConfig, attribList);
        this.mEGL.eglMakeCurrent(this.mEGLDisplay, this.mEGLSurface, this.mEGLSurface, this.mEGLContext);
        this.mGL = (GL10) this.mEGLContext.getGL();
        GLES20.glDisable(2929);
        if (ResourceLoader.checkGlError("glMakeCurrent") && this.first_initialization) {
            GalleryPickerUtils.instance();
            GalleryPickerUtils.setForceFiltersDisabled(true);
        }
        this.first_initialization = DumpConfigs;
    }

    public void setRenderer(Renderer renderer) {
        if (getThreadOwnerName().equals(Thread.currentThread().getName())) {
            this.renderer = renderer;
            this.renderer.onSurfaceCreated(this.mGL, this.mEGLConfig);
            this.renderer.onSurfaceChanged(this.mGL, this.width, this.height);
            return;
        }
        throw new IllegalAccessError("Current is not owning this GLRenderBuffer");
    }

    public void drawFrame(Runnable runnable) {
        if (this.renderer == null) {
            throw new IllegalStateException("Renderer was not set");
        } else if (!getThreadOwnerName().equals(Thread.currentThread().getName())) {
            throw new IllegalAccessError("Current thread cannot access this GLRenderBuffer");
        } else if (GalleryPickerUtils.getAreFiltersSupported()) {
            this.renderer.onDrawFrame(this.mGL);
            runnable.run();
        } else {
            runnable.run();
        }
    }

    private EGLConfig chooseConfig() {
        int[] attribList = new int[17];
        attribList[0] = 12339;
        attribList[1] = EGL_OPENGL_ES2_BIT;
        attribList[2] = 12352;
        attribList[3] = EGL_OPENGL_ES2_BIT;
        attribList[EGL_OPENGL_ES2_BIT] = 12326;
        attribList[6] = 12325;
        attribList[8] = 12324;
        attribList[9] = 8;
        attribList[10] = 12323;
        attribList[11] = 8;
        attribList[12] = 12322;
        attribList[13] = 8;
        attribList[14] = 12321;
        attribList[15] = 8;
        attribList[16] = 12344;
        int[] numConfig = new int[1];
        this.mEGL.eglChooseConfig(this.mEGLDisplay, attribList, null, 0, numConfig);
        int configSize = numConfig[0];
        this.mEGLConfigs = new EGLConfig[configSize];
        this.mEGL.eglChooseConfig(this.mEGLDisplay, attribList, this.mEGLConfigs, configSize, numConfig);
        dumpConfigs();
        if (this.mEGLConfigs.length > 0) {
            return this.mEGLConfigs[0];
        }
        return null;
    }

    private void dumpConfigs() {
    }

    private int getConfigAttrib(EGLConfig config, int attribute) {
        int[] value = new int[1];
        if (this.mEGL.eglGetConfigAttrib(this.mEGLDisplay, config, attribute, value)) {
            return value[0];
        }
        return 0;
    }

    public String getThreadOwnerName() {
        return this.threadOwnerName == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.threadOwnerName;
    }
}
