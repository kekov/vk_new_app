package com.vkontakte.android.mediapicker.gl;

import android.opengl.GLES20;
import java.util.List;

public class GLProgram extends GLES20 {
    static int gl_last_program;
    int gl_program;

    static void step(String err, Object... args) {
        ResourceLoader.checkGlError(String.format(err, args));
    }

    static int compileShader(int type, String shaderName, String src) {
        if (src == null || src.length() == 0) {
            throw new IllegalArgumentException("Empty shader SRC");
        }
        int gl_shader = glCreateShader(type);
        step("glCreateShader", new Object[0]);
        glShaderSource(gl_shader, src);
        step("glShaderSource", new Object[0]);
        glCompileShader(gl_shader);
        step("glCompileShader", new Object[0]);
        int[] status = new int[1];
        glGetShaderiv(gl_shader, 35713, status, 0);
        step("glGetShaderiv", new Object[0]);
        if (status[0] == 1) {
            return gl_shader;
        }
        glDeleteShader(gl_shader);
        ResourceLoader.checkGlError("glDeleteShader");
        return 0;
    }

    void loadVertexShader(String vshn, String fshn, String vsh, String fsh, List<GLAttrib> attributes) {
        if (this.gl_program != 0) {
            throw new IllegalStateException("gl_program is already inited");
        }
        int gl_vertex_shader = compileShader(35633, vshn, vsh);
        int gl_fragment_shader = compileShader(35632, fshn, fsh);
        if (!(gl_vertex_shader == 0 || gl_fragment_shader == 0)) {
            this.gl_program = glCreateProgram();
            step("glCreateProgram", new Object[0]);
            glAttachShader(this.gl_program, gl_vertex_shader);
            step("glAttachShader", new Object[0]);
            glAttachShader(this.gl_program, gl_fragment_shader);
            step("glAttachShader", new Object[0]);
            if (attributes != null) {
                for (GLAttrib attr : attributes) {
                    glBindAttribLocation(this.gl_program, attr.location, attr.name);
                    step("glBindAttribLocation %d %d %s", Integer.valueOf(this.gl_program), Integer.valueOf(attr.location), attr.name);
                }
            }
            glLinkProgram(this.gl_program);
            int[] status = new int[1];
            glGetProgramiv(this.gl_program, 35714, status, 0);
            if (status[0] != 1) {
                glDeleteProgram(this.gl_program);
                this.gl_program = 0;
                throw new IllegalAccessError("Program link error");
            }
            glUseProgram(this.gl_program);
            if (attributes != null) {
                for (GLAttrib attr2 : attributes) {
                    glEnableVertexAttribArray(attr2.location);
                    ResourceLoader.checkGlError("glEnableVertexAttribArray, location: " + attr2.location);
                }
            }
        }
        if (gl_vertex_shader != 0) {
            glDeleteShader(gl_vertex_shader);
        }
        if (gl_fragment_shader != 0) {
            glDeleteShader(gl_fragment_shader);
        }
    }

    void unload() {
        if (this.gl_program != 0) {
            glDeleteProgram(this.gl_program);
            ResourceLoader.checkGlError("glDeleteProgram");
            this.gl_program = 0;
            gl_last_program = 0;
        }
    }

    static {
        gl_last_program = 0;
    }

    void use() {
        if (this.gl_program != 0 && gl_last_program != this.gl_program) {
            gl_last_program = this.gl_program;
            glUseProgram(this.gl_program);
            ResourceLoader.checkGlError("glUseProgram");
        }
    }

    int uniformLocation(String name) {
        return this.gl_program == 0 ? 0 : glGetUniformLocation(this.gl_program, name);
    }
}
