package com.vkontakte.android.mediapicker.gl;

import android.graphics.Bitmap;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import com.vkontakte.android.mediapicker.utils.Loggable;

public class CLAHE {
    private static int bins;
    private static int block_radius;
    private static float slope;

    private static abstract class ByteGetter {

        /* renamed from: com.vkontakte.android.mediapicker.gl.CLAHE.ByteGetter.1 */
        class C15411 extends ByteGetter {
            private final /* synthetic */ int[] val$bytes;
            private final /* synthetic */ int val$height;

            C15411(int[] iArr, int i) {
                this.val$bytes = iArr;
                this.val$height = i;
                super();
            }

            public int get(int x, int y) {
                return this.val$bytes[(this.val$height * y) + x];
            }
        }

        public abstract int get(int i, int i2);

        private ByteGetter() {
        }

        public static ByteGetter wrap(int[] bytes, int height) {
            return new C15411(bytes, height);
        }
    }

    private interface PixelSetter {
        void set(int i, int i2, int i3);
    }

    static {
        block_radius = 63;
        bins = MotionEventCompat.ACTION_MASK;
        slope = 3.0f;
    }

    private static int roundPositive(float x) {
        return (int) (0.5f + x);
    }

    public static Bitmap process(Bitmap image) {
        return process(image, block_radius, bins, slope);
    }

    public static Bitmap process(Bitmap image, int blockRadius, int bins, float slope) {
        if (image == null) {
            Loggable.IPWarn("CLAHE error: image is null", new Object[0]);
            return null;
        }
        try {
            long ms = System.currentTimeMillis();
            Bitmap result = doProcess(image, blockRadius, bins, slope);
            Loggable.IPInfo("CLAHE done in %dms", Integer.valueOf((int) (System.currentTimeMillis() - ms)));
            return result;
        } catch (Throwable throwable) {
            Loggable.Error("Cannot CLAHE", throwable, new Object[0]);
            return null;
        }
    }

    private static int[] getPixels(int width, int height, Bitmap image) {
        int[] pixels = new int[(image.getWidth() * image.getHeight())];
        image.getPixels(pixels, 0, width, 0, 0, width, height);
        return pixels;
    }

    private static int[] getBytePixels(int width, int height, Bitmap image) {
        int[] pixels24 = getPixels(width, height, image);
        int[] pixels8 = new int[(width * height)];
        for (int i = 0; i < width * height; i++) {
            int c = pixels24[i] & 16777215;
            pixels8[i] = ((byte) ((int) ((((((double) ((16711680 & c) >> 16)) * 0.3333333333333333d) + (((double) ((MotionEventCompat.ACTION_POINTER_INDEX_MASK & c) >> 8)) * 0.3333333333333333d)) + (((double) (c & MotionEventCompat.ACTION_MASK)) * 0.3333333333333333d)) + 0.5d))) & MotionEventCompat.ACTION_MASK;
        }
        return pixels8;
    }

    private static Bitmap doProcess(Bitmap image, int blockRadius, int bins, float slope) throws Throwable {
        long ms = System.currentTimeMillis();
        int width = image.getWidth();
        int height = image.getHeight();
        ByteGetter getter = ByteGetter.wrap(getBytePixels(width, height, image), height);
        Object[] objArr = new Object[1];
        objArr[0] = Integer.valueOf((int) (System.currentTimeMillis() - ms));
        Loggable.Error("Done initialization in %dms, starting to do my job", objArr);
        ms = System.currentTimeMillis();
        String msg = " done of " + height;
        for (int y = 0; y < height; y++) {
            int yi;
            Log.e(Loggable.COMMON_TAG, new StringBuilder(String.valueOf(y)).append(msg).toString());
            int minY = Math.max(0, y - blockRadius);
            int maxY = Math.min(height, (y + blockRadius) + 1);
            int h = maxY - minY;
            int minX0 = Math.max(0, -blockRadius);
            int maxX0 = Math.min(width - 1, blockRadius);
            int[] hist = new int[(bins + 1)];
            int[] clipped = new int[(bins + 1)];
            for (yi = minY; yi < maxY; yi++) {
                for (int xi = minX0; xi < maxX0; xi++) {
                    float f = (float) bins;
                    int roundPositive = roundPositive((((float) getter.get(xi, yi)) / 255.0f) * r0);
                    hist[roundPositive] = hist[roundPositive] + 1;
                }
            }
            for (int x = 0; x < width; x++) {
                f = (float) bins;
                int v = roundPositive((((float) getter.get(x, y)) / 255.0f) * r0);
                int minX = Math.max(0, x - blockRadius);
                int maxX = Math.min(width, x + blockRadius);
                f = (float) bins;
                int limit = (int) (((((float) (h * (Math.min(width, maxX) - minX))) * slope) / r0) + 0.5f);
                if (minX > 0) {
                    int minX1 = minX - 1;
                    for (yi = minY; yi < maxY; yi++) {
                        f = (float) bins;
                        roundPositive = roundPositive((((float) getter.get(minX1, yi)) / 255.0f) * r0);
                        hist[roundPositive] = hist[roundPositive] - 1;
                    }
                }
                if (maxX <= width) {
                    int maxX1 = maxX - 1;
                    for (yi = minY; yi < maxY; yi++) {
                        f = (float) bins;
                        roundPositive = roundPositive((((float) getter.get(maxX1, yi)) / 255.0f) * r0);
                        hist[roundPositive] = hist[roundPositive] + 1;
                    }
                }
            }
        }
        objArr = new Object[1];
        objArr[0] = Integer.valueOf((int) (System.currentTimeMillis() - ms));
        Loggable.Error("Done in %dms", objArr);
        return null;
    }
}
