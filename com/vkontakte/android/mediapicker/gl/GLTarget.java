package com.vkontakte.android.mediapicker.gl;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import com.vkontakte.android.mediapicker.utils.Loggable;

public class GLTarget extends GLES20 {
    int[] gl_framebuffer;
    int height;
    GLTexture targetTexture;
    int width;

    public GLTarget() {
        this.gl_framebuffer = new int[1];
    }

    public GLTexture texture() {
        return this.targetTexture;
    }

    public void clear() {
    }

    public boolean check() {
        int status = glCheckFramebufferStatus(this.gl_framebuffer[0]);
        Loggable.GLError("======== FRAMEBUFFER %d STATUS %d", Integer.valueOf(this.gl_framebuffer[0]), Integer.valueOf(status));
        return true;
    }

    public void unload() {
        if (this.gl_framebuffer[0] != 0) {
            glDeleteFramebuffers(1, this.gl_framebuffer, 0);
            this.gl_framebuffer = new int[1];
        }
    }

    public void loadWithSize(int width, int height) {
        this.width = width;
        this.height = height;
        this.targetTexture = new GLTexture();
        this.targetTexture.loadWithSize(width, height);
        glGenFramebuffers(1, this.gl_framebuffer, 0);
    }

    public boolean loadWithImage(Bitmap bitmap) {
        this.width = bitmap.getWidth();
        this.height = bitmap.getHeight();
        Loggable.GLError("Width: %d, Height: %d", Integer.valueOf(this.width), Integer.valueOf(this.height));
        glGenFramebuffers(1, this.gl_framebuffer, 0);
        GLTexture texture = new GLTexture();
        this.targetTexture = texture;
        texture.loadWithImage(bitmap);
        activate();
        return true;
    }

    public void activate() {
        glBindFramebuffer(36160, this.gl_framebuffer[0]);
        glFramebufferTexture2D(36160, 36064, 3553, this.targetTexture.texture(), 0);
        glViewport(0, 0, this.width, this.height);
    }
}
