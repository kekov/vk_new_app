package com.vkontakte.android.mediapicker.gl;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.opengl.GLES20;
import com.vkontakte.android.mediapicker.providers.AssetsProvider;
import com.vkontakte.android.mediapicker.providers.ResourceProvider;
import com.vkontakte.android.mediapicker.utils.Loggable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import org.acra.ACRAConstants;

public class ResourceLoader {
    private static String streamToString(InputStream stream) throws IOException {
        Scanner scanner = new Scanner(stream, "UTF-8").useDelimiter("\\A");
        String result = scanner.hasNext() ? scanner.next() : null;
        stream.close();
        return result;
    }

    static String getCommonShader(String fileName) throws IOException {
        return streamToString(ResourceProvider.getApplicationContext().getAssets().open(AssetsProvider.getFiltersFolder() + "common/" + fileName));
    }

    static String getFilterShader(String filterName) throws IOException {
        String str;
        AssetManager assets = ResourceProvider.getApplicationContext().getAssets();
        StringBuilder stringBuilder = new StringBuilder(String.valueOf(AssetsProvider.getFiltersFolder()));
        if (filterName.equals("normal")) {
            str = "common";
        } else {
            str = filterName;
        }
        return streamToString(assets.open(stringBuilder.append(str).append("/").append(filterName).append(".fsh").toString()));
    }

    static Bitmap getFilterTexture(String path) throws IOException {
        return BitmapFactory.decodeStream(ResourceProvider.getApplicationContext().getAssets().open(path));
    }

    static Bitmap getMutableBitmap(byte[] data) {
        new Options().inMutable = true;
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }

    static boolean checkGlError(String error) {
        int count = 0;
        while (GLES20.glGetError() != 0) {
            Loggable.GLError("%s - glError %d %016X", error, Integer.valueOf(count), Integer.valueOf(GLES20.glGetError()));
            count++;
            if (count >= 20) {
                Loggable.GLError("%s - too much errors", error);
                break;
            }
        }
        if (count == 0) {
            Loggable.GLVerbose("%s - no errors", error);
        } else {
            String str = "%s - %s error%s";
            Object[] objArr = new Object[3];
            objArr[0] = error;
            objArr[1] = count == 1 ? "one" : new StringBuilder(String.valueOf(count)).toString();
            objArr[2] = count == 1 ? ACRAConstants.DEFAULT_STRING_VALUE : "s";
            Loggable.GLWarn(str, objArr);
        }
        if (count > 0) {
            return true;
        }
        return false;
    }
}
