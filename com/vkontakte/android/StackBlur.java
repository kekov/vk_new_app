package com.vkontakte.android;

import android.graphics.Bitmap;
import android.support.v4.view.MotionEventCompat;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.providers.ResourceProvider.Views;

public class StackBlur {
    private static final int[] mul_table;
    private static final int[] shg_table;

    private static class BlurStack {
        int f47b;
        int f48g;
        BlurStack next;
        int f49r;

        private BlurStack() {
        }
    }

    static {
        mul_table = new int[]{NewsEntry.FLAG_FRIENDS_ONLY, NewsEntry.FLAG_FRIENDS_ONLY, 456, NewsEntry.FLAG_FRIENDS_ONLY, 328, 456, 335, NewsEntry.FLAG_FRIENDS_ONLY, 405, 328, 271, 456, 388, 335, Views.COMPLETE_BUTTON_BIG, NewsEntry.FLAG_FRIENDS_ONLY, 454, 405, 364, 328, 298, 271, 496, 456, 420, 388, 360, 335, 312, Views.COMPLETE_BUTTON_BIG, 273, NewsEntry.FLAG_FRIENDS_ONLY, 482, 454, 428, 405, 383, 364, 345, 328, 312, 298, 284, 271, 259, 496, 475, 456, 437, 420, 404, 388, 374, 360, 347, 335, Views.ACTION_ENHANCE, 312, 302, Views.COMPLETE_BUTTON_BIG, 282, 273, 265, NewsEntry.FLAG_FRIENDS_ONLY, 497, 482, 468, 454, 441, 428, 417, 405, 394, 383, 373, 364, 354, 345, 337, 328, Views.ACTION_FILTER, 312, Views.DIVIDER_VIEW_LEFT, 298, Views.CANCEL_BUTTON_BIG, 284, 278, 271, 265, 259, 507, 496, 485, 475, 465, 456, 446, 437, 428, 420, 412, 404, 396, 388, 381, 374, 367, 360, 354, 347, 341, 335, 329, Views.ACTION_ENHANCE, 318, 312, 307, 302, 297, Views.COMPLETE_BUTTON_BIG, 287, 282, 278, 273, 269, 265, 261, NewsEntry.FLAG_FRIENDS_ONLY, 505, 497, 489, 482, 475, 468, 461, 454, 447, 441, 435, 428, 422, 417, 411, 405, 399, 394, 389, 383, 378, 373, 368, 364, 359, 354, 350, 345, 341, 337, 332, 328, 324, Views.ACTION_FILTER, 316, 312, 309, Views.DIVIDER_VIEW_LEFT, 301, 298, 294, Views.CANCEL_BUTTON_BIG, 287, 284, 281, 278, 274, 271, 268, 265, 262, 259, Views.FRAGMENT_VIEW, 507, GCMBroadcastReceiver.ID_FRIEND_NOTIFICATION, 496, 491, 485, 480, 475, 470, 465, 460, 456, 451, 446, 442, 437, 433, 428, 424, 420, 416, 412, 408, 404, 400, 396, 392, 388, 385, 381, 377, 374, 370, 367, 363, 360, 357, 354, 350, 347, 344, 341, 338, 335, 332, 329, 326, Views.ACTION_ENHANCE, Views.ACTION_FILTER, 318, 315, 312, 310, 307, Views.DIVIDER_VIEW, 302, 299, 297, 294, Views.COMPLETE_BUTTON_BIG, Views.COMPLETE_BUTTON, 287, 285, 282, 280, 278, 275, 273, 271, 269, 267, 265, 263, 261, 259};
        shg_table = new int[]{9, 11, 12, 13, 13, 14, 14, 15, 15, 15, 15, 16, 16, 16, 16, 17, 17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 18, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24};
    }

    public static void blurBitmap(Bitmap bitmap, int radius) {
        if (radius >= 1) {
            int y;
            int b_in_sum;
            int g_in_sum;
            int r_in_sum;
            int pr;
            int r_out_sum;
            int pg;
            int g_out_sum;
            int pb;
            int b_out_sum;
            int r_sum;
            int g_sum;
            int b_sum;
            int i;
            int p;
            int rbs;
            BlurStack stackIn;
            BlurStack stackOut;
            int x;
            radius |= 0;
            long time = System.currentTimeMillis();
            int[] pixels = new int[(bitmap.getWidth() * bitmap.getHeight())];
            bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            int div = (radius + radius) + 1;
            int w4 = width << 2;
            int widthMinus1 = width - 1;
            int heightMinus1 = height - 1;
            int radiusPlus1 = radius + 1;
            int sumFactor = ((radiusPlus1 + 1) * radiusPlus1) / 2;
            BlurStack blurStack = new BlurStack();
            BlurStack stackEnd = null;
            int i2 = 1;
            BlurStack stack = blurStack;
            while (i2 < div) {
                blurStack = new BlurStack();
                stack.next = blurStack;
                if (i2 == radiusPlus1) {
                    stackEnd = blurStack;
                }
                i2++;
                stack = blurStack;
            }
            stack.next = blurStack;
            int yi = 0;
            int yw = 0;
            int mul_sum = mul_table[radius];
            int shg_sum = shg_table[radius];
            BlurStack blurStack2 = stack;
            for (y = 0; y < height; y++) {
                b_in_sum = 0;
                g_in_sum = 0;
                r_in_sum = 0;
                pr = pixels[yi] & MotionEventCompat.ACTION_MASK;
                r_out_sum = radiusPlus1 * pr;
                pg = (pixels[yi] >> 8) & MotionEventCompat.ACTION_MASK;
                g_out_sum = radiusPlus1 * pg;
                pb = (pixels[yi] >> 16) & MotionEventCompat.ACTION_MASK;
                b_out_sum = radiusPlus1 * pb;
                r_sum = 0 + (sumFactor * pr);
                g_sum = 0 + (sumFactor * pg);
                b_sum = 0 + (sumFactor * pb);
                blurStack2 = blurStack;
                for (i2 = 0; i2 < radiusPlus1; i2++) {
                    blurStack2.f49r = pr;
                    blurStack2.f48g = pg;
                    blurStack2.f47b = pb;
                    blurStack2 = blurStack2.next;
                }
                for (i2 = 1; i2 < radiusPlus1; i2++) {
                    if (widthMinus1 < i2) {
                        i = widthMinus1;
                    } else {
                        i = i2;
                    }
                    p = yi + i;
                    pr = pixels[p] & MotionEventCompat.ACTION_MASK;
                    blurStack2.f49r = pr;
                    rbs = radiusPlus1 - i2;
                    r_sum += pr * rbs;
                    pg = (pixels[p] >> 8) & MotionEventCompat.ACTION_MASK;
                    blurStack2.f48g = pg;
                    g_sum += pg * rbs;
                    pb = (pixels[p] >> 16) & MotionEventCompat.ACTION_MASK;
                    blurStack2.f47b = pb;
                    b_sum += pb * rbs;
                    r_in_sum += pr;
                    g_in_sum += pg;
                    b_in_sum += pb;
                    blurStack2 = blurStack2.next;
                }
                stackIn = blurStack;
                stackOut = stackEnd;
                for (x = 0; x < width; x++) {
                    pixels[yi] = ((((r_sum * mul_sum) >> shg_sum) | (((g_sum * mul_sum) >> shg_sum) << 8)) | (((b_sum * mul_sum) >> shg_sum) << 16)) | Color.WINDOW_BACKGROUND_COLOR;
                    r_sum -= r_out_sum;
                    g_sum -= g_out_sum;
                    b_sum -= b_out_sum;
                    r_out_sum -= stackIn.f49r;
                    g_out_sum -= stackIn.f48g;
                    b_out_sum -= stackIn.f47b;
                    p = (x + radius) + 1;
                    if (p >= widthMinus1) {
                        p = widthMinus1;
                    }
                    p += yw;
                    i = pixels[p] & MotionEventCompat.ACTION_MASK;
                    stackIn.f49r = i;
                    r_in_sum += i;
                    i = (pixels[p] >> 8) & MotionEventCompat.ACTION_MASK;
                    stackIn.f48g = i;
                    g_in_sum += i;
                    i = (pixels[p] >> 16) & MotionEventCompat.ACTION_MASK;
                    stackIn.f47b = i;
                    b_in_sum += i;
                    r_sum += r_in_sum;
                    g_sum += g_in_sum;
                    b_sum += b_in_sum;
                    stackIn = stackIn.next;
                    pr = stackOut.f49r;
                    r_out_sum += pr;
                    pg = stackOut.f48g;
                    g_out_sum += pg;
                    pb = stackOut.f47b;
                    b_out_sum += pb;
                    r_in_sum -= pr;
                    g_in_sum -= pg;
                    b_in_sum -= pb;
                    stackOut = stackOut.next;
                    yi++;
                }
                yw += width;
            }
            for (x = 0; x < width; x++) {
                r_in_sum = 0;
                b_in_sum = 0;
                g_in_sum = 0;
                yi = x;
                pr = pixels[yi] & MotionEventCompat.ACTION_MASK;
                r_out_sum = radiusPlus1 * pr;
                pg = (pixels[yi] >> 8) & MotionEventCompat.ACTION_MASK;
                g_out_sum = radiusPlus1 * pg;
                pb = (pixels[yi] >> 16) & MotionEventCompat.ACTION_MASK;
                b_out_sum = radiusPlus1 * pb;
                r_sum = 0 + (sumFactor * pr);
                g_sum = 0 + (sumFactor * pg);
                b_sum = 0 + (sumFactor * pb);
                blurStack2 = blurStack;
                for (i2 = 0; i2 < radiusPlus1; i2++) {
                    blurStack2.f49r = pr;
                    blurStack2.f48g = pg;
                    blurStack2.f47b = pb;
                    blurStack2 = blurStack2.next;
                }
                int yp = width;
                for (i2 = 1; i2 <= radius; i2++) {
                    yi = yp + x;
                    pr = pixels[yi] & MotionEventCompat.ACTION_MASK;
                    blurStack2.f49r = pr;
                    rbs = radiusPlus1 - i2;
                    r_sum += pr * rbs;
                    pg = (pixels[yi] >> 8) & MotionEventCompat.ACTION_MASK;
                    blurStack2.f48g = pg;
                    g_sum += pg * rbs;
                    pb = (pixels[yi] >> 16) & MotionEventCompat.ACTION_MASK;
                    blurStack2.f47b = pb;
                    b_sum += pb * rbs;
                    r_in_sum += pr;
                    g_in_sum += pg;
                    b_in_sum += pb;
                    blurStack2 = blurStack2.next;
                    if (i2 < heightMinus1) {
                        yp += width;
                    }
                }
                yi = x;
                stackIn = blurStack;
                stackOut = stackEnd;
                for (y = 0; y < height; y++) {
                    pixels[yi] = ((((r_sum * mul_sum) >> shg_sum) | (((g_sum * mul_sum) >> shg_sum) << 8)) | (((b_sum * mul_sum) >> shg_sum) << 16)) | Color.WINDOW_BACKGROUND_COLOR;
                    r_sum -= r_out_sum;
                    g_sum -= g_out_sum;
                    b_sum -= b_out_sum;
                    r_out_sum -= stackIn.f49r;
                    g_out_sum -= stackIn.f48g;
                    b_out_sum -= stackIn.f47b;
                    p = y + radiusPlus1;
                    if (p >= heightMinus1) {
                        p = heightMinus1;
                    }
                    p = x + (p * width);
                    i = pixels[p] & MotionEventCompat.ACTION_MASK;
                    stackIn.f49r = i;
                    r_in_sum += i;
                    r_sum += r_in_sum;
                    i = (pixels[p] >> 8) & MotionEventCompat.ACTION_MASK;
                    stackIn.f48g = i;
                    g_in_sum += i;
                    g_sum += g_in_sum;
                    i = (pixels[p] >> 16) & MotionEventCompat.ACTION_MASK;
                    stackIn.f47b = i;
                    b_in_sum += i;
                    b_sum += b_in_sum;
                    stackIn = stackIn.next;
                    pr = stackOut.f49r;
                    r_out_sum += pr;
                    pg = stackOut.f48g;
                    g_out_sum += pg;
                    pb = stackOut.f47b;
                    b_out_sum += pb;
                    r_in_sum -= pr;
                    g_in_sum -= pg;
                    b_in_sum -= pb;
                    stackOut = stackOut.next;
                    yi += width;
                }
            }
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
            Log.m528i("vk", "StackBlur time " + (System.currentTimeMillis() - time));
        }
    }
}
