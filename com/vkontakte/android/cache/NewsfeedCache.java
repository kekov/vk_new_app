package com.vkontakte.android.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Log;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.NewsfeedList;
import com.vkontakte.android.VKApplication;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;

public class NewsfeedCache {
    private static Semaphore semaphore;

    /* renamed from: com.vkontakte.android.cache.NewsfeedCache.1 */
    class C05591 implements Runnable {
        private final /* synthetic */ Context val$context;
        private final /* synthetic */ List val$items;

        C05591(Context context, List list) {
            this.val$context = context;
            this.val$items = list;
        }

        public void run() {
            try {
                NewsfeedCache.semaphore.acquire();
                CacheOpenHelper helper = new CacheOpenHelper(this.val$context);
                SQLiteDatabase db = helper.getWritableDatabase();
                db.beginTransaction();
                try {
                    db.delete("news", null, null);
                    for (NewsEntry entry : this.val$items) {
                        entry.writeToSQLite(db, "news");
                    }
                    db.setTransactionSuccessful();
                } catch (Exception x) {
                    Log.m527e("vk", "Error writing news cache DB!", x);
                }
                db.endTransaction();
                db.close();
                helper.close();
                NewsfeedCache.setUpdateTime(this.val$context, (int) (System.currentTimeMillis() / 1000));
            } catch (Exception e) {
            }
            NewsfeedCache.semaphore.release();
        }
    }

    private static class CacheOpenHelper extends SQLiteOpenHelper {
        public CacheOpenHelper(Context context) {
            super(context, "posts.db", null, 11);
        }

        public void onCreate(SQLiteDatabase db) {
            CacheTables.createPosts(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
        }
    }

    static {
        semaphore = new Semaphore(1);
    }

    public static ArrayList<NewsEntry> get(Context context) {
        try {
            semaphore.acquire();
            CacheOpenHelper helper = new CacheOpenHelper(context);
            SQLiteDatabase db = helper.getReadableDatabase();
            ArrayList<NewsEntry> result = new ArrayList();
            try {
                Cursor cursor = db.query("news", null, null, null, null, null, "`time` desc");
                if (cursor != null && cursor.getCount() > 0) {
                    int i = 0;
                    cursor.moveToFirst();
                    do {
                        NewsEntry entry = new NewsEntry();
                        entry.readFromSQLite(cursor, context);
                        result.add(entry);
                        i++;
                    } while (cursor.moveToNext());
                }
            } catch (Exception x) {
                Log.m527e("vk", "Error reading news cache DB!", x);
            }
            db.close();
            helper.close();
            semaphore.release();
            return result;
        } catch (Exception e) {
            semaphore.release();
            return null;
        }
    }

    public static NewsEntry[] getPostsOnly(Context context) {
        try {
            CacheOpenHelper helper = new CacheOpenHelper(context);
            SQLiteDatabase db = helper.getReadableDatabase();
            NewsEntry[] result = new NewsEntry[0];
            try {
                Cursor cursor = db.query("news", null, "`flags`<16777216", null, null, null, "`time` desc");
                if (cursor != null && cursor.getCount() > 0) {
                    result = new NewsEntry[cursor.getCount()];
                    int i = 0;
                    cursor.moveToFirst();
                    do {
                        NewsEntry entry = new NewsEntry();
                        entry.readFromSQLite(cursor, context);
                        result[i] = entry;
                        i++;
                    } while (cursor.moveToNext());
                }
            } catch (Exception x) {
                Log.m527e("vk", "Error reading news cache DB!", x);
            }
            db.close();
            helper.close();
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    public static void replace(List<NewsEntry> items, Context context) {
        new Thread(new C05591(context, items)).start();
    }

    public static void add(NewsEntry entry, Context context) {
        try {
            CacheOpenHelper helper = new CacheOpenHelper(context);
            SQLiteDatabase db = helper.getWritableDatabase();
            try {
                entry.writeToSQLite(db, "news");
            } catch (Exception x) {
                Log.m527e("vk", "Error writing news cache DB!", x);
            }
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static void remove(int oid, int pid, Context context) {
        try {
            semaphore.acquire();
            CacheOpenHelper helper = new CacheOpenHelper(context);
            SQLiteDatabase db = helper.getWritableDatabase();
            try {
                db.delete("news", "`pid`=" + pid + " AND `uid`=" + oid, null);
            } catch (Exception x) {
                Log.m527e("vk", "Error writing news cache DB!", x);
            }
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        semaphore.release();
    }

    public static void update(Context context, int oid, int pid, int likes, int comments, int retweets, boolean liked, boolean retweeted) {
        try {
            CacheOpenHelper helper = new CacheOpenHelper(context);
            SQLiteDatabase db = helper.getWritableDatabase();
            int flags = 0;
            try {
                Cursor cursor = db.query("news", new String[]{"flags"}, "`pid`=" + pid + " AND `uid`=" + oid, null, null, null, null);
                if (cursor == null || cursor.getCount() <= 0) {
                    cursor.close();
                    cursor = db.query("news_comments", new String[]{"flags"}, "`pid`=" + pid + " AND `uid`=" + oid, null, null, null, null);
                } else {
                    cursor.moveToFirst();
                    flags = cursor.getInt(0);
                    cursor.close();
                }
                if (flags != 0 || cursor == null || cursor.getCount() <= 0) {
                    cursor.close();
                    cursor = db.query("wall", new String[]{"flags"}, "`pid`=" + pid + " AND `uid`=" + oid, null, null, null, null);
                } else {
                    cursor.moveToFirst();
                    flags = cursor.getInt(0);
                    cursor.close();
                }
                if (flags != 0 || cursor == null || cursor.getCount() <= 0) {
                    cursor.close();
                    db.close();
                    helper.close();
                    return;
                }
                cursor.moveToFirst();
                flags = cursor.getInt(0);
                cursor.close();
                ContentValues values = new ContentValues();
                values.put("likes", Integer.valueOf(likes));
                values.put("comments", Integer.valueOf(comments));
                if (liked) {
                    flags |= 8;
                } else {
                    flags &= -9;
                }
                if (retweeted) {
                    flags |= 4;
                } else {
                    flags &= -5;
                }
                values.put("flags", Integer.valueOf(flags));
                db.update("news", values, "`pid`=" + pid + " AND `uid`=" + oid, null);
                db.update("news_comments", values, "`pid`=" + pid + " AND `uid`=" + oid, null);
                db.update("wall", values, "`pid`=" + pid + " AND `uid`=" + oid, null);
                db.close();
                helper.close();
            } catch (Exception x) {
                Log.m527e("vk", "Error writing news cache DB!", x);
            }
        } catch (Exception e) {
        }
    }

    public static ArrayList<NewsfeedList> getLists() {
        ArrayList<NewsfeedList> result = new ArrayList();
        try {
            CacheOpenHelper helper = new CacheOpenHelper(VKApplication.context);
            SQLiteDatabase db = helper.getWritableDatabase();
            try {
                Cursor cursor = db.query("feed_lists", null, null, null, null, null, null);
                ContentValues values = new ContentValues();
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    do {
                        DatabaseUtils.cursorRowToContentValues(cursor, values);
                        result.add(new NewsfeedList(values.getAsInteger("lid").intValue(), values.getAsString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)));
                    } while (cursor.moveToNext());
                }
                cursor.close();
            } catch (Exception x) {
                Log.m527e("vk", "Error reading news cache DB!", x);
            }
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static void setLists(ArrayList<NewsfeedList> lists) {
        try {
            CacheOpenHelper helper = new CacheOpenHelper(VKApplication.context);
            SQLiteDatabase db = helper.getWritableDatabase();
            try {
                db.beginTransaction();
                db.delete("feed_lists", null, null);
                ContentValues values = new ContentValues();
                Iterator it = lists.iterator();
                while (it.hasNext()) {
                    NewsfeedList list = (NewsfeedList) it.next();
                    values.put("lid", Integer.valueOf(list.id));
                    values.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, list.title);
                    db.insert("feed_lists", null, values);
                }
                db.setTransactionSuccessful();
            } catch (Exception x) {
                Log.m527e("vk", "Error reading news cache DB!", x);
            }
            db.endTransaction();
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static boolean hasEntries(Context context) {
        try {
            boolean result;
            CacheOpenHelper helper = new CacheOpenHelper(context);
            SQLiteDatabase db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM `news`", null);
            cursor.moveToFirst();
            if (cursor.getInt(0) > 0) {
                result = true;
            } else {
                result = false;
            }
            cursor.close();
            db.close();
            helper.close();
            return result;
        } catch (Exception x) {
            Log.m527e("vk", "Error reading news cache DB!", x);
            return false;
        }
    }

    public static int getUpdateTime(Context context) {
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(new File(context.getFilesDir(), "newsfeed_last_update")));
            int t = in.readInt();
            in.close();
            return t;
        } catch (Exception e) {
            return (int) (System.currentTimeMillis() / 1000);
        }
    }

    public static void setUpdateTime(Context context, int time) {
        try {
            File f = new File(context.getFilesDir(), "newsfeed_last_update");
            f.createNewFile();
            DataOutputStream out = new DataOutputStream(new FileOutputStream(f));
            out.writeInt(time);
            out.close();
        } catch (Exception e) {
        }
    }
}
