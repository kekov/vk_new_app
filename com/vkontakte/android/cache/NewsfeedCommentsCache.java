package com.vkontakte.android.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.vkontakte.android.Log;
import com.vkontakte.android.NewsEntry;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Semaphore;

public class NewsfeedCommentsCache {
    private static Semaphore semaphore;

    /* renamed from: com.vkontakte.android.cache.NewsfeedCommentsCache.1 */
    class C05601 implements Runnable {
        private final /* synthetic */ Context val$context;
        private final /* synthetic */ ArrayList val$items;

        C05601(Context context, ArrayList arrayList) {
            this.val$context = context;
            this.val$items = arrayList;
        }

        public void run() {
            try {
                NewsfeedCommentsCache.semaphore.acquire();
                CacheOpenHelper helper = new CacheOpenHelper(this.val$context);
                SQLiteDatabase db = helper.getWritableDatabase();
                db.beginTransaction();
                try {
                    db.delete("news_comments", null, null);
                    Iterator it = this.val$items.iterator();
                    while (it.hasNext()) {
                        ((NewsEntry) it.next()).writeToSQLite(db, "news_comments");
                    }
                    db.setTransactionSuccessful();
                } catch (Exception x) {
                    Log.m527e("vk", "Error writing news_comments cache DB!", x);
                }
                db.endTransaction();
                db.close();
                helper.close();
                NewsfeedCommentsCache.setUpdateTime(this.val$context, (int) (System.currentTimeMillis() / 1000));
                Log.m528i("vk", "Updated news comments cache");
            } catch (Exception x2) {
                Log.m531w("vk", "Error updating comments cache", x2);
            }
            NewsfeedCommentsCache.semaphore.release();
        }
    }

    private static class CacheOpenHelper extends SQLiteOpenHelper {
        public CacheOpenHelper(Context context) {
            super(context, "posts.db", null, 11);
        }

        public void onCreate(SQLiteDatabase db) {
            CacheTables.createPosts(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
        }
    }

    static {
        semaphore = new Semaphore(1);
    }

    public static ArrayList<NewsEntry> get(Context context) {
        try {
            semaphore.acquire();
            CacheOpenHelper helper = new CacheOpenHelper(context);
            SQLiteDatabase db = helper.getReadableDatabase();
            ArrayList<NewsEntry> result = new ArrayList();
            try {
                Cursor cursor = db.query("news_comments", null, null, null, null, null, "`last_comment_time` desc");
                if (cursor != null && cursor.getCount() > 0) {
                    int i = 0;
                    cursor.moveToFirst();
                    do {
                        NewsEntry entry = new NewsEntry();
                        entry.readFromSQLite(cursor, context);
                        result.add(entry);
                        i++;
                    } while (cursor.moveToNext());
                }
            } catch (Exception x) {
                Log.m527e("vk", "Error reading news_comments cache DB!", x);
            }
            db.close();
            helper.close();
            semaphore.release();
            return result;
        } catch (Exception e) {
            semaphore.release();
            return null;
        }
    }

    public static void replace(ArrayList<NewsEntry> items, Context context) {
        new Thread(new C05601(context, items)).start();
    }

    public static void remove(int oid, int pid, Context context) {
        try {
            semaphore.acquire();
            CacheOpenHelper helper = new CacheOpenHelper(context);
            SQLiteDatabase db = helper.getWritableDatabase();
            try {
                db.delete("news_comments", "`pid`=" + pid + " AND `uid`=" + oid, null);
            } catch (Exception x) {
                Log.m527e("vk", "Error writing news_comments cache DB!", x);
            }
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        semaphore.release();
    }

    public static void update(Context context, int oid, int pid, int likes, int comments, boolean liked, boolean retweeted) {
        try {
            CacheOpenHelper helper = new CacheOpenHelper(context);
            SQLiteDatabase db = helper.getWritableDatabase();
            try {
                Cursor cursor = db.query("news_comments", new String[]{"flags"}, "`pid`=" + pid + " AND `uid`=" + oid, null, null, null, null);
                if (cursor != null && cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    ContentValues values = new ContentValues();
                    values.put("likes", Integer.valueOf(likes));
                    values.put("comments", Integer.valueOf(comments));
                    int flags = cursor.getInt(0);
                    if (liked) {
                        flags |= 8;
                    } else {
                        flags &= -9;
                    }
                    if (retweeted) {
                        flags |= 4;
                    } else {
                        flags &= -5;
                    }
                    values.put("flags", Integer.valueOf(flags));
                    db.update("news_comments", values, "`pid`=" + pid + " AND `uid`=" + oid, null);
                    db.close();
                    helper.close();
                }
            } catch (Exception x) {
                Log.m527e("vk", "Error writing news_comments cache DB!", x);
            }
        } catch (Exception e) {
        }
    }

    public static boolean hasEntries(Context context) {
        try {
            boolean result;
            semaphore.acquire();
            CacheOpenHelper helper = new CacheOpenHelper(context);
            SQLiteDatabase db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM `news_comments`", null);
            cursor.moveToFirst();
            if (cursor.getInt(0) > 0) {
                result = true;
            } else {
                result = false;
            }
            cursor.close();
            db.close();
            helper.close();
            semaphore.release();
            return result;
        } catch (Exception e) {
            semaphore.release();
            return false;
        }
    }

    public static int getUpdateTime(Context context) {
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(new File(context.getFilesDir(), "newsfeed_comments_last_update")));
            int t = in.readInt();
            in.close();
            return t;
        } catch (Exception e) {
            return (int) (System.currentTimeMillis() / 1000);
        }
    }

    public static void setUpdateTime(Context context, int time) {
        try {
            File f = new File(context.getFilesDir(), "newsfeed_comments_last_update");
            f.createNewFile();
            DataOutputStream out = new DataOutputStream(new FileOutputStream(f));
            out.writeInt(time);
            out.close();
        } catch (Exception e) {
        }
    }
}
