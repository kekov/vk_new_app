package com.vkontakte.android.cache;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Environment;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.StackBlur;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Locale;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class AlbumArtRetriever {
    public static final boolean USE_DISCOGS = false;
    private static LruCache<String, CoversHolder> cache;

    /* renamed from: com.vkontakte.android.cache.AlbumArtRetriever.1 */
    class C05531 implements Runnable {
        private final /* synthetic */ String val$_album;
        private final /* synthetic */ int val$aid;
        private final /* synthetic */ String val$artist;
        private final /* synthetic */ int val$oid;

        C05531(String str, String str2, int i, int i2) {
            this.val$_album = str;
            this.val$artist = str2;
            this.val$oid = i;
            this.val$aid = i2;
        }

        public void run() {
            try {
                String album = this.val$_album;
                if (album != null) {
                    album = album.replaceAll("\\([^\\)]+\\)", ACRAConstants.DEFAULT_STRING_VALUE);
                }
                Locale locale = Locale.US;
                String str = "http://api.discogs.com/database/search?type=master&release_title=%s&artist=%s";
                Object[] objArr = new Object[2];
                objArr[0] = album != null ? Uri.encode(album) : ACRAConstants.DEFAULT_STRING_VALUE;
                objArr[1] = this.val$artist != null ? Uri.encode(this.val$artist) : ACRAConstants.DEFAULT_STRING_VALUE;
                String url = String.format(locale, str, objArr);
                Log.m525d("vk", "Sending request -> " + url);
                String str2 = new String(Global.getURL(url), "UTF-8");
                Log.m525d("vk", "Discogs Resp=" + str2);
                JSONArray results = ((JSONObject) new JSONTokener(str2).nextValue()).getJSONArray("results");
                boolean acceptBad = false;
                boolean found = false;
                for (int k = 0; k < 2; k++) {
                    for (int i = 0; i < results.length(); i++) {
                        JSONObject result = results.getJSONObject(i);
                        JSONArray format = result.getJSONArray("format");
                        boolean accept = true;
                        for (int j = 0; j < format.length(); j++) {
                            String ff = format.getString(j).toLowerCase();
                            if (!acceptBad) {
                                if (!ff.startsWith("unofficial")) {
                                    if (!ff.startsWith("vinyl")) {
                                        if (ff.startsWith("compilation")) {
                                        }
                                    }
                                }
                                accept = false;
                                break;
                            }
                        }
                        if (accept) {
                            String imgUrl = result.getString("thumb").replace("R-90-", "R-");
                            if (!imgUrl.startsWith("http://s.pixogs.com")) {
                                Log.m525d("vk", "Downloading image " + imgUrl);
                                found = true;
                                byte[] imgData = Global.getURL(imgUrl);
                                Log.m525d("vk", "Download ok, size=" + imgData.length);
                                Options opts = new Options();
                                opts.inJustDecodeBounds = true;
                                BitmapFactory.decodeByteArray(imgData, 0, imgData.length, opts);
                                if (Math.abs(opts.outWidth - opts.outHeight) <= 100) {
                                    if (AlbumArtRetriever.saveCovers(imgData, this.val$oid, this.val$aid)) {
                                        AlbumArtRetriever.broadcastCoverAvailable(this.val$oid, this.val$aid);
                                    }
                                    if (!found) {
                                        break;
                                    }
                                    acceptBad = true;
                                } else {
                                    Log.m525d("vk", "not square");
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                    if (!found) {
                        break;
                    }
                    acceptBad = true;
                }
                if (!found) {
                    AudioCache.setCoverVersion(this.val$oid, this.val$aid, 1);
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    /* renamed from: com.vkontakte.android.cache.AlbumArtRetriever.2 */
    class C05542 implements Runnable {
        private final /* synthetic */ int val$aid;
        private final /* synthetic */ ImageLoadCallback val$callback;
        private final /* synthetic */ int val$oid;
        private final /* synthetic */ int val$type;

        C05542(int i, int i2, ImageLoadCallback imageLoadCallback, int i3) {
            this.val$oid = i;
            this.val$aid = i2;
            this.val$callback = imageLoadCallback;
            this.val$type = i3;
        }

        public void run() {
            try {
                File file = new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/" + this.val$oid + "_" + this.val$aid + ".covers");
                if (file.exists()) {
                    FileInputStream is = new FileInputStream(file);
                    DataInputStream s = new DataInputStream(is);
                    CoversHolder ch = new CoversHolder();
                    byte[] buf = new byte[s.readInt()];
                    s.read(buf);
                    ch.full = BitmapFactory.decodeByteArray(buf, 0, buf.length);
                    buf = new byte[s.readInt()];
                    s.read(buf);
                    ch.small = BitmapFactory.decodeByteArray(buf, 0, buf.length);
                    buf = new byte[s.readInt()];
                    s.read(buf);
                    ch.blur = BitmapFactory.decodeByteArray(buf, 0, buf.length);
                    is.close();
                    ch.aid = this.val$aid;
                    ch.oid = this.val$oid;
                    AlbumArtRetriever.cache.put(this.val$oid + "_" + this.val$aid, ch);
                    switch (this.val$type) {
                        case ValidationActivity.VRESULT_NONE /*0*/:
                            this.val$callback.onImageLoaded(ch.full, this.val$oid, this.val$aid);
                            return;
                        case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                            this.val$callback.onImageLoaded(ch.small, this.val$oid, this.val$aid);
                            return;
                        case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                            this.val$callback.onImageLoaded(ch.blur, this.val$oid, this.val$aid);
                            return;
                        default:
                            return;
                    }
                    Log.m532w("vk", x);
                }
                this.val$callback.notAvailable(this.val$oid, this.val$aid);
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    private static class CoversHolder {
        int aid;
        Bitmap blur;
        Bitmap full;
        int oid;
        Bitmap small;

        private CoversHolder() {
        }
    }

    public interface ImageLoadCallback {
        void notAvailable(int i, int i2);

        void onImageLoaded(Bitmap bitmap, int i, int i2);
    }

    static {
        cache = new LruCache(3);
    }

    public static boolean saveCovers(byte[] data, int oid, int aid) {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/" + oid + "_" + aid + ".covers");
            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
            if (bmp == null) {
                file.delete();
                return false;
            }
            FileOutputStream os = new FileOutputStream(file);
            DataOutputStream s = new DataOutputStream(os);
            s.writeInt(data.length);
            s.write(data);
            Bitmap thumb1 = Bitmap.createScaledBitmap(bmp, 170, 170, true);
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            thumb1.compress(CompressFormat.JPEG, 93, buf);
            s.writeInt(buf.size());
            s.write(buf.toByteArray());
            buf.reset();
            Bitmap thumb2 = Bitmap.createScaledBitmap(thumb1, 100, 100, false);
            StackBlur.blurBitmap(thumb2, 4);
            buf = new ByteArrayOutputStream();
            thumb2.compress(CompressFormat.JPEG, 99, buf);
            s.writeInt(buf.size());
            s.write(buf.toByteArray());
            os.close();
            return true;
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    public static void saveCoversFromFile(int oid, int aid) {
        File file;
        Exception x;
        if (AudioCache.getCoverVersion(oid, aid) < 1 && !new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/" + oid + "_" + aid + ".covers").exists()) {
            try {
                File file2 = new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/" + oid + "_" + aid);
                try {
                    FileInputStream in = new FileInputStream(file2);
                    int tagLen = 0;
                    int tagRead = 0;
                    ByteArrayOutputStream tagBuf = new ByteArrayOutputStream();
                    byte[] buf = new byte[10240];
                    while (true) {
                        int read = in.read(buf);
                        if (read <= 0) {
                            file = file2;
                            return;
                        }
                        if (buf[0] == 73 && buf[1] == 68 && buf[2] == 51) {
                            tagLen = (((buf[9] | (buf[8] << 7)) | (buf[7] << 14)) | (buf[6] << 21)) + 20;
                            Log.m528i("vk", "Found ID3v2 tag, len=" + tagLen);
                            if (tagLen < AudioCache.ID3_MAX_SIZE) {
                                tagBuf = new ByteArrayOutputStream();
                            }
                        }
                        if (tagBuf != null && tagRead < tagLen) {
                            tagBuf.write(buf, 0, Math.min(tagLen - tagRead, read));
                            tagRead += read;
                        }
                        if (tagBuf != null && tagRead >= tagLen) {
                            Log.m528i("vk", "Tag read, len=" + tagBuf.size());
                            ID3Parser parser = new ID3Parser(tagBuf.toByteArray());
                            tagBuf = null;
                            if (parser.getAlbumArt() == null || !saveCovers(parser.getAlbumArt(), oid, aid)) {
                                getCoversFromDiscogs(parser.getArtist(), parser.getAlbum(), oid, aid);
                            } else {
                                broadcastCoverAvailable(oid, aid);
                            }
                        }
                    }
                } catch (Exception e) {
                    x = e;
                    file = file2;
                }
            } catch (Exception e2) {
                x = e2;
                Log.m531w("vk", "error saving covers", x);
            }
        }
    }

    public static void getCoversFromDiscogs(String artist, String _album, int oid, int aid) {
    }

    private static void broadcastCoverAvailable(int oid, int aid) {
        Intent intent = new Intent(AudioCache.ACTION_ALBUM_ART_AVAILABLE);
        intent.putExtra("aid", aid);
        intent.putExtra("oid", oid);
        VKApplication.context.sendBroadcast(intent);
    }

    public static Bitmap getCoverImage(int aid, int oid, int type) {
        CoversHolder cached = (CoversHolder) cache.get(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString());
        if (cached != null) {
            switch (type) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return cached.full;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return cached.small;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return cached.blur;
            }
        }
        return null;
    }

    public static void getCoverImage(int aid, int oid, int type, ImageLoadCallback callback) {
        CoversHolder cached = (CoversHolder) cache.get(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString());
        if (cached != null) {
            switch (type) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    callback.onImageLoaded(cached.full, oid, aid);
                    return;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    callback.onImageLoaded(cached.small, oid, aid);
                    return;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    callback.onImageLoaded(cached.blur, oid, aid);
                    return;
                default:
                    return;
            }
        }
        new Thread(new C05542(oid, aid, callback, type)).start();
    }
}
