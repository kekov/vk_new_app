package com.vkontakte.android.cache;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.AudioFile;
import com.vkontakte.android.AudioPlayerService;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.AudioGetById;
import com.vkontakte.android.api.AudioGetById.Callback;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.acra.ACRAConstants;

public class AudioCache {
    public static final String ACTION_ALBUM_ART_AVAILABLE = "com.vkontakte.android.ALBUM_ART_AVAILABLE";
    public static final String ACTION_FILE_ADDED = "com.vkontakte.android.AUDIO_FILE_ADDED";
    public static final String ACTION_FILE_DELETED = "com.vkontakte.android.AUDIO_FILE_DELETED";
    private static final int COPY_BUFFER_SIZE = 10240;
    public static final int ID3_MAX_SIZE = 1024000;
    private static final int PROXY_PORT = 48329;
    public static Vector<String> cacheReqs;
    public static Vector<String> cachedIDs;
    private static Context context;
    private static boolean deleteCurrent;
    private static Thread dlPartsThread;
    private static Vector<RangesInfo> dlRequests;
    private static boolean filledIDs;
    private static ConcurrentHashMap<String, RangesInfo> ranges;
    private static final long[] retryIntervals;

    /* renamed from: com.vkontakte.android.cache.AudioCache.2 */
    class C05552 implements Comparator<FileRange> {
        C05552() {
        }

        public int compare(FileRange lhs, FileRange rhs) {
            return lhs.startOffset < rhs.startOffset ? -1 : 1;
        }
    }

    /* renamed from: com.vkontakte.android.cache.AudioCache.3 */
    class C05563 implements Runnable {
        private final /* synthetic */ int val$aid;
        private final /* synthetic */ int val$oid;

        C05563(int i, int i2) {
            this.val$oid = i;
            this.val$aid = i2;
        }

        public void run() {
            Log.m528i("vk", "Delete temp file " + this.val$oid + "_" + this.val$aid);
            File file = new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/" + this.val$oid + "_" + this.val$aid);
            if (AudioCache.ranges.containsKey(this.val$oid + "_" + this.val$aid)) {
                AudioCache.ranges.remove(this.val$oid + "_" + this.val$aid);
            }
            if (file.exists()) {
                file.delete();
                Log.m525d("vk", "temp file deleted");
            }
            File file2 = new File(file.getAbsolutePath() + ".covers");
            if (file2.exists()) {
                file2.delete();
            }
            file = new File(new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/"), this.val$oid + "_" + this.val$aid + ".deleted");
            Log.m528i("vk", "del " + file.getAbsolutePath() + " " + file.exists());
            if (file.exists()) {
                file.delete();
            }
            Log.m528i("vk", "End playback for " + this.val$oid + "_" + this.val$aid + " done!");
        }
    }

    /* renamed from: com.vkontakte.android.cache.AudioCache.4 */
    class C05574 implements Runnable {
        C05574() {
        }

        public void run() {
            int retries = 0;
            while (AudioCache.dlRequests.size() > 0) {
                try {
                    RangesInfo info = (RangesInfo) AudioCache.dlRequests.remove(0);
                    try {
                        URL url = new URL(info.url);
                        while (info.f50a.size() > 0) {
                            FileRange r = (FileRange) info.f50a.get(0);
                            Log.m528i("vk", "Downloading: " + r.startOffset + " - " + r.endOffset);
                            File dir = new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/");
                            if (!dir.exists()) {
                                dir.mkdirs();
                            }
                            File file = new File(dir, info.oid + "_" + info.aid);
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                            conn.setRequestProperty("Range", "bytes=" + r.startOffset + "-" + r.endOffset);
                            conn.connect();
                            InputStream sin = conn.getInputStream();
                            FileOutputStream os = new FileOutputStream(file, true);
                            os.getChannel().position((long) r.startOffset);
                            byte[] buf = new byte[AudioCache.COPY_BUFFER_SIZE];
                            while (true) {
                                int read = sin.read(buf);
                                if (read <= 0) {
                                    break;
                                }
                                os.write(buf, 0, read);
                                r.startOffset += read;
                            }
                            os.close();
                            sin.close();
                            conn.disconnect();
                            info.f50a.remove(0);
                            Log.m528i("vk", "Done!");
                        }
                        AudioCache.saveFile(true, info.file);
                    } catch (IOException e) {
                        Log.m530w("vk", "IOException, retrying...");
                        Thread.sleep(1000);
                        retries++;
                        AudioCache.dlRequests.add(info);
                    }
                } catch (Exception x) {
                    Log.m531w("vk", "error downloading", x);
                }
            }
            AudioCache.dlPartsThread = null;
        }
    }

    private static class CacheOpenHelper extends SQLiteOpenHelper {
        public CacheOpenHelper(Context context) {
            super(context, "audio.db", null, 5);
        }

        public void onCreate(SQLiteDatabase db) {
            CacheTables.createAudio(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.m530w("vk", "ON UPGRADE!! " + oldVersion + " -> " + newVersion);
            if (oldVersion < 6) {
                if (oldVersion == 3) {
                    db.execSQL("ALTER TABLE files ADD cover_version int not null default 0");
                    db.execSQL("ALTER TABLE files ADD lyrics text");
                }
                db.execSQL("ALTER TABLE files ADD file_size int not null default -1");
                return;
            }
            onCreate(db);
        }

        public void onDowngrade(SQLiteDatabase db, int oldv, int newv) {
        }

        public SQLiteDatabase getWritableDatabase() {
            SQLiteDatabase db;
            while (true) {
                try {
                    db = super.getWritableDatabase();
                    db.setLockingEnabled(false);
                    break;
                } catch (Exception e) {
                    try {
                        Thread.sleep(100);
                    } catch (Exception e2) {
                    }
                }
            }
            return db;
        }

        public SQLiteDatabase getReadableDatabase() {
            SQLiteDatabase db = super.getReadableDatabase();
            db.setLockingEnabled(false);
            return db;
        }
    }

    private static class FileRange {
        public int endOffset;
        public boolean needDownload;
        public int startOffset;

        private FileRange() {
        }
    }

    public static class Proxy {
        private ServerSocket ss;

        /* renamed from: com.vkontakte.android.cache.AudioCache.Proxy.1 */
        class C05581 implements Runnable {
            C05581() {
            }

            public void run() {
                try {
                    Proxy.this.ss = new ServerSocket();
                    Proxy.this.ss.bind(new InetSocketAddress("127.0.0.1", AudioCache.PROXY_PORT));
                    while (true) {
                        ProxyRunner proxyRunner = new ProxyRunner(Proxy.this.ss.accept());
                        Log.m528i("vk", "accepted");
                    }
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                }
            }
        }

        public Proxy(Context _context) {
            AudioCache.context = _context;
        }

        public void start() {
            new Thread(new C05581()).start();
        }

        public void stop() {
            try {
                this.ss.close();
            } catch (Exception e) {
            }
        }
    }

    private static class ProxyRunner implements Runnable {
        InputStream in;
        OutputStream out;
        private int retries;
        Socket sck;

        public ProxyRunner(Socket s) {
            this.sck = s;
            try {
                this.in = s.getInputStream();
                this.out = s.getOutputStream();
            } catch (Exception e) {
            }
            Thread thread = new Thread(this);
            thread.setPriority(1);
            thread.start();
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r44 = this;
            r14 = 0;
            r37 = 0;
            r31 = "";
        L_0x0005:
            r0 = r44;
            r0 = r0.in;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r37 = r40.read();	 Catch:{ Exception -> 0x04e8 }
            r40 = -1;
            r0 = r37;
            r1 = r40;
            if (r0 != r1) goto L_0x0042;
        L_0x0017:
            r40 = "vk";
            r0 = r40;
            r1 = r31;
            com.vkontakte.android.Log.m525d(r0, r1);	 Catch:{ Exception -> 0x04e8 }
            r40 = r31.length();	 Catch:{ Exception -> 0x04e8 }
            if (r40 != 0) goto L_0x0065;
        L_0x0026:
            r0 = r44;
            r0 = r0.in;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40.close();	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.out;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40.close();	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.sck;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40.close();	 Catch:{ Exception -> 0x04e8 }
        L_0x0041:
            return;
        L_0x0042:
            r40 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r41 = java.lang.String.valueOf(r31);	 Catch:{ Exception -> 0x04e8 }
            r40.<init>(r41);	 Catch:{ Exception -> 0x04e8 }
            r0 = r37;
            r0 = (char) r0;	 Catch:{ Exception -> 0x04e8 }
            r41 = r0;
            r40 = r40.append(r41);	 Catch:{ Exception -> 0x04e8 }
            r31 = r40.toString();	 Catch:{ Exception -> 0x04e8 }
            r40 = "\r\n\r\n";
            r0 = r31;
            r1 = r40;
            r40 = r0.endsWith(r1);	 Catch:{ Exception -> 0x04e8 }
            if (r40 == 0) goto L_0x0005;
        L_0x0064:
            goto L_0x0017;
        L_0x0065:
            r0 = r44;
            r1 = r31;
            r11 = r0.parseHeaders(r1);	 Catch:{ Exception -> 0x04e8 }
            r40 = "\r\n";
            r0 = r31;
            r1 = r40;
            r40 = r0.split(r1);	 Catch:{ Exception -> 0x04fc }
            r41 = 0;
            r40 = r40[r41];	 Catch:{ Exception -> 0x04fc }
            r41 = " ";
            r40 = r40.split(r41);	 Catch:{ Exception -> 0x04fc }
            r41 = 1;
            r40 = r40[r41];	 Catch:{ Exception -> 0x04fc }
            r41 = "/";
            r42 = "";
            r31 = r40.replaceFirst(r41, r42);	 Catch:{ Exception -> 0x04fc }
            r40 = ".mp3";
            r0 = r31;
            r1 = r40;
            r40 = r0.endsWith(r1);	 Catch:{ Exception -> 0x04fc }
            if (r40 == 0) goto L_0x00ab;
        L_0x0099:
            r40 = 0;
            r41 = r31.length();	 Catch:{ Exception -> 0x04fc }
            r41 = r41 + -4;
            r0 = r31;
            r1 = r40;
            r2 = r41;
            r31 = r0.substring(r1, r2);	 Catch:{ Exception -> 0x04fc }
        L_0x00ab:
            r40 = 8;
            r0 = r31;
            r1 = r40;
            r6 = android.util.Base64.decode(r0, r1);	 Catch:{ Exception -> 0x04fc }
            r38 = new java.lang.String;	 Catch:{ Exception -> 0x04fc }
            r40 = "UTF-8";
            r0 = r38;
            r1 = r40;
            r0.<init>(r6, r1);	 Catch:{ Exception -> 0x04fc }
        L_0x00c0:
            r40 = "vk";
            r41 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r42 = "URL ";
            r41.<init>(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r1 = r38;
            r41 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r41 = r41.toString();	 Catch:{ Exception -> 0x04e8 }
            com.vkontakte.android.Log.m528i(r40, r41);	 Catch:{ Exception -> 0x04e8 }
            r40 = "___";
            r0 = r38;
            r1 = r40;
            r6 = r0.split(r1);	 Catch:{ Exception -> 0x04e8 }
            r40 = 1;
            r40 = r6[r40];	 Catch:{ Exception -> 0x04e8 }
            r18 = java.lang.Integer.parseInt(r40);	 Catch:{ Exception -> 0x04e8 }
            r40 = 2;
            r40 = r6[r40];	 Catch:{ Exception -> 0x04e8 }
            r3 = java.lang.Integer.parseInt(r40);	 Catch:{ Exception -> 0x04e8 }
            r22 = new com.vkontakte.android.cache.AudioCache$FileRange;	 Catch:{ Exception -> 0x04e8 }
            r40 = 0;
            r0 = r22;
            r1 = r40;
            r0.<init>();	 Catch:{ Exception -> 0x04e8 }
            r12 = new com.vkontakte.android.cache.AudioCache$RangesInfo;	 Catch:{ Exception -> 0x04e8 }
            r40 = 0;
            r0 = r40;
            r12.<init>();	 Catch:{ Exception -> 0x04e8 }
            r40 = com.vkontakte.android.cache.AudioCache.ranges;	 Catch:{ Exception -> 0x04e8 }
            r41 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r42 = java.lang.String.valueOf(r18);	 Catch:{ Exception -> 0x04e8 }
            r41.<init>(r42);	 Catch:{ Exception -> 0x04e8 }
            r42 = "_";
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r41 = r0.append(r3);	 Catch:{ Exception -> 0x04e8 }
            r41 = r41.toString();	 Catch:{ Exception -> 0x04e8 }
            r40 = r40.containsKey(r41);	 Catch:{ Exception -> 0x04e8 }
            if (r40 != 0) goto L_0x0501;
        L_0x0129:
            r10 = new java.util.ArrayList;	 Catch:{ Exception -> 0x04e8 }
            r10.<init>();	 Catch:{ Exception -> 0x04e8 }
            r12.f50a = r10;	 Catch:{ Exception -> 0x04e8 }
            r0 = r18;
            r12.oid = r0;	 Catch:{ Exception -> 0x04e8 }
            r12.aid = r3;	 Catch:{ Exception -> 0x04e8 }
            r40 = com.vkontakte.android.AudioPlayerService.sharedInstance;	 Catch:{ Exception -> 0x04e8 }
            r40 = r40.getCurrentFile();	 Catch:{ Exception -> 0x04e8 }
            r0 = r40;
            r12.file = r0;	 Catch:{ Exception -> 0x04e8 }
            r40 = com.vkontakte.android.cache.AudioCache.ranges;	 Catch:{ Exception -> 0x04e8 }
            r41 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r42 = java.lang.String.valueOf(r18);	 Catch:{ Exception -> 0x04e8 }
            r41.<init>(r42);	 Catch:{ Exception -> 0x04e8 }
            r42 = "_";
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r41 = r0.append(r3);	 Catch:{ Exception -> 0x04e8 }
            r41 = r41.toString();	 Catch:{ Exception -> 0x04e8 }
            r0 = r40;
            r1 = r41;
            r0.put(r1, r12);	 Catch:{ Exception -> 0x04e8 }
        L_0x0164:
            r7 = new java.io.File;	 Catch:{ Exception -> 0x04e8 }
            r40 = android.os.Environment.getExternalStorageDirectory();	 Catch:{ Exception -> 0x04e8 }
            r41 = ".vkontakte/cache/audio/";
            r0 = r40;
            r1 = r41;
            r7.<init>(r0, r1);	 Catch:{ Exception -> 0x04e8 }
            r40 = r7.exists();	 Catch:{ Exception -> 0x04e8 }
            if (r40 != 0) goto L_0x017c;
        L_0x0179:
            r7.mkdirs();	 Catch:{ Exception -> 0x04e8 }
        L_0x017c:
            r8 = new java.io.File;	 Catch:{ Exception -> 0x04e8 }
            r40 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r41 = java.lang.String.valueOf(r18);	 Catch:{ Exception -> 0x04e8 }
            r40.<init>(r41);	 Catch:{ Exception -> 0x04e8 }
            r41 = "_";
            r40 = r40.append(r41);	 Catch:{ Exception -> 0x04e8 }
            r0 = r40;
            r40 = r0.append(r3);	 Catch:{ Exception -> 0x04e8 }
            r40 = r40.toString();	 Catch:{ Exception -> 0x04e8 }
            r0 = r40;
            r8.<init>(r7, r0);	 Catch:{ Exception -> 0x04e8 }
            r9 = r8.exists();	 Catch:{ Exception -> 0x04e8 }
            r40 = r8.exists();	 Catch:{ Exception -> 0x04e8 }
            if (r40 != 0) goto L_0x01a9;
        L_0x01a6:
            r8.createNewFile();	 Catch:{ Exception -> 0x04e8 }
        L_0x01a9:
            r24 = 0;
            r23 = -1;
            r40 = "range";
            r0 = r40;
            r40 = r11.containsKey(r0);	 Catch:{ Exception -> 0x04e8 }
            if (r40 == 0) goto L_0x0244;
        L_0x01b7:
            r40 = "range";
            r0 = r40;
            r21 = r11.get(r0);	 Catch:{ Exception -> 0x04e8 }
            r21 = (java.lang.String) r21;	 Catch:{ Exception -> 0x04e8 }
            r40 = "bytes=([0-9]+)-([0-9]+){0,1}";
            r20 = java.util.regex.Pattern.compile(r40);	 Catch:{ Exception -> 0x04e8 }
            r40 = "range";
            r0 = r40;
            r40 = r11.get(r0);	 Catch:{ Exception -> 0x04e8 }
            r40 = (java.lang.CharSequence) r40;	 Catch:{ Exception -> 0x04e8 }
            r0 = r20;
            r1 = r40;
            r17 = r0.matcher(r1);	 Catch:{ Exception -> 0x04e8 }
            r40 = r17.find();	 Catch:{ Exception -> 0x04e8 }
            if (r40 == 0) goto L_0x0207;
        L_0x01df:
            r40 = 1;
            r0 = r17;
            r1 = r40;
            r40 = r0.group(r1);	 Catch:{ Exception -> 0x04e8 }
            r24 = java.lang.Integer.parseInt(r40);	 Catch:{ Exception -> 0x04e8 }
            r40 = r17.groupCount();	 Catch:{ Exception -> 0x04e8 }
            r41 = 2;
            r0 = r40;
            r1 = r41;
            if (r0 <= r1) goto L_0x0207;
        L_0x01f9:
            r40 = 2;
            r0 = r17;
            r1 = r40;
            r40 = r0.group(r1);	 Catch:{ Exception -> 0x04e8 }
            r23 = java.lang.Integer.parseInt(r40);	 Catch:{ Exception -> 0x04e8 }
        L_0x0207:
            r40 = "vk";
            r41 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r42 = "Request Range = ";
            r41.<init>(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r1 = r24;
            r41 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r42 = " - ";
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r1 = r23;
            r41 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r41 = r41.toString();	 Catch:{ Exception -> 0x04e8 }
            com.vkontakte.android.Log.m528i(r40, r41);	 Catch:{ Exception -> 0x04e8 }
            r27 = new com.vkontakte.android.cache.AudioCache$FileRange;	 Catch:{ Exception -> 0x04e8 }
            r40 = 0;
            r0 = r27;
            r1 = r40;
            r0.<init>();	 Catch:{ Exception -> 0x04e8 }
            r0 = r24;
            r1 = r27;
            r1.startOffset = r0;	 Catch:{ Exception -> 0x04e8 }
            r0 = r23;
            r1 = r27;
            r1.endOffset = r0;	 Catch:{ Exception -> 0x04e8 }
        L_0x0244:
            r0 = r24;
            r1 = r22;
            r1.endOffset = r0;	 Catch:{ Exception -> 0x04e8 }
            r0 = r24;
            r1 = r22;
            r1.startOffset = r0;	 Catch:{ Exception -> 0x04e8 }
            r0 = r22;
            r10.add(r0);	 Catch:{ Exception -> 0x04e8 }
            r40 = new java.net.URL;	 Catch:{ Exception -> 0x04e8 }
            r41 = 0;
            r41 = r6[r41];	 Catch:{ Exception -> 0x04e8 }
            r40.<init>(r41);	 Catch:{ Exception -> 0x04e8 }
            r5 = r40.openConnection();	 Catch:{ Exception -> 0x04e8 }
            r5 = (java.net.HttpURLConnection) r5;	 Catch:{ Exception -> 0x04e8 }
            r41 = "Range";
            r40 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r42 = "bytes=";
            r0 = r40;
            r1 = r42;
            r0.<init>(r1);	 Catch:{ Exception -> 0x04e8 }
            r0 = r40;
            r1 = r24;
            r40 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r42 = "-";
            r0 = r40;
            r1 = r42;
            r42 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            if (r23 <= 0) goto L_0x0528;
        L_0x0285:
            r40 = java.lang.Integer.valueOf(r23);	 Catch:{ Exception -> 0x04e8 }
        L_0x0289:
            r0 = r42;
            r1 = r40;
            r40 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r40 = r40.toString();	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r1 = r40;
            r5.setRequestProperty(r0, r1);	 Catch:{ Exception -> 0x04e8 }
            r32 = 0;
            r40 = "vk";
            r41 = "Opening connection";
            com.vkontakte.android.Log.m525d(r40, r41);	 Catch:{ Exception -> 0x04e8 }
            r40 = 20000; // 0x4e20 float:2.8026E-41 double:9.8813E-320;
            r0 = r40;
            r5.setConnectTimeout(r0);	 Catch:{ Exception -> 0x04e8 }
        L_0x02ac:
            r0 = r44;
            r0 = r0.retries;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r41 = 3;
            r0 = r40;
            r1 = r41;
            if (r0 < r1) goto L_0x052c;
        L_0x02ba:
            r40 = 0;
            r0 = r40;
            r1 = r44;
            r1.retries = r0;	 Catch:{ Exception -> 0x04e8 }
            r40 = "vk";
            r41 = "Open ok";
            com.vkontakte.android.Log.m525d(r40, r41);	 Catch:{ Exception -> 0x04e8 }
            r15 = r5.getContentLength();	 Catch:{ Exception -> 0x04e8 }
            r40 = "Content-Range";
            r0 = r40;
            r29 = r5.getHeaderField(r0);	 Catch:{ Exception -> 0x04e8 }
            r26 = r15;
            if (r29 == 0) goto L_0x0317;
        L_0x02d9:
            r40 = "bytes ([0-9]+)-([0-9]+)/([0-9]+)";
            r40 = java.util.regex.Pattern.compile(r40);	 Catch:{ Exception -> 0x04e8 }
            r0 = r40;
            r1 = r29;
            r16 = r0.matcher(r1);	 Catch:{ Exception -> 0x04e8 }
            r40 = r16.find();	 Catch:{ Exception -> 0x04e8 }
            if (r40 == 0) goto L_0x0317;
        L_0x02ed:
            r40 = 1;
            r0 = r16;
            r1 = r40;
            r40 = r0.group(r1);	 Catch:{ Exception -> 0x04e8 }
            r24 = java.lang.Integer.parseInt(r40);	 Catch:{ Exception -> 0x04e8 }
            r40 = 2;
            r0 = r16;
            r1 = r40;
            r40 = r0.group(r1);	 Catch:{ Exception -> 0x04e8 }
            r23 = java.lang.Integer.parseInt(r40);	 Catch:{ Exception -> 0x04e8 }
            r40 = 3;
            r0 = r16;
            r1 = r40;
            r40 = r0.group(r1);	 Catch:{ Exception -> 0x04e8 }
            r26 = java.lang.Integer.parseInt(r40);	 Catch:{ Exception -> 0x04e8 }
        L_0x0317:
            r0 = r26;
            r12.f51l = r0;	 Catch:{ Exception -> 0x04e8 }
            r40 = 0;
            r40 = r6[r40];	 Catch:{ Exception -> 0x04e8 }
            r0 = r40;
            r12.url = r0;	 Catch:{ Exception -> 0x04e8 }
            r40 = "vk";
            r41 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r42 = "Content Length = ";
            r41.<init>(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r41 = r0.append(r15);	 Catch:{ Exception -> 0x04e8 }
            r42 = ", real len = ";
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r1 = r26;
            r41 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r41 = r41.toString();	 Catch:{ Exception -> 0x04e8 }
            com.vkontakte.android.Log.m528i(r40, r41);	 Catch:{ Exception -> 0x04e8 }
            r40 = -1;
            r0 = r23;
            r1 = r40;
            if (r0 != r1) goto L_0x0351;
        L_0x034f:
            r23 = r15 + -1;
        L_0x0351:
            r40 = "vk";
            r41 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r42 = "Response Range = ";
            r41.<init>(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r1 = r24;
            r41 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r42 = " - ";
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r1 = r23;
            r41 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r41 = r41.toString();	 Catch:{ Exception -> 0x04e8 }
            com.vkontakte.android.Log.m528i(r40, r41);	 Catch:{ Exception -> 0x04e8 }
            r28 = "";
            r0 = r12.file;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r0 = r26;
            r1 = r40;
            r1.fileSize = r0;	 Catch:{ Exception -> 0x04e8 }
            if (r24 > 0) goto L_0x0387;
        L_0x0385:
            if (r23 <= 0) goto L_0x05bc;
        L_0x0387:
            r40 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r41 = "HTTP/1.1 206 Partial Content\r\nContent-Range: bytes ";
            r40.<init>(r41);	 Catch:{ Exception -> 0x04e8 }
            r0 = r40;
            r1 = r24;
            r40 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r41 = "-";
            r41 = r40.append(r41);	 Catch:{ Exception -> 0x04e8 }
            if (r23 <= 0) goto L_0x05b8;
        L_0x039e:
            r40 = java.lang.Integer.valueOf(r23);	 Catch:{ Exception -> 0x04e8 }
        L_0x03a2:
            r0 = r41;
            r1 = r40;
            r40 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r41 = "/";
            r40 = r40.append(r41);	 Catch:{ Exception -> 0x04e8 }
            r0 = r40;
            r1 = r26;
            r40 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r41 = "\r\n";
            r40 = r40.append(r41);	 Catch:{ Exception -> 0x04e8 }
            r41 = "Content-Type: audio/mpeg\r\n";
            r40 = r40.append(r41);	 Catch:{ Exception -> 0x04e8 }
            r41 = "Content-Length: ";
            r40 = r40.append(r41);	 Catch:{ Exception -> 0x04e8 }
            r41 = r23 - r24;
            r40 = r40.append(r41);	 Catch:{ Exception -> 0x04e8 }
            r41 = "\r\n\r\n";
            r40 = r40.append(r41);	 Catch:{ Exception -> 0x04e8 }
            r28 = r40.toString();	 Catch:{ Exception -> 0x04e8 }
        L_0x03da:
            r0 = r44;
            r0 = r0.out;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r41 = "UTF-8";
            r0 = r28;
            r1 = r41;
            r41 = r0.getBytes(r1);	 Catch:{ Exception -> 0x04e8 }
            r40.write(r41);	 Catch:{ Exception -> 0x04e8 }
            r30 = new java.io.RandomAccessFile;	 Catch:{ Exception -> 0x04e8 }
            r40 = "rws";
            r0 = r30;
            r1 = r40;
            r0.<init>(r8, r1);	 Catch:{ Exception -> 0x04e8 }
            r0 = r24;
            r0 = (long) r0;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r0 = r30;
            r1 = r40;
            r0.seek(r1);	 Catch:{ Exception -> 0x04e8 }
            r40 = "vk";
            r41 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r42 = "Range start=";
            r41.<init>(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r1 = r24;
            r41 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r42 = ", position=";
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r42 = r30.getFilePointer();	 Catch:{ Exception -> 0x04e8 }
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r41 = r41.toString();	 Catch:{ Exception -> 0x04e8 }
            com.vkontakte.android.Log.m528i(r40, r41);	 Catch:{ Exception -> 0x04e8 }
            r33 = 0;
            r35 = 0;
            r36 = 0;
            r25 = 0;
            r40 = 10240; // 0x2800 float:1.4349E-41 double:5.059E-320;
            r0 = r40;
            r4 = new byte[r0];	 Catch:{ Exception -> 0x04e8 }
            r34 = r33;
        L_0x043a:
            r0 = r32;
            r25 = r0.read(r4);	 Catch:{ Exception -> 0x0717 }
            if (r25 > 0) goto L_0x05d5;
        L_0x0442:
            r33 = r34;
        L_0x0444:
            r30.close();	 Catch:{ Exception -> 0x04e8 }
            r32.close();	 Catch:{ Exception -> 0x04e8 }
            r5.disconnect();	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.in;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40.close();	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.out;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40.close();	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.sck;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40.close();	 Catch:{ Exception -> 0x04e8 }
            r40 = "vk";
            r41 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r42 = "DL end, rStart=";
            r41.<init>(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r22;
            r0 = r0.startOffset;	 Catch:{ Exception -> 0x04e8 }
            r42 = r0;
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r42 = ", rEnd=";
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r22;
            r0 = r0.endOffset;	 Catch:{ Exception -> 0x04e8 }
            r42 = r0;
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r42 = ", len=";
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r1 = r26;
            r41 = r0.append(r1);	 Catch:{ Exception -> 0x04e8 }
            r41 = r41.toString();	 Catch:{ Exception -> 0x04e8 }
            com.vkontakte.android.Log.m528i(r40, r41);	 Catch:{ Exception -> 0x04e8 }
            r40 = com.vkontakte.android.cache.AudioCache.cacheReqs;	 Catch:{ Exception -> 0x04e8 }
            r41 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r42 = java.lang.String.valueOf(r18);	 Catch:{ Exception -> 0x04e8 }
            r41.<init>(r42);	 Catch:{ Exception -> 0x04e8 }
            r42 = "_";
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r41 = r0.append(r3);	 Catch:{ Exception -> 0x04e8 }
            r41 = r41.toString();	 Catch:{ Exception -> 0x04e8 }
            r40 = r40.contains(r41);	 Catch:{ Exception -> 0x04e8 }
            if (r40 != 0) goto L_0x04c9;
        L_0x04c1:
            r40 = com.vkontakte.android.Global.uid;	 Catch:{ Exception -> 0x04e8 }
            r0 = r18;
            r1 = r40;
            if (r0 != r1) goto L_0x0041;
        L_0x04c9:
            r0 = r22;
            r0 = r0.startOffset;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            if (r40 != 0) goto L_0x0041;
        L_0x04d1:
            r0 = r22;
            r0 = r0.endOffset;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r0 = r40;
            r1 = r26;
            if (r0 != r1) goto L_0x0041;
        L_0x04dd:
            r40 = 1;
            r0 = r12.file;	 Catch:{ Exception -> 0x04e8 }
            r41 = r0;
            com.vkontakte.android.cache.AudioCache.saveFile(r40, r41);	 Catch:{ Exception -> 0x04e8 }
            goto L_0x0041;
        L_0x04e8:
            r39 = move-exception;
            r40 = "vk";
            r0 = r40;
            r1 = r39;
            com.vkontakte.android.Log.m532w(r0, r1);
            if (r14 == 0) goto L_0x0041;
        L_0x04f4:
            r14.close();	 Catch:{ Exception -> 0x04f9 }
            goto L_0x0041;
        L_0x04f9:
            r40 = move-exception;
            goto L_0x0041;
        L_0x04fc:
            r39 = move-exception;
            r38 = r31;
            goto L_0x00c0;
        L_0x0501:
            r40 = com.vkontakte.android.cache.AudioCache.ranges;	 Catch:{ Exception -> 0x04e8 }
            r41 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r42 = java.lang.String.valueOf(r18);	 Catch:{ Exception -> 0x04e8 }
            r41.<init>(r42);	 Catch:{ Exception -> 0x04e8 }
            r42 = "_";
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x04e8 }
            r0 = r41;
            r41 = r0.append(r3);	 Catch:{ Exception -> 0x04e8 }
            r41 = r41.toString();	 Catch:{ Exception -> 0x04e8 }
            r12 = r40.get(r41);	 Catch:{ Exception -> 0x04e8 }
            r12 = (com.vkontakte.android.cache.AudioCache.RangesInfo) r12;	 Catch:{ Exception -> 0x04e8 }
            r10 = r12.f50a;	 Catch:{ Exception -> 0x04e8 }
            goto L_0x0164;
        L_0x0528:
            r40 = "";
            goto L_0x0289;
        L_0x052c:
            r5.connect();	 Catch:{ FileNotFoundException -> 0x0535, IOException -> 0x0564 }
            r32 = r5.getInputStream();	 Catch:{ FileNotFoundException -> 0x0535, IOException -> 0x0564 }
            goto L_0x02ba;
        L_0x0535:
            r39 = move-exception;
            r0 = r44;
            r0 = r0.out;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r41 = "HTTP/1.1 404 Not Found\r\n\r\n";
            r42 = "UTF-8";
            r41 = r41.getBytes(r42);	 Catch:{ Exception -> 0x04e8 }
            r40.write(r41);	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.out;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40.close();	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.in;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40.close();	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.sck;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40.close();	 Catch:{ Exception -> 0x04e8 }
            goto L_0x0041;
        L_0x0564:
            r39 = move-exception;
            r40 = "vk";
            r0 = r40;
            r1 = r39;
            com.vkontakte.android.Log.m532w(r0, r1);	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.retries;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40 = r40 + 1;
            r0 = r40;
            r1 = r44;
            r1.retries = r0;	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.retries;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r41 = 3;
            r0 = r40;
            r1 = r41;
            if (r0 < r1) goto L_0x02ac;
        L_0x058a:
            r0 = r44;
            r0 = r0.out;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r41 = "HTTP/1.1 404 Not Found\r\n\r\n";
            r42 = "UTF-8";
            r41 = r41.getBytes(r42);	 Catch:{ Exception -> 0x04e8 }
            r40.write(r41);	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.out;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40.close();	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.in;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40.close();	 Catch:{ Exception -> 0x04e8 }
            r0 = r44;
            r0 = r0.sck;	 Catch:{ Exception -> 0x04e8 }
            r40 = r0;
            r40.close();	 Catch:{ Exception -> 0x04e8 }
            goto L_0x0041;
        L_0x05b8:
            r40 = "";
            goto L_0x03a2;
        L_0x05bc:
            r40 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x04e8 }
            r41 = "HTTP/1.1 200 OK\r\nContent-Type: audio/mpeg\r\nContent-Length: ";
            r40.<init>(r41);	 Catch:{ Exception -> 0x04e8 }
            r0 = r40;
            r40 = r0.append(r15);	 Catch:{ Exception -> 0x04e8 }
            r41 = "\r\n\r\n";
            r40 = r40.append(r41);	 Catch:{ Exception -> 0x04e8 }
            r28 = r40.toString();	 Catch:{ Exception -> 0x04e8 }
            goto L_0x03da;
        L_0x05d5:
            r0 = r44;
            r0 = r0.out;	 Catch:{ Exception -> 0x0717 }
            r40 = r0;
            r41 = 0;
            r0 = r40;
            r1 = r41;
            r2 = r25;
            r0.write(r4, r1, r2);	 Catch:{ Exception -> 0x0717 }
            r40 = 0;
            r0 = r30;
            r1 = r40;
            r2 = r25;
            r0.write(r4, r1, r2);	 Catch:{ Exception -> 0x0717 }
            r0 = r22;
            r0 = r0.endOffset;	 Catch:{ Exception -> 0x0717 }
            r40 = r0;
            if (r40 != 0) goto L_0x071b;
        L_0x05f9:
            r40 = 0;
            r40 = r4[r40];	 Catch:{ Exception -> 0x0717 }
            r41 = 73;
            r0 = r40;
            r1 = r41;
            if (r0 != r1) goto L_0x071b;
        L_0x0605:
            r40 = 1;
            r40 = r4[r40];	 Catch:{ Exception -> 0x0717 }
            r41 = 68;
            r0 = r40;
            r1 = r41;
            if (r0 != r1) goto L_0x071b;
        L_0x0611:
            r40 = 2;
            r40 = r4[r40];	 Catch:{ Exception -> 0x0717 }
            r41 = 51;
            r0 = r40;
            r1 = r41;
            if (r0 != r1) goto L_0x071b;
        L_0x061d:
            r40 = 9;
            r40 = r4[r40];	 Catch:{ Exception -> 0x0717 }
            r41 = 8;
            r41 = r4[r41];	 Catch:{ Exception -> 0x0717 }
            r41 = r41 << 7;
            r40 = r40 | r41;
            r41 = 7;
            r41 = r4[r41];	 Catch:{ Exception -> 0x0717 }
            r41 = r41 << 14;
            r40 = r40 | r41;
            r41 = 6;
            r41 = r4[r41];	 Catch:{ Exception -> 0x0717 }
            r41 = r41 << 21;
            r35 = r40 | r41;
            r35 = r35 + 20;
            r40 = "vk";
            r41 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0717 }
            r42 = "Found ID3v2 tag, len=";
            r41.<init>(r42);	 Catch:{ Exception -> 0x0717 }
            r0 = r41;
            r1 = r35;
            r41 = r0.append(r1);	 Catch:{ Exception -> 0x0717 }
            r41 = r41.toString();	 Catch:{ Exception -> 0x0717 }
            com.vkontakte.android.Log.m528i(r40, r41);	 Catch:{ Exception -> 0x0717 }
            r40 = 1024000; // 0xfa000 float:1.43493E-39 double:5.05923E-318;
            r0 = r35;
            r1 = r40;
            if (r0 >= r1) goto L_0x071b;
        L_0x065c:
            r33 = new java.io.ByteArrayOutputStream;	 Catch:{ Exception -> 0x0717 }
            r33.<init>();	 Catch:{ Exception -> 0x0717 }
        L_0x0661:
            if (r33 == 0) goto L_0x0680;
        L_0x0663:
            r0 = r36;
            r1 = r35;
            if (r0 >= r1) goto L_0x0680;
        L_0x0669:
            r40 = 0;
            r41 = r35 - r36;
            r0 = r41;
            r1 = r25;
            r41 = java.lang.Math.min(r0, r1);	 Catch:{ Exception -> 0x0707 }
            r0 = r33;
            r1 = r40;
            r2 = r41;
            r0.write(r4, r1, r2);	 Catch:{ Exception -> 0x0707 }
            r36 = r36 + r25;
        L_0x0680:
            if (r33 == 0) goto L_0x06e3;
        L_0x0682:
            r0 = r36;
            r1 = r35;
            if (r0 < r1) goto L_0x06e3;
        L_0x0688:
            r40 = "vk";
            r41 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0707 }
            r42 = "Tag read, len=";
            r41.<init>(r42);	 Catch:{ Exception -> 0x0707 }
            r42 = r33.size();	 Catch:{ Exception -> 0x0707 }
            r41 = r41.append(r42);	 Catch:{ Exception -> 0x0707 }
            r41 = r41.toString();	 Catch:{ Exception -> 0x0707 }
            com.vkontakte.android.Log.m528i(r40, r41);	 Catch:{ Exception -> 0x0707 }
            r19 = new com.vkontakte.android.cache.ID3Parser;	 Catch:{ Exception -> 0x0707 }
            r40 = r33.toByteArray();	 Catch:{ Exception -> 0x0707 }
            r0 = r19;
            r1 = r40;
            r0.<init>(r1);	 Catch:{ Exception -> 0x0707 }
            r33 = 0;
            r40 = r19.getAlbumArt();	 Catch:{ Exception -> 0x0707 }
            if (r40 == 0) goto L_0x06f5;
        L_0x06b5:
            r40 = r19.getAlbumArt();	 Catch:{ Exception -> 0x0707 }
            r0 = r40;
            r1 = r18;
            r40 = com.vkontakte.android.cache.AlbumArtRetriever.saveCovers(r0, r1, r3);	 Catch:{ Exception -> 0x0707 }
            if (r40 == 0) goto L_0x06f5;
        L_0x06c3:
            r13 = new android.content.Intent;	 Catch:{ Exception -> 0x0707 }
            r40 = "com.vkontakte.android.ALBUM_ART_AVAILABLE";
            r0 = r40;
            r13.<init>(r0);	 Catch:{ Exception -> 0x0707 }
            r40 = "aid";
            r0 = r40;
            r13.putExtra(r0, r3);	 Catch:{ Exception -> 0x0707 }
            r40 = "oid";
            r0 = r40;
            r1 = r18;
            r13.putExtra(r0, r1);	 Catch:{ Exception -> 0x0707 }
            r40 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x0707 }
            r0 = r40;
            r0.sendBroadcast(r13);	 Catch:{ Exception -> 0x0707 }
        L_0x06e3:
            r0 = r22;
            r0 = r0.endOffset;	 Catch:{ Exception -> 0x0707 }
            r40 = r0;
            r40 = r40 + r25;
            r0 = r40;
            r1 = r22;
            r1.endOffset = r0;	 Catch:{ Exception -> 0x0707 }
            r34 = r33;
            goto L_0x043a;
        L_0x06f5:
            r40 = r19.getArtist();	 Catch:{ Exception -> 0x0707 }
            r41 = r19.getAlbum();	 Catch:{ Exception -> 0x0707 }
            r0 = r40;
            r1 = r41;
            r2 = r18;
            com.vkontakte.android.cache.AlbumArtRetriever.getCoversFromDiscogs(r0, r1, r2, r3);	 Catch:{ Exception -> 0x0707 }
            goto L_0x06e3;
        L_0x0707:
            r39 = move-exception;
        L_0x0708:
            r40 = "vk";
            r41 = "Audio proxy error";
            r0 = r40;
            r1 = r41;
            r2 = r39;
            com.vkontakte.android.Log.m531w(r0, r1, r2);	 Catch:{ Exception -> 0x04e8 }
            goto L_0x0444;
        L_0x0717:
            r39 = move-exception;
            r33 = r34;
            goto L_0x0708;
        L_0x071b:
            r33 = r34;
            goto L_0x0661;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.cache.AudioCache.ProxyRunner.run():void");
        }

        private HashMap<String, String> parseHeaders(String req) {
            HashMap<String, String> headers = new HashMap();
            Matcher matcher = Pattern.compile("([A-Za-z-]+): ([^\r]+)").matcher(req);
            while (matcher.find()) {
                headers.put(matcher.group(1).toLowerCase(), matcher.group(2));
            }
            return headers;
        }
    }

    private static class RangesInfo {
        ArrayList<FileRange> f50a;
        int aid;
        AudioFile file;
        int f51l;
        int oid;
        public String url;

        private RangesInfo() {
            this.f50a = new ArrayList();
        }
    }

    /* renamed from: com.vkontakte.android.cache.AudioCache.1 */
    class C14121 implements Callback {
        private final /* synthetic */ Vector val$idsToGet;

        C14121(Vector vector) {
            this.val$idsToGet = vector;
        }

        public void success(ArrayList<AudioFile> files) {
            CacheOpenHelper helper = new CacheOpenHelper(VKApplication.context);
            SQLiteDatabase db = helper.getWritableDatabase();
            db.beginTransaction();
            try {
                Iterator it = files.iterator();
                while (it.hasNext()) {
                    AudioFile file = (AudioFile) it.next();
                    ContentValues values = new ContentValues();
                    values.put("aid", Integer.valueOf(file.aid));
                    values.put("oid", Integer.valueOf(file.oid));
                    values.put("artist", file.artist);
                    values.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, file.title);
                    values.put("duration", Integer.valueOf(file.duration));
                    values.put("lastplay", Integer.valueOf((int) (System.currentTimeMillis() / 1000)));
                    values.put("user", Boolean.valueOf(true));
                    values.put("lyrics_id", Integer.valueOf(file.lyricsID));
                    db.insert("files", null, values);
                    this.val$idsToGet.remove(file.oid + "_" + file.aid);
                }
                if (this.val$idsToGet.size() > 0) {
                    Log.m528i("vk", new StringBuilder(String.valueOf(this.val$idsToGet.size())).append(" files not found").toString());
                    it = this.val$idsToGet.iterator();
                    while (it.hasNext()) {
                        String id = (String) it.next();
                        db.delete("files", "oid=" + id.split("_")[0] + " AND aid=" + id.split("_")[1], null);
                        new File(Environment.getExternalStorageDirectory(), "/.vkontakte/cache/audio/" + id).delete();
                        Log.m528i("vk", "Deleted " + id + " as it does not exist on server.");
                    }
                }
                db.setTransactionSuccessful();
            } catch (Exception e) {
            }
            db.endTransaction();
            db.close();
            helper.close();
        }

        public void fail(int ecode, String emsg) {
        }
    }

    static {
        cachedIDs = new Vector();
        cacheReqs = new Vector();
        filledIDs = false;
        ranges = new ConcurrentHashMap();
        dlRequests = new Vector();
        retryIntervals = new long[]{1000, 2000, 5000, 10000, 15000};
        deleteCurrent = false;
    }

    public static void refillIDs(Context context) {
        cachedIDs.clear();
        filledIDs = false;
        fillIDs(context);
    }

    public static int getFileSize(String url) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestMethod("HEAD");
            conn.connect();
            int len = conn.getContentLength();
            conn.disconnect();
            return len;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return -1;
        }
    }

    public static void checkFileSize(String url, int oid, int aid) {
        SQLiteDatabase db = new CacheOpenHelper(context).getWritableDatabase();
        try {
            Cursor cursor = db.query("files", new String[]{"file_size"}, "oid=" + oid + " AND aid=" + aid, null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                int fsize = cursor.getInt(0);
                if (fsize == -1) {
                    fsize = getFileSize(url);
                    Log.m528i("vk", new StringBuilder(String.valueOf(oid)).append("_").append(aid).append(" size=").append(fsize).toString());
                }
                if (fsize != -1) {
                    File file = new File(Environment.getExternalStorageDirectory(), "/.vkontakte/cache/audio/" + oid + "_" + aid);
                    if (file.length() != ((long) fsize)) {
                        Log.m526e("vk", "File " + oid + "_" + aid + " has incorrect size - deleting!");
                        file.delete();
                        db.delete("files", "aid=" + aid + " AND oid=" + oid, null);
                        cachedIDs.remove(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString());
                        cacheReqs.add(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString());
                        Intent intent = new Intent(AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS);
                        intent.putExtra("reload_cached_list", true);
                        VKApplication.context.sendBroadcast(intent);
                    } else {
                        Log.m528i("vk", "File " + oid + "_" + aid + " OK");
                        ContentValues values = new ContentValues();
                        values.put("file_size", Integer.valueOf(fsize));
                        db.update("files", values, "aid=" + aid + " AND oid=" + oid, null);
                    }
                }
            }
            cursor.close();
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        db.close();
    }

    public static void fillIDs(Context context) {
        if (!filledIDs) {
            filledIDs = true;
            boolean deleted = false;
            CacheOpenHelper cacheOpenHelper = new CacheOpenHelper(context);
            SQLiteDatabase db = cacheOpenHelper.getWritableDatabase();
            Cursor cursor = db.query("files", new String[]{"oid", "aid", "file_size"}, null, null, null, null, null);
            cursor.moveToFirst();
            cachedIDs.clear();
            db.beginTransaction();
            Intent intent;
            File[] files;
            Vector<String> idsToGet;
            String name;
            File dfile;
            String[] sp;
            int oid;
            int aid;
            Cursor cursor1;
            boolean result;
            if (cursor == null || cursor.getCount() <= 0) {
                db.setTransactionSuccessful();
                db.endTransaction();
                cursor.close();
                if (deleted) {
                    intent = new Intent(AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS);
                    intent.putExtra("reload_cached_list", true);
                    VKApplication.context.sendBroadcast(intent);
                }
                files = new File(Environment.getExternalStorageDirectory(), "/.vkontakte/cache/audio/").listFiles();
                idsToGet = new Vector();
                for (File file : files) {
                    name = file.getName();
                    if (name.endsWith(".covers")) {
                        if (name.endsWith(".deleted")) {
                            try {
                                dfile = new File(file.getAbsolutePath() + ".deleted");
                                if (dfile.exists()) {
                                    sp = name.split("_");
                                    oid = Integer.parseInt(sp[0]);
                                    aid = Integer.parseInt(sp[1]);
                                    cursor1 = db.query("files", new String[]{"count(*)"}, "`oid`=" + oid + " and `aid`=" + aid, null, null, null, null);
                                    result = cursor1.getCount() <= 0 && cursor1.moveToFirst() && cursor1.getInt(0) > 0;
                                    cursor1.close();
                                    if (!result) {
                                        Log.m525d("vk", "need get " + oid + "_" + aid);
                                        idsToGet.add(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString());
                                        cachedIDs.add(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString());
                                    }
                                } else {
                                    file.delete();
                                    dfile.delete();
                                    new File(file.getAbsolutePath() + ".covers").delete();
                                    Log.m528i("vk", "Deleted " + name + " because it still existed");
                                }
                            } catch (Exception e) {
                                file.delete();
                                new File(file.getAbsolutePath() + ".covers").delete();
                            }
                        } else {
                            continue;
                        }
                    }
                }
                if (idsToGet.size() > 0) {
                    new AudioGetById(idsToGet).setCallback(new C14121(idsToGet)).exec();
                }
                db.close();
                cacheOpenHelper.close();
            }
            do {
                String fn = cursor.getInt(0) + "_" + cursor.getInt(1);
                int fsize = cursor.getInt(2);
                File file2 = new File(Environment.getExternalStorageDirectory(), "/.vkontakte/cache/audio/" + fn);
                dfile = new File(file2.getAbsolutePath() + ".deleted");
                File cfile = new File(file2.getAbsolutePath() + ".covers");
                if (dfile.exists()) {
                    Log.m528i("vk", new StringBuilder(String.valueOf(fn)).append(" IS DELETED!").toString());
                }
                if (!file2.exists() || file2.length() <= 0 || (!(fsize == -1 || file2.length() == ((long) fsize)) || dfile.exists())) {
                    try {
                        if ("mounted".equals(Environment.getExternalStorageState())) {
                            if (file2.exists()) {
                                file2.delete();
                            }
                            if (dfile.exists()) {
                                dfile.delete();
                            }
                            if (cfile.exists()) {
                                cfile.delete();
                            }
                            deleted = true;
                            Log.m528i("vk", "Deleting " + fn + " from DB (file on SD not found or is incomplete)");
                            db.delete("files", "oid=" + cursor.getInt(0) + " AND aid=" + cursor.getInt(1), null);
                        }
                    } catch (Exception e2) {
                    }
                } else {
                    cachedIDs.add(fn);
                }
            } while (cursor.moveToNext());
            db.setTransactionSuccessful();
            db.endTransaction();
            cursor.close();
            if (deleted) {
                intent = new Intent(AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS);
                intent.putExtra("reload_cached_list", true);
                VKApplication.context.sendBroadcast(intent);
            }
            files = new File(Environment.getExternalStorageDirectory(), "/.vkontakte/cache/audio/").listFiles();
            idsToGet = new Vector();
            for (File file3 : files) {
                name = file3.getName();
                if (name.endsWith(".covers")) {
                    if (name.endsWith(".deleted")) {
                        continue;
                    } else {
                        dfile = new File(file3.getAbsolutePath() + ".deleted");
                        if (dfile.exists()) {
                            sp = name.split("_");
                            oid = Integer.parseInt(sp[0]);
                            aid = Integer.parseInt(sp[1]);
                            cursor1 = db.query("files", new String[]{"count(*)"}, "`oid`=" + oid + " and `aid`=" + aid, null, null, null, null);
                            if (cursor1.getCount() <= 0) {
                            }
                            cursor1.close();
                            if (!result) {
                                Log.m525d("vk", "need get " + oid + "_" + aid);
                                idsToGet.add(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString());
                                cachedIDs.add(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString());
                            }
                        } else {
                            file3.delete();
                            dfile.delete();
                            new File(file3.getAbsolutePath() + ".covers").delete();
                            Log.m528i("vk", "Deleted " + name + " because it still existed");
                        }
                    }
                }
            }
            if (idsToGet.size() > 0) {
                new AudioGetById(idsToGet).setCallback(new C14121(idsToGet)).exec();
            }
            db.close();
            cacheOpenHelper.close();
        }
    }

    public static void endPlayback(int oid, int aid) {
        Log.m525d("vk", "End playback for " + oid + "_" + aid);
        if (deleteCurrent) {
            deleteCurrent = false;
            deleteTempFile(oid, aid);
            return;
        }
        if ((oid == Global.uid || cacheReqs.contains(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString())) && ranges.containsKey(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString())) {
            FileRange r;
            ArrayList<FileRange> al;
            ArrayList<FileRange> fr = ((RangesInfo) ranges.get(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString())).f50a;
            Collections.sort(fr, new C05552());
            int l = ((RangesInfo) ranges.get(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString())).f51l;
            Iterator it = fr.iterator();
            while (it.hasNext()) {
                r = (FileRange) it.next();
                al = new ArrayList();
                al.add(r);
                dbgRanges(al, l);
            }
            if (fr.size() == 0) {
                deleteTempFile(oid, aid);
                return;
            }
            int i = 0;
            while (i < fr.size()) {
                int j = 0;
                while (j < fr.size()) {
                    if (!(j == i || fr.get(j) == null || fr.get(i) == null)) {
                        FileRange a = (FileRange) fr.get(i);
                        FileRange b = (FileRange) fr.get(j);
                        if (b.startOffset >= a.startOffset && b.endOffset <= a.endOffset) {
                            fr.set(j, null);
                        }
                    }
                    j++;
                }
                i++;
            }
            do {
            } while (fr.remove(null));
            FileRange prev = (FileRange) fr.get(0);
            for (i = 1; i < fr.size(); i++) {
                r = (FileRange) fr.get(i);
                if (r != null) {
                    if (r.startOffset <= prev.endOffset) {
                        prev.endOffset = r.endOffset;
                        fr.set(i, null);
                    } else {
                        prev = r;
                    }
                }
            }
            do {
            } while (fr.remove(null));
            Log.m528i("vk", "After remove overlapping");
            it = fr.iterator();
            while (it.hasNext()) {
                r = (FileRange) it.next();
                al = new ArrayList();
                al.add(r);
                dbgRanges(al, l);
            }
            if (fr.size() == 1 && ((FileRange) fr.get(0)).startOffset == 0 && ((FileRange) fr.get(0)).endOffset == l) {
                Log.m528i("vk", "We already have full file");
                return;
            } else if (cacheReqs.contains(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString())) {
                FileRange d;
                ArrayList<FileRange> dl = new ArrayList();
                prev = (FileRange) fr.get(0);
                for (i = 1; i < fr.size(); i++) {
                    r = (FileRange) fr.get(i);
                    d = new FileRange();
                    d.startOffset = prev.endOffset;
                    d.endOffset = r.startOffset;
                    dl.add(d);
                    prev = r;
                }
                if (prev.endOffset < l) {
                    d = new FileRange();
                    d.startOffset = prev.endOffset;
                    d.endOffset = l;
                    dl.add(d);
                }
                Log.m528i("vk", "need to download: ");
                dbgRanges(dl, l);
                RangesInfo info = new RangesInfo();
                info.f51l = l;
                info.url = ((RangesInfo) ranges.get(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString())).url;
                info.f50a = dl;
                info.file = ((RangesInfo) ranges.get(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString())).file;
                downloadParts(info);
                return;
            } else {
                deleteTempFile(oid, aid);
            }
        }
        if (!isCachedByUser(oid, aid)) {
            deleteTempFile(oid, aid);
        }
    }

    private static void deleteTempFile(int oid, int aid) {
        new Thread(new C05563(oid, aid)).start();
    }

    private static void downloadParts(RangesInfo ri) {
        dlRequests.add(ri);
        if (dlPartsThread == null) {
            Thread thread = new Thread(new C05574());
            dlPartsThread = thread;
            thread.start();
        }
    }

    public static void saveFile(boolean forced, AudioFile file) {
        Log.m528i("vk", "Save file " + file.oid + "_" + file.aid);
        CacheOpenHelper helper;
        SQLiteDatabase db;
        ContentValues values;
        if (!isCached(file.oid, file.aid)) {
            if (!cachedIDs.contains(file.oid + "_" + file.aid)) {
                cachedIDs.add(file.oid + "_" + file.aid);
            }
            VKApplication.context.sendBroadcast(new Intent(AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS));
            helper = new CacheOpenHelper(context);
            db = helper.getWritableDatabase();
            try {
                values = new ContentValues();
                values.put("aid", Integer.valueOf(file.aid));
                values.put("oid", Integer.valueOf(file.oid));
                values.put("artist", file.artist);
                values.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, file.title);
                values.put("duration", Integer.valueOf(file.duration));
                values.put("lastplay", Integer.valueOf((int) (System.currentTimeMillis() / 1000)));
                values.put("user", Boolean.valueOf(forced));
                values.put("lyrics_id", Integer.valueOf(file.lyricsID));
                values.put("file_size", Integer.valueOf(file.fileSize));
                Log.m528i("vk", "Inserting: " + values);
                db.insert("files", null, values);
            } catch (Exception x) {
                Log.m531w("vk", "Error inserting audio cache file", x);
            }
            db.close();
            helper.close();
            File f = new File(new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/"), file.oid + "_" + file.aid + ".deleted");
            Log.m528i("vk", "del " + f.getAbsolutePath() + " " + f.exists());
            if (f.exists()) {
                f.delete();
            }
        } else if (forced) {
            helper = new CacheOpenHelper(context);
            db = helper.getWritableDatabase();
            try {
                values = new ContentValues();
                values.put("user", Boolean.valueOf(forced));
                db.update("files", values, "aid=" + file.aid + " and oid=" + file.oid, null);
            } catch (Exception e) {
            }
            db.close();
            helper.close();
        }
    }

    public static void saveCurrent(boolean forced) {
        saveFile(forced, AudioPlayerService.sharedInstance.getCurrentFile());
    }

    public static void updatePlayTime(int oid, int aid) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("lastplay", Integer.valueOf((int) (System.currentTimeMillis() / 1000)));
            db.update("files", values, "aid=" + aid + " and oid=" + oid, null);
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        db.close();
        helper.close();
    }

    public static int getCoverVersion(int oid, int aid) {
        int result = 0;
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.query("files", new String[]{"cover_version"}, "aid=" + aid + " AND oid=" + oid, null, null, null, null);
            cursor.moveToFirst();
            result = cursor.getInt(0);
            cursor.close();
        } catch (Throwable x) {
            if (x.getMessage().indexOf("no such column") > -1) {
                db.execSQL("ALTER TABLE files ADD cover_version int not null default 0");
                db.execSQL("ALTER TABLE files ADD lyrics text");
            }
            Log.m532w("vk", x);
        }
        db.close();
        helper.close();
        return result;
    }

    public static int setCoverVersion(int oid, int aid, int ver) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("cover_version", Integer.valueOf(ver));
            db.update("files", values, "aid=" + aid + " AND oid=" + oid, null);
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        db.close();
        helper.close();
        return 0;
    }

    public static String getLyrics(int oid, int aid) {
        String result = null;
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.query("files", new String[]{"lyrics"}, "aid=" + aid + " AND oid=" + oid, null, null, null, null);
            cursor.moveToFirst();
            result = cursor.getString(0);
            cursor.close();
        } catch (Throwable x) {
            if (x.getMessage().indexOf("no such column") > -1) {
                db.execSQL("ALTER TABLE files ADD cover_version int not null default 0");
                db.execSQL("ALTER TABLE files ADD lyrics text");
            }
            Log.m532w("vk", x);
        }
        db.close();
        helper.close();
        return result;
    }

    public static int saveLyrics(int oid, int aid, String l) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("lyrics", l);
            db.update("files", values, "aid=" + aid + " AND oid=" + oid, null);
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        db.close();
        helper.close();
        return 0;
    }

    public static boolean isCached(int oid, int aid) {
        if (!filledIDs) {
            fillIDs(VKApplication.context);
        }
        File dir = new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        if (cachedIDs.contains(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString())) {
            return true;
        }
        if (cacheReqs.contains(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString())) {
            return true;
        }
        boolean result = false;
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.query("files", new String[]{"count(*)"}, "`oid`=" + oid + " and `aid`=" + aid, null, null, null, null);
            if (cursor.getCount() <= 0 || !cursor.moveToFirst() || cursor.getInt(0) <= 0) {
                result = false;
            } else {
                result = true;
            }
            cursor.close();
        } catch (Exception e) {
        }
        db.close();
        helper.close();
        return result;
    }

    public static boolean isCachedByUser(int oid, int aid) {
        if (cacheReqs.contains(new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString())) {
            return true;
        }
        File dir = new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        if (oid == Global.uid) {
            return new File(dir, new StringBuilder(String.valueOf(oid)).append("_").append(aid).toString()).exists();
        }
        boolean result = false;
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.query("files", new String[]{"count(*)"}, "`oid`=" + oid + " and `aid`=" + aid + " and `user`=1", null, null, null, null);
            if (cursor.getCount() <= 0 || !cursor.moveToFirst() || cursor.getInt(0) <= 0) {
                result = false;
            } else {
                result = true;
            }
            cursor.close();
        } catch (Exception e) {
        }
        db.close();
        helper.close();
        return result;
    }

    public static void deleteOld() {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.query("files", null, "user=0", null, null, null, "lastplay asc");
            cursor.moveToFirst();
            if (cursor.getCount() > 10) {
                int nDel = cursor.getCount() - 10;
                String where = ACRAConstants.DEFAULT_STRING_VALUE;
                for (int i = 0; i < nDel; i++) {
                    Log.m525d("vk", "Deleted audio " + cursor.getInt(0) + "_" + cursor.getInt(1));
                    where = new StringBuilder(String.valueOf(where)).append("or(oid=").append(cursor.getInt(0)).append(" AND aid=").append(cursor.getInt(1)).append(")").toString();
                    new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/" + cursor.getInt(0) + "_" + cursor.getInt(1)).delete();
                    cachedIDs.remove(cursor.getInt(0) + "_" + cursor.getInt(1));
                    cursor.moveToNext();
                }
                cursor.close();
                db.delete("files", where.substring(2), null);
                Intent intent = new Intent(AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS);
                intent.putExtra("reload_cached_list", true);
                VKApplication.context.sendBroadcast(intent);
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        db.close();
        helper.close();
    }

    public static void clear() {
        CacheOpenHelper helper = new CacheOpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        Log.m525d("vk", "Clear audio cache");
        try {
            db.delete("files", null, null);
            for (File f : new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/").listFiles()) {
                Log.m525d("vk", "Deleting: " + f.getAbsolutePath());
                f.delete();
            }
            Log.m525d("vk", "All deleted");
            cachedIDs.clear();
            Intent intent = new Intent(AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS);
            intent.putExtra("reload_cached_list", true);
            VKApplication.context.sendBroadcast(intent);
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        db.close();
        helper.close();
    }

    public static void deleteCurrent() {
        AudioFile f = AudioPlayerService.sharedInstance.getCurrentFile();
        if (cachedIDs == null) {
            fillIDs(VKApplication.context);
        }
        cachedIDs.remove(f.oid + "_" + f.aid);
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            db.delete("files", "aid=" + f.aid + " and oid=" + f.oid, null);
        } catch (Exception e) {
        }
        db.close();
        helper.close();
        Intent intent = new Intent(AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS);
        intent.putExtra("reload_cached_list", true);
        VKApplication.context.sendBroadcast(intent);
        File file = new File(new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/"), f.oid + "_" + f.aid + ".deleted");
        try {
            file.createNewFile();
            FileOutputStream os = new FileOutputStream(file);
            os.write(1);
            os.close();
        } catch (Exception e2) {
        }
        deleteCurrent = true;
    }

    public static ArrayList<AudioFile> getCachedList(Context context) {
        File dir = new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/audio/");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        ArrayList<AudioFile> list = new ArrayList();
        Log.m528i("vk", "DB version = " + db.getVersion());
        try {
            Cursor cursor = db.query("files", null, null, null, null, null, null);
            cursor.moveToFirst();
            int i = 0;
            ContentValues values = new ContentValues();
            do {
                DatabaseUtils.cursorRowToContentValues(cursor, values);
                AudioFile file = new AudioFile();
                file.oid = values.getAsInteger("oid").intValue();
                file.aid = values.getAsInteger("aid").intValue();
                file.title = values.getAsString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                file.artist = values.getAsString("artist");
                file.duration = values.getAsInteger("duration").intValue();
                file.lyricsID = values.getAsInteger("lyrics_id").intValue();
                file.durationS = String.format("%d:%02d", new Object[]{Integer.valueOf(file.duration / 60), Integer.valueOf(file.duration % 60)});
                list.add(file);
                i++;
            } while (cursor.moveToNext());
            cursor.close();
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        db.close();
        helper.close();
        context = context;
        deleteOld();
        return list;
    }

    private static ArrayList<FileRange> intersectRanges(ArrayList<FileRange> a, FileRange b) {
        ArrayList<FileRange> result = new ArrayList();
        Iterator it = a.iterator();
        while (it.hasNext()) {
            FileRange r = (FileRange) it.next();
            if (b.startOffset >= r.startOffset && b.endOffset <= r.endOffset) {
                b.needDownload = false;
                result.add(b);
            } else if (b.startOffset < r.startOffset && b.endOffset > r.startOffset && b.endOffset <= r.endOffset) {
                r1 = new FileRange();
                r1.startOffset = b.startOffset;
                r1.endOffset = r.endOffset - 1;
                r1.needDownload = true;
                result.add(r1);
                r2 = new FileRange();
                r2.startOffset = r.startOffset;
                r2.endOffset = b.endOffset;
                r2.needDownload = false;
                result.add(r2);
            } else if (b.startOffset > r.startOffset && b.startOffset < r.endOffset && b.endOffset > r.endOffset) {
                r1 = new FileRange();
                r1.startOffset = b.startOffset;
                r1.endOffset = r.endOffset;
                r1.needDownload = false;
                result.add(r1);
                r2 = new FileRange();
                r2.startOffset = r.endOffset + 1;
                r2.endOffset = b.endOffset;
                r2.needDownload = true;
                result.add(r2);
            } else if (b.startOffset < r.startOffset && b.endOffset > r.endOffset) {
                r1 = new FileRange();
                r1.startOffset = b.startOffset;
                r1.endOffset = r.startOffset - 1;
                r1.needDownload = true;
                result.add(r1);
                r2 = new FileRange();
                r2.startOffset = r.startOffset;
                r2.endOffset = r.endOffset;
                r2.needDownload = false;
                result.add(r2);
                FileRange r3 = new FileRange();
                r3.startOffset = r.endOffset;
                r3.endOffset = b.endOffset;
                r3.needDownload = true;
                result.add(r3);
            }
        }
        if (result.size() == 0) {
            b.needDownload = true;
            result.add(b);
        }
        return result;
    }

    private static void dbgRanges(ArrayList<FileRange> r, int len) {
        int cs = len / 30;
        String s = ACRAConstants.DEFAULT_STRING_VALUE;
        for (int i = 0; i < 30; i++) {
            int l = cs * i;
            boolean f = false;
            Iterator it = r.iterator();
            while (it.hasNext()) {
                FileRange rn = (FileRange) it.next();
                if (rn.startOffset <= l && rn.endOffset >= l) {
                    f = true;
                    break;
                }
            }
            if (f) {
                s = new StringBuilder(String.valueOf(s)).append("#").toString();
            } else {
                s = new StringBuilder(String.valueOf(s)).append("-").toString();
            }
        }
        Log.m525d("vk", new StringBuilder(String.valueOf(s)).append(" [").append(((FileRange) r.get(0)).startOffset).append(" - ").append(((FileRange) r.get(r.size() - 1)).endOffset).append("]").toString());
    }
}
