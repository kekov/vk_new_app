package com.vkontakte.android.cache;

import android.support.v4.media.TransportMediator;
import android.support.v4.view.MotionEventCompat;
import com.vkontakte.android.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.util.HashMap;
import org.acra.ACRAConstants;

public class ID3Parser {
    private static final boolean DEBUG = false;
    private byte[] albumArt;
    HashMap<String, String> textData;

    public ID3Parser(byte[] data) {
        this.textData = new HashMap();
        try {
            DataInputStream s = new DataInputStream(new ByteArrayInputStream(data));
            s.skip(3);
            int ver = s.read();
            if (ver == 3 || ver == 4 || ver == 2) {
                s.read();
                int gflags = s.read();
                boolean unsync = (gflags & TransportMediator.FLAG_KEY_MEDIA_NEXT) > 0;
                boolean extHead = (gflags & 64) > 0;
                s.skip(4);
                if (unsync) {
                    byte[] b = new byte[s.available()];
                    s.read(b);
                    s = new DataInputStream(new ByteArrayInputStream(unsynchronize(b)));
                }
                if (extHead && ver != 2) {
                    s.skip(10);
                }
                while (s.available() > 4) {
                    byte[] _fid = new byte[(ver > 2 ? 4 : 3)];
                    s.read(_fid);
                    if (_fid[0] != null) {
                        int enc;
                        String frameId = new String(_fid);
                        int len = ver > 2 ? s.readInt() : ((s.read() << 16) | (s.read() << 8)) | s.read();
                        if (ver > 2) {
                            int flags = s.readShort();
                        }
                        if (frameId.startsWith("T")) {
                            if (!"TXXX".equals(frameId)) {
                                if (!"TXX".equals(frameId)) {
                                    enc = s.read();
                                    byte[] sdat = new byte[(len - 1)];
                                    s.read(sdat);
                                    this.textData.put(frameId, new String(sdat, 0, sdat.length, enc == 1 ? "UCS-2" : "cp1251").replace("\u0000", ACRAConstants.DEFAULT_STRING_VALUE));
                                }
                            }
                        }
                        if (!"APIC".equals(frameId)) {
                            if (!"PIC".equals(frameId)) {
                                s.skip((long) len);
                            }
                        }
                        enc = s.read();
                        int l = 0 + 1;
                        if (ver == 2) {
                            l += 3;
                            s.skip(3);
                        } else {
                            while (s.read() != 0) {
                                l++;
                            }
                            l++;
                        }
                        int picType = s.read();
                        l++;
                        while (s.read() != 0) {
                            l++;
                        }
                        this.albumArt = new byte[(len - (l + 1))];
                        Log.m528i("vk", "Allocated " + this.albumArt.length + " bytes, available = " + s.available());
                        s.readFully(this.albumArt);
                        Log.m525d("vk", "Available after cover = " + s.available());
                    } else {
                        return;
                    }
                }
                return;
            }
            Log.m530w("vk", "ID3 tag of unsupported version " + ver);
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    private byte[] unsynchronize(byte[] src) {
        ByteArrayInputStream in = new ByteArrayInputStream(src);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int prev = 0;
        while (in.available() > 0) {
            int b = in.read();
            if (b != 0 || prev != MotionEventCompat.ACTION_MASK) {
                out.write(b);
            }
            prev = b;
        }
        return out.toByteArray();
    }

    public byte[] getAlbumArt() {
        return this.albumArt;
    }

    public String getArtist() {
        return (String) this.textData.get(this.textData.containsKey("TPE1") ? "TPE1" : "TP1");
    }

    public String getAlbum() {
        return (String) this.textData.get(this.textData.containsKey("TALB") ? "TALB" : "TAL");
    }

    public String getTitle() {
        return (String) this.textData.get(this.textData.containsKey("TIT2") ? "TIT2" : "TT2");
    }
}
