package com.vkontakte.android.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import com.google.android.gcm.GCMConstants;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.Log;
import com.vkontakte.android.Message;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class ChatCache {
    private static final int MAX_DIALOGS = 50;

    private static class CacheOpenHelper extends SQLiteOpenHelper {
        public CacheOpenHelper(Context context) {
            super(context, "chats.db", null, 2);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("PRAGMA writable_schema = 1;");
            db.execSQL("delete from sqlite_master where type = 'table'");
            db.execSQL("PRAGMA writable_schema = 0");
            CacheTables.createChatStats(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
        }

        public SQLiteDatabase getReadableDatabase() {
            SQLiteDatabase db = null;
            while (db == null) {
                try {
                    db = super.getReadableDatabase();
                } catch (Exception e) {
                    try {
                        Thread.sleep(10);
                    } catch (Exception e2) {
                    }
                }
            }
            return db;
        }

        public SQLiteDatabase getWritableDatabase() {
            SQLiteDatabase db = null;
            while (db == null) {
                try {
                    db = super.getWritableDatabase();
                } catch (Exception e) {
                    try {
                        Thread.sleep(10);
                    } catch (Exception e2) {
                    }
                }
            }
            return db;
        }
    }

    public static Vector<Message> get(Context context, int peerID) {
        return null;
    }

    private static byte[] serializeAttachments(Message msg) {
        try {
            ByteArrayOutputStream o1 = new ByteArrayOutputStream();
            DataOutputStream o = new DataOutputStream(o1);
            if (msg.attachments != null) {
                Iterator it = msg.attachments.iterator();
                while (it.hasNext()) {
                    ((Attachment) it.next()).serialize(o);
                }
            }
            o.flush();
            return o1.toByteArray();
        } catch (Exception e) {
            return null;
        }
    }

    public static void add(Context context, int peerID, List<Message> msgs, boolean replace) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            CacheTables.createChat(db, peerID);
            if (replace) {
                db.delete("chat" + peerID, null, null);
            }
            updateStats(peerID, db);
            ContentValues values = new ContentValues();
            db.beginTransaction();
            try {
                for (Message msg : msgs) {
                    values.clear();
                    values.put("mid", Integer.valueOf(msg.id));
                    values.put(GCMConstants.EXTRA_SENDER, Integer.valueOf(msg.sender));
                    values.put("text", msg.text);
                    values.put("attachments", serializeAttachments(msg));
                    values.put("time", Integer.valueOf(msg.time));
                    values.put("readstate", Boolean.valueOf(msg.readState));
                    db.insert("chat" + peerID, null, values);
                }
                db.setTransactionSuccessful();
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
            db.endTransaction();
        } catch (Throwable x2) {
            Log.m532w("vk", x2);
        }
        db.close();
        helper.close();
    }

    public static void updateStats(int peerID, SQLiteDatabase db) throws Exception {
        ContentValues values = new ContentValues();
        values.put("peer", Integer.valueOf(peerID));
        values.put("last_update", Integer.valueOf((int) (System.currentTimeMillis() / 1000)));
        if (db.update("stats", values, "peer=" + peerID, null) == 0) {
            db.insert("stats", null, values);
            Cursor cursor = db.query("stats", null, null, null, null, null, "last_update desc");
            if (cursor != null && cursor.getCount() > MAX_DIALOGS) {
                ArrayList<Integer> peersToDelete = new ArrayList();
                cursor.moveToPosition(MAX_DIALOGS);
                do {
                    peersToDelete.add(Integer.valueOf(cursor.getInt(0)));
                    db.rawQuery("DROP TABLE chat" + cursor.getInt(0), null);
                } while (cursor.moveToNext());
                cursor.close();
                db.delete("stats", "peer IN (" + TextUtils.join(",", peersToDelete) + ")", null);
            }
        }
    }

    public static void setReadState(Context context, int peerID, int msgID, boolean read) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            CacheTables.createChat(db, peerID);
            ContentValues values = new ContentValues();
            values.put("readstate", Boolean.valueOf(read));
            db.update("chat" + peerID, values, "mid=" + msgID, null);
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        db.close();
        helper.close();
    }

    public static int getLastDate(Context context, int peerID) {
        int result = 0;
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.query("chat" + peerID, new String[]{"time"}, null, null, null, null, "time desc", "1");
            cursor.moveToFirst();
            result = cursor.getInt(0);
        } catch (Exception e) {
        }
        db.close();
        helper.close();
        return result;
    }
}
