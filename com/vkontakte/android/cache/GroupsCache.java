package com.vkontakte.android.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Log;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.Group;
import java.util.List;
import java.util.Vector;

public class GroupsCache {

    private static class CacheOpenHelper extends SQLiteOpenHelper {
        public CacheOpenHelper(Context context) {
            super(context, "groups.db", null, 3);
        }

        public void onCreate(SQLiteDatabase db) {
            CacheTables.createGroups(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
        }

        public SQLiteDatabase getWritableDatabase() {
            SQLiteDatabase db = super.getWritableDatabase();
            db.setLockingEnabled(false);
            return db;
        }

        public SQLiteDatabase getReadableDatabase() {
            SQLiteDatabase db = super.getReadableDatabase();
            db.setLockingEnabled(false);
            return db;
        }
    }

    public static Vector<Group> get() {
        CacheOpenHelper helper = new CacheOpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getReadableDatabase();
        Vector<Group> result = new Vector();
        try {
            Cursor cursor = db.query("groups", null, null, null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                ContentValues values = new ContentValues();
                do {
                    DatabaseUtils.cursorRowToContentValues(cursor, values);
                    Group group = new Group();
                    group.id = values.getAsInteger("id").intValue();
                    group.name = values.getAsString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                    group.photo = values.getAsString("photo");
                    group.type = values.getAsInteger(WebDialog.DIALOG_PARAM_TYPE).intValue();
                    group.startTime = values.getAsInteger("event_time").intValue();
                    group.isClosed = values.getAsInteger("closed").intValue();
                    group.isAdmin = values.getAsInteger("admin").intValue() == 1;
                    group.adminLevel = values.getAsInteger("admin_level").intValue();
                    result.add(group);
                } while (cursor.moveToNext());
            }
        } catch (Exception x) {
            Log.m527e("vk", "Error reading groups cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static Vector<Group> get(Context context, int count) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        Vector<Group> result = new Vector();
        int i = 0;
        try {
            Cursor cursor = db.query("groups", null, null, null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                ContentValues values = new ContentValues();
                do {
                    DatabaseUtils.cursorRowToContentValues(cursor, values);
                    Group group = new Group();
                    group.id = values.getAsInteger("id").intValue();
                    group.name = values.getAsString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                    group.photo = values.getAsString("photo");
                    group.type = values.getAsInteger(WebDialog.DIALOG_PARAM_TYPE).intValue();
                    group.startTime = values.getAsInteger("event_time").intValue();
                    group.isClosed = values.getAsInteger("closed").intValue();
                    group.isAdmin = values.getAsBoolean("admin").booleanValue();
                    group.adminLevel = values.getAsInteger("admin_level").intValue();
                    result.add(group);
                    i++;
                    if (i < count) {
                    }
                } while (cursor.moveToNext());
            }
        } catch (Exception x) {
            Log.m527e("vk", "Error reading groups cache DB!", x);
        }
        try {
            break;
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static void replace(List<Group> items) {
        CacheOpenHelper helper = new CacheOpenHelper(VKApplication.context);
        SQLiteDatabase db = null;
        try {
            db = helper.getWritableDatabase();
            db.beginTransaction();
            db.delete("groups", null, null);
            for (Group g : items) {
                ContentValues values = new ContentValues();
                values.put("id", Integer.valueOf(g.id));
                values.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, g.name);
                values.put("photo", g.photo);
                values.put(WebDialog.DIALOG_PARAM_TYPE, Integer.valueOf(g.type));
                values.put("closed", Integer.valueOf(g.isClosed));
                values.put("admin", Boolean.valueOf(g.isAdmin));
                values.put("admin_level", Integer.valueOf(g.adminLevel));
                values.put("event_time", Integer.valueOf(g.startTime));
                db.insert("groups", null, values);
            }
            db.setTransactionSuccessful();
            if (db != null) {
                db.endTransaction();
            }
        } catch (Exception x) {
            Log.m527e("vk", "Error writing groups cache DB!", x);
            if (db != null) {
                db.endTransaction();
            }
        } catch (Throwable th) {
            if (db != null) {
                db.endTransaction();
            }
        }
        if (db != null) {
            db.close();
        }
        helper.close();
    }

    public static void add(Group g, Context context) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("id", Integer.valueOf(g.id));
            values.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, g.name);
            values.put("photo", g.photo);
            values.put(WebDialog.DIALOG_PARAM_TYPE, Integer.valueOf(g.type));
            db.insert("groups", null, values);
        } catch (Exception x) {
            Log.m527e("vk", "Error writing friends cache DB!", x);
        }
        db.close();
        helper.close();
    }

    public static void remove(int gid, Context context) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            db.delete("groups", "`id`=" + gid, null);
        } catch (Exception x) {
            Log.m527e("vk", "Error writing groups cache DB!", x);
        }
        db.close();
        helper.close();
    }

    public static boolean hasEntries(Context context) {
        try {
            boolean result;
            CacheOpenHelper helper = new CacheOpenHelper(context);
            SQLiteDatabase db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM `groups`", null);
            cursor.moveToFirst();
            if (cursor.getInt(0) > 0) {
                result = true;
            } else {
                result = false;
            }
            cursor.close();
            db.close();
            helper.close();
            return result;
        } catch (Exception x) {
            Log.m527e("vk", "Error reading groups cache DB!", x);
            return false;
        }
    }
}
