package com.vkontakte.android.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import com.google.android.gcm.GCMConstants;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.ChatUser;
import com.vkontakte.android.DialogEntry;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.Message;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.data.Friends;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.acra.ACRAConstants;

public class Cache {
    private static final boolean DEBUG = false;

    private static class OpenHelper extends SQLiteOpenHelper {
        public OpenHelper(Context context) {
            super(context, "vk.db", null, 31);
        }

        public void onCreate(SQLiteDatabase db) {
            CacheTables.createCache(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
        }

        public SQLiteDatabase getWritableDatabase() {
            SQLiteDatabase db;
            while (true) {
                try {
                    db = super.getWritableDatabase();
                    db.setLockingEnabled(false);
                    break;
                } catch (Exception e) {
                    try {
                        Thread.sleep(100);
                    } catch (Exception e2) {
                    }
                }
            }
            return db;
        }

        public SQLiteDatabase getReadableDatabase() {
            SQLiteDatabase db;
            while (true) {
                try {
                    db = super.getReadableDatabase();
                    db.setLockingEnabled(false);
                    break;
                } catch (Exception e) {
                    try {
                        Thread.sleep(100);
                    } catch (Exception e2) {
                    }
                }
            }
            return db;
        }
    }

    public static ArrayList<UserProfile> getBirthdays(long date) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getReadableDatabase();
        ArrayList<UserProfile> result = new ArrayList();
        try {
            Date d = new Date(date);
            Date d2 = new Date(86400000 + date);
            Cursor cursor = db.rawQuery("SELECT * FROM birthdays JOIN users ON users.uid=birthdays.uid WHERE " + String.format(Locale.US, "(bday=%d AND bmonth=%d) OR (bday=%d AND bmonth=%d)", new Object[]{Integer.valueOf(d.getDate()), Integer.valueOf(d.getMonth() + 1), Integer.valueOf(d2.getDate()), Integer.valueOf(d2.getMonth() + 1)}) + " ORDER BY bmonth,bday", null);
            if (cursor == null || cursor.getCount() <= 0) {
                cursor.close();
                try {
                    db.close();
                    helper.close();
                } catch (Exception e) {
                }
                return result;
            }
            int i = 0;
            ContentValues values = new ContentValues();
            cursor.moveToFirst();
            do {
                DatabaseUtils.cursorRowToContentValues(cursor, values);
                UserProfile profile = new UserProfile();
                profile.uid = values.getAsInteger("uid").intValue();
                profile.firstName = values.getAsString("firstname");
                profile.lastName = values.getAsString("lastname");
                profile.fullName = profile.firstName + " " + profile.lastName;
                profile.photo = values.getAsString("photo_small");
                profile.bdate = values.getAsInteger("bday") + "." + values.getAsInteger("bmonth") + (values.getAsInteger("byear") != null ? "." + values.getAsInteger("byear") : ACRAConstants.DEFAULT_STRING_VALUE);
                profile.university = values.getAsString("name_r");
                result.add(profile);
                i++;
            } while (cursor.moveToNext());
            cursor.close();
            db.close();
            helper.close();
            return result;
        } catch (Exception x) {
            Log.m527e("vk", "Error reading friends cache DB!", x);
        }
    }

    public static ArrayList<UserProfile> getFriends() {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getReadableDatabase();
        ArrayList<UserProfile> result = new ArrayList();
        try {
            Cursor cursor = db.rawQuery("SELECT users.* FROM users LEFT JOIN friends_hints_order ON users.uid=friends_hints_order.uid WHERE is_friend=1 ORDER BY friends_hints_order.list_order ASC", null);
            if (cursor == null || cursor.getCount() <= 0) {
                cursor.close();
                try {
                    db.close();
                    helper.close();
                } catch (Exception e) {
                }
                return result;
            }
            int i = 0;
            ContentValues values = new ContentValues();
            cursor.moveToFirst();
            do {
                DatabaseUtils.cursorRowToContentValues(cursor, values);
                UserProfile profile = new UserProfile();
                profile.uid = values.getAsInteger("uid").intValue();
                profile.firstName = values.getAsString("firstname");
                profile.lastName = values.getAsString("lastname");
                profile.fullName = profile.firstName + " " + profile.lastName;
                profile.photo = values.getAsString("photo_small");
                profile.f151f = values.getAsInteger("f").intValue() == 1;
                profile.isFriend = true;
                result.add(profile);
                i++;
            } while (cursor.moveToNext());
            cursor.close();
            db.close();
            helper.close();
            return result;
        } catch (Exception x) {
            Log.m527e("vk", "Error reading friends cache DB!", x);
        }
    }

    public static ArrayList<UserProfile> getUsers(List<Integer> ids, boolean forceOld) {
        return getUsers(ids, forceOld, 0);
    }

    public static ArrayList<UserProfile> getUsers(List<Integer> ids, boolean forceOld, int nameCase) {
        Cursor cursor;
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getReadableDatabase();
        ArrayList<UserProfile> result = new ArrayList();
        if (nameCase == 0) {
            try {
                cursor = db.query("users", null, "uid in (" + TextUtils.join(",", ids) + ")", null, null, null, null);
            } catch (Exception x) {
                Log.m527e("vk", "Error reading friends cache DB!", x);
            }
        } else {
            cursor = db.rawQuery("SELECT users.uid, users_name_cases.firstname, users_name_cases.lastname, users.photo_small, users.f, users.is_friend, users.last_updated FROM users_name_cases JOIN users ON users_name_cases.uid=users.uid WHERE users_name_cases.uid IN (" + TextUtils.join(",", ids) + ") and name_case=" + nameCase, null);
        }
        if (cursor == null || cursor.getCount() <= 0) {
            cursor.close();
            try {
                db.close();
                helper.close();
            } catch (Exception e) {
            }
            return result;
        }
        int i = 0;
        ContentValues values = new ContentValues();
        cursor.moveToFirst();
        do {
            DatabaseUtils.cursorRowToContentValues(cursor, values);
            UserProfile profile = new UserProfile();
            profile.uid = values.getAsInteger("uid").intValue();
            profile.firstName = values.getAsString("firstname");
            profile.lastName = values.getAsString("lastname");
            profile.fullName = profile.firstName + " " + profile.lastName;
            profile.photo = values.getAsString("photo_small");
            profile.f151f = values.getAsInteger("f").intValue() == 1;
            profile.isFriend = values.getAsInteger("is_friend").intValue() == 1;
            if (forceOld || ((long) values.getAsInteger("last_updated").intValue()) > (System.currentTimeMillis() / 1000) - 86400) {
                result.add(profile);
            }
            i++;
        } while (cursor.moveToNext());
        cursor.close();
        db.close();
        helper.close();
        return result;
    }

    public static ArrayList<UserProfile> getImportedContacts(int service) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getReadableDatabase();
        ArrayList<UserProfile> result = new ArrayList();
        try {
            Cursor cursor = db.query("imported_contacts", null, "service=" + service, null, null, null, null);
            if (cursor == null || cursor.getCount() <= 0) {
                cursor.close();
                try {
                    db.close();
                    helper.close();
                } catch (Exception e) {
                }
                return result;
            }
            int i = 0;
            ContentValues values = new ContentValues();
            cursor.moveToFirst();
            do {
                DatabaseUtils.cursorRowToContentValues(cursor, values);
                UserProfile profile = new UserProfile();
                profile.uid = values.getAsInteger("vk_id").intValue();
                profile.extra = values.getAsString("external_id");
                profile.fullName = values.getAsString("external_name");
                profile.photo = values.getAsString("external_photo");
                profile.university = values.getAsString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION);
                profile.isFriend = values.getAsBoolean("req_sent").booleanValue();
                result.add(profile);
                i++;
            } while (cursor.moveToNext());
            cursor.close();
            db.close();
            helper.close();
            return result;
        } catch (Exception x) {
            Log.m527e("vk", "Error reading friends cache DB!", x);
        }
    }

    public static void saveImportedContacts(List<UserProfile> users, int service, boolean deleteOld) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            db.beginTransaction();
            if (deleteOld) {
                db.delete("imported_contacts", "service=" + service, null);
            }
            ContentValues values = new ContentValues();
            for (UserProfile u : users) {
                values.clear();
                values.put("vk_id", Integer.valueOf(u.uid));
                values.put("external_id", (String) u.extra);
                if (u.uid == 0) {
                    values.put("external_name", u.fullName);
                    values.put("external_photo", u.photo);
                }
                values.put("service", Integer.valueOf(service));
                values.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, u.university);
                values.put("req_sent", Boolean.valueOf(u.isFriend));
                db.insertWithOnConflict("imported_contacts", null, values, 5);
            }
            db.setTransactionSuccessful();
        } catch (Exception x) {
            Log.m527e("vk", "Error writing messages cache DB!", x);
        }
        try {
            db.endTransaction();
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static void updateFriends(ArrayList<UserProfile> users, boolean replace) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            db.beginTransaction();
            if (replace) {
                values.put("is_friend", Boolean.valueOf(false));
                db.update("users", values, null, null);
                db.delete("friends_hints_order", null, null);
            }
            int i = 0;
            Iterator it = users.iterator();
            while (it.hasNext()) {
                UserProfile user = (UserProfile) it.next();
                values.clear();
                values.put("uid", Integer.valueOf(user.uid));
                values.put("firstname", user.firstName);
                values.put("lastname", user.lastName);
                values.put("photo_small", user.photo);
                values.put("is_friend", Boolean.valueOf(true));
                values.put("f", Boolean.valueOf(user.f151f));
                values.put("last_updated", Integer.valueOf(((int) System.currentTimeMillis()) / LocationStatusCodes.GEOFENCE_NOT_AVAILABLE));
                db.insertWithOnConflict("users", null, values, 5);
                if (user.bdate != null && user.bdate.length() > 0) {
                    values.clear();
                    values.put("uid", Integer.valueOf(user.uid));
                    values.put("name_r", user.university);
                    String[] bd = user.bdate.split("\\.");
                    if (bd.length > 1) {
                        values.put("bday", Integer.valueOf(Integer.parseInt(bd[0])));
                        values.put("bmonth", Integer.valueOf(Integer.parseInt(bd[1])));
                        if (bd.length > 2) {
                            values.put("byear", Integer.valueOf(Integer.parseInt(bd[2])));
                        } else {
                            values.put("byear", Integer.valueOf(0));
                        }
                    }
                    db.insertWithOnConflict("birthdays", null, values, 5);
                }
                values.clear();
                values.put("uid", Integer.valueOf(user.uid));
                values.put("list_order", Integer.valueOf(replace ? i : 99999999));
                db.insert("friends_hints_order", null, values);
                i++;
            }
            db.setTransactionSuccessful();
        } catch (Exception x) {
            Log.m527e("vk", "Error writing friends cache DB!", x);
        }
        try {
            db.endTransaction();
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static void removeFriend(int uid) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("is_friend", Boolean.valueOf(false));
            db.update("users", values, "uid=" + uid, null);
        } catch (Exception x) {
            Log.m527e("vk", "Error writing friends cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static void addMessages(List<Message> msgs) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            for (Message m : msgs) {
                new AddMessageAction(m).apply(db);
            }
        } catch (Exception x) {
            Log.m527e("vk", "Error writing messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static ArrayList<DialogEntry> getDialogs(int offset, int count) {
        ArrayList<DialogEntry> result = new ArrayList();
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            String str = ",";
            Cursor cursor = db.query("dialogs", null, null, null, null, null, "time desc", new StringBuilder(String.valueOf(offset)).append(r18).append(count).toString());
            ContentValues values = new ContentValues();
            Log.m528i("vk", "count=" + cursor.getCount());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    DatabaseUtils.cursorRowToContentValues(cursor, values);
                    Message msg = new Message(values);
                    UserProfile user = new UserProfile();
                    user.uid = msg.peer;
                    if (msg.peer < 2000000000) {
                        user.firstName = values.getAsString("firstname");
                        user.lastName = values.getAsString("lastname");
                        user.fullName = user.firstName + " " + user.lastName;
                        user.photo = values.getAsString("photo_small");
                        user.online = Friends.getOnlineStatus(user.uid);
                    } else {
                        user.fullName = values.getAsString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                        user.online = values.getAsInteger("admin").intValue();
                        user.photo = values.getAsString("photo");
                    }
                    DialogEntry e = new DialogEntry();
                    e.lastMessage = msg;
                    e.profile = user;
                    result.add(e);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Throwable x) {
            Log.m527e("vk", "Error reading messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e2) {
        }
        return result;
    }

    public static int getDialogsCount() {
        int result = 0;
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT count(*) FROM dialogs", null);
            cursor.moveToFirst();
            result = cursor.getInt(0);
            cursor.close();
        } catch (Exception x) {
            Log.m527e("vk", "Error reading messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static void setMessageReadState(int mid, boolean state) {
        int i = 1;
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            if (!state) {
                i = 0;
            }
            new ModifyMessageFlagsAction(mid, 1, i).apply(db);
        } catch (Exception x) {
            Log.m527e("vk", "Error writing messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static void setMessageReadStateUpto(int mid, int peer, boolean state, boolean in) {
        int i = 1;
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            if (!state) {
                i = 0;
            }
            new ModifyMessageFlagsAction(mid, 1, i, in, peer).apply(db);
        } catch (Exception x) {
            Log.m527e("vk", "Error writing messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static void setMessageID(int mid, int newID) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("mid", Integer.valueOf(newID));
            db.update("messages", values, "mid=" + mid, null);
        } catch (Exception x) {
            Log.m527e("vk", "Error writing messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static boolean containsMessage(int mid) {
        boolean result = false;
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.query("messages", new String[]{"mid"}, "mid=" + mid, null, null, null, null);
            if (cursor.getCount() > 0) {
                result = true;
            } else {
                result = false;
            }
            cursor.close();
        } catch (Exception x) {
            Log.m527e("vk", "Error writing messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static void deleteDialog(int peer) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            db.delete("messages", "peer=" + peer, null);
        } catch (Exception x) {
            Log.m527e("vk", "Error writing messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static void updatePeers(List<UserProfile> users, boolean replace) {
        updatePeers(users, replace, 0);
    }

    public static void updatePeers(List<UserProfile> users, boolean replace, int nameCase) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        db.beginTransaction();
        for (UserProfile user : users) {
            if (user.uid >= 0 || user.uid <= -2000000000) {
                values.clear();
                if (user.uid < 2000000000) {
                    values.put("uid", Integer.valueOf(user.uid));
                    values.put("firstname", user.firstName);
                    values.put("lastname", user.lastName);
                    if (nameCase == 0) {
                        int i;
                        values.put("photo_small", user.photo);
                        values.put("is_friend", Boolean.valueOf(user.isFriend));
                        values.put("f", Boolean.valueOf(user.f151f));
                        values.put("last_updated", Integer.valueOf((int) (System.currentTimeMillis() / 1000)));
                        String str = "users";
                        if (replace) {
                            i = 5;
                        } else {
                            i = 4;
                        }
                        db.insertWithOnConflict(str, null, values, i);
                    } else {
                        if (replace) {
                            try {
                                db.delete("users_name_cases", "uid=" + user.uid + " AND name_case=" + nameCase, null);
                            } catch (Exception x) {
                                Log.m527e("vk", "Error writing users cache DB!", x);
                            }
                        }
                        values.put("name_case", Integer.valueOf(nameCase));
                        db.insert("users_name_cases", null, values);
                    }
                } else {
                    values.put("cid", Integer.valueOf(user.uid - 2000000000));
                    values.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, user.fullName);
                    values.put("admin", Integer.valueOf(user.online));
                    values.put("photo", user.photo);
                    values.put("need_update_users", Integer.valueOf(0));
                    db.insertWithOnConflict("chats", null, values, 5);
                }
            }
        }
        db.setTransactionSuccessful();
        try {
            db.endTransaction();
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static ArrayList<Message> getUnread(int limit) {
        ArrayList<Message> result = new ArrayList();
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.query("messages", null, "(flags & 1) = 1 AND sender<>" + Global.uid, null, null, null, "time desc", new StringBuilder(String.valueOf(limit)).toString());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                ContentValues values = new ContentValues();
                do {
                    DatabaseUtils.cursorRowToContentValues(cursor, values);
                    Message msg = new Message();
                    msg.id = values.getAsInteger("mid").intValue();
                    msg.peer = values.getAsInteger("peer").intValue();
                    msg.sender = values.getAsInteger(GCMConstants.EXTRA_SENDER).intValue();
                    msg.setText(values.getAsString("text"));
                    msg.time = values.getAsInteger("time").intValue();
                    msg.readState = values.getAsInteger("flags").intValue() == 0;
                    byte[] att = values.getAsByteArray("attachments");
                    if (att != null) {
                        msg.attachments = new ArrayList();
                        DataInputStream is = new DataInputStream(new ByteArrayInputStream(att));
                        int num = is.read();
                        for (int i = 0; i < num; i++) {
                            msg.attachments.add(Attachment.deserialize(is, is.readInt()));
                        }
                    }
                    result.add(msg);
                } while (cursor.moveToLast());
            }
        } catch (Throwable x) {
            Log.m527e("vk", "Error writing users cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static Message getMessageByID(int mid) {
        Message result = null;
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.query("messages", null, "mid=" + mid, null, null, null, null);
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                ContentValues values = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, values);
                result = new Message(values);
            }
            cursor.close();
            if (result != null && result.peer > 2000000000) {
                cursor = db.query("chats", new String[]{PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE}, "cid=" + (result.peer - 2000000000), null, null, null, null);
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    result.title = cursor.getString(0);
                }
                cursor.close();
            }
        } catch (Exception x) {
            Log.m527e("vk", "Error writing users cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static int getMaxMsgId() {
        int result = 0;
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT max(mid) FROM messages", null);
            cursor.moveToFirst();
            result = cursor.getInt(0);
            cursor.close();
        } catch (Exception x) {
            Log.m527e("vk", "Error reading messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static int applyMessagesActions(ArrayList<MessagesAction> acts) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        long t = System.currentTimeMillis();
        try {
            db.beginTransaction();
            Iterator it = acts.iterator();
            while (it.hasNext()) {
                MessagesAction act = (MessagesAction) it.next();
                long t1 = System.currentTimeMillis();
                act.apply(db);
            }
            db.setTransactionSuccessful();
        } catch (Exception x) {
            Log.m527e("vk", "Error writing messages cache DB!", x);
        }
        try {
            db.endTransaction();
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return 0;
    }

    public static int getMessagesHistoryCount(int peer) {
        int result = 0;
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT count(*) FROM messages WHERE peer=" + peer, null);
            cursor.moveToFirst();
            result = cursor.getInt(0);
            cursor.close();
        } catch (Exception x) {
            Log.m527e("vk", "Error reading messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static String[] getChatInfo(int id) {
        String[] result = new String[2];
        result[0] = "?";
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT title, photo FROM chats WHERE cid=" + id, null);
            cursor.moveToFirst();
            result[0] = cursor.getString(0);
            result[1] = cursor.getString(1);
            cursor.close();
        } catch (Exception x) {
            Log.m527e("vk", "Error reading messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static int getChatAdmin(int id) {
        int result = 0;
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT admin FROM chats WHERE cid=" + id, null);
            cursor.moveToFirst();
            result = cursor.getInt(0);
            cursor.close();
        } catch (Exception x) {
            Log.m527e("vk", "Error reading messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static ArrayList<ChatUser> getChatUsers(int id) {
        ArrayList<ChatUser> result = new ArrayList();
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        ArrayList<Integer> needGet = new ArrayList();
        try {
            Cursor cursor = db.query("chats_users", null, "cid=" + id, null, null, null, null);
            cursor.moveToFirst();
            ContentValues values = new ContentValues();
            if (cursor.getCount() > 0) {
                do {
                    DatabaseUtils.cursorRowToContentValues(cursor, values);
                    ChatUser cu = new ChatUser();
                    cu.user = new UserProfile();
                    cu.inviter = new UserProfile();
                    cu.user.uid = values.getAsInteger("uid").intValue();
                    cu.inviter.uid = values.getAsInteger("inviter").intValue();
                    if (!needGet.contains(Integer.valueOf(cu.user.uid))) {
                        needGet.add(Integer.valueOf(cu.user.uid));
                    }
                    if (!needGet.contains(Integer.valueOf(cu.inviter.uid))) {
                        needGet.add(Integer.valueOf(cu.inviter.uid));
                    }
                    result.add(cu);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Throwable x) {
            Log.m527e("vk", "Error reading messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        ArrayList<UserProfile> pp = Friends.getUsersBlocking(needGet);
        HashMap<Integer, UserProfile> profiles = new HashMap();
        Iterator it = pp.iterator();
        while (it.hasNext()) {
            UserProfile p = (UserProfile) it.next();
            profiles.put(Integer.valueOf(p.uid), p);
        }
        Iterator it2 = result.iterator();
        while (it2.hasNext()) {
            ChatUser user = (ChatUser) it2.next();
            user.user = (UserProfile) profiles.get(Integer.valueOf(user.user.uid));
            user.inviter = (UserProfile) profiles.get(Integer.valueOf(user.inviter.uid));
        }
        return result;
    }

    public static boolean needUpdateChat(int id) {
        boolean result = false;
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT need_update_users FROM chats WHERE cid=" + id, null);
            cursor.moveToFirst();
            if (cursor.getInt(0) == 1) {
                result = true;
            } else {
                result = false;
            }
            cursor.close();
        } catch (Exception x) {
            Log.m527e("vk", "Error reading messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static void setNeedUpdateChat(int id) {
        Log.m528i("vk", "SET NEED UPDATE CHAT " + id);
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("need_update_users", Integer.valueOf(1));
            db.update("chats", values, "cid=" + id, null);
        } catch (Exception x) {
            Log.m527e("vk", "Error writing messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static void deleteMessages(List<Integer> ids) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            db.delete("messages", "mid in (" + TextUtils.join(",", ids) + ")", null);
        } catch (Exception x) {
            Log.m527e("vk", "Error reading messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static void deleteAllMessages() {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            db.delete("messages", null, null);
        } catch (Exception x) {
            Log.m527e("vk", "Error reading messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static ArrayList<Message> getMessagesHistory(int peer, int offset, int count) {
        ArrayList<Message> result = new ArrayList();
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.query("messages", null, "peer=" + peer, null, null, null, "time desc, mid asc", new StringBuilder(String.valueOf(offset)).append(",").append(count).toString());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                ContentValues values = new ContentValues();
                do {
                    DatabaseUtils.cursorRowToContentValues(cursor, values);
                    result.add(0, new Message(values));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception x) {
            Log.m527e("vk", "Error reading messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static ArrayList<Message> getResendableMessages() {
        ArrayList<Message> result = new ArrayList();
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            Cursor cursor = db.query("messages", null, "mid<0 AND time>" + ((System.currentTimeMillis() / 1000) - 300), null, null, null, "time desc");
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                ContentValues values = new ContentValues();
                do {
                    DatabaseUtils.cursorRowToContentValues(cursor, values);
                    result.add(0, new Message(values));
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception x) {
            Log.m527e("vk", "Error reading messages cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static void updateChat(int id, String title, ArrayList<ChatUser> users, String photo) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        ArrayList<UserProfile> profiles = new ArrayList();
        try {
            ContentValues values = new ContentValues();
            if (title != null) {
                values.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, title);
                values.put("need_update_users", Integer.valueOf(0));
                db.update("chats", values, "cid=" + id, null);
            }
            if (photo != null) {
                values.put("photo", photo);
                db.update("chats", values, "cid=" + id, null);
            }
            if (users != null) {
                values.put("need_update_users", Integer.valueOf(0));
                db.update("chats", values, "cid=" + id, null);
                db.delete("chats_users", "cid=" + id, null);
                Iterator it = users.iterator();
                while (it.hasNext()) {
                    ChatUser user = (ChatUser) it.next();
                    values.clear();
                    values.put("cid", Integer.valueOf(id));
                    values.put("uid", Integer.valueOf(user.user.uid));
                    values.put("inviter", Integer.valueOf(user.inviter.uid));
                    values.put("invited", Integer.valueOf(0));
                    db.insert("chats_users", null, values);
                    if (!profiles.contains(user.user)) {
                        profiles.add(user.user);
                    }
                    if (!profiles.contains(user.inviter)) {
                        profiles.add(user.inviter);
                    }
                }
            }
        } catch (Exception x) {
            Log.m527e("vk", "Error writing messages DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        updatePeers(profiles, false);
    }

    public static void putApiRequest(String method, HashMap<String, String> params) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put("method", method);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            DataOutputStream s = new DataOutputStream(os);
            Set<String> keys = params.keySet();
            s.writeInt(keys.size());
            for (String k : keys) {
                s.writeUTF(k);
                s.writeUTF((String) params.get(k));
            }
            values.put("args", os.toByteArray());
            db.insert("api_queue", null, values);
            Log.m525d("vk", "Insert api request " + method + " (" + params + ") ok");
        } catch (Exception x) {
            Log.m527e("vk", "Error writing api queue DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static void deleteApiRequest(int id) {
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            db.delete("api_queue", "id=" + id, null);
        } catch (Exception x) {
            Log.m527e("vk", "Error writing api queue DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
    }

    public static int getApiRequest(HashMap<String, String> out) {
        int res = -1;
        OpenHelper helper = new OpenHelper(VKApplication.context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            Cursor cursor = db.query("api_queue", null, null, null, null, null, null, "1");
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                DatabaseUtils.cursorRowToContentValues(cursor, values);
                res = values.getAsInteger("id").intValue();
                String method = values.getAsString("method");
                DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(values.getAsByteArray("args")));
                int cnt = dataInputStream.readInt();
                for (int i = 0; i < cnt; i++) {
                    out.put(dataInputStream.readUTF(), dataInputStream.readUTF());
                }
                out.put("_method", method);
            }
            cursor.close();
        } catch (Throwable x) {
            Log.m527e("vk", "Error reading api queue DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return res;
    }
}
