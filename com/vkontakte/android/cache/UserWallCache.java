package com.vkontakte.android.cache;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.NewsEntry;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class UserWallCache {

    private static class CacheOpenHelper extends SQLiteOpenHelper {
        public CacheOpenHelper(Context context) {
            super(context, "posts.db", null, 11);
        }

        public void onCreate(SQLiteDatabase db) {
            CacheTables.createPosts(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
        }
    }

    public static ArrayList<NewsEntry> get(Context context) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        ArrayList<NewsEntry> result = new ArrayList();
        try {
            Cursor cursor = db.query("wall", null, null, null, null, null, "`time` desc");
            if (cursor != null && cursor.getCount() > 0) {
                int i = 0;
                cursor.moveToFirst();
                do {
                    NewsEntry entry = new NewsEntry();
                    entry.readFromSQLite(cursor, context);
                    entry.ownerID = Global.uid;
                    result.add(entry);
                    i++;
                } while (cursor.moveToNext());
            }
        } catch (Exception x) {
            Log.m527e("vk", "Error reading wall cache DB!", x);
        }
        try {
            db.close();
            helper.close();
        } catch (Exception e) {
        }
        return result;
    }

    public static void replace(ArrayList<NewsEntry> items, Context context) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete("wall", null, null);
            Iterator it = items.iterator();
            while (it.hasNext()) {
                ((NewsEntry) it.next()).writeToSQLite(db, "wall");
            }
            db.setTransactionSuccessful();
        } catch (Exception x) {
            Log.m527e("vk", "Error writing wall cache DB!", x);
        }
        db.endTransaction();
        db.close();
        helper.close();
        setUpdateTime(context, (int) (System.currentTimeMillis() / 1000));
    }

    public static void add(NewsEntry entry, Context context) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            entry.writeToSQLite(db, "wall");
        } catch (Exception x) {
            Log.m527e("vk", "Error writing wall cache DB!", x);
        }
        db.close();
        helper.close();
    }

    public static void remove(int pid, Context context) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            db.delete("wall", "`pid`=" + pid, null);
        } catch (Exception x) {
            Log.m527e("vk", "Error writing wall cache DB!", x);
        }
        db.close();
        helper.close();
    }

    public static void update(Context context, int oid, int pid, int likes, int comments, boolean liked) {
        CacheOpenHelper helper = new CacheOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        try {
            String flagsAct = "flags";
            if (liked) {
                flagsAct = "flags|8";
            } else {
                flagsAct = "flags&-9";
            }
            db.execSQL("UPDATE wall SET likes=" + likes + ", comments=" + comments + ", flags=" + flagsAct + " WHERE pid=" + pid);
        } catch (Exception x) {
            Log.m527e("vk", "Error writing wall cache DB!", x);
        }
        db.close();
        helper.close();
    }

    public static boolean hasEntries(Context context) {
        try {
            boolean result;
            CacheOpenHelper helper = new CacheOpenHelper(context);
            SQLiteDatabase db = helper.getReadableDatabase();
            Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM `wall`", null);
            cursor.moveToFirst();
            if (cursor.getInt(0) > 0) {
                result = true;
            } else {
                result = false;
            }
            cursor.close();
            db.close();
            helper.close();
            return result;
        } catch (Exception x) {
            Log.m527e("vk", "Error reading wall cache DB!", x);
            return false;
        }
    }

    public static int getUpdateTime(Context context) {
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(new File(context.getFilesDir(), "wall_last_update")));
            int t = in.readInt();
            in.close();
            return t;
        } catch (Exception e) {
            return (int) (System.currentTimeMillis() / 1000);
        }
    }

    public static void setUpdateTime(Context context, int time) {
        try {
            File f = new File(context.getFilesDir(), "wall_last_update");
            f.createNewFile();
            DataOutputStream out = new DataOutputStream(new FileOutputStream(f));
            out.writeInt(time);
            out.close();
        } catch (Exception e) {
        }
    }
}
