package com.vkontakte.android.cache;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.vkontakte.android.Global;
import java.io.IOException;

public class ModifyMessageFlagsAction extends MessagesAction {
    public static final int ACTION_CLEAR = 1;
    public static final int ACTION_SET = 0;
    private int action;
    private int flags;
    private boolean in;
    private boolean le;
    private int mid;
    private int peer;

    public ModifyMessageFlagsAction(int _mid, int _flags, int _action) {
        this.le = false;
        this.in = false;
        this.mid = _mid;
        this.flags = _flags;
        this.action = _action;
    }

    public ModifyMessageFlagsAction(int _mid, int _flags, int _action, boolean _in, int _peer) {
        this.le = false;
        this.in = false;
        this.mid = _mid;
        this.flags = _flags;
        this.action = _action;
        this.le = true;
        this.in = _in;
        this.peer = _peer;
    }

    public void apply(SQLiteDatabase db) throws SQLiteException, IOException {
        String flagsAct = "flags";
        if (this.action == 0) {
            flagsAct = "flags|" + this.flags;
        } else if (this.action == ACTION_CLEAR) {
            flagsAct = "flags&" + (this.flags ^ -1);
        }
        if (this.le) {
            String idTable = this.in ? "messages_read_ids_in" : "messages_read_ids_out";
            int lastRead = 0;
            String[] strArr = new String[ACTION_CLEAR];
            strArr[0] = "mid";
            Cursor cursor = db.query(idTable, strArr, "peer=" + this.peer, null, null, null, null);
            if (cursor.moveToFirst()) {
                lastRead = cursor.getInt(0);
            }
            cursor.close();
            db.execSQL("UPDATE messages SET flags=" + flagsAct + " WHERE peer=" + this.peer + " AND mid<=" + this.mid + " AND mid>" + lastRead + " AND sender" + (this.in ? "<>" : "=") + Global.uid);
            ContentValues values = new ContentValues();
            values.put("peer", Integer.valueOf(this.peer));
            values.put("mid", Integer.valueOf(this.mid));
            db.insertWithOnConflict(idTable, null, values, 5);
            return;
        }
        db.execSQL("UPDATE messages SET flags=" + flagsAct + " WHERE mid=" + this.mid);
    }
}
