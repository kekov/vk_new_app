package com.vkontakte.android.cache;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.google.android.gcm.GCMConstants;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.Log;
import com.vkontakte.android.Message;
import com.vkontakte.android.Message.FwdMessage;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.json.JSONObject;

public class AddMessageAction extends MessagesAction {
    private Message f152m;

    public AddMessageAction(Message msg) {
        this.f152m = msg;
    }

    public void apply(SQLiteDatabase db) throws SQLiteException, IOException {
        DataOutputStream out;
        Iterator it;
        ContentValues values = new ContentValues();
        values.put("mid", Integer.valueOf(this.f152m.id));
        values.put("peer", Integer.valueOf(this.f152m.peer));
        values.put(GCMConstants.EXTRA_SENDER, Integer.valueOf(this.f152m.sender));
        values.put("text", this.f152m.text);
        values.put("time", Integer.valueOf(this.f152m.time));
        if (this.f152m.attachments != null && this.f152m.attachments.size() > 0) {
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            buf.write(this.f152m.attachments.size());
            out = new DataOutputStream(buf);
            it = this.f152m.attachments.iterator();
            while (it.hasNext()) {
                Attachment att = (Attachment) it.next();
                if (att != null) {
                    att.serialize(out);
                }
            }
            values.put("attachments", buf.toByteArray());
        }
        if (this.f152m.fwdMessages != null && this.f152m.fwdMessages.size() > 0) {
            buf = new ByteArrayOutputStream();
            buf.write(this.f152m.fwdMessages.size());
            out = new DataOutputStream(buf);
            it = this.f152m.fwdMessages.iterator();
            while (it.hasNext()) {
                ((FwdMessage) it.next()).serialize(out);
            }
            values.put("fwd", buf.toByteArray());
        }
        int flags = 0;
        if (!this.f152m.readState) {
            flags = 0 | 1;
        }
        if (this.f152m.sendFailed) {
            flags |= 2;
        }
        if (this.f152m.isServiceMessage) {
            flags |= 4;
        }
        values.put("flags", Integer.valueOf(flags));
        if (this.f152m.extras != null && this.f152m.extras.size() > 0) {
            try {
                JSONObject obj = new JSONObject();
                for (String k : this.f152m.extras.keySet()) {
                    obj.put(k, this.f152m.extras.get(k));
                }
                values.put("extras", obj.toString());
            } catch (Exception x) {
                Log.m531w("vk", "Error serializing extras for message!", x);
            }
        }
        db.insertWithOnConflict("messages", null, values, 5);
    }
}
