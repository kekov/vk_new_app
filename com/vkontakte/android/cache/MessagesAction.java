package com.vkontakte.android.cache;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import java.io.IOException;

public abstract class MessagesAction {
    public abstract void apply(SQLiteDatabase sQLiteDatabase) throws SQLiteException, IOException;
}
