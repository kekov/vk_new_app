package com.vkontakte.android.cache;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import java.io.IOException;

public class DeleteMessageAction extends MessagesAction {
    private int mid;

    public DeleteMessageAction(int id) {
        this.mid = id;
    }

    public void apply(SQLiteDatabase db) throws SQLiteException, IOException {
        db.delete("messages", "mid=" + this.mid, null);
    }
}
