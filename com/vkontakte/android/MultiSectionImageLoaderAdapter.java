package com.vkontakte.android;

import android.graphics.Bitmap;
import com.vkontakte.android.ui.ListImageLoaderAdapter;

public abstract class MultiSectionImageLoaderAdapter extends ListImageLoaderAdapter {
    public abstract int getImageCountForItem(int i, int i2);

    public abstract String getImageURL(int i, int i2, int i3);

    public abstract int getItemCount(int i);

    public abstract int getSectionCount();

    public abstract boolean isSectionHeaderVisible(int i);

    private int[] resolveIndex(int item) {
        int[] result = new int[2];
        int ns = getSectionCount();
        int idx = 0;
        for (int i = 0; i < ns; i++) {
            if (isSectionHeaderVisible(i)) {
                if (idx == item) {
                    result[0] = i;
                    result[1] = -1;
                    break;
                }
                idx++;
            }
            int prevIdx = idx;
            idx += getItemCount(i);
            if (item >= prevIdx && item < idx && prevIdx != idx) {
                result[0] = i;
                result[1] = item - prevIdx;
                break;
            }
        }
        return result;
    }

    public int getItemCount() {
        int ns = getSectionCount();
        int cnt = 0;
        for (int i = 0; i < ns; i++) {
            if (isSectionHeaderVisible(i)) {
                cnt++;
            }
            cnt += getItemCount(i);
        }
        return cnt;
    }

    public int getImageCountForItem(int item) {
        int[] s = resolveIndex(item);
        if (s[1] == -1) {
            return 0;
        }
        return getImageCountForItem(s[0], s[1]);
    }

    public String getImageURL(int item, int image) {
        int[] s = resolveIndex(item);
        return getImageURL(s[0], s[1], image);
    }

    public void imageLoaded(int item, int image, Bitmap bitmap) {
        int[] s = resolveIndex(item);
        imageLoaded(s[0], s[1], image, bitmap);
    }

    public int getSectionHeadersCount() {
        int c = 0;
        int sc = getSectionCount();
        for (int i = 0; i < sc; i++) {
            if (isSectionHeaderVisible(i)) {
                c++;
            }
        }
        return c;
    }

    public void imageLoaded(int section, int item, int image, Bitmap bitmap) {
    }
}
