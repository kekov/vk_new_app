package com.vkontakte.android;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.MotionEventCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.PlacesAdd;
import com.vkontakte.android.api.PlacesAdd.Callback;
import java.util.ArrayList;
import java.util.Locale;
import org.acra.ACRAConstants;

public class SelectGeoPointActivity extends SherlockActivity {
    private EditText addressEdit;
    private boolean firstLocUpdate;
    private LocationClient locationClient;
    private GoogleMap map;
    private boolean mapMoved;
    private MapView mapView;
    private Marker marker;
    private View okBtn;
    private LatLng selectedPoint;
    private EditText titleEdit;

    /* renamed from: com.vkontakte.android.SelectGeoPointActivity.1 */
    class C04581 implements OnClickListener {
        C04581() {
        }

        public void onClick(DialogInterface dialog, int which) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps"));
            intent.addFlags(268435456);
            SelectGeoPointActivity.this.startActivity(intent);
            SelectGeoPointActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.SelectGeoPointActivity.2 */
    class C04592 implements OnClickListener {
        C04592() {
        }

        public void onClick(DialogInterface dialog, int which) {
            SelectGeoPointActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.SelectGeoPointActivity.3 */
    class C04603 implements OnCancelListener {
        C04603() {
        }

        public void onCancel(DialogInterface dialog) {
            SelectGeoPointActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.SelectGeoPointActivity.4 */
    class C04624 implements View.OnClickListener {

        /* renamed from: com.vkontakte.android.SelectGeoPointActivity.4.1 */
        class C04611 implements Runnable {
            private final /* synthetic */ GeoAttachment val$att;
            private final /* synthetic */ ProgressDialog val$pdlg;

            C04611(GeoAttachment geoAttachment, ProgressDialog progressDialog) {
                this.val$att = geoAttachment;
                this.val$pdlg = progressDialog;
            }

            public void run() {
                try {
                    Address addr = (Address) new Geocoder(SelectGeoPointActivity.this.getBaseContext(), Locale.getDefault()).getFromLocation(this.val$att.lat, this.val$att.lon, 1).get(0);
                    String a = ACRAConstants.DEFAULT_STRING_VALUE;
                    ArrayList<String> t = new ArrayList();
                    if (addr.getThoroughfare() != null) {
                        t.add(addr.getThoroughfare());
                    }
                    if (addr.getSubThoroughfare() != null) {
                        t.add(addr.getSubThoroughfare());
                    }
                    if (!(addr.getFeatureName() == null || addr.getFeatureName().equals(addr.getSubThoroughfare()))) {
                        t.add(addr.getFeatureName());
                    }
                    this.val$att.address = TextUtils.join(", ", t);
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                }
                this.val$pdlg.dismiss();
                Intent intent = new Intent();
                intent.putExtra("point", this.val$att);
                SelectGeoPointActivity.this.setResult(-1, intent);
                SelectGeoPointActivity.this.finish();
            }
        }

        /* renamed from: com.vkontakte.android.SelectGeoPointActivity.4.2 */
        class C13482 implements Callback {
            private final /* synthetic */ GeoAttachment val$att;

            C13482(GeoAttachment geoAttachment) {
                this.val$att = geoAttachment;
            }

            public void success(int id) {
                this.val$att.id = id;
                this.val$att.address = SelectGeoPointActivity.this.addressEdit.getText().toString();
                this.val$att.title = SelectGeoPointActivity.this.titleEdit.getText().toString();
                Intent intent = new Intent();
                intent.putExtra("point", this.val$att);
                SelectGeoPointActivity.this.setResult(-1, intent);
                SelectGeoPointActivity.this.finish();
            }

            public void fail(int ecode, String emsg) {
                Toast.makeText(SelectGeoPointActivity.this, C0436R.string.error, 0).show();
            }
        }

        C04624() {
        }

        public void onClick(View v) {
            GeoAttachment att = new GeoAttachment();
            att.lat = SelectGeoPointActivity.this.selectedPoint.latitude;
            att.lon = SelectGeoPointActivity.this.selectedPoint.longitude;
            if (SelectGeoPointActivity.this.getIntent().getBooleanExtra("create_place", false)) {
                new PlacesAdd(SelectGeoPointActivity.this.titleEdit.getText().toString(), SelectGeoPointActivity.this.addressEdit.getText().toString(), att.lat, att.lon).setCallback(new C13482(att)).wrapProgress(SelectGeoPointActivity.this).exec(SelectGeoPointActivity.this);
                return;
            }
            ProgressDialog pdlg = new ProgressDialog(SelectGeoPointActivity.this);
            pdlg.setMessage(SelectGeoPointActivity.this.getResources().getString(C0436R.string.loading));
            pdlg.setCancelable(false);
            pdlg.show();
            new Thread(new C04611(att, pdlg)).start();
        }
    }

    /* renamed from: com.vkontakte.android.SelectGeoPointActivity.5 */
    class C13495 implements OnMyLocationChangeListener {
        C13495() {
        }

        public void onMyLocationChange(Location ml) {
            if (!SelectGeoPointActivity.this.mapMoved || SelectGeoPointActivity.this.firstLocUpdate) {
                GoogleMap access$5 = SelectGeoPointActivity.this.map;
                SelectGeoPointActivity selectGeoPointActivity = SelectGeoPointActivity.this;
                LatLng latLng = new LatLng(ml.getLatitude(), ml.getLongitude());
                selectGeoPointActivity.selectedPoint = latLng;
                access$5.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
            }
            if (SelectGeoPointActivity.this.firstLocUpdate) {
                SelectGeoPointActivity.this.enableBtn(true);
                SelectGeoPointActivity.this.initMarker(ml.getLatitude(), ml.getLongitude());
                SelectGeoPointActivity.this.firstLocUpdate = false;
            }
        }
    }

    /* renamed from: com.vkontakte.android.SelectGeoPointActivity.6 */
    class C13506 implements OnCameraChangeListener {
        C13506() {
        }

        public void onCameraChange(CameraPosition pos) {
            SelectGeoPointActivity.this.mapMoved = true;
        }
    }

    /* renamed from: com.vkontakte.android.SelectGeoPointActivity.7 */
    class C13517 implements OnMapClickListener {
        C13517() {
        }

        public void onMapClick(LatLng ll) {
            if (SelectGeoPointActivity.this.marker != null) {
                SelectGeoPointActivity.this.marker.setPosition(ll);
            } else {
                SelectGeoPointActivity.this.initMarker(ll.latitude, ll.longitude);
                SelectGeoPointActivity.this.enableBtn(true);
            }
            SelectGeoPointActivity.this.selectedPoint = ll;
        }
    }

    /* renamed from: com.vkontakte.android.SelectGeoPointActivity.8 */
    class C13528 implements OnMarkerDragListener {
        C13528() {
        }

        public void onMarkerDragStart(Marker arg0) {
        }

        public void onMarkerDragEnd(Marker m) {
            SelectGeoPointActivity.this.selectedPoint = m.getPosition();
        }

        public void onMarkerDrag(Marker arg0) {
        }
    }

    public SelectGeoPointActivity() {
        this.firstLocUpdate = true;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.okBtn = View.inflate(this, C0436R.layout.ab_done_right, null);
        ((TextView) this.okBtn.findViewById(C0436R.id.ab_done_text)).setText(getIntent().getBooleanExtra("create_place", false) ? C0436R.string.done : C0436R.string.select);
        if (Global.isAppInstalled(this, "com.google.android.apps.maps") && GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext()) == 0) {
            enableBtn(false);
            this.okBtn.setOnClickListener(new C04624());
            this.mapView = new MapView(this);
            if (getIntent().getBooleanExtra("create_place", false)) {
                setTitle(C0436R.string.add_place);
                LinearLayout ll = new LinearLayout(this);
                ll.setOrientation(1);
                ll.addView(this.mapView, new LayoutParams(-1, -1, 1.0f));
                View sep = new View(this);
                sep.setBackgroundColor(-3158065);
                ll.addView(sep, new LayoutParams(-1, Global.scale(1.0f)));
                this.titleEdit = new EditText(this);
                this.titleEdit.setBackgroundResource(C0436R.drawable.bg_post_comments_top);
                this.titleEdit.setPadding(this.titleEdit.getPaddingLeft() + Global.scale(10.0f), this.titleEdit.getPaddingTop() + Global.scale(10.0f), this.titleEdit.getPaddingRight() + Global.scale(10.0f), this.titleEdit.getPaddingBottom() + Global.scale(10.0f));
                this.titleEdit.setInputType(524289);
                ll.addView(this.titleEdit, new LayoutParams(-1, -2));
                this.addressEdit = new EditText(this);
                this.addressEdit.setInputType(524289);
                this.addressEdit.setBackgroundResource(C0436R.drawable.bg_post_comments_btm);
                this.addressEdit.setPadding(this.addressEdit.getPaddingLeft() + Global.scale(10.0f), this.addressEdit.getPaddingTop() + Global.scale(10.0f), this.addressEdit.getPaddingRight() + Global.scale(10.0f), this.addressEdit.getPaddingBottom() + Global.scale(10.0f));
                ll.addView(this.addressEdit, new LayoutParams(-1, -2));
                this.titleEdit.setHint(C0436R.string.create_album_title);
                this.addressEdit.setHint(C0436R.string.address);
                this.titleEdit.setText(getIntent().getStringExtra("place_title"));
                this.addressEdit.setText(getIntent().getStringExtra("place_address"));
                setContentView((View) ll);
            } else {
                setContentView(this.mapView);
            }
            this.mapView.onCreate(b);
            this.map = this.mapView.getMap();
            if (this.map == null) {
                Toast.makeText(this, C0436R.string.error, 0).show();
                finish();
            }
            this.map.setMyLocationEnabled(true);
            try {
                MapsInitializer.initialize(this);
            } catch (Exception e) {
            }
            this.map.setOnMyLocationChangeListener(new C13495());
            this.map.setOnCameraChangeListener(new C13506());
            this.map.setOnMapClickListener(new C13517());
            return;
        }
        new Builder(this).setTitle(C0436R.string.maps_not_available).setMessage(C0436R.string.maps_not_available_descr).setPositiveButton(C0436R.string.open_google_play, new C04581()).setNegativeButton(C0436R.string.close, new C04592()).setOnCancelListener(new C04603()).show();
    }

    private void enableBtn(boolean enable) {
        this.okBtn.setEnabled(enable);
        if (VERSION.SDK_INT >= 11) {
            this.okBtn.findViewById(C0436R.id.ab_done_text).setAlpha(enable ? 1.0f : 0.5f);
            return;
        }
        ((TextView) this.okBtn.findViewById(C0436R.id.ab_done_text)).getCompoundDrawables()[0].setAlpha(enable ? MotionEventCompat.ACTION_MASK : TransportMediator.FLAG_KEY_MEDIA_NEXT);
        ((TextView) this.okBtn.findViewById(C0436R.id.ab_done_text)).setTextColor(enable ? -1 : -2130706433);
    }

    private void initMarker(double lat, double lon) {
        this.marker = this.map.addMarker(new MarkerOptions().position(new LatLng(lat, lon)).draggable(true));
        this.map.setOnMarkerDragListener(new C13528());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add((int) C0436R.string.send);
        item.setActionView(this.okBtn);
        item.setShowAsAction(2);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            setResult(0);
            finish();
        }
        return true;
    }

    public void onResume() {
        super.onResume();
        if (this.mapView != null) {
            this.mapView.onResume();
        }
    }

    public void onPause() {
        super.onPause();
        if (this.mapView != null) {
            this.mapView.onPause();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (VERSION.SDK_INT < 11) {
            ((TextView) this.okBtn.findViewById(C0436R.id.ab_done_text)).getCompoundDrawables()[0].setAlpha(MotionEventCompat.ACTION_MASK);
        }
        if (this.mapView != null) {
            this.mapView.onDestroy();
        }
    }
}
