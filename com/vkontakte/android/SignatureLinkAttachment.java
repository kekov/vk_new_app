package com.vkontakte.android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import java.io.DataOutputStream;
import java.io.IOException;
import org.acra.ACRAConstants;

public class SignatureLinkAttachment extends LinkAttachment {

    /* renamed from: com.vkontakte.android.SignatureLinkAttachment.1 */
    class C04921 implements OnClickListener {
        private final /* synthetic */ Context val$context;

        C04921(Context context) {
            this.val$context = context;
        }

        public void onClick(View v) {
            this.val$context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("vklink://view/?" + SignatureLinkAttachment.this.url)));
        }
    }

    public SignatureLinkAttachment(String _url, String _title) {
        super(_url, _title, ACRAConstants.DEFAULT_STRING_VALUE);
    }

    public View getViewForList(Context context, View reuse) {
        View v;
        if (reuse == null) {
            v = Attachment.getReusableView(context, "signature");
        } else {
            v = reuse;
        }
        this.title = new StringBuilder(String.valueOf(this.title)).toString();
        ((TextView) v.findViewById(C0436R.id.attach_title)).setText(this.title.length() > 0 ? this.title : context.getResources().getString(C0436R.string.attach_link));
        v.setOnClickListener(new C04921(context));
        return v;
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(15);
        os.writeUTF(this.url);
        os.writeUTF(this.title);
    }
}
