package com.vkontakte.android;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.vkontakte.android.fragments.PhotoViewerFragment;

public class PhotoViewerActivity extends SherlockFragmentActivity {
    private FrameLayout contentView;
    private SherlockFragment fragment;

    /* renamed from: com.vkontakte.android.PhotoViewerActivity.1 */
    class C03931 implements Runnable {
        C03931() {
        }

        public void run() {
            super.finish();
            PhotoViewerActivity.this.overridePendingTransition(0, 0);
        }
    }

    public void onCreate(Bundle b) {
        requestWindowFeature(9);
        super.onCreate(b);
        if (VERSION.SDK_INT < 9) {
            getWindow().setFormat(1);
        }
        if (!isTaskRoot()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        this.contentView = new FrameLayout(this);
        this.contentView.setId(C0436R.id.fragment_wrapper);
        setContentView(this.contentView);
        Bundle args = getIntent().getBundleExtra("args");
        try {
            this.fragment = (SherlockFragment) Class.forName("com.vkontakte.android.fragments." + getIntent().getStringExtra("class")).newInstance();
            this.fragment.setArguments(getIntent().getBundleExtra("args"));
            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, this.fragment, "news").commit();
        } catch (Exception e) {
            Toast.makeText(this, C0436R.string.error, 0).show();
            finish();
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        ((PhotoViewerFragment) this.fragment).onPrepareDismiss();
    }

    public void finish() {
        ((PhotoViewerFragment) this.fragment).animateOut(new C03931());
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        }
        onBackPressed();
        return true;
    }

    public void forceFinish() {
        super.finish();
    }
}
