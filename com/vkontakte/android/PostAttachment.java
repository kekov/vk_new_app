package com.vkontakte.android;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import java.io.DataOutputStream;
import java.io.IOException;

public class PostAttachment extends Attachment {
    public static final Creator<PostAttachment> CREATOR;
    public NewsEntry post;

    /* renamed from: com.vkontakte.android.PostAttachment.1 */
    class C04031 implements Creator<PostAttachment> {
        C04031() {
        }

        public PostAttachment createFromParcel(Parcel in) {
            return new PostAttachment((NewsEntry) in.readParcelable(ClassLoader.getSystemClassLoader()));
        }

        public PostAttachment[] newArray(int size) {
            return new PostAttachment[size];
        }
    }

    /* renamed from: com.vkontakte.android.PostAttachment.2 */
    class C04042 implements OnClickListener {
        C04042() {
        }

        public void onClick(View v) {
            Bundle args = new Bundle();
            args.putParcelable("entry", PostAttachment.this.post);
            Navigate.to("PostViewFragment", args, (Activity) v.getContext());
        }
    }

    public PostAttachment(NewsEntry entry) {
        this.post = entry;
    }

    static {
        CREATOR = new C04031();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.post, 0);
    }

    public View getFullView(Context context) {
        return getViewForList(context, null);
    }

    public View getViewForList(Context context, View reuse) {
        View v;
        if (reuse == null) {
            v = Attachment.getReusableView(context, "common");
        } else {
            v = reuse;
        }
        ((ImageView) v.findViewById(C0436R.id.attach_icon)).setImageResource(C0436R.drawable.ic_attach_post);
        ((TextView) v.findViewById(C0436R.id.attach_title)).setText(VKApplication.context.getResources().getString(C0436R.string.attach_wall_post));
        ((TextView) v.findViewById(C0436R.id.attach_subtitle)).setText(this.post.displayablePreviewText);
        v.setOnClickListener(new C04042());
        return v;
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(12);
        try {
            this.post.writeToStream(os);
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    public String toString() {
        return "wall" + this.post.ownerID + "_" + this.post.postID;
    }

    public LayoutParams getViewLayoutParams() {
        LayoutParams lp = new LayoutParams();
        lp.width = -1;
        lp.height = Global.scale(54.0f);
        return lp;
    }
}
