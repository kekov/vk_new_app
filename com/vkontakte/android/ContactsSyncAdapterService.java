package com.vkontakte.android;

import android.accounts.Account;
import android.accounts.OperationCanceledException;
import android.app.Service;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentProviderOperation.Builder;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.content.SyncStats;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.api.APIUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ContactsSyncAdapterService extends Service {
    private static long lastSyncTime;
    private static boolean needCancel;
    private static boolean needPausePhotoDownload;
    private static int numDlThreads;
    private static Object photoDownloadLock;
    private static ContentResolver resolver;
    private static boolean success;
    private static SyncAdapterImpl syncAdapter;
    private static int tries;

    private static class PhotoDownloader implements Runnable {
        Vector<PhotoUpdateRequest> reqs;
        Vector<PhotoUpdateRequest> results;

        public PhotoDownloader(Vector<PhotoUpdateRequest> _reqs, Vector<PhotoUpdateRequest> _results) {
            this.reqs = _reqs;
            this.results = _results;
        }

        public void run() {
            ContactsSyncAdapterService.numDlThreads = ContactsSyncAdapterService.numDlThreads + 1;
            while (this.reqs.size() > 0) {
                PhotoUpdateRequest req = (PhotoUpdateRequest) this.reqs.remove(0);
                req.data = Global.getURL(req.url);
                this.results.add(req);
                Log.m529v("vk", "Downloaded " + req.uid + " [size=" + (req.data != null ? Integer.valueOf(req.data.length) : "NULL") + "], " + req.url);
                if (ContactsSyncAdapterService.needPausePhotoDownload) {
                    synchronized (ContactsSyncAdapterService.photoDownloadLock) {
                        try {
                            ContactsSyncAdapterService.photoDownloadLock.wait();
                        } catch (InterruptedException e) {
                        }
                    }
                }
            }
            ContactsSyncAdapterService.numDlThreads = ContactsSyncAdapterService.numDlThreads - 1;
            Log.m529v("vk", "Photo DL thread exiting");
        }
    }

    private static class PhotoUpdateRequest {
        byte[] data;
        int existingID;
        int uid;
        String url;

        private PhotoUpdateRequest() {
        }
    }

    private static class SyncAdapterImpl extends AbstractThreadedSyncAdapter {
        private Context mContext;

        public SyncAdapterImpl(Context context) {
            super(context, true);
            this.mContext = context;
        }

        public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
            try {
                ContactsSyncAdapterService.performSync(this.mContext, account, extras, authority, provider, syncResult);
            } catch (OperationCanceledException e) {
            }
        }

        public void onSyncCanceled() {
            Log.m528i("vk", "Sync canceled");
            ContactsSyncAdapterService.needCancel = true;
        }
    }

    /* renamed from: com.vkontakte.android.ContactsSyncAdapterService.1 */
    class C12801 extends APIHandler {
        private final /* synthetic */ Vector val$contacts;
        private final /* synthetic */ boolean val$syncAllFriends;
        private final /* synthetic */ SyncResult val$syncResult;

        C12801(Vector vector, boolean z, SyncResult syncResult) {
            this.val$contacts = vector;
            this.val$syncAllFriends = z;
            this.val$syncResult = syncResult;
        }

        public void success(JSONObject r) {
            try {
                JSONArray a = r.getJSONArray("response");
                for (int i = 0; i < a.length(); i++) {
                    this.val$contacts.add(ContactsSyncAdapterService.getProfile(a.getJSONObject(i)));
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }

        public void fail(int ecode, String emsg) {
            Log.m526e("vk", "Sync error " + ecode + " " + emsg);
            if (!this.val$syncAllFriends) {
                this.val$syncResult.databaseError = true;
            }
        }
    }

    /* renamed from: com.vkontakte.android.ContactsSyncAdapterService.2 */
    class C12812 extends APIHandler {
        private final /* synthetic */ Vector val$contacts;

        C12812(Vector vector) {
            this.val$contacts = vector;
        }

        public void success(JSONObject r) {
            try {
                JSONArray a = APIUtils.unwrapArray(r, "response").array;
                Log.m530w("vk", "COUNT = " + a.length());
                for (int i = 0; i < a.length(); i++) {
                    JSONObject user = a.getJSONObject(i);
                    boolean contains = false;
                    int uid = user.getInt("id");
                    Iterator it = this.val$contacts.iterator();
                    while (it.hasNext()) {
                        if (((ExtendedUserProfile) it.next()).uid == uid) {
                            contains = true;
                            break;
                        }
                    }
                    if (!contains) {
                        this.val$contacts.add(ContactsSyncAdapterService.getProfile(user));
                    }
                }
                Log.m530w("vk", "Res COUNT = " + this.val$contacts.size());
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
            ContactsSyncAdapterService.success = true;
        }

        public void fail(int ecode, String emsg) {
            Log.m526e("vk", "Sync error " + ecode + " " + emsg);
            ContactsSyncAdapterService.tries = ContactsSyncAdapterService.tries - 1;
        }
    }

    public static class ExtendedUserProfile extends UserProfile {
        String bdate;
        String nickname;
        String phone;
    }

    static {
        lastSyncTime = 0;
        needCancel = false;
        success = false;
        needPausePhotoDownload = false;
        photoDownloadLock = new Object();
        numDlThreads = 0;
    }

    public IBinder onBind(Intent arg0) {
        return getSyncAdapter().getSyncAdapterBinder();
    }

    private SyncAdapterImpl getSyncAdapter() {
        if (syncAdapter == null) {
            syncAdapter = new SyncAdapterImpl(this);
        }
        return syncAdapter;
    }

    private static String zInt(String i) {
        if (i.length() == 1) {
            return "0" + i;
        }
        return i;
    }

    private static ExtendedUserProfile getProfile(JSONObject user) throws JSONException {
        ExtendedUserProfile up = new ExtendedUserProfile();
        up.uid = user.getInt("id");
        up.firstName = user.getString("first_name");
        up.lastName = user.getString("last_name");
        up.photo = user.optString("photo_200", user.getString("photo_medium_rec"));
        up.phone = null;
        if (user.has("phone")) {
            up.phone = user.getString("phone");
        } else if (user.has("mobile_phone")) {
            char[] pc = user.getString("mobile_phone").toCharArray();
            String ph = ACRAConstants.DEFAULT_STRING_VALUE;
            for (char c : pc) {
                if (Character.isDigit(c) || c == '+') {
                    ph = new StringBuilder(String.valueOf(ph)).append(c).toString();
                }
            }
            if (ph.length() > 0) {
                up.phone = ph;
            }
        }
        up.nickname = user.optString("nickname");
        if (user.has("bdate")) {
            String[] bd = user.getString("bdate").split("\\.");
            if (bd.length == 3) {
                up.bdate = bd[2] + "-" + zInt(bd[1]) + "-" + zInt(bd[0]);
            }
            if (bd.length == 2) {
                up.bdate = "0000-" + zInt(bd[1]) + "-" + zInt(bd[0]);
            }
        } else {
            up.bdate = null;
        }
        return up;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void performSync(android.content.Context r35, android.accounts.Account r36, android.os.Bundle r37, java.lang.String r38, android.content.ContentProviderClient r39, android.content.SyncResult r40) throws android.accounts.OperationCanceledException {
        /*
        r2 = r35.getContentResolver();	 Catch:{ Exception -> 0x0212 }
        resolver = r2;	 Catch:{ Exception -> 0x0212 }
        r2 = "vk";
        r3 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0212 }
        r5 = "performSync: ";
        r3.<init>(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = r36.toString();	 Catch:{ Exception -> 0x0212 }
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r3 = r3.toString();	 Catch:{ Exception -> 0x0212 }
        com.vkontakte.android.Log.m528i(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r2 = r35.getApplicationContext();	 Catch:{ Exception -> 0x0212 }
        com.vkontakte.android.VKApplication.context = r2;	 Catch:{ Exception -> 0x0212 }
        r40.clear();	 Catch:{ Exception -> 0x0212 }
        r2 = 0;
        r0 = r40;
        r0.fullSyncRequested = r2;	 Catch:{ Exception -> 0x0212 }
        r2 = java.lang.System.currentTimeMillis();	 Catch:{ Exception -> 0x0212 }
        r8 = lastSyncTime;	 Catch:{ Exception -> 0x0212 }
        r2 = r2 - r8;
        r8 = 300000; // 0x493e0 float:4.2039E-40 double:1.482197E-318;
        r2 = (r2 > r8 ? 1 : (r2 == r8 ? 0 : -1));
        if (r2 >= 0) goto L_0x0042;
    L_0x003a:
        r2 = "vk";
        r3 = "too many retries";
        com.vkontakte.android.Log.m526e(r2, r3);	 Catch:{ Exception -> 0x0212 }
    L_0x0041:
        return;
    L_0x0042:
        r2 = r35.getApplicationContext();	 Catch:{ Exception -> 0x0212 }
        r3 = 0;
        r5 = 0;
        r26 = r2.getSharedPreferences(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r2 = "sid";
        r0 = r26;
        r2 = r0.contains(r2);	 Catch:{ Exception -> 0x0212 }
        if (r2 == 0) goto L_0x008f;
    L_0x0056:
        r2 = "uid";
        r3 = 0;
        r0 = r26;
        r2 = r0.getInt(r2, r3);	 Catch:{ Exception -> 0x0212 }
        com.vkontakte.android.Global.uid = r2;	 Catch:{ Exception -> 0x0212 }
        r2 = "sid";
        r3 = 0;
        r0 = r26;
        r2 = r0.getString(r2, r3);	 Catch:{ Exception -> 0x0212 }
        com.vkontakte.android.Global.accessToken = r2;	 Catch:{ Exception -> 0x0212 }
        r2 = "secret";
        r3 = 0;
        r0 = r26;
        r2 = r0.getString(r2, r3);	 Catch:{ Exception -> 0x0212 }
        com.vkontakte.android.Global.secret = r2;	 Catch:{ Exception -> 0x0212 }
        r2 = org.acra.ErrorReporter.getInstance();	 Catch:{ Exception -> 0x0212 }
        r3 = "vk_uid";
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0212 }
        r8 = com.vkontakte.android.Global.uid;	 Catch:{ Exception -> 0x0212 }
        r8 = java.lang.String.valueOf(r8);	 Catch:{ Exception -> 0x0212 }
        r5.<init>(r8);	 Catch:{ Exception -> 0x0212 }
        r5 = r5.toString();	 Catch:{ Exception -> 0x0212 }
        r2.putCustomData(r3, r5);	 Catch:{ Exception -> 0x0212 }
    L_0x008f:
        r2 = "sync_all";
        r3 = 0;
        r0 = r26;
        r30 = r0.getBoolean(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r14 = new java.util.Vector;	 Catch:{ Exception -> 0x0212 }
        r14.<init>();	 Catch:{ Exception -> 0x0212 }
        r2 = resolver;	 Catch:{ Exception -> 0x0212 }
        r22 = getLocalPhoneNumbers(r2);	 Catch:{ Exception -> 0x0212 }
        if (r22 == 0) goto L_0x00ab;
    L_0x00a5:
        r2 = r22.size();	 Catch:{ Exception -> 0x0212 }
        if (r2 != 0) goto L_0x0221;
    L_0x00ab:
        r2 = "vk";
        r3 = "No local numbers";
        com.vkontakte.android.Log.m526e(r2, r3);	 Catch:{ Exception -> 0x0212 }
        if (r30 == 0) goto L_0x0041;
    L_0x00b4:
        r0 = r40;
        r2 = r0.databaseError;	 Catch:{ Exception -> 0x0212 }
        if (r2 != 0) goto L_0x0041;
    L_0x00ba:
        if (r30 == 0) goto L_0x00d3;
    L_0x00bc:
        r2 = 0;
        success = r2;	 Catch:{ Exception -> 0x0212 }
        r2 = 3;
        tries = r2;	 Catch:{ Exception -> 0x0212 }
    L_0x00c2:
        r2 = tries;	 Catch:{ Exception -> 0x0212 }
        if (r2 <= 0) goto L_0x00ca;
    L_0x00c6:
        r2 = success;	 Catch:{ Exception -> 0x0212 }
        if (r2 == 0) goto L_0x0257;
    L_0x00ca:
        r2 = success;	 Catch:{ Exception -> 0x0212 }
        if (r2 != 0) goto L_0x00d3;
    L_0x00ce:
        r2 = 1;
        r0 = r40;
        r0.databaseError = r2;	 Catch:{ Exception -> 0x0212 }
    L_0x00d3:
        r0 = r40;
        r2 = r0.databaseError;	 Catch:{ Exception -> 0x0212 }
        if (r2 != 0) goto L_0x0041;
    L_0x00d9:
        r19 = getLocalUids();	 Catch:{ Exception -> 0x0212 }
        r20 = r19.size();	 Catch:{ Exception -> 0x0212 }
        r6 = new java.util.ArrayList;	 Catch:{ Exception -> 0x0212 }
        r6.<init>();	 Catch:{ Exception -> 0x0212 }
        r7 = new java.util.Vector;	 Catch:{ Exception -> 0x0212 }
        r7.<init>();	 Catch:{ Exception -> 0x0212 }
        r25 = new java.util.Vector;	 Catch:{ Exception -> 0x0212 }
        r25.<init>();	 Catch:{ Exception -> 0x0212 }
        r8 = r14.iterator();	 Catch:{ Exception -> 0x0212 }
    L_0x00f4:
        r2 = r8.hasNext();	 Catch:{ Exception -> 0x0212 }
        if (r2 != 0) goto L_0x0274;
    L_0x00fa:
        r2 = r6.size();	 Catch:{ Exception -> 0x0212 }
        if (r2 <= 0) goto L_0x02db;
    L_0x0100:
        r2 = "vk";
        r3 = "Applying DB changes!";
        com.vkontakte.android.Log.m528i(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r2 = resolver;	 Catch:{ Exception -> 0x02ca }
        r3 = "com.android.contacts";
        r2.applyBatch(r3, r6);	 Catch:{ Exception -> 0x02ca }
        r2 = "vk";
        r3 = "Contacts updated";
        com.vkontakte.android.Log.m528i(r2, r3);	 Catch:{ Exception -> 0x0212 }
    L_0x0115:
        r2 = "vk";
        r3 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0212 }
        r5 = "Will update ";
        r3.<init>(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = r7.size();	 Catch:{ Exception -> 0x0212 }
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = " photos...";
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r3 = r3.toString();	 Catch:{ Exception -> 0x0212 }
        com.vkontakte.android.Log.m528i(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r32 = new java.util.ArrayList;	 Catch:{ Exception -> 0x0212 }
        r32.<init>();	 Catch:{ Exception -> 0x0212 }
        r18 = 0;
    L_0x013a:
        r2 = 5;
        r0 = r18;
        if (r0 < r2) goto L_0x02e4;
    L_0x013f:
        r2 = android.provider.ContactsContract.Data.CONTENT_URI;	 Catch:{ Exception -> 0x0212 }
        r2 = r2.buildUpon();	 Catch:{ Exception -> 0x0212 }
        r3 = "caller_is_syncadapter";
        r5 = "true";
        r2 = r2.appendQueryParameter(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r16 = r2.build();	 Catch:{ Exception -> 0x0212 }
    L_0x0151:
        r2 = r7.size();	 Catch:{ Exception -> 0x0212 }
        if (r2 > 0) goto L_0x0316;
    L_0x0157:
        r2 = r25.size();	 Catch:{ Exception -> 0x0212 }
        if (r2 > 0) goto L_0x0316;
    L_0x015d:
        r2 = numDlThreads;	 Catch:{ Exception -> 0x0212 }
        if (r2 > 0) goto L_0x0316;
    L_0x0161:
        r17 = new java.util.ArrayList;	 Catch:{ Exception -> 0x0212 }
        r17.<init>();	 Catch:{ Exception -> 0x0212 }
        r2 = r19.size();	 Catch:{ Exception -> 0x0212 }
        if (r2 <= 0) goto L_0x01cf;
    L_0x016c:
        r2 = r19.size();	 Catch:{ Exception -> 0x0212 }
        r0 = r20;
        if (r2 >= r0) goto L_0x01cf;
    L_0x0174:
        r33 = "";
        r3 = r19.iterator();	 Catch:{ Exception -> 0x0212 }
    L_0x017a:
        r2 = r3.hasNext();	 Catch:{ Exception -> 0x0212 }
        if (r2 != 0) goto L_0x0475;
    L_0x0180:
        r2 = 1;
        r0 = r33;
        r33 = r0.substring(r2);	 Catch:{ Exception -> 0x0212 }
        r0 = r40;
        r2 = r0.stats;	 Catch:{ Exception -> 0x0212 }
        r3 = r19.size();	 Catch:{ Exception -> 0x0212 }
        r8 = (long) r3;	 Catch:{ Exception -> 0x0212 }
        r2.numDeletes = r8;	 Catch:{ Exception -> 0x0212 }
        r2 = android.provider.ContactsContract.RawContacts.CONTENT_URI;	 Catch:{ Exception -> 0x0212 }
        r2 = r2.buildUpon();	 Catch:{ Exception -> 0x0212 }
        r3 = "caller_is_syncadapter";
        r5 = "true";
        r2 = r2.appendQueryParameter(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r2 = r2.build();	 Catch:{ Exception -> 0x0212 }
        r2 = android.content.ContentProviderOperation.newDelete(r2);	 Catch:{ Exception -> 0x0212 }
        r3 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0212 }
        r5 = "account_type='com.vkontakte.account' AND sync1 IN (";
        r3.<init>(r5);	 Catch:{ Exception -> 0x0212 }
        r0 = r33;
        r3 = r3.append(r0);	 Catch:{ Exception -> 0x0212 }
        r5 = ")";
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r3 = r3.toString();	 Catch:{ Exception -> 0x0212 }
        r5 = 0;
        r2 = r2.withSelection(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r23 = r2.build();	 Catch:{ Exception -> 0x0212 }
        r0 = r17;
        r1 = r23;
        r0.add(r1);	 Catch:{ Exception -> 0x0212 }
    L_0x01cf:
        r2 = r17.size();	 Catch:{ Exception -> 0x0212 }
        if (r2 <= 0) goto L_0x049a;
    L_0x01d5:
        r2 = "vk";
        r3 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0212 }
        r5 = "Deleting ";
        r3.<init>(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = r17.size();	 Catch:{ Exception -> 0x0212 }
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = " contacts";
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r3 = r3.toString();	 Catch:{ Exception -> 0x0212 }
        com.vkontakte.android.Log.m528i(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r2 = resolver;	 Catch:{ Exception -> 0x0212 }
        r3 = "com.android.contacts";
        r0 = r17;
        r2.applyBatch(r3, r0);	 Catch:{ Exception -> 0x0212 }
        r2 = "vk";
        r3 = "OK!";
        com.vkontakte.android.Log.m528i(r2, r3);	 Catch:{ Exception -> 0x0212 }
    L_0x0203:
        r2 = "vk";
        r3 = "Sync done!";
        com.vkontakte.android.Log.m528i(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r2 = java.lang.System.currentTimeMillis();	 Catch:{ Exception -> 0x0212 }
        lastSyncTime = r2;	 Catch:{ Exception -> 0x0212 }
        goto L_0x0041;
    L_0x0212:
        r34 = move-exception;
        r2 = "vk";
        r0 = r34;
        com.vkontakte.android.Log.m532w(r2, r0);
        r2 = 1;
        r0 = r40;
        r0.databaseError = r2;
        goto L_0x0041;
    L_0x0221:
        r2 = ",";
        r0 = r22;
        r21 = android.text.TextUtils.join(r2, r0);	 Catch:{ Exception -> 0x0212 }
        r2 = new com.vkontakte.android.APIRequest;	 Catch:{ Exception -> 0x0212 }
        r3 = "friends.getByPhones";
        r2.<init>(r3);	 Catch:{ Exception -> 0x0212 }
        r3 = 1;
        r2 = r2.forceHTTPS(r3);	 Catch:{ Exception -> 0x0212 }
        r3 = "fields";
        r5 = "photo_medium_rec,photo_200,bdate,contacts";
        r2 = r2.param(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r3 = "phones";
        r0 = r21;
        r2 = r2.param(r3, r0);	 Catch:{ Exception -> 0x0212 }
        r3 = new com.vkontakte.android.ContactsSyncAdapterService$1;	 Catch:{ Exception -> 0x0212 }
        r0 = r30;
        r1 = r40;
        r3.<init>(r14, r0, r1);	 Catch:{ Exception -> 0x0212 }
        r2 = r2.handler(r3);	 Catch:{ Exception -> 0x0212 }
        r2.execSync();	 Catch:{ Exception -> 0x0212 }
        goto L_0x00b4;
    L_0x0257:
        r2 = new com.vkontakte.android.APIRequest;	 Catch:{ Exception -> 0x0212 }
        r3 = "friends.get";
        r2.<init>(r3);	 Catch:{ Exception -> 0x0212 }
        r3 = "fields";
        r5 = "photo_medium_rec,photo_200,bdate,contacts";
        r2 = r2.param(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r3 = new com.vkontakte.android.ContactsSyncAdapterService$2;	 Catch:{ Exception -> 0x0212 }
        r3.<init>(r14);	 Catch:{ Exception -> 0x0212 }
        r2 = r2.handler(r3);	 Catch:{ Exception -> 0x0212 }
        r2.execSync();	 Catch:{ Exception -> 0x0212 }
        goto L_0x00c2;
    L_0x0274:
        r4 = r8.next();	 Catch:{ Exception -> 0x0212 }
        r4 = (com.vkontakte.android.ContactsSyncAdapterService.ExtendedUserProfile) r4;	 Catch:{ Exception -> 0x0212 }
        r0 = r40;
        r5 = r0.stats;	 Catch:{ Exception -> 0x0212 }
        r2 = r35;
        r3 = r36;
        r2 = addOrUpdateContact(r2, r3, r4, r5, r6, r7);	 Catch:{ Exception -> 0x0212 }
        if (r2 != 0) goto L_0x028f;
    L_0x0288:
        r2 = "vk";
        r3 = "Update contact error!!!";
        com.vkontakte.android.Log.m526e(r2, r3);	 Catch:{ Exception -> 0x0212 }
    L_0x028f:
        r2 = r4.uid;	 Catch:{ Exception -> 0x0212 }
        r2 = java.lang.Integer.valueOf(r2);	 Catch:{ Exception -> 0x0212 }
        r0 = r19;
        r0.remove(r2);	 Catch:{ Exception -> 0x0212 }
        r2 = r6.size();	 Catch:{ Exception -> 0x0212 }
        r3 = 50;
        if (r2 < r3) goto L_0x02ba;
    L_0x02a2:
        r2 = "vk";
        r3 = "Applying DB changes!";
        com.vkontakte.android.Log.m528i(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r2 = resolver;	 Catch:{ Exception -> 0x0212 }
        r3 = "com.android.contacts";
        r2.applyBatch(r3, r6);	 Catch:{ Exception -> 0x0212 }
        r6.clear();	 Catch:{ Exception -> 0x0212 }
        r2 = "vk";
        r3 = "Contacts updated";
        com.vkontakte.android.Log.m528i(r2, r3);	 Catch:{ Exception -> 0x0212 }
    L_0x02ba:
        r2 = needCancel;	 Catch:{ Exception -> 0x0212 }
        if (r2 == 0) goto L_0x00f4;
    L_0x02be:
        r2 = "vk";
        r3 = "need cancel!";
        com.vkontakte.android.Log.m526e(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r2 = 0;
        needCancel = r2;	 Catch:{ Exception -> 0x0212 }
        goto L_0x0041;
    L_0x02ca:
        r34 = move-exception;
        r2 = "vk";
        r3 = "OH SHI~";
        r0 = r34;
        com.vkontakte.android.Log.m527e(r2, r3, r0);	 Catch:{ Exception -> 0x0212 }
        r2 = 1;
        r0 = r40;
        r0.databaseError = r2;	 Catch:{ Exception -> 0x0212 }
        goto L_0x0041;
    L_0x02db:
        r2 = "vk";
        r3 = "Nothing to update";
        com.vkontakte.android.Log.m528i(r2, r3);	 Catch:{ Exception -> 0x0212 }
        goto L_0x0115;
    L_0x02e4:
        r31 = new java.lang.Thread;	 Catch:{ Exception -> 0x0212 }
        r2 = new com.vkontakte.android.ContactsSyncAdapterService$PhotoDownloader;	 Catch:{ Exception -> 0x0212 }
        r0 = r25;
        r2.<init>(r7, r0);	 Catch:{ Exception -> 0x0212 }
        r0 = r31;
        r0.<init>(r2);	 Catch:{ Exception -> 0x0212 }
        r0 = r32;
        r1 = r31;
        r0.add(r1);	 Catch:{ Exception -> 0x0212 }
        r31.start();	 Catch:{ Exception -> 0x0212 }
        r2 = "vk";
        r3 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0212 }
        r5 = "Started DL thread #";
        r3.<init>(r5);	 Catch:{ Exception -> 0x0212 }
        r0 = r18;
        r3 = r3.append(r0);	 Catch:{ Exception -> 0x0212 }
        r3 = r3.toString();	 Catch:{ Exception -> 0x0212 }
        com.vkontakte.android.Log.m525d(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r18 = r18 + 1;
        goto L_0x013a;
    L_0x0316:
        r2 = "vk";
        r3 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0212 }
        r5 = "updates ";
        r3.<init>(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = r7.size();	 Catch:{ Exception -> 0x0212 }
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = ", results ";
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = r25.size();	 Catch:{ Exception -> 0x0212 }
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = ", threads ";
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = numDlThreads;	 Catch:{ Exception -> 0x0212 }
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r3 = r3.toString();	 Catch:{ Exception -> 0x0212 }
        com.vkontakte.android.Log.m525d(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r2 = r25.size();	 Catch:{ Exception -> 0x0212 }
        r3 = 20;
        if (r2 >= r3) goto L_0x0354;
    L_0x0350:
        r2 = numDlThreads;	 Catch:{ Exception -> 0x0212 }
        if (r2 != 0) goto L_0x039c;
    L_0x0354:
        r2 = 1;
        needPausePhotoDownload = r2;	 Catch:{ Exception -> 0x0212 }
        r24 = new java.util.ArrayList;	 Catch:{ Exception -> 0x0212 }
        r24.<init>();	 Catch:{ Exception -> 0x0212 }
    L_0x035c:
        r2 = r25.size();	 Catch:{ Exception -> 0x0212 }
        if (r2 > 0) goto L_0x03a3;
    L_0x0362:
        r2 = "vk";
        r3 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0212 }
        r5 = "Updating ";
        r3.<init>(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = r24.size();	 Catch:{ Exception -> 0x0212 }
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = " photos";
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r3 = r3.toString();	 Catch:{ Exception -> 0x0212 }
        com.vkontakte.android.Log.m525d(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r2 = resolver;	 Catch:{ Exception -> 0x0212 }
        r3 = "com.android.contacts";
        r0 = r24;
        r2.applyBatch(r3, r0);	 Catch:{ Exception -> 0x0212 }
        r2 = "vk";
        r3 = "Update done...";
        com.vkontakte.android.Log.m529v(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r2 = 0;
        needPausePhotoDownload = r2;	 Catch:{ Exception -> 0x0212 }
        r3 = photoDownloadLock;	 Catch:{ Exception -> 0x0212 }
        monitor-enter(r3);	 Catch:{ Exception -> 0x0212 }
        r2 = photoDownloadLock;	 Catch:{ all -> 0x0472 }
        r2.notifyAll();	 Catch:{ all -> 0x0472 }
        monitor-exit(r3);	 Catch:{ all -> 0x0472 }
    L_0x039c:
        r2 = 10;
        java.lang.Thread.sleep(r2);	 Catch:{ Exception -> 0x0212 }
        goto L_0x0151;
    L_0x03a3:
        r2 = "vk";
        r3 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0212 }
        r5 = "res ";
        r3.<init>(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = r25.size();	 Catch:{ Exception -> 0x0212 }
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r3 = r3.toString();	 Catch:{ Exception -> 0x0212 }
        com.vkontakte.android.Log.m528i(r2, r3);	 Catch:{ Exception -> 0x0212 }
        r2 = 0;
        r0 = r25;
        r27 = r0.remove(r2);	 Catch:{ Exception -> 0x0212 }
        r27 = (com.vkontakte.android.ContactsSyncAdapterService.PhotoUpdateRequest) r27;	 Catch:{ Exception -> 0x0212 }
        r0 = r27;
        r2 = r0.existingID;	 Catch:{ Exception -> 0x0212 }
        if (r2 <= 0) goto L_0x0405;
    L_0x03ca:
        r2 = android.content.ContentProviderOperation.newUpdate(r16);	 Catch:{ Exception -> 0x0212 }
        r3 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0212 }
        r5 = "_id=";
        r3.<init>(r5);	 Catch:{ Exception -> 0x0212 }
        r0 = r27;
        r5 = r0.existingID;	 Catch:{ Exception -> 0x0212 }
        r3 = r3.append(r5);	 Catch:{ Exception -> 0x0212 }
        r3 = r3.toString();	 Catch:{ Exception -> 0x0212 }
        r5 = 0;
        r2 = r2.withSelection(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r3 = "data15";
        r0 = r27;
        r5 = r0.data;	 Catch:{ Exception -> 0x0212 }
        r2 = r2.withValue(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r3 = "data_sync1";
        r0 = r27;
        r5 = r0.url;	 Catch:{ Exception -> 0x0212 }
        r2 = r2.withValue(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r2 = r2.build();	 Catch:{ Exception -> 0x0212 }
        r0 = r24;
        r0.add(r2);	 Catch:{ Exception -> 0x0212 }
        goto L_0x035c;
    L_0x0405:
        r8 = resolver;	 Catch:{ Exception -> 0x0212 }
        r9 = android.provider.ContactsContract.RawContacts.CONTENT_URI;	 Catch:{ Exception -> 0x0212 }
        r2 = 1;
        r10 = new java.lang.String[r2];	 Catch:{ Exception -> 0x0212 }
        r2 = 0;
        r3 = "_id";
        r10[r2] = r3;	 Catch:{ Exception -> 0x0212 }
        r2 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0212 }
        r3 = "sync1=";
        r2.<init>(r3);	 Catch:{ Exception -> 0x0212 }
        r0 = r27;
        r3 = r0.uid;	 Catch:{ Exception -> 0x0212 }
        r2 = r2.append(r3);	 Catch:{ Exception -> 0x0212 }
        r11 = r2.toString();	 Catch:{ Exception -> 0x0212 }
        r12 = 0;
        r13 = 0;
        r15 = r8.query(r9, r10, r11, r12, r13);	 Catch:{ Exception -> 0x0212 }
        if (r15 == 0) goto L_0x035c;
    L_0x042c:
        r2 = r15.getCount();	 Catch:{ Exception -> 0x0212 }
        if (r2 <= 0) goto L_0x035c;
    L_0x0432:
        r15.moveToFirst();	 Catch:{ Exception -> 0x0212 }
        r2 = 0;
        r28 = r15.getInt(r2);	 Catch:{ Exception -> 0x0212 }
        r15.close();	 Catch:{ Exception -> 0x0212 }
        r2 = android.content.ContentProviderOperation.newInsert(r16);	 Catch:{ Exception -> 0x0212 }
        r3 = "raw_contact_id";
        r5 = java.lang.Integer.valueOf(r28);	 Catch:{ Exception -> 0x0212 }
        r2 = r2.withValue(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r3 = "mimetype";
        r5 = "vnd.android.cursor.item/photo";
        r2 = r2.withValue(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r3 = "data15";
        r0 = r27;
        r5 = r0.data;	 Catch:{ Exception -> 0x0212 }
        r2 = r2.withValue(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r3 = "data_sync1";
        r0 = r27;
        r5 = r0.url;	 Catch:{ Exception -> 0x0212 }
        r2 = r2.withValue(r3, r5);	 Catch:{ Exception -> 0x0212 }
        r2 = r2.build();	 Catch:{ Exception -> 0x0212 }
        r0 = r24;
        r0.add(r2);	 Catch:{ Exception -> 0x0212 }
        goto L_0x035c;
    L_0x0472:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0472 }
        throw r2;	 Catch:{ Exception -> 0x0212 }
    L_0x0475:
        r2 = r3.next();	 Catch:{ Exception -> 0x0212 }
        r2 = (java.lang.Integer) r2;	 Catch:{ Exception -> 0x0212 }
        r29 = r2.intValue();	 Catch:{ Exception -> 0x0212 }
        r2 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0212 }
        r5 = java.lang.String.valueOf(r33);	 Catch:{ Exception -> 0x0212 }
        r2.<init>(r5);	 Catch:{ Exception -> 0x0212 }
        r5 = ",";
        r2 = r2.append(r5);	 Catch:{ Exception -> 0x0212 }
        r0 = r29;
        r2 = r2.append(r0);	 Catch:{ Exception -> 0x0212 }
        r33 = r2.toString();	 Catch:{ Exception -> 0x0212 }
        goto L_0x017a;
    L_0x049a:
        r2 = "vk";
        r3 = "Nothing to delete";
        com.vkontakte.android.Log.m528i(r2, r3);	 Catch:{ Exception -> 0x0212 }
        goto L_0x0203;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.ContactsSyncAdapterService.performSync(android.content.Context, android.accounts.Account, android.os.Bundle, java.lang.String, android.content.ContentProviderClient, android.content.SyncResult):void");
    }

    private static Vector<Integer> getLocalUids() {
        Vector<Integer> ret = new Vector();
        Cursor cursor = resolver.query(RawContacts.CONTENT_URI, new String[]{"sync1"}, "account_type='com.vkontakte.account'", null, null);
        if (cursor == null || cursor.getCount() <= 0) {
            if (cursor != null) {
                cursor.close();
            }
            return ret;
        }
        cursor.moveToFirst();
        do {
            ret.add(Integer.valueOf(cursor.getInt(cursor.getColumnIndex("sync1"))));
        } while (cursor.moveToNext());
        if (cursor != null) {
            cursor.close();
        }
        return ret;
    }

    private static boolean addOrUpdateContact(Context context, Account account, ExtendedUserProfile p, SyncStats stats, ArrayList<ContentProviderOperation> operationList, Vector<PhotoUpdateRequest> photoUpdates) {
        boolean update = false;
        boolean photoChanged = true;
        boolean nameChanged = true;
        boolean bdateChanged = true;
        boolean phoneChanged = true;
        boolean bdateExists = false;
        boolean phoneExists = false;
        int updateContactID = 0;
        int existingPhotoID = -1;
        Uri dataUri = Data.CONTENT_URI.buildUpon().appendQueryParameter("caller_is_syncadapter", "true").build();
        boolean hasIm = false;
        Cursor cursor = resolver.query(RawContacts.CONTENT_URI, new String[]{"_id"}, "account_type='com.vkontakte.account' AND sync1=" + p.uid, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            update = true;
            cursor.moveToFirst();
            updateContactID = cursor.getInt(cursor.getColumnIndex("_id"));
            Log.m525d("vk", "Contact '" + p.firstName + " " + p.lastName + "' already exists, updating, ID=" + updateContactID);
            cursor = resolver.query(Data.CONTENT_URI, new String[]{"_id", "mimetype", "data_sync1", "data1", "data2", "data3"}, "raw_contact_id=" + updateContactID, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    try {
                        String type = cursor.getString(cursor.getColumnIndex("mimetype"));
                        if (type.equals("vnd.android.cursor.item/photo")) {
                            photoChanged = !cursor.getString(cursor.getColumnIndex("data_sync1")).equals(p.photo);
                            existingPhotoID = cursor.getInt(cursor.getColumnIndex("_id"));
                        } else {
                            if (type.equals("vnd.android.cursor.item/name")) {
                                nameChanged = (p.firstName.equals(cursor.getString(cursor.getColumnIndex("data2"))) && p.lastName.equals(cursor.getString(cursor.getColumnIndex("data3")))) ? false : true;
                            } else {
                                if (type.equals("vnd.android.cursor.item/phone_v2")) {
                                    phoneChanged = !cursor.getString(cursor.getColumnIndex("data1")).equals(p.phone);
                                    phoneExists = true;
                                } else {
                                    if (type.equals("vnd.android.cursor.item/contact_event")) {
                                        bdateChanged = !cursor.getString(cursor.getColumnIndex("data1")).equals(p.bdate);
                                        bdateExists = true;
                                    } else {
                                        if (type.equals("vnd.android.cursor.item/vnd.com.vkontakte.android.sendmsg")) {
                                            hasIm = true;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                } while (cursor.moveToNext());
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        if (!update) {
            Log.m528i("vk", "Contact '" + p.firstName + " " + p.lastName + "' does not exist, creating");
        }
        int valueBack = 0;
        if (!update) {
            Builder builder = ContentProviderOperation.newInsert(RawContacts.CONTENT_URI.buildUpon().appendQueryParameter("caller_is_syncadapter", "true").build());
            builder.withValue("account_name", account.name);
            builder.withValue("account_type", account.type);
            builder.withValue("sync1", Integer.valueOf(p.uid));
            operationList.add(builder.build());
            valueBack = operationList.size() - 1;
            Log.m528i("vk", "New insert, valueBack=" + valueBack);
        }
        if (nameChanged) {
            if (update) {
                builder = ContentProviderOperation.newUpdate(dataUri);
                builder.withSelection("raw_contact_id=" + updateContactID + " AND " + "mimetype" + "='" + "vnd.android.cursor.item/name" + "'", null);
            } else {
                builder = ContentProviderOperation.newInsert(dataUri);
                builder.withValueBackReference("raw_contact_id", valueBack);
            }
            builder.withValue("mimetype", "vnd.android.cursor.item/name");
            builder.withValue("data2", p.firstName);
            builder.withValue("data3", p.lastName);
            operationList.add(builder.build());
            Log.m525d("vk", "Updating name");
        }
        if (photoChanged && !p.photo.endsWith(".gif")) {
            PhotoUpdateRequest photoUpdateRequest = new PhotoUpdateRequest();
            photoUpdateRequest.uid = p.uid;
            photoUpdateRequest.url = p.photo;
            photoUpdateRequest.existingID = existingPhotoID;
            photoUpdates.add(photoUpdateRequest);
            Log.m525d("vk", "Updating photo");
        }
        if (p.phone != null && !"null".equals(p.phone) && p.phone.length() > 0 && phoneChanged) {
            if (!update) {
                builder = ContentProviderOperation.newInsert(dataUri);
                builder.withValueBackReference("raw_contact_id", valueBack);
            } else if (phoneExists) {
                builder = ContentProviderOperation.newUpdate(dataUri);
                builder.withSelection("raw_contact_id=" + updateContactID + " AND " + "mimetype" + "='" + "vnd.android.cursor.item/phone_v2" + "'", null);
            } else {
                builder = ContentProviderOperation.newInsert(dataUri);
                builder.withValue("raw_contact_id", Integer.valueOf(updateContactID));
            }
            builder.withValue("mimetype", "vnd.android.cursor.item/phone_v2");
            builder.withValue("data1", p.phone);
            builder.withValue("data2", Integer.valueOf(2));
            operationList.add(builder.build());
            Log.m525d("vk", "Updating phone");
        }
        if (p.bdate != null && bdateChanged) {
            if (!update) {
                builder = ContentProviderOperation.newInsert(dataUri);
                builder.withValueBackReference("raw_contact_id", valueBack);
            } else if (bdateExists) {
                builder = ContentProviderOperation.newUpdate(dataUri);
                builder.withSelection("raw_contact_id=" + updateContactID + " AND " + "mimetype" + "='" + "vnd.android.cursor.item/contact_event" + "'", null);
            } else {
                builder = ContentProviderOperation.newInsert(dataUri);
                builder.withValue("raw_contact_id", Integer.valueOf(updateContactID));
            }
            builder.withValueBackReference("raw_contact_id", valueBack);
            builder.withValue("mimetype", "vnd.android.cursor.item/contact_event");
            builder.withValue("data1", p.bdate);
            builder.withValue("data2", Integer.valueOf(3));
            operationList.add(builder.build());
            Log.m525d("vk", "Updating bdate");
        }
        if (!update) {
            builder = ContentProviderOperation.newInsert(dataUri);
            builder.withValueBackReference("raw_contact_id", valueBack);
            builder.withValue("mimetype", "vnd.android.cursor.item/vnd.com.vkontakte.android.profile");
            builder.withValue("data1", Integer.valueOf(p.uid));
            builder.withValue("data2", context.getResources().getString(C0436R.string.contact_profile_title));
            builder.withValue("data3", context.getResources().getString(C0436R.string.contact_profile_subtitle));
            operationList.add(builder.build());
        }
        if (!hasIm) {
            if (update) {
                builder = ContentProviderOperation.newInsert(dataUri);
                builder.withValue("raw_contact_id", Integer.valueOf(updateContactID));
            } else {
                builder = ContentProviderOperation.newInsert(dataUri);
                builder.withValueBackReference("raw_contact_id", valueBack);
            }
            builder.withValue("mimetype", "vnd.android.cursor.item/vnd.com.vkontakte.android.sendmsg");
            builder.withValue("data1", Integer.valueOf(p.uid));
            builder.withValue("data2", context.getResources().getString(C0436R.string.contact_profile_title));
            builder.withValue("data3", context.getResources().getString(C0436R.string.contact_message_subtitle));
            operationList.add(builder.build());
        }
        if (update) {
            stats.numUpdates++;
        } else {
            stats.numInserts++;
        }
        stats.numEntries++;
        Log.m525d("vk", "Added/updated contact: " + p.firstName + " " + p.lastName);
        return true;
    }

    public static ArrayList<String> getLocalPhoneNumbers(ContentResolver resolver) {
        Cursor cursor = resolver.query(RawContacts.CONTENT_URI, new String[]{"account_type", "_id"}, null, null, null);
        if (cursor == null || cursor.getCount() == 0) {
            Log.m530w("vk", "cursor.getCount = 0 || cursor==null");
            return null;
        }
        ArrayList<Long> idsToGet = new ArrayList();
        cursor.moveToFirst();
        do {
            if (!Auth.ACCOUNT_TYPE.equals(cursor.getString(cursor.getColumnIndex("account_type")))) {
                idsToGet.add(Long.valueOf(cursor.getLong(cursor.getColumnIndex("_id"))));
            }
        } while (cursor.moveToNext());
        if (idsToGet.size() == 0) {
            Log.m530w("vk", "idsToGet.size = 0");
            return null;
        }
        String ids = ACRAConstants.DEFAULT_STRING_VALUE;
        Iterator it = idsToGet.iterator();
        while (it.hasNext()) {
            ids = new StringBuilder(String.valueOf(ids)).append(",").append(((Long) it.next()).longValue()).toString();
        }
        ids = ids.substring(1);
        ArrayList<String> numbers = new ArrayList();
        cursor.close();
        cursor = resolver.query(Data.CONTENT_URI, new String[]{"data1"}, "mimetype='vnd.android.cursor.item/phone_v2' AND data2=2 AND raw_contact_id IN (" + ids + ")", null, null);
        if (cursor == null || cursor.getCount() == 0) {
            Log.m530w("vk", "cursor2.getCount = 0");
            return null;
        }
        cursor.moveToFirst();
        do {
            numbers.add(cursor.getString(cursor.getColumnIndex("data1")));
        } while (cursor.moveToNext());
        cursor.close();
        return numbers;
    }
}
