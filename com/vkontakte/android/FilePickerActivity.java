package com.vkontakte.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri.Builder;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.os.StatFs;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.actionbarsherlock.view.MenuItem;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.PaddingColorDrawable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import org.acra.ACRAConstants;

public class FilePickerActivity extends VKActivity {
    private FileListAdapter adapter;
    private FrameLayout contentView;
    private File currentDir;
    private EmptyView empty;
    private ArrayList<HistoryEntry> history;
    private ListImageLoaderWrapper imgLoader;
    private ArrayList<ListItem> items;
    private ListView list;
    private BroadcastReceiver receiver;

    /* renamed from: com.vkontakte.android.FilePickerActivity.1 */
    class C02661 extends BroadcastReceiver {

        /* renamed from: com.vkontakte.android.FilePickerActivity.1.1 */
        class C02651 implements Runnable {
            C02651() {
            }

            public void run() {
                if (FilePickerActivity.this.currentDir == null) {
                    FilePickerActivity.this.listRoots();
                } else {
                    FilePickerActivity.this.listFiles(FilePickerActivity.this.currentDir);
                }
            }
        }

        C02661() {
        }

        public void onReceive(Context arg0, Intent intent) {
            Log.m528i("vk", "Receive " + intent);
            Runnable r = new C02651();
            if ("android.intent.action.MEDIA_UNMOUNTED".equals(intent.getAction())) {
                FilePickerActivity.this.list.postDelayed(r, 1000);
            } else {
                r.run();
            }
        }
    }

    /* renamed from: com.vkontakte.android.FilePickerActivity.2 */
    class C02672 implements OnItemClickListener {
        C02672() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            ListItem item = (ListItem) FilePickerActivity.this.items.get(pos);
            File file = item.file;
            if (file.isDirectory()) {
                HistoryEntry he = new HistoryEntry(null);
                he.scrollItem = FilePickerActivity.this.list.getFirstVisiblePosition();
                he.scrollOffset = FilePickerActivity.this.list.getChildAt(0).getTop();
                he.dir = FilePickerActivity.this.currentDir;
                he.title = FilePickerActivity.this.getTitle().toString();
                if (FilePickerActivity.this.listFiles(file)) {
                    FilePickerActivity.this.history.add(he);
                    FilePickerActivity.this.setTitle(item.title);
                    FilePickerActivity.this.list.setSelection(0);
                }
            } else if (file.canRead()) {
                if (FilePickerActivity.this.getIntent().hasExtra("size_limit")) {
                    if (file.length() > FilePickerActivity.this.getIntent().getLongExtra("size_limit", 0)) {
                        FilePickerActivity.this.showErrorBox(FilePickerActivity.this.getString(C0436R.string.file_upload_limit, new Object[]{Global.langFileSize(FilePickerActivity.this.getIntent().getLongExtra("size_limit", 0), FilePickerActivity.this.getResources())}));
                        return;
                    }
                }
                PendingDocumentAttachment att = new PendingDocumentAttachment(file.getName(), file.getAbsolutePath(), (int) file.length(), item.thumb != null ? new Builder().scheme("file").path(file.getAbsolutePath()).build().toString() : null, 0, UploaderService.getNewID());
                ArrayList<Parcelable> a = new ArrayList();
                a.add(att);
                Intent result = new Intent();
                result.putParcelableArrayListExtra("files", a);
                FilePickerActivity.this.setResult(-1, result);
                FilePickerActivity.this.finish();
            } else {
                FilePickerActivity.this.showErrorBox(FilePickerActivity.this.getString(C0436R.string.access_error));
            }
        }
    }

    /* renamed from: com.vkontakte.android.FilePickerActivity.3 */
    class C02683 implements Comparator<File> {
        C02683() {
        }

        public int compare(File lhs, File rhs) {
            if (lhs.isDirectory() != rhs.isDirectory()) {
                return lhs.isDirectory() ? -1 : 1;
            } else {
                return lhs.getName().compareToIgnoreCase(rhs.getName());
            }
        }
    }

    private class FileListAdapter extends BaseAdapter {
        private FileListAdapter() {
        }

        public int getCount() {
            return FilePickerActivity.this.items.size();
        }

        public Object getItem(int position) {
            return FilePickerActivity.this.items.get(position);
        }

        public long getItemId(int position) {
            return 0;
        }

        public int getViewTypeCount() {
            return 2;
        }

        public int getItemViewType(int pos) {
            return ((ListItem) FilePickerActivity.this.items.get(pos)).subtitle.length() > 0 ? 0 : 1;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            ListItem item = (ListItem) FilePickerActivity.this.items.get(position);
            if (v == null) {
                v = View.inflate(FilePickerActivity.this, C0436R.layout.documents_item, null);
                if (item.subtitle.length() == 0) {
                    v.findViewById(C0436R.id.docs_item_info).setVisibility(8);
                }
            }
            ((TextView) v.findViewById(C0436R.id.docs_item_title)).setText(item.title);
            ((TextView) v.findViewById(C0436R.id.docs_item_type)).setText(item.ext.toUpperCase().substring(0, Math.min(item.ext.length(), 4)));
            ((TextView) v.findViewById(C0436R.id.docs_item_info)).setText(item.subtitle);
            if (item.thumb != null) {
                ((ImageView) v.findViewById(C0436R.id.docs_item_thumb)).setScaleType(ScaleType.CENTER_CROP);
                if (FilePickerActivity.this.imgLoader.isAlreadyLoaded(item.thumb)) {
                    ((ImageView) v.findViewById(C0436R.id.docs_item_thumb)).setImageBitmap(FilePickerActivity.this.imgLoader.get(item.thumb));
                } else {
                    ((ImageView) v.findViewById(C0436R.id.docs_item_thumb)).setImageBitmap(null);
                }
            } else if (item.icon != 0) {
                ((ImageView) v.findViewById(C0436R.id.docs_item_thumb)).setImageResource(item.icon);
                ((ImageView) v.findViewById(C0436R.id.docs_item_thumb)).setScaleType(ScaleType.CENTER);
            } else {
                ((ImageView) v.findViewById(C0436R.id.docs_item_thumb)).setImageBitmap(null);
            }
            return v;
        }
    }

    private class HistoryEntry {
        File dir;
        int scrollItem;
        int scrollOffset;
        String title;

        private HistoryEntry() {
        }
    }

    private class ListItem {
        String ext;
        File file;
        int icon;
        String subtitle;
        String thumb;
        String title;

        private ListItem() {
            this.subtitle = ACRAConstants.DEFAULT_STRING_VALUE;
            this.ext = ACRAConstants.DEFAULT_STRING_VALUE;
        }
    }

    private class DocsThumbsAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.FilePickerActivity.DocsThumbsAdapter.1 */
        class C02691 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C02691(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.docs_item_thumb);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private DocsThumbsAdapter() {
        }

        public int getItemCount() {
            return FilePickerActivity.this.items.size();
        }

        public int getImageCountForItem(int item) {
            return ((ListItem) FilePickerActivity.this.items.get(item)).thumb != null ? 1 : 0;
        }

        public String getImageURL(int item, int image) {
            return ((ListItem) FilePickerActivity.this.items.get(item)).thumb;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += FilePickerActivity.this.list.getHeaderViewsCount();
            if (item >= FilePickerActivity.this.list.getFirstVisiblePosition() && item <= FilePickerActivity.this.list.getLastVisiblePosition()) {
                FilePickerActivity.this.contentView.post(new C02691(FilePickerActivity.this.list.getChildAt(item - FilePickerActivity.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    public FilePickerActivity() {
        this.items = new ArrayList();
        this.history = new ArrayList();
        this.receiver = new C02661();
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        this.contentView = new FrameLayout(this);
        this.contentView.setBackgroundColor(-1);
        this.list = new ListView(this);
        ListView listView = this.list;
        ListAdapter fileListAdapter = new FileListAdapter();
        this.adapter = fileListAdapter;
        listView.setAdapter(fileListAdapter);
        if (VERSION.SDK_INT < 14) {
            this.list.setBackgroundColor(-1);
            this.list.setCacheColorHint(-1);
        }
        this.list.setSelector(C0436R.drawable.highlight);
        this.list.setDivider(new PaddingColorDrawable(-1710619, Global.scale(6.0f)));
        this.list.setDividerHeight(Global.scale(1.0f));
        this.contentView.addView(this.list);
        this.empty = EmptyView.create(this);
        this.empty.setButtonVisible(false);
        this.empty.setText((int) C0436R.string.no_files);
        this.contentView.addView(this.empty);
        this.list.setEmptyView(this.empty);
        setContentView(this.contentView);
        this.list.setOnItemClickListener(new C02672());
        this.imgLoader = new ListImageLoaderWrapper(new DocsThumbsAdapter(), this.list, null);
        listRoots();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.MEDIA_BAD_REMOVAL");
        filter.addAction("android.intent.action.MEDIA_CHECKING");
        filter.addAction("android.intent.action.MEDIA_EJECT");
        filter.addAction("android.intent.action.MEDIA_MOUNTED");
        filter.addAction("android.intent.action.MEDIA_NOFS");
        filter.addAction("android.intent.action.MEDIA_REMOVED");
        filter.addAction("android.intent.action.MEDIA_SHARED");
        filter.addAction("android.intent.action.MEDIA_UNMOUNTABLE");
        filter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        filter.addDataScheme("file");
        registerReceiver(this.receiver, filter);
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.receiver);
    }

    private void showErrorBox(String error) {
        new VKAlertDialog.Builder(this).setTitle(C0436R.string.error).setMessage(error).setPositiveButton(C0436R.string.ok, null).show();
    }

    private boolean listFiles(File dir) {
        if (dir.canRead()) {
            this.empty.setText((int) C0436R.string.no_files);
            try {
                File[] files = dir.listFiles();
                if (files == null) {
                    showErrorBox(getString(C0436R.string.unknown_error));
                    return false;
                }
                this.currentDir = dir;
                this.items.clear();
                Arrays.sort(files, new C02683());
                for (File file : files) {
                    if (!file.getName().startsWith(".")) {
                        ListItem item = new ListItem();
                        item.title = file.getName();
                        item.file = file;
                        if (file.isDirectory()) {
                            item.icon = C0436R.drawable.ic_directory;
                        } else {
                            String fname = file.getName();
                            String[] sp = fname.split("\\.");
                            item.ext = sp.length > 1 ? sp[sp.length - 1] : "?";
                            item.subtitle = Global.langFileSize(file.length(), getResources());
                            if (file.lastModified() > 0) {
                                item.subtitle += ", " + Global.langDateRelativeNoDiff((int) (file.lastModified() / 1000), getResources());
                            }
                            fname = fname.toLowerCase();
                            if (fname.endsWith(".jpg") || fname.endsWith(".png") || fname.endsWith(".gif") || fname.endsWith(".jpeg")) {
                                item.thumb = new Builder().scheme("file").path(file.getAbsolutePath()).appendQueryParameter("w", new StringBuilder(String.valueOf(Global.scale(55.0f))).toString()).build().toString();
                            }
                        }
                        this.items.add(item);
                    }
                }
                updateList();
                return true;
            } catch (Throwable x) {
                Log.m532w("vk", x);
                showErrorBox(x.getLocalizedMessage());
                return false;
            }
        } else if ((!dir.getAbsolutePath().startsWith(Environment.getExternalStorageDirectory().toString()) && !dir.getAbsolutePath().startsWith("/sdcard") && !dir.getAbsolutePath().startsWith("/mnt/sdcard")) || Environment.getExternalStorageState().equals("mounted") || Environment.getExternalStorageState().equals("mounted_ro")) {
            showErrorBox(getString(C0436R.string.access_error));
            return false;
        } else {
            this.currentDir = dir;
            this.items.clear();
            if ("shared".equals(Environment.getExternalStorageState())) {
                this.empty.setText((int) C0436R.string.file_usb_active);
            } else {
                this.empty.setText((int) C0436R.string.file_not_mounted);
            }
            updateList();
            return true;
        }
    }

    private String getRootSubtitle(String path) {
        StatFs stat = new StatFs(path);
        long total = ((long) stat.getBlockCount()) * ((long) stat.getBlockSize());
        Log.m528i("vk", new StringBuilder(String.valueOf(path)).append(": ").append(((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize())).append("/").append(total).toString());
        if (total == 0) {
            return ACRAConstants.DEFAULT_STRING_VALUE;
        }
        return getString(C0436R.string.file_free_subtitle, new Object[]{Global.langFileSize(free, getResources()), Global.langFileSize(total, getResources())});
    }

    private void listRoots() {
        setTitle(C0436R.string.pick_file);
        this.currentDir = null;
        this.items.clear();
        String extStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
        FilePickerActivity filePickerActivity = this;
        ListItem ext = new ListItem();
        int i = (VERSION.SDK_INT < 9 || Environment.isExternalStorageRemovable()) ? C0436R.string.file_sd_card : C0436R.string.file_internal_storage;
        ext.title = getString(i);
        i = (VERSION.SDK_INT < 9 || Environment.isExternalStorageRemovable()) ? C0436R.drawable.ic_external_storage : C0436R.drawable.ic_storage;
        ext.icon = i;
        ext.subtitle = getRootSubtitle(extStorage);
        ext.file = Environment.getExternalStorageDirectory();
        this.items.add(ext);
        BufferedReader reader = new BufferedReader(new FileReader("/proc/mounts"));
        HashMap<String, ArrayList<String>> aliases = new HashMap();
        ArrayList<String> result = new ArrayList();
        Object extDevice = null;
        while (true) {
            String line = reader.readLine();
            if (line == null) {
                break;
            }
            try {
                if (!((!line.contains("/mnt") && !line.contains("/storage") && !line.contains("/sdcard")) || line.contains("asec") || line.contains("tmpfs") || line.contains("none"))) {
                    String[] info = line.split(" ");
                    if (!aliases.containsKey(info[0])) {
                        aliases.put(info[0], new ArrayList());
                    }
                    ((ArrayList) aliases.get(info[0])).add(info[1]);
                    if (info[1].equals(extStorage)) {
                        extDevice = info[0];
                    }
                    result.add(info[1]);
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
        reader.close();
        if (extDevice != null) {
            result.removeAll((Collection) aliases.get(extDevice));
            Iterator it = result.iterator();
            while (it.hasNext()) {
                String path = (String) it.next();
                try {
                    boolean isSd = path.toLowerCase().contains("sd");
                    filePickerActivity = this;
                    ListItem item = new ListItem();
                    item.title = getString(isSd ? C0436R.string.file_sd_card : C0436R.string.file_external_storage);
                    item.icon = C0436R.drawable.ic_external_storage;
                    item.subtitle = getRootSubtitle(path);
                    item.file = new File(path);
                    this.items.add(item);
                } catch (Exception e) {
                }
            }
        }
        filePickerActivity = this;
        ListItem fs = new ListItem();
        fs.title = "/";
        fs.subtitle = getString(C0436R.string.file_system_root);
        fs.icon = C0436R.drawable.ic_directory;
        fs.file = new File("/");
        this.items.add(fs);
        updateList();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        }
        finish();
        return true;
    }

    public void onBackPressed() {
        if (this.history.size() > 0) {
            HistoryEntry he = (HistoryEntry) this.history.remove(this.history.size() - 1);
            setTitle(he.title);
            if (he.dir != null) {
                listFiles(he.dir);
            } else {
                listRoots();
            }
            this.list.setSelectionFromTop(he.scrollItem, he.scrollOffset);
            return;
        }
        super.onBackPressed();
    }

    private void updateList() {
        this.adapter.notifyDataSetChanged();
        this.imgLoader.updateImages();
    }
}
