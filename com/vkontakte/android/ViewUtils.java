package com.vkontakte.android;

import android.app.Activity;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ViewUtils {
    public static View findViewByType(View container, Class<? extends View> type) {
        if (type.isInstance(container)) {
            return container;
        }
        if (container instanceof ViewGroup) {
            ViewGroup g = (ViewGroup) container;
            for (int i = 0; i < g.getChildCount(); i++) {
                View result = findViewByType(g.getChildAt(i), type);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    public static Point getViewOffset(View v1, View v2) {
        int[] p1 = new int[2];
        int[] p2 = new int[2];
        v1.getLocationOnScreen(p1);
        v2.getLocationOnScreen(p2);
        return new Point(p1[0] - p2[0], p1[1] - p2[1]);
    }

    public static OnClickListener getViewOnClickListener(View view) {
        try {
            Method getListenerInfo = View.class.getDeclaredMethod("getListenerInfo", new Class[0]);
            getListenerInfo.setAccessible(true);
            Object listenerInfo = getListenerInfo.invoke(view, new Object[0]);
            Field listenerField = listenerInfo.getClass().getDeclaredField("mOnClickListener");
            listenerField.setAccessible(true);
            return (OnClickListener) listenerField.get(listenerInfo);
        } catch (Exception e) {
            return null;
        }
    }

    public static void setNoClipRecursive(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) view;
            vg.setClipChildren(false);
            vg.setClipToPadding(false);
            for (int i = 0; i < vg.getChildCount(); i++) {
                if (vg.getChildAt(i) instanceof ViewGroup) {
                    setNoClipRecursive(vg.getChildAt(i));
                }
            }
        }
    }

    public static void setNoFitRecursive(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) view;
            Log.m525d("vk", "Set no clip on " + vg);
            vg.setClipChildren(false);
            vg.setClipToPadding(false);
            for (int i = 0; i < vg.getChildCount(); i++) {
                if (vg.getChildAt(i) instanceof ViewGroup) {
                    setNoFitRecursive(vg.getChildAt(i));
                }
                vg.getChildAt(i).setFitsSystemWindows(false);
            }
        }
    }

    public static int getScreenOrientation(Activity act) {
        int rotation = act.getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics dm = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        if (((rotation == 0 || rotation == 2) && height > width) || ((rotation == 1 || rotation == 3) && width > height)) {
            switch (rotation) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return 1;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return 0;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return 9;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return 8;
                default:
                    Log.m526e("vk", "Unknown screen orientation. Defaulting to portrait.");
                    return 1;
            }
        }
        switch (rotation) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return 0;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                return 9;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return 8;
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                return 1;
            default:
                Log.m526e("vk", "Unknown screen orientation. Defaulting to landscape.");
                return 0;
        }
    }
}
