package com.vkontakte.android;

import android.content.Intent;
import android.content.pm.Signature;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.facebook.WebDialog;
import com.google.android.gcm.GCMConstants;
import com.vkontakte.android.ui.ErrorView;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import org.acra.ACRAConstants;

public class SDKAuthActivity extends VKActivity {
    private static final String TAG = "vk_sdk_auth";
    private FrameLayout contentView;
    private ErrorView error;
    private ProgressBar progress;
    private WebView webView;

    /* renamed from: com.vkontakte.android.SDKAuthActivity.1 */
    class C04551 implements OnLongClickListener {
        C04551() {
        }

        public boolean onLongClick(View v) {
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.SDKAuthActivity.2 */
    class C04562 implements OnClickListener {
        C04562() {
        }

        public void onClick(View v) {
            SDKAuthActivity.this.error.setVisibility(8);
            SDKAuthActivity.this.webView.reload();
        }
    }

    private class ChromeClient extends WebChromeClient {
        private ChromeClient() {
        }

        public void onProgressChanged(WebView view, int progr) {
            boolean visible;
            boolean z = true;
            int i = 0;
            if (progr < 100) {
                visible = true;
            } else {
                visible = false;
            }
            if (SDKAuthActivity.this.progress.getVisibility() != 0) {
                z = false;
            }
            if (visible != z) {
                if (visible) {
                    SDKAuthActivity.this.progress.setVisibility(0);
                } else {
                    SDKAuthActivity.this.progress.setVisibility(8);
                }
            }
            WebView access$1 = SDKAuthActivity.this.webView;
            if (visible || SDKAuthActivity.this.error.getVisibility() == 0) {
                i = 8;
            }
            access$1.setVisibility(i);
        }
    }

    private class WebClient extends WebViewClient {
        private WebClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Uri uri = Uri.parse(url);
            if ("oauth.vk.com".equals(uri.getHost()) && "/blank.html".equals(uri.getPath())) {
                uri = Uri.parse(url.replace('#', '?'));
                Intent result;
                if (uri.getQueryParameter(GCMConstants.EXTRA_ERROR) != null) {
                    Log.m530w(SDKAuthActivity.TAG, "Auth not successful [fail]: " + url);
                    result = new Intent();
                    result.putExtra(GCMConstants.EXTRA_ERROR, uri.getQueryParameter(GCMConstants.EXTRA_ERROR));
                    result.putExtra("error_reason", uri.getQueryParameter("error_reason"));
                    result.putExtra("error_description", uri.getQueryParameter("error_description"));
                    SDKAuthActivity.this.setResult(0, result);
                    SDKAuthActivity.this.finish();
                } else if (uri.getQueryParameter(WebDialog.DIALOG_PARAM_ACCESS_TOKEN) != null) {
                    String token = uri.getQueryParameter(WebDialog.DIALOG_PARAM_ACCESS_TOKEN);
                    String secret = uri.getQueryParameter("secret");
                    String expiresIn = uri.getQueryParameter("expires_in");
                    int uid = Integer.parseInt(uri.getQueryParameter("user_id"));
                    result = new Intent();
                    result.putExtra(WebDialog.DIALOG_PARAM_ACCESS_TOKEN, token);
                    if (secret != null) {
                        result.putExtra("secret", secret);
                    }
                    result.putExtra("user_id", uid);
                    result.putExtra("https_required", VKApplication.context.getSharedPreferences(null, 0).getBoolean("forceHTTPS", false) ? "1" : "0");
                    if (expiresIn != null) {
                        try {
                            result.putExtra("expires_in", Integer.parseInt(expiresIn));
                        } catch (Exception e) {
                        }
                    }
                    Log.m528i(SDKAuthActivity.TAG, "Auth ok");
                    SDKAuthActivity.this.setResult(-1, result);
                    SDKAuthActivity.this.finish();
                } else {
                    Log.m530w(SDKAuthActivity.TAG, "Auth not successful [unknown]: " + url);
                    SDKAuthActivity.this.setResult(0);
                    SDKAuthActivity.this.finish();
                }
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        public void onReceivedError(WebView view, int errCode, String descr, String url) {
            SDKAuthActivity.this.error.setErrorInfo(errCode, descr);
            SDKAuthActivity.this.error.setVisibility(0);
            SDKAuthActivity.this.webView.setVisibility(8);
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        if (getCallingPackage() == null) {
            Toast.makeText(this, "Be sure to call this using startActivityForResult()", 1).show();
            finish();
            return;
        }
        this.progress = new ProgressBar(this);
        this.contentView = new FrameLayout(this);
        this.contentView.setBackgroundColor(-1);
        this.webView = new WebView(this);
        this.webView.setWebViewClient(new WebClient());
        this.webView.setWebChromeClient(new ChromeClient());
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setOnLongClickListener(new C04551());
        this.webView.setLongClickable(false);
        this.contentView.addView(this.webView);
        this.contentView.addView(this.progress, new LayoutParams(Global.scale(40.0f), Global.scale(40.0f), 17));
        this.error = (ErrorView) View.inflate(this, C0436R.layout.error, null);
        this.error.setVisibility(8);
        this.contentView.addView(this.error);
        setContentView(this.contentView);
        this.webView.setVisibility(8);
        this.error.setOnRetryListener(new C04562());
        if (Global.accessToken != null) {
            loadPage();
        } else {
            startActivityForResult(new Intent(this, AuthActivity.class), 100);
        }
    }

    public String getSig(HashMap<String, String> params) {
        String src = "/authorize?";
        Set<String> e = params.keySet();
        ArrayList<String> parts = new ArrayList();
        for (String key : e) {
            parts.add(new StringBuilder(String.valueOf(key)).append("=").append((String) params.get(key)).toString());
        }
        return APIRequest.md5(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(src)).append(TextUtils.join("&", parts)).toString())).append(Global.secret).toString());
    }

    private void loadPage() {
        String pkg = getCallingPackage();
        String certFp = ACRAConstants.DEFAULT_STRING_VALUE;
        try {
            Signature sig = getPackageManager().getPackageInfo(pkg, 64).signatures[0];
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(sig.toByteArray());
            byte[] sha = md.digest();
            certFp = ACRAConstants.DEFAULT_STRING_VALUE;
            for (byte sb : sha) {
                certFp = new StringBuilder(String.valueOf(certFp)).append(String.format("%02X", new Object[]{Byte.valueOf(sb)})).toString();
            }
        } catch (Throwable x) {
            Log.m532w(TAG, x);
        }
        HashMap<String, String> params = new HashMap();
        params.put(WebDialog.DIALOG_PARAM_CLIENT_ID, new StringBuilder(String.valueOf(getIntent().getIntExtra(WebDialog.DIALOG_PARAM_CLIENT_ID, 0))).toString());
        params.put(WebDialog.DIALOG_PARAM_SCOPE, getIntent().getStringExtra(WebDialog.DIALOG_PARAM_SCOPE));
        params.put(WebDialog.DIALOG_PARAM_REDIRECT_URI, "https://oauth.vk.com/blank.html");
        params.put(WebDialog.DIALOG_PARAM_DISPLAY, "android");
        params.put("response_type", "token");
        params.put(WebDialog.DIALOG_PARAM_ACCESS_TOKEN, Global.accessToken);
        params.put("sdk_package", pkg);
        params.put("sdk_fingerprint", certFp);
        if (getIntent().hasExtra("revoke")) {
            params.put("revoke", "1");
        }
        Builder bldr = new Builder().scheme("https").authority("oauth.vk.com").path("/authorize");
        for (String k : params.keySet()) {
            bldr.appendQueryParameter(k, (String) params.get(k));
        }
        bldr.appendQueryParameter("sig", getSig(params));
        this.webView.loadUrl(bldr.build().toString());
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode != 100) {
            return;
        }
        if (resCode == -1) {
            loadPage();
            return;
        }
        setResult(0);
        finish();
    }
}
