package com.vkontakte.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class Navigate {
    private static long lastPhotoTime;

    static {
        lastPhotoTime = 0;
    }

    public static void to(String fragmentClassName, Bundle args, Activity act) {
        Intent intent = new Intent(act, FragmentWrapperActivity.class);
        intent.putExtra("class", fragmentClassName);
        intent.putExtra("args", args);
        act.startActivity(intent);
    }

    public static void to(String fragmentClassName, Bundle args, Activity act, boolean overlay, int inAnim, int outAnim) {
        Intent intent;
        int i = 0;
        if (!"PhotoViewerFragment".equals(fragmentClassName)) {
            intent = "AudioPlayerFragment".equals(fragmentClassName) ? new Intent(act, AudioPlayerActivity.class) : new Intent(act, FragmentWrapperActivity.class);
        } else if (System.currentTimeMillis() - lastPhotoTime >= 300) {
            lastPhotoTime = System.currentTimeMillis();
            intent = new Intent(act, PhotoViewerActivity.class);
            if (act instanceof SherlockActivity) {
                int height;
                SherlockActivity sa = (SherlockActivity) act;
                String str = "anim_clip_top";
                if (sa.getSupportActionBar() != null) {
                    height = sa.getSupportActionBar().getHeight();
                } else {
                    height = 0;
                }
                args.putInt(str, height);
            }
            SherlockFragmentActivity sa2;
            String str2;
            if (act instanceof MainActivity) {
                sa2 = (SherlockFragmentActivity) act;
                str2 = "anim_clip_top";
                if (sa2.getSupportActionBar() != null) {
                    i = sa2.getSupportActionBar().getHeight();
                }
                args.putInt(str2, i);
            } else if (act instanceof SherlockFragmentActivity) {
                sa2 = (SherlockFragmentActivity) act;
                str2 = "anim_clip_top";
                if (sa2.getSupportActionBar() != null) {
                    i = sa2.getSupportActionBar().getHeight();
                }
                args.putInt(str2, i);
            }
        } else {
            return;
        }
        intent.putExtra("class", fragmentClassName);
        intent.putExtra("args", args);
        intent.putExtra("overlaybar", overlay);
        if (!(inAnim == -1 || outAnim == -1)) {
            intent.putExtra("in_anim", inAnim);
            intent.putExtra("out_anim", outAnim);
        }
        act.startActivity(intent);
    }
}
