package com.vkontakte.android;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class LoadMoreCommentsView extends FrameLayout {
    public LoadMoreCommentsView(Context context) {
        super(context);
    }

    public LoadMoreCommentsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void showProgress(boolean show) {
        int i;
        int i2 = 4;
        View findViewById = findViewById(C0436R.id.loadmore_progress);
        if (show) {
            i = 0;
        } else {
            i = 4;
        }
        findViewById.setVisibility(i);
        View findViewById2 = findViewById(C0436R.id.loadmore_text);
        if (!show) {
            i2 = 0;
        }
        findViewById2.setVisibility(i2);
    }

    public void setNumComments(int n) {
        ((TextView) findViewById(C0436R.id.loadmore_text)).setText(Global.langPlural(C0436R.array.wallview_comments, n, getResources()));
    }
}
