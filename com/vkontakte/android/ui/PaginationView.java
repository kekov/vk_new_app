package com.vkontakte.android.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.animation.AlphaAnimation;
import com.vkontakte.android.Global;
import java.util.Timer;
import java.util.TimerTask;

public class PaginationView extends View {
    private Paint bgPaint;
    private boolean click;
    private int currentPage;
    private Timer fadeTimer;
    private boolean isClickable;
    private boolean isVisible;
    private Listener listener;
    private int numPages;
    private int offset;
    private int pWidth;
    private int[] pageMap;
    private float sx;
    private float sy;
    private Paint textPaint;
    private float touchslop;
    private int[] widths;

    /* renamed from: com.vkontakte.android.ui.PaginationView.1 */
    class C10211 extends TimerTask {

        /* renamed from: com.vkontakte.android.ui.PaginationView.1.1 */
        class C10191 implements Runnable {
            C10191() {
            }

            public void run() {
                AlphaAnimation aa = new AlphaAnimation(1.0f, 0.0f);
                aa.setFillAfter(true);
                aa.setDuration(200);
                PaginationView.this.startAnimation(aa);
                PaginationView.this.isVisible = false;
            }
        }

        /* renamed from: com.vkontakte.android.ui.PaginationView.1.2 */
        class C10202 implements Runnable {
            C10202() {
            }

            public void run() {
                if (!PaginationView.this.isVisible) {
                    PaginationView.this.isClickable = false;
                }
            }
        }

        C10211() {
        }

        public void run() {
            PaginationView.this.post(new C10191());
            PaginationView.this.postDelayed(new C10202(), 200);
        }
    }

    public interface Listener {
        void onPageSelected(int i);
    }

    public PaginationView(Context context) {
        super(context);
        this.numPages = 20;
        this.currentPage = 3;
        this.isClickable = true;
        this.widths = new int[6];
        this.pageMap = new int[6];
        this.bgPaint = new Paint();
        this.bgPaint.setColor(-1610612736);
        this.bgPaint.setAntiAlias(true);
        this.textPaint = new Paint();
        this.textPaint.setTypeface(Global.boldFont);
        this.textPaint.setTextSize((float) Global.scale(22.0f));
        this.textPaint.setColor(-1);
        this.textPaint.setAntiAlias(true);
        this.touchslop = (float) ViewConfiguration.getTouchSlop();
    }

    public void setPageCount(int c) {
        this.numPages = c;
        invalidate();
    }

    public void setCurrentPage(int p) {
        this.currentPage = p;
        invalidate();
    }

    public void onMeasure(int wms, int hms) {
        setMeasuredDimension(MeasureSpec.getSize(wms), Math.round((-this.textPaint.ascent()) + this.textPaint.descent()) + Global.scale(23.0f));
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (!this.isClickable || !isEnabled()) {
            return false;
        }
        show();
        int x = (int) (ev.getX() - ((float) this.offset));
        if (x < 0 || x > this.pWidth) {
            return false;
        }
        if ((ev.getAction() & MotionEventCompat.ACTION_MASK) != 0) {
            return true;
        }
        int w = 0;
        int i = 0;
        while (i < this.widths.length) {
            if (x < w || x > this.widths[i] + w) {
                w += this.widths[i];
                i++;
            } else {
                if (this.listener != null) {
                    this.listener.onPageSelected(this.pageMap[i]);
                }
                invalidate();
                return true;
            }
        }
        if (x >= this.pWidth || this.listener == null) {
            return true;
        }
        this.listener.onPageSelected(this.numPages);
        return true;
    }

    public void onDraw(Canvas c) {
        int i;
        int po;
        c.translate(0.0f, (float) Global.scale(3.0f));
        for (i = 0; i < this.widths.length; i++) {
            this.widths[i] = 0;
        }
        int fullWidth = Global.scale(10.0f);
        int spacing = Global.scale(20.0f);
        i = 0;
        if (this.currentPage > 2) {
            this.widths[0] = ((int) this.textPaint.measureText("\u00ab")) + spacing;
            this.pageMap[0] = 1;
            fullWidth += this.widths[0];
            int[] iArr = this.widths;
            iArr[0] = iArr[0] + Global.scale(5.0f);
            i = 0 + 1;
        } else {
            fullWidth += Global.scale(15.0f);
        }
        if (this.currentPage > 1) {
            int w = (int) (this.textPaint.measureText(new StringBuilder(String.valueOf(this.currentPage - 1)).toString()) + ((float) spacing));
            this.widths[i] = w;
            if (i == 0) {
                iArr = this.widths;
                iArr[i] = iArr[i] + Global.scale(15.0f);
            }
            this.pageMap[i] = this.currentPage - 1;
            fullWidth += w;
            i++;
        }
        this.widths[i] = ((int) this.textPaint.measureText(new StringBuilder(String.valueOf(this.currentPage)).toString())) + spacing;
        fullWidth += this.widths[i];
        if (i == 0 || this.currentPage == this.numPages) {
            iArr = this.widths;
            iArr[i] = iArr[i] + Global.scale(15.0f);
        }
        this.pageMap[i] = 0;
        i++;
        if (this.currentPage < this.numPages) {
            this.widths[i] = ((int) this.textPaint.measureText(new StringBuilder(String.valueOf(this.currentPage + 1)).toString())) + spacing;
            fullWidth += this.widths[i];
            if (this.currentPage == this.numPages - 1) {
                iArr = this.widths;
                iArr[i] = iArr[i] + Global.scale(15.0f);
            }
            this.pageMap[i] = this.currentPage + 1;
            i++;
        }
        if (this.currentPage < this.numPages - 1) {
            this.widths[i] = ((int) this.textPaint.measureText("\u00bb")) + spacing;
            fullWidth += this.widths[i];
            this.pageMap[i] = this.numPages;
            i++;
        } else {
            fullWidth += Global.scale(10.0f);
        }
        c.drawRoundRect(new RectF((float) ((getWidth() - fullWidth) - Global.scale(3.0f)), 0.0f, (float) (getWidth() - Global.scale(3.0f)), (float) (Math.round((-this.textPaint.ascent()) + this.textPaint.descent()) + Global.scale(20.0f))), (float) Global.scale(5.0f), (float) Global.scale(5.0f), this.bgPaint);
        this.pWidth = fullWidth;
        int offset = ((getWidth() - fullWidth) - Global.scale(3.0f)) + Global.scale(15.0f);
        this.offset = (getWidth() - fullWidth) - Global.scale(3.0f);
        this.textPaint.setAlpha(TransportMediator.FLAG_KEY_MEDIA_NEXT);
        int textY = Global.scale(10.0f);
        if (this.currentPage > 2) {
            drawText(c, "\u00ab", offset, textY, this.textPaint);
            offset = (int) (((float) offset) + (this.textPaint.measureText("\u00ab") + ((float) spacing)));
        } else {
            offset += Global.scale(10.0f);
        }
        if (this.currentPage > 1) {
            drawText(c, new StringBuilder(String.valueOf(this.currentPage - 1)).toString(), offset, textY, this.textPaint);
            po = offset;
            offset = (int) (((float) offset) + (this.textPaint.measureText(new StringBuilder(String.valueOf(this.currentPage - 1)).toString()) + ((float) spacing)));
            c.drawRect((float) (po - (spacing / 2)), (((float) textY) - this.textPaint.ascent()) + this.textPaint.descent(), (float) (offset - (spacing / 2)), ((((float) textY) - this.textPaint.ascent()) + this.textPaint.descent()) + 1.0f, this.textPaint);
        }
        this.textPaint.setAlpha(MotionEventCompat.ACTION_MASK);
        drawText(c, new StringBuilder(String.valueOf(this.currentPage)).toString(), offset, textY, this.textPaint);
        po = offset;
        offset = (int) (((float) offset) + (this.textPaint.measureText(new StringBuilder(String.valueOf(this.currentPage)).toString()) + ((float) spacing)));
        c.drawRect((float) (po - (spacing / 2)), (((float) textY) - this.textPaint.ascent()) + this.textPaint.descent(), (float) (offset - (spacing / 2)), ((((float) textY) - this.textPaint.ascent()) + this.textPaint.descent()) + ((float) Global.scale(3.0f)), this.textPaint);
        this.textPaint.setAlpha(TransportMediator.FLAG_KEY_MEDIA_NEXT);
        if (this.currentPage < this.numPages) {
            drawText(c, new StringBuilder(String.valueOf(this.currentPage + 1)).toString(), offset, textY, this.textPaint);
            po = offset;
            offset = (int) (((float) offset) + (this.textPaint.measureText(new StringBuilder(String.valueOf(this.currentPage + 1)).toString()) + ((float) spacing)));
            c.drawRect((float) (po - (spacing / 2)), (((float) textY) - this.textPaint.ascent()) + this.textPaint.descent(), (float) (offset - (spacing / 2)), ((((float) textY) - this.textPaint.ascent()) + this.textPaint.descent()) + 1.0f, this.textPaint);
        }
        if (this.currentPage < this.numPages - 1) {
            drawText(c, "\u00bb", offset, textY, this.textPaint);
            offset = (int) (((float) offset) + (this.textPaint.measureText("\u00bb") + ((float) spacing)));
        } else {
            offset += Global.scale(10.0f);
        }
        c.translate(0.0f, (float) Global.scale(-3.0f));
    }

    private void drawText(Canvas c, String text, int x, int y, Paint paint) {
        c.drawText(text, (float) x, ((float) y) - paint.ascent(), paint);
    }

    public void hide() {
        if (this.isVisible) {
            if (this.fadeTimer != null) {
                this.fadeTimer.cancel();
            }
            this.fadeTimer = new Timer();
            this.fadeTimer.schedule(new C10211(), 1000);
        }
    }

    public void hideNow() {
        AlphaAnimation aa = new AlphaAnimation(1.0f, 0.0f);
        aa.setFillAfter(true);
        aa.setDuration(0);
        startAnimation(aa);
        this.isVisible = false;
    }

    public void show() {
        if (this.fadeTimer != null) {
            this.fadeTimer.cancel();
            this.fadeTimer = null;
        }
        if (!this.isVisible) {
            this.isClickable = true;
            AlphaAnimation aa = new AlphaAnimation(0.0f, 1.0f);
            aa.setFillAfter(true);
            aa.setDuration(200);
            startAnimation(aa);
            this.isVisible = true;
        }
    }

    public void setListener(Listener l) {
        this.listener = l;
    }

    public int getPageCount() {
        return this.numPages;
    }
}
