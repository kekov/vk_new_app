package com.vkontakte.android.ui;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import com.vkontakte.android.C0436R;

public class ActionBarHacks {
    public static View getActionBar(Activity act) {
        int id = Resources.getSystem().getIdentifier("action_bar", "id", "android");
        if (id == 0) {
            id = C0436R.id.abs__action_bar;
        }
        if (id != 0) {
            return act.findViewById(id);
        }
        return null;
    }

    public static View getActionBarOverlay(Activity act) {
        int id = Resources.getSystem().getIdentifier("action_bar_overlay_layout", "id", "android");
        if (id != 0) {
            return act.findViewById(id);
        }
        return null;
    }
}
