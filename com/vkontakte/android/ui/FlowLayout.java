package com.vkontakte.android.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.vkontakte.android.Global;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class FlowLayout extends ViewGroup {
    static final /* synthetic */ boolean $assertionsDisabled;
    private Vector<Integer> lineHeights;
    private List<LayoutParams> lparams;
    private int measuredHeight;
    public int pwidth;

    public static class LayoutParams extends android.view.ViewGroup.LayoutParams {
        public boolean breakAfter;
        public boolean center;
        public boolean floating;
        public int height;
        public int horizontal_spacing;
        public int vertical_spacing;
        public int width;

        public LayoutParams(int horizontal_spacing, int vertical_spacing) {
            super(0, 0);
            this.horizontal_spacing = horizontal_spacing;
            this.vertical_spacing = vertical_spacing;
        }

        public LayoutParams() {
            super(0, 0);
        }
    }

    static {
        $assertionsDisabled = !FlowLayout.class.desiredAssertionStatus();
    }

    public FlowLayout(Context context) {
        super(context);
        this.lineHeights = new Vector();
        this.pwidth = Global.scale(5.0f);
        this.measuredHeight = 0;
    }

    public FlowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.lineHeights = new Vector();
        this.pwidth = Global.scale(5.0f);
        this.measuredHeight = 0;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<android.graphics.Rect> layoutWithParams(java.util.List<com.vkontakte.android.ui.FlowLayout.LayoutParams> r25, int r26, int r27) {
        /*
        r24 = this;
        r0 = r25;
        r1 = r24;
        r1.lparams = r0;
        r17 = new java.util.ArrayList;
        r17.<init>();
        r8 = r25.size();
        r18 = r26;
        r19 = r24.getPaddingLeft();
        r20 = r24.getPaddingTop();
        r12 = 0;
        r5 = 0;
        r16 = 0;
        r9 = 0;
        r0 = r24;
        r0 = r0.lineHeights;
        r21 = r0;
        r21.clear();
        r10 = 0;
        r13 = 0;
        r15 = 0;
        r11 = 0;
    L_0x002b:
        if (r11 < r8) goto L_0x0057;
    L_0x002d:
        if (r13 <= 0) goto L_0x003c;
    L_0x002f:
        r0 = r24;
        r0 = r0.lineHeights;
        r21 = r0;
        r22 = java.lang.Integer.valueOf(r13);
        r21.add(r22);
    L_0x003c:
        r19 = r24.getPaddingLeft();
        r20 = r24.getPaddingTop();
        r9 = 0;
        r16 = 0;
        r5 = 0;
        r12 = 0;
        r11 = 0;
    L_0x004a:
        if (r11 < r8) goto L_0x00ec;
    L_0x004c:
        r21 = r24.getFullHeight();
        r0 = r21;
        r1 = r24;
        r1.measuredHeight = r0;
        return r17;
    L_0x0057:
        r0 = r25;
        r14 = r0.get(r11);
        r14 = (com.vkontakte.android.ui.FlowLayout.LayoutParams) r14;
        r0 = r14.width;
        r21 = r0;
        if (r21 > 0) goto L_0x0073;
    L_0x0065:
        r7 = r26;
    L_0x0067:
        r6 = r14.height;
        if (r6 >= 0) goto L_0x0076;
    L_0x006b:
        r21 = new java.lang.IllegalArgumentException;
        r22 = "Height should be constant";
        r21.<init>(r22);
        throw r21;
    L_0x0073:
        r7 = r14.width;
        goto L_0x0067;
    L_0x0076:
        if (r5 != 0) goto L_0x0088;
    L_0x0078:
        r21 = r19 + r7;
        r0 = r24;
        r0 = r0.pwidth;
        r22 = r0;
        r22 = r22 + r18;
        r0 = r21;
        r1 = r22;
        if (r0 <= r1) goto L_0x00a6;
    L_0x0088:
        r19 = r24.getPaddingLeft();
        r21 = java.lang.Math.max(r13, r10);
        r20 = r20 + r21;
        r0 = r24;
        r0 = r0.lineHeights;
        r21 = r0;
        r22 = java.lang.Math.max(r13, r10);
        r22 = java.lang.Integer.valueOf(r22);
        r21.add(r22);
        r13 = 0;
        r10 = 0;
        r5 = 0;
    L_0x00a6:
        r0 = r14.vertical_spacing;
        r21 = r0;
        r21 = r21 + r6;
        r0 = r21;
        r13 = java.lang.Math.max(r13, r0);
        r0 = r14.floating;
        r21 = r0;
        if (r21 == 0) goto L_0x00e2;
    L_0x00b8:
        r0 = r14.vertical_spacing;
        r21 = r0;
        r21 = r21 + r6;
        r20 = r20 + r21;
        r0 = r14.vertical_spacing;
        r21 = r0;
        r21 = r21 + r6;
        r10 = r10 + r21;
        r21 = r19 + r7;
        r0 = r21;
        r15 = java.lang.Math.max(r15, r0);
    L_0x00d0:
        r5 = r14.breakAfter;
        r0 = r14.horizontal_spacing;
        r21 = r0;
        r21 = r19 - r21;
        r0 = r21;
        r15 = java.lang.Math.max(r15, r0);
        r11 = r11 + 1;
        goto L_0x002b;
    L_0x00e2:
        r10 = 0;
        r0 = r14.horizontal_spacing;
        r21 = r0;
        r21 = r21 + r7;
        r19 = r19 + r21;
        goto L_0x00d0;
    L_0x00ec:
        r0 = r25;
        r14 = r0.get(r11);
        r14 = (com.vkontakte.android.ui.FlowLayout.LayoutParams) r14;
        r0 = r14.width;
        r21 = r0;
        if (r21 > 0) goto L_0x0108;
    L_0x00fa:
        r7 = r18;
    L_0x00fc:
        r6 = r14.height;
        if (r6 >= 0) goto L_0x010b;
    L_0x0100:
        r21 = new java.lang.IllegalArgumentException;
        r22 = "Height should be constant";
        r21.<init>(r22);
        throw r21;
    L_0x0108:
        r7 = r14.width;
        goto L_0x00fc;
    L_0x010b:
        r0 = r14.floating;
        r21 = r0;
        if (r21 != 0) goto L_0x0115;
    L_0x0111:
        if (r16 == 0) goto L_0x0115;
    L_0x0113:
        r20 = r9;
    L_0x0115:
        if (r5 != 0) goto L_0x0127;
    L_0x0117:
        r21 = r19 + r7;
        r0 = r24;
        r0 = r0.pwidth;
        r22 = r0;
        r22 = r22 + r18;
        r0 = r21;
        r1 = r22;
        if (r0 <= r1) goto L_0x0142;
    L_0x0127:
        r19 = r24.getPaddingLeft();
        r0 = r24;
        r0 = r0.lineHeights;
        r21 = r0;
        r0 = r21;
        r21 = r0.elementAt(r12);
        r21 = (java.lang.Integer) r21;
        r21 = r21.intValue();
        r20 = r20 + r21;
        r12 = r12 + 1;
        r5 = 0;
    L_0x0142:
        r0 = r14.center;
        r21 = r0;
        if (r21 == 0) goto L_0x0152;
    L_0x0148:
        r21 = r24.getWidth();
        r21 = r21 / 2;
        r22 = r7 / 2;
        r19 = r21 - r22;
    L_0x0152:
        r21 = "vk";
        r22 = new java.lang.StringBuilder;
        r23 = java.lang.String.valueOf(r19);
        r22.<init>(r23);
        r23 = ";";
        r22 = r22.append(r23);
        r0 = r22;
        r1 = r20;
        r22 = r0.append(r1);
        r23 = ";";
        r22 = r22.append(r23);
        r0 = r22;
        r22 = r0.append(r7);
        r23 = ";";
        r22 = r22.append(r23);
        r0 = r22;
        r22 = r0.append(r6);
        r22 = r22.toString();
        com.vkontakte.android.Log.m529v(r21, r22);
        r21 = new android.graphics.Rect;
        r22 = r19 + r7;
        r23 = r20 + r6;
        r0 = r21;
        r1 = r19;
        r2 = r20;
        r3 = r22;
        r4 = r23;
        r0.<init>(r1, r2, r3, r4);
        r0 = r17;
        r1 = r21;
        r0.add(r1);
        r0 = r14.floating;
        r21 = r0;
        if (r21 == 0) goto L_0x01be;
    L_0x01aa:
        if (r16 != 0) goto L_0x01b0;
    L_0x01ac:
        r9 = r20;
        r16 = 1;
    L_0x01b0:
        r0 = r14.vertical_spacing;
        r21 = r0;
        r21 = r21 + r6;
        r20 = r20 + r21;
    L_0x01b8:
        r5 = r14.breakAfter;
        r11 = r11 + 1;
        goto L_0x004a;
    L_0x01be:
        r16 = 0;
        r0 = r14.horizontal_spacing;
        r21 = r0;
        r21 = r21 + r7;
        r19 = r19 + r21;
        goto L_0x01b8;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.ui.FlowLayout.layoutWithParams(java.util.List, int, int):java.util.List<android.graphics.Rect>");
    }

    public int getFullHeight() {
        int h = 0;
        Iterator it = this.lineHeights.iterator();
        while (it.hasNext()) {
            h += ((Integer) it.next()).intValue();
        }
        return h;
    }

    public void resetParams() {
        this.lparams = null;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void onMeasure(int r21, int r22) {
        /*
        r20 = this;
        r18 = $assertionsDisabled;
        if (r18 != 0) goto L_0x0010;
    L_0x0004:
        r18 = android.view.View.MeasureSpec.getMode(r21);
        if (r18 != 0) goto L_0x0010;
    L_0x000a:
        r18 = new java.lang.AssertionError;
        r18.<init>();
        throw r18;
    L_0x0010:
        r18 = android.view.View.MeasureSpec.getSize(r21);
        r19 = r20.getPaddingLeft();
        r18 = r18 - r19;
        r19 = r20.getPaddingRight();
        r15 = r18 - r19;
        r18 = android.view.View.MeasureSpec.getSize(r22);
        r19 = r20.getPaddingTop();
        r18 = r18 - r19;
        r19 = r20.getPaddingBottom();
        r10 = r18 - r19;
        r7 = r20.getChildCount();
        r12 = 0;
        r16 = r20.getPaddingLeft();
        r17 = r20.getPaddingTop();
        r14 = 0;
        r18 = android.view.View.MeasureSpec.getMode(r22);
        r19 = -2147483648; // 0xffffffff80000000 float:-0.0 double:NaN;
        r0 = r18;
        r1 = r19;
        if (r0 != r1) goto L_0x00a6;
    L_0x004a:
        r18 = -2147483648; // 0xffffffff80000000 float:-0.0 double:NaN;
        r0 = r18;
        r4 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r0);
    L_0x0052:
        r0 = r20;
        r0 = r0.lineHeights;
        r18 = r0;
        r18.clear();
        r2 = 0;
        r8 = 0;
        r11 = 0;
    L_0x005e:
        r0 = r20;
        r0 = r0.lparams;
        r18 = r0;
        if (r18 == 0) goto L_0x019e;
    L_0x0066:
        r0 = r20;
        r0 = r0.lparams;
        r18 = r0;
        r18 = r18.size();
    L_0x0070:
        r0 = r18;
        r18 = java.lang.Math.max(r7, r0);
        r0 = r18;
        if (r11 < r0) goto L_0x00af;
    L_0x007a:
        r18 = android.view.View.MeasureSpec.getMode(r22);
        if (r18 != 0) goto L_0x01af;
    L_0x0080:
        r10 = java.lang.Math.max(r12, r8);
        r0 = r20;
        r0 = r0.lineHeights;
        r18 = r0;
        r19 = r18.iterator();
    L_0x008e:
        r18 = r19.hasNext();
        if (r18 != 0) goto L_0x01a2;
    L_0x0094:
        r18 = android.view.View.MeasureSpec.getMode(r21);
        r19 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r0 = r18;
        r1 = r19;
        if (r0 != r1) goto L_0x01de;
    L_0x00a0:
        r0 = r20;
        r0.setMeasuredDimension(r15, r10);
    L_0x00a5:
        return;
    L_0x00a6:
        r18 = 0;
        r19 = 0;
        r4 = android.view.View.MeasureSpec.makeMeasureSpec(r18, r19);
        goto L_0x0052;
    L_0x00af:
        r0 = r20;
        r3 = r0.getChildAt(r11);
        if (r3 == 0) goto L_0x00c3;
    L_0x00b7:
        r18 = r3.getVisibility();
        r19 = 8;
        r0 = r18;
        r1 = r19;
        if (r0 != r1) goto L_0x00c5;
    L_0x00c3:
        if (r3 != 0) goto L_0x0164;
    L_0x00c5:
        if (r3 == 0) goto L_0x0168;
    L_0x00c7:
        r18 = r3.getLayoutParams();
        r18 = (com.vkontakte.android.ui.FlowLayout.LayoutParams) r18;
        r13 = r18;
    L_0x00cf:
        if (r3 == 0) goto L_0x00e4;
    L_0x00d1:
        r0 = r13.width;
        r18 = r0;
        if (r18 > 0) goto L_0x017a;
    L_0x00d7:
        r18 = -2147483648; // 0xffffffff80000000 float:-0.0 double:NaN;
        r0 = r18;
        r18 = android.view.View.MeasureSpec.makeMeasureSpec(r15, r0);
    L_0x00df:
        r0 = r18;
        r3.measure(r0, r4);
    L_0x00e4:
        r0 = r13.width;
        r18 = r0;
        if (r18 > 0) goto L_0x0189;
    L_0x00ea:
        if (r3 == 0) goto L_0x0186;
    L_0x00ec:
        r6 = r3.getMeasuredWidth();
    L_0x00f0:
        r0 = r13.height;
        r18 = r0;
        if (r18 > 0) goto L_0x0190;
    L_0x00f6:
        if (r3 == 0) goto L_0x018d;
    L_0x00f8:
        r5 = r3.getMeasuredHeight();
    L_0x00fc:
        if (r2 != 0) goto L_0x010e;
    L_0x00fe:
        r18 = r16 + r6;
        r0 = r20;
        r0 = r0.pwidth;
        r19 = r0;
        r19 = r19 + r15;
        r0 = r18;
        r1 = r19;
        if (r0 <= r1) goto L_0x012c;
    L_0x010e:
        r16 = r20.getPaddingLeft();
        r18 = java.lang.Math.max(r12, r8);
        r17 = r17 + r18;
        r0 = r20;
        r0 = r0.lineHeights;
        r18 = r0;
        r19 = java.lang.Math.max(r12, r8);
        r19 = java.lang.Integer.valueOf(r19);
        r18.add(r19);
        r12 = 0;
        r8 = 0;
        r2 = 0;
    L_0x012c:
        r0 = r13.vertical_spacing;
        r18 = r0;
        r18 = r18 + r5;
        r0 = r18;
        r12 = java.lang.Math.max(r12, r0);
        r0 = r13.floating;
        r18 = r0;
        if (r18 == 0) goto L_0x0194;
    L_0x013e:
        r0 = r13.vertical_spacing;
        r18 = r0;
        r18 = r18 + r5;
        r17 = r17 + r18;
        r0 = r13.vertical_spacing;
        r18 = r0;
        r18 = r18 + r5;
        r8 = r8 + r18;
        r18 = r16 + r6;
        r0 = r18;
        r14 = java.lang.Math.max(r14, r0);
    L_0x0156:
        r2 = r13.breakAfter;
        r0 = r13.horizontal_spacing;
        r18 = r0;
        r18 = r16 - r18;
        r0 = r18;
        r14 = java.lang.Math.max(r14, r0);
    L_0x0164:
        r11 = r11 + 1;
        goto L_0x005e;
    L_0x0168:
        r0 = r20;
        r0 = r0.lparams;
        r18 = r0;
        r0 = r18;
        r18 = r0.get(r11);
        r18 = (com.vkontakte.android.ui.FlowLayout.LayoutParams) r18;
        r13 = r18;
        goto L_0x00cf;
    L_0x017a:
        r0 = r13.width;
        r18 = r0;
        r19 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r18 = android.view.View.MeasureSpec.makeMeasureSpec(r18, r19);
        goto L_0x00df;
    L_0x0186:
        r6 = r15;
        goto L_0x00f0;
    L_0x0189:
        r6 = r13.width;
        goto L_0x00f0;
    L_0x018d:
        r5 = 0;
        goto L_0x00fc;
    L_0x0190:
        r5 = r13.height;
        goto L_0x00fc;
    L_0x0194:
        r8 = 0;
        r0 = r13.horizontal_spacing;
        r18 = r0;
        r18 = r18 + r6;
        r16 = r16 + r18;
        goto L_0x0156;
    L_0x019e:
        r18 = 0;
        goto L_0x0070;
    L_0x01a2:
        r18 = r19.next();
        r18 = (java.lang.Integer) r18;
        r9 = r18.intValue();
        r10 = r10 + r9;
        goto L_0x008e;
    L_0x01af:
        r18 = android.view.View.MeasureSpec.getMode(r22);
        r19 = -2147483648; // 0xffffffff80000000 float:-0.0 double:NaN;
        r0 = r18;
        r1 = r19;
        if (r0 != r1) goto L_0x0094;
    L_0x01bb:
        r18 = r17 + r12;
        r0 = r18;
        if (r0 >= r10) goto L_0x0094;
    L_0x01c1:
        r10 = r12;
        r0 = r20;
        r0 = r0.lineHeights;
        r18 = r0;
        r19 = r18.iterator();
    L_0x01cc:
        r18 = r19.hasNext();
        if (r18 == 0) goto L_0x0094;
    L_0x01d2:
        r18 = r19.next();
        r18 = (java.lang.Integer) r18;
        r9 = r18.intValue();
        r10 = r10 + r9;
        goto L_0x01cc;
    L_0x01de:
        r0 = r20;
        r0.setMeasuredDimension(r14, r10);
        goto L_0x00a5;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.ui.FlowLayout.onMeasure(int, int):void");
    }

    protected android.view.ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(Global.scale(ImageViewHolder.PaddingSize), Global.scale(ImageViewHolder.PaddingSize));
    }

    protected boolean checkLayoutParams(android.view.ViewGroup.LayoutParams p) {
        if (p instanceof LayoutParams) {
            return true;
        }
        return false;
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = getChildCount();
        int width = r - l;
        int xpos = getPaddingLeft();
        int ypos = getPaddingTop();
        int line = 0;
        boolean breakNext = false;
        boolean prevFloat = false;
        int floatY = 0;
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != 8) {
                LayoutParams lp = (LayoutParams) child.getLayoutParams();
                int childw = lp.width <= 0 ? child.getMeasuredWidth() : lp.width;
                int childh = lp.height <= 0 ? child.getMeasuredHeight() : lp.height;
                if (!lp.floating && prevFloat) {
                    ypos = floatY;
                }
                if (breakNext || xpos + childw > this.pwidth + width) {
                    xpos = getPaddingLeft();
                    ypos += ((Integer) this.lineHeights.elementAt(line)).intValue();
                    line++;
                }
                if (lp.center) {
                    xpos = (getWidth() / 2) - (childw / 2);
                }
                child.layout(xpos, ypos, xpos + childw, ypos + childh);
                if (lp.floating) {
                    if (!prevFloat) {
                        floatY = ypos;
                        prevFloat = true;
                    }
                    ypos += lp.vertical_spacing + childh;
                } else {
                    prevFloat = false;
                    xpos += lp.horizontal_spacing + childw;
                }
                breakNext = lp.breakAfter;
            }
        }
    }
}
