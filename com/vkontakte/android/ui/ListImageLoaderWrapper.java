package com.vkontakte.android.ui;

import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.ListView;
import com.vkontakte.android.ImageCache;
import com.vkontakte.android.Log;
import java.lang.reflect.Method;

public class ListImageLoaderWrapper implements OnScrollListener {
    private static final String TAG = "vk-img-wrapper";
    private NewListImageLoader imgLoader;
    private boolean isScrolling;
    private long lastChangeTime;
    private boolean lastScrollFwd;
    private AdapterView<?> list;
    private Listener listener;
    private Runnable loadRunnable;
    private int prevVisFirst;
    private int prevVisLast;
    private boolean retrying;
    private OnScrollListener scrollListener;
    private Runnable scrollStopRunnable;
    private int viCount;
    private int viStart;
    private boolean wasFastScrolling;

    /* renamed from: com.vkontakte.android.ui.ListImageLoaderWrapper.1 */
    class C10021 implements OnPreDrawListener {
        double prevPos;

        C10021() {
        }

        public boolean onPreDraw() {
            if (ListImageLoaderWrapper.this.list.getChildCount() > 0) {
                double pos = (double) ListImageLoaderWrapper.this.list.getFirstVisiblePosition();
                View topItem = null;
                for (int i = 0; i < ListImageLoaderWrapper.this.list.getChildCount(); i++) {
                    topItem = ListImageLoaderWrapper.this.list.getChildAt(i);
                    if (topItem.getHeight() > 0) {
                        break;
                    }
                }
                if (topItem != null) {
                    pos += Math.abs(((double) (topItem.getHeight() - topItem.getTop())) / ((double) topItem.getHeight()));
                    if (pos != this.prevPos) {
                        ListImageLoaderWrapper.this.isScrolling = true;
                        if (ListImageLoaderWrapper.this.scrollStopRunnable != null) {
                            ListImageLoaderWrapper.this.list.removeCallbacks(ListImageLoaderWrapper.this.scrollStopRunnable);
                            ListImageLoaderWrapper.this.scrollStopRunnable = null;
                        }
                        if (ListImageLoaderWrapper.this.isScrolling) {
                            ListImageLoaderWrapper.this.scrollStopRunnable = new ScrollStopDetector(null);
                            ListImageLoaderWrapper.this.list.postDelayed(ListImageLoaderWrapper.this.scrollStopRunnable, 150);
                        }
                    }
                    this.prevPos = pos;
                }
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.ui.ListImageLoaderWrapper.2 */
    class C10032 implements Runnable {
        boolean updated;

        C10032() {
            this.updated = false;
        }

        public void run() {
            if (!this.updated) {
                this.updated = true;
                ListImageLoaderWrapper.this.doUpdateImages();
            }
        }
    }

    /* renamed from: com.vkontakte.android.ui.ListImageLoaderWrapper.3 */
    class C10043 implements OnPreDrawListener {
        private final /* synthetic */ Runnable val$r;

        C10043(Runnable runnable) {
            this.val$r = runnable;
        }

        public boolean onPreDraw() {
            ListImageLoaderWrapper.this.list.getViewTreeObserver().removeOnPreDrawListener(this);
            this.val$r.run();
            return true;
        }
    }

    public interface Listener {
        void onScrollStarted();

        void onScrollStopped();

        void onScrolledToLastItem();
    }

    private class Loader implements Runnable {
        private Loader() {
        }

        public void run() {
            ListImageLoaderWrapper.this.imgLoader.loadRange(ListImageLoaderWrapper.this.viStart - ListImageLoaderWrapper.this.getNumHeaders(), (ListImageLoaderWrapper.this.viStart + ListImageLoaderWrapper.this.viCount) - ListImageLoaderWrapper.this.getNumHeaders());
            if (ListImageLoaderWrapper.this.lastScrollFwd) {
                ListImageLoaderWrapper.this.imgLoader.loadRange(ListImageLoaderWrapper.this.viStart + ListImageLoaderWrapper.this.viCount, ListImageLoaderWrapper.this.viStart + (ListImageLoaderWrapper.this.viCount * 2));
                ListImageLoaderWrapper.this.imgLoader.loadRange((ListImageLoaderWrapper.this.viStart - ListImageLoaderWrapper.this.getNumHeaders()) - ListImageLoaderWrapper.this.viCount, ListImageLoaderWrapper.this.viStart + ListImageLoaderWrapper.this.viCount);
            } else {
                ListImageLoaderWrapper.this.imgLoader.loadRange((ListImageLoaderWrapper.this.viStart - ListImageLoaderWrapper.this.getNumHeaders()) - ListImageLoaderWrapper.this.viCount, ListImageLoaderWrapper.this.viStart + ListImageLoaderWrapper.this.viCount);
                ListImageLoaderWrapper.this.imgLoader.loadRange(ListImageLoaderWrapper.this.viStart + ListImageLoaderWrapper.this.viCount, ListImageLoaderWrapper.this.viStart + (ListImageLoaderWrapper.this.viCount * 2));
            }
            ListImageLoaderWrapper.this.loadRunnable = null;
        }
    }

    private class ScrollStopDetector implements Runnable {
        private ScrollStopDetector() {
        }

        public void run() {
            ListImageLoaderWrapper.this.realScrollStateChanged((AbsListView) ListImageLoaderWrapper.this.list, 0);
            ListImageLoaderWrapper.this.isScrolling = false;
            ListImageLoaderWrapper.this.scrollStopRunnable = null;
        }
    }

    public interface ExtendedListener extends Listener {
        void onScroll(int i, int i2, int i3);
    }

    public ListImageLoaderWrapper(ListImageLoaderAdapter adapter, AdapterView<?> listView, Listener listener) {
        this.retrying = false;
        this.lastScrollFwd = false;
        this.lastChangeTime = 0;
        this.wasFastScrolling = false;
        this.isScrolling = false;
        this.imgLoader = new NewListImageLoader();
        this.imgLoader.setAdapter(adapter);
        this.listener = listener;
        this.list = listView;
        if (this.list instanceof AbsListView) {
            ((AbsListView) this.list).setOnScrollListener(this);
        } else {
            try {
                Method m = this.list.getClass().getMethod("setOnScrollListener", new Class[]{OnScrollListener.class});
                if (m != null) {
                    m.invoke(this.list, new Object[]{this});
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
        listView.getViewTreeObserver().addOnPreDrawListener(new C10021());
    }

    public void setOnScrollListener(OnScrollListener sl) {
        this.scrollListener = sl;
    }

    public void updateImages() {
        Runnable r = new C10032();
        this.list.getViewTreeObserver().addOnPreDrawListener(new C10043(r));
        this.list.postDelayed(r, 100);
    }

    private void doUpdateImages() {
        NewListImageLoader newListImageLoader = this.imgLoader;
        this.isScrolling = false;
        newListImageLoader.setIsScrolling(false);
        this.viStart = this.list.getFirstVisiblePosition();
        this.viCount = this.list.getLastVisiblePosition() - this.viStart;
        this.retrying = false;
        if (this.viCount <= 0) {
            this.viCount = Math.max(5, this.list.getLastVisiblePosition() - this.list.getFirstVisiblePosition());
        }
        this.imgLoader.cancelAll();
        this.imgLoader.loadRange(this.viStart - getNumHeaders(), (this.viStart + this.viCount) - getNumHeaders());
        this.imgLoader.loadRange(this.viStart + this.viCount, this.viStart + (this.viCount * 2));
        this.imgLoader.loadRange((this.viStart - getNumHeaders()) - this.viCount, this.viStart + this.viCount);
    }

    public void clear() {
    }

    public void activate() {
        updateImages();
    }

    public void deactivate() {
        this.imgLoader.cancelAll();
    }

    public boolean isAlreadyLoaded(String url) {
        return ImageCache.isInTopCache(url);
    }

    public Bitmap get(String url) {
        return ImageCache.getFromTop(url);
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (this.scrollListener != null) {
            this.scrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
        }
        if (this.viStart != firstVisibleItem) {
            boolean z;
            if (this.viStart < firstVisibleItem) {
                z = true;
            } else {
                z = false;
            }
            this.lastScrollFwd = z;
        }
        this.viCount = visibleItemCount;
        this.viStart = firstVisibleItem;
        int visLast = this.viCount + this.viStart;
        if (!(visLast == this.prevVisLast && this.viStart == this.prevVisFirst)) {
            if (System.currentTimeMillis() - this.lastChangeTime <= 150) {
                this.imgLoader.cancelAll();
                this.wasFastScrolling = true;
            } else if (this.wasFastScrolling) {
                this.imgLoader.setIsScrolling(true);
                this.wasFastScrolling = false;
                this.imgLoader.loadRange(this.list.getFirstVisiblePosition() - getNumHeaders(), this.list.getLastVisiblePosition());
                if (this.lastScrollFwd) {
                    this.imgLoader.loadRange(this.viStart + this.viCount, this.viStart + (this.viCount * 2));
                    this.imgLoader.loadRange((this.viStart - getNumHeaders()) - this.viCount, this.viStart + this.viCount);
                } else {
                    this.imgLoader.loadRange((this.viStart - getNumHeaders()) - this.viCount, this.viStart + this.viCount);
                    this.imgLoader.loadRange(this.viStart + this.viCount, this.viStart + (this.viCount * 2));
                }
            } else {
                if (this.prevVisFirst > this.viStart) {
                    this.imgLoader.loadRange(this.viStart, this.prevVisFirst - 1);
                }
                if (this.prevVisLast < visLast) {
                    this.imgLoader.loadRange(this.prevVisLast + 1, visLast);
                }
                if (this.prevVisLast > visLast) {
                    this.imgLoader.cancelRange(visLast + 1, this.prevVisLast);
                }
                if (this.prevVisFirst < this.viStart) {
                    this.imgLoader.cancelRange(this.prevVisFirst, this.viStart - 1);
                }
            }
            this.lastChangeTime = System.currentTimeMillis();
            this.prevVisFirst = this.viStart;
            this.prevVisLast = visLast;
        }
        if (!(firstVisibleItem + visibleItemCount != totalItemCount || visibleItemCount == 0 || totalItemCount == 0 || this.listener == null)) {
            this.listener.onScrolledToLastItem();
        }
        if (this.listener != null && (this.listener instanceof ExtendedListener)) {
            ((ExtendedListener) this.listener).onScroll(firstVisibleItem - getNumHeaders(), visibleItemCount, (totalItemCount - getNumHeaders()) - getNumFooters());
        }
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState != 0) {
            realScrollStateChanged(view, scrollState);
            this.isScrolling = true;
        }
    }

    private void realScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == 0 && this.listener != null) {
            this.listener.onScrollStopped();
        }
        if (scrollState == 1 && this.listener != null) {
            this.listener.onScrollStarted();
        }
        if (scrollState == 1) {
            this.imgLoader.cancelAll();
        }
        if (this.scrollListener != null) {
            this.scrollListener.onScrollStateChanged(view, scrollState);
        }
        if (scrollState == 0) {
            this.imgLoader.setIsScrolling(false);
            this.wasFastScrolling = false;
            this.imgLoader.cancelAll();
            this.imgLoader.loadRange(this.viStart - getNumHeaders(), this.viStart + this.viCount);
            if (this.lastScrollFwd) {
                this.imgLoader.loadRange(this.viStart + this.viCount, this.viStart + (this.viCount * 2));
                this.imgLoader.loadRange((this.viStart - getNumHeaders()) - this.viCount, this.viStart + this.viCount);
                return;
            }
            this.imgLoader.loadRange((this.viStart - getNumHeaders()) - this.viCount, this.viStart + this.viCount);
            this.imgLoader.loadRange(this.viStart + this.viCount, this.viStart + (this.viCount * 2));
            return;
        }
        if (this.loadRunnable != null) {
            this.list.removeCallbacks(this.loadRunnable);
            this.loadRunnable = null;
        }
        this.imgLoader.setIsScrolling(true);
    }

    private int getNumHeaders() {
        if (this.list instanceof ListView) {
            return ((ListView) this.list).getHeaderViewsCount();
        }
        return 0;
    }

    private int getNumFooters() {
        if (this.list instanceof ListView) {
            return ((ListView) this.list).getFooterViewsCount();
        }
        return 0;
    }

    public AdapterView<?> getListView() {
        return this.list;
    }
}
