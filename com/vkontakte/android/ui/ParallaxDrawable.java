package com.vkontakte.android.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;

public class ParallaxDrawable extends Drawable {
    private Bitmap bmp;
    private int[] cornerRadiuses;
    private float offset;
    private Paint paint;
    private Path path;
    private BitmapShader shader;

    public ParallaxDrawable() {
        this.offset = 0.0f;
        this.cornerRadiuses = new int[4];
        this.paint = new Paint();
        this.paint.setFilterBitmap(true);
        this.paint.setAntiAlias(true);
    }

    public void setBitmap(Bitmap b) {
        this.bmp = b;
        if (this.bmp != null) {
            this.shader = new BitmapShader(this.bmp, TileMode.REPEAT, TileMode.CLAMP);
            this.paint.setShader(this.shader);
        }
    }

    public void setOffset(float off) {
        this.offset = off;
    }

    public void draw(Canvas c) {
        if (this.bmp != null) {
            Rect bnds = copyBounds();
            int h = this.bmp.getHeight();
            int w = Math.round(((float) this.bmp.getWidth()) * (((float) bnds.height()) / ((float) h)));
            if (w < bnds.width()) {
                w = (int) (((double) w) * Math.ceil((double) (((float) bnds.width()) / ((float) w))));
            }
            int x = bnds.left - Math.round(((float) (w - bnds.width())) * this.offset);
            Matrix matrix = new Matrix();
            matrix.setScale(((float) bnds.height()) / ((float) h), ((float) bnds.height()) / ((float) h));
            matrix.postTranslate((float) x, 0.0f);
            this.shader.setLocalMatrix(matrix);
            if (this.path == null) {
                this.path = new Path();
                this.path.addRoundRect(new RectF(bnds), new float[]{(float) this.cornerRadiuses[0], (float) this.cornerRadiuses[0], (float) this.cornerRadiuses[1], (float) this.cornerRadiuses[1], (float) this.cornerRadiuses[2], (float) this.cornerRadiuses[2], (float) this.cornerRadiuses[3], (float) this.cornerRadiuses[3]}, Direction.CW);
            }
            c.drawPath(this.path, this.paint);
        }
    }

    public void setCornersRadius(int topLeft, int topRight, int bottomLeft, int bottomRight) {
        this.cornerRadiuses = new int[]{topLeft, topRight, bottomLeft, bottomRight};
        this.path = null;
    }

    public void setBounds(Rect rect) {
        super.setBounds(rect);
        this.path = null;
    }

    public void setBounds(int l, int t, int r, int b) {
        super.setBounds(l, t, r, b);
        this.path = null;
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int alpha) {
    }

    public void setColorFilter(ColorFilter cf) {
    }
}
