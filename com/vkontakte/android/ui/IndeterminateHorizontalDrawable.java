package com.vkontakte.android.ui;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import com.vkontakte.android.Global;

public class IndeterminateHorizontalDrawable extends Drawable {
    private Paint bgPaint;
    private Paint fgPaint;
    private int maxOffset;
    private int offset;

    public IndeterminateHorizontalDrawable() {
        this.fgPaint = new Paint();
        this.fgPaint.setColor(-1381395);
        this.fgPaint.setAntiAlias(true);
        this.fgPaint.setStrokeWidth((float) Global.scale(10.0f));
        this.fgPaint.setStrokeCap(Cap.BUTT);
        this.bgPaint = new Paint();
        this.bgPaint.setColor(-13388315);
        this.bgPaint.setAntiAlias(true);
        this.maxOffset = Global.scale(40.0f);
    }

    public void draw(Canvas c) {
        int height = Global.scale(3.0f);
        Rect b = new Rect();
        copyBounds(b);
        b.top = b.centerY() - (height / 2);
        b.bottom = b.top + height;
        c.drawRect(new RectF(b), this.bgPaint);
        c.save();
        c.clipRect(b);
        c.translate((float) b.left, (float) b.top);
        int i = -1;
        while (i < 100 && Global.scale(40.0f) * i <= b.width()) {
            c.drawLine((float) (((Global.scale(40.0f) * i) - Global.scale(20.0f)) + this.offset), (float) (b.height() + Global.scale(20.0f)), (float) ((((i + 1) * Global.scale(40.0f)) + Global.scale(20.0f)) + this.offset), (float) (-Global.scale(20.0f)), this.fgPaint);
            i++;
        }
        this.offset += Global.scale(1.0f);
        this.offset %= this.maxOffset;
        c.restore();
        if (isVisible()) {
            invalidateSelf();
        }
    }

    public int getOpacity() {
        return -3;
    }

    public void setAlpha(int alpha) {
    }

    public void setColorFilter(ColorFilter cf) {
    }
}
