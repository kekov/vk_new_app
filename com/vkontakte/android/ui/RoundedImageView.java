package com.vkontakte.android.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.ExploreByTouchHelper;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.vkontakte.android.BitmapHack;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageCache;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;

public class RoundedImageView extends ImageView {
    private Bitmap bitmap;
    private float cornerRadius;
    private Drawable image;
    private boolean needRecreateBitmap;
    private Bitmap overlay;
    private String title;

    public RoundedImageView(Context context) {
        super(context);
        this.cornerRadius = 4.0f;
        this.needRecreateBitmap = true;
        this.bitmap = null;
        this.overlay = null;
        this.title = null;
        init(context, null, 0);
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.cornerRadius = 4.0f;
        this.needRecreateBitmap = true;
        this.bitmap = null;
        this.overlay = null;
        this.title = null;
        init(context, attrs, 0);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.cornerRadius = 4.0f;
        this.needRecreateBitmap = true;
        this.bitmap = null;
        this.overlay = null;
        this.title = null;
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            this.cornerRadius = (float) context.obtainStyledAttributes(attrs, C0436R.styleable.RoundedImageView, defStyle, 0).getDimensionPixelSize(0, 0);
        }
    }

    public void setImageBitmap(Bitmap b) {
        setImageDrawable(new BitmapDrawable(b));
    }

    public void setImageResource(int resID) {
        setImageDrawable(getResources().getDrawable(resID));
    }

    public void setImageDrawable(Drawable d) {
        this.image = d;
        this.needRecreateBitmap = true;
        invalidate();
    }

    public void setCornerRadius(int r) {
        this.cornerRadius = (float) r;
        this.needRecreateBitmap = true;
    }

    public void setOverlay(Bitmap overlay) {
        this.overlay = overlay;
        this.needRecreateBitmap = true;
    }

    public void setTitle(String title) {
        this.title = title;
        this.needRecreateBitmap = true;
    }

    private void recreateBitmap() {
        try {
            int w = getWidth();
            int h = getHeight();
            int pr = getPaddingRight();
            int pl = getPaddingLeft();
            int pt = getPaddingTop();
            int pb = getPaddingBottom();
            if (Global.useBitmapHack && this.bitmap != null) {
                BitmapHack.unhackBitmap(this.bitmap);
            }
            this.bitmap = Bitmap.createBitmap((w - pr) - pl, (h - pb) - pt, Config.ARGB_8888);
            if (Global.useBitmapHack) {
                BitmapHack.hackBitmap(this.bitmap);
            }
            Canvas canvas = new Canvas(this.bitmap);
            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, (w - pl) - pr, (h - pb) - pt);
            RectF rectF = new RectF(rect);
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(-1);
            canvas.drawRoundRect(rectF, this.cornerRadius, this.cornerRadius, paint);
            paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
            paint.setFilterBitmap(true);
            if (this.image instanceof BitmapDrawable) {
                Bitmap img = ((BitmapDrawable) this.image).getBitmap();
                if (img != null) {
                    Rect imgRect = new Rect();
                    float vratio = ((float) ((w - pl) - pr)) / ((float) ((h - pb) - pt));
                    float iratio = ((float) img.getWidth()) / ((float) img.getHeight());
                    if (getScaleType() != ScaleType.CENTER_CROP ? vratio <= iratio : vratio > iratio) {
                        float ih = ((float) img.getHeight()) * (rectF.width() / ((float) img.getWidth()));
                        imgRect = new Rect(0, (int) ((rectF.height() / ImageViewHolder.PaddingSize) - (ih / ImageViewHolder.PaddingSize)), rect.width(), (int) ((rectF.height() / ImageViewHolder.PaddingSize) + (ih / ImageViewHolder.PaddingSize)));
                    } else {
                        float iw = ((float) img.getWidth()) * (rectF.height() / ((float) img.getHeight()));
                        imgRect = new Rect((int) ((rectF.width() / ImageViewHolder.PaddingSize) - (iw / ImageViewHolder.PaddingSize)), 0, (int) ((rectF.width() / ImageViewHolder.PaddingSize) + (iw / ImageViewHolder.PaddingSize)), rect.height());
                    }
                    canvas.drawBitmap(img, new Rect(0, 0, img.getWidth(), img.getHeight()), imgRect, paint);
                }
            } else {
                this.image.setBounds(0, 0, (w - pr) - pl, (h - pb) - pt);
                this.image.draw(canvas);
            }
            if (this.overlay != null) {
                canvas.drawBitmap(this.overlay, (float) ((((w - pr) - pl) / 2) - (this.overlay.getWidth() / 2)), (float) ((((h - pb) - pt) / 2) - (this.overlay.getHeight() / 2)), null);
            }
            if (this.title != null) {
                Paint bpaint = new Paint();
                bpaint.setColor(ExploreByTouchHelper.INVALID_ID);
                bpaint.setAntiAlias(true);
                Paint tpaint = new Paint();
                tpaint.setTypeface(Global.regFont);
                tpaint.setTextSize((float) Global.scale(12.0f));
                tpaint.setAntiAlias(true);
                tpaint.setColor(-1);
                tpaint.setShadowLayer(1.0E-8f, 0.0f, 1.0f, Color.WINDOW_BACKGROUND_COLOR);
                int th = ((int) ((-tpaint.ascent()) + tpaint.descent())) + Global.scale(6.0f);
                int len = tpaint.breakText(this.title, true, (float) (((w - pl) - pr) - Global.scale(10.0f)), null);
                canvas.save();
                canvas.clipRect(new Rect(0, (int) (((float) ((h - pt) - pb)) - this.cornerRadius), (w - pl) - pr, (h - pt) - pb));
                canvas.drawRoundRect(new RectF(0.0f, (float) (((h - pt) - pb) - th), (float) ((w - pr) - pl), (float) ((h - pt) - pb)), this.cornerRadius, this.cornerRadius, bpaint);
                canvas.restore();
                canvas.drawRect(0.0f, (float) (((h - pt) - pb) - th), (float) ((w - pr) - pl), (float) ((int) (((float) ((h - pt) - pb)) - this.cornerRadius)), bpaint);
                if (len < this.title.length()) {
                    canvas.drawText(new StringBuilder(String.valueOf(this.title.substring(0, len))).append("...").toString(), (float) Global.scale(3.0f), (((float) (((h - pt) - pb) - th)) - tpaint.ascent()) + ((float) Global.scale(4.0f)), tpaint);
                    return;
                }
                canvas.drawText(this.title, ((float) (((w - pl) - pr) / 2)) - (tpaint.measureText(this.title) / ImageViewHolder.PaddingSize), (((float) (((h - pt) - pb) - th)) - tpaint.ascent()) + ((float) Global.scale(4.0f)), tpaint);
            }
        } catch (Throwable th2) {
            ImageCache.clearTopLevel();
        }
    }

    public void onDraw(Canvas c) {
        int w = getWidth();
        int h = getHeight();
        int pr = getPaddingRight();
        int pl = getPaddingLeft();
        int pt = getPaddingTop();
        int pb = getPaddingBottom();
        if (this.image != null) {
            if (this.needRecreateBitmap) {
                recreateBitmap();
                this.needRecreateBitmap = false;
            }
            if (this.bitmap != null) {
                c.drawBitmap(this.bitmap, (float) pl, (float) pt, null);
            }
        }
    }

    public void finalize() {
        if (Global.useBitmapHack && this.bitmap != null) {
            BitmapHack.unhackBitmap(this.bitmap);
        }
    }
}
