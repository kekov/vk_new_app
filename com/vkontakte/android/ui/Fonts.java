package com.vkontakte.android.ui;

import android.graphics.Typeface;
import android.os.Build.VERSION;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashMap;
import org.acra.ACRAConstants;

public class Fonts {
    private static HashMap<Integer, Typeface> condensed;
    private static Typeface lightItalicTypeface;
    private static Typeface lightTypeface;

    static {
        condensed = new HashMap();
    }

    public static Typeface getRobotoLight() {
        if (lightTypeface != null) {
            return lightTypeface;
        }
        if (VERSION.SDK_INT >= 16) {
            lightTypeface = Typeface.create("sans-serif-light", 0);
        } else {
            lightTypeface = Typeface.createFromAsset(VKApplication.context.getAssets(), "fonts/Roboto-Light.ttf");
        }
        return lightTypeface;
    }

    public static Typeface getRobotoLightItalic() {
        if (lightItalicTypeface != null) {
            return lightItalicTypeface;
        }
        if (VERSION.SDK_INT >= 16) {
            lightItalicTypeface = Typeface.create("sans-serif-light", 2);
        } else {
            lightItalicTypeface = Typeface.createFromAsset(VKApplication.context.getAssets(), "fonts/Roboto-LightItalic.ttf");
        }
        return lightItalicTypeface;
    }

    public static Typeface getRobotoCondensed(int style) {
        if (condensed.containsKey(Integer.valueOf(style))) {
            return (Typeface) condensed.get(Integer.valueOf(style));
        }
        Typeface result;
        if (VERSION.SDK_INT >= 16) {
            result = Typeface.create("sans-serif-condensed", style);
        } else {
            String file = ACRAConstants.DEFAULT_STRING_VALUE;
            switch (style) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    file = "fonts/RobotoCondensed-Bold.ttf";
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    file = "fonts/RobotoCondensed-Italic.ttf";
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    file = "fonts/RobotoCondensed-BoldItalic.ttf";
                    break;
                default:
                    file = "fonts/RobotoCondensed-Regular.ttf";
                    break;
            }
            result = Typeface.createFromAsset(VKApplication.context.getAssets(), file);
        }
        condensed.put(Integer.valueOf(style), result);
        return result;
    }
}
