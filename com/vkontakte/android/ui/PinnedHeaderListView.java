package com.vkontakte.android.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v4.widget.ExploreByTouchHelper;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListAdapter;

public class PinnedHeaderListView extends RefreshableListView implements OnScrollListener {
    private PinnedSectionedHeaderAdapter mAdapter;
    private View mCurrentHeader;
    private int mCurrentSection;
    private float mHeaderOffset;
    private boolean mShouldPin;
    private OnScrollListener scrollListener;

    public interface PinnedSectionedHeaderAdapter {
        int getSectionForPosition(int i);

        View getSectionHeaderView(int i, View view, ViewGroup viewGroup);

        boolean isSectionHeader(int i);
    }

    public PinnedHeaderListView(Context context) {
        super(context);
        this.mShouldPin = true;
        this.mCurrentSection = 0;
        this.scrollListener = null;
        super.setOnScrollListener(this);
    }

    public PinnedHeaderListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mShouldPin = true;
        this.mCurrentSection = 0;
        this.scrollListener = null;
        super.setOnScrollListener(this);
    }

    public PinnedHeaderListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mShouldPin = true;
        this.mCurrentSection = 0;
        this.scrollListener = null;
        super.setOnScrollListener(this);
    }

    public void setPinHeaders(boolean shouldPin) {
        this.mShouldPin = shouldPin;
    }

    public void setAdapter(ListAdapter adapter) {
        this.mAdapter = (PinnedSectionedHeaderAdapter) adapter;
        super.setAdapter(adapter);
    }

    public void onLayout(boolean changed, int l, int t, int r, int b) {
        try {
            super.onLayout(changed, l, t, r, b);
            int firstVis = getFirstVisiblePosition() - getHeaderViewsCount();
            if (this.mCurrentHeader != null && firstVis >= 0) {
                this.mCurrentHeader.measure(MeasureSpec.makeMeasureSpec(((r - l) - getPaddingLeft()) - getPaddingRight(), 1073741824), MeasureSpec.makeMeasureSpec(b - t, ExploreByTouchHelper.INVALID_ID));
                this.mCurrentHeader.layout(getPaddingLeft(), getPaddingTop(), getPaddingLeft() + this.mCurrentHeader.getMeasuredWidth(), getPaddingTop() + this.mCurrentHeader.getMeasuredHeight());
            }
        } catch (Exception e) {
        }
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (this.scrollListener != null) {
            this.scrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
        }
        firstVisibleItem -= getHeaderViewsCount();
        if (firstVisibleItem < 0) {
            if (this.mCurrentHeader != null) {
                this.mCurrentHeader.setVisibility(0);
                requestLayout();
            }
        } else if (this.mAdapter != null && this.mShouldPin) {
            this.mCurrentHeader = getHeaderView(this.mAdapter.getSectionForPosition(firstVisibleItem), this.mCurrentHeader);
            this.mHeaderOffset = 0.0f;
            for (int i = firstVisibleItem; i < firstVisibleItem + visibleItemCount; i++) {
                if (this.mAdapter.isSectionHeader(i)) {
                    View header = getChildAt(i - firstVisibleItem);
                    float headerTop = (float) header.getTop();
                    float pinnedHeaderHeight = 0.0f;
                    if (this.mCurrentHeader != null) {
                        pinnedHeaderHeight = (float) this.mCurrentHeader.getMeasuredHeight();
                    }
                    header.setVisibility(0);
                    if (pinnedHeaderHeight >= headerTop && headerTop > 0.0f) {
                        this.mHeaderOffset = headerTop - ((float) header.getHeight());
                    } else if (headerTop <= 0.0f) {
                        header.setVisibility(4);
                    }
                }
            }
            invalidate();
        }
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (this.scrollListener != null) {
            this.scrollListener.onScrollStateChanged(view, scrollState);
        }
    }

    private View getHeaderView(int section, View oldView) {
        boolean shouldLayout = section != this.mCurrentSection || oldView == null;
        if ((this.mAdapter instanceof MultiSectionAdapter) && !((MultiSectionAdapter) this.mAdapter).isSectionHeaderVisible(section)) {
            return null;
        }
        View view = this.mAdapter.getSectionHeaderView(section, oldView, this);
        if (!shouldLayout) {
            return view;
        }
        ensurePinnedHeaderLayout(view);
        this.mCurrentSection = section;
        return view;
    }

    private void ensurePinnedHeaderLayout(View header) {
        if (header.isLayoutRequested()) {
            int heightSpec;
            int widthSpec = MeasureSpec.makeMeasureSpec(getWidth(), 1073741824);
            LayoutParams layoutParams = header.getLayoutParams();
            if (layoutParams == null || layoutParams.height <= 0) {
                heightSpec = MeasureSpec.makeMeasureSpec(0, 0);
            } else {
                heightSpec = MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824);
            }
            header.measure(widthSpec, heightSpec);
            header.layout(0, 0, getWidth(), header.getMeasuredHeight());
        }
    }

    public void dispatchDraw(Canvas canvas) {
        try {
            super.dispatchDraw(canvas);
            if (getFirstVisiblePosition() < getHeaderViewsCount()) {
                if (this.mCurrentHeader != null) {
                    this.mCurrentHeader.invalidate();
                }
            } else if (this.mAdapter != null && this.mShouldPin && this.mCurrentHeader != null) {
                int saveCount = canvas.save();
                canvas.translate((float) getPaddingLeft(), this.mHeaderOffset);
                canvas.clipRect(0, 0, getWidth(), this.mCurrentHeader.getMeasuredHeight());
                this.mCurrentHeader.draw(canvas);
                canvas.restoreToCount(saveCount);
            }
        } catch (Exception e) {
        }
    }

    public void setOnScrollListener(OnScrollListener l) {
        this.scrollListener = l;
    }
}
