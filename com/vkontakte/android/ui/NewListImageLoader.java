package com.vkontakte.android.ui;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageCache.RequestWrapper;
import com.vkontakte.android.Log;
import com.vkontakte.android.background.WorkerThread;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.Semaphore;

public class NewListImageLoader {
    private static final boolean DEBUG = false;
    private static final String TAG = "vk-img-loader";
    private static final int THREAD_COUNT = 4;
    private static ArrayList<WorkerThread> threads;
    private ListImageLoaderAdapter adapter;
    private Vector<RunnableTask> incomplete;
    private boolean isScrolling;
    private Semaphore semaphore;
    private int threadIndex;

    private class RunnableTask implements Runnable {
        public boolean canceled;
        public int image;
        public int item;
        private RequestWrapper reqWrapper;
        public boolean set;
        public int thread;
        public String url;

        private RunnableTask() {
            this.canceled = NewListImageLoader.DEBUG;
            this.set = true;
        }

        public void cancel() {
            this.canceled = true;
            try {
                if (this.reqWrapper != null) {
                    this.reqWrapper.request.abort();
                }
            } catch (Exception e) {
            }
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r10 = this;
            r5 = r10.canceled;
            if (r5 == 0) goto L_0x0005;
        L_0x0004:
            return;
        L_0x0005:
            r5 = new com.vkontakte.android.ImageCache$RequestWrapper;	 Catch:{ Exception -> 0x00b8 }
            r5.<init>();	 Catch:{ Exception -> 0x00b8 }
            r10.reqWrapper = r5;	 Catch:{ Exception -> 0x00b8 }
            r0 = 0;
            r5 = r10.url;	 Catch:{ Exception -> 0x00b8 }
            r6 = "M";
            r5 = r5.startsWith(r6);	 Catch:{ Exception -> 0x00b8 }
            if (r5 == 0) goto L_0x0080;
        L_0x0017:
            r5 = r10.url;	 Catch:{ Exception -> 0x00b8 }
            r6 = "\\|";
            r4 = r5.split(r6);	 Catch:{ Exception -> 0x00b8 }
            r5 = r4.length;	 Catch:{ Exception -> 0x00b8 }
            r5 = r5 + -1;
            r6 = 4;
            r3 = java.lang.Math.min(r5, r6);	 Catch:{ Exception -> 0x00b8 }
            r1 = new android.graphics.Bitmap[r3];	 Catch:{ Exception -> 0x00b8 }
            r2 = 1;
        L_0x002a:
            r5 = r3 + 1;
            if (r2 < r5) goto L_0x006e;
        L_0x002e:
            r5 = r10.set;	 Catch:{ Exception -> 0x00b8 }
            if (r5 == 0) goto L_0x003b;
        L_0x0032:
            r0 = com.vkontakte.android.ui.NewListImageLoader.drawMultichatPhoto(r1);	 Catch:{ Exception -> 0x00b8 }
            r5 = r10.url;	 Catch:{ Exception -> 0x00b8 }
            com.vkontakte.android.ImageCache.put(r5, r0);	 Catch:{ Exception -> 0x00b8 }
        L_0x003b:
            r5 = r10.canceled;	 Catch:{ Exception -> 0x00b8 }
            if (r5 != 0) goto L_0x0004;
        L_0x003f:
            r5 = r10.set;	 Catch:{ Exception -> 0x00b8 }
            if (r5 == 0) goto L_0x0052;
        L_0x0043:
            if (r0 == 0) goto L_0x0052;
        L_0x0045:
            r5 = com.vkontakte.android.ui.NewListImageLoader.this;	 Catch:{ Exception -> 0x00b8 }
            r5 = r5.adapter;	 Catch:{ Exception -> 0x00b8 }
            r6 = r10.item;	 Catch:{ Exception -> 0x00b8 }
            r7 = r10.image;	 Catch:{ Exception -> 0x00b8 }
            r5.imageLoaded(r6, r7, r0);	 Catch:{ Exception -> 0x00b8 }
        L_0x0052:
            r5 = com.vkontakte.android.ui.NewListImageLoader.this;	 Catch:{ Exception -> 0x00b6 }
            r5 = r5.semaphore;	 Catch:{ Exception -> 0x00b6 }
            r5.acquire();	 Catch:{ Exception -> 0x00b6 }
        L_0x005b:
            r5 = com.vkontakte.android.ui.NewListImageLoader.this;
            r5 = r5.incomplete;
            r5.remove(r10);
            r5 = com.vkontakte.android.ui.NewListImageLoader.this;
            r5 = r5.semaphore;
            r5.release();
            goto L_0x0004;
        L_0x006e:
            r5 = r2 + -1;
            r6 = r4[r2];	 Catch:{ Exception -> 0x00b8 }
            r7 = r10.reqWrapper;	 Catch:{ Exception -> 0x00b8 }
            r8 = 0;
            r9 = r10.set;	 Catch:{ Exception -> 0x00b8 }
            r6 = com.vkontakte.android.ImageCache.get(r6, r7, r8, r9);	 Catch:{ Exception -> 0x00b8 }
            r1[r5] = r6;	 Catch:{ Exception -> 0x00b8 }
            r2 = r2 + 1;
            goto L_0x002a;
        L_0x0080:
            r5 = r10.url;	 Catch:{ Exception -> 0x00b8 }
            r6 = "A";
            r5 = r5.startsWith(r6);	 Catch:{ Exception -> 0x00b8 }
            if (r5 == 0) goto L_0x00aa;
        L_0x008a:
            r5 = r10.url;	 Catch:{ Exception -> 0x00b8 }
            r6 = "\\|";
            r4 = r5.split(r6);	 Catch:{ Exception -> 0x00b8 }
            r2 = 1;
        L_0x0093:
            r5 = r4.length;	 Catch:{ Exception -> 0x00b8 }
            if (r2 >= r5) goto L_0x003b;
        L_0x0096:
            r5 = r4[r2];	 Catch:{ Exception -> 0x00b8 }
            r6 = r10.reqWrapper;	 Catch:{ Exception -> 0x00b8 }
            r7 = 0;
            r8 = r10.set;	 Catch:{ Exception -> 0x00b8 }
            r0 = com.vkontakte.android.ImageCache.get(r5, r6, r7, r8);	 Catch:{ Exception -> 0x00b8 }
            if (r0 != 0) goto L_0x003b;
        L_0x00a3:
            r5 = r10.set;	 Catch:{ Exception -> 0x00b8 }
            if (r5 == 0) goto L_0x003b;
        L_0x00a7:
            r2 = r2 + 1;
            goto L_0x0093;
        L_0x00aa:
            r5 = r10.url;	 Catch:{ Exception -> 0x00b8 }
            r6 = r10.reqWrapper;	 Catch:{ Exception -> 0x00b8 }
            r7 = 0;
            r8 = r10.set;	 Catch:{ Exception -> 0x00b8 }
            r0 = com.vkontakte.android.ImageCache.get(r5, r6, r7, r8);	 Catch:{ Exception -> 0x00b8 }
            goto L_0x003b;
        L_0x00b6:
            r5 = move-exception;
            goto L_0x005b;
        L_0x00b8:
            r5 = move-exception;
            goto L_0x0052;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.ui.NewListImageLoader.RunnableTask.run():void");
        }

        public void setDecode(boolean decode) {
            this.set = decode;
            if (this.reqWrapper != null) {
                this.reqWrapper.decode = decode;
            }
        }

        public String toString() {
            return "[" + this.item + "/" + this.image + "] " + this.url + " t" + this.thread;
        }
    }

    static {
        threads = new ArrayList();
        for (int i = 0; i < THREAD_COUNT; i++) {
            WorkerThread wt = new WorkerThread("ImageLoader thread #" + (i + 1));
            wt.start();
            threads.add(wt);
        }
    }

    public NewListImageLoader() {
        this.incomplete = new Vector();
        this.threadIndex = new Random().nextInt(THREAD_COUNT);
        this.semaphore = new Semaphore(1, true);
    }

    public void setAdapter(ListImageLoaderAdapter a) {
        this.adapter = a;
    }

    public ListImageLoaderAdapter getAdapter() {
        return this.adapter;
    }

    public void loadRange(int start, int end) {
        try {
            start = Math.max(0, start);
            end = Math.min(end, this.adapter.getItemCount() - 1);
            for (int i = start; i <= end; i++) {
                loadSingleItem(i);
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    public void loadSingleItem(int item) {
        try {
            this.semaphore.acquire();
        } catch (Exception e) {
        }
        try {
            int cnt = this.adapter.getImageCountForItem(item);
            for (int i = 0; i < cnt; i++) {
                RunnableTask task = new RunnableTask();
                task.item = item;
                task.image = i;
                task.url = this.adapter.getImageURL(item, i);
                task.set = this.isScrolling ? DEBUG : true;
                task.thread = this.threadIndex;
                this.incomplete.add(task);
                ((WorkerThread) threads.get(this.threadIndex)).postRunnable(task, 0);
                this.threadIndex++;
                this.threadIndex %= THREAD_COUNT;
            }
        } catch (Exception e2) {
        }
        this.semaphore.release();
    }

    public boolean isLoading(int item) {
        try {
            this.semaphore.acquire();
        } catch (Exception e) {
        }
        Iterator it = this.incomplete.iterator();
        while (it.hasNext()) {
            if (((RunnableTask) it.next()).item == item) {
                this.semaphore.release();
                return true;
            }
        }
        this.semaphore.release();
        return DEBUG;
    }

    public void cancel(int item) {
        try {
            this.semaphore.acquire();
        } catch (Exception e) {
        }
        Iterator<RunnableTask> itr = this.incomplete.iterator();
        while (itr.hasNext()) {
            RunnableTask t = (RunnableTask) itr.next();
            if (t.item == item) {
                itr.remove();
                t.cancel();
            }
        }
        this.semaphore.release();
    }

    public void cancelRange(int start, int end) {
        try {
            this.semaphore.acquire();
        } catch (Exception e) {
        }
        Iterator<RunnableTask> itr = this.incomplete.iterator();
        while (itr.hasNext()) {
            RunnableTask t = (RunnableTask) itr.next();
            if (t.item >= start && t.item <= end) {
                itr.remove();
                t.cancel();
            }
        }
        this.semaphore.release();
    }

    public void cancelAll() {
        try {
            this.semaphore.acquire();
        } catch (Exception e) {
        }
        Iterator it = this.incomplete.iterator();
        while (it.hasNext()) {
            ((RunnableTask) it.next()).cancel();
        }
        this.incomplete.clear();
        this.semaphore.release();
    }

    public void setIsScrolling(boolean s) {
        this.isScrolling = s;
        try {
            this.semaphore.acquire();
        } catch (Exception e) {
        }
        Iterator it = this.incomplete.iterator();
        while (it.hasNext()) {
            ((RunnableTask) it.next()).setDecode(s ? DEBUG : true);
        }
        this.semaphore.release();
    }

    public boolean isScrolling() {
        return this.isScrolling;
    }

    public static Bitmap drawMultichatPhoto(Bitmap[] bmps) {
        int bs = (int) (50.0f * Global.displayDensity);
        Bitmap bmp = Bitmap.createBitmap(bs, bs, Config.ARGB_8888);
        Canvas c = new Canvas(bmp);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        if (bmps.length == 2) {
            c.drawBitmap(bmps[0], new Rect(bmps[0].getWidth() / THREAD_COUNT, 0, (bmps[0].getWidth() / THREAD_COUNT) * 3, bmps[0].getHeight()), new Rect(0, 0, bs / 2, bs), paint);
            c.drawBitmap(bmps[1], new Rect(bmps[1].getWidth() / THREAD_COUNT, 0, (bmps[1].getWidth() / THREAD_COUNT) * 3, bmps[1].getHeight()), new Rect(bs / 2, 0, bs, bs), paint);
        } else if (bmps.length == 3) {
            c.drawBitmap(bmps[0], new Rect(bmps[0].getWidth() / THREAD_COUNT, 0, (bmps[0].getWidth() / THREAD_COUNT) * 3, bmps[0].getHeight()), new Rect(0, 0, bs / 2, bs), paint);
            c.drawBitmap(bmps[1], new Rect(0, 0, bmps[1].getWidth(), bmps[1].getHeight()), new Rect(bs / 2, 0, bs, bs / 2), paint);
            c.drawBitmap(bmps[2], new Rect(0, 0, bmps[2].getWidth(), bmps[2].getHeight()), new Rect(bs / 2, bs / 2, bs, bs), paint);
        } else if (bmps.length == THREAD_COUNT) {
            c.drawBitmap(bmps[0], new Rect(0, 0, bmps[0].getWidth(), bmps[0].getHeight()), new Rect(0, 0, bs / 2, bs / 2), paint);
            c.drawBitmap(bmps[1], new Rect(0, 0, bmps[1].getWidth(), bmps[1].getHeight()), new Rect(0, bs / 2, bs / 2, bs), paint);
            c.drawBitmap(bmps[2], new Rect(0, 0, bmps[2].getWidth(), bmps[2].getHeight()), new Rect(bs / 2, 0, bs, bs / 2), paint);
            c.drawBitmap(bmps[3], new Rect(0, 0, bmps[3].getWidth(), bmps[3].getHeight()), new Rect(bs / 2, bs / 2, bs, bs), paint);
        }
        return bmp;
    }
}
