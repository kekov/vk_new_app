package com.vkontakte.android.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Rect;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewPropertyAnimator;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SwipeDismissListViewTouchListener implements OnTouchListener {
    private long mAnimationTime;
    private DismissCallbacks mCallbacks;
    private int mDismissAnimationRefCount;
    private int mDownPosition;
    private View mDownView;
    private float mDownX;
    private float mDownY;
    private ListView mListView;
    private int mMaxFlingVelocity;
    private int mMinFlingVelocity;
    private boolean mPaused;
    private List<PendingDismissData> mPendingDismisses;
    private int mSlop;
    private boolean mSwiping;
    private int mSwipingSlop;
    private VelocityTracker mVelocityTracker;
    private int mViewWidth;

    /* renamed from: com.vkontakte.android.ui.SwipeDismissListViewTouchListener.1 */
    class C10591 implements OnScrollListener {
        C10591() {
        }

        public void onScrollStateChanged(AbsListView absListView, int scrollState) {
            boolean z = true;
            SwipeDismissListViewTouchListener swipeDismissListViewTouchListener = SwipeDismissListViewTouchListener.this;
            if (scrollState == 1) {
                z = false;
            }
            swipeDismissListViewTouchListener.setEnabled(z);
        }

        public void onScroll(AbsListView absListView, int i, int i1, int i2) {
        }
    }

    /* renamed from: com.vkontakte.android.ui.SwipeDismissListViewTouchListener.2 */
    class C10602 extends AnimatorListenerAdapter {
        private final /* synthetic */ int val$downPosition;
        private final /* synthetic */ View val$downView;

        C10602(View view, int i) {
            this.val$downView = view;
            this.val$downPosition = i;
        }

        public void onAnimationEnd(Animator animation) {
            SwipeDismissListViewTouchListener.this.performDismiss(this.val$downView, this.val$downPosition);
        }
    }

    /* renamed from: com.vkontakte.android.ui.SwipeDismissListViewTouchListener.3 */
    class C10613 extends AnimatorListenerAdapter {
        private final /* synthetic */ int val$originalHeight;

        C10613(int i) {
            this.val$originalHeight = i;
        }

        public void onAnimationEnd(Animator animation) {
            SwipeDismissListViewTouchListener swipeDismissListViewTouchListener = SwipeDismissListViewTouchListener.this;
            swipeDismissListViewTouchListener.mDismissAnimationRefCount = swipeDismissListViewTouchListener.mDismissAnimationRefCount - 1;
            if (SwipeDismissListViewTouchListener.this.mDismissAnimationRefCount == 0) {
                Collections.sort(SwipeDismissListViewTouchListener.this.mPendingDismisses);
                int[] dismissPositions = new int[SwipeDismissListViewTouchListener.this.mPendingDismisses.size()];
                for (int i = SwipeDismissListViewTouchListener.this.mPendingDismisses.size() - 1; i >= 0; i--) {
                    dismissPositions[i] = ((PendingDismissData) SwipeDismissListViewTouchListener.this.mPendingDismisses.get(i)).position;
                }
                SwipeDismissListViewTouchListener.this.mCallbacks.onDismiss(SwipeDismissListViewTouchListener.this.mListView, dismissPositions);
                SwipeDismissListViewTouchListener.this.mDownPosition = -1;
                for (PendingDismissData pendingDismiss : SwipeDismissListViewTouchListener.this.mPendingDismisses) {
                    pendingDismiss.view.setAlpha(1.0f);
                    pendingDismiss.view.setTranslationX(0.0f);
                    LayoutParams lp = pendingDismiss.view.getLayoutParams();
                    lp.height = this.val$originalHeight;
                    pendingDismiss.view.setLayoutParams(lp);
                }
                long time = SystemClock.uptimeMillis();
                SwipeDismissListViewTouchListener.this.mListView.dispatchTouchEvent(MotionEvent.obtain(time, time, 3, 0.0f, 0.0f, 0));
                SwipeDismissListViewTouchListener.this.mPendingDismisses.clear();
            }
        }
    }

    /* renamed from: com.vkontakte.android.ui.SwipeDismissListViewTouchListener.4 */
    class C10624 implements AnimatorUpdateListener {
        private final /* synthetic */ View val$dismissView;
        private final /* synthetic */ LayoutParams val$lp;

        C10624(LayoutParams layoutParams, View view) {
            this.val$lp = layoutParams;
            this.val$dismissView = view;
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.val$lp.height = ((Integer) valueAnimator.getAnimatedValue()).intValue();
            this.val$dismissView.setLayoutParams(this.val$lp);
        }
    }

    public interface DismissCallbacks {
        boolean canDismiss(int i);

        void onDismiss(ListView listView, int[] iArr);
    }

    class PendingDismissData implements Comparable<PendingDismissData> {
        public int position;
        public View view;

        public PendingDismissData(int position, View view) {
            this.position = position;
            this.view = view;
        }

        public int compareTo(PendingDismissData other) {
            return other.position - this.position;
        }
    }

    public SwipeDismissListViewTouchListener(ListView listView, DismissCallbacks callbacks) {
        this.mViewWidth = 1;
        this.mPendingDismisses = new ArrayList();
        this.mDismissAnimationRefCount = 0;
        ViewConfiguration vc = ViewConfiguration.get(listView.getContext());
        this.mSlop = vc.getScaledTouchSlop();
        this.mMinFlingVelocity = vc.getScaledMinimumFlingVelocity() * 16;
        this.mMaxFlingVelocity = vc.getScaledMaximumFlingVelocity();
        this.mAnimationTime = (long) listView.getContext().getResources().getInteger(17694720);
        this.mListView = listView;
        this.mCallbacks = callbacks;
    }

    public void setEnabled(boolean enabled) {
        this.mPaused = !enabled;
    }

    public OnScrollListener makeScrollListener() {
        return new C10591();
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int i = this.mViewWidth;
        if (r0 < 2) {
            this.mViewWidth = this.mListView.getWidth();
        }
        float deltaX;
        switch (motionEvent.getActionMasked()) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                if (this.mPaused) {
                    return false;
                }
                Rect rect = new Rect();
                int childCount = this.mListView.getChildCount();
                int[] listViewCoords = new int[2];
                this.mListView.getLocationOnScreen(listViewCoords);
                int x = ((int) motionEvent.getRawX()) - listViewCoords[0];
                int y = ((int) motionEvent.getRawY()) - listViewCoords[1];
                for (int i2 = 0; i2 < childCount; i2++) {
                    View child = this.mListView.getChildAt(i2);
                    child.getHitRect(rect);
                    if (rect.contains(x, y)) {
                        this.mDownView = child;
                        if (this.mDownView != null) {
                            this.mDownX = motionEvent.getRawX();
                            this.mDownY = motionEvent.getRawY();
                            this.mDownPosition = this.mListView.getPositionForView(this.mDownView);
                            if (this.mCallbacks.canDismiss(this.mDownPosition)) {
                                this.mDownView = null;
                            } else {
                                this.mVelocityTracker = VelocityTracker.obtain();
                                this.mVelocityTracker.addMovement(motionEvent);
                            }
                        }
                        return false;
                    }
                }
                if (this.mDownView != null) {
                    this.mDownX = motionEvent.getRawX();
                    this.mDownY = motionEvent.getRawY();
                    this.mDownPosition = this.mListView.getPositionForView(this.mDownView);
                    if (this.mCallbacks.canDismiss(this.mDownPosition)) {
                        this.mDownView = null;
                    } else {
                        this.mVelocityTracker = VelocityTracker.obtain();
                        this.mVelocityTracker.addMovement(motionEvent);
                    }
                }
                return false;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                if (this.mVelocityTracker != null) {
                    deltaX = motionEvent.getRawX() - this.mDownX;
                    this.mVelocityTracker.addMovement(motionEvent);
                    this.mVelocityTracker.computeCurrentVelocity(LocationStatusCodes.GEOFENCE_NOT_AVAILABLE);
                    float velocityX = this.mVelocityTracker.getXVelocity();
                    float absVelocityX = Math.abs(velocityX);
                    float absVelocityY = Math.abs(this.mVelocityTracker.getYVelocity());
                    boolean dismiss = false;
                    boolean dismissRight = false;
                    if (Math.abs(deltaX) <= ((float) (this.mViewWidth / 2)) || !this.mSwiping) {
                        if (((float) this.mMinFlingVelocity) <= absVelocityX) {
                            if (absVelocityX <= ((float) this.mMaxFlingVelocity) && absVelocityY < absVelocityX && this.mSwiping) {
                                dismiss = ((velocityX > 0.0f ? 1 : (velocityX == 0.0f ? 0 : -1)) < 0 ? 1 : null) == ((deltaX > 0.0f ? 1 : (deltaX == 0.0f ? 0 : -1)) < 0 ? 1 : null);
                                dismissRight = this.mVelocityTracker.getXVelocity() > 0.0f;
                            }
                        }
                    } else {
                        dismiss = true;
                        dismissRight = deltaX > 0.0f;
                    }
                    if (dismiss) {
                        i = this.mDownPosition;
                        if (r0 != -1) {
                            View downView = this.mDownView;
                            int downPosition = this.mDownPosition;
                            this.mDismissAnimationRefCount++;
                            ViewPropertyAnimator animate = this.mDownView.animate();
                            if (dismissRight) {
                                i = this.mViewWidth;
                            } else {
                                i = -this.mViewWidth;
                            }
                            animate.translationX((float) i).alpha(0.0f).setDuration(this.mAnimationTime).setListener(new C10602(downView, downPosition));
                            this.mVelocityTracker.recycle();
                            this.mVelocityTracker = null;
                            this.mDownX = 0.0f;
                            this.mDownY = 0.0f;
                            this.mDownView = null;
                            this.mDownPosition = -1;
                            this.mSwiping = false;
                            break;
                        }
                    }
                    this.mDownView.animate().translationX(0.0f).alpha(1.0f).setDuration(this.mAnimationTime).setListener(null);
                    this.mVelocityTracker.recycle();
                    this.mVelocityTracker = null;
                    this.mDownX = 0.0f;
                    this.mDownY = 0.0f;
                    this.mDownView = null;
                    this.mDownPosition = -1;
                    this.mSwiping = false;
                }
                break;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                if (!(this.mVelocityTracker == null || this.mPaused)) {
                    this.mVelocityTracker.addMovement(motionEvent);
                    deltaX = motionEvent.getRawX() - this.mDownX;
                    float deltaY = motionEvent.getRawY() - this.mDownY;
                    if (Math.abs(deltaX) > ((float) this.mSlop) && Math.abs(deltaY) < Math.abs(deltaX) / ImageViewHolder.PaddingSize) {
                        this.mSwiping = true;
                        if (deltaX > 0.0f) {
                            i = this.mSlop;
                        } else {
                            i = -this.mSlop;
                        }
                        this.mSwipingSlop = i;
                        this.mListView.requestDisallowInterceptTouchEvent(true);
                        MotionEvent cancelEvent = MotionEvent.obtain(motionEvent);
                        cancelEvent.setAction((motionEvent.getActionIndex() << 8) | 3);
                        this.mListView.onTouchEvent(cancelEvent);
                        cancelEvent.recycle();
                    }
                    if (this.mSwiping) {
                        this.mDownView.setTranslationX(deltaX - ((float) this.mSwipingSlop));
                        this.mDownView.setAlpha(Math.max(0.0f, Math.min(1.0f, 1.0f - ((ImageViewHolder.PaddingSize * Math.abs(deltaX)) / ((float) this.mViewWidth)))));
                        return true;
                    }
                }
                break;
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                if (this.mVelocityTracker != null) {
                    if (this.mDownView != null && this.mSwiping) {
                        this.mDownView.animate().translationX(0.0f).alpha(1.0f).setDuration(this.mAnimationTime).setListener(null);
                    }
                    this.mVelocityTracker.recycle();
                    this.mVelocityTracker = null;
                    this.mDownX = 0.0f;
                    this.mDownY = 0.0f;
                    this.mDownView = null;
                    this.mDownPosition = -1;
                    this.mSwiping = false;
                    break;
                }
                break;
        }
        return false;
    }

    private void performDismiss(View dismissView, int dismissPosition) {
        LayoutParams lp = dismissView.getLayoutParams();
        ValueAnimator animator = ValueAnimator.ofInt(new int[]{dismissView.getHeight(), 1}).setDuration(this.mAnimationTime);
        animator.addListener(new C10613(originalHeight));
        animator.addUpdateListener(new C10624(lp, dismissView));
        this.mPendingDismisses.add(new PendingDismissData(dismissPosition, dismissView));
        animator.start();
    }
}
