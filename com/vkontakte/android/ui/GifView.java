package com.vkontakte.android.ui;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class GifView extends View {
    private Thread animThread;
    private int curFrame;
    private boolean error;
    private Paint errorBgPaint;
    private Paint errorPaint;
    private Bitmap frameBuffer;
    private ArrayList<Frame> frames;
    private int gifH;
    private int gifW;
    private Paint imgPaint;
    private boolean loaded;
    private int loop;
    private boolean runAnimation;
    private InputStream stream;
    private Object syncLock;
    private boolean waiting;

    /* renamed from: com.vkontakte.android.ui.GifView.1 */
    class C09991 implements Runnable {
        private final /* synthetic */ String val$url;

        C09991(String str) {
            this.val$url = str;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r13 = this;
            r10 = "vk";
            r11 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0062 }
            r12 = "Loading: ";
            r11.<init>(r12);	 Catch:{ Exception -> 0x0062 }
            r12 = r13.val$url;	 Catch:{ Exception -> 0x0062 }
            r11 = r11.append(r12);	 Catch:{ Exception -> 0x0062 }
            r11 = r11.toString();	 Catch:{ Exception -> 0x0062 }
            com.vkontakte.android.Log.m525d(r10, r11);	 Catch:{ Exception -> 0x0062 }
            r8 = new java.net.URL;	 Catch:{ Exception -> 0x0062 }
            r10 = r13.val$url;	 Catch:{ Exception -> 0x0062 }
            r8.<init>(r10);	 Catch:{ Exception -> 0x0062 }
            r1 = r8.openConnection();	 Catch:{ Exception -> 0x0062 }
            r1.connect();	 Catch:{ Exception -> 0x0062 }
            r2 = new java.io.File;	 Catch:{ Exception -> 0x0062 }
            r10 = android.os.Environment.getExternalStorageDirectory();	 Catch:{ Exception -> 0x0062 }
            r11 = ".vkontakte";
            r2.<init>(r10, r11);	 Catch:{ Exception -> 0x0062 }
            r10 = r2.exists();	 Catch:{ Exception -> 0x0062 }
            if (r10 != 0) goto L_0x0038;
        L_0x0035:
            r2.mkdir();	 Catch:{ Exception -> 0x0062 }
        L_0x0038:
            r5 = new java.io.File;	 Catch:{ Exception -> 0x0062 }
            r10 = ".nomedia";
            r5.<init>(r2, r10);	 Catch:{ Exception -> 0x0062 }
            r10 = r5.exists();	 Catch:{ Exception -> 0x0062 }
            if (r10 != 0) goto L_0x0048;
        L_0x0045:
            r5.createNewFile();	 Catch:{ Exception -> 0x0062 }
        L_0x0048:
            r3 = new java.io.File;	 Catch:{ Exception -> 0x0062 }
            r10 = "tmp.gif";
            r3.<init>(r2, r10);	 Catch:{ Exception -> 0x0062 }
            r3.createNewFile();	 Catch:{ Exception -> 0x0062 }
            r10 = r1.getContentLength();	 Catch:{ Exception -> 0x0062 }
            r11 = 52428800; // 0x3200000 float:4.7019774E-37 double:2.5903269E-316;
            if (r10 <= r11) goto L_0x0074;
        L_0x005a:
            r10 = new java.io.IOException;	 Catch:{ Exception -> 0x0062 }
            r11 = "too big";
            r10.<init>(r11);	 Catch:{ Exception -> 0x0062 }
            throw r10;	 Catch:{ Exception -> 0x0062 }
        L_0x0062:
            r9 = move-exception;
            r10 = "vk";
            com.vkontakte.android.Log.m532w(r10, r9);
            r10 = com.vkontakte.android.ui.GifView.this;
            r11 = 1;
            r10.error = r11;
            r10 = com.vkontakte.android.ui.GifView.this;
            r10.postInvalidate();
        L_0x0073:
            return;
        L_0x0074:
            r6 = new java.io.FileOutputStream;	 Catch:{ Exception -> 0x0062 }
            r6.<init>(r3);	 Catch:{ Exception -> 0x0062 }
            r4 = r1.getInputStream();	 Catch:{ Exception -> 0x0062 }
            r10 = 10240; // 0x2800 float:1.4349E-41 double:5.059E-320;
            r0 = new byte[r10];	 Catch:{ Exception -> 0x0062 }
            r7 = 0;
        L_0x0082:
            r7 = r4.read(r0);	 Catch:{ Exception -> 0x0062 }
            if (r7 > 0) goto L_0x008f;
        L_0x0088:
            r4.close();	 Catch:{ Exception -> 0x0062 }
            r6.close();	 Catch:{ Exception -> 0x0062 }
            goto L_0x0073;
        L_0x008f:
            r10 = 0;
            r6.write(r0, r10, r7);	 Catch:{ Exception -> 0x0062 }
            r10 = com.vkontakte.android.ui.GifView.this;	 Catch:{ Exception -> 0x0062 }
            r10 = r10.loaded;	 Catch:{ Exception -> 0x0062 }
            if (r10 != 0) goto L_0x00a4;
        L_0x009b:
            r10 = com.vkontakte.android.ui.GifView.this;	 Catch:{ Exception -> 0x0062 }
            r11 = r3.getAbsolutePath();	 Catch:{ Exception -> 0x0062 }
            r10.load(r11);	 Catch:{ Exception -> 0x0062 }
        L_0x00a4:
            r10 = com.vkontakte.android.ui.GifView.this;	 Catch:{ Exception -> 0x0062 }
            r10 = r10.waiting;	 Catch:{ Exception -> 0x0062 }
            if (r10 == 0) goto L_0x0082;
        L_0x00ac:
            r10 = com.vkontakte.android.ui.GifView.this;	 Catch:{ Exception -> 0x0062 }
            r11 = 0;
            r10.waiting = r11;	 Catch:{ Exception -> 0x0062 }
            r10 = com.vkontakte.android.ui.GifView.this;	 Catch:{ Exception -> 0x0062 }
            r11 = r10.syncLock;	 Catch:{ Exception -> 0x0062 }
            monitor-enter(r11);	 Catch:{ Exception -> 0x0062 }
            r10 = com.vkontakte.android.ui.GifView.this;	 Catch:{ all -> 0x00c4 }
            r10 = r10.syncLock;	 Catch:{ all -> 0x00c4 }
            r10.notify();	 Catch:{ all -> 0x00c4 }
            monitor-exit(r11);	 Catch:{ all -> 0x00c4 }
            goto L_0x0082;
        L_0x00c4:
            r10 = move-exception;
            monitor-exit(r11);	 Catch:{ all -> 0x00c4 }
            throw r10;	 Catch:{ Exception -> 0x0062 }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.ui.GifView.1.run():void");
        }
    }

    private class AnimationRunner implements Runnable {

        /* renamed from: com.vkontakte.android.ui.GifView.AnimationRunner.1 */
        class C10001 implements Runnable {
            C10001() {
            }

            public void run() {
                GifView.this.requestLayout();
            }
        }

        private AnimationRunner() {
        }

        public void run() {
            byte[] transpGif = new byte[43];
            transpGif[0] = (byte) 71;
            transpGif[1] = (byte) 73;
            transpGif[2] = (byte) 70;
            transpGif[3] = (byte) 56;
            transpGif[4] = (byte) 57;
            transpGif[5] = (byte) 97;
            transpGif[6] = (byte) 1;
            transpGif[8] = (byte) 1;
            transpGif[10] = Byte.MIN_VALUE;
            transpGif[13] = (byte) -1;
            transpGif[14] = (byte) -1;
            transpGif[15] = (byte) -1;
            transpGif[19] = (byte) 33;
            transpGif[20] = (byte) -7;
            transpGif[21] = (byte) 4;
            transpGif[22] = (byte) 1;
            transpGif[27] = (byte) 44;
            transpGif[32] = (byte) 1;
            transpGif[34] = (byte) 1;
            transpGif[37] = (byte) 2;
            transpGif[38] = (byte) 2;
            transpGif[39] = (byte) 68;
            transpGif[40] = (byte) 1;
            transpGif[42] = (byte) 59;
            boolean useAlphaHack = (BitmapFactory.decodeByteArray(transpGif, 0, transpGif.length).getPixel(0, 0) & Color.WINDOW_BACKGROUND_COLOR) != 0;
            if (useAlphaHack) {
                Log.m530w("vk", "Enabling gif alpha hack - Google, please fix this ASAP!");
            }
            while (GifView.this.runAnimation) {
                Paint clearPaint = new Paint();
                clearPaint.setColor(-1);
                InputStream dataInputStream = new DataInputStream(new SyncedInputStream(GifView.this.stream));
                byte[] sig = new byte[6];
                dataInputStream.readFully(sig);
                if (sig[0] == 71 && sig[1] == 73 && sig[2] == 70 && sig[3] == 56) {
                    try {
                        GifView.this.gifW = GifView.this.readShort(dataInputStream);
                        GifView.this.gifH = GifView.this.readShort(dataInputStream);
                        int lim = ((ActivityManager) GifView.this.getContext().getSystemService("activity")).getMemoryClass() > 32 ? 1500 : 800;
                        if (GifView.this.gifW > lim || GifView.this.gifH > lim) {
                            throw new IOException("Too big: " + GifView.this.gifW + "x" + GifView.this.gifH);
                        }
                        GifView.this.post(new C10001());
                        if (GifView.this.frameBuffer == null) {
                            GifView.this.frameBuffer = Bitmap.createBitmap(GifView.this.gifW, GifView.this.gifH, Config.ARGB_8888);
                        }
                        Canvas canvas = new Canvas(GifView.this.frameBuffer);
                        int flags = dataInputStream.read();
                        int bgColorIndex = dataInputStream.read();
                        dataInputStream.read();
                        byte[] globalPal = null;
                        if ((flags & TransportMediator.FLAG_KEY_MEDIA_NEXT) > 0) {
                            globalPal = new byte[((1 << ((flags & 7) + 1)) * 3)];
                            dataInputStream.readFully(globalPal);
                        }
                        clearPaint.setColor(globalPal != null ? ((((globalPal[bgColorIndex * 3] & MotionEventCompat.ACTION_MASK) << 16) | ((globalPal[(bgColorIndex * 3) + 1] & MotionEventCompat.ACTION_MASK) << 8)) | (globalPal[(bgColorIndex * 3) + 2] & MotionEventCompat.ACTION_MASK)) | Color.WINDOW_BACKGROUND_COLOR : Color.WINDOW_BACKGROUND_COLOR);
                        long delay = 100;
                        int transpColor = -1;
                        boolean clearBg = false;
                        long t = System.currentTimeMillis();
                        int n = 0;
                        while (GifView.this.runAnimation) {
                            int c = dataInputStream.read();
                            int lflags;
                            int l;
                            if (c == 33) {
                                int xtype = dataInputStream.read();
                                if (xtype == 249) {
                                    dataInputStream.read();
                                    lflags = dataInputStream.read();
                                    delay = (long) (GifView.this.readShort(dataInputStream) * 10);
                                    if (delay < 30) {
                                        delay = 100;
                                    }
                                    transpColor = dataInputStream.read();
                                    if ((lflags & 1) == 0) {
                                        transpColor = -1;
                                    }
                                    clearBg = (lflags >> 2) == 2;
                                    dataInputStream.read();
                                } else if (xtype == 255) {
                                    dataInputStream.read();
                                    byte[] _appid = new byte[8];
                                    dataInputStream.read(_appid);
                                    String appid = new String(_appid);
                                    dataInputStream.skip(3);
                                    if ("NETSCAPE".equals(appid)) {
                                        dataInputStream.read();
                                        dataInputStream.read();
                                        GifView.this.loop = GifView.this.readShort(dataInputStream);
                                        dataInputStream.read();
                                    } else {
                                        for (l = dataInputStream.read(); l > 0; l = dataInputStream.read()) {
                                            dataInputStream.skip((long) l);
                                        }
                                    }
                                } else {
                                    for (l = dataInputStream.read(); l > 0; l = dataInputStream.read()) {
                                        dataInputStream.skip((long) l);
                                    }
                                }
                            } else if (c == 44) {
                                byte[] bArr;
                                int left = GifView.this.readShort(dataInputStream);
                                int top = GifView.this.readShort(dataInputStream);
                                int width = GifView.this.readShort(dataInputStream);
                                int height = GifView.this.readShort(dataInputStream);
                                lflags = dataInputStream.read();
                                boolean interlaced = (lflags & 64) > 0;
                                byte[] localPal = null;
                                if ((lflags & TransportMediator.FLAG_KEY_MEDIA_NEXT) > 0) {
                                    localPal = new byte[((1 << ((lflags & 7) + 1)) * 3)];
                                    dataInputStream.readFully(localPal, 0, localPal.length);
                                }
                                ByteArrayOutputStream buf = new ByteArrayOutputStream();
                                buf.write("GIF89a".getBytes());
                                GifView.this.writeShort(buf, width);
                                GifView.this.writeShort(buf, height);
                                buf.write((localPal != null ? lflags & 7 : flags & 7) | 240);
                                buf.write(bgColorIndex);
                                buf.write(0);
                                if (localPal != null) {
                                    bArr = localPal;
                                } else {
                                    bArr = globalPal;
                                }
                                buf.write(bArr);
                                buf.write(33);
                                buf.write(249);
                                buf.write(4);
                                buf.write(transpColor >= 0 ? 1 : 0);
                                GifView.this.writeShort(buf, 0);
                                buf.write(transpColor);
                                buf.write(0);
                                buf.write(44);
                                GifView.this.writeShort(buf, 0);
                                GifView.this.writeShort(buf, 0);
                                GifView.this.writeShort(buf, width);
                                GifView.this.writeShort(buf, height);
                                buf.write(interlaced ? 64 : 0);
                                buf.write(dataInputStream.read());
                                l = dataInputStream.read();
                                buf.write(l);
                                byte[] db = new byte[MotionEventCompat.ACTION_MASK];
                                while (l > 0) {
                                    dataInputStream.readFully(db, 0, l);
                                    buf.write(db, 0, l);
                                    l = dataInputStream.read();
                                    buf.write(l);
                                }
                                buf.write(59);
                                Options opts = new Options();
                                opts.inPreferredConfig = Config.ARGB_8888;
                                Frame frame = new Frame(null);
                                frame.image = BitmapFactory.decodeByteArray(buf.toByteArray(), 0, buf.size(), opts);
                                frame.f57x = left;
                                frame.f58y = top;
                                frame.delay = delay;
                                frame.clear = clearBg;
                                if (useAlphaHack && transpColor >= 0 && !clearBg) {
                                    byte[] pal;
                                    int[] pixels = new int[(frame.image.getWidth() * frame.image.getHeight())];
                                    frame.image.getPixels(pixels, 0, frame.image.getWidth(), 0, 0, frame.image.getWidth(), frame.image.getHeight());
                                    if (localPal == null) {
                                        pal = globalPal;
                                    } else {
                                        pal = localPal;
                                    }
                                    if (transpColor < pal.length / 3) {
                                        int transpRGB = ((((pal[transpColor * 3] & MotionEventCompat.ACTION_MASK) << 16) | ((pal[(transpColor * 3) + 1] & MotionEventCompat.ACTION_MASK) << 8)) | (pal[(transpColor * 3) + 2] & MotionEventCompat.ACTION_MASK)) | Color.WINDOW_BACKGROUND_COLOR;
                                        for (int i = 0; i < pixels.length; i++) {
                                            if (pixels[i] == transpRGB) {
                                                pixels[i] = 0;
                                            }
                                        }
                                        frame.image = Bitmap.createBitmap(pixels, frame.image.getWidth(), frame.image.getHeight(), Config.ARGB_8888);
                                    }
                                }
                                if (delay > 0) {
                                    try {
                                        Thread.sleep(Math.max(0, delay - (System.currentTimeMillis() - t)));
                                    } catch (Exception e) {
                                    }
                                }
                                t = System.currentTimeMillis();
                                if (frame.clear) {
                                    canvas.drawRect(new Rect(0, 0, canvas.getWidth(), canvas.getHeight()), clearPaint);
                                }
                                try {
                                    canvas.drawBitmap(frame.image, (float) frame.f57x, (float) frame.f58y, null);
                                } catch (Exception e2) {
                                }
                                GifView.this.postInvalidate();
                                long nextDelay = frame.delay;
                                n++;
                            } else if (c == 59) {
                                ((FileInputStream) GifView.this.stream).getChannel().position(0);
                                break;
                            }
                        }
                    } catch (Throwable x) {
                        Log.m532w("vk", x);
                        GifView.this.error = true;
                        GifView.this.postInvalidate();
                    }
                } else {
                    dataInputStream.close();
                    throw new IOException("Incorrect GIF signature " + new String(sig));
                }
            }
            try {
                GifView.this.stream.close();
            } catch (Exception e3) {
            }
        }
    }

    private class Frame {
        boolean clear;
        long delay;
        Bitmap image;
        int f57x;
        int f58y;

        private Frame() {
        }
    }

    private class SyncedInputStream extends FilterInputStream {
        protected SyncedInputStream(InputStream in) {
            super(in);
        }

        private void waitForData() {
            GifView.this.waiting = true;
            synchronized (GifView.this.syncLock) {
                try {
                    GifView.this.syncLock.wait();
                } catch (Exception e) {
                }
            }
        }

        public int read() throws IOException {
            if (this.in.available() < 1) {
                waitForData();
            }
            return this.in.read();
        }

        public int read(byte[] b) throws IOException {
            if (this.in.available() < b.length) {
                waitForData();
            }
            return this.in.read(b);
        }

        public int read(byte[] b, int offset, int len) throws IOException {
            if (this.in.available() < len) {
                waitForData();
            }
            return this.in.read(b, offset, len);
        }
    }

    public GifView(Context context) {
        super(context);
        this.frames = new ArrayList();
        this.loop = 0;
        this.loaded = false;
        this.curFrame = 0;
        this.runAnimation = true;
        this.waiting = false;
        this.syncLock = new Object();
        this.imgPaint = new Paint();
        this.errorPaint = new Paint();
        this.errorBgPaint = new Paint();
        this.error = false;
        init();
    }

    public GifView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.frames = new ArrayList();
        this.loop = 0;
        this.loaded = false;
        this.curFrame = 0;
        this.runAnimation = true;
        this.waiting = false;
        this.syncLock = new Object();
        this.imgPaint = new Paint();
        this.errorPaint = new Paint();
        this.errorBgPaint = new Paint();
        this.error = false;
        init();
    }

    public GifView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.frames = new ArrayList();
        this.loop = 0;
        this.loaded = false;
        this.curFrame = 0;
        this.runAnimation = true;
        this.waiting = false;
        this.syncLock = new Object();
        this.imgPaint = new Paint();
        this.errorPaint = new Paint();
        this.errorBgPaint = new Paint();
        this.error = false;
        init();
    }

    private void init() {
        this.imgPaint.setFilterBitmap(true);
        this.errorPaint.setColor(Color.WINDOW_BACKGROUND_COLOR);
        this.errorPaint.setAntiAlias(true);
        this.errorPaint.setTextSize((float) Global.scale(15.0f));
        this.errorBgPaint.setColor(-2130706433);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.runAnimation = false;
        if (this.animThread != null) {
            this.animThread.interrupt();
        }
    }

    public void onMeasure(int wms, int hms) {
        if (this.gifW == 0 || this.gifH == 0) {
            super.onMeasure(wms, hms);
            setMeasuredDimension(getMeasuredWidth(), Global.scale(100.0f));
            return;
        }
        float vratio = ((float) MeasureSpec.getSize(wms)) / ((float) MeasureSpec.getSize(hms));
        float iratio = ((float) this.gifW) / ((float) this.gifH);
        if (Float.isInfinite(vratio)) {
            vratio = 0.0f;
        }
        int maxW = Math.min(Global.scale((float) this.gifW), MeasureSpec.getSize(wms));
        int maxH = Math.min(Global.scale((float) this.gifH), MeasureSpec.getSize(hms));
        if (vratio < iratio) {
            setMeasuredDimension(maxW, (int) ((((float) maxW) / ((float) this.gifW)) * ((float) this.gifH)));
        } else {
            setMeasuredDimension((int) ((((float) maxH) / ((float) this.gifH)) * ((float) this.gifW)), maxH);
        }
    }

    public void loadURL(String url) {
        new Thread(new C09991(url)).start();
    }

    public void load(String path) {
        try {
            load(new FileInputStream(new File(path)));
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    public void load(InputStream _s) {
        this.stream = _s;
        this.loaded = true;
        this.animThread = new Thread(new AnimationRunner());
        this.animThread.start();
    }

    private int readShort(InputStream is) throws IOException {
        return is.read() | (is.read() << 8);
    }

    private void writeShort(OutputStream os, int i) throws IOException {
        os.write(i);
        os.write(i >> 8);
    }

    public void onDraw(Canvas c) {
        if (this.loaded) {
            if (this.frameBuffer != null) {
                if (((float) c.getWidth()) / ((float) c.getHeight()) < ((float) this.frameBuffer.getWidth()) / ((float) this.frameBuffer.getHeight())) {
                    c.drawBitmap(this.frameBuffer, null, new Rect(0, 0, c.getWidth(), (int) ((((float) c.getWidth()) / ((float) this.frameBuffer.getWidth())) * ((float) this.frameBuffer.getHeight()))), this.imgPaint);
                } else {
                    c.drawBitmap(this.frameBuffer, null, new Rect(0, 0, (int) ((((float) c.getHeight()) / ((float) this.frameBuffer.getHeight())) * ((float) this.frameBuffer.getWidth())), c.getHeight()), this.imgPaint);
                }
            }
            if (this.error) {
                c.drawRect(0.0f, 0.0f, (float) c.getWidth(), (float) c.getHeight(), this.errorBgPaint);
                String error = getResources().getString(C0436R.string.error);
                c.drawText(error, ((float) (c.getWidth() / 2)) - (this.errorPaint.measureText(error) / ImageViewHolder.PaddingSize), ((float) (c.getHeight() / 2)) - ((Math.abs(this.errorPaint.ascent()) + this.errorPaint.descent()) / ImageViewHolder.PaddingSize), this.errorPaint);
                return;
            }
            return;
        }
        error = getResources().getString(C0436R.string.loading);
        c.drawText(error, ((float) (c.getWidth() / 2)) - (this.errorPaint.measureText(error) / ImageViewHolder.PaddingSize), ((float) (c.getHeight() / 2)) - ((Math.abs(this.errorPaint.ascent()) + this.errorPaint.descent()) / ImageViewHolder.PaddingSize), this.errorPaint);
    }
}
