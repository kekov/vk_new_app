package com.vkontakte.android.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import com.vkontakte.android.Global;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;

public class FwdMessageLevelView extends View {
    private int level;
    private Paint paint;

    public FwdMessageLevelView(Context context) {
        super(context);
        this.level = 1;
        init();
    }

    public FwdMessageLevelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.level = 1;
        init();
    }

    public FwdMessageLevelView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.level = 1;
        init();
    }

    public void init() {
        this.paint = new Paint();
        this.paint.setColor(-2136100685);
    }

    public void onMeasure(int wms, int hms) {
        setMeasuredDimension(this.level * Global.scale(6.0f), MeasureSpec.getSize(hms));
    }

    public void setLevel(int l) {
        this.level = l;
    }

    public void onDraw(Canvas c) {
        for (int i = 0; i < this.level; i++) {
            c.drawRect((float) (Global.scale(6.0f) * i), 0.0f, (float) ((Global.scale(6.0f) * i) + Global.scale(ImageViewHolder.PaddingSize)), (float) c.getHeight(), this.paint);
        }
    }
}
