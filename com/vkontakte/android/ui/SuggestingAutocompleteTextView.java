package com.vkontakte.android.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

public class SuggestingAutocompleteTextView extends AutoCompleteTextView {

    /* renamed from: com.vkontakte.android.ui.SuggestingAutocompleteTextView.1 */
    class C10581 implements Runnable {
        C10581() {
        }

        public void run() {
            super.showDropDown();
        }
    }

    public SuggestingAutocompleteTextView(Context context) {
        super(context);
    }

    public SuggestingAutocompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SuggestingAutocompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public boolean enoughToFilter() {
        return true;
    }

    public void showDropDown() {
        postDelayed(new C10581(), 200);
    }
}
