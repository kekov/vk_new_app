package com.vkontakte.android.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.ExploreByTouchHelper;
import android.text.Html;
import android.text.TextUtils.TruncateAt;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.actionbarsherlock.widget.SearchView;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.DialogEntry;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.api.MessagesSearchDialogs;
import com.vkontakte.android.api.MessagesSearchDialogs.Callback;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.fragments.DialogsFragment.SelectionListener;
import java.util.ArrayList;
import java.util.Iterator;

public class MessagesSearchSuggestionsPopup {
    private SuggestionsAdapter adapter;
    private Context f59c;
    private TextView emptyView;
    private TextView footerView;
    private ListImageLoaderWrapper imgLoader;
    private ListView list;
    private Runnable pendingSearch;
    private ProgressBar progress;
    private APIRequest req;
    private SearchView searchView;
    private SelectionListener selListener;
    private ArrayList<UserProfile> users;
    private FrameLayout wrap;

    /* renamed from: com.vkontakte.android.ui.MessagesSearchSuggestionsPopup.1 */
    class C10141 extends TextView {
        C10141(Context $anonymous0) {
            super($anonymous0);
        }

        public void onMeasure(int wms, int hms) {
            super.onMeasure(MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(wms), 1073741824), hms);
        }
    }

    /* renamed from: com.vkontakte.android.ui.MessagesSearchSuggestionsPopup.2 */
    class C10152 implements OnItemClickListener {
        C10152() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            MessagesSearchSuggestionsPopup.this.wrap.setVisibility(8);
            if (pos < MessagesSearchSuggestionsPopup.this.users.size()) {
                UserProfile p = (UserProfile) MessagesSearchSuggestionsPopup.this.users.get(pos);
                if (MessagesSearchSuggestionsPopup.this.selListener != null) {
                    DialogEntry e = new DialogEntry();
                    e.profile = p;
                    MessagesSearchSuggestionsPopup.this.selListener.onItemSelected(e);
                    return;
                }
                Bundle args = new Bundle();
                args.putInt("id", p.uid);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, p.fullName);
                args.putCharSequence("photo", p.photo);
                Navigate.to("ChatFragment", args, (Activity) MessagesSearchSuggestionsPopup.this.f59c);
                MessagesSearchSuggestionsPopup.this.searchView.setIconified(true);
                MessagesSearchSuggestionsPopup.this.searchView.setIconified(true);
                return;
            }
            MessagesSearchSuggestionsPopup.this.searchView.setQuery(MessagesSearchSuggestionsPopup.this.searchView.getQuery(), true);
        }
    }

    /* renamed from: com.vkontakte.android.ui.MessagesSearchSuggestionsPopup.3 */
    class C10163 implements Runnable {
        private final /* synthetic */ String val$q;

        C10163(String str) {
            this.val$q = str;
        }

        public void run() {
            MessagesSearchSuggestionsPopup.this.searchOnServer(this.val$q);
        }
    }

    private class DropDownListView extends ListView {
        private boolean mListSelectionHidden;

        public DropDownListView(Context context) {
            super(context);
        }

        public boolean isInTouchMode() {
            return this.mListSelectionHidden || super.isInTouchMode();
        }

        public boolean hasWindowFocus() {
            return true;
        }

        public boolean isFocused() {
            return true;
        }

        public boolean hasFocus() {
            return true;
        }
    }

    private class SuggestionsAdapter extends BaseAdapter {
        private SuggestionsAdapter() {
        }

        public int getCount() {
            return MessagesSearchSuggestionsPopup.this.users.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v;
            if (convertView != null) {
                v = convertView;
            } else {
                v = View.inflate(MessagesSearchSuggestionsPopup.this.f59c, C0436R.layout.messages_search_suggestion, null);
                v.setLayoutParams(new LayoutParams(-1, -2));
            }
            UserProfile p = (UserProfile) MessagesSearchSuggestionsPopup.this.users.get(position);
            ((TextView) v.findViewById(C0436R.id.msg_suggestion_text)).setText(p.fullName);
            if (p.uid > 2000000000) {
                ((ImageView) v.findViewById(C0436R.id.msg_suggestion_photo)).setImageResource(C0436R.drawable.ic_chat_multi);
            } else if (MessagesSearchSuggestionsPopup.this.imgLoader.isAlreadyLoaded(p.photo)) {
                ((ImageView) v.findViewById(C0436R.id.msg_suggestion_photo)).setImageBitmap(MessagesSearchSuggestionsPopup.this.imgLoader.get(p.photo));
            } else {
                ((ImageView) v.findViewById(C0436R.id.msg_suggestion_photo)).setImageResource(C0436R.drawable.user_placeholder);
            }
            return v;
        }
    }

    /* renamed from: com.vkontakte.android.ui.MessagesSearchSuggestionsPopup.4 */
    class C15544 implements Callback {
        C15544() {
        }

        public void success(ArrayList<UserProfile> res) {
            int i = 8;
            MessagesSearchSuggestionsPopup.this.req = null;
            Iterator it = MessagesSearchSuggestionsPopup.this.users.iterator();
            while (it.hasNext()) {
                UserProfile u = (UserProfile) it.next();
                Iterator it2 = res.iterator();
                while (it2.hasNext()) {
                    UserProfile u2 = (UserProfile) it2.next();
                    if (u2.uid == u.uid) {
                        res.remove(u2);
                        break;
                    }
                }
            }
            MessagesSearchSuggestionsPopup.this.users.addAll(res);
            MessagesSearchSuggestionsPopup.this.updateList();
            MessagesSearchSuggestionsPopup.this.progress.setVisibility(8);
            TextView access$11 = MessagesSearchSuggestionsPopup.this.emptyView;
            if (MessagesSearchSuggestionsPopup.this.list.getCount() <= 0) {
                i = 0;
            }
            access$11.setVisibility(i);
        }

        public void fail(int ecode, String emsg) {
            int i = 8;
            MessagesSearchSuggestionsPopup.this.req = null;
            MessagesSearchSuggestionsPopup.this.progress.setVisibility(8);
            TextView access$11 = MessagesSearchSuggestionsPopup.this.emptyView;
            if (MessagesSearchSuggestionsPopup.this.list.getCount() <= 0) {
                i = 0;
            }
            access$11.setVisibility(i);
        }
    }

    private class SearchPhotosAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.ui.MessagesSearchSuggestionsPopup.SearchPhotosAdapter.1 */
        class C10171 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C10171(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.msg_suggestion_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private SearchPhotosAdapter() {
        }

        public int getItemCount() {
            return MessagesSearchSuggestionsPopup.this.users.size();
        }

        public int getImageCountForItem(int item) {
            return ((UserProfile) MessagesSearchSuggestionsPopup.this.users.get(item)).uid < 2000000000 ? 1 : 0;
        }

        public String getImageURL(int item, int image) {
            return ((UserProfile) MessagesSearchSuggestionsPopup.this.users.get(item)).photo;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += MessagesSearchSuggestionsPopup.this.list.getHeaderViewsCount();
            if (item >= MessagesSearchSuggestionsPopup.this.list.getFirstVisiblePosition() && item <= MessagesSearchSuggestionsPopup.this.list.getLastVisiblePosition()) {
                ((Activity) MessagesSearchSuggestionsPopup.this.f59c).runOnUiThread(new C10171(MessagesSearchSuggestionsPopup.this.list.getChildAt(item - MessagesSearchSuggestionsPopup.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    public MessagesSearchSuggestionsPopup(SearchView s, Context context, boolean allowMsgs) {
        this.users = new ArrayList();
        this.searchView = s;
        this.f59c = context;
        this.list = new DropDownListView(context);
        this.footerView = new C10141(context);
        this.footerView.setLayoutParams(new LayoutParams(-1, Global.scale(42.0f)));
        this.footerView.setGravity(17);
        this.footerView.setTextSize(18.0f);
        this.footerView.setSingleLine();
        this.footerView.setEllipsize(TruncateAt.MIDDLE);
        this.footerView.setPadding(Global.scale(12.0f), 0, Global.scale(12.0f), 0);
        this.footerView.setTextColor(ExploreByTouchHelper.INVALID_ID);
        if (allowMsgs) {
            this.list.addFooterView(this.footerView);
        }
        ListView listView = this.list;
        ListAdapter suggestionsAdapter = new SuggestionsAdapter();
        this.adapter = suggestionsAdapter;
        listView.setAdapter(suggestionsAdapter);
        this.list.setSelector(C0436R.drawable.highlight);
        this.list.setDivider(new ColorDrawable(-1710619));
        this.list.setDividerHeight(Global.scale(1.0f));
        this.list.setBackgroundColor(-1);
        int pad = this.list.getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding);
        this.list.setPadding(pad, 0, pad, 0);
        this.list.setOnItemClickListener(new C10152());
        this.list.onWindowFocusChanged(true);
        this.imgLoader = new ListImageLoaderWrapper(new SearchPhotosAdapter(), this.list, null);
        this.emptyView = new TextView(this.f59c);
        this.emptyView.setTextAppearance(this.f59c, C0436R.style.EmptyTextView);
        this.emptyView.setGravity(17);
        this.emptyView.setText(C0436R.string.nothing_found);
        this.progress = new ProgressBar(this.f59c);
        this.progress.setVisibility(8);
        this.wrap = new FrameLayout(this.f59c);
        this.wrap.addView(this.list);
        this.wrap.addView(this.emptyView, new FrameLayout.LayoutParams(-2, -2, 17));
        this.wrap.addView(this.progress, new FrameLayout.LayoutParams(Global.scale(40.0f), Global.scale(40.0f), 17));
        this.wrap.setVisibility(8);
        this.emptyView.setVisibility(8);
    }

    public void setSelectionListener(SelectionListener sl) {
        this.selListener = sl;
    }

    public View getView() {
        return this.wrap;
    }

    public void updateQuery(String q) {
        int i = 8;
        Log.m528i("vk", "update query " + q);
        this.emptyView.setVisibility(8);
        this.progress.setVisibility(8);
        if (q.length() > 0) {
            this.footerView.setText(Html.fromHtml(new StringBuilder(String.valueOf(this.f59c.getResources().getString(C0436R.string.search_in_msgs1))).append(" <b>").append(q.replace("<", "&lt;")).append("</b> ").append(this.f59c.getResources().getString(C0436R.string.search_in_msgs2)).toString()));
            this.users.clear();
            this.users.addAll(Friends.search(q));
            updateList();
            if (this.req != null) {
                this.req.cancel();
                this.req = null;
            }
            if (this.pendingSearch != null) {
                this.list.removeCallbacks(this.pendingSearch);
            }
            this.pendingSearch = new C10163(q);
            ProgressBar progressBar = this.progress;
            if (this.list.getCount() <= 0) {
                i = 0;
            }
            progressBar.setVisibility(i);
            this.list.postDelayed(this.pendingSearch, 1000);
            if (this.wrap.getVisibility() != 0) {
                this.wrap.setVisibility(0);
                return;
            }
            return;
        }
        this.wrap.setVisibility(8);
    }

    private void searchOnServer(String q) {
        this.req = new MessagesSearchDialogs(q).setCallback(new C15544());
        this.req.exec(this.list);
        this.pendingSearch = null;
    }

    private void updateList() {
        this.adapter.notifyDataSetChanged();
        this.imgLoader.updateImages();
    }

    public boolean onKeyPreIme(int code, KeyEvent event) {
        if (this.list.getVisibility() != 0 || code != 4) {
            return false;
        }
        if (event.getAction() != 1) {
            return true;
        }
        this.list.setVisibility(8);
        return true;
    }

    public void onPause() {
        this.imgLoader.deactivate();
    }

    public void onResume() {
        this.imgLoader.activate();
    }
}
