package com.vkontakte.android.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vkontakte.android.C0436R;

public class ErrorView extends LinearLayout {
    public ErrorView(Context context) {
        super(context);
        init();
    }

    public ErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ErrorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
    }

    public void onFinishInflate() {
        ((TextView) findViewById(C0436R.id.error_text)).setTypeface(Fonts.getRobotoLight());
    }

    public void setOnRetryListener(OnClickListener l) {
        findViewById(C0436R.id.error_button).setOnClickListener(l);
    }

    public void setErrorInfo(int code, String msg) {
    }

    public void setMessage(String msg) {
        ((TextView) findViewById(C0436R.id.error_text)).setText(msg);
    }

    public void setIsInline(boolean inl) {
        findViewById(C0436R.id.error_icon).setVisibility(inl ? 8 : 4);
    }
}
