package com.vkontakte.android.ui;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageCache;
import com.vkontakte.android.ImageCache.RequestWrapper;
import com.vkontakte.android.Log;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

public class ListImageLoader {
    private static final boolean DEBUG = true;
    private static int numThreads;
    private ListImageLoaderAdapter adapter;
    public boolean allowRepeat;
    private ConcurrentHashMap<String, Bitmap> images;
    public boolean[] needCancelLoad;
    private LinkedList<Task> queue;
    private boolean removed;
    public boolean[] running;
    private RequestWrapper[] wrappers;

    /* renamed from: com.vkontakte.android.ui.ListImageLoader.1 */
    class C10011 implements Runnable {
        private final /* synthetic */ int val$firstVisibleItem;
        private final /* synthetic */ int val$numVisibleItems;
        private final /* synthetic */ boolean val$scrollFwd;

        C10011(int i, int i2, boolean z) {
            this.val$firstVisibleItem = i;
            this.val$numVisibleItems = i2;
            this.val$scrollFwd = z;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r23 = this;
            r20 = 0;
            r0 = r23;
            r0 = r0.val$firstVisibleItem;	 Catch:{ Exception -> 0x031e }
            r21 = r0;
            r0 = r23;
            r0 = r0.val$numVisibleItems;	 Catch:{ Exception -> 0x031e }
            r22 = r0;
            r21 = r21 - r22;
            r15 = java.lang.Math.max(r20, r21);	 Catch:{ Exception -> 0x031e }
            r3 = 0;
            r5 = 0;
        L_0x0016:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r0 = r20;
            r0 = r0.running;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r0 = r20;
            r0 = r0.length;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r0 = r20;
            if (r5 < r0) goto L_0x019d;
        L_0x002b:
            if (r3 != 0) goto L_0x01b6;
        L_0x002d:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r20 = r20.adapter;	 Catch:{ Exception -> 0x031e }
            r20 = r20.getItemCount();	 Catch:{ Exception -> 0x031e }
            r0 = r23;
            r0 = r0.val$firstVisibleItem;	 Catch:{ Exception -> 0x031e }
            r21 = r0;
            r0 = r23;
            r0 = r0.val$numVisibleItems;	 Catch:{ Exception -> 0x031e }
            r22 = r0;
            r22 = r22 * 2;
            r21 = r21 + r22;
            r4 = java.lang.Math.min(r20, r21);	 Catch:{ Exception -> 0x031e }
            r20 = "vk_img_loader";
            r21 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x031e }
            r22 = "LOAD R ";
            r21.<init>(r22);	 Catch:{ Exception -> 0x031e }
            r0 = r21;
            r21 = r0.append(r15);	 Catch:{ Exception -> 0x031e }
            r22 = "; ";
            r21 = r21.append(r22);	 Catch:{ Exception -> 0x031e }
            r0 = r21;
            r21 = r0.append(r4);	 Catch:{ Exception -> 0x031e }
            r21 = r21.toString();	 Catch:{ Exception -> 0x031e }
            com.vkontakte.android.Log.m528i(r20, r21);	 Catch:{ Exception -> 0x031e }
            r0 = r23;
            r0 = r0.val$firstVisibleItem;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r0 = r20;
            r11 = java.lang.Math.max(r15, r0);	 Catch:{ Exception -> 0x0312 }
            r0 = r23;
            r0 = r0.val$firstVisibleItem;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r0 = r23;
            r0 = r0.val$numVisibleItems;	 Catch:{ Exception -> 0x0312 }
            r21 = r0;
            r20 = r20 + r21;
            r0 = r20;
            r10 = java.lang.Math.min(r0, r4);	 Catch:{ Exception -> 0x0312 }
            r20 = "vk_img_loader";
            r21 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0312 }
            r22 = "Load visible ";
            r21.<init>(r22);	 Catch:{ Exception -> 0x0312 }
            r0 = r21;
            r21 = r0.append(r11);	 Catch:{ Exception -> 0x0312 }
            r22 = "; ";
            r21 = r21.append(r22);	 Catch:{ Exception -> 0x0312 }
            r0 = r21;
            r21 = r0.append(r10);	 Catch:{ Exception -> 0x0312 }
            r21 = r21.toString();	 Catch:{ Exception -> 0x0312 }
            com.vkontakte.android.Log.m528i(r20, r21);	 Catch:{ Exception -> 0x0312 }
            r5 = r11;
        L_0x00b4:
            if (r5 < r10) goto L_0x01ea;
        L_0x00b6:
            r0 = r23;
            r0 = r0.val$scrollFwd;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            if (r20 == 0) goto L_0x0248;
        L_0x00be:
            r0 = r23;
            r0 = r0.val$firstVisibleItem;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r0 = r23;
            r0 = r0.val$numVisibleItems;	 Catch:{ Exception -> 0x0312 }
            r21 = r0;
            r11 = r20 + r21;
            r10 = r4;
        L_0x00cd:
            if (r10 <= r11) goto L_0x00f4;
        L_0x00cf:
            r20 = "vk_img_loader";
            r21 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0312 }
            r22 = "Load scroll dir ";
            r21.<init>(r22);	 Catch:{ Exception -> 0x0312 }
            r0 = r21;
            r21 = r0.append(r11);	 Catch:{ Exception -> 0x0312 }
            r22 = "; ";
            r21 = r21.append(r22);	 Catch:{ Exception -> 0x0312 }
            r0 = r21;
            r21 = r0.append(r10);	 Catch:{ Exception -> 0x0312 }
            r21 = r21.toString();	 Catch:{ Exception -> 0x0312 }
            com.vkontakte.android.Log.m528i(r20, r21);	 Catch:{ Exception -> 0x0312 }
            r5 = r11;
        L_0x00f2:
            if (r5 < r10) goto L_0x024f;
        L_0x00f4:
            r0 = r23;
            r0 = r0.val$scrollFwd;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            if (r20 != 0) goto L_0x02ad;
        L_0x00fc:
            r0 = r23;
            r0 = r0.val$firstVisibleItem;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r0 = r23;
            r0 = r0.val$numVisibleItems;	 Catch:{ Exception -> 0x0312 }
            r21 = r0;
            r11 = r20 + r21;
            r10 = r4;
        L_0x010b:
            if (r10 <= r11) goto L_0x0132;
        L_0x010d:
            r20 = "vk_img_loader";
            r21 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0312 }
            r22 = "Load opposite dir ";
            r21.<init>(r22);	 Catch:{ Exception -> 0x0312 }
            r0 = r21;
            r21 = r0.append(r11);	 Catch:{ Exception -> 0x0312 }
            r22 = "; ";
            r21 = r21.append(r22);	 Catch:{ Exception -> 0x0312 }
            r0 = r21;
            r21 = r0.append(r10);	 Catch:{ Exception -> 0x0312 }
            r21 = r21.toString();	 Catch:{ Exception -> 0x0312 }
            com.vkontakte.android.Log.m528i(r20, r21);	 Catch:{ Exception -> 0x0312 }
            r5 = r11;
        L_0x0130:
            if (r5 < r10) goto L_0x02b4;
        L_0x0132:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r21 = r20.images;	 Catch:{ Exception -> 0x031e }
            monitor-enter(r21);	 Catch:{ Exception -> 0x031e }
            r5 = 0;
        L_0x013e:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ all -> 0x037b }
            r20 = r0;
            r0 = r20;
            r0 = r0.needCancelLoad;	 Catch:{ all -> 0x037b }
            r20 = r0;
            r0 = r20;
            r0 = r0.length;	 Catch:{ all -> 0x037b }
            r20 = r0;
            r0 = r20;
            if (r5 < r0) goto L_0x0321;
        L_0x0153:
            r8 = new java.util.Vector;	 Catch:{ all -> 0x037b }
            r8.<init>();	 Catch:{ all -> 0x037b }
            r13 = new java.util.Vector;	 Catch:{ all -> 0x037b }
            r13.<init>();	 Catch:{ all -> 0x037b }
            r5 = r15;
        L_0x015e:
            if (r5 < r4) goto L_0x0335;
        L_0x0160:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ all -> 0x037b }
            r20 = r0;
            r20 = r20.images;	 Catch:{ all -> 0x037b }
            r9 = r20.keySet();	 Catch:{ all -> 0x037b }
            r20 = r9.iterator();	 Catch:{ all -> 0x037b }
        L_0x0172:
            r22 = r20.hasNext();	 Catch:{ all -> 0x037b }
            if (r22 != 0) goto L_0x037e;
        L_0x0178:
            r20 = r13.iterator();	 Catch:{ all -> 0x037b }
        L_0x017c:
            r22 = r20.hasNext();	 Catch:{ all -> 0x037b }
            if (r22 != 0) goto L_0x038f;
        L_0x0182:
            monitor-exit(r21);	 Catch:{ all -> 0x037b }
            r20 = com.vkontakte.android.ui.ListImageLoader.numThreads;	 Catch:{ Exception -> 0x031e }
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x031e }
            r21 = r0;
            r21 = r21.queue;	 Catch:{ Exception -> 0x031e }
            r21 = r21.size();	 Catch:{ Exception -> 0x031e }
            r12 = java.lang.Math.min(r20, r21);	 Catch:{ Exception -> 0x031e }
            r5 = 0;
        L_0x019a:
            if (r5 < r12) goto L_0x03a6;
        L_0x019c:
            return;
        L_0x019d:
            if (r3 != 0) goto L_0x01b4;
        L_0x019f:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r0 = r20;
            r0 = r0.running;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r20 = r20[r5];	 Catch:{ Exception -> 0x031e }
            if (r20 != 0) goto L_0x01b4;
        L_0x01af:
            r3 = 0;
        L_0x01b0:
            r5 = r5 + 1;
            goto L_0x0016;
        L_0x01b4:
            r3 = 1;
            goto L_0x01b0;
        L_0x01b6:
            r20 = 10;
            java.lang.Thread.sleep(r20);	 Catch:{ Exception -> 0x03d6 }
        L_0x01bb:
            r3 = 0;
            r5 = 0;
        L_0x01bd:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r0 = r20;
            r0 = r0.running;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r0 = r20;
            r0 = r0.length;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r0 = r20;
            if (r5 >= r0) goto L_0x002b;
        L_0x01d2:
            if (r3 != 0) goto L_0x01e8;
        L_0x01d4:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r0 = r20;
            r0 = r0.running;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r20 = r20[r5];	 Catch:{ Exception -> 0x031e }
            if (r20 != 0) goto L_0x01e8;
        L_0x01e4:
            r3 = 0;
        L_0x01e5:
            r5 = r5 + 1;
            goto L_0x01bd;
        L_0x01e8:
            r3 = 1;
            goto L_0x01e5;
        L_0x01ea:
            r7 = 0;
        L_0x01eb:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r20 = r20.adapter;	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            r20 = r0.getImageCountForItem(r5);	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            if (r7 < r0) goto L_0x0203;
        L_0x01ff:
            r5 = r5 + 1;
            goto L_0x00b4;
        L_0x0203:
            r17 = new com.vkontakte.android.ui.ListImageLoader$Task;	 Catch:{ Exception -> 0x0312 }
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r21 = 0;
            r0 = r17;
            r1 = r20;
            r2 = r21;
            r0.<init>(r2);	 Catch:{ Exception -> 0x0312 }
            r0 = r17;
            r0.item = r5;	 Catch:{ Exception -> 0x0312 }
            r0 = r17;
            r0.image = r7;	 Catch:{ Exception -> 0x0312 }
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r20 = r20.adapter;	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            r20 = r0.getImageURL(r5, r7);	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            r1 = r17;
            r1.url = r0;	 Catch:{ Exception -> 0x0312 }
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r20 = r20.queue;	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            r1 = r17;
            r0.offer(r1);	 Catch:{ Exception -> 0x0312 }
            r7 = r7 + 1;
            goto L_0x01eb;
        L_0x0248:
            r11 = r15;
            r0 = r23;
            r10 = r0.val$firstVisibleItem;	 Catch:{ Exception -> 0x0312 }
            goto L_0x00cd;
        L_0x024f:
            r7 = 0;
        L_0x0250:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r20 = r20.adapter;	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            r20 = r0.getImageCountForItem(r5);	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            if (r7 < r0) goto L_0x0268;
        L_0x0264:
            r5 = r5 + 1;
            goto L_0x00f2;
        L_0x0268:
            r17 = new com.vkontakte.android.ui.ListImageLoader$Task;	 Catch:{ Exception -> 0x0312 }
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r21 = 0;
            r0 = r17;
            r1 = r20;
            r2 = r21;
            r0.<init>(r2);	 Catch:{ Exception -> 0x0312 }
            r0 = r17;
            r0.item = r5;	 Catch:{ Exception -> 0x0312 }
            r0 = r17;
            r0.image = r7;	 Catch:{ Exception -> 0x0312 }
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r20 = r20.adapter;	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            r20 = r0.getImageURL(r5, r7);	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            r1 = r17;
            r1.url = r0;	 Catch:{ Exception -> 0x0312 }
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r20 = r20.queue;	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            r1 = r17;
            r0.offer(r1);	 Catch:{ Exception -> 0x0312 }
            r7 = r7 + 1;
            goto L_0x0250;
        L_0x02ad:
            r11 = r15;
            r0 = r23;
            r10 = r0.val$firstVisibleItem;	 Catch:{ Exception -> 0x0312 }
            goto L_0x010b;
        L_0x02b4:
            r7 = 0;
        L_0x02b5:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r20 = r20.adapter;	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            r20 = r0.getImageCountForItem(r5);	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            if (r7 < r0) goto L_0x02cd;
        L_0x02c9:
            r5 = r5 + 1;
            goto L_0x0130;
        L_0x02cd:
            r17 = new com.vkontakte.android.ui.ListImageLoader$Task;	 Catch:{ Exception -> 0x0312 }
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r21 = 0;
            r0 = r17;
            r1 = r20;
            r2 = r21;
            r0.<init>(r2);	 Catch:{ Exception -> 0x0312 }
            r0 = r17;
            r0.item = r5;	 Catch:{ Exception -> 0x0312 }
            r0 = r17;
            r0.image = r7;	 Catch:{ Exception -> 0x0312 }
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r20 = r20.adapter;	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            r20 = r0.getImageURL(r5, r7);	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            r1 = r17;
            r1.url = r0;	 Catch:{ Exception -> 0x0312 }
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x0312 }
            r20 = r0;
            r20 = r20.queue;	 Catch:{ Exception -> 0x0312 }
            r0 = r20;
            r1 = r17;
            r0.offer(r1);	 Catch:{ Exception -> 0x0312 }
            r7 = r7 + 1;
            goto L_0x02b5;
        L_0x0312:
            r19 = move-exception;
            r20 = "vk_img_loader";
            r0 = r20;
            r1 = r19;
            com.vkontakte.android.Log.m532w(r0, r1);	 Catch:{ Exception -> 0x031e }
            goto L_0x019c;
        L_0x031e:
            r20 = move-exception;
            goto L_0x019c;
        L_0x0321:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ all -> 0x037b }
            r20 = r0;
            r0 = r20;
            r0 = r0.needCancelLoad;	 Catch:{ all -> 0x037b }
            r20 = r0;
            r22 = 0;
            r20[r5] = r22;	 Catch:{ all -> 0x037b }
            r5 = r5 + 1;
            goto L_0x013e;
        L_0x0335:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ all -> 0x037b }
            r20 = r0;
            r20 = r20.adapter;	 Catch:{ all -> 0x037b }
            r0 = r20;
            r6 = r0.getImageCountForItem(r5);	 Catch:{ all -> 0x037b }
            r7 = 0;
        L_0x0346:
            if (r7 < r6) goto L_0x034c;
        L_0x0348:
            r5 = r5 + 1;
            goto L_0x015e;
        L_0x034c:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ all -> 0x037b }
            r20 = r0;
            r20 = r20.adapter;	 Catch:{ all -> 0x037b }
            r0 = r20;
            r18 = r0.getImageURL(r5, r7);	 Catch:{ all -> 0x037b }
            if (r18 != 0) goto L_0x0361;
        L_0x035e:
            r7 = r7 + 1;
            goto L_0x0346;
        L_0x0361:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ all -> 0x037b }
            r20 = r0;
            r20 = r20.images;	 Catch:{ all -> 0x037b }
            r0 = r20;
            r1 = r18;
            r20 = r0.containsKey(r1);	 Catch:{ all -> 0x037b }
            if (r20 == 0) goto L_0x035e;
        L_0x0375:
            r0 = r18;
            r8.add(r0);	 Catch:{ all -> 0x037b }
            goto L_0x035e;
        L_0x037b:
            r20 = move-exception;
            monitor-exit(r21);	 Catch:{ all -> 0x037b }
            throw r20;	 Catch:{ Exception -> 0x031e }
        L_0x037e:
            r14 = r20.next();	 Catch:{ all -> 0x037b }
            r14 = (java.lang.String) r14;	 Catch:{ all -> 0x037b }
            r22 = r8.contains(r14);	 Catch:{ all -> 0x037b }
            if (r22 != 0) goto L_0x0172;
        L_0x038a:
            r13.add(r14);	 Catch:{ all -> 0x037b }
            goto L_0x0172;
        L_0x038f:
            r14 = r20.next();	 Catch:{ all -> 0x037b }
            r14 = (java.lang.String) r14;	 Catch:{ all -> 0x037b }
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ all -> 0x037b }
            r22 = r0;
            r22 = r22.images;	 Catch:{ all -> 0x037b }
            r0 = r22;
            r0.remove(r14);	 Catch:{ all -> 0x037b }
            goto L_0x017c;
        L_0x03a6:
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x031e }
            r20 = r0;
            r20 = r20.wrappers;	 Catch:{ Exception -> 0x031e }
            r21 = new com.vkontakte.android.ImageCache$RequestWrapper;	 Catch:{ Exception -> 0x031e }
            r21.<init>();	 Catch:{ Exception -> 0x031e }
            r20[r5] = r21;	 Catch:{ Exception -> 0x031e }
            r16 = new java.lang.Thread;	 Catch:{ Exception -> 0x031e }
            r20 = new com.vkontakte.android.ui.ListImageLoader$TaskRunner;	 Catch:{ Exception -> 0x031e }
            r0 = r23;
            r0 = com.vkontakte.android.ui.ListImageLoader.this;	 Catch:{ Exception -> 0x031e }
            r21 = r0;
            r0 = r20;
            r1 = r21;
            r0.<init>(r5);	 Catch:{ Exception -> 0x031e }
            r0 = r16;
            r1 = r20;
            r0.<init>(r1);	 Catch:{ Exception -> 0x031e }
            r16.start();	 Catch:{ Exception -> 0x031e }
            r5 = r5 + 1;
            goto L_0x019a;
        L_0x03d6:
            r20 = move-exception;
            goto L_0x01bb;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.ui.ListImageLoader.1.run():void");
        }
    }

    private class Task {
        int image;
        int item;
        String url;

        private Task() {
        }
    }

    private class TaskRunner implements Runnable {
        int index;

        public TaskRunner(int i) {
            this.index = 0;
            this.index = i;
        }

        public void run() {
            Log.m525d("vk_img_loader", "Started img loader thread #" + this.index);
            ListImageLoader.this.running[this.index] = ListImageLoader.DEBUG;
            while (!ListImageLoader.this.needCancelLoad[this.index]) {
                Task task = (Task) ListImageLoader.this.queue.poll();
                if (task == null) {
                    break;
                }
                String url = task.url;
                Log.m525d("vk_img_loader", "thread " + this.index + ": loading " + url);
                if (url != null) {
                    if (ListImageLoader.this.images.containsKey(url)) {
                        ListImageLoader.this.adapter.imageLoaded(task.item, task.image, (Bitmap) ListImageLoader.this.images.get(url));
                        Log.m525d("vk_img_loader", "thread " + this.index + ": returned from ram" + ListImageLoader.this.images.get(url) + " " + url);
                    } else if (url.startsWith("M")) {
                        if (url.length() < 4) {
                            bmp = ((BitmapDrawable) VKApplication.context.getResources().getDrawable(C0436R.drawable.ic_chat_multi)).getBitmap();
                        } else {
                            String[] mdUrls = url.split("\\|");
                            Bitmap[] bmps = new Bitmap[(mdUrls.length - 1)];
                            for (int k = 1; k < mdUrls.length; k++) {
                                Log.m525d("vk_img_loader", "M_URL[" + k + "]=" + mdUrls[k]);
                                bmps[k - 1] = ImageCache.get(mdUrls[k], ListImageLoader.this.wrappers[this.index], null, ListImageLoader.DEBUG);
                            }
                            bmp = ListImageLoader.drawMultichatPhoto(bmps);
                            ImageCache.put(url, bmp);
                            if (bmp == null || bmps.length < 2) {
                                bmp = ((BitmapDrawable) VKApplication.context.getResources().getDrawable(C0436R.drawable.ic_chat_multi)).getBitmap();
                            }
                        }
                        if (bmp != null) {
                            ListImageLoader.this.images.put(url, bmp);
                            ListImageLoader.this.adapter.imageLoaded(task.item, task.image, bmp);
                        }
                    } else {
                        bmp = ImageCache.get(url, ListImageLoader.this.wrappers[this.index], null, ListImageLoader.DEBUG);
                        if (ListImageLoader.this.needCancelLoad[this.index]) {
                            ListImageLoader.this.needCancelLoad[this.index] = false;
                            ListImageLoader.this.running[this.index] = false;
                            Log.m525d("vk_img_loader", "Img loader thread #" + this.index + " canceled");
                            return;
                        }
                        if (!(url == null || bmp == null)) {
                            try {
                                ListImageLoader.this.images.put(url, bmp);
                            } catch (Exception e) {
                            }
                        }
                        try {
                            Log.m529v("vk_img_loader", "thread " + this.index + ": loaded " + url);
                            if (bmp != null) {
                                ListImageLoader.this.adapter.imageLoaded(task.item, task.image, bmp);
                            }
                        } catch (Exception x) {
                            Log.m527e("vk_img_loader", "Error on img loader thread #" + this.index, x);
                        }
                    }
                }
            }
            ListImageLoader.this.running[this.index] = false;
            Log.m525d("vk_img_loader", "Img loader thread #" + this.index + " exited");
        }
    }

    static {
        numThreads = Math.min(Runtime.getRuntime().availableProcessors() * 4, 8);
    }

    public ListImageLoader() {
        this.adapter = null;
        this.images = new ConcurrentHashMap();
        this.wrappers = new RequestWrapper[numThreads];
        this.removed = false;
        this.allowRepeat = DEBUG;
        this.needCancelLoad = new boolean[numThreads];
        this.running = new boolean[numThreads];
        this.queue = new LinkedList();
    }

    public void setAdapter(ListImageLoaderAdapter a) {
        this.adapter = a;
    }

    public ListImageLoaderAdapter getAdapter() {
        return this.adapter;
    }

    public void load(int firstVisibleItem, int numVisibleItems, boolean scrollFwd) {
        if (this.removed || this.adapter == null) {
            Log.m530w("vk_img_loader", "removed || adapter=null");
            return;
        }
        this.queue.clear();
        new Thread(new C10011(firstVisibleItem, numVisibleItems, scrollFwd)).start();
    }

    public void cancel() {
        for (int i = 0; i < this.needCancelLoad.length; i++) {
            this.needCancelLoad[i] = DEBUG;
        }
        for (RequestWrapper wrapper : this.wrappers) {
            if (!(wrapper == null || wrapper.request == null)) {
                wrapper.request.abort();
            }
        }
    }

    public void clear() {
        this.removed = DEBUG;
        cancel();
        this.images.clear();
    }

    public void activate() {
        this.removed = false;
    }

    public boolean isAlreadyLoaded(String url) {
        if (url == null || this.images == null) {
            return false;
        }
        if (this.images.containsKey(url) || ImageCache.isInTopCache(url)) {
            return DEBUG;
        }
        return false;
    }

    public Bitmap getImage(String url) {
        if (this.images.contains(url)) {
            return (Bitmap) this.images.get(url);
        }
        if (ImageCache.isInTopCache(url)) {
            return ImageCache.getFromTop(url);
        }
        return null;
    }

    public static Bitmap drawMultichatPhoto(Bitmap[] bmps) {
        int bs = (int) (50.0f * Global.displayDensity);
        Bitmap bmp = Bitmap.createBitmap(bs, bs, Config.ARGB_8888);
        Canvas c = new Canvas(bmp);
        Paint paint = new Paint();
        paint.setAntiAlias(DEBUG);
        paint.setFilterBitmap(DEBUG);
        if (bmps.length == 2) {
            c.drawBitmap(bmps[0], new Rect(bmps[0].getWidth() / 4, 0, (bmps[0].getWidth() / 4) * 3, bmps[0].getHeight()), new Rect(0, 0, bs / 2, bs), paint);
            c.drawBitmap(bmps[1], new Rect(bmps[1].getWidth() / 4, 0, (bmps[1].getWidth() / 4) * 3, bmps[1].getHeight()), new Rect(bs / 2, 0, bs, bs), paint);
        } else if (bmps.length == 3) {
            c.drawBitmap(bmps[0], new Rect(bmps[0].getWidth() / 4, 0, (bmps[0].getWidth() / 4) * 3, bmps[0].getHeight()), new Rect(0, 0, bs / 2, bs), paint);
            c.drawBitmap(bmps[1], new Rect(0, 0, bmps[1].getWidth(), bmps[1].getHeight()), new Rect(bs / 2, 0, bs, bs / 2), paint);
            c.drawBitmap(bmps[2], new Rect(0, 0, bmps[2].getWidth(), bmps[2].getHeight()), new Rect(bs / 2, bs / 2, bs, bs), paint);
        } else if (bmps.length == 4) {
            c.drawBitmap(bmps[0], new Rect(0, 0, bmps[0].getWidth(), bmps[0].getHeight()), new Rect(0, 0, bs / 2, bs / 2), paint);
            c.drawBitmap(bmps[1], new Rect(0, 0, bmps[1].getWidth(), bmps[1].getHeight()), new Rect(0, bs / 2, bs / 2, bs), paint);
            c.drawBitmap(bmps[2], new Rect(0, 0, bmps[2].getWidth(), bmps[2].getHeight()), new Rect(bs / 2, 0, bs, bs / 2), paint);
            c.drawBitmap(bmps[3], new Rect(0, 0, bmps[3].getWidth(), bmps[3].getHeight()), new Rect(bs / 2, bs / 2, bs, bs), paint);
        }
        return bmp;
    }

    private static void drawBitmapRounded(Canvas c, Bitmap bmp, Rect src, Rect dst) {
        Paint paint = new Paint();
        paint.setAntiAlias(DEBUG);
        paint.setFilterBitmap(DEBUG);
        Path p = new Path();
        if (Global.displayDensity > 1.0f) {
            p.addRoundRect(new RectF(dst), 2.8f, 2.8f, Direction.CW);
        } else {
            p.addRoundRect(new RectF(dst), ImageViewHolder.PaddingSize, ImageViewHolder.PaddingSize, Direction.CW);
        }
        c.save();
        c.clipPath(p);
        c.drawBitmap(bmp, src, dst, paint);
        c.restore();
    }
}
