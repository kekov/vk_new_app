package com.vkontakte.android.ui;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.os.Build.VERSION;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.SeekBar;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.EmojiPopup;
import com.vkontakte.android.Global;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.MenuListView;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MenuOverlayView extends FrameLayout {
    private static final int CONTENT_VIEW = 2;
    private static final int DIR_LEFT = -1;
    private static final int DIR_NONE = 0;
    private static final int DIR_RIGHT = 1;
    private static final int HALF_OPEN_WIDTH = 47;
    public static final int MODE_FULL_VISIBLE = 2;
    public static final int MODE_HALF_VISIBLE = 1;
    public static final int MODE_HIDDEN = 0;
    private static final boolean isShittyDevice;
    private boolean animating;
    private boolean backConsumed;
    private boolean closing;
    private boolean disableScrollNow;
    private boolean dropAllEvents;
    private GestureDetector gestureDetector;
    private SimpleOnGestureListener gestureListener;
    private float lastFlingSpeed;
    private int lastScrollDirection;
    ListView listView;
    private MenuListView lv;
    private Method mSetTranslationX;
    private boolean menuOpen;
    private int menuWidth;
    private int mode;
    private int padding;
    private int sOffset;
    private int scrollOffset;
    private boolean scrolling;
    private View shadowView;
    private float touchslop;

    /* renamed from: com.vkontakte.android.ui.MenuOverlayView.1 */
    class C10051 extends SimpleOnGestureListener {
        C10051() {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            MenuOverlayView.this.lastFlingSpeed = velocityX;
            return true;
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            MenuOverlayView.this.lastFlingSpeed = 0.0f;
            if (MenuOverlayView.this.disableScrollNow || ((float) MenuOverlayView.this.padding) - distanceX < 0.0f) {
                return false;
            }
            distanceX += (float) MenuOverlayView.this.scrollOffset;
            if (!MenuOverlayView.this.scrolling && Math.abs(distanceX) > Math.abs(distanceY)) {
                MenuOverlayView.this.scrolling = true;
                if (MenuOverlayView.this.canScroll(e1)) {
                    MenuOverlayView.this.startDragging();
                    MenuOverlayView.this.updateDragPosition(true);
                } else {
                    MenuOverlayView.this.disableScrollNow = true;
                    MenuOverlayView.this.padding = MenuOverlayView.DIR_NONE;
                    MenuOverlayView.this.scrolling = false;
                    return false;
                }
            }
            if (MenuOverlayView.this.scrolling) {
                if (Math.abs(distanceX) > MenuOverlayView.this.touchslop) {
                    if (distanceX > 0.0f) {
                        MenuOverlayView.this.lastScrollDirection = MenuOverlayView.DIR_LEFT;
                    }
                    if (distanceX < 0.0f) {
                        MenuOverlayView.this.lastScrollDirection = MenuOverlayView.MODE_HALF_VISIBLE;
                    }
                }
                MenuOverlayView menuOverlayView = MenuOverlayView.this;
                menuOverlayView.padding = (int) (((float) menuOverlayView.padding) - distanceX);
                if (MenuOverlayView.this.padding < 0) {
                    MenuOverlayView.this.disableScrollNow = true;
                    MenuOverlayView.this.padding = MenuOverlayView.DIR_NONE;
                    MenuOverlayView.this.scrolling = false;
                    MenuOverlayView.this.updateDragPosition(true);
                    return false;
                }
                MenuOverlayView.this.updateDragPosition(true);
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.ui.MenuOverlayView.2 */
    class C10062 implements Runnable {
        C10062() {
        }

        public void run() {
            MenuOverlayView.this.menuOpen = true;
            MenuOverlayView.this.requestLayout();
            MenuOverlayView.this.animating = false;
            MenuOverlayView.this.getChildAt(MenuOverlayView.MODE_FULL_VISIBLE).invalidate();
        }
    }

    /* renamed from: com.vkontakte.android.ui.MenuOverlayView.3 */
    class C10073 implements AnimatorListener {
        C10073() {
        }

        public void onAnimationStart(Animator animation) {
        }

        public void onAnimationRepeat(Animator animation) {
        }

        public void onAnimationEnd(Animator animation) {
            MenuOverlayView.this.menuOpen = true;
            MenuOverlayView.this.getChildAt(MenuOverlayView.MODE_FULL_VISIBLE).setTranslationX(0.0f);
            MenuOverlayView.this.requestLayout();
            MenuOverlayView.this.animating = false;
        }

        public void onAnimationCancel(Animator animation) {
        }
    }

    /* renamed from: com.vkontakte.android.ui.MenuOverlayView.4 */
    class C10084 implements Runnable {
        C10084() {
        }

        public void run() {
            MenuOverlayView.this.setMenuVisible(false);
            MenuOverlayView.this.requestLayout();
            MenuOverlayView.this.animating = false;
        }
    }

    /* renamed from: com.vkontakte.android.ui.MenuOverlayView.5 */
    class C10095 implements AnimatorListener {
        C10095() {
        }

        public void onAnimationStart(Animator animation) {
        }

        public void onAnimationRepeat(Animator animation) {
        }

        public void onAnimationEnd(Animator animation) {
            MenuOverlayView.this.getChildAt(MenuOverlayView.MODE_FULL_VISIBLE).setTranslationX(0.0f);
            MenuOverlayView.this.setMenuVisible(false);
            MenuOverlayView.this.requestLayout();
            MenuOverlayView.this.animating = false;
        }

        public void onAnimationCancel(Animator animation) {
        }
    }

    /* renamed from: com.vkontakte.android.ui.MenuOverlayView.6 */
    class C10106 implements Runnable {
        private final /* synthetic */ int val$m;

        C10106(int i) {
            this.val$m = i;
        }

        public void run() {
            LayoutParams lp = new LayoutParams(MenuOverlayView.DIR_LEFT, MenuOverlayView.DIR_LEFT);
            if (this.val$m == 0) {
                lp.leftMargin = MenuOverlayView.DIR_NONE;
            } else if (this.val$m == MenuOverlayView.MODE_HALF_VISIBLE) {
                lp.leftMargin = Global.scale(47.0f);
                MenuOverlayView.this.setMenuVisible(true);
            } else if (this.val$m == MenuOverlayView.MODE_FULL_VISIBLE) {
                lp.leftMargin = MenuOverlayView.this.menuWidth;
                MenuOverlayView.this.setMenuVisible(true);
                MenuOverlayView.this.lv.setTranslationX(0.0f);
                MenuOverlayView.this.lv.setAlpha(1.0f);
                MenuOverlayView.this.shadowView.setTranslationX(0.0f);
            }
            MenuOverlayView.this.mode = this.val$m;
            MenuOverlayView.this.getChildAt(MenuOverlayView.MODE_FULL_VISIBLE).setLayoutParams(lp);
            MenuOverlayView.this.requestLayout();
        }
    }

    /* renamed from: com.vkontakte.android.ui.MenuOverlayView.7 */
    class C10117 implements Runnable {
        C10117() {
        }

        public void run() {
            MenuOverlayView.this.animating = false;
            MenuOverlayView.this.shadowView.clearAnimation();
            MenuOverlayView.this.getChildAt(MenuOverlayView.MODE_FULL_VISIBLE).clearAnimation();
            MenuOverlayView.this.setMenuVisible(false);
        }
    }

    /* renamed from: com.vkontakte.android.ui.MenuOverlayView.8 */
    class C10128 implements AnimatorListener {
        C10128() {
        }

        public void onAnimationStart(Animator animation) {
        }

        public void onAnimationRepeat(Animator animation) {
        }

        public void onAnimationEnd(Animator animation) {
            MenuOverlayView.this.animating = false;
            MenuOverlayView.this.setMenuVisible(false);
        }

        public void onAnimationCancel(Animator animation) {
        }
    }

    /* renamed from: com.vkontakte.android.ui.MenuOverlayView.9 */
    class C10139 implements Runnable {
        C10139() {
        }

        public void run() {
            MenuOverlayView.this.menuOpen = true;
            MenuOverlayView.this.animating = false;
            MenuOverlayView.this.requestLayout();
        }
    }

    static {
        boolean z = true;
        List<String> deviceNames = Arrays.asList(new String[]{"ZTE V970M", "LG-P880", "SP-A20i"});
        if (!(VERSION.SDK_INT == 15 && deviceNames.contains(Build.MODEL))) {
            z = false;
        }
        isShittyDevice = z;
    }

    public MenuOverlayView(Context context) {
        super(context);
        this.scrolling = false;
        this.disableScrollNow = false;
        this.padding = DIR_NONE;
        this.menuOpen = false;
        this.dropAllEvents = false;
        this.menuWidth = 270;
        this.lastFlingSpeed = 0.0f;
        this.backConsumed = false;
        this.closing = false;
        this.scrollOffset = DIR_NONE;
        this.sOffset = DIR_NONE;
        this.mode = DIR_NONE;
        this.animating = false;
        this.gestureListener = new C10051();
        init();
    }

    public MenuOverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.scrolling = false;
        this.disableScrollNow = false;
        this.padding = DIR_NONE;
        this.menuOpen = false;
        this.dropAllEvents = false;
        this.menuWidth = 270;
        this.lastFlingSpeed = 0.0f;
        this.backConsumed = false;
        this.closing = false;
        this.scrollOffset = DIR_NONE;
        this.sOffset = DIR_NONE;
        this.mode = DIR_NONE;
        this.animating = false;
        this.gestureListener = new C10051();
        init();
    }

    public MenuOverlayView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.scrolling = false;
        this.disableScrollNow = false;
        this.padding = DIR_NONE;
        this.menuOpen = false;
        this.dropAllEvents = false;
        this.menuWidth = 270;
        this.lastFlingSpeed = 0.0f;
        this.backConsumed = false;
        this.closing = false;
        this.scrollOffset = DIR_NONE;
        this.sOffset = DIR_NONE;
        this.mode = DIR_NONE;
        this.animating = false;
        this.gestureListener = new C10051();
        init();
    }

    public void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed) {
            this.menuWidth = Math.min(Global.scale(BitmapDescriptorFactory.HUE_VIOLET), (r - l) - Global.scale(70.0f));
            LayoutParams llp = new LayoutParams(this.menuWidth, DIR_LEFT);
            Rect rect = new Rect();
            getWindowVisibleDisplayFrame(rect);
            llp.topMargin = rect.top;
            llp.gravity = 80;
            this.lv.setLayoutParams(llp);
        }
        super.onLayout(changed, l, t, r, b);
        if (this.mode == MODE_FULL_VISIBLE) {
            getChildAt(MODE_FULL_VISIBLE).layout(this.menuWidth, DIR_NONE, r - l, b - t);
            this.shadowView.layout(this.menuWidth - this.shadowView.getMeasuredWidth(), DIR_NONE, this.menuWidth, b - t);
        } else if (this.mode == MODE_HALF_VISIBLE) {
            getChildAt(MODE_FULL_VISIBLE).layout(Global.scale(47.0f), DIR_NONE, r - l, b - t);
            this.shadowView.layout(Global.scale(47.0f) - this.shadowView.getMeasuredWidth(), DIR_NONE, Global.scale(47.0f), b - t);
        }
        if (this.menuOpen && this.padding == 0) {
            getChildAt(MODE_FULL_VISIBLE).clearAnimation();
            getChildAt(MODE_FULL_VISIBLE).layout(this.menuWidth, DIR_NONE, this.menuWidth + r, b - t);
        }
    }

    public MenuListView getListView() {
        return this.lv;
    }

    private void init() {
        this.gestureDetector = new GestureDetector(getContext(), this.gestureListener);
        this.touchslop = (float) ViewConfiguration.get(getContext()).getScaledTouchSlop();
        this.lv = new MenuListView(getContext());
        this.listView = this.lv.list;
        addView(this.lv);
        this.shadowView = new View(getContext());
        this.shadowView.setLayoutParams(new LayoutParams(Global.scale(16.0f), DIR_LEFT));
        this.shadowView.setBackgroundResource(C0436R.drawable.left_shadow);
        addView(this.shadowView);
        setFocusable(true);
        setFocusableInTouchMode(true);
        if (Integer.parseInt(VERSION.SDK) >= 11) {
            try {
                Class[] clsArr = new Class[MODE_HALF_VISIBLE];
                clsArr[DIR_NONE] = Float.TYPE;
                this.mSetTranslationX = View.class.getMethod("setTranslationX", clsArr);
            } catch (Exception e) {
            }
        }
        setMenuVisible(false);
    }

    private boolean canScroll(MotionEvent ev) {
        if (findHorizontalScrollView(getChildAt(MODE_FULL_VISIBLE), (int) ev.getRawX(), (int) ev.getRawY())) {
            return false;
        }
        ViewPager vp = findViewPager(getChildAt(MODE_FULL_VISIBLE));
        if (vp == null) {
            return true;
        }
        int[] scr = new int[MODE_FULL_VISIBLE];
        vp.getLocationOnScreen(scr);
        if (vp.getCurrentItem() == 0 || ev.getRawY() < ((float) scr[MODE_HALF_VISIBLE]) || ev.getRawY() > ((float) (scr[MODE_HALF_VISIBLE] + vp.getHeight())) || ev.getRawX() < ((float) scr[DIR_NONE]) || ev.getRawX() > ((float) (scr[DIR_NONE] + vp.getWidth()))) {
            return true;
        }
        return false;
    }

    private boolean findHorizontalScrollView(View container, int x, int y) {
        if (container.getVisibility() != 0) {
            return false;
        }
        if ((container instanceof HorizontalScrollView) || (container instanceof PhotoFeedView) || (container instanceof SeekBar)) {
            int[] loc = new int[MODE_FULL_VISIBLE];
            container.getLocationOnScreen(loc);
            if (x < loc[DIR_NONE] || x > loc[DIR_NONE] + container.getWidth() || y < loc[MODE_HALF_VISIBLE] || y > loc[MODE_HALF_VISIBLE] + container.getHeight()) {
                return false;
            }
            return true;
        } else if (!(container instanceof ViewGroup)) {
            return false;
        } else {
            ViewGroup g = (ViewGroup) container;
            for (int i = DIR_NONE; i < g.getChildCount(); i += MODE_HALF_VISIBLE) {
                if (findHorizontalScrollView(g.getChildAt(i), x, y)) {
                    return true;
                }
            }
            return false;
        }
    }

    private ViewPager findViewPager(View container) {
        if (container.getVisibility() == 8) {
            return null;
        }
        if (container instanceof ViewPager) {
            return (ViewPager) container;
        }
        if (container instanceof ViewGroup) {
            ViewGroup g = (ViewGroup) container;
            for (int i = DIR_NONE; i < g.getChildCount(); i += MODE_HALF_VISIBLE) {
                View vp = findViewPager(g.getChildAt(i));
                if (vp != null) {
                    return vp;
                }
            }
        }
        return null;
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (this.mode == MODE_FULL_VISIBLE && !this.menuOpen) {
            return super.onInterceptTouchEvent(ev);
        }
        if (!this.menuOpen) {
            this.gestureDetector.onTouchEvent(ev);
        }
        if ((ev.getAction() & MotionEventCompat.ACTION_MASK) == 0) {
            this.disableScrollNow = false;
        }
        if (this.scrolling || (this.menuOpen && ev.getX() >= ((float) this.menuWidth))) {
            return true;
        }
        return false;
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (this.animating) {
            return false;
        }
        return super.dispatchTouchEvent(ev);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (this.mode == MODE_FULL_VISIBLE) {
            return super.onTouchEvent(ev);
        }
        if (!this.menuOpen && (ev.getAction() & MotionEventCompat.ACTION_MASK) == 0) {
            this.disableScrollNow = false;
        }
        if (!this.menuOpen) {
            this.gestureDetector.onTouchEvent(ev);
        }
        if (this.menuOpen && (ev.getAction() & MotionEventCompat.ACTION_MASK) == 0) {
            if (ev.getX() > ((float) (this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE)))) {
                this.scrolling = true;
                this.closing = true;
                this.padding = this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE);
                this.menuOpen = false;
                this.disableScrollNow = false;
                startDragging();
                this.gestureDetector.onTouchEvent(ev);
                requestLayout();
                return true;
            }
        }
        if (this.menuOpen) {
            this.lv.dispatchTouchEvent(ev);
            return true;
        }
        if ((ev.getAction() & MotionEventCompat.ACTION_MASK) == MODE_HALF_VISIBLE) {
            this.disableScrollNow = false;
            if (this.scrolling) {
                if (!this.closing || this.lastScrollDirection == MODE_HALF_VISIBLE) {
                    TranslateAnimation anim;
                    AnimatorSet set;
                    ArrayList<Animator> anims;
                    float[] fArr;
                    if (this.padding > Global.scale(70.0f)) {
                        long d = (long) (((((float) (this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE))) / this.lastFlingSpeed) * 1000.0f) * (((float) this.padding) / ((float) (this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE)))));
                        this.animating = true;
                        if (VERSION.SDK_INT < 11 || isShittyDevice) {
                            anim = new TranslateAnimation((float) this.padding, (float) (this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE)), 0.0f, 0.0f);
                            TranslateAnimation anim2 = new TranslateAnimation((float) (this.padding - Global.scale(16.0f)), (float) ((this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE)) - Global.scale(16.0f)), 0.0f, 0.0f);
                            anim.setFillAfter(true);
                            anim2.setFillAfter(true);
                            if (this.lastFlingSpeed > 0.0f) {
                                if (this.padding < this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE)) {
                                    if (d > 500) {
                                        d = 500;
                                    }
                                    anim.setInterpolator(new DecelerateInterpolator());
                                    anim.setDuration(d);
                                    anim2.setInterpolator(new DecelerateInterpolator());
                                    anim2.setDuration(d);
                                    getChildAt(MODE_FULL_VISIBLE).startAnimation(anim);
                                    this.shadowView.startAnimation(anim2);
                                    postDelayed(new C10062(), anim.getDuration());
                                }
                            }
                            anim.setDuration(300);
                            anim2.setDuration(300);
                            getChildAt(MODE_FULL_VISIBLE).startAnimation(anim);
                            this.shadowView.startAnimation(anim2);
                            postDelayed(new C10062(), anim.getDuration());
                        } else {
                            View childAt;
                            String str;
                            float[] fArr2;
                            Interpolator itrp = new DecelerateInterpolator();
                            if (this.lastFlingSpeed > 0.0f) {
                                if (this.padding < this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE)) {
                                    if (d > 500) {
                                        d = 500;
                                    }
                                    if (d < 200) {
                                        d = 200;
                                    }
                                    set = new AnimatorSet();
                                    anims = new ArrayList();
                                    childAt = getChildAt(MODE_FULL_VISIBLE);
                                    str = "translationX";
                                    fArr2 = new float[MODE_HALF_VISIBLE];
                                    fArr2[DIR_NONE] = (float) (this.menuWidth - (this.mode != MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE));
                                    anims.add(ObjectAnimator.ofFloat(childAt, str, fArr2).setDuration(d));
                                    childAt = this.shadowView;
                                    str = "translationX";
                                    fArr2 = new float[MODE_HALF_VISIBLE];
                                    fArr2[DIR_NONE] = (float) ((this.menuWidth - (this.mode != MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE)) - Global.scale(16.0f));
                                    anims.add(ObjectAnimator.ofFloat(childAt, str, fArr2).setDuration(d));
                                    if (this.mode == 0) {
                                        fArr = new float[MODE_HALF_VISIBLE];
                                        fArr[DIR_NONE] = 1.0f;
                                        anims.add(ObjectAnimator.ofFloat(this.lv, "alpha", fArr).setDuration(d));
                                        fArr = new float[MODE_HALF_VISIBLE];
                                        fArr[DIR_NONE] = 0.0f;
                                        anims.add(ObjectAnimator.ofFloat(this.lv, "translationX", fArr).setDuration(d));
                                    }
                                    set.playTogether(anims);
                                    set.setInterpolator(itrp);
                                    set.addListener(new C10073());
                                    set.start();
                                }
                            }
                            d = 300;
                            if (d < 200) {
                                d = 200;
                            }
                            set = new AnimatorSet();
                            anims = new ArrayList();
                            childAt = getChildAt(MODE_FULL_VISIBLE);
                            str = "translationX";
                            fArr2 = new float[MODE_HALF_VISIBLE];
                            if (this.mode != MODE_HALF_VISIBLE) {
                            }
                            fArr2[DIR_NONE] = (float) (this.menuWidth - (this.mode != MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE));
                            anims.add(ObjectAnimator.ofFloat(childAt, str, fArr2).setDuration(d));
                            childAt = this.shadowView;
                            str = "translationX";
                            fArr2 = new float[MODE_HALF_VISIBLE];
                            if (this.mode != MODE_HALF_VISIBLE) {
                            }
                            fArr2[DIR_NONE] = (float) ((this.menuWidth - (this.mode != MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE)) - Global.scale(16.0f));
                            anims.add(ObjectAnimator.ofFloat(childAt, str, fArr2).setDuration(d));
                            if (this.mode == 0) {
                                fArr = new float[MODE_HALF_VISIBLE];
                                fArr[DIR_NONE] = 1.0f;
                                anims.add(ObjectAnimator.ofFloat(this.lv, "alpha", fArr).setDuration(d));
                                fArr = new float[MODE_HALF_VISIBLE];
                                fArr[DIR_NONE] = 0.0f;
                                anims.add(ObjectAnimator.ofFloat(this.lv, "translationX", fArr).setDuration(d));
                            }
                            set.playTogether(anims);
                            set.setInterpolator(itrp);
                            set.addListener(new C10073());
                            set.start();
                        }
                        this.padding = DIR_NONE;
                        updateDragPosition(false);
                    } else {
                        this.animating = true;
                        if (VERSION.SDK_INT < 11 || isShittyDevice) {
                            anim = new TranslateAnimation((float) this.padding, 0.0f, 0.0f, 0.0f);
                            anim.setFillAfter(true);
                            anim.setDuration(300);
                            getChildAt(MODE_FULL_VISIBLE).startAnimation(anim);
                            anim = new TranslateAnimation((float) (this.padding - Global.scale(16.0f)), (float) (-Global.scale(16.0f)), 0.0f, 0.0f);
                            anim.setFillAfter(true);
                            anim.setDuration(300);
                            this.shadowView.startAnimation(anim);
                            postDelayed(new C10084(), 300);
                        } else {
                            set = new AnimatorSet();
                            anims = new ArrayList();
                            fArr = new float[MODE_HALF_VISIBLE];
                            fArr[DIR_NONE] = 0.0f;
                            anims.add(ObjectAnimator.ofFloat(getChildAt(MODE_FULL_VISIBLE), "translationX", fArr).setDuration(300));
                            fArr = new float[MODE_HALF_VISIBLE];
                            fArr[DIR_NONE] = (float) (-Global.scale(16.0f));
                            anims.add(ObjectAnimator.ofFloat(this.shadowView, "translationX", fArr).setDuration(300));
                            if (this.mode == 0) {
                                fArr = new float[MODE_HALF_VISIBLE];
                                fArr[DIR_NONE] = 0.3f;
                                anims.add(ObjectAnimator.ofFloat(this.lv, "alpha", fArr).setDuration(300));
                                fArr = new float[MODE_HALF_VISIBLE];
                                fArr[DIR_NONE] = (float) ((-this.menuWidth) / MODE_FULL_VISIBLE);
                                anims.add(ObjectAnimator.ofFloat(this.lv, "translationX", fArr).setDuration(300));
                            }
                            set.playTogether(anims);
                            set.setInterpolator(new DecelerateInterpolator());
                            set.addListener(new C10095());
                            set.start();
                        }
                        this.padding = DIR_NONE;
                        updateDragPosition(false);
                    }
                    this.scrolling = false;
                    this.scrollOffset = DIR_NONE;
                } else if (this.padding == 0) {
                    closeMenu(DIR_NONE);
                    this.closing = false;
                    return true;
                } else {
                    if (this.lastFlingSpeed < 0.0f) {
                        closeMenu((int) Math.abs((((float) this.padding) / this.lastFlingSpeed) * 1000.0f));
                    } else {
                        closeMenu();
                    }
                    this.closing = false;
                    return true;
                }
            }
        }
        return true;
    }

    public void setMode(int m) {
        post(new C10106(m));
    }

    private void startDragging() {
        if (!(this.mSetTranslationX == null || isShittyDevice)) {
            updateDragPosition(true);
            getChildAt(MODE_FULL_VISIBLE).clearAnimation();
            this.shadowView.clearAnimation();
        }
        setMenuVisible(true);
        View focus = ((Activity) getContext()).getCurrentFocus();
        if (focus != null) {
            ((InputMethodManager) getContext().getSystemService("input_method")).hideSoftInputFromWindow(focus.getWindowToken(), DIR_NONE);
            focus.clearFocus();
        }
        getContext().sendBroadcast(new Intent(EmojiPopup.ACTION_HIDE_POPUP), permission.ACCESS_DATA);
    }

    private void updateDragPosition(boolean slide) {
        if (!(this.mSetTranslationX == null || isShittyDevice)) {
            try {
                getChildAt(MODE_FULL_VISIBLE).setTranslationX((float) this.padding);
                if (slide) {
                    if (this.padding >= this.menuWidth || this.mode != 0) {
                        this.lv.setAlpha(1.0f);
                        this.lv.setTranslationX(0.0f);
                    } else {
                        this.lv.setAlpha(((((float) this.padding) / ((float) this.menuWidth)) * 0.7f) + 0.3f);
                        this.lv.setTranslationX((float) (((-this.menuWidth) / MODE_FULL_VISIBLE) + (this.padding / MODE_FULL_VISIBLE)));
                    }
                }
                if (this.padding == 0) {
                    this.shadowView.setTranslationX(0.0f);
                } else {
                    this.shadowView.setTranslationX((float) ((this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE) + (this.padding - Global.scale(16.0f))));
                }
            } catch (Exception e) {
            }
        }
        invalidate();
    }

    public void closeMenu(int d) {
        if (d > 500) {
            d = 500;
        }
        if (this.mode != MODE_FULL_VISIBLE) {
            requestLayout();
            if (d > 0) {
                this.animating = true;
                int scale;
                if (VERSION.SDK_INT < 11 || isShittyDevice) {
                    if (this.padding == 0) {
                        scale = this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE);
                    } else {
                        scale = this.padding;
                    }
                    TranslateAnimation anim = new TranslateAnimation((float) scale, 0.0f, 0.0f, 0.0f);
                    anim.setFillAfter(true);
                    anim.setDuration((long) d);
                    getChildAt(MODE_FULL_VISIBLE).startAnimation(anim);
                    anim = new TranslateAnimation((float) ((this.padding == 0 ? this.menuWidth : this.padding) - Global.scale(16.0f)), (float) Global.scale(-16.0f), 0.0f, 0.0f);
                    anim.setFillAfter(true);
                    anim.setDuration((long) d);
                    this.shadowView.startAnimation(anim);
                    postDelayed(new C10117(), (long) (d + 10));
                } else {
                    ArrayList<Animator> anims = new ArrayList();
                    View childAt = getChildAt(MODE_FULL_VISIBLE);
                    String str = "translationX";
                    float[] fArr = new float[MODE_FULL_VISIBLE];
                    if (this.padding == 0) {
                        int i = this.menuWidth;
                        if (this.mode == MODE_HALF_VISIBLE) {
                            scale = Global.scale(47.0f);
                        } else {
                            scale = DIR_NONE;
                        }
                        scale = i - scale;
                    } else {
                        scale = this.padding;
                    }
                    fArr[DIR_NONE] = (float) scale;
                    fArr[MODE_HALF_VISIBLE] = 0.0f;
                    anims.add(ObjectAnimator.ofFloat(childAt, str, fArr).setDuration((long) d));
                    childAt = this.shadowView;
                    str = "translationX";
                    fArr = new float[MODE_FULL_VISIBLE];
                    fArr[DIR_NONE] = (float) ((this.padding == 0 ? this.menuWidth : this.padding) - Global.scale(16.0f));
                    fArr[MODE_HALF_VISIBLE] = (float) Global.scale(-16.0f);
                    anims.add(ObjectAnimator.ofFloat(childAt, str, fArr).setDuration((long) d));
                    if (this.mode == 0) {
                        float[] fArr2 = new float[MODE_HALF_VISIBLE];
                        fArr2[DIR_NONE] = 0.3f;
                        anims.add(ObjectAnimator.ofFloat(this.lv, "alpha", fArr2).setDuration((long) d));
                        fArr2 = new float[MODE_HALF_VISIBLE];
                        fArr2[DIR_NONE] = (float) ((-this.menuWidth) / MODE_FULL_VISIBLE);
                        anims.add(ObjectAnimator.ofFloat(this.lv, "translationX", fArr2).setDuration((long) d));
                    }
                    AnimatorSet set = new AnimatorSet();
                    set.playTogether(anims);
                    set.setInterpolator(new DecelerateInterpolator());
                    set.addListener(new C10128());
                    set.start();
                }
            } else {
                setMenuVisible(false);
            }
            this.menuOpen = false;
            this.scrolling = false;
            this.padding = DIR_NONE;
            updateDragPosition(false);
        }
    }

    public void closeMenu() {
        closeMenu(PhotoView.THUMB_ANIM_DURATION);
    }

    public boolean drawChild(Canvas canvas, View view, long time) {
        if (VERSION.SDK_INT >= 11 && !isShittyDevice) {
            return super.drawChild(canvas, view, time);
        }
        canvas.save();
        if (view == getChildAt(MODE_FULL_VISIBLE)) {
            canvas.translate((float) this.padding, 0.0f);
        }
        if (view == this.shadowView && this.padding != 0) {
            canvas.translate((float) (this.padding - Global.scale(16.0f)), 0.0f);
        }
        boolean res = super.drawChild(canvas, view, time);
        canvas.restore();
        return res;
    }

    public void openMenu() {
        if (this.mode != MODE_FULL_VISIBLE) {
            this.animating = true;
            if (VERSION.SDK_INT < 11 || isShittyDevice) {
                TranslateAnimation anim = new TranslateAnimation((float) this.padding, (float) (this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE)), 0.0f, 0.0f);
                TranslateAnimation anim2 = new TranslateAnimation((float) (this.padding - Global.scale(16.0f)), (float) ((this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE)) - Global.scale(16.0f)), 0.0f, 0.0f);
                anim.setFillAfter(true);
                anim2.setFillAfter(true);
                anim.setDuration(300);
                anim2.setDuration(300);
                getChildAt(MODE_FULL_VISIBLE).startAnimation(anim);
                this.shadowView.startAnimation(anim2);
                this.padding = DIR_NONE;
                updateDragPosition(false);
                postDelayed(new C10139(), anim.getDuration());
            } else if (this.mode == 0) {
                AnimatorSet set = new AnimatorSet();
                r6 = new Animator[4];
                float[] fArr = new float[MODE_FULL_VISIBLE];
                fArr[DIR_NONE] = (float) ((-this.menuWidth) / MODE_FULL_VISIBLE);
                fArr[MODE_HALF_VISIBLE] = 0.0f;
                r6[MODE_HALF_VISIBLE] = ObjectAnimator.ofFloat(this.lv, "translationX", fArr).setDuration(300);
                View childAt = getChildAt(MODE_FULL_VISIBLE);
                String str = "translationX";
                float[] fArr2 = new float[MODE_HALF_VISIBLE];
                fArr2[DIR_NONE] = (float) (this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE));
                r6[MODE_FULL_VISIBLE] = ObjectAnimator.ofFloat(childAt, str, fArr2).setDuration(300);
                childAt = this.shadowView;
                str = "translationX";
                fArr2 = new float[MODE_HALF_VISIBLE];
                fArr2[DIR_NONE] = (float) ((this.menuWidth - (this.mode == MODE_HALF_VISIBLE ? Global.scale(47.0f) : DIR_NONE)) - Global.scale(16.0f));
                r6[3] = ObjectAnimator.ofFloat(childAt, str, fArr2).setDuration(300);
                set.playTogether(r6);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListener() {
                    public void onAnimationStart(Animator animation) {
                    }

                    public void onAnimationRepeat(Animator animation) {
                    }

                    public void onAnimationEnd(Animator animation) {
                        MenuOverlayView.this.menuOpen = true;
                        MenuOverlayView.this.getChildAt(MenuOverlayView.MODE_FULL_VISIBLE).setTranslationX(0.0f);
                        MenuOverlayView.this.animating = false;
                        MenuOverlayView.this.requestLayout();
                    }

                    public void onAnimationCancel(Animator animation) {
                    }
                });
                set.start();
            }
            View focus = ((Activity) getContext()).getCurrentFocus();
            if (focus != null) {
                ((InputMethodManager) getContext().getSystemService("input_method")).hideSoftInputFromWindow(focus.getWindowToken(), DIR_NONE);
            }
            setMenuVisible(true);
        }
    }

    public boolean onKeyDown(int code, KeyEvent keyEvent) {
        if (code != 4 || !this.menuOpen) {
            return super.onKeyDown(code, keyEvent);
        }
        closeMenu();
        this.backConsumed = true;
        return true;
    }

    public boolean onKeyUp(int code, KeyEvent event) {
        if (!this.backConsumed || code != 4) {
            return super.onKeyDown(code, event);
        }
        this.backConsumed = false;
        return true;
    }

    public void updateInfo() {
        this.lv.updateUserInfo();
    }

    private void setMenuVisible(boolean vis) {
        int i = DIR_NONE;
        if (this.mode == 0) {
            this.lv.setVisibility(vis ? DIR_NONE : 4);
            if (vis) {
                i = -14473165;
            }
            setBackgroundColor(i);
        }
    }
}
