package com.vkontakte.android.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.ViewUtils;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.AnimationDuration;

public class ReorderableListView extends ListView {
    private static final TypeEvaluator<Rect> sBoundEvaluator;
    private final int INVALID_ID;
    private final int INVALID_POINTER_ID;
    private final int MOVE_DURATION;
    private final int SMOOTH_SCROLL_AMOUNT_AT_EDGE;
    private DragListener dragListener;
    private long mAboveItemId;
    private int mActivePointerId;
    private long mBelowItemId;
    private boolean mCellIsMobile;
    private int mDownX;
    private int mDownY;
    private BitmapDrawable mHoverCell;
    private Rect mHoverCellCurrentBounds;
    private Rect mHoverCellOriginalBounds;
    private boolean mIsMobileScrolling;
    private boolean mIsWaitingForScrollFinish;
    private int mLastEventY;
    private long mMobileItemId;
    private OnItemLongClickListener mOnItemLongClickListener;
    private OnScrollListener mScrollListener;
    private int mScrollState;
    private int mSmoothScrollAmountAtEdge;
    private int mTotalOffset;
    private int prevOrientation;

    /* renamed from: com.vkontakte.android.ui.ReorderableListView.1 */
    class C10521 implements OnItemLongClickListener {
        C10521() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View arg1, int pos, long id) {
            ReorderableListView.this.mTotalOffset = 0;
            if ((ReorderableListView.this.getAdapter() instanceof DraggableChecker) && !((DraggableChecker) ReorderableListView.this.getAdapter()).canDragItem(pos)) {
                return false;
            }
            int position = ReorderableListView.this.pointToPosition(ReorderableListView.this.mDownX, ReorderableListView.this.mDownY);
            View selectedView = ReorderableListView.this.getChildAt(position - ReorderableListView.this.getFirstVisiblePosition());
            ReorderableListView.this.mMobileItemId = ReorderableListView.this.getAdapter().getItemId(position);
            ReorderableListView.this.mHoverCell = ReorderableListView.this.getAndAddHoverView(selectedView);
            selectedView.setVisibility(4);
            ReorderableListView.this.mCellIsMobile = true;
            ReorderableListView.this.updateNeighborViewsForID(ReorderableListView.this.mMobileItemId);
            if (ReorderableListView.this.dragListener != null) {
                ReorderableListView.this.dragListener.onDragStart(ReorderableListView.this.mMobileItemId);
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.ui.ReorderableListView.2 */
    class C10532 implements TypeEvaluator<Rect> {
        C10532() {
        }

        public Rect evaluate(float fraction, Rect startValue, Rect endValue) {
            return new Rect(interpolate(startValue.left, endValue.left, fraction), interpolate(startValue.top, endValue.top, fraction), interpolate(startValue.right, endValue.right, fraction), interpolate(startValue.bottom, endValue.bottom, fraction));
        }

        public int interpolate(int start, int end, float fraction) {
            return (int) (((float) start) + (((float) (end - start)) * fraction));
        }
    }

    /* renamed from: com.vkontakte.android.ui.ReorderableListView.3 */
    class C10543 implements OnScrollListener {
        private int mCurrentFirstVisibleItem;
        private int mCurrentScrollState;
        private int mCurrentVisibleItemCount;
        private int mPreviousFirstVisibleItem;
        private int mPreviousVisibleItemCount;

        C10543() {
            this.mPreviousFirstVisibleItem = -1;
            this.mPreviousVisibleItemCount = -1;
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int i;
            this.mCurrentFirstVisibleItem = firstVisibleItem;
            this.mCurrentVisibleItemCount = visibleItemCount;
            if (this.mPreviousFirstVisibleItem == -1) {
                i = this.mCurrentFirstVisibleItem;
            } else {
                i = this.mPreviousFirstVisibleItem;
            }
            this.mPreviousFirstVisibleItem = i;
            if (this.mPreviousVisibleItemCount == -1) {
                i = this.mCurrentVisibleItemCount;
            } else {
                i = this.mPreviousVisibleItemCount;
            }
            this.mPreviousVisibleItemCount = i;
            checkAndHandleFirstVisibleCellChange();
            checkAndHandleLastVisibleCellChange();
            this.mPreviousFirstVisibleItem = this.mCurrentFirstVisibleItem;
            this.mPreviousVisibleItemCount = this.mCurrentVisibleItemCount;
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            this.mCurrentScrollState = scrollState;
            ReorderableListView.this.mScrollState = scrollState;
            isScrollCompleted();
        }

        private void isScrollCompleted() {
            if (this.mCurrentVisibleItemCount > 0 && this.mCurrentScrollState == 0) {
                if (ReorderableListView.this.mCellIsMobile && ReorderableListView.this.mIsMobileScrolling) {
                    ReorderableListView.this.handleMobileCellScroll();
                } else if (ReorderableListView.this.mIsWaitingForScrollFinish) {
                    ReorderableListView.this.touchEventsEnded();
                }
            }
        }

        public void checkAndHandleFirstVisibleCellChange() {
            if (this.mCurrentFirstVisibleItem != this.mPreviousFirstVisibleItem && ReorderableListView.this.mCellIsMobile && ReorderableListView.this.mMobileItemId != -1) {
                ReorderableListView.this.updateNeighborViewsForID(ReorderableListView.this.mMobileItemId);
                ReorderableListView.this.handleCellSwitch();
            }
        }

        public void checkAndHandleLastVisibleCellChange() {
            if (this.mCurrentFirstVisibleItem + this.mCurrentVisibleItemCount != this.mPreviousFirstVisibleItem + this.mPreviousVisibleItemCount && ReorderableListView.this.mCellIsMobile && ReorderableListView.this.mMobileItemId != -1) {
                ReorderableListView.this.updateNeighborViewsForID(ReorderableListView.this.mMobileItemId);
                ReorderableListView.this.handleCellSwitch();
            }
        }
    }

    /* renamed from: com.vkontakte.android.ui.ReorderableListView.4 */
    class C10554 implements OnPreDrawListener {
        private final /* synthetic */ int val$deltaY;
        private final /* synthetic */ ViewTreeObserver val$observer;
        private final /* synthetic */ long val$switchItemID;
        private final /* synthetic */ int val$switchViewStartTop;

        C10554(ViewTreeObserver viewTreeObserver, long j, int i, int i2) {
            this.val$observer = viewTreeObserver;
            this.val$switchItemID = j;
            this.val$deltaY = i;
            this.val$switchViewStartTop = i2;
        }

        public boolean onPreDraw() {
            this.val$observer.removeOnPreDrawListener(this);
            View switchView = ReorderableListView.this.getViewForID(this.val$switchItemID);
            ReorderableListView reorderableListView = ReorderableListView.this;
            reorderableListView.mTotalOffset = reorderableListView.mTotalOffset + this.val$deltaY;
            switchView.setTranslationY((float) (this.val$switchViewStartTop - switchView.getTop()));
            ObjectAnimator animator = ObjectAnimator.ofFloat(switchView, View.TRANSLATION_Y, new float[]{0.0f});
            animator.setDuration(150);
            animator.start();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.ui.ReorderableListView.5 */
    class C10565 implements AnimatorUpdateListener {
        C10565() {
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            ReorderableListView.this.invalidate();
        }
    }

    /* renamed from: com.vkontakte.android.ui.ReorderableListView.6 */
    class C10576 extends AnimatorListenerAdapter {
        private final /* synthetic */ View val$mobileView;

        C10576(View view) {
            this.val$mobileView = view;
        }

        public void onAnimationStart(Animator animation) {
            ReorderableListView.this.setEnabled(false);
        }

        public void onAnimationEnd(Animator animation) {
            ReorderableListView.this.mAboveItemId = -1;
            ReorderableListView.this.mMobileItemId = -1;
            ReorderableListView.this.mBelowItemId = -1;
            this.val$mobileView.setVisibility(0);
            ReorderableListView.this.mHoverCell = null;
            ReorderableListView.this.setEnabled(true);
            ReorderableListView.this.invalidate();
        }
    }

    public interface DragListener {
        void onDragDrop(long j);

        void onDragStart(long j);
    }

    public interface DraggableChecker {
        boolean canDragItem(int i);

        boolean canDragToPosition(int i);
    }

    public interface Swappable {
        void swapItems(int i, int i2);
    }

    public ReorderableListView(Context context) {
        super(context);
        this.SMOOTH_SCROLL_AMOUNT_AT_EDGE = 15;
        this.MOVE_DURATION = AnimationDuration.SELECTION_EXIT_FADE;
        this.mLastEventY = -1;
        this.mDownY = -1;
        this.mDownX = -1;
        this.mTotalOffset = 0;
        this.mCellIsMobile = false;
        this.mIsMobileScrolling = false;
        this.mSmoothScrollAmountAtEdge = 0;
        this.INVALID_ID = -1;
        this.mAboveItemId = -1;
        this.mMobileItemId = -1;
        this.mBelowItemId = -1;
        this.INVALID_POINTER_ID = -1;
        this.mActivePointerId = -1;
        this.mIsWaitingForScrollFinish = false;
        this.mScrollState = 0;
        this.mOnItemLongClickListener = new C10521();
        this.mScrollListener = new C10543();
        init(context);
    }

    public ReorderableListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.SMOOTH_SCROLL_AMOUNT_AT_EDGE = 15;
        this.MOVE_DURATION = AnimationDuration.SELECTION_EXIT_FADE;
        this.mLastEventY = -1;
        this.mDownY = -1;
        this.mDownX = -1;
        this.mTotalOffset = 0;
        this.mCellIsMobile = false;
        this.mIsMobileScrolling = false;
        this.mSmoothScrollAmountAtEdge = 0;
        this.INVALID_ID = -1;
        this.mAboveItemId = -1;
        this.mMobileItemId = -1;
        this.mBelowItemId = -1;
        this.INVALID_POINTER_ID = -1;
        this.mActivePointerId = -1;
        this.mIsWaitingForScrollFinish = false;
        this.mScrollState = 0;
        this.mOnItemLongClickListener = new C10521();
        this.mScrollListener = new C10543();
        init(context);
    }

    public ReorderableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.SMOOTH_SCROLL_AMOUNT_AT_EDGE = 15;
        this.MOVE_DURATION = AnimationDuration.SELECTION_EXIT_FADE;
        this.mLastEventY = -1;
        this.mDownY = -1;
        this.mDownX = -1;
        this.mTotalOffset = 0;
        this.mCellIsMobile = false;
        this.mIsMobileScrolling = false;
        this.mSmoothScrollAmountAtEdge = 0;
        this.INVALID_ID = -1;
        this.mAboveItemId = -1;
        this.mMobileItemId = -1;
        this.mBelowItemId = -1;
        this.INVALID_POINTER_ID = -1;
        this.mActivePointerId = -1;
        this.mIsWaitingForScrollFinish = false;
        this.mScrollState = 0;
        this.mOnItemLongClickListener = new C10521();
        this.mScrollListener = new C10543();
        init(context);
    }

    public void init(Context context) {
        setOnItemLongClickListener(this.mOnItemLongClickListener);
        setOnScrollListener(this.mScrollListener);
        this.mSmoothScrollAmountAtEdge = (int) (15.0f / context.getResources().getDisplayMetrics().density);
    }

    public void setDragListener(DragListener dl) {
        this.dragListener = dl;
    }

    private BitmapDrawable getAndAddHoverView(View v) {
        int w = v.getWidth();
        int h = v.getHeight();
        int top = v.getTop();
        int left = v.getLeft();
        BitmapDrawable drawable = new BitmapDrawable(getResources(), getBitmapWithBorder(v));
        drawable.setAlpha(180);
        this.mHoverCellOriginalBounds = new Rect(left, top, left + w, top + h);
        this.mHoverCellCurrentBounds = new Rect(this.mHoverCellOriginalBounds);
        drawable.setBounds(this.mHoverCellCurrentBounds);
        return drawable;
    }

    private Bitmap getBitmapWithBorder(View v) {
        return getBitmapFromView(v);
    }

    private Bitmap getBitmapFromView(View v) {
        Bitmap bitmap = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        if (v.getBackground() == null) {
            Paint bgPaint = new Paint();
            bgPaint.setColor(-1);
            canvas.drawRect(new Rect(0, 0, v.getWidth(), v.getHeight()), bgPaint);
        }
        v.draw(canvas);
        return bitmap;
    }

    private void updateNeighborViewsForID(long itemID) {
        long j = -1;
        int position = getPositionForID(itemID);
        this.mAboveItemId = position <= 0 ? -1 : getAdapter().getItemId(position - 1);
        if (position < getAdapter().getCount() - 1) {
            j = getAdapter().getItemId(position + 1);
        }
        this.mBelowItemId = j;
    }

    public View getViewForID(long itemID) {
        int firstVisiblePosition = getFirstVisiblePosition();
        for (int i = 0; i < getChildCount(); i++) {
            View v = getChildAt(i);
            if (getAdapter().getItemId(firstVisiblePosition + i) == itemID) {
                return v;
            }
        }
        return null;
    }

    public int getPositionForID(long itemID) {
        View v = getViewForID(itemID);
        if (v == null) {
            return -1;
        }
        return getPositionForView(v);
    }

    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.mHoverCell != null) {
            this.mHoverCell.draw(canvas);
        }
    }

    private void lockOrientation() {
        this.prevOrientation = ((Activity) getContext()).getRequestedOrientation();
        ((Activity) getContext()).setRequestedOrientation(ViewUtils.getScreenOrientation((Activity) getContext()));
    }

    private void unlockOrientation() {
        ((Activity) getContext()).setRequestedOrientation(this.prevOrientation);
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEventCompat.ACTION_MASK) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                this.mDownX = (int) event.getX();
                this.mDownY = (int) event.getY();
                this.mActivePointerId = event.getPointerId(0);
                lockOrientation();
                break;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                touchEventsEnded();
                break;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                if (this.mActivePointerId != -1) {
                    this.mLastEventY = (int) event.getY(event.findPointerIndex(this.mActivePointerId));
                    int deltaY = this.mLastEventY - this.mDownY;
                    if (this.mCellIsMobile) {
                        this.mHoverCellCurrentBounds.offsetTo(this.mHoverCellOriginalBounds.left, (this.mHoverCellOriginalBounds.top + deltaY) + this.mTotalOffset);
                        this.mHoverCell.setBounds(this.mHoverCellCurrentBounds);
                        invalidate();
                        handleCellSwitch();
                        this.mIsMobileScrolling = false;
                        handleMobileCellScroll();
                        return false;
                    }
                }
                break;
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                touchEventsCancelled();
                break;
            case UserListView.TYPE_POLL_VOTERS /*6*/:
                if (event.getPointerId((event.getAction() & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8) == this.mActivePointerId) {
                    touchEventsEnded();
                    break;
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    private void handleCellSwitch() {
        int deltaY = this.mLastEventY - this.mDownY;
        int deltaYTotal = (this.mHoverCellOriginalBounds.top + this.mTotalOffset) + deltaY;
        View belowView = getViewForID(this.mBelowItemId);
        View mobileView = getViewForID(this.mMobileItemId);
        View aboveView = getViewForID(this.mAboveItemId);
        boolean isBelow = belowView != null && deltaYTotal > belowView.getTop();
        boolean isAbove = aboveView != null && deltaYTotal < aboveView.getTop();
        if (isBelow || isAbove) {
            View switchView;
            long switchItemID = isBelow ? this.mBelowItemId : this.mAboveItemId;
            if (isBelow) {
                switchView = belowView;
            } else {
                switchView = aboveView;
            }
            int originalItem = getPositionForView(mobileView);
            if ((getAdapter() instanceof DraggableChecker) && !((DraggableChecker) getAdapter()).canDragToPosition(getPositionForView(switchView))) {
                return;
            }
            if (switchView == null) {
                updateNeighborViewsForID(this.mMobileItemId);
                return;
            }
            ((Swappable) getAdapter()).swapItems(originalItem, getPositionForView(switchView));
            ((BaseAdapter) getAdapter()).notifyDataSetChanged();
            this.mDownY = this.mLastEventY;
            int switchViewStartTop = switchView.getTop();
            mobileView.setVisibility(0);
            switchView.setVisibility(4);
            updateNeighborViewsForID(this.mMobileItemId);
            ViewTreeObserver observer = getViewTreeObserver();
            observer.addOnPreDrawListener(new C10554(observer, switchItemID, deltaY, switchViewStartTop));
        }
    }

    private void touchEventsEnded() {
        View mobileView = getViewForID(this.mMobileItemId);
        if (this.mCellIsMobile || this.mIsWaitingForScrollFinish) {
            this.mCellIsMobile = false;
            this.mIsWaitingForScrollFinish = false;
            this.mIsMobileScrolling = false;
            this.mActivePointerId = -1;
            if (this.mScrollState != 0) {
                this.mIsWaitingForScrollFinish = true;
                return;
            }
            this.mHoverCellCurrentBounds.offsetTo(this.mHoverCellOriginalBounds.left, mobileView.getTop());
            ObjectAnimator hoverViewAnimator = ObjectAnimator.ofObject(this.mHoverCell, "bounds", sBoundEvaluator, new Object[]{this.mHoverCellCurrentBounds});
            hoverViewAnimator.addUpdateListener(new C10565());
            hoverViewAnimator.addListener(new C10576(mobileView));
            hoverViewAnimator.start();
            unlockOrientation();
            if (this.dragListener != null) {
                this.dragListener.onDragDrop(this.mMobileItemId);
                return;
            }
            return;
        }
        touchEventsCancelled();
    }

    private void touchEventsCancelled() {
        View mobileView = getViewForID(this.mMobileItemId);
        if (this.mCellIsMobile) {
            this.mAboveItemId = -1;
            this.mMobileItemId = -1;
            this.mBelowItemId = -1;
            mobileView.setVisibility(0);
            this.mHoverCell = null;
            invalidate();
        }
        this.mCellIsMobile = false;
        this.mIsMobileScrolling = false;
        this.mActivePointerId = -1;
        unlockOrientation();
    }

    static {
        sBoundEvaluator = new C10532();
    }

    private void handleMobileCellScroll() {
        this.mIsMobileScrolling = handleMobileCellScroll(this.mHoverCellCurrentBounds);
    }

    public boolean handleMobileCellScroll(Rect r) {
        int offset = computeVerticalScrollOffset();
        int height = getHeight();
        int extent = computeVerticalScrollExtent();
        int range = computeVerticalScrollRange();
        int hoverViewTop = r.top;
        int hoverHeight = r.height();
        if (hoverViewTop <= 0 && offset > 0) {
            smoothScrollBy(-this.mSmoothScrollAmountAtEdge, 0);
            return true;
        } else if (hoverViewTop + hoverHeight < height || offset + extent >= range) {
            return false;
        } else {
            smoothScrollBy(this.mSmoothScrollAmountAtEdge, 0);
            return true;
        }
    }
}
