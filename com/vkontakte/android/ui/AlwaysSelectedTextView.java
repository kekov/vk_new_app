package com.vkontakte.android.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class AlwaysSelectedTextView extends TextView {
    public AlwaysSelectedTextView(Context context) {
        super(context);
    }

    public AlwaysSelectedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AlwaysSelectedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public boolean isSelected() {
        return true;
    }
}
