package com.vkontakte.android.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class CorrectlyMeasuringTextView extends TextView {
    public CorrectlyMeasuringTextView(Context context) {
        super(context);
    }

    public CorrectlyMeasuringTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CorrectlyMeasuringTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r23, int r24) {
        /*
        r22 = this;
        super.onMeasure(r23, r24);
        r8 = r22.getLayout();
        r19 = r8.getLineCount();
        r20 = 1;
        r0 = r19;
        r1 = r20;
        if (r0 > r1) goto L_0x0014;
    L_0x0013:
        return;
    L_0x0014:
        r10 = 0;
        r17 = r22.getText();
        r5 = 0;
    L_0x001a:
        r19 = r8.getLineCount();
        r0 = r19;
        if (r5 < r0) goto L_0x003c;
    L_0x0022:
        r19 = r22.getPaddingLeft();
        r19 = r19 + r10;
        r20 = r22.getPaddingRight();
        r19 = r19 + r20;
        r20 = r22.getMeasuredHeight();
        r0 = r22;
        r1 = r19;
        r2 = r20;
        r0.setMeasuredDimension(r1, r2);
        goto L_0x0013;
    L_0x003c:
        r3 = 0;
        r19 = r8.getLineStart(r5);
        r20 = r8.getLineEnd(r5);
        r0 = r17;
        r1 = r19;
        r2 = r20;
        r9 = r0.subSequence(r1, r2);
        r0 = r9 instanceof android.text.Spanned;
        r19 = r0;
        if (r19 == 0) goto L_0x0099;
    L_0x0055:
        r12 = r9;
        r12 = (android.text.Spanned) r12;
        r19 = 0;
        r20 = r9.length();
        r21 = com.vkontakte.android.ui.XImageSpan.class;
        r0 = r19;
        r1 = r20;
        r2 = r21;
        r14 = r12.getSpans(r0, r1, r2);
        if (r14 == 0) goto L_0x0099;
    L_0x006c:
        r0 = r14.length;
        r19 = r0;
        if (r19 <= 0) goto L_0x0099;
    L_0x0071:
        r3 = r14.length;
        r13 = new java.util.ArrayList;
        r13.<init>();
        r4 = new java.util.ArrayList;
        r4.<init>();
        r0 = r14.length;
        r20 = r0;
        r19 = 0;
    L_0x0081:
        r0 = r19;
        r1 = r20;
        if (r0 < r1) goto L_0x00ca;
    L_0x0087:
        r6 = 0;
        r16 = "";
        r15 = r12.toString();
        r7 = 0;
    L_0x008f:
        r19 = r15.length();
        r0 = r19;
        if (r7 < r0) goto L_0x00e9;
    L_0x0097:
        r9 = r16;
    L_0x0099:
        r19 = r8.getPaint();
        r20 = 0;
        r21 = r9.length();
        r0 = r19;
        r1 = r20;
        r2 = r21;
        r19 = r0.measureText(r9, r1, r2);
        r20 = 1101004800; // 0x41a00000 float:20.0 double:5.439686476E-315;
        r20 = com.vkontakte.android.Global.scale(r20);
        r20 = r20 * r3;
        r0 = r20;
        r0 = (float) r0;
        r20 = r0;
        r19 = r19 + r20;
        r18 = java.lang.Math.round(r19);
        r0 = r18;
        r10 = java.lang.Math.max(r10, r0);
        r5 = r5 + 1;
        goto L_0x001a;
    L_0x00ca:
        r11 = r14[r19];
        r21 = r12.getSpanStart(r11);
        r21 = java.lang.Integer.valueOf(r21);
        r0 = r21;
        r13.add(r0);
        r21 = r12.getSpanEnd(r11);
        r21 = java.lang.Integer.valueOf(r21);
        r0 = r21;
        r4.add(r0);
        r19 = r19 + 1;
        goto L_0x0081;
    L_0x00e9:
        r19 = java.lang.Integer.valueOf(r7);
        r0 = r19;
        r19 = r13.contains(r0);
        if (r19 != 0) goto L_0x011d;
    L_0x00f5:
        if (r6 == 0) goto L_0x0103;
    L_0x00f7:
        r19 = java.lang.Integer.valueOf(r7);
        r0 = r19;
        r19 = r4.contains(r0);
        if (r19 == 0) goto L_0x011d;
    L_0x0103:
        r19 = new java.lang.StringBuilder;
        r20 = java.lang.String.valueOf(r16);
        r19.<init>(r20);
        r20 = r15.charAt(r7);
        r19 = r19.append(r20);
        r16 = r19.toString();
        r6 = 0;
    L_0x0119:
        r7 = r7 + 1;
        goto L_0x008f;
    L_0x011d:
        r6 = 1;
        goto L_0x0119;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.ui.CorrectlyMeasuringTextView.onMeasure(int, int):void");
    }
}
