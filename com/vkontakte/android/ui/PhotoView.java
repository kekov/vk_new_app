package com.vkontakte.android.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Scroller;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageCache.ProgressCallback;
import com.vkontakte.android.Log;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.Future;

public class PhotoView extends View implements OnGestureListener, OnDoubleTapListener {
    public static final int PAGE_SPACING;
    private static final String TAG = "vk_photoview";
    public static final int THUMB_ANIM_DURATION = 300;
    private PhotoViewerAdapter adapter;
    private long animDuration;
    private float animScale;
    private long animStartTime;
    private float animTx;
    private float animTy;
    private float bgAlpha;
    private Paint bgPaint;
    private Paint bitmapPaint;
    private OnClickListener clickListener;
    private float density;
    private GestureDetector detector;
    private boolean disallowZoom;
    private DismissListener dismissListener;
    private boolean dismissed;
    private boolean dismissing;
    private boolean doubleTap;
    private int dragStartPointerID;
    private float dragStartTx;
    private float dragStartTy;
    private float dragStartX;
    private float dragStartY;
    private boolean dragging;
    private boolean dropTouches;
    private ErrorView error;
    private boolean firstResume;
    private boolean fitBySmallestSide;
    private boolean inAnimDone;
    private boolean inited;
    private boolean isList;
    private Interpolator itrp;
    private int limitX;
    private int limitY;
    private int listPosition;
    private Vector<RunnableFuture<Bitmap>> loadingBitmaps;
    private float maxScale;
    private int maxX;
    private int maxY;
    private float minScale;
    private int minX;
    private int minY;
    private NavigationListener navListener;
    private boolean outAnimDone;
    private FrameLayout overlay;
    private float pinchCenterX;
    private float pinchCenterY;
    private long pinchEndTime;
    private float pinchStartDist;
    private float pinchStartScale;
    private Runnable postedPhotoSwitch;
    private int prevCallbackPos;
    private boolean prevSwitchFwd;
    private CircularProgressBar progressBar;
    private boolean resetOnResize;
    private float scale;
    private float scaleNext;
    private float scalePrev;
    private float scaleStartTx;
    private float scaleStartTy;
    private Scroller scroller;
    private boolean secondIsNext;
    private Runnable showProgress;
    private Texture texture;
    private Texture texture2;
    private Texture textureNext;
    private Texture texturePrev;
    private Rect thumbRect;
    private ClippingImageView thumbView;
    private float touchslop;
    private float translateX;
    private float translateY;
    private float viewportH;
    private float viewportW;
    private ColorDrawable windowBg;
    private ZoomListener zoomListener;

    /* renamed from: com.vkontakte.android.ui.PhotoView.1 */
    class C10301 implements OnClickListener {
        C10301() {
        }

        public void onClick(View v) {
            PhotoView.this.error.setVisibility(8);
            PhotoView.this.progressBar.setVisibility(PhotoView.PAGE_SPACING);
            PhotoView.this.load(PhotoView.this.adapter.shouldPreload() ? new int[]{PhotoView.this.listPosition, PhotoView.this.listPosition + 1} : new int[]{PhotoView.this.listPosition}, new Texture[]{PhotoView.this.texture, PhotoView.this.textureNext}, new int[]{PhotoView.this.listPosition, PhotoView.this.listPosition + 1});
        }
    }

    /* renamed from: com.vkontakte.android.ui.PhotoView.2 */
    class C10312 extends FrameLayout {
        C10312(Context $anonymous0) {
            super($anonymous0);
        }

        protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
            if ((child == PhotoView.this.progressBar || child == PhotoView.this.error) && (!PhotoView.this.inAnimDone || !PhotoView.this.outAnimDone)) {
                return false;
            }
            canvas.save();
            if (PhotoView.this.inAnimDone && PhotoView.this.outAnimDone) {
                canvas.translate(PhotoView.this.translateX, PhotoView.this.translateY);
                if (System.currentTimeMillis() - PhotoView.this.animStartTime < PhotoView.this.animDuration) {
                    float ai = PhotoView.this.itrp.getInterpolation(((float) (System.currentTimeMillis() - PhotoView.this.animStartTime)) / ((float) PhotoView.this.animDuration));
                    canvas.translate((PhotoView.this.animTx - PhotoView.this.translateX) * ai, (PhotoView.this.animTy - PhotoView.this.translateY) * ai);
                    invalidate();
                }
            }
            boolean result = super.drawChild(canvas, child, drawingTime);
            canvas.restore();
            return result;
        }

        public void draw(Canvas c) {
            if (PhotoView.this.thumbRect != null) {
                int[] f = new int[2];
                getLocationOnScreen(f);
                c.translate(0.0f, (float) (-f[1]));
                c.drawRect(PhotoView.this.thumbRect, PhotoView.this.bgPaint);
                c.translate(0.0f, (float) f[1]);
            }
            super.draw(c);
        }

        public void onLayout(boolean changed, int l, int t, int r, int b) {
            super.onLayout(changed, l, t, r, b);
            Drawable d = PhotoView.this.thumbView.getDrawable();
            if (d != null) {
                int w = r - l;
                int h = b - t;
                float scale;
                if (((float) w) / ((float) h) < ((float) d.getIntrinsicWidth()) / ((float) d.getIntrinsicHeight())) {
                    scale = ((float) w) / ((float) d.getIntrinsicWidth());
                    PhotoView.this.thumbView.layout(PhotoView.PAGE_SPACING, Math.round(((float) (h / 2)) - ((((float) d.getIntrinsicHeight()) * scale) / ImageViewHolder.PaddingSize)), w, Math.round(((float) (h / 2)) + ((((float) d.getIntrinsicHeight()) * scale) / ImageViewHolder.PaddingSize)));
                    return;
                }
                scale = ((float) h) / ((float) d.getIntrinsicHeight());
                PhotoView.this.thumbView.layout(Math.round(((float) (w / 2)) - ((((float) d.getIntrinsicWidth()) * scale) / ImageViewHolder.PaddingSize)), PhotoView.PAGE_SPACING, Math.round(((float) (w / 2)) + ((((float) d.getIntrinsicWidth()) * scale) / ImageViewHolder.PaddingSize)), h);
            }
        }
    }

    /* renamed from: com.vkontakte.android.ui.PhotoView.3 */
    class C10333 implements OnPreDrawListener {
        private final /* synthetic */ int val$clipSize;
        private final /* synthetic */ Rect val$rect;

        /* renamed from: com.vkontakte.android.ui.PhotoView.3.1 */
        class C10321 extends AnimatorListenerAdapter {
            C10321() {
            }

            public void onAnimationEnd(Animator a) {
                PhotoView.this.overlay.setBackgroundDrawable(null);
                PhotoView.this.inAnimDone = true;
                PhotoView.this.invalidate();
            }
        }

        C10333(Rect rect, int i) {
            this.val$rect = rect;
            this.val$clipSize = i;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onPreDraw() {
            /*
            r21 = this;
            r0 = r21;
            r14 = com.vkontakte.android.ui.PhotoView.this;
            r14 = r14.thumbView;
            r14 = r14.getViewTreeObserver();
            r0 = r21;
            r14.removeOnPreDrawListener(r0);
            r14 = 2;
            r4 = new int[r14];
            r0 = r21;
            r14 = com.vkontakte.android.ui.PhotoView.this;
            r14 = r14.thumbView;
            r14.getLocationOnScreen(r4);
            r0 = r21;
            r14 = r0.val$rect;
            r14 = r14.width();
            r14 = (float) r14;
            r0 = r21;
            r15 = com.vkontakte.android.ui.PhotoView.this;
            r15 = r15.thumbView;
            r15 = r15.getWidth();
            r15 = (float) r15;
            r6 = r14 / r15;
            r0 = r21;
            r14 = r0.val$rect;
            r14 = r14.height();
            r14 = (float) r14;
            r0 = r21;
            r15 = com.vkontakte.android.ui.PhotoView.this;
            r15 = r15.thumbView;
            r15 = r15.getHeight();
            r15 = (float) r15;
            r7 = r14 / r15;
            r0 = r21;
            r14 = r0.val$rect;
            r14 = r14.left;
            r15 = 0;
            r15 = r4[r15];
            r14 = r14 - r15;
            r11 = (float) r14;
            r0 = r21;
            r14 = r0.val$rect;
            r14 = r14.top;
            r15 = 1;
            r15 = r4[r15];
            r14 = r14 - r15;
            r12 = (float) r14;
            r5 = java.lang.Math.max(r6, r7);
            r0 = r21;
            r14 = r0.val$rect;
            r14 = r14.width();
            r14 = (float) r14;
            r0 = r21;
            r15 = r0.val$rect;
            r15 = r15.height();
            r15 = (float) r15;
            r9 = r14 / r15;
            r0 = r21;
            r14 = com.vkontakte.android.ui.PhotoView.this;
            r14 = r14.thumbView;
            r14 = r14.getWidth();
            r14 = (float) r14;
            r0 = r21;
            r15 = com.vkontakte.android.ui.PhotoView.this;
            r15 = r15.thumbView;
            r15 = r15.getHeight();
            r15 = (float) r15;
            r3 = r14 / r15;
            r2 = 0;
            r1 = 0;
            r14 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1));
            if (r14 == 0) goto L_0x00d4;
        L_0x009f:
            r14 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1));
            if (r14 <= 0) goto L_0x0266;
        L_0x00a3:
            r0 = r21;
            r14 = com.vkontakte.android.ui.PhotoView.this;
            r14 = r14.thumbView;
            r14 = r14.getHeight();
            r14 = (float) r14;
            r10 = r14 * r5;
            r0 = r21;
            r14 = r0.val$rect;
            r14 = r14.height();
            r14 = (float) r14;
            r14 = r10 - r14;
            r15 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
            r14 = r14 / r15;
            r14 = r14 / r5;
            r2 = java.lang.Math.round(r14);
            r0 = r21;
            r14 = r0.val$rect;
            r14 = r14.height();
            r14 = (float) r14;
            r14 = r10 - r14;
            r15 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
            r14 = r14 / r15;
            r12 = r12 - r14;
        L_0x00d4:
            r14 = 0;
            r2 = java.lang.Math.max(r14, r2);
            r14 = 0;
            r1 = java.lang.Math.max(r14, r1);
            r0 = r21;
            r14 = com.vkontakte.android.ui.PhotoView.this;
            r14 = r14.thumbView;
            r15 = 0;
            r14.setPivotX(r15);
            r0 = r21;
            r14 = com.vkontakte.android.ui.PhotoView.this;
            r14 = r14.thumbView;
            r15 = 0;
            r14.setPivotY(r15);
            r0 = r21;
            r14 = com.vkontakte.android.ui.PhotoView.this;
            r14 = r14.thumbView;
            r14.setScaleX(r5);
            r0 = r21;
            r14 = com.vkontakte.android.ui.PhotoView.this;
            r14 = r14.thumbView;
            r14.setScaleY(r5);
            r0 = r21;
            r14 = com.vkontakte.android.ui.PhotoView.this;
            r14 = r14.thumbView;
            r14.setTranslationX(r11);
            r0 = r21;
            r14 = com.vkontakte.android.ui.PhotoView.this;
            r14 = r14.thumbView;
            r14.setTranslationY(r12);
            r8 = new android.animation.AnimatorSet;
            r8.<init>();
            r14 = 8;
            r14 = new android.animation.Animator[r14];
            r15 = 0;
            r0 = r21;
            r0 = com.vkontakte.android.ui.PhotoView.this;
            r16 = r0;
            r16 = r16.thumbView;
            r17 = "scaleX";
            r18 = 1;
            r0 = r18;
            r0 = new float[r0];
            r18 = r0;
            r19 = 0;
            r20 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
            r18[r19] = r20;
            r16 = android.animation.ObjectAnimator.ofFloat(r16, r17, r18);
            r14[r15] = r16;
            r15 = 1;
            r0 = r21;
            r0 = com.vkontakte.android.ui.PhotoView.this;
            r16 = r0;
            r16 = r16.thumbView;
            r17 = "scaleY";
            r18 = 1;
            r0 = r18;
            r0 = new float[r0];
            r18 = r0;
            r19 = 0;
            r20 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
            r18[r19] = r20;
            r16 = android.animation.ObjectAnimator.ofFloat(r16, r17, r18);
            r14[r15] = r16;
            r15 = 2;
            r0 = r21;
            r0 = com.vkontakte.android.ui.PhotoView.this;
            r16 = r0;
            r16 = r16.thumbView;
            r17 = "translationX";
            r18 = 1;
            r0 = r18;
            r0 = new float[r0];
            r18 = r0;
            r19 = 0;
            r20 = 0;
            r18[r19] = r20;
            r16 = android.animation.ObjectAnimator.ofFloat(r16, r17, r18);
            r14[r15] = r16;
            r15 = 3;
            r0 = r21;
            r0 = com.vkontakte.android.ui.PhotoView.this;
            r16 = r0;
            r16 = r16.thumbView;
            r17 = "translationY";
            r18 = 1;
            r0 = r18;
            r0 = new float[r0];
            r18 = r0;
            r19 = 0;
            r20 = 0;
            r18[r19] = r20;
            r16 = android.animation.ObjectAnimator.ofFloat(r16, r17, r18);
            r14[r15] = r16;
            r15 = 4;
            r0 = r21;
            r0 = com.vkontakte.android.ui.PhotoView.this;
            r16 = r0;
            r16 = r16.windowBg;
            r17 = "alpha";
            r18 = 2;
            r0 = r18;
            r0 = new int[r0];
            r18 = r0;
            r18 = {0, 255};
            r16 = android.animation.ObjectAnimator.ofInt(r16, r17, r18);
            r14[r15] = r16;
            r15 = 5;
            r0 = r21;
            r0 = com.vkontakte.android.ui.PhotoView.this;
            r16 = r0;
            r16 = r16.thumbView;
            r17 = "clipBottom";
            r18 = 2;
            r0 = r18;
            r0 = new int[r0];
            r18 = r0;
            r19 = 0;
            r18[r19] = r2;
            r19 = 1;
            r20 = 0;
            r18[r19] = r20;
            r16 = android.animation.ObjectAnimator.ofInt(r16, r17, r18);
            r14[r15] = r16;
            r15 = 6;
            r0 = r21;
            r0 = com.vkontakte.android.ui.PhotoView.this;
            r16 = r0;
            r16 = r16.thumbView;
            r17 = "clipTop";
            r18 = 2;
            r0 = r18;
            r0 = new int[r0];
            r18 = r0;
            r19 = 0;
            r0 = r21;
            r0 = r0.val$clipSize;
            r20 = r0;
            r0 = r20;
            r0 = (float) r0;
            r20 = r0;
            r20 = r20 / r5;
            r20 = java.lang.Math.round(r20);
            r20 = r20 + r2;
            r18[r19] = r20;
            r19 = 1;
            r20 = 0;
            r18[r19] = r20;
            r16 = android.animation.ObjectAnimator.ofInt(r16, r17, r18);
            r14[r15] = r16;
            r15 = 7;
            r0 = r21;
            r0 = com.vkontakte.android.ui.PhotoView.this;
            r16 = r0;
            r16 = r16.thumbView;
            r17 = "clipHorizontal";
            r18 = 2;
            r0 = r18;
            r0 = new int[r0];
            r18 = r0;
            r19 = 0;
            r18[r19] = r1;
            r19 = 1;
            r20 = 0;
            r18[r19] = r20;
            r16 = android.animation.ObjectAnimator.ofInt(r16, r17, r18);
            r14[r15] = r16;
            r8.playTogether(r14);
            r14 = 300; // 0x12c float:4.2E-43 double:1.48E-321;
            r8.setDuration(r14);
            r14 = new com.vkontakte.android.ui.PhotoView$3$1;
            r0 = r21;
            r14.<init>();
            r8.addListener(r14);
            r8.start();
            r14 = 1;
            return r14;
        L_0x0266:
            r0 = r21;
            r14 = com.vkontakte.android.ui.PhotoView.this;
            r14 = r14.thumbView;
            r14 = r14.getWidth();
            r14 = (float) r14;
            r13 = r14 * r5;
            r0 = r21;
            r14 = r0.val$rect;
            r14 = r14.width();
            r14 = (float) r14;
            r14 = r13 - r14;
            r15 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
            r14 = r14 / r15;
            r14 = r14 / r5;
            r1 = java.lang.Math.round(r14);
            r0 = r21;
            r14 = r0.val$rect;
            r14 = r14.width();
            r14 = (float) r14;
            r14 = r13 - r14;
            r15 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
            r14 = r14 / r15;
            r11 = r11 - r14;
            goto L_0x00d4;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.ui.PhotoView.3.onPreDraw():boolean");
        }
    }

    /* renamed from: com.vkontakte.android.ui.PhotoView.4 */
    class C10364 implements OnPreDrawListener {
        private final /* synthetic */ Runnable val$after;
        private final /* synthetic */ int val$clipSize;
        private final /* synthetic */ Rect val$rect;

        /* renamed from: com.vkontakte.android.ui.PhotoView.4.1 */
        class C10351 extends AnimatorListenerAdapter {
            private final /* synthetic */ Runnable val$after;

            /* renamed from: com.vkontakte.android.ui.PhotoView.4.1.1 */
            class C10341 implements Runnable {
                private final /* synthetic */ Runnable val$after;

                C10341(Runnable runnable) {
                    this.val$after = runnable;
                }

                public void run() {
                    PhotoView.this.overlay.setBackgroundDrawable(null);
                    PhotoView.this.thumbView.setVisibility(8);
                    PhotoView.this.inAnimDone = true;
                    PhotoView.this.invalidate();
                    if (this.val$after != null) {
                        this.val$after.run();
                    }
                }
            }

            C10351(Runnable runnable) {
                this.val$after = runnable;
            }

            public void onAnimationEnd(Animator a) {
                PhotoView.this.post(new C10341(this.val$after));
            }
        }

        C10364(Rect rect, int i, Runnable runnable) {
            this.val$rect = rect;
            this.val$clipSize = i;
            this.val$after = runnable;
        }

        public boolean onPreDraw() {
            if (!PhotoView.this.outAnimDone) {
                return true;
            }
            PhotoView.this.thumbView.getViewTreeObserver().removeOnPreDrawListener(this);
            PhotoView.this.overlay.setBackgroundDrawable(PhotoView.this.windowBg);
            PhotoView.this.outAnimDone = false;
            PhotoView.this.invalidate();
            int[] pos = new int[2];
            PhotoView.this.thumbView.getLocationOnScreen(pos);
            float transX = (float) (this.val$rect.left - pos[PhotoView.PAGE_SPACING]);
            float transY = (float) (this.val$rect.top - pos[1]);
            float scale = Math.max(((float) this.val$rect.width()) / ((float) PhotoView.this.thumbView.getWidth()), ((float) this.val$rect.height()) / ((float) PhotoView.this.thumbView.getHeight()));
            float tRatio = ((float) this.val$rect.width()) / ((float) this.val$rect.height());
            float ivRatio = ((float) PhotoView.this.thumbView.getWidth()) / ((float) PhotoView.this.thumbView.getHeight());
            int clipV = PhotoView.PAGE_SPACING;
            int clipH = PhotoView.PAGE_SPACING;
            if (tRatio != ivRatio) {
                if (tRatio > ivRatio) {
                    float th = ((float) PhotoView.this.thumbView.getHeight()) * scale;
                    clipV = Math.round(((th - ((float) this.val$rect.height())) / ImageViewHolder.PaddingSize) / scale);
                    transY -= (th - ((float) this.val$rect.height())) / ImageViewHolder.PaddingSize;
                } else {
                    float tw = ((float) PhotoView.this.thumbView.getWidth()) * scale;
                    clipH = Math.round(((tw - ((float) this.val$rect.width())) / ImageViewHolder.PaddingSize) / scale);
                    transX -= (tw - ((float) this.val$rect.width())) / ImageViewHolder.PaddingSize;
                }
            }
            PhotoView.this.thumbView.setPivotX(0.0f);
            PhotoView.this.thumbView.setPivotY(0.0f);
            PhotoView.this.thumbView.setScaleX(1.0f);
            PhotoView.this.thumbView.setScaleY(1.0f);
            PhotoView.this.thumbView.setTranslationX(0.0f);
            PhotoView.this.thumbView.setTranslationY(PhotoView.this.translateY);
            AnimatorSet set = new AnimatorSet();
            r14 = new Animator[8];
            float[] fArr = new float[]{scale};
            r14[PhotoView.PAGE_SPACING] = ObjectAnimator.ofFloat(PhotoView.this.thumbView, "scaleX", fArr);
            fArr = new float[]{scale};
            r14[1] = ObjectAnimator.ofFloat(PhotoView.this.thumbView, "scaleY", fArr);
            fArr = new float[]{transX};
            r14[2] = ObjectAnimator.ofFloat(PhotoView.this.thumbView, "translationX", fArr);
            fArr = new float[]{transY};
            r14[3] = ObjectAnimator.ofFloat(PhotoView.this.thumbView, "translationY", fArr);
            PhotoView photoView = PhotoView.this;
            r18 = new int[2];
            r18[PhotoView.PAGE_SPACING] = Math.round(PhotoView.this.bgAlpha * 255.0f);
            r18[1] = PhotoView.PAGE_SPACING;
            r14[4] = ObjectAnimator.ofInt(r0.windowBg, "alpha", r18);
            r18 = new int[]{PhotoView.PAGE_SPACING, clipV};
            r14[5] = ObjectAnimator.ofInt(PhotoView.this.thumbView, "clipBottom", r18);
            photoView = PhotoView.this;
            r18 = new int[2];
            r18[PhotoView.PAGE_SPACING] = PhotoView.PAGE_SPACING;
            r18[1] = Math.round(((float) this.val$clipSize) / scale) + clipV;
            r14[6] = ObjectAnimator.ofInt(r0.thumbView, "clipTop", r18);
            r18 = new int[]{PhotoView.PAGE_SPACING, clipH};
            r14[7] = ObjectAnimator.ofInt(PhotoView.this.thumbView, "clipHorizontal", r18);
            set.playTogether(r14);
            set.setDuration(300);
            set.addListener(new C10351(this.val$after));
            set.start();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.ui.PhotoView.5 */
    class C10375 implements Runnable {
        private final /* synthetic */ boolean val$fwd;

        C10375(boolean z) {
            this.val$fwd = z;
        }

        public void run() {
            PhotoView.this.postedPhotoSwitch = null;
            PhotoView photoView = PhotoView.this;
            PhotoView.this.translateY = 0.0f;
            photoView.translateX = 0.0f;
            PhotoView.this.switchPhoto(this.val$fwd);
        }
    }

    /* renamed from: com.vkontakte.android.ui.PhotoView.6 */
    class C10386 implements Runnable {
        private final /* synthetic */ long val$tt;

        C10386(long j) {
            this.val$tt = j;
        }

        public void run() {
            Log.m528i(PhotoView.TAG, "wait before load: " + (System.currentTimeMillis() - this.val$tt));
            if (!PhotoView.this.inited) {
                PhotoView.this.post(this);
            } else if (PhotoView.this.loadingBitmaps.size() <= 0) {
                PhotoView.this.loadPhotos(true);
            }
        }
    }

    /* renamed from: com.vkontakte.android.ui.PhotoView.7 */
    class C10447 implements Runnable {
        private final /* synthetic */ int[] val$positions;
        private final /* synthetic */ Texture[] val$textures;
        private final /* synthetic */ int[] val$thumbPositions;

        /* renamed from: com.vkontakte.android.ui.PhotoView.7.1 */
        class C10401 implements Runnable {
            private final /* synthetic */ int val$pos;
            private final /* synthetic */ Bitmap val$thumb;

            /* renamed from: com.vkontakte.android.ui.PhotoView.7.1.1 */
            class C10391 implements Runnable {
                C10391() {
                }

                public void run() {
                    Global.showViewAnimated(PhotoView.this.progressBar, true, PhotoView.THUMB_ANIM_DURATION);
                    PhotoView.this.showProgress = null;
                }
            }

            C10401(int i, Bitmap bitmap) {
                this.val$pos = i;
                this.val$thumb = bitmap;
            }

            public void run() {
                if (PhotoView.this.adapter.isCached(this.val$pos)) {
                    PhotoView.this.progressBar.setVisibility(8);
                } else {
                    if (PhotoView.this.showProgress != null) {
                        PhotoView.this.removeCallbacks(PhotoView.this.showProgress);
                    }
                    PhotoView.this.showProgress = new C10391();
                }
                PhotoView.this.postDelayed(PhotoView.this.showProgress, 400);
                PhotoView.this.progressBar.setProgress(0.0d);
                if (this.val$thumb != null) {
                    PhotoView.this.thumbView.setImageBitmap(this.val$thumb);
                    PhotoView.this.thumbView.setVisibility(PhotoView.PAGE_SPACING);
                    Log.m528i(PhotoView.TAG, "Set thumb image " + PhotoView.this.listPosition);
                } else {
                    PhotoView.this.thumbView.setImageBitmap(null);
                }
                PhotoView.this.overlay.requestLayout();
            }
        }

        /* renamed from: com.vkontakte.android.ui.PhotoView.7.3 */
        class C10423 implements Runnable {
            C10423() {
            }

            public void run() {
                if (PhotoView.this.showProgress != null) {
                    PhotoView.this.removeCallbacks(PhotoView.this.showProgress);
                }
                PhotoView.this.progressBar.setVisibility(8);
                PhotoView.this.thumbView.setVisibility(PhotoView.PAGE_SPACING);
                Global.showViewAnimated(PhotoView.this.thumbView, false, PhotoView.THUMB_ANIM_DURATION);
            }
        }

        /* renamed from: com.vkontakte.android.ui.PhotoView.7.4 */
        class C10434 implements Runnable {
            C10434() {
            }

            public void run() {
                if (PhotoView.this.showProgress != null) {
                    PhotoView.this.removeCallbacks(PhotoView.this.showProgress);
                }
                PhotoView.this.progressBar.setVisibility(8);
                PhotoView.this.error.setVisibility(PhotoView.PAGE_SPACING);
            }
        }

        /* renamed from: com.vkontakte.android.ui.PhotoView.7.2 */
        class C15562 implements ProgressCallback {
            private final /* synthetic */ int val$pos;

            /* renamed from: com.vkontakte.android.ui.PhotoView.7.2.1 */
            class C10411 implements Runnable {
                private final /* synthetic */ int val$progress;
                private final /* synthetic */ int val$total;

                C10411(int i, int i2) {
                    this.val$progress = i;
                    this.val$total = i2;
                }

                public void run() {
                    PhotoView.this.progressBar.setProgress(((double) this.val$progress) / ((double) this.val$total));
                }
            }

            C15562(int i) {
                this.val$pos = i;
            }

            public void onProgressChanged(int progress, int total) {
                if (this.val$pos == PhotoView.this.listPosition) {
                    ((Activity) PhotoView.this.getContext()).runOnUiThread(new C10411(progress, total));
                }
            }
        }

        C10447(Texture[] textureArr, int[] iArr, int[] iArr2) {
            this.val$textures = textureArr;
            this.val$thumbPositions = iArr;
            this.val$positions = iArr2;
        }

        public void run() {
            int i;
            long tm = System.currentTimeMillis();
            int i2 = PhotoView.PAGE_SPACING;
            while (this.val$textures[PhotoView.PAGE_SPACING] == null) {
                try {
                    Thread.sleep(10);
                } catch (Exception e) {
                }
            }
            Log.m528i(PhotoView.TAG, "Wait for init: " + (System.currentTimeMillis() - tm));
            Iterator it = PhotoView.this.loadingBitmaps.iterator();
            while (it.hasNext()) {
                ((RunnableFuture) it.next()).cancel(false);
            }
            Texture[] textureArr = this.val$textures;
            int length = textureArr.length;
            for (i = PhotoView.PAGE_SPACING; i < length; i++) {
                Texture t = textureArr[i];
                t.loaded = false;
                t.thumb = false;
            }
            PhotoView.this.loadingBitmaps.clear();
            int[] iArr = this.val$thumbPositions;
            length = iArr.length;
            for (i = PhotoView.PAGE_SPACING; i < length; i++) {
                int pos = iArr[i];
                Bitmap thumb = PhotoView.this.adapter.getThumb(pos);
                if (thumb != null) {
                    Log.m528i(PhotoView.TAG, "Set thumb " + pos);
                    this.val$textures[i2].thumb = true;
                    this.val$textures[i2].data = thumb;
                    this.val$textures[i2].cropWidth = thumb.getWidth();
                    this.val$textures[i2].cropHeight = thumb.getHeight();
                    PhotoView.this.updateScale(false);
                    PhotoView.this.postInvalidate();
                }
                i2++;
            }
            i2 = PhotoView.PAGE_SPACING;
            int[] iArr2 = this.val$positions;
            int length2 = iArr2.length;
            for (int i3 = PhotoView.PAGE_SPACING; i3 < length2; i3++) {
                pos = iArr2[i3];
                if (pos >= 0 && pos < PhotoView.this.adapter.getCount()) {
                    try {
                        if (System.currentTimeMillis() - PhotoView.this.animStartTime < PhotoView.this.animDuration) {
                            Thread.sleep(PhotoView.this.animDuration - (System.currentTimeMillis() - PhotoView.this.animStartTime));
                        }
                        Texture tex = this.val$textures[i2];
                        thumb = PhotoView.this.adapter.getThumb(pos);
                        if (pos == PhotoView.this.listPosition) {
                            ((Activity) PhotoView.this.getContext()).runOnUiThread(new C10401(pos, thumb));
                        }
                        tex.data = thumb;
                        tex.thumb = true;
                        RunnableFuture<Bitmap> b = PhotoView.this.adapter.getPhoto(pos);
                        b.setProgressCallback(new C15562(pos));
                        PhotoView.this.loadingBitmaps.add(b);
                        b.run();
                        PhotoView.this.loadingBitmaps.remove(b);
                        if (b.isCancelled()) {
                            Log.m525d(PhotoView.TAG, "Load for " + pos + " canceled");
                            return;
                        }
                        Bitmap bmp = (Bitmap) b.get();
                        if (bmp != null) {
                            tex.cropWidth = bmp.getWidth();
                            tex.cropHeight = bmp.getHeight();
                            boolean z = Global.useBitmapHack;
                            z = Global.useBitmapHack;
                            tex.data = bmp;
                            tex.loaded = true;
                            tex.thumb = false;
                            PhotoView.this.updateScale(pos == PhotoView.this.listPosition);
                            PhotoView.this.updateMinMax(PhotoView.this.scale);
                            if (pos == PhotoView.this.listPosition) {
                                while (!PhotoView.this.inAnimDone) {
                                    Thread.sleep(20);
                                }
                                ((Activity) PhotoView.this.getContext()).runOnUiThread(new C10423());
                            }
                            PhotoView.this.postInvalidate();
                            String str = PhotoView.TAG;
                            StringBuilder append = new StringBuilder("Loaded bitmap ").append(pos).append(" into texture ");
                            String str2 = tex == PhotoView.this.textureNext ? "NEXT" : tex == PhotoView.this.texturePrev ? "PREV" : "CURRENT";
                            Log.m525d(str, append.append(str2).append(" (cur ").append(PhotoView.this.listPosition).append(")").toString());
                            Log.m528i(PhotoView.TAG, "show thumb");
                        } else if (pos == PhotoView.this.listPosition) {
                            ((Activity) PhotoView.this.getContext()).runOnUiThread(new C10434());
                        }
                        i2++;
                    } catch (Throwable x) {
                        Log.m532w(PhotoView.TAG, x);
                    }
                }
            }
        }
    }

    public interface DismissListener {
        void onDismiss();

        void onPrepareDismiss();
    }

    public interface NavigationListener {
        void onPositionChanged(int i);
    }

    public interface PhotoViewerAdapter {
        boolean allowZoom(int i);

        int getCount();

        RunnableFuture<Bitmap> getPhoto(int i);

        float getPhotoLoadProgress(int i);

        Bitmap getThumb(int i);

        boolean isCached(int i);

        boolean isPhotoLoaded(int i);

        void requestPhotoDownload(int i);

        boolean shouldPreload();
    }

    public interface RunnableFuture<T> extends Runnable, Future<T> {
        void setProgressCallback(ProgressCallback progressCallback);
    }

    private class Texture {
        int cropHeight;
        int cropWidth;
        Bitmap data;
        int id;
        boolean loaded;
        int photoIndex;
        boolean thumb;

        public Texture(int _id) {
            this.loaded = false;
            this.thumb = false;
            this.id = _id;
        }
    }

    public interface ZoomListener {
        void onZoomChanged(float f, float f2, float f3);
    }

    static {
        PAGE_SPACING = Global.scale(10.0f);
    }

    public PhotoView(Context context) {
        super(context);
        this.scale = 1.0f;
        this.dragStartPointerID = -1;
        this.minScale = 1.0f;
        this.maxScale = ImageViewHolder.PaddingSize;
        this.itrp = new DecelerateInterpolator();
        this.scroller = new Scroller(getContext());
        this.pinchEndTime = 0;
        this.doubleTap = false;
        this.isList = true;
        this.listPosition = PAGE_SPACING;
        this.secondIsNext = true;
        this.scaleNext = 1.0f;
        this.scalePrev = 1.0f;
        this.inited = false;
        this.loadingBitmaps = new Vector();
        this.firstResume = true;
        this.fitBySmallestSide = false;
        this.resetOnResize = true;
        this.limitX = PAGE_SPACING;
        this.limitY = PAGE_SPACING;
        this.dropTouches = false;
        this.inAnimDone = true;
        this.outAnimDone = true;
        this.windowBg = new ColorDrawable(Color.WINDOW_BACKGROUND_COLOR);
        this.dismissing = false;
        this.dragging = false;
        this.bgAlpha = 1.0f;
        this.bgPaint = new Paint();
        this.dismissed = false;
        this.prevCallbackPos = PAGE_SPACING;
        this.prevSwitchFwd = true;
        this.disallowZoom = false;
        init();
    }

    public PhotoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.scale = 1.0f;
        this.dragStartPointerID = -1;
        this.minScale = 1.0f;
        this.maxScale = ImageViewHolder.PaddingSize;
        this.itrp = new DecelerateInterpolator();
        this.scroller = new Scroller(getContext());
        this.pinchEndTime = 0;
        this.doubleTap = false;
        this.isList = true;
        this.listPosition = PAGE_SPACING;
        this.secondIsNext = true;
        this.scaleNext = 1.0f;
        this.scalePrev = 1.0f;
        this.inited = false;
        this.loadingBitmaps = new Vector();
        this.firstResume = true;
        this.fitBySmallestSide = false;
        this.resetOnResize = true;
        this.limitX = PAGE_SPACING;
        this.limitY = PAGE_SPACING;
        this.dropTouches = false;
        this.inAnimDone = true;
        this.outAnimDone = true;
        this.windowBg = new ColorDrawable(Color.WINDOW_BACKGROUND_COLOR);
        this.dismissing = false;
        this.dragging = false;
        this.bgAlpha = 1.0f;
        this.bgPaint = new Paint();
        this.dismissed = false;
        this.prevCallbackPos = PAGE_SPACING;
        this.prevSwitchFwd = true;
        this.disallowZoom = false;
        init();
    }

    private void init() {
        this.texture = new Texture(PAGE_SPACING);
        this.textureNext = new Texture(1);
        this.texturePrev = new Texture(2);
        this.bgPaint.setColor(-1);
        this.bitmapPaint = new Paint();
        this.bitmapPaint.setFilterBitmap(true);
        this.touchslop = (float) ViewConfiguration.get(getContext()).getScaledTouchSlop();
        this.progressBar = new CircularProgressBar(getContext());
        this.error = (ErrorView) inflate(getContext(), C0436R.layout.error, null);
        this.thumbView = new ClippingImageView(getContext());
        this.detector = new GestureDetector(getContext(), this);
        this.detector.setOnDoubleTapListener(this);
        this.density = getResources().getDisplayMetrics().density;
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        Log.m528i(TAG, "photo viewer init");
        this.error.setBackgroundColor(-1157627904);
        this.error.setOnRetryListener(new C10301());
        this.error.setVisibility(8);
    }

    public void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed) {
            this.viewportW = (float) (((r - l) - getPaddingLeft()) - getPaddingRight());
            this.viewportH = (float) (((b - t) - getPaddingTop()) - getPaddingBottom());
            updateScale(true);
            updateMinMax(this.scale);
            this.translateX = clamp(this.translateX, (float) this.minX, (float) this.maxX);
            this.translateY = clamp(this.translateY, (float) this.minY, (float) this.maxY);
        }
        this.inited = true;
    }

    public void setPosition(int p) {
        if (this.listPosition != p) {
            this.listPosition = p;
            if (this.texture != null) {
                this.translateY = 0.0f;
                this.translateX = 0.0f;
                updateScale(true);
                updateMinMax(this.scale);
                postInvalidate();
                load(this.adapter.shouldPreload() ? new int[]{this.listPosition, this.listPosition + 1} : new int[]{this.listPosition}, new Texture[]{this.texture, this.textureNext}, new int[]{this.listPosition, this.listPosition + 1});
                this.prevCallbackPos = p;
            }
        }
    }

    public void setBgColor(int color) {
        this.bgPaint.setColor(color);
    }

    private void updateScale(boolean changed) {
        if (((float) this.texture.cropWidth) / ((float) this.texture.cropHeight) > this.viewportW / this.viewportH || this.fitBySmallestSide) {
            float fw = this.viewportW;
            float fh = this.viewportH;
            if (this.fitBySmallestSide) {
                fw -= (float) (this.limitX * 2);
                fh -= (float) (this.limitY * 2);
            }
            this.minScale = fw / ((float) this.texture.cropWidth);
            this.maxScale = fh / ((float) this.texture.cropHeight);
        } else {
            this.minScale = this.viewportH / ((float) this.texture.cropHeight);
            this.maxScale = this.viewportW / ((float) this.texture.cropWidth);
        }
        if (this.maxScale < ImageViewHolder.PaddingSize) {
            this.maxScale = ImageViewHolder.PaddingSize;
        }
        if (this.scale < this.minScale || this.scale > this.maxScale || changed) {
            this.scale = this.minScale;
            this.animScale = this.minScale;
        }
        float ih = (float) this.texture.cropHeight;
        float i2w = (float) (this.texture2 != null ? this.texture2.cropWidth : 1);
        float i2h = (float) (this.texture2 != null ? this.texture2.cropHeight : 1);
        if (((float) this.texture.cropWidth) == 0.0f || ih == 0.0f) {
            float iw = 1.0f;
        }
        if (i2w == 0.0f || i2h == 0.0f) {
            i2h = 1.0f;
            i2w = 1.0f;
        }
        if (this.texture2 != null && (this.texture2.loaded || this.texture2.thumb)) {
            if (i2w / i2h > this.viewportW / this.viewportH) {
                this.scaleNext = this.viewportW / i2w;
            } else {
                this.scaleNext = this.viewportH / i2h;
            }
        }
        Log.m525d(TAG, "Update scale, min=" + this.minScale + ", max=" + this.maxScale + ", next=" + this.scaleNext + ", current=" + this.scale);
    }

    private void updateMinMax(float scale) {
        if (((float) this.texture.cropWidth) / ((float) this.texture.cropHeight) > this.viewportW / this.viewportH) {
            if (scale > this.minScale || this.fitBySmallestSide) {
                this.maxX = Math.round(((((float) this.texture.cropWidth) * scale) / ImageViewHolder.PaddingSize) - ((this.viewportW / ImageViewHolder.PaddingSize) - ((float) this.limitX)));
                this.minX = -this.maxX;
                if (((float) this.texture.cropHeight) * scale > this.viewportH - ((float) this.limitY)) {
                    this.maxY = Math.round(((((float) this.texture.cropHeight) * scale) / ImageViewHolder.PaddingSize) - ((this.viewportH / ImageViewHolder.PaddingSize) - ((float) this.limitY)));
                    this.minY = -this.maxY;
                    return;
                }
                this.minY = PAGE_SPACING;
                this.maxY = PAGE_SPACING;
                return;
            }
            this.minY = PAGE_SPACING;
            this.maxY = PAGE_SPACING;
            this.minX = PAGE_SPACING;
            this.maxX = PAGE_SPACING;
        } else if (scale > this.minScale || this.fitBySmallestSide) {
            this.maxY = Math.round(((((float) this.texture.cropHeight) * scale) / ImageViewHolder.PaddingSize) - ((this.viewportH / ImageViewHolder.PaddingSize) - ((float) this.limitY)));
            this.minY = -this.maxY;
            if (((float) this.texture.cropWidth) * scale > this.viewportW) {
                this.maxX = Math.round(((((float) this.texture.cropWidth) * scale) / ImageViewHolder.PaddingSize) - ((this.viewportW / ImageViewHolder.PaddingSize) - ((float) this.limitX)));
                this.minX = -this.maxX;
                return;
            }
            this.minX = PAGE_SPACING;
            this.maxX = PAGE_SPACING;
        } else {
            this.minY = PAGE_SPACING;
            this.maxY = PAGE_SPACING;
            this.minX = PAGE_SPACING;
            this.maxX = PAGE_SPACING;
        }
    }

    public View getOverlayView() {
        if (this.overlay != null) {
            return this.overlay;
        }
        this.overlay = new C10312(getContext());
        this.overlay.addView(this.thumbView);
        this.overlay.addView(this.progressBar, new LayoutParams(Global.scale(70.0f), Global.scale(70.0f), 17));
        this.overlay.addView(this.error);
        return this.overlay;
    }

    public void setThumb(Bitmap thumb, boolean showProgress) {
        int i = 8;
        this.thumbView.setImageBitmap(thumb);
        this.thumbView.getParent().requestLayout();
        if (showProgress) {
            this.error.setVisibility(8);
        }
        CircularProgressBar circularProgressBar = this.progressBar;
        if (showProgress) {
            i = PAGE_SPACING;
        }
        circularProgressBar.setVisibility(i);
    }

    public void setErrorText(String text) {
        this.error.setMessage(text);
    }

    public int getPosition() {
        return this.listPosition;
    }

    private float clamp(float x, float min, float max) {
        return Math.min(Math.max(x, min), max);
    }

    private void updateTextures() {
        if (!this.isList || ((this.translateX > ((float) this.maxX) && this.listPosition == 0) || (this.translateX < ((float) this.minX) && this.listPosition == this.adapter.getCount() - 1))) {
            if (this.translateX > ((float) this.maxX)) {
                this.translateX = ((float) this.maxX) + ((this.translateX - ((float) this.maxX)) / ImageViewHolder.PaddingSize);
            }
            if (this.translateX < ((float) this.minX)) {
                this.translateX = ((float) this.minX) + ((this.translateX - ((float) this.minX)) / ImageViewHolder.PaddingSize);
            }
            if (this.texture2 != null) {
                this.texture2 = null;
                updateScale(false);
            }
        } else if (this.translateX > ((float) this.maxX) && this.texture2 != this.texturePrev && this.listPosition > 0) {
            this.texture2 = this.texturePrev;
            this.secondIsNext = false;
            updateScale(false);
        } else if (this.translateX < ((float) this.minX) && this.texture2 != this.textureNext && this.listPosition < this.adapter.getCount()) {
            this.texture2 = this.textureNext;
            this.secondIsNext = true;
            updateScale(false);
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (System.currentTimeMillis() - this.animStartTime < this.animDuration && this.animScale != this.scale) {
            this.dropTouches = true;
        }
        if (this.texture == null || !this.inited || this.dropTouches || !this.inAnimDone || !this.outAnimDone || !isEnabled()) {
            this.dropTouches = true;
            if (ev.getAction() == 1) {
                this.dropTouches = false;
            }
            return true;
        } else if (ev.getPointerCount() > 1 && (this.texture == null || !this.texture.loaded)) {
            this.dropTouches = true;
            return true;
        } else if (ev.getPointerCount() == 1 && this.detector.onTouchEvent(ev) && this.doubleTap) {
            this.doubleTap = false;
            return true;
        } else {
            if (ev.getAction() == 1) {
                if (!this.dismissing || Math.abs(this.translateY) <= this.viewportH / 4.0f || this.dismissed) {
                    this.dragging = false;
                    this.dismissing = false;
                    updateMinMax(clamp(this.scale, this.minScale, this.maxScale));
                    if (System.currentTimeMillis() - this.animStartTime < this.animDuration) {
                        return true;
                    }
                    if (this.translateX - ((float) this.maxX) > ((float) (getWidth() / 2)) && this.listPosition > 0) {
                        goToPrev();
                        return true;
                    } else if ((-(this.translateX - ((float) this.minX))) > ((float) (getWidth() / 2)) && this.listPosition < this.adapter.getCount() - 1) {
                        goToNext();
                        return true;
                    } else if (this.translateX < ((float) this.minX) || this.translateX > ((float) this.maxX) || this.translateY < ((float) this.minY) || this.translateY > ((float) this.maxY)) {
                        animateTo(clamp(this.scale, this.minScale, this.maxScale), clamp(this.translateX, (float) this.minX, (float) this.maxX), clamp(this.translateY, (float) this.minY, (float) this.maxY));
                    }
                } else {
                    if (!(this.dismissListener == null || this.dismissed)) {
                        this.dismissListener.onDismiss();
                    }
                    this.dismissing = false;
                    return true;
                }
            }
            if (ev.getAction() == 0) {
                if (!this.scroller.isFinished()) {
                    this.scroller.abortAnimation();
                }
                if (System.currentTimeMillis() - this.animStartTime < this.animDuration) {
                    float at = this.itrp.getInterpolation(((float) (System.currentTimeMillis() - this.animStartTime)) / ((float) this.animDuration));
                    this.animStartTime = 0;
                    this.translateX += (float) Math.round((this.animTx - this.translateX) * at);
                    this.translateY += (float) Math.round((this.animTy - this.translateY) * at);
                    this.scale += (this.animScale - this.scale) * at;
                    invalidate();
                }
                if (this.postedPhotoSwitch != null) {
                    removeCallbacks(this.postedPhotoSwitch);
                    this.postedPhotoSwitch = null;
                }
                this.dragStartX = ev.getX();
                this.dragStartY = ev.getY();
                this.dragStartTx = this.translateX;
                this.dragStartTy = this.translateY;
                this.dragStartPointerID = ev.getPointerId(PAGE_SPACING);
                if (this.prevCallbackPos != this.listPosition) {
                    if (this.prevCallbackPos == this.listPosition + 1) {
                        this.dragStartX -= this.viewportW + ((float) PAGE_SPACING);
                        this.translateX += this.viewportW + ((float) PAGE_SPACING);
                        switchPhoto(true);
                    } else if (this.prevCallbackPos == this.listPosition - 1) {
                        this.dragStartX += this.viewportW + ((float) PAGE_SPACING);
                        this.translateX -= this.viewportW + ((float) PAGE_SPACING);
                        switchPhoto(false);
                    }
                    updateTextures();
                    invalidate();
                    Log.m525d(TAG, "Already switched " + this.prevCallbackPos);
                }
            }
            if (ev.getAction() == 2 && ev.getPointerCount() == 1) {
                if (System.currentTimeMillis() < this.pinchEndTime + 300 || System.currentTimeMillis() - this.animStartTime < this.animDuration) {
                    return true;
                }
                if (this.dragStartPointerID != ev.getPointerId(PAGE_SPACING)) {
                    this.dragStartX = ev.getX();
                    this.dragStartY = ev.getY();
                    this.dragStartTx = this.translateX;
                    this.dragStartTy = this.translateY;
                    this.dragStartPointerID = ev.getPointerId(PAGE_SPACING);
                    return true;
                }
                if (this.maxX > 0 || (this.isList && !this.dismissing)) {
                    this.translateX = (this.dragStartTx + ev.getX()) - this.dragStartX;
                    updateTextures();
                } else {
                    this.translateX = 0.0f;
                }
                if (this.scale == this.minScale && (Math.abs(this.translateY) > this.touchslop || (Math.abs(this.translateX) < this.touchslop && !this.dragging))) {
                    this.translateX = 0.0f;
                }
                if (this.scale == this.minScale && Math.abs(this.translateX) > this.touchslop && !this.dragging) {
                    this.dragging = true;
                    if (this.translateX > 0.0f) {
                        this.dragStartX += this.touchslop;
                        this.translateX -= this.touchslop;
                    } else {
                        this.dragStartX -= this.touchslop;
                        this.translateX += this.touchslop;
                    }
                }
                this.translateY = (this.dragStartTy + ev.getY()) - this.dragStartY;
                if (this.scale != this.minScale) {
                    if (this.translateY > ((float) this.maxY)) {
                        this.translateY = ((float) this.maxY) + ((this.translateY - ((float) this.maxY)) / ImageViewHolder.PaddingSize);
                    }
                    if (this.translateY < ((float) this.minY)) {
                        this.translateY = ((float) this.minY) + ((this.translateY - ((float) this.minY)) / ImageViewHolder.PaddingSize);
                    }
                } else if (this.scale == this.minScale) {
                    if (Math.abs(this.translateX) > this.touchslop || ((Math.abs(this.translateY) < this.touchslop && !this.dismissing) || this.dragging)) {
                        this.translateY = 0.0f;
                    } else if (Math.abs(this.translateY) > this.touchslop) {
                        if (!(this.dismissListener == null || this.dismissing || this.dismissed)) {
                            this.dismissListener.onPrepareDismiss();
                            if (this.translateY > 0.0f) {
                                this.dragStartY += this.touchslop;
                                this.translateY -= this.touchslop;
                            } else {
                                this.dragStartY -= this.touchslop;
                                this.translateY += this.touchslop;
                            }
                        }
                        this.dismissing = true;
                    }
                }
                if (this.translateX - ((float) this.maxX) > ((float) (getWidth() / 2)) && this.listPosition > 0) {
                    callPositionChanged(this.listPosition - 1);
                } else if ((-(this.translateX - ((float) this.minX))) <= ((float) (getWidth() / 2)) || this.listPosition >= this.adapter.getCount() - 1) {
                    callPositionChanged(this.listPosition);
                } else {
                    callPositionChanged(this.listPosition + 1);
                }
                if (this.scale == this.minScale) {
                    if (this.translateX < 0.0f && (-(this.translateX - ((float) this.minX))) > this.viewportW + ((float) PAGE_SPACING)) {
                        switchPhoto(true);
                        this.dragStartX -= this.viewportW + ((float) PAGE_SPACING);
                        this.translateX += this.viewportW + ((float) PAGE_SPACING);
                    } else if (this.translateX > 0.0f && this.translateX - ((float) this.maxX) > this.viewportW + ((float) PAGE_SPACING)) {
                        switchPhoto(false);
                        this.dragStartX += this.viewportW + ((float) PAGE_SPACING);
                        this.translateX -= this.viewportW + ((float) PAGE_SPACING);
                    }
                }
                invalidate();
                if (this.thumbView.getVisibility() == 0) {
                    this.overlay.invalidate();
                }
            }
            if ((this.dismissing || this.animStartTime + this.animDuration > System.currentTimeMillis()) && ev.getPointerCount() > 1) {
                return true;
            }
            if ((ev.getAction() & MotionEventCompat.ACTION_MASK) == 5) {
                if ((-(this.translateX - ((float) this.minX))) > this.touchslop || this.translateX - ((float) this.maxX) > this.touchslop) {
                    this.disallowZoom = true;
                    return true;
                }
                this.pinchStartDist = (float) Math.hypot((double) (ev.getX(1) - ev.getX(PAGE_SPACING)), (double) (ev.getY(1) - ev.getY(PAGE_SPACING)));
                this.pinchStartScale = this.scale;
                this.pinchCenterX = (ev.getX(PAGE_SPACING) + ev.getX(1)) / ImageViewHolder.PaddingSize;
                this.pinchCenterY = (ev.getY(PAGE_SPACING) + ev.getY(1)) / ImageViewHolder.PaddingSize;
                this.scaleStartTx = this.translateX;
                this.scaleStartTy = this.translateY;
            }
            if ((ev.getAction() & MotionEventCompat.ACTION_MASK) == 6) {
                if (this.disallowZoom) {
                    this.disallowZoom = false;
                    return true;
                }
                this.dragStartPointerID = -1;
                if (this.scale < this.minScale) {
                    animateTo(this.minScale, 0.0f, 0.0f);
                    updateMinMax(this.minScale);
                } else if (this.scale > this.maxScale) {
                    updateMinMax(this.maxScale);
                    animateTo(this.maxScale, clamp(this.translateX, (float) this.minX, (float) this.maxX), clamp(this.translateY, (float) this.minY, (float) this.maxY));
                }
                this.pinchEndTime = System.currentTimeMillis();
            }
            if ((ev.getAction() & MotionEventCompat.ACTION_MASK) == 2 && ev.getPointerCount() == 2 && (this.adapter == null || this.adapter.allowZoom(this.listPosition))) {
                if (this.disallowZoom) {
                    return true;
                }
                if (this.dismissing) {
                    return true;
                }
                this.scale = (((float) Math.hypot((double) (ev.getX(1) - ev.getX(PAGE_SPACING)), (double) (ev.getY(1) - ev.getY(PAGE_SPACING)))) / this.pinchStartDist) * this.pinchStartScale;
                this.translateX = (this.pinchCenterX - (this.viewportW / ImageViewHolder.PaddingSize)) - (((this.pinchCenterX - (this.viewportW / ImageViewHolder.PaddingSize)) - this.scaleStartTx) * (this.scale / this.pinchStartScale));
                this.translateY = (this.pinchCenterY - (this.viewportH / ImageViewHolder.PaddingSize)) - (((this.pinchCenterY - (this.viewportH / ImageViewHolder.PaddingSize)) - this.scaleStartTy) * (this.scale / this.pinchStartScale));
                if (this.zoomListener != null) {
                    this.zoomListener.onZoomChanged(this.scale, this.maxScale, this.minScale);
                }
                invalidate();
            }
            return true;
        }
    }

    public void animateIn(Rect rect, int clipSize) {
        this.thumbRect = new Rect(rect);
        Rect rect2 = this.thumbRect;
        rect2.top += clipSize;
        this.inAnimDone = false;
        this.overlay.setBackgroundDrawable(this.windowBg);
        this.thumbView.getViewTreeObserver().addOnPreDrawListener(new C10333(rect, clipSize));
    }

    public void animateOut(Rect rect, int clipSize, Runnable after) {
        if (this.inAnimDone) {
            this.thumbView.setVisibility(PAGE_SPACING);
            this.thumbView.clearAnimation();
            if (this.texture.data != null) {
                this.thumbView.setImageBitmap(this.texture.data);
            }
            requestLayout();
            this.thumbView.getViewTreeObserver().addOnPreDrawListener(new C10364(rect, clipSize, after));
        }
    }

    public boolean onTrackballEvent(MotionEvent ev) {
        if (ev.getAction() == 2) {
            this.translateX = clamp(this.translateX + (ev.getX() * 50.0f), (float) this.minX, (float) this.maxX);
            this.translateY = clamp(this.translateY - (ev.getY() * 50.0f), (float) this.minY, (float) this.maxY);
            postInvalidate();
        }
        if (ev.getAction() == 0) {
            if (this.scale < this.maxScale) {
                animateTo(this.maxScale, this.translateX, this.translateY);
            } else {
                animateTo(this.minScale, 0.0f, 0.0f);
            }
        }
        return true;
    }

    public void setOnClickListener(OnClickListener l) {
        this.clickListener = l;
    }

    public void setLimitOffsets(int x, int y) {
        this.limitY = y;
        this.limitX = x;
    }

    public void setNavigationListener(NavigationListener l) {
        this.navListener = l;
    }

    public void setDismissListener(DismissListener l) {
        this.dismissListener = l;
    }

    public void setZoomListener(ZoomListener l) {
        this.zoomListener = l;
    }

    private void callPositionChanged(int pos) {
        if (!(this.navListener == null || pos == this.prevCallbackPos)) {
            this.navListener.onPositionChanged(pos);
        }
        this.prevCallbackPos = pos;
    }

    private void animateTo(float newScale, float newTx, float newTy) {
        Log.m525d(TAG, "animate to " + newScale + ", " + newTx + ", " + newTy + " from " + this.scale + ", " + this.translateX + ", " + this.translateY);
        this.animScale = newScale;
        this.animTx = newTx;
        this.animTy = newTy;
        this.animStartTime = System.currentTimeMillis();
        this.animDuration = 300;
        postInvalidate();
        this.overlay.postInvalidate();
        if (this.zoomListener != null) {
            this.zoomListener.onZoomChanged(newScale, this.maxScale, this.minScale);
        }
    }

    public boolean onDoubleTap(MotionEvent e) {
        if ((this.scale == this.minScale && Math.abs(this.translateY) > 0.0f) || this.animStartTime + this.animDuration > System.currentTimeMillis() || this.translateX > ((float) this.maxX) + this.touchslop || this.translateX < ((float) this.minX) - this.touchslop) {
            return false;
        }
        if (this.texture != null && !this.texture.loaded) {
            return false;
        }
        if (this.scale < this.maxScale) {
            float atx = (e.getX() - (this.viewportW / ImageViewHolder.PaddingSize)) - (((e.getX() - (this.viewportW / ImageViewHolder.PaddingSize)) - this.translateX) * (this.maxScale / this.scale));
            float aty = (e.getY() - (this.viewportH / ImageViewHolder.PaddingSize)) - (((e.getY() - (this.viewportH / ImageViewHolder.PaddingSize)) - this.translateY) * (this.maxScale / this.scale));
            updateMinMax(this.maxScale);
            animateTo(this.maxScale, clamp(atx, (float) this.minX, (float) this.maxX), clamp(aty, (float) this.minY, (float) this.maxY));
        } else {
            animateTo(this.minScale, 0.0f, 0.0f);
        }
        this.doubleTap = true;
        return true;
    }

    public boolean onDoubleTapEvent(MotionEvent e) {
        return false;
    }

    public boolean onSingleTapConfirmed(MotionEvent e) {
        if (this.clickListener == null) {
            return false;
        }
        this.clickListener.onClick(this);
        return true;
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (this.scale == this.minScale && this.dismissing && Math.abs(velocityY) > ((float) Global.scale(1000.0f)) && this.dismissListener != null) {
            this.dismissListener.onDismiss();
            this.dismissed = true;
        } else if ((this.translateX - ((float) this.maxX) > 0.0f || (-(this.translateX - ((float) this.minX))) > 0.0f) && Math.abs(velocityX) > 650.0f * this.density) {
            Log.m528i(TAG, "Switch by velocity");
            if (velocityX > 0.0f && this.listPosition > 0) {
                goToPrev();
            } else if (velocityX < 0.0f && this.listPosition < this.adapter.getCount() - 1) {
                goToNext();
            }
        } else if (System.currentTimeMillis() > this.pinchEndTime + 300) {
            this.scroller.fling(Math.round(this.translateX), Math.round(this.translateY), Math.round(velocityX), Math.round(velocityY), this.minX, this.maxX, this.minY, this.maxY);
            postInvalidate();
        }
        return true;
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public void goToNext() {
        animateTo(this.scale, (((float) this.minX) - this.viewportW) - ((float) PAGE_SPACING), this.translateY);
        postSwitch(true);
    }

    public void goToPrev() {
        animateTo(this.scale, (((float) this.maxX) + this.viewportW) + ((float) PAGE_SPACING), this.translateY);
        postSwitch(false);
    }

    private void postSwitch(boolean fwd) {
        Runnable c10375 = new C10375(fwd);
        this.postedPhotoSwitch = c10375;
        postDelayed(c10375, this.animDuration + 50);
    }

    private void switchPhoto(boolean fwd) {
        try {
            Iterator it = this.loadingBitmaps.iterator();
            while (it.hasNext()) {
                ((RunnableFuture) it.next()).cancel(false);
            }
        } catch (Exception e) {
        }
        this.loadingBitmaps.clear();
        if (fwd) {
            this.listPosition++;
        } else {
            this.listPosition--;
        }
        if (this.listPosition < 0) {
            this.listPosition = PAGE_SPACING;
        }
        if (this.listPosition >= this.adapter.getCount()) {
            this.listPosition = this.adapter.getCount() - 1;
        }
        callPositionChanged(this.listPosition);
        Texture tmp;
        if (fwd) {
            Log.m528i(TAG, "Switch photo >> [" + this.listPosition + "]");
            tmp = this.texturePrev;
            this.texturePrev = this.texture;
            this.texture = this.textureNext;
            this.textureNext = tmp;
        } else {
            Log.m528i(TAG, "Switch photo << [" + this.listPosition + "]");
            tmp = this.textureNext;
            this.textureNext = this.texture;
            this.texture = this.texturePrev;
            this.texturePrev = tmp;
        }
        updateScale(true);
        updateMinMax(this.scale);
        loadPhotos(fwd);
        if (!this.texture.thumb || this.texture.data == null) {
            this.thumbView.setImageBitmap(null);
        } else {
            this.thumbView.setImageBitmap(this.texture.data);
            this.thumbView.setVisibility(PAGE_SPACING);
            this.overlay.invalidate();
        }
        if (this.prevSwitchFwd != fwd) {
            Log.m528i(TAG, "Switch dir was " + this.prevSwitchFwd + ", now " + fwd);
        }
        this.prevSwitchFwd = fwd;
        if (this.texturePrev == this.textureNext) {
            Log.m526e(TAG, "OH SHIT!");
        }
        if (this.texture == this.textureNext) {
            Log.m526e(TAG, "OH SHIT!");
        }
        if (this.texture == this.texturePrev) {
            Log.m526e(TAG, "OH SHIT!");
        }
        if (this.progressBar.getVisibility() == 0) {
            this.progressBar.setVisibility(8);
            Global.showViewAnimated(this.progressBar, true, THUMB_ANIM_DURATION);
        }
        invalidate();
        this.overlay.requestLayout();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadPhotos(boolean r8) {
        /*
        r7 = this;
        r4 = 2;
        r6 = 1;
        r5 = 0;
        if (r8 == 0) goto L_0x0059;
    L_0x0005:
        r1 = r7.listPosition;
        r1 = r1 + 1;
        r2 = r7.adapter;
        r2 = r2.getCount();
        if (r1 >= r2) goto L_0x005f;
    L_0x0011:
        r1 = r7.adapter;
        r1 = r1.shouldPreload();
        if (r1 != 0) goto L_0x0027;
    L_0x0019:
        r2 = r7.adapter;
        if (r8 == 0) goto L_0x00bf;
    L_0x001d:
        r1 = r7.listPosition;
        r1 = r1 + 1;
    L_0x0021:
        r1 = r2.isCached(r1);
        if (r1 == 0) goto L_0x005f;
    L_0x0027:
        r1 = r7.texture;
        r1 = r1.loaded;
        if (r1 != 0) goto L_0x00d4;
    L_0x002d:
        r2 = new int[r4];
        r1 = r7.listPosition;
        r2[r5] = r1;
        if (r8 == 0) goto L_0x00c5;
    L_0x0035:
        r1 = r7.listPosition;
        r1 = r1 + 1;
    L_0x0039:
        r2[r6] = r1;
        r3 = new com.vkontakte.android.ui.PhotoView.Texture[r4];
        r1 = r7.texture;
        r3[r5] = r1;
        if (r8 == 0) goto L_0x00cb;
    L_0x0043:
        r1 = r7.textureNext;
    L_0x0045:
        r3[r6] = r1;
        r4 = new int[r4];
        r1 = r7.listPosition;
        r4[r5] = r1;
        if (r8 == 0) goto L_0x00cf;
    L_0x004f:
        r1 = r7.listPosition;
        r1 = r1 + 1;
    L_0x0053:
        r4[r6] = r1;
        r7.load(r2, r3, r4);
    L_0x0058:
        return;
    L_0x0059:
        r1 = r7.listPosition;
        r1 = r1 + -1;
        if (r1 >= 0) goto L_0x0011;
    L_0x005f:
        r1 = r7.texture;
        r1 = r1.loaded;
        if (r1 != 0) goto L_0x0088;
    L_0x0065:
        r2 = new int[r6];
        r1 = r7.listPosition;
        r2[r5] = r1;
        r3 = new com.vkontakte.android.ui.PhotoView.Texture[r4];
        r1 = r7.texture;
        r3[r5] = r1;
        if (r8 == 0) goto L_0x0102;
    L_0x0073:
        r1 = r7.textureNext;
    L_0x0075:
        r3[r6] = r1;
        r4 = new int[r4];
        r1 = r7.listPosition;
        r4[r5] = r1;
        if (r8 == 0) goto L_0x0106;
    L_0x007f:
        r1 = r7.listPosition;
        r1 = r1 + 1;
    L_0x0083:
        r4[r6] = r1;
        r7.load(r2, r3, r4);
    L_0x0088:
        if (r8 == 0) goto L_0x010c;
    L_0x008a:
        r1 = r7.textureNext;
        r1.loaded = r5;
        r1 = r7.listPosition;
        r1 = r1 + 1;
        r2 = r7.adapter;
        r2 = r2.getCount();
        if (r1 >= r2) goto L_0x0058;
    L_0x009a:
        r1 = r7.adapter;
        r2 = r7.listPosition;
        r2 = r2 + 1;
        r0 = r1.getThumb(r2);
        if (r0 == 0) goto L_0x0058;
    L_0x00a6:
        r1 = r7.textureNext;
        r1.thumb = r6;
        r1 = r7.textureNext;
        r2 = r0.getWidth();
        r1.cropWidth = r2;
        r1 = r7.textureNext;
        r2 = r0.getHeight();
        r1.cropHeight = r2;
        r1 = r7.textureNext;
        r1.data = r0;
        goto L_0x0058;
    L_0x00bf:
        r1 = r7.listPosition;
        r1 = r1 + -1;
        goto L_0x0021;
    L_0x00c5:
        r1 = r7.listPosition;
        r1 = r1 + -1;
        goto L_0x0039;
    L_0x00cb:
        r1 = r7.texturePrev;
        goto L_0x0045;
    L_0x00cf:
        r1 = r7.listPosition;
        r1 = r1 + -1;
        goto L_0x0053;
    L_0x00d4:
        r2 = new int[r6];
        if (r8 == 0) goto L_0x00f5;
    L_0x00d8:
        r1 = r7.listPosition;
        r1 = r1 + 1;
    L_0x00dc:
        r2[r5] = r1;
        r3 = new com.vkontakte.android.ui.PhotoView.Texture[r6];
        if (r8 == 0) goto L_0x00fa;
    L_0x00e2:
        r1 = r7.textureNext;
    L_0x00e4:
        r3[r5] = r1;
        r4 = new int[r6];
        if (r8 == 0) goto L_0x00fd;
    L_0x00ea:
        r1 = r7.listPosition;
        r1 = r1 + 1;
    L_0x00ee:
        r4[r5] = r1;
        r7.load(r2, r3, r4);
        goto L_0x0058;
    L_0x00f5:
        r1 = r7.listPosition;
        r1 = r1 + -1;
        goto L_0x00dc;
    L_0x00fa:
        r1 = r7.texturePrev;
        goto L_0x00e4;
    L_0x00fd:
        r1 = r7.listPosition;
        r1 = r1 + -1;
        goto L_0x00ee;
    L_0x0102:
        r1 = r7.texturePrev;
        goto L_0x0075;
    L_0x0106:
        r1 = r7.listPosition;
        r1 = r1 + -1;
        goto L_0x0083;
    L_0x010c:
        r1 = r7.texturePrev;
        r1.loaded = r5;
        r1 = r7.listPosition;
        r1 = r1 + -1;
        if (r1 < 0) goto L_0x0058;
    L_0x0116:
        r1 = r7.adapter;
        r2 = r7.listPosition;
        r2 = r2 + -1;
        r0 = r1.getThumb(r2);
        if (r0 == 0) goto L_0x0058;
    L_0x0122:
        r1 = r7.texturePrev;
        r1.thumb = r6;
        r1 = r7.texturePrev;
        r2 = r0.getWidth();
        r1.cropWidth = r2;
        r1 = r7.texturePrev;
        r2 = r0.getHeight();
        r1.cropHeight = r2;
        r1 = r7.texturePrev;
        r1.data = r0;
        goto L_0x0058;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.ui.PhotoView.loadPhotos(boolean):void");
    }

    public void setAdapter(PhotoViewerAdapter a) {
        this.adapter = a;
        if (this.inited) {
            loadPhotos(true);
        } else {
            post(new C10386(System.currentTimeMillis()));
        }
    }

    private void load(int[] positions, Texture[] textures, int[] thumbPositions) {
        if (this.error.getVisibility() == 0) {
            this.error.setVisibility(8);
        }
        new Thread(new C10447(textures, thumbPositions, positions)).start();
    }

    public float getTranslateX() {
        return this.translateX;
    }

    public float getTranslateY() {
        return this.translateY;
    }

    public float getScale() {
        return this.scale;
    }

    public int getPhotoWidth() {
        return this.texture.cropWidth;
    }

    public int getPhotoHeight() {
        return this.texture.cropHeight;
    }

    public void setFitBySmallestSide(boolean fit) {
        this.fitBySmallestSide = fit;
    }

    public void setResetOnResize(boolean reset) {
        this.resetOnResize = reset;
    }

    public void onDraw(Canvas c) {
        if (this.inAnimDone && this.outAnimDone) {
            if (this.thumbRect != null) {
                int[] f = new int[2];
                getLocationOnScreen(f);
                c.translate(0.0f, (float) (-f[1]));
                c.drawRect(this.thumbRect, this.bgPaint);
                c.translate(0.0f, (float) f[1]);
            }
            c.save();
            c.translate((float) ((((getWidth() - getPaddingLeft()) - getPaddingRight()) / 2) + getPaddingLeft()), (float) ((((getHeight() - getPaddingTop()) - getPaddingBottom()) / 2) + getPaddingTop()));
            float aty = this.translateY;
            if (System.currentTimeMillis() - this.animStartTime < this.animDuration) {
                if (!this.scroller.isFinished()) {
                    this.scroller.abortAnimation();
                }
                float ai = this.itrp.getInterpolation(((float) (System.currentTimeMillis() - this.animStartTime)) / ((float) this.animDuration));
                float ts = this.scale + ((this.animScale - this.scale) * ai);
                float tx = (this.translateX + ((this.animTx - this.translateX) * ai)) / ts;
                float ty = (this.translateY + ((this.animTy - this.translateY) * ai)) / ts;
                aty = ty;
                if (tx - ((float) this.maxX) > ((float) (getWidth() / 2)) && this.listPosition > 0) {
                    callPositionChanged(this.listPosition - 1);
                } else if ((-(tx - ((float) this.minX))) > ((float) (getWidth() / 2)) && this.listPosition < this.adapter.getCount() - 1) {
                    callPositionChanged(this.listPosition + 1);
                }
                c.scale(ts, ts);
                c.translate(tx, ty);
                invalidate();
            } else {
                if (this.animStartTime != 0) {
                    this.translateX = this.animTx;
                    this.translateY = this.animTy;
                    this.scale = this.animScale;
                    this.bgAlpha = 1.0f;
                    this.animStartTime = 0;
                    updateMinMax(this.scale);
                }
                if (!this.scroller.isFinished() && this.scroller.computeScrollOffset()) {
                    if (this.scroller.getStartX() < this.maxX && this.scroller.getStartX() > this.minX) {
                        this.translateX = (float) this.scroller.getCurrX();
                    }
                    if (this.scroller.getStartY() < this.maxY && this.scroller.getStartY() > this.minY) {
                        this.translateY = (float) this.scroller.getCurrY();
                    }
                    invalidate();
                }
                c.translate(this.translateX, this.translateY);
                c.scale(this.scale, this.scale);
                aty = this.translateY;
                this.overlay.invalidate();
            }
            int ba = MotionEventCompat.ACTION_MASK;
            if (this.scale == this.minScale) {
                if (Float.isNaN(aty)) {
                    aty = 0.0f;
                }
                this.bgAlpha = 1.0f - (Math.abs(aty) / ((float) getHeight()));
                ba = Math.round(this.bgAlpha * 255.0f);
            }
            c.drawColor(ba << 24);
            if (this.texture != null && ((this.texture.loaded || this.texture.thumb) && this.texture.data != null)) {
                if (!(this.progressBar.getVisibility() == 8 || this.texture.thumb)) {
                    this.progressBar.setVisibility(8);
                }
                c.drawBitmap(this.texture.data, ((float) (-this.texture.cropWidth)) / ImageViewHolder.PaddingSize, ((float) (-this.texture.cropHeight)) / ImageViewHolder.PaddingSize, this.bitmapPaint);
            }
            if (this.scale >= this.minScale && this.texture2 != null && this.texture2.data != null && (this.texture2.loaded || this.texture2.thumb)) {
                c.translate(0.0f, (-this.translateY) / this.scale);
                float k = (float) (this.secondIsNext ? 1 : -1);
                c.translate((((this.viewportW + ((float) PAGE_SPACING)) * k) / this.minScale) / ImageViewHolder.PaddingSize, 0.0f);
                c.scale(this.scaleNext / this.scale, this.scaleNext / this.scale);
                c.translate((((this.viewportW + ((float) PAGE_SPACING)) * k) / this.scaleNext) / ImageViewHolder.PaddingSize, 0.0f);
                c.drawBitmap(this.texture2.data, ((float) (-this.texture2.cropWidth)) / ImageViewHolder.PaddingSize, ((float) (-this.texture2.cropHeight)) / ImageViewHolder.PaddingSize, this.bitmapPaint);
            }
            c.restore();
        }
    }

    public void setThumbBounds(Rect r) {
        this.thumbRect = r;
        this.overlay.invalidate();
    }
}
