package com.vkontakte.android.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View.OnKeyListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import android.widget.EditText;

public class XEditText extends EditText {
    private OnKeyListener keyListener;

    private class ZanyInputConnection extends InputConnectionWrapper {
        public ZanyInputConnection(InputConnection target, boolean mutable) {
            super(target, mutable);
        }

        public boolean sendKeyEvent(KeyEvent event) {
            if (event.getAction() == 0 && event.getKeyCode() == 67 && XEditText.this.keyListener != null) {
                XEditText.this.keyListener.onKey(XEditText.this, event.getKeyCode(), event);
            }
            return super.sendKeyEvent(event);
        }
    }

    public XEditText(Context context) {
        super(context);
    }

    public XEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public XEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        return new ZanyInputConnection(super.onCreateInputConnection(outAttrs), true);
    }

    public void setOnKeyListener(OnKeyListener l) {
        this.keyListener = l;
        super.setOnKeyListener(l);
    }
}
