package com.vkontakte.android.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Spinner;
import com.vkontakte.android.C0436R;

public class OverlaySpinner extends Spinner {
    private Drawable overlay;
    private boolean padAsBg;
    private int padBtm;
    private int padLeft;
    private boolean padOverlay;
    private int padRight;
    private int padTop;

    public OverlaySpinner(Context context) {
        super(context);
        this.padOverlay = false;
        this.padAsBg = false;
    }

    public OverlaySpinner(Context context, int mode) {
        super(context, mode);
        this.padOverlay = false;
        this.padAsBg = false;
    }

    public OverlaySpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.padOverlay = false;
        this.padAsBg = false;
        init(attrs, 0);
    }

    public OverlaySpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.padOverlay = false;
        this.padAsBg = false;
        init(attrs, defStyle);
    }

    public OverlaySpinner(Context context, AttributeSet attrs, int defStyle, int mode) {
        super(context, attrs, defStyle, mode);
        this.padOverlay = false;
        this.padAsBg = false;
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, C0436R.styleable.OverlayView, defStyle, 0);
            int r = a.getResourceId(0, 0);
            if (r != 0) {
                setOverlay(r);
            }
            this.padOverlay = a.getBoolean(1, false);
            this.padAsBg = a.getBoolean(2, false);
        }
        setWillNotDraw(false);
    }

    public void setOverlay(int resID) {
        setOverlay(getResources().getDrawable(resID));
    }

    public void setPadOverlay(boolean pad) {
        this.padOverlay = pad;
        invalidate();
    }

    public void setPressed(boolean pressed) {
        View parent = (View) getParent();
        if (VERSION.SDK_INT > 15 || !parent.isPressed() || isDuplicateParentStateEnabled()) {
            super.setPressed(pressed);
        }
    }

    public void setOverlayPadding(int l, int t, int r, int b) {
        this.padLeft = l;
        this.padTop = t;
        this.padRight = r;
        this.padBtm = b;
    }

    public void setOverlay(Drawable d) {
        if (this.overlay != null) {
            this.overlay.setCallback(null);
        }
        this.overlay = d;
        this.overlay.setCallback(this);
        invalidate();
    }

    public void draw(Canvas c) {
        super.draw(c);
        float[] mx = new float[9];
        c.getMatrix().getValues(mx);
        c.translate(-mx[2], -mx[5]);
        if (this.overlay != null) {
            if (this.padOverlay) {
                this.overlay.setBounds(getPaddingLeft(), getPaddingTop(), c.getWidth() - getPaddingRight(), c.getHeight() - getPaddingBottom());
            } else if (this.padAsBg) {
                Rect r = new Rect();
                if (!(getBackground() == null || getBackground().getPadding(r))) {
                    r.set(0, 0, 0, 0);
                }
                this.overlay.setBounds(r.left, r.top, c.getWidth() - r.right, c.getHeight() - r.bottom);
            } else {
                this.overlay.setBounds(this.padLeft, this.padTop, c.getWidth() - this.padRight, c.getHeight() - this.padBtm);
            }
            this.overlay.draw(c);
        }
    }

    protected boolean verifyDrawable(Drawable d) {
        return super.verifyDrawable(d) || d == this.overlay;
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.overlay != null && this.overlay.isStateful()) {
            this.overlay.setState(getDrawableState());
            postInvalidate();
        }
    }
}
