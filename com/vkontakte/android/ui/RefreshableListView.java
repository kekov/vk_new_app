package com.vkontakte.android.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.MethodAnimation;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.lang.reflect.Method;
import org.acra.ACRAConstants;

public class RefreshableListView extends ListView implements OnScrollListener, OnItemLongClickListener {
    private static final int SCROLLBAR_SIZE = 200;
    private static final int STATE_PULL = 1;
    private static final int STATE_REFRESHING = 3;
    private static final int STATE_RELEASE = 2;
    private float animStartOffset;
    private boolean animatingAfterRefresh;
    private Bitmap arrowBitmap;
    private Bitmap arrowFlipBitmap;
    private float arrowRotation;
    private MethodAnimation bwdAnim;
    private boolean click;
    private float clickStartY;
    private boolean completelyDisableOverscroll;
    private float density;
    private float dragStartY;
    private boolean draggingEnabled;
    private boolean draggingTop;
    private float drawOffset;
    private boolean drawShadow;
    private boolean eventCanceled;
    private boolean fromTop;
    private MethodAnimation fwdAnim;
    private boolean highlightAfterClick;
    private Interpolator itrp;
    private OnRefreshListener listener;
    private OnItemLongClickListener longClickListener;
    private Method msetOverScrollMode;
    private boolean needReturnTop;
    private float offset;
    private int prevScrollState;
    private int pullBG;
    private FrameLayout refreshContainer;
    private boolean refreshEnabled;
    private View refreshView;
    private float refreshViewHeight;
    private OnScrollListener scrollListener;
    private int scrollState;
    private Drawable shadowDrawable;
    private int state;
    private boolean stoppedDraggingTop;
    private String subtext;
    private Paint subtextPaint;
    private String text;
    private Paint textPaint;
    private float touchslop;

    /* renamed from: com.vkontakte.android.ui.RefreshableListView.1 */
    class C10451 implements Runnable {
        C10451() {
        }

        public void run() {
            RefreshableListView.this.animatingAfterRefresh = false;
            RefreshableListView.this.enableOverscroll(true);
        }
    }

    /* renamed from: com.vkontakte.android.ui.RefreshableListView.2 */
    class C10462 implements Runnable {
        C10462() {
        }

        public void run() {
            RefreshableListView.this.animatingAfterRefresh = false;
            RefreshableListView.this.enableOverscroll(true);
        }
    }

    /* renamed from: com.vkontakte.android.ui.RefreshableListView.3 */
    class C10473 implements Runnable {
        private final /* synthetic */ int val$_i;

        C10473(int i) {
            this.val$_i = i;
        }

        public void run() {
            super.onTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), RefreshableListView.STATE_RELEASE, 0.0f, (float) (RefreshableListView.this.getHeight() - (this.val$_i * 50)), 0));
        }
    }

    /* renamed from: com.vkontakte.android.ui.RefreshableListView.4 */
    class C10484 implements Runnable {
        C10484() {
        }

        public void run() {
            super.onTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), RefreshableListView.STATE_PULL, 0.0f, (float) (RefreshableListView.this.getHeight() - 550), 0));
        }
    }

    /* renamed from: com.vkontakte.android.ui.RefreshableListView.5 */
    class C10495 implements Runnable {
        C10495() {
        }

        public void run() {
            if (RefreshableListView.this.listener != null) {
                RefreshableListView.this.subtext = RefreshableListView.this.listener.getLastUpdatedTime();
            }
            RefreshableListView.this.state = RefreshableListView.STATE_REFRESHING;
            RefreshableListView.this.refreshView.setVisibility(0);
            if (RefreshableListView.this.listener != null) {
                RefreshableListView.this.listener.onRefresh();
            }
            RefreshableListView.this.drawOffset = 0.0f;
            RefreshableListView.this.setSelection(0);
        }
    }

    /* renamed from: com.vkontakte.android.ui.RefreshableListView.6 */
    class C10506 implements Runnable {
        private final /* synthetic */ int val$pos;

        C10506(int i) {
            this.val$pos = i;
        }

        public void run() {
            RefreshableListView.this.setSelection(this.val$pos);
            RefreshableListView.this.scrollListener.onScrollStateChanged(RefreshableListView.this, 0);
        }
    }

    /* renamed from: com.vkontakte.android.ui.RefreshableListView.7 */
    class C10517 implements Runnable {
        private final /* synthetic */ int val$itemHeight;
        private final /* synthetic */ DecelerateInterpolator val$itrp;
        private final /* synthetic */ int val$pos;
        private final /* synthetic */ int val$startPos;
        private final /* synthetic */ long val$startTime;

        C10517(int i, int i2, long j, DecelerateInterpolator decelerateInterpolator, int i3) {
            this.val$pos = i;
            this.val$startPos = i2;
            this.val$startTime = j;
            this.val$itrp = decelerateInterpolator;
            this.val$itemHeight = i3;
        }

        public void run() {
            int ic = this.val$pos - this.val$startPos;
            float _t = Math.min(((float) (System.currentTimeMillis() - this.val$startTime)) / BitmapDescriptorFactory.HUE_MAGENTA, 1.0f);
            float t = this.val$itrp.getInterpolation(_t);
            int item = (int) Math.floor((double) (((float) ic) * t));
            RefreshableListView.this.setSelectionFromTop(this.val$startPos + item, -Math.round(((float) this.val$itemHeight) * ((((float) ic) * t) - ((float) item))));
            if (_t != 1.0f) {
                RefreshableListView.this.postOnAnimation(this);
            } else {
                RefreshableListView.this.scrollListener.onScrollStateChanged(RefreshableListView.this, 0);
            }
        }
    }

    public interface OnRefreshListener {
        String getLastUpdatedTime();

        void onRefresh();

        void onScrolled(float f);
    }

    public RefreshableListView(Context context) {
        super(context);
        this.drawOffset = 0.0f;
        this.draggingTop = false;
        this.needReturnTop = false;
        this.stoppedDraggingTop = false;
        this.animatingAfterRefresh = false;
        this.refreshViewHeight = 55.0f;
        this.state = STATE_PULL;
        this.arrowRotation = 0.0f;
        this.text = "Pull to refresh";
        this.subtext = ACRAConstants.DEFAULT_STRING_VALUE;
        this.listener = null;
        this.pullBG = -1381654;
        this.refreshEnabled = true;
        this.click = false;
        this.draggingEnabled = true;
        this.clickStartY = 0.0f;
        this.scrollState = 0;
        this.prevScrollState = 0;
        this.highlightAfterClick = true;
        this.drawShadow = false;
        this.eventCanceled = false;
        this.fromTop = true;
        this.offset = 0.0f;
        this.completelyDisableOverscroll = false;
        init();
    }

    public RefreshableListView(Context context, boolean fromTop) {
        super(context);
        this.drawOffset = 0.0f;
        this.draggingTop = false;
        this.needReturnTop = false;
        this.stoppedDraggingTop = false;
        this.animatingAfterRefresh = false;
        this.refreshViewHeight = 55.0f;
        this.state = STATE_PULL;
        this.arrowRotation = 0.0f;
        this.text = "Pull to refresh";
        this.subtext = ACRAConstants.DEFAULT_STRING_VALUE;
        this.listener = null;
        this.pullBG = -1381654;
        this.refreshEnabled = true;
        this.click = false;
        this.draggingEnabled = true;
        this.clickStartY = 0.0f;
        this.scrollState = 0;
        this.prevScrollState = 0;
        this.highlightAfterClick = true;
        this.drawShadow = false;
        this.eventCanceled = false;
        this.fromTop = true;
        this.offset = 0.0f;
        this.completelyDisableOverscroll = false;
        this.fromTop = fromTop;
        init();
    }

    public RefreshableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.drawOffset = 0.0f;
        this.draggingTop = false;
        this.needReturnTop = false;
        this.stoppedDraggingTop = false;
        this.animatingAfterRefresh = false;
        this.refreshViewHeight = 55.0f;
        this.state = STATE_PULL;
        this.arrowRotation = 0.0f;
        this.text = "Pull to refresh";
        this.subtext = ACRAConstants.DEFAULT_STRING_VALUE;
        this.listener = null;
        this.pullBG = -1381654;
        this.refreshEnabled = true;
        this.click = false;
        this.draggingEnabled = true;
        this.clickStartY = 0.0f;
        this.scrollState = 0;
        this.prevScrollState = 0;
        this.highlightAfterClick = true;
        this.drawShadow = false;
        this.eventCanceled = false;
        this.fromTop = true;
        this.offset = 0.0f;
        this.completelyDisableOverscroll = false;
        init();
    }

    public RefreshableListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.drawOffset = 0.0f;
        this.draggingTop = false;
        this.needReturnTop = false;
        this.stoppedDraggingTop = false;
        this.animatingAfterRefresh = false;
        this.refreshViewHeight = 55.0f;
        this.state = STATE_PULL;
        this.arrowRotation = 0.0f;
        this.text = "Pull to refresh";
        this.subtext = ACRAConstants.DEFAULT_STRING_VALUE;
        this.listener = null;
        this.pullBG = -1381654;
        this.refreshEnabled = true;
        this.click = false;
        this.draggingEnabled = true;
        this.clickStartY = 0.0f;
        this.scrollState = 0;
        this.prevScrollState = 0;
        this.highlightAfterClick = true;
        this.drawShadow = false;
        this.eventCanceled = false;
        this.fromTop = true;
        this.offset = 0.0f;
        this.completelyDisableOverscroll = false;
        init();
    }

    private void init() {
        this.itrp = new DecelerateInterpolator();
        this.density = getResources().getDisplayMetrics().density;
        this.refreshViewHeight *= this.density;
        this.touchslop = (float) ViewConfiguration.get(getContext()).getScaledTouchSlop();
        this.refreshView = inflate(getContext(), C0436R.layout.pull_to_refresh, null);
        this.refreshContainer = new FrameLayout(getContext());
        this.refreshContainer.addView(this.refreshView, new LayoutParams(-1, (int) this.refreshViewHeight));
        this.refreshView.setVisibility(8);
        this.refreshView.findViewById(C0436R.id.p2r_progressbar).setVisibility(0);
        this.refreshView.setBackgroundColor(-1381654);
        if (this.fromTop) {
            addHeaderView(this.refreshContainer);
        } else {
            addFooterView(this.refreshContainer);
        }
        Bitmap bitmap = ((BitmapDrawable) getResources().getDrawable(C0436R.drawable.ic_pull_arrow)).getBitmap();
        this.arrowFlipBitmap = bitmap;
        this.arrowBitmap = bitmap;
        this.textPaint = new Paint();
        this.textPaint.setAntiAlias(true);
        this.textPaint.setColor(-2141891243);
        this.textPaint.setTypeface(Typeface.create(Global.boldFont, STATE_PULL));
        this.textPaint.setTextSize(14.0f * this.density);
        this.subtextPaint = new Paint();
        this.subtextPaint.setAntiAlias(true);
        this.subtextPaint.setColor(-2141891243);
        this.subtextPaint.setTypeface(Global.regFont);
        this.subtextPaint.setTextSize((float) ((int) Math.ceil((double) (13.0f * this.density))));
        this.text = getResources().getString(this.fromTop ? C0436R.string.pull_to_refresh : C0436R.string.pull_to_refresh_bottom);
        this.bwdAnim = new MethodAnimation(this, "onAnimRotateBackward", null);
        this.bwdAnim.setDuration(150);
        this.bwdAnim.setInterpolator(new LinearInterpolator());
        this.fwdAnim = new MethodAnimation(this, "onAnimRotateForward", null);
        this.fwdAnim.setDuration(150);
        this.fwdAnim.setInterpolator(new LinearInterpolator());
        setDrawingCacheEnabled(false);
        setAnimationCacheEnabled(false);
        setScrollingCacheEnabled(false);
        setSelector(new ColorDrawable(0));
        try {
            Class[] clsArr = new Class[STATE_PULL];
            clsArr[0] = Integer.TYPE;
            this.msetOverScrollMode = getClass().getMethod("setOverScrollMode", clsArr);
        } catch (Exception e) {
        }
        this.shadowDrawable = getResources().getDrawable(C0436R.drawable.list_shadow);
        if (Build.MODEL.toLowerCase().contains("zte")) {
            this.completelyDisableOverscroll = true;
            enableOverscroll(false);
        }
        super.setOnScrollListener(this);
        super.setOnItemLongClickListener(this);
    }

    private void enableOverscroll(boolean enable) {
        int i = 0;
        if (this.msetOverScrollMode != null) {
            try {
                if (this.completelyDisableOverscroll) {
                    enable = false;
                }
                Method method = this.msetOverScrollMode;
                Object[] objArr = new Object[STATE_PULL];
                if (!enable) {
                    i = STATE_RELEASE;
                }
                objArr[0] = Integer.valueOf(i);
                method.invoke(this, objArr);
            } catch (Exception e) {
            }
        }
    }

    public boolean isRefreshing() {
        return this.state == STATE_REFRESHING;
    }

    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
        return false;
    }

    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        checkParentView(this);
    }

    private void checkParentView(View v) {
        if (v != null) {
            try {
                checkParentView((View) v.getParent());
            } catch (Exception e) {
            }
        }
    }

    public void setFromTop(boolean fromTop) {
        this.fromTop = fromTop;
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (this.draggingEnabled) {
            if (ev.getAction() == 0) {
                this.clickStartY = ev.getY();
                this.click = true;
                this.prevScrollState = this.scrollState;
                this.eventCanceled = false;
            }
            if (ev.getAction() == STATE_RELEASE) {
                if (Math.abs(this.clickStartY - ev.getY()) > this.touchslop) {
                    this.click = false;
                }
                boolean z;
                if (this.fromTop) {
                    if (!this.draggingTop && computeVerticalScrollOffset() == 0 && ((getChildCount() == 0 || getChildAt(0).getTop() >= 0) && !this.stoppedDraggingTop && this.dragStartY != -9000.0f && !this.animatingAfterRefresh && (!isFastScrollEnabled() || ev.getX() <= ((float) (getWidth() - Global.scale(BitmapDescriptorFactory.HUE_ORANGE)))))) {
                        enableOverscroll(false);
                        this.draggingTop = true;
                        this.dragStartY = ev.getY();
                        if (this.listener != null) {
                            this.subtext = this.listener.getLastUpdatedTime();
                        }
                    } else if (this.draggingTop) {
                        this.drawOffset = (ev.getY() - this.dragStartY) / ImageViewHolder.PaddingSize;
                        if (this.listener != null) {
                            this.listener.onScrolled(this.drawOffset);
                        }
                        if (this.drawOffset > this.refreshViewHeight && this.state == STATE_PULL) {
                            this.state = STATE_RELEASE;
                            this.text = getResources().getString(C0436R.string.release_to_refresh);
                            animateRotateForward();
                        }
                        if (this.drawOffset < this.refreshViewHeight && this.state == STATE_RELEASE) {
                            this.state = STATE_PULL;
                            this.text = getResources().getString(C0436R.string.pull_to_refresh);
                            animateRotateBackward();
                        }
                        if (this.drawOffset < 0.0f) {
                            this.drawOffset = 0.0f;
                            if (this.listener != null) {
                                this.listener.onScrolled(this.drawOffset);
                            }
                            this.draggingTop = false;
                            this.stoppedDraggingTop = true;
                            if (super.computeVerticalScrollExtent() < super.computeVerticalScrollRange()) {
                                enableOverscroll(true);
                            }
                        } else {
                            invalidate();
                            if (this.needReturnTop) {
                                return true;
                            }
                        }
                        if (((double) this.drawOffset) > ((double) this.touchslop) * 1.5d || this.needReturnTop) {
                            z = true;
                        } else {
                            z = false;
                        }
                        this.needReturnTop = z;
                    } else if (this.stoppedDraggingTop) {
                        this.stoppedDraggingTop = false;
                    }
                } else if (!this.draggingTop && super.computeVerticalScrollOffset() >= super.computeVerticalScrollRange() - super.computeVerticalScrollExtent() && !this.stoppedDraggingTop && this.dragStartY != -9000.0f && !this.animatingAfterRefresh) {
                    enableOverscroll(false);
                    this.draggingTop = true;
                    this.dragStartY = ev.getY();
                    if (this.listener != null) {
                        this.subtext = this.listener.getLastUpdatedTime();
                    }
                } else if (this.draggingTop) {
                    this.drawOffset = (ev.getY() - this.dragStartY) / ImageViewHolder.PaddingSize;
                    if (this.listener != null) {
                        this.listener.onScrolled(-this.drawOffset);
                    }
                    if ((-this.drawOffset) > this.refreshViewHeight && this.state == STATE_PULL) {
                        this.state = STATE_RELEASE;
                        this.text = getResources().getString(C0436R.string.release_to_refresh);
                        animateRotateForward();
                    }
                    if ((-this.drawOffset) < this.refreshViewHeight && this.state == STATE_RELEASE) {
                        this.state = STATE_PULL;
                        this.text = getResources().getString(C0436R.string.pull_to_refresh_bottom);
                        animateRotateBackward();
                    }
                    if (this.drawOffset > 0.0f) {
                        this.drawOffset = 0.0f;
                        if (this.listener != null) {
                            this.listener.onScrolled(this.drawOffset);
                        }
                        this.draggingTop = false;
                        this.stoppedDraggingTop = true;
                        if (super.computeVerticalScrollExtent() < super.computeVerticalScrollRange()) {
                            enableOverscroll(true);
                        }
                    } else {
                        invalidate();
                        if (this.needReturnTop) {
                            return true;
                        }
                    }
                    if (((double) (-this.drawOffset)) > ((double) this.touchslop) * 1.5d || this.needReturnTop) {
                        z = true;
                    } else {
                        z = false;
                    }
                    this.needReturnTop = z;
                } else if (this.stoppedDraggingTop) {
                    this.stoppedDraggingTop = false;
                }
            }
            if (ev.getAction() == STATE_REFRESHING && this.draggingTop) {
                this.drawOffset = 0.0f;
                this.dragStartY = 0.0f;
                this.draggingTop = false;
                this.state = STATE_PULL;
                invalidate();
                enableOverscroll(true);
                this.eventCanceled = true;
                super.onTouchEvent(ev);
                return true;
            }
            if (ev.getAction() == STATE_PULL && this.draggingTop) {
                onScrollStateChanged(this, 0);
                this.drawOffset = (ev.getY() - this.dragStartY) / ImageViewHolder.PaddingSize;
                if (this.fromTop) {
                    if (this.listener != null) {
                        this.listener.onScrolled(this.drawOffset);
                    }
                    if (this.drawOffset > this.touchslop) {
                        if (this.refreshEnabled && this.drawOffset > this.refreshViewHeight && this.state != STATE_REFRESHING) {
                            this.state = STATE_REFRESHING;
                            this.drawOffset -= this.refreshViewHeight;
                            if (this.listener != null) {
                                this.listener.onScrolled(this.drawOffset);
                            }
                            this.refreshView.setVisibility(0);
                            if (this.listener != null) {
                                this.listener.onRefresh();
                            }
                        }
                        this.draggingTop = false;
                        animateTopBounce();
                        this.needReturnTop = false;
                        return true;
                    }
                    this.draggingTop = false;
                    this.drawOffset = 0.0f;
                    if (this.listener != null) {
                        this.listener.onScrolled(this.drawOffset);
                    }
                    this.needReturnTop = false;
                } else {
                    if (this.listener != null) {
                        this.listener.onScrolled(this.drawOffset);
                    }
                    if ((-this.drawOffset) > this.touchslop) {
                        if (this.refreshEnabled && (-this.drawOffset) > this.refreshViewHeight && this.state != STATE_REFRESHING) {
                            this.state = STATE_REFRESHING;
                            this.drawOffset += this.refreshViewHeight;
                            if (this.listener != null) {
                                this.listener.onScrolled(this.drawOffset);
                            }
                            this.refreshView.setVisibility(0);
                            if (getFirstVisiblePosition() == 0 && getLastVisiblePosition() == getCount() - 1) {
                                try {
                                    this.refreshContainer.setPadding(0, getHeight() - getChildAt(getChildCount() - 1).getBottom(), 0, 0);
                                } catch (Exception e) {
                                }
                            }
                            if (this.listener != null) {
                                this.listener.onRefresh();
                            }
                        }
                        setSelection(getCount() - 1);
                        this.draggingTop = false;
                        animateTopBounce();
                        this.needReturnTop = false;
                        return true;
                    }
                    this.draggingTop = false;
                    this.drawOffset = 0.0f;
                    if (this.listener != null) {
                        this.listener.onScrolled(this.drawOffset);
                    }
                    this.needReturnTop = false;
                }
                enableOverscroll(true);
            }
            if (ev.getAction() == STATE_PULL) {
                this.dragStartY = 0.0f;
            }
            try {
                int i = VERSION.SDK_INT;
                return super.onTouchEvent(ev);
            } catch (Exception x) {
                Log.m527e("vk", "aw shit", x);
                return true;
            }
        }
        try {
            return super.onTouchEvent(ev);
        } catch (Throwable x2) {
            Log.m532w("vk", x2);
            return false;
        }
    }

    public void setOnItemLongClickListener(OnItemLongClickListener l) {
        this.longClickListener = l;
    }

    public void refreshDone() {
        refreshDone(true);
    }

    public void refreshDone(boolean animate) {
        if (this.state == STATE_REFRESHING) {
            this.state = STATE_PULL;
            this.text = getResources().getString(C0436R.string.pull_to_refresh);
            this.arrowRotation = 0.0f;
            this.refreshView.setVisibility(8);
            this.animatingAfterRefresh = true;
            if (this.draggingTop) {
                this.dragStartY = -9000.0f;
                this.draggingTop = false;
            }
            if (!this.fromTop) {
                this.refreshContainer.setPadding(0, 0, 0, 0);
                if (getLastVisiblePosition() == getCount() - 1 && animate) {
                    this.drawOffset = (float) (-this.refreshView.getHeight());
                    if (this.listener != null) {
                        this.listener.onScrolled(this.drawOffset);
                    }
                    animateTopBounce();
                    postDelayed(new C10462(), 400);
                    return;
                }
                this.animatingAfterRefresh = false;
            } else if (getFirstVisiblePosition() == 0 && animate) {
                this.drawOffset = (float) this.refreshView.getHeight();
                if (this.listener != null) {
                    this.listener.onScrolled(this.drawOffset);
                }
                animateTopBounce();
                postDelayed(new C10451(), 400);
            } else {
                this.animatingAfterRefresh = false;
            }
        }
    }

    public void animateTopBounce() {
        clearAnimation();
        this.animStartOffset = this.drawOffset;
        MethodAnimation ma = new MethodAnimation(this, "onAnim", null);
        ma.setDuration((long) Math.max(350, (int) this.drawOffset));
        ma.setInterpolator(new DecelerateInterpolator(ImageViewHolder.PaddingSize));
        startAnimation(ma);
    }

    public void setOffset(float o) {
        this.offset = o;
    }

    private void animateRotateForward() {
        startAnimation(this.fwdAnim);
    }

    private void animateRotateBackward() {
        startAnimation(this.bwdAnim);
    }

    public void onAnim(float time) {
        this.drawOffset = this.animStartOffset * this.itrp.getInterpolation(1.0f - time);
        if (this.listener != null) {
            this.listener.onScrolled(this.drawOffset);
        }
        invalidate();
    }

    public void onAnimRotateForward(float time) {
        this.arrowRotation = BitmapDescriptorFactory.HUE_CYAN * this.itrp.getInterpolation(time);
        invalidate();
    }

    public void onAnimRotateBackward(float time) {
        this.arrowRotation = BitmapDescriptorFactory.HUE_CYAN * (1.0f - this.itrp.getInterpolation(time));
        invalidate();
    }

    public void setTopColor(int c) {
        this.pullBG = c;
        this.refreshView.setBackgroundColor(c);
    }

    public void setArrowResource(int r, int rf) {
        this.arrowBitmap = BitmapFactory.decodeResource(getResources(), r);
        this.arrowFlipBitmap = BitmapFactory.decodeResource(getResources(), rf);
    }

    public void setArrowResource(int r) {
        setArrowResource(r, r);
    }

    public void setProgressResource(int r) {
        ((ProgressBar) this.refreshView.findViewById(C0436R.id.p2r_progressbar)).setIndeterminateDrawable(getResources().getDrawable(r));
    }

    public void setTextColor(int color) {
        this.subtextPaint.setColor(color);
        this.subtextPaint.setShadowLayer(0.0f, 0.0f, 0.0f, 0);
    }

    public void dispatchDraw(Canvas c) {
        if (this.drawOffset < 0.0f && this.fromTop) {
            this.drawOffset = 0.0f;
        }
        if (this.drawOffset > 0.0f && !this.fromTop) {
            this.drawOffset = 0.0f;
        }
        c.save();
        c.translate(0.0f, (float) Math.round(this.drawOffset));
        try {
            super.dispatchDraw(c);
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        Paint bgPaint = new Paint();
        bgPaint.setColor(this.pullBG);
        if (this.drawOffset > 0.0f) {
            c.drawRect(0.0f, -this.drawOffset, (float) c.getWidth(), 0.0f, bgPaint);
            if (this.refreshEnabled && this.state != STATE_REFRESHING) {
                Matrix arrowMatrix = new Matrix();
                arrowMatrix.postRotate(-this.arrowRotation, (float) (this.arrowBitmap.getWidth() / STATE_RELEASE), (float) (this.arrowBitmap.getHeight() / STATE_RELEASE));
                arrowMatrix.postTranslate((float) ((getWidth() / STATE_RELEASE) - (this.arrowBitmap.getWidth() / STATE_RELEASE)), -((this.refreshViewHeight / ImageViewHolder.PaddingSize) + ((float) (this.arrowBitmap.getHeight() / STATE_RELEASE))));
                c.drawBitmap(this.arrowRotation > 90.0f ? this.arrowFlipBitmap : this.arrowBitmap, arrowMatrix, null);
                if (this.subtext != null) {
                    c.drawText(this.subtext, ((float) (getWidth() / STATE_RELEASE)) - (this.subtextPaint.measureText(this.subtext) / ImageViewHolder.PaddingSize), (-((this.refreshViewHeight / ImageViewHolder.PaddingSize) + ((float) (this.arrowBitmap.getHeight() / STATE_RELEASE)))) - ((float) Global.scale(15.0f)), this.subtextPaint);
                }
            }
        }
        if (!this.fromTop && this.drawOffset < 0.0f) {
            c.translate(0.0f, -this.offset);
            c.drawRect(0.0f, (float) getHeight(), (float) getWidth(), ((float) getHeight()) - this.drawOffset, bgPaint);
            if (this.refreshEnabled && this.state != STATE_REFRESHING) {
                arrowMatrix = new Matrix();
                arrowMatrix.postRotate(-this.arrowRotation, (float) (this.arrowBitmap.getWidth() / STATE_RELEASE), (float) (this.arrowBitmap.getHeight() / STATE_RELEASE));
                arrowMatrix.postRotate(BitmapDescriptorFactory.HUE_CYAN, (float) (this.arrowBitmap.getWidth() / STATE_RELEASE), (float) (this.arrowBitmap.getHeight() / STATE_RELEASE));
                arrowMatrix.postTranslate((float) ((getWidth() / STATE_RELEASE) - (this.arrowBitmap.getWidth() / STATE_RELEASE)), (((float) getHeight()) + (this.refreshViewHeight / ImageViewHolder.PaddingSize)) - ((float) (this.arrowBitmap.getHeight() / STATE_RELEASE)));
                c.drawBitmap(this.arrowRotation > 0.0f ? this.arrowBitmap : this.arrowFlipBitmap, arrowMatrix, null);
                if (this.subtext != null) {
                    c.drawText(this.subtext, ((float) (getWidth() / STATE_RELEASE)) - (this.subtextPaint.measureText(this.subtext) / ImageViewHolder.PaddingSize), (((((float) getHeight()) + (this.refreshViewHeight / ImageViewHolder.PaddingSize)) + ((float) (this.arrowBitmap.getHeight() / STATE_RELEASE))) + ((float) Global.scale(15.0f))) + (-this.subtextPaint.ascent()), this.subtextPaint);
                }
            }
            c.translate(0.0f, this.offset);
        }
        if (this.state == STATE_REFRESHING) {
            c.drawRect(new Rect(0, this.refreshContainer.getTop(), this.refreshContainer.getLeft(), this.refreshContainer.getBottom()), bgPaint);
            c.drawRect(new Rect(this.refreshContainer.getRight(), this.refreshContainer.getTop(), c.getWidth(), this.refreshContainer.getBottom()), bgPaint);
        }
        c.restore();
        if (!this.drawShadow) {
            return;
        }
        if (this.drawOffset > 0.0f || this.state == STATE_REFRESHING) {
            this.shadowDrawable.setBounds(0, 0, getWidth(), Global.scale(6.0f));
            float a = Math.min(this.drawOffset / this.refreshViewHeight, 1.0f) * 255.0f;
            if (this.state == STATE_REFRESHING) {
                int i;
                if (computeVerticalScrollOffset() == 0) {
                    i = MotionEventCompat.ACTION_MASK;
                } else {
                    i = 0;
                }
                a = (float) i;
            }
            this.shadowDrawable.setAlpha((int) a);
            this.shadowDrawable.draw(c);
        }
    }

    public void setOnRefreshListener(OnRefreshListener l) {
        this.listener = l;
    }

    public float getRefreshOffset() {
        return this.drawOffset;
    }

    public int computeVerticalScrollExtent() {
        if (super.computeVerticalScrollExtent() < super.computeVerticalScrollRange()) {
            return SCROLLBAR_SIZE;
        }
        return super.computeVerticalScrollExtent();
    }

    public int computeVerticalScrollOffset() {
        if (this.fromTop) {
            return (int) (((float) ((super.computeVerticalScrollRange() - 200) + 100)) * (((float) Math.max(super.computeVerticalScrollOffset() - 100, 0)) / ((float) ((super.computeVerticalScrollRange() - super.computeVerticalScrollExtent()) - 100))));
        }
        float scp = ((float) Math.max(super.computeVerticalScrollOffset(), 0)) / ((float) ((super.computeVerticalScrollRange() - super.computeVerticalScrollExtent()) - 100));
        if (scp < GroundOverlayOptions.NO_DIMENSION) {
            scp = 1.0f;
        }
        return (int) (((float) (super.computeVerticalScrollRange() - 200)) * scp);
    }

    public void scrollDown() {
        int d = (int) ((1.0f / ((float) getCount())) * 10.0f);
        boolean rr = super.onTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), 0, 0.0f, (float) getHeight(), 0));
        for (int i = 0; i < 10; i += STATE_PULL) {
            postDelayed(new C10473(i), (long) (d * i));
        }
        postDelayed(new C10484(), (long) (d * 11));
    }

    public void setRefreshEnabled(boolean refreshEnabled) {
        this.refreshEnabled = refreshEnabled;
    }

    public boolean isRefreshEnabled() {
        return this.refreshEnabled;
    }

    public void setOnScrollListener(OnScrollListener l) {
        this.scrollListener = l;
    }

    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (this.scrollListener != null) {
            this.scrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
        }
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.scrollState = scrollState;
        if (this.scrollListener != null) {
            this.scrollListener.onScrollStateChanged(view, scrollState);
        }
    }

    public boolean isHighlightAfterClick() {
        return this.highlightAfterClick;
    }

    public void setHighlightAfterClick(boolean highlightAfterClick) {
        this.highlightAfterClick = highlightAfterClick;
    }

    public boolean isDraggingEnabled() {
        return this.draggingEnabled;
    }

    public void setDraggingEnabled(boolean draggingEnabled) {
        this.draggingEnabled = draggingEnabled;
    }

    public void refresh() {
        post(new C10495());
    }

    public boolean onItemLongClick(AdapterView<?> aview, View view, int pos, long id) {
        if (this.longClickListener == null || this.eventCanceled) {
            return false;
        }
        return this.longClickListener.onItemLongClick(aview, view, pos, id);
    }

    public void smoothScrollToPosition(int pos) {
        if (getFirstVisiblePosition() == pos) {
            setSelection(pos);
        } else if (VERSION.SDK_INT < 16) {
            this.scrollListener.onScrollStateChanged(this, STATE_RELEASE);
            smoothScrollToPositionFromTop(pos, 0, PhotoView.THUMB_ANIM_DURATION);
            postDelayed(new C10506(pos), 350);
        } else {
            DecelerateInterpolator itrp = new DecelerateInterpolator();
            long startTime = System.currentTimeMillis();
            int startPos = getFirstVisiblePosition();
            int avg = 0;
            for (int i = 0; i < getChildCount(); i += STATE_PULL) {
                avg += getChildAt(i).getHeight();
            }
            post(new C10517(pos, startPos, startTime, itrp, avg / getChildCount()));
        }
    }
}
