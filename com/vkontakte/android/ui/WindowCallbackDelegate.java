package com.vkontakte.android.ui;

import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window.Callback;
import android.view.WindowManager.LayoutParams;
import android.view.accessibility.AccessibilityEvent;

public class WindowCallbackDelegate implements Callback {
    public Callback f62o;

    public WindowCallbackDelegate(Callback orig) {
        this.f62o = orig;
    }

    public boolean dispatchGenericMotionEvent(MotionEvent event) {
        return this.f62o.dispatchGenericMotionEvent(event);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        return this.f62o.dispatchKeyEvent(event);
    }

    public boolean dispatchKeyShortcutEvent(KeyEvent event) {
        return this.f62o.dispatchKeyShortcutEvent(event);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent event) {
        return this.f62o.dispatchPopulateAccessibilityEvent(event);
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        return this.f62o.dispatchTouchEvent(event);
    }

    public boolean dispatchTrackballEvent(MotionEvent event) {
        return this.f62o.dispatchTrackballEvent(event);
    }

    public void onActionModeFinished(ActionMode mode) {
        this.f62o.onActionModeFinished(mode);
    }

    public void onActionModeStarted(ActionMode mode) {
        this.f62o.onActionModeStarted(mode);
    }

    public void onAttachedToWindow() {
        this.f62o.onAttachedToWindow();
    }

    public void onContentChanged() {
        this.f62o.onContentChanged();
    }

    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        return this.f62o.onCreatePanelMenu(featureId, menu);
    }

    public View onCreatePanelView(int featureId) {
        return this.f62o.onCreatePanelView(featureId);
    }

    public void onDetachedFromWindow() {
        this.f62o.onDetachedFromWindow();
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        return this.f62o.onMenuItemSelected(featureId, item);
    }

    public boolean onMenuOpened(int featureId, Menu menu) {
        return this.f62o.onMenuOpened(featureId, menu);
    }

    public void onPanelClosed(int featureId, Menu menu) {
        this.f62o.onPanelClosed(featureId, menu);
    }

    public boolean onPreparePanel(int featureId, View view, Menu menu) {
        return this.f62o.onPreparePanel(featureId, view, menu);
    }

    public boolean onSearchRequested() {
        return this.f62o.onSearchRequested();
    }

    public void onWindowAttributesChanged(LayoutParams attrs) {
        this.f62o.onWindowAttributesChanged(attrs);
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        this.f62o.onWindowFocusChanged(hasFocus);
    }

    public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
        return this.f62o.onWindowStartingActionMode(callback);
    }
}
