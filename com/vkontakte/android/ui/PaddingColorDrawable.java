package com.vkontakte.android.ui;

import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;

public class PaddingColorDrawable extends ColorDrawable {
    private int pad;

    public PaddingColorDrawable(int color, int padding) {
        super(color);
        this.pad = padding;
    }

    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(this.pad + left, top, right - this.pad, bottom);
    }

    public void setBounds(Rect rect) {
        Rect r = new Rect(rect);
        r.left += this.pad;
        r.right -= this.pad;
        super.setBounds(r);
    }
}
