package com.vkontakte.android.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.Layout.Alignment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.view.View.MeasureSpec;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.AudioAttachment;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.DialogEntry;
import com.vkontakte.android.DocumentAttachment;
import com.vkontakte.android.GeoAttachment;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.PhotoAttachment;
import com.vkontakte.android.PostAttachment;
import com.vkontakte.android.StickerAttachment;
import com.vkontakte.android.VideoAttachment;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import java.util.ArrayList;
import java.util.List;
import org.acra.ACRAConstants;

public class DialogEntryView extends View {
    private Drawable attachIcon;
    private Paint clearPaint;
    private String dir;
    private Paint dirPaint;
    private DialogEntry entry;
    private boolean isAccelerated;
    private long lastPressed;
    private Drawable onlineIcon;
    private Drawable onlineMobileIcon;
    private Paint onlinePaint;
    private Paint overPaint;
    private Bitmap photo;
    private Bitmap photo2;
    private Bitmap placeholder;
    private boolean search;
    private CharSequence text;
    private boolean textIsAttach;
    private Layout textLayout;
    private Paint textPaint;
    private String time;
    private Paint timePaint;
    private Paint titlePaint;
    private Paint unreadPaint;

    private static class LineBreaker {
        private float mLengthEllipsis;
        private float mLengthEllipsisMore;
        private float mLengthLastLine;
        private ArrayList<int[]> mLines;
        private boolean mRequiredEllipsis;

        public LineBreaker() {
            this.mRequiredEllipsis = false;
            this.mLines = new ArrayList();
        }

        public int breakText(String input, int maxWidth, TextPaint tp) {
            return breakText(input, null, null, -1, maxWidth, tp);
        }

        public int breakText(String input, String ellipsis, String ellipsisMore, int maxLines, int maxWidth, TextPaint tp) {
            this.mLines.clear();
            this.mRequiredEllipsis = false;
            this.mLengthLastLine = 0.0f;
            this.mLengthEllipsis = 0.0f;
            this.mLengthEllipsisMore = 0.0f;
            if (maxWidth == -1) {
                ArrayList arrayList = this.mLines;
                Object obj = new int[2];
                obj[1] = input.length();
                arrayList.add(obj);
                return (int) (tp.measureText(input) + 0.5f);
            }
            if (ellipsis != null) {
                this.mLengthEllipsis = tp.measureText(ellipsis);
            }
            if (ellipsisMore != null) {
                this.mLengthEllipsisMore = tp.measureText(ellipsisMore);
            }
            int posStartThisLine = -1;
            float lengthThisLine = 0.0f;
            boolean breakWords = true;
            int pos = 0;
            while (pos < input.length()) {
                if (posStartThisLine == -1) {
                    posStartThisLine = pos;
                }
                if (this.mLines.size() == maxLines) {
                    this.mRequiredEllipsis = true;
                    break;
                }
                float widthOfChar = tp.measureText(new StringBuilder(String.valueOf(input.charAt(pos))).toString());
                boolean newLineRequired = false;
                if (input.charAt(pos) == '\n') {
                    newLineRequired = true;
                    this.mLines.add(new int[]{posStartThisLine, pos - 1});
                } else if (lengthThisLine + widthOfChar >= ((float) maxWidth)) {
                    newLineRequired = true;
                    if (input.charAt(pos) == ' ' || !breakWords) {
                        pos--;
                        this.mLines.add(new int[]{posStartThisLine, pos});
                    } else {
                        while (input.charAt(pos) != ' ') {
                            pos--;
                            if (pos < 0) {
                                return 0;
                            }
                        }
                        this.mLines.add(new int[]{posStartThisLine, pos});
                    }
                }
                if (newLineRequired) {
                    posStartThisLine = -1;
                    lengthThisLine = 0.0f;
                    if (this.mLines.size() == maxLines - 1) {
                        maxWidth = (int) (((float) maxWidth) - (this.mLengthEllipsis + this.mLengthEllipsisMore));
                        breakWords = false;
                    }
                } else {
                    lengthThisLine += widthOfChar;
                    if (pos == input.length() - 1) {
                        this.mLines.add(new int[]{posStartThisLine, pos});
                    }
                }
                pos++;
            }
            if (this.mRequiredEllipsis) {
                int[] pairLast = (int[]) this.mLines.get(this.mLines.size() - 1);
                this.mLengthLastLine = tp.measureText(input.substring(pairLast[0], pairLast[1] + 1));
            }
            if (this.mLines.size() == 0) {
                return 0;
            }
            return this.mLines.size() == 1 ? (int) (tp.measureText(input) + 0.5f) : maxWidth;
        }

        public boolean getRequiredEllipsis() {
            return this.mRequiredEllipsis;
        }

        public List<int[]> getLines() {
            return this.mLines;
        }

        public float getLengthLastEllipsizedLine() {
            return this.mLengthLastLine;
        }

        public float getLengthLastEllipsizedLinePlusEllipsis() {
            return this.mLengthLastLine + this.mLengthEllipsis;
        }

        public float getLengthEllipsis() {
            return this.mLengthEllipsis;
        }

        public float getLengthEllipsisMore() {
            return this.mLengthEllipsisMore;
        }
    }

    public DialogEntryView(Context context) {
        super(context);
        this.isAccelerated = false;
        this.textIsAttach = false;
        this.isAccelerated = Global.isHardwareAccelerated(this);
        this.titlePaint = new Paint();
        this.titlePaint.setTextSize((float) Global.scale(GalleryPickerFooterView.BADGE_SIZE));
        this.titlePaint.setAntiAlias(true);
        this.titlePaint.setColor(Color.WINDOW_BACKGROUND_COLOR);
        this.textPaint = new Paint();
        this.textPaint.setTypeface(Global.regFont);
        this.textPaint.setTextSize((float) Global.scale(15.0f));
        this.textPaint.setColor(-7829368);
        this.textPaint.setAntiAlias(true);
        this.clearPaint = new Paint();
        if (this.isAccelerated) {
            this.clearPaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
        }
        this.clearPaint.setColor(Color.WINDOW_BACKGROUND_COLOR);
        this.clearPaint.setAntiAlias(true);
        this.overPaint = new Paint();
        if (this.isAccelerated) {
            this.overPaint.setXfermode(new PorterDuffXfermode(Mode.DST_OVER));
        }
        this.overPaint.setFilterBitmap(true);
        this.timePaint = new Paint();
        this.timePaint.setColor(-6118750);
        this.timePaint.setTextSize((float) Global.scale(13.0f));
        this.timePaint.setTypeface(Typeface.defaultFromStyle(2));
        this.timePaint.setAntiAlias(true);
        this.placeholder = Global.getResBitmap(getResources(), C0436R.drawable.user_placeholder);
        this.onlinePaint = new Paint();
        this.onlinePaint.setColor(-12607520);
        this.onlinePaint.setAntiAlias(true);
        this.onlinePaint.setTextSize((float) Global.scale(12.0f));
        this.unreadPaint = new Paint();
        this.unreadPaint.setColor(563063239);
        this.unreadPaint.setAntiAlias(true);
        this.onlineIcon = context.getResources().getDrawable(C0436R.drawable.ic_online);
        this.onlineMobileIcon = context.getResources().getDrawable(C0436R.drawable.ic_online_mobile);
    }

    public void setData(DialogEntry e, boolean s) {
        if (this.search != s) {
            requestLayout();
        }
        this.entry = e;
        this.search = s;
        this.time = Global.langDateShort(e.lastMessage.time);
        this.attachIcon = null;
        if (this.search) {
            this.dir = getResources().getString(e.lastMessage.out ? C0436R.string.msg_out : C0436R.string.msg_in);
        }
        updateText();
        invalidate();
        if (e.profile.uid > 2000000000) {
            setContentDescription(new StringBuilder(String.valueOf(getResources().getString(C0436R.string.notification_in_chat_summary, new Object[]{e.profile.fullName}))).append(" (").append(Global.langDate(getResources(), e.lastMessage.time)).append("): ").append(this.text).toString());
        } else {
            setContentDescription(new StringBuilder(String.valueOf(e.profile.fullName)).append(" (").append(Global.langDate(getResources(), e.lastMessage.time)).append("): ").append(this.text).toString());
        }
    }

    public void onMeasure(int wms, int hms) {
        setMeasuredDimension(MeasureSpec.getSize(wms), Global.scale(76.0f));
    }

    public void setBitmap(Bitmap b, int n) {
        if (n == 0) {
            this.photo = b;
        } else {
            this.photo2 = b;
        }
        invalidate();
    }

    public void updateText() {
        try {
            int i;
            int offset;
            if (this.entry.lastMessage.out || this.entry.profile.uid > 2000000000) {
                i = (!this.entry.lastMessage.out || this.entry.lastMessage.readState) ? 40 : 45;
                offset = Global.scale((float) i);
            } else {
                offset = 0;
            }
            int tfw = ((getWidth() - Global.scale(83.0f)) - (this.attachIcon != null ? this.attachIcon.getIntrinsicWidth() + Global.scale(5.0f) : 0)) - offset;
            if (tfw >= 0) {
                CharSequence dtext = this.entry.lastMessage.displayableText;
                if (dtext != null || this.entry.lastMessage.isServiceMessage) {
                    if (this.entry.lastMessage.isServiceMessage && this.entry.lastMessage.text != null) {
                        dtext = this.entry.lastMessage.getServiceMessageText(Friends.getFromAll(this.entry.lastMessage.sender), this.entry.lastMessage.extras.getString("action_user_name_acc"));
                    }
                    String attachText = null;
                    if (this.entry.lastMessage.attachments.size() > 0) {
                        Attachment att = (Attachment) this.entry.lastMessage.attachments.get(0);
                        if (att instanceof PhotoAttachment) {
                            attachText = getResources().getString(C0436R.string.photo);
                        } else if (att instanceof VideoAttachment) {
                            attachText = getResources().getString(C0436R.string.video);
                        } else if (att instanceof AudioAttachment) {
                            attachText = getResources().getString(C0436R.string.audio);
                        } else if (att instanceof DocumentAttachment) {
                            DocumentAttachment da = (DocumentAttachment) att;
                            Resources resources = getResources();
                            i = (da.url == null || da.url.length() == 0) ? C0436R.string.gift : C0436R.string.doc;
                            attachText = resources.getString(i);
                        } else if (att instanceof PostAttachment) {
                            attachText = getResources().getString(C0436R.string.attach_wall_post);
                        } else if (att instanceof GeoAttachment) {
                            attachText = getResources().getString(C0436R.string.place);
                        } else if (att instanceof StickerAttachment) {
                            attachText = getResources().getString(C0436R.string.sticker);
                        }
                    } else if (this.entry.lastMessage.fwdMessages != null && this.entry.lastMessage.fwdMessages.size() > 0) {
                        attachText = Global.langPlural(C0436R.array.num_attach_fwd_message, this.entry.lastMessage.fwdMessages.size(), getResources());
                    }
                    if (dtext.length() != 0 || attachText == null) {
                        this.textIsAttach = false;
                    } else {
                        dtext = attachText;
                        this.textIsAttach = true;
                    }
                    int i2;
                    SpannableStringBuilder builder;
                    if (offset > 0 || !(attachText == null || this.textIsAttach)) {
                        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(dtext);
                        for (i2 = 0; i2 < spannableStringBuilder.length(); i2++) {
                            if (spannableStringBuilder.charAt(i2) == '\n') {
                                spannableStringBuilder.replace(i2, i2 + 1, " ");
                            }
                        }
                        this.text = TextUtils.ellipsize(spannableStringBuilder, new TextPaint(this.textPaint), (float) tfw, TruncateAt.END);
                        if (this.text instanceof Spannable) {
                            for (Object s : ((Spannable) this.text).getSpans(0, this.text.length() - 1, URLSpan.class)) {
                                ((Spannable) this.text).removeSpan(s);
                            }
                        }
                        if (offset == 0 && attachText != null) {
                            builder = new SpannableStringBuilder();
                            builder.append(this.text);
                            CharSequence spannableString = new SpannableString(attachText);
                            spannableString.setSpan(new ForegroundColorSpan(-11635292), 0, spannableString.length(), 0);
                            builder.append("\n");
                            builder.append(spannableString);
                            this.text = builder;
                        }
                        this.textLayout = new StaticLayout(this.text, new TextPaint(this.textPaint), getWidth(), Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                        return;
                    }
                    builder = new SpannableStringBuilder();
                    builder.append(dtext);
                    int lc = 0;
                    int seq = 0;
                    for (i2 = 0; i2 < builder.length(); i2++) {
                        if (builder.charAt(i2) == '\n') {
                            if (lc > 0) {
                                builder.replace(i2, i2 + 1, seq == 0 ? " " : ACRAConstants.DEFAULT_STRING_VALUE);
                            }
                            lc++;
                            seq++;
                        } else {
                            seq = 0;
                        }
                    }
                    for (Object removeSpan : builder.getSpans(0, builder.length() - 1, URLSpan.class)) {
                        builder.removeSpan(removeSpan);
                    }
                    this.text = builder;
                    this.textLayout = new StaticLayout(this.text, new TextPaint(this.textPaint), tfw, Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                    SpannableStringBuilder builder2 = new SpannableStringBuilder();
                    for (i2 = 0; i2 < Math.min(2, this.textLayout.getLineCount()); i2++) {
                        CharSequence line = builder.subSequence(this.textLayout.getLineStart(i2), this.textLayout.getLineEnd(i2));
                        if (i2 == 1) {
                            line = TextUtils.ellipsize(line, new TextPaint(this.textPaint), (float) (tfw - Global.scale(12.0f)), TruncateAt.END);
                        }
                        builder2.append(line);
                    }
                    if (this.textLayout.getLineCount() > 2 && !builder2.toString().endsWith("...")) {
                        builder2.append("...");
                    }
                    this.textLayout = new StaticLayout(builder2, new TextPaint(this.textPaint), tfw, Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                }
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
            this.textLayout = new StaticLayout(ACRAConstants.DEFAULT_STRING_VALUE, new TextPaint(this.textPaint), 20, Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
        }
    }

    public void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed && this.entry != null) {
            updateText();
        }
        super.onLayout(changed, l, t, r, b);
    }

    public void onDraw(Canvas c) {
        int offset;
        c.save();
        int width = ((getWidth() - ((int) this.timePaint.measureText(this.time))) - Global.scale(10.0f)) - Global.scale(8.0f);
        int scale = (this.entry.profile.online <= 0 || this.entry.profile.uid >= 2000000000) ? 0 : Global.scale(10.0f);
        int maxTextW = width - scale;
        c.clipRect(new Rect(0, 0, maxTextW, getHeight()));
        c.drawText(this.entry.profile.fullName, (float) Global.scale(78.0f), (this.titlePaint.descent() - this.titlePaint.ascent()) + ((float) Global.scale(5.0f)), this.titlePaint);
        c.restore();
        this.textPaint.setColor(this.textIsAttach ? -11635292 : 1996488704);
        c.save();
        if (!(this.text instanceof Spannable) || ((ImageSpan[]) ((Spannable) this.text).getSpans(0, this.text.length(), ImageSpan.class)).length <= 0) {
        }
        if (this.entry.lastMessage.out || this.entry.profile.uid > 2000000000) {
            scale = (!this.entry.lastMessage.out || this.entry.lastMessage.readState) ? 40 : 45;
            offset = Global.scale((float) scale);
        } else {
            offset = 0;
        }
        if (!this.entry.lastMessage.readState) {
            if (this.entry.lastMessage.out) {
                c.drawRect(new RectF((float) ((offset - Global.scale(7.0f)) + Global.scale(78.0f)), (float) Global.scale(35.0f), (float) (getWidth() - Global.scale(5.0f)), (float) (Global.scale(35.0f) + Global.scale(33.0f))), this.unreadPaint);
            } else {
                c.drawRect(new Rect(0, 0, c.getWidth(), c.getHeight()), this.unreadPaint);
            }
        }
        if (this.textLayout != null) {
            if (offset > 0 || this.attachIcon != null) {
                c.translate((float) (((this.attachIcon != null ? this.attachIcon.getIntrinsicWidth() + Global.scale(5.0f) : 0) + Global.scale(78.0f)) + offset), ((this.titlePaint.descent() - this.titlePaint.ascent()) + ((float) (this.textLayout.getHeight() / this.textLayout.getLineCount()))) + ((float) Global.scale(6.0f)));
            } else {
                c.translate((float) Global.scale(78.0f), ((this.titlePaint.descent() - this.titlePaint.ascent()) + (((float) (this.textLayout.getHeight() / this.textLayout.getLineCount())) * 1.0f)) - ((float) Global.scale(5.0f)));
            }
            this.textLayout.getPaint().set(this.textPaint);
            this.textLayout.draw(c);
            c.restore();
        } else {
            Log.m526e("vk", "textLayout is null - DAFUQ?! " + this.entry.lastMessage);
        }
        if (offset > 0 && this.photo2 != null) {
            c.drawBitmap(this.photo2, null, new Rect(Global.scale(78.0f), Global.scale(35.0f), Global.scale(78.0f) + Global.scale(33.0f), Global.scale(35.0f) + Global.scale(33.0f)), null);
        }
        this.timePaint.setColor(1426063360);
        Rect tbounds = new Rect();
        this.timePaint.getTextBounds(this.time, 0, this.time.length(), tbounds);
        c.drawText(this.time, (float) ((getWidth() - tbounds.width()) - Global.scale(10.0f)), (this.timePaint.descent() - this.timePaint.ascent()) + ((float) Global.scale(8.0f)), this.timePaint);
        maxTextW -= Global.scale(78.0f);
        if (this.entry.profile.online > 0 && this.entry.profile.uid < 2000000000) {
            this.titlePaint.getTextBounds(this.entry.profile.fullName, 0, this.entry.profile.fullName.length(), tbounds);
            Drawable icon = this.entry.profile.online == 1 ? this.onlineIcon : this.onlineMobileIcon;
            int cx = (Math.min(tbounds.width(), maxTextW) + Global.scale(88.0f)) - (icon.getIntrinsicWidth() / 2);
            int cy = Global.scale(18.0f) - (icon.getIntrinsicHeight() / 2);
            icon.setBounds(cx, cy, icon.getIntrinsicWidth() + cx, icon.getIntrinsicHeight() + cy);
            icon.draw(c);
        }
        c.drawBitmap(this.photo != null ? this.photo : this.placeholder, null, new Rect(Global.scale(8.0f), Global.scale(8.0f), Global.scale(68.0f), Global.scale(68.0f)), this.overPaint);
    }
}
