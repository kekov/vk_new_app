package com.vkontakte.android.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class AudioMiniPlayerView extends LinearLayout {
    public AudioMiniPlayerView(Context context) {
        super(context);
    }

    public AudioMiniPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AudioMiniPlayerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
