package com.vkontakte.android.ui.posts;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageAttachment;

public class CommonAttachmentPostDisplayItem extends PostDisplayItem {
    public Attachment att;

    public CommonAttachmentPostDisplayItem(int _postID, int _postOwnerID, Attachment _att) {
        super(_postID, _postOwnerID);
        this.att = _att;
    }

    public int getType() {
        return 7;
    }

    public int getImageCount() {
        return this.att instanceof ImageAttachment ? 1 : 0;
    }

    public View getView(Context context, View reuse) {
        View view = reuse;
        View attachView = null;
        if (reuse == null) {
            view = new FrameLayout(context);
        } else {
            attachView = ((FrameLayout) view).getChildAt(0);
        }
        LayoutParams lp = new LayoutParams(-1, -2, 17);
        lp.leftMargin = Global.scale(5.0f);
        lp.rightMargin = Global.scale(5.0f);
        View av = this.att.getViewForList(context, attachView);
        if (attachView == null) {
            av.setLayoutParams(lp);
            ((FrameLayout) view).addView(av);
        }
        return view;
    }

    public String getImageURL(int image) {
        return this.att instanceof ImageAttachment ? ((ImageAttachment) this.att).getImageURL() : null;
    }

    public void setImage(int image, View view, Bitmap bmp, boolean fromCache) {
        if (!(this.att instanceof ImageAttachment)) {
            return;
        }
        if (bmp == null) {
            ((ImageAttachment) this.att).clearImage(view);
        } else {
            ((ImageAttachment) this.att).setImage(view, bmp, fromCache);
        }
    }
}
