package com.vkontakte.android.ui.posts;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.RepostActivity;
import com.vkontakte.android.api.WallLike;
import com.vkontakte.android.api.WallLike.Callback;
import com.vkontakte.android.ui.OverlayTextView;
import org.acra.ACRAConstants;

public class FooterPostDisplayItem extends PostDisplayItem {
    private OnClickListener commentsOnClick;
    public NewsEntry f157e;
    private boolean feedback;
    private OnClickListener likesOnClick;
    private boolean liking;
    private OnClickListener repostsOnClick;
    private boolean showLikes;

    /* renamed from: com.vkontakte.android.ui.posts.FooterPostDisplayItem.1 */
    class C10731 implements OnClickListener {
        C10731() {
        }

        public void onClick(View v) {
            Bundle args = new Bundle();
            args.putParcelable("entry", FooterPostDisplayItem.this.f157e);
            args.putBoolean("comment", true);
            Navigate.to("PostViewFragment", args, (Activity) v.getContext());
        }
    }

    /* renamed from: com.vkontakte.android.ui.posts.FooterPostDisplayItem.2 */
    class C10742 implements OnClickListener {
        C10742() {
        }

        public void onClick(View v) {
            FooterPostDisplayItem.this.like(!FooterPostDisplayItem.this.f157e.flag(8), v);
        }
    }

    /* renamed from: com.vkontakte.android.ui.posts.FooterPostDisplayItem.3 */
    class C10753 implements OnClickListener {
        C10753() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), RepostActivity.class);
            intent.putExtra("post", FooterPostDisplayItem.this.f157e);
            v.getContext().startActivity(intent);
        }
    }

    private static class ViewHolder {
        TextView comments;
        TextView likes;
        TextView reposts;

        private ViewHolder() {
        }
    }

    /* renamed from: com.vkontakte.android.ui.posts.FooterPostDisplayItem.4 */
    class C15594 implements Callback {
        private final /* synthetic */ int val$lOid;
        private final /* synthetic */ int val$lPid;
        private final /* synthetic */ boolean val$liked;
        private final /* synthetic */ View val$view;

        /* renamed from: com.vkontakte.android.ui.posts.FooterPostDisplayItem.4.1 */
        class C10761 implements Runnable {
            private final /* synthetic */ View val$view;

            C10761(View view) {
                this.val$view = view;
            }

            public void run() {
                FooterPostDisplayItem.this.like(FooterPostDisplayItem.this.f157e.flag(8), this.val$view);
            }
        }

        /* renamed from: com.vkontakte.android.ui.posts.FooterPostDisplayItem.4.2 */
        class C10772 implements Runnable {
            private final /* synthetic */ View val$view;

            C10772(View view) {
                this.val$view = view;
            }

            public void run() {
                FooterPostDisplayItem.this.getView(this.val$view.getContext(), (View) this.val$view.getParent());
            }
        }

        /* renamed from: com.vkontakte.android.ui.posts.FooterPostDisplayItem.4.3 */
        class C10783 implements Runnable {
            private final /* synthetic */ View val$view;

            C10783(View view) {
                this.val$view = view;
            }

            public void run() {
                Toast.makeText(this.val$view.getContext(), C0436R.string.error, 0).show();
                FooterPostDisplayItem.this.getView(this.val$view.getContext(), (View) this.val$view.getParent());
            }
        }

        C15594(View view, int i, int i2, boolean z) {
            this.val$view = view;
            this.val$lOid = i;
            this.val$lPid = i2;
            this.val$liked = z;
        }

        public void success(int likes, int retweets, int postID) {
            FooterPostDisplayItem.this.liking = false;
            NewsEntry _e = (NewsEntry) this.val$view.getTag(C0436R.id.wall_view_post);
            if (_e != null && this.val$lOid == _e.ownerID && this.val$lPid == _e.postID) {
                FooterPostDisplayItem.this.f157e.numLikes = likes;
                if (this.val$liked) {
                    FooterPostDisplayItem.this.f157e.numRetweets = retweets;
                }
                if (FooterPostDisplayItem.this.f157e.flag(8) != this.val$liked) {
                    this.val$view.post(new C10761(this.val$view));
                } else {
                    this.val$view.post(new C10772(this.val$view));
                }
            }
        }

        public void fail(int ecode, String emsg) {
            if (FooterPostDisplayItem.this.f157e != null && this.val$lOid == FooterPostDisplayItem.this.f157e.ownerID && this.val$lPid == FooterPostDisplayItem.this.f157e.postID) {
                NewsEntry newsEntry;
                if (this.val$liked) {
                    newsEntry = FooterPostDisplayItem.this.f157e;
                    newsEntry.numLikes--;
                } else {
                    newsEntry = FooterPostDisplayItem.this.f157e;
                    newsEntry.numLikes++;
                }
                FooterPostDisplayItem.this.f157e.flag(8, !this.val$liked);
                FooterPostDisplayItem.this.liking = false;
                this.val$view.post(new C10783(this.val$view));
            }
        }
    }

    public FooterPostDisplayItem(NewsEntry _e, boolean _feedback, boolean likes) {
        super(_e.postID, _e.ownerID);
        this.f157e = _e;
        this.commentsOnClick = new C10731();
        this.likesOnClick = new C10742();
        this.repostsOnClick = new C10753();
        this.showLikes = likes;
        this.feedback = _feedback;
    }

    public int getType() {
        return 1;
    }

    public int getImageCount() {
        return 0;
    }

    public View getView(Context context, View reuse) {
        ViewHolder holder;
        int i = 10;
        View view = reuse;
        if (reuse == null) {
            view = View.inflate(context, this.feedback ? C0436R.layout.news_item_footer_feedback : C0436R.layout.news_item_footer, null);
            holder = new ViewHolder();
            holder.likes = (TextView) view.findViewById(C0436R.id.post_likes);
            holder.comments = (TextView) view.findViewById(C0436R.id.post_comments);
            holder.reposts = (TextView) view.findViewById(C0436R.id.post_reposts);
            view.setTag(holder);
        }
        holder = (ViewHolder) view.getTag();
        int padContent = Global.scale(15.0f);
        int padEmpty = Global.scale(20.0f);
        if (this.f157e.numComments > 0) {
            holder.comments.setText(new StringBuilder(String.valueOf(this.f157e.numComments)).toString());
            holder.comments.setCompoundDrawablePadding(Global.scale(10.0f));
            holder.comments.setPadding(padContent, 0, padContent, 0);
        } else {
            holder.comments.setText(ACRAConstants.DEFAULT_STRING_VALUE);
            holder.comments.setCompoundDrawablePadding(0);
            holder.comments.setPadding(padEmpty, 0, padEmpty, 0);
        }
        if (this.f157e.numLikes > 0) {
            holder.likes.setText(new StringBuilder(String.valueOf(this.f157e.numLikes)).toString());
            holder.likes.setCompoundDrawablePadding(Global.scale(10.0f));
            holder.likes.setPadding(padContent, 0, padContent, 0);
        } else {
            holder.likes.setText(ACRAConstants.DEFAULT_STRING_VALUE);
            holder.likes.setCompoundDrawablePadding(0);
            holder.likes.setPadding(padEmpty, 0, padEmpty, 0);
        }
        if (this.f157e.numRetweets > 0) {
            holder.reposts.setText(new StringBuilder(String.valueOf(this.f157e.numRetweets)).toString());
            holder.reposts.setCompoundDrawablePadding(Global.scale(10.0f));
            holder.reposts.setPadding(padContent, 0, padContent, 0);
        } else {
            holder.reposts.setText(ACRAConstants.DEFAULT_STRING_VALUE);
            holder.reposts.setCompoundDrawablePadding(0);
            holder.reposts.setPadding(padEmpty, 0, padEmpty, 0);
        }
        if (this.showLikes) {
            holder.likes.setVisibility(0);
            holder.reposts.setVisibility(0);
        } else {
            holder.likes.setVisibility(4);
            holder.reposts.setVisibility(4);
        }
        holder.likes.setSelected(this.f157e.flag(8));
        holder.reposts.setSelected(this.f157e.flag(4));
        view.setTag(C0436R.id.wall_view_post, this.f157e);
        holder.likes.setOnClickListener(this.likesOnClick);
        holder.reposts.setOnClickListener(this.repostsOnClick);
        holder.comments.setOnClickListener(this.commentsOnClick);
        if (this.feedback) {
            int i2;
            if (this.f157e.flag(8)) {
                ((OverlayTextView) holder.likes).setBackgroundResource(C0436R.drawable.btn_post_act);
                ((OverlayTextView) holder.likes).setOverlay((int) C0436R.drawable.btn_post_act_hl);
                ((OverlayTextView) holder.likes).setTextColor(-1);
                ((OverlayTextView) holder.likes).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_post_liked, 0, 0, 0);
            } else {
                ((OverlayTextView) holder.likes).setBackgroundResource(C0436R.drawable.btn_post);
                ((OverlayTextView) holder.likes).setOverlay((int) C0436R.drawable.btn_post_hl);
                ((OverlayTextView) holder.likes).setTextColor(-6710887);
                ((OverlayTextView) holder.likes).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_post_like, 0, 0, 0);
            }
            TextView textView = holder.likes;
            if (this.f157e.numLikes > 0) {
                i2 = 10;
            } else {
                i2 = 15;
            }
            textView.setPadding(Global.scale((float) i2), 0, Global.scale((float) (this.f157e.numLikes > 0 ? 10 : 15)), 0);
            if (this.f157e.flag(4)) {
                ((OverlayTextView) holder.reposts).setBackgroundResource(C0436R.drawable.btn_post_act);
                ((OverlayTextView) holder.reposts).setOverlay((int) C0436R.drawable.btn_post_act_hl);
                ((OverlayTextView) holder.reposts).setTextColor(-1);
                ((OverlayTextView) holder.reposts).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_post_reposted, 0, 0, 0);
            } else {
                ((OverlayTextView) holder.reposts).setBackgroundResource(C0436R.drawable.btn_post);
                ((OverlayTextView) holder.reposts).setOverlay((int) C0436R.drawable.btn_post_hl);
                ((OverlayTextView) holder.reposts).setTextColor(-6710887);
                ((OverlayTextView) holder.reposts).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_post_repost, 0, 0, 0);
            }
            textView = holder.reposts;
            if (this.f157e.numRetweets > 0) {
                i2 = 10;
            } else {
                i2 = 15;
            }
            i2 = Global.scale((float) i2);
            if (this.f157e.numRetweets <= 0) {
                i = 15;
            }
            textView.setPadding(i2, 0, Global.scale((float) i), 0);
            ((OverlayTextView) holder.comments).setOverlay((int) C0436R.drawable.btn_post_hl);
        }
        return view;
    }

    public String getImageURL(int image) {
        return null;
    }

    public void setImage(int image, View view, Bitmap bmp, boolean fromCache) {
    }

    private void like(boolean liked, View view) {
        this.f157e.flag(8, liked);
        NewsEntry newsEntry;
        if (liked) {
            newsEntry = this.f157e;
            newsEntry.numLikes++;
        } else {
            newsEntry = this.f157e;
            newsEntry.numLikes--;
        }
        getView(view.getContext(), (View) view.getParent());
        if (!this.liking) {
            this.liking = true;
            boolean z = liked;
            int i = 0;
            new WallLike(z, this.f157e.ownerID, this.f157e.postID, false, this.f157e.type, i, ACRAConstants.DEFAULT_STRING_VALUE).setCallback(new C15594(view, this.f157e.ownerID, this.f157e.postID, liked)).exec();
        }
    }
}
