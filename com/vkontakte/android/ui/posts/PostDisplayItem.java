package com.vkontakte.android.ui.posts;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;

public abstract class PostDisplayItem {
    public static final int TYPE_ATTACH_CONTAINER = 9;
    public static final int TYPE_AUDIO_ATTACH = 6;
    public static final int TYPE_BUTTONS = 10;
    public static final int TYPE_COMMENT = 4;
    public static final int TYPE_COMMON_ATTACH = 7;
    public static final int TYPE_FOOTER = 1;
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_REPOST = 3;
    public static final int TYPE_SIGNATURE = 8;
    public static final int TYPE_TEXT = 2;
    public static final int TYPE_THUMBS_BLOCK = 5;
    public int bgType;
    public boolean clickable;
    public int postID;
    public int postOwnerID;

    public abstract int getImageCount();

    public abstract String getImageURL(int i);

    public abstract int getType();

    public abstract View getView(Context context, View view);

    public abstract void setImage(int i, View view, Bitmap bitmap, boolean z);

    public PostDisplayItem(int _postID, int _postOwnerID) {
        this.clickable = true;
        this.postID = _postID;
        this.postOwnerID = _postOwnerID;
    }
}
