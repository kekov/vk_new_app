package com.vkontakte.android.ui.posts;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.data.Posts;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import org.acra.ACRAConstants;

public class HeaderPostDisplayItem extends PostDisplayItem {
    public OnClickListener menuClickListener;
    private OnClickListener photoOnClick;
    public boolean photosMode;
    public NewsEntry post;
    public boolean showMenu;

    /* renamed from: com.vkontakte.android.ui.posts.HeaderPostDisplayItem.1 */
    class C10791 implements OnClickListener {
        C10791() {
        }

        public void onClick(View v) {
            Bundle args = new Bundle();
            args.putInt("id", HeaderPostDisplayItem.this.post.userID);
            Navigate.to("ProfileFragment", args, (Activity) v.getContext());
        }
    }

    private static class ViewHolder {
        View menuBtn;
        TextView name;
        ImageView photo;
        View profileBtn;
        TextView time;

        private ViewHolder() {
        }
    }

    public HeaderPostDisplayItem(NewsEntry _post, boolean _photosMode) {
        super(_post.postID, _post.ownerID);
        this.showMenu = true;
        this.photosMode = _photosMode;
        this.post = _post;
        this.photoOnClick = new C10791();
    }

    public int getType() {
        return 0;
    }

    public int getImageCount() {
        return 1;
    }

    public View getView(Context context, View reuse) {
        ViewHolder holder;
        View view = reuse;
        if (reuse == null) {
            view = View.inflate(context, C0436R.layout.news_item_header, null);
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(C0436R.id.poster_name_view);
            holder.time = (TextView) view.findViewById(C0436R.id.post_info_view);
            holder.photo = (ImageView) view.findViewById(C0436R.id.user_photo);
            holder.menuBtn = view.findViewById(C0436R.id.post_options_btn);
            holder.profileBtn = holder.photo;
            view.setTag(holder);
        }
        CharSequence infoHtml = ACRAConstants.DEFAULT_STRING_VALUE;
        if (!this.photosMode) {
            infoHtml = Global.langDateRelative(this.post.time, context.getResources());
        } else if (this.post.type == 7) {
            infoHtml = new StringBuilder(String.valueOf(Global.langPlural(this.post.f44f ? C0436R.array.photos_tagged_short_f : C0436R.array.photos_tagged_short_m, this.post.postID, context.getResources()))).append(", ").append(Global.langDateRelative(this.post.time, context.getResources())).toString();
        } else {
            infoHtml = new StringBuilder(String.valueOf(Global.langPlural(C0436R.array.photos, this.post.postID, context.getResources()))).append(", ").append(Global.langDateRelative(this.post.time, context.getResources())).toString();
        }
        if (this.post.flag(NewsEntry.FLAG_FRIENDS_ONLY) || this.post.flag(GLRenderBuffer.EGL_SURFACE_SIZE)) {
            SpannableStringBuilder bldr = new SpannableStringBuilder(infoHtml);
            Spannable sp = Factory.getInstance().newSpannable("F");
            Drawable d = context.getResources().getDrawable(this.post.flag(NewsEntry.FLAG_FRIENDS_ONLY) ? C0436R.drawable.ic_post_friendsonly : C0436R.drawable.ic_post_pinned);
            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
            sp.setSpan(new ImageSpan(d, 1), 0, 1, 0);
            bldr.append(" ");
            bldr.append(sp);
            infoHtml = bldr;
        }
        holder = (ViewHolder) view.getTag();
        holder.name.setText(this.post.userName);
        holder.time.setText(infoHtml);
        if (holder.menuBtn != null) {
            holder.menuBtn.setOnClickListener(this.menuClickListener);
            holder.menuBtn.setVisibility(this.showMenu ? 0 : 4);
        }
        holder.profileBtn.setOnClickListener(this.photoOnClick);
        if (this.post.type == 0) {
            Posts.trackPostView(this.post);
        }
        return view;
    }

    public String getImageURL(int image) {
        return this.post.userPhotoURL;
    }

    public void setImage(int image, View view, Bitmap bmp, boolean fromCache) {
        ViewHolder holder = (ViewHolder) view.getTag();
        if (bmp != null) {
            holder.photo.setImageBitmap(bmp);
        } else {
            holder.photo.setImageResource(this.post.userID > 0 ? C0436R.drawable.user_placeholder : C0436R.drawable.group_placeholder);
        }
    }
}
