package com.vkontakte.android.ui.posts;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.vkontakte.android.Global;
import com.vkontakte.android.SignatureLinkAttachment;

public class SignaturePostDisplayItem extends PostDisplayItem {
    public SignatureLinkAttachment att;

    public SignaturePostDisplayItem(int _postID, int _postOwnerID, SignatureLinkAttachment _att) {
        super(_postID, _postOwnerID);
        this.att = _att;
    }

    public int getType() {
        return 8;
    }

    public int getImageCount() {
        return 0;
    }

    public View getView(Context context, View reuse) {
        View view = reuse;
        View attachView = null;
        if (reuse == null) {
            view = new FrameLayout(context);
        } else {
            attachView = ((FrameLayout) view).getChildAt(0);
        }
        LayoutParams lp = new LayoutParams(-1, -2, 17);
        lp.leftMargin = Global.scale(5.0f);
        lp.rightMargin = Global.scale(5.0f);
        View av = this.att.getViewForList(context, attachView);
        if (attachView == null) {
            av.setLayoutParams(lp);
            ((FrameLayout) view).addView(av);
        }
        return view;
    }

    public String getImageURL(int image) {
        return null;
    }

    public void setImage(int image, View view, Bitmap bmp, boolean fromCache) {
    }
}
