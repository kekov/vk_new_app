package com.vkontakte.android.ui.posts;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.ui.FlowLayout;

public class CommentPostDisplayItem extends PostDisplayItem {
    public int numComments;
    public String text;
    public int time;
    public String userName;
    public String userPhoto;

    private static class ViewHolder {
        FlowLayout attachments;
        TextView name;
        ImageView photo;
        TextView text;
        TextView time;
        TextView title;

        private ViewHolder() {
        }
    }

    public CommentPostDisplayItem(int _postID, int _postOwnerID, String _text, String _userName, String _userPhoto, int _numComments, int _time) {
        super(_postID, _postOwnerID);
        this.text = _text;
        this.userName = _userName;
        this.userPhoto = _userPhoto;
        this.numComments = _numComments;
        this.time = _time;
    }

    public int getType() {
        return 4;
    }

    public int getImageCount() {
        return 1;
    }

    public View getView(Context context, View reuse) {
        ViewHolder holder;
        View view = reuse;
        if (reuse == null) {
            view = View.inflate(context, C0436R.layout.ncomment, null);
            holder = new ViewHolder();
            holder.title = (TextView) view.findViewById(C0436R.id.ncomm_ntext);
            holder.text = (TextView) view.findViewById(C0436R.id.ncomm_comment);
            holder.name = (TextView) view.findViewById(C0436R.id.ncomm_user);
            holder.time = (TextView) view.findViewById(C0436R.id.ncomm_time);
            holder.photo = (ImageView) view.findViewById(C0436R.id.ncomm_photo);
            holder.attachments = (FlowLayout) view.findViewById(C0436R.id.ncomm_attach_wrap);
            view.setTag(holder);
        }
        holder = (ViewHolder) view.getTag();
        holder.text.setText(Global.replaceEmoji(Global.replaceHTML(this.text).replaceAll("\\[id(\\d+)\\|([^\\]]+)\\]", "$2")));
        if (this.text.length() == 0) {
            holder.text.setVisibility(8);
        } else {
            holder.text.setVisibility(0);
        }
        holder.name.setText(this.userName);
        holder.time.setText(Global.langDateRelative(this.time, context.getResources()));
        if (this.numComments > 1) {
            holder.title.setText(Global.langPlural(C0436R.array.ncomments_last, this.numComments, context.getResources()));
        } else {
            holder.title.setText(C0436R.string.comments_one_comment);
        }
        return view;
    }

    public String getImageURL(int image) {
        return this.userPhoto;
    }

    public void setImage(int image, View view, Bitmap bmp, boolean fromCache) {
        ViewHolder holder = (ViewHolder) view.getTag();
        if (bmp != null) {
            holder.photo.setImageBitmap(bmp);
        } else {
            holder.photo.setImageResource(C0436R.drawable.user_placeholder);
        }
    }
}
