package com.vkontakte.android.ui.posts;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.FrameLayout;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageAttachment;
import com.vkontakte.android.ui.FlowLayout;
import java.util.ArrayList;
import java.util.Iterator;

public class AttachContainerPostDisplayItem extends PostDisplayItem {
    public ArrayList<Attachment> atts;

    private static class ViewHolder {
        FlowLayout flowLayout;

        private ViewHolder() {
        }
    }

    public AttachContainerPostDisplayItem(int _postID, int _postOwnerID, ArrayList<Attachment> _atts) {
        super(_postID, _postOwnerID);
        this.atts = _atts;
    }

    public int getType() {
        return 9;
    }

    public int getImageCount() {
        int count = 0;
        Iterator it = this.atts.iterator();
        while (it.hasNext()) {
            if (((Attachment) it.next()) instanceof ImageAttachment) {
                count++;
            }
        }
        return count;
    }

    public View getView(Context context, View reuse) {
        ViewHolder holder;
        View view = reuse;
        if (reuse == null) {
            view = new FrameLayout(context);
            FlowLayout fl = new FlowLayout(context);
            ((FrameLayout) view).addView(fl);
            holder = new ViewHolder();
            holder.flowLayout = fl;
            view.setTag(holder);
            fl.setPadding(Global.scale(5.0f), 0, Global.scale(5.0f), 0);
        }
        holder = (ViewHolder) view.getTag();
        for (int i = 0; i < holder.flowLayout.getChildCount(); i++) {
            View att = holder.flowLayout.getChildAt(i);
            if (att.getTag() != null && (att.getTag() instanceof String)) {
                Attachment.reuseView(att, att.getTag().toString());
            }
        }
        holder.flowLayout.removeAllViews();
        Iterator it = this.atts.iterator();
        while (it.hasNext()) {
            Attachment att2 = (Attachment) it.next();
            if (att2 instanceof ImageAttachment) {
                holder.flowLayout.addView(att2.getViewForList(context, null));
            }
        }
        it = this.atts.iterator();
        while (it.hasNext()) {
            att2 = (Attachment) it.next();
            if (!(att2 instanceof ImageAttachment)) {
                holder.flowLayout.addView(att2.getViewForList(context, null));
            }
        }
        return view;
    }

    public String getImageURL(int image) {
        int i = 0;
        Iterator it = this.atts.iterator();
        while (it.hasNext()) {
            Attachment att = (Attachment) it.next();
            if (att instanceof ImageAttachment) {
                if (i == image) {
                    return ((ImageAttachment) att).getImageURL();
                }
                i++;
            }
        }
        return null;
    }

    public void setImage(int image, View view, Bitmap bmp, boolean fromCache) {
        View av = ((ViewHolder) view.getTag()).flowLayout.getChildAt(image);
        if (bmp == null) {
            ((ImageAttachment) this.atts.get(image)).clearImage(av);
        } else {
            ((ImageAttachment) this.atts.get(image)).setImage(av, bmp, fromCache);
        }
    }
}
