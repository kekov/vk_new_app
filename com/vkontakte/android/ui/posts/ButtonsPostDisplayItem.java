package com.vkontakte.android.ui.posts;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.WallDelete;
import com.vkontakte.android.api.WallDelete.Callback;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.data.Posts;

public class ButtonsPostDisplayItem extends PostDisplayItem {
    private NewsEntry post;

    /* renamed from: com.vkontakte.android.ui.posts.ButtonsPostDisplayItem.1 */
    class C10701 implements OnClickListener {
        C10701() {
        }

        public void onClick(View v) {
            Group g = Groups.getById(-ButtonsPostDisplayItem.this.post.ownerID);
            Intent intent = new Intent(v.getContext(), NewPostActivity.class);
            intent.putExtra("edit", ButtonsPostDisplayItem.this.post);
            intent.putExtra("public", true);
            if (g != null) {
                intent.putExtra("group_title", g.name);
                intent.putExtra("group_photo", g.photo);
            }
            v.getContext().startActivity(intent);
        }
    }

    /* renamed from: com.vkontakte.android.ui.posts.ButtonsPostDisplayItem.2 */
    class C10722 implements OnClickListener {
        private final /* synthetic */ Context val$context;

        /* renamed from: com.vkontakte.android.ui.posts.ButtonsPostDisplayItem.2.1 */
        class C10711 implements DialogInterface.OnClickListener {
            private final /* synthetic */ Context val$context;

            C10711(Context context) {
                this.val$context = context;
            }

            public void onClick(DialogInterface dialog, int which) {
                ButtonsPostDisplayItem.this.deletePost(ButtonsPostDisplayItem.this.post, this.val$context);
            }
        }

        C10722(Context context) {
            this.val$context = context;
        }

        public void onClick(View v) {
            new Builder(v.getContext()).setTitle(C0436R.string.confirm).setMessage(C0436R.string.delete_confirm).setPositiveButton(C0436R.string.yes, new C10711(this.val$context)).setNegativeButton(C0436R.string.no, null).show();
        }
    }

    /* renamed from: com.vkontakte.android.ui.posts.ButtonsPostDisplayItem.3 */
    class C15583 implements Callback {
        private final /* synthetic */ Context val$context;
        private final /* synthetic */ NewsEntry val$e;

        C15583(NewsEntry newsEntry, Context context) {
            this.val$e = newsEntry;
            this.val$context = context;
        }

        public void success() {
            Intent intent = new Intent(Posts.ACTION_POST_DELETED_BROADCAST);
            intent.putExtra("owner_id", this.val$e.ownerID);
            intent.putExtra("post_id", this.val$e.postID);
            intent.putExtra("post", this.val$e);
            this.val$context.sendBroadcast(intent, permission.ACCESS_DATA);
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(this.val$context, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    public ButtonsPostDisplayItem(NewsEntry e) {
        super(e.postID, e.ownerID);
        this.post = e;
    }

    public int getType() {
        return 10;
    }

    public int getImageCount() {
        return 0;
    }

    public View getView(Context context, View reuse) {
        View v = reuse;
        if (v == null) {
            v = View.inflate(context, C0436R.layout.card_buttons, null);
        }
        v.findViewById(C0436R.id.friend_req_btn_add).setOnClickListener(new C10701());
        v.findViewById(C0436R.id.friend_req_btn_decline).setOnClickListener(new C10722(context));
        return v;
    }

    public String getImageURL(int image) {
        return null;
    }

    public void setImage(int image, View view, Bitmap bmp, boolean fromCache) {
    }

    private void deletePost(NewsEntry e, Context context) {
        try {
            new WallDelete(e.ownerID, e.postID, e.type).setCallback(new C15583(e, context)).wrapProgress(context).exec((Activity) context);
        } catch (Exception e2) {
        }
    }
}
