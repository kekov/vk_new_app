package com.vkontakte.android.ui.posts;

import android.content.Context;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;

public class TextPostDisplayItem extends PostDisplayItem {
    public boolean gray;
    public CharSequence text;

    private static class ViewHolder {
        TextView text;

        private ViewHolder() {
        }
    }

    public TextPostDisplayItem(int _postID, int _postOwnerID, CharSequence _text, boolean _gray) {
        super(_postID, _postOwnerID);
        this.text = _text;
        this.gray = _gray;
    }

    public int getType() {
        return 2;
    }

    public int getImageCount() {
        return 0;
    }

    public View getView(Context context, View reuse) {
        ViewHolder holder;
        View view = reuse;
        if (reuse == null) {
            view = View.inflate(context, C0436R.layout.news_item_text, null);
            holder = new ViewHolder();
            holder.text = (TextView) view.findViewById(C0436R.id.post_view);
            holder.text.setTextSize(1, 16.0f + (((float) Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("fontSize", "0"))) * ImageViewHolder.PaddingSize));
            view.setTag(holder);
        }
        holder = (ViewHolder) view.getTag();
        holder.text.setText(this.text);
        if (this.gray) {
            holder.text.setTextColor(1426063360);
        } else {
            holder.text.setTextColor(Color.WINDOW_BACKGROUND_COLOR);
        }
        return view;
    }

    public String getImageURL(int image) {
        return null;
    }

    public void setImage(int image, View view, Bitmap bmp, boolean fromCache) {
    }
}
