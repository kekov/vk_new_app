package com.vkontakte.android.ui.posts;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import com.vkontakte.android.Global;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;

public class PostHighlightDrawable extends Drawable {
    public int extendBottom;
    public int extendTop;
    private Paint paint;

    public PostHighlightDrawable() {
        this.extendTop = 0;
        this.extendBottom = 0;
        this.paint = new Paint();
        this.paint.setColor(1720505850);
        this.paint.setAntiAlias(true);
    }

    public void draw(Canvas canvas) {
        Rect bounds = copyBounds();
        bounds.top += Global.scale(8.0f);
        bounds.left += Global.scale(8.5f);
        bounds.right -= Global.scale(8.5f);
        bounds.bottom -= Global.scale(8.0f);
        canvas.drawRoundRect(new RectF(bounds), (float) Global.scale(ImageViewHolder.PaddingSize), (float) Global.scale(ImageViewHolder.PaddingSize), this.paint);
    }

    public void setBounds(Rect rect) {
        Rect nrect = new Rect(rect);
        nrect.bottom += this.extendBottom;
        nrect.top -= this.extendTop;
        super.setBounds(nrect);
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int alpha) {
        this.paint.setAlpha(Math.round(((float) alpha) * 0.4f));
    }

    public void setColorFilter(ColorFilter cf) {
    }
}
