package com.vkontakte.android.ui.posts;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import com.vkontakte.android.AlbumAttachment;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageAttachment;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.Photo;
import com.vkontakte.android.PhotoAttachment;
import com.vkontakte.android.ThumbAttachment;
import com.vkontakte.android.ViewUtils;
import com.vkontakte.android.fragments.PhotoViewerFragment;
import com.vkontakte.android.mediapicker.gl.GLFilterContext;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.ui.FlowLayout;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ThumbsBlockPostDisplayItem extends PostDisplayItem {
    public ArrayList<ThumbAttachment> atts;
    public boolean photosMode;
    public NewsEntry post;

    /* renamed from: com.vkontakte.android.ui.posts.ThumbsBlockPostDisplayItem.1 */
    class C10811 extends FrameLayout {
        C10811(Context $anonymous0) {
            super($anonymous0);
        }

        public void onMeasure(int wms, int hms) {
            super.onMeasure(wms, hms);
            setMeasuredDimension(MeasureSpec.getSize(wms), getMeasuredHeight());
        }
    }

    /* renamed from: com.vkontakte.android.ui.posts.ThumbsBlockPostDisplayItem.2 */
    class C10822 implements OnClickListener {
        private final /* synthetic */ View val$container;
        private final /* synthetic */ int val$index;
        private final /* synthetic */ ArrayList val$photos;

        C10822(int i, ArrayList arrayList, View view) {
            this.val$index = i;
            this.val$photos = arrayList;
            this.val$container = view;
        }

        public void onClick(View v) {
            ThumbsBlockPostDisplayItem.this.openPhotoList(this.val$index, v, this.val$photos, ThumbsBlockPostDisplayItem.this.post, (ViewGroup) this.val$container);
        }
    }

    private static class ViewHolder {
        FlowLayout flowLayout;

        private ViewHolder() {
        }
    }

    public ThumbsBlockPostDisplayItem(NewsEntry _post, List<ThumbAttachment> _atts, boolean _photosMode) {
        super(_post.postID, _post.ownerID);
        this.atts = new ArrayList();
        this.atts.addAll(_atts);
        this.photosMode = _photosMode;
        this.post = _post;
    }

    public int getType() {
        return 5;
    }

    public int getImageCount() {
        return this.atts.size();
    }

    public View getView(Context context, View reuse) {
        ViewHolder holder;
        int i;
        View view = reuse;
        if (reuse == null) {
            View fl = new C10811(context);
            FlowLayout f = new FlowLayout(context);
            LayoutParams lp = new LayoutParams(-2, -2, 17);
            lp.leftMargin = Global.scale(5.0f);
            lp.rightMargin = Global.scale(5.0f);
            f.setLayoutParams(lp);
            fl.addView(f);
            holder = new ViewHolder();
            holder.flowLayout = f;
            fl.setTag(holder);
            view = fl;
        }
        holder = (ViewHolder) view.getTag();
        for (i = 0; i < holder.flowLayout.getChildCount(); i++) {
            View att = holder.flowLayout.getChildAt(i);
            if (att.getTag() != null && (att.getTag() instanceof String)) {
                Attachment.reuseView(att, att.getTag().toString());
            }
        }
        ArrayList<Photo> photos = new ArrayList();
        Iterator it = this.atts.iterator();
        while (it.hasNext()) {
            ThumbAttachment att2 = (ThumbAttachment) it.next();
            if ((att2 instanceof PhotoAttachment) && !(att2 instanceof AlbumAttachment)) {
                photos.add(new Photo((PhotoAttachment) att2));
            }
        }
        holder.flowLayout.removeAllViews();
        i = 0;
        Iterator it2 = this.atts.iterator();
        while (it2.hasNext()) {
            att2 = (ThumbAttachment) it2.next();
            View av = ((Attachment) att2).getViewForList(context, null);
            if ((att2 instanceof PhotoAttachment) && !(att2 instanceof AlbumAttachment)) {
                av.setOnClickListener(new C10822(i, photos, holder.flowLayout));
            }
            i++;
            holder.flowLayout.addView(av);
        }
        return view;
    }

    public String getImageURL(int image) {
        return ((ImageAttachment) this.atts.get(image)).getImageURL();
    }

    public void setImage(int image, View view, Bitmap bmp, boolean fromCache) {
        View av = ((ViewHolder) view.getTag()).flowLayout.getChildAt(image);
        if (bmp == null) {
            ((ImageAttachment) this.atts.get(image)).clearImage(av);
        } else {
            ((ImageAttachment) this.atts.get(image)).setImage(av, bmp, fromCache);
        }
    }

    private void openPhotoList(int index, View view, ArrayList<Photo> photos, NewsEntry e, ViewGroup attachContainer) {
        for (int i = 0; i < Math.min(attachContainer.getChildCount(), photos.size()); i++) {
            int i2;
            View v = attachContainer.getChildAt(i);
            int[] pos = new int[2];
            v.getLocationOnScreen(pos);
            ((Photo) photos.get(i)).viewBounds = new Rect(pos[0], pos[1], pos[0] + v.getWidth(), pos[1] + v.getHeight());
            int top = ViewUtils.getViewOffset(v, (View) ((View) attachContainer.getParent()).getParent()).y;
            Photo photo = (Photo) photos.get(i);
            if (top < 0) {
                i2 = -top;
            } else {
                i2 = 0;
            }
            photo.viewClipTop = i2;
        }
        Drawable d;
        Bundle args;
        if (e == null || (!(e.type == 6 || e.type == 7 || e.type == 9) || e.postID <= 5)) {
            d = ((ImageView) attachContainer.getChildAt(index)).getDrawable();
            if (d != null && (d instanceof BitmapDrawable)) {
                PhotoViewerFragment.sharedThumb = ((BitmapDrawable) d).getBitmap();
            }
            args = new Bundle();
            args.putInt("orientation", view.getResources().getConfiguration().orientation);
            args.putParcelableArrayList("list", photos);
            args.putInt(GLFilterContext.AttributePosition, index);
            if (this.photosMode) {
                args.putInt("bg_color", Color.ALBUMS_LIST_BACKGROUND);
            }
            Navigate.to("PhotoViewerFragment", args, (Activity) view.getContext(), true, -1, -1);
            return;
        }
        args = new Bundle();
        d = ((ImageView) attachContainer.getChildAt(index)).getDrawable();
        if (d != null && (d instanceof BitmapDrawable)) {
            PhotoViewerFragment.sharedThumb = ((BitmapDrawable) d).getBitmap();
        }
        args.putInt("orientation", view.getResources().getConfiguration().orientation);
        args.putParcelableArrayList("list", photos);
        args.putInt(GLFilterContext.AttributePosition, index);
        args.putParcelable("feed_entry", e);
        if (this.photosMode) {
            args.putInt("bg_color", Color.ALBUMS_LIST_BACKGROUND);
        }
        Navigate.to("PhotoViewerFragment", args, (Activity) view.getContext(), true, -1, -1);
    }
}
