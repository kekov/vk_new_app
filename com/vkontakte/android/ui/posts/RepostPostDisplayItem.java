package com.vkontakte.android.ui.posts;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import org.acra.ACRAConstants;

public class RepostPostDisplayItem extends PostDisplayItem {
    private OnClickListener clickListener;
    public int origID;
    public int repostType;
    public int time;
    public int uid;
    public String userName;
    public String userPhoto;

    /* renamed from: com.vkontakte.android.ui.posts.RepostPostDisplayItem.1 */
    class C10801 implements OnClickListener {
        C10801() {
        }

        public void onClick(View v) {
            String type = "wall";
            if (RepostPostDisplayItem.this.repostType == 1) {
                type = "photo";
            }
            if (RepostPostDisplayItem.this.repostType == 2) {
                type = "video";
            }
            v.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("vkontakte://vk.com/" + type + RepostPostDisplayItem.this.uid + "_" + RepostPostDisplayItem.this.origID)));
        }
    }

    private static class ViewHolder {
        TextView name;
        ImageView photo;
        TextView time;

        private ViewHolder() {
        }
    }

    public RepostPostDisplayItem(int _postID, int _postOwnerID, String _userName, String _userPhoto, int _uid, int _time, int _origID, int _repostType) {
        super(_postID, _postOwnerID);
        this.clickListener = new C10801();
        this.userName = _userName;
        this.userPhoto = _userPhoto;
        this.uid = _uid;
        this.time = _time;
        this.origID = _origID;
        this.repostType = _repostType;
    }

    public int getType() {
        return 3;
    }

    public int getImageCount() {
        return 1;
    }

    public View getView(Context context, View reuse) {
        ViewHolder holder;
        View view = reuse;
        if (reuse == null) {
            view = View.inflate(context, C0436R.layout.news_item_repost, null);
            holder = new ViewHolder();
            holder.name = (TextView) view.findViewById(C0436R.id.post_retweet_name);
            holder.time = (TextView) view.findViewById(C0436R.id.post_retweet_time);
            holder.photo = (ImageView) view.findViewById(C0436R.id.post_retweet_photo);
            view.setTag(holder);
        }
        holder = (ViewHolder) view.getTag();
        holder.name.setText(this.userName);
        holder.name.setOnClickListener(this.clickListener);
        holder.time.setText(new StringBuilder(String.valueOf(Global.langDateRelative(this.time, context.getResources()))).append(this.repostType == 0 ? ACRAConstants.DEFAULT_STRING_VALUE : " " + context.getResources().getString(C0436R.string.ntf_to_post)).toString());
        return view;
    }

    public String getImageURL(int image) {
        return this.userPhoto;
    }

    public void setImage(int image, View view, Bitmap bmp, boolean fromCache) {
        ViewHolder holder = (ViewHolder) view.getTag();
        if (bmp != null) {
            holder.photo.setImageBitmap(bmp);
        } else {
            holder.photo.setImageResource(this.uid > 0 ? C0436R.drawable.user_placeholder : C0436R.drawable.group_placeholder);
        }
    }
}
