package com.vkontakte.android.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;
import android.widget.LinearLayout;

public class FullWidthLinearLayout extends LinearLayout {
    public FullWidthLinearLayout(Context context) {
        super(context);
    }

    public FullWidthLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FullWidthLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void onMeasure(int wms, int hms) {
        super.onMeasure(MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(wms), 1073741824), hms);
    }
}
