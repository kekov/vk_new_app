package com.vkontakte.android.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListAdapter;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.Photo;
import com.vkontakte.android.Photo.Image;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.PhotosGetAll;
import com.vkontakte.android.api.PhotosGetAll.Callback;
import com.vkontakte.android.mediapicker.gl.GLFilterContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class PhotoFeedView extends FrameLayout {
    private static final int FIRST_PADDING = 8;
    private static final float touchslop;
    private PhotoViewsAdapter adapter;
    private ArrayList<Photo> allPhotos;
    private Paint bitmapPaint;
    private boolean dataLoading;
    private ListImageLoaderWrapper imgLoader;
    private int imgLoaderPage;
    private DecelerateInterpolator interpolator;
    private HorizontalListView list;
    private boolean moreAvailable;
    private int offset;
    private int oid;
    private int page;
    private int pageWidth;
    private ArrayList<PhotoLayout> photos;
    private Paint placeholderPaint;
    private int startOffset;
    private int totalPhotos;
    private VelocityTracker tracker;
    private ArrayList<Photo> unsortedPhotos;

    /* renamed from: com.vkontakte.android.ui.PhotoFeedView.1 */
    class C10231 implements OnItemClickListener {
        C10231() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            Bundle args = new Bundle();
            args.putInt(GLFilterContext.AttributePosition, pos);
            args.putParcelableArrayList("list", PhotoFeedView.this.allPhotos);
            args.putInt("all_uid", PhotoFeedView.this.oid);
            args.putInt("total", PhotoFeedView.this.totalPhotos);
            Navigate.to("PhotoViewerFragment", args, (Activity) PhotoFeedView.this.getContext(), true, -1, -1);
        }
    }

    /* renamed from: com.vkontakte.android.ui.PhotoFeedView.2 */
    class C10242 implements OnScrollListener {
        C10242() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount > totalItemCount - 5 && !PhotoFeedView.this.dataLoading && PhotoFeedView.this.moreAvailable) {
                PhotoFeedView.this.preloadMorePhotos();
            }
        }
    }

    /* renamed from: com.vkontakte.android.ui.PhotoFeedView.3 */
    class C10253 implements Runnable {
        C10253() {
        }

        public void run() {
            PhotoFeedView.this.makePages(0);
        }
    }

    /* renamed from: com.vkontakte.android.ui.PhotoFeedView.4 */
    class C10264 implements Runnable {
        C10264() {
        }

        public void run() {
            PhotoFeedView.this.invalidate();
            PhotoFeedView.this.imgLoaderPage = PhotoFeedView.this.page;
            PhotoFeedView.this.adapter.notifyDataSetChanged();
            Log.m528i("vk", "Update photo feed images");
            PhotoFeedView.this.imgLoader.updateImages();
        }
    }

    private class FixedImageView extends ImageView {
        int f60h;
        int f61w;

        public FixedImageView(Context context) {
            super(context);
        }

        public void onMeasure(int wms, int hms) {
            setMeasuredDimension(this.f60h, this.f61w);
        }

        public void setImageBitmap(Bitmap bmp) {
            if (bmp != null) {
                Matrix m = new Matrix();
                m.postRotate(90.0f);
                bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), m, false);
            }
            super.setImageBitmap(bmp);
        }
    }

    private class PhotoLayout {
        int height;
        Bitmap image;
        Photo photo;
        int width;

        private PhotoLayout() {
        }
    }

    private class PhotoViewsAdapter extends BaseAdapter {
        private PhotoViewsAdapter() {
        }

        public int getCount() {
            return PhotoFeedView.this.photos.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            FixedImageView iv;
            if (convertView == null) {
                iv = new FixedImageView(PhotoFeedView.this.getContext());
                iv.setScaleType(ScaleType.CENTER_CROP);
            } else {
                iv = (FixedImageView) convertView;
            }
            PhotoLayout l = (PhotoLayout) PhotoFeedView.this.photos.get(position);
            iv.f61w = l.width;
            iv.f60h = l.height;
            if (PhotoFeedView.this.imgLoader.isAlreadyLoaded(l.photo.getImage('m').url)) {
                iv.setImageBitmap(PhotoFeedView.this.imgLoader.get(l.photo.getImage('m').url));
            } else {
                iv.setImageDrawable(new ColorDrawable(-921103));
            }
            return iv;
        }
    }

    /* renamed from: com.vkontakte.android.ui.PhotoFeedView.5 */
    class C15555 implements Callback {
        C15555() {
        }

        public void success(int total, Vector<Photo> photos) {
            boolean z = false;
            PhotoFeedView.this.dataLoading = false;
            PhotoFeedView.this.totalPhotos = total;
            PhotoFeedView.this.unsortedPhotos.addAll(photos);
            PhotoFeedView photoFeedView = PhotoFeedView.this;
            photoFeedView.offset = photoFeedView.offset + photos.size();
            PhotoFeedView.this.allPhotos.addAll(photos);
            PhotoFeedView.this.makePages(PhotoFeedView.this.getWidth());
            photoFeedView = PhotoFeedView.this;
            if (PhotoFeedView.this.allPhotos.size() < total) {
                z = true;
            }
            photoFeedView.moreAvailable = z;
        }

        public void fail(int ecode, String emsg) {
            PhotoFeedView.this.dataLoading = false;
        }
    }

    private class ImageAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.ui.PhotoFeedView.ImageAdapter.1 */
        class C10271 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C10271(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    ((ImageView) this.val$v).setImageBitmap(this.val$bitmap);
                }
            }
        }

        private ImageAdapter() {
        }

        public int getItemCount() {
            return PhotoFeedView.this.photos.size();
        }

        public int getImageCountForItem(int item) {
            return 1;
        }

        public String getImageURL(int item, int image) {
            return ((PhotoLayout) PhotoFeedView.this.photos.get(item)).photo.getImage('m').url;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            if (item >= PhotoFeedView.this.list.getFirstVisiblePosition() && item <= PhotoFeedView.this.list.getLastVisiblePosition()) {
                PhotoFeedView.this.post(new C10271(PhotoFeedView.this.list.getChildAt(item - PhotoFeedView.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    static {
        touchslop = (float) ViewConfiguration.get(VKApplication.context).getScaledTouchSlop();
    }

    public PhotoFeedView(Context context) {
        super(context);
        this.startOffset = 0;
        this.unsortedPhotos = new ArrayList();
        this.photos = new ArrayList();
        this.totalPhotos = 0;
        this.page = 0;
        this.pageWidth = 0;
        this.offset = 0;
        this.allPhotos = new ArrayList();
        this.interpolator = new DecelerateInterpolator(1.5f);
        this.imgLoaderPage = 0;
        this.dataLoading = false;
        this.moreAvailable = true;
        init();
    }

    public PhotoFeedView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.startOffset = 0;
        this.unsortedPhotos = new ArrayList();
        this.photos = new ArrayList();
        this.totalPhotos = 0;
        this.page = 0;
        this.pageWidth = 0;
        this.offset = 0;
        this.allPhotos = new ArrayList();
        this.interpolator = new DecelerateInterpolator(1.5f);
        this.imgLoaderPage = 0;
        this.dataLoading = false;
        this.moreAvailable = true;
        init();
    }

    public PhotoFeedView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.startOffset = 0;
        this.unsortedPhotos = new ArrayList();
        this.photos = new ArrayList();
        this.totalPhotos = 0;
        this.page = 0;
        this.pageWidth = 0;
        this.offset = 0;
        this.allPhotos = new ArrayList();
        this.interpolator = new DecelerateInterpolator(1.5f);
        this.imgLoaderPage = 0;
        this.dataLoading = false;
        this.moreAvailable = true;
        init();
    }

    private void init() {
        this.placeholderPaint = new Paint();
        this.placeholderPaint.setColor(-921103);
        this.bitmapPaint = new Paint();
        this.bitmapPaint.setAntiAlias(true);
        this.bitmapPaint.setFilterBitmap(true);
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.list = new HorizontalListView(getContext());
        HorizontalListView horizontalListView = this.list;
        ListAdapter photoViewsAdapter = new PhotoViewsAdapter();
        this.adapter = photoViewsAdapter;
        horizontalListView.setAdapter(photoViewsAdapter);
        this.list.setPadding(Global.scale(8.0f), 0, 0, 0);
        this.list.setClipToPadding(false);
        this.list.setDivider(new ColorDrawable(0));
        this.list.setDividerHeight(Global.scale(5.0f));
        this.list.setPadding(0, Global.scale(8.0f), 0, Global.scale(8.0f));
        if (VERSION.SDK_INT <= 10) {
            this.list.setCacheColorHint(-13551805);
            this.list.setBackgroundColor(-13551805);
        }
        addView(this.list);
        this.list.setOnItemClickListener(new C10231());
        this.imgLoader = new ListImageLoaderWrapper(new ImageAdapter(), this.list, null);
        this.imgLoader.setOnScrollListener(new C10242());
    }

    public void init(List<Photo> firstPhotos, int oid, int total) {
        this.unsortedPhotos.addAll(firstPhotos);
        this.oid = oid;
        this.offset = firstPhotos.size();
        this.allPhotos.addAll(firstPhotos);
        this.totalPhotos = total;
        makePages(getWidth());
    }

    private void makePages(int viewWidth) {
        Log.m525d("vk", "ViewWidth=" + viewWidth + ", unsorted size=" + this.unsortedPhotos.size());
        if (viewWidth == 0) {
            View v = ((Activity) getContext()).getWindow().getDecorView();
            Rect rect = new Rect();
            v.getGlobalVisibleRect(rect);
            viewWidth = rect.width();
            Log.m525d("vk", "NEW ViewWidth=" + viewWidth);
            if (viewWidth == 0) {
                post(new C10253());
                return;
            }
        }
        viewWidth = Math.round(((float) viewWidth) * 1.2f);
        this.pageWidth = viewWidth;
        float avgNphotos = (float) (viewWidth / Global.scale(80.0f));
        if (Global.isTablet) {
            avgNphotos *= 0.85f;
        }
        while (true) {
            if (this.unsortedPhotos.size() <= ((int) avgNphotos) && this.allPhotos.size() != this.totalPhotos) {
                break;
            }
            ArrayList<Float> ratios = new ArrayList();
            ArrayList<Float> ratiosCropped = new ArrayList();
            ArrayList<PhotoLayout> page = new ArrayList();
            Iterator it = this.unsortedPhotos.iterator();
            while (it.hasNext()) {
                Image im = ((Photo) it.next()).getImage('x');
                if (im == null) {
                    Log.m526e("vk", "x image is null");
                } else if (im.width <= 0 || im.height <= 0) {
                    ratios.add(Float.valueOf(1.2f));
                } else {
                    ratios.add(Float.valueOf(((float) im.width) / ((float) im.height)));
                }
            }
            if (ratios.size() == 0) {
                break;
            }
            Iterator it2 = ratios.iterator();
            while (it2.hasNext()) {
                ratiosCropped.add(Float.valueOf(Math.min(1.0f, ((Float) it2.next()).floatValue())));
            }
            float max_w = (float) (viewWidth - Global.scale(3.0f));
            float margin_w = (float) Global.scale(4.0f);
            float nphotos = (float) (viewWidth / Global.scale(80.0f));
            int i = 0;
            while (true) {
                if (((float) i) >= Math.min(nphotos, (float) ratios.size())) {
                    break;
                }
                float r = ((Float) ratios.get(i)).floatValue();
                if (((double) r) < 0.9d) {
                    nphotos = (float) (((double) nphotos) + 0.3d);
                }
                if (((double) r) <= 1.1d) {
                    if (((double) r) >= 0.9d) {
                        nphotos = (float) (((double) nphotos) + 0.2d);
                    }
                }
                i++;
            }
            if (max_w > ((float) Global.scale(800.0f))) {
                nphotos *= 0.85f;
            }
            float line_height_real = calculateMultiThumbsHeight(ratiosCropped.subList(0, Math.min(ratiosCropped.size(), Math.round(nphotos))), max_w, margin_w);
            float line_height = (float) Global.scale(70.0f);
            int maxphotos = Math.min(Math.round(nphotos), this.unsortedPhotos.size());
            if (this.unsortedPhotos.size() < Math.round(nphotos) && this.allPhotos.size() < this.totalPhotos) {
                break;
            }
            if (this.unsortedPhotos.size() == 0) {
                break;
            }
            if (Math.round(nphotos) > this.unsortedPhotos.size()) {
                max_w = (float) Math.round((float) (Global.scale(80.0f) * maxphotos));
                line_height_real = calculateMultiThumbsHeight(ratiosCropped.subList(0, Math.min(ratiosCropped.size(), Math.round(nphotos))), max_w, margin_w);
            }
            for (i = 0; i < maxphotos; i++) {
                int width = (int) (((Float) ratiosCropped.remove(0)).floatValue() * line_height_real);
                PhotoLayout layout = new PhotoLayout(null);
                layout.width = width;
                layout.height = (int) line_height;
                layout.photo = (Photo) this.unsortedPhotos.remove(0);
                page.add(layout);
            }
            this.photos.addAll(page);
            Log.m528i("vk", "Add page, size=" + page.size());
        }
        Log.m528i("vk", "photos : " + this.unsortedPhotos.size());
        updateEverything();
    }

    private float calculateMultiThumbsHeight(List<Float> ratios, float width, float margin) {
        return (width - (((float) (ratios.size() - 1)) * margin)) / sum(ratios);
    }

    private float sum(List<Float> a) {
        float sum = 0.0f;
        for (Float floatValue : a) {
            sum += floatValue.floatValue();
        }
        return sum;
    }

    private void updateEverything() {
        try {
            ((Activity) getContext()).runOnUiThread(new C10264());
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    private void preloadMorePhotos() {
        this.dataLoading = true;
        new PhotosGetAll(this.oid, this.offset, 25).setCallback(new C15555()).exec((View) this);
    }

    public void onLayout(boolean changed, int l, int t, int r, int b) {
        int w = r - l;
        int h = b - t;
        this.list.layout(0, ((-w) + h) - Global.scale(5.0f), w, h - Global.scale(5.0f));
    }

    public void onPause() {
        this.imgLoader.deactivate();
    }

    public void onResume() {
        this.imgLoader.activate();
    }

    public void onClick(int[] item) {
    }

    public void onMeasure(int wms, int hms) {
        super.onMeasure(wms, MeasureSpec.makeMeasureSpec(Global.scale(85.0f), 1073741824));
    }
}
