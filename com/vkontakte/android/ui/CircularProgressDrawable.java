package com.vkontakte.android.ui;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import com.vkontakte.android.Global;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;

public class CircularProgressDrawable extends Drawable {
    private Paint bgPaint;
    private boolean dimBg;
    private Paint erasePaint;
    private int level;
    private boolean pad;
    private Paint pbPaint;
    private Paint pbgPaint;

    public CircularProgressDrawable() {
        this.dimBg = true;
        this.pad = true;
        this.bgPaint = new Paint();
        this.bgPaint.setColor(1342177280);
        this.pbgPaint = new Paint();
        this.pbgPaint.setColor(1090519039);
        this.pbgPaint.setStyle(Style.STROKE);
        this.pbgPaint.setStrokeWidth((float) Global.scale(3.0f));
        this.pbgPaint.setAntiAlias(true);
        this.pbPaint = new Paint();
        this.pbPaint.setColor(-1);
        this.pbPaint.setStyle(Style.STROKE);
        this.pbPaint.setStrokeWidth((float) Global.scale(3.0f));
        this.pbPaint.setAntiAlias(true);
        this.erasePaint = new Paint();
        this.erasePaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
        this.erasePaint.setColor(-1);
        this.erasePaint.setAntiAlias(true);
    }

    public void setColors(int ringColor, int fillColor) {
        this.pbgPaint.setColor(ringColor);
        this.pbPaint.setColor(fillColor);
    }

    public void setDimBackground(boolean dim) {
        this.dimBg = dim;
    }

    public void setThickness(int t) {
        this.pbPaint.setStrokeWidth((float) Global.scale((float) t));
        this.pbgPaint.setStrokeWidth((float) Global.scale((float) t));
    }

    public void setPad(boolean p) {
        this.pad = p;
    }

    public boolean onLevelChange(int level) {
        this.level = level;
        invalidateSelf();
        return true;
    }

    public void draw(Canvas c) {
        float progr = ((float) getLevel()) / 10000.0f;
        Rect rect = copyBounds();
        int cs = this.pad ? (int) Math.min(((float) rect.width()) / ImageViewHolder.PaddingSize, (float) Global.scale(100.0f)) : rect.width() - ((int) this.pbPaint.getStrokeWidth());
        if (this.dimBg) {
            c.drawRect(rect, this.bgPaint);
        }
        RectF pb = new RectF((float) ((rect.left + (rect.width() / 2)) - (cs / 2)), (float) ((rect.top + (rect.height() / 2)) - (cs / 2)), (float) ((rect.left + (rect.width() / 2)) + (cs / 2)), (float) ((rect.top + (rect.height() / 2)) + (cs / 2)));
        c.drawArc(pb, 0.0f, 360.0f, false, this.pbgPaint);
        c.drawArc(pb, 0.0f, 360.0f * progr, false, this.pbPaint);
    }

    public int getIntrinsicWidth() {
        return -1;
    }

    public int getIntrinsicHeight() {
        return -1;
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int alpha) {
    }

    public void setColorFilter(ColorFilter cf) {
    }
}
