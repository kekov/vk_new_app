package com.vkontakte.android.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;

public class PhotoListItem extends FrameLayout {
    private long animStartTime;
    private Paint blackPaint;
    private int image;
    private ImageView img;
    private int item;
    private boolean reset;

    /* renamed from: com.vkontakte.android.ui.PhotoListItem.1 */
    class C10281 implements Runnable {
        C10281() {
        }

        public void run() {
            PhotoListItem.this.img.clearAnimation();
        }
    }

    public PhotoListItem(Context context) {
        super(context);
        this.animStartTime = 0;
        this.reset = true;
        this.blackPaint = new Paint();
        init();
    }

    public PhotoListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.animStartTime = 0;
        this.reset = true;
        this.blackPaint = new Paint();
        init();
    }

    public PhotoListItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.animStartTime = 0;
        this.reset = true;
        this.blackPaint = new Paint();
        init();
    }

    private void init() {
        setBackgroundColor(-14540254);
        this.img = new ImageView(getContext());
        this.img.setBackgroundColor(Color.WINDOW_BACKGROUND_COLOR);
        this.img.setVisibility(4);
        addView(this.img);
        this.blackPaint.setColor(Color.WINDOW_BACKGROUND_COLOR);
        setWillNotDraw(false);
    }

    public void setBitmapAnimated(Bitmap bmp) {
        this.img.setImageBitmap(bmp);
        this.img.setVisibility(0);
        this.img.clearAnimation();
        if (this.reset && getWidth() > 0) {
            this.animStartTime = System.currentTimeMillis();
            AlphaAnimation aa = new AlphaAnimation(0.0f, 1.0f);
            aa.setDuration(200);
            this.img.startAnimation(aa);
            postDelayed(new C10281(), 200);
            this.reset = false;
        }
    }

    public void setBitmap(Bitmap bmp) {
        this.img.clearAnimation();
        this.img.setVisibility(0);
        this.img.setImageBitmap(bmp);
        this.reset = false;
    }

    public Bitmap getBitmap() {
        Drawable d = this.img.getDrawable();
        if (d == null || !(d instanceof BitmapDrawable)) {
            return null;
        }
        return ((BitmapDrawable) d).getBitmap();
    }

    public void reset() {
        this.reset = true;
        this.img.clearAnimation();
        this.img.setImageBitmap(null);
        this.img.setVisibility(4);
    }

    public void onDraw(Canvas c) {
        super.onDraw(c);
        if (getPaddingTop() > 0) {
            c.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getPaddingTop(), this.blackPaint);
        }
    }
}
