package com.vkontakte.android.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings.Secure;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.AudioAttachment;
import com.vkontakte.android.AudioFile;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.DocumentChooserActivity;
import com.vkontakte.android.FragmentWrapperActivity;
import com.vkontakte.android.FwdMessagesAttachment;
import com.vkontakte.android.GeoAttachment;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImagePickerActivity;
import com.vkontakte.android.Log;
import com.vkontakte.android.Message;
import com.vkontakte.android.PhotoAttachment;
import com.vkontakte.android.SelectGeoPointActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.VideoAttachment;
import com.vkontakte.android.ViewUtils;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.VideoFile;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.ui.AttachmentsEditorView.Callback;
import java.util.ArrayList;
import java.util.Iterator;

public class WriteBar extends LinearLayout {
    private static final int AUDIO_RESULT = 10003;
    private static final int DOC_RESULT = 10004;
    private static final int LOCATION_RESULT = 10005;
    private static final int PHOTO_RESULT = 10001;
    private static final int VIDEO_RESULT = 10002;
    private AttachmentsEditorView attEditor;
    private View attachDivider;
    private SherlockFragment fragment;
    int[] icons;
    private boolean mapAllowed;
    private int maxAtts;
    private Runnable onUplDone;
    private Runnable onUplFail;

    /* renamed from: com.vkontakte.android.ui.WriteBar.1 */
    class C10631 implements OnClickListener {
        C10631() {
        }

        public void onClick(View v) {
            int[] l = new int[2];
            v.getLocationInWindow(l);
            WriteBar.this.openAttachMenu(83, l[0], Global.scale(32.5f), C0436R.drawable.attach_menu_border);
        }
    }

    /* renamed from: com.vkontakte.android.ui.WriteBar.3 */
    class C10653 implements OnPreDrawListener {
        C10653() {
        }

        public boolean onPreDraw() {
            WriteBar.this.getViewTreeObserver().removeOnPreDrawListener(this);
            ((ViewGroup) WriteBar.this.getParent()).setClipChildren(false);
            ((ViewGroup) WriteBar.this.getParent()).setClipToPadding(false);
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.ui.WriteBar.4 */
    class C10664 extends BaseAdapter {
        String[] opts;

        C10664() {
            this.opts = WriteBar.this.getResources().getStringArray(C0436R.array.message_attach_options);
        }

        public int getCount() {
            return (!WriteBar.this.mapAllowed || WriteBar.this.attEditor.containsGeoAttachment()) ? this.opts.length - 1 : this.opts.length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View view, ViewGroup parent) {
            if (view == null) {
                view = View.inflate(WriteBar.this.getContext(), C0436R.layout.listitem_attach, null);
            }
            ((ImageView) view.findViewById(C0436R.id.attahclist_icon)).setImageResource(WriteBar.this.icons[position]);
            ((TextView) view.findViewById(C0436R.id.attachlist_title)).setText(this.opts[position]);
            return view;
        }
    }

    /* renamed from: com.vkontakte.android.ui.WriteBar.5 */
    class C10675 implements OnTouchListener {
        private final /* synthetic */ Dialog val$dlg;

        C10675(Dialog dialog) {
            this.val$dlg = dialog;
        }

        public boolean onTouch(View v, MotionEvent event) {
            this.val$dlg.dismiss();
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.ui.WriteBar.6 */
    class C10696 implements OnItemClickListener {
        private final /* synthetic */ Dialog val$dlg;

        /* renamed from: com.vkontakte.android.ui.WriteBar.6.1 */
        class C10681 implements DialogInterface.OnClickListener {
            C10681() {
            }

            public void onClick(DialogInterface dialog, int which) {
                WriteBar.this.getContext().startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            }
        }

        C10696(Dialog dialog) {
            this.val$dlg = dialog;
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            if (WriteBar.this.attEditor.getCount() < WriteBar.this.maxAtts || pos == 4) {
                Intent intent;
                Bundle args;
                switch (pos) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                        intent = new Intent(WriteBar.this.getContext(), ImagePickerActivity.class);
                        intent.putExtra("limit", WriteBar.this.maxAtts - WriteBar.this.attEditor.getCount());
                        WriteBar.this.fragment.startActivityForResult(intent, WriteBar.PHOTO_RESULT);
                        break;
                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                        args = new Bundle();
                        args.putBoolean("select", true);
                        intent = new Intent(WriteBar.this.getContext(), FragmentWrapperActivity.class);
                        intent.putExtra("class", "AudioListFragment");
                        intent.putExtra("args", args);
                        WriteBar.this.fragment.startActivityForResult(intent, WriteBar.AUDIO_RESULT);
                        break;
                    case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                        args = new Bundle();
                        args.putBoolean("select", true);
                        intent = new Intent(WriteBar.this.getContext(), FragmentWrapperActivity.class);
                        intent.putExtra("class", "VideoListFragment");
                        intent.putExtra("args", args);
                        WriteBar.this.fragment.startActivityForResult(intent, WriteBar.VIDEO_RESULT);
                        break;
                    case Group.ADMIN_LEVEL_ADMIN /*3*/:
                        intent = new Intent(WriteBar.this.getContext(), DocumentChooserActivity.class);
                        intent.putExtra("limit", WriteBar.this.maxAtts - WriteBar.this.attEditor.getCount());
                        WriteBar.this.fragment.startActivityForResult(intent, WriteBar.DOC_RESULT);
                        break;
                    case UserListView.TYPE_FAVE /*4*/:
                        try {
                            String locationProviders = Secure.getString(WriteBar.this.getContext().getContentResolver(), "location_providers_allowed");
                            if (locationProviders != null && locationProviders.length() != 0) {
                                WriteBar.this.fragment.startActivityForResult(new Intent(WriteBar.this.getContext(), SelectGeoPointActivity.class), WriteBar.LOCATION_RESULT);
                                break;
                            }
                            new Builder(WriteBar.this.getContext()).setTitle(C0436R.string.location_disabled_title).setMessage(C0436R.string.location_disabled).setPositiveButton(C0436R.string.open_settings, new C10681()).setNegativeButton(C0436R.string.cancel, null).show();
                            return;
                        } catch (Exception e) {
                            Toast.makeText(WriteBar.this.getContext(), C0436R.string.error, 0).show();
                            break;
                        }
                        break;
                }
                this.val$dlg.dismiss();
                return;
            }
            Toast.makeText(WriteBar.this.getContext(), WriteBar.this.getResources().getString(C0436R.string.attachments_limit, new Object[]{Integer.valueOf(WriteBar.this.maxAtts)}), 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.ui.WriteBar.2 */
    class C15572 implements Callback {

        /* renamed from: com.vkontakte.android.ui.WriteBar.2.1 */
        class C10641 implements Runnable {
            C10641() {
            }

            public void run() {
                WriteBar.this.setAttEditorVisible(false);
            }
        }

        C15572() {
        }

        public void onAttachmentRemoved(Attachment att) {
            if (WriteBar.this.attEditor.getRealCount() != 0) {
                return;
            }
            if (VERSION.SDK_INT >= 14) {
                WriteBar.this.postDelayed(new C10641(), 300);
            } else {
                WriteBar.this.setAttEditorVisible(false);
            }
        }

        public void onAllUploadsDone() {
            if (WriteBar.this.onUplDone != null) {
                WriteBar.this.onUplDone.run();
                WriteBar writeBar = WriteBar.this;
                WriteBar.this.onUplFail = null;
                writeBar.onUplDone = null;
            }
        }

        public void onUploadFailed() {
            if (WriteBar.this.onUplFail != null) {
                WriteBar.this.onUplFail.run();
                WriteBar writeBar = WriteBar.this;
                WriteBar.this.onUplFail = null;
                writeBar.onUplDone = null;
            }
        }
    }

    public WriteBar(Context context) {
        super(context);
        this.maxAtts = 10;
        this.mapAllowed = true;
        this.icons = new int[]{C0436R.drawable.ic_attach_menu_photo, C0436R.drawable.ic_attach_menu_audio, C0436R.drawable.ic_attach_menu_video, C0436R.drawable.ic_attach_menu_document, C0436R.drawable.ic_attach_menu_location};
        init();
    }

    public WriteBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.maxAtts = 10;
        this.mapAllowed = true;
        this.icons = new int[]{C0436R.drawable.ic_attach_menu_photo, C0436R.drawable.ic_attach_menu_audio, C0436R.drawable.ic_attach_menu_video, C0436R.drawable.ic_attach_menu_document, C0436R.drawable.ic_attach_menu_location};
        init();
    }

    public WriteBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.maxAtts = 10;
        this.mapAllowed = true;
        this.icons = new int[]{C0436R.drawable.ic_attach_menu_photo, C0436R.drawable.ic_attach_menu_audio, C0436R.drawable.ic_attach_menu_video, C0436R.drawable.ic_attach_menu_document, C0436R.drawable.ic_attach_menu_location};
        init();
    }

    private void init() {
        setBackgroundColor(-1);
        setOrientation(1);
        addView(inflate(getContext(), C0436R.layout.write_bar, null));
        findViewById(C0436R.id.writebar_attach).setOnClickListener(new C10631());
        this.attachDivider = new View(getContext());
        this.attachDivider.setBackgroundColor(-2171170);
        LayoutParams lp = new LayoutParams(-1, Global.scale(1.0f));
        int scale = Global.scale(12.0f);
        lp.rightMargin = scale;
        lp.leftMargin = scale;
        addView(this.attachDivider, 0, lp);
        this.attEditor = new AttachmentsEditorView(getContext());
        addView(this.attEditor, 0, new LayoutParams(-1, Global.scale(91.0f)));
        setAttEditorVisible(false);
        this.attEditor.setCallback(new C15572());
        if (VERSION.SDK_INT >= 14) {
            getViewTreeObserver().addOnPreDrawListener(new C10653());
            ViewUtils.setNoClipRecursive(this);
        }
    }

    public void setFragment(SherlockFragment f) {
        this.fragment = f;
    }

    public void setUploadType(int type, int oid) {
        this.attEditor.uploadType = type;
        this.attEditor.uploadOwnerId = oid;
    }

    public String getText() {
        return ((EditText) findViewById(C0436R.id.writebar_edit)).getText().toString();
    }

    public boolean isTextEmpty() {
        return getText().length() == 0;
    }

    public boolean isUploading() {
        return this.attEditor.isUploading();
    }

    public void setText(CharSequence text) {
        ((EditText) findViewById(C0436R.id.writebar_edit)).setText(text);
    }

    public void openAttachMenu(int gravity, int x, int y) {
        openAttachMenu(gravity, x, y, C0436R.drawable.attach_menu_border);
    }

    public void openAttachMenu(int gravity, int x, int y, int bg) {
        if (this.attEditor.getCount() < this.maxAtts || this.mapAllowed) {
            ListAdapter attachAdapter = new C10664();
            Dialog dlg = new Dialog(getContext());
            dlg.getWindow().requestFeature(1);
            WindowManager.LayoutParams wlp = dlg.getWindow().getAttributes();
            wlp.gravity = gravity;
            wlp.x = x;
            wlp.y = y;
            wlp.width = Global.scale(195.0f);
            wlp.verticalMargin = 0.0f;
            wlp.horizontalMargin = 0.0f;
            dlg.getWindow().setAttributes(wlp);
            dlg.getWindow().getDecorView().setBackgroundResource(bg);
            dlg.getWindow().setSoftInputMode(0);
            dlg.getWindow().getDecorView().setOnTouchListener(new C10675(dlg));
            dlg.getWindow().setFlags(0, 2);
            dlg.setCanceledOnTouchOutside(true);
            ListView lv = new ListView(getContext());
            lv.setSelector(C0436R.drawable.highlight);
            lv.setAdapter(attachAdapter);
            lv.setOnItemClickListener(new C10696(dlg));
            dlg.setContentView(lv, new ViewGroup.LayoutParams(Global.scale(195.0f), -2));
            dlg.show();
            return;
        }
        Toast.makeText(getContext(), getResources().getString(C0436R.string.attachments_limit, new Object[]{Integer.valueOf(this.maxAtts)}), 0).show();
    }

    public ArrayList<Attachment> getAttachments() {
        return this.attEditor.getAll();
    }

    public void clearAttachments() {
        this.attEditor.clear();
        setAttEditorVisible(false);
    }

    public void addAttachment(Attachment att) {
        this.attEditor.add(att);
        setAttEditorVisible(true);
    }

    public void setAttachLimits(int max, boolean allowMap) {
        this.mapAllowed = allowMap;
        this.maxAtts = max;
    }

    private void setAttEditorVisible(boolean visible) {
        int i;
        int i2 = 0;
        AttachmentsEditorView attachmentsEditorView = this.attEditor;
        if (visible) {
            i = 0;
        } else {
            i = 8;
        }
        attachmentsEditorView.setVisibility(i);
        View view = this.attachDivider;
        if (!visible) {
            i2 = 8;
        }
        view.setVisibility(i2);
    }

    public void focus() {
        findViewById(C0436R.id.writebar_edit).requestFocus();
        ((EditText) findViewById(C0436R.id.writebar_edit)).setSelection(getText().length());
        ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(findViewById(C0436R.id.writebar_edit), 1);
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        Log.m528i("vk", "On activity result, req=" + reqCode + ", res=" + resCode + ", data=" + data);
        if (resCode == -1 && data != null) {
            Iterator it;
            Log.m528i("vk", "On activity result 2, req=" + reqCode + ", res=" + resCode + ", data=" + data);
            setAttEditorVisible(true);
            if (reqCode == AUDIO_RESULT) {
                this.attEditor.add(new AudioAttachment((AudioFile) data.getParcelableExtra("audio")));
            }
            if (reqCode == DOC_RESULT) {
                it = data.getParcelableArrayListExtra("documents").iterator();
                while (it.hasNext()) {
                    this.attEditor.add((Attachment) ((Parcelable) it.next()));
                }
            }
            if (reqCode == VIDEO_RESULT) {
                this.attEditor.add(new VideoAttachment((VideoFile) data.getParcelableExtra("video")));
            }
            if (reqCode == PHOTO_RESULT) {
                if (data.hasExtra("attachment")) {
                    this.attEditor.add((PhotoAttachment) data.getParcelableExtra("attachment"));
                } else if (data.hasExtra("files")) {
                    it = data.getStringArrayListExtra("files").iterator();
                    while (it.hasNext()) {
                        this.attEditor.add(new PendingPhotoAttachment((String) it.next()));
                    }
                } else {
                    this.attEditor.add(new PendingPhotoAttachment(data.getStringExtra("file")));
                }
            }
            if (reqCode == LOCATION_RESULT) {
                this.attEditor.add((GeoAttachment) data.getParcelableExtra("point"));
            }
        }
    }

    public void addFwdMessages(ArrayList<Message> msgs) {
        int i = 0;
        boolean found = false;
        Iterator it = this.attEditor.getAll().iterator();
        while (it.hasNext()) {
            if (((Attachment) it.next()) instanceof FwdMessagesAttachment) {
                found = true;
                break;
            }
            i++;
        }
        setAttEditorVisible(true);
        this.attEditor.add(new FwdMessagesAttachment(msgs));
        if (found) {
            this.attEditor.removeNoCallback(i);
        }
    }

    public void waitForUploads(Runnable onDone, Runnable onFailed) {
        if (this.attEditor.isUploading()) {
            this.onUplDone = onDone;
            this.onUplFail = onFailed;
            return;
        }
        onDone.run();
    }
}
