package com.vkontakte.android.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.ImageView;
import com.vkontakte.android.C0436R;

public class SquareImageView extends ImageView {
    private int opB;
    private int opL;
    private int opR;
    private int opT;
    private Drawable overlay;
    private int padding;

    public SquareImageView(Context context) {
        super(context);
        this.opL = -1;
        init();
    }

    public SquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.opL = -1;
        init();
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.opL = -1;
        init();
    }

    private void init() {
        this.overlay = getResources().getDrawable(C0436R.drawable.aplayer_cover_border);
        Rect pad = new Rect();
        this.overlay.getPadding(pad);
        setPadding(pad.left, pad.top, pad.right, pad.bottom);
    }

    public void onMeasure(int wms, int hms) {
        AlbumScrollView sv = (AlbumScrollView) ((View) getParent()).getParent();
        int sh = MeasureSpec.getSize(sv.heightMSpec) - sv.getChildAt(0).getPaddingTop();
        int sw = MeasureSpec.getSize(sv.widthMSpec);
        if (this.opL == -1) {
            this.opL = getPaddingLeft();
            this.opR = getPaddingRight();
            this.opT = getPaddingTop();
            this.opB = getPaddingBottom();
        }
        if (sh < sw) {
            setMeasuredDimension(sh, sh);
            this.padding = 0;
        } else {
            setMeasuredDimension(sw, sh);
            this.padding = (sh - sw) / 2;
        }
        setPadding(this.opL, this.opT + this.padding, this.opR, this.opB + this.padding);
    }

    public void onDraw(Canvas c) {
        c.save();
        c.clipRect(new Rect(getPaddingLeft(), getPaddingTop(), getWidth() - getPaddingRight(), getHeight() - getPaddingBottom()));
        try {
            super.onDraw(c);
        } catch (Exception e) {
        }
        c.restore();
        this.overlay.setBounds(0, getPaddingTop() - this.opT, getWidth(), getHeight() - (getPaddingBottom() - this.opB));
        this.overlay.draw(c);
    }
}
