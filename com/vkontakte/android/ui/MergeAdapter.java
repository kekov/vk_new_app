package com.vkontakte.android.ui;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;

public class MergeAdapter extends BaseAdapter implements SectionIndexer {
    protected String noItemsText;
    protected ArrayList<ListAdapter> pieces;

    private class CascadeDataSetObserver extends DataSetObserver {
        private CascadeDataSetObserver() {
        }

        public void onChanged() {
            MergeAdapter.this.notifyDataSetChanged();
        }

        public void onInvalidated() {
            MergeAdapter.this.notifyDataSetInvalidated();
        }
    }

    public MergeAdapter() {
        this.pieces = new ArrayList();
    }

    public void addAdapter(ListAdapter adapter) {
        this.pieces.add(adapter);
        adapter.registerDataSetObserver(new CascadeDataSetObserver());
    }

    public Object getItem(int position) {
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            ListAdapter piece = (ListAdapter) it.next();
            int size = piece.getCount();
            if (position < size) {
                return piece.getItem(position);
            }
            position -= size;
        }
        return null;
    }

    public void setNoItemsText(String text) {
        this.noItemsText = text;
    }

    public ListAdapter getAdapter(int position) {
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            ListAdapter piece = (ListAdapter) it.next();
            int size = piece.getCount();
            if (position < size) {
                return piece;
            }
            position -= size;
        }
        return null;
    }

    public int getCount() {
        int total = 0;
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            total += ((ListAdapter) it.next()).getCount();
        }
        if (total != 0 || this.noItemsText == null) {
            return total;
        }
        return 1;
    }

    public int getViewTypeCount() {
        int total = 0;
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            total += ((ListAdapter) it.next()).getViewTypeCount();
        }
        return Math.max(total, 1);
    }

    public int getItemViewType(int position) {
        int typeOffset = 0;
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            ListAdapter piece = (ListAdapter) it.next();
            int size = piece.getCount();
            if (position < size) {
                return typeOffset + piece.getItemViewType(position);
            }
            position -= size;
            typeOffset += piece.getViewTypeCount();
        }
        return -1;
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public boolean isEnabled(int position) {
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            ListAdapter piece = (ListAdapter) it.next();
            int size = piece.getCount();
            if (position < size) {
                return piece.isEnabled(position);
            }
            position -= size;
        }
        return false;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            ListAdapter piece = (ListAdapter) it.next();
            int size = piece.getCount();
            if (position < size) {
                return piece.getView(position, convertView, parent);
            }
            position -= size;
        }
        if (this.noItemsText == null) {
            return null;
        }
        TextView text = new TextView(parent.getContext());
        text.setText(this.noItemsText);
        return text;
    }

    public long getItemId(int position) {
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            ListAdapter piece = (ListAdapter) it.next();
            int size = piece.getCount();
            if (position < size) {
                return piece.getItemId(position);
            }
            position -= size;
        }
        return -1;
    }

    public int getPositionForSection(int section) {
        int position = 0;
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            ListAdapter piece = (ListAdapter) it.next();
            if (piece instanceof SectionIndexer) {
                Object[] sections = ((SectionIndexer) piece).getSections();
                int numSections = 0;
                if (sections != null) {
                    numSections = sections.length;
                }
                if (section < numSections) {
                    return ((SectionIndexer) piece).getPositionForSection(section) + position;
                }
                if (sections != null) {
                    section -= numSections;
                }
            }
            position += piece.getCount();
        }
        return 0;
    }

    public int getSectionForPosition(int position) {
        int section = 0;
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            ListAdapter piece = (ListAdapter) it.next();
            int size = piece.getCount();
            if (position >= size) {
                if (piece instanceof SectionIndexer) {
                    Object[] sections = ((SectionIndexer) piece).getSections();
                    if (sections != null) {
                        section += sections.length;
                    }
                }
                position -= size;
            } else if (piece instanceof SectionIndexer) {
                return ((SectionIndexer) piece).getSectionForPosition(position) + section;
            } else {
                return 0;
            }
        }
        return 0;
    }

    public Object[] getSections() {
        ArrayList<Object> sections = new ArrayList();
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            ListAdapter piece = (ListAdapter) it.next();
            if (piece instanceof SectionIndexer) {
                Object[] curSections = ((SectionIndexer) piece).getSections();
                if (curSections != null) {
                    for (Object section : curSections) {
                        sections.add(section);
                    }
                }
            }
        }
        if (sections.size() == 0) {
            return null;
        }
        return sections.toArray(new Object[0]);
    }
}
