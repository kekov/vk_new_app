package com.vkontakte.android.ui;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.LineBackgroundSpan;
import android.text.style.MetricAffectingSpan;

public class BadgeSpan extends MetricAffectingSpan implements LineBackgroundSpan {
    private Paint paint;

    public BadgeSpan() {
        this.paint = new Paint();
        this.paint.setColor(-16711936);
    }

    public void drawBackground(Canvas c, Paint p, int left, int right, int top, int baseline, int bottom, CharSequence text, int start, int end, int lnum) {
        c.drawRect(new Rect(left, top, right, bottom), this.paint);
    }

    public void updateMeasureState(TextPaint p) {
        p.setTypeface(Typeface.DEFAULT_BOLD);
    }

    public void updateDrawState(TextPaint tp) {
        tp.setTypeface(Typeface.DEFAULT_BOLD);
    }
}
