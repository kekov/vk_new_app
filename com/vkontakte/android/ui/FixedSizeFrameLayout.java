package com.vkontakte.android.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class FixedSizeFrameLayout extends FrameLayout {
    private int f55h;
    private int f56w;

    public FixedSizeFrameLayout(Context context) {
        super(context);
    }

    public FixedSizeFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FixedSizeFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setSize(int w, int h) {
        this.f56w = w;
        this.f55h = h;
        requestLayout();
    }

    public void onMeasure(int wms, int hms) {
        super.onMeasure(this.f56w | 1073741824, this.f55h | 1073741824);
    }
}
