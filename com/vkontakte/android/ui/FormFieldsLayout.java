package com.vkontakte.android.ui;

import android.content.Context;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FormFieldsLayout extends LinearLayout {
    private boolean isTablet;

    /* renamed from: com.vkontakte.android.ui.FormFieldsLayout.1 */
    class C09981 implements OnPreDrawListener {
        C09981() {
        }

        public boolean onPreDraw() {
            FormFieldsLayout.this.getViewTreeObserver().removeOnPreDrawListener(this);
            View title = FormFieldsLayout.this.getChildAt(0);
            if (title != null && (title instanceof TextView)) {
                ((TextView) title).setGravity(5);
            }
            return true;
        }
    }

    public FormFieldsLayout(Context context) {
        super(context);
        init();
    }

    public FormFieldsLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FormFieldsLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void init() {
        boolean z = VERSION.SDK_INT >= 14 && getResources().getConfiguration().isLayoutSizeAtLeast(3);
        this.isTablet = z;
        if (this.isTablet) {
            setOrientation(0);
            getViewTreeObserver().addOnPreDrawListener(new C09981());
            return;
        }
        setOrientation(1);
    }

    public void onMeasure(int wms, int hms) {
        if (this.isTablet) {
            wms = MeasureSpec.getMode(wms) | ((int) (((float) MeasureSpec.getSize(wms)) * 0.8f));
        }
        super.onMeasure(wms, hms);
    }
}
