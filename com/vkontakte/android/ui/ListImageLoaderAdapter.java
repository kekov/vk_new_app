package com.vkontakte.android.ui;

import android.graphics.Bitmap;

public abstract class ListImageLoaderAdapter {
    public abstract int getImageCountForItem(int i);

    public abstract String getImageURL(int i, int i2);

    public abstract int getItemCount();

    public abstract void imageLoaded(int i, int i2, Bitmap bitmap);
}
