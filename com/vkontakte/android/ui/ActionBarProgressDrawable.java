package com.vkontakte.android.ui;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;

public class ActionBarProgressDrawable extends Drawable {
    private static final long HEIGHT_ANIM_DURATION = 300;
    private static final long STEP_ANIM_DURATION = 300;
    private int animToStep;
    private Drawable bg;
    private long heightAnimStart;
    private Interpolator itrp;
    private Paint rectPaint;
    private int step;
    private long stepAnimStart;
    private int total;

    public ActionBarProgressDrawable() {
        this.step = 0;
        this.total = 1;
        this.animToStep = -1;
        this.bg = VKApplication.context.getResources().getDrawable(C0436R.drawable.bg_actionbar);
        this.rectPaint = new Paint();
        this.rectPaint.setColor(-1);
        this.itrp = new DecelerateInterpolator();
    }

    public void draw(Canvas canvas) {
        int height;
        int offset;
        Rect bnds = copyBounds();
        this.bg.setBounds(bnds);
        this.bg.draw(canvas);
        float stepSize = ((float) bnds.width()) / ((float) this.total);
        boolean animRunning = false;
        if (System.currentTimeMillis() - this.heightAnimStart < STEP_ANIM_DURATION) {
            height = Math.round(((float) Global.scale(ImageViewHolder.PaddingSize)) * this.itrp.getInterpolation(((float) (System.currentTimeMillis() - this.heightAnimStart)) / BitmapDescriptorFactory.HUE_MAGENTA));
            animRunning = true;
        } else {
            height = Global.scale(ImageViewHolder.PaddingSize);
        }
        if (System.currentTimeMillis() - this.stepAnimStart < STEP_ANIM_DURATION) {
            offset = Math.round((((float) this.step) * stepSize) + (this.itrp.getInterpolation(((float) (System.currentTimeMillis() - this.stepAnimStart)) / BitmapDescriptorFactory.HUE_MAGENTA) * ((((float) this.animToStep) * stepSize) - (((float) this.step) * stepSize))));
            animRunning = true;
        } else {
            if (this.animToStep != -1) {
                this.step = this.animToStep;
                this.animToStep = -1;
            }
            offset = Math.round(((float) this.step) * stepSize);
        }
        canvas.drawRect(new Rect(offset, bnds.height() - height, Math.round(((float) offset) + stepSize), bnds.height()), this.rectPaint);
        if (animRunning) {
            invalidateSelf();
        }
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int alpha) {
    }

    public void setColorFilter(ColorFilter cf) {
    }

    public void setStepCount(int count) {
        this.total = count;
        invalidateSelf();
    }

    public void setStep(int step) {
        this.step = step;
    }

    public void setStepAnimated(int step) {
        if (this.step != step) {
            this.animToStep = step;
            this.stepAnimStart = System.currentTimeMillis();
            invalidateSelf();
        }
    }

    public void animateHeight() {
        this.heightAnimStart = System.currentTimeMillis();
        invalidateSelf();
    }
}
