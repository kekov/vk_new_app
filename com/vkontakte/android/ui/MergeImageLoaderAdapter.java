package com.vkontakte.android.ui;

import android.graphics.Bitmap;
import java.util.ArrayList;
import java.util.Iterator;

public class MergeImageLoaderAdapter extends ListImageLoaderAdapter {
    private ArrayList<ListImageLoaderAdapter> pieces;

    public MergeImageLoaderAdapter() {
        this.pieces = new ArrayList();
    }

    public void addAdapter(ListImageLoaderAdapter adapter) {
        this.pieces.add(adapter);
    }

    public int getItemCount() {
        int total = 0;
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            total += ((ListImageLoaderAdapter) it.next()).getItemCount();
        }
        return total;
    }

    public int getImageCountForItem(int position) {
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            ListImageLoaderAdapter piece = (ListImageLoaderAdapter) it.next();
            int size = piece.getItemCount();
            if (position < size) {
                return piece.getImageCountForItem(position);
            }
            position -= size;
        }
        return 0;
    }

    public String getImageURL(int position, int image) {
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            ListImageLoaderAdapter piece = (ListImageLoaderAdapter) it.next();
            int size = piece.getItemCount();
            if (position < size) {
                return piece.getImageURL(position, image);
            }
            position -= size;
        }
        return null;
    }

    public void imageLoaded(int position, int image, Bitmap bitmap) {
        int _pos = position;
        Iterator it = this.pieces.iterator();
        while (it.hasNext()) {
            ListImageLoaderAdapter piece = (ListImageLoaderAdapter) it.next();
            int size = piece.getItemCount();
            if (position < size) {
                piece.imageLoaded(_pos, image, bitmap);
                return;
            }
            position -= size;
        }
    }
}
