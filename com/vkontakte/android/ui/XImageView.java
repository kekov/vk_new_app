package com.vkontakte.android.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

public class XImageView extends ImageView {
    public XImageView(Context context) {
        super(context);
    }

    public XImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public XImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
