package com.vkontakte.android.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import com.vkontakte.android.Global;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;

public class CircularProgressBar extends View {
    private final int IND_LOOP_TIME;
    private final int IND_TRANS_TIME;
    private double animFrom;
    private long animStartTime;
    private int animTime;
    private double animTo;
    private Paint bgPaint;
    private Paint blackBgPaint;
    private DecelerateInterpolator ditp;
    private Paint erasePaint;
    private MyInterpolator itp;
    private long pAnimStartTime;
    private Paint paint;
    private double progress;
    private int startAlpha;

    private class MyInterpolator implements Interpolator {
        private AccelerateInterpolator acc;
        private DecelerateInterpolator dec;

        public MyInterpolator() {
            this.dec = new DecelerateInterpolator(1.5f);
            this.acc = new AccelerateInterpolator(1.5f);
        }

        public float getInterpolation(float input) {
            if (((double) input) <= 0.5d) {
                return this.acc.getInterpolation(input * ImageViewHolder.PaddingSize) * 0.5f;
            }
            return (this.dec.getInterpolation((input - 0.5f) * ImageViewHolder.PaddingSize) * 0.5f) + 0.5f;
        }
    }

    public CircularProgressBar(Context context) {
        super(context);
        this.itp = new MyInterpolator();
        this.ditp = new DecelerateInterpolator(ImageViewHolder.PaddingSize);
        this.IND_LOOP_TIME = 3500;
        this.IND_TRANS_TIME = PhotoView.THUMB_ANIM_DURATION;
        this.animTime = 0;
        this.animStartTime = 0;
        this.startAlpha = 0;
        init();
    }

    public CircularProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.itp = new MyInterpolator();
        this.ditp = new DecelerateInterpolator(ImageViewHolder.PaddingSize);
        this.IND_LOOP_TIME = 3500;
        this.IND_TRANS_TIME = PhotoView.THUMB_ANIM_DURATION;
        this.animTime = 0;
        this.animStartTime = 0;
        this.startAlpha = 0;
        init();
    }

    public CircularProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.itp = new MyInterpolator();
        this.ditp = new DecelerateInterpolator(ImageViewHolder.PaddingSize);
        this.IND_LOOP_TIME = 3500;
        this.IND_TRANS_TIME = PhotoView.THUMB_ANIM_DURATION;
        this.animTime = 0;
        this.animStartTime = 0;
        this.startAlpha = 0;
        init();
    }

    private void init() {
        this.paint = new Paint();
        this.paint.setAntiAlias(true);
        this.paint.setColor(-1);
        this.paint.setStyle(Style.STROKE);
        this.paint.setStrokeWidth((float) Global.scale(6.0f));
        if (isInEditMode()) {
            this.progress = 0.45d;
        }
        this.bgPaint = new Paint();
        this.bgPaint.setAntiAlias(true);
        this.bgPaint.setColor(1627389951);
        this.bgPaint.setStyle(Style.STROKE);
        this.bgPaint.setStrokeWidth((float) Global.scale(6.0f));
        this.blackBgPaint = new Paint();
        this.blackBgPaint.setAntiAlias(true);
        this.blackBgPaint.setColor(536870912);
        this.blackBgPaint.setStyle(Style.STROKE);
        this.blackBgPaint.setStrokeWidth((float) Global.scale(10.0f));
        this.erasePaint = new Paint();
        this.erasePaint.setAntiAlias(true);
        this.erasePaint.setColor(Color.WINDOW_BACKGROUND_COLOR);
        this.erasePaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
    }

    public void setProgress(double p) {
        if (this.pAnimStartTime == 0) {
            this.animFrom = this.progress;
        }
        this.animTo = p;
        this.animTime = (int) Math.round((this.animTo - this.animFrom) * 1000.0d);
        if (p > 0.95d && ((long) this.animTime) - (System.currentTimeMillis() - this.pAnimStartTime) > 200) {
            this.animTime = (int) ((System.currentTimeMillis() - this.pAnimStartTime) + 200);
        }
        this.pAnimStartTime = System.currentTimeMillis();
        this.progress = p;
        if (p != 0.0d) {
            this.animStartTime = 0;
        }
        postInvalidate();
    }

    public void onDraw(Canvas c) {
        RectF arcRect = new RectF((float) Global.scale(5.0f), (float) Global.scale(5.0f), (float) (getWidth() - Global.scale(5.0f)), (float) (getHeight() - Global.scale(5.0f)));
        if (this.progress == 0.0d) {
            if (this.animStartTime == 0) {
                this.animStartTime = System.currentTimeMillis();
            }
            float at = ((float) ((int) (System.currentTimeMillis() - this.animStartTime))) / 3500.0f;
            c.drawArc(arcRect, 0.0f, 360.0f, false, this.blackBgPaint);
            this.bgPaint.setAlpha(yforx(at) + 46);
            c.drawArc(arcRect, 0.0f, 360.0f, false, this.bgPaint);
            postInvalidate();
            return;
        }
        boolean posted = false;
        if (this.bgPaint.getAlpha() != 96) {
            if (this.animStartTime == 0) {
                this.animStartTime = System.currentTimeMillis();
                this.startAlpha = this.bgPaint.getAlpha();
            }
            int animTime = (int) (System.currentTimeMillis() - this.animStartTime);
            float it = this.ditp.getInterpolation(((float) animTime) / 3500.0f);
            if (animTime >= PhotoView.THUMB_ANIM_DURATION) {
                this.animStartTime = 0;
                this.bgPaint.setAlpha(96);
            } else {
                this.bgPaint.setAlpha(this.startAlpha + ((int) (((float) (96 - this.startAlpha)) * it)));
                postInvalidate();
                posted = true;
            }
        }
        if (this.pAnimStartTime != 0) {
            long aTime = System.currentTimeMillis() - this.pAnimStartTime;
            if (aTime < ((long) this.animTime)) {
                double np = this.animFrom + ((this.animTo - this.animFrom) * ((double) this.ditp.getInterpolation(((float) aTime) / ((float) this.animTime))));
                if (np > this.progress) {
                    this.progress = np;
                }
            } else {
                this.pAnimStartTime = 0;
                this.progress = this.animTo;
            }
            if (!posted) {
                postInvalidate();
            }
        }
        c.drawArc(arcRect, 0.0f, 360.0f, false, this.blackBgPaint);
        c.drawArc(arcRect, 0.0f, 360.0f, false, this.bgPaint);
        c.drawArc(arcRect, -90.0f, (float) (this.progress * 360.0d), false, this.paint);
    }

    int yforx(float x) {
        return (int) Math.round((-94.0d * Math.abs(Math.sin(((double) x) * 3.141592653589793d))) + 100.0d);
    }
}
