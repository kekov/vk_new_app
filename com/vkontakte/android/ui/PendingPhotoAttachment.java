package com.vkontakte.android.ui;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.view.View;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.ThumbAttachment;
import com.vkontakte.android.UploaderService;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.mediapicker.providers.GalleryPickerProvider;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import java.io.DataOutputStream;
import java.io.IOException;

public class PendingPhotoAttachment extends Attachment implements ThumbAttachment {
    private boolean breakAfter;
    private int displayH;
    private int displayW;
    public String fileUri;
    private boolean floating;
    public int f155h;
    public int id;
    private boolean paddingAfter;
    public int f156w;

    /* renamed from: com.vkontakte.android.ui.PendingPhotoAttachment.1 */
    class C10221 extends View {
        C10221(Context $anonymous0) {
            super($anonymous0);
        }

        public void onMeasure(int wms, int hms) {
            setMeasuredDimension(PendingPhotoAttachment.this.displayW, PendingPhotoAttachment.this.displayH);
        }
    }

    public PendingPhotoAttachment(String file) {
        this.fileUri = file;
        this.id = UploaderService.getNewID();
        fillSize();
    }

    public PendingPhotoAttachment(String file, int id) {
        this.fileUri = file;
        this.id = id;
        fillSize();
    }

    private void fillSize() {
        try {
            Options opts = new Options();
            opts.inJustDecodeBounds = true;
            Uri u = Uri.parse(this.fileUri);
            if (GalleryPickerProvider.STYLED_IMAGE_URI_SCHEME.equals(u.getScheme())) {
                if (u.getQueryParameter("crop") != null) {
                    this.f155h = 500;
                    this.f156w = 500;
                    return;
                }
                u = GalleryPickerProvider.getOriginalUri(this.fileUri);
            }
            ParcelFileDescriptor fd = VKApplication.context.getContentResolver().openFileDescriptor(u, "r");
            BitmapFactory.decodeFileDescriptor(fd.getFileDescriptor(), null, opts);
            fd.close();
            this.f156w = opts.outWidth;
            this.f155h = opts.outHeight;
        } catch (Exception e) {
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
    }

    public View getFullView(Context context) {
        View v = new C10221(context);
        v.setBackgroundResource(C0436R.drawable.photo_placeholder);
        LayoutParams params = new LayoutParams(Global.scale(ImageViewHolder.PaddingSize), Global.scale((float) (this.paddingAfter ? 10 : 2)));
        if (this.breakAfter || this.floating) {
            params.breakAfter = this.breakAfter;
            params.floating = this.floating;
        }
        v.setLayoutParams(params);
        return v;
    }

    public View getViewForList(Context context, View reuse) {
        return getFullView(context);
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(14);
        os.writeUTF(this.fileUri);
        os.writeInt(this.id);
    }

    public float getRatio() {
        return ((float) this.f156w) / ((float) this.f155h);
    }

    public void setViewSize(float width, float height, boolean breakAfter, boolean floating) {
        this.displayW = (int) width;
        this.displayH = (int) height;
        this.breakAfter = breakAfter;
        this.floating = floating;
    }

    public void setPaddingAfter(boolean p) {
        this.paddingAfter = p;
    }

    public int getWidth() {
        return this.f156w;
    }

    public int getHeight() {
        return this.f155h;
    }

    public String getThumbURL() {
        return null;
    }

    public int getWidth(char size) {
        return this.f156w;
    }

    public LayoutParams getViewLayoutParams() {
        LayoutParams lp = new LayoutParams(Global.scale(ImageViewHolder.PaddingSize), Global.scale((float) (this.paddingAfter ? 10 : 2)));
        if (this.breakAfter || this.floating) {
            lp.breakAfter = this.breakAfter;
            lp.floating = this.floating;
        }
        lp.width = this.displayW;
        lp.height = this.displayH;
        return lp;
    }
}
