package com.vkontakte.android.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vkontakte.android.C0436R;

public class EmptyView extends LinearLayout {
    public EmptyView(Context context) {
        super(context);
    }

    public EmptyView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EmptyView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public static EmptyView create(Context context) {
        EmptyView r = (EmptyView) View.inflate(context, C0436R.layout.empty, null);
        r.setLayoutParams(new LayoutParams(-1, -2, 17));
        r.findViewById(C0436R.id.empty_button).setVisibility(8);
        return r;
    }

    public void setText(CharSequence text) {
        ((TextView) findViewById(C0436R.id.empty_text)).setText(text);
    }

    public void setText(int resID) {
        ((TextView) findViewById(C0436R.id.empty_text)).setText(resID);
    }

    public void setButtonText(CharSequence text) {
        ((TextView) findViewById(C0436R.id.empty_button)).setText(text);
    }

    public void setButtonText(int resID) {
        ((TextView) findViewById(C0436R.id.empty_button)).setText(resID);
    }

    public void setOnBtnClickListener(OnClickListener listener) {
        findViewById(C0436R.id.empty_button).setOnClickListener(listener);
    }

    public void setButtonVisible(boolean visible) {
        findViewById(C0436R.id.empty_button).setVisibility(visible ? 0 : 8);
    }
}
