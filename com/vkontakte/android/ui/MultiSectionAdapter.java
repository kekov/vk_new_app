package com.vkontakte.android.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.ui.PinnedHeaderListView.PinnedSectionedHeaderAdapter;
import java.util.HashMap;

public abstract class MultiSectionAdapter extends BaseAdapter implements PinnedSectionedHeaderAdapter {
    private HashMap<Integer, TextView> headerViews;

    public abstract int getItemCount(int i);

    public abstract long getItemId(int i, int i2);

    public abstract int getSectionCount();

    public abstract String getSectionTitle(int i);

    public abstract View getView(int i, int i2, View view);

    public abstract boolean isSectionHeaderVisible(int i);

    public MultiSectionAdapter() {
        this.headerViews = new HashMap();
    }

    public void notifyDataSetChanged() {
        this.headerViews.clear();
        super.notifyDataSetChanged();
    }

    public int[] resolveIndex(int item) {
        int[] result = new int[2];
        int ns = getSectionCount();
        int idx = 0;
        for (int i = 0; i < ns; i++) {
            if (isSectionHeaderVisible(i)) {
                if (idx == item) {
                    result[0] = i;
                    result[1] = -1;
                    break;
                }
                idx++;
            }
            int prevIdx = idx;
            idx += getItemCount(i);
            if (item >= prevIdx && item < idx && prevIdx != idx) {
                result[0] = i;
                result[1] = item - prevIdx;
                break;
            }
        }
        return result;
    }

    private TextView getSectionHeaderView(Context context) {
        return (TextView) View.inflate(context, getHeaderLayoutResource(), null);
    }

    public int getHeaderLayoutResource() {
        return C0436R.layout.list_section_header;
    }

    public int getCount() {
        int ns = getSectionCount();
        int cnt = 0;
        for (int i = 0; i < ns; i++) {
            if (isSectionHeaderVisible(i)) {
                cnt++;
            }
            cnt += getItemCount(i);
        }
        return cnt;
    }

    public Object getItem(int pos) {
        int[] idx = resolveIndex(pos);
        if (idx[1] == -1) {
            return null;
        }
        return getItem(idx[0], idx[1]);
    }

    public Object getItem(int section, int position) {
        return null;
    }

    public long getItemId(int pos) {
        int[] idx = resolveIndex(pos);
        if (idx[1] == -1) {
            return (long) (-pos);
        }
        return getItemId(idx[0], idx[1]);
    }

    public View getView(int pos, View view, ViewGroup group) {
        int[] idx = resolveIndex(pos);
        if (idx[1] == -1) {
            return getSectionHeaderView(idx[0], view, group);
        }
        return getView(idx[0], idx[1], view);
    }

    public int getItemViewType(int pos) {
        int[] s = resolveIndex(pos);
        if (s[1] == -1) {
            return 1;
        }
        return getItemViewType(s[0], s[1]);
    }

    public int getViewTypeCount() {
        return getExtraViewTypeCount() + 2;
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public boolean isEnabled(int pos) {
        int[] idx = resolveIndex(pos);
        if (idx[1] == -1) {
            return false;
        }
        return isEnabled(idx[0], idx[1]);
    }

    public boolean isEnabled(int section, int item) {
        return true;
    }

    public int getExtraViewTypeCount() {
        return 0;
    }

    public int getItemViewType(int section, int item) {
        return 0;
    }

    public boolean isSectionHeader(int pos) {
        return resolveIndex(pos)[1] == -1;
    }

    public int getSectionForPosition(int pos) {
        return resolveIndex(pos)[0];
    }

    public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {
        if (this.headerViews.containsKey(Integer.valueOf(section))) {
            return (View) this.headerViews.get(Integer.valueOf(section));
        }
        View view;
        if (convertView == null) {
            view = getSectionHeaderView(parent.getContext());
            view.setLayoutParams(new LayoutParams(-1, -2));
        } else {
            TextView view2 = (TextView) convertView;
        }
        if (convertView != null) {
            try {
                if (this.headerViews.containsValue(convertView) && this.headerViews.get(Integer.valueOf(section)) != convertView) {
                    for (Integer intValue : this.headerViews.keySet()) {
                        int i = intValue.intValue();
                        if (this.headerViews.get(Integer.valueOf(i)) == convertView) {
                            if (i == section - 1 || i <= section + 8) {
                                view = getSectionHeaderView(parent.getContext());
                            } else {
                                this.headerViews.remove(Integer.valueOf(i));
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
        this.headerViews.put(Integer.valueOf(section), view);
        view.setText(getSectionTitle(section).toUpperCase());
        return view;
    }
}
