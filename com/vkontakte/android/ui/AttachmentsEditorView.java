package com.vkontakte.android.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore.Images.Thumbnails;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.animation.OvershootInterpolator;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.facebook.WebDialog;
import com.vkontakte.android.APIController;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.AlbumAttachment;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.AudioAttachment;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.DocumentAttachment;
import com.vkontakte.android.FwdMessagesAttachment;
import com.vkontakte.android.GeoAttachment;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageCache;
import com.vkontakte.android.LinkAttachment;
import com.vkontakte.android.Log;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.PendingDocumentAttachment;
import com.vkontakte.android.PhotoAttachment;
import com.vkontakte.android.PollAttachment;
import com.vkontakte.android.PollEditorActivity;
import com.vkontakte.android.PostAttachment;
import com.vkontakte.android.UploaderService;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.VideoAttachment;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.providers.GalleryPickerProvider;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.acra.ACRAConstants;

public class AttachmentsEditorView extends HorizontalScrollView {
    private ArrayList<Attachment> attachments;
    private Callback callback;
    private LinearLayout content;
    private int dragPos;
    private float dragStartX;
    private float dragStartY;
    private View dragView;
    private long initTime;
    private float lastTouchX;
    private float lastTouchY;
    private BroadcastReceiver receiver;
    private OnClickListener removeClickListener;
    private OnClickListener retryClickListener;
    private Runnable scrollRunner;
    public int uploadOwnerId;
    public int uploadType;

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.10 */
    class AnonymousClass10 implements Runnable {
        private final /* synthetic */ int val$index;

        AnonymousClass10(int i) {
            this.val$index = i;
        }

        public void run() {
            AttachmentsEditorView.this.smoothScrollTo((this.val$index + 1) * Global.scale(80.0f), 0);
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.12 */
    class AnonymousClass12 implements Runnable {
        private final /* synthetic */ VideoAttachment val$att;
        private final /* synthetic */ View val$v;

        /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.12.1 */
        class C09841 implements Runnable {
            private final /* synthetic */ Bitmap val$bmp;
            private final /* synthetic */ View val$v;

            C09841(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bmp = bitmap;
            }

            public void run() {
                ((ImageView) this.val$v.findViewById(C0436R.id.attach_thumb)).setImageBitmap(this.val$bmp);
            }
        }

        AnonymousClass12(VideoAttachment videoAttachment, View view) {
            this.val$att = videoAttachment;
            this.val$v = view;
        }

        public void run() {
            ((Activity) AttachmentsEditorView.this.getContext()).runOnUiThread(new C09841(this.val$v, ImageCache.get(this.val$att.image)));
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.13 */
    class AnonymousClass13 implements Runnable {
        private final /* synthetic */ String val$fileUri;
        private final /* synthetic */ View val$v;

        /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.13.1 */
        class C09851 implements Runnable {
            private final /* synthetic */ Bitmap val$bmp;
            private final /* synthetic */ View val$v;

            C09851(Bitmap bitmap, View view) {
                this.val$bmp = bitmap;
                this.val$v = view;
            }

            public void run() {
                Log.m528i("vk", "Set thumb " + this.val$bmp);
                ((ImageView) this.val$v.findViewById(C0436R.id.attach_thumb)).setImageBitmap(this.val$bmp);
            }
        }

        AnonymousClass13(String str, View view) {
            this.val$fileUri = str;
            this.val$v = view;
        }

        public void run() {
            try {
                Bitmap bmp;
                Uri uri = Uri.parse(this.val$fileUri);
                Log.m528i("vk", "Add attachment: " + uri);
                try {
                    Cursor cursor = Thumbnails.queryMiniThumbnails(AttachmentsEditorView.this.getContext().getContentResolver(), uri, 1, null);
                    if (cursor != null && cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        String thumb = cursor.getString(cursor.getColumnIndex("_data"));
                        Log.m525d("vk", "Image thumb " + thumb);
                        uri = Uri.parse(thumb);
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (Exception e) {
                }
                if (uri.getScheme().equals("file") || uri.getScheme().equals("content") || !GalleryPickerProvider.instance().getNeedUseStyledThumb(this.val$fileUri)) {
                    if (uri.getScheme().equals(GalleryPickerProvider.STYLED_IMAGE_URI_SCHEME)) {
                        uri = GalleryPickerProvider.getOriginalUri(this.val$fileUri);
                    }
                    ParcelFileDescriptor fd = AttachmentsEditorView.this.getContext().getContentResolver().openFileDescriptor(uri, "r");
                    int rotation = 0;
                    if (uri.getScheme().equals("content")) {
                        try {
                            Cursor c = AttachmentsEditorView.this.getContext().getContentResolver().query(uri, new String[]{"orientation"}, null, null, null);
                            if (c.moveToFirst()) {
                                rotation = c.getInt(0);
                            }
                            Log.m528i("vk", "img rotation is " + rotation);
                        } catch (Exception e2) {
                        }
                    }
                    if (uri.getScheme().equals("file") || rotation == 0) {
                        String realPath = AttachmentsEditorView.this.getRealPathFromURI(uri);
                        if (realPath != null) {
                            try {
                                int o = new ExifInterface(realPath).getAttributeInt("Orientation", 0);
                                Log.m528i("vk", "Exif orientation " + o);
                                switch (o) {
                                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                                        rotation = 0;
                                        break;
                                    case Group.ADMIN_LEVEL_ADMIN /*3*/:
                                        rotation = 180;
                                        break;
                                    case UserListView.TYPE_POLL_VOTERS /*6*/:
                                        rotation = 90;
                                        break;
                                    case UserListView.TYPE_BLACKLIST /*8*/:
                                        rotation = 270;
                                        break;
                                }
                            } catch (Exception e3) {
                            }
                        }
                    }
                    Options opts = new Options();
                    opts.inJustDecodeBounds = true;
                    BitmapFactory.decodeFileDescriptor(fd.getFileDescriptor(), null, opts);
                    int ts = Global.scale(90.0f);
                    int bs = Math.min(opts.outWidth, opts.outHeight);
                    opts.inJustDecodeBounds = false;
                    if (bs > ts) {
                        opts.inSampleSize = (int) Math.floor((double) (((float) bs) / ((float) ts)));
                    }
                    Bitmap _bmp = BitmapFactory.decodeFileDescriptor(fd.getFileDescriptor(), null, opts);
                    if (rotation != 0) {
                        Matrix matrix = new Matrix();
                        matrix.preRotate((float) rotation);
                        _bmp = Bitmap.createBitmap(_bmp, 0, 0, _bmp.getWidth(), _bmp.getHeight(), matrix, false);
                    }
                    bmp = _bmp;
                    fd.close();
                } else {
                    String fname = "thumbs/t" + APIRequest.md5(this.val$fileUri) + ".jpg";
                    Log.m528i("vk", "Before get styled " + this.val$fileUri + ", fname=" + fname);
                    bmp = GalleryPickerProvider.instance().getThumbnail(this.val$fileUri, new File(VKApplication.context.getCacheDir(), fname).getAbsolutePath());
                    Log.m528i("vk", "After get, result = " + bmp);
                }
                ((Activity) AttachmentsEditorView.this.getContext()).runOnUiThread(new C09851(bmp, this.val$v));
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.14 */
    class AnonymousClass14 implements Runnable {
        private final /* synthetic */ PhotoAttachment val$att;
        private final /* synthetic */ View val$v;

        /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.14.1 */
        class C09861 implements Runnable {
            private final /* synthetic */ Bitmap val$bmp;
            private final /* synthetic */ View val$v;

            C09861(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bmp = bitmap;
            }

            public void run() {
                ((ImageView) this.val$v.findViewById(C0436R.id.attach_thumb)).setImageBitmap(this.val$bmp);
            }
        }

        AnonymousClass14(PhotoAttachment photoAttachment, View view) {
            this.val$att = photoAttachment;
            this.val$v = view;
        }

        public void run() {
            try {
                String url = this.val$att.getThumbURL();
                Log.m525d("vk", "Loading: " + url);
                Bitmap bmp = ImageCache.get(url);
                Log.m525d("vk", "Loaded: " + bmp);
                while (this.val$v.getWidth() == 0) {
                    Thread.sleep(100);
                }
                AttachmentsEditorView.this.post(new C09861(this.val$v, bmp));
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.15 */
    class AnonymousClass15 implements Runnable {
        private final /* synthetic */ DocumentAttachment val$att;
        private final /* synthetic */ View val$v;

        /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.15.1 */
        class C09871 implements Runnable {
            private final /* synthetic */ Bitmap val$bmp;
            private final /* synthetic */ View val$v;

            C09871(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bmp = bitmap;
            }

            public void run() {
                ((ImageView) this.val$v.findViewById(C0436R.id.attach_thumb)).setImageBitmap(this.val$bmp);
            }
        }

        AnonymousClass15(DocumentAttachment documentAttachment, View view) {
            this.val$att = documentAttachment;
            this.val$v = view;
        }

        public void run() {
            try {
                Bitmap bmp = ImageCache.get(this.val$att.thumb);
                while (this.val$v.getWidth() == 0) {
                    Thread.sleep(100);
                }
                AttachmentsEditorView.this.post(new C09871(this.val$v, bmp));
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.16 */
    class AnonymousClass16 implements Runnable {
        private final /* synthetic */ GeoAttachment val$att;
        private final /* synthetic */ View val$v;

        /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.16.1 */
        class C09881 implements Runnable {
            private final /* synthetic */ Bitmap val$bmp;
            private final /* synthetic */ View val$v;

            C09881(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bmp = bitmap;
            }

            public void run() {
                ((ImageView) this.val$v.findViewById(C0436R.id.attach_thumb)).setImageBitmap(this.val$bmp);
            }
        }

        AnonymousClass16(GeoAttachment geoAttachment, View view) {
            this.val$att = geoAttachment;
            this.val$v = view;
        }

        public void run() {
            try {
                Bitmap map = ImageCache.get(Global.getStaticMapURL(this.val$att.lat, this.val$att.lon, 85, 85, 14));
                Bitmap bmp = Bitmap.createBitmap(Global.scale(85.0f), Global.scale(85.0f), Config.ARGB_8888);
                Canvas c = new Canvas(bmp);
                Paint paint = new Paint();
                paint.setFilterBitmap(true);
                c.drawBitmap(map, null, new Rect(0, 0, Global.scale(85.0f), Global.scale(85.0f)), paint);
                Drawable marker = AttachmentsEditorView.this.getResources().getDrawable(C0436R.drawable.map_marker);
                marker.setBounds((Global.scale(85.0f) / 2) - (marker.getIntrinsicWidth() / 2), (Global.scale(85.0f) / 2) - marker.getIntrinsicHeight(), (Global.scale(85.0f) / 2) + (marker.getIntrinsicWidth() / 2), Global.scale(85.0f) / 2);
                marker.draw(c);
                AttachmentsEditorView.this.post(new C09881(this.val$v, bmp));
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.17 */
    class AnonymousClass17 implements Runnable {
        private final /* synthetic */ PendingPhotoAttachment val$att;

        AnonymousClass17(PendingPhotoAttachment pendingPhotoAttachment) {
            this.val$att = pendingPhotoAttachment;
        }

        public void run() {
            Intent intent = new Intent(AttachmentsEditorView.this.getContext(), UploaderService.class);
            intent.putExtra("new", 1);
            intent.putExtra("file", this.val$att.fileUri);
            intent.putExtra("id", this.val$att.id);
            intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, AttachmentsEditorView.this.uploadType);
            if (AttachmentsEditorView.this.uploadOwnerId != 0) {
                HashMap<String, String> params = new HashMap();
                params.put("owner_id", new StringBuilder(String.valueOf(AttachmentsEditorView.this.uploadOwnerId)).toString());
                params.put("_nopost", ACRAConstants.DEFAULT_STRING_VALUE);
                Log.m525d("vk", "req params=" + params);
                intent.putExtra("req_params", params);
            }
            AttachmentsEditorView.this.getContext().startService(intent);
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.18 */
    class AnonymousClass18 implements Runnable {
        private final /* synthetic */ PendingDocumentAttachment val$att;

        AnonymousClass18(PendingDocumentAttachment pendingDocumentAttachment) {
            this.val$att = pendingDocumentAttachment;
        }

        public void run() {
            Intent intent = new Intent(AttachmentsEditorView.this.getContext(), UploaderService.class);
            intent.putExtra("new", 1);
            intent.putExtra("file", this.val$att.url);
            intent.putExtra("id", this.val$att.did);
            intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 4);
            intent.putExtra("no_notify", true);
            HashMap<String, String> params = new HashMap();
            params.put("_mehod", "docs.getWallUploadServer");
            if (AttachmentsEditorView.this.uploadOwnerId < 0 && (AttachmentsEditorView.this.getContext() instanceof NewPostActivity) && Groups.getAdminLevel(-AttachmentsEditorView.this.uploadOwnerId) >= 2) {
                params.put("group_id", new StringBuilder(String.valueOf(-AttachmentsEditorView.this.uploadOwnerId)).toString());
            }
            intent.putExtra("req_params", params);
            AttachmentsEditorView.this.getContext().startService(intent);
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.1 */
    class C09891 implements OnClickListener {
        C09891() {
        }

        public void onClick(View v) {
            AttachmentsEditorView.this.remove((Attachment) ((View) v.getParent()).getTag());
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.2 */
    class C09902 implements OnClickListener {
        C09902() {
        }

        public void onClick(View v) {
            View aview = (View) v.getParent();
            Object t = aview.getTag();
            if (t instanceof PendingPhotoAttachment) {
                PendingPhotoAttachment a = (PendingPhotoAttachment) aview.getTag();
                Log.m528i("vk", "RETRY CLICK " + a);
                aview.findViewById(C0436R.id.attach_progress).setVisibility(0);
                aview.findViewById(C0436R.id.attach_error).setVisibility(8);
                ((ProgressBar) aview.findViewById(C0436R.id.attach_progress)).setProgress(0);
                a.id = UploaderService.getNewID();
                AttachmentsEditorView.this.uploadPhoto(a);
            }
            if (t instanceof PendingDocumentAttachment) {
                PendingDocumentAttachment a2 = (PendingDocumentAttachment) aview.getTag();
                Log.m528i("vk", "RETRY CLICK " + a2);
                aview.findViewById(C0436R.id.attach_progress).setVisibility(0);
                aview.findViewById(C0436R.id.attach_error).setVisibility(8);
                ((ProgressBar) aview.findViewById(C0436R.id.attach_progress)).setProgress(0);
                a2.did = UploaderService.getNewID();
                AttachmentsEditorView.this.uploadDocument(a2);
            }
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.3 */
    class C09913 extends BroadcastReceiver {
        C09913() {
        }

        public void onReceive(Context context, Intent intent) {
            int id;
            int i;
            Iterator it;
            Attachment att;
            View v;
            if (APIController.API_DEBUG) {
                Log.m528i("vk", "upload state " + intent.getAction() + ", id=" + intent.getIntExtra("id", -1));
            }
            if (UploaderService.ACTION_UPLOAD_PROGRESS.equals(intent.getAction())) {
                id = intent.getIntExtra("id", 0);
                i = 0;
                it = AttachmentsEditorView.this.attachments.iterator();
                while (it.hasNext()) {
                    att = (Attachment) it.next();
                    if (((att instanceof PendingPhotoAttachment) && ((PendingPhotoAttachment) att).id == id) || ((att instanceof PendingDocumentAttachment) && ((PendingDocumentAttachment) att).did == id)) {
                        v = AttachmentsEditorView.this.findViewWithTag(att);
                        if (v == null) {
                            v = AttachmentsEditorView.this.content.getChildAt(i);
                        }
                        if (v == null) {
                            i++;
                        } else if (v.findViewById(C0436R.id.attach_progress) != null) {
                            ((ProgressBar) v.findViewById(C0436R.id.attach_progress)).setProgress(intent.getIntExtra("done", 0));
                            ((ProgressBar) v.findViewById(C0436R.id.attach_progress)).setMax(intent.getIntExtra("total", 0));
                            if (APIController.API_DEBUG) {
                                Log.m528i("vk", "Upload " + id + " progress: " + intent.getIntExtra("done", 0) + "/" + intent.getIntExtra("total", 0));
                            }
                        }
                    } else {
                        i++;
                    }
                }
            }
            if (UploaderService.ACTION_UPLOAD_DONE.equals(intent.getAction())) {
                id = intent.getIntExtra("id", 0);
                i = 0;
                it = AttachmentsEditorView.this.attachments.iterator();
                while (it.hasNext()) {
                    att = (Attachment) it.next();
                    Attachment na;
                    if ((att instanceof PendingPhotoAttachment) && ((PendingPhotoAttachment) att).id == id) {
                        v = AttachmentsEditorView.this.findViewWithTag(att);
                        if (v == null) {
                            v = AttachmentsEditorView.this.content.getChildAt(i);
                        }
                        if (v == null) {
                            i++;
                        } else if (v.findViewById(C0436R.id.attach_progress) != null) {
                            v.findViewById(C0436R.id.attach_progress).setVisibility(8);
                            na = (Attachment) intent.getParcelableExtra("attachment");
                            v.setTag(na);
                            AttachmentsEditorView.this.attachments.set(i, na);
                            if (APIController.API_DEBUG) {
                                Log.m528i("vk", "Upload " + id + " done: " + intent.getParcelableExtra("attachment"));
                            }
                            AttachmentsEditorView.this.callback.onAllUploadsDone();
                        }
                    } else if ((att instanceof PendingDocumentAttachment) && ((PendingDocumentAttachment) att).did == id) {
                        v = AttachmentsEditorView.this.findViewWithTag(att);
                        if (v == null) {
                            v = AttachmentsEditorView.this.content.getChildAt(i);
                        }
                        if (v == null) {
                            i++;
                        } else if (v.findViewById(C0436R.id.attach_progress) != null) {
                            v.findViewById(C0436R.id.attach_progress).setVisibility(8);
                            na = (Attachment) intent.getParcelableExtra("attachment");
                            v.setTag(na);
                            AttachmentsEditorView.this.attachments.set(i, na);
                            if (APIController.API_DEBUG) {
                                Log.m528i("vk", "Upload " + id + " done: " + intent.getParcelableExtra("attachment"));
                            }
                            if (!(AttachmentsEditorView.this.isUploading() || AttachmentsEditorView.this.callback == null)) {
                                AttachmentsEditorView.this.callback.onAllUploadsDone();
                            }
                        }
                    } else {
                        i++;
                    }
                }
                AttachmentsEditorView.this.callback.onAllUploadsDone();
            }
            if (UploaderService.ACTION_UPLOAD_FAILED.equals(intent.getAction())) {
                id = intent.getIntExtra("id", 0);
                i = 0;
                it = AttachmentsEditorView.this.attachments.iterator();
                while (it.hasNext()) {
                    att = (Attachment) it.next();
                    if (((att instanceof PendingPhotoAttachment) && ((PendingPhotoAttachment) att).id == id) || ((att instanceof PendingDocumentAttachment) && ((PendingDocumentAttachment) att).did == id)) {
                        if (AttachmentsEditorView.this.callback != null) {
                            AttachmentsEditorView.this.callback.onUploadFailed();
                        }
                        v = AttachmentsEditorView.this.findViewWithTag(att);
                        if (v == null) {
                            v = AttachmentsEditorView.this.content.getChildAt(i);
                        }
                        if (!(v == null || v.findViewById(C0436R.id.attach_progress) == null)) {
                            v.findViewById(C0436R.id.attach_progress).setVisibility(8);
                            v.findViewById(C0436R.id.attach_error).setVisibility(0);
                            return;
                        }
                    }
                    i++;
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.4 */
    class C09924 extends LinearLayout {
        C09924(Context $anonymous0) {
            super($anonymous0);
        }

        public void onAttachedToWindow() {
            setChildrenDrawingOrderEnabled(true);
        }

        protected int getChildDrawingOrder(int count, int i) {
            if (AttachmentsEditorView.this.dragView == null || i < AttachmentsEditorView.this.dragPos) {
                return i;
            }
            if (i == count - 1) {
                return AttachmentsEditorView.this.dragPos;
            }
            return i + 1;
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.5 */
    class C09935 implements TimeInterpolator {
        C09935() {
        }

        public float getInterpolation(float input) {
            return Math.min(1.0f, 6.0f * input);
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.6 */
    class C09946 extends AnimatorListenerAdapter {
        C09946() {
        }

        public void onAnimationStart(Animator anim) {
            ((View) ((ObjectAnimator) ((AnimatorSet) anim).getChildAnimations().get(0)).getTarget()).setAlpha(0.0f);
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.7 */
    class C09957 extends AnimatorListenerAdapter {
        C09957() {
        }

        public void onAnimationEnd(Animator anim) {
            View view = (View) ((ObjectAnimator) ((AnimatorSet) anim).getChildAnimations().get(0)).getTarget();
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.8 */
    class C09968 implements OnClickListener {
        private final /* synthetic */ PollAttachment val$la;

        C09968(PollAttachment pollAttachment) {
            this.val$la = pollAttachment;
        }

        public void onClick(View v) {
            Intent intent = new Intent(AttachmentsEditorView.this.getContext(), PollEditorActivity.class);
            intent.putExtra("poll", this.val$la);
            ((Activity) AttachmentsEditorView.this.getContext()).startActivityForResult(intent, 9);
        }
    }

    /* renamed from: com.vkontakte.android.ui.AttachmentsEditorView.9 */
    class C09979 implements OnPreDrawListener {
        private final /* synthetic */ int val$index;
        private final /* synthetic */ View val$view;

        C09979(View view, int i) {
            this.val$view = view;
            this.val$index = i;
        }

        public boolean onPreDraw() {
            AttachmentsEditorView.this.getViewTreeObserver().removeOnPreDrawListener(this);
            LayoutParams lp = new LayoutParams(Global.scale(80.0f), Global.scale(80.0f));
            lp.leftMargin = Global.scale(4.0f);
            AttachmentsEditorView.this.content.addView(this.val$view, Math.min(this.val$index, AttachmentsEditorView.this.content.getChildCount()), lp);
            return true;
        }
    }

    public interface Callback {
        void onAllUploadsDone();

        void onAttachmentRemoved(Attachment attachment);

        void onUploadFailed();
    }

    private class ScrollRunner implements Runnable {
        int f54k;

        public ScrollRunner(int _k) {
            this.f54k = 1;
            this.f54k = _k;
        }

        public void run() {
            if (AttachmentsEditorView.this.dragView != null) {
                if (AttachmentsEditorView.this.getScrollX() == AttachmentsEditorView.this.content.getWidth() - AttachmentsEditorView.this.getWidth() && this.f54k > 0) {
                    return;
                }
                if (AttachmentsEditorView.this.getScrollX() != 0 || this.f54k >= 0) {
                    AttachmentsEditorView.this.scrollBy(Global.scale((float) (this.f54k * 2)), 0);
                    AttachmentsEditorView attachmentsEditorView = AttachmentsEditorView.this;
                    attachmentsEditorView.dragStartX = attachmentsEditorView.dragStartX - ((float) Global.scale((float) (this.f54k * 2)));
                    AttachmentsEditorView.this.updateDragPosition();
                    AttachmentsEditorView.this.postDelayed(this, 10);
                }
            }
        }
    }

    public AttachmentsEditorView(Context context) {
        super(context);
        this.attachments = new ArrayList();
        this.removeClickListener = new C09891();
        this.retryClickListener = new C09902();
        this.receiver = new C09913();
        this.uploadType = 5;
        this.uploadOwnerId = 0;
        this.initTime = System.currentTimeMillis();
        init();
    }

    public AttachmentsEditorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.attachments = new ArrayList();
        this.removeClickListener = new C09891();
        this.retryClickListener = new C09902();
        this.receiver = new C09913();
        this.uploadType = 5;
        this.uploadOwnerId = 0;
        this.initTime = System.currentTimeMillis();
        init();
    }

    public AttachmentsEditorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.attachments = new ArrayList();
        this.removeClickListener = new C09891();
        this.retryClickListener = new C09902();
        this.receiver = new C09913();
        this.uploadType = 5;
        this.uploadOwnerId = 0;
        this.initTime = System.currentTimeMillis();
        init();
    }

    private void init() {
        this.content = new C09924(getContext());
        this.content.setOrientation(0);
        this.content.setGravity(16);
        if (VERSION.SDK_INT >= 14) {
            this.content.setLayoutTransition(new LayoutTransition());
            resetTransitionAnims();
        }
        addView(this.content);
        setFillViewport(true);
        setHorizontalScrollBarEnabled(false);
    }

    private void resetTransitionAnims() {
        LayoutTransition trans = this.content.getLayoutTransition();
        trans.setAnimateParentHierarchy(false);
        AnimatorSet animAdding = new AnimatorSet();
        animAdding.playTogether(new Animator[]{ObjectAnimator.ofFloat(null, "scaleX", new float[]{0.1f, 1.0f}), ObjectAnimator.ofFloat(null, "scaleY", new float[]{0.1f, 1.0f}), ObjectAnimator.ofFloat(null, "alpha", new float[]{0.0f, 1.0f})});
        ((Animator) animAdding.getChildAnimations().get(0)).setInterpolator(new OvershootInterpolator());
        ((Animator) animAdding.getChildAnimations().get(1)).setInterpolator(new OvershootInterpolator());
        ((Animator) animAdding.getChildAnimations().get(2)).setInterpolator(new C09935());
        animAdding.addListener(new C09946());
        trans.setAnimator(2, animAdding);
        AnimatorSet animRemoving = new AnimatorSet();
        r3 = new Animator[4];
        r3[2] = ObjectAnimator.ofFloat(null, "rotation", new float[]{90.0f}).setDuration(300);
        r3[3] = ObjectAnimator.ofFloat(null, "alpha", new float[]{0.0f}).setDuration(300);
        animRemoving.playTogether(r3);
        animRemoving.addListener(new C09957());
        trans.setAnimator(3, animRemoving);
        trans.setStartDelay(1, 150);
        trans.setStartDelay(0, 0);
        trans.setStartDelay(2, 500);
        trans.setStartDelay(3, 0);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        IntentFilter filter = new IntentFilter();
        filter.addAction(UploaderService.ACTION_UPLOAD_PROGRESS);
        filter.addAction(UploaderService.ACTION_UPLOAD_STARTED);
        filter.addAction(UploaderService.ACTION_UPLOAD_DONE);
        filter.addAction(UploaderService.ACTION_UPLOAD_FAILED);
        getContext().registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            getContext().unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
    }

    public void setCallback(Callback c) {
        this.callback = c;
    }

    public void add(Attachment att) {
        Log.m528i("vk", "Attach editor add");
        int idx = this.attachments.size();
        if (this.attachments.size() > 0) {
            int photos = 0;
            int albums = 0;
            int videos = 0;
            int audios = 0;
            int polls = 0;
            int docs = 0;
            int others = 0;
            int fwds = 0;
            Iterator it = this.attachments.iterator();
            while (it.hasNext()) {
                Attachment a = (Attachment) it.next();
                if (a instanceof AlbumAttachment) {
                    albums++;
                } else if ((a instanceof PhotoAttachment) || (a instanceof PendingPhotoAttachment)) {
                    photos++;
                } else if (a instanceof VideoAttachment) {
                    videos++;
                } else if (a instanceof AudioAttachment) {
                    audios++;
                } else if (a instanceof PollAttachment) {
                    polls++;
                } else if (a instanceof DocumentAttachment) {
                    docs++;
                } else if (a instanceof FwdMessagesAttachment) {
                    fwds++;
                } else {
                    others++;
                }
            }
            if (att instanceof AlbumAttachment) {
                idx = photos + albums;
            } else if ((att instanceof PhotoAttachment) || (att instanceof PendingPhotoAttachment)) {
                idx = photos;
            } else if (att instanceof VideoAttachment) {
                idx = (photos + albums) + videos;
            } else if (att instanceof AudioAttachment) {
                idx = ((photos + albums) + videos) + audios;
            } else if (att instanceof PollAttachment) {
                idx = (((photos + albums) + videos) + audios) + polls;
            } else if (att instanceof DocumentAttachment) {
                idx = ((((photos + albums) + videos) + audios) + polls) + docs;
            } else {
                idx = (((((photos + albums) + videos) + audios) + polls) + docs) + others;
            }
            if (att instanceof GeoAttachment) {
                idx = this.attachments.size() - fwds;
            }
        }
        this.attachments.add(idx, att);
        if (att instanceof PhotoAttachment) {
            addAttachView(createPhotoView((PhotoAttachment) att), att, idx);
        } else if (att instanceof PendingPhotoAttachment) {
            addAttachView(createPhotoView((PendingPhotoAttachment) att), att, idx);
            uploadPhoto((PendingPhotoAttachment) att);
        } else if (att instanceof AudioAttachment) {
            AudioAttachment a2 = (AudioAttachment) att;
            addAttachView(createImagelessView(getResources().getString(C0436R.string.audio), a2.artist, a2.title, false), att, idx);
        } else if (att instanceof VideoAttachment) {
            addAttachView(createVideoView((VideoAttachment) att), att, idx);
        } else if (att instanceof DocumentAttachment) {
            DocumentAttachment a3 = (DocumentAttachment) att;
            String[] sp = a3.title.split("\\.");
            if (att instanceof PendingDocumentAttachment) {
                uploadDocument((PendingDocumentAttachment) att);
            }
            if (a3.thumb != null) {
                if (a3.thumb.length() > 0) {
                    if (a3 instanceof PendingDocumentAttachment) {
                        addAttachView(createPendingDocumentView(a3, new StringBuilder(String.valueOf(sp[sp.length - 1].toUpperCase())).append(", ").append(Global.langFileSize((long) a3.size, getResources())).toString()), att, idx);
                        return;
                    }
                    addAttachView(createDocumentView(a3, new StringBuilder(String.valueOf(sp[sp.length - 1].toUpperCase())).append(", ").append(Global.langFileSize((long) a3.size, getResources())).toString()), att, idx);
                    return;
                }
            }
            String string = getResources().getString(C0436R.string.doc);
            String str = a3.title;
            int length = sp.length;
            long j = (long) a3.size;
            boolean z = att instanceof PendingDocumentAttachment;
            addAttachView(createImagelessView(string, str, new StringBuilder(String.valueOf(sp[r0 - 1].toUpperCase())).append(", ").append(Global.langFileSize(r0, getResources())).toString(), z), att, idx);
        } else if (att instanceof GeoAttachment) {
            addAttachView(createLocationView((GeoAttachment) att), att, idx);
        } else if (att instanceof FwdMessagesAttachment) {
            addAttachView(createFwdView(((FwdMessagesAttachment) att).msgs.size()), att, idx);
        } else if (att instanceof PostAttachment) {
            NewsEntry post = ((PostAttachment) att).post;
            addAttachView(createImagelessView(getResources().getString(C0436R.string.attach_wall_post), post.displayablePreviewText.toString(), ACRAConstants.DEFAULT_STRING_VALUE, false), att, idx);
        } else if (att instanceof LinkAttachment) {
            LinkAttachment la = (LinkAttachment) att;
            addAttachView(createImagelessView(getResources().getString(C0436R.string.attach_link), la.url.replace("http://", ACRAConstants.DEFAULT_STRING_VALUE).replace("https://", ACRAConstants.DEFAULT_STRING_VALUE), la.title, false), att, idx);
        } else if (att instanceof PollAttachment) {
            PollAttachment la2 = (PollAttachment) att;
            View av = createImagelessView(getResources().getString(C0436R.string.attach_poll), la2.question, ACRAConstants.DEFAULT_STRING_VALUE, false);
            av.setOnClickListener(new C09968(la2));
            addAttachView(av, att, idx);
        } else {
            Log.m526e("vk", "Unknown attachment: " + att);
        }
    }

    public void remove(Attachment a) {
        int index = -1;
        for (int i = 0; i < this.attachments.size(); i++) {
            if (this.attachments.get(i) == a) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            remove(index);
        }
    }

    public int getCount() {
        int size = 0;
        Iterator it = this.attachments.iterator();
        while (it.hasNext()) {
            Attachment att = (Attachment) it.next();
            if (!((att instanceof GeoAttachment) || (att instanceof FwdMessagesAttachment))) {
                size++;
            }
        }
        return size;
    }

    public int getRealCount() {
        return this.attachments.size();
    }

    public void clear() {
        this.content.removeAllViews();
        this.attachments.clear();
        if (this.callback != null) {
            this.callback.onAttachmentRemoved(null);
        }
    }

    public boolean isUploading() {
        Iterator it = this.attachments.iterator();
        while (it.hasNext()) {
            Attachment att = (Attachment) it.next();
            if (att instanceof PendingPhotoAttachment) {
                return true;
            }
            if (att instanceof PendingDocumentAttachment) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Attachment> getAll() {
        ArrayList<Attachment> att = new ArrayList();
        att.addAll(this.attachments);
        return att;
    }

    public void remove(int index) {
        Attachment att = (Attachment) this.attachments.remove(index);
        this.content.removeViewAt(index);
        if (this.callback != null) {
            this.callback.onAttachmentRemoved(att);
        }
        if (att instanceof PendingPhotoAttachment) {
            Intent intent = new Intent(getContext(), UploaderService.class);
            intent.setAction("CANCEL");
            intent.putExtra("id", ((PendingPhotoAttachment) att).id);
            getContext().startService(intent);
        }
        if (att instanceof PendingDocumentAttachment) {
            intent = new Intent(getContext(), UploaderService.class);
            intent.setAction("CANCEL");
            intent.putExtra("id", ((PendingDocumentAttachment) att).did);
            getContext().startService(intent);
        }
    }

    public void removeNoCallback(int index) {
        this.content.removeViewAt(index);
        this.attachments.remove(index);
    }

    public boolean containsGeoAttachment() {
        Iterator it = this.attachments.iterator();
        while (it.hasNext()) {
            if (((Attachment) it.next()) instanceof GeoAttachment) {
                return true;
            }
        }
        return false;
    }

    private void addAttachView(View view, Attachment att, int index) {
        view.setTag(att);
        getViewTreeObserver().addOnPreDrawListener(new C09979(view, index));
        if (System.currentTimeMillis() - this.initTime > 500 && (Math.floor((double) (((float) getScrollX()) / 80.0f)) > ((double) index) || Math.ceil((double) (((float) (getScrollX() + getWidth())) / 80.0f)) < ((double) index))) {
            postDelayed(new AnonymousClass10(index), 150);
        }
        if (VERSION.SDK_INT >= 14) {
            view.setOnLongClickListener(new OnLongClickListener() {
                public boolean onLongClick(View v) {
                    AttachmentsEditorView.this.dragStartX = AttachmentsEditorView.this.lastTouchX;
                    AttachmentsEditorView.this.dragStartY = AttachmentsEditorView.this.lastTouchY;
                    AttachmentsEditorView.this.dragPos = 0;
                    for (int i = 0; i < AttachmentsEditorView.this.content.getChildCount(); i++) {
                        if (AttachmentsEditorView.this.content.getChildAt(i) == v) {
                            AttachmentsEditorView.this.dragPos = i;
                            break;
                        }
                    }
                    AttachmentsEditorView.this.dragView = v;
                    AttachmentsEditorView.this.content.getLayoutTransition().setAnimator(2, null);
                    AttachmentsEditorView.this.content.getLayoutTransition().setAnimator(3, null);
                    AttachmentsEditorView.this.content.getLayoutTransition().setStartDelay(1, 0);
                    AttachmentsEditorView.this.content.getLayoutTransition().setStartDelay(0, 0);
                    AttachmentsEditorView.this.content.getLayoutTransition().setStartDelay(2, 0);
                    AttachmentsEditorView.this.content.getLayoutTransition().setStartDelay(3, 0);
                    AttachmentsEditorView.this.content.invalidate();
                    v.animate().scaleX(1.1f).scaleY(1.1f).setDuration(200).start();
                    return true;
                }
            });
        }
    }

    private View createImagelessView(String type, String title, String subtitle, boolean progress) {
        View v = inflate(getContext(), progress ? C0436R.layout.attach_edit_imageless_progress : C0436R.layout.attach_edit_imageless, null);
        ((TextView) v.findViewById(C0436R.id.attach_title)).setText(title);
        ((TextView) v.findViewById(C0436R.id.attach_subtitle)).setText(subtitle);
        ((TextView) v.findViewById(C0436R.id.attach_type)).setText(type);
        if (progress) {
            ((ProgressBar) v.findViewById(C0436R.id.attach_progress)).setProgressDrawable(new CircularProgressDrawable());
            ((ProgressBar) v.findViewById(C0436R.id.attach_progress)).setIndeterminateDrawable(new CircularProgressDrawable());
            ((ProgressBar) v.findViewById(C0436R.id.attach_progress)).setIndeterminate(false);
            v.findViewById(C0436R.id.attach_error).setOnClickListener(this.retryClickListener);
        }
        v.findViewById(C0436R.id.attach_remove).setOnClickListener(this.removeClickListener);
        return v;
    }

    private View createFwdView(int count) {
        View v = inflate(getContext(), C0436R.layout.attach_edit_fwd_message, null);
        ((TextView) v.findViewById(C0436R.id.attach_title)).setText(Global.langPlural(C0436R.array.messages, count, getResources()));
        ((TextView) v.findViewById(C0436R.id.attach_type)).setText(new StringBuilder(String.valueOf(count)).toString());
        v.findViewById(C0436R.id.attach_remove).setOnClickListener(this.removeClickListener);
        return v;
    }

    private View createVideoView(VideoAttachment att) {
        View v = inflate(getContext(), C0436R.layout.attach_edit_video, null);
        ((TextView) v.findViewById(C0436R.id.attach_video_duration)).setText(String.format("%d:%02d", new Object[]{Integer.valueOf(att.duration / 60), Integer.valueOf(att.duration % 60)}));
        ((TextView) v.findViewById(C0436R.id.attach_video_duration)).setTypeface(Fonts.getRobotoLight());
        new Thread(new AnonymousClass12(att, v)).start();
        v.findViewById(C0436R.id.attach_remove).setOnClickListener(this.removeClickListener);
        return v;
    }

    private String getRealPathFromURI(Uri contentUri) {
        if (contentUri.getScheme().equals("file")) {
            return contentUri.getPath();
        }
        Cursor cursor = getContext().getContentResolver().query(contentUri, new String[]{"_data"}, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private View createPhotoView(PendingPhotoAttachment att) {
        return createLocalImageView(att.fileUri, C0436R.layout.attach_edit_photo);
    }

    private View createPendingDocumentView(DocumentAttachment att, String title) {
        View v = createLocalImageView(att.thumb, C0436R.layout.attach_edit_doc_thumb);
        ((TextView) v.findViewById(C0436R.id.attach_title)).setText(title);
        return v;
    }

    private View createLocalImageView(String fileUri, int layout) {
        View v = inflate(getContext(), layout, null);
        ((ProgressBar) v.findViewById(C0436R.id.attach_progress)).setProgressDrawable(new CircularProgressDrawable());
        ((ProgressBar) v.findViewById(C0436R.id.attach_progress)).setIndeterminateDrawable(new CircularProgressDrawable());
        ((ProgressBar) v.findViewById(C0436R.id.attach_progress)).setIndeterminate(false);
        new Thread(new AnonymousClass13(fileUri, v)).start();
        v.findViewById(C0436R.id.attach_remove).setOnClickListener(this.removeClickListener);
        v.findViewById(C0436R.id.attach_error).setOnClickListener(this.retryClickListener);
        return v;
    }

    private View createPhotoView(PhotoAttachment att) {
        View v = inflate(getContext(), C0436R.layout.attach_edit_photo, null);
        v.findViewById(C0436R.id.attach_progress).setVisibility(8);
        new Thread(new AnonymousClass14(att, v)).start();
        v.findViewById(C0436R.id.attach_remove).setOnClickListener(this.removeClickListener);
        return v;
    }

    private View createDocumentView(DocumentAttachment att, String title) {
        View v = inflate(getContext(), C0436R.layout.attach_edit_doc_thumb, null);
        v.findViewById(C0436R.id.attach_progress).setVisibility(8);
        ((TextView) v.findViewById(C0436R.id.attach_title)).setText(title);
        new Thread(new AnonymousClass15(att, v)).start();
        v.findViewById(C0436R.id.attach_remove).setOnClickListener(this.removeClickListener);
        return v;
    }

    private View createLocationView(GeoAttachment att) {
        View v = inflate(getContext(), C0436R.layout.attach_edit_photo, null);
        v.findViewById(C0436R.id.attach_progress).setVisibility(8);
        new Thread(new AnonymousClass16(att, v)).start();
        v.findViewById(C0436R.id.attach_remove).setOnClickListener(this.removeClickListener);
        return v;
    }

    private void uploadPhoto(PendingPhotoAttachment att) {
        postDelayed(new AnonymousClass17(att), 300);
    }

    private void uploadDocument(PendingDocumentAttachment att) {
        postDelayed(new AnonymousClass18(att), 300);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        this.lastTouchX = ev.getX();
        this.lastTouchY = ev.getY();
        if (this.dragView == null) {
            return super.dispatchTouchEvent(ev);
        }
        if (ev.getAction() == 2) {
            updateDragPosition();
            return true;
        } else if (ev.getAction() != 1 && ev.getAction() != 3) {
            return true;
        } else {
            resetTransitionAnims();
            this.dragView.animate().scaleX(1.0f).scaleY(1.0f).translationX(0.0f).translationY(0.0f).setDuration(200).start();
            this.dragView = null;
            return true;
        }
    }

    private void updateDragPosition() {
        this.dragView.setTranslationX(this.lastTouchX - this.dragStartX);
        this.dragView.setTranslationY(this.lastTouchY - this.dragStartY);
        int rdpos = Math.round((((float) this.dragView.getLeft()) + this.dragView.getTranslationX()) / ((float) this.dragView.getWidth()));
        if (rdpos != this.dragPos && rdpos >= 0 && rdpos < this.content.getChildCount() && canReorder((Attachment) this.attachments.get(this.dragPos), (Attachment) this.attachments.get(rdpos))) {
            this.content.removeView(this.dragView);
            this.content.addView(this.dragView, rdpos);
            Attachment b = (Attachment) this.attachments.get(rdpos);
            this.attachments.set(rdpos, (Attachment) this.attachments.get(this.dragPos));
            this.attachments.set(this.dragPos, b);
            this.dragView.setTranslationX(this.dragView.getTranslationX() - ((float) ((rdpos - this.dragPos) * this.dragView.getWidth())));
            this.dragStartX += (float) ((rdpos - this.dragPos) * this.dragView.getWidth());
            this.dragPos = rdpos;
        }
        if (((((float) this.dragView.getLeft()) + this.dragView.getTranslationX()) + ((float) ((this.dragView.getWidth() / 3) * 2))) - ((float) getScrollX()) > ((float) getWidth())) {
            if (this.scrollRunner == null) {
                this.scrollRunner = new ScrollRunner(1);
                post(this.scrollRunner);
            }
        } else if (((((float) this.dragView.getLeft()) + this.dragView.getTranslationX()) + ((float) (this.dragView.getWidth() / 3))) - ((float) getScrollX()) < 0.0f) {
            if (this.scrollRunner == null) {
                this.scrollRunner = new ScrollRunner(-1);
                post(this.scrollRunner);
            }
        } else if (this.scrollRunner != null) {
            removeCallbacks(this.scrollRunner);
            this.scrollRunner = null;
        }
    }

    private boolean canReorder(Attachment a, Attachment b) {
        boolean z = true;
        if ((a instanceof FwdMessagesAttachment) || (a instanceof GeoAttachment)) {
            return false;
        }
        if (a instanceof AlbumAttachment) {
            return b instanceof AlbumAttachment;
        }
        if ((a instanceof PhotoAttachment) || (a instanceof PendingPhotoAttachment)) {
            if ((b instanceof PhotoAttachment) || (b instanceof PendingPhotoAttachment)) {
                return true;
            }
            return false;
        } else if (a instanceof VideoAttachment) {
            return b instanceof VideoAttachment;
        } else {
            if (a instanceof AudioAttachment) {
                return b instanceof AudioAttachment;
            }
            if (a instanceof PollAttachment) {
                return b instanceof PollAttachment;
            }
            if (a instanceof DocumentAttachment) {
                return b instanceof DocumentAttachment;
            }
            if ((b instanceof PhotoAttachment) || (b instanceof VideoAttachment) || (b instanceof AudioAttachment) || (b instanceof PollAttachment) || (b instanceof DocumentAttachment)) {
                z = false;
            }
            return z;
        }
    }
}
