package com.vkontakte.android.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;

public class PhotoStripView extends View {
    private Bitmap[] bmps;
    private SimpleOnGestureListener gListener;
    private GestureDetector gestureDetector;
    private OnPhotoClickListener listener;
    private int padding;
    private Paint paint;
    private Drawable placeholder;

    /* renamed from: com.vkontakte.android.ui.PhotoStripView.1 */
    class C10291 extends SimpleOnGestureListener {
        C10291() {
        }

        public boolean onSingleTapUp(MotionEvent ev) {
            int index = Math.round(ev.getX() / ((float) (PhotoStripView.this.getHeight() + PhotoStripView.this.padding)));
            if (index < PhotoStripView.this.bmps.length) {
                PhotoStripView.this.playSoundEffect(0);
                if (PhotoStripView.this.listener != null) {
                    PhotoStripView.this.listener.onPhotoClick(PhotoStripView.this, index);
                }
            }
            return true;
        }
    }

    public interface OnPhotoClickListener {
        void onPhotoClick(PhotoStripView photoStripView, int i);
    }

    public PhotoStripView(Context context) {
        super(context);
        this.bmps = new Bitmap[3];
        this.padding = Global.scale(3.0f);
        this.gListener = new C10291();
        init();
    }

    public PhotoStripView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.bmps = new Bitmap[3];
        this.padding = Global.scale(3.0f);
        this.gListener = new C10291();
        init();
    }

    public PhotoStripView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.bmps = new Bitmap[3];
        this.padding = Global.scale(3.0f);
        this.gListener = new C10291();
        init();
    }

    private void init() {
        this.paint = new Paint();
        this.paint.setFilterBitmap(true);
        this.placeholder = getResources().getDrawable(C0436R.drawable.user_placeholder);
        this.gestureDetector = new GestureDetector(getContext(), this.gListener);
    }

    public void setCount(int count) {
        this.bmps = new Bitmap[count];
    }

    public void setBitmap(int pos, Bitmap bmp) {
        this.bmps[pos] = bmp;
        invalidate();
    }

    public boolean onTouchEvent(MotionEvent ev) {
        this.gestureDetector.onTouchEvent(ev);
        return true;
    }

    public void setListener(OnPhotoClickListener l) {
        this.listener = l;
    }

    public void onDraw(Canvas c) {
        super.onDraw(c);
        int size = getHeight();
        for (int i = 0; i < this.bmps.length; i++) {
            Bitmap bmp = this.bmps[i];
            Rect dest = new Rect((size * i) + (this.padding * i), 0, ((size * i) + (this.padding * i)) + size, size);
            if (bmp != null) {
                c.drawBitmap(bmp, null, dest, this.paint);
            } else {
                this.placeholder.setBounds(dest);
                this.placeholder.draw(c);
            }
        }
    }
}
