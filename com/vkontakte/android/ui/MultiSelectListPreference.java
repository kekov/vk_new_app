package com.vkontakte.android.ui;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.res.TypedArray;
import android.preference.ListPreference;
import android.util.AttributeSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.acra.ACRAConstants;

public class MultiSelectListPreference extends ListPreference {
    private static final String DEFAULT_SEPARATOR = ";";
    private boolean[] entryChecked;
    private String separator;

    /* renamed from: com.vkontakte.android.ui.MultiSelectListPreference.1 */
    class C10181 implements OnMultiChoiceClickListener {
        C10181() {
        }

        public void onClick(DialogInterface dialog, int which, boolean val) {
            MultiSelectListPreference.this.entryChecked[which] = val;
        }
    }

    public MultiSelectListPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.entryChecked = new boolean[getEntries().length];
        this.separator = DEFAULT_SEPARATOR;
    }

    public MultiSelectListPreference(Context context) {
        this(context, null);
    }

    protected void onPrepareDialogBuilder(Builder builder) {
        CharSequence[] entries = getEntries();
        CharSequence[] entryValues = getEntryValues();
        if (entries == null || entryValues == null || entries.length != entryValues.length) {
            throw new IllegalStateException("MultiSelectListPreference requires an entries array and an entryValues array which are both the same length");
        }
        restoreCheckedEntries();
        builder.setMultiChoiceItems(entries, this.entryChecked, new C10181());
    }

    private CharSequence[] unpack(CharSequence val) {
        if (val == null || ACRAConstants.DEFAULT_STRING_VALUE.equals(val)) {
            return new CharSequence[0];
        }
        return ((String) val).split(this.separator);
    }

    public CharSequence[] getCheckedValues() {
        return unpack(getValue());
    }

    private void restoreCheckedEntries() {
        CharSequence[] entryValues = getEntryValues();
        CharSequence[] vals = unpack(getValue());
        if (vals != null) {
            List<CharSequence> valuesList = Arrays.asList(vals);
            for (int i = 0; i < entryValues.length; i++) {
                this.entryChecked[i] = valuesList.contains(entryValues[i]);
            }
        }
    }

    protected void onDialogClosed(boolean positiveResult) {
        List<CharSequence> values = new ArrayList();
        CharSequence[] entryValues = getEntryValues();
        if (positiveResult && entryValues != null) {
            for (int i = 0; i < entryValues.length; i++) {
                if (this.entryChecked[i]) {
                    values.add(entryValues[i]);
                }
            }
            String value = join(values, this.separator);
            setSummary(prepareSummary(values));
            setValueAndEvent(value);
        }
    }

    private void setValueAndEvent(String value) {
        if (callChangeListener(unpack(value))) {
            setValue(value);
        }
    }

    private CharSequence prepareSummary(List<CharSequence> joined) {
        List<String> titles = new ArrayList();
        CharSequence[] entryTitle = getEntries();
        int ix = 0;
        for (CharSequence value : getEntryValues()) {
            if (joined.contains(value)) {
                titles.add((String) entryTitle[ix]);
            }
            ix++;
        }
        return join(titles, ", ");
    }

    protected Object onGetDefaultValue(TypedArray typedArray, int index) {
        return typedArray.getTextArray(index);
    }

    protected void onSetInitialValue(boolean restoreValue, Object rawDefaultValue) {
        CharSequence[] defaultValue;
        String value;
        if (rawDefaultValue == null) {
            defaultValue = new CharSequence[0];
        } else {
            defaultValue = (CharSequence[]) rawDefaultValue;
        }
        String joinedDefaultValue = join(Arrays.asList(defaultValue), this.separator);
        if (restoreValue) {
            value = getPersistedString(joinedDefaultValue);
        } else {
            value = joinedDefaultValue;
        }
        setSummary(prepareSummary(Arrays.asList(unpack(value))));
        setValueAndEvent(value);
    }

    protected static String join(Iterable<?> iterable, String separator) {
        if (iterable != null) {
            Iterator<?> oIter = iterable.iterator();
            if (oIter.hasNext()) {
                StringBuilder oBuilder = new StringBuilder(String.valueOf(oIter.next()));
                while (oIter.hasNext()) {
                    oBuilder.append(separator).append(oIter.next());
                }
                return oBuilder.toString();
            }
        }
        return ACRAConstants.DEFAULT_STRING_VALUE;
    }
}
