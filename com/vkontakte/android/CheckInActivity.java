package com.vkontakte.android;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.LayoutParams;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition.Builder;
import com.google.android.gms.maps.model.LatLng;
import com.vkontakte.android.api.PlacesSearch;
import com.vkontakte.android.api.PlacesSearch.Callback;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.LoadMoreFooterView;
import com.vkontakte.android.ui.PaddingColorDrawable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import org.acra.ACRAConstants;

public class CheckInActivity extends VKActivity {
    private static final int CHECKIN_RESULT = 8345;
    private static final int CREATE_PLACE_RESULT = 8346;
    private PlacesAdapter adapter;
    private GeoPlace addPlace;
    private GeoPlace currentLocation;
    private APIRequest currentRequest;
    private boolean firstLocationUpdate;
    private ListImageLoaderWrapper imgLoader;
    private ListView list;
    private GoogleMap map;
    private MapView mapView;
    private ArrayList<GeoPlace> nearPlaces;
    private ArrayList<GeoPlace> places;
    private Location prevLocation;
    private LoadMoreFooterView progress;
    private ArrayList<GeoPlace> searchResults;
    private Runnable searchRunnable;
    private SearchView searchView;

    /* renamed from: com.vkontakte.android.CheckInActivity.1 */
    class C02241 implements OnClickListener {
        C02241() {
        }

        public void onClick(DialogInterface dialog, int which) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps"));
            intent.addFlags(268435456);
            CheckInActivity.this.startActivity(intent);
            CheckInActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.CheckInActivity.2 */
    class C02252 implements OnClickListener {
        C02252() {
        }

        public void onClick(DialogInterface dialog, int which) {
            CheckInActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.CheckInActivity.3 */
    class C02263 implements OnCancelListener {
        C02263() {
        }

        public void onCancel(DialogInterface dialog) {
            CheckInActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.CheckInActivity.6 */
    class C02276 implements OnItemClickListener {
        C02276() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            pos -= CheckInActivity.this.list.getHeaderViewsCount();
            if (pos >= 0) {
                GeoPlace place = (GeoPlace) CheckInActivity.this.places.get(pos);
                Intent intent;
                if (place.id == -1) {
                    if (CheckInActivity.this.prevLocation != null) {
                        GeoAttachment point = new GeoAttachment();
                        point.lat = CheckInActivity.this.prevLocation.getLatitude();
                        point.lon = CheckInActivity.this.prevLocation.getLongitude();
                        point.address = place.address;
                        intent = new Intent();
                        intent.putExtra("point", point);
                        CheckInActivity.this.setResult(-1, intent);
                        CheckInActivity.this.finish();
                    }
                } else if (place.id != -2) {
                    GeoAttachment att = new GeoAttachment();
                    att.lat = place.lat;
                    att.lon = place.lon;
                    att.address = place.address;
                    att.id = place.id;
                    att.title = place.title;
                    att.photo = place.photo;
                    Bundle args = new Bundle();
                    args.putBoolean("checkin", true);
                    args.putParcelable("point", att);
                    intent = new Intent(CheckInActivity.this, FragmentWrapperActivity.class);
                    intent.putExtra("class", "GeoPlaceFragment");
                    intent.putExtra("args", args);
                    CheckInActivity.this.startActivityForResult(intent, CheckInActivity.CHECKIN_RESULT);
                } else if (CheckInActivity.this.prevLocation != null) {
                    intent = new Intent(CheckInActivity.this, SelectGeoPointActivity.class);
                    intent.putExtra("create_place", true);
                    intent.putExtra("place_address", CheckInActivity.this.addPlace.address);
                    intent.putExtra("place_title", CheckInActivity.this.searchView.getQuery());
                    CheckInActivity.this.startActivityForResult(intent, CheckInActivity.CREATE_PLACE_RESULT);
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.CheckInActivity.7 */
    class C02287 implements OnScrollListener {
        C02287() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 1 && CheckInActivity.this.getCurrentFocus() != null) {
                ((InputMethodManager) CheckInActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(CheckInActivity.this.getCurrentFocus().getWindowToken(), 0);
                CheckInActivity.this.getCurrentFocus().clearFocus();
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (VERSION.SDK_INT >= 14 && firstVisibleItem == 0) {
                CheckInActivity.this.mapView.setTranslationY((float) ((-CheckInActivity.this.mapView.getTop()) / 2));
                CheckInActivity.this.mapView.invalidate();
            }
        }
    }

    /* renamed from: com.vkontakte.android.CheckInActivity.9 */
    class C02309 implements Runnable {

        /* renamed from: com.vkontakte.android.CheckInActivity.9.1 */
        class C02291 implements Runnable {
            C02291() {
            }

            public void run() {
                CheckInActivity.this.updateList();
            }
        }

        C02309() {
        }

        public void run() {
            try {
                Address addr = (Address) new Geocoder(CheckInActivity.this.getBaseContext()).getFromLocation(CheckInActivity.this.prevLocation.getLatitude(), CheckInActivity.this.prevLocation.getLongitude(), 1).get(0);
                String a = ACRAConstants.DEFAULT_STRING_VALUE;
                ArrayList<String> t = new ArrayList();
                if (addr.getThoroughfare() != null) {
                    t.add(addr.getThoroughfare());
                }
                if (addr.getSubThoroughfare() != null) {
                    t.add(addr.getSubThoroughfare());
                }
                if (!(addr.getFeatureName() == null || addr.getFeatureName().equals(addr.getSubThoroughfare()))) {
                    t.add(addr.getFeatureName());
                }
                a = TextUtils.join(", ", t);
                if (a == null || "null".equals(a)) {
                    a = CheckInActivity.this.getString(C0436R.string.loading);
                }
                CheckInActivity.this.currentLocation.address = a;
                CheckInActivity.this.addPlace.address = a;
                CheckInActivity.this.runOnUiThread(new C02291());
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    private class PlacesAdapter extends BaseAdapter {
        private PlacesAdapter() {
        }

        public int getCount() {
            return CheckInActivity.this.places.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return (long) ((GeoPlace) CheckInActivity.this.places.get(position)).id;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View view = convertView;
            if (view == null) {
                view = View.inflate(CheckInActivity.this, C0436R.layout.places_item, null);
                holder = new ViewHolder(null);
                holder.title = (TextView) view.findViewById(C0436R.id.flist_item_text);
                holder.subtitle = (TextView) view.findViewById(C0436R.id.flist_item_subtext);
                holder.checkins = (TextView) view.findViewById(C0436R.id.places_item_checkins);
                holder.photo = (ImageView) view.findViewById(C0436R.id.flist_item_photo);
                view.setTag(holder);
            }
            GeoPlace place = (GeoPlace) CheckInActivity.this.places.get(position);
            holder = (ViewHolder) view.getTag();
            holder.title.setText(place.title);
            holder.subtitle.setText(new StringBuilder(String.valueOf(place.distance > 0 ? place.distance + " " + CheckInActivity.this.getString(C0436R.string.meters) + ", " : ACRAConstants.DEFAULT_STRING_VALUE)).append(place.address).toString());
            holder.checkins.setText((place.checkins > 0 ? Integer.valueOf(place.checkins) : ACRAConstants.DEFAULT_STRING_VALUE));
            holder.checkins.setVisibility(place.checkins > 0 ? 0 : 4);
            if (place.id == -1) {
                holder.photo.setImageResource(C0436R.drawable.ic_current_place);
            } else if (place.id == -2) {
                holder.photo.setImageBitmap(null);
            } else if (place.photo == null) {
                holder.photo.setImageResource(C0436R.drawable.ic_place_placeholder);
            } else if (CheckInActivity.this.imgLoader.isAlreadyLoaded(place.photo)) {
                holder.photo.setImageBitmap(CheckInActivity.this.imgLoader.get(place.photo));
            } else {
                holder.photo.setImageResource(C0436R.drawable.ic_place_placeholder);
            }
            return view;
        }
    }

    private class SearchLoader implements Runnable {
        private SearchLoader() {
        }

        public void run() {
            CheckInActivity.this.loadData(CheckInActivity.this.searchView.getQuery());
        }
    }

    private class ViewHolder {
        TextView checkins;
        ImageView photo;
        TextView subtitle;
        TextView title;

        private ViewHolder() {
        }
    }

    /* renamed from: com.vkontakte.android.CheckInActivity.10 */
    class AnonymousClass10 implements Callback {
        private final /* synthetic */ String val$q;

        AnonymousClass10(String str) {
            this.val$q = str;
        }

        public void success(ArrayList<GeoPlace> _places) {
            CheckInActivity.this.currentRequest = null;
            CheckInActivity.this.places.clear();
            if (this.val$q == null) {
                CheckInActivity.this.places.add(CheckInActivity.this.currentLocation);
            }
            CheckInActivity.this.places.addAll(_places);
            if (this.val$q != null) {
                CheckInActivity.this.places.add(CheckInActivity.this.addPlace);
            }
            CheckInActivity.this.progress.setVisible(false);
            CheckInActivity.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            CheckInActivity.this.currentRequest = null;
        }
    }

    /* renamed from: com.vkontakte.android.CheckInActivity.4 */
    class C12774 extends MapView {
        C12774(Context $anonymous0, GoogleMapOptions $anonymous1) {
            super($anonymous0, $anonymous1);
        }

        public boolean dispatchTouchEvent(MotionEvent ev) {
            return false;
        }

        public void dispatchDraw(Canvas c) {
            c.save();
            if (VERSION.SDK_INT >= 14) {
                c.clipRect(0.0f, 0.0f, (float) getWidth(), ((float) getHeight()) - getTranslationY());
            }
            super.dispatchDraw(c);
            c.restore();
        }
    }

    /* renamed from: com.vkontakte.android.CheckInActivity.5 */
    class C12785 implements OnMyLocationChangeListener {
        C12785() {
        }

        public void onMyLocationChange(Location loc) {
            CheckInActivity.this.map.moveCamera(CameraUpdateFactory.newCameraPosition(new Builder().target(new LatLng(loc.getLatitude(), loc.getLongitude())).zoom(16.0f).build()));
            CheckInActivity.this.firstLocationUpdate = false;
            if (CheckInActivity.this.prevLocation != null && (CheckInActivity.this.prevLocation.distanceTo(loc) > 20.0f || (CheckInActivity.this.prevLocation.getAccuracy() > 100.0f && loc.getAccuracy() < 100.0f))) {
                CheckInActivity.this.prevLocation = loc;
                if (CheckInActivity.this.searchView.getQuery().length() == 0) {
                    CheckInActivity.this.loadData(null);
                }
            }
            if (CheckInActivity.this.prevLocation == null) {
                CheckInActivity.this.prevLocation = loc;
                CheckInActivity.this.loadData(null);
            }
        }
    }

    /* renamed from: com.vkontakte.android.CheckInActivity.8 */
    class C12798 implements OnQueryTextListener {
        C12798() {
        }

        public boolean onQueryTextSubmit(String query) {
            ((InputMethodManager) CheckInActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(CheckInActivity.this.getCurrentFocus().getWindowToken(), 0);
            if (CheckInActivity.this.searchRunnable != null) {
                CheckInActivity.this.list.removeCallbacks(CheckInActivity.this.searchRunnable);
                CheckInActivity.this.searchRunnable = null;
            }
            if (query.length() == 0) {
                query = null;
            }
            CheckInActivity.this.loadData(query);
            return true;
        }

        public boolean onQueryTextChange(String newText) {
            if (newText.length() == 0) {
                newText = null;
            }
            if (newText == null || newText.length() <= 0) {
                CheckInActivity.this.places = CheckInActivity.this.nearPlaces;
            } else {
                CheckInActivity.this.places = CheckInActivity.this.searchResults;
                CheckInActivity.this.searchResults.clear();
                CheckInActivity.this.progress.setVisible(true);
            }
            CheckInActivity.this.updateList();
            if (CheckInActivity.this.searchRunnable != null) {
                CheckInActivity.this.list.removeCallbacks(CheckInActivity.this.searchRunnable);
            }
            if (newText != null) {
                CheckInActivity.this.searchRunnable = new SearchLoader(null);
                CheckInActivity.this.list.postDelayed(CheckInActivity.this.searchRunnable, 1500);
            }
            return false;
        }
    }

    private class PlacePhotosAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.CheckInActivity.PlacePhotosAdapter.1 */
        class C02311 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C02311(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.flist_item_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private PlacePhotosAdapter() {
        }

        public int getItemCount() {
            return CheckInActivity.this.places.size();
        }

        public int getImageCountForItem(int item) {
            return ((GeoPlace) CheckInActivity.this.places.get(item)).photo != null ? 1 : 0;
        }

        public String getImageURL(int item, int image) {
            return ((GeoPlace) CheckInActivity.this.places.get(item)).photo;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += CheckInActivity.this.list.getHeaderViewsCount();
            if (item >= CheckInActivity.this.list.getFirstVisiblePosition() && item <= CheckInActivity.this.list.getLastVisiblePosition()) {
                CheckInActivity.this.runOnUiThread(new C02311(CheckInActivity.this.list.getChildAt(item - CheckInActivity.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    public CheckInActivity() {
        this.firstLocationUpdate = false;
        this.nearPlaces = new ArrayList();
        this.searchResults = new ArrayList();
        this.currentLocation = new GeoPlace();
        this.addPlace = new GeoPlace();
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        if (Global.isAppInstalled(this, "com.google.android.apps.maps") && GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext()) == 0) {
            this.places = this.nearPlaces;
            this.list = new ListView(this);
            this.list.setBackgroundColor(-1);
            this.list.setDrawSelectorOnTop(true);
            int pad = getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding);
            this.list.setDivider(new PaddingColorDrawable(-1644826, pad / 2));
            this.list.setDividerHeight(Global.scale(1.0f));
            try {
                MapsInitializer.initialize(this);
                this.mapView = new C12774(this, new GoogleMapOptions().compassEnabled(false).zoomControlsEnabled(false));
                this.mapView.setLayoutParams(new LayoutParams(-1, getResources().getDimensionPixelSize(C0436R.dimen.map_height)));
                this.mapView.onCreate(b);
                this.map = this.mapView.getMap();
                if (this.map == null) {
                    Toast.makeText(this, C0436R.string.error, 0).show();
                    finish();
                    return;
                }
                this.map.getUiSettings().setMyLocationButtonEnabled(false);
                this.map.setMyLocationEnabled(true);
                this.map.setOnMyLocationChangeListener(new C12785());
                this.progress = new LoadMoreFooterView(this);
                this.list.addHeaderView(this.mapView, null, false);
                this.list.addFooterView(this.progress, null, false);
                ListView listView = this.list;
                LayerDrawable ld = new LayerDrawable(new Drawable[]{getResources().getDrawable(C0436R.drawable.highlight)});
                listView.setSelector(ld);
                ld.setLayerInset(0, pad, 0, pad, 0);
                listView = this.list;
                ListAdapter placesAdapter = new PlacesAdapter();
                this.adapter = placesAdapter;
                listView.setAdapter(placesAdapter);
                setContentView(this.list);
                this.imgLoader = new ListImageLoaderWrapper(new PlacePhotosAdapter(), this.list, null);
                this.currentLocation.title = getString(C0436R.string.current_location);
                this.currentLocation.id = -1;
                this.currentLocation.address = getString(C0436R.string.loading);
                this.addPlace.title = getString(C0436R.string.add_place);
                this.addPlace.id = -2;
                this.places.add(this.currentLocation);
                this.list.setOnItemClickListener(new C02276());
                this.imgLoader.setOnScrollListener(new C02287());
                this.searchView = new SearchView(getSupportActionBar().getThemedContext());
                this.searchView.setQueryHint(getResources().getString(C0436R.string.search));
                try {
                    Field searchField = SearchView.class.getDeclaredField("mSearchButton");
                    searchField.setAccessible(true);
                    ((ImageView) searchField.get(this.searchView)).setImageResource(C0436R.drawable.ic_ab_search);
                    searchField = SearchView.class.getDeclaredField("mSearchPlate");
                    searchField.setAccessible(true);
                    ((TextView) ((LinearLayout) searchField.get(this.searchView)).getChildAt(0)).setHintTextColor(-2130706433);
                } catch (Exception e) {
                }
                this.searchView.setOnQueryTextListener(new C12798());
                return;
            } catch (GooglePlayServicesNotAvailableException e2) {
                Toast.makeText(this, C0436R.string.error, 0).show();
                finish();
                return;
            }
        }
        new VKAlertDialog.Builder(this).setTitle(C0436R.string.maps_not_available).setMessage(C0436R.string.maps_not_available_descr).setPositiveButton(C0436R.string.open_google_play, new C02241()).setNegativeButton(C0436R.string.close, new C02252()).setOnCancelListener(new C02263()).show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem search = menu.add((int) C0436R.string.search);
        search.setShowAsAction(2);
        search.setActionView(this.searchView);
        return true;
    }

    public void onPause() {
        super.onPause();
        if (this.mapView != null) {
            this.mapView.onPause();
        }
    }

    public void onResume() {
        super.onResume();
        if (this.mapView != null) {
            this.mapView.onResume();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.currentRequest != null) {
            this.currentRequest.cancel();
            this.currentRequest = null;
        }
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (resCode == -1 && reqCode == CHECKIN_RESULT) {
            setResult(-1, data);
            finish();
        }
        if (resCode == -1 && reqCode == CREATE_PLACE_RESULT) {
            setResult(-1, data);
            finish();
        }
    }

    private void loadData(String q) {
        int i = 1;
        if (this.currentRequest != null) {
            this.currentRequest.cancel();
        }
        if (this.prevLocation != null) {
            this.progress.setVisible(this.places.size() == 0);
            this.currentLocation.address = new StringBuilder(String.valueOf(this.prevLocation.getLatitude())).append(",").append(this.prevLocation.getLongitude()).toString();
            updateList();
            new Thread(new C02309()).start();
            if (q != null) {
                this.progress.setVisible(true);
            }
            double latitude = this.prevLocation.getLatitude();
            double longitude = this.prevLocation.getLongitude();
            if (q != null) {
                i = 3;
            } else if (this.prevLocation.getAccuracy() > 100.0f) {
                i = 2;
            }
            this.currentRequest = new PlacesSearch(latitude, longitude, i, q).setCallback(new AnonymousClass10(q)).exec((Activity) this);
        }
    }

    private void updateList() {
        this.adapter.notifyDataSetChanged();
        this.imgLoader.updateImages();
    }
}
