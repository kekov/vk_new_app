package com.vkontakte.android;

import android.animation.LayoutTransition;
import android.animation.LayoutTransition.TransitionListener;
import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.MotionEventCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.vkontakte.android.api.PollOption;
import com.vkontakte.android.api.PollsCreate;
import com.vkontakte.android.api.PollsCreate.Callback;
import com.vkontakte.android.api.PollsEdit;
import com.vkontakte.android.api.PollsGetById;
import com.vkontakte.android.api.PollsGetById.ExCallback;
import de.ankri.views.Switch;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import org.acra.ACRAConstants;

public class PollEditorActivity extends VKActivity {
    private TextWatcher changeListener;
    private ArrayList<PollOption> editOptions;
    private View okBtn;
    private ViewGroup options;
    private ArrayList<PollOption> origEditOptions;
    private PollAttachment poll;
    private int prevHeight;
    private OnClickListener removeClickListener;
    private boolean skipFrame;

    /* renamed from: com.vkontakte.android.PollEditorActivity.1 */
    class C03971 implements OnClickListener {
        C03971() {
        }

        public void onClick(View v) {
            if (PollEditorActivity.this.options.getChildCount() > 3) {
                PollEditorActivity.this.options.removeView((View) v.getParent());
                PollEditorActivity.this.findViewById(C0436R.id.poll_option_add).setVisibility(PollEditorActivity.this.options.getChildCount() < 11 ? 0 : 8);
                int index = ((Integer) v.getTag()).intValue();
                if (PollEditorActivity.this.editOptions != null && index < PollEditorActivity.this.editOptions.size()) {
                    PollEditorActivity.this.editOptions.remove(index);
                }
                PollEditorActivity.this.updateButton();
                PollEditorActivity.this.updateRemoveButtons();
            }
        }
    }

    /* renamed from: com.vkontakte.android.PollEditorActivity.2 */
    class C03982 implements TextWatcher {
        C03982() {
        }

        public void afterTextChanged(Editable s) {
            PollEditorActivity.this.updateButton();
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    /* renamed from: com.vkontakte.android.PollEditorActivity.3 */
    class C03993 implements OnClickListener {
        C03993() {
        }

        public void onClick(View v) {
            PollEditorActivity.this.done();
        }
    }

    /* renamed from: com.vkontakte.android.PollEditorActivity.4 */
    class C04004 implements OnClickListener {
        C04004() {
        }

        public void onClick(View v) {
            PollEditorActivity.this.addOptionRow(ACRAConstants.DEFAULT_STRING_VALUE);
        }
    }

    /* renamed from: com.vkontakte.android.PollEditorActivity.5 */
    class C04015 implements OnPreDrawListener {
        C04015() {
        }

        public boolean onPreDraw() {
            int h = PollEditorActivity.this.options.getHeight();
            boolean s = PollEditorActivity.this.skipFrame;
            if (PollEditorActivity.this.skipFrame) {
                PollEditorActivity.this.skipFrame = false;
                h = PollEditorActivity.this.prevHeight;
            } else {
                PollEditorActivity.this.prevHeight = h;
            }
            PollEditorActivity.this.findViewById(C0436R.id.poll_anonym_header).setTranslationY((float) (h - PollEditorActivity.this.options.getMeasuredHeight()));
            PollEditorActivity.this.findViewById(C0436R.id.poll_anonym_wrap).setTranslationY((float) (h - PollEditorActivity.this.options.getMeasuredHeight()));
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.PollEditorActivity.6 */
    class C04026 implements TransitionListener {
        C04026() {
        }

        public void startTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {
            if (container == PollEditorActivity.this.options && transitionType == 3) {
                PollEditorActivity.this.skipFrame = true;
                return;
            }
            PollEditorActivity.this.findViewById(C0436R.id.poll_anonym_header).setTranslationY((float) (PollEditorActivity.this.options.getHeight() - PollEditorActivity.this.options.getMeasuredHeight()));
            PollEditorActivity.this.findViewById(C0436R.id.poll_anonym_wrap).setTranslationY((float) (PollEditorActivity.this.options.getHeight() - PollEditorActivity.this.options.getMeasuredHeight()));
        }

        public void endTransition(LayoutTransition transition, ViewGroup container, View view, int transitionType) {
        }
    }

    /* renamed from: com.vkontakte.android.PollEditorActivity.8 */
    class C13388 implements Callback {
        C13388() {
        }

        public void success(PollAttachment poll) {
            Intent result = new Intent();
            result.putExtra("poll", poll);
            PollEditorActivity.this.setResult(-1, result);
            PollEditorActivity.this.finish();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PollEditorActivity.this, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.PollEditorActivity.9 */
    class C13399 implements PollsEdit.Callback {
        private final /* synthetic */ String val$question;

        C13399(String str) {
            this.val$question = str;
        }

        public void success() {
            PollEditorActivity.this.poll.question = this.val$question;
            Intent result = new Intent();
            result.putExtra("poll", PollEditorActivity.this.poll);
            PollEditorActivity.this.setResult(-1, result);
            PollEditorActivity.this.finish();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PollEditorActivity.this, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.PollEditorActivity.7 */
    class C16197 implements ExCallback {
        C16197() {
        }

        public void success(String question, int userAnswer, PollOption[] options, boolean pub) {
            PollEditorActivity.this.editOptions = new ArrayList();
            PollEditorActivity.this.editOptions.addAll(Arrays.asList(options));
            PollEditorActivity.this.origEditOptions = new ArrayList();
            PollEditorActivity.this.origEditOptions.addAll(Arrays.asList(options));
            for (PollOption opt : options) {
                PollEditorActivity.this.addOptionRow(opt.title);
            }
            ((TextView) PollEditorActivity.this.findViewById(C0436R.id.poll_question)).setText(question);
            PollEditorActivity.this.initAnimation();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PollEditorActivity.this, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
            PollEditorActivity.this.finish();
        }

        public void canceled() {
            PollEditorActivity.this.finish();
        }
    }

    public PollEditorActivity() {
        this.removeClickListener = new C03971();
        this.changeListener = new C03982();
        this.skipFrame = false;
        this.prevHeight = -1;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView((int) C0436R.layout.poll_edit);
        this.options = (ViewGroup) findViewById(C0436R.id.poll_options_container);
        this.okBtn = View.inflate(this, C0436R.layout.ab_done_right, null);
        ((TextView) this.okBtn.findViewById(C0436R.id.ab_done_text)).setText(this.poll == null ? C0436R.string.done : C0436R.string.save);
        this.okBtn.setOnClickListener(new C03993());
        if (getIntent().hasExtra("poll")) {
            setTitle(C0436R.string.poll_edit);
            this.poll = (PollAttachment) getIntent().getParcelableExtra("poll");
            findViewById(C0436R.id.poll_anonym_header).setVisibility(8);
            findViewById(C0436R.id.poll_anonym_wrap).setVisibility(8);
            loadPoll();
        } else {
            setTitle(C0436R.string.poll_create);
            addOptionRow(ACRAConstants.DEFAULT_STRING_VALUE);
            addOptionRow(ACRAConstants.DEFAULT_STRING_VALUE);
            initAnimation();
        }
        findViewById(C0436R.id.poll_option_add).setOnClickListener(new C04004());
        Switch sw = (Switch) findViewById(C0436R.id.poll_anonym_switch);
        sw.setTextOn(getString(C0436R.string.on).toUpperCase());
        sw.setTextOff(getString(C0436R.string.off).toUpperCase());
        sw.setTextColor(-1);
        sw.setTrackDrawable(getResources().getDrawable(C0436R.drawable.switch_track));
        sw.setThumbDrawable(getResources().getDrawable(C0436R.drawable.switch_thumb));
        sw.setSwitchTextColor(new ColorStateList(new int[][]{new int[0]}, new int[]{-1}));
        ((TextView) findViewById(C0436R.id.poll_question)).addTextChangedListener(this.changeListener);
    }

    private void initAnimation() {
        if (VERSION.SDK_INT >= 14) {
            LayoutTransition lt = new LayoutTransition();
            lt.setStartDelay(1, lt.getDuration(3) / 3);
            lt.setStartDelay(2, lt.getDuration(0) / 2);
            this.options.getViewTreeObserver().addOnPreDrawListener(new C04015());
            lt.addTransitionListener(new C04026());
            this.options.setLayoutTransition(lt);
        }
    }

    private void addOptionRow(String text) {
        View opt = View.inflate(this, C0436R.layout.poll_edit_option, null);
        opt.findViewById(C0436R.id.poll_option_remove).setTag(Integer.valueOf(this.options.getChildCount() - 1));
        opt.findViewById(C0436R.id.poll_option_remove).setOnClickListener(this.removeClickListener);
        ((TextView) opt.findViewById(C0436R.id.poll_option_text)).setText(text);
        ((TextView) opt.findViewById(C0436R.id.poll_option_text)).addTextChangedListener(this.changeListener);
        this.options.addView(opt, this.options.getChildCount() - 1);
        findViewById(C0436R.id.poll_option_add).setVisibility(this.options.getChildCount() < 11 ? 0 : 8);
        if (VERSION.SDK_INT >= 14) {
            ((ViewGroup) opt).getLayoutTransition().setAnimateParentHierarchy(false);
        }
        updateButton();
        updateRemoveButtons();
    }

    private void updateRemoveButtons() {
        for (int i = 0; i < this.options.getChildCount() - 1; i++) {
            this.options.getChildAt(i).findViewById(C0436R.id.poll_option_remove).setVisibility(this.options.getChildCount() > 3 ? 0 : 8);
        }
    }

    private void loadPoll() {
        new PollsGetById(this.poll.oid, this.poll.pid).setCallback(new C16197()).wrapProgress(this).exec((Activity) this);
    }

    private void done() {
        int i;
        View v;
        if (this.poll == null) {
            ArrayList<String> options = new ArrayList();
            for (i = 0; i < this.options.getChildCount(); i++) {
                v = this.options.getChildAt(i);
                if (v.findViewById(C0436R.id.poll_option_text) == null) {
                    break;
                }
                options.add(((TextView) v.findViewById(C0436R.id.poll_option_text)).getText().toString());
            }
            new PollsCreate(((TextView) findViewById(C0436R.id.poll_question)).getText().toString(), options, getIntent().getIntExtra("oid", 0), ((Switch) findViewById(C0436R.id.poll_anonym_switch)).isChecked()).setCallback(new C13388()).wrapProgress(this).exec((Activity) this);
            return;
        }
        String str;
        ArrayList<Integer> remove = new ArrayList();
        ArrayList<String> add = new ArrayList();
        HashMap<String, String> edit = new HashMap();
        for (i = 0; i < this.options.getChildCount(); i++) {
            v = this.options.getChildAt(i);
            if (v.findViewById(C0436R.id.poll_option_text) == null) {
                break;
            }
            String txt = ((TextView) v.findViewById(C0436R.id.poll_option_text)).getText().toString();
            if (i >= this.editOptions.size()) {
                add.add(txt);
            } else if (!((PollOption) this.editOptions.get(i)).title.equals(txt)) {
                edit.put(new StringBuilder(String.valueOf(((PollOption) this.editOptions.get(i)).id)).toString(), txt);
            }
        }
        Iterator it = this.origEditOptions.iterator();
        while (it.hasNext()) {
            PollOption o = (PollOption) it.next();
            if (!this.editOptions.contains(o)) {
                remove.add(Integer.valueOf(o.id));
            }
        }
        String question = ((TextView) findViewById(C0436R.id.poll_question)).getText().toString();
        int i2 = this.poll.oid;
        int i3 = this.poll.pid;
        if (question.equals(this.poll.question)) {
            str = null;
        } else {
            str = question;
        }
        new PollsEdit(i2, i3, str, add, remove, edit).setCallback(new C13399(question)).wrapProgress(this).exec((Activity) this);
    }

    private void updateButton() {
        boolean disable;
        boolean z = true;
        if (((TextView) findViewById(C0436R.id.poll_question)).getText().toString().trim().length() == 0) {
            disable = true;
        } else {
            disable = false;
        }
        for (int i = 0; i < this.options.getChildCount(); i++) {
            View v = this.options.getChildAt(i);
            if (v.findViewById(C0436R.id.poll_option_text) == null) {
                break;
            }
            int i2;
            if (((TextView) v.findViewById(C0436R.id.poll_option_text)).getText().toString().trim().length() == 0) {
                i2 = 1;
            } else {
                i2 = 0;
            }
            disable |= i2;
        }
        View view = this.okBtn;
        if (disable) {
            z = false;
        }
        view.setEnabled(z);
        if (VERSION.SDK_INT >= 11) {
            this.okBtn.findViewById(C0436R.id.ab_done_text).setAlpha(!disable ? 1.0f : 0.5f);
            return;
        }
        ((TextView) this.okBtn.findViewById(C0436R.id.ab_done_text)).getCompoundDrawables()[0].setAlpha(!disable ? MotionEventCompat.ACTION_MASK : TransportMediator.FLAG_KEY_MEDIA_NEXT);
        ((TextView) this.okBtn.findViewById(C0436R.id.ab_done_text)).setTextColor(!disable ? -1 : -2130706433);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add((int) C0436R.string.send);
        item.setActionView(this.okBtn);
        item.setShowAsAction(2);
        return true;
    }
}
