package com.vkontakte.android;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.text.ClipboardManager;
import android.text.Html;
import android.util.AttributeSet;
import android.util.StateSet;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView.LayoutParams;
import android.widget.AbsListView.RecyclerListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.facebook.WebDialog;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.FaveGetPosts;
import com.vkontakte.android.api.NewsfeedAddBan;
import com.vkontakte.android.api.NewsfeedGet;
import com.vkontakte.android.api.NewsfeedGet.Callback;
import com.vkontakte.android.api.NewsfeedGetComments;
import com.vkontakte.android.api.NewsfeedSearch;
import com.vkontakte.android.api.NewsfeedUnsubscribe;
import com.vkontakte.android.api.WallDelete;
import com.vkontakte.android.api.WallEdit;
import com.vkontakte.android.api.WallGet;
import com.vkontakte.android.cache.NewsfeedCache;
import com.vkontakte.android.cache.NewsfeedCommentsCache;
import com.vkontakte.android.cache.UserWallCache;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.data.Posts;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.OverlayTextView;
import com.vkontakte.android.ui.PhotoView;
import com.vkontakte.android.ui.RefreshableListView;
import com.vkontakte.android.ui.RefreshableListView.OnRefreshListener;
import com.vkontakte.android.ui.posts.AttachContainerPostDisplayItem;
import com.vkontakte.android.ui.posts.AudioPostDisplayItem;
import com.vkontakte.android.ui.posts.ButtonsPostDisplayItem;
import com.vkontakte.android.ui.posts.CommentPostDisplayItem;
import com.vkontakte.android.ui.posts.CommonAttachmentPostDisplayItem;
import com.vkontakte.android.ui.posts.FooterPostDisplayItem;
import com.vkontakte.android.ui.posts.HeaderPostDisplayItem;
import com.vkontakte.android.ui.posts.PostDisplayItem;
import com.vkontakte.android.ui.posts.PostHighlightDrawable;
import com.vkontakte.android.ui.posts.RepostPostDisplayItem;
import com.vkontakte.android.ui.posts.SignaturePostDisplayItem;
import com.vkontakte.android.ui.posts.TextPostDisplayItem;
import com.vkontakte.android.ui.posts.ThumbsBlockPostDisplayItem;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.acra.ACRAConstants;

public class NewsView extends FrameLayout implements OnRefreshListener {
    public static final int MODE_COMMENTS = 3;
    public static final int MODE_DEFAULT = 0;
    public static final int MODE_FAVE = 2;
    public static final int MODE_PHOTOS = 1;
    public static final int MODE_POSTPONED = 5;
    public static final int MODE_SEARCH = 4;
    public static final int MODE_SUGGESTED = 6;
    private static boolean viewsPrecreated;
    protected ProgressBar bigProgress;
    private boolean clearForNew;
    protected String commentsFrom;
    private APIRequest currentReq;
    protected boolean dataLoading;
    protected EmptyView emptyView;
    protected ErrorView errorView;
    protected FrameLayout footerView;
    private PostHighlightDrawable highlight;
    protected ListImageLoaderWrapper imgLoader;
    private int itemLayout;
    private ArrayList<PostDisplayItem> items;
    protected int lastUpdateTime;
    RefreshableListView list;
    private int listID;
    private int mode;
    protected boolean moreAvailable;
    private String newFrom;
    protected Vector<NewsEntry> newNews;
    private APIRequest newNewsReq;
    private int newOffset;
    private OverlayTextView newPostsBtn;
    protected Vector<NewsEntry> news;
    protected int offset;
    private Runnable onPauseRunnable;
    protected boolean preloadOnReady;
    protected Vector<NewsEntry> preloadedNews;
    protected boolean preloading;
    protected boolean prependNewEntries;
    protected boolean refreshingOnStart;
    private String searchQuery;

    /* renamed from: com.vkontakte.android.NewsView.13 */
    class AnonymousClass13 implements Runnable {
        private final /* synthetic */ ArrayList val$forCache;

        AnonymousClass13(ArrayList arrayList) {
            this.val$forCache = arrayList;
        }

        public void run() {
            NewsfeedCache.replace(this.val$forCache, NewsView.this.getContext());
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.15 */
    class AnonymousClass15 implements Runnable {
        private final /* synthetic */ ArrayList val$forCache;

        AnonymousClass15(ArrayList arrayList) {
            this.val$forCache = arrayList;
        }

        public void run() {
            Iterator it = this.val$forCache.iterator();
            while (it.hasNext()) {
                NewsfeedCache.add((NewsEntry) it.next(), NewsView.this.getContext());
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.19 */
    class AnonymousClass19 implements Runnable {
        private final /* synthetic */ boolean val$refresh;

        /* renamed from: com.vkontakte.android.NewsView.19.2 */
        class C03492 implements Runnable {
            private final /* synthetic */ boolean val$refresh;

            C03492(boolean z) {
                this.val$refresh = z;
            }

            public void run() {
                NewsView.this.onDataLoaded(Posts.feed, this.val$refresh);
                NewsView.this.preloadedNews.addAll(Posts.preloadedFeed);
                NewsView.this.list.setSelectionFromTop(Posts.feedItem, Posts.feedItemOffset);
                NewsView.this.commentsFrom = Posts.feedFrom;
                NewsView.this.offset = Posts.feedOffset;
            }
        }

        /* renamed from: com.vkontakte.android.NewsView.19.3 */
        class C03503 implements Runnable {
            private final /* synthetic */ ArrayList val$e;
            private final /* synthetic */ boolean val$refresh;

            C03503(ArrayList arrayList, boolean z) {
                this.val$e = arrayList;
                this.val$refresh = z;
            }

            public void run() {
                if (this.val$e.size() <= 10) {
                    NewsView.this.onDataLoaded(this.val$e, this.val$refresh);
                } else {
                    NewsView.this.onDataLoaded(this.val$e.subList(NewsView.MODE_DEFAULT, 10), this.val$refresh);
                    for (int i = 10; i < this.val$e.size(); i += NewsView.MODE_PHOTOS) {
                        NewsView.this.preloadedNews.add((NewsEntry) this.val$e.get(i));
                    }
                }
                NewsView.this.lastUpdateTime = NewsfeedCache.getUpdateTime(NewsView.this.getContext());
            }
        }

        /* renamed from: com.vkontakte.android.NewsView.19.1 */
        class C13221 implements Callback {
            private final /* synthetic */ boolean val$refresh;

            /* renamed from: com.vkontakte.android.NewsView.19.1.1 */
            class C03471 implements Runnable {
                private final /* synthetic */ String val$newFrom;
                private final /* synthetic */ ArrayList val$news;
                private final /* synthetic */ boolean val$refresh;

                C03471(boolean z, String str, ArrayList arrayList) {
                    this.val$refresh = z;
                    this.val$newFrom = str;
                    this.val$news = arrayList;
                }

                public void run() {
                    if (this.val$refresh && NewsView.this.newNewsReq != null) {
                        NewsView.this.newNewsReq.cancel();
                        NewsView.this.newNewsReq = null;
                    }
                    if (this.val$refresh) {
                        NewsView.this.newNews.clear();
                        NewsView.this.updateNewPostsBtn();
                    }
                    if (!"0".equals(this.val$newFrom)) {
                        NewsView.this.commentsFrom = this.val$newFrom;
                    }
                    if (NewsView.this.mode == NewsView.MODE_PHOTOS) {
                        Iterator it = this.val$news.iterator();
                        while (it.hasNext()) {
                            NewsEntry e = (NewsEntry) it.next();
                            CharSequence charSequence = ACRAConstants.DEFAULT_STRING_VALUE;
                            e.text = charSequence;
                            e.displayablePreviewText = charSequence;
                        }
                    }
                    int i;
                    if (this.val$news.size() > 10) {
                        NewsView.this.onDataLoaded(this.val$news.subList(NewsView.MODE_DEFAULT, 10), this.val$refresh);
                        if (this.val$refresh) {
                            NewsView.this.preloadedNews.clear();
                        }
                        for (i = 10; i < this.val$news.size(); i += NewsView.MODE_PHOTOS) {
                            NewsView.this.preloadedNews.add((NewsEntry) this.val$news.get(i));
                        }
                    } else if (NewsView.this.preloading) {
                        for (i = NewsView.MODE_DEFAULT; i < this.val$news.size(); i += NewsView.MODE_PHOTOS) {
                            NewsView.this.preloadedNews.add((NewsEntry) this.val$news.get(i));
                        }
                        NewsView.this.dataLoading = false;
                    } else {
                        NewsView.this.onDataLoaded(this.val$news, this.val$refresh);
                    }
                    NewsView.this.preloading = false;
                    if (NewsView.this.preloadOnReady) {
                        NewsView.this.preloadOnReady = false;
                        NewsView.this.preloading = true;
                        NewsView.this.loadData(false);
                    }
                }
            }

            /* renamed from: com.vkontakte.android.NewsView.19.1.2 */
            class C03482 implements Runnable {
                private final /* synthetic */ int val$ecode;
                private final /* synthetic */ String val$emsg;
                private final /* synthetic */ boolean val$refresh;

                C03482(boolean z, int i, String str) {
                    this.val$refresh = z;
                    this.val$ecode = i;
                    this.val$emsg = str;
                }

                public void run() {
                    if (this.val$refresh) {
                        NewsView.this.list.refreshDone();
                    }
                    NewsView.this.onError(this.val$ecode, this.val$emsg);
                    NewsView.this.currentReq = null;
                }
            }

            C13221(boolean z) {
                this.val$refresh = z;
            }

            public void success(ArrayList<NewsEntry> news, String newFrom) {
                NewsView.this.currentReq = null;
                if ((this.val$refresh || NewsView.this.lastUpdateTime == 0) && NewsView.this.mode != NewsView.MODE_PHOTOS) {
                    NewsfeedCache.replace(news, NewsView.this.getContext());
                    NewsView.this.getContext().getSharedPreferences("news", NewsView.MODE_DEFAULT).edit().putString("feed_from", newFrom).commit();
                }
                ((Activity) NewsView.this.getContext()).runOnUiThread(new C03471(this.val$refresh, newFrom, news));
            }

            public void fail(int ecode, String emsg) {
                NewsView access$0 = NewsView.this;
                NewsView access$02 = NewsView.this;
                NewsView.this.dataLoading = false;
                access$02.prependNewEntries = false;
                access$0.refreshingOnStart = false;
                ((Activity) NewsView.this.getContext()).runOnUiThread(new C03482(this.val$refresh, ecode, emsg));
            }
        }

        AnonymousClass19(boolean z) {
            this.val$refresh = z;
        }

        public void run() {
            boolean z = true;
            if (NewsView.this.lastUpdateTime > 0 || this.val$refresh || NewsView.this.mode == NewsView.MODE_PHOTOS || (NewsView.this.news.size() == 0 && !NewsfeedCache.hasEntries(NewsView.this.getContext()))) {
                if (this.val$refresh) {
                    NewsView.this.commentsFrom = ACRAConstants.DEFAULT_STRING_VALUE;
                }
                NewsView newsView = NewsView.this;
                String str = NewsView.this.commentsFrom;
                int i = NewsView.this.preloading ? 10 : 20;
                if (NewsView.this.mode != NewsView.MODE_PHOTOS) {
                    z = false;
                }
                newsView.currentReq = new NewsfeedGet(str, i, z, NewsView.this.listID).setCallback(new C13221(this.val$refresh));
                NewsView.this.currentReq.execSync();
            } else if (Posts.feed.size() > 0) {
                ((Activity) NewsView.this.getContext()).runOnUiThread(new C03492(this.val$refresh));
            } else {
                ((Activity) NewsView.this.getContext()).runOnUiThread(new C03503(NewsfeedCache.get(NewsView.this.getContext()), this.val$refresh));
                NewsView.this.dataLoading = false;
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.1 */
    class C03511 implements Runnable {
        C03511() {
        }

        public void run() {
            if (!NewsView.viewsPrecreated) {
                String[] types = new String[]{"photo", "common", "audio", "video", "signature", "album", "doc_thumb"};
                int[] nums = new int[]{20, 20, 20, 15, NewsView.MODE_POSTPONED, NewsView.MODE_POSTPONED, NewsView.MODE_POSTPONED};
                for (int i = NewsView.MODE_DEFAULT; i < types.length; i += NewsView.MODE_PHOTOS) {
                    ArrayList<View> vs = new ArrayList();
                    for (int j = NewsView.MODE_DEFAULT; j < nums[i]; j += NewsView.MODE_PHOTOS) {
                        vs.add(Attachment.getReusableView(NewsView.this.getContext(), types[i]));
                    }
                    Iterator it = vs.iterator();
                    while (it.hasNext()) {
                        Attachment.reuseView((View) it.next(), types[i]);
                    }
                }
                NewsView.viewsPrecreated = true;
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.21 */
    class AnonymousClass21 implements Runnable {
        private final /* synthetic */ boolean val$refresh;

        /* renamed from: com.vkontakte.android.NewsView.21.1 */
        class C03521 implements Runnable {
            private final /* synthetic */ ArrayList val$e;
            private final /* synthetic */ boolean val$refresh;

            C03521(ArrayList arrayList, boolean z) {
                this.val$e = arrayList;
                this.val$refresh = z;
            }

            public void run() {
                if (this.val$e.size() <= 10) {
                    NewsView.this.onDataLoaded(this.val$e, this.val$refresh);
                } else {
                    ArrayList<NewsEntry> nn = new ArrayList();
                    nn.addAll(this.val$e.subList(NewsView.MODE_DEFAULT, 10));
                    NewsView.this.onDataLoaded(nn, this.val$refresh);
                    for (int i = 10; i < this.val$e.size(); i += NewsView.MODE_PHOTOS) {
                        NewsView.this.preloadedNews.add((NewsEntry) this.val$e.get(i));
                    }
                }
                NewsView.this.lastUpdateTime = NewsfeedCommentsCache.getUpdateTime(NewsView.this.getContext());
            }
        }

        AnonymousClass21(boolean z) {
            this.val$refresh = z;
        }

        public void run() {
            ((Activity) NewsView.this.getContext()).runOnUiThread(new C03521(NewsfeedCommentsCache.get(NewsView.this.getContext()), this.val$refresh));
            NewsView.this.dataLoading = false;
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.25 */
    class AnonymousClass25 implements OnMenuItemClickListener {
        private final /* synthetic */ ArrayList val$acts;
        private final /* synthetic */ NewsEntry val$e;

        AnonymousClass25(ArrayList arrayList, NewsEntry newsEntry) {
            this.val$acts = arrayList;
            this.val$e = newsEntry;
        }

        public boolean onMenuItemClick(MenuItem item) {
            NewsView.this.performPostAction((String) this.val$acts.get(item.getItemId()), this.val$e);
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.26 */
    class AnonymousClass26 implements OnClickListener {
        private final /* synthetic */ ArrayList val$acts;
        private final /* synthetic */ NewsEntry val$e;

        AnonymousClass26(ArrayList arrayList, NewsEntry newsEntry) {
            this.val$acts = arrayList;
            this.val$e = newsEntry;
        }

        public void onClick(DialogInterface dialog, int which) {
            NewsView.this.performPostAction((String) this.val$acts.get(which), this.val$e);
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.27 */
    class AnonymousClass27 implements OnClickListener {
        private final /* synthetic */ NewsEntry val$e;

        AnonymousClass27(NewsEntry newsEntry) {
            this.val$e = newsEntry;
        }

        public void onClick(DialogInterface dialog, int which) {
            NewsView.this.deletePost(this.val$e);
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.29 */
    class AnonymousClass29 implements OnClickListener {
        private final /* synthetic */ NewsEntry val$e;
        private final /* synthetic */ EditText val$edit;

        AnonymousClass29(NewsEntry newsEntry, EditText editText) {
            this.val$e = newsEntry;
            this.val$edit = editText;
        }

        public void onClick(DialogInterface dialog, int which) {
            NewsView.this.saveRepostComment(this.val$e, this.val$edit.getText().toString());
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.34 */
    class AnonymousClass34 implements View.OnClickListener {
        private final /* synthetic */ NewsEntry val$e;

        AnonymousClass34(NewsEntry newsEntry) {
            this.val$e = newsEntry;
        }

        public void onClick(View v) {
            NewsView.this.showItemOptions(v, this.val$e);
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.3 */
    class C03533 implements OnTouchListener {
        C03533() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            try {
                if (event.getAction() == 0) {
                    int pos = NewsView.this.list.pointToPosition((int) event.getX(), (int) event.getY());
                    if (pos >= NewsView.this.getPostsOffset() + NewsView.this.list.getHeaderViewsCount()) {
                        PostDisplayItem sItem = (PostDisplayItem) NewsView.this.items.get((pos - NewsView.this.getPostsOffset()) - NewsView.this.list.getHeaderViewsCount());
                        View itemView = NewsView.this.list.getChildAt(pos - NewsView.this.list.getFirstVisiblePosition());
                        int vTop = itemView.getTop();
                        int vBtm = itemView.getTop() + itemView.getHeight();
                        int top = vTop;
                        int btm = vBtm;
                        int i = NewsView.MODE_DEFAULT;
                        Iterator it = NewsView.this.items.iterator();
                        while (it.hasNext()) {
                            PostDisplayItem item = (PostDisplayItem) it.next();
                            int i2 = item.postID;
                            int i3 = sItem.postID;
                            if (i2 == r0) {
                                i2 = item.postOwnerID;
                                i3 = sItem.postOwnerID;
                                if (i2 == r0) {
                                    int vpos = (NewsView.this.getPostsOffset() + i) + NewsView.this.list.getHeaderViewsCount();
                                    if (vpos >= NewsView.this.list.getFirstVisiblePosition() && vpos <= NewsView.this.list.getLastVisiblePosition()) {
                                        View vview = NewsView.this.list.getChildAt(vpos - NewsView.this.list.getFirstVisiblePosition());
                                        top = Math.min(top, vview.getTop());
                                        btm = Math.max(btm, vview.getTop() + vview.getHeight());
                                    }
                                }
                            }
                            i += NewsView.MODE_PHOTOS;
                        }
                        int extendTop = vTop - top;
                        NewsView.this.highlight.extendBottom = btm - vBtm;
                        NewsView.this.highlight.extendTop = extendTop;
                    }
                }
            } catch (Exception e) {
            }
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.5 */
    class C03545 implements OnItemClickListener {
        C03545() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
            try {
                PostDisplayItem item = (PostDisplayItem) NewsView.this.items.get((pos - NewsView.this.list.getHeaderViewsCount()) - NewsView.this.getPostsOffset());
                NewsEntry e = null;
                Iterator it = NewsView.this.news.iterator();
                while (it.hasNext()) {
                    NewsEntry post = (NewsEntry) it.next();
                    if (post.ownerID == item.postOwnerID && post.postID == item.postID) {
                        e = post;
                        break;
                    }
                }
                if (e != null && e.type != NewsView.MODE_SUGGESTED && e.type != 7) {
                    Bundle args;
                    if (e.type == NewsView.MODE_SEARCH) {
                        args = new Bundle();
                        args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, e.text);
                        args.putInt("gid", -e.ownerID);
                        args.putInt("tid", e.postID);
                        args.putInt("offset", e.numComments - (e.numComments % 20));
                        Navigate.to("BoardTopicViewFragment", args, (Activity) NewsView.this.getContext());
                        return;
                    }
                    args = new Bundle();
                    args.putParcelable("entry", e);
                    Navigate.to("PostViewFragment", args, (Activity) NewsView.this.getContext());
                }
            } catch (Exception e2) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.6 */
    class C03556 implements RecyclerListener {
        C03556() {
        }

        public void onMovedToScrapHeap(View view) {
            if (view instanceof NewsItemView) {
                ((NewsItemView) view).resetAttachments();
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.7 */
    class C03567 implements View.OnClickListener {
        C03567() {
        }

        public void onClick(View v) {
            NewsView.this.onEmptyBtnClick();
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.8 */
    class C03578 implements View.OnClickListener {
        C03578() {
        }

        public void onClick(View v) {
            NewsView.this.errorView.setVisibility(8);
            NewsView.this.bigProgress.setVisibility(NewsView.MODE_DEFAULT);
            NewsView.this.loadData(true);
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.9 */
    class C03589 implements View.OnClickListener {
        C03589() {
        }

        public void onClick(View v) {
            NewsView.this.showNew();
        }
    }

    protected class NewsAdapter extends BaseAdapter {
        protected NewsAdapter() {
        }

        public int getCount() {
            return NewsView.this.items.size();
        }

        public Object getItem(int pos) {
            return Integer.valueOf(pos);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public boolean isEnabled(int position) {
            return ((PostDisplayItem) NewsView.this.items.get(position)).clickable;
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public int getViewTypeCount() {
            return 20;
        }

        public int getItemViewType(int pos) {
            return ((PostDisplayItem) NewsView.this.items.get(pos)).getType();
        }

        public View getView(int position, View cview, ViewGroup group) {
            PostDisplayItem item = (PostDisplayItem) NewsView.this.items.get(position);
            View view = item.getView(NewsView.this.getContext(), cview);
            for (int i = NewsView.MODE_DEFAULT; i < item.getImageCount(); i += NewsView.MODE_PHOTOS) {
                String url = item.getImageURL(i);
                if (NewsView.this.imgLoader.isAlreadyLoaded(url)) {
                    item.setImage(i, view, NewsView.this.imgLoader.get(url), true);
                } else {
                    item.setImage(i, view, null, false);
                }
            }
            int bg = NewsView.MODE_DEFAULT;
            switch (item.bgType) {
                case NewsView.MODE_DEFAULT /*0*/:
                    bg = C0436R.drawable.bg_post_comments_mid_nosep;
                    break;
                case NewsView.MODE_PHOTOS /*1*/:
                    bg = C0436R.drawable.bg_post_comments_top;
                    break;
                case NewsView.MODE_FAVE /*2*/:
                    bg = C0436R.drawable.bg_post_comments_btm_nosep;
                    break;
            }
            if (bg != 0) {
                view.setBackgroundResource(bg);
            }
            view.setLayoutParams(new LayoutParams(-1, -2));
            return view;
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.17 */
    class AnonymousClass17 implements FaveGetPosts.Callback {
        private final /* synthetic */ int val$count;
        private final /* synthetic */ boolean val$refresh;

        AnonymousClass17(int i, boolean z) {
            this.val$count = i;
            this.val$refresh = z;
        }

        public void success(ArrayList<NewsEntry> news, int total) {
            NewsView.this.currentReq = null;
            NewsView newsView = NewsView.this;
            newsView.offset += this.val$count;
            int i;
            if (news.size() > 10) {
                NewsView.this.onDataLoaded(news.subList(NewsView.MODE_DEFAULT, 10), this.val$refresh);
                if (this.val$refresh) {
                    NewsView.this.preloadedNews.clear();
                }
                for (i = 10; i < news.size(); i += NewsView.MODE_PHOTOS) {
                    NewsView.this.preloadedNews.add((NewsEntry) news.get(i));
                }
            } else if (NewsView.this.preloading) {
                for (i = NewsView.MODE_DEFAULT; i < news.size(); i += NewsView.MODE_PHOTOS) {
                    NewsView.this.preloadedNews.add((NewsEntry) news.get(i));
                }
            } else {
                NewsView.this.onDataLoaded(news, this.val$refresh);
            }
            NewsView.this.dataLoading = false;
            NewsView.this.preloading = false;
            if (NewsView.this.preloadOnReady) {
                NewsView.this.preloadOnReady = false;
                NewsView.this.preloading = true;
                NewsView.this.loadData(false);
            }
        }

        public void fail(int ecode, String emsg) {
            NewsView newsView = NewsView.this;
            NewsView newsView2 = NewsView.this;
            NewsView.this.dataLoading = false;
            newsView2.prependNewEntries = false;
            newsView.refreshingOnStart = false;
            if (this.val$refresh) {
                NewsView.this.list.refreshDone();
            }
            NewsView.this.onError(ecode, emsg);
            NewsView.this.currentReq = null;
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.18 */
    class AnonymousClass18 implements NewsfeedSearch.Callback {
        private final /* synthetic */ boolean val$refresh;

        AnonymousClass18(boolean z) {
            this.val$refresh = z;
        }

        public void success(ArrayList<NewsEntry> news, String newFrom) {
            NewsView.this.currentReq = null;
            if (!"0".equals(newFrom)) {
                NewsView.this.commentsFrom = newFrom;
            }
            int i;
            if (news.size() > 10) {
                NewsView.this.onDataLoaded(news.subList(NewsView.MODE_DEFAULT, 10), this.val$refresh);
                if (this.val$refresh) {
                    NewsView.this.preloadedNews.clear();
                }
                for (i = 10; i < news.size(); i += NewsView.MODE_PHOTOS) {
                    NewsView.this.preloadedNews.add((NewsEntry) news.get(i));
                }
            } else if (NewsView.this.preloading) {
                for (i = NewsView.MODE_DEFAULT; i < news.size(); i += NewsView.MODE_PHOTOS) {
                    NewsView.this.preloadedNews.add((NewsEntry) news.get(i));
                }
            } else {
                NewsView.this.onDataLoaded(news, this.val$refresh);
            }
            NewsView.this.dataLoading = false;
            NewsView.this.preloading = false;
            if (NewsView.this.preloadOnReady) {
                NewsView.this.preloadOnReady = false;
                NewsView.this.preloading = true;
                NewsView.this.loadData(false);
            }
        }

        public void fail(int ecode, String emsg) {
            NewsView newsView = NewsView.this;
            NewsView newsView2 = NewsView.this;
            NewsView.this.dataLoading = false;
            newsView2.prependNewEntries = false;
            newsView.refreshingOnStart = false;
            if (this.val$refresh) {
                NewsView.this.list.refreshDone();
            }
            NewsView.this.onError(ecode, emsg);
            NewsView.this.currentReq = null;
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.20 */
    class AnonymousClass20 implements NewsfeedGetComments.Callback {
        private final /* synthetic */ boolean val$refresh;

        AnonymousClass20(boolean z) {
            this.val$refresh = z;
        }

        public void success(ArrayList<NewsEntry> news, String newFrom) {
            NewsView.this.currentReq = null;
            if (!"0".equals(newFrom)) {
                NewsView.this.commentsFrom = newFrom;
            }
            if (this.val$refresh || NewsView.this.lastUpdateTime == 0) {
                NewsfeedCommentsCache.replace(news, NewsView.this.getContext());
                NewsView.this.getContext().getSharedPreferences("news", NewsView.MODE_DEFAULT).edit().putString("comments_from", newFrom).commit();
            }
            int i;
            if (news.size() > 10) {
                NewsView.this.onDataLoaded(news.subList(NewsView.MODE_DEFAULT, 10), this.val$refresh);
                for (i = 10; i < news.size(); i += NewsView.MODE_PHOTOS) {
                    NewsView.this.preloadedNews.add((NewsEntry) news.get(i));
                }
            } else if (NewsView.this.preloading) {
                for (i = NewsView.MODE_DEFAULT; i < news.size(); i += NewsView.MODE_PHOTOS) {
                    NewsView.this.preloadedNews.add((NewsEntry) news.get(i));
                }
                NewsView.this.dataLoading = false;
            } else {
                NewsView.this.onDataLoaded(news, this.val$refresh);
            }
            NewsView.this.preloading = false;
            if (NewsView.this.preloadOnReady) {
                NewsView.this.preloadOnReady = false;
                NewsView.this.preloading = true;
                NewsView.this.loadData(false);
            }
        }

        public void fail(int ecode, String emsg) {
            NewsView.this.dataLoading = false;
            if (this.val$refresh) {
                NewsView.this.list.refreshDone();
            }
            NewsView.this.onError(ecode, emsg);
            NewsView.this.currentReq = null;
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.22 */
    class AnonymousClass22 implements WallGet.Callback {
        private final /* synthetic */ boolean val$refresh;

        AnonymousClass22(boolean z) {
            this.val$refresh = z;
        }

        public void success(ArrayList<NewsEntry> news, int total, Object status, int postponedCount, int suggestedCount) {
            NewsView.this.currentReq = null;
            int i;
            if (news.size() > 10) {
                NewsView.this.onDataLoaded(news.subList(NewsView.MODE_DEFAULT, 10), this.val$refresh);
                for (i = 10; i < news.size(); i += NewsView.MODE_PHOTOS) {
                    NewsView.this.preloadedNews.add((NewsEntry) news.get(i));
                }
            } else if (NewsView.this.preloading) {
                for (i = NewsView.MODE_DEFAULT; i < news.size(); i += NewsView.MODE_PHOTOS) {
                    NewsView.this.preloadedNews.add((NewsEntry) news.get(i));
                }
                NewsView.this.dataLoading = false;
            } else {
                NewsView.this.onDataLoaded(news, this.val$refresh);
            }
            NewsView newsView = NewsView.this;
            newsView.offset = (NewsView.this.preloading ? 10 : 20) + newsView.offset;
            NewsView.this.preloading = false;
            if (NewsView.this.preloadOnReady) {
                NewsView.this.preloadOnReady = false;
                NewsView.this.preloading = true;
                NewsView.this.loadData(false);
            }
        }

        public void fail(int ecode, String emsg) {
            NewsView.this.dataLoading = false;
            if (this.val$refresh) {
                NewsView.this.list.refreshDone();
            }
            NewsView.this.onError(ecode, emsg);
            NewsView.this.currentReq = null;
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.28 */
    class AnonymousClass28 implements NewsfeedUnsubscribe.Callback {
        private final /* synthetic */ NewsEntry val$e;

        AnonymousClass28(NewsEntry newsEntry) {
            this.val$e = newsEntry;
        }

        public void success() {
            NewsView.this.remove(this.val$e.ownerID, this.val$e.postID);
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(NewsView.this.getContext(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, NewsView.MODE_DEFAULT).show();
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.2 */
    class C13232 extends RefreshableListView {
        long upTime;

        C13232(Context $anonymous0) {
            super($anonymous0);
            this.upTime = 0;
        }

        public boolean onTouchEvent(MotionEvent ev) {
            if (ev.getAction() == NewsView.MODE_PHOTOS) {
                this.upTime = System.currentTimeMillis();
                invalidate();
            }
            return super.onTouchEvent(ev);
        }

        public void dispatchDraw(Canvas c) {
            super.dispatchDraw(c);
            if (System.currentTimeMillis() - this.upTime < 400) {
                invalidate();
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.30 */
    class AnonymousClass30 implements WallEdit.Callback {
        private final /* synthetic */ NewsEntry val$e;
        private final /* synthetic */ String val$newComment;

        AnonymousClass30(NewsEntry newsEntry, String str) {
            this.val$e = newsEntry;
            this.val$newComment = str;
        }

        public void success() {
            Toast.makeText(NewsView.this.getContext(), C0436R.string.post_edit_saved, NewsView.MODE_DEFAULT).show();
            this.val$e.retweetText = Global.replaceMentions(this.val$newComment);
            if (this.val$e.ownerID == Global.uid) {
                UserWallCache.remove(this.val$e.postID, NewsView.this.getContext());
                UserWallCache.add(this.val$e, NewsView.this.getContext());
            }
            if (this.val$e.ownerID == this.val$e.userID) {
                NewsfeedCache.remove(this.val$e.ownerID, this.val$e.postID, NewsView.this.getContext());
                NewsfeedCache.add(this.val$e, NewsView.this.getContext());
            }
            Intent intent = new Intent(Posts.ACTION_POST_REPLACED_BROADCAST);
            intent.putExtra("entry", this.val$e);
            NewsView.this.getContext().sendBroadcast(intent);
            NewsView.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(NewsView.this.getContext(), C0436R.string.error, NewsView.MODE_DEFAULT).show();
            NewsView.this.editRepostComment(this.val$e, this.val$newComment);
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.31 */
    class AnonymousClass31 implements NewsfeedAddBan.Callback {
        private final /* synthetic */ NewsEntry val$de;

        AnonymousClass31(NewsEntry newsEntry) {
            this.val$de = newsEntry;
        }

        public void success() {
            ArrayList<NewsEntry> toRemove = new ArrayList();
            Iterator it = NewsView.this.news.iterator();
            while (it.hasNext()) {
                NewsEntry e = (NewsEntry) it.next();
                if (e.userID == this.val$de.ownerID) {
                    toRemove.add(e);
                }
            }
            it = toRemove.iterator();
            while (it.hasNext()) {
                NewsView.this.news.remove((NewsEntry) it.next());
            }
            Iterator<PostDisplayItem> itr = NewsView.this.items.iterator();
            while (itr.hasNext()) {
                if (((PostDisplayItem) itr.next()).postOwnerID == this.val$de.ownerID) {
                    itr.remove();
                }
            }
            Toast.makeText(NewsView.this.getContext(), this.val$de.ownerID > 0 ? C0436R.string.news_banned_user : C0436R.string.news_banned_group, NewsView.MODE_PHOTOS).show();
            NewsView.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(NewsView.this.getContext(), C0436R.string.error, NewsView.MODE_DEFAULT).show();
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.32 */
    class AnonymousClass32 implements WallDelete.Callback {
        private final /* synthetic */ NewsEntry val$e;

        AnonymousClass32(NewsEntry newsEntry) {
            this.val$e = newsEntry;
        }

        public void success() {
            NewsView.this.news.remove(this.val$e);
            NewsfeedCache.remove(this.val$e.ownerID, this.val$e.postID, NewsView.this.getContext());
            if (this.val$e.ownerID == Global.uid) {
                UserWallCache.remove(this.val$e.postID, NewsView.this.getContext());
            }
            Intent intent = new Intent(Posts.ACTION_POST_DELETED_BROADCAST);
            intent.putExtra("owner_id", this.val$e.ownerID);
            intent.putExtra("post_id", this.val$e.postID);
            intent.putExtra("post", this.val$e);
            NewsView.this.getContext().sendBroadcast(intent, permission.ACCESS_DATA);
            NewsView.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(NewsView.this.getContext(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, NewsView.MODE_DEFAULT).show();
        }
    }

    /* renamed from: com.vkontakte.android.NewsView.4 */
    class C13244 implements Listener {
        C13244() {
        }

        public void onScrolledToLastItem() {
            if ((NewsView.this.dataLoading && !NewsView.this.preloading) || !NewsView.this.moreAvailable) {
                return;
            }
            if (NewsView.this.preloading) {
                NewsView.this.preloading = false;
                NewsView.this.preloadOnReady = true;
            } else if (NewsView.this.preloadedNews.size() > 0) {
                NewsView.this.onDataLoaded(NewsView.this.preloadedNews, false);
                NewsView.this.preloadedNews.clear();
                NewsView.this.preloading = true;
                NewsView.this.loadData(false);
            } else {
                NewsView.this.loadData(false);
            }
        }

        public void onScrollStopped() {
        }

        public void onScrollStarted() {
        }
    }

    protected class NewsPhotosAdapter extends ListImageLoaderAdapter {
        int offset;

        /* renamed from: com.vkontakte.android.NewsView.NewsPhotosAdapter.1 */
        class C03591 implements Runnable {
            private final /* synthetic */ int val$_item;
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$image;
            private final /* synthetic */ View val$view;

            C03591(View view, int i, int i2, Bitmap bitmap) {
                this.val$view = view;
                this.val$_item = i;
                this.val$image = i2;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$view != null) {
                    try {
                        ((PostDisplayItem) NewsView.this.items.get(this.val$_item - NewsView.this.getPostsOffset())).setImage(this.val$image, this.val$view, this.val$bitmap, false);
                    } catch (Throwable x) {
                        Log.m532w("vk", x);
                    }
                }
            }
        }

        public NewsPhotosAdapter(int offset) {
            this.offset = offset;
        }

        public int getItemCount() {
            return NewsView.this.items.size();
        }

        public int getImageCountForItem(int item) {
            return ((PostDisplayItem) NewsView.this.items.get(item)).getImageCount();
        }

        public String getImageURL(int item, int image) {
            return ((PostDisplayItem) NewsView.this.items.get(item)).getImageURL(image);
        }

        public void imageLoaded(int _item, int image, Bitmap bitmap) {
            int item = _item + NewsView.this.list.getHeaderViewsCount();
            if (item >= NewsView.this.list.getFirstVisiblePosition() && item <= NewsView.this.list.getLastVisiblePosition()) {
                NewsView.this.post(new C03591(NewsView.this.list.getChildAt(item - NewsView.this.list.getFirstVisiblePosition()), _item, image, bitmap));
            }
        }
    }

    static {
        viewsPrecreated = false;
    }

    public NewsView(Context context) {
        super(context);
        this.news = new Vector();
        this.preloadedNews = new Vector();
        this.newNews = new Vector();
        this.items = new ArrayList();
        this.dataLoading = false;
        this.moreAvailable = false;
        this.lastUpdateTime = MODE_DEFAULT;
        this.commentsFrom = "0";
        this.prependNewEntries = false;
        this.refreshingOnStart = false;
        this.preloading = false;
        this.preloadOnReady = false;
        this.searchQuery = null;
        this.itemLayout = C0436R.layout.news_item;
        this.listID = MODE_DEFAULT;
        this.clearForNew = false;
        init();
    }

    public NewsView(Context context, int layout) {
        super(context);
        this.news = new Vector();
        this.preloadedNews = new Vector();
        this.newNews = new Vector();
        this.items = new ArrayList();
        this.dataLoading = false;
        this.moreAvailable = false;
        this.lastUpdateTime = MODE_DEFAULT;
        this.commentsFrom = "0";
        this.prependNewEntries = false;
        this.refreshingOnStart = false;
        this.preloading = false;
        this.preloadOnReady = false;
        this.searchQuery = null;
        this.itemLayout = C0436R.layout.news_item;
        this.listID = MODE_DEFAULT;
        this.clearForNew = false;
        this.itemLayout = layout;
        init();
    }

    public NewsView(Context context, boolean noinit) {
        super(context);
        this.news = new Vector();
        this.preloadedNews = new Vector();
        this.newNews = new Vector();
        this.items = new ArrayList();
        this.dataLoading = false;
        this.moreAvailable = false;
        this.lastUpdateTime = MODE_DEFAULT;
        this.commentsFrom = "0";
        this.prependNewEntries = false;
        this.refreshingOnStart = false;
        this.preloading = false;
        this.preloadOnReady = false;
        this.searchQuery = null;
        this.itemLayout = C0436R.layout.news_item;
        this.listID = MODE_DEFAULT;
        this.clearForNew = false;
        if (!noinit) {
            init();
        }
    }

    public NewsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.news = new Vector();
        this.preloadedNews = new Vector();
        this.newNews = new Vector();
        this.items = new ArrayList();
        this.dataLoading = false;
        this.moreAvailable = false;
        this.lastUpdateTime = MODE_DEFAULT;
        this.commentsFrom = "0";
        this.prependNewEntries = false;
        this.refreshingOnStart = false;
        this.preloading = false;
        this.preloadOnReady = false;
        this.searchQuery = null;
        this.itemLayout = C0436R.layout.news_item;
        this.listID = MODE_DEFAULT;
        this.clearForNew = false;
        init();
    }

    public NewsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.news = new Vector();
        this.preloadedNews = new Vector();
        this.newNews = new Vector();
        this.items = new ArrayList();
        this.dataLoading = false;
        this.moreAvailable = false;
        this.lastUpdateTime = MODE_DEFAULT;
        this.commentsFrom = "0";
        this.prependNewEntries = false;
        this.refreshingOnStart = false;
        this.preloading = false;
        this.preloadOnReady = false;
        this.searchQuery = null;
        this.itemLayout = C0436R.layout.news_item;
        this.listID = MODE_DEFAULT;
        this.clearForNew = false;
        init();
    }

    protected void init() {
        postDelayed(new C03511(), 1000);
        setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        this.footerView = new FrameLayout(getContext());
        ProgressBar pb = new ProgressBar(getContext());
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(Global.scale(BitmapDescriptorFactory.HUE_ORANGE), Global.scale(BitmapDescriptorFactory.HUE_ORANGE));
        lp.gravity = 17;
        pb.setLayoutParams(lp);
        this.footerView.setPadding(MODE_DEFAULT, Global.scale(7.0f), MODE_DEFAULT, Global.scale(7.0f));
        this.footerView.addView(pb);
        pb.setVisibility(8);
        this.list = new C13232(getContext());
        this.list.addFooterView(this.footerView);
        addHeaderViews();
        if (this.list.getAdapter() == null) {
            this.list.setAdapter(createAdapter());
        }
        this.list.setDivider(null);
        this.list.setDividerHeight(MODE_DEFAULT);
        this.list.setHeaderDividersEnabled(false);
        if (VERSION.SDK_INT <= 10) {
            this.list.setCacheColorHint(getResources().getColor(C0436R.color.cards_bg));
            this.list.setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        }
        this.list.setTopColor(getResources().getColor(C0436R.color.cards_bg));
        this.list.setPadding(MODE_DEFAULT, MODE_DEFAULT, MODE_DEFAULT, MODE_DEFAULT);
        this.list.setVerticalFadingEdgeEnabled(false);
        this.list.setOnRefreshListener(this);
        StateListDrawable selector = new StateListDrawable();
        int[] iArr = new int[MODE_PHOTOS];
        iArr[MODE_DEFAULT] = 16842919;
        Drawable postHighlightDrawable = new PostHighlightDrawable();
        this.highlight = postHighlightDrawable;
        selector.addState(iArr, postHighlightDrawable);
        selector.addState(StateSet.WILD_CARD, new ColorDrawable(MODE_DEFAULT));
        if (VERSION.SDK_INT >= 14) {
            selector.setExitFadeDuration(PhotoView.THUMB_ANIM_DURATION);
        }
        this.list.setSelector(selector);
        this.list.setDrawSelectorOnTop(true);
        this.list.setOnTouchListener(new C03533());
        addView(this.list);
        this.imgLoader = new ListImageLoaderWrapper(createImageLoaderAdapter(), this.list, new C13244());
        this.list.setOnItemClickListener(new C03545());
        this.list.setRecyclerListener(new C03556());
        this.emptyView = EmptyView.create(getContext());
        this.emptyView.setText((int) C0436R.string.no_news_all);
        this.emptyView.setOnBtnClickListener(new C03567());
        this.emptyView.setVisibility(8);
        this.emptyView.setButtonVisible(true);
        this.emptyView.setButtonText((int) C0436R.string.find_friends);
        addView(this.emptyView);
        this.bigProgress = new ProgressBar(getContext());
        FrameLayout.LayoutParams lp2 = new FrameLayout.LayoutParams(-2, -2);
        lp2.gravity = 17;
        this.bigProgress.setLayoutParams(lp2);
        this.bigProgress.setVisibility(MODE_DEFAULT);
        addView(this.bigProgress);
        this.commentsFrom = getContext().getSharedPreferences("news", MODE_DEFAULT).getString("feed_from", "0");
        this.errorView = (ErrorView) inflate(getContext(), C0436R.layout.error, null);
        this.errorView.setOnRetryListener(new C03578());
        this.errorView.setVisibility(8);
        addView(this.errorView);
        this.newPostsBtn = new OverlayTextView(getContext());
        this.newPostsBtn.setText("qweqwe");
        this.newPostsBtn.setTextColor(-12607520);
        this.newPostsBtn.setTextSize(16.0f);
        this.newPostsBtn.setBackgroundResource(C0436R.drawable.bg_panel_new_posts);
        this.newPostsBtn.setGravity(17);
        this.newPostsBtn.setVisibility(MODE_SEARCH);
        this.newPostsBtn.setOverlay((int) C0436R.drawable.highlight);
        this.newPostsBtn.setPadOverlay(true);
        this.newPostsBtn.setOnClickListener(new C03589());
        addView(this.newPostsBtn, new FrameLayout.LayoutParams(-1, -2, 48));
    }

    private void onEmptyBtnClick() {
        switch (this.listID) {
            case -3:
                Navigate.to("SuggestionsRecommendationsFragment", new Bundle(), (Activity) getContext());
            case BoardTopicsFragment.ORDER_CREATED_ASC /*-2*/:
            case MODE_DEFAULT /*0*/:
                Navigate.to("SuggestionsFriendsFragment", new Bundle(), (Activity) getContext());
            default:
        }
    }

    public void scrollToTop() {
        this.list.setSelection(MODE_DEFAULT);
    }

    public void onDetachedFromWindow() {
        if (this.currentReq != null) {
            this.currentReq.cancel();
        }
        if (this.newNewsReq != null) {
            this.newNewsReq.cancel();
        }
    }

    protected ListAdapter createAdapter() {
        return new NewsAdapter();
    }

    protected ListImageLoaderAdapter createImageLoaderAdapter() {
        return new NewsPhotosAdapter(this.list.getHeaderViewsCount());
    }

    protected void addHeaderViews() {
    }

    public int getCount() {
        return this.news.size();
    }

    public void initWithSearch() {
        init();
        this.commentsFrom = "0";
        this.mode = MODE_SEARCH;
        this.list.setRefreshEnabled(false);
        this.list.setDraggingEnabled(false);
        FrameLayout.LayoutParams lparams = new FrameLayout.LayoutParams(-1, -2);
        lparams.gravity = 48;
        this.emptyView.setLayoutParams(lparams);
        this.emptyView.setGravity(MODE_PHOTOS);
        this.emptyView.setText((int) C0436R.string.news_search_explain);
        this.emptyView.setVisibility(MODE_DEFAULT);
        this.emptyView.setPadding(MODE_DEFAULT, Global.scale(100.0f), MODE_DEFAULT, MODE_DEFAULT);
        this.emptyView.setButtonVisible(false);
        this.bigProgress.setVisibility(8);
    }

    public void initFave() {
        this.mode = MODE_FAVE;
        init();
        this.commentsFrom = "0";
        this.list.setRefreshEnabled(false);
        this.list.setDraggingEnabled(false);
        this.emptyView.setText((int) C0436R.string.no_posts);
        this.emptyView.setButtonVisible(false);
    }

    public void initPhotos() {
        this.mode = MODE_PHOTOS;
        init();
        this.commentsFrom = "0";
        this.emptyView.setText((int) C0436R.string.no_news_photos);
    }

    public void initSuggests(int uid) {
        this.listID = uid;
        this.mode = MODE_SUGGESTED;
        init();
        this.emptyView.setText((int) C0436R.string.no_suggested_posts);
        this.emptyView.setButtonVisible(false);
    }

    public void initPostponed(int uid) {
        this.listID = uid;
        this.mode = MODE_POSTPONED;
        init();
        this.emptyView.setText((int) C0436R.string.no_postponed_posts);
        this.emptyView.setButtonVisible(false);
    }

    public void prepend(NewsEntry e) {
        int itemPos = MODE_DEFAULT;
        int entryPos = MODE_DEFAULT;
        if (this.news.size() > 0 && ((NewsEntry) this.news.get(MODE_DEFAULT)).flag(GLRenderBuffer.EGL_SURFACE_SIZE)) {
            entryPos = MODE_DEFAULT + MODE_PHOTOS;
            Iterator it = this.items.iterator();
            while (it.hasNext() && ((PostDisplayItem) it.next()).postID == ((NewsEntry) this.news.get(MODE_DEFAULT)).postID) {
                itemPos += MODE_PHOTOS;
            }
        }
        this.news.add(entryPos, e);
        this.items.addAll(itemPos, buildItems(e));
        this.emptyView.setVisibility(8);
        updateList();
    }

    public void remove(int oid, int pid) {
        Iterator<PostDisplayItem> itr = this.items.iterator();
        while (itr.hasNext()) {
            PostDisplayItem item = (PostDisplayItem) itr.next();
            if (item.postID == pid && item.postOwnerID == oid) {
                itr.remove();
            }
        }
        Iterator it = this.news.iterator();
        while (it.hasNext()) {
            NewsEntry e = (NewsEntry) it.next();
            if ((oid == 0 || e.ownerID == oid) && e.postID == pid) {
                this.news.remove(e);
                break;
            }
        }
        updateList();
        if (this.news.size() == 0) {
            this.emptyView.setVisibility(MODE_DEFAULT);
        }
    }

    public void update(int oid, int pid, int likes, int comments, int retweets, boolean liked, boolean retweeted) {
        Iterator it = this.news.iterator();
        while (it.hasNext()) {
            NewsEntry e = (NewsEntry) it.next();
            if ((oid == 0 || e.ownerID == oid) && e.postID == pid) {
                e.numLikes = likes;
                e.numComments = comments;
                e.numRetweets = retweets;
                e.flag(8, liked);
                e.flag(MODE_SEARCH, retweeted);
                updateList();
                return;
            }
        }
    }

    public void replace(NewsEntry ne) {
        int i = MODE_DEFAULT;
        int insertAt = -1;
        Iterator<PostDisplayItem> itr = this.items.iterator();
        while (itr.hasNext()) {
            PostDisplayItem item = (PostDisplayItem) itr.next();
            if (item.postID == ne.postID && item.postOwnerID == ne.ownerID) {
                if (insertAt == -1) {
                    insertAt = i;
                }
                itr.remove();
            }
            i += MODE_PHOTOS;
        }
        this.items.addAll(Math.max(insertAt, MODE_DEFAULT), buildItems(ne));
        i = MODE_DEFAULT;
        Iterator it = this.news.iterator();
        while (it.hasNext()) {
            NewsEntry e = (NewsEntry) it.next();
            if ((ne.ownerID == 0 || e.ownerID == ne.ownerID) && e.postID == ne.postID) {
                this.news.set(i, ne);
                break;
            }
            i += MODE_PHOTOS;
        }
        updateList();
    }

    public void reloadFromCache() {
        this.news.clear();
        this.items.clear();
        onDataLoaded(NewsfeedCache.get(getContext()), false);
    }

    public void setSearchQuery(String q) {
        this.searchQuery = q;
        if (ACRAConstants.DEFAULT_STRING_VALUE.equals(q)) {
            this.bigProgress.setVisibility(8);
            return;
        }
        this.emptyView.setVisibility(8);
        this.bigProgress.setVisibility(MODE_DEFAULT);
    }

    private void updateNewPostsBtn() {
        boolean curState;
        boolean newState;
        if (this.newPostsBtn.getVisibility() == 0) {
            curState = true;
        } else {
            curState = false;
        }
        if (this.newNews.size() > 0) {
            newState = true;
        } else {
            newState = false;
        }
        if (this.newNews.size() > 0) {
            if (this.clearForNew) {
                this.newPostsBtn.setText(C0436R.string.new_posts);
            } else {
                this.newPostsBtn.setText(Global.langPlural(C0436R.array.new_posts, this.newNews.size(), getResources()));
            }
        }
        if (curState != newState) {
            TranslateAnimation anim;
            TranslateAnimation ba;
            if (newState) {
                anim = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) Global.scale(39.0f));
                anim.setDuration(300);
                anim.setInterpolator(new DecelerateInterpolator());
                anim.setFillAfter(true);
                anim.setAnimationListener(new AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                        NewsView.this.list.clearAnimation();
                        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(-1, -1, 80);
                        lp.topMargin = Global.scale(39.0f);
                        NewsView.this.list.setLayoutParams(lp);
                    }
                });
                this.list.startAnimation(anim);
                this.newPostsBtn.setVisibility(MODE_DEFAULT);
                ba = new TranslateAnimation(0.0f, 0.0f, (float) Global.scale(-39.0f), 0.0f);
                ba.setDuration(300);
                ba.setInterpolator(new DecelerateInterpolator());
                this.newPostsBtn.startAnimation(ba);
                return;
            }
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(-1, -1);
            lp.topMargin = MODE_DEFAULT;
            this.list.setLayoutParams(lp);
            anim = new TranslateAnimation(0.0f, 0.0f, (float) Global.scale(39.0f), 0.0f);
            anim.setDuration(300);
            anim.setInterpolator(new DecelerateInterpolator());
            this.list.startAnimation(anim);
            ba = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) Global.scale(-39.0f));
            ba.setDuration(300);
            ba.setInterpolator(new DecelerateInterpolator());
            ba.setAnimationListener(new AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    NewsView.this.newPostsBtn.clearAnimation();
                    NewsView.this.newPostsBtn.setVisibility(NewsView.MODE_SEARCH);
                }
            });
            this.newPostsBtn.startAnimation(ba);
        }
    }

    private void showNew() {
        ArrayList<PostDisplayItem> nitems;
        Iterator it;
        if (this.clearForNew) {
            this.news.clear();
            this.preloadedNews.clear();
            this.items.clear();
            this.news.addAll(this.newNews.subList(MODE_DEFAULT, Math.min(10, this.newNews.size())));
            nitems = new ArrayList();
            it = this.news.iterator();
            while (it.hasNext()) {
                nitems.addAll(buildItems((NewsEntry) it.next()));
            }
            this.items.addAll(MODE_DEFAULT, nitems);
            if (this.newNews.size() > 10) {
                this.preloadedNews.addAll(this.newNews.subList(10, this.newNews.size()));
            }
            updateList();
            this.list.postDelayed(new Runnable() {
                public void run() {
                    NewsView.this.list.setSelectionFromTop(NewsView.MODE_DEFAULT, NewsView.MODE_DEFAULT);
                    NewsView.this.imgLoader.updateImages();
                }
            }, 200);
            getContext().getSharedPreferences("news", MODE_DEFAULT).edit().putString("feed_from", this.newFrom).putInt("feed_offset", this.newOffset).commit();
            this.commentsFrom = this.newFrom;
            ArrayList<NewsEntry> forCache = new ArrayList();
            forCache.addAll(this.newNews);
            this.newNews.clear();
            new Thread(new AnonymousClass13(forCache)).start();
            updateNewPostsBtn();
            return;
        }
        this.news.addAll(MODE_DEFAULT, this.newNews);
        nitems = new ArrayList();
        it = this.newNews.iterator();
        while (it.hasNext()) {
            nitems.addAll(buildItems((NewsEntry) it.next()));
        }
        this.items.addAll(MODE_DEFAULT, nitems);
        updateList();
        this.list.post(new Runnable() {
            public void run() {
                NewsView.this.list.setSelectionFromTop(NewsView.MODE_DEFAULT, NewsView.MODE_DEFAULT);
                NewsView.this.imgLoader.updateImages();
            }
        });
        forCache = new ArrayList();
        forCache.addAll(this.newNews);
        this.newNews.clear();
        new Thread(new AnonymousClass15(forCache)).start();
        updateNewPostsBtn();
    }

    public void preloadNew() {
        if (this.errorView.getVisibility() != 0) {
            postDelayed(new Runnable() {

                /* renamed from: com.vkontakte.android.NewsView.16.1 */
                class C13211 implements Callback {
                    C13211() {
                    }

                    public void success(ArrayList<NewsEntry> rnews, String rnewFrom) {
                        NewsView.this.newNewsReq = null;
                        NewsView.this.newFrom = rnewFrom;
                        NewsView.this.newNews.clear();
                        int numNew = NewsView.MODE_DEFAULT;
                        boolean found = false;
                        Iterator it = rnews.iterator();
                        while (it.hasNext()) {
                            NewsEntry e = (NewsEntry) it.next();
                            Iterator it2 = NewsView.this.news.iterator();
                            while (it2.hasNext()) {
                                NewsEntry ne = (NewsEntry) it2.next();
                                if (e.type == ne.type && e.postID == ne.postID && e.ownerID == ne.ownerID) {
                                    Log.m528i("vk", "Found intersection, numNew=" + numNew);
                                    found = true;
                                    break;
                                }
                            }
                            if (found) {
                                break;
                            }
                            numNew += NewsView.MODE_PHOTOS;
                            NewsView.this.newNews.add(e);
                        }
                        if (!found) {
                            Log.m528i("vk", "Not found intersection :(");
                        }
                        NewsView.this.clearForNew = !found;
                        NewsView.this.updateNewPostsBtn();
                    }

                    public void fail(int ecode, String emsg) {
                        NewsView.this.newNewsReq = null;
                    }
                }

                public void run() {
                    NewsView.this.newNewsReq = new NewsfeedGet(ACRAConstants.DEFAULT_STRING_VALUE, 20, false, NewsView.this.listID).setCallback(new C13211()).exec(NewsView.this);
                }
            }, 300);
        }
    }

    public void beforeDestroy() {
        int i = MODE_DEFAULT;
        if (this.mode == 0) {
            Posts.feed.clear();
            Posts.feed.addAll(this.news);
            Posts.preloadedFeed.clear();
            Posts.preloadedFeed.addAll(this.preloadedNews);
            Posts.feedItem = this.list.getFirstVisiblePosition();
            if (this.list.getChildCount() > 0) {
                i = this.list.getChildAt(MODE_DEFAULT).getTop();
            }
            Posts.feedItemOffset = i;
            Posts.feedFrom = this.commentsFrom;
            Log.m528i("vk", "before destroy");
        }
    }

    public void loadData(boolean refresh) {
        int count = 10;
        if (!this.dataLoading) {
            Log.m525d("vk", "Load data!");
            this.dataLoading = true;
            this.footerView.getChildAt(MODE_DEFAULT).setVisibility(this.moreAvailable ? MODE_DEFAULT : 8);
            if (this.mode == MODE_FAVE) {
                if (refresh) {
                    this.offset = MODE_DEFAULT;
                }
                int i = this.offset;
                if (!this.preloading) {
                    count = 20;
                }
                this.currentReq = new FaveGetPosts(i, count).setCallback(new AnonymousClass17(count, refresh)).exec((View) this);
            } else if (this.mode == MODE_SEARCH && this.searchQuery != null) {
                if (refresh) {
                    this.commentsFrom = "0";
                    this.offset = MODE_DEFAULT;
                }
                this.list.setRefreshEnabled(true);
                this.list.setDraggingEnabled(true);
                String str = this.searchQuery;
                String str2 = this.commentsFrom;
                if (!this.preloading) {
                    count = 20;
                }
                this.currentReq = new NewsfeedSearch(str, str2, count).setCallback(new AnonymousClass18(refresh)).exec((View) this);
            } else if (this.mode == 0 || this.mode == MODE_PHOTOS) {
                if (refresh && this.newNewsReq != null) {
                    this.newNewsReq.cancel();
                    this.newNewsReq = null;
                }
                APIController.runInApi(new AnonymousClass19(refresh));
            } else if (this.mode == MODE_COMMENTS) {
                if (refresh) {
                    this.commentsFrom = "0";
                }
                if (this.lastUpdateTime > 0 || !NewsfeedCommentsCache.hasEntries(getContext()) || ((long) this.lastUpdateTime) < (System.currentTimeMillis() / 1000) - 7200) {
                    this.currentReq = new NewsfeedGetComments(this.commentsFrom, this.offset).setCallback(new AnonymousClass20(refresh)).exec((View) this);
                } else {
                    new Thread(new AnonymousClass21(refresh)).start();
                }
            } else if (this.mode == MODE_POSTPONED || this.mode == MODE_SUGGESTED) {
                if (refresh) {
                    this.offset = MODE_DEFAULT;
                }
                int i2 = this.listID;
                int i3 = this.offset;
                if (!this.preloading) {
                    count = 20;
                }
                this.currentReq = new WallGet(i2, i3, count, this.mode == MODE_POSTPONED ? "postponed" : "suggests").setCallback(new AnonymousClass22(refresh)).exec((View) this);
            }
        }
    }

    public boolean setList(int id) {
        if (id == this.listID) {
            return false;
        }
        if (id != -9000) {
            this.listID = id;
        }
        this.bigProgress.setVisibility(MODE_DEFAULT);
        this.news.clear();
        this.preloadedNews.clear();
        this.items.clear();
        updateList();
        this.moreAvailable = false;
        this.footerView.getChildAt(MODE_DEFAULT).setVisibility(8);
        this.emptyView.setVisibility(8);
        this.newNews.clear();
        if (this.newNewsReq != null) {
            this.newNewsReq.cancel();
            this.newNewsReq = null;
        }
        if (this.currentReq != null) {
            this.currentReq.cancel();
            this.currentReq = null;
            this.dataLoading = false;
            this.list.refreshDone();
        }
        updateNewPostsBtn();
        switch (this.listID) {
            case -3:
                this.emptyView.setText((int) C0436R.string.no_news_groups);
                this.emptyView.setButtonText((int) C0436R.string.empty_find_groups);
                this.emptyView.setButtonVisible(true);
                break;
            case BoardTopicsFragment.ORDER_CREATED_ASC /*-2*/:
                this.emptyView.setText((int) C0436R.string.no_news_friends);
                this.emptyView.setButtonText((int) C0436R.string.find_friends);
                this.emptyView.setButtonVisible(true);
                break;
            case BoardTopicsFragment.ORDER_UPDATED_ASC /*-1*/:
                this.emptyView.setText((int) C0436R.string.no_news_recommendations);
                this.emptyView.setButtonVisible(false);
                break;
            case MODE_DEFAULT /*0*/:
                this.emptyView.setText((int) C0436R.string.no_news_all);
                this.emptyView.setButtonText((int) C0436R.string.find_friends);
                this.emptyView.setButtonVisible(true);
                break;
            default:
                this.emptyView.setText((int) C0436R.string.no_news_list);
                this.emptyView.setButtonVisible(false);
                break;
        }
        return true;
    }

    public int getList() {
        return this.listID;
    }

    private boolean containsEntry(NewsEntry e) {
        Iterator it = this.news.iterator();
        while (it.hasNext()) {
            if (((NewsEntry) it.next()).equals(e)) {
                return true;
            }
        }
        return false;
    }

    protected void onDataLoaded(List<NewsEntry> news, boolean refresh) {
        boolean z = true;
        if (news != null) {
            int i;
            int firstVisible = this.list.getFirstVisiblePosition();
            int numAdded = MODE_DEFAULT;
            if (this.refreshingOnStart && !this.prependNewEntries) {
                boolean z2;
                if (firstVisible > 0) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                this.prependNewEntries = z2;
            }
            if (this.prependNewEntries) {
                for (int i2 = news.size() - 1; i2 >= 0; i2--) {
                    if (!containsEntry((NewsEntry) news.get(i2))) {
                        this.news.add(MODE_DEFAULT, (NewsEntry) news.get(i2));
                        this.items.addAll(MODE_DEFAULT, buildItems((NewsEntry) news.get(i2)));
                        numAdded += MODE_PHOTOS;
                    }
                }
            } else {
                if (refresh) {
                    this.news.clear();
                    this.items.clear();
                }
                for (NewsEntry e : news) {
                    this.news.add(e);
                    this.items.addAll(buildItems(e));
                }
            }
            this.list.setDraggingEnabled(true);
            int itemOffset = -1;
            if (this.refreshingOnStart) {
                if (firstVisible == 0) {
                    if (this.list.getChildCount() > MODE_FAVE) {
                        itemOffset = this.list.getChildAt(MODE_FAVE).getTop();
                    }
                } else if (this.list.getChildCount() > 0) {
                    itemOffset = this.list.getChildAt(MODE_DEFAULT).getTop();
                }
            }
            updateList();
            boolean animate = true;
            if (refresh && this.prependNewEntries) {
                this.list.setSelectionFromTop(firstVisible + numAdded, itemOffset);
                if (!this.prependNewEntries || (firstVisible == 0 && numAdded == 0)) {
                    animate = true;
                } else {
                    animate = false;
                }
                this.prependNewEntries = false;
            }
            this.bigProgress.clearAnimation();
            this.emptyView.clearAnimation();
            this.bigProgress.setVisibility(8);
            if (news.size() <= 0) {
                z = false;
            }
            this.moreAvailable = z;
            View childAt = this.footerView.getChildAt(MODE_DEFAULT);
            if (this.moreAvailable) {
                i = MODE_DEFAULT;
            } else {
                i = 8;
            }
            childAt.setVisibility(i);
            this.dataLoading = false;
            if (refresh) {
                this.list.refreshDone(animate);
            }
            if (refresh || this.lastUpdateTime == 0) {
                this.lastUpdateTime = (int) (System.currentTimeMillis() / 1000);
            }
            EmptyView emptyView = this.emptyView;
            if (this.news.size() > 0) {
                i = 8;
            } else {
                i = MODE_DEFAULT;
            }
            emptyView.setVisibility(i);
            this.prependNewEntries = false;
            this.refreshingOnStart = false;
            this.errorView.setVisibility(8);
        }
    }

    protected void onError(int code, String text) {
        if (this.news.size() == 0 && this.bigProgress.getVisibility() == 0) {
            Global.showViewAnimated(this.bigProgress, false, PhotoView.THUMB_ANIM_DURATION);
            Global.showViewAnimated(this.errorView, true, PhotoView.THUMB_ANIM_DURATION);
            this.list.setDraggingEnabled(false);
            return;
        }
        Toast.makeText(getContext(), C0436R.string.err_text, MODE_DEFAULT).show();
    }

    protected void setEmptyLabel(int resID) {
        this.emptyView.setText(resID);
    }

    protected void setEmptyPadding(int p) {
        this.emptyView.setPadding(Global.scale(7.0f), p, Global.scale(7.0f), Global.scale(7.0f));
    }

    public void setCommentsMode(boolean comments) {
        this.mode = MODE_COMMENTS;
        this.commentsFrom = getContext().getSharedPreferences("news", MODE_DEFAULT).getString(comments ? "comments_from" : "feed_from", "0");
        this.offset = getContext().getSharedPreferences("news", MODE_DEFAULT).getInt(comments ? "comments_offset" : "feed_offset", MODE_DEFAULT);
        this.lastUpdateTime = MODE_DEFAULT;
        this.itemLayout = C0436R.layout.news_item_feedback;
        this.emptyView.setText((int) C0436R.string.no_news_comments);
        this.emptyView.setButtonVisible(false);
    }

    public void updateList() {
        ((Activity) getContext()).runOnUiThread(new Runnable() {
            public void run() {
                ((BaseAdapter) ((HeaderViewListAdapter) NewsView.this.list.getAdapter()).getWrappedAdapter()).notifyDataSetChanged();
                NewsView.this.imgLoader.updateImages();
                NewsView.this.emptyView.clearAnimation();
                if (NewsView.this.mode == NewsView.MODE_SEARCH && NewsView.this.searchQuery != null && NewsView.this.searchQuery.length() > 0) {
                    NewsView.this.emptyView.setText((int) C0436R.string.nothing_found);
                }
                NewsView.this.emptyView.setVisibility(NewsView.this.news.size() > 0 ? 8 : NewsView.MODE_DEFAULT);
            }
        });
    }

    public void updateList2() {
        post(new Runnable() {
            public void run() {
                ((BaseAdapter) ((HeaderViewListAdapter) NewsView.this.list.getAdapter()).getWrappedAdapter()).notifyDataSetChanged();
            }
        });
    }

    private void showItemOptions(View view, NewsEntry e) {
        ArrayList<String> items = new ArrayList();
        ArrayList<String> acts = new ArrayList();
        if (canHideFromFeed() && e.ownerID != Global.uid) {
            items.add(getResources().getString(C0436R.string.hide_from_newsfeed));
            acts.add("hide");
        }
        if (e.flag(32) && e.retweetOrigTime > 0) {
            items.add(getResources().getString(C0436R.string.show_original_post));
            acts.add("orig");
        }
        if (e.flag(TransportMediator.FLAG_KEY_MEDIA_NEXT) && (((long) (e.time + 86400)) > System.currentTimeMillis() / 1000 || e.flag(NewsEntry.FLAG_SUGGESTED))) {
            items.add(getResources().getString(C0436R.string.edit));
            acts.add("edit");
        }
        if (e.flag(NewsEntry.FLAG_POSTPONED)) {
            items.add(getResources().getString(C0436R.string.publish_now));
            acts.add("publish");
        }
        if (!(e.type == MODE_SUGGESTED || e.type == 7)) {
            items.add(getResources().getString(C0436R.string.copy_link));
            acts.add("link");
            if (e.flag(64) || e.ownerID == Global.uid || e.userID == Global.uid) {
                items.add(getResources().getString(C0436R.string.delete));
                acts.add("del");
            }
        }
        if (!(e.userID == Global.uid || ((e.type != 0 && e.type != MODE_PHOTOS && e.type != MODE_FAVE) || e.flag(NewsEntry.FLAG_SUGGESTED) || e.flag(NewsEntry.FLAG_POSTPONED)))) {
            items.add(getResources().getString(C0436R.string.report_content));
            acts.add("report");
        }
        if (this.mode == MODE_COMMENTS) {
            items.add(getResources().getString(C0436R.string.comments_unsubscribe));
            acts.add("unsubscribe");
        }
        if (VERSION.SDK_INT >= 11) {
            PopupMenu pm = new PopupMenu(getContext(), view);
            for (int i = MODE_DEFAULT; i < items.size(); i += MODE_PHOTOS) {
                pm.getMenu().add(MODE_DEFAULT, i, MODE_DEFAULT, (CharSequence) items.get(i));
            }
            pm.setOnMenuItemClickListener(new AnonymousClass25(acts, e));
            pm.show();
            return;
        }
        new Builder(getContext()).setItems((CharSequence[]) items.toArray(new String[MODE_DEFAULT]), new AnonymousClass26(acts, e)).show();
    }

    private void performPostAction(String act, NewsEntry e) {
        String type;
        Intent intent;
        if ("hide".equals(act)) {
            hideSource(e);
        }
        if ("link".equals(act)) {
            String ln = "http://vk.com/";
            if (e.type == 0) {
                ln = new StringBuilder(String.valueOf(ln)).append("wall").toString();
            } else if (e.type == MODE_FAVE) {
                ln = new StringBuilder(String.valueOf(ln)).append("video").toString();
            } else if (e.type == MODE_PHOTOS) {
                ln = new StringBuilder(String.valueOf(ln)).append("photo").toString();
            } else if (e.type == MODE_SEARCH) {
                ln = new StringBuilder(String.valueOf(ln)).append("topic").toString();
            }
            ((ClipboardManager) getContext().getSystemService("clipboard")).setText(new StringBuilder(String.valueOf(ln)).append(e.ownerID).append("_").append(e.postID).toString());
            Toast.makeText(getContext(), C0436R.string.link_copied, MODE_DEFAULT).show();
        }
        if ("orig".equals(act)) {
            type = "wall";
            if (e.retweetType == MODE_PHOTOS) {
                type = "photo";
            }
            if (e.retweetType == MODE_FAVE) {
                type = "video";
            }
            getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("vkontakte://vk.com/" + type + e.retweetUID + "_" + e.retweetOrigId)));
        }
        if ("edit".equals(act)) {
            if (e.flag(32)) {
                editRepostComment(e, Html.fromHtml(e.retweetText).toString());
                return;
            }
            intent = new Intent(getContext(), NewPostActivity.class);
            intent.putExtra("edit", e);
            if (e.flag(NewsEntry.FLAG_POSTPONED) && this.listID < 0) {
                intent.putExtra("public", true);
            }
            getContext().startActivity(intent);
        }
        if ("del".equals(act)) {
            new Builder(getContext()).setTitle(C0436R.string.confirm).setMessage(C0436R.string.delete_confirm).setPositiveButton(C0436R.string.yes, new AnonymousClass27(e)).setNegativeButton(C0436R.string.no, null).show();
        }
        if ("report".equals(act)) {
            intent = new Intent(getContext(), ReportContentActivity.class);
            intent.putExtra("itemID", e.postID);
            intent.putExtra("ownerID", e.ownerID);
            type = "post";
            if (e.type == MODE_PHOTOS) {
                type = "photo";
            }
            if (e.type == MODE_FAVE) {
                type = "video";
            }
            intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, type);
            getContext().startActivity(intent);
        }
        if ("publish".equals(act)) {
            Posts.publishPostponed(e, (Activity) getContext(), null);
        }
        if ("unsubscribe".equals(act)) {
            new NewsfeedUnsubscribe(e.ownerID, e.postID, e.type).setCallback(new AnonymousClass28(e)).wrapProgress(getContext()).exec((View) this);
        }
    }

    private void editRepostComment(NewsEntry e, String text) {
        EditText edit = new EditText(getContext());
        edit.setText(text);
        edit.setLines(MODE_SEARCH);
        edit.setGravity(48);
        new Builder(getContext()).setTitle(C0436R.string.add_comment_hint).setView(edit).setPositiveButton(C0436R.string.save, new AnonymousClass29(e, edit)).setNegativeButton(C0436R.string.cancel, null).show();
    }

    private void saveRepostComment(NewsEntry e, String newComment) {
        new WallEdit(e.postID, e.ownerID, newComment).setCallback(new AnonymousClass30(e, newComment)).wrapProgress(getContext()).exec((Activity) getContext());
    }

    private void hideSource(NewsEntry de) {
        new NewsfeedAddBan(de.ownerID).setCallback(new AnonymousClass31(de)).wrapProgress(getContext()).exec((View) this);
    }

    private void deletePost(NewsEntry e) {
        try {
            new WallDelete(e.ownerID, e.postID, e.type).setCallback(new AnonymousClass32(e)).wrapProgress(getContext()).exec((View) this);
        } catch (Exception e2) {
        }
    }

    protected boolean canHideFromFeed() {
        return this.mode == 0 && (this.listID == 0 || this.listID == -2 || this.listID == -3);
    }

    public void invalidateList() {
        post(new Runnable() {
            public void run() {
                ((BaseAdapter) ((HeaderViewListAdapter) NewsView.this.list.getAdapter()).getWrappedAdapter()).notifyDataSetInvalidated();
                NewsView.this.imgLoader.updateImages();
            }
        });
    }

    protected int getPostsOffset() {
        return MODE_DEFAULT;
    }

    public void onRefresh() {
        this.newNews.clear();
        updateNewPostsBtn();
        loadData(true);
    }

    public String getLastUpdatedTime() {
        if (this.lastUpdateTime > 0) {
            return new StringBuilder(String.valueOf(getResources().getString(C0436R.string.updated))).append(" ").append(Global.langDateRelativeNoDiff(this.lastUpdateTime, getResources())).toString();
        }
        return getResources().getString(C0436R.string.not_updated);
    }

    public void refresh(boolean prepend) {
        this.prependNewEntries = prepend;
        this.refreshingOnStart = true;
        this.list.refresh();
    }

    public void onPause() {
        this.imgLoader.deactivate();
    }

    public void onResume() {
        this.imgLoader.activate();
    }

    protected List<PostDisplayItem> buildAttachItems(List<Attachment> attachments, int postID, int ownerID, NewsEntry e) {
        Iterator it;
        ArrayList<PostDisplayItem> result = new ArrayList();
        ArrayList<ThumbAttachment> tAtts = new ArrayList();
        ArrayList<AudioAttachment> audios = new ArrayList();
        ArrayList<Attachment> other = new ArrayList();
        ArrayList<Attachment> common = new ArrayList();
        SignatureLinkAttachment sig = null;
        RepostAttachment repost = null;
        for (Attachment att : attachments) {
            if (att != null) {
                if (att instanceof ThumbAttachment) {
                    tAtts.add((ThumbAttachment) att);
                } else if (att instanceof AudioAttachment) {
                    audios.add((AudioAttachment) att);
                } else if (att instanceof SignatureLinkAttachment) {
                    sig = (SignatureLinkAttachment) att;
                } else if (att instanceof RepostAttachment) {
                    repost = (RepostAttachment) att;
                } else if ((!(att instanceof DocumentAttachment) || ((ImageAttachment) att).getImageURL() == null) && (!(att instanceof GeoAttachment) || ((GeoAttachment) att).id > 0)) {
                    common.add(att);
                } else {
                    other.add(att);
                }
            }
        }
        if (tAtts.size() > 0) {
            result.add(new ThumbsBlockPostDisplayItem(e, tAtts, false));
        }
        if (audios.size() > 0) {
            int i;
            AudioFile[] pl = new AudioFile[audios.size()];
            for (i = MODE_DEFAULT; i < audios.size(); i += MODE_PHOTOS) {
                pl[i] = new AudioFile((AudioAttachment) audios.get(i));
            }
            i = MODE_DEFAULT;
            it = audios.iterator();
            while (it.hasNext()) {
                AudioAttachment aa = (AudioAttachment) it.next();
                aa.playlist = pl;
                aa.playlistPos = i;
                result.add(new AudioPostDisplayItem(postID, ownerID, aa));
                i += MODE_PHOTOS;
            }
        }
        if (other.size() > 0) {
            result.add(new AttachContainerPostDisplayItem(postID, ownerID, other));
        }
        if (common.size() > 0) {
            it = common.iterator();
            while (it.hasNext()) {
                result.add(new CommonAttachmentPostDisplayItem(postID, ownerID, (Attachment) it.next()));
            }
        }
        if (sig != null) {
            result.add(new SignaturePostDisplayItem(postID, ownerID, sig));
        }
        if (repost != null) {
            ArrayList<PostDisplayItem> arrayList = result;
            arrayList.add(new RepostPostDisplayItem(postID, ownerID, repost.name, repost.photo, repost.ownerID, repost.time, repost.postID, repost.type));
        }
        return result;
    }

    protected List<PostDisplayItem> buildItems(NewsEntry e) {
        ArrayList<PostDisplayItem> result = new ArrayList();
        HeaderPostDisplayItem hdr = new HeaderPostDisplayItem(e, this.mode == MODE_PHOTOS);
        hdr.menuClickListener = new AnonymousClass34(e);
        boolean z = !(e.type == MODE_SUGGESTED || e.type == 7 || e.type == 9) || (canHideFromFeed() && e.ownerID != Global.uid);
        hdr.showMenu = z;
        result.add(hdr);
        if (e.flag(32)) {
            if (e.retweetText != null && e.retweetText.length() > 0) {
                result.add(new TextPostDisplayItem(e.postID, e.ownerID, e.displayableRetweetText, false));
            }
            if (e.repostAttachments.size() > 0) {
                result.addAll(buildAttachItems(e.repostAttachments, e.postID, e.ownerID, e));
            }
            result.add(new RepostPostDisplayItem(e.postID, e.ownerID, e.retweetUserName, e.retweetUserPhoto, e.retweetUID, e.retweetOrigTime, e.retweetOrigId, e.retweetType));
        }
        if (e.text.length() > 0) {
            result.add(new TextPostDisplayItem(e.postID, e.ownerID, e.displayablePreviewText, e.flag(16)));
        }
        result.addAll(buildAttachItems(e.attachments, e.postID, e.ownerID, e));
        if (!(e.type == MODE_SUGGESTED || e.type == 7 || e.type == 9 || e.flag(NewsEntry.FLAG_SUGGESTED) || e.flag(NewsEntry.FLAG_POSTPONED))) {
            boolean z2;
            z = this.mode == MODE_COMMENTS;
            if (this.mode == MODE_COMMENTS && e.type == MODE_SEARCH) {
                z2 = false;
            } else {
                z2 = true;
            }
            result.add(new FooterPostDisplayItem(e, z, z2));
        }
        if (e.flag(NewsEntry.FLAG_SUGGESTED) && Groups.getAdminLevel(-e.ownerID) >= MODE_FAVE) {
            result.add(new ButtonsPostDisplayItem(e));
        }
        if (e.lastComment != null) {
            result.add(new CommentPostDisplayItem(e.postID, e.ownerID, e.lastComment, e.lastCommentUserName, e.lastCommentUserPhoto, e.numComments, e.lastCommentTime));
        }
        ((PostDisplayItem) result.get(MODE_DEFAULT)).bgType = MODE_PHOTOS;
        ((PostDisplayItem) result.get(result.size() - 1)).bgType = MODE_FAVE;
        if (e.type == 7 || e.type == MODE_SUGGESTED || e.type == 9) {
            Iterator it = result.iterator();
            while (it.hasNext()) {
                ((PostDisplayItem) it.next()).clickable = false;
            }
        }
        return result;
    }

    public void onScrolled(float offset) {
    }
}
