package com.vkontakte.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import com.vkontakte.android.PhotoAttachment.FixedSizeImageView;
import com.vkontakte.android.api.WallLike;
import com.vkontakte.android.api.WallLike.Callback;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.fragments.PhotoViewerFragment;
import com.vkontakte.android.mediapicker.gl.GLFilterContext;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.ui.FlowLayout;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import org.acra.ACRAConstants;

public class NewsItemView extends RelativeLayout implements OnPreDrawListener {
    private ArrayList<AttachmentViewInfo> attViews;
    private FlowLayout attachContainer;
    protected AudioFile[] audioPlaylist;
    NewsEntry f45e;
    private ListImageLoaderWrapper imgLoader;
    boolean imgRemoved;
    private boolean liking;
    private long lst;
    private TextView name;
    int nlikes;
    protected OnClickListener photoClickListener;
    private boolean photosMode;
    private TextView post;
    private TextView postComments;
    private TextView postLikes;
    private TextView postReposts;
    private FlowLayout repostAttachContainer;
    private TextView retweetName;
    private ImageView retweetPhoto;
    private TextView retweetText;
    private TextView retweetTime;
    public boolean showDateAgo;
    String ta;
    private OnInfoUpdateListener updListener;

    /* renamed from: com.vkontakte.android.NewsItemView.1 */
    class C03401 implements OnClickListener {
        C03401() {
        }

        public void onClick(View v) {
            Bundle args = new Bundle();
            args.putParcelable("entry", NewsItemView.this.f45e);
            args.putBoolean("comment", true);
            Navigate.to("PostViewFragment", args, (Activity) NewsItemView.this.getContext());
        }
    }

    /* renamed from: com.vkontakte.android.NewsItemView.2 */
    class C03412 implements OnClickListener {
        C03412() {
        }

        public void onClick(View v) {
            NewsItemView.this.like(!NewsItemView.this.f45e.flag(8));
        }
    }

    /* renamed from: com.vkontakte.android.NewsItemView.3 */
    class C03423 implements OnClickListener {
        C03423() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(NewsItemView.this.getContext(), RepostActivity.class);
            intent.putExtra("post", NewsItemView.this.f45e);
            NewsItemView.this.getContext().startActivity(intent);
        }
    }

    /* renamed from: com.vkontakte.android.NewsItemView.4 */
    class C03434 implements OnClickListener {
        private final /* synthetic */ int val$containerID;
        private final /* synthetic */ NewsEntry val$e;
        private final /* synthetic */ int val$idx;
        private final /* synthetic */ View val$item;
        private final /* synthetic */ ArrayList val$photos;

        C03434(int i, ArrayList arrayList, NewsEntry newsEntry, View view, int i2) {
            this.val$idx = i;
            this.val$photos = arrayList;
            this.val$e = newsEntry;
            this.val$item = view;
            this.val$containerID = i2;
        }

        public void onClick(View v) {
            NewsItemView.openPhotoList(this.val$idx, v, this.val$photos, this.val$e, this.val$item, this.val$containerID);
        }
    }

    private static class AttachmentViewInfo {
        Rect rect;
        View view;

        private AttachmentViewInfo() {
        }
    }

    public interface OnInfoUpdateListener {
        void onInfoUpdate(NewsItemView newsItemView, NewsEntry newsEntry);
    }

    /* renamed from: com.vkontakte.android.NewsItemView.5 */
    class C13205 implements Callback {
        private final /* synthetic */ int val$lOid;
        private final /* synthetic */ int val$lPid;
        private final /* synthetic */ boolean val$liked;

        /* renamed from: com.vkontakte.android.NewsItemView.5.1 */
        class C03441 implements Runnable {
            C03441() {
            }

            public void run() {
                NewsItemView.this.like(NewsItemView.this.f45e.flag(8));
            }
        }

        /* renamed from: com.vkontakte.android.NewsItemView.5.2 */
        class C03452 implements Runnable {
            C03452() {
            }

            public void run() {
                NewsItemView.this.updateInfoLine();
            }
        }

        /* renamed from: com.vkontakte.android.NewsItemView.5.3 */
        class C03463 implements Runnable {
            C03463() {
            }

            public void run() {
                Toast.makeText(NewsItemView.this.getContext(), C0436R.string.error, 0).show();
                NewsItemView.this.updateInfoLine();
            }
        }

        C13205(int i, int i2, boolean z) {
            this.val$lOid = i;
            this.val$lPid = i2;
            this.val$liked = z;
        }

        public void success(int likes, int retweets, int postID) {
            if (NewsItemView.this.f45e != null && this.val$lOid == NewsItemView.this.f45e.ownerID && this.val$lPid == NewsItemView.this.f45e.postID) {
                NewsItemView.this.f45e.numLikes = likes;
                if (this.val$liked) {
                    NewsItemView.this.f45e.numRetweets = retweets;
                }
                NewsItemView.this.liking = false;
                if (NewsItemView.this.f45e.flag(8) != this.val$liked) {
                    NewsItemView.this.post(new C03441());
                } else {
                    NewsItemView.this.post(new C03452());
                }
            }
        }

        public void fail(int ecode, String emsg) {
            if (NewsItemView.this.f45e != null && this.val$lOid == NewsItemView.this.f45e.ownerID && this.val$lPid == NewsItemView.this.f45e.postID) {
                NewsEntry newsEntry;
                if (this.val$liked) {
                    newsEntry = NewsItemView.this.f45e;
                    newsEntry.numLikes--;
                } else {
                    newsEntry = NewsItemView.this.f45e;
                    newsEntry.numLikes++;
                }
                NewsItemView.this.f45e.flag(8, !this.val$liked);
                NewsItemView.this.liking = false;
                NewsItemView.this.post(new C03463());
            }
        }
    }

    public NewsItemView(Context context) {
        super(context);
        this.f45e = null;
        this.showDateAgo = true;
        this.ta = ACRAConstants.DEFAULT_STRING_VALUE;
        this.nlikes = 0;
        this.imgRemoved = true;
        this.photosMode = false;
        this.liking = false;
        this.attViews = new ArrayList();
    }

    public NewsItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.f45e = null;
        this.showDateAgo = true;
        this.ta = ACRAConstants.DEFAULT_STRING_VALUE;
        this.nlikes = 0;
        this.imgRemoved = true;
        this.photosMode = false;
        this.liking = false;
        this.attViews = new ArrayList();
    }

    public void onFinishInflate() {
        this.attachContainer = (FlowLayout) findViewById(C0436R.id.post_attach_container);
        this.repostAttachContainer = (FlowLayout) findViewById(C0436R.id.post_repost_attach_container);
        this.postComments = (TextView) findViewById(C0436R.id.post_comments);
        this.postLikes = (TextView) findViewById(C0436R.id.post_likes);
        this.postReposts = (TextView) findViewById(C0436R.id.post_reposts);
        this.retweetName = (TextView) findViewById(C0436R.id.post_retweet_name);
        this.retweetTime = (TextView) findViewById(C0436R.id.post_retweet_time);
        this.retweetPhoto = (ImageView) findViewById(C0436R.id.post_retweet_photo);
        this.name = (TextView) findViewById(C0436R.id.poster_name_view);
        this.retweetText = (TextView) findViewById(C0436R.id.post_retweet_text);
        this.post = (TextView) findViewById(C0436R.id.post_view);
        int k = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getContext()).getString("fontSize", "0"));
        this.post.setTextSize(1, (((float) k) * ImageViewHolder.PaddingSize) + 16.0f);
        this.retweetText.setTextSize(1, (((float) k) * ImageViewHolder.PaddingSize) + 16.0f);
        if (findViewById(C0436R.id.post_comments) != null) {
            this.postComments.setOnClickListener(new C03401());
            this.postLikes.setOnClickListener(new C03412());
            this.postReposts.setOnClickListener(new C03423());
        }
    }

    public void onAttachedToWindow() {
        ((View) getParent()).getViewTreeObserver().addOnPreDrawListener(this);
    }

    public void onDetachedFromWindow() {
        getViewTreeObserver().removeOnPreDrawListener(this);
    }

    public void reset() {
        this.liking = false;
        this.f45e = null;
        this.attViews.clear();
        this.audioPlaylist = null;
        this.photoClickListener = null;
        this.attachContainer.resetParams();
    }

    private static void logtime(long t) {
        Log.m525d("vk", "- " + (System.currentTimeMillis() - t));
    }

    public void setData(NewsEntry entry, boolean photosMode, ListImageLoaderWrapper imgLoader) {
        long t = System.currentTimeMillis();
        this.imgLoader = imgLoader;
        this.photosMode = photosMode;
        if (this.f45e != null) {
            throw new IllegalStateException();
        }
        this.f45e = entry;
        if (this.f45e.userName == null) {
            this.f45e.userName = "DELETED";
        }
        this.name.setText(this.f45e.userName);
        if (this.f45e.type == 0) {
            if (this.f45e.flag(32)) {
                this.retweetName.setVisibility(0);
                this.retweetPhoto.setVisibility(0);
                this.retweetTime.setVisibility(0);
                this.retweetName.setText(this.f45e.retweetUserName);
                String rtime = ACRAConstants.DEFAULT_STRING_VALUE;
                if (this.f45e.retweetOrigTime == -1) {
                    rtime = getResources().getString(C0436R.string.photo);
                } else if (this.f45e.retweetOrigTime == -2) {
                    rtime = getResources().getString(C0436R.string.video);
                } else {
                    rtime = this.f45e.retweetOrigTime > 0 ? Global.langDateRelative(this.f45e.retweetOrigTime, getResources()) : ACRAConstants.DEFAULT_STRING_VALUE;
                }
                this.retweetTime.setText(rtime);
                if (this.f45e.retweetText == null || this.f45e.retweetText.length() <= 0) {
                    this.retweetText.setVisibility(8);
                } else {
                    this.retweetText.setVisibility(0);
                    this.retweetText.setText(this.f45e.displayableRetweetText);
                }
            } else {
                this.retweetName.setVisibility(8);
                this.retweetPhoto.setVisibility(8);
                this.retweetTime.setVisibility(8);
                this.retweetText.setVisibility(8);
            }
        } else if (findViewById(C0436R.id.post_retweet_name) != null) {
            this.retweetName.setVisibility(8);
            this.retweetPhoto.setVisibility(8);
            this.retweetTime.setVisibility(8);
            this.retweetText.setVisibility(8);
        }
        Log.m528i("vk", "Attachments = " + this.f45e.attachments);
        if (!photosMode) {
            if (this.f45e.flag(16)) {
                this.post.setTextColor(getResources().getColorStateList(C0436R.color.hint));
            } else {
                this.post.setTextColor(getResources().getColorStateList(C0436R.color.main_text));
            }
            this.post.setText(this.f45e.displayablePreviewText);
        }
        this.nlikes = entry.numLikes;
        if (this.f45e.type != 6 && this.f45e.type != 7 && this.f45e.type != 9) {
            this.post.setVisibility(this.f45e.text.length() == 0 ? 8 : 0);
            if (!photosMode) {
                this.postLikes.setVisibility(0);
                this.postComments.setVisibility(0);
                this.postReposts.setVisibility(this.f45e.type != 4 ? 0 : 8);
                if (findViewById(C0436R.id.post_divider) != null) {
                    findViewById(C0436R.id.post_divider).setVisibility(0);
                }
            }
        } else if (!photosMode) {
            this.postLikes.setVisibility(8);
            this.postComments.setVisibility(8);
            this.postReposts.setVisibility(8);
            findViewById(C0436R.id.post_divider).setVisibility(8);
        }
        resetAttachments();
        if (this.f45e.attachments.size() > 0) {
            this.attachContainer.setVisibility(0);
            addAttachments(this, this.f45e.attachments, this.f45e, C0436R.id.post_attach_container);
        } else {
            this.attachContainer.setVisibility(8);
        }
        if (!photosMode) {
            if (this.f45e.repostAttachments.size() > 0) {
                this.repostAttachContainer.setVisibility(0);
                addAttachments(this, this.f45e.repostAttachments, this.f45e, C0436R.id.post_repost_attach_container);
            } else {
                this.repostAttachContainer.setVisibility(8);
            }
        }
        updateInfoLine();
        if (this.f45e.lastComment != null) {
            View lcv = findViewById(C0436R.id.post_ncomment);
            if (lcv == null) {
                lcv = View.inflate(getContext(), C0436R.layout.ncomment, null);
                lcv.setId(C0436R.id.post_ncomment);
                lcv.setClickable(false);
                lcv.setFocusable(false);
                LayoutParams lp = new LayoutParams(-1, -2);
                lp.addRule(3, C0436R.id.post_comments);
                lcv.setLayoutParams(lp);
                addView(lcv);
            }
            ((TextView) lcv.findViewById(C0436R.id.ncomm_comment)).setText(Global.replaceEmoji(Global.replaceHTML(this.f45e.lastComment).replaceAll("\\[id(\\d+)\\|([^\\]]+)\\]", "$2")));
            ((TextView) lcv.findViewById(C0436R.id.ncomm_user)).setText(this.f45e.lastCommentUserName);
            ((TextView) lcv.findViewById(C0436R.id.ncomm_time)).setText(Global.langDate(getResources(), this.f45e.lastCommentTime));
            lcv.findViewById(C0436R.id.ncomm_photo).setTag(this.f45e.lastCommentUserPhoto);
            if (this.f45e.numComments > 1) {
                ((TextView) findViewById(C0436R.id.ncomm_ntext)).setText(Global.langPlural(C0436R.array.ncomments_last, this.f45e.numComments, getResources()));
            } else {
                ((TextView) findViewById(C0436R.id.ncomm_ntext)).setText(C0436R.string.comments_one_comment);
            }
        }
    }

    public void onLayout(boolean c, int l, int t, int r, int b) {
        long tm = System.currentTimeMillis();
        super.onLayout(c, l, t, r, b);
        Log.m528i("vk", "Layout = " + (System.currentTimeMillis() - tm));
    }

    public void resetAttachments() {
        if (this.attachContainer != null) {
            int i;
            View att;
            for (i = 0; i < this.attachContainer.getChildCount(); i++) {
                att = this.attachContainer.getChildAt(i);
                if (att.getTag() != null && (att.getTag() instanceof String)) {
                    Attachment.reuseView(att, att.getTag().toString());
                }
            }
            this.attachContainer.removeAllViews();
            if (this.repostAttachContainer != null) {
                for (i = 0; i < this.repostAttachContainer.getChildCount(); i++) {
                    att = this.repostAttachContainer.getChildAt(i);
                    if (att.getTag() != null && (att.getTag() instanceof String)) {
                        Attachment.reuseView(att, att.getTag().toString());
                    }
                }
                this.repostAttachContainer.removeAllViews();
            }
        }
    }

    public static void addAttachments(View item, ArrayList<Attachment> atts, NewsEntry e, int containerID) {
        Log.m528i("vk", "begin add atts");
        long t = System.currentTimeMillis();
        int pi = 0;
        int ai = 0;
        ArrayList<Photo> photos = new ArrayList();
        ArrayList<AudioFile> audios = new ArrayList();
        int nPhotos = 0;
        Iterator it = atts.iterator();
        while (it.hasNext()) {
            Attachment att = (Attachment) it.next();
            if ((att instanceof PhotoAttachment) && !(att instanceof AlbumAttachment)) {
                nPhotos++;
                photos.add(new Photo((PhotoAttachment) att));
            }
            if (att instanceof AudioAttachment) {
                AudioAttachment aa = (AudioAttachment) att;
                audios.add(new AudioFile(aa.aid, aa.oid, aa.artist, aa.title, aa.duration, null));
            }
        }
        AudioFile[] audioPlaylist = null;
        if (audios.size() > 0) {
            audioPlaylist = (AudioFile[]) audios.toArray(new AudioFile[audios.size()]);
        }
        View thumb = null;
        Iterator it2 = atts.iterator();
        while (it2.hasNext()) {
            att = (Attachment) it2.next();
            if (att != null) {
                View v = e != null ? att.getViewForList(item.getContext(), null) : att.getFullView(item.getContext());
                logtime(t);
                if (nPhotos > 0 && (att instanceof PhotoAttachment) && !(att instanceof AlbumAttachment)) {
                    Photo photo = new Photo((PhotoAttachment) att);
                    v.setOnClickListener(new C03434(pi, photos, e, item, containerID));
                    pi++;
                    if (nPhotos == 1) {
                        ((ImageView) v).setScaleType(ScaleType.FIT_START);
                    }
                }
                if (att instanceof AudioAttachment) {
                    AudioAttachView aav = (AudioAttachView) v;
                    aav.playlist = audioPlaylist;
                    aav.playlistPos = ai;
                    ai++;
                }
                if ((att instanceof ThumbAttachment) || (att instanceof DocumentAttachment)) {
                    thumb = v;
                }
                logtime(t);
                ((ViewGroup) item.findViewById(containerID)).addView(v);
                logtime(t);
            }
        }
        if (thumb != null) {
            FlowLayout.LayoutParams lp = (FlowLayout.LayoutParams) thumb.getLayoutParams();
            if (lp != null) {
                lp.breakAfter = true;
                thumb.setLayoutParams(lp);
            }
        }
        logtime(t);
        Log.m528i("vk", "end add atts");
    }

    protected void updateInfoLine() {
        if (this.f45e.type != 8) {
            CharSequence infoHtml = ACRAConstants.DEFAULT_STRING_VALUE;
            if (!this.showDateAgo) {
                infoHtml = Global.langDate(getResources(), this.f45e.time);
            } else if (this.photosMode) {
                StringBuilder append = new StringBuilder(String.valueOf(Global.langPlural(Friends.getFromAll(this.f45e.ownerID).f151f ? C0436R.array.photos_tagged_short_f : C0436R.array.photos_tagged_short_m, this.f45e.postID, getResources()))).append(", ");
                String langDateRelative = Global.langDateRelative(this.f45e.time, getResources());
                this.ta = langDateRelative;
                infoHtml = append.append(langDateRelative).toString();
            } else {
                infoHtml = Global.langDateRelative(this.f45e.time, getResources());
                this.ta = infoHtml;
            }
            if (this.f45e.flag(NewsEntry.FLAG_FRIENDS_ONLY) || this.f45e.flag(GLRenderBuffer.EGL_SURFACE_SIZE)) {
                SpannableStringBuilder bldr = new SpannableStringBuilder(infoHtml);
                Spannable sp = Factory.getInstance().newSpannable("F");
                Drawable d = getResources().getDrawable(this.f45e.flag(NewsEntry.FLAG_FRIENDS_ONLY) ? C0436R.drawable.ic_post_friendsonly : C0436R.drawable.ic_post_pinned);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                sp.setSpan(new ImageSpan(d, 1), 0, 1, 0);
                bldr.append(" ");
                bldr.append(sp);
                infoHtml = bldr;
            }
            int padContent = Global.scale(15.0f);
            int padEmpty = Global.scale(20.0f);
            if (!(this.f45e.type == 6 || this.f45e.type == 7 || this.f45e.type == 9)) {
                if (this.f45e.numComments > 0) {
                    ((TextView) findViewById(C0436R.id.post_comments)).setText(new StringBuilder(String.valueOf(this.f45e.numComments)).toString());
                    ((TextView) findViewById(C0436R.id.post_comments)).setCompoundDrawablePadding(Global.scale(10.0f));
                    findViewById(C0436R.id.post_comments).setPadding(padContent, 0, padContent, 0);
                } else {
                    ((TextView) findViewById(C0436R.id.post_comments)).setText(ACRAConstants.DEFAULT_STRING_VALUE);
                    ((TextView) findViewById(C0436R.id.post_comments)).setCompoundDrawablePadding(0);
                    findViewById(C0436R.id.post_comments).setPadding(padEmpty, 0, padEmpty, 0);
                }
                if (this.f45e.numLikes > 0) {
                    ((TextView) findViewById(C0436R.id.post_likes)).setText(new StringBuilder(String.valueOf(this.f45e.numLikes)).toString());
                    ((TextView) findViewById(C0436R.id.post_likes)).setCompoundDrawablePadding(Global.scale(10.0f));
                    findViewById(C0436R.id.post_likes).setPadding(padContent, 0, padContent, 0);
                } else {
                    ((TextView) findViewById(C0436R.id.post_likes)).setText(ACRAConstants.DEFAULT_STRING_VALUE);
                    ((TextView) findViewById(C0436R.id.post_likes)).setCompoundDrawablePadding(0);
                    findViewById(C0436R.id.post_likes).setPadding(padEmpty, 0, padEmpty, 0);
                }
                if (this.f45e.numRetweets > 0) {
                    ((TextView) findViewById(C0436R.id.post_reposts)).setText(new StringBuilder(String.valueOf(this.f45e.numRetweets)).toString());
                    ((TextView) findViewById(C0436R.id.post_reposts)).setCompoundDrawablePadding(Global.scale(10.0f));
                    findViewById(C0436R.id.post_reposts).setPadding(padContent, 0, padContent, 0);
                } else {
                    ((TextView) findViewById(C0436R.id.post_reposts)).setText(ACRAConstants.DEFAULT_STRING_VALUE);
                    ((TextView) findViewById(C0436R.id.post_reposts)).setCompoundDrawablePadding(0);
                    findViewById(C0436R.id.post_reposts).setPadding(padEmpty, 0, padEmpty, 0);
                }
                findViewById(C0436R.id.post_likes).setSelected(this.f45e.flag(8));
                findViewById(C0436R.id.post_reposts).setSelected(this.f45e.flag(4));
            }
            ((TextView) findViewById(C0436R.id.post_info_view)).setText(infoHtml);
            if (this.updListener != null) {
                this.updListener.onInfoUpdate(this, this.f45e);
            }
        }
    }

    public void setOnInfoUpdateListener(OnInfoUpdateListener l) {
        this.updListener = l;
    }

    private void updateAttachViews() {
        if (getParent() != null && this.f45e != null && this.f45e.attachments.size() >= 5) {
            int bOffset = (getHeight() - getInvisPartBottom()) - this.attachContainer.getTop();
            int i = 0;
            Iterator it = this.attViews.iterator();
            while (it.hasNext()) {
                AttachmentViewInfo info = (AttachmentViewInfo) it.next();
                if (info.view == null && info.rect.top <= bOffset) {
                    Attachment att = (Attachment) this.f45e.attachments.get(i);
                    View view = att.getViewForList(getContext(), null);
                    info.view = view;
                    if (view instanceof AudioAttachView) {
                        AudioAttachment aa = (AudioAttachment) att;
                        int j = 0;
                        while (j < this.audioPlaylist.length) {
                            if (this.audioPlaylist[j].oid == aa.oid && this.audioPlaylist[j].aid == aa.aid) {
                                ((AudioAttachView) view).playlistPos = j;
                                break;
                            }
                            j++;
                        }
                        ((AudioAttachView) view).playlist = this.audioPlaylist;
                    }
                    if ((att instanceof PhotoAttachment) && !(att instanceof AlbumAttachment)) {
                        view.setTag(C0436R.id.photo_image, Integer.valueOf(i));
                        view.setOnClickListener(this.photoClickListener);
                    }
                    this.attachContainer.addView(view);
                    String src;
                    if (att instanceof AlbumAttachment) {
                        src = ((PhotoAttachment) att).getThumbURL();
                        if (!this.imgLoader.isAlreadyLoaded(src) || this.imgLoader.get(src) == null) {
                            ((ImageView) view.findViewById(C0436R.id.attach_album_image)).setImageDrawable(new ColorDrawable(this.photosMode ? -15592942 : Color.LIGHT_GRAY));
                        } else {
                            ((ImageView) view.findViewById(C0436R.id.attach_album_image)).setImageBitmap(this.imgLoader.get(src));
                            ((FixedSizeImageView) view.findViewById(C0436R.id.attach_album_image)).dontAnimate();
                        }
                    } else if (att instanceof PhotoAttachment) {
                        src = ((PhotoAttachment) att).getThumbURL();
                        if (!this.imgLoader.isAlreadyLoaded(src) || this.imgLoader.get(src) == null) {
                            ((ImageView) view).setImageDrawable(new ColorDrawable(this.photosMode ? -15592942 : Color.LIGHT_GRAY));
                        } else {
                            ((ImageView) view).setImageBitmap(this.imgLoader.get(src));
                            ((FixedSizeImageView) view).dontAnimate();
                        }
                    } else if (att instanceof VideoAttachment) {
                        src = ((VideoAttachment) att).image;
                        if (this.imgLoader.isAlreadyLoaded(src)) {
                            ((VideoAttachView) view).setImageBitmap(this.imgLoader.get(src));
                        }
                    }
                }
                i++;
            }
        }
    }

    public void updateDate() {
        String t = Global.langDateRelative(this.f45e.time, getResources());
        if (!this.ta.equals(t)) {
            ((TextView) findViewById(C0436R.id.post_info_view)).setText(t);
            this.ta = t;
        }
    }

    public void updateLikes() {
        updateInfoLine();
    }

    public View getImageAttachView(int idx) {
        int cnt = 0;
        int i = 0;
        Iterator it = this.f45e.repostAttachments.iterator();
        while (it.hasNext()) {
            if (((Attachment) it.next()) instanceof ImageAttachment) {
                if (cnt == idx) {
                    return this.repostAttachContainer.getChildAt(i);
                }
                cnt++;
            }
            i++;
        }
        i = 0;
        it = this.f45e.attachments.iterator();
        while (it.hasNext()) {
            if (((Attachment) it.next()) instanceof ImageAttachment) {
                if (cnt == idx) {
                    return this.attachContainer.getChildAt(i);
                }
                cnt++;
            }
            i++;
        }
        return null;
    }

    private static void openPhotoList(int index, View view, ArrayList<Photo> photos, NewsEntry e, View container, int vID) {
        ViewGroup attachContainer = (ViewGroup) container.findViewById(vID);
        for (int i = 0; i < Math.min(attachContainer.getChildCount(), photos.size()); i++) {
            int i2;
            View v = attachContainer.getChildAt(i);
            int[] pos = new int[2];
            v.getLocationOnScreen(pos);
            ((Photo) photos.get(i)).viewBounds = new Rect(pos[0], pos[1], pos[0] + v.getWidth(), pos[1] + v.getHeight());
            int top = ViewUtils.getViewOffset(v, (View) container.getParent()).y;
            Photo photo = (Photo) photos.get(i);
            if (top < 0) {
                i2 = -top;
            } else {
                i2 = 0;
            }
            photo.viewClipTop = i2;
        }
        Drawable d;
        Bundle args;
        if (e == null || (!(e.type == 6 || e.type == 7 || e.type == 9) || e.postID <= 5)) {
            d = ((ImageView) attachContainer.getChildAt(index)).getDrawable();
            if (d != null && (d instanceof BitmapDrawable)) {
                PhotoViewerFragment.sharedThumb = ((BitmapDrawable) d).getBitmap();
            }
            args = new Bundle();
            args.putInt("orientation", view.getResources().getConfiguration().orientation);
            args.putParcelableArrayList("list", photos);
            args.putInt(GLFilterContext.AttributePosition, index);
            if ((container instanceof NewsItemView) && ((NewsItemView) container).photosMode) {
                args.putInt("bg_color", Color.ALBUMS_LIST_BACKGROUND);
            }
            Navigate.to("PhotoViewerFragment", args, (Activity) container.getContext(), true, -1, -1);
            return;
        }
        args = new Bundle();
        d = ((ImageView) attachContainer.getChildAt(index)).getDrawable();
        if (d != null && (d instanceof BitmapDrawable)) {
            PhotoViewerFragment.sharedThumb = ((BitmapDrawable) d).getBitmap();
        }
        args.putInt("orientation", view.getResources().getConfiguration().orientation);
        args.putParcelableArrayList("list", photos);
        args.putInt(GLFilterContext.AttributePosition, index);
        args.putParcelable("feed_entry", e);
        if ((container instanceof NewsItemView) && ((NewsItemView) container).photosMode) {
            args.putInt("bg_color", Color.ALBUMS_LIST_BACKGROUND);
        }
        Navigate.to("PhotoViewerFragment", args, (Activity) container.getContext(), true, -1, -1);
    }

    private void like(boolean liked) {
        this.f45e.flag(8, liked);
        NewsEntry newsEntry;
        if (liked) {
            newsEntry = this.f45e;
            newsEntry.numLikes++;
        } else {
            newsEntry = this.f45e;
            newsEntry.numLikes--;
        }
        updateInfoLine();
        if (!this.liking) {
            this.liking = true;
            boolean z = liked;
            int i = 0;
            new WallLike(z, this.f45e.ownerID, this.f45e.postID, false, this.f45e.type, i, ACRAConstants.DEFAULT_STRING_VALUE).setCallback(new C13205(this.f45e.ownerID, this.f45e.postID, liked)).exec();
        }
    }

    private int getInvisPartBottom() {
        int pHeight = ((View) getParent()).getHeight();
        if (getBottom() > pHeight) {
            return getBottom() - pHeight;
        }
        return 0;
    }

    private int getInvisPartTop() {
        return -Math.min(0, getTop());
    }

    public boolean onPreDraw() {
        if (getParent() != null) {
            updateAttachViews();
        }
        return true;
    }
}
