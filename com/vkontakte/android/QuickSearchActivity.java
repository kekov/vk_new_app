package com.vkontakte.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockActivity;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.SearchGetHints;
import com.vkontakte.android.api.SearchGetHints.Callback;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.LoadMoreFooterView;
import com.vkontakte.android.ui.MultiSectionAdapter;
import com.vkontakte.android.ui.PaddingColorDrawable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.acra.ACRAConstants;

public class QuickSearchActivity extends SherlockActivity {
    private static final int MAX_WIDTH;
    private static final int SPEECH_RESULT = 101;
    private ResultsAdapter adapter;
    private View box;
    private LinearLayout contentView;
    private boolean dataLoading;
    private TextView emptyView;
    private EditText field;
    private LoadMoreFooterView footerProgress;
    private ArrayList<UserProfile> globalResults;
    private ListImageLoaderWrapper imgLoader;
    private String link;
    private ListView list;
    private ArrayList<UserProfile> localResults;
    private Runnable localSearchRunnable;
    private APIRequest searchReq;
    private Runnable searchRunnable;
    private FrameLayout wrap;

    /* renamed from: com.vkontakte.android.QuickSearchActivity.1 */
    class C04281 extends ListView {
        C04281(Context $anonymous0) {
            super($anonymous0);
        }

        protected float getBottomFadingEdgeStrength() {
            return 0.0f;
        }
    }

    /* renamed from: com.vkontakte.android.QuickSearchActivity.3 */
    class C04293 implements OnItemClickListener {
        C04293() {
        }

        public void onItemClick(AdapterView<?> adapterView, View vv, int pos, long id) {
            if (id == 0) {
                QuickSearchActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("vkontakte://vk.com/" + QuickSearchActivity.this.link)));
            } else if (id == 2147483647L) {
                args = new Bundle();
                args.putString("q", ((TextView) QuickSearchActivity.this.findViewById(C0436R.id.quick_search_box)).getText().toString());
                Navigate.to("BrowseUsersFragment", args, QuickSearchActivity.this);
            } else {
                args = new Bundle();
                args.putInt("id", (int) id);
                Navigate.to("ProfileFragment", args, QuickSearchActivity.this);
            }
        }
    }

    /* renamed from: com.vkontakte.android.QuickSearchActivity.4 */
    class C04304 implements TextWatcher {
        C04304() {
        }

        public void afterTextChanged(Editable arg0) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            QuickSearchActivity.this.findViewById(C0436R.id.quick_search_clear).setVisibility(s.length() > 0 ? QuickSearchActivity.MAX_WIDTH : 8);
            QuickSearchActivity.this.searchLocal(s.toString());
            if (s.length() > 0) {
                QuickSearchActivity.this.footerProgress.setVisible(true);
            }
            if (QuickSearchActivity.this.searchReq != null) {
                QuickSearchActivity.this.searchReq.cancel();
                QuickSearchActivity.this.searchReq = null;
            }
            QuickSearchActivity.this.globalResults.clear();
            QuickSearchActivity.this.updateList();
            if (QuickSearchActivity.this.searchRunnable != null) {
                QuickSearchActivity.this.contentView.removeCallbacks(QuickSearchActivity.this.searchRunnable);
            } else {
                QuickSearchActivity.this.searchRunnable = new SearchRunnable(null);
            }
            QuickSearchActivity.this.contentView.postDelayed(QuickSearchActivity.this.searchRunnable, 300);
            if (QuickSearchActivity.this.localSearchRunnable != null) {
                QuickSearchActivity.this.contentView.removeCallbacks(QuickSearchActivity.this.localSearchRunnable);
            } else {
                QuickSearchActivity.this.localSearchRunnable = new LocalSearchRunnable(null);
            }
            QuickSearchActivity.this.contentView.postDelayed(QuickSearchActivity.this.localSearchRunnable, 200);
        }
    }

    /* renamed from: com.vkontakte.android.QuickSearchActivity.5 */
    class C04315 implements OnClickListener {
        C04315() {
        }

        public void onClick(View v) {
            QuickSearchActivity.this.startVoiceRecognizer();
        }
    }

    /* renamed from: com.vkontakte.android.QuickSearchActivity.6 */
    class C04326 implements OnClickListener {
        C04326() {
        }

        public void onClick(View v) {
            QuickSearchActivity.this.field.setText(ACRAConstants.DEFAULT_STRING_VALUE);
        }
    }

    /* renamed from: com.vkontakte.android.QuickSearchActivity.7 */
    class C04337 implements OnEditorActionListener {
        C04337() {
        }

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            ((InputMethodManager) QuickSearchActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(v.getWindowToken(), QuickSearchActivity.MAX_WIDTH);
            if (QuickSearchActivity.this.searchRunnable != null) {
                QuickSearchActivity.this.contentView.removeCallbacks(QuickSearchActivity.this.searchRunnable);
                QuickSearchActivity.this.searchRunnable.run();
                QuickSearchActivity.this.searchRunnable = null;
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.QuickSearchActivity.9 */
    class C04349 implements Runnable {
        C04349() {
        }

        public void run() {
            QuickSearchActivity.this.imgLoader.updateImages();
        }
    }

    private class LocalSearchRunnable implements Runnable {
        private LocalSearchRunnable() {
        }

        public void run() {
            QuickSearchActivity.this.localSearchRunnable = null;
            QuickSearchActivity.this.searchLocal(QuickSearchActivity.this.field.getText().toString());
        }
    }

    private class SearchRunnable implements Runnable {
        private SearchRunnable() {
        }

        public void run() {
            QuickSearchActivity.this.searchRunnable = null;
            QuickSearchActivity.this.searchFromNetwork(QuickSearchActivity.this.field.getText().toString());
        }
    }

    /* renamed from: com.vkontakte.android.QuickSearchActivity.2 */
    class C13402 implements Listener {
        C13402() {
        }

        public void onScrolledToLastItem() {
        }

        public void onScrollStopped() {
        }

        public void onScrollStarted() {
            if (QuickSearchActivity.this.getCurrentFocus() != null) {
                View f = QuickSearchActivity.this.getCurrentFocus();
                f.clearFocus();
                ((InputMethodManager) QuickSearchActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(f.getWindowToken(), QuickSearchActivity.MAX_WIDTH);
            }
        }
    }

    /* renamed from: com.vkontakte.android.QuickSearchActivity.8 */
    class C13418 implements Callback {
        private final /* synthetic */ String val$q;

        C13418(String str) {
            this.val$q = str;
        }

        public void success(List<UserProfile> result) {
            QuickSearchActivity.this.searchReq = null;
            QuickSearchActivity.this.globalResults.clear();
            try {
                Matcher matcher = Pattern.compile("^(?:(?:http|https)://)?(?:vk\\.com|vkontakte\\.ru)?/?([a-zA-Z0-9_\\.\\?=&%-]+)$").matcher(this.val$q);
                UserProfile p;
                UserProfile linkProfile;
                if (matcher.matches()) {
                    QuickSearchActivity.this.link = matcher.group(1);
                    if (!(QuickSearchActivity.this.link.matches("id[0-9]+") || QuickSearchActivity.this.link.matches("club[0-9]+"))) {
                        p = new UserProfile();
                        p.fullName = QuickSearchActivity.this.getString(C0436R.string.attach_link);
                        p.university = "vk.com/" + QuickSearchActivity.this.link;
                        QuickSearchActivity.this.globalResults.add(p);
                        linkProfile = p;
                    }
                } else {
                    matcher = Pattern.compile("^@([a-zA-Z0-9_\\.-]+)$").matcher(this.val$q);
                    if (matcher.matches()) {
                        QuickSearchActivity.this.link = matcher.group(1);
                        p = new UserProfile();
                        p.fullName = QuickSearchActivity.this.getString(C0436R.string.attach_link);
                        p.university = "vk.com/" + QuickSearchActivity.this.link;
                        QuickSearchActivity.this.globalResults.add(p);
                        linkProfile = p;
                    }
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
            UserProfile extSearchProfile = new UserProfile();
            extSearchProfile.fullName = QuickSearchActivity.this.getString(C0436R.string.suggest_type_ext_search);
            extSearchProfile.uid = ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
            extSearchProfile.university = null;
            QuickSearchActivity.this.globalResults.add(extSearchProfile);
            for (UserProfile pG : result) {
                boolean found = false;
                Iterator it = QuickSearchActivity.this.localResults.iterator();
                while (it.hasNext()) {
                    if (pG.uid == ((UserProfile) it.next()).uid) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    QuickSearchActivity.this.globalResults.add(pG);
                }
            }
            QuickSearchActivity.this.footerProgress.setVisible(false);
            QuickSearchActivity.this.dataLoading = false;
            QuickSearchActivity.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            QuickSearchActivity.this.searchReq = null;
            Toast.makeText(QuickSearchActivity.this, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, QuickSearchActivity.MAX_WIDTH).show();
            QuickSearchActivity.this.footerProgress.setVisible(false);
            QuickSearchActivity.this.dataLoading = false;
            QuickSearchActivity.this.updateList();
        }
    }

    private class ResultsAdapter extends MultiSectionAdapter {
        private ResultsAdapter() {
        }

        public View getView(int section, int item, View convertView) {
            UserProfile profile = (UserProfile) (section == 0 ? QuickSearchActivity.this.localResults : QuickSearchActivity.this.globalResults).get(item);
            View view = convertView;
            if (view == null) {
                view = View.inflate(QuickSearchActivity.this, profile.university != null ? C0436R.layout.quick_search_item_subtitle : C0436R.layout.quick_search_item, null);
            }
            view.setBackgroundResource(item == 0 ? C0436R.drawable.highlight_left_menu_first : C0436R.drawable.highlight_left_menu);
            view.setPadding(Global.scale(13.0f), QuickSearchActivity.MAX_WIDTH, Global.scale(13.0f), QuickSearchActivity.MAX_WIDTH);
            if (profile.f151f) {
                SpannableStringBuilder sb = new SpannableStringBuilder();
                sb.append(profile.fullName);
                Spannable sp = Factory.getInstance().newSpannable("F");
                Drawable d = QuickSearchActivity.this.getResources().getDrawable(C0436R.drawable.ic_verified);
                d.setBounds(QuickSearchActivity.MAX_WIDTH, QuickSearchActivity.MAX_WIDTH, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                sp.setSpan(new ImageSpan(d, 1), QuickSearchActivity.MAX_WIDTH, 1, QuickSearchActivity.MAX_WIDTH);
                sb.append("\u00a0");
                sb.append(sp);
                ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(sb);
            } else {
                ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(profile.fullName);
            }
            if (profile.university != null) {
                ((TextView) view.findViewById(C0436R.id.flist_item_subtext)).setText(profile.university);
            }
            if (profile.uid == 0) {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(C0436R.drawable.ic_left_link);
            } else if (profile.uid == ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED) {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(C0436R.drawable.ic_left_open_search);
            } else if (QuickSearchActivity.this.imgLoader.isAlreadyLoaded(profile.photo)) {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageBitmap(QuickSearchActivity.this.imgLoader.get(profile.photo));
            } else {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(profile.uid > 0 ? C0436R.drawable.user_placeholder_dark : C0436R.drawable.group_placeholder_dark);
            }
            View findViewById = view.findViewById(C0436R.id.flist_item_online);
            int i = (profile.uid <= 0 || profile.online <= 0) ? 8 : QuickSearchActivity.MAX_WIDTH;
            findViewById.setVisibility(i);
            ((ImageView) view.findViewById(C0436R.id.flist_item_online)).setImageResource(profile.online == 1 ? C0436R.drawable.ic_left_online : C0436R.drawable.ic_left_online_mobile);
            return view;
        }

        public String getSectionTitle(int section) {
            if (section == 1) {
                return QuickSearchActivity.this.getString(C0436R.string.search_global);
            }
            return ACRAConstants.DEFAULT_STRING_VALUE;
        }

        public int getSectionCount() {
            return 2;
        }

        public int getItemCount(int section) {
            switch (section) {
                case QuickSearchActivity.MAX_WIDTH:
                    return QuickSearchActivity.this.localResults.size();
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return QuickSearchActivity.this.globalResults.size();
                default:
                    return QuickSearchActivity.MAX_WIDTH;
            }
        }

        public long getItemId(int section, int item) {
            ArrayList access$0;
            if (section == 0) {
                try {
                    access$0 = QuickSearchActivity.this.localResults;
                } catch (Exception e) {
                    return -1;
                }
            }
            access$0 = QuickSearchActivity.this.globalResults;
            return (long) ((UserProfile) access$0.get(item)).uid;
        }

        public boolean isSectionHeaderVisible(int section) {
            return section == 1 && QuickSearchActivity.this.globalResults.size() > 0;
        }

        public int getExtraViewTypeCount() {
            return 2;
        }

        public int getItemViewType(int section, int item) {
            return ((UserProfile) (section == 0 ? QuickSearchActivity.this.localResults : QuickSearchActivity.this.globalResults).get(item)).university != null ? 2 : QuickSearchActivity.MAX_WIDTH;
        }

        public int getHeaderLayoutResource() {
            return C0436R.layout.list_menu_section_header;
        }
    }

    private class ResultsImagesAdapter extends MultiSectionImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.QuickSearchActivity.ResultsImagesAdapter.1 */
        class C04351 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C04351(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.flist_item_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private ResultsImagesAdapter() {
        }

        public int getSectionCount() {
            return QuickSearchActivity.this.adapter.getSectionCount();
        }

        public int getItemCount(int section) {
            return QuickSearchActivity.this.adapter.getItemCount(section);
        }

        public boolean isSectionHeaderVisible(int section) {
            return QuickSearchActivity.this.adapter.isSectionHeaderVisible(section);
        }

        public int getImageCountForItem(int section, int item) {
            long id = QuickSearchActivity.this.adapter.getItemId(section, item);
            return (id == 0 || id == 2147483647L) ? QuickSearchActivity.MAX_WIDTH : 1;
        }

        public String getImageURL(int section, int item, int image) {
            return ((UserProfile) (section == 0 ? QuickSearchActivity.this.localResults : QuickSearchActivity.this.globalResults).get(item)).photo;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += QuickSearchActivity.this.list.getHeaderViewsCount();
            if (item >= QuickSearchActivity.this.list.getFirstVisiblePosition() && item <= QuickSearchActivity.this.list.getLastVisiblePosition()) {
                QuickSearchActivity.this.runOnUiThread(new C04351(QuickSearchActivity.this.list.getChildAt(item - QuickSearchActivity.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    public QuickSearchActivity() {
        this.localResults = new ArrayList();
        this.globalResults = new ArrayList();
        this.dataLoading = false;
        this.link = ACRAConstants.DEFAULT_STRING_VALUE;
    }

    static {
        MAX_WIDTH = Global.scale(400.0f);
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        getSupportActionBar().hide();
        this.contentView = new LinearLayout(this);
        this.contentView.setOrientation(1);
        this.contentView.setBackgroundColor(-14473165);
        this.box = View.inflate(this, C0436R.layout.quick_search_box, null);
        this.contentView.addView(this.box);
        this.list = new C04281(this);
        if (VERSION.SDK_INT < 11) {
            this.list.setBackgroundColor(-14473165);
            this.list.setCacheColorHint(-14473165);
        }
        this.footerProgress = new LoadMoreFooterView(this);
        this.footerProgress.getProgressBar().setIndeterminateDrawable(getResources().getDrawable(C0436R.drawable.progress_light));
        this.list.addFooterView(this.footerProgress, null, false);
        this.footerProgress.setVisible(false);
        ListView listView = this.list;
        ListAdapter resultsAdapter = new ResultsAdapter();
        this.adapter = resultsAdapter;
        listView.setAdapter(resultsAdapter);
        this.list.setSelector(new ColorDrawable(MAX_WIDTH));
        this.list.setDivider(new PaddingColorDrawable(1056964608, Global.scale(6.0f)));
        this.list.setDividerHeight(Global.scale(1.0f));
        this.list.setVerticalFadingEdgeEnabled(true);
        this.list.setFadingEdgeLength(Global.scale(15.0f));
        this.emptyView = new TextView(this);
        this.emptyView.setText(C0436R.string.nothing_found);
        this.emptyView.setGravity(49);
        this.emptyView.setPadding(MAX_WIDTH, Global.scale(BitmapDescriptorFactory.HUE_ORANGE), MAX_WIDTH, MAX_WIDTH);
        this.emptyView.setTextSize(GalleryPickerFooterView.BADGE_SIZE);
        this.emptyView.setTextColor(-1426063361);
        this.wrap = new FrameLayout(this);
        this.wrap.addView(this.list);
        this.wrap.addView(this.emptyView, new LayoutParams(-1, -1));
        this.contentView.addView(this.wrap, new LinearLayout.LayoutParams(-1, -1));
        setContentView(this.contentView);
        this.imgLoader = new ListImageLoaderWrapper(new ResultsImagesAdapter(), this.list, new C13402());
        this.imgLoader.updateImages();
        this.list.setOnItemClickListener(new C04293());
        this.field = (EditText) findViewById(C0436R.id.quick_search_box);
        this.field.addTextChangedListener(new C04304());
        findViewById(C0436R.id.quick_search_voice).setOnClickListener(new C04315());
        if (!voiceSearchAvailable()) {
            findViewById(C0436R.id.quick_search_voice).setVisibility(8);
        }
        findViewById(C0436R.id.quick_search_clear).setOnClickListener(new C04326());
        this.field.setOnEditorActionListener(new C04337());
        findViewById(C0436R.id.quick_search_clear).setVisibility(8);
        searchLocal(ACRAConstants.DEFAULT_STRING_VALUE);
        updateWidth();
    }

    public void onConfigurationChanged(Configuration cfg) {
        super.onConfigurationChanged(cfg);
        updateWidth();
    }

    private void updateWidth() {
        int sw = getResources().getDisplayMetrics().widthPixels;
        if (sw > MAX_WIDTH) {
            int pad = sw - MAX_WIDTH;
            this.contentView.setPadding(pad / 2, MAX_WIDTH, pad / 2, MAX_WIDTH);
            return;
        }
        this.contentView.setPadding(MAX_WIDTH, MAX_WIDTH, MAX_WIDTH, MAX_WIDTH);
    }

    private boolean voiceSearchAvailable() {
        if (getPackageManager().queryIntentActivities(new Intent("android.speech.action.RECOGNIZE_SPEECH"), MAX_WIDTH).size() > 0) {
            return true;
        }
        return false;
    }

    private void searchFromNetwork(String q) {
        if (q.length() == 0) {
            this.globalResults.clear();
            updateList();
            this.footerProgress.setVisible(false);
            return;
        }
        this.dataLoading = true;
        updateList();
        this.searchReq = new SearchGetHints(q).setCallback(new C13418(q)).exec((Activity) this);
    }

    private void searchLocal(String q) {
        UserProfile p;
        if (q == null || q.length() == 0) {
            this.localResults.clear();
            Friends.getFriends(this.localResults);
            if (this.localResults.size() > 30) {
                this.localResults.subList(30, this.localResults.size()).clear();
            }
        } else {
            this.localResults.clear();
            this.localResults.addAll(Friends.search(q));
            List<Group> gres = Groups.search(q);
            ArrayList<Integer> ids = new ArrayList();
            for (Group g : gres) {
                if (!ids.contains(Integer.valueOf(g.id))) {
                    ids.add(Integer.valueOf(g.id));
                    p = new UserProfile();
                    p.uid = -g.id;
                    p.fullName = g.name;
                    p.photo = g.photo;
                    switch (g.type) {
                        case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                            p.university = getString(g.isClosed > 0 ? C0436R.string.closed_event : C0436R.string.open_event);
                            break;
                        case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                            p.university = getString(C0436R.string.public_page);
                            break;
                        default:
                            int i = g.isClosed == 0 ? C0436R.string.open_group : g.isClosed == 1 ? C0436R.string.closed_group : C0436R.string.private_group;
                            p.university = getString(i);
                            break;
                    }
                    this.localResults.add(p);
                }
            }
        }
        for (int i2 = MAX_WIDTH; i2 < this.localResults.size(); i2++) {
            UserProfile pp = (UserProfile) this.localResults.get(i2);
            if (pp.uid >= 0) {
                p = new UserProfile();
                p.uid = pp.uid;
                p.photo = pp.photo;
                p.firstName = pp.firstName;
                p.lastName = pp.lastName;
                p.fullName = p.firstName + " " + p.lastName;
                if (pp.uid > 0) {
                    p.online = pp.online;
                    p.university = null;
                }
                this.localResults.set(i2, p);
            }
        }
        updateList();
    }

    private void updateList() {
        this.adapter.notifyDataSetChanged();
        TextView textView = this.emptyView;
        int i = (this.adapter.getCount() != 0 || this.footerProgress.isVisible()) ? 8 : MAX_WIDTH;
        textView.setVisibility(i);
        this.list.post(new C04349());
    }

    private void startVoiceRecognizer() {
        Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
        intent.putExtra("android.speech.extra.MAX_RESULTS", 10);
        startActivityForResult(intent, SPEECH_RESULT);
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode == SPEECH_RESULT && resCode == -1) {
            ArrayList<String> results = data.getStringArrayListExtra("android.speech.extra.RESULTS");
            if (results.size() > 0) {
                this.field.setText((CharSequence) results.get(MAX_WIDTH));
            }
        }
    }

    public void onPause() {
        super.onPause();
        this.imgLoader.deactivate();
    }

    public void onResume() {
        super.onResume();
        this.imgLoader.activate();
    }
}
