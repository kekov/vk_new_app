package com.vkontakte.android;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.MotionEventCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.android.vending.billing.IInAppBillingService;
import com.android.vending.billing.IInAppBillingService.Stub;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.StoreGetInventory;
import com.vkontakte.android.api.StoreGetInventory.Callback;
import com.vkontakte.android.data.StickerPack;
import com.vkontakte.android.data.Stickers;
import com.vkontakte.android.fragments.StickersDetailsFragment;
import com.vkontakte.android.fragments.SuggestionsFriendsFragment;
import com.vkontakte.android.ui.CircularProgressDrawable;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.PhotoView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class StoreActivity extends VKFragmentActivity {
    private StoreAdapter adapter;
    private OnClickListener btnClickListener;
    private FrameLayout content;
    private ErrorView error;
    private ListImageLoaderWrapper imgLoader;
    private ListView list;
    private ArrayList<StickerPack> packs;
    private ProgressBar progress;
    private BroadcastReceiver receiver;

    /* renamed from: com.vkontakte.android.StoreActivity.1 */
    class C05101 implements OnClickListener {

        /* renamed from: com.vkontakte.android.StoreActivity.1.1 */
        class C05081 implements Runnable {
            private final /* synthetic */ View val$v;

            C05081(View view) {
                this.val$v = view;
            }

            public void run() {
                this.val$v.setEnabled(true);
            }
        }

        /* renamed from: com.vkontakte.android.StoreActivity.1.2 */
        class C05092 implements Runnable {
            private final /* synthetic */ StickerPack val$pck;

            C05092(StickerPack stickerPack) {
                this.val$pck = stickerPack;
            }

            public void run() {
                if (Stickers.getPackState(this.val$pck.id) == 3) {
                    StoreActivity.this.downloadPack(this.val$pck);
                }
            }
        }

        C05101() {
        }

        public void onClick(View v) {
            v.setEnabled(false);
            v.postDelayed(new C05081(v), 300);
            StickerPack pck = (StickerPack) StoreActivity.this.packs.get(((Integer) v.getTag()).intValue());
            if (pck.state == 3 || pck.state == 4 || pck.state == 5) {
                StoreActivity.this.downloadPack(pck);
            } else if (pck.storeID == null || pck.storeID.length() <= 0) {
                Stickers.activateFreePack(pck.id, StoreActivity.this, new C05092(pck));
            } else {
                Intent intent = new Intent(StoreActivity.this, BillingActivity.class);
                intent.putExtra("product", pck.id);
                intent.putExtra("store_id", pck.storeID);
                StoreActivity.this.startActivityForResult(intent, 101);
            }
        }
    }

    /* renamed from: com.vkontakte.android.StoreActivity.2 */
    class C05112 extends BroadcastReceiver {
        C05112() {
        }

        public void onReceive(Context arg0, Intent intent) {
            if (Stickers.ACTION_STICKERS_UPDATED.equals(intent.getAction())) {
                Iterator it = StoreActivity.this.packs.iterator();
                while (it.hasNext()) {
                    StickerPack pck = (StickerPack) it.next();
                    pck.state = Stickers.getPackState(pck.id);
                }
                StoreActivity.this.updateList();
            }
            if (Stickers.ACTION_STICKERS_DOWNLOAD_PROGRESS.equals(intent.getAction())) {
                int id = intent.getIntExtra("id", 0);
                float progress = intent.getFloatExtra("progress", 0.0f);
                int i = 0;
                Iterator it2 = StoreActivity.this.packs.iterator();
                while (it2.hasNext()) {
                    if (((StickerPack) it2.next()).id == id && i >= StoreActivity.this.list.getFirstVisiblePosition() && i <= StoreActivity.this.list.getLastVisiblePosition()) {
                        View item = StoreActivity.this.list.getChildAt(i - StoreActivity.this.list.getFirstVisiblePosition());
                        item.findViewById(C0436R.id.sticker_progress).setVisibility(0);
                        item.findViewById(C0436R.id.sticker_button).setVisibility(8);
                        ((ProgressBar) item.findViewById(C0436R.id.sticker_progress)).setProgress(Math.round(100.0f * progress));
                    }
                    i++;
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.StoreActivity.3 */
    class C05123 implements OnItemClickListener {
        C05123() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            StickerPack pck = (StickerPack) StoreActivity.this.packs.get(pos);
            StickersDetailsFragment fragment = new StickersDetailsFragment();
            Bundle args = new Bundle();
            args.putInt("id", pck.id);
            args.putString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, pck.title);
            args.putString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, pck.description);
            args.putInt("count", pck.count);
            args.putString("price", pck.price);
            args.putString("storeID", pck.storeID);
            args.putString("author", pck.author);
            args.putString("background_src", "http://vk.com/images/store/stickers/" + pck.id + "/background.png");
            args.putStringArrayList("slides", pck.previews);
            if (pck.state == 7) {
                args.putBoolean("unavailable", true);
            }
            fragment.setArguments(args);
            fragment.show(StoreActivity.this.getSupportFragmentManager(), "detail");
        }
    }

    /* renamed from: com.vkontakte.android.StoreActivity.4 */
    class C05134 implements OnClickListener {
        C05134() {
        }

        public void onClick(View v) {
            StoreActivity.this.progress.setVisibility(0);
            StoreActivity.this.error.setVisibility(8);
            StoreActivity.this.loadData();
        }
    }

    /* renamed from: com.vkontakte.android.StoreActivity.6 */
    class C05176 implements ServiceConnection {
        private final /* synthetic */ HashSet val$items;

        /* renamed from: com.vkontakte.android.StoreActivity.6.1 */
        class C05161 implements Runnable {
            private final /* synthetic */ ServiceConnection val$conn;
            private final /* synthetic */ HashSet val$items;
            private final /* synthetic */ IInAppBillingService val$service;

            /* renamed from: com.vkontakte.android.StoreActivity.6.1.1 */
            class C05141 implements Runnable {
                C05141() {
                }

                public void run() {
                    StoreActivity.this.updateList();
                    Global.showViewAnimated(StoreActivity.this.list, true, PhotoView.THUMB_ANIM_DURATION);
                    Global.showViewAnimated(StoreActivity.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                }
            }

            /* renamed from: com.vkontakte.android.StoreActivity.6.1.2 */
            class C05152 implements Runnable {
                private final /* synthetic */ Exception val$x;

                C05152(Exception exception) {
                    this.val$x = exception;
                }

                public void run() {
                    StoreActivity.this.error.setErrorInfo(-2, this.val$x.getMessage());
                    Global.showViewAnimated(StoreActivity.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                    Global.showViewAnimated(StoreActivity.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                }
            }

            C05161(HashSet hashSet, IInAppBillingService iInAppBillingService, ServiceConnection serviceConnection) {
                this.val$items = hashSet;
                this.val$service = iInAppBillingService;
                this.val$conn = serviceConnection;
            }

            public void run() {
                Bundle bundle = new Bundle();
                ArrayList<String> skuList = new ArrayList();
                Iterator it = this.val$items.iterator();
                while (it.hasNext()) {
                    skuList.add((String) it.next());
                }
                Log.m525d("vk", "Getting prices for " + skuList);
                bundle.putStringArrayList("ITEM_ID_LIST", skuList);
                try {
                    Bundle details = this.val$service.getSkuDetails(3, StoreActivity.this.getPackageName(), "inapp", bundle);
                    int response = details.getInt("RESPONSE_CODE");
                    if (response != 6) {
                        ArrayList<String> responseList;
                        StickerPack pack;
                        HashMap<String, String> prices = new HashMap();
                        if (details.containsKey("DETAILS_LIST")) {
                            responseList = details.getStringArrayList("DETAILS_LIST");
                        } else {
                            responseList = new ArrayList();
                        }
                        it = responseList.iterator();
                        while (it.hasNext()) {
                            JSONObject object = new JSONObject((String) it.next());
                            prices.put(object.getString("productId"), object.getString("price"));
                        }
                        Iterator it2 = StoreActivity.this.packs.iterator();
                        while (it2.hasNext()) {
                            pack = (StickerPack) it2.next();
                            if (pack.storeID != null) {
                                if (pack.storeID.length() > 0) {
                                    pack.price = (String) prices.get(pack.storeID);
                                }
                            }
                        }
                        Iterator<StickerPack> itr = StoreActivity.this.packs.iterator();
                        while (itr.hasNext()) {
                            pack = (StickerPack) itr.next();
                            if (pack.storeID != null) {
                                if (pack.storeID.length() > 0 && pack.price == null) {
                                    if (Stickers.getPackState(pack.id) != 0) {
                                        if (Stickers.getPackState(pack.id) != 6) {
                                        }
                                    }
                                    String str = pack.title;
                                    int i = pack.id;
                                    Log.m530w("vk", "Didn't get price for " + r0 + ", id=" + r0 + ", store_id=" + pack.storeID);
                                    pack.state = 7;
                                }
                            }
                        }
                        StoreActivity.this.runOnUiThread(new C05141());
                        C05176 c05176 = C05176.this;
                        StoreActivity.this.unbindService(this.val$conn);
                        return;
                    }
                    throw new Exception("error getting prices, response=" + response);
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                    StoreActivity.this.runOnUiThread(new C05152(x));
                }
            }
        }

        C05176(HashSet hashSet) {
            this.val$items = hashSet;
        }

        public void onServiceDisconnected(ComponentName name) {
        }

        public void onServiceConnected(ComponentName name, IBinder binder) {
            IInAppBillingService service = Stub.asInterface(binder);
            APIController.runInBg(new C05161(this.val$items, service, this));
        }
    }

    /* renamed from: com.vkontakte.android.StoreActivity.7 */
    class C05197 implements ServiceConnection {

        /* renamed from: com.vkontakte.android.StoreActivity.7.1 */
        class C05181 implements Runnable {
            private final /* synthetic */ ServiceConnection val$conn;
            private final /* synthetic */ IInAppBillingService val$service;

            C05181(IInAppBillingService iInAppBillingService, ServiceConnection serviceConnection) {
                this.val$service = iInAppBillingService;
                this.val$conn = serviceConnection;
            }

            public void run() {
                try {
                    boolean needRestore = false;
                    Iterator it = this.val$service.getPurchases(3, StoreActivity.this.getPackageName(), "inapp", null).getStringArrayList("INAPP_PURCHASE_DATA_LIST").iterator();
                    while (it.hasNext()) {
                        String d = (String) it.next();
                        Log.m528i("vk", d);
                        JSONObject o = new JSONObject(d);
                        String dp = o.getString("developerPayload");
                        String token = o.getString("purchaseToken");
                        String orderId = o.getString("orderId");
                        if (Integer.parseInt(dp.split(",")[0]) == Global.uid) {
                            needRestore = true;
                        }
                    }
                    if (needRestore) {
                        Intent intent = new Intent(StoreActivity.this, BillingActivity.class);
                        intent.putExtra("restore", true);
                        intent.putExtra("cancelable", true);
                        StoreActivity.this.startActivityForResult(intent, SuggestionsFriendsFragment.GPLUS_ERROR_RESULT);
                    }
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                }
                StoreActivity.this.unbindService(this.val$conn);
            }
        }

        C05197() {
        }

        public void onServiceDisconnected(ComponentName name) {
        }

        public void onServiceConnected(ComponentName name, IBinder binder) {
            APIController.runInBg(new C05181(Stub.asInterface(binder), this));
        }
    }

    private class StoreAdapter extends BaseAdapter {
        private StoreAdapter() {
        }

        public int getCount() {
            return StoreActivity.this.packs.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View view, ViewGroup parent) {
            if (view == null) {
                view = View.inflate(StoreActivity.this, C0436R.layout.store_item, null);
                view.findViewById(C0436R.id.sticker_button).setOnClickListener(StoreActivity.this.btnClickListener);
                view.findViewById(C0436R.id.sticker_error).setOnClickListener(StoreActivity.this.btnClickListener);
                CircularProgressDrawable pd = new CircularProgressDrawable();
                pd.setColors(-986896, -4210753);
                pd.setThickness(2);
                pd.setPad(false);
                pd.setDimBackground(false);
                ((ProgressBar) view.findViewById(C0436R.id.sticker_progress)).setProgressDrawable(pd);
            }
            StickerPack pack = (StickerPack) StoreActivity.this.packs.get(position);
            ((TextView) view.findViewById(C0436R.id.sticker_title)).setText(pack.title);
            if (pack.isNew) {
                Drawable d = StoreActivity.this.getResources().getDrawable(C0436R.drawable.ic_stickers_list_new);
                d.setBounds(0, 0, Global.scale(7.0f), Global.scale(7.0f));
                ((TextView) view.findViewById(C0436R.id.sticker_title)).setCompoundDrawables(d, null, null, null);
                ((TextView) view.findViewById(C0436R.id.sticker_title)).setCompoundDrawablePadding(Global.scale(8.0f));
            } else {
                ((TextView) view.findViewById(C0436R.id.sticker_title)).setCompoundDrawables(null, null, null, null);
            }
            ((TextView) view.findViewById(C0436R.id.sticker_subtitle)).setText(pack.author);
            if (StickerDownloaderService.currentInstance != null && (StickerDownloaderService.currentInstance.getCurrentPackId() == pack.id || StickerDownloaderService.currentInstance.isInQueue(pack.id))) {
                view.findViewById(C0436R.id.sticker_button).setVisibility(8);
                view.findViewById(C0436R.id.sticker_ok).setVisibility(8);
                view.findViewById(C0436R.id.sticker_progress).setVisibility(0);
                view.findViewById(C0436R.id.sticker_error).setVisibility(8);
                if (StickerDownloaderService.currentInstance.isInQueue(pack.id)) {
                    ((ProgressBar) view.findViewById(C0436R.id.sticker_progress)).setProgress(0);
                } else {
                    ((ProgressBar) view.findViewById(C0436R.id.sticker_progress)).setProgress(Math.round(StickerDownloaderService.currentInstance.getCurrentProgress() * 100.0f));
                }
            } else if (pack.state == 4) {
                view.findViewById(C0436R.id.sticker_button).setVisibility(8);
                view.findViewById(C0436R.id.sticker_progress).setVisibility(8);
                view.findViewById(C0436R.id.sticker_ok).setVisibility(0);
                view.findViewById(C0436R.id.sticker_error).setVisibility(8);
            } else if (pack.state == 3 || pack.state == 5) {
                ((TextView) view.findViewById(C0436R.id.sticker_button)).setText(C0436R.string.download);
                view.findViewById(C0436R.id.sticker_ok).setVisibility(8);
                view.findViewById(C0436R.id.sticker_progress).setVisibility(8);
                view.findViewById(C0436R.id.sticker_button).setVisibility(0);
                view.findViewById(C0436R.id.sticker_error).setVisibility(8);
            } else if (pack.state == 0 || pack.state == 7) {
                ((TextView) view.findViewById(C0436R.id.sticker_button)).setText(pack.price != null ? pack.price : StoreActivity.this.getString(C0436R.string.price_free));
                view.findViewById(C0436R.id.sticker_ok).setVisibility(8);
                view.findViewById(C0436R.id.sticker_progress).setVisibility(8);
                view.findViewById(C0436R.id.sticker_button).setVisibility(0);
                view.findViewById(C0436R.id.sticker_error).setVisibility(8);
                if (pack.state == 7) {
                    ((TextView) view.findViewById(C0436R.id.sticker_button)).setText(C0436R.string.unavailable);
                    view.findViewById(C0436R.id.sticker_button).setEnabled(false);
                    view.findViewById(C0436R.id.sticker_button).getBackground().setAlpha(TransportMediator.FLAG_KEY_MEDIA_NEXT);
                } else {
                    view.findViewById(C0436R.id.sticker_button).setEnabled(true);
                    view.findViewById(C0436R.id.sticker_button).getBackground().setAlpha(MotionEventCompat.ACTION_MASK);
                }
            } else {
                view.findViewById(C0436R.id.sticker_ok).setVisibility(8);
                view.findViewById(C0436R.id.sticker_progress).setVisibility(8);
                view.findViewById(C0436R.id.sticker_button).setVisibility(8);
                view.findViewById(C0436R.id.sticker_error).setVisibility(0);
            }
            view.findViewById(C0436R.id.sticker_button).setTag(Integer.valueOf(position));
            view.findViewById(C0436R.id.sticker_error).setTag(Integer.valueOf(position));
            if (StoreActivity.this.imgLoader.isAlreadyLoaded(pack.thumb)) {
                ((ImageView) view.findViewById(C0436R.id.sticker_thumb)).setImageBitmap(StoreActivity.this.imgLoader.get(pack.thumb));
            } else {
                ((ImageView) view.findViewById(C0436R.id.sticker_thumb)).setImageDrawable(new ColorDrawable(0));
            }
            if (getCount() == 1) {
                view.setBackgroundResource(C0436R.drawable.bg_post);
            } else if (position == 0) {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_top);
            } else if (position == StoreActivity.this.packs.size() - 1) {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_btm);
            } else {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_mid);
            }
            return view;
        }
    }

    /* renamed from: com.vkontakte.android.StoreActivity.5 */
    class C13635 implements Callback {
        C13635() {
        }

        public void success(List<StickerPack> res) {
            StoreActivity.this.packs.clear();
            StoreActivity.this.packs.addAll(res);
            boolean changedState = false;
            Iterator it = StoreActivity.this.packs.iterator();
            while (it.hasNext()) {
                StickerPack pck = (StickerPack) it.next();
                int state = Stickers.getPackState(pck.id);
                Log.m528i("vk", "local " + state + ", api " + pck.state);
                if (state == 4 || state == pck.state) {
                    pck.state = state;
                } else if (pck.state == 3) {
                    StoreActivity.this.getSharedPreferences("stickers", 0).edit().putBoolean("owned" + pck.id, true).putBoolean("confirmed" + pck.id, true).putString("content" + pck.id, pck.downloadLink).putString("base_url" + pck.id, pck.baseURL).putString("s_base_url" + pck.id, pck.stickersBaseURL).putString("ordering" + pck.id, TextUtils.join(",", pck.ids)).commit();
                    changedState = true;
                } else {
                    pck.state = state;
                }
            }
            if (changedState) {
                Stickers.broadcastUpdate();
            }
            StoreActivity.this.updateList();
            StoreActivity.this.getPrices();
            if (Stickers.hasNewStockItems()) {
                StoreActivity.this.getSharedPreferences("stickers", 0).edit().putBoolean("has_new", false).commit();
                Stickers.broadcastUpdate();
            }
        }

        public void fail(int ecode, String emsg) {
            StoreActivity.this.error.setErrorInfo(ecode, emsg);
            Global.showViewAnimated(StoreActivity.this.error, true, PhotoView.THUMB_ANIM_DURATION);
            Global.showViewAnimated(StoreActivity.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
        }
    }

    private class StickerThumbsAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.StoreActivity.StickerThumbsAdapter.1 */
        class C05201 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C05201(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.sticker_thumb);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private StickerThumbsAdapter() {
        }

        public int getItemCount() {
            return StoreActivity.this.adapter.getCount();
        }

        public int getImageCountForItem(int item) {
            return 1;
        }

        public String getImageURL(int item, int image) {
            return ((StickerPack) StoreActivity.this.packs.get(item)).thumb;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += StoreActivity.this.list.getHeaderViewsCount();
            if (item >= StoreActivity.this.list.getFirstVisiblePosition() && item <= StoreActivity.this.list.getLastVisiblePosition()) {
                StoreActivity.this.runOnUiThread(new C05201(StoreActivity.this.list.getChildAt(item - StoreActivity.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    public StoreActivity() {
        this.packs = new ArrayList();
        this.btnClickListener = new C05101();
        this.receiver = new C05112();
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        this.content = new FrameLayout(this);
        this.content.setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        this.list = new ListView(this);
        this.list.setVisibility(8);
        ListView listView = this.list;
        ListAdapter storeAdapter = new StoreAdapter();
        this.adapter = storeAdapter;
        listView.setAdapter(storeAdapter);
        this.list.setDividerHeight(0);
        this.list.setSelector(new ColorDrawable(0));
        this.list.setOnItemClickListener(new C05123());
        this.content.addView(this.list);
        this.progress = new ProgressBar(this);
        this.content.addView(this.progress, new LayoutParams(Global.scale(40.0f), Global.scale(40.0f), 17));
        this.error = (ErrorView) View.inflate(this, C0436R.layout.error, null);
        this.error.setVisibility(8);
        this.error.setOnRetryListener(new C05134());
        this.content.addView(this.error);
        setContentView(this.content);
        this.imgLoader = new ListImageLoaderWrapper(new StickerThumbsAdapter(), this.list, null);
        loadData();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Stickers.ACTION_STICKERS_UPDATED);
        filter.addAction(Stickers.ACTION_STICKERS_DOWNLOAD_PROGRESS);
        registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
        checkIncompleteTransactions();
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(C0436R.menu.store, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0436R.id.settings) {
            startActivity(new Intent(this, StickerManagerActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private void downloadPack(StickerPack pck) {
        Intent intent = new Intent(this, StickerDownloaderService.class);
        intent.putExtra("id", pck.id);
        intent.putExtra(PlusShare.KEY_CALL_TO_ACTION_URL, getSharedPreferences("stickers", 0).getString("content" + pck.id, ACRAConstants.DEFAULT_STRING_VALUE));
        intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, pck.title);
        startService(intent);
    }

    private void updateList() {
        this.adapter.notifyDataSetChanged();
        this.imgLoader.updateImages();
    }

    private void loadData() {
        new StoreGetInventory().setCallback(new C13635()).exec((Activity) this);
    }

    private void getPrices() {
        HashSet<String> items = new HashSet();
        Iterator it = this.packs.iterator();
        while (it.hasNext()) {
            StickerPack pack = (StickerPack) it.next();
            if (pack.storeID != null && pack.storeID.length() > 0) {
                items.add(pack.storeID);
            }
        }
        Log.m528i("vk", "Get prices for " + items);
        if (items.size() == 0) {
            Global.showViewAnimated(this.list, true, PhotoView.THUMB_ANIM_DURATION);
            Global.showViewAnimated(this.progress, false, PhotoView.THUMB_ANIM_DURATION);
            return;
        }
        bindService(new Intent("com.android.vending.billing.InAppBillingService.BIND"), new C05176(items), 1);
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        Iterator it;
        StickerPack pck;
        super.onActivityResult(reqCode, resCode, data);
        if (reqCode == 101 && resCode == -1) {
            if (data != null) {
                int id = data.getIntExtra("product", 0);
                it = this.packs.iterator();
                while (it.hasNext()) {
                    pck = (StickerPack) it.next();
                    if (pck.id == id) {
                        pck.state = Stickers.getPackState(id);
                        if (pck.state == 3) {
                            Intent intent = new Intent(this, StickerDownloaderService.class);
                            intent.putExtra("id", pck.id);
                            intent.putExtra(PlusShare.KEY_CALL_TO_ACTION_URL, getSharedPreferences("stickers", 0).getString("content" + pck.id, ACRAConstants.DEFAULT_STRING_VALUE));
                            intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, pck.title);
                            startService(intent);
                        }
                        updateList();
                        Stickers.broadcastUpdate();
                        if (data.hasExtra(LongPollService.EXTRA_MESSAGE)) {
                            new Builder(this).setTitle(C0436R.string.error).setMessage(data.getStringExtra(LongPollService.EXTRA_MESSAGE)).setPositiveButton(C0436R.string.ok, null).show();
                        }
                    }
                }
                Stickers.broadcastUpdate();
                if (data.hasExtra(LongPollService.EXTRA_MESSAGE)) {
                    new Builder(this).setTitle(C0436R.string.error).setMessage(data.getStringExtra(LongPollService.EXTRA_MESSAGE)).setPositiveButton(C0436R.string.ok, null).show();
                }
            } else {
                return;
            }
        }
        if (reqCode == SuggestionsFriendsFragment.GPLUS_ERROR_RESULT) {
            it = this.packs.iterator();
            while (it.hasNext()) {
                pck = (StickerPack) it.next();
                pck.state = Stickers.getPackState(pck.id);
            }
            updateList();
        }
    }

    private void checkIncompleteTransactions() {
        bindService(new Intent("com.android.vending.billing.InAppBillingService.BIND"), new C05197(), 1);
    }
}
