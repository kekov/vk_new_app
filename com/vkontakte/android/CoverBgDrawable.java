package com.vkontakte.android;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.view.MotionEventCompat;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;

public class CoverBgDrawable extends Drawable {
    private long animDuration;
    private long animStart;
    private Drawable bg;
    private Bitmap bmp;
    private Paint paint;

    public CoverBgDrawable(Bitmap b) {
        this.animDuration = 1000;
        if (b != null) {
            int[] pixels = new int[(b.getWidth() * b.getHeight())];
            b.getPixels(pixels, 0, b.getWidth(), 0, 0, b.getWidth(), b.getHeight());
            for (int i = 0; i < pixels.length; i++) {
                pixels[i] = ((pixels[i] >> 2) & 4144959) | Color.WINDOW_BACKGROUND_COLOR;
            }
            this.bmp = Bitmap.createBitmap(pixels, b.getWidth(), b.getHeight(), Config.ARGB_8888);
            this.paint = new Paint();
            this.paint.setFilterBitmap(true);
        }
    }

    public void draw(Canvas c) {
        if (this.bmp != null) {
            Rect bnds = copyBounds();
            c.save();
            c.clipRect(bnds);
            Rect dst = new Rect();
            if (bnds.height() > bnds.width()) {
                dst.top = bnds.top;
                dst.bottom = bnds.bottom;
                dst.left = bnds.left - ((bnds.height() - bnds.width()) / 2);
                dst.right = bnds.right + ((bnds.height() - bnds.width()) / 2);
            } else {
                dst.left = bnds.left;
                dst.right = bnds.right;
                dst.top = bnds.top - ((bnds.width() - bnds.height()) / 2);
                dst.bottom = bnds.bottom + ((bnds.width() - bnds.height()) / 2);
            }
            if (System.currentTimeMillis() < this.animStart + this.animDuration) {
                this.paint.setAlpha((int) (255.0f * (((float) (System.currentTimeMillis() - this.animStart)) / ((float) this.animDuration))));
                invalidateSelf();
                this.bg.setBounds(copyBounds());
                this.bg.draw(c);
            } else {
                this.paint.setAlpha(MotionEventCompat.ACTION_MASK);
                this.bg = null;
            }
            c.drawBitmap(this.bmp, null, dst, this.paint);
            c.restore();
        }
    }

    public Bitmap getBitmap() {
        return this.bmp;
    }

    public void fadeIn(Drawable from) {
        if (from == null) {
            this.bg = VKApplication.context.getResources().getDrawable(C0436R.drawable.bg_audio_player);
        } else {
            this.bg = from;
        }
        this.animStart = System.currentTimeMillis();
        invalidateSelf();
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int arg0) {
    }

    public void setColorFilter(ColorFilter cf) {
    }
}
