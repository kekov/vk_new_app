package com.vkontakte.android;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.RemoteViews;
import com.vkontakte.android.cache.AlbumArtRetriever;
import org.acra.ACRAConstants;

public class PlayerBigWidget extends AppWidgetProvider {
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        update(context, appWidgetManager);
    }

    public static void update(Context context, AppWidgetManager awm) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), C0436R.layout.widget_player_big);
        Intent playpause = new Intent(context, AudioPlayerService.class);
        playpause.setAction("PlayPause");
        playpause.putExtra("action", 3);
        PendingIntent pendingPlaypause = PendingIntent.getService(context, 0, playpause, 0);
        Intent nextTrack = new Intent(context, AudioPlayerService.class);
        nextTrack.setAction("Next");
        nextTrack.putExtra("action", 5);
        PendingIntent pendingNextTrack = PendingIntent.getService(context, 0, nextTrack, 0);
        Intent intent = new Intent(context, AudioPlayerService.class);
        intent.setAction("Prev");
        intent.putExtra("action", 6);
        PendingIntent pendingPrevTrack = PendingIntent.getService(context, 0, intent, 0);
        intent = new Intent(context, AudioPlayerService.class);
        intent.setAction("Shuffle");
        intent.putExtra("action", 10);
        PendingIntent pendingShuffle = PendingIntent.getService(context, 0, intent, 0);
        intent = new Intent(context, AudioPlayerService.class);
        intent.setAction("Repeat");
        intent.putExtra("action", 9);
        PendingIntent pendingRepeat = PendingIntent.getService(context, 0, intent, 0);
        intent = new Intent(context, AudioPlayerService.class);
        intent.setAction("Show");
        intent.putExtra("action", 4);
        intent.putExtra("from_notify", true);
        PendingIntent pendingShowPlayer = PendingIntent.getService(context, 0, intent, 0);
        Intent openApp = new Intent(context, FragmentWrapperActivity.class);
        openApp.setAction("fdsafsafdsafs");
        openApp.putExtra("class", "AudioListFragment");
        openApp.putExtra("args", new Bundle());
        PendingIntent appPending = PendingIntent.getActivity(context, 0, openApp, 0);
        if (AudioPlayerService.sharedInstance == null || AudioPlayerService.sharedInstance.getCurrentFile() == null) {
            remoteViews.setOnClickPendingIntent(C0436R.id.w_player_clickbox, appPending);
            remoteViews.setTextViewText(C0436R.id.w_player_title, ACRAConstants.DEFAULT_STRING_VALUE);
            remoteViews.setTextViewText(C0436R.id.w_player_artist, ACRAConstants.DEFAULT_STRING_VALUE);
            remoteViews.setInt(C0436R.id.w_player_play_pause, "setBackgroundColor", 0);
            remoteViews.setInt(C0436R.id.w_player_next, "setBackgroundColor", 0);
            remoteViews.setInt(C0436R.id.w_player_prev, "setBackgroundColor", 0);
            remoteViews.setInt(C0436R.id.w_player_shuffle, "setBackgroundColor", 0);
            remoteViews.setInt(C0436R.id.w_player_repeat, "setBackgroundColor", 0);
            remoteViews.setViewVisibility(C0436R.id.w_player_cover, 8);
            remoteViews.setViewVisibility(C0436R.id.w_player_placeholder, 0);
            remoteViews.setViewVisibility(C0436R.id.w_player_inactive_view, 0);
        } else {
            AudioFile f = AudioPlayerService.sharedInstance.getCurrentFile();
            if (f != null) {
                remoteViews.setOnClickPendingIntent(C0436R.id.w_player_play_pause, pendingPlaypause);
                remoteViews.setOnClickPendingIntent(C0436R.id.w_player_next, pendingNextTrack);
                remoteViews.setOnClickPendingIntent(C0436R.id.w_player_prev, pendingPrevTrack);
                remoteViews.setOnClickPendingIntent(C0436R.id.w_player_shuffle, pendingShuffle);
                remoteViews.setOnClickPendingIntent(C0436R.id.w_player_repeat, pendingRepeat);
                remoteViews.setOnClickPendingIntent(C0436R.id.w_player_clickbox, pendingShowPlayer);
                remoteViews.setTextViewText(C0436R.id.w_player_artist, f.artist);
                remoteViews.setTextViewText(C0436R.id.w_player_title, f.title);
                Bitmap cover = AlbumArtRetriever.getCoverImage(f.aid, f.oid, 0);
                if (cover != null) {
                    remoteViews.setImageViewBitmap(C0436R.id.w_player_cover, cover);
                    remoteViews.setViewVisibility(C0436R.id.w_player_cover, 0);
                    remoteViews.setViewVisibility(C0436R.id.w_player_placeholder, 8);
                } else {
                    remoteViews.setViewVisibility(C0436R.id.w_player_cover, 8);
                    remoteViews.setViewVisibility(C0436R.id.w_player_placeholder, 0);
                }
            }
            remoteViews.setBoolean(C0436R.id.w_player_artist, "setSingleLine", true);
            remoteViews.setImageViewResource(C0436R.id.w_player_play_pause, AudioPlayerService.sharedInstance.isPlaying() ? C0436R.drawable.ic_awidget_pause : C0436R.drawable.ic_awidget_play);
            remoteViews.setImageViewResource(C0436R.id.w_player_shuffle, AudioPlayerService.sharedInstance.isRandom() ? C0436R.drawable.ic_aplayer_shuffle_active : C0436R.drawable.ic_aplayer_shuffle_normal);
            remoteViews.setImageViewResource(C0436R.id.w_player_repeat, AudioPlayerService.sharedInstance.isLoop() ? C0436R.drawable.ic_aplayer_repeat_active : C0436R.drawable.ic_aplayer_repeat_normal);
            remoteViews.setViewVisibility(C0436R.id.w_player_inactive_view, 8);
            remoteViews.setInt(C0436R.id.w_player_play_pause, "setBackgroundResource", C0436R.drawable.highlight);
            remoteViews.setInt(C0436R.id.w_player_next, "setBackgroundResource", C0436R.drawable.highlight);
            remoteViews.setInt(C0436R.id.w_player_prev, "setBackgroundResource", C0436R.drawable.highlight);
            remoteViews.setInt(C0436R.id.w_player_shuffle, "setBackgroundResource", C0436R.drawable.highlight);
            remoteViews.setInt(C0436R.id.w_player_repeat, "setBackgroundResource", C0436R.drawable.highlight);
        }
        awm.updateAppWidget(new ComponentName(context, PlayerBigWidget.class), remoteViews);
    }
}
