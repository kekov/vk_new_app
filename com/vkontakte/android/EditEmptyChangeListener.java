package com.vkontakte.android;

import android.text.Editable;
import android.text.TextWatcher;

public abstract class EditEmptyChangeListener implements TextWatcher {
    private boolean isEmpty;

    public abstract void onEmptyChanged(boolean z);

    public EditEmptyChangeListener() {
        this.isEmpty = true;
    }

    public void afterTextChanged(Editable s) {
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        boolean e = s.length() == 0;
        if (e != this.isEmpty) {
            this.isEmpty = e;
            onEmptyChanged(e);
        }
    }
}
