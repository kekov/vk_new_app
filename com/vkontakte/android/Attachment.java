package com.vkontakte.android;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.media.TransportMediator;
import android.support.v4.widget.ExploreByTouchHelper;
import android.view.View;
import android.widget.ImageView;
import com.facebook.WebDialog;
import com.google.android.gcm.GCMConstants;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.PhotoAttachment.FixedSizeImageView;
import com.vkontakte.android.PhotoAttachment.Image;
import com.vkontakte.android.api.Document;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.VideoFile;
import com.vkontakte.android.api.WallGetById;
import com.vkontakte.android.api.WallGetById.Callback;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.providers.LangProvider.StringKeys;
import com.vkontakte.android.mediapicker.providers.ResourceProvider.Views;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import com.vkontakte.android.ui.PendingPhotoAttachment;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import net.hockeyapp.android.Strings;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class Attachment implements Parcelable {
    private static HashMap<String, ArrayList<SoftReference<View>>> reusableViews;

    /* renamed from: com.vkontakte.android.Attachment.1 */
    class C12651 implements Callback {
        private final /* synthetic */ NewsEntry[] val$post;

        C12651(NewsEntry[] newsEntryArr) {
            this.val$post = newsEntryArr;
        }

        public void success(NewsEntry[] news) {
            this.val$post[0] = news[0];
        }

        public void fail(int ecode, String emsg) {
        }
    }

    /* renamed from: com.vkontakte.android.Attachment.2 */
    class C12662 implements Callback {
        private final /* synthetic */ NewsEntry[] val$post;

        C12662(NewsEntry[] newsEntryArr) {
            this.val$post = newsEntryArr;
        }

        public void success(NewsEntry[] news) {
            this.val$post[0] = news[0];
        }

        public void fail(int ecode, String emsg) {
        }
    }

    public abstract View getFullView(Context context);

    public abstract View getViewForList(Context context, View view);

    public abstract LayoutParams getViewLayoutParams();

    public abstract void serialize(DataOutputStream dataOutputStream) throws IOException;

    static {
        reusableViews = new HashMap();
    }

    public static Attachment deserialize(DataInputStream is, int type) throws IOException {
        int nImgs;
        Image[] imgs;
        int i;
        switch (type) {
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                nImgs = is.readInt();
                imgs = new Image[nImgs];
                for (i = 0; i < nImgs; i++) {
                    imgs[i] = new Image(is.readChar(), is.readUTF(), is.readInt(), is.readInt());
                }
                return new PhotoAttachment(imgs, is.readInt(), is.readInt(), is.readInt(), is.readUTF(), is.readInt(), is.readInt(), is.readUTF());
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return is.readInt() == 0 ? new VideoAttachment(is.readUTF(), is.readUTF(), is.readInt(), is.readInt(), is.readInt(), is.readUTF()) : new VideoAttachment(VideoFile.createFromStream(is));
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                return new AudioAttachment(is.readUTF(), is.readUTF(), is.readInt(), is.readInt(), is.readInt());
            case UserListView.TYPE_FAVE /*4*/:
                return new PollAttachment(is.readUTF(), is.readInt(), is.readInt());
            case UserListView.TYPE_FOLLOWERS /*5*/:
                return new LinkAttachment(is.readUTF(), is.readUTF(), is.readUTF());
            case UserListView.TYPE_FAVE_LINKS /*7*/:
                return new NoteAttachment(is.readUTF(), is.readInt(), is.readInt());
            case UserListView.TYPE_BLACKLIST /*8*/:
                return new PhotoAttachment(is.readUTF(), null, -1, -1, -1);
            case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                return new DocumentAttachment(is.readUTF(), is.readUTF(), is.readInt(), is.readUTF(), is.readInt(), is.readInt());
            case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                return new GeoAttachment(is.readDouble(), is.readDouble(), is.readUTF(), is.readUTF(), is.readInt(), is.readUTF());
            case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                return new WikiAttachment(is.readUTF(), is.readUTF(), is.readInt(), is.readInt());
            case Strings.UPDATE_DIALOG_POSITIVE_BUTTON_ID /*12*/:
                return new PostAttachment(new NewsEntry(is));
            case Strings.EXPIRY_INFO_TITLE_ID /*13*/:
                nImgs = is.readInt();
                imgs = new Image[nImgs];
                for (i = 0; i < nImgs; i++) {
                    imgs[i] = new Image(is.readChar(), is.readUTF(), is.readInt(), is.readInt());
                }
                return new AlbumAttachment(imgs, is.readInt(), is.readInt(), is.readInt(), is.readUTF(), is.readInt());
            case Strings.EXPIRY_INFO_TEXT_ID /*14*/:
                return new PendingPhotoAttachment(is.readUTF(), is.readInt());
            case Strings.FEEDBACK_FAILED_TITLE_ID /*15*/:
                return new SignatureLinkAttachment(is.readUTF(), is.readUTF());
            case TransportMediator.FLAG_KEY_MEDIA_PAUSE /*16*/:
                Attachment sa = new StickerAttachment();
                sa.id = is.readInt();
                sa.images = new String[is.readInt()];
                for (i = 0; i < sa.images.length; i++) {
                    sa.images[i] = is.readUTF();
                }
                sa.width = is.readInt();
                sa.height = is.readInt();
                return sa;
            case StringKeys.SELECT_ALBUM /*17*/:
                return new RepostAttachment(is.readInt(), is.readInt(), is.readInt(), is.readUTF(), is.readUTF(), is.readInt());
            default:
                return null;
        }
    }

    public static Attachment parse(JSONObject o) {
        try {
            String type = o.getString(WebDialog.DIALOG_PARAM_TYPE);
            JSONObject att = o.getJSONObject(type);
            if (!type.equals("photo")) {
                Attachment pa;
                if (type.equals("graffiti")) {
                    pa = new PhotoAttachment(att.getString("photo_200"), att.getString("photo_586"), att.getInt("owner_id"), att.getInt("id"), (int) ExploreByTouchHelper.INVALID_ID);
                    ((Image) pa.images.get("m")).width = 200;
                    ((Image) pa.images.get("m")).height = 100;
                    ((Image) pa.images.get("x")).width = 586;
                    ((Image) pa.images.get("x")).height = Views.COMPLETE_BUTTON_BIG_ICON;
                    return pa;
                }
                if (type.equals("posted_photo")) {
                    pa = new PhotoAttachment(att.getString("photo_130"), att.getString("photo_604"), att.getInt("owner_id"), att.getInt("id"), (int) ExploreByTouchHelper.INVALID_ID);
                    ((Image) pa.images.get("m")).width = TransportMediator.KEYCODE_MEDIA_RECORD;
                    ((Image) pa.images.get("m")).height = 98;
                    ((Image) pa.images.get("x")).width = 604;
                    ((Image) pa.images.get("x")).height = 480;
                    return pa;
                }
                int i;
                if (type.equals("album")) {
                    JSONArray sizes = att.getJSONObject("thumb").optJSONArray("sizes");
                    if (sizes != null) {
                        ArrayList<Image> photos = new ArrayList();
                        for (i = 0; i < sizes.length(); i++) {
                            JSONObject so = sizes.getJSONObject(i);
                            photos.add(new Image(so.optString(WebDialog.DIALOG_PARAM_TYPE, "?").charAt(0), so.getString("src"), so.getInt("width"), so.getInt("height")));
                        }
                        return new AlbumAttachment((Image[]) photos.toArray(new Image[0]), att.getInt("owner_id"), att.optInt("pid", -1), att.optInt("id", -7), att.optString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE), att.getInt("size"));
                    }
                    return new AlbumAttachment(new Image[]{new Image('m', att.getJSONObject("thumb").getString("src"), 0, 0), new Image('x', att.getJSONObject("thumb").getString("src_big"), 0, 0), new Image('y', att.getJSONObject("thumb").optString("src_xbig", null), 0, 0)}, att.getInt("owner_id"), att.optInt("pid", -att.optInt("gid")), att.optInt("aid"), att.optString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE), att.getInt("size"));
                }
                if (type.equals(GCMConstants.EXTRA_APPLICATION_PENDING_INTENT)) {
                    return new PhotoAttachment(att.getString("src"), null, -1, -1, -1);
                }
                if (type.equals("audio")) {
                    return new AudioAttachment(new AudioFile(att));
                }
                if (type.equals("video")) {
                    return new VideoAttachment(new VideoFile(att));
                }
                if (type.equals("doc")) {
                    return new DocumentAttachment(new Document(att));
                }
                if (type.equals("gift")) {
                    return new DocumentAttachment(VKApplication.context.getResources().getString(C0436R.string.gift), null, 0, att.getString("thumb_256"), -1, -1);
                }
                if (type.equals("link")) {
                    return new LinkAttachment(att.getString(PlusShare.KEY_CALL_TO_ACTION_URL), att.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE), att.optString("preview_page", ACRAConstants.DEFAULT_STRING_VALUE));
                }
                if (type.equals("poll")) {
                    return new PollAttachment(att.getString("question"), att.getInt("owner_id"), att.getInt("id"));
                }
                if (type.equals("note")) {
                    return new NoteAttachment(att.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE), att.getInt("owner_id"), att.getInt("id"));
                }
                if (type.equals("page")) {
                    return new WikiAttachment(att.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE), att.optString("section"), -att.optInt("group_id"), att.optInt("page_id"));
                }
                NewsEntry[] post;
                if (!type.equals("wall")) {
                    if (type.equals("sticker")) {
                        return new StickerAttachment(att.getInt("id"), new String[]{att.getString("photo_64"), att.getString("photo_128"), att.getString("photo_256")}, att.getInt("width"), att.getInt("height"));
                    }
                    if (type.equals("wall_reply")) {
                        post = new NewsEntry[1];
                        new WallGetById(new String[]{att.getInt("owner_id") + "_" + att.getInt("post_id")}).setCallback(new C12662(post)).execSync();
                        return post[0] != null ? new PostAttachment(post[0]) : null;
                    }
                    Log.m530w("vk", "Unknown attachment " + o);
                    return null;
                } else if (att.has("copy_post_id") || att.has("copy_history")) {
                    post = new NewsEntry[1];
                    new WallGetById(new String[]{att.getInt("to_id") + "_" + att.getInt("id")}).setCallback(new C12651(post)).execSync();
                    return new PostAttachment(post[0]);
                } else {
                    int uid;
                    HashMap<Integer, String> names = new HashMap();
                    HashMap<Integer, String> photos2 = new HashMap();
                    JSONObject users1 = att.optJSONObject("from");
                    JSONObject users2 = att.optJSONObject("copy_owner");
                    if (users1 != null) {
                        for (i = 0; i < users1.length(); i++) {
                            if (users1.has("id")) {
                                uid = users1.getInt("id");
                                if (users1.has("first_name")) {
                                    String str;
                                    names.put(Integer.valueOf(uid), users1.getString("first_name") + " " + users1.getString("last_name"));
                                    Integer valueOf = Integer.valueOf(uid);
                                    if (Global.displayDensity > 1.0f) {
                                        str = "photo_100";
                                    } else {
                                        str = "photo_50";
                                    }
                                    photos2.put(valueOf, users1.optString(str, ACRAConstants.DEFAULT_STRING_VALUE));
                                } else {
                                    names.put(Integer.valueOf(-uid), users1.getString("name"));
                                    photos2.put(Integer.valueOf(-uid), users1.optString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50", ACRAConstants.DEFAULT_STRING_VALUE));
                                }
                            } else {
                                if (users1.has("uid")) {
                                    uid = users1.getInt("uid");
                                    names.put(Integer.valueOf(uid), users1.getString("first_name") + " " + users1.getString("last_name"));
                                    photos2.put(Integer.valueOf(uid), users1.optString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec", ACRAConstants.DEFAULT_STRING_VALUE));
                                } else {
                                    uid = -users1.getInt("gid");
                                    names.put(Integer.valueOf(uid), users1.getString("name"));
                                    photos2.put(Integer.valueOf(uid), users1.optString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50", ACRAConstants.DEFAULT_STRING_VALUE));
                                }
                            }
                        }
                    }
                    if (users2 != null) {
                        for (i = 0; i < users2.length(); i++) {
                            if (users2.has("id")) {
                                uid = users2.getInt("id");
                                String ph = users2.optString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                                if (ph == null) {
                                    ph = users2.getString(Global.displayDensity > 1.0f ? "photo_100" : "photo_50");
                                }
                                if (users2.has("first_name")) {
                                    names.put(Integer.valueOf(uid), users2.getString("first_name") + " " + users2.getString("last_name"));
                                    photos2.put(Integer.valueOf(uid), ph);
                                } else {
                                    names.put(Integer.valueOf(-uid), users2.getString("name"));
                                    photos2.put(Integer.valueOf(-uid), ph);
                                }
                            } else {
                                uid = -users2.getInt("gid");
                                names.put(Integer.valueOf(uid), users2.getString("name"));
                                photos2.put(Integer.valueOf(uid), users2.getString(Global.displayDensity > 1.0f ? "photo" : "photo_medium"));
                            }
                        }
                    }
                    return new PostAttachment(new NewsEntry(att, names, photos2));
                }
            } else if (att.optJSONArray("sizes") != null) {
                return new PhotoAttachment(new Photo(att));
            } else {
                return new PhotoAttachment(new Image[]{new Image('m', att.getString("src"), 0, 0), new Image('x', att.getString("src_big"), 0, 0), new Image('y', att.optString("src_xbig", null), 0, 0)}, att.getInt("owner_id"), att.optInt("id", -att.optInt("gid")), att.optInt("aid", -7), att.optString("text"), att.optInt("user_id", att.getInt("owner_id")), att.optInt("created"), att.optString("access_key", ACRAConstants.DEFAULT_STRING_VALUE));
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    public static GeoAttachment parseGeo(JSONObject j) throws Exception {
        String[] c2 = j.getString("coordinates").split(" ");
        double lat = Double.parseDouble(c2[0]);
        double lon = Double.parseDouble(c2[1]);
        if (!j.has("place")) {
            return new GeoAttachment(lat, lon, null, null, -1, null);
        }
        GeoPlace place = new GeoPlace(j.getJSONObject("place"));
        return new GeoAttachment(lat, lon, place.title, place.address, place.id, place.photo);
    }

    public static View getReusableView(Context context, String type) {
        long t = System.currentTimeMillis();
        if (!reusableViews.containsKey(type)) {
            reusableViews.put(type, new ArrayList());
        }
        Iterator<SoftReference<View>> it = ((ArrayList) reusableViews.get(type)).iterator();
        while (it.hasNext()) {
            if (((SoftReference) it.next()).get() == null) {
                it.remove();
            }
        }
        if (((ArrayList) reusableViews.get(type)).size() > 0) {
            return (View) ((SoftReference) ((ArrayList) reusableViews.get(type)).remove(0)).get();
        }
        View v;
        if ("common".equals(type)) {
            v = View.inflate(context, C0436R.layout.post_attach_common, null);
            v.setTag(type);
            return v;
        } else if ("signature".equals(type)) {
            v = View.inflate(context, C0436R.layout.post_attach_signature, null);
            v.setTag(type);
            return v;
        } else if ("audio".equals(type)) {
            v = new AudioAttachView(context);
            v.setTag(type);
            return v;
        } else if ("album".equals(type)) {
            v = View.inflate(context, C0436R.layout.attach_album, null);
            v.setTag(type);
            return v;
        } else if ("video".equals(type)) {
            v = View.inflate(context, C0436R.layout.attach_video, null);
            v.setTag(type);
            return v;
        } else if ("photo".equals(type)) {
            v = new FixedSizeImageView(context);
            v.setTag(type);
            return v;
        } else if ("doc_thumb".equals(type)) {
            DocAttachView v2 = (DocAttachView) View.inflate(context, C0436R.layout.attach_doc_thumb, null);
            v2.setTag(type);
            return v2;
        } else if (!"repost".equals(type)) {
            return null;
        } else {
            v = View.inflate(context, C0436R.layout.wall_retweet, null);
            v.setTag(type);
            return v;
        }
    }

    public static void reuseView(View view, String type) {
        if (!reusableViews.containsKey(type)) {
            reusableViews.put(type, new ArrayList());
        }
        ((ArrayList) reusableViews.get(type)).add(new SoftReference(view));
        if ("photo".equals(type)) {
            ((ImageView) view).setImageBitmap(null);
        }
        if ("doc_thumb".equals(type)) {
            ((DocAttachView) view).reset();
        }
    }

    public static void sort(ArrayList<Attachment> atts) {
        ArrayList<Attachment> photos = new ArrayList();
        ArrayList<Attachment> albums = new ArrayList();
        ArrayList<Attachment> videos = new ArrayList();
        ArrayList<Attachment> audios = new ArrayList();
        ArrayList<Attachment> others = new ArrayList();
        Iterator it = atts.iterator();
        while (it.hasNext()) {
            Attachment a = (Attachment) it.next();
            if (a instanceof AlbumAttachment) {
                albums.add(a);
            } else if (a instanceof PhotoAttachment) {
                photos.add(a);
            } else if (a instanceof VideoAttachment) {
                videos.add(a);
            } else if ((a instanceof AudioAttachment) || (a instanceof DocumentAttachment) || (a instanceof PollAttachment)) {
                audios.add(a);
            } else {
                others.add(a);
            }
        }
        atts.clear();
        atts.addAll(photos);
        atts.addAll(albums);
        atts.addAll(videos);
        atts.addAll(audios);
        atts.addAll(others);
    }
}
