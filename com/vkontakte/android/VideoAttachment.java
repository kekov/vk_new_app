package com.vkontakte.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import com.vkontakte.android.api.VideoFile;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.ui.FlowLayout;
import java.io.DataOutputStream;
import java.io.IOException;
import org.acra.ACRAConstants;

public class VideoAttachment extends Attachment implements ThumbAttachment, ImageAttachment {
    public static final Creator<VideoAttachment> CREATOR;
    public String accessKey;
    public String bigImage;
    private transient boolean breakAfter;
    private transient int displayH;
    private transient int displayW;
    public int duration;
    private transient boolean floating;
    public String image;
    public int oid;
    private transient boolean paddingAfter;
    public String title;
    VideoFile vfile;
    public int vid;

    /* renamed from: com.vkontakte.android.VideoAttachment.1 */
    class C05301 implements Creator<VideoAttachment> {
        C05301() {
        }

        public VideoAttachment createFromParcel(Parcel in) {
            if (in.readInt() == 0) {
                return new VideoAttachment(in.readString(), in.readString(), in.readInt(), in.readInt(), in.readInt(), in.readString());
            }
            return new VideoAttachment((VideoFile) VideoFile.CREATOR.createFromParcel(in));
        }

        public VideoAttachment[] newArray(int size) {
            return new VideoAttachment[size];
        }
    }

    public VideoAttachment(String _title, String _image, int _oid, int _vid, int _duration) {
        this.title = _title;
        this.image = _image;
        this.oid = _oid;
        this.vid = _vid;
        this.duration = _duration;
    }

    public VideoAttachment(String _title, String _image, int _oid, int _vid, int _duration, String _akey) {
        this.title = _title;
        this.image = _image;
        this.oid = _oid;
        this.vid = _vid;
        this.duration = _duration;
        this.accessKey = _akey;
        if (ACRAConstants.DEFAULT_STRING_VALUE.equals(_akey)) {
            this.accessKey = null;
        }
    }

    public VideoAttachment(VideoFile vf) {
        this.title = vf.title;
        this.image = vf.urlBigThumb;
        this.oid = vf.oid;
        this.vid = vf.vid;
        this.duration = vf.duration;
        this.bigImage = vf.urlBigThumb;
        this.vfile = vf;
    }

    static {
        CREATOR = new C05301();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.vfile == null ? 0 : 1);
        if (this.vfile == null) {
            dest.writeString(this.title);
            dest.writeString(this.image);
            dest.writeString(this.bigImage);
            dest.writeInt(this.oid);
            dest.writeInt(this.vid);
            dest.writeInt(this.duration);
            dest.writeString(this.accessKey);
            return;
        }
        this.vfile.writeToParcel(dest, 0);
    }

    public View getFullView(Context context) {
        return getViewForList(context, null);
    }

    public View getViewForList(Context context, View reuse) {
        return getView(context, false);
    }

    public View getView(Context context, boolean big) {
        VideoAttachView vav;
        int i;
        if (big) {
            vav = (VideoAttachView) View.inflate(context, C0436R.layout.attach_video_big, null);
        } else {
            vav = (VideoAttachView) Attachment.getReusableView(context, "video");
        }
        if (this.vfile == null) {
            vav.file = new VideoFile();
            vav.file.vid = this.vid;
            vav.file.oid = this.oid;
            vav.file.title = this.title;
            vav.file.duration = this.duration;
            vav.file.accessKey = this.accessKey;
        } else {
            vav.file = this.vfile;
        }
        vav.findViewById(C0436R.id.video_preview).setLayoutParams(new LayoutParams(this.displayW, this.displayH));
        int scale = Global.scale(ImageViewHolder.PaddingSize);
        if (this.paddingAfter) {
            i = 10;
        } else {
            i = 2;
        }
        FlowLayout.LayoutParams lp = new FlowLayout.LayoutParams(scale, Global.scale((float) i));
        lp.breakAfter = this.breakAfter;
        lp.floating = this.floating;
        lp.width = this.displayW;
        lp.height = this.displayH;
        if (this.displayW < Global.scale(100.0f) || this.displayH < Global.scale(100.0f)) {
            vav.findViewById(C0436R.id.video_play_icon).setVisibility(8);
        } else {
            vav.findViewById(C0436R.id.video_play_icon).setVisibility(0);
        }
        if (this.displayW > Global.scale(250.0f)) {
            vav.findViewById(C0436R.id.attach_title).setVisibility(0);
        } else {
            vav.findViewById(C0436R.id.attach_title).setVisibility(4);
        }
        ((TextView) vav.findViewById(C0436R.id.attach_title)).setText(this.title);
        ((TextView) vav.findViewById(C0436R.id.attach_duration)).setText(String.format("%d:%02d", new Object[]{Integer.valueOf(this.duration / 60), Integer.valueOf(this.duration % 60)}));
        View findViewById = vav.findViewById(C0436R.id.attach_duration);
        if (this.duration > 0) {
            i = 0;
        } else {
            i = 4;
        }
        findViewById.setVisibility(i);
        ((ImageView) vav.findViewById(C0436R.id.video_preview)).setImageBitmap(null);
        vav.setLayoutParams(lp);
        return vav;
    }

    public void setPaddingAfter(boolean p) {
        this.paddingAfter = p;
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(2);
        os.writeInt(this.vfile == null ? 0 : 1);
        if (this.vfile == null) {
            os.writeUTF(this.title);
            os.writeUTF(this.image);
            os.writeInt(this.oid);
            os.writeInt(this.vid);
            os.writeInt(this.duration);
            os.writeUTF(this.accessKey == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.accessKey);
            return;
        }
        this.vfile.writeToStream(os);
    }

    public float getRatio() {
        return 1.3333334f;
    }

    public void setViewSize(float width, float height, boolean breakAfter, boolean floating) {
        this.displayW = Math.round(width);
        this.displayH = Math.round(height);
        this.breakAfter = breakAfter;
        this.floating = floating;
    }

    public int getWidth() {
        return 3200;
    }

    public int getHeight() {
        return 2400;
    }

    public String getThumbURL() {
        return this.image;
    }

    public int getWidth(char size) {
        return 3200;
    }

    public String toString() {
        return "video" + this.oid + "_" + this.vid + (this.accessKey != null ? "_" + this.accessKey : ACRAConstants.DEFAULT_STRING_VALUE);
    }

    public FlowLayout.LayoutParams getViewLayoutParams() {
        FlowLayout.LayoutParams lp = new FlowLayout.LayoutParams(Global.scale(ImageViewHolder.PaddingSize), Global.scale((float) (this.paddingAfter ? 10 : 2)));
        if (this.breakAfter || this.floating) {
            lp.breakAfter = this.breakAfter;
            lp.floating = this.floating;
        }
        lp.width = this.displayW;
        lp.height = this.displayH;
        return lp;
    }

    public String getImageURL() {
        return this.image;
    }

    public void setImage(View view, Bitmap img, boolean fromCache) {
        ((VideoAttachView) view).setImageBitmap(img);
    }

    public void clearImage(View view) {
        ((VideoAttachView) view).setImageBitmap(null);
    }
}
