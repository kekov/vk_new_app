package com.vkontakte.android;

import android.graphics.Bitmap;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class BitmapHack {
    private static ArrayList<Bitmap> hackedBitmaps;
    private static final VMRuntimeHack runtime;

    static class VMRuntimeHack {
        private Object runtime;
        private Method trackAllocation;
        private Method trackFree;

        public boolean trackAlloc(long size) {
            if (this.runtime == null) {
                return false;
            }
            try {
                Object res = this.trackAllocation.invoke(this.runtime, new Object[]{Long.valueOf(size)});
                if (res instanceof Boolean) {
                    return ((Boolean) res).booleanValue();
                }
                return true;
            } catch (IllegalArgumentException e) {
                return false;
            } catch (IllegalAccessException e2) {
                return false;
            } catch (InvocationTargetException e3) {
                return false;
            }
        }

        public boolean trackFree(long size) {
            if (this.runtime == null) {
                return false;
            }
            try {
                Object res = this.trackFree.invoke(this.runtime, new Object[]{Long.valueOf(size)});
                if (res instanceof Boolean) {
                    return ((Boolean) res).booleanValue();
                }
                return true;
            } catch (IllegalArgumentException e) {
                return false;
            } catch (IllegalAccessException e2) {
                return false;
            } catch (InvocationTargetException e3) {
                return false;
            }
        }

        public VMRuntimeHack() {
            this.runtime = null;
            this.trackAllocation = null;
            this.trackFree = null;
            boolean success = false;
            try {
                Class cl = Class.forName("dalvik.system.VMRuntime");
                this.runtime = cl.getMethod("getRuntime", new Class[0]).invoke(null, new Object[0]);
                this.trackAllocation = cl.getMethod("trackExternalAllocation", new Class[]{Long.TYPE});
                this.trackFree = cl.getMethod("trackExternalFree", new Class[]{Long.TYPE});
                success = true;
            } catch (ClassNotFoundException e) {
            } catch (SecurityException e2) {
            } catch (NoSuchMethodException e3) {
            } catch (IllegalArgumentException e4) {
            } catch (IllegalAccessException e5) {
            } catch (InvocationTargetException e6) {
            }
            if (!success) {
                Log.m528i("vk", "VMRuntime hack does not work!");
                this.runtime = null;
                this.trackAllocation = null;
                this.trackFree = null;
            }
        }
    }

    static {
        hackedBitmaps = new ArrayList();
        runtime = new VMRuntimeHack();
    }

    public static void hackBitmap(Bitmap bmp) {
        if (!hackedBitmaps.contains(bmp)) {
            runtime.trackFree((long) (bmp.getRowBytes() * bmp.getHeight()));
            hackedBitmaps.add(bmp);
        }
    }

    public static void unhackBitmap(Bitmap bmp) {
        if (hackedBitmaps.contains(bmp)) {
            runtime.trackAlloc((long) (bmp.getRowBytes() * bmp.getHeight()));
            hackedBitmaps.remove(bmp);
        }
    }

    public static void unhackList(List<Bitmap> bmps) {
        for (Bitmap bmp : bmps) {
            unhackBitmap(bmp);
        }
    }
}
