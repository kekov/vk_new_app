package com.vkontakte.android;

public class NewsfeedList {
    public int id;
    public String title;

    public NewsfeedList(int id, String title) {
        this.id = id;
        this.title = title;
    }
}
