package com.vkontakte.android;

import android.content.Context;
import android.os.Parcel;
import android.view.View;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class FwdMessagesAttachment extends Attachment {
    public ArrayList<Message> msgs;

    public FwdMessagesAttachment(ArrayList<Message> fw) {
        this.msgs = fw;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
    }

    public View getFullView(Context context) {
        return null;
    }

    public View getViewForList(Context context, View reuse) {
        return null;
    }

    public void serialize(DataOutputStream os) throws IOException {
    }

    public LayoutParams getViewLayoutParams() {
        return null;
    }
}
