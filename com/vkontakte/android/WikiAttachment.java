package com.vkontakte.android;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import java.io.DataOutputStream;
import java.io.IOException;

public class WikiAttachment extends Attachment {
    public static final Creator<WikiAttachment> CREATOR;
    int oid;
    int pid;
    String section;
    String title;

    /* renamed from: com.vkontakte.android.WikiAttachment.1 */
    class C05461 implements Creator<WikiAttachment> {
        C05461() {
        }

        public WikiAttachment createFromParcel(Parcel in) {
            return new WikiAttachment(in.readString(), in.readString(), in.readInt(), in.readInt());
        }

        public WikiAttachment[] newArray(int size) {
            return new WikiAttachment[size];
        }
    }

    /* renamed from: com.vkontakte.android.WikiAttachment.2 */
    class C05472 implements OnClickListener {
        C05472() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), WikiViewActivity.class);
            intent.putExtra("oid", WikiAttachment.this.oid);
            intent.putExtra("pid", WikiAttachment.this.pid);
            intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, WikiAttachment.this.title);
            intent.putExtra("section", WikiAttachment.this.section);
            v.getContext().startActivity(intent);
        }
    }

    public WikiAttachment(String title, String section, int oid, int pid) {
        this.title = title;
        this.section = section;
        this.oid = oid;
        this.pid = pid;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel p, int flags) {
        p.writeString(this.title);
        p.writeString(this.section);
        p.writeInt(this.oid);
        p.writeInt(this.pid);
    }

    static {
        CREATOR = new C05461();
    }

    public View getFullView(Context context) {
        return getViewForList(context, null);
    }

    public View getViewForList(Context context, View reuse) {
        View v;
        if (reuse == null) {
            v = Attachment.getReusableView(context, "common");
        } else {
            v = reuse;
        }
        ((ImageView) v.findViewById(C0436R.id.attach_icon)).setImageResource(C0436R.drawable.ic_attach_link);
        ((TextView) v.findViewById(C0436R.id.attach_title)).setText(this.title);
        ((TextView) v.findViewById(C0436R.id.attach_subtitle)).setText(C0436R.string.attach_wiki);
        v.setOnClickListener(new C05472());
        return v;
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(11);
        os.writeUTF(this.title);
        os.writeUTF(this.section);
        os.writeInt(this.oid);
        os.writeInt(this.pid);
    }

    public LayoutParams getViewLayoutParams() {
        LayoutParams lp = new LayoutParams();
        lp.width = -1;
        lp.height = Global.scale(54.0f);
        return lp;
    }
}
