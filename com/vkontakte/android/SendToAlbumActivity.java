package com.vkontakte.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Toast;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.mediapicker.GalleryPickerActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class SendToAlbumActivity extends Activity {
    private static final int FILTER_RESULT = 101;
    private int aid;
    Intent intent;
    private String title;

    /* renamed from: com.vkontakte.android.SendToAlbumActivity.2 */
    class C04712 implements OnClickListener {
        private final /* synthetic */ int[] val$aids;
        private final /* synthetic */ String[] val$atitles;

        C04712(int[] iArr, String[] strArr) {
            this.val$aids = iArr;
            this.val$atitles = strArr;
        }

        public void onClick(DialogInterface dialog, int which) {
            if (this.val$aids[which] == -1) {
                SendToAlbumActivity.this.createAlbumAndSend();
            } else {
                SendToAlbumActivity.this.doSend(this.val$aids[which], this.val$atitles[which]);
            }
        }
    }

    /* renamed from: com.vkontakte.android.SendToAlbumActivity.3 */
    class C04723 implements OnCancelListener {
        C04723() {
        }

        public void onCancel(DialogInterface dialog) {
            SendToAlbumActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.SendToAlbumActivity.1 */
    class C13531 extends APIHandler {
        private final /* synthetic */ ProgressDialog val$pdlg;

        /* renamed from: com.vkontakte.android.SendToAlbumActivity.1.1 */
        class C04701 implements Runnable {
            private final /* synthetic */ int[] val$aids;
            private final /* synthetic */ String[] val$atitles;

            C04701(String[] strArr, int[] iArr) {
                this.val$atitles = strArr;
                this.val$aids = iArr;
            }

            public void run() {
                SendToAlbumActivity.this.showDialog(this.val$atitles, this.val$aids);
            }
        }

        C13531(ProgressDialog progressDialog) {
            this.val$pdlg = progressDialog;
        }

        public void success(JSONObject r) {
            try {
                int len;
                JSONArray a = r.getJSONObject("response").getJSONArray("items");
                if (a == null || a.length() <= 0) {
                    len = 1;
                } else {
                    len = a.length();
                }
                String[] atitles = new String[len];
                int[] aids = new int[len];
                if (a == null || a.length() <= 0) {
                    atitles[0] = SendToAlbumActivity.this.getResources().getString(C0436R.string.new_album);
                    aids[0] = -1;
                } else {
                    for (int i = 0; i < a.length(); i++) {
                        atitles[i] = a.getJSONObject(i).getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
                        aids[i] = a.getJSONObject(i).getInt("id");
                    }
                }
                this.val$pdlg.dismiss();
                SendToAlbumActivity.this.runOnUiThread(new C04701(atitles, aids));
            } catch (Throwable x) {
                this.val$pdlg.dismiss();
                SendToAlbumActivity.this.finish();
                Log.m532w("vk", x);
            }
        }

        public void fail(int ecode, String emsg) {
            this.val$pdlg.dismiss();
            SendToAlbumActivity.this.finish();
            Toast.makeText(SendToAlbumActivity.this, C0436R.string.err_text, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.SendToAlbumActivity.4 */
    class C13544 extends APIHandler {
        C13544() {
        }

        public void success(JSONObject o) {
            try {
                SendToAlbumActivity.this.doSend(o.getJSONObject("response").getInt("id"), SendToAlbumActivity.this.getResources().getString(C0436R.string.new_album_title));
            } catch (Exception e) {
            }
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(SendToAlbumActivity.this, C0436R.string.err_text, 0).show();
            SendToAlbumActivity.this.finish();
        }
    }

    public void onCreate(Bundle b) {
        overridePendingTransition(0, 0);
        super.onCreate(b);
        this.intent = getIntent();
        setContentView(new View(this));
        if (Global.uid == 0) {
            startActivityForResult(new Intent(this, AuthActivity.class), 100);
        } else {
            loadAlbums();
        }
    }

    private void loadAlbums() {
        ProgressDialog pdlg = new ProgressDialog(this);
        pdlg.setCancelable(false);
        pdlg.setMessage(getResources().getString(C0436R.string.loading));
        pdlg.show();
        new APIRequest("photos.getAlbums").handler(new C13531(pdlg)).exec((Activity) this);
    }

    public void doSend(int aid, String title) {
        this.aid = aid;
        this.title = title;
        Intent fIntent;
        if ("android.intent.action.SEND_MULTIPLE".equals(this.intent.getAction())) {
            fIntent = new Intent(this, GalleryPickerActivity.class);
            ArrayList<Parcelable> list = this.intent.getParcelableArrayListExtra("android.intent.extra.STREAM");
            fIntent.setAction("android.intent.action.SEND_MULTIPLE");
            fIntent.putExtra("android.intent.extra.STREAM", list);
            startActivityForResult(fIntent, FILTER_RESULT);
        } else if ("android.intent.action.SEND".equals(this.intent.getAction())) {
            fIntent = new Intent(this, GalleryPickerActivity.class);
            Uri uri = (Uri) this.intent.getParcelableExtra("android.intent.extra.STREAM");
            fIntent.setAction("android.intent.action.SEND");
            fIntent.putExtra("android.intent.extra.STREAM", uri);
            startActivityForResult(fIntent, FILTER_RESULT);
        }
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode == FILTER_RESULT) {
            if (resCode == -1) {
                ArrayList<Uri> uris = new ArrayList();
                Iterator it = data.getStringArrayListExtra("images").iterator();
                while (it.hasNext()) {
                    uris.add(Uri.parse((String) it.next()));
                }
                Intent intent = new Intent(this, UploaderService.class);
                intent.putParcelableArrayListExtra("files", uris);
                HashMap<String, String> params = new HashMap();
                params.put("aid", new StringBuilder(String.valueOf(this.aid)).toString());
                intent.putExtra("req_params", params);
                intent.putExtra("info", this.title);
                intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 1);
                startService(intent);
            }
            finish();
        }
        if (reqCode != 100) {
            return;
        }
        if (resCode == -1) {
            loadAlbums();
        } else {
            finish();
        }
    }

    public void showDialog(String[] atitles, int[] aids) {
        new Builder(this).setTitle(C0436R.string.select_album).setItems(atitles, new C04712(aids, atitles)).setOnCancelListener(new C04723()).show();
    }

    private void createAlbumAndSend() {
        new APIRequest("photos.createAlbum").param(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, getResources().getString(C0436R.string.new_album_title)).handler(new C13544()).exec((Activity) this);
    }
}
