package com.vkontakte.android;

import android.app.ActivityManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.view.MotionEventCompat;
import com.actionbarsherlock.view.Menu;
import com.jakewharton.DiskLruCache;
import com.jakewharton.DiskLruCache.Editor;
import com.jakewharton.DiskLruCache.Snapshot;
import com.vkontakte.android.cache.LruCache;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.PhantomReference;
import java.util.ArrayList;
import org.acra.ACRAConstants;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class ImageCache {
    public static final boolean DEBUG = false;
    public static final long MAX_CACHE_SIZE_INTERNAL = 5242880;
    public static final int MAX_CACHE_SIZE_RAM;
    public static final long MAX_CACHE_SIZE_SD = 15728640;
    private static LruCache<String, Bitmap> cache;
    public static File cacheDir;
    private static DiskLruCache diskCache;
    private static HttpClient httpClient;
    public static long maxCacheSize;
    private static ArrayList<PhantomReference<Bitmap>> uncachedBitmaps;

    public interface ProgressCallback {
        void onProgressChanged(int i, int i2);
    }

    public static class RequestWrapper {
        public boolean decode;
        public HttpGet request;

        public RequestWrapper() {
            this.decode = true;
        }
    }

    /* renamed from: com.vkontakte.android.ImageCache.1 */
    class C12921 extends LruCache<String, Bitmap> {
        C12921(int $anonymous0) {
            super($anonymous0);
        }

        protected int sizeOf(String key, Bitmap value) {
            return value.getRowBytes() * value.getHeight();
        }

        protected void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue) {
            if (Global.useBitmapHack) {
                BitmapHack.unhackBitmap(oldValue);
            }
        }
    }

    static {
        cacheDir = null;
        MAX_CACHE_SIZE_RAM = ((((ActivityManager) VKApplication.context.getSystemService("activity")).getMemoryClass() / 7) * GLRenderBuffer.EGL_SURFACE_SIZE) * GLRenderBuffer.EGL_SURFACE_SIZE;
        maxCacheSize = MAX_CACHE_SIZE_INTERNAL;
        uncachedBitmaps = new ArrayList();
        cache = new C12921(MAX_CACHE_SIZE_RAM);
        diskCache = null;
        open();
    }

    public static void clearTopLevel() {
        cache.evictAll();
    }

    public static LruCache<String, Bitmap> getLruCache() {
        return cache;
    }

    public static void remove(String url) {
        cache.remove(url);
    }

    public static void put(String url, Bitmap bmp) {
        cache.put(url, bmp);
    }

    public static boolean isInTopCache(String url) {
        if (url == null) {
            return DEBUG;
        }
        if (url.startsWith("A")) {
            String[] urls = url.split("\\|");
            for (int i = 1; i < urls.length; i++) {
                if (_isInTopCache(urls[i])) {
                    return true;
                }
            }
        }
        return _isInTopCache(url);
    }

    private static boolean _isInTopCache(String url) {
        return (cache == null || url == null || !cache.contains(url) || cache.get(url) == null) ? DEBUG : true;
    }

    public static Bitmap getFromTop(String url) {
        if (url.startsWith("A")) {
            String[] urls = url.split("\\|");
            for (int i = 1; i < urls.length; i++) {
                if (_isInTopCache(urls[i])) {
                    return (Bitmap) cache.get(urls[i]);
                }
            }
        }
        return (Bitmap) cache.get(url);
    }

    public static boolean isInCache(String url) {
        if (cache.contains(url)) {
            return true;
        }
        try {
            if (diskCache.get(fn(url)) != null) {
                return true;
            }
        } catch (Exception e) {
        }
        return DEBUG;
    }

    private static String fn(String url) {
        return APIRequest.md5(url);
    }

    private static void open() {
        try {
            diskCache = DiskLruCache.open(new File(VKApplication.context.getCacheDir(), "images"), 1, 1, MAX_CACHE_SIZE_INTERNAL);
        } catch (Exception e) {
        }
    }

    public static void close() {
        try {
            diskCache.close();
        } catch (Exception e) {
        }
    }

    public static Bitmap get(String url) {
        return get(url, null, null, true);
    }

    public static Bitmap get(String url, RequestWrapper w, ProgressCallback pc, boolean decode) {
        Bitmap bmp;
        try {
            if (cache.contains(url)) {
                if (!decode) {
                    return null;
                }
                bmp = (Bitmap) cache.get(url);
                if (bmp != null) {
                    return bmp;
                }
                cache.remove(url);
            }
            Snapshot entry = diskCache.get(fn(url));
            InputStream in;
            if (entry != null && (!decode || isValidBitmap(entry))) {
                in = entry.getInputStream(MAX_CACHE_SIZE_RAM);
                bmp = decode ? decodeImage(in) : null;
                in.close();
                if (!decode) {
                    return bmp;
                }
                if (Global.useBitmapHack) {
                    BitmapHack.hackBitmap(bmp);
                }
                cache.put(url, bmp);
                return bmp;
            } else if (url.startsWith("content:")) {
                in = VKApplication.context.getContentResolver().openInputStream(Uri.parse(url));
                bmp = decode ? decodeImage(in) : null;
                in.close();
                if (!decode) {
                    return bmp;
                }
                if (Global.useBitmapHack) {
                    BitmapHack.hackBitmap(bmp);
                }
                try {
                    cache.put(url, bmp);
                    return bmp;
                } catch (Exception e) {
                    return bmp;
                }
            } else if (url.startsWith("file:")) {
                Uri uri = Uri.parse(url);
                int width = MAX_CACHE_SIZE_RAM;
                if (uri.getQueryParameter("w") != null) {
                    width = Integer.parseInt(uri.getQueryParameter("w"));
                }
                Options opts = new Options();
                if (width > 0) {
                    opts.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(uri.getPath(), opts);
                    opts.inJustDecodeBounds = DEBUG;
                    opts.inSampleSize = (int) Math.floor((double) (((float) opts.outWidth) / ((float) width)));
                }
                bmp = BitmapFactory.decodeFile(uri.getPath(), opts);
                try {
                    cache.put(url, bmp);
                    return bmp;
                } catch (Exception e2) {
                    return bmp;
                }
            } else {
                Editor editor = null;
                try {
                    editor = diskCache.edit(fn(url));
                    if (editor == null) {
                        while (true) {
                            editor = diskCache.edit(fn(url));
                            if (editor != null) {
                                break;
                            }
                            Thread.sleep(10);
                        }
                        if (editor != null) {
                            editor.abort();
                        }
                        return get(url, w, pc, decode);
                    }
                    OutputStream out = editor.newOutputStream(MAX_CACHE_SIZE_RAM);
                    boolean ok = downloadFile(url, w, pc, out);
                    out.close();
                    if (ok) {
                        editor.commit();
                        in = diskCache.get(fn(url)).getInputStream(MAX_CACHE_SIZE_RAM);
                        if (w != null) {
                            decode = w.decode;
                        }
                        bmp = decode ? decodeImage(in) : null;
                        if (!decode) {
                            return bmp;
                        }
                        if (Global.useBitmapHack) {
                            BitmapHack.hackBitmap(bmp);
                        }
                        try {
                            cache.put(url, bmp);
                            return bmp;
                        } catch (Exception e3) {
                            return bmp;
                        }
                    }
                    editor.abort();
                    diskCache.remove(fn(url));
                    return null;
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                    if (editor != null) {
                        editor.abort();
                    }
                }
            }
        } catch (Exception e4) {
            return bmp;
        } catch (Throwable x2) {
            Log.m531w("vk", url, x2);
            Log.m526e("vk", "WTF2");
            return null;
        }
    }

    public static byte[] getBytes(String url, RequestWrapper w) {
        try {
            Snapshot entry = diskCache.get(fn(url));
            if (entry != null) {
                InputStream in = entry.getInputStream(MAX_CACHE_SIZE_RAM);
                byte[] buffer = new byte[((int) entry.getLength(MAX_CACHE_SIZE_RAM))];
                in.read(buffer);
                in.close();
                return buffer;
            }
            byte[] imgData = Global.getURL(url, w, null);
            if (imgData == null) {
                return null;
            }
            try {
                Editor editor = diskCache.edit(fn(url));
                OutputStream out = editor.newOutputStream(MAX_CACHE_SIZE_RAM);
                out.write(imgData);
                out.close();
                editor.commit();
            } catch (Exception e) {
            }
            return imgData;
        } catch (Exception e2) {
            return null;
        }
    }

    public static boolean save(String url, String to) {
        byte[] b = getBytes(url, null);
        try {
            FileOutputStream os = new FileOutputStream(new File(to));
            os.write(b);
            os.close();
            return true;
        } catch (Exception e) {
            return DEBUG;
        }
    }

    private static boolean isValidBitmap(Snapshot e) {
        InputStream is = e.getInputStream(MAX_CACHE_SIZE_RAM);
        try {
            new Options().inJustDecodeBounds = true;
            DataInputStream in = new DataInputStream(is);
            int header = in.readInt();
            ((FileInputStream) is).getChannel().position(e.getLength(MAX_CACHE_SIZE_RAM) - 4);
            int trailer = in.readInt();
            if (header == -2555936 && (Menu.USER_MASK & trailer) == 65497) {
                return true;
            }
            if (header == 1195984440 && (trailer & MotionEventCompat.ACTION_MASK) == 59) {
                return true;
            }
            if (header == -1991225785 && trailer == -1371381630) {
                return true;
            }
            try {
                is.close();
            } catch (Exception e2) {
            }
            return DEBUG;
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    private static Bitmap decodeImage(InputStream data) {
        if (data == null) {
            Log.m530w("vk", "tried to decode null image");
            return null;
        }
        try {
            Options opts1 = new Options();
            opts1.inJustDecodeBounds = true;
            if (data instanceof FileInputStream) {
                ((FileInputStream) data).getChannel().position(0);
            }
            BitmapFactory.decodeStream(data, null, opts1);
            if (data instanceof FileInputStream) {
                ((FileInputStream) data).getChannel().position(0);
            }
            if (opts1.outWidth > 1280 || opts1.outHeight > GLRenderBuffer.EGL_SURFACE_SIZE) {
                return null;
            }
            Options opts = new Options();
            opts.inDither = DEBUG;
            return BitmapFactory.decodeStream(data, null, opts);
        } catch (Throwable t) {
            Log.m527e("vk", "OH SHI~", t);
            if (cache.size() > GLRenderBuffer.EGL_SURFACE_SIZE) {
                cache.evictAll();
                System.gc();
                return decodeImage(data);
            }
            Log.m526e("vk", "WTF?!");
            return null;
        }
    }

    public static boolean downloadFile(String url, RequestWrapper w, ProgressCallback pc, OutputStream out) {
        if (httpClient == null) {
            HttpParams hParams = new BasicHttpParams();
            HttpProtocolParams.setUseExpectContinue(hParams, DEBUG);
            HttpProtocolParams.setUserAgent(hParams, APIController.USER_AGENT);
            HttpConnectionParams.setSocketBufferSize(hParams, ACRAConstants.DEFAULT_BUFFER_SIZE_IN_BYTES);
            HttpConnectionParams.setConnectionTimeout(hParams, 30000);
            HttpConnectionParams.setSoTimeout(hParams, 30000);
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(hParams, registry), hParams);
        }
        HttpGet httppost = new HttpGet(url);
        if (w != null) {
            w.request = httppost;
        }
        InputStream is = null;
        HttpResponse response = null;
        try {
            response = httpClient.execute(httppost);
            is = response.getEntity().getContent();
            int len = (int) response.getEntity().getContentLength();
            int loaded = MAX_CACHE_SIZE_RAM;
            byte[] rd = new byte[5120];
            while (true) {
                int l = is.read(rd);
                if (l <= 0) {
                    break;
                }
                out.write(rd, MAX_CACHE_SIZE_RAM, l);
                if (pc != null) {
                    loaded += l;
                    pc.onProgressChanged(loaded, len);
                }
            }
            is.close();
            is = null;
            response = null;
            if (w != null) {
                w.request = null;
            }
            if (response != null) {
                try {
                    response.getEntity().consumeContent();
                } catch (Exception e) {
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e2) {
                }
            }
            return true;
        } catch (Throwable th) {
            if (response != null) {
                try {
                    response.getEntity().consumeContent();
                } catch (Exception e3) {
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e4) {
                }
            }
        }
    }

    public static File getCacheDir() {
        if ("sd".equals(PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getString("imgCacheLocation", ACRAConstants.DEFAULT_STRING_VALUE))) {
            File file = new File(Environment.getExternalStorageDirectory(), ".vkontakte/cache/images");
            if (!file.exists()) {
                file.mkdirs();
            }
            File nomedia = new File(Environment.getExternalStorageDirectory(), ".vkontakte/.nomedia");
            try {
                if (!nomedia.exists()) {
                    nomedia.createNewFile();
                }
            } catch (Exception e) {
            }
            maxCacheSize = MAX_CACHE_SIZE_SD;
            return file;
        }
        maxCacheSize = MAX_CACHE_SIZE_INTERNAL;
        return VKApplication.context.getCacheDir();
    }

    public static void clear() {
        try {
            diskCache.delete();
        } catch (Exception e) {
        }
        open();
    }
}
