package com.vkontakte.android;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.widget.Toast;
import com.facebook.WebDialog;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.ContentReport;
import com.vkontakte.android.api.ContentReport.Callback;

public class ReportContentActivity extends Activity {
    private static final int[] typeMap;

    /* renamed from: com.vkontakte.android.ReportContentActivity.1 */
    class C04371 implements OnClickListener {
        C04371() {
        }

        public void onClick(DialogInterface dialog, int which) {
            ReportContentActivity.this.sendReport(ReportContentActivity.typeMap[which]);
        }
    }

    /* renamed from: com.vkontakte.android.ReportContentActivity.2 */
    class C04382 implements OnCancelListener {
        C04382() {
        }

        public void onCancel(DialogInterface dialog) {
            ReportContentActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.ReportContentActivity.3 */
    class C13423 implements Callback {
        C13423() {
        }

        public void success() {
            Toast.makeText(ReportContentActivity.this, C0436R.string.report_sent, 0).show();
            ReportContentActivity.this.finish();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(ReportContentActivity.this, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
            ReportContentActivity.this.finish();
        }
    }

    static {
        int[] iArr = new int[7];
        iArr[1] = 6;
        iArr[2] = 5;
        iArr[3] = 4;
        iArr[4] = 1;
        iArr[5] = 2;
        iArr[6] = 3;
        typeMap = iArr;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        new Builder(this).setTitle(C0436R.string.report_content).setItems(C0436R.array.report_types, new C04371()).setOnCancelListener(new C04382()).show();
    }

    private void sendReport(int type) {
        new ContentReport(getIntent().getIntExtra("ownerID", 0), getIntent().getIntExtra("itemID", 0), getIntent().getStringExtra(WebDialog.DIALOG_PARAM_TYPE), type).setCallback(new C13423()).wrapProgress(this).exec((Activity) this);
    }
}
