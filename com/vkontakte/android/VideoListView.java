package com.vkontakte.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.os.Parcelable;
import android.text.ClipboardManager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.FaveGetVideos;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.GroupsGetById;
import com.vkontakte.android.api.GroupsGetById.Callback;
import com.vkontakte.android.api.VideoAdd;
import com.vkontakte.android.api.VideoAlbum;
import com.vkontakte.android.api.VideoFile;
import com.vkontakte.android.api.VideoGet;
import com.vkontakte.android.api.VideoGetUserVideos;
import com.vkontakte.android.api.VideoSearch;
import com.vkontakte.android.api.WallDelete;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Friends.GetUsersCallback;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.PhotoView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import org.acra.ACRAConstants;

public class VideoListView extends FrameLayout implements Listener, OnItemClickListener, OnEditorActionListener {
    public static final int COMMENTS_REQ = 700;
    public static final int TYPE_FAVE = 2;
    public static final int TYPE_USER = 0;
    public static final int TYPE_WITH_USER = 1;
    private OnClickListener actionsClickListener;
    private VideoAdapter adapter;
    private Vector<VideoAlbum> albums;
    private ProgressBar bigProgress;
    private VideoViewCallback callback;
    private int currentAlbum;
    private APIRequest currentRequest;
    private boolean dataLoading;
    private EmptyView emptyView;
    private ErrorView error;
    private boolean firstLoad;
    private FrameLayout footerView;
    private String groupName;
    private String groupPhoto;
    private ListImageLoaderWrapper imgWrapper;
    private int itemHeight;
    private GridView list;
    private Vector<VideoFile> listVideos;
    private FrameLayout listWrap;
    private String localSearchQuery;
    private boolean moreAvailable;
    private boolean needFullReload;
    private int numLocal;
    public boolean searchHd;
    public int searchLength;
    private String searchQuery;
    public boolean selectMode;
    private int type;
    private int uid;
    private Vector<VideoFile> videos;

    /* renamed from: com.vkontakte.android.VideoListView.13 */
    class AnonymousClass13 implements DialogInterface.OnClickListener {
        private final /* synthetic */ VideoFile val$vf;

        AnonymousClass13(VideoFile videoFile) {
            this.val$vf = videoFile;
        }

        public void onClick(DialogInterface dialog, int which) {
            VideoListView.this.doDeleteVideo(this.val$vf);
        }
    }

    /* renamed from: com.vkontakte.android.VideoListView.1 */
    class C05331 implements OnClickListener {

        /* renamed from: com.vkontakte.android.VideoListView.1.1 */
        class C05311 implements DialogInterface.OnClickListener {
            private final /* synthetic */ ArrayList val$acts;
            private final /* synthetic */ VideoFile val$vf;

            C05311(ArrayList arrayList, VideoFile videoFile) {
                this.val$acts = arrayList;
                this.val$vf = videoFile;
            }

            public void onClick(DialogInterface dialog, int which) {
                String act = (String) this.val$acts.get(which);
                if ("comments".equals(act)) {
                    VideoListView.this.openVideoComments(this.val$vf);
                } else if ("add".equals(act)) {
                    VideoListView.this.addVideo(this.val$vf);
                } else if ("delete".equals(act)) {
                    VideoListView.this.deleteVideo(this.val$vf);
                } else if ("link".equals(act)) {
                    VideoListView.this.copyLink(this.val$vf);
                }
            }
        }

        /* renamed from: com.vkontakte.android.VideoListView.1.2 */
        class C05322 implements OnMenuItemClickListener {
            private final /* synthetic */ ArrayList val$acts;
            private final /* synthetic */ VideoFile val$vf;

            C05322(ArrayList arrayList, VideoFile videoFile) {
                this.val$acts = arrayList;
                this.val$vf = videoFile;
            }

            public boolean onMenuItemClick(MenuItem item) {
                String act = (String) this.val$acts.get(item.getItemId());
                if ("comments".equals(act)) {
                    VideoListView.this.openVideoComments(this.val$vf);
                } else if ("add".equals(act)) {
                    VideoListView.this.addVideo(this.val$vf);
                } else if ("delete".equals(act)) {
                    VideoListView.this.deleteVideo(this.val$vf);
                } else if ("link".equals(act)) {
                    VideoListView.this.copyLink(this.val$vf);
                }
                return true;
            }
        }

        C05331() {
        }

        public void onClick(View v) {
            if (v.getTag() != null) {
                int pos = ((Integer) v.getTag()).intValue();
                ArrayList<String> opts = new ArrayList();
                ArrayList<String> acts = new ArrayList();
                opts.add(VideoListView.this.getResources().getString(C0436R.string.comments));
                acts.add("comments");
                VideoFile vf = (VideoFile) VideoListView.this.videos.get(pos);
                if (vf.oid == Global.uid || (vf.oid < 0 && Groups.getAdminLevel(-vf.oid) >= VideoListView.TYPE_FAVE)) {
                    opts.add(VideoListView.this.getResources().getString(C0436R.string.delete));
                    acts.add("delete");
                }
                if (vf.oid != Global.uid) {
                    opts.add(VideoListView.this.getResources().getString(C0436R.string.add));
                    acts.add("add");
                }
                opts.add(VideoListView.this.getResources().getString(C0436R.string.copy_link));
                acts.add("link");
                if (pos >= 0 && pos < VideoListView.this.videos.size()) {
                    if (VERSION.SDK_INT < 14) {
                        new Builder(VideoListView.this.getContext()).setItems((CharSequence[]) opts.toArray(new String[VideoListView.TYPE_USER]), new C05311(acts, vf)).show();
                        return;
                    }
                    PopupMenu pm = new PopupMenu(VideoListView.this.getContext(), v);
                    for (int i = VideoListView.TYPE_USER; i < opts.size(); i += VideoListView.TYPE_WITH_USER) {
                        pm.getMenu().add(VideoListView.TYPE_USER, i, VideoListView.TYPE_USER, (CharSequence) opts.get(i));
                    }
                    pm.setOnMenuItemClickListener(new C05322(acts, vf));
                    pm.show();
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.VideoListView.2 */
    class C05342 implements OnClickListener {
        C05342() {
        }

        public void onClick(View v) {
            if (VideoListView.this.callback != null) {
                VideoListView.this.callback.showAddDialog();
            }
        }
    }

    /* renamed from: com.vkontakte.android.VideoListView.3 */
    class C05353 implements OnClickListener {
        C05353() {
        }

        public void onClick(View v) {
            VideoListView.this.error.setVisibility(8);
            VideoListView.this.bigProgress.setVisibility(VideoListView.TYPE_USER);
            VideoListView.this.loadData();
        }
    }

    /* renamed from: com.vkontakte.android.VideoListView.4 */
    class C05364 implements Runnable {
        private final /* synthetic */ int val$w;

        C05364(int i) {
            this.val$w = i;
        }

        public void run() {
            VideoListView.this.updateSizes(this.val$w);
        }
    }

    /* renamed from: com.vkontakte.android.VideoListView.9 */
    class C05379 implements Runnable {
        C05379() {
        }

        public void run() {
            Log.m528i("vk", "UPDATE LIST");
            VideoListView.this.adapter.notifyDataSetChanged();
            VideoListView.this.imgWrapper.updateImages();
        }
    }

    private class VideoAdapter extends BaseAdapter {
        boolean first;

        /* renamed from: com.vkontakte.android.VideoListView.VideoAdapter.1 */
        class C05381 implements Runnable {
            C05381() {
            }

            public void run() {
                VideoAdapter.this.first = false;
            }
        }

        private VideoAdapter() {
            this.first = true;
        }

        public int getCount() {
            int i = VideoListView.TYPE_USER;
            if (VideoListView.this.videos.size() == 0) {
                return VideoListView.TYPE_USER;
            }
            int size = VideoListView.this.videos.size();
            if (VideoListView.this.moreAvailable) {
                i = VideoListView.TYPE_WITH_USER;
            }
            return i + size;
        }

        public Object getItem(int arg0) {
            return null;
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public int getItemViewType(int item) {
            if (item == VideoListView.this.videos.size()) {
                return VideoListView.TYPE_WITH_USER;
            }
            return VideoListView.TYPE_USER;
        }

        public int getViewTypeCount() {
            return VideoListView.TYPE_FAVE;
        }

        public View getView(int pos, View conv, ViewGroup group) {
            int i = VideoListView.TYPE_USER;
            if (pos != VideoListView.this.videos.size()) {
                if (conv == null || conv.findViewById(C0436R.id.album_title) == null || (conv != null && "shit".equals(conv.getTag()))) {
                    conv = VideoListView.inflate(VideoListView.this.getContext(), C0436R.layout.video_list_item, null);
                }
                if (this.first) {
                    conv.setTag("shit");
                    conv.postDelayed(new C05381(), 100);
                }
                conv.setLayoutParams(new LayoutParams(-1, VideoListView.this.itemHeight));
                VideoFile v = null;
                try {
                    v = (VideoFile) VideoListView.this.videos.get(pos);
                } catch (Exception e) {
                }
                if (v == null) {
                    Log.m526e("vk", "DAFUQ");
                    return conv;
                }
                String d;
                if (v.duration > 3600) {
                    d = String.format("%d:%02d:%02d", new Object[]{Integer.valueOf(v.duration / 3600), Integer.valueOf((v.duration % 3600) / 60), Integer.valueOf(v.duration % 60)});
                } else {
                    Object[] objArr = new Object[VideoListView.TYPE_FAVE];
                    objArr[VideoListView.TYPE_USER] = Integer.valueOf(v.duration / 60);
                    objArr[VideoListView.TYPE_WITH_USER] = Integer.valueOf(v.duration % 60);
                    d = String.format("%d:%02d", objArr);
                }
                ((TextView) conv.findViewById(C0436R.id.album_title)).setText(v.title);
                ((TextView) conv.findViewById(C0436R.id.album_qty)).setText(Global.langPlural(C0436R.array.video_views, v.views, VideoListView.this.getResources()));
                ((TextView) conv.findViewById(C0436R.id.video_duration)).setText(d);
                conv.findViewById(C0436R.id.video_duration).setVisibility(v.duration > 0 ? VideoListView.TYPE_USER : 8);
                if (VideoListView.this.imgWrapper.isAlreadyLoaded(v.urlBigThumb)) {
                    ((ImageView) conv.findViewById(C0436R.id.album_thumb)).setImageBitmap(VideoListView.this.imgWrapper.get(v.urlBigThumb));
                } else {
                    ((ImageView) conv.findViewById(C0436R.id.album_thumb)).setImageDrawable(new ColorDrawable(Color.LIGHT_GRAY));
                }
                conv.findViewById(C0436R.id.album_actions).setTag(Integer.valueOf(pos));
                View findViewById = conv.findViewById(C0436R.id.album_actions);
                if (VideoListView.this.selectMode) {
                    i = 8;
                }
                findViewById.setVisibility(i);
                conv.findViewById(C0436R.id.album_actions).setOnClickListener(VideoListView.this.actionsClickListener);
                return conv;
            } else if (conv != null) {
                conv.setLayoutParams(new LayoutParams(-1, Global.scale(50.0f)));
                return conv;
            } else {
                View fl = new FrameLayout(VideoListView.this.getContext());
                fl.addView(new ProgressBar(VideoListView.this.getContext()), new FrameLayout.LayoutParams(Global.scale(BitmapDescriptorFactory.HUE_ORANGE), Global.scale(BitmapDescriptorFactory.HUE_ORANGE), 17));
                fl.setLayoutParams(new LayoutParams(-1, Global.scale(50.0f)));
                return fl;
            }
        }

        public void notifyDataSetChanged() {
            this.first = true;
            super.notifyDataSetChanged();
        }
    }

    public interface VideoViewCallback {
        void openComments(VideoFile videoFile, String str, String str2);

        void showAddDialog();
    }

    /* renamed from: com.vkontakte.android.VideoListView.11 */
    class AnonymousClass11 implements Callback {
        private final /* synthetic */ VideoFile val$vf;

        AnonymousClass11(VideoFile videoFile) {
            this.val$vf = videoFile;
        }

        public void success(Group[] groups) {
            VideoListView.this.doOpenComments(groups[VideoListView.TYPE_USER].name, groups[VideoListView.TYPE_USER].photo, this.val$vf);
        }

        public void fail(int ecode, String emsg) {
        }
    }

    /* renamed from: com.vkontakte.android.VideoListView.12 */
    class AnonymousClass12 implements GetUsersCallback {
        private final /* synthetic */ ProgressDialog val$pdlg;
        private final /* synthetic */ VideoFile val$vf;

        AnonymousClass12(ProgressDialog progressDialog, VideoFile videoFile) {
            this.val$pdlg = progressDialog;
            this.val$vf = videoFile;
        }

        public void onUsersLoaded(ArrayList<UserProfile> users) {
            this.val$pdlg.dismiss();
            VideoListView.this.doOpenComments(new StringBuilder(String.valueOf(((UserProfile) users.get(VideoListView.TYPE_USER)).firstName)).append(" ").append(((UserProfile) users.get(VideoListView.TYPE_USER)).lastName).toString(), ((UserProfile) users.get(VideoListView.TYPE_USER)).photo, this.val$vf);
        }
    }

    /* renamed from: com.vkontakte.android.VideoListView.14 */
    class AnonymousClass14 implements WallDelete.Callback {
        private final /* synthetic */ VideoFile val$vf;

        AnonymousClass14(VideoFile videoFile) {
            this.val$vf = videoFile;
        }

        public void success() {
            VideoListView.this.removeItem(this.val$vf.oid, this.val$vf.vid);
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(VideoListView.this.getContext(), C0436R.string.error, VideoListView.TYPE_USER).show();
        }
    }

    /* renamed from: com.vkontakte.android.VideoListView.15 */
    class AnonymousClass15 implements VideoAdd.Callback {
        private final /* synthetic */ VideoFile val$vf;

        AnonymousClass15(VideoFile videoFile) {
            this.val$vf = videoFile;
        }

        public void success(int vid) {
            if (VideoListView.this.type == 0 && (VideoListView.this.uid == 0 || VideoListView.this.uid == Global.uid)) {
                VideoListView.this.listVideos.add(this.val$vf);
            }
            Context context = VideoListView.this.getContext();
            Resources resources = VideoListView.this.getResources();
            Object[] objArr = new Object[VideoListView.TYPE_WITH_USER];
            objArr[VideoListView.TYPE_USER] = this.val$vf.title;
            Toast.makeText(context, resources.getString(C0436R.string.video_added, objArr), VideoListView.TYPE_USER).show();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(VideoListView.this.getContext(), C0436R.string.error, VideoListView.TYPE_USER).show();
        }
    }

    /* renamed from: com.vkontakte.android.VideoListView.5 */
    class C13835 implements VideoSearch.Callback {
        C13835() {
        }

        public void success(Vector<VideoFile> videos) {
            boolean z = true;
            int i = VideoListView.TYPE_USER;
            VideoListView.this.videos.addAll(videos);
            VideoListView.this.updateList();
            VideoListView.this.dataLoading = false;
            if (VideoListView.this.listWrap.getVisibility() == 8) {
                Global.showViewAnimated(VideoListView.this.bigProgress, false, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(VideoListView.this.listWrap, true, PhotoView.THUMB_ANIM_DURATION);
                VideoListView.this.scrollToTop();
            }
            VideoListView videoListView = VideoListView.this;
            if (videos.size() <= 0) {
                z = false;
            }
            videoListView.moreAvailable = z;
            View childAt = VideoListView.this.footerView.getChildAt(VideoListView.TYPE_USER);
            if (!VideoListView.this.moreAvailable) {
                i = 8;
            }
            childAt.setVisibility(i);
            VideoListView.this.currentRequest = null;
        }

        public void fail(int ecode, String emsg) {
            VideoListView.this.currentRequest = null;
            VideoListView.this.dataLoading = false;
            if (VideoListView.this.videos.size() == 0) {
                VideoListView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(VideoListView.this.bigProgress, false, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(VideoListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(VideoListView.this.getContext(), C0436R.string.error, VideoListView.TYPE_USER).show();
        }
    }

    /* renamed from: com.vkontakte.android.VideoListView.6 */
    class C13846 implements VideoGet.Callback {
        C13846() {
        }

        public void success(int total, Vector<VideoFile> videos) {
            int i;
            VideoListView.this.videos.addAll(videos);
            VideoListView.this.listVideos.addAll(videos);
            VideoListView.this.updateList();
            VideoListView.this.dataLoading = false;
            VideoListView.this.moreAvailable = total > VideoListView.this.videos.size();
            View childAt = VideoListView.this.footerView.getChildAt(VideoListView.TYPE_USER);
            if (VideoListView.this.moreAvailable) {
                i = VideoListView.TYPE_USER;
            } else {
                i = 8;
            }
            childAt.setVisibility(i);
            if (VideoListView.this.listWrap.getVisibility() == 8) {
                Global.showViewAnimated(VideoListView.this.bigProgress, false, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(VideoListView.this.listWrap, true, PhotoView.THUMB_ANIM_DURATION);
                VideoListView.this.scrollToTop();
            }
            VideoListView.this.currentRequest = null;
        }

        public void fail(int ecode, String emsg) {
            VideoListView.this.dataLoading = false;
            VideoListView.this.currentRequest = null;
            if (VideoListView.this.videos.size() == 0) {
                VideoListView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(VideoListView.this.bigProgress, false, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(VideoListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(VideoListView.this.getContext(), C0436R.string.error, VideoListView.TYPE_USER).show();
        }
    }

    /* renamed from: com.vkontakte.android.VideoListView.7 */
    class C13857 implements VideoGetUserVideos.Callback {
        C13857() {
        }

        public void success(int total, Vector<VideoFile> videos) {
            int i;
            VideoListView.this.videos.addAll(videos);
            VideoListView.this.listVideos.addAll(videos);
            VideoListView.this.updateList();
            VideoListView.this.dataLoading = false;
            VideoListView.this.moreAvailable = total > VideoListView.this.videos.size();
            Log.m528i("vk", "More available = " + VideoListView.this.moreAvailable);
            View childAt = VideoListView.this.footerView.getChildAt(VideoListView.TYPE_USER);
            if (VideoListView.this.moreAvailable) {
                i = VideoListView.TYPE_USER;
            } else {
                i = 8;
            }
            childAt.setVisibility(i);
            if (VideoListView.this.listWrap.getVisibility() == 8) {
                Global.showViewAnimated(VideoListView.this.bigProgress, false, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(VideoListView.this.listWrap, true, PhotoView.THUMB_ANIM_DURATION);
                VideoListView.this.scrollToTop();
            }
            VideoListView.this.currentRequest = null;
        }

        public void fail(int ecode, String emsg) {
            VideoListView.this.dataLoading = false;
            VideoListView.this.currentRequest = null;
            if (VideoListView.this.videos.size() == 0) {
                VideoListView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(VideoListView.this.bigProgress, false, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(VideoListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(VideoListView.this.getContext(), C0436R.string.error, VideoListView.TYPE_USER).show();
        }
    }

    /* renamed from: com.vkontakte.android.VideoListView.8 */
    class C13868 implements FaveGetVideos.Callback {
        C13868() {
        }

        public void success(int total, Vector<VideoFile> videos) {
            int i;
            VideoListView.this.videos.addAll(videos);
            VideoListView.this.listVideos.addAll(videos);
            VideoListView.this.updateList();
            VideoListView.this.dataLoading = false;
            VideoListView videoListView = VideoListView.this;
            boolean z = videos.size() > 0 || total > VideoListView.this.videos.size();
            videoListView.moreAvailable = z;
            View childAt = VideoListView.this.footerView.getChildAt(VideoListView.TYPE_USER);
            if (VideoListView.this.moreAvailable) {
                i = VideoListView.TYPE_USER;
            } else {
                i = 8;
            }
            childAt.setVisibility(i);
            if (VideoListView.this.listWrap.getVisibility() == 8) {
                Global.showViewAnimated(VideoListView.this.bigProgress, false, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(VideoListView.this.listWrap, true, PhotoView.THUMB_ANIM_DURATION);
                VideoListView.this.scrollToTop();
            }
            VideoListView.this.currentRequest = null;
        }

        public void fail(int ecode, String emsg) {
            VideoListView.this.dataLoading = false;
            VideoListView.this.currentRequest = null;
            if (VideoListView.this.videos.size() == 0) {
                VideoListView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(VideoListView.this.bigProgress, false, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(VideoListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(VideoListView.this.getContext(), C0436R.string.error, VideoListView.TYPE_USER).show();
        }
    }

    private class VideoImagesAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.VideoListView.VideoImagesAdapter.1 */
        class C05391 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$pos;

            C05391(int i, Bitmap bitmap) {
                this.val$pos = i;
                this.val$bitmap = bitmap;
            }

            public void run() {
                View v = VideoListView.this.list.getChildAt(this.val$pos - VideoListView.this.list.getFirstVisiblePosition());
                if (v != null && v.findViewById(C0436R.id.album_thumb) != null) {
                    ((ImageView) v.findViewById(C0436R.id.album_thumb)).setImageBitmap(this.val$bitmap);
                }
            }
        }

        private VideoImagesAdapter() {
        }

        public int getItemCount() {
            return VideoListView.this.videos.size();
        }

        public int getImageCountForItem(int item) {
            return VideoListView.TYPE_WITH_USER;
        }

        public String getImageURL(int item, int image) {
            try {
                if (item < VideoListView.this.videos.size()) {
                    return ((VideoFile) VideoListView.this.videos.get(item)).urlBigThumb;
                }
                return null;
            } catch (Exception e) {
                return null;
            }
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            if (item >= VideoListView.this.list.getFirstVisiblePosition() && item <= VideoListView.this.list.getLastVisiblePosition()) {
                VideoListView.this.post(new C05391(item, bitmap));
            }
        }
    }

    public VideoListView(Context context, int uid, int type, String groupName, String groupPhoto, VideoViewCallback cb) {
        super(context);
        this.videos = new Vector();
        this.listVideos = new Vector();
        this.dataLoading = false;
        this.moreAvailable = true;
        this.searchQuery = ACRAConstants.DEFAULT_STRING_VALUE;
        this.localSearchQuery = ACRAConstants.DEFAULT_STRING_VALUE;
        this.firstLoad = true;
        this.currentRequest = null;
        this.numLocal = TYPE_USER;
        this.selectMode = false;
        this.itemHeight = TYPE_USER;
        this.searchHd = false;
        this.searchLength = TYPE_USER;
        this.albums = null;
        this.currentAlbum = TYPE_USER;
        this.needFullReload = true;
        this.actionsClickListener = new C05331();
        this.uid = uid;
        this.type = type;
        this.groupName = groupName;
        this.groupPhoto = groupPhoto;
        this.callback = cb;
        init();
    }

    public VideoListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.videos = new Vector();
        this.listVideos = new Vector();
        this.dataLoading = false;
        this.moreAvailable = true;
        this.searchQuery = ACRAConstants.DEFAULT_STRING_VALUE;
        this.localSearchQuery = ACRAConstants.DEFAULT_STRING_VALUE;
        this.firstLoad = true;
        this.currentRequest = null;
        this.numLocal = TYPE_USER;
        this.selectMode = false;
        this.itemHeight = TYPE_USER;
        this.searchHd = false;
        this.searchLength = TYPE_USER;
        this.albums = null;
        this.currentAlbum = TYPE_USER;
        this.needFullReload = true;
        this.actionsClickListener = new C05331();
        init();
    }

    public VideoListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.videos = new Vector();
        this.listVideos = new Vector();
        this.dataLoading = false;
        this.moreAvailable = true;
        this.searchQuery = ACRAConstants.DEFAULT_STRING_VALUE;
        this.localSearchQuery = ACRAConstants.DEFAULT_STRING_VALUE;
        this.firstLoad = true;
        this.currentRequest = null;
        this.numLocal = TYPE_USER;
        this.selectMode = false;
        this.itemHeight = TYPE_USER;
        this.searchHd = false;
        this.searchLength = TYPE_USER;
        this.albums = null;
        this.currentAlbum = TYPE_USER;
        this.needFullReload = true;
        this.actionsClickListener = new C05331();
        init();
    }

    private void init() {
        boolean z;
        this.footerView = new FrameLayout(getContext());
        ProgressBar pb = new ProgressBar(getContext());
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(Global.scale(BitmapDescriptorFactory.HUE_ORANGE), Global.scale(BitmapDescriptorFactory.HUE_ORANGE));
        lp.gravity = 17;
        pb.setLayoutParams(lp);
        pb.setVisibility(8);
        this.footerView.setPadding(Global.scale(7.0f), Global.scale(7.0f), Global.scale(7.0f), Global.scale(7.0f));
        this.footerView.addView(pb);
        setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        this.list = new GridView(getContext());
        GridView gridView = this.list;
        ListAdapter videoAdapter = new VideoAdapter();
        this.adapter = videoAdapter;
        gridView.setAdapter(videoAdapter);
        this.list.setOnItemClickListener(this);
        this.list.setCacheColorHint(getResources().getColor(C0436R.color.cards_bg));
        this.list.setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        this.list.setVerticalFadingEdgeEnabled(false);
        this.list.setOnItemClickListener(this);
        this.list.setVerticalSpacing(Global.scale(4.0f));
        this.list.setSelector(C0436R.drawable.highlight_post);
        this.list.setDrawSelectorOnTop(true);
        this.list.setPadding(Global.scale(4.0f), TYPE_USER, Global.scale(4.0f), Global.scale(10.0f));
        this.list.setClipToPadding(false);
        this.list.setScrollBarStyle(33554432);
        this.listWrap = new FrameLayout(getContext());
        this.listWrap.addView(this.list);
        this.imgWrapper = new ListImageLoaderWrapper(new VideoImagesAdapter(), this.list, this);
        this.bigProgress = new ProgressBar(getContext());
        FrameLayout.LayoutParams lp2 = new FrameLayout.LayoutParams(-2, -2);
        lp2.gravity = 17;
        this.bigProgress.setLayoutParams(lp2);
        this.bigProgress.setVisibility(TYPE_USER);
        addView(this.bigProgress);
        this.emptyView = EmptyView.create(getContext());
        this.emptyView.setText(this.uid == Global.uid ? C0436R.string.no_videos_me : C0436R.string.no_videos);
        EmptyView emptyView = this.emptyView;
        if ((this.uid == Global.uid || this.uid == 0) && this.type == 0) {
            z = true;
        } else {
            z = false;
        }
        emptyView.setButtonVisible(z);
        this.emptyView.setButtonText((int) C0436R.string.add_video);
        this.emptyView.setOnBtnClickListener(new C05342());
        this.listWrap.addView(this.emptyView);
        this.list.setEmptyView(this.emptyView);
        this.listWrap.setVisibility(8);
        addView(this.listWrap);
        if (this.type != TYPE_FAVE) {
            loadData();
        }
        this.error = (ErrorView) inflate(getContext(), C0436R.layout.error, null);
        addView(this.error);
        this.error.setVisibility(8);
        this.error.setOnRetryListener(new C05353());
    }

    public void onDetachedFromWindow() {
        if (this.currentRequest != null) {
            this.currentRequest.cancel();
        }
    }

    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        post(new C05364(w));
    }

    private void updateSizes(int sw) {
        int vis = this.list.getFirstVisiblePosition();
        int colCount = Math.round(((float) sw) / ((float) Global.scale(350.0f)));
        this.list.setNumColumns(colCount);
        this.itemHeight = Math.round(((float) ((sw / colCount) - Global.scale(4.0f))) / 1.5f);
        this.list.setSelection(vis);
    }

    public void setAlbum(int id) {
        boolean z = false;
        this.currentAlbum = id;
        this.type = TYPE_USER;
        this.videos.clear();
        this.listVideos.clear();
        updateList();
        this.bigProgress.setVisibility(TYPE_USER);
        this.listWrap.setVisibility(8);
        this.footerView.getChildAt(TYPE_USER).setVisibility(8);
        loadData();
        EmptyView emptyView = this.emptyView;
        if ((this.uid == Global.uid || this.uid == 0) && this.type == 0) {
            z = true;
        }
        emptyView.setButtonVisible(z);
    }

    public void setType(int type) {
        boolean z = false;
        this.type = type;
        this.videos.clear();
        this.listVideos.clear();
        updateList();
        this.bigProgress.setVisibility(TYPE_USER);
        this.listWrap.setVisibility(8);
        this.footerView.getChildAt(TYPE_USER).setVisibility(8);
        loadData();
        EmptyView emptyView = this.emptyView;
        if ((this.uid == Global.uid || this.uid == 0) && type == 0) {
            z = true;
        }
        emptyView.setButtonVisible(z);
    }

    public void loadData() {
        if (this.currentRequest != null) {
            this.currentRequest.cancel();
        }
        if (this.searchQuery.length() > 0) {
            this.dataLoading = true;
            this.currentRequest = new VideoSearch(this.searchQuery, this.videos.size(), 20, this.searchHd, this.searchLength).setCallback(new C13835()).exec((View) this);
            return;
        }
        if (this.type == 0) {
            this.dataLoading = true;
            this.currentRequest = new VideoGet(this.uid, this.videos.size(), 50, this.currentAlbum).setCallback(new C13846()).exec((View) this);
        }
        if (this.type == TYPE_WITH_USER) {
            this.dataLoading = true;
            this.currentRequest = new VideoGetUserVideos(this.uid, this.videos.size(), 50).setCallback(new C13857()).exec((View) this);
        }
        if (this.type == TYPE_FAVE) {
            this.dataLoading = true;
            this.currentRequest = new FaveGetVideos(this.videos.size(), 50).setCallback(new C13868()).exec((View) this);
        }
    }

    private void updateList() {
        ((Activity) getContext()).runOnUiThread(new C05379());
    }

    private void scrollToTop() {
        this.list.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
            public boolean onPreDraw() {
                VideoListView.this.list.getViewTreeObserver().removeOnPreDrawListener(this);
                VideoListView.this.list.setSelection(VideoListView.TYPE_USER);
                return true;
            }
        });
    }

    public void onScrolledToLastItem() {
        if (!this.dataLoading && this.moreAvailable && this.videos.size() > 0) {
            loadData();
        }
    }

    public void onScrollStarted() {
        Activity act = (Activity) getContext();
        if (act.getCurrentFocus() != null) {
            ((InputMethodManager) getContext().getSystemService("input_method")).hideSoftInputFromWindow(act.getCurrentFocus().getWindowToken(), TYPE_FAVE);
            act.getCurrentFocus().clearFocus();
        }
    }

    public void onScrollStopped() {
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        Intent intent;
        if (this.selectMode) {
            Activity a = (Activity) getContext();
            intent = new Intent();
            intent.putExtra("video", (Parcelable) this.videos.get(pos));
            a.setResult(-1, intent);
            a.finish();
        } else if (this.searchQuery == null || this.searchQuery.length() <= 0) {
            openVideoComments((VideoFile) this.videos.get(pos));
        } else {
            intent = new Intent(getContext(), NewVideoPlayerActivity.class);
            intent.putExtra("file", (Parcelable) this.videos.get(pos));
            getContext().startActivity(intent);
        }
    }

    private void openVideoComments(VideoFile vf) {
        if (vf.oid == Global.uid) {
            SharedPreferences prefs = getContext().getSharedPreferences(null, TYPE_USER);
            doOpenComments(prefs.getString("username", "DELETED"), prefs.getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE), vf);
        } else if (vf.ownerName != null) {
            doOpenComments(vf.ownerName, vf.ownerPhoto, vf);
        } else if (vf.oid == this.uid && this.groupName != null) {
            doOpenComments(this.groupName, this.groupPhoto, vf);
        } else if (vf.oid < 0) {
            int[] iArr = new int[TYPE_WITH_USER];
            iArr[TYPE_USER] = -vf.oid;
            new GroupsGetById(iArr).setCallback(new AnonymousClass11(vf)).wrapProgress(getContext()).exec((View) this);
        } else {
            ProgressDialog pdlg = new ProgressDialog(getContext());
            pdlg.setMessage(getResources().getString(C0436R.string.loading));
            pdlg.show();
            ArrayList<Integer> al = new ArrayList();
            al.add(Integer.valueOf(vf.oid));
            Friends.getUsers(al, new AnonymousClass12(pdlg, vf));
        }
    }

    public void globalSearch(String q) {
        if (q == null || q.length() == 0) {
            boolean z;
            this.emptyView.setText(this.uid == Global.uid ? C0436R.string.no_videos_me : C0436R.string.no_videos);
            EmptyView emptyView = this.emptyView;
            if ((this.uid == Global.uid || this.uid == 0) && this.type == 0) {
                z = true;
            } else {
                z = false;
            }
            emptyView.setButtonVisible(z);
        } else {
            this.emptyView.setButtonVisible(false);
            this.emptyView.setText((int) C0436R.string.nothing_found);
        }
        this.list.setSelection(TYPE_USER);
        this.searchQuery = q;
        this.videos.clear();
        this.listWrap.setVisibility(8);
        this.bigProgress.setVisibility(TYPE_USER);
        loadData();
    }

    private void doOpenComments(String name, String photo, VideoFile vf) {
        if (this.callback != null) {
            this.callback.openComments(vf, name, photo);
        }
    }

    private void deleteVideo(VideoFile vf) {
        new Builder(getContext()).setMessage(C0436R.string.delete_video_confirm).setTitle(C0436R.string.delete_video).setIcon(ACRAConstants.DEFAULT_DIALOG_ICON).setPositiveButton(C0436R.string.yes, new AnonymousClass13(vf)).setNegativeButton(C0436R.string.no, null).show();
    }

    private void doDeleteVideo(VideoFile vf) {
        new WallDelete(vf.oid, vf.vid, TYPE_FAVE).setCallback(new AnonymousClass14(vf)).wrapProgress(getContext()).exec((View) this);
    }

    private void copyLink(VideoFile vf) {
        ((ClipboardManager) getContext().getSystemService("clipboard")).setText("http://vk.com/video" + vf.oid + "_" + vf.vid);
        Toast.makeText(getContext(), C0436R.string.link_copied, TYPE_USER).show();
    }

    public void removeItem(int oid, int vid) {
        Iterator it = this.videos.iterator();
        while (it.hasNext()) {
            VideoFile f = (VideoFile) it.next();
            if (f.oid == oid && f.vid == vid) {
                this.videos.remove(f);
                updateList();
                return;
            }
        }
    }

    private void addVideo(VideoFile vf) {
        new VideoAdd(vf.oid, vf.vid).setCallback(new AnonymousClass15(vf)).exec((View) this);
    }

    public boolean onEditorAction(TextView view, int actID, KeyEvent event) {
        if (event == null || event.getAction() == 0) {
            String sq = view.getText().toString();
            if (!sq.equals(this.searchQuery)) {
                this.searchQuery = sq;
                this.numLocal = TYPE_USER;
                this.footerView.getChildAt(TYPE_USER).setVisibility(8);
                this.videos.clear();
                if (sq.length() == 0) {
                    this.videos.addAll(this.listVideos);
                    updateList();
                } else {
                    updateList();
                    this.emptyView.setVisibility(8);
                    this.bigProgress.setVisibility(TYPE_USER);
                    loadData();
                }
                ((InputMethodManager) getContext().getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), TYPE_USER);
            }
        }
        return true;
    }

    public void localSearch(String q) {
        this.localSearchQuery = q;
        this.videos.clear();
        this.videos.addAll(this.listVideos);
        updateList();
        this.searchQuery = ACRAConstants.DEFAULT_STRING_VALUE;
        scrollToTop();
    }

    public int getCount() {
        return this.listVideos.size();
    }

    public void onPause() {
        this.imgWrapper.deactivate();
    }

    public void onResume() {
        this.imgWrapper.activate();
    }
}
