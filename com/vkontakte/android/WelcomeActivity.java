package com.vkontakte.android;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import org.acra.ACRAConstants;

public class WelcomeActivity extends Activity {
    private View alertView;
    int currentSyncOption;
    boolean syncAll;
    boolean syncEnabled;
    boolean syncSettingsMode;

    /* renamed from: com.vkontakte.android.WelcomeActivity.1 */
    class C05401 implements OnClickListener {
        C05401() {
        }

        public void onClick(View v) {
            WelcomeActivity.this.setSyncOption(0);
        }
    }

    /* renamed from: com.vkontakte.android.WelcomeActivity.2 */
    class C05412 implements OnClickListener {
        C05412() {
        }

        public void onClick(View v) {
            WelcomeActivity.this.setSyncOption(1);
        }
    }

    /* renamed from: com.vkontakte.android.WelcomeActivity.3 */
    class C05423 implements OnClickListener {
        C05423() {
        }

        public void onClick(View v) {
            WelcomeActivity.this.setSyncOption(2);
        }
    }

    /* renamed from: com.vkontakte.android.WelcomeActivity.4 */
    class C05434 implements DialogInterface.OnClickListener {
        C05434() {
        }

        public void onClick(DialogInterface dialog, int which) {
            WelcomeActivity.this.saveSyncSettings();
            Intent intent = new Intent();
            intent.putExtra("option", WelcomeActivity.this.currentSyncOption);
            WelcomeActivity.this.setResult(-1, intent);
            WelcomeActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.WelcomeActivity.5 */
    class C05445 implements DialogInterface.OnClickListener {
        C05445() {
        }

        public void onClick(DialogInterface dialog, int which) {
            WelcomeActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.WelcomeActivity.6 */
    class C05456 implements OnCancelListener {
        C05456() {
        }

        public void onCancel(DialogInterface dialog) {
            WelcomeActivity.this.finish();
        }
    }

    public WelcomeActivity() {
        this.syncSettingsMode = false;
    }

    public void onCreate(Bundle b) {
        boolean z = true;
        super.onCreate(b);
        overridePendingTransition(0, 0);
        getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
        setContentView(new View(this));
        this.alertView = View.inflate(this, C0436R.layout.welcome, null);
        this.syncAll = getApplicationContext().getSharedPreferences(null, 0).getBoolean("sync_all", false);
        this.syncEnabled = false;
        AccountManager am = AccountManager.get(VKApplication.context);
        Account[] accounts = am.getAccountsByType(Auth.ACCOUNT_TYPE);
        if (accounts.length == 0) {
            am.addAccountExplicitly(new Account(VKApplication.context.getSharedPreferences(null, 0).getString("username", ACRAConstants.DEFAULT_STRING_VALUE), Auth.ACCOUNT_TYPE), null, null);
            accounts = new Account[]{account};
        }
        this.syncEnabled = ContentResolver.getSyncAutomatically(accounts[0], "com.android.contacts");
        RadioButton radioButton = (RadioButton) this.alertView.findViewById(C0436R.id.welcome_sync_opt1);
        boolean z2 = this.syncAll && this.syncEnabled;
        radioButton.setChecked(z2);
        radioButton = (RadioButton) this.alertView.findViewById(C0436R.id.welcome_sync_opt2);
        if (this.syncAll || !this.syncEnabled) {
            z2 = false;
        } else {
            z2 = true;
        }
        radioButton.setChecked(z2);
        radioButton = (RadioButton) this.alertView.findViewById(C0436R.id.welcome_sync_opt3);
        if (this.syncAll || this.syncEnabled) {
            z = false;
        }
        radioButton.setChecked(z);
        this.currentSyncOption = Auth.getCurrentSyncOption(this);
        this.alertView.findViewById(C0436R.id.welcome_sync1).setOnClickListener(new C05401());
        this.alertView.findViewById(C0436R.id.welcome_sync2).setOnClickListener(new C05412());
        this.alertView.findViewById(C0436R.id.welcome_sync3).setOnClickListener(new C05423());
        if (VERSION.SDK_INT < 11) {
            setTextColor(this.alertView);
        }
        new Builder(this).setTitle(C0436R.string.sett_sync).setView(this.alertView).setPositiveButton(C0436R.string.save, new C05434()).setNegativeButton(C0436R.string.close, new C05445()).setOnCancelListener(new C05456()).show();
    }

    private void setTextColor(View v) {
        if (v instanceof ViewGroup) {
            ViewGroup g = (ViewGroup) v;
            for (int i = 0; i < g.getChildCount(); i++) {
                setTextColor(g.getChildAt(i));
            }
        }
        if (v instanceof TextView) {
            TextView t = (TextView) v;
            t.setTextColor(t.getCurrentTextColor() | 16777215);
        }
    }

    private void saveSyncSettings() {
        try {
            AccountManager am = AccountManager.get(VKApplication.context);
            Account[] accounts = am.getAccountsByType(Auth.ACCOUNT_TYPE);
            if (accounts.length == 0) {
                am.addAccountExplicitly(new Account(VKApplication.context.getSharedPreferences(null, 0).getString("username", ACRAConstants.DEFAULT_STRING_VALUE), Auth.ACCOUNT_TYPE), null, null);
                accounts = new Account[]{account};
            }
            ContentResolver.setSyncAutomatically(accounts[0], "com.android.contacts", this.syncEnabled);
            getApplicationContext().getSharedPreferences(null, 0).edit().putBoolean("sync_all", this.syncAll).commit();
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    private void setSyncOption(int o) {
        boolean z;
        this.currentSyncOption = o;
        ((RadioButton) this.alertView.findViewById(C0436R.id.welcome_sync_opt1)).setChecked(o == 0);
        RadioButton radioButton = (RadioButton) this.alertView.findViewById(C0436R.id.welcome_sync_opt2);
        if (o == 1) {
            z = true;
        } else {
            z = false;
        }
        radioButton.setChecked(z);
        radioButton = (RadioButton) this.alertView.findViewById(C0436R.id.welcome_sync_opt3);
        if (o == 2) {
            z = true;
        } else {
            z = false;
        }
        radioButton.setChecked(z);
        switch (o) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                this.syncAll = true;
                this.syncEnabled = true;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                this.syncAll = false;
                this.syncEnabled = true;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                this.syncEnabled = false;
                this.syncAll = false;
            default:
        }
    }
}
