package com.vkontakte.android;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import java.io.DataOutputStream;
import java.io.IOException;

public class PollAttachment extends Attachment {
    public static final Creator<PollAttachment> CREATOR;
    public int oid;
    public int pid;
    public String question;

    /* renamed from: com.vkontakte.android.PollAttachment.1 */
    class C03961 implements Creator<PollAttachment> {
        C03961() {
        }

        public PollAttachment createFromParcel(Parcel in) {
            return new PollAttachment(in.readString(), in.readInt(), in.readInt());
        }

        public PollAttachment[] newArray(int size) {
            return new PollAttachment[size];
        }
    }

    public PollAttachment(String _question, int _oid, int _pid) {
        this.question = _question;
        this.oid = _oid;
        this.pid = _pid;
    }

    static {
        CREATOR = new C03961();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.question);
        dest.writeInt(this.oid);
        dest.writeInt(this.pid);
    }

    public View getFullView(Context context) {
        PollAttachView pv = new PollAttachView(context, this.oid, this.pid);
        pv.loadData();
        return pv;
    }

    public View getViewForList(Context context, View reuse) {
        View v;
        if (reuse == null) {
            v = Attachment.getReusableView(context, "common");
        } else {
            v = reuse;
        }
        ((ImageView) v.findViewById(C0436R.id.attach_icon)).setImageResource(C0436R.drawable.ic_attach_poll);
        ((TextView) v.findViewById(C0436R.id.attach_title)).setText(this.question);
        ((TextView) v.findViewById(C0436R.id.attach_subtitle)).setText(C0436R.string.attach_poll);
        v.setOnClickListener(null);
        v.setClickable(false);
        return v;
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(4);
        os.writeUTF(this.question);
        os.writeInt(this.oid);
        os.writeInt(this.pid);
    }

    public String toString() {
        return "poll" + this.oid + "_" + this.pid;
    }

    public LayoutParams getViewLayoutParams() {
        LayoutParams lp = new LayoutParams();
        lp.width = -1;
        lp.height = Global.scale(54.0f);
        return lp;
    }
}
