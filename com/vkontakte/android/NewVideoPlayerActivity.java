package com.vkontakte.android;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.media.CamcorderProfile;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.NativeProtocol;
import com.facebook.WebDialog;
import com.google.android.gms.games.GamesClient;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.VideoAdd;
import com.vkontakte.android.api.VideoFile;
import com.vkontakte.android.api.VideoGetById;
import com.vkontakte.android.api.VideoGetById.Callback;
import com.vkontakte.android.api.VideoReportStats;
import com.vkontakte.android.api.WallDelete;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.media.PlayerWrapper;
import com.vkontakte.android.media.PlayerWrapper.PlayerStateListener;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.ui.CircularProgressBar;
import com.vkontakte.android.ui.Fonts;
import com.vkontakte.android.ui.PhotoView;
import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;
import org.acra.ACRAConstants;

public class NewVideoPlayerActivity extends SherlockActivity implements PlayerStateListener, OnSeekBarChangeListener {
    private View actionBarView;
    private int addedID;
    private boolean adding;
    private Animation animBottomHide;
    private Animation animBottomShow;
    private Animation animTopHide;
    private Animation animTopShow;
    private CallStateReceiver callStateReceiver;
    private boolean canLike;
    private boolean completed;
    private int curQuality;
    private View endMenu;
    private VideoFile file;
    private boolean fillScreen;
    private boolean firstResize;
    private int height;
    private boolean ignoreNextVisibilityChange;
    private boolean incViewSent;
    private boolean isLiked;
    private boolean needRestartPlayer;
    private int numComments;
    private int numLikes;
    private boolean pausedByCall;
    private int playedTime;
    private PlayerWrapper player;
    private boolean playing;
    private ProgressBar progress;
    private boolean ready;
    private SeekBar seekBar;
    private boolean seeking;
    private View statusBG;
    private int statusBarHeight;
    private SurfaceView surface;
    long f186t;
    private TextureView texView;
    private ImageView thumb;
    private Timer timer;
    private int uiVisibility;
    private boolean uiVisible;
    private WakeLock wakelock;
    private int width;

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.14 */
    class AnonymousClass14 implements Runnable {
        private final /* synthetic */ int val$sec;

        AnonymousClass14(int i) {
            this.val$sec = i;
        }

        public void run() {
            if (!NewVideoPlayerActivity.this.seeking) {
                NewVideoPlayerActivity.this.seekBar.setProgress(this.val$sec);
            }
            ((TextView) NewVideoPlayerActivity.this.findViewById(C0436R.id.video_time1)).setText(String.format("%d:%02d", new Object[]{Integer.valueOf(this.val$sec / 60), Integer.valueOf(this.val$sec % 60)}));
        }
    }

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.15 */
    class AnonymousClass15 extends WebViewClient {
        private final /* synthetic */ boolean[] val$canShowProgress;

        AnonymousClass15(boolean[] zArr) {
            this.val$canShowProgress = zArr;
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            this.val$canShowProgress[0] = true;
            view.loadUrl(url);
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.16 */
    class AnonymousClass16 extends WebChromeClient {
        CustomViewCallback customCallback;
        View customView;
        private final /* synthetic */ boolean[] val$canShowProgress;
        private final /* synthetic */ CircularProgressBar val$progress;
        private final /* synthetic */ FrameLayout val$webWrap;
        private final /* synthetic */ WebView val$wv;

        AnonymousClass16(WebView webView, FrameLayout frameLayout, CircularProgressBar circularProgressBar, boolean[] zArr) {
            this.val$wv = webView;
            this.val$webWrap = frameLayout;
            this.val$progress = circularProgressBar;
            this.val$canShowProgress = zArr;
        }

        public void onShowCustomView(View view, CustomViewCallback callback) {
            Log.m525d("vk", "on show custom view");
            if (this.customView != null) {
                callback.onCustomViewHidden();
                return;
            }
            this.customView = view;
            this.customCallback = callback;
            this.val$wv.setVisibility(8);
            this.val$webWrap.addView(this.customView, new LayoutParams(-1, -1));
        }

        public void onHideCustomView() {
            Log.m525d("vk", "On hide custom view");
            if (this.customView != null && this.customCallback != null) {
                this.val$webWrap.removeView(this.customView);
                this.customCallback.onCustomViewHidden();
                this.customView = null;
                this.customCallback = null;
                this.val$wv.setVisibility(0);
            }
        }

        public void onProgressChanged(WebView view, int progr) {
            if (progr == 100 && this.val$progress.getVisibility() == 0) {
                Global.showViewAnimated(this.val$progress, false, PhotoView.THUMB_ANIM_DURATION);
                this.val$canShowProgress[0] = false;
            } else if (this.val$progress.getVisibility() == 8 && this.val$canShowProgress[0]) {
                this.val$progress.setVisibility(0);
            }
            this.val$progress.setProgress(((double) progr) / 100.0d);
        }

        public View getVideoLoadingProgressView() {
            ProgressBar progr = new ProgressBar(NewVideoPlayerActivity.this);
            progr.setIndeterminateDrawable(NewVideoPlayerActivity.this.getResources().getDrawable(C0436R.drawable.progress_light));
            progr.setBackgroundResource(C0436R.drawable.video_btn_bg_up);
            progr.setPadding(Global.scale(10.0f), Global.scale(10.0f), Global.scale(10.0f), Global.scale(10.0f));
            return progr;
        }
    }

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.19 */
    class AnonymousClass19 implements Runnable {
        private final /* synthetic */ int val$errCode;

        /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.19.1 */
        class C03281 implements OnClickListener {
            C03281() {
            }

            public void onClick(DialogInterface dialog, int which) {
                NewVideoPlayerActivity.this.finish();
            }
        }

        /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.19.2 */
        class C03292 implements OnCancelListener {
            C03292() {
            }

            public void onCancel(DialogInterface dialog) {
                NewVideoPlayerActivity.this.finish();
            }
        }

        AnonymousClass19(int i) {
            this.val$errCode = i;
        }

        public void run() {
            int stringR = -1;
            switch (this.val$errCode) {
                case BoardTopicsFragment.ORDER_UPDATED_ASC /*-1*/:
                    stringR = C0436R.string.video_err_network;
                    break;
                case ValidationActivity.VRESULT_NONE /*0*/:
                    stringR = C0436R.string.video_err_os;
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    stringR = C0436R.string.video_err_decode;
                    break;
            }
            if (stringR != -1) {
                new Builder(NewVideoPlayerActivity.this).setTitle(C0436R.string.error).setMessage(stringR).setPositiveButton(C0436R.string.ok, new C03281()).setOnCancelListener(new C03292()).show();
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.1 */
    class C03301 implements OnPreDrawListener {
        C03301() {
        }

        public boolean onPreDraw() {
            NewVideoPlayerActivity.this.statusBG.getViewTreeObserver().removeOnPreDrawListener(this);
            if (NewVideoPlayerActivity.this.actionBarView != null) {
                int[] loc = new int[2];
                NewVideoPlayerActivity.this.actionBarView.getLocationOnScreen(loc);
                NewVideoPlayerActivity.this.statusBG.setLayoutParams(new LayoutParams(-1, loc[1], 48));
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.2 */
    class C03322 implements View.OnClickListener {
        C03322() {
        }

        public void onClick(View v) {
            if (NewVideoPlayerActivity.this.player != null && NewVideoPlayerActivity.this.ready) {
                if (NewVideoPlayerActivity.this.completed) {
                    NewVideoPlayerActivity.this.replay();
                    return;
                }
                NewVideoPlayerActivity.this.hideUI(ACRAConstants.DEFAULT_SOCKET_TIMEOUT);
                if (!(NewVideoPlayerActivity.this.endMenu == null || NewVideoPlayerActivity.this.endMenu.getParent() == null)) {
                    ((ViewGroup) NewVideoPlayerActivity.this.endMenu.getParent()).removeView(NewVideoPlayerActivity.this.endMenu);
                }
                if (NewVideoPlayerActivity.this.playing) {
                    NewVideoPlayerActivity.this.player.pause();
                    ((ImageView) NewVideoPlayerActivity.this.findViewById(C0436R.id.video_btn)).setImageResource(C0436R.drawable.ic_video_play);
                } else {
                    NewVideoPlayerActivity.this.player.play();
                    ((ImageView) NewVideoPlayerActivity.this.findViewById(C0436R.id.video_btn)).setImageResource(C0436R.drawable.ic_video_pause);
                }
                NewVideoPlayerActivity.this.playing = !NewVideoPlayerActivity.this.playing;
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.3 */
    class C03333 implements View.OnClickListener {
        C03333() {
        }

        public void onClick(View v) {
            if (!NewVideoPlayerActivity.this.ready) {
                return;
            }
            if (NewVideoPlayerActivity.this.uiVisible) {
                NewVideoPlayerActivity.this.hideUI();
            } else {
                NewVideoPlayerActivity.this.showUI();
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.4 */
    class C03344 implements View.OnClickListener {
        C03344() {
        }

        public void onClick(View v) {
            if (NewVideoPlayerActivity.this.ready) {
                NewVideoPlayerActivity.this.hideUI(ACRAConstants.DEFAULT_SOCKET_TIMEOUT);
                NewVideoPlayerActivity.this.fillScreen = !NewVideoPlayerActivity.this.fillScreen;
                ((ImageView) NewVideoPlayerActivity.this.findViewById(C0436R.id.video_resize)).setImageResource(NewVideoPlayerActivity.this.fillScreen ? C0436R.drawable.ic_video_shrink : C0436R.drawable.ic_video_expand);
                NewVideoPlayerActivity.this.resize();
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.5 */
    class C03365 implements Runnable {
        private final /* synthetic */ View val$root;

        /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.5.1 */
        class C03351 implements OnSystemUiVisibilityChangeListener {
            C03351() {
            }

            public void onSystemUiVisibilityChange(int visibility) {
                NewVideoPlayerActivity.this.uiVisibility = visibility;
                Log.m528i("vk", "onSystemUiVisibilityChange " + visibility);
                if (Integer.parseInt(VERSION.SDK) >= 14) {
                    NewVideoPlayerActivity.this.resize();
                }
                if (NewVideoPlayerActivity.this.ignoreNextVisibilityChange) {
                    NewVideoPlayerActivity.this.ignoreNextVisibilityChange = false;
                } else if (visibility == 0) {
                    NewVideoPlayerActivity.this.showUI();
                }
            }
        }

        C03365(View view) {
            this.val$root = view;
        }

        public void run() {
            this.val$root.setOnSystemUiVisibilityChangeListener(new C03351());
        }
    }

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.6 */
    class C03376 implements View.OnClickListener {
        C03376() {
        }

        public void onClick(View v) {
            NewVideoPlayerActivity.this.replay();
        }
    }

    private class CallStateReceiver extends PhoneStateListener {
        private CallStateReceiver() {
        }

        public void onCallStateChanged(int state, String incomingNumber) {
            Log.m528i("vk", "call state is " + state + "; " + NewVideoPlayerActivity.this.playing);
            if (!(NewVideoPlayerActivity.this.pausedByCall || state == 0 || !NewVideoPlayerActivity.this.playing)) {
                NewVideoPlayerActivity.this.pausedByCall = true;
                NewVideoPlayerActivity.this.player.pause();
            }
            if (NewVideoPlayerActivity.this.pausedByCall && state == 0) {
                NewVideoPlayerActivity.this.pausedByCall = false;
                NewVideoPlayerActivity.this.player.play();
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.7 */
    class C13177 implements Callback {
        C13177() {
        }

        public void success(VideoFile video) {
            Log.m528i("vk", "Video file = " + video);
            if (video == null) {
                Toast.makeText(NewVideoPlayerActivity.this, C0436R.string.post_not_found, 0).show();
                NewVideoPlayerActivity.this.finish();
                return;
            }
            NewVideoPlayerActivity.this.file = video;
            NewVideoPlayerActivity.this.setTitle(NewVideoPlayerActivity.this.file.title);
            if (NewVideoPlayerActivity.this.findViewById(C0436R.id.video_root) != null) {
                ((TextView) NewVideoPlayerActivity.this.findViewById(C0436R.id.video_time2)).setText(String.format("%d:%02d", new Object[]{Integer.valueOf(NewVideoPlayerActivity.this.file.duration / 60), Integer.valueOf(NewVideoPlayerActivity.this.file.duration % 60)}));
                NewVideoPlayerActivity.this.seekBar.setMax(NewVideoPlayerActivity.this.file.duration);
                NewVideoPlayerActivity.this.showUI();
                NewVideoPlayerActivity.this.cancelHideUI();
            }
            NewVideoPlayerActivity.this.startFile();
        }

        public void fail(int ecode, String emsg) {
            NewVideoPlayerActivity.this.onError(-1);
        }
    }

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.8 */
    class C13188 implements VideoAdd.Callback {
        C13188() {
        }

        public void success(int vid) {
            NewVideoPlayerActivity.this.addedID = vid;
            NewVideoPlayerActivity.this.invalidateOptionsMenu();
            NewVideoPlayerActivity.this.hideUI(LocationStatusCodes.GEOFENCE_NOT_AVAILABLE);
            NewVideoPlayerActivity.this.adding = false;
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(NewVideoPlayerActivity.this, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
            NewVideoPlayerActivity.this.hideUI(LocationStatusCodes.GEOFENCE_NOT_AVAILABLE);
            NewVideoPlayerActivity.this.adding = false;
        }
    }

    /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.9 */
    class C13199 implements WallDelete.Callback {
        C13199() {
        }

        public void success() {
            NewVideoPlayerActivity.this.addedID = 0;
            NewVideoPlayerActivity.this.invalidateOptionsMenu();
            NewVideoPlayerActivity.this.hideUI(LocationStatusCodes.GEOFENCE_NOT_AVAILABLE);
            NewVideoPlayerActivity.this.adding = false;
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(NewVideoPlayerActivity.this, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
            NewVideoPlayerActivity.this.hideUI(LocationStatusCodes.GEOFENCE_NOT_AVAILABLE);
            NewVideoPlayerActivity.this.adding = false;
        }
    }

    public NewVideoPlayerActivity() {
        this.uiVisible = true;
        this.fillScreen = false;
        this.ready = false;
        this.ignoreNextVisibilityChange = false;
        this.pausedByCall = false;
        this.callStateReceiver = new CallStateReceiver();
        this.isLiked = false;
        this.canLike = true;
        this.needRestartPlayer = false;
        this.completed = false;
        this.uiVisibility = 1;
        this.firstResize = true;
        this.curQuality = 0;
        this.playedTime = 0;
        this.incViewSent = false;
        this.addedID = 0;
        this.adding = false;
        this.statusBarHeight = 0;
        this.f186t = System.currentTimeMillis();
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        setRequestedOrientation(VERSION.SDK_INT >= 9 ? 6 : 0);
        setVolumeControlStream(3);
        getWindow().setFlags(GLRenderBuffer.EGL_SURFACE_SIZE, GLRenderBuffer.EGL_SURFACE_SIZE);
        getWindow().addFlags(67108864);
        requestWindowFeature(9);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(-1442840576));
        setContentView((int) C0436R.layout.video_player);
        findViewById(C0436R.id.video_root).setBackgroundColor(Color.WINDOW_BACKGROUND_COLOR);
        int abId = Resources.getSystem().getIdentifier("action_bar_container", "id", "android");
        if (abId == 0) {
            abId = C0436R.id.abs__action_bar_container;
        }
        this.actionBarView = findViewById(abId);
        if (VERSION.SDK_INT >= 19) {
            this.statusBG = new View(this);
            this.statusBG.setBackgroundColor(-1442840576);
            this.statusBG.getViewTreeObserver().addOnPreDrawListener(new C03301());
            ViewUtils.setNoFitRecursive(getWindow().getDecorView());
        }
        if (getIntent().hasExtra("thumb")) {
            this.thumb = new ImageView(this);
            getWindow().getDecorView().getGlobalVisibleRect(new Rect());
            this.thumb.setLayoutParams(new LayoutParams(-1, -1));
            this.thumb.setScaleType(ScaleType.FIT_CENTER);
            this.thumb.setBackgroundColor(Color.WINDOW_BACKGROUND_COLOR);
            byte[] d = getIntent().getByteArrayExtra("thumb");
            this.thumb.setImageBitmap(BitmapFactory.decodeByteArray(d, 0, d.length));
            ((ViewGroup) findViewById(C0436R.id.video_root)).addView(this.thumb, 0);
            if (VERSION.SDK_INT >= 5) {
                overridePendingTransition(17432576, 17432577);
            }
        }
        this.progress = (ProgressBar) findViewById(C0436R.id.video_progress);
        this.seekBar = (SeekBar) findViewById(C0436R.id.video_seekbar);
        this.file = (VideoFile) getIntent().getParcelableExtra("file");
        ActivityUtils.setBeamLink(this, "video" + this.file.oid + "_" + this.file.vid);
        this.animBottomHide = AnimationUtils.loadAnimation(this, C0436R.anim.slide_bottom_out);
        this.animBottomHide.setDuration(300);
        this.animTopHide = AnimationUtils.loadAnimation(this, C0436R.anim.slide_top_out);
        this.animTopHide.setDuration(300);
        this.animBottomShow = AnimationUtils.loadAnimation(this, C0436R.anim.slide_bottom_in);
        this.animBottomShow.setDuration(300);
        this.animTopShow = AnimationUtils.loadAnimation(this, C0436R.anim.slide_top_in);
        this.animTopShow.setDuration(300);
        this.seekBar.setOnSeekBarChangeListener(this);
        findViewById(C0436R.id.video_btn).setOnClickListener(new C03322());
        findViewById(C0436R.id.video_click).setOnClickListener(new C03333());
        findViewById(C0436R.id.video_resize).setOnClickListener(new C03344());
        setTitle(this.file.title);
        ((TextView) findViewById(C0436R.id.video_time2)).setText(String.format("%d:%02d", new Object[]{Integer.valueOf(this.file.duration / 60), Integer.valueOf(this.file.duration % 60)}));
        this.seekBar.setMax(this.file.duration);
        this.wakelock = ((PowerManager) getSystemService("power")).newWakeLock(10, "VkVideoPlayer");
        this.wakelock.acquire();
        Intent intent = new Intent(AudioPlayerService.B_ACTION_PLAYER_CONTROL);
        intent.putExtra("action", 7);
        sendBroadcast(intent);
        if (Integer.parseInt(VERSION.SDK) >= 11) {
            View root = findViewById(C0436R.id.video_root);
            root.post(new C03365(root));
        }
        ((TelephonyManager) getSystemService("phone")).listen(this.callStateReceiver, 32);
        this.endMenu = View.inflate(this, C0436R.layout.video_end_menu, null);
        this.endMenu.findViewById(C0436R.id.vbtn_replay).setOnClickListener(new C03376());
        ((TextView) findViewById(C0436R.id.video_time1)).setTypeface(Fonts.getRobotoLightItalic());
        ((TextView) findViewById(C0436R.id.video_time2)).setTypeface(Fonts.getRobotoLightItalic());
        if ((this.file.url240 == null || this.file.url240.length() == 0) && (this.file.urlExternal == null || this.file.urlExternal.length() == 0)) {
            findViewById(C0436R.id.video_bottombar).setVisibility(8);
            if (this.actionBarView != null) {
                this.actionBarView.setVisibility(8);
            } else {
                getSupportActionBar().hide();
            }
            this.uiVisible = false;
            new VideoGetById(this.file.oid, this.file.vid, this.file.accessKey).setCallback(new C13177()).exec((Activity) this);
        } else {
            startFile();
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void replay() {
        this.completed = false;
        if (!(this.thumb == null || this.thumb.getParent() == null)) {
            ((ViewGroup) this.thumb.getParent()).removeView(this.thumb);
        }
        if (this.player != null && this.ready) {
            hideUI(ACRAConstants.DEFAULT_SOCKET_TIMEOUT);
            if (this.seekBar.getProgress() == this.seekBar.getMax()) {
                this.seekBar.setProgress(0);
                this.player.seek(0);
            }
            if (!(this.endMenu == null || this.endMenu.getParent() == null)) {
                ((ViewGroup) this.endMenu.getParent()).removeView(this.endMenu);
            }
            if (!this.needRestartPlayer || !this.player.isHardwareAccelerated()) {
                new VideoReportStats(this.file.oid, this.file.vid, this.curQuality, true).exec();
            } else if (this.surface != null) {
                ((ViewGroup) this.surface.getParent()).removeView(this.surface);
                this.surface = new SurfaceView(this);
                this.surface.setBackgroundColor(Color.WINDOW_BACKGROUND_COLOR);
                this.surface.setZOrderMediaOverlay(true);
                ((ViewGroup) findViewById(C0436R.id.video_root)).addView(this.surface, 1);
                this.player.stopAndRelease();
                String ds = this.player.getDataSource();
                this.player = new PlayerWrapper((Context) this, this.surface.getHolder());
                this.player.setListener(this);
                this.player.init(ds);
            }
            this.needRestartPlayer = false;
            this.player.play();
            ((ImageView) findViewById(C0436R.id.video_btn)).setImageResource(C0436R.drawable.ic_video_pause);
            this.playing = true;
            this.incViewSent = false;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean z;
        boolean z2 = false;
        getSupportMenuInflater().inflate(C0436R.menu.video_player, menu);
        MenuItem findItem = menu.findItem(C0436R.id.add);
        if (this.file.oid != Global.uid) {
            z = true;
        } else {
            z = false;
        }
        findItem.setVisible(z);
        menu.findItem(C0436R.id.add).setIcon(this.addedID != 0 ? C0436R.drawable.ic_ab_done : C0436R.drawable.ic_ab_add);
        MenuItem findItem2 = menu.findItem(C0436R.id.report);
        if (this.file.oid != Global.uid) {
            z2 = true;
        }
        findItem2.setVisible(z2);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            onBackPressed();
            return true;
        }
        if (item.getItemId() == C0436R.id.add && !this.adding) {
            addVideo();
        }
        if (item.getItemId() == C0436R.id.report) {
            Intent intent = new Intent(this, ReportContentActivity.class);
            intent.putExtra("itemID", this.file.vid);
            intent.putExtra("ownerID", this.file.oid);
            intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, "video");
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void addVideo() {
        this.adding = true;
        cancelHideUI();
        if (this.addedID == 0) {
            new VideoAdd(this.file.oid, this.file.vid).setCallback(new C13188()).exec((Activity) this);
        } else {
            new WallDelete(Global.uid, this.addedID, 2).setCallback(new C13199()).exec((Activity) this);
        }
    }

    private void startFile() {
        if (this.file.url240 == null && this.file.url360 == null && this.file.url480 == null) {
            if (this.file.urlExternal != null && this.file.urlExternal.startsWith("http://www.youtube.com/")) {
                openYoutubeApp(this.file.urlExternal);
            } else if (this.file.urlExternal == null) {
                finish();
            } else {
                Log.m528i("vk", "ext=" + this.file.urlExternal + "; embed=" + this.file.urlEmbed);
                if (this.file.urlEmbed != null) {
                    playExternal(this.file.urlEmbed);
                } else {
                    startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse(this.file.urlExternal)), 100500);
                }
            }
        } else if (Integer.parseInt(VERSION.SDK) < 5) {
            onError(0);
        } else {
            String[] q = new String[]{"240", "360", "480", "720 (HD)"};
            int n = 1;
            int mq = getMaxQuality();
            if (this.file.url360 != null) {
                n = 1 + 1;
            }
            if (this.file.url480 != null && mq >= 480) {
                n++;
            }
            if (this.file.url720 != null && mq >= 720) {
                n++;
            }
            if (n <= 1 || Integer.parseInt(VERSION.SDK) < 8) {
                initPlayer(this.file.url240);
                return;
            }
            String[] items = new String[n];
            System.arraycopy(q, 0, items, 0, n);
            new Builder(this).setItems(items, new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    String url = NewVideoPlayerActivity.this.file.url240;
                    switch (which) {
                        case ValidationActivity.VRESULT_NONE /*0*/:
                            url = NewVideoPlayerActivity.this.file.url240;
                            break;
                        case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                            url = NewVideoPlayerActivity.this.file.url360;
                            break;
                        case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                            url = NewVideoPlayerActivity.this.file.url480;
                            break;
                        case Group.ADMIN_LEVEL_ADMIN /*3*/:
                            url = NewVideoPlayerActivity.this.file.url720;
                            break;
                    }
                    NewVideoPlayerActivity.this.curQuality = which;
                    NewVideoPlayerActivity.this.initPlayer(url);
                }
            }).setOnCancelListener(new OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    NewVideoPlayerActivity.this.finish();
                }
            }).setTitle(C0436R.string.video_select_quality).show();
        }
    }

    private int getMaxQuality() {
        if (VERSION.SDK_INT < 9) {
            return 360;
        }
        if (Camera.getNumberOfCameras() == 0) {
            return 720;
        }
        CameraInfo info = new CameraInfo();
        boolean hasBack = false;
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, info);
            if (info.facing == 0) {
                hasBack = true;
                break;
            }
        }
        if (!hasBack) {
            return 720;
        }
        try {
            CamcorderProfile profile = CamcorderProfile.get(1);
            if (profile.videoFrameHeight >= 720) {
                return 720;
            }
            if (profile.videoFrameHeight >= 480) {
                return 480;
            }
            return 360;
        } catch (Exception e) {
            return 720;
        }
    }

    public void initPlayer(String url) {
        if (VERSION.SDK_INT < 16 || url.endsWith(".flv") || PreferenceManager.getDefaultSharedPreferences(this).getBoolean("forceVideoSurface", false)) {
            this.f186t = System.currentTimeMillis();
            this.surface = new SurfaceView(this);
            this.surface.setZOrderMediaOverlay(true);
            ((ViewGroup) findViewById(C0436R.id.video_root)).addView(this.surface, 0);
            if (VERSION.SDK_INT >= 11 && !url.endsWith(".flv")) {
                SurfaceView bg = new SurfaceView(this);
                bg.getHolder().addCallback(new SurfaceHolder.Callback() {
                    public void surfaceDestroyed(SurfaceHolder holder) {
                    }

                    public void surfaceCreated(SurfaceHolder holder) {
                        Canvas c = holder.lockCanvas();
                        Paint paint = new Paint();
                        paint.setColor(Color.WINDOW_BACKGROUND_COLOR);
                        c.drawRect(new Rect(0, 0, NewVideoPlayerActivity.this.getResources().getDisplayMetrics().widthPixels, NewVideoPlayerActivity.this.getResources().getDisplayMetrics().heightPixels), paint);
                        holder.unlockCanvasAndPost(c);
                    }

                    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                        Canvas c = holder.lockCanvas();
                        Paint paint = new Paint();
                        paint.setColor(Color.WINDOW_BACKGROUND_COLOR);
                        c.drawRect(new Rect(0, 0, width, height), paint);
                        holder.unlockCanvasAndPost(c);
                    }
                });
                ((ViewGroup) findViewById(C0436R.id.video_root)).addView(bg, 0);
            }
            this.player = new PlayerWrapper((Context) this, this.surface.getHolder());
            this.player.init(url);
            this.player.setListener(this);
            return;
        }
        this.texView = new TextureView(this);
        this.texView.setScaleX(1.000001f);
        ((ViewGroup) findViewById(C0436R.id.video_root)).addView(this.texView, 0);
        this.player = new PlayerWrapper((Context) this, this.texView);
        this.player.init(url);
        this.player.setListener(this);
    }

    public void onPlayerReady(int w, int h) {
        this.width = w;
        this.height = h;
        runOnUiThread(new Runnable() {
            public void run() {
                if (!(NewVideoPlayerActivity.this.thumb == null || NewVideoPlayerActivity.this.thumb.getParent() == null)) {
                    ((ViewGroup) NewVideoPlayerActivity.this.thumb.getParent()).removeView(NewVideoPlayerActivity.this.thumb);
                }
                Log.m528i("vk", "VIDEO SIZE = " + NewVideoPlayerActivity.this.width + "x" + NewVideoPlayerActivity.this.height);
                NewVideoPlayerActivity.this.resize();
                NewVideoPlayerActivity.this.findViewById(C0436R.id.video_progress_wrap).setVisibility(8);
                NewVideoPlayerActivity.this.player.play();
                Log.m528i("vk", "t = " + (System.currentTimeMillis() - NewVideoPlayerActivity.this.f186t));
                new VideoReportStats(NewVideoPlayerActivity.this.file.oid, NewVideoPlayerActivity.this.file.vid, NewVideoPlayerActivity.this.curQuality, true).exec();
                ((ImageView) NewVideoPlayerActivity.this.findViewById(C0436R.id.video_btn)).setImageResource(C0436R.drawable.ic_video_pause);
                NewVideoPlayerActivity.this.showUI();
                NewVideoPlayerActivity.this.hideUI(GamesClient.STATUS_ACHIEVEMENT_UNLOCK_FAILURE);
                NewVideoPlayerActivity.this.playing = true;
            }
        });
        this.ready = true;
    }

    public void resize() {
        float k;
        if (this.texView != null) {
            int realScrW;
            int realScrH;
            float scaleY;
            float scaleX;
            int visibleW = getResources().getDisplayMetrics().widthPixels;
            int visibleH = getResources().getDisplayMetrics().heightPixels;
            if (VERSION.SDK_INT >= 17) {
                Point size = new Point();
                ((WindowManager) getSystemService("window")).getDefaultDisplay().getRealSize(size);
                realScrW = size.x;
                realScrH = size.y;
            } else {
                try {
                    WindowManager wm = (WindowManager) getSystemService("window");
                    Method mGetRawW = Display.class.getMethod("getRawWidth", new Class[0]);
                    Method mGetRawH = Display.class.getMethod("getRawHeight", new Class[0]);
                    Object[] objArr = new Object[0];
                    realScrW = ((Integer) mGetRawW.invoke(wm.getDefaultDisplay(), objArr)).intValue();
                    objArr = new Object[0];
                    realScrH = ((Integer) mGetRawH.invoke(wm.getDefaultDisplay(), objArr)).intValue();
                } catch (Exception e) {
                    realScrW = visibleW;
                    realScrH = visibleH;
                }
            }
            if (this.firstResize) {
                this.texView.setLayoutParams(new LayoutParams(realScrW, realScrH));
            }
            float sratio = ((float) visibleW) / ((float) visibleH);
            float vratio = ((float) this.width) / ((float) this.height);
            float f;
            if (this.fillScreen ? vratio > sratio : vratio < sratio) {
                k = ((float) realScrH) / ((float) this.height);
                f = (float) this.width;
                scaleY = 1.0f;
                scaleX = (r0 * k) / ((float) realScrW);
            } else {
                k = ((float) realScrW) / ((float) this.width);
                f = (float) this.height;
                scaleY = (r0 * k) / ((float) realScrH);
                scaleX = 1.0f;
            }
            Log.m528i("vk", "Video scale: " + scaleX + ", " + scaleY);
            if (this.firstResize) {
                this.texView.setTranslationX(0.0f);
                this.texView.setTranslationY(0.0f);
                this.texView.setScaleX(scaleX);
                this.texView.setScaleY(scaleY);
                this.firstResize = false;
            } else {
                ObjectAnimator animX = ObjectAnimator.ofFloat(this.texView, "scaleX", new float[]{scaleX});
                ObjectAnimator animY = ObjectAnimator.ofFloat(this.texView, "scaleY", new float[]{scaleY});
                AnimatorSet set = new AnimatorSet();
                AnimatorSet.Builder bldr = set.play(animX).with(animY);
                if (0.0f != 0.0f) {
                    bldr.with(ObjectAnimator.ofFloat(this.texView, "translationX", new float[]{0.0f}));
                }
                if (this.texView.getTranslationX() != 0.0f && 0.0f == 0.0f) {
                    bldr.with(ObjectAnimator.ofFloat(this.texView, "translationX", new float[]{0.0f}));
                }
                if (0.0f != 0.0f) {
                    bldr.with(ObjectAnimator.ofFloat(this.texView, "translationY", new float[]{0.0f}));
                }
                if (this.texView.getTranslationY() != 0.0f && 0.0f == 0.0f) {
                    bldr.with(ObjectAnimator.ofFloat(this.texView, "translationY", new float[]{0.0f}));
                }
                set.setDuration(300);
                set.setInterpolator(new DecelerateInterpolator());
                set.start();
            }
        }
        if (this.surface != null) {
            int scrW = findViewById(C0436R.id.video_root).getWidth();
            int scrH = findViewById(C0436R.id.video_root).getHeight();
            sratio = ((float) scrW) / ((float) scrH);
            vratio = ((float) this.width) / ((float) this.height);
            LayoutParams lp;
            if (this.fillScreen ? vratio > sratio : vratio < sratio) {
                k = ((float) scrH) / ((float) this.height);
                int w = (int) (((float) this.width) * k);
                lp = new LayoutParams((int) (((float) this.width) * k), scrH);
                lp.gravity = 17;
                this.surface.setLayoutParams(lp);
                return;
            }
            k = ((float) scrW) / ((float) this.width);
            int h = (int) (((float) this.height) * k);
            lp = new LayoutParams(scrW, (int) (((float) this.height) * k));
            lp.gravity = 17;
            this.surface.setLayoutParams(lp);
        }
    }

    public void onUpdatePlaybackPosition(int sec) {
        this.playedTime++;
        if (this.playedTime >= this.file.duration / 2 && !this.incViewSent) {
            this.incViewSent = true;
            new VideoReportStats(this.file.oid, this.file.vid, this.curQuality, false).exec();
        }
        runOnUiThread(new AnonymousClass14(sec));
    }

    private void openYoutubeApp(String url) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
        for (ResolveInfo ri : getPackageManager().queryIntentActivities(intent, NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST)) {
            if (ri.activityInfo.packageName.equals("com.google.android.youtube")) {
                intent.setClassName(ri.activityInfo.packageName, ri.activityInfo.name);
                break;
            }
        }
        startActivityForResult(intent, 100500);
        if (VERSION.SDK_INT >= 5) {
            overridePendingTransition(17432576, 17432577);
        }
    }

    private void playExternal(String url) {
        FrameLayout webWrap = new FrameLayout(this);
        WebView wv = new WebView(this);
        CircularProgressBar progress = new CircularProgressBar(this);
        boolean[] canShowProgress = new boolean[]{true};
        wv.setWebViewClient(new AnonymousClass15(canShowProgress));
        wv.setWebChromeClient(new AnonymousClass16(wv, webWrap, progress, canShowProgress));
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setPluginState(PluginState.ON);
        wv.setBackgroundColor(Color.WINDOW_BACKGROUND_COLOR);
        webWrap.setBackgroundColor(Color.WINDOW_BACKGROUND_COLOR);
        setContentView((View) webWrap);
        webWrap.addView(wv, new LayoutParams(-1, -1));
        webWrap.addView(progress, new LayoutParams(Global.scale(70.0f), Global.scale(70.0f), 17));
        progress.setVisibility(8);
        wv.loadUrl(url);
        getSupportActionBar().hide();
    }

    public boolean dispatchKeyEvent(KeyEvent ev) {
        if (ev.getKeyCode() != 4) {
            return super.dispatchKeyEvent(ev);
        }
        if (ev.getAction() != 1) {
            return true;
        }
        onBackPressed();
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        finish();
    }

    public void onDestroy() {
        super.onDestroy();
        this.wakelock.release();
        if (this.player != null) {
            this.player.stopAndRelease();
        }
        Intent intent = new Intent(AudioPlayerService.B_ACTION_PLAYER_CONTROL);
        intent.putExtra("action", 8);
        sendBroadcast(intent);
        new Timer().schedule(new TimerTask() {
            public void run() {
                System.exit(0);
            }
        }, 200);
        ((TelephonyManager) getSystemService("phone")).listen(this.callStateReceiver, 0);
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
        if (this.ready) {
            this.seeking = true;
            cancelHideUI();
        }
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        if (this.ready) {
            this.seeking = false;
            this.player.seek(seekBar.getProgress());
            hideUI(ACRAConstants.DEFAULT_SOCKET_TIMEOUT);
        }
    }

    public void onPlaybackCompleted() {
        if (this.file.repeat) {
            this.player.seek(0);
            this.player.play();
            return;
        }
        this.playing = false;
        this.completed = true;
        runOnUiThread(new Runnable() {
            public void run() {
                ((ImageView) NewVideoPlayerActivity.this.findViewById(C0436R.id.video_btn)).setImageResource(C0436R.drawable.ic_video_play);
                NewVideoPlayerActivity.this.showUI();
                NewVideoPlayerActivity.this.cancelHideUI();
                LayoutParams lp = new LayoutParams(-2, -2);
                lp.gravity = 17;
                NewVideoPlayerActivity.this.endMenu.setLayoutParams(lp);
                if (NewVideoPlayerActivity.this.thumb != null) {
                    try {
                        if (NewVideoPlayerActivity.this.thumb.getParent() != null) {
                            ((ViewGroup) NewVideoPlayerActivity.this.thumb.getParent()).removeView(NewVideoPlayerActivity.this.thumb);
                        }
                        ((ViewGroup) NewVideoPlayerActivity.this.findViewById(C0436R.id.video_root)).addView(NewVideoPlayerActivity.this.thumb, 2);
                    } catch (Exception e) {
                    }
                }
                if (NewVideoPlayerActivity.this.endMenu.getParent() == null) {
                    ((ViewGroup) NewVideoPlayerActivity.this.findViewById(C0436R.id.video_root)).addView(NewVideoPlayerActivity.this.endMenu);
                }
                AlphaAnimation aa = new AlphaAnimation(0.0f, 1.0f);
                aa.setDuration(350);
                NewVideoPlayerActivity.this.endMenu.startAnimation(aa);
            }
        });
    }

    public void onError(int errCode) {
        runOnUiThread(new AnonymousClass19(errCode));
    }

    public void onUpdateBuffered(int percent) {
        this.seekBar.setSecondaryProgress((int) ((((float) percent) / 100.0f) * ((float) this.file.duration)));
    }

    private void hideUI() {
        if (this.timer != null) {
            this.timer.cancel();
        }
        this.timer = null;
        if (this.uiVisible) {
            this.uiVisible = false;
            findViewById(C0436R.id.video_bottombar).startAnimation(this.animBottomHide);
            if (this.actionBarView != null) {
                this.actionBarView.startAnimation(this.animTopHide);
                if (this.statusBG != null) {
                    this.statusBG.startAnimation(this.animTopHide);
                }
            } else {
                getSupportActionBar().hide();
            }
            findViewById(C0436R.id.video_root).postDelayed(new Runnable() {
                public void run() {
                    NewVideoPlayerActivity.this.findViewById(C0436R.id.video_bottombar).setVisibility(4);
                    if (NewVideoPlayerActivity.this.actionBarView != null) {
                        NewVideoPlayerActivity.this.actionBarView.setVisibility(4);
                    }
                    if (NewVideoPlayerActivity.this.statusBG != null) {
                        NewVideoPlayerActivity.this.statusBG.setVisibility(4);
                    }
                }
            }, 300);
            if (Integer.parseInt(VERSION.SDK) >= 14) {
                findViewById(C0436R.id.video_root).postDelayed(new Runnable() {
                    public void run() {
                        NewVideoPlayerActivity.this.ignoreNextVisibilityChange = true;
                        try {
                            View.class.getMethod("setSystemUiVisibility", new Class[]{Integer.TYPE}).invoke(NewVideoPlayerActivity.this.findViewById(C0436R.id.video_root), new Object[]{Integer.valueOf(2)});
                        } catch (Exception e) {
                        }
                        NewVideoPlayerActivity.this.resize();
                    }
                }, 300);
            } else if (Integer.parseInt(VERSION.SDK) >= 11) {
                this.ignoreNextVisibilityChange = false;
                try {
                    View.class.getMethod("setSystemUiVisibility", new Class[]{Integer.TYPE}).invoke(findViewById(C0436R.id.video_root), new Object[]{Integer.valueOf(1)});
                } catch (Exception e) {
                }
            }
        }
    }

    private void hideUI(int delay) {
        if (this.timer != null) {
            this.timer.cancel();
        }
        this.timer = new Timer();
        this.timer.schedule(new TimerTask() {

            /* renamed from: com.vkontakte.android.NewVideoPlayerActivity.22.1 */
            class C03311 implements Runnable {
                C03311() {
                }

                public void run() {
                    NewVideoPlayerActivity.this.hideUI();
                }
            }

            public void run() {
                NewVideoPlayerActivity.this.runOnUiThread(new C03311());
            }
        }, (long) delay);
    }

    private void cancelHideUI() {
        if (this.timer != null) {
            this.timer.cancel();
        }
        this.timer = null;
    }

    private void showUI() {
        if (!this.uiVisible) {
            this.uiVisible = true;
            findViewById(C0436R.id.video_bottombar).setVisibility(0);
            findViewById(C0436R.id.video_bottombar).startAnimation(this.animBottomShow);
            if (this.actionBarView != null) {
                this.actionBarView.setVisibility(0);
                this.actionBarView.startAnimation(this.animTopShow);
            } else {
                getSupportActionBar().show();
            }
            if (Integer.parseInt(VERSION.SDK) >= 11) {
                this.ignoreNextVisibilityChange = true;
                try {
                    View.class.getMethod("setSystemUiVisibility", new Class[]{Integer.TYPE}).invoke(findViewById(C0436R.id.video_root), new Object[]{Integer.valueOf(0)});
                } catch (Exception e) {
                }
            }
            hideUI(ACRAConstants.DEFAULT_SOCKET_TIMEOUT);
        }
    }

    public void onPause() {
        super.onPause();
        if (this.completed) {
            this.needRestartPlayer = true;
        }
    }

    public void onEndOfBuffer() {
        runOnUiThread(new Runnable() {
            public void run() {
                NewVideoPlayerActivity.this.findViewById(C0436R.id.video_progress_wrap).setVisibility(0);
            }
        });
    }

    public void onPlaybackResumed() {
        runOnUiThread(new Runnable() {
            public void run() {
                NewVideoPlayerActivity.this.findViewById(C0436R.id.video_progress_wrap).setVisibility(8);
            }
        });
    }
}
