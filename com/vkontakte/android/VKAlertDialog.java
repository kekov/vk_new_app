package com.vkontakte.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;

public class VKAlertDialog extends AlertDialog {

    public static class Builder extends android.app.AlertDialog.Builder {
        boolean isCancelable;

        public Builder(Context ctx) {
            super(ctx);
            this.isCancelable = true;
            init(ctx);
        }

        public Builder(Context ctx, int theme) {
            super(ctx, theme);
            this.isCancelable = true;
            init(ctx);
        }

        private void init(Context ctx) {
        }

        public android.app.AlertDialog.Builder setCancelable(boolean c) {
            this.isCancelable = c;
            return super.setCancelable(c);
        }

        public AlertDialog show() {
            AlertDialog ad = super.show();
            if (this.isCancelable) {
                ad.setCanceledOnTouchOutside(true);
            }
            ad.getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
            return ad;
        }

        public AlertDialog create() {
            AlertDialog ad = super.create();
            if (this.isCancelable) {
                ad.setCanceledOnTouchOutside(true);
            }
            ad.getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
            return ad;
        }
    }

    public VKAlertDialog(Context context) {
        super(context);
    }

    public VKAlertDialog(Context context, int theme) {
        super(context, theme);
    }

    public VKAlertDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }
}
