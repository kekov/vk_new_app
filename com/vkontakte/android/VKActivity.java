package com.vkontakte.android;

import android.os.Bundle;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class VKActivity extends SherlockActivity {
    public void onCreate(Bundle b) {
        super.onCreate(b);
        getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
        if (!isTaskRoot() && getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        }
        onBackPressed();
        return true;
    }

    public void onPause() {
        super.onPause();
        AppStateTracker.onActivityPaused();
    }

    public void onResume() {
        super.onResume();
        AppStateTracker.onActivityResumed(this);
    }
}
