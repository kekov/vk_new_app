package com.vkontakte.android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.actionbarsherlock.app.SherlockActivity;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;

public class ValidationActivity extends SherlockActivity {
    public static final int VRESULT_CANCEL = 1;
    public static final int VRESULT_NONE = 0;
    public static final int VRESULT_RETRY = 2;
    public static int result;
    private boolean phoneEntered;
    private ProgressDialog progress;
    private WebView webView;

    /* renamed from: com.vkontakte.android.ValidationActivity.1 */
    class C05261 extends WebViewClient {
        C05261() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Uri uri = Uri.parse(url);
            if ("oauth.vk.com".equals(uri.getHost()) && "/blank.html".equals(uri.getPath())) {
                uri = Uri.parse(url.replace('#', '?'));
                if (ValidationActivity.this.getIntent().getBooleanExtra("return_result", false)) {
                    if (uri.getQueryParameter("success") == null || uri.getQueryParameter(WebDialog.DIALOG_PARAM_ACCESS_TOKEN) == null) {
                        ValidationActivity.this.setResult(ValidationActivity.VRESULT_NONE);
                    } else {
                        Intent intent = new Intent();
                        intent.putExtra(WebDialog.DIALOG_PARAM_ACCESS_TOKEN, uri.getQueryParameter(WebDialog.DIALOG_PARAM_ACCESS_TOKEN));
                        intent.putExtra("secret", uri.getQueryParameter("secret"));
                        intent.putExtra("user_id", Integer.parseInt(uri.getQueryParameter("user_id")));
                        ValidationActivity.this.setResult(-1, intent);
                    }
                    ValidationActivity.this.finish();
                } else if (uri.getQueryParameter("fail") != null) {
                    LongPollService.logOut(true);
                    ValidationActivity.result = ValidationActivity.VRESULT_CANCEL;
                    ValidationActivity.this.finish();
                } else if (uri.getQueryParameter("cancel") != null) {
                    ValidationActivity.result = ValidationActivity.VRESULT_CANCEL;
                    ValidationActivity.this.finish();
                } else if (uri.getQueryParameter("success") != null) {
                    if (uri.getQueryParameter(WebDialog.DIALOG_PARAM_ACCESS_TOKEN) != null) {
                        String token = uri.getQueryParameter(WebDialog.DIALOG_PARAM_ACCESS_TOKEN);
                        String secret = uri.getQueryParameter("secret");
                        int uid = Integer.parseInt(uri.getQueryParameter("user_id"));
                        if (uid != Global.uid) {
                            LongPollService.logOut(true);
                        }
                        Global.uid = uid;
                        Global.accessToken = token;
                        Global.secret = secret;
                        VKApplication.context.getSharedPreferences(null, ValidationActivity.VRESULT_NONE).edit().putInt("uid", Global.uid).putString("sid", Global.accessToken).putString("secret", Global.secret).putBoolean("new_auth", true).commit();
                    }
                    ValidationActivity.result = ValidationActivity.VRESULT_RETRY;
                    ValidationActivity.this.finish();
                }
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        public void onPageFinished(WebView webView, String url) {
            CookieSyncManager.getInstance().sync();
            Uri uri = Uri.parse(url);
            if (ValidationActivity.this.getIntent().hasExtra("phone") && "m.vk.com".equals(uri.getHost()) && "/restore".equals(uri.getPath()) && uri.getQueryParameter("rh") != null) {
                ValidationActivity.this.webView.loadUrl("javascript:document.getElementsByTagName('input')[0].value='" + ValidationActivity.this.getIntent().getStringExtra("phone") + "';void(0);");
                ValidationActivity.this.phoneEntered = true;
            }
        }
    }

    /* renamed from: com.vkontakte.android.ValidationActivity.2 */
    class C05272 extends WebChromeClient {
        C05272() {
        }

        public void onProgressChanged(WebView view, int progr) {
            boolean visible = progr < 100;
            if (visible == ValidationActivity.this.progress.isShowing()) {
                return;
            }
            if (visible) {
                ValidationActivity.this.progress.show();
            } else {
                ValidationActivity.this.progress.dismiss();
            }
        }
    }

    public ValidationActivity() {
        this.phoneEntered = false;
    }

    static {
        result = VRESULT_NONE;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        getSupportActionBar().hide();
        this.webView = new WebView(this);
        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().sync();
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setWebViewClient(new C05261());
        this.webView.setWebChromeClient(new C05272());
        this.webView.loadUrl(getIntent().getStringExtra(PlusShare.KEY_CALL_TO_ACTION_URL));
        setContentView(this.webView);
        this.progress = new ProgressDialog(this);
        this.progress.setMessage(getString(C0436R.string.loading));
    }

    public void finish() {
        super.finish();
    }

    public void onBackPressed() {
        if (result == 0 && !getIntent().getBooleanExtra("return_result", false)) {
            result = VRESULT_CANCEL;
        }
        super.onBackPressed();
    }
}
