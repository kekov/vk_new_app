package com.vkontakte.android;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.WebDialog;
import com.vkontakte.android.mediapicker.providers.GalleryPickerProvider;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;

public class SendSinglePhotoActivity extends SherlockActivity {
    private Bitmap bmp;
    private EditText edit;
    private ImageView image;
    private View sendBtn;

    /* renamed from: com.vkontakte.android.SendSinglePhotoActivity.1 */
    class C04661 implements OnClickListener {
        C04661() {
        }

        public void onClick(View v) {
            SendSinglePhotoActivity.this.send();
        }
    }

    /* renamed from: com.vkontakte.android.SendSinglePhotoActivity.2 */
    class C04672 implements Runnable {
        C04672() {
        }

        public void run() {
            ((InputMethodManager) SendSinglePhotoActivity.this.getSystemService("input_method")).showSoftInput(SendSinglePhotoActivity.this.edit, 1);
        }
    }

    /* renamed from: com.vkontakte.android.SendSinglePhotoActivity.3 */
    class C04693 implements Runnable {
        private final /* synthetic */ String val$uri;

        /* renamed from: com.vkontakte.android.SendSinglePhotoActivity.3.1 */
        class C04681 implements Runnable {
            C04681() {
            }

            public void run() {
                SendSinglePhotoActivity.this.image.setImageBitmap(SendSinglePhotoActivity.this.bmp);
                SendSinglePhotoActivity.this.image.setLayoutParams(new LayoutParams(-1, (int) ((((float) SendSinglePhotoActivity.this.image.getWidth()) / ((float) SendSinglePhotoActivity.this.bmp.getWidth())) * ((float) SendSinglePhotoActivity.this.bmp.getHeight()))));
            }
        }

        C04693(String str) {
            this.val$uri = str;
        }

        public void run() {
            try {
                Uri url = Uri.parse(this.val$uri);
                if (GalleryPickerProvider.STYLED_IMAGE_URI_SCHEME.equals(url.getScheme()) && GalleryPickerProvider.instance().getNeedUseStyledThumb(this.val$uri)) {
                    SendSinglePhotoActivity.this.bmp = GalleryPickerProvider.instance().getThumbnail(this.val$uri, new File(VKApplication.context.getCacheDir(), "thumbs/t" + APIRequest.md5(this.val$uri) + ".jpg").getAbsolutePath());
                } else {
                    if (GalleryPickerProvider.STYLED_IMAGE_URI_SCHEME.equals(url.getScheme())) {
                        url = GalleryPickerProvider.getOriginalUri(this.val$uri);
                    }
                    InputStream stream = SendSinglePhotoActivity.this.getContentResolver().openInputStream(url);
                    Options opts = new Options();
                    opts.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(stream, null, opts);
                    DisplayMetrics metrics = new DisplayMetrics();
                    SendSinglePhotoActivity.this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    int sample = Math.max(opts.outWidth, opts.outHeight) / Math.min(metrics.widthPixels, metrics.heightPixels);
                    ((FileInputStream) stream).getChannel().position(0);
                    opts = new Options();
                    opts.inSampleSize = sample;
                    SendSinglePhotoActivity.this.bmp = BitmapFactory.decodeStream(stream, null, opts);
                }
                SendSinglePhotoActivity.this.runOnUiThread(new C04681());
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView((int) C0436R.layout.send_photo);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(C0436R.drawable.bg_actionbar_black));
        this.edit = (EditText) findViewById(C0436R.id.photo_descr);
        this.image = (ImageView) findViewById(C0436R.id.photo_image);
        this.sendBtn = View.inflate(this, C0436R.layout.ab_done_right, null);
        this.sendBtn.setOnClickListener(new C04661());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (!getIntent().hasExtra("show_hint")) {
            findViewById(C0436R.id.photo_hint).setVisibility(8);
        }
        loadPhoto(getIntent().getStringExtra("file"));
        this.edit.requestFocus();
        this.edit.post(new C04672());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add((int) C0436R.string.send);
        item.setActionView(this.sendBtn);
        item.setShowAsAction(2);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            onBackPressed();
        }
        return true;
    }

    private void send() {
        finish();
        Intent intent = new Intent(this, UploaderService.class);
        intent.putExtra("file", getIntent().getStringExtra("file"));
        HashMap<String, String> params = (HashMap) getIntent().getSerializableExtra("req_params");
        params.put("text", this.edit.getText().toString());
        intent.putExtra("req_params", params);
        intent.putExtra("info", getIntent().getStringExtra("info"));
        intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 1);
        startService(intent);
    }

    private void loadPhoto(String uri) {
        new Thread(new C04693(uri)).start();
    }
}
