package com.vkontakte.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.api.AccountGetBanned;
import com.vkontakte.android.api.FaveGetLinks;
import com.vkontakte.android.api.FaveGetUsers;
import com.vkontakte.android.api.GroupsGetMembers;
import com.vkontakte.android.api.LikesGetList;
import com.vkontakte.android.api.LikesGetList.Callback;
import com.vkontakte.android.api.PlacesGetCheckinProfiles;
import com.vkontakte.android.api.PollsGetVoters;
import com.vkontakte.android.api.SubscriptionsGetFollowers;
import com.vkontakte.android.api.UsersGetSubscriptions;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.PhotoView;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class UserListView extends FrameLayout implements Listener, OnItemClickListener {
    public static final int TYPE_BLACKLIST = 8;
    public static final int TYPE_CHAT_MEMBERS = 2;
    public static final int TYPE_CHECKINS = 9;
    public static final int TYPE_FAVE = 4;
    public static final int TYPE_FAVE_LINKS = 7;
    public static final int TYPE_FOLLOWERS = 5;
    public static final int TYPE_GROUP_MEMBERS = 1;
    public static final int TYPE_LIKES = 0;
    public static final int TYPE_POLL_VOTERS = 6;
    public static final int TYPE_PREDEFINED = 3;
    public static final int TYPE_USER_SUBSCRIPTIONS = 10;
    private ListAdapter adapter;
    private Bundle args;
    private FrameLayout contentWrap;
    private APIRequest currentReq;
    protected boolean dataLoading;
    private int dataOffset;
    private EmptyView emptyView;
    private ErrorView error;
    protected FrameLayout footerView;
    protected boolean hasExtended;
    protected ListImageLoaderWrapper imgLoader;
    public ListView list;
    protected boolean moreAvailable;
    protected boolean preloadOnReady;
    protected Vector<UserProfile> preloadedUsers;
    protected boolean preloading;
    protected ProgressBar progress;
    private int type;
    protected Vector<UserProfile> users;

    /* renamed from: com.vkontakte.android.UserListView.1 */
    class C05221 implements OnClickListener {
        C05221() {
        }

        public void onClick(View v) {
            UserListView.this.error.setVisibility(UserListView.TYPE_BLACKLIST);
            UserListView.this.progress.setVisibility(UserListView.TYPE_LIKES);
            UserListView.this.loadData();
        }
    }

    protected class UserListAdapter extends BaseAdapter {
        protected UserListAdapter() {
        }

        public int getCount() {
            return UserListView.this.users.size();
        }

        public Object getItem(int pos) {
            return null;
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int pos, View view, ViewGroup group) {
            if (view == null) {
                view = UserListView.inflate(UserListView.this.getContext(), UserListView.this.hasExtended ? C0436R.layout.friend_list_item_subtitle : C0436R.layout.friend_list_item, null);
                if (UserListView.this.hasExtended) {
                    ((ImageView) view.findViewById(C0436R.id.flist_item_online)).setImageResource(C0436R.drawable.ic_online);
                }
            }
            view.setFocusable(false);
            UserProfile p = (UserProfile) UserListView.this.users.get(pos);
            ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(p.fullName);
            view.findViewById(C0436R.id.flist_item_online).setVisibility(p.online > 0 ? UserListView.TYPE_LIKES : UserListView.TYPE_BLACKLIST);
            ((ImageView) view.findViewById(C0436R.id.flist_item_online)).setImageResource(p.online == UserListView.TYPE_GROUP_MEMBERS ? C0436R.drawable.ic_online : C0436R.drawable.ic_online_mobile);
            if (UserListView.this.hasExtended) {
                ((TextView) view.findViewById(C0436R.id.flist_item_subtext)).setText(p.firstName);
            }
            if (UserListView.this.imgLoader.isAlreadyLoaded(p.photo)) {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageBitmap(UserListView.this.imgLoader.get(p.photo));
            } else {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(C0436R.drawable.user_placeholder);
            }
            return view;
        }
    }

    /* renamed from: com.vkontakte.android.UserListView.2 */
    class C13752 implements Callback {
        C13752() {
        }

        public void success(int count, Vector<UserProfile> users) {
            int i;
            boolean z = true;
            UserListView.this.currentReq = null;
            if (UserListView.this.preloading) {
                UserListView.this.preloadedUsers.addAll(users);
            } else if (users.size() > 50) {
                UserListView.this.users.addAll(users.subList(UserListView.TYPE_LIKES, 50));
                UserListView.this.preloadedUsers.addAll(users.subList(50, users.size()));
            } else {
                UserListView.this.users.addAll(users);
            }
            UserListView.this.preloading = false;
            if (UserListView.this.preloadOnReady) {
                UserListView.this.preloading = true;
                UserListView.this.preloadOnReady = false;
                UserListView.this.loadData();
            }
            UserListView.this.updateList();
            UserListView userListView = UserListView.this;
            if (UserListView.this.users.size() >= count) {
                z = false;
            }
            userListView.moreAvailable = z;
            UserListView.this.progress.setVisibility(UserListView.TYPE_BLACKLIST);
            View childAt = UserListView.this.footerView.getChildAt(UserListView.TYPE_LIKES);
            if (UserListView.this.moreAvailable) {
                i = UserListView.TYPE_LIKES;
            } else {
                i = UserListView.TYPE_BLACKLIST;
            }
            childAt.setVisibility(i);
            if (UserListView.this.contentWrap.getVisibility() != 0) {
                UserListView.this.contentWrap.setVisibility(UserListView.TYPE_LIKES);
            }
        }

        public void fail(int ecode, String emsg) {
            UserListView.this.currentReq = null;
            if (UserListView.this.users.size() == 0) {
                UserListView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(UserListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(UserListView.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(UserListView.this.getContext(), C0436R.string.err_text, UserListView.TYPE_LIKES).show();
        }
    }

    /* renamed from: com.vkontakte.android.UserListView.3 */
    class C13763 implements GroupsGetMembers.Callback {
        C13763() {
        }

        public void success(int count, Vector<UserProfile> users) {
            int i;
            boolean z = true;
            UserListView.this.currentReq = null;
            if (UserListView.this.preloading) {
                UserListView.this.preloadedUsers.addAll(users);
            } else if (users.size() > 50) {
                UserListView.this.users.addAll(users.subList(UserListView.TYPE_LIKES, 50));
                UserListView.this.preloadedUsers.addAll(users.subList(50, users.size()));
            } else {
                UserListView.this.users.addAll(users);
            }
            UserListView.this.preloading = false;
            if (UserListView.this.preloadOnReady) {
                UserListView.this.preloading = true;
                UserListView.this.preloadOnReady = false;
                UserListView.this.loadData();
            }
            UserListView.this.updateList();
            UserListView userListView = UserListView.this;
            if (UserListView.this.users.size() >= count) {
                z = false;
            }
            userListView.moreAvailable = z;
            UserListView.this.progress.setVisibility(UserListView.TYPE_BLACKLIST);
            View childAt = UserListView.this.footerView.getChildAt(UserListView.TYPE_LIKES);
            if (UserListView.this.moreAvailable) {
                i = UserListView.TYPE_LIKES;
            } else {
                i = UserListView.TYPE_BLACKLIST;
            }
            childAt.setVisibility(i);
            if (UserListView.this.contentWrap.getVisibility() != 0) {
                UserListView.this.contentWrap.setVisibility(UserListView.TYPE_LIKES);
            }
        }

        public void fail(int ecode, String emsg) {
            UserListView.this.currentReq = null;
            if (UserListView.this.users.size() == 0) {
                UserListView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(UserListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(UserListView.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(UserListView.this.getContext(), C0436R.string.err_text, UserListView.TYPE_LIKES).show();
        }
    }

    /* renamed from: com.vkontakte.android.UserListView.4 */
    class C13774 implements SubscriptionsGetFollowers.Callback {
        C13774() {
        }

        public void success(int count, Vector<UserProfile> users) {
            int i;
            boolean z = true;
            UserListView.this.currentReq = null;
            if (UserListView.this.preloading) {
                UserListView.this.preloadedUsers.addAll(users);
            } else if (users.size() > 50) {
                UserListView.this.users.addAll(users.subList(UserListView.TYPE_LIKES, 50));
                UserListView.this.preloadedUsers.addAll(users.subList(50, users.size()));
            } else {
                UserListView.this.users.addAll(users);
            }
            UserListView.this.preloading = false;
            if (UserListView.this.preloadOnReady) {
                UserListView.this.preloading = true;
                UserListView.this.preloadOnReady = false;
                UserListView.this.loadData();
            }
            UserListView.this.updateList();
            UserListView userListView = UserListView.this;
            if (UserListView.this.users.size() >= count) {
                z = false;
            }
            userListView.moreAvailable = z;
            UserListView.this.progress.setVisibility(UserListView.TYPE_BLACKLIST);
            View childAt = UserListView.this.footerView.getChildAt(UserListView.TYPE_LIKES);
            if (UserListView.this.moreAvailable) {
                i = UserListView.TYPE_LIKES;
            } else {
                i = UserListView.TYPE_BLACKLIST;
            }
            childAt.setVisibility(i);
            if (UserListView.this.contentWrap.getVisibility() != 0) {
                UserListView.this.contentWrap.setVisibility(UserListView.TYPE_LIKES);
            }
        }

        public void fail(int ecode, String emsg) {
            UserListView.this.currentReq = null;
            if (UserListView.this.users.size() == 0) {
                UserListView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(UserListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(UserListView.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(UserListView.this.getContext(), C0436R.string.err_text, UserListView.TYPE_LIKES).show();
        }
    }

    /* renamed from: com.vkontakte.android.UserListView.5 */
    class C13785 implements AccountGetBanned.Callback {
        C13785() {
        }

        public void success(int count, ArrayList<UserProfile> users) {
            boolean z = true;
            UserListView.this.currentReq = null;
            if (UserListView.this.preloading) {
                UserListView.this.preloadedUsers.addAll(users);
            } else if (users.size() > 50) {
                UserListView.this.users.addAll(users.subList(UserListView.TYPE_LIKES, 50));
                UserListView.this.preloadedUsers.addAll(users.subList(50, users.size()));
            } else {
                UserListView.this.users.addAll(users);
            }
            UserListView.this.preloading = false;
            if (UserListView.this.preloadOnReady) {
                UserListView.this.preloading = true;
                UserListView.this.preloadOnReady = false;
                UserListView.this.loadData();
            }
            UserListView.this.updateList();
            UserListView userListView = UserListView.this;
            if (UserListView.this.users.size() >= count && users.size() != 0) {
                z = false;
            }
            userListView.moreAvailable = z;
            UserListView.this.progress.setVisibility(UserListView.TYPE_BLACKLIST);
            UserListView.this.footerView.getChildAt(UserListView.TYPE_LIKES).setVisibility(UserListView.this.moreAvailable ? UserListView.TYPE_LIKES : UserListView.TYPE_BLACKLIST);
            if (UserListView.this.contentWrap.getVisibility() != 0) {
                UserListView.this.contentWrap.setVisibility(UserListView.TYPE_LIKES);
            }
        }

        public void fail(int ecode, String emsg) {
            UserListView.this.currentReq = null;
            UserListView.this.dataLoading = false;
            if (UserListView.this.users.size() == 0) {
                UserListView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(UserListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(UserListView.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(UserListView.this.getContext(), C0436R.string.err_text, UserListView.TYPE_LIKES).show();
        }
    }

    /* renamed from: com.vkontakte.android.UserListView.6 */
    class C13796 implements PollsGetVoters.Callback {
        C13796() {
        }

        public void success(int count, Vector<UserProfile> users) {
            int i;
            boolean z = true;
            UserListView.this.currentReq = null;
            if (UserListView.this.preloading) {
                UserListView.this.preloadedUsers.addAll(users);
            } else if (users.size() > 50) {
                UserListView.this.users.addAll(users.subList(UserListView.TYPE_LIKES, 50));
                UserListView.this.preloadedUsers.addAll(users.subList(50, users.size()));
            } else {
                UserListView.this.users.addAll(users);
            }
            UserListView.this.preloading = false;
            if (UserListView.this.preloadOnReady) {
                UserListView.this.preloading = true;
                UserListView.this.preloadOnReady = false;
                UserListView.this.loadData();
            }
            UserListView.this.updateList();
            UserListView userListView = UserListView.this;
            if (UserListView.this.users.size() >= count) {
                z = false;
            }
            userListView.moreAvailable = z;
            UserListView.this.progress.setVisibility(UserListView.TYPE_BLACKLIST);
            View childAt = UserListView.this.footerView.getChildAt(UserListView.TYPE_LIKES);
            if (UserListView.this.moreAvailable) {
                i = UserListView.TYPE_LIKES;
            } else {
                i = UserListView.TYPE_BLACKLIST;
            }
            childAt.setVisibility(i);
            if (UserListView.this.contentWrap.getVisibility() != 0) {
                UserListView.this.contentWrap.setVisibility(UserListView.TYPE_LIKES);
            }
        }

        public void fail(int ecode, String emsg) {
            UserListView.this.currentReq = null;
            UserListView.this.dataLoading = false;
            if (UserListView.this.users.size() == 0) {
                UserListView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(UserListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(UserListView.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(UserListView.this.getContext(), C0436R.string.err_text, UserListView.TYPE_LIKES).show();
        }
    }

    /* renamed from: com.vkontakte.android.UserListView.7 */
    class C13807 implements FaveGetUsers.Callback {
        C13807() {
        }

        public void success(List<UserProfile> users) {
            int i = UserListView.TYPE_BLACKLIST;
            UserListView.this.currentReq = null;
            UserListView.this.users.addAll(users);
            UserListView.this.preloading = false;
            if (UserListView.this.preloadOnReady) {
                UserListView.this.preloading = true;
                UserListView.this.preloadOnReady = false;
                UserListView.this.loadData();
            }
            UserListView.this.updateList();
            UserListView.this.moreAvailable = false;
            UserListView.this.progress.setVisibility(UserListView.TYPE_BLACKLIST);
            View childAt = UserListView.this.footerView.getChildAt(UserListView.TYPE_LIKES);
            if (UserListView.this.moreAvailable) {
                i = UserListView.TYPE_LIKES;
            }
            childAt.setVisibility(i);
            UserListView.this.dataLoading = false;
            if (UserListView.this.contentWrap.getVisibility() != 0) {
                UserListView.this.contentWrap.setVisibility(UserListView.TYPE_LIKES);
            }
        }

        public void fail(int ecode, String emsg) {
            UserListView.this.dataLoading = false;
            UserListView.this.currentReq = null;
            if (UserListView.this.users.size() == 0) {
                UserListView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(UserListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(UserListView.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(UserListView.this.getContext(), C0436R.string.err_text, UserListView.TYPE_LIKES).show();
        }
    }

    /* renamed from: com.vkontakte.android.UserListView.8 */
    class C13818 implements FaveGetLinks.Callback {
        C13818() {
        }

        public void success(List<UserProfile> users) {
            int i = UserListView.TYPE_BLACKLIST;
            UserListView.this.currentReq = null;
            UserListView.this.users.addAll(users);
            UserListView.this.preloading = false;
            if (UserListView.this.preloadOnReady) {
                UserListView.this.preloading = true;
                UserListView.this.preloadOnReady = false;
                UserListView.this.loadData();
            }
            UserListView.this.updateList();
            UserListView.this.moreAvailable = false;
            UserListView.this.progress.setVisibility(UserListView.TYPE_BLACKLIST);
            View childAt = UserListView.this.footerView.getChildAt(UserListView.TYPE_LIKES);
            if (UserListView.this.moreAvailable) {
                i = UserListView.TYPE_LIKES;
            }
            childAt.setVisibility(i);
            if (UserListView.this.contentWrap.getVisibility() != 0) {
                UserListView.this.contentWrap.setVisibility(UserListView.TYPE_LIKES);
            }
        }

        public void fail(int ecode, String emsg) {
            UserListView.this.currentReq = null;
            UserListView.this.dataLoading = false;
            if (UserListView.this.users.size() == 0) {
                UserListView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(UserListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(UserListView.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(UserListView.this.getContext(), C0436R.string.err_text, UserListView.TYPE_LIKES).show();
        }
    }

    /* renamed from: com.vkontakte.android.UserListView.9 */
    class C13829 implements PlacesGetCheckinProfiles.Callback {
        C13829() {
        }

        public void success(ArrayList<UserProfile> users, int newOffset, int count) {
            int i;
            boolean z = true;
            UserListView.this.currentReq = null;
            UserListView.this.dataOffset = newOffset;
            if (UserListView.this.preloading) {
                UserListView.this.preloadedUsers.addAll(users);
            } else if (users.size() > 25) {
                UserListView.this.users.addAll(users.subList(UserListView.TYPE_LIKES, 25));
                UserListView.this.preloadedUsers.addAll(users.subList(25, users.size()));
            } else {
                UserListView.this.users.addAll(users);
            }
            UserListView.this.preloading = false;
            if (UserListView.this.preloadOnReady) {
                UserListView.this.preloading = true;
                UserListView.this.preloadOnReady = false;
                UserListView.this.loadData();
            }
            UserListView.this.updateList();
            UserListView userListView = UserListView.this;
            if (UserListView.this.users.size() >= count) {
                z = false;
            }
            userListView.moreAvailable = z;
            UserListView.this.progress.setVisibility(UserListView.TYPE_BLACKLIST);
            View childAt = UserListView.this.footerView.getChildAt(UserListView.TYPE_LIKES);
            if (UserListView.this.moreAvailable) {
                i = UserListView.TYPE_LIKES;
            } else {
                i = UserListView.TYPE_BLACKLIST;
            }
            childAt.setVisibility(i);
            if (UserListView.this.contentWrap.getVisibility() != 0) {
                UserListView.this.contentWrap.setVisibility(UserListView.TYPE_LIKES);
            }
        }

        public void fail(int ecode, String emsg) {
            UserListView.this.currentReq = null;
            UserListView.this.dataLoading = false;
            if (UserListView.this.users.size() == 0) {
                UserListView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(UserListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(UserListView.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(UserListView.this.getContext(), C0436R.string.err_text, UserListView.TYPE_LIKES).show();
        }
    }

    private class UserPhotosAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.UserListView.UserPhotosAdapter.1 */
        class C05231 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C05231(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.flist_item_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private UserPhotosAdapter() {
        }

        public int getItemCount() {
            return UserListView.this.users.size();
        }

        public int getImageCountForItem(int item) {
            return UserListView.TYPE_GROUP_MEMBERS;
        }

        public String getImageURL(int item, int image) {
            return ((UserProfile) UserListView.this.users.get(item)).photo;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += UserListView.this.list.getHeaderViewsCount();
            if (item >= UserListView.this.list.getFirstVisiblePosition() && item <= UserListView.this.list.getLastVisiblePosition()) {
                UserListView.this.post(new C05231(UserListView.this.list.getChildAt(item - UserListView.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    public UserListView(Context context, int _type, Bundle args) {
        super(context);
        this.users = new Vector();
        this.preloadedUsers = new Vector();
        this.dataLoading = false;
        this.moreAvailable = false;
        this.preloading = false;
        this.preloadOnReady = false;
        this.hasExtended = false;
        this.dataOffset = TYPE_LIKES;
        this.type = _type;
        if (this.type == TYPE_FAVE_LINKS || (args != null && args.getBoolean("extended"))) {
            this.hasExtended = true;
        }
        this.args = args;
        init();
    }

    private void init() {
        this.footerView = new FrameLayout(getContext());
        ProgressBar pb = new ProgressBar(getContext());
        LayoutParams lp = new LayoutParams(Global.scale(BitmapDescriptorFactory.HUE_ORANGE), Global.scale(BitmapDescriptorFactory.HUE_ORANGE));
        lp.gravity = 17;
        pb.setLayoutParams(lp);
        pb.setVisibility(TYPE_BLACKLIST);
        this.footerView.setPadding(TYPE_LIKES, Global.scale(7.0f), TYPE_LIKES, Global.scale(7.0f));
        this.footerView.addView(pb);
        setBackgroundColor(-1);
        this.list = new ListView(getContext());
        this.imgLoader = new ListImageLoaderWrapper(new UserPhotosAdapter(), this.list, this);
        ListView listView = this.list;
        ListAdapter adapter = getAdapter();
        this.adapter = adapter;
        listView.setAdapter(adapter);
        this.list.setDivider(null);
        if (VERSION.SDK_INT <= TYPE_CHECKINS) {
            this.list.setCacheColorHint(-1);
            this.list.setBackgroundColor(-1);
        }
        this.list.setOnScrollListener(this.imgLoader);
        this.list.setSelector(C0436R.drawable.highlight);
        this.list.setPadding(getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding), TYPE_LIKES, getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding), TYPE_LIKES);
        this.list.setScrollBarStyle(33554432);
        this.list.setDivider(new ColorDrawable(-1710619));
        this.list.setDividerHeight(Global.scale(1.0f));
        this.list.setOnItemClickListener(this);
        this.contentWrap = new FrameLayout(getContext());
        this.contentWrap.addView(this.list);
        this.emptyView = EmptyView.create(getContext());
        EmptyView emptyView = this.emptyView;
        CharSequence string = (this.args == null || !this.args.containsKey("emptyText")) ? getResources().getString(C0436R.string.empty_list) : this.args.getString("emptyText");
        emptyView.setText(string);
        this.contentWrap.addView(this.emptyView);
        this.list.setEmptyView(this.emptyView);
        this.contentWrap.setVisibility(TYPE_BLACKLIST);
        addView(this.contentWrap);
        this.progress = new ProgressBar(getContext());
        LayoutParams lp2 = new LayoutParams(-2, -2);
        lp2.gravity = 17;
        this.progress.setLayoutParams(lp2);
        this.progress.setVisibility(TYPE_LIKES);
        addView(this.progress);
        this.error = (ErrorView) inflate(getContext(), C0436R.layout.error, null);
        addView(this.error);
        this.error.setVisibility(TYPE_BLACKLIST);
        this.error.setOnRetryListener(new C05221());
    }

    protected ListAdapter getAdapter() {
        return new UserListAdapter();
    }

    public void onScrolledToLastItem() {
        if ((this.dataLoading && !this.preloading) || !this.moreAvailable) {
            return;
        }
        if (this.preloading) {
            this.preloading = false;
            this.preloadOnReady = true;
        } else if (this.preloadedUsers.size() > 0) {
            this.users.addAll(this.preloadedUsers);
            updateList();
            this.preloadedUsers.clear();
            this.preloading = true;
            loadData();
        } else {
            loadData();
        }
    }

    public void onScrollStarted() {
    }

    public void onScrollStopped() {
    }

    public void onDetachedFromWindow() {
        if (this.currentReq != null) {
            this.currentReq.cancel();
        }
    }

    public void loadData() {
        if (!this.dataLoading) {
            if (this.type == 0) {
                this.currentReq = new LikesGetList(this.args.getInt("ltype", TYPE_LIKES), this.args.getInt("lptype", TYPE_LIKES), this.args.getInt("oid", TYPE_LIKES), this.args.getInt("item_id", TYPE_LIKES), this.users.size(), this.preloading ? 50 : 100, this.args.getBoolean("friends_only"), this.args.getString("filter")).setCallback(new C13752()).exec((View) this);
            }
            if (this.type == TYPE_GROUP_MEMBERS) {
                this.currentReq = new GroupsGetMembers(this.args.getInt("gid", TYPE_LIKES), this.users.size(), 100).setCallback(new C13763()).exec((View) this);
            }
            if (this.type == TYPE_PREDEFINED) {
                this.users.addAll(this.args.getParcelableArrayList("users"));
                updateList();
                this.moreAvailable = false;
                this.progress.setVisibility(TYPE_BLACKLIST);
                this.footerView.getChildAt(TYPE_LIKES).setVisibility(TYPE_BLACKLIST);
                if (this.contentWrap.getVisibility() != 0) {
                    this.contentWrap.setVisibility(TYPE_LIKES);
                }
            }
            if (this.type == TYPE_FOLLOWERS) {
                this.currentReq = new SubscriptionsGetFollowers(this.args.getInt("uid", TYPE_LIKES), this.users.size(), 100).setCallback(new C13774()).exec((View) this);
            }
            if (this.type == TYPE_BLACKLIST) {
                this.currentReq = new AccountGetBanned(this.users.size(), 100).setCallback(new C13785()).exec((View) this);
            }
            if (this.type == TYPE_POLL_VOTERS) {
                this.currentReq = new PollsGetVoters(this.args.getInt("oid", TYPE_LIKES), this.args.getInt("poll_id", TYPE_LIKES), this.args.getInt("answer_id", TYPE_LIKES), this.users.size(), 100).setCallback(new C13796()).exec((View) this);
            }
            if (this.type == TYPE_FAVE) {
                if (this.users.size() <= 0) {
                    this.dataLoading = true;
                    this.currentReq = new FaveGetUsers().setCallback(new C13807()).exec((View) this);
                } else {
                    return;
                }
            }
            if (this.type == TYPE_FAVE_LINKS) {
                this.currentReq = new FaveGetLinks().setCallback(new C13818()).exec((View) this);
            }
            if (this.type == TYPE_CHECKINS) {
                this.currentReq = new PlacesGetCheckinProfiles(this.args.getInt("place_id"), this.dataOffset, this.preloading ? 25 : 50).setCallback(new C13829()).exec((View) this);
            }
            if (this.type == TYPE_USER_SUBSCRIPTIONS) {
                this.currentReq = new UsersGetSubscriptions(this.args.getInt("uid", TYPE_LIKES), this.users.size(), this.preloading ? 50 : 100).setCallback(new UsersGetSubscriptions.Callback() {
                    public void success(ArrayList<UserProfile> users, int count) {
                        int i;
                        boolean z = true;
                        UserListView.this.currentReq = null;
                        if (UserListView.this.preloading) {
                            UserListView.this.preloadedUsers.addAll(users);
                        } else if (users.size() > 50) {
                            UserListView.this.users.addAll(users.subList(UserListView.TYPE_LIKES, 50));
                            UserListView.this.preloadedUsers.addAll(users.subList(50, users.size()));
                        } else {
                            UserListView.this.users.addAll(users);
                        }
                        UserListView.this.preloading = false;
                        if (UserListView.this.preloadOnReady) {
                            UserListView.this.preloading = true;
                            UserListView.this.preloadOnReady = false;
                            UserListView.this.loadData();
                        }
                        UserListView.this.updateList();
                        UserListView userListView = UserListView.this;
                        if (UserListView.this.users.size() >= count) {
                            z = false;
                        }
                        userListView.moreAvailable = z;
                        UserListView.this.progress.setVisibility(UserListView.TYPE_BLACKLIST);
                        View childAt = UserListView.this.footerView.getChildAt(UserListView.TYPE_LIKES);
                        if (UserListView.this.moreAvailable) {
                            i = UserListView.TYPE_LIKES;
                        } else {
                            i = UserListView.TYPE_BLACKLIST;
                        }
                        childAt.setVisibility(i);
                        if (UserListView.this.contentWrap.getVisibility() != 0) {
                            UserListView.this.contentWrap.setVisibility(UserListView.TYPE_LIKES);
                        }
                    }

                    public void fail(int ecode, String emsg) {
                        UserListView.this.currentReq = null;
                        UserListView.this.dataLoading = false;
                        if (UserListView.this.users.size() == 0) {
                            UserListView.this.error.setErrorInfo(ecode, emsg);
                            Global.showViewAnimated(UserListView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                            Global.showViewAnimated(UserListView.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                            return;
                        }
                        Toast.makeText(UserListView.this.getContext(), C0436R.string.err_text, UserListView.TYPE_LIKES).show();
                    }
                }).exec((View) this);
            }
        }
    }

    public void updateList() {
        ((BaseAdapter) this.adapter).notifyDataSetChanged();
        this.imgLoader.updateImages();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        pos -= this.list.getHeaderViewsCount();
        if (this.type == TYPE_FAVE_LINKS) {
            Intent intent = new Intent(getContext(), LinkRedirActivity.class);
            intent.setData(Uri.parse(((UserProfile) this.users.get(pos)).lastName));
            getContext().startActivity(intent);
            return;
        }
        Bundle args = new Bundle();
        args.putInt("id", ((UserProfile) this.users.get(pos)).uid);
        Navigate.to("ProfileFragment", args, (Activity) getContext());
    }

    public void invalidateList() {
        postDelayed(new Runnable() {
            public void run() {
                ((UserListAdapter) UserListView.this.adapter).notifyDataSetInvalidated();
            }
        }, 10);
    }

    public void onPause() {
        this.imgLoader.deactivate();
    }

    public void onResume() {
        this.imgLoader.activate();
    }
}
