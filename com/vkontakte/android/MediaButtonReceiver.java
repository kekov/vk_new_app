package com.vkontakte.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.media.TransportMediator;
import android.view.KeyEvent;

public class MediaButtonReceiver extends BroadcastReceiver {
    public static boolean receiveEvents;

    static {
        receiveEvents = true;
    }

    public void onReceive(Context context, Intent intent) {
        if (receiveEvents) {
            KeyEvent event = (KeyEvent) intent.getParcelableExtra("android.intent.extra.KEY_EVENT");
            Log.m525d("vk", "MEDIA BUTTON code=" + event.getKeyCode() + "; action=" + event.getAction());
            if (event.getAction() == 0 && AudioPlayerService.sharedInstance != null) {
                switch (event.getKeyCode()) {
                    case 79:
                    case 85:
                        AudioPlayerService.sharedInstance.togglePlayPause();
                    case 87:
                        AudioPlayerService.sharedInstance.nextTrack();
                    case 88:
                        AudioPlayerService.sharedInstance.prevTrack();
                    case TransportMediator.KEYCODE_MEDIA_PLAY /*126*/:
                        if (!AudioPlayerService.sharedInstance.isPlaying()) {
                            AudioPlayerService.sharedInstance.togglePlayPause();
                        }
                    case TransportMediator.KEYCODE_MEDIA_PAUSE /*127*/:
                        if (AudioPlayerService.sharedInstance.isPlaying()) {
                            AudioPlayerService.sharedInstance.togglePlayPause();
                        }
                    default:
                }
            }
        }
    }
}
