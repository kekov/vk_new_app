package com.vkontakte.android;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.Rect;
import android.graphics.RectF;
import android.widget.ImageView;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.util.Vector;

public class ImageLoader {
    public boolean isFinished;
    Vector<QueueEntry> f43q;
    private boolean running;
    Thread thread;
    private boolean waiting;

    /* renamed from: com.vkontakte.android.ImageLoader.1 */
    class C02771 implements Runnable {
        C02771() {
        }

        public void run() {
            ImageLoader.this.runThread();
        }
    }

    /* renamed from: com.vkontakte.android.ImageLoader.2 */
    class C02782 implements Runnable {
        private final /* synthetic */ Bitmap val$bmp;
        private final /* synthetic */ QueueEntry val$e;

        C02782(QueueEntry queueEntry, Bitmap bitmap) {
            this.val$e = queueEntry;
            this.val$bmp = bitmap;
        }

        public void run() {
            this.val$e.f42v.setImageBitmap(this.val$bmp);
        }
    }

    /* renamed from: com.vkontakte.android.ImageLoader.3 */
    class C02793 implements Runnable {
        private final /* synthetic */ Bitmap val$bmp;
        private final /* synthetic */ QueueEntry val$e;

        C02793(QueueEntry queueEntry, Bitmap bitmap) {
            this.val$e = queueEntry;
            this.val$bmp = bitmap;
        }

        public void run() {
            this.val$e.f42v.setImageBitmap(this.val$bmp);
        }
    }

    private static class QueueEntry {
        long id;
        boolean resize;
        String url;
        ImageView f42v;

        public QueueEntry(ImageView _v, String _url, boolean _resize, long _id) {
            this.f42v = _v;
            this.url = _url;
            this.resize = _resize;
            this.id = _id;
        }
    }

    public ImageLoader() {
        this.f43q = new Vector();
        this.running = false;
        this.waiting = false;
        this.isFinished = false;
    }

    public void start() {
        Thread thread = new Thread(new C02771());
        this.thread = thread;
        thread.start();
        this.thread.setPriority(1);
    }

    public void cancel() {
        this.f43q.clear();
    }

    public void pause() {
        this.waiting = true;
    }

    public void resume() {
        this.waiting = false;
    }

    public ImageLoader add(ImageView iv, boolean resize, long id) {
        this.f43q.add(new QueueEntry(iv, (String) iv.getTag(), resize, id));
        return this;
    }

    private void runThread() {
        this.running = true;
        while (this.f43q != null && this.f43q.size() != 0) {
            try {
                QueueEntry e = (QueueEntry) this.f43q.remove(0);
                if (e.url.startsWith("M|")) {
                    String[] mdUrls = e.url.split("\\|");
                    Bitmap[] bmps = new Bitmap[(mdUrls.length - 1)];
                    for (int i = 1; i < mdUrls.length; i++) {
                        bmps[i - 1] = ImageCache.get(mdUrls[i]);
                    }
                    e.f42v.post(new C02793(e, drawMultichatPhoto(bmps)));
                } else {
                    Bitmap bmp = ImageCache.get(e.url);
                    while (e.f42v.getHandler() == null) {
                        Thread.sleep(10);
                    }
                    e.f42v.postDelayed(new C02782(e, bmp), 0);
                }
                Thread.sleep(10);
                while (this.waiting) {
                    Thread.sleep(250);
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
        this.isFinished = true;
        this.running = false;
    }

    private static Bitmap drawMultichatPhoto(Bitmap[] bmps) {
        int bs = (int) (50.0f * Global.displayDensity);
        Bitmap bmp = Bitmap.createBitmap(bs, bs, Config.ARGB_8888);
        Canvas c = new Canvas(bmp);
        if (bmps.length == 2) {
            drawBitmapRounded(c, bmps[0], new Rect(bmps[0].getWidth() / 4, 0, (bmps[0].getWidth() / 4) * 3, bmps[0].getHeight()), new Rect(0, 0, bs / 2, bs));
            drawBitmapRounded(c, bmps[1], new Rect(bmps[1].getWidth() / 4, 0, (bmps[1].getWidth() / 4) * 3, bmps[1].getHeight()), new Rect((bs / 2) + 1, 0, bs, bs));
        } else if (bmps.length == 3) {
            drawBitmapRounded(c, bmps[0], new Rect(bmps[0].getWidth() / 4, 0, (bmps[0].getWidth() / 4) * 3, bmps[0].getHeight()), new Rect(0, 0, bs / 2, bs));
            drawBitmapRounded(c, bmps[1], new Rect(0, 0, bmps[1].getWidth(), bmps[1].getHeight()), new Rect((bs / 2) + 1, 0, bs, bs / 2));
            drawBitmapRounded(c, bmps[2], new Rect(0, 0, bmps[2].getWidth(), bmps[2].getHeight()), new Rect((bs / 2) + 1, (bs / 2) + 1, bs, bs));
        } else if (bmps.length == 4) {
            drawBitmapRounded(c, bmps[0], new Rect(0, 0, bmps[0].getWidth(), bmps[0].getHeight()), new Rect(0, 0, bs / 2, bs / 2));
            drawBitmapRounded(c, bmps[1], new Rect(0, 0, bmps[1].getWidth(), bmps[1].getHeight()), new Rect(0, (bs / 2) + 1, bs / 2, bs));
            drawBitmapRounded(c, bmps[2], new Rect(0, 0, bmps[2].getWidth(), bmps[2].getHeight()), new Rect((bs / 2) + 1, 0, bs, bs / 2));
            drawBitmapRounded(c, bmps[3], new Rect(0, 0, bmps[3].getWidth(), bmps[3].getHeight()), new Rect((bs / 2) + 1, (bs / 2) + 1, bs, bs));
        }
        return bmp;
    }

    private static void drawBitmapRounded(Canvas c, Bitmap bmp, Rect src, Rect dst) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        Path p = new Path();
        if (Global.displayDensity > 1.0f) {
            p.addRoundRect(new RectF(dst), 2.8f, 2.8f, Direction.CW);
        } else {
            p.addRoundRect(new RectF(dst), ImageViewHolder.PaddingSize, ImageViewHolder.PaddingSize, Direction.CW);
        }
        c.save();
        c.clipPath(p);
        c.drawBitmap(bmp, src, dst, paint);
        c.restore();
    }
}
