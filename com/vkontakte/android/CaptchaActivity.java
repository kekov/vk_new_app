package com.vkontakte.android;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.VKAlertDialog.Builder;

public class CaptchaActivity extends Activity {
    public static boolean isReady;
    public static String lastKey;
    private ImageView image;
    private EditText input;
    private ProgressBar progress;
    private String url;

    /* renamed from: com.vkontakte.android.CaptchaActivity.1 */
    class C02131 implements OnClickListener {
        C02131() {
        }

        public void onClick(DialogInterface dialog, int which) {
            CaptchaActivity.this.captchaDone();
        }
    }

    /* renamed from: com.vkontakte.android.CaptchaActivity.2 */
    class C02142 implements OnClickListener {
        C02142() {
        }

        public void onClick(DialogInterface dialog, int which) {
            CaptchaActivity.this.captchaCanceled();
        }
    }

    /* renamed from: com.vkontakte.android.CaptchaActivity.3 */
    class C02153 implements OnCancelListener {
        C02153() {
        }

        public void onCancel(DialogInterface dialog) {
            CaptchaActivity.this.captchaCanceled();
        }
    }

    /* renamed from: com.vkontakte.android.CaptchaActivity.4 */
    class C02164 implements Runnable {
        C02164() {
        }

        public void run() {
            ((InputMethodManager) CaptchaActivity.this.getSystemService("input_method")).showSoftInput(CaptchaActivity.this.input, 0);
        }
    }

    /* renamed from: com.vkontakte.android.CaptchaActivity.5 */
    class C02175 implements Runnable {
        C02175() {
        }

        public void run() {
            try {
                byte[] imdata = Global.getURL(CaptchaActivity.this.url);
                if (imdata != null) {
                    CaptchaActivity.this.displayImage(BitmapFactory.decodeByteArray(imdata, 0, imdata.length));
                }
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.CaptchaActivity.6 */
    class C02186 implements Runnable {
        private final /* synthetic */ Bitmap val$bitmap;

        C02186(Bitmap bitmap) {
            this.val$bitmap = bitmap;
        }

        public void run() {
            CaptchaActivity.this.image.setImageBitmap(this.val$bitmap);
            CaptchaActivity.this.progress.setVisibility(8);
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        overridePendingTransition(0, 0);
        setContentView(new View(this));
        LinearLayout layout = new LinearLayout(this);
        int padding = Global.scale(12.0f);
        int imageWidth = (int) (130.0f * Math.max(1.0f, Global.displayDensity));
        int imageHeight = (int) (50.0f * Math.max(1.0f, Global.displayDensity));
        layout.setPadding(padding, padding, padding, padding);
        layout.setOrientation(1);
        layout.setGravity(1);
        FrameLayout imgFrame = new FrameLayout(this);
        LayoutParams flParams = new LayoutParams(imageWidth, imageHeight);
        flParams.bottomMargin = padding;
        imgFrame.setLayoutParams(flParams);
        this.progress = new ProgressBar(this);
        FrameLayout.LayoutParams pbParams = new FrameLayout.LayoutParams(-2, -2);
        pbParams.gravity = 17;
        this.progress.setLayoutParams(pbParams);
        imgFrame.addView(this.progress);
        this.image = new ImageView(this);
        FrameLayout.LayoutParams ivParams = new FrameLayout.LayoutParams(-1, -1);
        ivParams.gravity = 17;
        this.image.setLayoutParams(ivParams);
        imgFrame.addView(this.image);
        layout.addView(imgFrame);
        this.input = new EditText(this);
        this.input.setInputType(176);
        this.input.setSingleLine(true);
        this.input.setLayoutParams(new LayoutParams(imageWidth, -2));
        layout.addView(this.input);
        this.url = getIntent().getStringExtra(PlusShare.KEY_CALL_TO_ACTION_URL);
        isReady = false;
        new Builder(this).setView(layout).setTitle(C0436R.string.captcha_hint).setPositiveButton(C0436R.string.ok, new C02131()).setNegativeButton(C0436R.string.cancel, new C02142()).setOnCancelListener(new C02153()).show();
        loadImage();
        this.input.requestFocus();
        this.input.postDelayed(new C02164(), 500);
    }

    private void loadImage() {
        new Thread(new C02175()).start();
    }

    private void displayImage(Bitmap bitmap) {
        runOnUiThread(new C02186(bitmap));
    }

    private void captchaDone() {
        lastKey = this.input.getText().toString();
        isReady = true;
        finish();
    }

    private void captchaCanceled() {
        lastKey = null;
        isReady = true;
        finish();
    }
}
