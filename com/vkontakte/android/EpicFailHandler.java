package com.vkontakte.android;

import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;

public class EpicFailHandler implements UncaughtExceptionHandler {
    UncaughtExceptionHandler defHandler;

    public EpicFailHandler() {
        this.defHandler = Thread.getDefaultUncaughtExceptionHandler();
    }

    public void uncaughtException(Thread thread, Throwable throwable) {
        try {
            File f = new File(Environment.getExternalStorageDirectory(), "VK_fail_" + System.currentTimeMillis() + ".txt");
            Writer result = new StringWriter();
            PrintWriter printWriter = new PrintWriter(result);
            throwable.printStackTrace(printWriter);
            String stacktrace = result.toString();
            printWriter.close();
            f.createNewFile();
            FileOutputStream os = new FileOutputStream(f);
            os.write(stacktrace.getBytes("UTF-8"));
            os.flush();
            os.close();
        } catch (Throwable x) {
            Log.m532w("vk", x);
        } finally {
            this.defHandler.uncaughtException(thread, throwable);
        }
    }
}
