package com.vkontakte.android;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.widget.RemoteViews;
import com.vkontakte.android.cache.AlbumArtRetriever;
import org.acra.ACRAConstants;

public class PlayerWidget extends AppWidgetProvider {
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        update(context, appWidgetManager);
    }

    public static void update(Context context, AppWidgetManager awm) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), C0436R.layout.widget_player);
        Intent intent = new Intent(context, AudioPlayerService.class);
        intent.setAction("PlayPause");
        intent.putExtra("action", 3);
        remoteViews.setOnClickPendingIntent(C0436R.id.w_player_play_pause, PendingIntent.getService(context, 0, intent, 0));
        intent = new Intent(context, AudioPlayerService.class);
        intent.setAction("Next");
        intent.putExtra("action", 5);
        remoteViews.setOnClickPendingIntent(C0436R.id.w_player_next, PendingIntent.getService(context, 0, intent, 0));
        intent = new Intent(context, AudioPlayerService.class);
        intent.setAction("Prev");
        intent.putExtra("action", 6);
        remoteViews.setOnClickPendingIntent(C0436R.id.w_player_prev, PendingIntent.getService(context, 0, intent, 0));
        intent = new Intent(context, AudioPlayerService.class);
        intent.setAction("Show");
        intent.putExtra("action", 4);
        intent.putExtra("from_notify", true);
        PendingIntent pendingShowPlayer = PendingIntent.getService(context, 0, intent, 0);
        intent = new Intent(context, FragmentWrapperActivity.class);
        intent.setAction("fdsafsafdsafs");
        intent.putExtra("class", "AudioListFragment");
        intent.putExtra("args", new Bundle());
        PendingIntent appPending = PendingIntent.getActivity(context, 0, intent, 0);
        if (AudioPlayerService.sharedInstance == null || AudioPlayerService.sharedInstance.getCurrentFile() == null) {
            remoteViews.setTextViewText(C0436R.id.w_player_title, ACRAConstants.DEFAULT_STRING_VALUE);
            remoteViews.setTextViewText(C0436R.id.w_player_artist, context.getResources().getString(C0436R.string.audio_widget_inactive));
            remoteViews.setBoolean(C0436R.id.w_player_artist, "setSingleLine", false);
            remoteViews.setOnClickPendingIntent(C0436R.id.w_player_wrap, appPending);
            remoteViews.setViewVisibility(C0436R.id.w_player_btns_wrap, 8);
            remoteViews.setImageViewResource(C0436R.id.w_player_cover, C0436R.drawable.awidget_small_placeholder);
            remoteViews.setImageViewResource(C0436R.id.w_player_bg, C0436R.drawable.transparent);
        } else {
            AudioFile f = AudioPlayerService.sharedInstance.getCurrentFile();
            if (f != null) {
                remoteViews.setOnClickPendingIntent(C0436R.id.w_player_title_wrap, pendingShowPlayer);
                remoteViews.setTextViewText(C0436R.id.w_player_artist, f.artist + " - ");
                remoteViews.setTextViewText(C0436R.id.w_player_title, f.title);
                Bitmap cover = AlbumArtRetriever.getCoverImage(f.aid, f.oid, 0);
                if (cover != null) {
                    Bitmap cv = Bitmap.createBitmap(200, 200, Config.ARGB_8888);
                    Canvas c = new Canvas(cv);
                    Paint paint = new Paint();
                    paint.setFilterBitmap(true);
                    c.drawBitmap(cover, null, new Rect(0, 0, c.getWidth(), c.getHeight()), paint);
                    remoteViews.setImageViewBitmap(C0436R.id.w_player_cover, cv);
                    try {
                        Bitmap bl = AlbumArtRetriever.getCoverImage(f.aid, f.oid, 2).copy(Config.ARGB_8888, true);
                        int[] pixels = new int[(bl.getWidth() * bl.getHeight())];
                        bl.getPixels(pixels, 0, bl.getWidth(), 0, 0, bl.getWidth(), bl.getHeight());
                        for (int i = 0; i < pixels.length; i++) {
                            pixels[i] = (pixels[i] & 16777215) | -1291845632;
                        }
                        bl.setPixels(pixels, 0, bl.getWidth(), 0, 0, bl.getWidth(), bl.getHeight());
                        remoteViews.setImageViewBitmap(C0436R.id.w_player_bg, bl);
                    } catch (Exception e) {
                    }
                } else {
                    remoteViews.setImageViewResource(C0436R.id.w_player_cover, C0436R.drawable.awidget_small_placeholder);
                    remoteViews.setImageViewResource(C0436R.id.w_player_bg, C0436R.drawable.transparent);
                }
            }
            remoteViews.setBoolean(C0436R.id.w_player_artist, "setSingleLine", true);
            remoteViews.setImageViewResource(C0436R.id.w_player_play_pause, AudioPlayerService.sharedInstance.isPlaying() ? C0436R.drawable.ic_audio_panel_pause : C0436R.drawable.ic_audio_panel_play);
            remoteViews.setViewVisibility(C0436R.id.w_player_btns_wrap, 0);
        }
        awm.updateAppWidget(new ComponentName(context, PlayerWidget.class), remoteViews);
    }
}
