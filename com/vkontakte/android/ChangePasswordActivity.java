package com.vkontakte.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.AccountChangePassword;
import com.vkontakte.android.api.AccountChangePassword.Callback;

public class ChangePasswordActivity extends Activity {
    private AlertDialog dlg;
    private TextWatcher textListener;
    private View view;

    /* renamed from: com.vkontakte.android.ChangePasswordActivity.1 */
    class C02191 implements TextWatcher {
        C02191() {
        }

        public void afterTextChanged(Editable arg0) {
            String old = ((TextView) ChangePasswordActivity.this.view.findViewById(C0436R.id.old_password)).getText().toString();
            String new1 = ((TextView) ChangePasswordActivity.this.view.findViewById(C0436R.id.new_password)).getText().toString();
            String new2 = ((TextView) ChangePasswordActivity.this.view.findViewById(C0436R.id.new_password2)).getText().toString();
            Button button = ChangePasswordActivity.this.dlg.getButton(-1);
            boolean z = old.length() > 0 && new1.length() > 0 && new2.length() > 0;
            button.setEnabled(z);
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    /* renamed from: com.vkontakte.android.ChangePasswordActivity.2 */
    class C02202 implements OnDismissListener {
        C02202() {
        }

        public void onDismiss(DialogInterface dialog) {
            ChangePasswordActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.ChangePasswordActivity.3 */
    class C02213 implements OnClickListener {
        C02213() {
        }

        public void onClick(View v) {
            String old = ((TextView) ChangePasswordActivity.this.view.findViewById(C0436R.id.old_password)).getText().toString();
            String new1 = ((TextView) ChangePasswordActivity.this.view.findViewById(C0436R.id.new_password)).getText().toString();
            String new2 = ((TextView) ChangePasswordActivity.this.view.findViewById(C0436R.id.new_password2)).getText().toString();
            if (old.length() >= 4) {
                if (!new1.equals(new2)) {
                    Toast.makeText(ChangePasswordActivity.this, C0436R.string.passwords_not_match, 0).show();
                } else if (new1.length() < 6) {
                    Toast.makeText(ChangePasswordActivity.this, C0436R.string.signup_pass_too_short, 0).show();
                } else {
                    ChangePasswordActivity.this.changePassword(old, new1);
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.ChangePasswordActivity.4 */
    class C12764 implements Callback {

        /* renamed from: com.vkontakte.android.ChangePasswordActivity.4.1 */
        class C02221 implements Runnable {
            C02221() {
            }

            public void run() {
                Toast.makeText(ChangePasswordActivity.this, C0436R.string.password_changed, 0).show();
                ChangePasswordActivity.this.finish();
            }
        }

        /* renamed from: com.vkontakte.android.ChangePasswordActivity.4.2 */
        class C02232 implements Runnable {
            private final /* synthetic */ int val$ecode;

            C02232(int i) {
                this.val$ecode = i;
            }

            public void run() {
                if (this.val$ecode == 15) {
                    Toast.makeText(ChangePasswordActivity.this, C0436R.string.old_password_incorrect, 0).show();
                } else {
                    Toast.makeText(ChangePasswordActivity.this, this.val$ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
                }
            }
        }

        C12764() {
        }

        public void success(String token, String secret) {
            Auth.setData(token, secret, Global.uid, true);
            ChangePasswordActivity.this.runOnUiThread(new C02221());
        }

        public void fail(int ecode, String emsg) {
            ChangePasswordActivity.this.runOnUiThread(new C02232(ecode));
        }
    }

    public ChangePasswordActivity() {
        this.textListener = new C02191();
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        this.view = View.inflate(this, C0436R.layout.change_password, null);
        ((TextView) this.view.findViewById(C0436R.id.old_password)).setTypeface(Typeface.DEFAULT);
        ((TextView) this.view.findViewById(C0436R.id.new_password)).setTypeface(Typeface.DEFAULT);
        ((TextView) this.view.findViewById(C0436R.id.new_password2)).setTypeface(Typeface.DEFAULT);
        ((TextView) this.view.findViewById(C0436R.id.old_password)).addTextChangedListener(this.textListener);
        ((TextView) this.view.findViewById(C0436R.id.new_password)).addTextChangedListener(this.textListener);
        ((TextView) this.view.findViewById(C0436R.id.new_password2)).addTextChangedListener(this.textListener);
        this.dlg = new Builder(this).setTitle(C0436R.string.change_password).setView(this.view).setPositiveButton(C0436R.string.ok, null).setNegativeButton(C0436R.string.cancel, null).show();
        this.dlg.setOnDismissListener(new C02202());
        this.dlg.getButton(-1).setEnabled(false);
        this.dlg.getButton(-1).setOnClickListener(new C02213());
    }

    private void changePassword(String old, String nw) {
        new AccountChangePassword(old, nw).setCallback(new C12764()).wrapProgress(this).exec();
    }
}
