package com.vkontakte.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.style.ImageSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.ImageCache.ProgressCallback;
import com.vkontakte.android.ImageCache.RequestWrapper;
import com.vkontakte.android.ui.XImageSpan;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.acra.ACRAConstants;
import org.acra.ErrorReporter;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONObject;

public class Global {
    public static final float FONT_SIZE_MULTIPLIER = 2.0f;
    public static String accessToken;
    public static boolean authOK;
    public static Typeface boldFont;
    public static float displayDensity;
    private static final char[] emojiChars;
    private static HashMap<String, String> emojiReplacements;
    private static Pattern entitiesPtn;
    public static int[] friendHints;
    private static HttpClient httpclient;
    public static boolean inited;
    public static boolean isTablet;
    public static LongPollService longPoll;
    public static int maxErrIdx;
    public static boolean maybeTablet;
    public static Bitmap myPhotoBitmap;
    public static String[] placeTypes;
    public static int realFriendCount;
    public static Typeface regFont;
    public static Resources res;
    public static String secret;
    public static int timeDiff;
    public static int uid;
    public static boolean useBitmapHack;

    /* renamed from: com.vkontakte.android.Global.1 */
    class C02761 implements AnimationListener {
        private final /* synthetic */ int val$duration;
        private final /* synthetic */ long val$t;
        private final /* synthetic */ View val$view;

        C02761(long j, int i, View view) {
            this.val$t = j;
            this.val$duration = i;
            this.val$view = view;
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            if (System.currentTimeMillis() - this.val$t >= ((long) (this.val$duration - 20))) {
                this.val$view.setVisibility(8);
                this.val$view.clearAnimation();
            }
        }
    }

    static {
        try {
            res = VKApplication.context.getResources();
            displayDensity = VKApplication.context.getResources().getDisplayMetrics().density;
            SharedPreferences prefs = VKApplication.context.getApplicationContext().getSharedPreferences(null, 0);
            if (prefs.contains("sid")) {
                uid = prefs.getInt("uid", 0);
                accessToken = prefs.getString("sid", null);
                secret = prefs.getString("secret", null);
                ErrorReporter.getInstance().putCustomData("vk_uid", new StringBuilder(String.valueOf(uid)).toString());
            }
            inited = true;
        } catch (Exception e) {
        }
        authOK = true;
        placeTypes = null;
        realFriendCount = -1;
        longPoll = null;
        maxErrIdx = 0;
        friendHints = null;
        inited = false;
        timeDiff = 0;
        useBitmapHack = false;
        entitiesPtn = null;
        emojiChars = new char[]{'\u00a9', '\u00ae', '\u203c', '\u2049', '\u2122', '\u2139', '\u2194', '\u2195', '\u2196', '\u2197', '\u2198', '\u2199', '\u21a9', '\u21aa', '\u231a', '\u231b', '\u23e9', '\u23ea', '\u23eb', '\u23ec', '\u23f0', '\u23f3', '\u24c2', '\u25aa', '\u25ab', '\u25b6', '\u25c0', '\u25fb', '\u25fc', '\u25fd', '\u25fe', '\u2600', '\u2601', '\u260e', '\u2611', '\u2614', '\u2615', '\u261d', '\u263a', '\u2648', '\u2649', '\u264a', '\u264b', '\u264c', '\u264d', '\u264e', '\u264f', '\u2650', '\u2651', '\u2652', '\u2653', '\u2660', '\u2663', '\u2665', '\u2666', '\u2668', '\u267b', '\u267f', '\u2693', '\u26a0', '\u26a1', '\u26aa', '\u26ab', '\u26bd', '\u26be', '\u26c4', '\u26c5', '\u26ce', '\u26d4', '\u26ea', '\u26f2', '\u26f3', '\u26f5', '\u26fa', '\u26fd', '\u2702', '\u2705', '\u2708', '\u2709', '\u270a', '\u270b', '\u270c', '\u270f', '\u2712', '\u2714', '\u2716', '\u2728', '\u2733', '\u2734', '\u2744', '\u2747', '\u274c', '\u274e', '\u2753', '\u2754', '\u2755', '\u2757', '\u2764', '\u2795', '\u2796', '\u2797', '\u27a1', '\u27b0', '\u27bf', '\u2934', '\u2935', '\u2b05', '\u2b06', '\u2b07', '\u2b1b', '\u2b1c', '\u2b50', '\u2b55', '\u3030', '\u303d', '\u3297', '\u3299'};
    }

    public static String langDate(Resources res, int _dt) {
        if (res == null) {
            res = VKApplication.context.getResources();
        }
        long dt = ((long) _dt) * 1000;
        Date dnow = new Date(System.currentTimeMillis());
        long daystart = ((System.currentTimeMillis() - ((long) (dnow.getHours() * 3600000))) - ((long) (dnow.getMinutes() * 60000))) - ((long) (dnow.getSeconds() * LocationStatusCodes.GEOFENCE_NOT_AVAILABLE));
        Date d = new Date(dt);
        String r = ACRAConstants.DEFAULT_STRING_VALUE;
        if (daystart < dt && 86400000 + daystart >= dt) {
            return String.format("%s %d:%02d", new Object[]{res.getString(C0436R.string.today_at), Integer.valueOf(d.getHours()), Integer.valueOf(d.getMinutes())});
        } else if (86400000 + daystart < dt && 172800000 + daystart > dt) {
            return String.format("%s %d:%02d", new Object[]{res.getString(C0436R.string.tomorrow_at), Integer.valueOf(d.getHours()), Integer.valueOf(d.getMinutes())});
        } else if (daystart - 86400000 >= dt || daystart < dt) {
            r = d.getDate() + " ";
            if (d.getYear() != dnow.getYear()) {
                r = new StringBuilder(String.valueOf(r)).append(res.getStringArray(C0436R.array.months_short)[d.getMonth()]).append(" ").append(d.getYear() + 1900).toString();
            } else {
                r = new StringBuilder(String.valueOf(r)).append(res.getStringArray(C0436R.array.months_full)[d.getMonth()]).toString();
            }
            return new StringBuilder(String.valueOf(r)).append(String.format(" %s %d:%02d", new Object[]{res.getString(C0436R.string.date_at), Integer.valueOf(d.getHours()), Integer.valueOf(d.getMinutes())})).toString();
        } else {
            return String.format("%s %d:%02d", new Object[]{res.getString(C0436R.string.yesterday_at), Integer.valueOf(d.getHours()), Integer.valueOf(d.getMinutes())});
        }
    }

    public static String langDateShort(int _dt) {
        if (res == null) {
            res = VKApplication.context.getResources();
        }
        long dt = ((long) _dt) * 1000;
        Date dnow = new Date(System.currentTimeMillis());
        long daystart = ((System.currentTimeMillis() - ((long) (dnow.getHours() * 3600000))) - ((long) (dnow.getMinutes() * 60000))) - ((long) (dnow.getSeconds() * LocationStatusCodes.GEOFENCE_NOT_AVAILABLE));
        Date d = new Date(dt);
        String r = ACRAConstants.DEFAULT_STRING_VALUE;
        if (daystart < dt) {
            return String.format("%d:%02d", new Object[]{Integer.valueOf(d.getHours()), Integer.valueOf(d.getMinutes())});
        } else if (daystart - 86400000 >= dt || daystart < dt) {
            return d.getDate() + " " + res.getStringArray(C0436R.array.months_short)[d.getMonth()] + " " + (d.getYear() + 1900);
        } else {
            return res.getString(C0436R.string.yesterday);
        }
    }

    public static String langDateDay(int _dt) {
        if (res == null) {
            res = VKApplication.context.getResources();
        }
        long dt = ((long) _dt) * 1000;
        Date dnow = new Date(System.currentTimeMillis());
        long daystart = ((System.currentTimeMillis() - ((long) (dnow.getHours() * 3600000))) - ((long) (dnow.getMinutes() * 60000))) - ((long) (dnow.getSeconds() * LocationStatusCodes.GEOFENCE_NOT_AVAILABLE));
        Date d = new Date(dt);
        String r = ACRAConstants.DEFAULT_STRING_VALUE;
        if (daystart < dt && 86400000 + daystart >= dt) {
            return res.getString(C0436R.string.today);
        }
        if (86400000 + daystart < dt && 172800000 + daystart > dt) {
            return res.getString(C0436R.string.tomorrow);
        }
        if (daystart - 86400000 < dt && daystart >= dt) {
            return res.getString(C0436R.string.yesterday);
        }
        if (dnow.getYear() == d.getYear()) {
            return d.getDate() + " " + res.getStringArray(C0436R.array.months_full)[d.getMonth()];
        }
        return d.getDate() + " " + res.getStringArray(C0436R.array.months_full)[d.getMonth()] + " " + (d.getYear() + 1900);
    }

    public static String langDateRelative(int dt, Resources r) {
        int diff = ((int) (System.currentTimeMillis() / 1000)) - dt;
        if (diff >= 14400 || diff < 0) {
            return langDate(r, dt);
        }
        if (diff >= 10800) {
            return r.getStringArray(C0436R.array.date_ago_hrs)[2];
        }
        if (diff >= 7200) {
            return r.getStringArray(C0436R.array.date_ago_hrs)[1];
        }
        if (diff >= 3600) {
            return r.getStringArray(C0436R.array.date_ago_hrs)[0];
        }
        if (diff >= 60) {
            return langPlural(C0436R.array.date_ago_mins, diff / 60, r);
        }
        if (diff <= 10) {
            return r.getString(C0436R.string.date_ago_now);
        }
        return langPlural(C0436R.array.date_ago_secs, diff, r);
    }

    public static String langDateRelativeNoDiff(int dt, Resources r) {
        int diff = ((int) (System.currentTimeMillis() / 1000)) - dt;
        if (diff >= 14400 || diff < 0) {
            return langDate(r, dt);
        }
        if (diff >= 10800) {
            return r.getStringArray(C0436R.array.date_ago_hrs)[2];
        }
        if (diff >= 7200) {
            return r.getStringArray(C0436R.array.date_ago_hrs)[1];
        }
        if (diff >= 3600) {
            return r.getStringArray(C0436R.array.date_ago_hrs)[0];
        }
        if (diff >= 60) {
            return langPlural(C0436R.array.date_ago_mins, diff / 60, r);
        }
        if (diff <= 10) {
            return r.getString(C0436R.string.date_ago_now);
        }
        return langPlural(C0436R.array.date_ago_secs, diff, r);
    }

    private static String formatFloat(float f) {
        if (f == ((float) Math.round(f))) {
            f = (float) ((int) f);
        }
        return new StringBuilder(String.valueOf(f)).toString();
    }

    public static String langFileSize(long sz, Resources r) {
        if (sz > 1073741824) {
            return new StringBuilder(String.valueOf(formatFloat(((float) Math.round((((float) sz) / 1.07374182E9f) * 100.0f)) / 100.0f))).append(" ").append(r.getString(C0436R.string.fsize_gb)).toString();
        }
        if (sz > 1048576) {
            return new StringBuilder(String.valueOf(formatFloat(((float) Math.round((((float) sz) / 1048576.0f) * 10.0f)) / 10.0f))).append(" ").append(r.getString(C0436R.string.fsize_mb)).toString();
        }
        if (sz > 1024) {
            return new StringBuilder(String.valueOf(Math.round(((float) sz) / 1024.0f))).append(" ").append(r.getString(C0436R.string.fsize_kb)).toString();
        }
        return new StringBuilder(String.valueOf(sz)).append(" ").append(r.getString(C0436R.string.fsize_b)).toString();
    }

    public static String time(int t) {
        Date d = new Date(((long) t) * 1000);
        return String.format("%d:%02d", new Object[]{Integer.valueOf(d.getHours()), Integer.valueOf(d.getMinutes())});
    }

    public static String replaceHTML(String s) {
        if (s == null) {
            return ACRAConstants.DEFAULT_STRING_VALUE;
        }
        s = s.replace("<br>", "\\n");
        StringBuffer buf = new StringBuffer();
        if (entitiesPtn == null) {
            entitiesPtn = Pattern.compile("&([a-zA-Z0-9#]+);");
        }
        Matcher matcher = entitiesPtn.matcher(s);
        String replacement = ACRAConstants.DEFAULT_STRING_VALUE;
        while (matcher.find()) {
            String entity = matcher.group(1);
            if (entity.startsWith("#")) {
                char ch = (char) Integer.parseInt(entity.substring(1));
                if (ch == '\\') {
                    replacement = "\\\\\\\\";
                } else if (ch == '\"') {
                    replacement = "\\\\\"";
                } else if (Character.isISOControl(ch)) {
                    replacement = ACRAConstants.DEFAULT_STRING_VALUE;
                } else {
                    replacement = Character.toString(ch);
                }
            } else if ("gt".equalsIgnoreCase(entity)) {
                replacement = ">";
            } else if ("lt".equalsIgnoreCase(entity)) {
                replacement = "<";
            } else if ("amp".equalsIgnoreCase(entity)) {
                replacement = "&";
            } else if ("quot".equalsIgnoreCase(entity)) {
                replacement = "\\\\\"";
            } else if ("ndash".equalsIgnoreCase(entity)) {
                replacement = "-";
            } else {
                replacement = "?";
            }
            matcher.appendReplacement(buf, replacement);
        }
        matcher.appendTail(buf);
        return buf.toString();
    }

    public static String replaceMentions(String s) {
        return s.replace("&amp;", "&").replace("<", "&lt;").replaceAll("\\[id(\\d+)\\|([^\\]]+)\\]", "<a href='vkontakte://profile/$1'>$2</a>").replaceAll("\\[club(\\d+)\\|([^\\]]+)\\]", "<a href='vkontakte://profile/-$1'>$2</a>").replaceAll("(#[\u0401\u0451\u0404\u0454\u00cf\u00ef@0-9a-zA-Z\u0430-\u044f\u0410-\u042f_]+)", "<a href='vkontakte://search/$1'>$1</a>").replaceAll("((?:(?:http|https)://)?[a-zA-Z\u0430-\u044f\u0410-\u042f0-9\\.-]+\\.[a-zA-Z\u0440\u0444\u0420\u0424]{2,4}[0-9a-zA-Z~/?\\.=#!%&\\+_-]*(?<!\\.)(?<!!))", "<a href='vklink://view/?$1'>$1</a>");
    }

    public static String unwrapMentions(String s) {
        if (s == null) {
            return ACRAConstants.DEFAULT_STRING_VALUE;
        }
        return s.replaceAll("\\[id(\\d+)\\|([^\\]]+)\\]", "$2").replaceAll("\\[club(\\d+)\\|([^\\]]+)\\]", "$2");
    }

    public static ArrayList<String> extractLinks(String s) {
        ArrayList<String> ss = new ArrayList();
        Matcher m = Pattern.compile("((?:(?:http|https)://)?[a-zA-Z\u0430-\u044f\u0410-\u042f0-9\\.-]+\\.[a-zA-Z\u0440\u0444\u0420\u0424]{2,4}[0-9a-zA-Z~/?\\.=#!%&_-]*(?<!\\.)(?<!!))").matcher(s);
        while (m.find()) {
            ss.add(m.group());
        }
        return ss;
    }

    public static Bitmap getResBitmap(Resources res, int id) {
        Drawable d = res.getDrawable(id);
        if (d == null || !(d instanceof BitmapDrawable)) {
            return null;
        }
        return ((BitmapDrawable) d).getBitmap();
    }

    public static byte[] getURL(String url) {
        return getURL(url, null, null);
    }

    public static byte[] getURL(String url, RequestWrapper w, ProgressCallback pc) {
        if (httpclient == null) {
            HttpParams hParams = new BasicHttpParams();
            HttpProtocolParams.setUseExpectContinue(hParams, false);
            HttpProtocolParams.setUserAgent(hParams, APIController.USER_AGENT);
            HttpConnectionParams.setSocketBufferSize(hParams, ACRAConstants.DEFAULT_BUFFER_SIZE_IN_BYTES);
            HttpConnectionParams.setConnectionTimeout(hParams, 30000);
            HttpConnectionParams.setSoTimeout(hParams, 30000);
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            httpclient = new DefaultHttpClient(new ThreadSafeClientConnManager(hParams, registry), hParams);
        }
        HttpGet httppost = new HttpGet(url);
        if (!url.endsWith(".jpg")) {
            if (!url.endsWith(".gif")) {
                if (!url.endsWith(".png")) {
                    httppost.addHeader("Accept-Encoding", "gzip");
                }
            }
        }
        if (w != null) {
            w.request = httppost;
        }
        try {
            HttpResponse response = httpclient.execute(httppost);
            InputStream is = response.getEntity().getContent();
            Header contentEncoding = response.getFirstHeader("Content-Encoding");
            if (contentEncoding != null && "gzip".equalsIgnoreCase(contentEncoding.getValue())) {
                is = new GZIPInputStream(is);
            }
            int len = (int) response.getEntity().getContentLength();
            int loaded = 0;
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            byte[] rd = new byte[5120];
            while (true) {
                int l = is.read(rd);
                if (l <= 0) {
                    break;
                }
                buf.write(rd, 0, l);
                if (pc != null) {
                    loaded += l;
                    pc.onProgressChanged(loaded, len);
                }
            }
            is.close();
            if (w != null) {
                w.request = null;
            }
            return buf.toByteArray();
        } catch (Throwable e) {
            if (Log.logFile != null) {
                Log.m531w("vk", "Error downloading " + url, e);
            }
            if (w != null) {
                w.request = null;
            }
            return null;
        }
    }

    public static boolean isHardwareAccelerated(View view) {
        if (Integer.parseInt(VERSION.SDK) < 11) {
            return false;
        }
        try {
            return ((Boolean) View.class.getMethod("isHardwareAccelerated", new Class[0]).invoke(view, new Object[0])).booleanValue();
        } catch (Exception e) {
            return false;
        }
    }

    public static String langPlural(int rID, int n, Resources r) {
        String lang = Locale.getDefault().getISO3Language();
        if (lang.equals("rus") || lang.equals("ukr")) {
            if ((n / 10) % 10 == 1) {
                return String.format(r.getStringArray(rID)[2], new Object[]{Integer.valueOf(n)});
            } else if (n % 10 == 1) {
                return String.format(r.getStringArray(rID)[0], new Object[]{Integer.valueOf(n)});
            } else if (n % 10 > 4 || n % 10 == 0) {
                return String.format(r.getStringArray(rID)[2], new Object[]{Integer.valueOf(n)});
            } else {
                return String.format(r.getStringArray(rID)[1], new Object[]{Integer.valueOf(n)});
            }
        } else if (n == 1) {
            return String.format(r.getStringArray(rID)[0], new Object[]{Integer.valueOf(n)});
        } else {
            return String.format(r.getStringArray(rID)[1], new Object[]{Integer.valueOf(n)});
        }
    }

    public static String getDeviceLang() {
        String l = Locale.getDefault().getISO3Language();
        if ("rus".equals(l)) {
            return "ru";
        }
        if ("ukr".equals(l)) {
            return "ua";
        }
        return "en";
    }

    public static boolean hasSysFeature(PackageManager pm, String feature) {
        boolean z = false;
        try {
            z = Boolean.parseBoolean(pm.getClass().getMethod("hasSystemFeature", new Class[]{String.class}).invoke(pm, new Object[]{feature}).toString());
        } catch (Exception e) {
        }
        return z;
    }

    public static int scale(float dip) {
        return Math.round(displayDensity * dip);
    }

    public static void dumpViewHierarchy(View v, int depth) {
        int i;
        String logstr = ACRAConstants.DEFAULT_STRING_VALUE;
        for (i = 0; i < depth; i++) {
            logstr = new StringBuilder(String.valueOf(logstr)).append("-").toString();
        }
        logstr = new StringBuilder(String.valueOf(logstr)).append(" ").append(v.toString()).toString();
        if (v instanceof TextView) {
            logstr = new StringBuilder(String.valueOf(logstr)).append(" ['").append(((TextView) v).getText()).append("']").toString();
        }
        logstr = new StringBuilder(String.valueOf(logstr)).append(" (").append(v.getWidth()).append("x").append(v.getHeight()).append(")").toString();
        switch (v.getVisibility()) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                logstr = new StringBuilder(String.valueOf(logstr)).append(" VISIBLE").toString();
                break;
            case UserListView.TYPE_FAVE /*4*/:
                logstr = new StringBuilder(String.valueOf(logstr)).append(" INVISIBLE").toString();
                break;
            case UserListView.TYPE_BLACKLIST /*8*/:
                logstr = new StringBuilder(String.valueOf(logstr)).append(" GONE").toString();
                break;
        }
        logstr = new StringBuilder(String.valueOf(logstr)).append(" BG=").append(v.getBackground()).toString();
        if (v instanceof FrameLayout) {
            logstr = new StringBuilder(String.valueOf(logstr)).append(" FG=").append(((FrameLayout) v).getForeground()).toString();
        }
        logstr = new StringBuilder(String.valueOf(logstr)).append(" pad=").append(v.getPaddingLeft()).append(";").append(v.getPaddingTop()).append(";").append(v.getPaddingRight()).append(";").append(v.getPaddingBottom()).toString();
        if (v.getLayoutParams() instanceof MarginLayoutParams) {
            MarginLayoutParams pp = (MarginLayoutParams) v.getLayoutParams();
            logstr = new StringBuilder(String.valueOf(logstr)).append(" margins=").append(pp.leftMargin).append(";").append(pp.topMargin).append(";").append(pp.rightMargin).append(";").append(pp.bottomMargin).toString();
        }
        Log.m528i("vk", logstr);
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (i = 0; i < vg.getChildCount(); i++) {
                dumpViewHierarchy(vg.getChildAt(i), depth + 1);
            }
        }
    }

    public static void dumpParentViewHierarchy(View v) {
        while (v.getParent() != null && (v.getParent() instanceof View)) {
            v = (View) v.getParent();
        }
        dumpViewHierarchy(v, 0);
    }

    public static void removeImages(View v) {
        if ((v instanceof ImageView) && v.getTag() != null) {
            ((ImageView) v).setImageBitmap(null);
        }
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                removeImages(vg.getChildAt(i));
            }
        }
    }

    public static boolean isAppInstalled(Context context, String pkg) {
        try {
            context.getPackageManager().getPackageInfo(pkg, 1);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String getStaticMapURL(double lat, double lon) {
        int i = 1;
        Locale locale = Locale.US;
        String str = "http://maps.googleapis.com/maps/api/staticmap?center=%1$s,%2$s&zoom=16&size=300x130&sensor=false&scale=%3$d&language=%4$s";
        Object[] objArr = new Object[4];
        objArr[0] = new StringBuilder(String.valueOf(lat)).toString();
        objArr[1] = new StringBuilder(String.valueOf(lon)).toString();
        if (displayDensity > 1.0f) {
            i = 2;
        }
        objArr[2] = Integer.valueOf(i);
        objArr[3] = System.getProperty("user.language");
        return String.format(locale, str, objArr);
    }

    public static String getStaticMapURL(double lat, double lon, int w, int h) {
        int i = 1;
        Locale locale = Locale.US;
        String str = "http://maps.googleapis.com/maps/api/staticmap?center=%1$s,%2$s&zoom=16&size=%5$dx%6$d&sensor=false&scale=%3$d&language=%4$s";
        Object[] objArr = new Object[6];
        objArr[0] = new StringBuilder(String.valueOf(lat)).toString();
        objArr[1] = new StringBuilder(String.valueOf(lon)).toString();
        if (displayDensity > 1.0f) {
            i = 2;
        }
        objArr[2] = Integer.valueOf(i);
        objArr[3] = System.getProperty("user.language");
        objArr[4] = Integer.valueOf(w);
        objArr[5] = Integer.valueOf(h);
        return String.format(locale, str, objArr);
    }

    public static String getStaticMapURL(double lat, double lon, int w, int h, int zoom) {
        int i = 1;
        Locale locale = Locale.US;
        String str = "http://maps.googleapis.com/maps/api/staticmap?center=%1$s,%2$s&zoom=%7$d&size=%5$dx%6$d&sensor=false&scale=%3$d&language=%4$s";
        Object[] objArr = new Object[7];
        objArr[0] = new StringBuilder(String.valueOf(lat)).toString();
        objArr[1] = new StringBuilder(String.valueOf(lon)).toString();
        if (displayDensity > 1.0f) {
            i = 2;
        }
        objArr[2] = Integer.valueOf(i);
        objArr[3] = System.getProperty("user.language");
        objArr[4] = Integer.valueOf(w);
        objArr[5] = Integer.valueOf(h);
        objArr[6] = Integer.valueOf(zoom);
        return String.format(locale, str, objArr);
    }

    public static int getUserOnlineStatus(JSONObject user) {
        if (user.optInt("online", 0) != 1) {
            return 0;
        }
        if (user.optInt("online_mobile") != 1) {
            return 1;
        }
        if (user.optInt("online_app") > 0) {
            return 3;
        }
        return 2;
    }

    private static boolean inArray(char c, char[] a) {
        for (char cc : a) {
            if (cc == c) {
                return true;
            }
        }
        return false;
    }

    public static CharSequence replaceEmoji(CharSequence cs) {
        if (cs == null || cs.length() == 0) {
            return cs;
        }
        CharSequence s;
        if (cs instanceof Spannable) {
            s = (Spannable) cs;
        } else {
            s = Factory.getInstance().newSpannable(cs);
        }
        long buf = 0;
        for (int i = 0; i < cs.length(); i++) {
            char c = cs.charAt(i);
            if (c == '\ud83c' || c == '\ud83d' || (buf != 0 && (-4294967296L & buf) == 0 && c >= '\udde6' && c <= '\uddfa')) {
                buf = (buf << 16) | ((long) c);
            } else if (buf > 0 && (61440 & c) == 53248) {
                d = Emoji.getEmojiDrawable((buf << 16) | ((long) c));
                if (d != null) {
                    ImageSpan span = new XImageSpan(d, 0);
                    if (c < '\udde6' || c > '\uddfa') {
                        s.setSpan(span, i - 1, i + 1, 0);
                    } else {
                        s.setSpan(span, i - 3, i + 1, 0);
                    }
                }
                buf = 0;
            } else if (c == '\u20e3') {
                if (i > 0) {
                    char c2 = cs.charAt(i - 1);
                    if ((c2 >= '0' && c2 <= '9') || c2 == '#') {
                        d = Emoji.getEmojiDrawable((((long) c2) << 16) | ((long) c));
                        if (d != null) {
                            s.setSpan(new XImageSpan(d, 0), i - 1, i + 1, 0);
                        }
                        buf = 0;
                    }
                }
            } else if (inArray(c, emojiChars)) {
                d = Emoji.getEmojiDrawable((long) c);
                if (d != null) {
                    s.setSpan(new XImageSpan(d, 0), i, i + 1, 0);
                }
            }
        }
        return s;
    }

    public static boolean isPressed(int[] state) {
        if (state == null) {
            return false;
        }
        for (int s : state) {
            if (s == 16842919) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSelected(int[] state) {
        if (state == null) {
            return false;
        }
        for (int s : state) {
            if (s == 16842913) {
                return true;
            }
        }
        return false;
    }

    public static boolean isPressedOrSelected(int[] state) {
        if (state == null) {
            return false;
        }
        for (int s : state) {
            if (s == 16842919 || s == 16842913) {
                return true;
            }
        }
        return false;
    }

    public static boolean isTouchwiz() {
        return new File("/system/app/SecLauncher2.apk").exists();
    }

    public static List<Integer> stringToIntArray(String str) {
        ArrayList<Integer> res = new ArrayList();
        for (String si : str.split(",")) {
            if (!(si == null || si.length() == 0)) {
                try {
                    res.add(Integer.valueOf(Integer.parseInt(si)));
                } catch (NumberFormatException e) {
                }
            }
        }
        return res;
    }

    public static void showViewAnimated(View view, boolean show, int duration) {
        AlphaAnimation aa;
        if (show && view.getVisibility() != 0) {
            view.setVisibility(0);
            aa = new AlphaAnimation(0.0f, 1.0f);
            aa.setDuration((long) duration);
            view.startAnimation(aa);
        } else if (!show && view.getVisibility() == 0) {
            aa = new AlphaAnimation(1.0f, 0.0f);
            long t = System.currentTimeMillis();
            aa.setDuration((long) duration);
            aa.setAnimationListener(new C02761(t, duration, view));
            view.startAnimation(aa);
        }
    }
}
