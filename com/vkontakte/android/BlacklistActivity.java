package com.vkontakte.android;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.vkontakte.android.EditableUserListView.OnUserRemovedListener;
import com.vkontakte.android.api.AccountBanUser;
import com.vkontakte.android.api.AccountBanUser.Callback;

public class BlacklistActivity extends SherlockActivity {
    private EditableUserListView view;

    /* renamed from: com.vkontakte.android.BlacklistActivity.1 */
    class C12731 implements OnUserRemovedListener {
        C12731() {
        }

        public void onUserRemoved(UserProfile user) {
            BlacklistActivity.this.unban(user);
        }
    }

    /* renamed from: com.vkontakte.android.BlacklistActivity.2 */
    class C12742 implements Callback {
        private final /* synthetic */ UserProfile val$user;

        C12742(UserProfile userProfile) {
            this.val$user = userProfile;
        }

        public void success() {
            BlacklistActivity.this.view.removeUser(this.val$user);
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(BlacklistActivity.this, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        this.view = new EditableUserListView(this, 8, null);
        setContentView(this.view);
        this.view.loadData();
        this.view.setOnUserRemovedListener(new C12731());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        }
        onBackPressed();
        return true;
    }

    public void onPause() {
        super.onPause();
        AppStateTracker.onActivityPaused();
    }

    public void onResume() {
        super.onResume();
        AppStateTracker.onActivityResumed(this);
    }

    private void unban(UserProfile user) {
        new AccountBanUser(user.uid, false).setCallback(new C12742(user)).wrapProgress(this).exec((Activity) this);
    }
}
