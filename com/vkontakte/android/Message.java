package com.vkontakte.android;

import android.content.ContentValues;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.Html;
import android.text.Spannable;
import com.google.android.gcm.GCMConstants;
import com.google.android.gms.plus.PlusShare;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.acra.ACRAConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Message implements Parcelable {
    public static final int ATTACH_AUDIO = 1;
    public static final int ATTACH_DOCUMENT = 3;
    public static final int ATTACH_FWD_MESSAGE = 4;
    public static final int ATTACH_LOCATION = 5;
    public static final int ATTACH_PHOTO = 0;
    public static final int ATTACH_POST = 6;
    public static final int ATTACH_VIDEO = 2;
    public static final Creator<Message> CREATOR;
    public static final int FLAG_FAILED = 2;
    public static final int FLAG_SERVICE = 4;
    public static final int FLAG_UNREAD = 1;
    public ArrayList<Attachment> attachments;
    public CharSequence displayableText;
    public Bundle extras;
    public ArrayList<FwdMessage> fwdMessages;
    public int id;
    public boolean isServiceMessage;
    public boolean out;
    public int peer;
    public boolean readState;
    public boolean sendFailed;
    public int sender;
    public String text;
    public int time;
    public String title;

    /* renamed from: com.vkontakte.android.Message.1 */
    class C03151 implements Creator<Message> {
        C03151() {
        }

        public Message createFromParcel(Parcel in) {
            return new Message(null);
        }

        public Message[] newArray(int size) {
            return new Message[size];
        }
    }

    public static class FwdMessage {
        public ArrayList<Attachment> attachments;
        public CharSequence displayableText;
        public ArrayList<FwdMessage> fwdMessages;
        public int id;
        public int sender;
        public String text;
        public int time;
        public String username;
        public String userphoto;

        public FwdMessage() {
            this.username = ACRAConstants.DEFAULT_STRING_VALUE;
            this.userphoto = ACRAConstants.DEFAULT_STRING_VALUE;
        }

        public static FwdMessage deserialize(DataInputStream s) throws IOException {
            int i;
            FwdMessage m = new FwdMessage();
            m.sender = s.readInt();
            m.time = s.readInt();
            m.setText(s.readUTF());
            m.username = s.readUTF();
            m.userphoto = s.readUTF();
            m.id = s.readInt();
            int atts = s.readInt();
            m.attachments = new ArrayList();
            for (i = Message.ATTACH_PHOTO; i < atts; i += Message.FLAG_UNREAD) {
                Attachment att = Attachment.deserialize(s, s.readInt());
                if (att != null) {
                    m.attachments.add(att);
                }
            }
            int nfwd = s.readInt();
            m.fwdMessages = new ArrayList();
            for (i = Message.ATTACH_PHOTO; i < nfwd; i += Message.FLAG_UNREAD) {
                m.fwdMessages.add(deserialize(s));
            }
            return m;
        }

        public void setText(String t) {
            this.text = t;
            this.displayableText = NewsEntry.stripUnderlines((Spannable) Global.replaceEmoji(Html.fromHtml(Global.replaceMentions(t).replace("\n", "<br/>"))));
        }

        public void serialize(DataOutputStream s) throws IOException {
            s.writeInt(this.sender);
            s.writeInt(this.time);
            s.writeUTF(this.text);
            s.writeUTF(this.username);
            s.writeUTF(this.userphoto);
            s.writeInt(this.id);
            s.writeInt(this.attachments.size());
            Iterator it = this.attachments.iterator();
            while (it.hasNext()) {
                ((Attachment) it.next()).serialize(s);
            }
            s.writeInt(this.fwdMessages.size());
            it = this.fwdMessages.iterator();
            while (it.hasNext()) {
                ((FwdMessage) it.next()).serialize(s);
            }
        }
    }

    public Message() {
        this.extras = new Bundle();
        this.out = false;
        this.sendFailed = false;
        this.fwdMessages = new ArrayList();
        this.isServiceMessage = false;
        this.readState = false;
        this.attachments = new ArrayList();
    }

    private Message(Parcel p) {
        boolean z = true;
        this.extras = new Bundle();
        this.out = false;
        this.sendFailed = false;
        this.fwdMessages = new ArrayList();
        this.isServiceMessage = false;
        this.readState = false;
        this.attachments = new ArrayList();
        try {
            boolean z2;
            int i;
            this.extras = p.readBundle(null);
            setText(p.readString());
            this.title = p.readString();
            this.out = p.readInt() == FLAG_UNREAD;
            if (p.readInt() == FLAG_UNREAD) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.sendFailed = z2;
            if (p.readInt() == FLAG_UNREAD) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.readState = z2;
            if (p.readInt() != FLAG_UNREAD) {
                z = false;
            }
            this.isServiceMessage = z;
            this.time = p.readInt();
            this.id = p.readInt();
            this.sender = p.readInt();
            this.peer = p.readInt();
            int len = p.readInt();
            this.attachments = new ArrayList();
            for (i = ATTACH_PHOTO; i < len; i += FLAG_UNREAD) {
                byte[] buf = new byte[p.readInt()];
                p.readByteArray(buf);
                DataInputStream is = new DataInputStream(new ByteArrayInputStream(buf));
                Attachment att = Attachment.deserialize(is, is.readInt());
                if (att != null) {
                    this.attachments.add(att);
                }
            }
            this.fwdMessages = new ArrayList();
            len = p.readInt();
            for (i = ATTACH_PHOTO; i < len; i += FLAG_UNREAD) {
                byte[] sdata = new byte[p.readInt()];
                p.readByteArray(sdata);
                this.fwdMessages.add(FwdMessage.deserialize(new DataInputStream(new ByteArrayInputStream(sdata))));
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    public Message(Message m) {
        this.extras = new Bundle();
        this.out = false;
        this.sendFailed = false;
        this.fwdMessages = new ArrayList();
        this.isServiceMessage = false;
        this.readState = false;
        this.attachments = new ArrayList();
        setText(m.text);
        this.title = m.title;
        this.out = m.out;
        this.sendFailed = m.sendFailed;
        this.readState = m.readState;
        this.isServiceMessage = m.isServiceMessage;
        this.time = m.time;
        this.id = m.id;
        this.sender = m.sender;
        this.peer = m.peer;
        this.attachments = new ArrayList();
        this.attachments.addAll(m.attachments);
        this.fwdMessages = new ArrayList();
        this.fwdMessages.addAll(m.fwdMessages);
    }

    public Message(JSONObject obj) {
        this(obj, new HashMap(), new HashMap());
    }

    public Message(JSONObject obj, HashMap<Integer, String> users, HashMap<Integer, String> photos) {
        boolean z = true;
        this.extras = new Bundle();
        this.out = false;
        this.sendFailed = false;
        this.fwdMessages = new ArrayList();
        this.isServiceMessage = false;
        this.readState = false;
        this.attachments = new ArrayList();
        try {
            boolean z2;
            this.id = obj.optInt("id", obj.optInt("mid"));
            if (obj.has("from_id")) {
                this.sender = obj.getInt("from_id");
                this.peer = obj.has("chat_id") ? obj.getInt("chat_id") + 2000000000 : obj.optInt("user_id", obj.optInt("from_id"));
            } else if (obj.has("uid")) {
                int i;
                this.sender = obj.getInt("out") == FLAG_UNREAD ? Global.uid : obj.getInt("uid");
                if (obj.has("chat_id")) {
                    i = obj.getInt("chat_id") + 2000000000;
                } else {
                    i = obj.getInt("uid");
                }
                this.peer = i;
            } else {
                this.sender = obj.getInt("out") == FLAG_UNREAD ? Global.uid : obj.getInt("user_id");
                this.peer = obj.has("chat_id") ? obj.getInt("chat_id") + 2000000000 : obj.getInt("user_id");
            }
            setText(obj.getString("body"));
            this.time = obj.getInt("date");
            this.title = obj.optString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            if (obj.optInt("out") == FLAG_UNREAD) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.out = z2;
            if (obj.getInt(LongPollService.EXTRA_READ_STATE) != FLAG_UNREAD) {
                z = false;
            }
            this.readState = z;
            if (obj.has("action")) {
                this.isServiceMessage = true;
                this.extras.putString("action", obj.getString("action"));
                if (obj.has("action_text")) {
                    this.extras.putString("action_text", obj.getString("action_text"));
                }
                if (obj.has("action_mid")) {
                    this.extras.putInt("action_mid", obj.getInt("action_mid"));
                }
                if (obj.has("action_email")) {
                    this.extras.putString("action_email", obj.getString("action_email"));
                }
            }
            this.attachments = new ArrayList();
            if (obj.has("attachments")) {
                JSONArray atts = obj.getJSONArray("attachments");
                for (int i2 = ATTACH_PHOTO; i2 < atts.length(); i2 += FLAG_UNREAD) {
                    Attachment att = Attachment.parse(atts.getJSONObject(i2));
                    if (att != null) {
                        this.attachments.add(att);
                    }
                }
            }
            Attachment.sort(this.attachments);
            this.fwdMessages = new ArrayList();
            if (obj.has("fwd_messages")) {
                this.fwdMessages.addAll(parseFwdMessages(obj.getJSONArray("fwd_messages"), users, photos));
            }
            if (obj.has("geo")) {
                JSONObject jg = obj.getJSONObject("geo");
                if (jg.has("coordinates")) {
                    this.attachments.add(Attachment.parseGeo(jg));
                }
            }
        } catch (Exception x) {
            Log.m531w("vk", "error parsing message", x);
        }
    }

    public Message(ContentValues values) {
        this.extras = new Bundle();
        this.out = false;
        this.sendFailed = false;
        this.fwdMessages = new ArrayList();
        this.isServiceMessage = false;
        this.readState = false;
        this.attachments = new ArrayList();
        try {
            DataInputStream is;
            int num;
            int i;
            this.id = values.getAsInteger("mid").intValue();
            this.peer = values.getAsInteger("peer").intValue();
            this.sender = values.getAsInteger(GCMConstants.EXTRA_SENDER).intValue();
            setText(values.getAsString("text"));
            this.time = values.getAsInteger("time").intValue();
            int flags = values.getAsInteger("flags").intValue();
            this.readState = (flags & FLAG_UNREAD) == 0;
            this.sendFailed = (flags & FLAG_FAILED) > 0;
            this.isServiceMessage = (flags & FLAG_SERVICE) > 0;
            this.attachments = new ArrayList();
            byte[] att = values.getAsByteArray("attachments");
            if (att != null) {
                is = new DataInputStream(new ByteArrayInputStream(att));
                num = is.read();
                for (i = ATTACH_PHOTO; i < num; i += FLAG_UNREAD) {
                    Attachment a = Attachment.deserialize(is, is.readInt());
                    if (a != null) {
                        this.attachments.add(a);
                    }
                }
            }
            this.fwdMessages = new ArrayList();
            byte[] fwd = values.getAsByteArray("fwd");
            if (fwd != null) {
                is = new DataInputStream(new ByteArrayInputStream(fwd));
                num = is.read();
                for (i = ATTACH_PHOTO; i < num; i += FLAG_UNREAD) {
                    this.fwdMessages.add(FwdMessage.deserialize(is));
                }
            }
            this.out = this.sender == Global.uid;
            String xtra = values.getAsString("extras");
            if (xtra != null) {
                JSONObject obj = new JSONObject(xtra);
                Iterator<String> keys = obj.keys();
                while (keys.hasNext()) {
                    String k = (String) keys.next();
                    Object o = obj.get(k);
                    if (o instanceof Integer) {
                        this.extras.putInt(k, ((Integer) o).intValue());
                    } else if (o instanceof Boolean) {
                        this.extras.putBoolean(k, ((Boolean) o).booleanValue());
                    } else {
                        this.extras.putString(k, (String) o);
                    }
                }
            }
        } catch (Exception x) {
            Log.m531w("vk", "error parsing message", x);
        }
    }

    public void setText(String t) {
        this.text = t;
        this.displayableText = NewsEntry.stripUnderlines((Spannable) Global.replaceEmoji(Html.fromHtml(Global.replaceMentions(t).replace("\n", "<br/>"))));
    }

    public int describeContents() {
        return ATTACH_PHOTO;
    }

    public void writeToParcel(Parcel out, int flags) {
        int i;
        Iterator it;
        int i2 = FLAG_UNREAD;
        int i3 = ATTACH_PHOTO;
        out.writeBundle(this.extras);
        out.writeString(this.text);
        out.writeString(this.title);
        out.writeInt(this.out ? FLAG_UNREAD : ATTACH_PHOTO);
        if (this.sendFailed) {
            i = FLAG_UNREAD;
        } else {
            i = ATTACH_PHOTO;
        }
        out.writeInt(i);
        if (this.readState) {
            i = FLAG_UNREAD;
        } else {
            i = ATTACH_PHOTO;
        }
        out.writeInt(i);
        if (!this.isServiceMessage) {
            i2 = ATTACH_PHOTO;
        }
        out.writeInt(i2);
        out.writeInt(this.time);
        out.writeInt(this.id);
        out.writeInt(this.sender);
        out.writeInt(this.peer);
        out.writeInt(this.attachments == null ? ATTACH_PHOTO : this.attachments.size());
        if (this.attachments != null) {
            it = this.attachments.iterator();
            while (it.hasNext()) {
                Attachment att = (Attachment) it.next();
                try {
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    att.serialize(new DataOutputStream(os));
                    byte[] b = os.toByteArray();
                    out.writeInt(b.length);
                    out.writeByteArray(b);
                } catch (Exception e) {
                }
            }
        }
        if (this.fwdMessages != null) {
            i3 = this.fwdMessages.size();
        }
        out.writeInt(i3);
        if (this.fwdMessages != null) {
            try {
                it = this.fwdMessages.iterator();
                while (it.hasNext()) {
                    FwdMessage m = (FwdMessage) it.next();
                    ByteArrayOutputStream buf = new ByteArrayOutputStream();
                    m.serialize(new DataOutputStream(buf));
                    byte[] bb = buf.toByteArray();
                    out.writeInt(bb.length);
                    out.writeByteArray(bb);
                }
            } catch (Exception e2) {
            }
        }
    }

    static {
        CREATOR = new C03151();
    }

    public String toString() {
        return this.id + "; " + this.text + "; " + this.attachments + "; " + (this.attachments == null ? ATTACH_PHOTO : this.attachments.size()) + "; " + this.extras.getString("action");
    }

    public String getServiceMessageText(UserProfile senderProfile, String userAcc) {
        String dtext = ACRAConstants.DEFAULT_STRING_VALUE;
        String act = this.extras.getString("action");
        Resources resources;
        int i;
        Object[] objArr;
        if ("chat_photo_update".equals(act)) {
            resources = VKApplication.context.getResources();
            if (senderProfile.f151f) {
                i = C0436R.string.chat_photo_updated_f;
            } else {
                i = C0436R.string.chat_photo_updated_m;
            }
            objArr = new Object[FLAG_UNREAD];
            objArr[ATTACH_PHOTO] = senderProfile.fullName;
            return resources.getString(i, objArr);
        } else if ("chat_photo_remove".equals(act)) {
            resources = VKApplication.context.getResources();
            i = senderProfile.f151f ? C0436R.string.chat_photo_removed_f : C0436R.string.chat_photo_removed_m;
            objArr = new Object[FLAG_UNREAD];
            objArr[ATTACH_PHOTO] = senderProfile.fullName;
            return resources.getString(i, objArr);
        } else if ("chat_create".equals(act)) {
            resources = VKApplication.context.getResources();
            i = senderProfile.f151f ? C0436R.string.serv_created_chat_f : C0436R.string.serv_created_chat_m;
            objArr = new Object[FLAG_FAILED];
            objArr[ATTACH_PHOTO] = senderProfile.fullName;
            objArr[FLAG_UNREAD] = this.extras.getString("action_text");
            return resources.getString(i, objArr);
        } else if ("chat_title_update".equals(act)) {
            resources = VKApplication.context.getResources();
            i = senderProfile.f151f ? C0436R.string.serv_renamed_chat_f : C0436R.string.serv_renamed_chat_m;
            objArr = new Object[FLAG_FAILED];
            objArr[ATTACH_PHOTO] = senderProfile.fullName;
            objArr[FLAG_UNREAD] = this.extras.getString("action_text");
            return resources.getString(i, objArr);
        } else if ("chat_invite_user".equals(act)) {
            if (this.extras.getInt("action_mid") == this.sender) {
                resources = VKApplication.context.getResources();
                i = senderProfile.f151f ? C0436R.string.serv_user_returned_f : C0436R.string.serv_user_returned_m;
                objArr = new Object[FLAG_UNREAD];
                objArr[ATTACH_PHOTO] = senderProfile.fullName;
                return resources.getString(i, objArr);
            }
            if (this.extras.containsKey("action_email")) {
                name = this.extras.getString("action_email");
            } else {
                name = userAcc;
            }
            resources = VKApplication.context.getResources();
            i = senderProfile.f151f ? C0436R.string.serv_user_invited_f : C0436R.string.serv_user_invited_m;
            objArr = new Object[FLAG_FAILED];
            objArr[ATTACH_PHOTO] = senderProfile.fullName;
            objArr[FLAG_UNREAD] = name;
            return resources.getString(i, objArr);
        } else if (!"chat_kick_user".equals(act)) {
            return ACRAConstants.DEFAULT_STRING_VALUE;
        } else {
            if (this.extras.getInt("action_mid") == this.sender) {
                resources = VKApplication.context.getResources();
                i = senderProfile.f151f ? C0436R.string.serv_user_left_f : C0436R.string.serv_user_left_m;
                objArr = new Object[FLAG_UNREAD];
                objArr[ATTACH_PHOTO] = senderProfile.fullName;
                return resources.getString(i, objArr);
            }
            if (this.extras.containsKey("action_email")) {
                name = this.extras.getString("action_email");
            } else {
                name = userAcc;
            }
            resources = VKApplication.context.getResources();
            i = senderProfile.f151f ? C0436R.string.serv_user_kicked_f : C0436R.string.serv_user_kicked_m;
            objArr = new Object[FLAG_FAILED];
            objArr[ATTACH_PHOTO] = senderProfile.fullName;
            objArr[FLAG_UNREAD] = name;
            return resources.getString(i, objArr);
        }
    }

    public FwdMessage forward() {
        FwdMessage f = new FwdMessage();
        f.sender = this.sender;
        f.time = this.time;
        f.text = this.text;
        f.displayableText = this.displayableText;
        f.id = this.id;
        f.attachments = new ArrayList();
        f.attachments.addAll(this.attachments);
        f.fwdMessages = new ArrayList();
        f.fwdMessages.addAll(this.fwdMessages);
        return f;
    }

    private ArrayList<FwdMessage> parseFwdMessages(JSONArray msgs, HashMap<Integer, String> names, HashMap<Integer, String> photos) throws JSONException {
        ArrayList<FwdMessage> result = new ArrayList();
        for (int i = ATTACH_PHOTO; i < msgs.length(); i += FLAG_UNREAD) {
            JSONObject jm = msgs.getJSONObject(i);
            FwdMessage m = new FwdMessage();
            m.sender = jm.optInt("user_id", jm.optInt("uid"));
            m.time = jm.getInt("date");
            m.setText(jm.getString("body"));
            m.username = names.containsKey(Integer.valueOf(m.sender)) ? (String) names.get(Integer.valueOf(m.sender)) : "DELETED";
            m.userphoto = photos.containsKey(Integer.valueOf(m.sender)) ? (String) photos.get(Integer.valueOf(m.sender)) : "http://vk.com/images/camera_c.gif";
            m.attachments = new ArrayList();
            if (jm.has("attachments")) {
                JSONArray atts = jm.getJSONArray("attachments");
                for (int j = ATTACH_PHOTO; j < atts.length(); j += FLAG_UNREAD) {
                    Attachment att = Attachment.parse(atts.getJSONObject(j));
                    if (att != null) {
                        m.attachments.add(att);
                    }
                }
            }
            Attachment.sort(m.attachments);
            m.fwdMessages = new ArrayList();
            if (jm.has("fwd_messages")) {
                m.fwdMessages.addAll(parseFwdMessages(jm.getJSONArray("fwd_messages"), names, photos));
            }
            result.add(m);
        }
        return result;
    }
}
