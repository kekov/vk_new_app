package com.vkontakte.android;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.preference.PreferenceManager;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.games.GamesClient;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.api.C2DMRegisterDevice;
import com.vkontakte.android.api.C2DMRegisterDevice.Callback;
import com.vkontakte.android.api.GetWallInfo;
import com.vkontakte.android.cache.Cache;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.data.Messages;
import com.vkontakte.android.data.Stickers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;
import org.acra.ErrorReporter;
import org.json.JSONArray;
import org.json.JSONObject;

public class NetworkStateReceiver extends BroadcastReceiver {
    public static final String ACTION_GROUPS_UPDATED = "com.vkontakte.android.GROUPS_UPDATED";
    private static final int TYPE_UPDATE_PERIOD = 10000;
    private static String currentNetworkType;
    public static boolean disableBigImages;
    private static final String[] highSpeedTypes;
    public static boolean isConnected;
    private static long lastUpdatedType;
    private static final int[] sl;
    private static int tries;
    public static boolean userInfoUpdated;

    /* renamed from: com.vkontakte.android.NetworkStateReceiver.2 */
    class C03162 implements Runnable {

        /* renamed from: com.vkontakte.android.NetworkStateReceiver.2.1 */
        class C13131 extends APIHandler {
            private final /* synthetic */ int val$id;
            private final /* synthetic */ APIRequest val$req;

            C13131(APIRequest aPIRequest, int i) {
                this.val$req = aPIRequest;
                this.val$id = i;
            }

            public void success(JSONObject r) {
                if ("friends.add".equals((String) this.val$req.params.get("method"))) {
                    int uid = Integer.parseInt((String) this.val$req.params.get("user_id"));
                    ArrayList<Integer> uids = new ArrayList();
                    uids.add(Integer.valueOf(uid));
                    Friends.add((UserProfile) Friends.getUsersBlocking(uids).get(0));
                }
                Cache.deleteApiRequest(this.val$id);
            }

            public void fail(int ecode, String emsg) {
                if (ecode == 15 || ecode == 1 || ecode == 14) {
                    Cache.deleteApiRequest(this.val$id);
                }
            }
        }

        C03162() {
        }

        public void run() {
            while (true) {
                try {
                    HashMap<String, String> args = new HashMap();
                    int id = Cache.getApiRequest(args);
                    if (args.containsKey("uid")) {
                        args.put("user_id", (String) args.remove("uid"));
                    }
                    Log.m525d("vk", "Got api req: " + id + " " + args);
                    if (id != -1) {
                        APIRequest req = new APIRequest((String) args.remove("_method"));
                        req.params.putAll(args);
                        req.handler(new C13131(req, id));
                        req.execSync();
                    } else {
                        return;
                    }
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                    return;
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.NetworkStateReceiver.3 */
    class C03173 implements Runnable {
        private final /* synthetic */ Context val$context;

        C03173(Context context) {
            this.val$context = context;
        }

        public void run() {
            Log.m528i("vk", "do reauth");
            Auth.doReauth();
            Log.m528i("vk", "done");
            NetworkStateReceiver.updateUserInfo(this.val$context);
        }
    }

    /* renamed from: com.vkontakte.android.NetworkStateReceiver.1 */
    class C13121 implements Callback {
        C13121() {
        }

        public void success() {
            VKApplication.context.getSharedPreferences(null, 0).edit().remove("need_update_gcm").commit();
        }

        public void fail(int ecode, String emsg) {
        }
    }

    /* renamed from: com.vkontakte.android.NetworkStateReceiver.4 */
    class C13144 implements GetWallInfo.Callback {
        private final /* synthetic */ Context val$context;

        C13144(Context context) {
            this.val$context = context;
        }

        public void success(String name, String photo, boolean exportTwi, boolean exportFb, int time, int intro, int country, boolean hasNewItems) {
            NetworkStateReceiver.userInfoUpdated = true;
            SharedPreferences prefs = this.val$context.getSharedPreferences(null, 0);
            int serverTime = time;
            int localTime = (int) (System.currentTimeMillis() / 1000);
            Log.m528i("vk", "t diff = " + Math.round(((float) (localTime - serverTime)) / 3600.0f) + " hrs [" + (localTime - serverTime) + "]");
            Global.timeDiff = localTime - serverTime;
            Log.m528i("vk", "using time offset " + 0);
            Log.m528i("vk", "User country is " + country);
            prefs.edit().putInt("usercountry", country).putString("username", name).putString("userphoto", photo).putInt("time_diff", 0).putBoolean("export_twitter_avail", exportTwi).putBoolean("export_facebook_avail", exportFb).putInt("intro", intro).commit();
            this.val$context.getSharedPreferences("stickers", 0).edit().putBoolean("has_new", hasNewItems).commit();
            NetworkStateReceiver.updateFriendlist(this.val$context);
        }

        public void fail(int ecode, String emsg) {
            Log.m526e("vk", "FAIL " + ecode + " " + emsg);
            try {
                Thread.sleep((long) NetworkStateReceiver.sl[5 - NetworkStateReceiver.tries]);
            } catch (Exception e) {
            }
            NetworkStateReceiver.tries = NetworkStateReceiver.tries - 1;
            if (NetworkStateReceiver.tries > 0) {
                NetworkStateReceiver.updateUserInfo(this.val$context);
            } else {
                NetworkStateReceiver.tries = 5;
            }
        }
    }

    /* renamed from: com.vkontakte.android.NetworkStateReceiver.5 */
    class C13155 extends APIHandler {
        private final /* synthetic */ Context val$context;

        C13155(Context context) {
            this.val$context = context;
        }

        public void success(JSONObject o) {
            this.val$context.getSharedPreferences(null, 0).edit().putInt("last_get_notify", (int) (System.currentTimeMillis() / 1000)).commit();
            try {
                JSONArray a = o.getJSONArray("response");
                if (a.length() > 0) {
                    JSONObject nt = a.getJSONObject(a.length() - 1);
                    String md = APIRequest.md5(nt.getString(LongPollService.EXTRA_MESSAGE));
                    Intent intent = new Intent(this.val$context, NotificationActivity.class);
                    Iterator<String> keys = nt.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        intent.putExtra(key, nt.getString(key));
                    }
                    intent.addFlags(268435456);
                    this.val$context.startActivity(intent);
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    static {
        isConnected = false;
        userInfoUpdated = false;
        tries = 5;
        sl = new int[]{1500, GamesClient.STATUS_ACHIEVEMENT_UNLOCK_FAILURE, GamesClient.STATUS_MULTIPLAYER_ERROR_CREATION_NOT_ALLOWED, 12000, 24000};
        disableBigImages = false;
        lastUpdatedType = 0;
        currentNetworkType = null;
        highSpeedTypes = new String[]{"3g", "lte", "wifi", "ethernet"};
    }

    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.TIME_SET".equals(intent.getAction())) {
            updateUserInfo(context);
            return;
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        PendingIntent pbIntent = PendingIntent.getBroadcast(context, 0, new Intent(context, BirthdayBroadcastReceiver.class), 134217728);
        AlarmManager as = (AlarmManager) context.getSystemService("alarm");
        if (prefs.getBoolean("notifyBDays", true)) {
            as.setRepeating(1, ((System.currentTimeMillis() - (System.currentTimeMillis() % 86400000)) + 86400000) - ((long) TimeZone.getDefault().getRawOffset()), 86400000, pbIntent);
        } else {
            as.cancel(pbIntent);
        }
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            boolean _isConnected = !intent.getBooleanExtra("noConnectivity", false);
            currentNetworkType = getNetworkType();
            Log.m528i("vk", "!!!!!!!!!! NETWORK CHANGED to " + currentNetworkType);
            if (isConnected != _isConnected) {
                isConnected = _isConnected;
                if (isConnected) {
                    context.sendBroadcast(new Intent(Stickers.ACTION_STICKERS_UPDATED));
                    if (Global.longPoll == null && AppStateTracker.getCurrentActivity() != null) {
                        VKApplication.context.startService(new Intent(VKApplication.context, LongPollService.class));
                    }
                    if (context.getSharedPreferences(null, 0).contains("need_update_gcm")) {
                        new C2DMRegisterDevice(context.getSharedPreferences(null, 0).getString("need_update_gcm", null)).setCallback(new C13121()).exec();
                    }
                    Log.m528i("vk", "Before update info");
                    updateInfo(context);
                    ArrayList<Message> resend = Cache.getResendableMessages();
                    ArrayList<Integer> mids = new ArrayList();
                    Log.m528i("vk", "Before resend " + resend);
                    Iterator it = resend.iterator();
                    while (it.hasNext()) {
                        Message m = (Message) it.next();
                        if (m.sendFailed) {
                            Message msg = Messages.send(m.peer, m.text, m.attachments, m.fwdMessages);
                            Messages.add(msg, null, null);
                            mids.add(Integer.valueOf(m.id));
                            Intent b = new Intent(LongPollService.ACTION_NEW_MESSAGE);
                            b.putExtra(LongPollService.EXTRA_PEER_ID, msg.peer);
                            b.putExtra(LongPollService.EXTRA_MESSAGE, msg);
                            context.sendBroadcast(b, permission.ACCESS_DATA);
                            b = new Intent(LongPollService.ACTION_MESSAGE_DELETED);
                            b.putExtra(LongPollService.EXTRA_MSG_ID, m.id);
                            context.sendBroadcast(b, permission.ACCESS_DATA);
                        }
                    }
                    if (mids.size() > 0) {
                        Messages.delete(mids);
                    }
                    new Thread(new C03162()).start();
                }
            }
        }
    }

    public static void updateInfo(Context context) {
        if (!userInfoUpdated) {
            Log.m529v("vk", "about to update user info...");
            if (VKApplication.context.getSharedPreferences(null, 0).contains("uid")) {
                if (!Global.inited) {
                    Global.res = context.getResources();
                    Global.displayDensity = context.getResources().getDisplayMetrics().density;
                    SharedPreferences prefs = context.getApplicationContext().getSharedPreferences(null, 0);
                    if (prefs.contains("sid")) {
                        Global.uid = prefs.getInt("uid", 0);
                        Global.accessToken = prefs.getString("sid", null);
                        Global.secret = prefs.getString("secret", null);
                        ErrorReporter.getInstance().putCustomData("vk_uid", new StringBuilder(String.valueOf(Global.uid)).toString());
                    }
                    Global.inited = true;
                }
                if (VKApplication.context.getSharedPreferences(null, 0).contains("new_auth")) {
                    updateUserInfo(context);
                    return;
                } else {
                    new Thread(new C03173(context)).start();
                    return;
                }
            }
            Log.m529v("vk", "not logged in.");
        }
    }

    private static void updateUserInfo(Context context) {
        if (Global.uid > 0) {
            new C2DMRegisterDevice(GCMRegistrar.getRegistrationId(VKApplication.context)).exec();
            new GetWallInfo(Global.uid).setCallback(new C13144(context)).exec();
        }
    }

    public static void getNotifications(Context context) {
        if ((System.currentTimeMillis() / 1000) - ((long) context.getSharedPreferences(null, 0).getInt("last_get_notify", 0)) >= 3600 && Global.accessToken != null) {
            try {
                new APIRequest("internal.getNotifications").param("device", Build.MODEL).param("os", VERSION.SDK + "," + VERSION.RELEASE).param("app_version", context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode).param("locale", System.getProperty("user.language")).handler(new C13155(context)).exec();
            } catch (Exception e) {
            }
        }
    }

    private static void updateFriendlist(Context context) {
        if (Global.uid > 0 && Global.accessToken != null) {
            Friends.reload(true);
            Groups.reload(true);
            Stickers.updateInfo();
        }
    }

    public static boolean isConnected() {
        return ((ConnectivityManager) VKApplication.context.getSystemService("connectivity")).getActiveNetworkInfo() != null;
    }

    public static boolean isHighSpeed() {
        if (!PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getBoolean("bigImagesMobile", true) && isMobile()) {
            return false;
        }
        if (currentNetworkType == null || System.currentTimeMillis() - lastUpdatedType > 10000) {
            currentNetworkType = getNetworkType();
        }
        for (String s : highSpeedTypes) {
            if (s.equals(currentNetworkType)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMobile() {
        NetworkInfo info = ((ConnectivityManager) VKApplication.context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info != null && info.getType() == 0) {
            return true;
        }
        return false;
    }

    public static String getNetworkType() {
        NetworkInfo info = ((ConnectivityManager) VKApplication.context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info == null) {
            return "none";
        }
        if (info.getType() == 0) {
            if ("GPRS".equals(info.getSubtypeName()) || "EDGE".equals(info.getSubtypeName())) {
                return "edge";
            }
            if ("LTE".equals(info.getSubtypeName())) {
                return "lte";
            }
            return "3g";
        } else if (info.getType() == 1) {
            return "wifi";
        } else {
            if (info.getType() == 9) {
                return "ethernet";
            }
            return "other";
        }
    }
}
