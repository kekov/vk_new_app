package com.vkontakte.android;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.MotionEventCompat;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.AnimationDuration;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import org.acra.ACRAConstants;

public class PhotoAttachment extends Attachment implements ThumbAttachment, ImageAttachment {
    public static final Creator<PhotoAttachment> CREATOR;
    public String accessKey;
    public int aid;
    public boolean breakAfter;
    public int date;
    public String descr;
    public int displayH;
    public int displayW;
    public boolean floating;
    public boolean hasSize;
    public HashMap<String, Image> images;
    public int oid;
    public boolean paddingAfter;
    public boolean photosMode;
    public int pid;
    public float ratio;
    public String srcBig;
    public String srcThumb;
    public String thumbType;
    public int uid;
    boolean useSmall;

    /* renamed from: com.vkontakte.android.PhotoAttachment.1 */
    class C03921 implements Creator<PhotoAttachment> {
        C03921() {
        }

        public PhotoAttachment createFromParcel(Parcel in) {
            int nImgs = in.readInt();
            Image[] imgs = new Image[nImgs];
            for (int i = 0; i < nImgs; i++) {
                imgs[i] = new Image((char) in.readInt(), in.readString(), in.readInt(), in.readInt());
            }
            return new PhotoAttachment(imgs, in.readInt(), in.readInt(), in.readInt(), in.readString(), in.readInt(), in.readInt(), in.readString());
        }

        public PhotoAttachment[] newArray(int size) {
            return new PhotoAttachment[size];
        }
    }

    public static class FixedSizeImageView extends ImageView {
        private boolean animated;
        public int displayH;
        public int displayW;
        private int phAlpha;
        private Drawable placeholder;

        public FixedSizeImageView(Context context) {
            super(context);
            this.phAlpha = 0;
            this.animated = false;
            this.placeholder = new ColorDrawable(Color.LIGHT_GRAY);
        }

        public void onMeasure(int w, int h) {
            setMeasuredDimension(this.displayW, this.displayH);
        }

        public void animateAlpha() {
            if (!this.animated && VERSION.SDK_INT >= 11) {
                ValueAnimator anim = ObjectAnimator.ofInt(this, "drawableAlpha", new int[]{MotionEventCompat.ACTION_MASK, 0});
                anim.setInterpolator(new DecelerateInterpolator(0.7f));
                anim.setDuration(400);
                anim.start();
                this.animated = true;
            }
        }

        public void setImageBitmap(Bitmap b) {
            super.setImageBitmap(b);
            if (b == null) {
                this.animated = false;
            }
        }

        public void dontAnimate() {
            this.animated = true;
        }

        public void setDrawableAlpha(int a) {
            this.phAlpha = a;
            invalidate();
        }

        public void onDraw(Canvas c) {
            super.onDraw(c);
            if (this.phAlpha > 0) {
                this.placeholder.setAlpha(this.phAlpha);
                this.placeholder.setBounds(0, 0, getWidth(), getHeight());
                this.placeholder.draw(c);
                this.placeholder.setAlpha(MotionEventCompat.ACTION_MASK);
            }
        }
    }

    public static class Image implements Serializable {
        public int height;
        public char type;
        public String url;
        public int width;

        public Image(char type, String url, int w, int h) {
            this.type = type;
            this.url = url;
            this.width = w;
            this.height = h;
        }
    }

    public PhotoAttachment(Image[] _images, int _oid, int _pid, int _aid, String _descr, int _uid, int _date, String akey) {
        boolean z;
        int i = 0;
        this.images = new HashMap();
        this.thumbType = "m";
        if (((ActivityManager) VKApplication.context.getSystemService("activity")).getMemoryClass() < 20) {
            z = true;
        } else {
            z = false;
        }
        this.useSmall = z;
        int length = _images.length;
        while (i < length) {
            Image img = _images[i];
            if (img.url != null) {
                this.images.put(new StringBuilder(String.valueOf(img.type)).toString(), img);
            }
            i++;
        }
        this.pid = _pid;
        this.oid = _oid;
        this.aid = _aid;
        this.descr = _descr;
        this.uid = _uid;
        this.date = _date;
        this.accessKey = akey;
        Image im = (Image) this.images.get("x");
        if (im.width <= 0 || im.height <= 0) {
            this.ratio = GroundOverlayOptions.NO_DIMENSION;
        } else {
            this.ratio = ((float) im.width) / ((float) im.height);
        }
        if (this.images.containsKey("x")) {
            HashMap hashMap = this.images;
            Object obj = ((Global.displayDensity > 1.0f || Global.isTablet) && this.images.containsKey("y")) ? "y" : "x";
            this.srcBig = ((Image) hashMap.get(obj)).url;
        } else {
            this.srcBig = "http://vk.com/images/x_null.gif";
        }
        this.srcThumb = this.images.containsKey("m") ? ((Image) this.images.get("m")).url : "http://vk.com/images/x_null.gif";
    }

    public PhotoAttachment(HashMap<String, Image> _images, int _oid, int _pid, int _aid, String _descr) {
        boolean z;
        this.images = new HashMap();
        this.thumbType = "m";
        if (((ActivityManager) VKApplication.context.getSystemService("activity")).getMemoryClass() < 20) {
            z = true;
        } else {
            z = false;
        }
        this.useSmall = z;
        this.images.putAll(_images);
        this.pid = _pid;
        this.oid = _oid;
        this.aid = _aid;
        this.descr = _descr;
        Image im = (Image) this.images.get("x");
        if (im.width <= 0 || im.height <= 0) {
            this.ratio = GroundOverlayOptions.NO_DIMENSION;
        } else {
            this.ratio = ((float) im.width) / ((float) im.height);
        }
        if (this.images.containsKey("x")) {
            HashMap hashMap = this.images;
            Object obj = ((Global.displayDensity > 1.0f || Global.isTablet) && this.images.containsKey("y")) ? "y" : "x";
            this.srcBig = ((Image) hashMap.get(obj)).url;
        }
        this.srcThumb = ((Image) this.images.get("m")).url;
    }

    public PhotoAttachment(Photo photo) {
        boolean z;
        this.images = new HashMap();
        this.thumbType = "m";
        if (((ActivityManager) VKApplication.context.getSystemService("activity")).getMemoryClass() < 20) {
            z = true;
        } else {
            z = false;
        }
        this.useSmall = z;
        this.pid = photo.id;
        this.oid = photo.ownerID;
        this.aid = photo.albumID;
        this.uid = photo.userID;
        this.date = photo.date;
        this.descr = photo.descr;
        Iterator it = photo.sizes.iterator();
        while (it.hasNext()) {
            com.vkontakte.android.Photo.Image im = (com.vkontakte.android.Photo.Image) it.next();
            Image i = new Image();
            i.height = im.height;
            i.width = im.width;
            i.url = im.url;
            i.type = im.type;
            this.images.put(new StringBuilder(String.valueOf(i.type)).toString(), i);
        }
        Image im2 = (Image) this.images.get("x");
        if (im2.width <= 0 || im2.height <= 0) {
            this.ratio = GroundOverlayOptions.NO_DIMENSION;
        } else {
            this.ratio = ((float) im2.width) / ((float) im2.height);
        }
        this.accessKey = photo.accessKey;
    }

    public PhotoAttachment(String thumb, String big, int _oid, int _pid, int _aid) {
        this(thumb, big, _oid, _pid, _aid, null);
    }

    public PhotoAttachment(String thumb, String big, int _oid, int _pid, int _aid, String _descr) {
        this(new Image[]{new Image('m', thumb, 0, 0), new Image('x', big, 0, 0)}, _oid, _pid, _aid, _descr, 0, 0, ACRAConstants.DEFAULT_STRING_VALUE);
    }

    static {
        CREATOR = new C03921();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int arg1) {
        Set<String> keys = this.images.keySet();
        parcel.writeInt(keys.size());
        for (String k : keys) {
            Image im = (Image) this.images.get(k);
            parcel.writeInt(k.charAt(0));
            parcel.writeString(im.url);
            parcel.writeInt(im.width);
            parcel.writeInt(im.height);
        }
        parcel.writeInt(this.oid);
        parcel.writeInt(this.pid);
        parcel.writeInt(this.aid);
        parcel.writeString(this.descr);
        parcel.writeInt(this.uid);
        parcel.writeInt(this.date);
        parcel.writeString(this.accessKey);
    }

    public View getFullView(Context context) {
        return getViewForList(context, null);
    }

    public View getViewForList(Context context, View reuse) {
        FixedSizeImageView iv = (FixedSizeImageView) Attachment.getReusableView(context, "photo");
        iv.setImageResource(C0436R.drawable.photo_placeholder);
        iv.setId(1);
        iv.setScaleType(this.hasSize ? ScaleType.CENTER_CROP : ScaleType.FIT_CENTER);
        if (this.displayW == 0) {
            this.displayW = AnimationDuration.FILTERS_APPEARING;
        }
        if (this.displayH == 0) {
            this.displayH = 100;
        }
        iv.setMinimumHeight(this.displayH);
        iv.setMaxHeight(this.displayH);
        iv.setMinimumWidth(this.displayW);
        iv.setMaxWidth(this.displayW);
        iv.displayW = this.displayW;
        iv.displayH = this.displayH;
        LayoutParams params = new LayoutParams(Global.scale(ImageViewHolder.PaddingSize), Global.scale((float) (this.paddingAfter ? 10 : 2)));
        if (this.breakAfter || this.floating) {
            params.breakAfter = this.breakAfter;
            params.floating = this.floating;
        }
        iv.setLayoutParams(params);
        return iv;
    }

    public LayoutParams getViewLayoutParams() {
        LayoutParams lp = new LayoutParams(Global.scale(ImageViewHolder.PaddingSize), Global.scale((float) (this.paddingAfter ? 10 : 2)));
        if (this.breakAfter || this.floating) {
            lp.breakAfter = this.breakAfter;
            lp.floating = this.floating;
        }
        lp.width = this.displayW;
        lp.height = this.displayH;
        return lp;
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(1);
        Set<String> keys = this.images.keySet();
        os.writeInt(keys.size());
        for (String k : keys) {
            Image im = (Image) this.images.get(k);
            os.writeChar(k.charAt(0));
            os.writeUTF(im.url);
            os.writeInt(im.width);
            os.writeInt(im.height);
        }
        os.writeInt(this.oid);
        os.writeInt(this.pid);
        os.writeInt(this.aid);
        os.writeUTF(this.descr != null ? this.descr : ACRAConstants.DEFAULT_STRING_VALUE);
        os.writeInt(this.uid);
        os.writeInt(this.date);
        os.writeUTF(this.accessKey != null ? this.accessKey : ACRAConstants.DEFAULT_STRING_VALUE);
    }

    public float getRatio() {
        return this.ratio;
    }

    public int getWidth(char sz) {
        return ((Image) this.images.get(new StringBuilder(String.valueOf(sz)).toString())).width;
    }

    private String getThumbType(int width, int height, HashMap<String, Image> imgs) {
        int max_s = Math.max(width, height);
        if (max_s <= 75) {
            return "s";
        }
        if (max_s < TransportMediator.KEYCODE_MEDIA_RECORD) {
            return "m";
        }
        if (imgs.containsKey("o") && ((Image) imgs.get("o")).width >= width && ((Image) imgs.get("o")).height >= height) {
            return "o";
        }
        if (imgs.containsKey("p") && ((((Image) imgs.get("p")).width >= width && ((Image) imgs.get("p")).height >= height) || this.useSmall)) {
            return "p";
        }
        if (imgs.containsKey("q") && ((Image) imgs.get("q")).width >= width && ((Image) imgs.get("q")).height >= height) {
            return "q";
        }
        if (!imgs.containsKey("r") || ((Image) imgs.get("r")).width < width || ((Image) imgs.get("r")).height < height) {
            return "x";
        }
        return "r";
    }

    public void setViewSize(float width, float height, boolean breakAfter, boolean floating) {
        this.displayW = Math.round(width);
        this.displayH = Math.round(height);
        this.thumbType = getThumbType(this.displayW, this.displayH, this.images);
        this.breakAfter = breakAfter;
        this.floating = floating;
        this.hasSize = true;
    }

    public void setPaddingAfter(boolean p) {
        this.paddingAfter = p;
    }

    public int getWidth() {
        return this.displayW;
    }

    public int getHeight() {
        return this.displayH;
    }

    public String getThumbURL() {
        try {
            if (!NetworkStateReceiver.isConnected()) {
                return "A|" + ((Image) this.images.get(this.thumbType)).url + "|" + ((Image) this.images.get(this.images.containsKey("p") ? "p" : "m")).url;
            } else if (NetworkStateReceiver.isHighSpeed()) {
                return ((Image) this.images.get(this.thumbType)).url;
            } else {
                if (this.images.containsKey("p")) {
                    return ((Image) this.images.get("p")).url;
                }
                return ((Image) this.images.get("m")).url;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public String toString() {
        return "photo" + this.oid + "_" + this.pid + (this.accessKey != null ? "_" + this.accessKey : ACRAConstants.DEFAULT_STRING_VALUE);
    }

    public String getImageURL() {
        return getThumbURL();
    }

    public void setImage(View view, Bitmap img, boolean fromCache) {
        if (view != null) {
            ((ImageView) view).setImageBitmap(img);
            if (fromCache) {
                ((FixedSizeImageView) view).dontAnimate();
            } else {
                ((FixedSizeImageView) view).animateAlpha();
            }
        }
    }

    public void clearImage(View view) {
        ((ImageView) view).setImageDrawable(new ColorDrawable(this.photosMode ? -15592942 : Color.LIGHT_GRAY));
    }
}
