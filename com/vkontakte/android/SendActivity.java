package com.vkontakte.android;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.mediapicker.GalleryPickerActivity;
import java.util.HashMap;

public class SendActivity extends Activity {
    private static final int FILTER_RESULT = 101;
    private static final int TYPE_AUDIO = 3;
    private static final int TYPE_FILE = 4;
    private static final int TYPE_IMAGE = 1;
    private static final int TYPE_TEXT = 0;
    private static final int TYPE_VIDEO = 2;
    private static final String[] audioExtensions;
    private static final String[] docExtensions;
    private static final String[] imageExtensions;
    private static final String[] videoExtensions;
    boolean needFinish;

    /* renamed from: com.vkontakte.android.SendActivity.1 */
    class C04631 implements OnClickListener {
        private final /* synthetic */ EditText val$descrEdit;
        private final /* synthetic */ EditText val$titleEdit;

        C04631(EditText editText, EditText editText2) {
            this.val$titleEdit = editText;
            this.val$descrEdit = editText2;
        }

        public void onClick(DialogInterface dialog, int which) {
            Intent nIntent = new Intent(SendActivity.this, UploaderService.class);
            nIntent.putExtra("file", SendActivity.this.getIntent().getParcelableExtra("android.intent.extra.STREAM").toString());
            HashMap<String, String> params = new HashMap();
            params.put("name", this.val$titleEdit.getText().toString());
            params.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, this.val$descrEdit.getText().toString());
            nIntent.putExtra("req_params", params);
            nIntent.putExtra(WebDialog.DIALOG_PARAM_TYPE, SendActivity.TYPE_VIDEO);
            SendActivity.this.startService(nIntent);
            SendActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.SendActivity.2 */
    class C04642 implements OnClickListener {
        C04642() {
        }

        public void onClick(DialogInterface dialog, int which) {
            SendActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.SendActivity.3 */
    class C04653 implements OnCancelListener {
        C04653() {
        }

        public void onCancel(DialogInterface dialog) {
            SendActivity.this.finish();
        }
    }

    public SendActivity() {
        this.needFinish = true;
    }

    static {
        String[] strArr = new String[TYPE_FILE];
        strArr[TYPE_TEXT] = "jpg";
        strArr[TYPE_IMAGE] = "jpeg";
        strArr[TYPE_VIDEO] = "png";
        strArr[TYPE_AUDIO] = "gif";
        imageExtensions = strArr;
        strArr = new String[TYPE_IMAGE];
        strArr[TYPE_TEXT] = "mp3";
        audioExtensions = strArr;
        videoExtensions = new String[]{"avi", "mp4", "3gp", "mpeg", "mov", "flv", "wmv"};
        docExtensions = new String[]{"doc", "docx", "xls", "xlsx", "ppt", "pptx", "rtf", "pdf", "png", "jpg", "gif", "psd", "mp3", "djvu", "fb2", "ps"};
    }

    public void onCreate(Bundle b) {
        overridePendingTransition(TYPE_TEXT, TYPE_TEXT);
        super.onCreate(b);
        if (Global.uid == 0) {
            startActivityForResult(new Intent(this, AuthActivity.class), 100);
        } else {
            processIntent();
        }
    }

    private void processIntent() {
        Intent intent = getIntent();
        Intent nIntent;
        if (intent.hasExtra("android.intent.extra.STREAM")) {
            Uri stream = (Uri) intent.getParcelableExtra("android.intent.extra.STREAM");
            if (stream == null) {
                Log.m526e("vk", "Stream URI is null, nothing to share, closing.");
                finish();
                return;
            } else if ("content".equals(stream.getScheme())) {
                String ctype = getContentResolver().getType(stream);
                Log.m528i("vk", "Send Type=" + ctype);
                if (ctype != null) {
                    if (ctype.startsWith("image/")) {
                        Log.m528i("vk", "Sending as a photo");
                        fIntent = new Intent(this, GalleryPickerActivity.class);
                        fIntent.setAction("android.intent.action.SEND");
                        fIntent.putExtra("android.intent.extra.STREAM", stream);
                        startActivityForResult(fIntent, FILTER_RESULT);
                        this.needFinish = false;
                    } else if (ctype.equals("audio/mpeg")) {
                        Log.m528i("vk", "Sending as an audio file");
                        nIntent = new Intent(this, UploaderService.class);
                        nIntent.putExtra("file", stream.toString());
                        nIntent.putExtra("req_params", new HashMap());
                        nIntent.putExtra(WebDialog.DIALOG_PARAM_TYPE, TYPE_AUDIO);
                        startService(nIntent);
                    } else if (ctype.startsWith("video/")) {
                        Log.m528i("vk", "Sending as a video");
                        this.needFinish = false;
                        showVideoDialog();
                    }
                }
            } else if ("file".equals(stream.getScheme()) || stream.getScheme() == null) {
                String filename = stream.getLastPathSegment();
                if (filename != null) {
                    String[] fparts = filename.split("\\.");
                    String ext = fparts[fparts.length - 1].toLowerCase();
                    if (isInArray(ext, imageExtensions)) {
                        fIntent = new Intent(this, GalleryPickerActivity.class);
                        fIntent.setAction("android.intent.action.SEND");
                        fIntent.putExtra("android.intent.extra.STREAM", stream);
                        startActivityForResult(fIntent, FILTER_RESULT);
                        this.needFinish = false;
                    } else if (isInArray(ext, audioExtensions)) {
                        nIntent = new Intent(this, UploaderService.class);
                        nIntent.putExtra("file", stream.toString());
                        nIntent.putExtra("req_params", new HashMap());
                        nIntent.putExtra(WebDialog.DIALOG_PARAM_TYPE, TYPE_AUDIO);
                        startService(nIntent);
                    } else if (isInArray(ext, videoExtensions)) {
                        this.needFinish = false;
                        showVideoDialog();
                    } else {
                        nIntent = new Intent(this, UploaderService.class);
                        nIntent.putExtra("file", stream.toString());
                        nIntent.putExtra("req_params", new HashMap());
                        nIntent.putExtra(WebDialog.DIALOG_PARAM_TYPE, TYPE_FILE);
                        startService(nIntent);
                    }
                } else {
                    return;
                }
            } else {
                Log.m526e("vk", "URI has an unsupported scheme");
                notSupported();
            }
        } else if (intent.hasExtra("android.intent.extra.TEXT")) {
            nIntent = new Intent(this, NewPostActivity.class);
            nIntent.setAction("android.intent.action.SEND");
            nIntent.putExtra("text", intent.getStringExtra("android.intent.extra.TEXT"));
            if (intent.hasExtra("android.intent.extra.SUBJECT")) {
                nIntent.putExtra("link_title", intent.getStringExtra("android.intent.extra.SUBJECT"));
            }
            startActivity(nIntent);
        } else {
            notSupported();
        }
        if (this.needFinish) {
            finish();
        }
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode == FILTER_RESULT && resCode == -1) {
            Intent nIntent = new Intent(this, NewPostActivity.class);
            nIntent.putExtra("photoURI", Uri.parse((String) data.getStringArrayListExtra("images").get(TYPE_TEXT)));
            if (getIntent().hasExtra("android.intent.extra.TEXT")) {
                nIntent.putExtra("text", getIntent().getStringExtra("android.intent.extra.TEXT"));
            }
            startActivity(nIntent);
        }
        if (reqCode == 100 && resCode == -1) {
            processIntent();
        } else {
            finish();
        }
    }

    public void showVideoDialog() {
        LinearLayout ll = new LinearLayout(this);
        int padding = (int) (7.0f * Global.displayDensity);
        ll.setPadding(padding, padding, padding, padding);
        ll.setOrientation(TYPE_IMAGE);
        EditText titleEdit = new EditText(this);
        titleEdit.setHint(C0436R.string.share_video_name);
        ll.addView(titleEdit);
        try {
            titleEdit.setText(getTitleFromURI((Uri) getIntent().getParcelableExtra("android.intent.extra.STREAM")));
        } catch (Exception e) {
        }
        EditText descrEdit = new EditText(this);
        descrEdit.setHint(C0436R.string.share_video_description);
        ll.addView(descrEdit);
        new Builder(this).setTitle(C0436R.string.share_video_title).setView(ll).setPositiveButton(C0436R.string.ok, new C04631(titleEdit, descrEdit)).setNegativeButton(C0436R.string.cancel, new C04642()).setOnCancelListener(new C04653()).show();
    }

    private void showOptionsList(int contentType) {
    }

    private String getTitleFromURI(Uri contentUri) {
        String[] proj = new String[TYPE_IMAGE];
        proj[TYPE_TEXT] = PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE;
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private boolean isInArray(String what, String[] array) {
        int length = array.length;
        for (int i = TYPE_TEXT; i < length; i += TYPE_IMAGE) {
            if (array[i].equals(what)) {
                return true;
            }
        }
        return false;
    }

    private void notSupported() {
        Toast.makeText(this, C0436R.string.share_unsupported, TYPE_TEXT).show();
    }
}
