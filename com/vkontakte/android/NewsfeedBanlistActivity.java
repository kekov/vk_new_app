package com.vkontakte.android;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.api.NewsfeedDeleteBan;
import com.vkontakte.android.api.NewsfeedGetBanned;
import com.vkontakte.android.api.NewsfeedGetBanned.Callback;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.MultiSectionAdapter;
import com.vkontakte.android.ui.PhotoView;
import java.util.ArrayList;
import java.util.Iterator;

public class NewsfeedBanlistActivity extends SherlockActivity implements Listener, OnItemClickListener {
    private BanListAdapter adapter;
    protected FrameLayout contentView;
    private FrameLayout contentWrap;
    protected boolean dataLoading;
    private TextView emptyView;
    private ArrayList<UserProfile> groups;
    private ListImageLoaderWrapper imgLoader;
    protected ListView list;
    protected ProgressBar progress;
    private ArrayList<UserProfile> users;

    /* renamed from: com.vkontakte.android.NewsfeedBanlistActivity.3 */
    class C03603 implements Runnable {
        C03603() {
        }

        public void run() {
            NewsfeedBanlistActivity.this.adapter.notifyDataSetInvalidated();
        }
    }

    /* renamed from: com.vkontakte.android.NewsfeedBanlistActivity.1 */
    class C13251 implements Callback {
        C13251() {
        }

        public void success(ArrayList<UserProfile> _users, ArrayList<UserProfile> _groups) {
            NewsfeedBanlistActivity.this.users.addAll(_users);
            NewsfeedBanlistActivity.this.groups.addAll(_groups);
            NewsfeedBanlistActivity.this.adapter.notifyDataSetChanged();
            Global.showViewAnimated(NewsfeedBanlistActivity.this.contentWrap, true, PhotoView.THUMB_ANIM_DURATION);
            Global.showViewAnimated(NewsfeedBanlistActivity.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
            NewsfeedBanlistActivity.this.imgLoader.updateImages();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(NewsfeedBanlistActivity.this, C0436R.string.err_text, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.NewsfeedBanlistActivity.2 */
    class C13262 implements NewsfeedDeleteBan.Callback {
        private final /* synthetic */ int val$id;

        C13262(int i) {
            this.val$id = i;
        }

        public void success() {
            Iterator it;
            UserProfile p;
            if (this.val$id > 0) {
                it = NewsfeedBanlistActivity.this.users.iterator();
                while (it.hasNext()) {
                    p = (UserProfile) it.next();
                    if (p.uid == this.val$id) {
                        NewsfeedBanlistActivity.this.users.remove(p);
                        break;
                    }
                }
                Toast.makeText(NewsfeedBanlistActivity.this, C0436R.string.news_unbanned_user, 1).show();
            } else {
                it = NewsfeedBanlistActivity.this.groups.iterator();
                while (it.hasNext()) {
                    p = (UserProfile) it.next();
                    if (p.uid == this.val$id) {
                        NewsfeedBanlistActivity.this.groups.remove(p);
                        break;
                    }
                }
                Toast.makeText(NewsfeedBanlistActivity.this, C0436R.string.news_unbanned_group, 1).show();
            }
            NewsfeedBanlistActivity.this.adapter.notifyDataSetChanged();
            NewsfeedBanlistActivity.this.imgLoader.updateImages();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(NewsfeedBanlistActivity.this, C0436R.string.err_text, 0).show();
        }
    }

    protected class BanListAdapter extends MultiSectionAdapter {

        /* renamed from: com.vkontakte.android.NewsfeedBanlistActivity.BanListAdapter.1 */
        class C03611 implements OnClickListener {
            C03611() {
            }

            public void onClick(View v) {
                NewsfeedBanlistActivity.this.unban(((Integer) v.getTag()).intValue());
            }
        }

        protected BanListAdapter() {
        }

        public View getView(int section, int item, View view) {
            if (view == null) {
                view = View.inflate(NewsfeedBanlistActivity.this, C0436R.layout.news_banlist_item, null);
                view.findViewById(C0436R.id.flist_item_btn).setOnClickListener(new C03611());
            }
            view.setFocusable(false);
            UserProfile p = (UserProfile) (section == 0 ? NewsfeedBanlistActivity.this.users : NewsfeedBanlistActivity.this.groups).get(item);
            ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(p.fullName);
            if (NewsfeedBanlistActivity.this.imgLoader.isAlreadyLoaded(p.photo)) {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageBitmap(NewsfeedBanlistActivity.this.imgLoader.get(p.photo));
            } else {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(p.uid > 0 ? C0436R.drawable.user_placeholder : C0436R.drawable.group_placeholder);
            }
            view.findViewById(C0436R.id.flist_item_btn).setTag(Integer.valueOf(p.uid));
            return view;
        }

        public String getSectionTitle(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return NewsfeedBanlistActivity.this.getResources().getString(C0436R.string.people);
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return NewsfeedBanlistActivity.this.getResources().getString(C0436R.string.groups);
                default:
                    return null;
            }
        }

        public int getSectionCount() {
            return 2;
        }

        public int getItemCount(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return NewsfeedBanlistActivity.this.users.size();
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return NewsfeedBanlistActivity.this.groups.size();
                default:
                    return 0;
            }
        }

        public long getItemId(int section, int item) {
            return 0;
        }

        public boolean isSectionHeaderVisible(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    if (NewsfeedBanlistActivity.this.users.size() <= 0) {
                        return false;
                    }
                    return true;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    if (NewsfeedBanlistActivity.this.groups.size() <= 0) {
                        return false;
                    }
                    return true;
                default:
                    return false;
            }
        }
    }

    private class PhotosAdapter extends MultiSectionImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.NewsfeedBanlistActivity.PhotosAdapter.1 */
        class C03621 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$item;

            C03621(int i, Bitmap bitmap) {
                this.val$item = i;
                this.val$bitmap = bitmap;
            }

            public void run() {
                try {
                    ((ImageView) NewsfeedBanlistActivity.this.list.getChildAt(this.val$item - NewsfeedBanlistActivity.this.list.getFirstVisiblePosition()).findViewById(C0436R.id.flist_item_photo)).setImageBitmap(this.val$bitmap);
                } catch (Exception e) {
                }
            }
        }

        private PhotosAdapter() {
        }

        public int getSectionCount() {
            return 2;
        }

        public int getItemCount(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return NewsfeedBanlistActivity.this.users.size();
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return NewsfeedBanlistActivity.this.groups.size();
                default:
                    return 0;
            }
        }

        public boolean isSectionHeaderVisible(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    if (NewsfeedBanlistActivity.this.users.size() <= 0) {
                        return false;
                    }
                    return true;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    if (NewsfeedBanlistActivity.this.groups.size() <= 0) {
                        return false;
                    }
                    return true;
                default:
                    return false;
            }
        }

        public int getImageCountForItem(int section, int item) {
            return 1;
        }

        public String getImageURL(int section, int item, int image) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return ((UserProfile) NewsfeedBanlistActivity.this.users.get(item)).photo;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return ((UserProfile) NewsfeedBanlistActivity.this.groups.get(item)).photo;
                default:
                    return null;
            }
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            if (item >= NewsfeedBanlistActivity.this.list.getFirstVisiblePosition() && item <= NewsfeedBanlistActivity.this.list.getLastVisiblePosition()) {
                NewsfeedBanlistActivity.this.runOnUiThread(new C03621(item, bitmap));
            }
        }
    }

    public NewsfeedBanlistActivity() {
        this.users = new ArrayList();
        this.groups = new ArrayList();
        this.dataLoading = false;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        this.contentView = new FrameLayout(this);
        this.contentView.setBackgroundColor(-1);
        if (getIntent().hasExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)) {
            setTitle(getIntent().getStringExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
        }
        setContentView(this.contentView);
        this.list = new ListView(this);
        this.imgLoader = new ListImageLoaderWrapper(new PhotosAdapter(), this.list, this);
        ListView listView = this.list;
        ListAdapter banListAdapter = new BanListAdapter();
        this.adapter = banListAdapter;
        listView.setAdapter(banListAdapter);
        this.list.setDivider(new ColorDrawable(-1710619));
        this.list.setDividerHeight(Global.scale(1.0f));
        if (VERSION.SDK_INT >= 11) {
            this.list.setCacheColorHint(-1);
            this.list.setBackgroundColor(-1);
        }
        this.list.setVerticalFadingEdgeEnabled(false);
        this.list.setOnScrollListener(this.imgLoader);
        this.list.setSelector(C0436R.drawable.highlight);
        this.list.setPadding(getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding), 0, getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding), 0);
        this.list.setScrollBarStyle(33554432);
        this.list.setOnItemClickListener(this);
        this.contentWrap = new FrameLayout(this);
        this.contentWrap.addView(this.list);
        this.emptyView = new TextView(this);
        this.emptyView.setTextAppearance(this, C0436R.style.EmptyTextView);
        this.emptyView.setGravity(17);
        this.emptyView.setText(C0436R.string.news_banlist_empty);
        this.emptyView.setPadding(Global.scale(10.0f), 0, Global.scale(10.0f), 0);
        this.contentWrap.addView(this.emptyView);
        this.list.setEmptyView(this.emptyView);
        this.contentWrap.setVisibility(4);
        this.contentView.addView(this.contentWrap);
        this.progress = new ProgressBar(this);
        LayoutParams lp2 = new LayoutParams(Global.scale(40.0f), Global.scale(40.0f));
        lp2.gravity = 17;
        this.progress.setLayoutParams(lp2);
        this.progress.setVisibility(0);
        this.contentView.addView(this.progress);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadData();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    private void loadData() {
        new NewsfeedGetBanned().setCallback(new C13251()).exec((Activity) this);
    }

    private void unban(int id) {
        new NewsfeedDeleteBan(id).setCallback(new C13262(id)).wrapProgress(this).exec((Activity) this);
    }

    public void onPause() {
        super.onPause();
        this.imgLoader.deactivate();
    }

    public void onResume() {
        super.onResume();
        this.imgLoader.activate();
        if (Global.isTablet) {
            this.list.postDelayed(new C03603(), 10);
        }
    }

    public void onScrolledToLastItem() {
    }

    public void onScrollStarted() {
    }

    public void onScrollStopped() {
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
        int[] p = this.adapter.resolveIndex(pos - this.list.getHeaderViewsCount());
        Bundle args = new Bundle();
        args.putInt("id", ((UserProfile) (p[0] == 0 ? this.users : this.groups).get(p[1])).uid);
        Navigate.to("ProfileFragment", args, this);
    }
}
