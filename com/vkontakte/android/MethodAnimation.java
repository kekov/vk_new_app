package com.vkontakte.android;

import android.view.animation.Animation;
import android.view.animation.Transformation;
import java.lang.reflect.Method;

public class MethodAnimation extends Animation {
    Method method;
    Object obj;
    Object param;

    public MethodAnimation(Object obj, String methodName, Object addParam) {
        if (addParam == null) {
            try {
                this.obj = obj;
                this.method = obj.getClass().getMethod(methodName, new Class[]{Float.TYPE});
                return;
            } catch (Exception e) {
                return;
            }
        }
        try {
            this.obj = obj;
            this.param = addParam;
            this.method = obj.getClass().getDeclaredMethod(methodName, new Class[]{Float.TYPE, Integer.TYPE});
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    protected void applyTransformation(float interpolatedTime, Transformation t) {
        t.clear();
        try {
            if (this.param == null) {
                this.method.invoke(this.obj, new Object[]{Float.valueOf(interpolatedTime)});
                return;
            }
            this.method.invoke(this.obj, new Object[]{Float.valueOf(interpolatedTime), this.param});
        } catch (Exception e) {
        }
    }
}
