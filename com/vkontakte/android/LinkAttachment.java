package com.vkontakte.android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import java.io.DataOutputStream;
import java.io.IOException;
import org.acra.ACRAConstants;

public class LinkAttachment extends Attachment {
    public static final Creator<LinkAttachment> CREATOR;
    public String previewPage;
    public String title;
    public String url;

    /* renamed from: com.vkontakte.android.LinkAttachment.1 */
    class C02821 implements Creator<LinkAttachment> {
        C02821() {
        }

        public LinkAttachment createFromParcel(Parcel in) {
            return new LinkAttachment(in.readString(), in.readString(), in.readString());
        }

        public LinkAttachment[] newArray(int size) {
            return new LinkAttachment[size];
        }
    }

    /* renamed from: com.vkontakte.android.LinkAttachment.2 */
    class C02832 implements OnClickListener {
        private final /* synthetic */ Context val$context;

        C02832(Context context) {
            this.val$context = context;
        }

        public void onClick(View v) {
            if (LinkAttachment.this.previewPage == null || LinkAttachment.this.previewPage.length() <= 0) {
                this.val$context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("vklink://view/?" + LinkAttachment.this.url)));
                return;
            }
            Intent intent = new Intent(v.getContext(), WikiViewActivity.class);
            String[] sp = LinkAttachment.this.previewPage.split("_");
            intent.putExtra("oid", Integer.parseInt(sp[0]));
            intent.putExtra("pid", Integer.parseInt(sp[1]));
            intent.putExtra("site", true);
            this.val$context.startActivity(intent);
        }
    }

    /* renamed from: com.vkontakte.android.LinkAttachment.3 */
    class C02843 implements OnLongClickListener {
        private final /* synthetic */ Context val$context;

        C02843(Context context) {
            this.val$context = context;
        }

        public boolean onLongClick(View v) {
            if (LinkAttachment.this.previewPage == null || LinkAttachment.this.previewPage.length() <= 0) {
                return false;
            }
            this.val$context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("vklink://view/?" + LinkAttachment.this.url)));
            return true;
        }
    }

    public LinkAttachment(String _url, String _title, String preview) {
        this.url = _url;
        this.title = _title;
        this.previewPage = preview;
    }

    static {
        CREATOR = new C02821();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.url);
        dest.writeString(this.previewPage);
    }

    public View getFullView(Context context) {
        return getViewForList(context, null);
    }

    public View getViewForList(Context context, View reuse) {
        View v;
        if (reuse == null) {
            v = Attachment.getReusableView(context, "common");
        } else {
            v = reuse;
        }
        ((ImageView) v.findViewById(C0436R.id.attach_icon)).setImageResource(C0436R.drawable.ic_attach_link);
        ((TextView) v.findViewById(C0436R.id.attach_title)).setText(this.title.length() > 0 ? this.title : context.getResources().getString(C0436R.string.attach_link));
        ((TextView) v.findViewById(C0436R.id.attach_subtitle)).setText(this.url.replaceAll("http(s){0,1}://", ACRAConstants.DEFAULT_STRING_VALUE));
        v.setOnClickListener(new C02832(context));
        v.setOnLongClickListener(new C02843(context));
        return v;
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(5);
        os.writeUTF(this.url);
        os.writeUTF(this.title);
        os.writeUTF(this.previewPage);
    }

    public String toString() {
        return this.url;
    }

    public LayoutParams getViewLayoutParams() {
        LayoutParams lp = new LayoutParams();
        lp.width = -1;
        lp.height = Global.scale(54.0f);
        return lp;
    }
}
