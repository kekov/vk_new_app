package com.vkontakte.android;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.WebDialog;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.api.PollOption;
import com.vkontakte.android.api.PollsAddVote;
import com.vkontakte.android.api.PollsDeleteVote;
import com.vkontakte.android.api.PollsGetById;
import com.vkontakte.android.api.PollsGetById.Callback;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import java.util.ArrayList;

public class PollAttachView extends LinearLayout {
    private TextView infoView;
    public boolean isBoard;
    public boolean isPublic;
    private int oid;
    private ArrayList<View> optionViews;
    private PollOption[] options;
    private int pid;
    private String question;
    private int userOption;

    /* renamed from: com.vkontakte.android.PollAttachView.2 */
    class C03942 implements OnClickListener {
        C03942() {
        }

        public void onClick(View v) {
            PollOption o = (PollOption) v.getTag();
            if (PollAttachView.this.userOption == 0) {
                PollAttachView.this.vote(o);
            } else if (PollAttachView.this.isPublic) {
                Bundle args = new Bundle();
                args.putInt(WebDialog.DIALOG_PARAM_TYPE, 6);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, o.title);
                args.putInt("oid", PollAttachView.this.oid);
                args.putInt("poll_id", PollAttachView.this.pid);
                args.putInt("answer_id", o.id);
                Navigate.to("UserListFragment", args, (Activity) PollAttachView.this.getContext());
            }
        }
    }

    /* renamed from: com.vkontakte.android.PollAttachView.3 */
    class C03953 implements OnLongClickListener {
        C03953() {
        }

        public boolean onLongClick(View v) {
            if (PollAttachView.this.userOption == 0) {
                return false;
            }
            PollAttachView.this.unvote();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.PollAttachView.1 */
    class C13351 implements Callback {
        C13351() {
        }

        public void success(String question, int userAnswer, PollOption[] options, boolean pub) {
            PollAttachView.this.isPublic = pub;
            PollAttachView.this.onLoaded(question, userAnswer, options, pub);
        }

        public void fail(int ecode, String emsg) {
            PollAttachView.this.onFailed(emsg);
        }
    }

    /* renamed from: com.vkontakte.android.PollAttachView.4 */
    class C13364 implements PollsAddVote.Callback {
        private final /* synthetic */ PollOption val$opt;

        C13364(PollOption pollOption) {
            this.val$opt = pollOption;
        }

        public void success(boolean voted) {
            int totalVotes = 1;
            PollAttachView.this.userOption = this.val$opt.id;
            for (PollOption opt : PollAttachView.this.options) {
                totalVotes += opt.numVotes;
            }
            for (int i = 0; i < PollAttachView.this.options.length; i++) {
                if (PollAttachView.this.options[i].id == this.val$opt.id) {
                    PollOption pollOption = PollAttachView.this.options[i];
                    pollOption.numVotes++;
                }
                PollAttachView.this.options[i].percent = (((float) PollAttachView.this.options[i].numVotes) / ((float) totalVotes)) * 100.0f;
            }
            PollAttachView.this.updateText();
            PollAttachView.this.animateProgress();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PollAttachView.this.getContext(), C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.PollAttachView.5 */
    class C13375 implements PollsDeleteVote.Callback {
        C13375() {
        }

        public void success(boolean voted) {
            int totalVotes = 0;
            for (PollOption opt : PollAttachView.this.options) {
                totalVotes += opt.numVotes;
            }
            for (int i = 0; i < PollAttachView.this.options.length; i++) {
                if (PollAttachView.this.options[i].id == PollAttachView.this.userOption) {
                    PollOption pollOption = PollAttachView.this.options[i];
                    pollOption.numVotes--;
                }
                PollAttachView.this.options[i].percent = (((float) PollAttachView.this.options[i].numVotes) / ((float) totalVotes)) * 100.0f;
            }
            PollAttachView.this.userOption = 0;
            PollAttachView.this.updateText();
            PollAttachView.this.animateProgress();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PollAttachView.this.getContext(), C0436R.string.error, 0).show();
        }
    }

    public PollAttachView(Context context, int _oid, int _pid) {
        super(context);
        this.optionViews = new ArrayList();
        this.oid = _oid;
        this.pid = _pid;
        setOrientation(1);
        ProgressBar pb = new ProgressBar(getContext());
        LayoutParams pl = new LayoutParams(Global.scale(BitmapDescriptorFactory.HUE_ORANGE), Global.scale(BitmapDescriptorFactory.HUE_ORANGE));
        pl.gravity = 17;
        pb.setLayoutParams(pl);
        addView(pb);
        setPadding(Global.scale(5.0f), 0, Global.scale(5.0f), Global.scale(10.0f));
    }

    public PollAttachView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.optionViews = new ArrayList();
    }

    public void loadData() {
        new PollsGetById(this.oid, this.pid).setCallback(new C13351()).exec((Activity) getContext());
    }

    public void onMeasure(int wms, int hms) {
        super.onMeasure(wms, hms);
        setMeasuredDimension(MeasureSpec.getSize(wms), getMeasuredHeight());
    }

    private void onFailed(String emsg) {
        removeAllViews();
        TextView tv = new TextView(getContext());
        tv.setTextColor(Color.WINDOW_BACKGROUND_COLOR);
        tv.setPadding(Global.scale(5.0f), Global.scale(5.0f), Global.scale(5.0f), Global.scale(5.0f));
        tv.setGravity(17);
        tv.setText(getResources().getString(C0436R.string.error_loading_poll, new Object[]{emsg}));
        setBackgroundColor(-8739);
        addView(tv);
    }

    public void onLoaded(String question, int uanswer, PollOption[] opts, boolean pub) {
        this.options = opts;
        this.question = question;
        this.userOption = uanswer;
        removeAllViews();
        TextView pqtView = new TextView(getContext());
        pqtView.setTextColor(Color.WINDOW_BACKGROUND_COLOR);
        pqtView.setText(question);
        pqtView.setTextSize(GalleryPickerFooterView.BADGE_SIZE);
        addView(pqtView);
        int total = 0;
        for (PollOption o : opts) {
            total += o.numVotes;
        }
        this.infoView = new TextView(getContext());
        this.infoView.setTextColor(getResources().getColorStateList(C0436R.color.hint));
        this.infoView.setText(new StringBuilder(String.valueOf(getResources().getString(pub ? C0436R.string.poll_open : C0436R.string.poll_anonym))).append(", ").append(Global.langPlural(C0436R.array.num_poll_votes, total, getResources())).toString());
        this.infoView.setTextSize(15.0f);
        addView(this.infoView);
        int maxvotes = 0;
        int maxpercent = 0;
        for (PollOption opt : opts) {
            maxpercent = Math.max(maxpercent, (int) (opt.percent * 10.0f));
            maxvotes = maxpercent;
        }
        maxvotes = new StringBuilder(String.valueOf(maxvotes)).toString().length();
        for (PollOption opt2 : opts) {
            RelativeLayout pItem = new RelativeLayout(getContext());
            pItem.setLayoutParams(new LayoutParams(-1, -2));
            TextView itemTitle = new TextView(getContext());
            itemTitle.setTextColor(Color.WINDOW_BACKGROUND_COLOR);
            itemTitle.setText(opt2.title);
            itemTitle.setId(C0436R.id.poll_opt_title);
            itemTitle.setPadding(0, Global.scale(5.0f), 0, 0);
            itemTitle.setTypeface(uanswer == opt2.id ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            pItem.addView(itemTitle);
            ProgressBar pbar = new ProgressBar(getContext(), null, 16842872);
            pbar.setProgressDrawable(getResources().getDrawable(C0436R.drawable.poll_progress));
            pbar.setMax(maxpercent);
            pbar.setProgress(uanswer != 0 ? (int) (opt2.percent * 10.0f) : 0);
            pbar.setId(C0436R.id.poll_opt_progress);
            RelativeLayout.LayoutParams pparams = new RelativeLayout.LayoutParams(-1, Global.scale(20.0f));
            pparams.addRule(3, C0436R.id.poll_opt_title);
            pparams.rightMargin = Global.scale(50.0f);
            pparams.topMargin = Global.scale(5.0f);
            pbar.setLayoutParams(pparams);
            pItem.addView(pbar);
            TextView prView = new TextView(getContext());
            prView.setTextColor(Color.WINDOW_BACKGROUND_COLOR);
            prView.setTextSize(15.0f);
            prView.setId(C0436R.id.poll_opt_percent);
            Float[] fArr = new Object[1];
            fArr[0] = Float.valueOf(opt2.percent);
            prView.setText(String.format("%.1f%%", fArr));
            prView.setVisibility(uanswer != 0 ? 0 : 4);
            ViewGroup.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(6, C0436R.id.poll_opt_progress);
            layoutParams.addRule(8, C0436R.id.poll_opt_progress);
            layoutParams.addRule(11);
            prView.setLayoutParams(layoutParams);
            pItem.addView(prView);
            TextView overlayView = new TextView(getContext());
            overlayView.setTextColor(getResources().getColorStateList(C0436R.color.hint));
            overlayView.setTextSize(15.0f);
            overlayView.setId(C0436R.id.poll_opt_overlay);
            overlayView.setGravity(17);
            if (uanswer == 0) {
                overlayView.setText(C0436R.string.poll_btn_vote);
            } else {
                overlayView.setText(new StringBuilder(String.valueOf(opt2.numVotes)).toString());
            }
            RelativeLayout.LayoutParams olp = new RelativeLayout.LayoutParams(-2, -2);
            olp.addRule(6, C0436R.id.poll_opt_progress);
            olp.addRule(8, C0436R.id.poll_opt_progress);
            olp.addRule(5, C0436R.id.poll_opt_progress);
            olp.addRule(7, C0436R.id.poll_opt_progress);
            overlayView.setLayoutParams(olp);
            pItem.addView(overlayView);
            pItem.setTag(opt2);
            pItem.setOnClickListener(new C03942());
            pItem.setOnLongClickListener(new C03953());
            this.optionViews.add(pItem);
            addView(pItem);
        }
    }

    private void updateText() {
        int total = 0;
        for (int i = 0; i < this.options.length; i++) {
            Typeface typeface;
            PollOption opt = this.options[i];
            total += opt.numVotes;
            View ov = (View) this.optionViews.get(i);
            if (this.userOption != 0) {
                ov.findViewById(C0436R.id.poll_opt_percent).setVisibility(0);
                ((TextView) ov.findViewById(C0436R.id.poll_opt_percent)).setText(String.format("%.1f%%", new Object[]{Float.valueOf(opt.percent)}));
                ((TextView) ov.findViewById(C0436R.id.poll_opt_overlay)).setText(new StringBuilder(String.valueOf(opt.numVotes)).toString());
            } else {
                ov.findViewById(C0436R.id.poll_opt_percent).setVisibility(4);
                ((TextView) ov.findViewById(C0436R.id.poll_opt_percent)).setText(String.format("%.1f%%", new Object[]{Float.valueOf(opt.percent)}));
                ((TextView) ov.findViewById(C0436R.id.poll_opt_overlay)).setText(C0436R.string.poll_btn_vote);
            }
            TextView textView = (TextView) ov.findViewById(C0436R.id.poll_opt_title);
            if (this.userOption == opt.id) {
                typeface = Typeface.DEFAULT_BOLD;
            } else {
                typeface = Typeface.DEFAULT;
            }
            textView.setTypeface(typeface);
        }
        this.infoView.setText(new StringBuilder(String.valueOf(getResources().getString(this.isPublic ? C0436R.string.poll_open : C0436R.string.poll_anonym))).append(", ").append(Global.langPlural(C0436R.array.num_poll_votes, total, getResources())).toString());
    }

    private void animateProgress() {
        PollOption opt;
        float maxp = 0.0f;
        for (PollOption opt2 : this.options) {
            maxp = Math.max(maxp, opt2.percent);
        }
        int i;
        View ov;
        if (VERSION.SDK_INT >= 11) {
            ArrayList<Animator> anims = new ArrayList();
            for (i = 0; i < this.options.length; i++) {
                opt2 = this.options[i];
                ov = (View) this.optionViews.get(i);
                ((ProgressBar) ov.findViewById(C0436R.id.poll_opt_progress)).setMax((int) (maxp * 10.0f));
                if (this.userOption != 0) {
                    anims.add(ObjectAnimator.ofInt(ov.findViewById(C0436R.id.poll_opt_progress), "progress", new int[]{(int) (opt2.percent * 10.0f)}));
                } else {
                    anims.add(ObjectAnimator.ofInt(ov.findViewById(C0436R.id.poll_opt_progress), "progress", new int[]{0}));
                }
            }
            AnimatorSet set = new AnimatorSet();
            set.playTogether(anims);
            set.setDuration(400);
            set.start();
            return;
        }
        for (i = 0; i < this.options.length; i++) {
            opt2 = this.options[i];
            ov = (View) this.optionViews.get(i);
            ((ProgressBar) ov.findViewById(C0436R.id.poll_opt_progress)).setMax((int) (maxp * 10.0f));
            if (this.userOption != 0) {
                ((ProgressBar) ov.findViewById(C0436R.id.poll_opt_progress)).setProgress((int) (opt2.percent * 10.0f));
            } else {
                ((ProgressBar) ov.findViewById(C0436R.id.poll_opt_progress)).setProgress(0);
            }
        }
    }

    private void vote(PollOption opt) {
        new PollsAddVote(this.oid, this.pid, opt.id, this.isBoard).setCallback(new C13364(opt)).wrapProgress(getContext()).exec((View) this);
    }

    private void unvote() {
        new PollsDeleteVote(this.oid, this.pid, this.userOption, this.isBoard).setCallback(new C13375()).wrapProgress(getContext()).exec((View) this);
    }
}
