package com.vkontakte.android;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.Vector;

public class NewsComment implements Parcelable {
    public static final Creator<NewsComment> CREATOR;
    public ArrayList<Attachment> attachments;
    public boolean canDelete;
    public int cid;
    public int cnum;
    public CharSequence displayableText;
    public boolean isDeleted;
    public boolean isLiked;
    public Vector<String> linkTitles;
    public Vector<String> links;
    public int numLikes;
    public String respToName;
    public int resp_to;
    public String text;
    public int time;
    public int uid;
    public String userName;
    public String userPhoto;
    public String userRName;
    public String userWName;

    /* renamed from: com.vkontakte.android.NewsComment.1 */
    class C03381 implements Creator<NewsComment> {
        C03381() {
        }

        public NewsComment createFromParcel(Parcel in) {
            return new NewsComment(in);
        }

        public NewsComment[] newArray(int size) {
            return new NewsComment[size];
        }
    }

    public NewsComment() {
        this.attachments = new ArrayList();
    }

    public NewsComment(Parcel p) {
        this.attachments = new ArrayList();
    }

    public void writeToParcel(Parcel p, int flags) {
        int i;
        int i2 = 1;
        p.writeString(this.text);
        p.writeString(this.userName);
        p.writeString(this.userRName);
        p.writeString(this.userWName);
        p.writeInt(this.time);
        p.writeString(this.respToName);
        p.writeString(this.userPhoto);
        p.writeInt(this.cid);
        p.writeInt(this.uid);
        p.writeInt(this.cnum);
        p.writeInt(this.resp_to);
        p.writeInt(this.canDelete ? 1 : 0);
        p.writeStringList(this.links);
        p.writeStringList(this.linkTitles);
        p.writeInt(this.numLikes);
        if (this.isLiked) {
            i = 1;
        } else {
            i = 0;
        }
        p.writeInt(i);
        if (!this.isDeleted) {
            i2 = 0;
        }
        p.writeInt(i2);
        this.displayableText = Global.replaceEmoji(this.text);
    }

    public void setText(String t) {
        this.text = t;
        this.displayableText = Global.replaceEmoji(this.text.replaceAll("\\[(id|club)(\\d+)\\|([^\\]]+)\\]", "$3"));
    }

    static {
        CREATOR = new C03381();
    }

    public int describeContents() {
        return 0;
    }
}
