package com.vkontakte.android;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.Document;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.GalleryPickerActivity;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class DocumentChooserActivity extends Activity {
    private static final int FILE_RESULT = 103;
    private static final int LIST_RESULT = 102;
    private static final int PHOTO_RESULT = 101;

    /* renamed from: com.vkontakte.android.DocumentChooserActivity.1 */
    class C02401 implements OnClickListener {
        C02401() {
        }

        public void onClick(DialogInterface dialog, int which) {
            Intent intent;
            switch (which) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    intent = new Intent(DocumentChooserActivity.this, GalleryPickerActivity.class);
                    intent.putExtra("selection_limit", DocumentChooserActivity.this.getIntent().getIntExtra("limit", 10));
                    intent.putExtra("prevent_styling", true);
                    DocumentChooserActivity.this.startActivityForResult(intent, DocumentChooserActivity.PHOTO_RESULT);
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    intent = new Intent(DocumentChooserActivity.this, FilePickerActivity.class);
                    intent.putExtra("size_limit", 209715200);
                    DocumentChooserActivity.this.startActivityForResult(intent, DocumentChooserActivity.FILE_RESULT);
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    Bundle args = new Bundle();
                    args.putBoolean("select", true);
                    intent = new Intent(DocumentChooserActivity.this, FragmentWrapperActivity.class);
                    intent.putExtra("class", "DocumentsFragment");
                    intent.putExtra("args", args);
                    DocumentChooserActivity.this.startActivityForResult(intent, DocumentChooserActivity.LIST_RESULT);
                default:
            }
        }
    }

    /* renamed from: com.vkontakte.android.DocumentChooserActivity.2 */
    class C02412 implements OnCancelListener {
        C02412() {
        }

        public void onCancel(DialogInterface dialog) {
            DocumentChooserActivity.this.finish();
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(new View(this));
        new Builder(this).setTitle(C0436R.string.add_doc_title).setItems(getIntent().getBooleanExtra("no_my", false) ? new String[]{getString(C0436R.string.add_doc_photo), getString(C0436R.string.add_doc_file)} : new String[]{getString(C0436R.string.add_doc_photo), getString(C0436R.string.add_doc_file), getString(C0436R.string.add_doc_list)}, new C02401()).setOnCancelListener(new C02412()).show();
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (resCode == -1) {
            ArrayList<Parcelable> al;
            Intent result;
            if (reqCode == LIST_RESULT) {
                al = new ArrayList();
                al.add(new DocumentAttachment((Document) data.getParcelableExtra("document")));
                result = new Intent();
                result.putExtra("documents", al);
                setResult(-1, result);
                finish();
            }
            if (reqCode == PHOTO_RESULT) {
                ArrayList<String> photos = data.getStringArrayListExtra("images");
                al = new ArrayList();
                Iterator it = photos.iterator();
                while (it.hasNext()) {
                    String file = (String) it.next();
                    Uri uri = Uri.parse(file);
                    if ("file".equals(uri.getScheme()) || "content".equals(uri.getScheme())) {
                        al.add(new PendingDocumentAttachment(uri.getLastPathSegment(), file, (int) new File(uri.getPath()).length(), file, 0, UploaderService.getNewID()));
                    }
                }
                result = new Intent();
                result.putExtra("documents", al);
                setResult(-1, result);
                finish();
            }
            if (reqCode == FILE_RESULT) {
                ArrayList<Parcelable> res = data.getParcelableArrayListExtra("files");
                result = new Intent();
                result.putExtra("documents", res);
                setResult(-1, result);
                finish();
                return;
            }
            return;
        }
        finish();
    }
}
