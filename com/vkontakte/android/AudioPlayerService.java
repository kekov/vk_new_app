package com.vkontakte.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.RemoteControlClient;
import android.media.RemoteControlClient.OnGetPlaybackPositionListener;
import android.media.RemoteControlClient.OnPlaybackPositionUpdateListener;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcelable;
import android.os.StatFs;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.api.AudioSetBroadcast;
import com.vkontakte.android.cache.AlbumArtRetriever;
import com.vkontakte.android.cache.AlbumArtRetriever.ImageLoadCallback;
import com.vkontakte.android.cache.AudioCache;
import com.vkontakte.android.cache.AudioCache.Proxy;
import com.vkontakte.android.mediapicker.gl.GLFilterContext;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class AudioPlayerService extends Service implements OnBufferingUpdateListener, OnCompletionListener, OnErrorListener {
    public static final int ACTION_NEW_FILE = 1;
    public static final int ACTION_NEW_PLAYLIST = 2;
    public static final int ACTION_NEXT_TRACK = 5;
    public static final int ACTION_PAUSE_IF_PLAYING = 7;
    public static final int ACTION_PLAY_IF_PAUSED = 8;
    public static final int ACTION_PLAY_PAUSE = 3;
    public static final int ACTION_PREV_TRACK = 6;
    public static final String ACTION_SERVICE_STOPPING = "com.vkontakte.android.SERVICE_STOPPING";
    public static final int ACTION_SHOW_PLAYER = 4;
    public static final int ACTION_TOGGLE_REPEAT = 9;
    public static final int ACTION_TOGGLE_SHUFFLE = 10;
    public static final String ACTION_UPDATE_AUDIO_LISTS = "com.vkontakte.android.UPDATE_AUDIO_LISTS";
    public static final String ACTION_UPDATE_PLAYING = "com.vkontakte.android.PLAYER_PLAYING";
    public static final String B_ACTION_PLAYER_CONTROL = "com.vkontakte.android.PLAYER_CONTROL";
    private static final int ID_NOTIFICATION = 300;
    private static final long MIN_SPACE_TO_CACHE = 52428800;
    private static final long PLAYER_RELEASE_DELAY = 60000;
    private static final int PROXY_PORT = 48329;
    private static final long SERVICE_STOP_DELAY = 1800000;
    public static final int STATE_INITING = 3;
    public static final int STATE_PAUSED = 2;
    public static final int STATE_PLAYING = 1;
    public static final int STATE_STOPPED = 0;
    private static ArrayList<AttachViewCallback> attachCallbacks;
    private static Proxy cacheProxy;
    public static ArrayList<AudioFile> listToPlay;
    private static PendingIntent notificationContentIntent;
    private static boolean pauseAfterInit;
    private static ArrayList<ProgressCallback> progressCallbacks;
    public static AudioPlayerService sharedInstance;
    private Vector<AudioAttachView> attachViews;
    private boolean buggyPlayer;
    private boolean cacheCurrent;
    private CallStateReceiver callStateReceiver;
    private boolean cancelReleaseWifiLock;
    private AudioFile currentFile;
    private boolean currentIsCached;
    private ArrayList<AudioFile> currentPlaylist;
    private boolean error;
    private int errorRetries;
    private boolean haveAudioFocus;
    private boolean headsetPlugState;
    public boolean initing;
    private boolean loop;
    private int nBuffered;
    private int needSeekTo;
    private NotificationManager nm;
    private Notification notification;
    private boolean pausedByCall;
    private boolean pausedBySystem;
    private MediaPlayer player;
    private PlayerCallback playerCallback;
    private Timer playerStopTimer;
    private int playlistPosition;
    private boolean random;
    private ArrayList<AudioFile> randomPlaylist;
    private BroadcastReceiver receiver;
    private RemoteControlClient remoteControlClient;
    private Timer serviceStopTimer;
    private boolean startAfterCall;
    private int statusUserID;
    private TelephonyManager telManager;
    private Timer timer;
    private boolean useCustomNotification;
    private WifiLock wifiLock;

    /* renamed from: com.vkontakte.android.AudioPlayerService.11 */
    class AnonymousClass11 implements Runnable {
        private final /* synthetic */ float val$from;

        AnonymousClass11(float f) {
            this.val$from = f;
        }

        public void run() {
            int i = AudioPlayerService.STATE_PLAYING;
            while (i <= 50) {
                try {
                    float vol = this.val$from + ((((float) i) / 50.0f) * (1.0f - this.val$from));
                    AudioPlayerService.this.player.setVolume(vol, vol);
                    try {
                        Thread.sleep(10);
                    } catch (Exception e) {
                    }
                    i += AudioPlayerService.STATE_PLAYING;
                } catch (Exception e2) {
                    return;
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerService.12 */
    class AnonymousClass12 implements Runnable {
        private final /* synthetic */ float val$to;

        AnonymousClass12(float f) {
            this.val$to = f;
        }

        public void run() {
            int i = 50;
            while (i >= AudioPlayerService.STATE_PLAYING) {
                try {
                    float vol = this.val$to + ((((float) i) / 50.0f) * (1.0f - this.val$to));
                    AudioPlayerService.this.player.setVolume(vol, vol);
                    try {
                        Thread.sleep(10);
                    } catch (Exception e) {
                    }
                    i--;
                } catch (Exception e2) {
                    return;
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerService.1 */
    class C01881 extends BroadcastReceiver {
        C01881() {
        }

        public void onReceive(Context context, Intent intent) {
            if (AudioPlayerService.B_ACTION_PLAYER_CONTROL.equals(intent.getAction())) {
                int act = intent.getIntExtra("action", -1);
                Log.m525d("vk", "action=" + act);
                switch (act) {
                    case AudioPlayerService.STATE_PLAYING /*1*/:
                        AudioPlayerService.this.currentPlaylist = null;
                        AudioPlayerService.this.statusUserID = intent.getIntExtra("act_uid", 0);
                        AudioPlayerService.this.playNewFile((AudioFile) intent.getParcelableExtra("file"));
                        break;
                    case AudioPlayerService.STATE_PAUSED /*2*/:
                        Parcelable[] pa = intent.getParcelableArrayExtra("list");
                        AudioFile[] af = new AudioFile[pa.length];
                        System.arraycopy(pa, 0, af, 0, pa.length);
                        AudioPlayerService.this.statusUserID = intent.getIntExtra("act_uid", 0);
                        AudioPlayerService.this.playNewPlaylist(af, intent.getIntExtra(GLFilterContext.AttributePosition, 0));
                        break;
                    case AudioPlayerService.STATE_INITING /*3*/:
                        AudioPlayerService.this.togglePlayPause();
                        break;
                    case AudioPlayerService.ACTION_SHOW_PLAYER /*4*/:
                        AudioPlayerService.this.showPlayer(!intent.hasExtra("no_anim"), intent.hasExtra("from_notify"));
                        break;
                    case AudioPlayerService.ACTION_NEXT_TRACK /*5*/:
                        AudioPlayerService.this.nextTrack();
                        break;
                    case AudioPlayerService.ACTION_PREV_TRACK /*6*/:
                        AudioPlayerService.this.prevTrack();
                        break;
                    case AudioPlayerService.ACTION_PAUSE_IF_PLAYING /*7*/:
                        if (AudioPlayerService.this.isPlaying()) {
                            AudioPlayerService.this.pausedBySystem = true;
                            AudioPlayerService.this.togglePlayPause();
                            break;
                        }
                        break;
                    case AudioPlayerService.ACTION_PLAY_IF_PAUSED /*8*/:
                        if (!AudioPlayerService.this.isPlaying() && AudioPlayerService.this.pausedBySystem) {
                            AudioPlayerService.this.togglePlayPause();
                            break;
                        }
                    case AudioPlayerService.ACTION_TOGGLE_REPEAT /*9*/:
                        AudioPlayerService.this.setLoop(!AudioPlayerService.this.isLoop());
                        break;
                    case AudioPlayerService.ACTION_TOGGLE_SHUFFLE /*10*/:
                        AudioPlayerService.this.setRandom(!AudioPlayerService.this.isRandom());
                        break;
                }
            }
            if ("android.intent.action.HEADSET_PLUG".equals(intent.getAction())) {
                boolean newPlugState;
                if (intent.getIntExtra("state", 0) == AudioPlayerService.STATE_PLAYING) {
                    newPlugState = true;
                } else {
                    newPlugState = false;
                }
                if (!newPlugState && AudioPlayerService.this.headsetPlugState) {
                    if (AudioPlayerService.this.isPlaying()) {
                        AudioPlayerService.this.togglePlayPause();
                    } else if (AudioPlayerService.this.pausedByCall) {
                        AudioPlayerService.this.startAfterCall = false;
                    }
                }
                AudioPlayerService.this.headsetPlugState = newPlugState;
            }
            if (AudioCache.ACTION_ALBUM_ART_AVAILABLE.equals(intent.getAction())) {
                int aid = intent.getIntExtra("aid", 0);
                int oid = intent.getIntExtra("oid", 0);
                Log.m525d("vk", "PS Cover available: " + aid + "_" + oid);
                if (AudioPlayerService.this.currentFile != null && AudioPlayerService.this.currentFile.aid == aid && AudioPlayerService.this.currentFile.oid == oid) {
                    AudioPlayerService.this.getCoverForWidgets();
                    AudioPlayerService.this.updateNotification();
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerService.2 */
    class C01892 implements Runnable {
        private final /* synthetic */ AudioFile val$f;

        C01892(AudioFile audioFile) {
            this.val$f = audioFile;
        }

        public void run() {
            try {
                Thread.sleep(2000);
            } catch (Exception e) {
            }
            if (AudioPlayerService.this.currentFile != null && this.val$f.oid == AudioPlayerService.this.currentFile.oid && this.val$f.aid == AudioPlayerService.this.currentFile.aid) {
                AudioPlayerService.this.updateBroadcast();
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerService.3 */
    class C01903 implements Runnable {

        /* renamed from: com.vkontakte.android.AudioPlayerService.3.1 */
        class C12691 extends APIHandler {
            C12691() {
            }

            public void success(JSONObject r) {
                try {
                    AudioPlayerService.this.currentFile.url = r.getString("response");
                    AudioPlayerService.this.doStartPlayer();
                } catch (Exception e) {
                }
            }

            public void fail(int ecode, String emsg) {
                Log.m530w("vk", "error getting file url " + ecode + " " + emsg);
                AudioPlayerService.this.onError(null, AudioPlayerService.STATE_PLAYING, -1004);
            }
        }

        C01903() {
        }

        public void run() {
            AudioPlayerService.pauseAfterInit = false;
            if (AudioPlayerService.this.currentFile.url != null || AudioCache.isCached(AudioPlayerService.this.currentFile.oid, AudioPlayerService.this.currentFile.aid)) {
                AudioPlayerService.this.doStartPlayer();
            } else {
                new APIRequest("execute").param("code", "return API.audio.getById({\"audios\":\"" + AudioPlayerService.this.currentFile.oid + "_" + AudioPlayerService.this.currentFile.aid + "\"})[0].url;").handler(new C12691()).execSync();
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerService.4 */
    class C01914 implements OnPreparedListener {
        C01914() {
        }

        public void onPrepared(MediaPlayer mp) {
            try {
                if (AudioPlayerService.this.needSeekTo != 0) {
                    AudioPlayerService.this.player.seekTo(AudioPlayerService.this.needSeekTo);
                    AudioPlayerService.this.needSeekTo = 0;
                }
                AudioPlayerService.this.player.setLooping(AudioPlayerService.this.loop);
                if (!AudioPlayerService.pauseAfterInit) {
                    AudioPlayerService.this.player.start();
                }
                if (AudioPlayerService.this.playerCallback != null) {
                    AudioPlayerService.this.errorRetries = AudioPlayerService.ACTION_NEXT_TRACK;
                    try {
                        AudioPlayerService.this.playerCallback.displayInfo(AudioPlayerService.this.currentFile);
                        AudioPlayerService.this.playerCallback.setPlaying(AudioPlayerService.this.player.isPlaying());
                    } catch (Exception e) {
                    }
                }
                AudioPlayerService.this.initing = false;
                Log.m525d("vk", "player started");
                if (AudioPlayerService.this.playerStopTimer != null) {
                    try {
                        AudioPlayerService.this.playerStopTimer.cancel();
                    } catch (Exception e2) {
                    }
                }
                AudioPlayerService.this.playerStopTimer = null;
                if (AudioPlayerService.this.serviceStopTimer != null) {
                    try {
                        AudioPlayerService.this.serviceStopTimer.cancel();
                    } catch (Exception e3) {
                    }
                }
                AudioPlayerService.this.serviceStopTimer = null;
                if (!AudioPlayerService.pauseAfterInit) {
                    AudioPlayerService.this.broadcastPlayStateChanged(true);
                }
                AudioPlayerService.this.startUpdatingProgress();
                AudioPlayerService.pauseAfterInit = false;
            } catch (Exception e4) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerService.5 */
    class C01925 implements Runnable {
        C01925() {
        }

        public void run() {
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
            }
            if (AudioPlayerService.this.cancelReleaseWifiLock) {
                AudioPlayerService.this.cancelReleaseWifiLock = false;
                return;
            }
            if (AudioPlayerService.this.wifiLock.isHeld()) {
                AudioPlayerService.this.wifiLock.release();
            }
            Log.m525d("vk", "released wifi lock");
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerService.7 */
    class C01937 implements OnAudioFocusChangeListener {
        C01937() {
        }

        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioPlayerService.STATE_PLAYING) {
                Log.m525d("vk", "AUDIOFOCUS_GAIN, paused by system=" + AudioPlayerService.this.pausedBySystem + ", paused by call=" + AudioPlayerService.this.pausedByCall);
                if (!(!AudioPlayerService.this.startAfterCall || AudioPlayerService.this.player == null || AudioPlayerService.this.haveAudioFocus)) {
                    if (AudioPlayerService.this.isPlaying()) {
                        AudioPlayerService.this.fadeIn(0.2f);
                    } else if (AudioPlayerService.this.pausedBySystem) {
                        AudioPlayerService.this.pausedBySystem = false;
                        AudioPlayerService.this.togglePlayPause();
                    }
                }
                AudioPlayerService.this.startAfterCall = true;
                AudioPlayerService.this.haveAudioFocus = true;
            }
            if (focusChange == -1) {
                Log.m525d("vk", "AUDIOFOCUS_LOSS");
                if (AudioPlayerService.this.player != null && AudioPlayerService.this.haveAudioFocus) {
                    try {
                        if (AudioPlayerService.this.player.isPlaying()) {
                            AudioPlayerService.this.togglePlayPause();
                            AudioPlayerService.this.pausedBySystem = true;
                        }
                    } catch (Exception e) {
                    }
                }
                AudioPlayerService.this.haveAudioFocus = false;
            }
            if (focusChange == -2) {
                Log.m525d("vk", "AUDIOFOCUS_LOSS_TRANSIENT");
                if (AudioPlayerService.this.player != null) {
                    try {
                        if (AudioPlayerService.this.player.isPlaying()) {
                            AudioPlayerService.this.pausedBySystem = true;
                            AudioPlayerService.this.togglePlayPause();
                        }
                    } catch (Exception e2) {
                    }
                }
                AudioPlayerService.this.haveAudioFocus = false;
            }
            if (focusChange == -3) {
                Log.m525d("vk", "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                if (AudioPlayerService.this.player != null) {
                    try {
                        if (AudioPlayerService.this.player.isPlaying()) {
                            AudioPlayerService.this.fadeOut(0.2f);
                        }
                    } catch (Exception e3) {
                    }
                }
                AudioPlayerService.this.haveAudioFocus = false;
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerService.8 */
    class C01948 implements OnGetPlaybackPositionListener {
        C01948() {
        }

        public long onGetPlaybackPosition() {
            if (AudioPlayerService.this.player != null) {
                try {
                    return (long) AudioPlayerService.this.player.getCurrentPosition();
                } catch (Exception e) {
                }
            }
            return 0;
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerService.9 */
    class C01959 implements OnPlaybackPositionUpdateListener {
        C01959() {
        }

        public void onPlaybackPositionUpdate(long newPositionMs) {
            if (AudioPlayerService.this.player != null) {
                try {
                    AudioPlayerService.this.player.seekTo((int) newPositionMs);
                    AudioPlayerService.this.remoteControlClient.setPlaybackState(AudioPlayerService.this.player.isPlaying() ? AudioPlayerService.STATE_INITING : AudioPlayerService.STATE_PAUSED, (long) AudioPlayerService.this.player.getCurrentPosition(), 1.0f);
                } catch (Exception e) {
                }
            }
        }
    }

    public interface AttachViewCallback {
        void onPlayStateChanged(int i, int i2, int i3);
    }

    private class CallStateReceiver extends PhoneStateListener {
        private CallStateReceiver() {
        }

        public void onCallStateChanged(int state, String incomingNumber) {
            if (!(AudioPlayerService.this.pausedByCall || state == 0 || !AudioPlayerService.this.isPlaying() || AudioPlayerService.this.player == null)) {
                AudioPlayerService.this.pausedByCall = true;
                if (AudioPlayerService.this.player != null && AudioPlayerService.this.isPlaying()) {
                    AudioPlayerService.this.togglePlayPause();
                }
            }
            if (AudioPlayerService.this.pausedByCall && state == 0) {
                AudioPlayerService.this.pausedByCall = false;
                if (AudioPlayerService.this.startAfterCall && !AudioPlayerService.this.isPlaying()) {
                    AudioPlayerService.this.togglePlayPause();
                    AudioPlayerService.this.fadeIn();
                }
            }
        }
    }

    public interface PlayerCallback {
        void displayInfo(AudioFile audioFile);

        void setBuffered(int i);

        void setNumber(int i, int i2);

        void setPlaying(boolean z);

        void setPosition(int i, long j);
    }

    public interface ProgressCallback {
        void onBufferProgressChanged(int i, int i2, int i3);

        void onPlayProgressChanged(int i, int i2, int i3);
    }

    private class ReleasePlayerRunnable extends TimerTask {
        private ReleasePlayerRunnable() {
        }

        public void run() {
            if (AudioPlayerService.this.player != null) {
                try {
                    AudioPlayerService.this.player.stop();
                    AudioPlayerService.this.player.release();
                    AudioPlayerService.this.player = null;
                    AudioPlayerService.this.playerStopTimer = null;
                    Log.m525d("vk", "Player released.");
                } catch (Exception e) {
                }
            }
        }
    }

    private class StopServiceRunnable extends TimerTask {
        private StopServiceRunnable() {
        }

        public void run() {
            AudioPlayerService.this.stopSelf();
            AudioPlayerService.this.serviceStopTimer = null;
        }
    }

    private class UpdateProgressTimerTask extends TimerTask {
        private UpdateProgressTimerTask() {
        }

        public void run() {
            if (AudioPlayerService.this.player == null) {
                return;
            }
            if (AudioPlayerService.this.playerCallback != null || AudioPlayerService.progressCallbacks.size() > 0) {
                float _p = 0.0f;
                long pos = 0;
                if (!AudioPlayerService.this.initing) {
                    try {
                        pos = (long) AudioPlayerService.this.player.getCurrentPosition();
                        _p = ((float) AudioPlayerService.this.player.getCurrentPosition()) / ((float) AudioPlayerService.this.player.getDuration());
                    } catch (Exception e) {
                    }
                }
                if (AudioPlayerService.this.playerCallback != null) {
                    float p = _p;
                    if (AudioPlayerService.this.playerCallback != null) {
                        AudioPlayerService.this.playerCallback.setPosition((int) (p * 1000.0f), pos);
                    }
                }
                if (AudioPlayerService.this.currentFile != null) {
                    Iterator it = AudioPlayerService.progressCallbacks.iterator();
                    while (it.hasNext()) {
                        ((ProgressCallback) it.next()).onPlayProgressChanged(AudioPlayerService.this.currentFile.oid, AudioPlayerService.this.currentFile.aid, (int) (_p * 1000.0f));
                    }
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerService.6 */
    class C12706 extends APIHandler {
        C12706() {
        }

        public void success(JSONObject r) {
            try {
                JSONObject audio = r.getJSONObject("response").optJSONObject("audio");
                if (audio != null) {
                    AudioPlayerService.this.currentPlaylist.add(new AudioFile(audio));
                    AudioPlayerService.this.playNewFile((AudioFile) AudioPlayerService.this.currentPlaylist.get(AudioPlayerService.this.playlistPosition));
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }

        public void fail(int ecode, String emsg) {
        }
    }

    public AudioPlayerService() {
        boolean z = true;
        this.player = null;
        this.timer = null;
        this.nBuffered = 0;
        this.currentFile = null;
        this.currentPlaylist = new ArrayList();
        this.randomPlaylist = new ArrayList();
        this.playlistPosition = -1;
        this.initing = false;
        this.attachViews = new Vector();
        this.loop = false;
        this.random = false;
        this.cacheCurrent = false;
        this.pausedBySystem = false;
        this.currentIsCached = false;
        this.errorRetries = ACTION_NEXT_TRACK;
        this.statusUserID = 0;
        this.cancelReleaseWifiLock = false;
        this.telManager = null;
        this.pausedByCall = false;
        this.callStateReceiver = new CallStateReceiver();
        this.buggyPlayer = false;
        this.headsetPlugState = false;
        this.haveAudioFocus = false;
        this.needSeekTo = 0;
        this.startAfterCall = true;
        if (VERSION.SDK_INT < 14) {
            z = false;
        }
        this.useCustomNotification = z;
        this.error = false;
        this.receiver = new C01881();
    }

    static {
        attachCallbacks = new ArrayList();
        progressCallbacks = new ArrayList();
        pauseAfterInit = false;
    }

    public static void addAttachViewCallback(AttachViewCallback c) {
        attachCallbacks.add(c);
    }

    public static void removeAttachViewCallback(AttachViewCallback c) {
        attachCallbacks.remove(c);
    }

    public static void addProgressCallback(ProgressCallback c) {
        progressCallbacks.add(c);
        if (sharedInstance != null) {
            sharedInstance.startUpdatingProgress();
        }
    }

    public static void removeProgressCallback(ProgressCallback c) {
        progressCallbacks.remove(c);
        if (sharedInstance != null && progressCallbacks.size() == 0) {
            sharedInstance.stopUpdatingProgress();
        }
    }

    private void startUpdatingProgress() {
        if (this.timer != null) {
            return;
        }
        if (this.playerCallback != null || progressCallbacks.size() > 0) {
            this.timer = new Timer();
            this.timer.schedule(new UpdateProgressTimerTask(), 0, 200);
        }
    }

    private void stopUpdatingProgress() {
        if (this.timer != null && this.playerCallback == null) {
            this.timer.cancel();
            this.timer = null;
        }
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onCreate() {
        sharedInstance = this;
        this.nm = (NotificationManager) getSystemService("notification");
        this.notification = new Notification(C0436R.drawable.ic_stat_notify_play, null, 0);
        Notification notification = this.notification;
        notification.flags |= 34;
        Intent intent = new Intent(this, getClass());
        intent.putExtra("action", ACTION_SHOW_PLAYER);
        intent.putExtra("from_notify", true);
        PendingIntent pintent = PendingIntent.getService(this, 0, intent, 134217728);
        notificationContentIntent = pintent;
        this.notification.setLatestEventInfo(this, null, null, pintent);
        if (cacheProxy != null) {
            cacheProxy.stop();
        }
        cacheProxy = new Proxy(getApplicationContext());
        cacheProxy.start();
        if (this.wifiLock == null) {
            this.wifiLock = ((WifiManager) getSystemService("wifi")).createWifiLock("vk_audio_streaming_player");
        }
        if (this.telManager == null) {
            this.telManager = (TelephonyManager) getSystemService("phone");
            this.telManager.listen(this.callStateReceiver, 32);
        }
        try {
            this.buggyPlayer = new File("/system/lib/libCedarX.so").exists();
            if (this.buggyPlayer) {
                Log.m530w("vk", "Found CedarX, enabling workaround!");
            }
        } catch (Throwable th) {
        }
    }

    public void onStart(Intent intent, int startId) {
        onStartCommand(intent, 0, startId);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean z = true;
        boolean z2 = false;
        int act = intent.getIntExtra("action", -1);
        Log.m525d("vk", "action=" + act);
        switch (act) {
            case STATE_PLAYING /*1*/:
                this.currentPlaylist = null;
                this.statusUserID = intent.getIntExtra("act_uid", 0);
                this.currentPlaylist = new ArrayList();
                this.currentPlaylist.add((AudioFile) intent.getParcelableExtra("file"));
                playNewFile((AudioFile) intent.getParcelableExtra("file"));
                break;
            case STATE_PAUSED /*2*/:
                if (!intent.hasExtra("list_al")) {
                    Parcelable[] pa = intent.getParcelableArrayExtra("list");
                    this.statusUserID = intent.getIntExtra("act_uid", 0);
                    if (pa.length != 0) {
                        AudioFile[] af = new AudioFile[pa.length];
                        System.arraycopy(pa, 0, af, 0, pa.length);
                        playNewPlaylist(af, intent.getIntExtra(GLFilterContext.AttributePosition, 0));
                        break;
                    }
                }
                ArrayList<AudioFile> files = intent.getParcelableArrayListExtra("list_al");
                if (files.size() != 0) {
                    playNewPlaylist((AudioFile[]) files.toArray(new AudioFile[0]), intent.getIntExtra(GLFilterContext.AttributePosition, 0));
                    break;
                }
                playNewPlaylist((AudioFile[]) listToPlay.toArray(new AudioFile[0]), intent.getIntExtra(GLFilterContext.AttributePosition, 0));
                break;
                break;
            case STATE_INITING /*3*/:
                if (intent.hasExtra("from_notify")) {
                    z = false;
                }
                togglePlayPause(z);
                break;
            case ACTION_SHOW_PLAYER /*4*/:
                if (!intent.hasExtra("no_anim")) {
                    z2 = true;
                }
                showPlayer(z2, intent.hasExtra("from_notify"));
                break;
            case ACTION_NEXT_TRACK /*5*/:
                nextTrack();
                break;
            case ACTION_PREV_TRACK /*6*/:
                prevTrack();
                break;
            case ACTION_TOGGLE_REPEAT /*9*/:
                if (!isLoop()) {
                    z2 = true;
                }
                setLoop(z2);
                break;
            case ACTION_TOGGLE_SHUFFLE /*10*/:
                if (!isRandom()) {
                    z2 = true;
                }
                setRandom(z2);
                break;
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(B_ACTION_PLAYER_CONTROL);
        filter.addAction("android.intent.action.HEADSET_PLUG");
        filter.addAction(AudioCache.ACTION_ALBUM_ART_AVAILABLE);
        registerReceiver(this.receiver, filter);
        return STATE_PAUSED;
    }

    private void playNewFile(AudioFile f) {
        this.needSeekTo = 0;
        Log.m525d("vk", "New file " + f);
        if (f != null) {
            if (this.currentFile != null) {
                AudioCache.endPlayback(this.currentFile.oid, this.currentFile.aid);
                broadcastPlayStateChanged(false);
            }
            this.currentFile = f;
            registerRemoteControl();
            broadcastPlayStateChanged(false);
            Thread t = new Thread(new C01892(f));
            t.setPriority(STATE_PLAYING);
            t.start();
            this.attachViews.clear();
            if (this.playerCallback != null) {
                this.playerCallback.setPosition(0, 0);
                this.playerCallback.setBuffered(0);
                this.playerCallback.displayInfo(f);
                if (this.currentPlaylist != null) {
                    this.playerCallback.setNumber(this.playlistPosition + STATE_PLAYING, this.currentPlaylist.size());
                }
            }
            this.initing = true;
            this.notification.setLatestEventInfo(this, f.artist, f.title, notificationContentIntent);
            updateNotification();
            Iterator it = attachCallbacks.iterator();
            while (it.hasNext()) {
                ((AttachViewCallback) it.next()).onPlayStateChanged(this.currentFile.oid, this.currentFile.aid, STATE_INITING);
            }
            new Thread(new C01903()).start();
            updateWidgets();
        }
    }

    public boolean isEnoughSpaceToCache() {
        return getExternalFreeSpace() > MIN_SPACE_TO_CACHE;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void doStartPlayer() {
        /*
        r10 = this;
        r5 = r10.player;
        if (r5 == 0) goto L_0x000e;
    L_0x0004:
        r5 = r10.player;	 Catch:{ Exception -> 0x0228 }
        r5.stop();	 Catch:{ Exception -> 0x0228 }
    L_0x0009:
        r5 = r10.player;
        r5.release();
    L_0x000e:
        monitor-enter(r10);
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        if (r5 == 0) goto L_0x0018;
    L_0x0013:
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        r5.release();	 Catch:{ Exception -> 0x0215 }
    L_0x0018:
        r5 = "vk";
        r6 = "player starting";
        com.vkontakte.android.Log.m525d(r5, r6);	 Catch:{ Exception -> 0x0215 }
        r5 = 1;
        r10.initing = r5;	 Catch:{ Exception -> 0x0215 }
        r5 = 0;
        r10.nBuffered = r5;	 Catch:{ Exception -> 0x0215 }
        r5 = 0;
        r10.error = r5;	 Catch:{ Exception -> 0x0215 }
        r5 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r5 = r5.oid;	 Catch:{ Exception -> 0x0215 }
        r6 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r6 = r6.aid;	 Catch:{ Exception -> 0x0215 }
        r5 = com.vkontakte.android.cache.AudioCache.isCached(r5, r6);	 Catch:{ Exception -> 0x0215 }
        if (r5 == 0) goto L_0x0045;
    L_0x0036:
        r5 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r5 = r5.url;	 Catch:{ Exception -> 0x0215 }
        r6 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r6 = r6.oid;	 Catch:{ Exception -> 0x0215 }
        r7 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r7 = r7.aid;	 Catch:{ Exception -> 0x0215 }
        com.vkontakte.android.cache.AudioCache.checkFileSize(r5, r6, r7);	 Catch:{ Exception -> 0x0215 }
    L_0x0045:
        r5 = new android.media.MediaPlayer;	 Catch:{ Exception -> 0x0215 }
        r5.<init>();	 Catch:{ Exception -> 0x0215 }
        r10.player = r5;	 Catch:{ Exception -> 0x0215 }
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        if (r5 != 0) goto L_0x0059;
    L_0x0050:
        r5 = "vk";
        r6 = "Player=null";
        com.vkontakte.android.Log.m526e(r5, r6);	 Catch:{ Exception -> 0x0215 }
        monitor-exit(r10);	 Catch:{ all -> 0x013b }
    L_0x0058:
        return;
    L_0x0059:
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        r6 = r10.getApplicationContext();	 Catch:{ Exception -> 0x0215 }
        r7 = 1;
        r5.setWakeMode(r6, r7);	 Catch:{ Exception -> 0x0215 }
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        r5.setOnBufferingUpdateListener(r10);	 Catch:{ Exception -> 0x0215 }
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        r5.setOnCompletionListener(r10);	 Catch:{ Exception -> 0x0215 }
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        r5.setOnErrorListener(r10);	 Catch:{ Exception -> 0x0215 }
        r5 = 0;
        r10.currentIsCached = r5;	 Catch:{ Exception -> 0x0215 }
        r5 = "mounted";
        r6 = android.os.Environment.getExternalStorageState();	 Catch:{ Exception -> 0x0215 }
        r5 = r5.equals(r6);	 Catch:{ Exception -> 0x0215 }
        if (r5 == 0) goto L_0x021d;
    L_0x0081:
        r5 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r5 = r5.oid;	 Catch:{ Exception -> 0x0215 }
        r6 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r6 = r6.aid;	 Catch:{ Exception -> 0x0215 }
        r2 = com.vkontakte.android.cache.AudioCache.isCached(r5, r6);	 Catch:{ Exception -> 0x0215 }
        r5 = new java.io.File;	 Catch:{ Exception -> 0x0215 }
        r6 = android.os.Environment.getExternalStorageDirectory();	 Catch:{ Exception -> 0x0215 }
        r7 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0215 }
        r8 = ".vkontakte/cache/audio/";
        r7.<init>(r8);	 Catch:{ Exception -> 0x0215 }
        r8 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r8 = r8.oid;	 Catch:{ Exception -> 0x0215 }
        r7 = r7.append(r8);	 Catch:{ Exception -> 0x0215 }
        r8 = "_";
        r7 = r7.append(r8);	 Catch:{ Exception -> 0x0215 }
        r8 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r8 = r8.aid;	 Catch:{ Exception -> 0x0215 }
        r7 = r7.append(r8);	 Catch:{ Exception -> 0x0215 }
        r7 = r7.toString();	 Catch:{ Exception -> 0x0215 }
        r5.<init>(r6, r7);	 Catch:{ Exception -> 0x0215 }
        r5 = r5.exists();	 Catch:{ Exception -> 0x0215 }
        if (r5 != 0) goto L_0x00c3;
    L_0x00bd:
        r2 = 0;
        r5 = sharedInstance;	 Catch:{ Exception -> 0x0215 }
        com.vkontakte.android.cache.AudioCache.refillIDs(r5);	 Catch:{ Exception -> 0x0215 }
    L_0x00c3:
        r5 = android.preference.PreferenceManager.getDefaultSharedPreferences(r10);	 Catch:{ Exception -> 0x0215 }
        r6 = "enableAudioCache";
        r7 = 1;
        r1 = r5.getBoolean(r6, r7);	 Catch:{ Exception -> 0x0215 }
        if (r2 == 0) goto L_0x00db;
    L_0x00d0:
        r5 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r5 = r5.oid;	 Catch:{ Exception -> 0x0215 }
        r6 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r6 = r6.aid;	 Catch:{ Exception -> 0x0215 }
        com.vkontakte.android.cache.AlbumArtRetriever.saveCoversFromFile(r5, r6);	 Catch:{ Exception -> 0x0215 }
    L_0x00db:
        if (r2 == 0) goto L_0x013e;
    L_0x00dd:
        r5 = r10.buggyPlayer;	 Catch:{ Exception -> 0x0215 }
        if (r5 != 0) goto L_0x013e;
    L_0x00e1:
        r5 = 1;
        r10.currentIsCached = r5;	 Catch:{ Exception -> 0x0215 }
        r5 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r5 = r5.oid;	 Catch:{ Exception -> 0x0215 }
        r6 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r6 = r6.aid;	 Catch:{ Exception -> 0x0215 }
        com.vkontakte.android.cache.AudioCache.updatePlayTime(r5, r6);	 Catch:{ Exception -> 0x0215 }
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        r6 = new java.io.File;	 Catch:{ Exception -> 0x0215 }
        r7 = android.os.Environment.getExternalStorageDirectory();	 Catch:{ Exception -> 0x0215 }
        r8 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0215 }
        r9 = ".vkontakte/cache/audio/";
        r8.<init>(r9);	 Catch:{ Exception -> 0x0215 }
        r9 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r9 = r9.oid;	 Catch:{ Exception -> 0x0215 }
        r8 = r8.append(r9);	 Catch:{ Exception -> 0x0215 }
        r9 = "_";
        r8 = r8.append(r9);	 Catch:{ Exception -> 0x0215 }
        r9 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r9 = r9.aid;	 Catch:{ Exception -> 0x0215 }
        r8 = r8.append(r9);	 Catch:{ Exception -> 0x0215 }
        r8 = r8.toString();	 Catch:{ Exception -> 0x0215 }
        r6.<init>(r7, r8);	 Catch:{ Exception -> 0x0215 }
        r6 = r6.getAbsolutePath();	 Catch:{ Exception -> 0x0215 }
        r5.setDataSource(r6);	 Catch:{ Exception -> 0x0215 }
    L_0x0122:
        r5 = "vk";
        r6 = "player preparing";
        com.vkontakte.android.Log.m525d(r5, r6);	 Catch:{ Exception -> 0x0215 }
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        r5.prepareAsync();	 Catch:{ Exception -> 0x0215 }
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        r6 = new com.vkontakte.android.AudioPlayerService$4;	 Catch:{ Exception -> 0x0215 }
        r6.<init>();	 Catch:{ Exception -> 0x0215 }
        r5.setOnPreparedListener(r6);	 Catch:{ Exception -> 0x0215 }
    L_0x0138:
        monitor-exit(r10);	 Catch:{ all -> 0x013b }
        goto L_0x0058;
    L_0x013b:
        r5 = move-exception;
        monitor-exit(r10);	 Catch:{ all -> 0x013b }
        throw r5;
    L_0x013e:
        if (r1 == 0) goto L_0x014b;
    L_0x0140:
        r5 = r10.getExternalFreeSpace();	 Catch:{ Exception -> 0x0215 }
        r7 = 52428800; // 0x3200000 float:4.7019774E-37 double:2.5903269E-316;
        r5 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1));
        if (r5 > 0) goto L_0x0151;
    L_0x014b:
        r5 = r10.buggyPlayer;	 Catch:{ Exception -> 0x0215 }
        if (r5 == 0) goto L_0x0201;
    L_0x014f:
        if (r2 == 0) goto L_0x0201;
    L_0x0151:
        if (r2 == 0) goto L_0x0159;
    L_0x0153:
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        r6 = 0;
        r5.setOnBufferingUpdateListener(r6);	 Catch:{ Exception -> 0x0215 }
    L_0x0159:
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0215 }
        r6 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r6 = r6.url;	 Catch:{ Exception -> 0x0215 }
        r6 = java.lang.String.valueOf(r6);	 Catch:{ Exception -> 0x0215 }
        r5.<init>(r6);	 Catch:{ Exception -> 0x0215 }
        r6 = "___";
        r5 = r5.append(r6);	 Catch:{ Exception -> 0x0215 }
        r6 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r6 = r6.oid;	 Catch:{ Exception -> 0x0215 }
        r5 = r5.append(r6);	 Catch:{ Exception -> 0x0215 }
        r6 = "___";
        r5 = r5.append(r6);	 Catch:{ Exception -> 0x0215 }
        r6 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r6 = r6.aid;	 Catch:{ Exception -> 0x0215 }
        r5 = r5.append(r6);	 Catch:{ Exception -> 0x0215 }
        r3 = r5.toString();	 Catch:{ Exception -> 0x0215 }
        r5 = android.os.Build.VERSION.SDK;	 Catch:{ Exception -> 0x01e5 }
        r5 = java.lang.Integer.parseInt(r5);	 Catch:{ Exception -> 0x01e5 }
        r6 = 8;
        if (r5 < r6) goto L_0x01eb;
    L_0x0190:
        r5 = "android.util.Base64";
        r5 = java.lang.Class.forName(r5);	 Catch:{ Exception -> 0x01e5 }
        r6 = "encodeToString";
        r7 = 2;
        r7 = new java.lang.Class[r7];	 Catch:{ Exception -> 0x01e5 }
        r8 = 0;
        r9 = byte[].class;
        r7[r8] = r9;	 Catch:{ Exception -> 0x01e5 }
        r8 = 1;
        r9 = java.lang.Integer.TYPE;	 Catch:{ Exception -> 0x01e5 }
        r7[r8] = r9;	 Catch:{ Exception -> 0x01e5 }
        r5 = r5.getMethod(r6, r7);	 Catch:{ Exception -> 0x01e5 }
        r6 = 0;
        r7 = 2;
        r7 = new java.lang.Object[r7];	 Catch:{ Exception -> 0x01e5 }
        r8 = 0;
        r9 = "UTF-8";
        r9 = r3.getBytes(r9);	 Catch:{ Exception -> 0x01e5 }
        r7[r8] = r9;	 Catch:{ Exception -> 0x01e5 }
        r8 = 1;
        r9 = 11;
        r9 = java.lang.Integer.valueOf(r9);	 Catch:{ Exception -> 0x01e5 }
        r7[r8] = r9;	 Catch:{ Exception -> 0x01e5 }
        r0 = r5.invoke(r6, r7);	 Catch:{ Exception -> 0x01e5 }
        r0 = (java.lang.String) r0;	 Catch:{ Exception -> 0x01e5 }
        r6 = r10.player;	 Catch:{ Exception -> 0x01e5 }
        r5 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x01e5 }
        r7 = "http://127.0.0.1:48329/";
        r5.<init>(r7);	 Catch:{ Exception -> 0x01e5 }
        r7 = r5.append(r0);	 Catch:{ Exception -> 0x01e5 }
        r5 = r10.buggyPlayer;	 Catch:{ Exception -> 0x01e5 }
        if (r5 == 0) goto L_0x01e8;
    L_0x01d6:
        r5 = ".mp3";
    L_0x01d8:
        r5 = r7.append(r5);	 Catch:{ Exception -> 0x01e5 }
        r5 = r5.toString();	 Catch:{ Exception -> 0x01e5 }
        r6.setDataSource(r5);	 Catch:{ Exception -> 0x01e5 }
        goto L_0x0122;
    L_0x01e5:
        r5 = move-exception;
        goto L_0x0122;
    L_0x01e8:
        r5 = "";
        goto L_0x01d8;
    L_0x01eb:
        r5 = r10.player;	 Catch:{ Exception -> 0x01e5 }
        r6 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x01e5 }
        r7 = "http://127.0.0.1:48329/";
        r6.<init>(r7);	 Catch:{ Exception -> 0x01e5 }
        r6 = r6.append(r3);	 Catch:{ Exception -> 0x01e5 }
        r6 = r6.toString();	 Catch:{ Exception -> 0x01e5 }
        r5.setDataSource(r6);	 Catch:{ Exception -> 0x01e5 }
        goto L_0x0122;
    L_0x0201:
        r5 = com.vkontakte.android.NetworkStateReceiver.isConnected();	 Catch:{ Exception -> 0x0215 }
        if (r5 != 0) goto L_0x020a;
    L_0x0207:
        monitor-exit(r10);	 Catch:{ all -> 0x013b }
        goto L_0x0058;
    L_0x020a:
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        r6 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r6 = r6.url;	 Catch:{ Exception -> 0x0215 }
        r5.setDataSource(r6);	 Catch:{ Exception -> 0x0215 }
        goto L_0x0122;
    L_0x0215:
        r4 = move-exception;
        r5 = "vk";
        com.vkontakte.android.Log.m532w(r5, r4);	 Catch:{ all -> 0x013b }
        goto L_0x0138;
    L_0x021d:
        r5 = r10.player;	 Catch:{ Exception -> 0x0215 }
        r6 = r10.currentFile;	 Catch:{ Exception -> 0x0215 }
        r6 = r6.url;	 Catch:{ Exception -> 0x0215 }
        r5.setDataSource(r6);	 Catch:{ Exception -> 0x0215 }
        goto L_0x0122;
    L_0x0228:
        r5 = move-exception;
        goto L_0x0009;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.AudioPlayerService.doStartPlayer():void");
    }

    private void playNewPlaylist(AudioFile[] files, int startPos) {
        this.currentPlaylist.clear();
        this.currentPlaylist.addAll(Arrays.asList(files));
        if (this.random) {
            this.randomPlaylist.clear();
            this.randomPlaylist.addAll(this.currentPlaylist);
            Collections.shuffle(this.randomPlaylist);
        }
        this.playlistPosition = startPos;
        playNewFile(files[startPos]);
    }

    public void togglePlayPause() {
        togglePlayPause(true);
    }

    public void togglePlayPause(boolean cancelNotify) {
        boolean z = false;
        if (this.currentFile != null) {
            if (this.currentFile.url == null && !isPlaying() && !AudioCache.isCached(this.currentFile.oid, this.currentFile.aid)) {
                playNewFile(this.currentFile);
            } else if (this.initing) {
                boolean z2;
                if (pauseAfterInit) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                pauseAfterInit = z2;
                if (pauseAfterInit) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                broadcastPlayStateChanged(z2);
                if (this.playerCallback != null) {
                    PlayerCallback playerCallback = this.playerCallback;
                    if (!pauseAfterInit) {
                        z = true;
                    }
                    playerCallback.setPlaying(z);
                }
                updateWidgets();
            } else if (this.player == null) {
                if (this.serviceStopTimer != null) {
                    try {
                        this.serviceStopTimer.cancel();
                    } catch (Exception e) {
                    }
                }
                this.serviceStopTimer = null;
                doStartPlayer();
                updateNotification();
            } else {
                if (this.player.isPlaying()) {
                    this.player.pause();
                    this.needSeekTo = this.player.getCurrentPosition();
                    if (cancelNotify) {
                        stopForeground(true);
                    } else {
                        updateNotification();
                    }
                    if (this.playerStopTimer != null) {
                        try {
                            this.playerStopTimer.cancel();
                        } catch (Exception e2) {
                        }
                    }
                    try {
                        this.playerStopTimer = new Timer();
                        this.playerStopTimer.schedule(new ReleasePlayerRunnable(), PLAYER_RELEASE_DELAY);
                        if (this.serviceStopTimer != null) {
                            try {
                                this.serviceStopTimer.cancel();
                            } catch (Exception e3) {
                            }
                        }
                        this.serviceStopTimer = new Timer();
                        this.serviceStopTimer.schedule(new StopServiceRunnable(), SERVICE_STOP_DELAY);
                        broadcastPlayStateChanged(false);
                    } catch (Exception e4) {
                        return;
                    }
                }
                if (this.playerStopTimer != null) {
                    try {
                        this.playerStopTimer.cancel();
                    } catch (Exception e5) {
                    }
                }
                this.playerStopTimer = null;
                if (this.serviceStopTimer != null) {
                    try {
                        this.serviceStopTimer.cancel();
                    } catch (Exception e6) {
                    }
                }
                this.serviceStopTimer = null;
                this.pausedBySystem = false;
                this.player.start();
                updateNotification();
                broadcastPlayStateChanged(true);
                if (!this.haveAudioFocus) {
                    registerRemoteControl();
                }
                if (this.playerCallback != null) {
                    this.playerCallback.setPlaying(this.player.isPlaying());
                }
                updateWidgets();
            }
        }
    }

    private void broadcastPlayStateChanged(boolean state) {
        int i = STATE_INITING;
        if (this.currentFile != null) {
            Log.m528i("vk", "Broadcast play state " + state);
            sendBroadcast(new Intent(ACTION_UPDATE_AUDIO_LISTS));
            if (this.remoteControlClient != null) {
                RemoteControlClient remoteControlClient;
                if (VERSION.SDK_INT >= 18) {
                    long ppos = 0;
                    try {
                        if (this.player != null) {
                            ppos = (long) this.player.getCurrentPosition();
                        }
                    } catch (Exception e) {
                    }
                    remoteControlClient = this.remoteControlClient;
                    if (!state) {
                        i = STATE_PAUSED;
                    }
                    remoteControlClient.setPlaybackState(i, ppos, 1.0f);
                } else {
                    remoteControlClient = this.remoteControlClient;
                    if (!state) {
                        i = STATE_PAUSED;
                    }
                    remoteControlClient.setPlaybackState(i);
                }
            }
            Iterator it = attachCallbacks.iterator();
            while (it.hasNext()) {
                ((AttachViewCallback) it.next()).onPlayStateChanged(this.currentFile.oid, this.currentFile.aid, state ? STATE_PLAYING : STATE_PAUSED);
            }
            if (state) {
                try {
                    if (this.wifiLock.isHeld()) {
                        this.cancelReleaseWifiLock = true;
                    } else {
                        this.wifiLock.acquire();
                    }
                    Log.m525d("vk", "acquired wifi lock");
                    return;
                } catch (Exception e2) {
                    return;
                }
            }
            new Thread(new C01925()).start();
        }
    }

    public AudioFile getCurrentFile() {
        return this.currentFile;
    }

    public boolean isPlaying() {
        if (this.error) {
            return false;
        }
        if (this.player != null) {
            try {
                if (this.initing || this.player.isPlaying()) {
                    return true;
                }
                return false;
            } catch (Exception e) {
                return this.initing;
            }
        } else if (!this.initing || pauseAfterInit) {
            return false;
        } else {
            return true;
        }
    }

    public void registerPlayerView(PlayerCallback a) {
        this.playerCallback = a;
        if (this.timer != null) {
            this.timer.cancel();
        }
        this.timer = new Timer();
        this.timer.schedule(new UpdateProgressTimerTask(), 0, 200);
        if (this.currentFile != null && a != null) {
            a.displayInfo(this.currentFile);
            a.setBuffered(this.nBuffered * ACTION_TOGGLE_SHUFFLE);
            a.setPlaying(isPlaying());
            if (this.currentPlaylist != null) {
                a.setNumber(this.playlistPosition + STATE_PLAYING, this.currentPlaylist.size());
            }
        }
    }

    public void unregisterPlayerView(PlayerCallback a) {
        if (this.playerCallback != null && this.playerCallback.equals(a)) {
            if (progressCallbacks.size() == 0) {
                this.timer.cancel();
                this.timer = null;
            }
            this.playerCallback = null;
        }
    }

    public void seek(int p) {
        if (this.player != null) {
            try {
                this.player.seekTo((int) ((((float) this.player.getDuration()) / 1000.0f) * ((float) p)));
            } catch (Exception e) {
            }
        }
    }

    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        this.nBuffered = percent;
        if (this.playerCallback != null) {
            this.playerCallback.setBuffered(percent * ACTION_TOGGLE_SHUFFLE);
        }
        if (this.currentFile != null) {
            Iterator it = progressCallbacks.iterator();
            while (it.hasNext()) {
                ((ProgressCallback) it.next()).onBufferProgressChanged(this.currentFile.oid, this.currentFile.aid, percent * ACTION_TOGGLE_SHUFFLE);
            }
        }
    }

    public void nextTrack() {
        if (this.currentPlaylist == null || this.currentPlaylist.size() <= STATE_PLAYING) {
            return;
        }
        if (this.statusUserID == 0) {
            this.playlistPosition += STATE_PLAYING;
            if (this.playlistPosition >= this.currentPlaylist.size()) {
                this.playlistPosition = 0;
            }
            playNewFile(this.random ? (AudioFile) this.randomPlaylist.get(this.playlistPosition) : (AudioFile) this.currentPlaylist.get(this.playlistPosition));
            return;
        }
        new APIRequest("status.get").param("user_id", this.statusUserID).handler(new C12706()).exec();
    }

    public void prevTrack() {
        if (this.currentPlaylist != null && this.currentPlaylist.size() > STATE_PLAYING) {
            this.playlistPosition--;
            if (this.playlistPosition < 0) {
                this.playlistPosition = this.currentPlaylist.size() - 1;
            }
            AudioFile audioFile = (this.random && this.statusUserID == 0) ? (AudioFile) this.randomPlaylist.get(this.playlistPosition) : (AudioFile) this.currentPlaylist.get(this.playlistPosition);
            playNewFile(audioFile);
        }
    }

    public void jumpToTrack(int pos) {
        if (this.currentPlaylist != null) {
            this.playlistPosition = pos;
            if (this.playlistPosition < 0) {
                this.playlistPosition = this.currentPlaylist.size() - 1;
            }
            AudioFile audioFile = (this.random && this.statusUserID == 0) ? (AudioFile) this.randomPlaylist.get(this.playlistPosition) : (AudioFile) this.currentPlaylist.get(this.playlistPosition);
            playNewFile(audioFile);
        }
    }

    public void onCompletion(MediaPlayer mp) {
        Iterator it = this.attachViews.iterator();
        while (it.hasNext()) {
            ((AudioAttachView) it.next()).setPlaying(false);
        }
        broadcastPlayStateChanged(false);
        if (this.currentPlaylist != null) {
            if (this.errorRetries > 0) {
                nextTrack();
            }
            if (this.errorRetries <= 0) {
                this.errorRetries = ACTION_NEXT_TRACK;
                return;
            }
            return;
        }
        stopForeground(true);
        if (this.playerCallback != null) {
            this.playerCallback.setPlaying(false);
            this.playerCallback.setPosition(0, 0);
            this.playerCallback.setBuffered(0);
        }
    }

    public void showPlayer(boolean animate, boolean fromNotification) {
        if (this.playerCallback == null) {
            Intent intent = new Intent(getApplicationContext(), AudioPlayerActivity.class);
            intent.addFlags(402653184);
            intent.putExtra("class", "AudioPlayerFragment");
            intent.putExtra("args", new Bundle());
            intent.putExtra("overlaybar", true);
            startActivity(intent);
        }
    }

    public boolean isViewShown() {
        return this.playerCallback != null;
    }

    public int getOid() {
        if (this.currentFile != null) {
            return this.currentFile.oid;
        }
        return -1;
    }

    public int getAid() {
        if (this.currentFile != null) {
            return this.currentFile.aid;
        }
        return -1;
    }

    public void addAttachView(AudioAttachView v) {
        this.attachViews.add(v);
        v.setPlaying(isPlaying());
    }

    public void removeAttachView(AudioAttachView v) {
        this.attachViews.remove(v);
    }

    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.m526e("vk", "MediaPlayer error: " + what + ", " + extra);
        this.error = true;
        this.initing = false;
        if (what != -38 && AudioCache.isCached(this.currentFile.oid, this.currentFile.aid)) {
            AudioCache.deleteCurrent();
            AudioCache.endPlayback(this.currentFile.oid, this.currentFile.aid);
            playNewFile(this.currentFile);
            return true;
        } else if (what == STATE_PLAYING && extra == -1004 && !this.currentFile.retried) {
            Log.m525d("vk", "404 not found");
            Toast.makeText(getApplicationContext(), C0436R.string.audio_play_error, 0).show();
            try {
                if (this.playerStopTimer != null) {
                    this.playerStopTimer.cancel();
                    this.playerStopTimer = null;
                }
                try {
                    this.player.stop();
                } catch (Exception e) {
                }
                this.player.release();
                this.player = null;
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
            broadcastPlayStateChanged(false);
            stopForeground(true);
            return true;
        } else if (what == -38) {
            return true;
        } else {
            Toast.makeText(getApplicationContext(), C0436R.string.audio_play_error, 0).show();
            this.errorRetries--;
            return false;
        }
    }

    public void registerRemoteControl() {
        AudioManager am = (AudioManager) getSystemService("audio");
        try {
            am.registerMediaButtonEventReceiver(new ComponentName(this, MediaButtonReceiver.class));
            Intent mediaButtonIntent = new Intent("android.intent.action.MEDIA_BUTTON");
            mediaButtonIntent.setComponent(new ComponentName(this, MediaButtonReceiver.class));
            PendingIntent mediaPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, mediaButtonIntent, 0);
            OnAudioFocusChangeListener listener = new C01937();
            if (!this.haveAudioFocus) {
                Log.m525d("vk", "Request audio focus. " + this.haveAudioFocus);
                am.requestAudioFocus(listener, STATE_INITING, STATE_PLAYING);
            }
            if (VERSION.SDK_INT >= 14) {
                this.remoteControlClient = new RemoteControlClient(mediaPendingIntent);
                am.registerRemoteControlClient(this.remoteControlClient);
                this.remoteControlClient.setPlaybackState(STATE_INITING);
                this.remoteControlClient.setTransportControlFlags(405);
                this.remoteControlClient.editMetadata(true).putString(STATE_PAUSED, this.currentFile.artist).putString(13, this.currentFile.artist).putString(ACTION_PAUSE_IF_PLAYING, this.currentFile.title).putBitmap(100, null).putLong(ACTION_TOGGLE_REPEAT, ((long) this.currentFile.duration) * 1000).apply();
                if (VERSION.SDK_INT >= 18) {
                    this.remoteControlClient.setOnGetPlaybackPositionListener(new C01948());
                    this.remoteControlClient.setPlaybackPositionUpdateListener(new C01959());
                }
            }
            getCoverForWidgets();
            Log.m525d("vk", "registered...");
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    private void updateRemoteControl(Bitmap cover) {
    }

    public void setVolume(float vol) {
        if (this.player != null) {
            try {
                this.player.setVolume(vol, vol);
            } catch (Exception e) {
            }
        }
    }

    public void unregisterRemoteControl() {
        AudioManager am = (AudioManager) getSystemService("audio");
        try {
            am.registerMediaButtonEventReceiver(new ComponentName(this, MediaButtonReceiver.class));
            am.abandonAudioFocus(null);
            this.haveAudioFocus = false;
            if (VERSION.SDK_INT >= 14) {
                am.unregisterRemoteControlClient(this.remoteControlClient);
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    public boolean removeCurrentFile() {
        if (this.currentFile.oid != Global.uid) {
            return false;
        }
        if (this.currentPlaylist == null) {
            return false;
        }
        if (new AudioFile[(this.currentPlaylist.size() - 1)].length == 0) {
            try {
                stopForeground(true);
                Iterator it = this.attachViews.iterator();
                while (it.hasNext()) {
                    ((AudioAttachView) it.next()).setPlaying(false);
                }
                this.player = null;
                this.currentFile = null;
                this.currentPlaylist = null;
                updateWidgets();
                this.player.release();
                return true;
            } catch (Exception e) {
                return true;
            }
        }
        this.currentPlaylist.remove(this.currentFile);
        if (this.playlistPosition >= this.currentPlaylist.size()) {
            this.playlistPosition = 0;
        }
        playNewFile((AudioFile) this.currentPlaylist.get(this.playlistPosition));
        return false;
    }

    public int getPlaylistPosition() {
        return this.playlistPosition;
    }

    public int getPlaylistLength() {
        return this.currentPlaylist != null ? this.currentPlaylist.size() : STATE_PLAYING;
    }

    public AudioFile getPlaylistItem(int index) {
        if (this.currentPlaylist != null && index >= 0 && index < this.currentPlaylist.size()) {
            return (this.random && this.statusUserID == 0) ? (AudioFile) this.randomPlaylist.get(index) : (AudioFile) this.currentPlaylist.get(index);
        } else {
            return null;
        }
    }

    public void getPlaylist(List<AudioFile> output) {
        if (this.random) {
            output.addAll(this.randomPlaylist);
        } else {
            output.addAll(this.currentPlaylist);
        }
    }

    public void swapPlaylistItems(int first, int second) {
        ArrayList<AudioFile> pl = this.random ? this.randomPlaylist : this.currentPlaylist;
        AudioFile tmp = (AudioFile) pl.get(first);
        pl.set(first, (AudioFile) pl.get(second));
        pl.set(second, tmp);
        if (first == this.playlistPosition) {
            this.playlistPosition = second;
            if (this.playerCallback != null) {
                this.playerCallback.setNumber(this.playlistPosition + STATE_PLAYING, this.currentPlaylist.size());
            }
        } else if (second == this.playlistPosition) {
            this.playlistPosition = first;
            if (this.playerCallback != null) {
                this.playerCallback.setNumber(this.playlistPosition + STATE_PLAYING, this.currentPlaylist.size());
            }
        }
    }

    public void removePlaylistItem(int pos) {
        if (this.random) {
            this.currentPlaylist.remove(this.randomPlaylist.remove(pos));
        } else {
            this.currentPlaylist.remove(pos);
        }
        if (this.playlistPosition > pos) {
            this.playlistPosition--;
        }
        if (this.playerCallback != null) {
            this.playerCallback.setNumber(this.playlistPosition + STATE_PLAYING, this.currentPlaylist.size());
        }
    }

    public boolean enqueue(AudioFile f) {
        Iterator it = this.currentPlaylist.iterator();
        while (it.hasNext()) {
            AudioFile af = (AudioFile) it.next();
            if (af.oid == f.oid && af.aid == f.aid) {
                return false;
            }
        }
        this.currentPlaylist.add(f);
        if (this.random) {
            this.randomPlaylist.add(f);
        }
        if (this.playerCallback != null) {
            this.playerCallback.setNumber(this.playlistPosition + STATE_PLAYING, this.currentPlaylist.size());
        }
        return true;
    }

    public void setVolume(float left, float right) {
        if (this.player != null) {
            try {
                this.player.setVolume(left, right);
            } catch (Exception e) {
            }
        }
    }

    public void onDestroy() {
        if (this.player != null) {
            if (isPlaying()) {
                this.player.stop();
            }
            this.player.release();
            this.player = null;
        }
        if (this.currentFile != null) {
            AudioCache.endPlayback(this.currentFile.oid, this.currentFile.aid);
        }
        this.currentFile = null;
        unregisterReceiver(this.receiver);
        updateWidgets();
        if (this.telManager != null) {
            this.telManager.listen(this.callStateReceiver, 0);
        }
        stopForeground(true);
        sharedInstance = null;
        if (cacheProxy != null) {
            cacheProxy.stop();
            cacheProxy = null;
        }
        sendBroadcast(new Intent(ACTION_SERVICE_STOPPING), permission.ACCESS_DATA);
        super.onDestroy();
    }

    public void setCurrentFileIDs(int oid, int aid) {
        Log.m525d("vk", "Set current file ids " + oid + "_" + aid);
        if (this.currentFile.oldAid == 0) {
            this.currentFile.oldAid = this.currentFile.aid;
            this.currentFile.oldOid = this.currentFile.oid;
            Log.m525d("vk", "Saved old ids " + this.currentFile.oldOid + "_" + this.currentFile.oldAid);
        }
        this.currentFile.oid = oid;
        this.currentFile.aid = aid;
    }

    public void resetCurrentFileIDs() {
        if (this.currentFile.oldAid != 0) {
            this.currentFile.aid = this.currentFile.oldAid;
            this.currentFile.oid = this.currentFile.oldOid;
            AudioFile audioFile = this.currentFile;
            this.currentFile.oldOid = 0;
            audioFile.oldAid = 0;
            Log.m525d("vk", "Reset file id to " + this.currentFile.oid + "_" + this.currentFile.aid);
        }
    }

    private void updateWidgets() {
        AppWidgetManager awm = AppWidgetManager.getInstance(this);
        int[] ids = awm.getAppWidgetIds(new ComponentName(this, PlayerWidget.class));
        if (ids != null && ids.length > 0) {
            PlayerWidget.update(this, awm);
        }
        ids = awm.getAppWidgetIds(new ComponentName(this, PlayerBigWidget.class));
        if (ids != null && ids.length > 0) {
            PlayerBigWidget.update(this, awm);
        }
    }

    public boolean isLoop() {
        return this.loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
        if (this.player != null) {
            try {
                this.player.setLooping(loop);
            } catch (Exception e) {
            }
        }
        updateWidgets();
    }

    public boolean isRandom() {
        return this.random;
    }

    public void setRandom(boolean random) {
        this.random = random;
        if (random && this.currentPlaylist != null) {
            this.randomPlaylist.clear();
            this.randomPlaylist.addAll(this.currentPlaylist);
            Collections.shuffle(this.randomPlaylist);
            this.randomPlaylist.remove(this.currentFile);
            this.randomPlaylist.add(this.playlistPosition, this.currentFile);
        }
        updateWidgets();
    }

    public void cacheCurrentFile() {
        AudioCache.saveCurrent(true);
    }

    private long getExternalFreeSpace() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
        return ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
    }

    public void setCurrentFile(AudioFile f) {
        this.currentFile = f;
        this.currentPlaylist = null;
    }

    public void setBroadcast(ArrayList<Integer> targets) {
        if (this.currentFile != null) {
            int id;
            ArrayList<Integer> prev = getBroadcastTargets();
            ArrayList<Integer> enable = new ArrayList();
            ArrayList<Integer> disable = new ArrayList();
            Iterator it = targets.iterator();
            while (it.hasNext()) {
                id = ((Integer) it.next()).intValue();
                if (!prev.contains(Integer.valueOf(id))) {
                    enable.add(Integer.valueOf(id));
                }
            }
            it = prev.iterator();
            while (it.hasNext()) {
                id = ((Integer) it.next()).intValue();
                if (!targets.contains(Integer.valueOf(id))) {
                    disable.add(Integer.valueOf(id));
                }
            }
            Log.m528i("vk", "Set broadcast: enable=" + enable + ", disable=" + disable);
            getSharedPreferences(null, 0).edit().putString("audio_broadcast", TextUtils.join(",", targets)).commit();
            if (enable.size() > 0) {
                new AudioSetBroadcast(this.currentFile, enable).exec();
            }
            if (disable.size() > 0) {
                new AudioSetBroadcast(null, disable).exec();
            }
        }
    }

    public void updateBroadcast() {
        ArrayList<Integer> t = getBroadcastTargets();
        if (t.size() != 0) {
            new AudioSetBroadcast(this.currentFile, t).exec();
        }
    }

    public ArrayList<Integer> getBroadcastTargets() {
        int i = 0;
        ArrayList<Integer> result = new ArrayList();
        String[] ss = getSharedPreferences(null, 0).getString("audio_broadcast", ACRAConstants.DEFAULT_STRING_VALUE).split(",");
        int length = ss.length;
        while (i < length) {
            try {
                result.add(Integer.valueOf(Integer.parseInt(ss[i])));
            } catch (Exception e) {
            }
            i += STATE_PLAYING;
        }
        return result;
    }

    public boolean isBroadcast() {
        return getBroadcastTargets().size() > 0;
    }

    public void fadeIn() {
        new Thread(new Runnable() {
            public void run() {
                int i = AudioPlayerService.STATE_PLAYING;
                while (i <= 100) {
                    float vol = ((float) i) / 100.0f;
                    try {
                        AudioPlayerService.this.player.setVolume(vol, vol);
                        try {
                            Thread.sleep(20);
                        } catch (Exception e) {
                        }
                        i += AudioPlayerService.STATE_PLAYING;
                    } catch (Exception e2) {
                        return;
                    }
                }
            }
        }).start();
    }

    public void fadeIn(float from) {
        new Thread(new AnonymousClass11(from)).start();
    }

    public void fadeOut(float to) {
        new Thread(new AnonymousClass12(to)).start();
    }

    private void updateNotification() {
        Runnable r = new Runnable() {

            /* renamed from: com.vkontakte.android.AudioPlayerService.13.1 */
            class C12681 implements ImageLoadCallback {
                C12681() {
                }

                public void onImageLoaded(Bitmap _bmp, int oid, int aid) {
                    if (oid == AudioPlayerService.this.currentFile.oid && aid == AudioPlayerService.this.currentFile.aid && AudioPlayerService.this.notification != null && AudioPlayerService.this.useCustomNotification) {
                        try {
                            Bitmap bmp = Bitmap.createScaledBitmap(_bmp, Global.scale(130.0f), Global.scale(130.0f), true);
                            AudioPlayerService.this.notification.contentView.setImageViewBitmap(C0436R.id.cover, bmp);
                            if (VERSION.SDK_INT >= 16) {
                                AudioPlayerService.this.notification.bigContentView.setImageViewBitmap(C0436R.id.cover, bmp);
                            }
                        } catch (Exception e) {
                        }
                        AudioPlayerService.this.startForeground(AudioPlayerService.ID_NOTIFICATION, AudioPlayerService.this.notification);
                    }
                }

                public void notAvailable(int oid, int aid) {
                }
            }

            public void run() {
                if (AudioPlayerService.this.useCustomNotification) {
                    Intent playpause = new Intent(AudioPlayerService.this, AudioPlayerService.class);
                    playpause.setAction("PlayPauseN");
                    playpause.putExtra("action", AudioPlayerService.STATE_INITING);
                    playpause.putExtra("from_notify", true);
                    PendingIntent pendingPlaypause = PendingIntent.getService(AudioPlayerService.this, 0, playpause, 0);
                    Intent next = new Intent(AudioPlayerService.this, AudioPlayerService.class);
                    next.setAction("NextN");
                    next.putExtra("action", AudioPlayerService.ACTION_NEXT_TRACK);
                    PendingIntent pendingNext = PendingIntent.getService(AudioPlayerService.this, 0, next, 0);
                    Intent prev = new Intent(AudioPlayerService.this, AudioPlayerService.class);
                    prev.setAction("PrevN");
                    prev.putExtra("action", AudioPlayerService.ACTION_PREV_TRACK);
                    PendingIntent pendingPrev = PendingIntent.getService(AudioPlayerService.this, 0, prev, 0);
                    RemoteViews views = new RemoteViews(AudioPlayerService.this.getPackageName(), C0436R.layout.audio_notification);
                    views.setTextViewText(C0436R.id.title, AudioPlayerService.this.currentFile.title);
                    views.setTextViewText(C0436R.id.content, AudioPlayerService.this.currentFile.artist);
                    views.setImageViewResource(C0436R.id.cover, C0436R.drawable.aplayer_cover_placeholder);
                    views.setImageViewResource(C0436R.id.playpause, AudioPlayerService.this.isPlaying() ? C0436R.drawable.ic_audio_panel_pause : C0436R.drawable.ic_audio_panel_play);
                    views.setOnClickPendingIntent(C0436R.id.playpause, pendingPlaypause);
                    views.setOnClickPendingIntent(C0436R.id.next, pendingNext);
                    int bgRes = Resources.getSystem().getIdentifier("notification_bg", "drawable", "android");
                    int imgBgRes = Resources.getSystem().getIdentifier("notification_template_icon_bg", "drawable", "android");
                    if (bgRes != 0) {
                        views.setInt(C0436R.id.notification_root, "setBackgroundResource", bgRes);
                    }
                    AudioPlayerService.this.notification.contentView = views;
                    if (VERSION.SDK_INT >= 16) {
                        RemoteViews xviews = new RemoteViews(AudioPlayerService.this.getPackageName(), C0436R.layout.audio_notification_expanded);
                        xviews.setTextViewText(C0436R.id.title, AudioPlayerService.this.currentFile.title);
                        xviews.setTextViewText(C0436R.id.content, AudioPlayerService.this.currentFile.artist);
                        xviews.setImageViewResource(C0436R.id.cover, C0436R.drawable.aplayer_cover_placeholder);
                        xviews.setImageViewResource(C0436R.id.playpause, AudioPlayerService.this.isPlaying() ? C0436R.drawable.ic_audio_panel_pause : C0436R.drawable.ic_audio_panel_play);
                        xviews.setOnClickPendingIntent(C0436R.id.playpause, pendingPlaypause);
                        xviews.setOnClickPendingIntent(C0436R.id.next, pendingNext);
                        xviews.setOnClickPendingIntent(C0436R.id.prev, pendingPrev);
                        if (bgRes != 0) {
                            xviews.setInt(C0436R.id.notification_root, "setBackgroundResource", bgRes);
                        }
                        AudioPlayerService.this.notification.bigContentView = xviews;
                    }
                    AlbumArtRetriever.getCoverImage(AudioPlayerService.this.currentFile.aid, AudioPlayerService.this.currentFile.oid, 0, new C12681());
                }
                AudioPlayerService.this.startForeground(AudioPlayerService.ID_NOTIFICATION, AudioPlayerService.this.notification);
            }
        };
        if (Looper.myLooper() == Looper.getMainLooper()) {
            new Thread(r).start();
        } else {
            r.run();
        }
    }

    private void getCoverForWidgets() {
        Log.m525d("vk", "Before get cover image");
        AlbumArtRetriever.getCoverImage(this.currentFile.aid, this.currentFile.oid, 0, new ImageLoadCallback() {
            public void onImageLoaded(Bitmap bmp, int oid, int aid) {
                Log.m525d("vk", "Image loaded");
                if (oid == AudioPlayerService.this.currentFile.oid && aid == AudioPlayerService.this.currentFile.aid) {
                    AudioPlayerService.this.updateWidgets();
                    if (AudioPlayerService.this.remoteControlClient != null) {
                        Log.m525d("vk", "Before apply");
                        AudioPlayerService.this.remoteControlClient.editMetadata(false).putBitmap(100, bmp).apply();
                        Log.m525d("vk", "Apply");
                    }
                }
            }

            public void notAvailable(int oid, int aid) {
                Log.m530w("vk", "Image failed!");
                if (AudioPlayerService.this.remoteControlClient != null && oid == AudioPlayerService.this.currentFile.oid && aid == AudioPlayerService.this.currentFile.aid) {
                    AudioPlayerService.this.remoteControlClient.editMetadata(false).putBitmap(100, null).apply();
                }
            }
        });
    }
}
