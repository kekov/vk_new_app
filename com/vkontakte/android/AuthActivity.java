package com.vkontakte.android;

import android.accounts.AccountAuthenticatorActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Auth.AuthResultReceiver;
import com.vkontakte.android.VKAlertDialog.Builder;
import java.util.HashMap;
import org.acra.ACRAConstants;

public class AuthActivity extends AccountAuthenticatorActivity implements AuthResultReceiver {
    private static final int AUTH_RESULT = 102;
    private static final int REGISTER_RESULT = 100;
    private static final int VALIDATION_RESULT = 101;
    public static boolean active;
    boolean authDone;
    private String photo;
    ProgressDialog progress;
    WebView webview;

    /* renamed from: com.vkontakte.android.AuthActivity.1 */
    class C01981 implements OnClickListener {
        C01981() {
        }

        public void onClick(DialogInterface dialog, int which) {
            AuthActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.AuthActivity.2 */
    class C01992 implements OnCancelListener {
        C01992() {
        }

        public void onCancel(DialogInterface dialog) {
            AuthActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.AuthActivity.3 */
    class C02003 implements View.OnClickListener {
        C02003() {
        }

        public void onClick(View v) {
            AuthActivity.this.startActivityForResult(new Intent(AuthActivity.this, LoginActivity.class), AuthActivity.AUTH_RESULT);
        }
    }

    /* renamed from: com.vkontakte.android.AuthActivity.4 */
    class C02014 implements View.OnClickListener {
        C02014() {
        }

        public void onClick(View v) {
            AuthActivity.this.startActivityForResult(new Intent(AuthActivity.this, SignupActivity.class), AuthActivity.REGISTER_RESULT);
        }
    }

    /* renamed from: com.vkontakte.android.AuthActivity.5 */
    class C02025 implements Runnable {
        C02025() {
        }

        public void run() {
            Log.m528i("vk", "Will upload photo " + AuthActivity.this.photo);
            try {
                ((InputMethodManager) AuthActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(AuthActivity.this.getCurrentFocus().getWindowToken(), 0);
            } catch (Exception e) {
            }
            AuthActivity.this.setResult(-1);
            if (AuthActivity.this.getIntent().hasExtra("accountAuthenticatorResponse")) {
                try {
                    Parcelable response = AuthActivity.this.getIntent().getParcelableExtra("accountAuthenticatorResponse");
                    Bundle res = new Bundle();
                    res.putString("authAccount", VKApplication.context.getSharedPreferences(null, 0).getString("username", ACRAConstants.DEFAULT_STRING_VALUE));
                    res.putString("accountType", Auth.ACCOUNT_TYPE);
                    AuthActivity.this.setAccountAuthenticatorResult(res);
                } catch (Exception e2) {
                }
            }
            if (AuthActivity.this.photo != null) {
                Intent intent = new Intent(AuthActivity.this, UploaderService.class);
                intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 6);
                intent.putExtra("file", AuthActivity.this.photo);
                AuthActivity.this.startService(intent);
            }
            AuthActivity.this.photo = null;
            AuthActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.AuthActivity.6 */
    class C02036 implements Runnable {
        private final /* synthetic */ int val$result;

        C02036(int i) {
            this.val$result = i;
        }

        public void run() {
            AuthActivity.this.progress.dismiss();
            if (this.val$result != Auth.REAUTH_CANCELED) {
                if (this.val$result == Auth.REAUTH_OPEN_BROWSER) {
                    AuthActivity.this.openBrowser(Auth.lastError);
                    return;
                }
                String msg = AuthActivity.this.getResources().getString(this.val$result == Auth.REAUTH_ERROR_INCORRECT_PASSWORD ? C0436R.string.auth_error : C0436R.string.auth_error_network);
                if (this.val$result == Auth.REAUTH_ERROR_INCORRECT_PASSWORD) {
                    msg = new StringBuilder(String.valueOf(msg)).append(" (").append(AuthActivity.this.getResources().getString(C0436R.string.error)).append(": ").append(Auth.lastError).append(")").toString();
                }
                new Builder(AuthActivity.this).setMessage(msg).setTitle(C0436R.string.auth_error_title).setIcon(VERSION.SDK_INT >= 11 ? 0 : ACRAConstants.DEFAULT_DIALOG_ICON).setPositiveButton(C0436R.string.ok, null).show();
            }
            AuthActivity.this.photo = null;
        }
    }

    /* renamed from: com.vkontakte.android.AuthActivity.7 */
    class C02047 implements Runnable {
        private final /* synthetic */ Intent val$intent;

        C02047(Intent intent) {
            this.val$intent = intent;
        }

        public void run() {
            Auth.setData(this.val$intent.getStringExtra(WebDialog.DIALOG_PARAM_ACCESS_TOKEN), this.val$intent.getStringExtra("secret"), this.val$intent.getIntExtra("user_id", 0), true);
            AuthActivity.this.authDone(Auth.REAUTH_SUCCESS, null);
        }
    }

    public AuthActivity() {
        this.authDone = false;
    }

    static {
        active = false;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        SharedPreferences prefs = getSharedPreferences(null, 0);
        if (!prefs.contains("uid") || prefs.getInt("uid", 0) <= 0) {
            requestWindowFeature(1);
            setContentView(C0436R.layout.auth);
            findViewById(C0436R.id.auth_login_btn).setOnClickListener(new C02003());
            findViewById(C0436R.id.auth_signup_btn).setOnClickListener(new C02014());
            active = true;
            return;
        }
        if (VERSION.SDK_INT >= 11) {
            setTheme(16973931);
        }
        new Builder(this).setTitle(C0436R.string.error).setMessage(C0436R.string.already_logged_in).setIcon(ACRAConstants.DEFAULT_DIALOG_ICON).setPositiveButton(C0436R.string.ok, new C01981()).setOnCancelListener(new C01992()).show();
    }

    public void onDestroy() {
        super.onDestroy();
        active = false;
    }

    public boolean onKeyDown(int code, KeyEvent evt) {
        if (code != 4) {
            return super.onKeyDown(code, evt);
        }
        setResult(0);
        finish();
        return true;
    }

    public void authDone(int result, HashMap<String, String> hashMap) {
        if (result == Auth.REAUTH_SUCCESS) {
            runOnUiThread(new C02025());
        } else {
            runOnUiThread(new C02036(result));
        }
    }

    private void openBrowser(String url, String phone) {
        Intent intent = new Intent(this, ValidationActivity.class);
        intent.putExtra(PlusShare.KEY_CALL_TO_ACTION_URL, url);
        intent.putExtra("return_result", true);
        intent.putExtra("phone", phone);
        startActivityForResult(intent, VALIDATION_RESULT);
    }

    private void openBrowser(String url) {
        Intent intent = new Intent(this, ValidationActivity.class);
        intent.putExtra(PlusShare.KEY_CALL_TO_ACTION_URL, url);
        intent.putExtra("return_result", true);
        startActivityForResult(intent, VALIDATION_RESULT);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REGISTER_RESULT && resultCode == -1) {
            authDone(Auth.REAUTH_SUCCESS, null);
        }
        if (requestCode == REGISTER_RESULT && resultCode == 1) {
            openBrowser("https://oauth.vk.com/restore?scope=nohttps,all&client_id=2274003&client_secret=hHbZxrka2uZ6jB1inYsH", intent.getStringExtra("number"));
        }
        if (requestCode == AUTH_RESULT && resultCode == -1) {
            authDone(Auth.REAUTH_SUCCESS, null);
        }
        if (requestCode == VALIDATION_RESULT && resultCode == -1) {
            this.progress.show();
            new Thread(new C02047(intent)).start();
        }
    }
}
