package com.vkontakte.android.fragments;

import android.widget.ListAdapter;
import com.vkontakte.android.data.database.CitiesAutocompleteAdapter;
import com.vkontakte.android.data.database.City;
import com.vkontakte.android.fragments.DatabaseSearchFragment.Callback;

public class CitySelectFragment extends DatabaseSearchFragment<City> {

    public interface CityCallback extends Callback<City> {
    }

    public ListAdapter getAdapter() {
        CitiesAutocompleteAdapter adapter = new CitiesAutocompleteAdapter();
        adapter.setCountry(getArguments().getInt("country"));
        adapter.setShowNone(getArguments().getBoolean("show_none"));
        return adapter;
    }
}
