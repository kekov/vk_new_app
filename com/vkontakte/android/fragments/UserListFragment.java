package com.vkontakte.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.UserListView;
import org.acra.ACRAConstants;

public class UserListFragment extends SherlockFragment {
    private UserListView view;

    public void onAttach(Activity act) {
        super.onAttach(act);
        this.view = new UserListView(act, getArguments().getInt(WebDialog.DIALOG_PARAM_TYPE), getArguments());
        SherlockFragmentActivity sa = getSherlockActivity();
        sa.getSupportActionBar().setDisplayShowTitleEnabled(true);
        sa.getSupportActionBar().setNavigationMode(0);
        String title = ACRAConstants.DEFAULT_STRING_VALUE;
        if (getArguments().containsKey(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)) {
            title = getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE).toString();
        }
        sa.setTitle(title);
        this.view.loadData();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.view;
    }

    public void onPause() {
        super.onPause();
        this.view.onPause();
    }

    public void onResume() {
        super.onResume();
        this.view.onResume();
    }
}
