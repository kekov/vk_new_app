package com.vkontakte.android.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.media.TransportMediator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.ActivityUtils;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageCache;
import com.vkontakte.android.ImagePickerActivity;
import com.vkontakte.android.Log;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.Photo;
import com.vkontakte.android.Photo.Image;
import com.vkontakte.android.SendSinglePhotoActivity;
import com.vkontakte.android.StackBlur;
import com.vkontakte.android.UploaderService;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ViewUtils;
import com.vkontakte.android.api.FaveGetPhotos;
import com.vkontakte.android.api.PhotoAlbum;
import com.vkontakte.android.api.PhotosGet;
import com.vkontakte.android.api.PhotosGet.Callback;
import com.vkontakte.android.api.PhotosGetUserPhotos;
import com.vkontakte.android.mediapicker.gl.GLFilterContext;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.AnimationDuration;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.ui.ActionBarHacks;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.Fonts;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.ExtendedListener;
import com.vkontakte.android.ui.LoadMoreFooterView;
import com.vkontakte.android.ui.PhotoView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class PhotoListFragment extends SherlockFragment implements OnClickListener {
    private static final int UPLOAD_RESULT = 3890;
    private Drawable actionBarBg;
    private PhotosAdapter adapter;
    private PhotoAlbum album;
    private FrameLayout contentView;
    private FrameLayout contentWrap;
    private APIRequest currentReq;
    private boolean dataLoading;
    private TextView emptyView;
    private ErrorView error;
    private LoadMoreFooterView footerView;
    private View headerView;
    private ListImageLoaderWrapper imgLoader;
    private ArrayList<ArrayList<PhotoLayout>> layout;
    private ListView list;
    private boolean moreAvailable;
    private ArrayList<Photo> photos;
    private ProgressBar progress;
    private BroadcastReceiver receiver;
    private ArrayList<ImageView> reusableImageViews;
    private boolean useFadingActionBar;

    /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.1 */
    class C07401 extends BroadcastReceiver {
        C07401() {
        }

        public void onReceive(Context arg0, Intent intent) {
            if (UploaderService.ACTION_PHOTO_ADDED.equals(intent.getAction()) && intent.getIntExtra("aid", 0) == PhotoListFragment.this.album.id) {
                PhotoListFragment.this.photos.add((Photo) intent.getParcelableExtra("photo"));
                PhotoListFragment.this.layoutPhotos();
                PhotoAlbum access$0 = PhotoListFragment.this.album;
                access$0.numPhotos++;
                PhotoListFragment.this.updateCount();
            }
            if (UploaderService.ACTION_PHOTO_REMOVED.equals(intent.getAction()) && intent.getIntExtra("aid", 0) == PhotoListFragment.this.album.id) {
                int pid = intent.getIntExtra("pid", 0);
                Iterator it = PhotoListFragment.this.photos.iterator();
                while (it.hasNext()) {
                    Photo photo = (Photo) it.next();
                    if (photo.id == pid) {
                        PhotoListFragment.this.photos.remove(photo);
                        access$0 = PhotoListFragment.this.album;
                        access$0.numPhotos--;
                        PhotoListFragment.this.updateCount();
                        break;
                    }
                }
                PhotoListFragment.this.layoutPhotos();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.3 */
    class C07413 implements OnClickListener {
        C07413() {
        }

        public void onClick(View v) {
            PhotoListFragment.this.error.setVisibility(8);
            PhotoListFragment.this.progress.setVisibility(0);
            PhotoListFragment.this.loadData();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.4 */
    class C07434 implements Runnable {

        /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.4.1 */
        class C07421 implements Runnable {
            private final /* synthetic */ Bitmap val$_bmp;

            C07421(Bitmap bitmap) {
                this.val$_bmp = bitmap;
            }

            public void run() {
                ((ImageView) PhotoListFragment.this.headerView.findViewById(C0436R.id.album_thumb)).setImageBitmap(this.val$_bmp);
            }
        }

        C07434() {
        }

        public void run() {
            try {
                Bitmap bmp = ImageCache.get(PhotoListFragment.this.album.thumbURL);
                if (bmp.getWidth() < 200 || bmp.getHeight() < 130) {
                    int th;
                    int tw;
                    if (bmp.getWidth() > bmp.getHeight()) {
                        th = TransportMediator.KEYCODE_MEDIA_RECORD;
                        tw = Math.round((((float) bmp.getWidth()) / ((float) bmp.getHeight())) * 130.0f);
                    } else {
                        tw = 200;
                        th = Math.round((((float) bmp.getHeight()) / ((float) bmp.getWidth())) * 200.0f);
                    }
                    bmp = Bitmap.createScaledBitmap(bmp, tw, th, false);
                }
                if (bmp != null) {
                    float scale;
                    int cropX;
                    int cropY;
                    int cropW;
                    int cropH;
                    if (((float) bmp.getWidth()) / ((float) bmp.getHeight()) >= 1.5384616f) {
                        scale = 130.0f / ((float) bmp.getHeight());
                        cropX = Math.round((((float) (bmp.getWidth() / 2)) * scale) - 100.0f);
                        cropY = 0;
                        cropW = Math.round(200.0f / scale);
                        cropH = bmp.getHeight();
                    } else {
                        scale = 200.0f / ((float) bmp.getWidth());
                        cropX = 0;
                        cropY = Math.round((((float) (bmp.getHeight() / 2)) * scale) - 65.0f);
                        cropW = bmp.getWidth();
                        cropH = Math.round(130.0f / scale);
                    }
                    Log.m528i("vk", "cropping: " + bmp.getWidth() + "x" + bmp.getHeight() + " -> " + cropX + "," + cropY + ", " + cropW + "x" + cropH + ", s=" + scale);
                    Matrix m = new Matrix();
                    m.postScale(scale, scale);
                    bmp = Bitmap.createBitmap(bmp, cropX, cropY, cropW, cropH, m, false);
                    StackBlur.blurBitmap(bmp, 5);
                    Bitmap _bmp = bmp;
                    if (PhotoListFragment.this.getActivity() != null) {
                        PhotoListFragment.this.getActivity().runOnUiThread(new C07421(_bmp));
                    }
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.5 */
    class C07445 implements Runnable {
        C07445() {
        }

        public void run() {
            PhotoListFragment.this.layoutPhotos();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.9 */
    class C07489 implements Runnable {
        C07489() {
        }

        public void run() {
            PhotoListFragment.this.imgLoader.updateImages();
        }
    }

    private class PhotoLayout {
        public int height;
        public int index;
        public Photo photo;
        public int width;

        private PhotoLayout() {
        }
    }

    private class PhotosAdapter extends BaseAdapter {
        private PhotosAdapter() {
        }

        public int getCount() {
            return PhotoListFragment.this.layout.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public boolean isEnabled(int item) {
            return false;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LinearLayout l;
            if (convertView != null) {
                l = (LinearLayout) convertView;
                for (int i = 0; i < l.getChildCount(); i++) {
                    PhotoListFragment.this.reusableImageViews.add((ImageView) l.getChildAt(i));
                }
                l.removeAllViews();
            } else {
                l = new LinearLayout(PhotoListFragment.this.getActivity());
                l.setOrientation(0);
            }
            ArrayList<PhotoLayout> pl = (ArrayList) PhotoListFragment.this.layout.get(position);
            int image = 0;
            int wsum = 0;
            Iterator it = pl.iterator();
            while (it.hasNext()) {
                ImageView iv;
                PhotoLayout p = (PhotoLayout) it.next();
                if (PhotoListFragment.this.reusableImageViews.size() > 0) {
                    iv = (ImageView) PhotoListFragment.this.reusableImageViews.remove(0);
                } else {
                    iv = new ImageView(PhotoListFragment.this.getActivity());
                    iv.setBackgroundColor(-14145496);
                    iv.setScaleType(ScaleType.CENTER_CROP);
                    iv.setOnClickListener(PhotoListFragment.this);
                }
                int i2 = (image != pl.size() + -1 || Math.abs((p.width + wsum) - PhotoListFragment.this.list.getWidth()) >= Global.scale(10.0f)) ? p.width : -1;
                LayoutParams lp = new LayoutParams(i2, p.height);
                wsum += p.width;
                if (image != 0) {
                    lp.leftMargin = Global.scale(ImageViewHolder.PaddingSize);
                }
                l.addView(iv, lp);
                String url = ((PhotoLayout) ((ArrayList) PhotoListFragment.this.layout.get(position)).get(image)).photo.getImage(Global.displayDensity > 1.5f ? 'p' : 'm', 'm').url;
                if (PhotoListFragment.this.imgLoader.isAlreadyLoaded(url)) {
                    iv.setImageBitmap(PhotoListFragment.this.imgLoader.get(url));
                } else {
                    iv.setImageBitmap(null);
                }
                iv.setTag(Integer.valueOf(p.index));
                image++;
            }
            return l;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.6 */
    class C14986 implements Callback {

        /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.6.1 */
        class C07451 implements Runnable {
            C07451() {
            }

            public void run() {
                PhotoListFragment.this.contentWrap.setVisibility(0);
                PhotoListFragment.this.animate();
            }
        }

        C14986() {
        }

        public void success(int total, Vector<Photo> photos) {
            boolean z;
            PhotoListFragment.this.currentReq = null;
            PhotoListFragment.this.photos.addAll(photos);
            PhotoListFragment.this.layoutPhotos();
            if (PhotoListFragment.this.progress.getVisibility() != 8) {
                Global.showViewAnimated(PhotoListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                PhotoListFragment.this.contentWrap.post(new C07451());
            }
            PhotoListFragment photoListFragment = PhotoListFragment.this;
            if (PhotoListFragment.this.photos.size() < total) {
                z = true;
            } else {
                z = false;
            }
            photoListFragment.moreAvailable = z;
            PhotoListFragment.this.dataLoading = false;
            if (PhotoListFragment.this.footerView != null) {
                PhotoListFragment.this.footerView.setVisible(PhotoListFragment.this.moreAvailable);
            }
        }

        public void fail(int ecode, String emsg) {
            PhotoListFragment.this.dataLoading = false;
            PhotoListFragment.this.currentReq = null;
            if (PhotoListFragment.this.photos.size() == 0) {
                PhotoListFragment.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(PhotoListFragment.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(PhotoListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(PhotoListFragment.this.getActivity(), C0436R.string.err_text, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.7 */
    class C14997 implements PhotosGetUserPhotos.Callback {

        /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.7.1 */
        class C07461 implements Runnable {
            C07461() {
            }

            public void run() {
                PhotoListFragment.this.contentWrap.setVisibility(0);
                PhotoListFragment.this.animate();
            }
        }

        C14997() {
        }

        public void success(int total, Vector<Photo> photos) {
            boolean needAnim;
            boolean z = true;
            PhotoListFragment.this.currentReq = null;
            if (PhotoListFragment.this.photos.size() == 0) {
                needAnim = true;
            } else {
                needAnim = false;
            }
            PhotoListFragment.this.photos.addAll(photos);
            PhotoListFragment.this.layoutPhotos();
            if (needAnim) {
                Global.showViewAnimated(PhotoListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                PhotoListFragment.this.contentWrap.post(new C07461());
            }
            PhotoListFragment photoListFragment = PhotoListFragment.this;
            if (PhotoListFragment.this.photos.size() >= total) {
                z = false;
            }
            photoListFragment.moreAvailable = z;
            PhotoListFragment.this.dataLoading = false;
            PhotoListFragment.this.footerView.setVisible(PhotoListFragment.this.moreAvailable);
        }

        public void fail(int ecode, String emsg) {
            PhotoListFragment.this.dataLoading = false;
            PhotoListFragment.this.currentReq = null;
            if (PhotoListFragment.this.photos.size() == 0) {
                PhotoListFragment.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(PhotoListFragment.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(PhotoListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(PhotoListFragment.this.getActivity(), C0436R.string.err_text, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.8 */
    class C15008 implements FaveGetPhotos.Callback {

        /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.8.1 */
        class C07471 implements Runnable {
            C07471() {
            }

            public void run() {
                PhotoListFragment.this.contentWrap.setVisibility(0);
                PhotoListFragment.this.animate();
            }
        }

        C15008() {
        }

        public void success(int total, Vector<Photo> photos) {
            boolean needAnim;
            boolean z = true;
            PhotoListFragment.this.currentReq = null;
            PhotoListFragment.this.album.numPhotos = total;
            if (PhotoListFragment.this.photos.size() == 0) {
                needAnim = true;
            } else {
                needAnim = false;
            }
            PhotoListFragment.this.photos.addAll(photos);
            PhotoListFragment.this.layoutPhotos();
            if (needAnim) {
                Global.showViewAnimated(PhotoListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                PhotoListFragment.this.contentWrap.post(new C07471());
            }
            PhotoListFragment photoListFragment = PhotoListFragment.this;
            if (PhotoListFragment.this.photos.size() >= total) {
                z = false;
            }
            photoListFragment.moreAvailable = z;
            PhotoListFragment.this.dataLoading = false;
            PhotoListFragment.this.footerView.setVisible(PhotoListFragment.this.moreAvailable);
        }

        public void fail(int ecode, String emsg) {
            PhotoListFragment.this.dataLoading = false;
            PhotoListFragment.this.currentReq = null;
            if (PhotoListFragment.this.photos.size() == 0) {
                PhotoListFragment.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(PhotoListFragment.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(PhotoListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(PhotoListFragment.this.getActivity(), C0436R.string.err_text, 0).show();
        }
    }

    private class PhotoImagesAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.PhotoImagesAdapter.1 */
        class C07491 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$image;
            private final /* synthetic */ View val$v;

            C07491(View view, int i, Bitmap bitmap) {
                this.val$v = view;
                this.val$image = i;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = ((ViewGroup) this.val$v).getChildAt(this.val$image);
                    if (vv != null && (vv instanceof ImageView)) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private PhotoImagesAdapter() {
        }

        public int getItemCount() {
            return PhotoListFragment.this.layout.size();
        }

        public int getImageCountForItem(int item) {
            return ((ArrayList) PhotoListFragment.this.layout.get(item)).size();
        }

        public String getImageURL(int item, int image) {
            return ((PhotoLayout) ((ArrayList) PhotoListFragment.this.layout.get(item)).get(image)).photo.getImage(Global.displayDensity > 1.5f ? 'p' : 'm', 'm').url;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += PhotoListFragment.this.list.getHeaderViewsCount();
            if (item >= PhotoListFragment.this.list.getFirstVisiblePosition() && item <= PhotoListFragment.this.list.getLastVisiblePosition()) {
                PhotoListFragment.this.getActivity().runOnUiThread(new C07491(PhotoListFragment.this.list.getChildAt(item - PhotoListFragment.this.list.getFirstVisiblePosition()), image, bitmap));
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoListFragment.2 */
    class C16212 implements ExtendedListener {
        C16212() {
        }

        public void onScrolledToLastItem() {
            if (PhotoListFragment.this.moreAvailable && !PhotoListFragment.this.dataLoading) {
                PhotoListFragment.this.loadData();
            }
        }

        public void onScrollStopped() {
        }

        public void onScrollStarted() {
        }

        public void onScroll(int firstItem, int visibleCount, int total) {
            if (VERSION.SDK_INT >= 14) {
                PhotoListFragment.this.headerView.findViewById(C0436R.id.album_thumb).setTranslationY((float) ((-PhotoListFragment.this.headerView.getTop()) / 2));
                if (PhotoListFragment.this.useFadingActionBar) {
                    PhotoListFragment.this.actionBarBg.setAlpha(Math.round(Math.min(((float) (-PhotoListFragment.this.headerView.getTop())) / ((float) (PhotoListFragment.this.headerView.getHeight() / 2)), 1.0f) * 255.0f));
                }
            }
        }
    }

    public PhotoListFragment() {
        this.photos = new ArrayList();
        this.layout = new ArrayList();
        this.reusableImageViews = new ArrayList();
        boolean z = VERSION.SDK_INT >= 14 && !Build.BRAND.toLowerCase().contains("zte");
        this.useFadingActionBar = z;
        this.receiver = new C07401();
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        this.actionBarBg = act.getResources().getDrawable(C0436R.drawable.bg_actionbar_black).mutate();
        if (!getArguments().getBoolean("nohead")) {
            if (this.useFadingActionBar) {
                this.actionBarBg.setAlpha(0);
                View overlay = ActionBarHacks.getActionBarOverlay(getActivity());
                if (overlay != null) {
                    ((ViewGroup) overlay).setWillNotDraw(true);
                }
            }
            getSherlockActivity().getSupportActionBar().setBackgroundDrawable(this.actionBarBg);
        }
        this.album = (PhotoAlbum) getArguments().getParcelable("album");
        this.headerView = View.inflate(act, C0436R.layout.photo_list_header, null);
        this.headerView.setLayoutParams(new AbsListView.LayoutParams(-1, Global.scale(230.0f)));
        this.contentView = new FrameLayout(act);
        this.contentWrap = new FrameLayout(act);
        this.list = new ListView(act);
        this.contentView.setBackgroundColor(-15198184);
        this.list.setBackgroundColor(-15198184);
        this.list.setCacheColorHint(-15198184);
        this.list.setDivider(new ColorDrawable(0));
        this.list.setDividerHeight(Global.scale(ImageViewHolder.PaddingSize));
        this.list.setScrollBarStyle(33554432);
        if (this.album.id != -9001) {
            this.list.addHeaderView(this.headerView, null, false);
        } else {
            this.emptyView = new TextView(act);
            this.emptyView.setTextAppearance(act, C0436R.style.EmptyTextView);
            this.emptyView.setGravity(17);
            this.emptyView.setText(C0436R.string.no_photos);
            this.emptyView.setTextColor(-2130706433);
            this.list.setEmptyView(this.emptyView);
        }
        if (this.album.id <= -9000 || this.album.numPhotos > 200) {
            this.footerView = new LoadMoreFooterView(act);
            this.list.addFooterView(this.footerView, null, false);
            this.moreAvailable = true;
            this.footerView.getProgressBar().setIndeterminateDrawable(getResources().getDrawable(C0436R.drawable.progress_light));
        }
        ActivityUtils.setBeamLink(getActivity(), "album" + this.album.oid + "_" + this.album.id);
        ListView listView = this.list;
        ListAdapter photosAdapter = new PhotosAdapter();
        this.adapter = photosAdapter;
        listView.setAdapter(photosAdapter);
        this.contentWrap.addView(this.list);
        if (this.emptyView != null) {
            this.contentWrap.addView(this.emptyView);
        }
        this.contentView.addView(this.contentWrap);
        this.progress = new ProgressBar(act);
        this.progress.setIndeterminateDrawable(getResources().getDrawable(C0436R.drawable.progress_light));
        this.contentView.addView(this.progress, new FrameLayout.LayoutParams(Global.scale(45.0f), Global.scale(45.0f), 17));
        this.contentWrap.setVisibility(4);
        if (!getArguments().getBoolean("nohead")) {
            act.setTitle(this.album.title);
        }
        this.imgLoader = new ListImageLoaderWrapper(new PhotoImagesAdapter(), this.list, new C16212());
        ((TextView) this.headerView.findViewById(C0436R.id.album_title)).setTypeface(Fonts.getRobotoLight());
        ((TextView) this.headerView.findViewById(C0436R.id.album_qty)).setTypeface(Fonts.getRobotoLight());
        ((TextView) this.headerView.findViewById(C0436R.id.album_desc)).setTypeface(Fonts.getRobotoLight());
        ((TextView) this.headerView.findViewById(C0436R.id.album_title)).setText(this.album.title);
        updateCount();
        ((TextView) this.headerView.findViewById(C0436R.id.album_desc)).setText(this.album.descr);
        if (this.album.descr == null || this.album.descr.length() == 0) {
            this.headerView.findViewById(C0436R.id.album_desc).setVisibility(8);
        }
        this.error = (ErrorView) View.inflate(getActivity(), C0436R.layout.error, null);
        this.error.setOnRetryListener(new C07413());
        this.error.setVisibility(8);
        this.contentView.addView(this.error);
        new Thread(new C07434()).start();
        loadData();
        if (this.album.id > 0) {
            setHasOptionsMenu(true);
        }
        int i = VERSION.SDK_INT;
    }

    private void updateCount() {
        if (this.album.numPhotos > 0) {
            ((TextView) this.headerView.findViewById(C0436R.id.album_qty)).setText(Global.langPlural(C0436R.array.album_numphotos, this.album.numPhotos, getResources()));
        } else {
            ((TextView) this.headerView.findViewById(C0436R.id.album_qty)).setText(C0436R.string.no_photos);
        }
    }

    public void onCreate(Bundle s) {
        super.onCreate(s);
        IntentFilter filter = new IntentFilter();
        filter.addAction(UploaderService.ACTION_PHOTO_ADDED);
        filter.addAction(UploaderService.ACTION_PHOTO_REMOVED);
        VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
    }

    public void onDetach() {
        if (VERSION.SDK_INT >= 14 && !getArguments().getBoolean("nohead")) {
            View overlay = ActionBarHacks.getActionBarOverlay(getActivity());
            if (overlay != null) {
                ((ViewGroup) overlay).setWillNotDraw(true);
            }
        }
        super.onDetach();
    }

    public void onDestroy() {
        super.onDestroy();
        VKApplication.context.unregisterReceiver(this.receiver);
        if (this.currentReq != null) {
            this.currentReq.cancel();
        }
    }

    public void onPause() {
        super.onPause();
        this.imgLoader.deactivate();
    }

    public void onResume() {
        super.onResume();
        this.imgLoader.activate();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(C0436R.menu.photoalbums, menu);
        MenuItem findItem = menu.findItem(C0436R.id.create);
        boolean z = (this.album.oid == Global.uid || this.album.canUpload) && !getArguments().getBoolean("select");
        findItem.setVisible(z);
        menu.findItem(C0436R.id.create).setTitle((int) C0436R.string.add);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0436R.id.create) {
            Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
            intent.putExtra("allow_album", false);
            startActivityForResult(intent, UPLOAD_RESULT);
        }
        return true;
    }

    public void onConfigurationChanged(Configuration cfg) {
        this.list.postDelayed(new C07445(), 200);
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode != UPLOAD_RESULT || resCode != -1) {
            return;
        }
        HashMap<String, String> params;
        if (data.hasExtra("files")) {
            Iterator it = data.getStringArrayListExtra("files").iterator();
            while (it.hasNext()) {
                String file = (String) it.next();
                Intent intent = new Intent(getActivity(), UploaderService.class);
                intent.putExtra("file", file);
                params = new HashMap();
                params.put("aid", new StringBuilder(String.valueOf(this.album.id)).toString());
                if (this.album.oid < 0) {
                    params.put("gid", new StringBuilder(String.valueOf(-this.album.oid)).toString());
                }
                intent.putExtra("req_params", params);
                intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 1);
                getActivity().startService(intent);
            }
            return;
        }
        intent = new Intent(getActivity(), SendSinglePhotoActivity.class);
        intent.putExtra("file", data.getStringExtra("file"));
        intent.putExtra("show_hint", true);
        params = new HashMap();
        params.put("aid", new StringBuilder(String.valueOf(this.album.id)).toString());
        if (this.album.oid < 0) {
            params.put("gid", new StringBuilder(String.valueOf(-this.album.oid)).toString());
        }
        intent.putExtra("req_params", params);
        startActivity(intent);
    }

    private void loadData() {
        this.dataLoading = true;
        if (this.album.id > -9000) {
            this.currentReq = new PhotosGet(this.album.oid, this.album.id, this.photos.size(), 200).setCallback(new C14986()).exec(getActivity());
        } else if (this.album.id == -9000) {
            this.dataLoading = true;
            this.currentReq = new PhotosGetUserPhotos(this.album.oid, this.photos.size(), 200).setCallback(new C14997()).exec(getActivity());
        } else if (this.album.id == -9001) {
            this.currentReq = new FaveGetPhotos(this.photos.size(), 200).setCallback(new C15008()).exec(getActivity());
        }
    }

    private void layoutPhotos() {
        if (getActivity() != null) {
            this.imgLoader.deactivate();
            this.layout.clear();
            int viewWidth = this.list.getWidth() + Global.scale(ImageViewHolder.PaddingSize);
            Log.m525d("vk", "ViewWidth=" + viewWidth);
            if (viewWidth <= Global.scale(5.0f)) {
                View v = getActivity().getWindow().getDecorView();
                Rect rect = new Rect();
                v.getGlobalVisibleRect(rect);
                viewWidth = rect.width();
                Log.m525d("vk", "NEW ViewWidth=" + viewWidth);
            }
            float avgNphotos = (float) (viewWidth / Global.scale(80.0f));
            int photosToProcess = this.photos.size();
            int curPhoto = 0;
            while (photosToProcess > 0) {
                ArrayList<Float> ratios = new ArrayList();
                ArrayList<Float> ratiosCropped = new ArrayList();
                ArrayList<PhotoLayout> page = new ArrayList();
                int i = curPhoto;
                while (true) {
                    if (i >= this.photos.size()) {
                        break;
                    }
                    Image im = ((Photo) this.photos.get(i)).getImage('x');
                    if (im == null) {
                        Log.m526e("vk", "x image is null");
                        return;
                    }
                    if (im.width <= 0 || im.height <= 0) {
                        ratios.add(Float.valueOf(1.2f));
                    } else {
                        ratios.add(Float.valueOf(((float) im.width) / ((float) im.height)));
                    }
                    i++;
                }
                if (ratios.size() != 0) {
                    Iterator it = ratios.iterator();
                    while (it.hasNext()) {
                        ratiosCropped.add(Float.valueOf(Math.min(1.0f, ((Float) it.next()).floatValue())));
                    }
                    float max_w = (float) viewWidth;
                    float margin_w = (float) Global.scale(ImageViewHolder.PaddingSize);
                    float nphotos = (float) (viewWidth / Global.scale(80.0f));
                    i = 0;
                    while (true) {
                        if (((float) i) >= Math.min(nphotos, (float) ratios.size())) {
                            break;
                        }
                        float r = ((Float) ratios.get(i)).floatValue();
                        if (((double) r) < 0.9d) {
                            nphotos = (float) (((double) nphotos) + 0.3d);
                        }
                        if (((double) r) <= 1.1d) {
                            if (((double) r) >= 0.9d) {
                                nphotos = (float) (((double) nphotos) + 0.2d);
                            }
                        }
                        i++;
                    }
                    float line_height_real = calculateMultiThumbsHeight(ratiosCropped.subList(0, Math.min(ratiosCropped.size(), Math.round(nphotos))), max_w, margin_w);
                    float line_height = Math.min(line_height_real, (float) Global.scale(80.0f));
                    int maxphotos = Math.min(Math.round(nphotos), photosToProcess);
                    Log.m528i("vk", "maxphotos=" + maxphotos + ", pages size=" + this.layout.size() + ", unsorted=" + photosToProcess + ", num=" + nphotos + ", all loaded=" + this.photos.size());
                    if (Math.round(nphotos) > photosToProcess) {
                        max_w = (float) Math.round((float) (Global.scale(80.0f) * maxphotos));
                        line_height_real = calculateMultiThumbsHeight(ratiosCropped.subList(0, Math.min(ratiosCropped.size(), Math.round(nphotos))), max_w, margin_w);
                        Log.m528i("vk", "max_w is now " + max_w);
                    }
                    for (i = 0; i < maxphotos; i++) {
                        int width = (int) (((Float) ratiosCropped.remove(0)).floatValue() * line_height_real);
                        PhotoLayout layout = new PhotoLayout(null);
                        layout.width = width;
                        layout.height = (int) line_height;
                        layout.photo = (Photo) this.photos.get(curPhoto);
                        layout.index = curPhoto;
                        curPhoto++;
                        photosToProcess--;
                        page.add(layout);
                    }
                    this.layout.add(page);
                } else {
                    return;
                }
            }
            this.adapter.notifyDataSetChanged();
            this.list.postDelayed(new C07489(), 100);
        }
    }

    private void animate() {
        int i;
        if (VERSION.SDK_INT >= 11) {
            ObjectAnimator headerAnim = ObjectAnimator.ofFloat(this.headerView, "alpha", new float[]{0.0f, 1.0f}).setDuration(200);
            AnimatorSet set = new AnimatorSet();
            set.play(headerAnim);
            ArrayList<Animator> anims = new ArrayList();
            i = this.list.getHeaderViewsCount();
            while (i < this.list.getChildCount()) {
                View ch = this.list.getChildAt(i);
                ch.setTranslationY((float) (this.list.getHeight() - ch.getTop()));
                ObjectAnimator anim = ObjectAnimator.ofFloat(ch, "translationY", new float[]{(float) (this.list.getHeight() - ch.getTop()), 0.0f});
                anim.setDuration((long) PhotoView.THUMB_ANIM_DURATION);
                anim.setStartDelay((long) ((i > 0 ? 0 : 200) + ((i - this.list.getHeaderViewsCount()) * AnimationDuration.SELECTION_EXIT_FADE)));
                Log.m528i("vk", "start delay " + i + " = " + anim.getStartDelay());
                anim.setInterpolator(new DecelerateInterpolator());
                anims.add(anim);
                i++;
            }
            set.playTogether(anims);
            set.start();
            return;
        }
        AlphaAnimation headerAnim2 = new AlphaAnimation(0.0f, 1.0f);
        headerAnim2.setDuration(200);
        i = this.list.getHeaderViewsCount();
        while (i < this.list.getChildCount()) {
            ch = this.list.getChildAt(i);
            TranslateAnimation anim2 = new TranslateAnimation(0.0f, 0.0f, (float) (this.list.getHeight() - ch.getTop()), 0.0f);
            anim2.setDuration((long) PhotoView.THUMB_ANIM_DURATION);
            anim2.setStartOffset((long) ((i > 0 ? 0 : 200) + ((i - this.list.getHeaderViewsCount()) * AnimationDuration.SELECTION_EXIT_FADE)));
            anim2.setInterpolator(new DecelerateInterpolator());
            ch.startAnimation(anim2);
            i++;
        }
        this.headerView.startAnimation(headerAnim2);
    }

    private float calculateMultiThumbsHeight(List<Float> ratios, float width, float margin) {
        return (width - (((float) (ratios.size() - 1)) * margin)) / sum(ratios);
    }

    private float sum(List<Float> a) {
        float sum = 0.0f;
        for (Float floatValue : a) {
            sum += floatValue.floatValue();
        }
        return sum;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentView;
    }

    public void onClick(View v) {
        int pos = ((Integer) v.getTag()).intValue();
        if (getArguments().getBoolean("select")) {
            Intent intent = new Intent();
            intent.putExtra("photo", (Parcelable) this.photos.get(pos));
            getActivity().setResult(-1, intent);
            getActivity().finish();
            return;
        }
        Iterator it = this.photos.iterator();
        while (it.hasNext()) {
            ((Photo) it.next()).viewBounds = null;
        }
        int i = this.list.getFirstVisiblePosition();
        while (i <= this.list.getLastVisiblePosition()) {
            if (i >= this.list.getHeaderViewsCount() && i < this.list.getHeaderViewsCount() + this.layout.size()) {
                ArrayList<PhotoLayout> l = (ArrayList) this.layout.get(i - this.list.getHeaderViewsCount());
                ViewGroup item = (ViewGroup) this.list.getChildAt(i - this.list.getFirstVisiblePosition());
                int actionBarHeight = VERSION.SDK_INT >= 14 ? getSherlockActivity().getSupportActionBar().getHeight() : 0;
                for (int j = 0; j < l.size(); j++) {
                    int i2;
                    PhotoLayout lt = (PhotoLayout) l.get(j);
                    vpos = new int[2];
                    item.getChildAt(j).getLocationOnScreen(vpos);
                    vpos[0] = vpos[0] + item.getChildAt(j).getPaddingLeft();
                    vpos[1] = vpos[1] + item.getChildAt(j).getPaddingTop();
                    lt.photo.viewBounds = new Rect(vpos[0], vpos[1], (vpos[0] + item.getChildAt(j).getWidth()) - item.getChildAt(j).getPaddingRight(), (vpos[1] + item.getChildAt(j).getHeight()) - item.getChildAt(j).getPaddingBottom());
                    int top = ViewUtils.getViewOffset(item.getChildAt(j), this.list).y - actionBarHeight;
                    Photo photo = lt.photo;
                    if (top < 0) {
                        i2 = -top;
                    } else {
                        i2 = 0;
                    }
                    photo.viewClipTop = i2;
                }
            }
            i++;
        }
        Bundle args = new Bundle();
        if (this.photos.size() <= PhotoView.THUMB_ANIM_DURATION) {
            args.putParcelableArrayList("list", this.photos);
        } else {
            args.putBoolean("shared_list", true);
            PhotoViewerFragment.sharedList = this.photos;
        }
        args.putInt("orientation", getResources().getConfiguration().orientation);
        args.putInt(GLFilterContext.AttributePosition, pos);
        args.putInt("bg_color", -15198184);
        args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, this.album.title);
        Drawable d = ((ImageView) v).getDrawable();
        if (d != null && (d instanceof BitmapDrawable)) {
            PhotoViewerFragment.sharedThumb = ((BitmapDrawable) d).getBitmap();
        }
        if (this.photos.size() < this.album.numPhotos) {
            args.putInt("total", this.album.numPhotos);
            args.putInt("aid", this.album.id);
            args.putInt("oid", this.album.oid);
        }
        args.putInt("from_album", this.album.id);
        args.putInt("from_album_oid", this.album.oid);
        Navigate.to("PhotoViewerFragment", args, getActivity(), true, -1, -1);
    }
}
