package com.vkontakte.android.fragments;

import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com.fourmob.datetimepicker.date.CalendarDatePickerDialog;
import com.fourmob.datetimepicker.time.RadialPickerLayout;
import com.fourmob.datetimepicker.time.RadialTimePickerDialog;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.VKAlertDialog;
import java.util.Calendar;

public class DateTimePickerDialogFragment extends SherlockDialogFragment {
    private Calendar date;
    private OnSelectedListener listener;
    private View view;

    /* renamed from: com.vkontakte.android.fragments.DateTimePickerDialogFragment.1 */
    class C06531 implements OnClickListener {
        C06531() {
        }

        public void onClick(DialogInterface dialog, int which) {
            if (DateTimePickerDialogFragment.this.listener != null) {
                DateTimePickerDialogFragment.this.listener.onDateSelected(DateTimePickerDialogFragment.this.date);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DateTimePickerDialogFragment.2 */
    class C06542 implements OnClickListener {
        C06542() {
        }

        public void onClick(DialogInterface dialog, int which) {
            if (DateTimePickerDialogFragment.this.listener != null) {
                DateTimePickerDialogFragment.this.listener.onDateSelected(null);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DateTimePickerDialogFragment.3 */
    class C06563 implements View.OnClickListener {

        /* renamed from: com.vkontakte.android.fragments.DateTimePickerDialogFragment.3.1 */
        class C06551 implements OnTimeSetListener {
            C06551() {
            }

            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                DateTimePickerDialogFragment.this.date.set(11, hourOfDay);
                DateTimePickerDialogFragment.this.date.set(12, Math.round(((float) minute) / 5.0f) * 5);
                DateTimePickerDialogFragment.this.updateTimer();
            }
        }

        /* renamed from: com.vkontakte.android.fragments.DateTimePickerDialogFragment.3.2 */
        class C14542 implements RadialTimePickerDialog.OnTimeSetListener {
            C14542() {
            }

            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
                DateTimePickerDialogFragment.this.date.set(11, hourOfDay);
                DateTimePickerDialogFragment.this.date.set(12, Math.round(((float) minute) / 5.0f) * 5);
                DateTimePickerDialogFragment.this.updateTimer();
            }
        }

        C06563() {
        }

        public void onClick(View v) {
            if (VERSION.SDK_INT < 14) {
                new TimePickerDialog(DateTimePickerDialogFragment.this.getActivity(), new C06551(), DateTimePickerDialogFragment.this.date.get(11), DateTimePickerDialogFragment.this.date.get(12), true).show();
                return;
            }
            RadialTimePickerDialog dlg = RadialTimePickerDialog.newInstance(new C14542(), DateTimePickerDialogFragment.this.date.get(11), DateTimePickerDialogFragment.this.date.get(12), true);
            dlg.show(DateTimePickerDialogFragment.this.getActivity().getFragmentManager(), "timepicker");
            dlg.setDoneButtonText(DateTimePickerDialogFragment.this.getString(C0436R.string.done));
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DateTimePickerDialogFragment.4 */
    class C06584 implements View.OnClickListener {

        /* renamed from: com.vkontakte.android.fragments.DateTimePickerDialogFragment.4.1 */
        class C06571 implements OnDateSetListener {
            C06571() {
            }

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c = Calendar.getInstance();
                c.set(year, monthOfYear, dayOfMonth);
                if (c.getTimeInMillis() < System.currentTimeMillis() - 1000 || c.getTimeInMillis() > System.currentTimeMillis() + 31536000000L) {
                    Toast.makeText(DateTimePickerDialogFragment.this.getActivity(), C0436R.string.invalid_date, 0).show();
                    return;
                }
                DateTimePickerDialogFragment.this.date.set(year, monthOfYear, dayOfMonth);
                DateTimePickerDialogFragment.this.updateTimer();
            }
        }

        /* renamed from: com.vkontakte.android.fragments.DateTimePickerDialogFragment.4.2 */
        class C14552 implements CalendarDatePickerDialog.OnDateSetListener {
            C14552() {
            }

            public void onDateSet(CalendarDatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c = Calendar.getInstance();
                c.set(year, monthOfYear, dayOfMonth);
                if (c.getTimeInMillis() < System.currentTimeMillis() - 1000 || c.getTimeInMillis() > System.currentTimeMillis() + 31536000000L) {
                    Toast.makeText(DateTimePickerDialogFragment.this.getActivity(), C0436R.string.invalid_date, 0).show();
                    return;
                }
                DateTimePickerDialogFragment.this.date.set(year, monthOfYear, dayOfMonth);
                DateTimePickerDialogFragment.this.updateTimer();
            }
        }

        C06584() {
        }

        public void onClick(View v) {
            if (VERSION.SDK_INT < 14) {
                new DatePickerDialog(DateTimePickerDialogFragment.this.getActivity(), new C06571(), DateTimePickerDialogFragment.this.date.get(1), DateTimePickerDialogFragment.this.date.get(2), DateTimePickerDialogFragment.this.date.get(5)).show();
                return;
            }
            Calendar now = Calendar.getInstance();
            CalendarDatePickerDialog dlg = CalendarDatePickerDialog.newInstance(new C14552(), DateTimePickerDialogFragment.this.date.get(1), DateTimePickerDialogFragment.this.date.get(2), DateTimePickerDialogFragment.this.date.get(5));
            dlg.setMinDay(now.get(5), now.get(2), now.get(1));
            dlg.setMaxDay(now.get(5), now.get(2), now.get(1) + 1);
            dlg.show(DateTimePickerDialogFragment.this.getActivity().getFragmentManager(), "datepicker");
            dlg.setDoneButtonText(DateTimePickerDialogFragment.this.getString(C0436R.string.done));
        }
    }

    public interface OnSelectedListener {
        void onDateSelected(Calendar calendar);
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        this.date = Calendar.getInstance();
        if (getArguments() == null) {
            this.date.add(11, 4);
            this.date.set(13, 0);
            this.date.set(14, 0);
            this.date.set(12, Math.round(((float) this.date.get(12)) / 5.0f) * 5);
        } else {
            this.date.setTimeInMillis(getArguments().getLong("date"));
        }
        updateTimer();
    }

    private void updateTimer() {
        if (this.view != null) {
            ((TextView) this.view.findViewById(C0436R.id.btn_date)).setText(Global.langDateDay((int) (this.date.getTimeInMillis() / 1000)));
            ((TextView) this.view.findViewById(C0436R.id.btn_time)).setText(String.format("%d:%02d", new Object[]{Integer.valueOf(this.date.get(11)), Integer.valueOf(this.date.get(12))}));
        }
    }

    public void setOnSelectedListener(OnSelectedListener l) {
        this.listener = l;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Builder adb = new VKAlertDialog.Builder(getActivity()).setTitle(C0436R.string.publish_date).setPositiveButton(C0436R.string.ok, new C06531()).setNegativeButton(C0436R.string.cancel, null);
        if (getArguments() != null) {
            adb.setNeutralButton(C0436R.string.reset, new C06542());
        }
        this.view = View.inflate(getActivity(), C0436R.layout.date_time_picker, null);
        this.view.findViewById(C0436R.id.btn_time).setOnClickListener(new C06563());
        this.view.findViewById(C0436R.id.btn_date).setOnClickListener(new C06584());
        updateTimer();
        adb.setView(this.view);
        return adb.create();
    }
}
