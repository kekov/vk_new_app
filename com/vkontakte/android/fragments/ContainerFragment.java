package com.vkontakte.android.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.Fragment.SavedState;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.BackStackEntry;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import com.actionbarsherlock.app.SherlockFragment;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ContainerFragment extends SherlockFragment {
    private boolean activityCreated;
    private ArrayList<Fragment> children;
    private InnerFragmentManager innerFragmentManager;
    private ArrayList<Runnable> runQueue;

    private class InnerFragmentManager extends FragmentManager {
        private FragmentManager f153o;

        public InnerFragmentManager(FragmentManager orig) {
            this.f153o = orig;
        }

        public void addOnBackStackChangedListener(OnBackStackChangedListener arg0) {
            this.f153o.addOnBackStackChangedListener(arg0);
        }

        public FragmentTransaction beginTransaction() {
            return new InnerFragmentTransaction(ContainerFragment.this.getFragmentManager().beginTransaction());
        }

        public void dump(String arg0, FileDescriptor arg1, PrintWriter arg2, String[] arg3) {
            this.f153o.dump(arg0, arg1, arg2, arg3);
        }

        public boolean executePendingTransactions() {
            return this.f153o.executePendingTransactions();
        }

        public Fragment findFragmentById(int arg0) {
            return this.f153o.findFragmentById(arg0);
        }

        public Fragment findFragmentByTag(String arg0) {
            return this.f153o.findFragmentByTag(arg0);
        }

        public BackStackEntry getBackStackEntryAt(int arg0) {
            return this.f153o.getBackStackEntryAt(arg0);
        }

        public int getBackStackEntryCount() {
            return this.f153o.getBackStackEntryCount();
        }

        public Fragment getFragment(Bundle arg0, String arg1) {
            return this.f153o.getFragment(arg0, arg1);
        }

        public List<Fragment> getFragments() {
            return this.f153o.getFragments();
        }

        public void popBackStack() {
            this.f153o.popBackStack();
        }

        public void popBackStack(String arg0, int arg1) {
            this.f153o.popBackStack(arg0, arg1);
        }

        public void popBackStack(int arg0, int arg1) {
            this.f153o.popBackStack(arg0, arg1);
        }

        public boolean popBackStackImmediate() {
            return this.f153o.popBackStackImmediate();
        }

        public boolean popBackStackImmediate(String arg0, int arg1) {
            return this.f153o.popBackStackImmediate(arg0, arg1);
        }

        public boolean popBackStackImmediate(int arg0, int arg1) {
            return this.f153o.popBackStackImmediate(arg0, arg1);
        }

        public void putFragment(Bundle arg0, String arg1, Fragment arg2) {
            this.f153o.putFragment(arg0, arg1, arg2);
        }

        public void removeOnBackStackChangedListener(OnBackStackChangedListener arg0) {
            this.f153o.removeOnBackStackChangedListener(arg0);
        }

        public SavedState saveFragmentInstanceState(Fragment arg0) {
            return this.f153o.saveFragmentInstanceState(arg0);
        }
    }

    private class InnerFragmentTransaction extends FragmentTransaction {
        private ArrayList<Fragment> fragmentsToAdd;
        private ArrayList<Fragment> fragmentsToRemove;
        private FragmentTransaction f154o;

        /* renamed from: com.vkontakte.android.fragments.ContainerFragment.InnerFragmentTransaction.1 */
        class C06451 implements Runnable {
            C06451() {
            }

            public void run() {
                InnerFragmentTransaction.this.f154o.commit();
                ContainerFragment.this.children.addAll(InnerFragmentTransaction.this.fragmentsToAdd);
                ContainerFragment.this.children.removeAll(InnerFragmentTransaction.this.fragmentsToRemove);
            }
        }

        /* renamed from: com.vkontakte.android.fragments.ContainerFragment.InnerFragmentTransaction.2 */
        class C06462 implements Runnable {
            C06462() {
            }

            public void run() {
                InnerFragmentTransaction.this.f154o.commitAllowingStateLoss();
                ContainerFragment.this.children.addAll(InnerFragmentTransaction.this.fragmentsToAdd);
                ContainerFragment.this.children.removeAll(InnerFragmentTransaction.this.fragmentsToRemove);
            }
        }

        public InnerFragmentTransaction(FragmentTransaction orig) {
            this.fragmentsToAdd = new ArrayList();
            this.fragmentsToRemove = new ArrayList();
            this.f154o = orig;
        }

        public FragmentTransaction add(Fragment fragment, String tag) {
            this.f154o.add(fragment, tag);
            this.fragmentsToAdd.add(fragment);
            return this;
        }

        public FragmentTransaction add(int containerID, Fragment fragment) {
            this.f154o.add(containerID, fragment);
            this.fragmentsToAdd.add(fragment);
            return this;
        }

        public FragmentTransaction add(int containerID, Fragment fragment, String tag) {
            this.f154o.add(containerID, fragment, tag);
            this.fragmentsToAdd.add(fragment);
            return this;
        }

        public FragmentTransaction addToBackStack(String arg0) {
            throw new UnsupportedOperationException("Back stack not supported for inner fragments");
        }

        public FragmentTransaction attach(Fragment fragment) {
            this.f154o.attach(fragment);
            this.fragmentsToAdd.add(fragment);
            return this;
        }

        public int commit() {
            if (ContainerFragment.this.activityCreated) {
                this.f154o.commit();
                ContainerFragment.this.children.addAll(this.fragmentsToAdd);
                ContainerFragment.this.children.removeAll(this.fragmentsToRemove);
            } else {
                ContainerFragment.this.runQueue.add(new C06451());
            }
            return -1;
        }

        public int commitAllowingStateLoss() {
            if (ContainerFragment.this.activityCreated) {
                this.f154o.commitAllowingStateLoss();
                ContainerFragment.this.children.addAll(this.fragmentsToAdd);
                ContainerFragment.this.children.removeAll(this.fragmentsToRemove);
            } else {
                ContainerFragment.this.runQueue.add(new C06462());
            }
            return -1;
        }

        public FragmentTransaction detach(Fragment fragment) {
            this.fragmentsToRemove.add(fragment);
            return this.f154o.detach(fragment);
        }

        public FragmentTransaction disallowAddToBackStack() {
            throw new UnsupportedOperationException("Back stack not supported for inner fragments");
        }

        public FragmentTransaction hide(Fragment fragment) {
            this.f154o.hide(fragment);
            return this;
        }

        public boolean isAddToBackStackAllowed() {
            throw new UnsupportedOperationException("Back stack not supported for inner fragments");
        }

        public boolean isEmpty() {
            return this.f154o.isEmpty();
        }

        public FragmentTransaction remove(Fragment fragment) {
            this.f154o.remove(fragment);
            this.fragmentsToRemove.add(fragment);
            return this;
        }

        public FragmentTransaction replace(int containerID, Fragment fragment) {
            this.f154o.replace(containerID, fragment);
            return this;
        }

        public FragmentTransaction replace(int containerID, Fragment fragment, String tag) {
            this.f154o.replace(containerID, fragment, tag);
            return this;
        }

        public FragmentTransaction setBreadCrumbShortTitle(int title) {
            this.f154o.setBreadCrumbShortTitle(title);
            return this;
        }

        public FragmentTransaction setBreadCrumbShortTitle(CharSequence title) {
            this.f154o.setBreadCrumbShortTitle(title);
            return this;
        }

        public FragmentTransaction setBreadCrumbTitle(int title) {
            this.f154o.setBreadCrumbTitle(title);
            return this;
        }

        public FragmentTransaction setBreadCrumbTitle(CharSequence title) {
            this.f154o.setBreadCrumbTitle(title);
            return this;
        }

        public FragmentTransaction setCustomAnimations(int arg0, int arg1) {
            this.f154o.setCustomAnimations(arg0, arg1);
            return this;
        }

        public FragmentTransaction setCustomAnimations(int arg0, int arg1, int arg2, int arg3) {
            this.f154o.setCustomAnimations(arg0, arg1, arg2, arg3);
            return this;
        }

        public FragmentTransaction setTransition(int arg0) {
            this.f154o.setTransition(arg0);
            return this;
        }

        public FragmentTransaction setTransitionStyle(int arg0) {
            this.f154o.setTransitionStyle(arg0);
            return this;
        }

        public FragmentTransaction show(Fragment fragment) {
            this.f154o.show(fragment);
            return this;
        }
    }

    public ContainerFragment() {
        this.runQueue = new ArrayList();
        this.activityCreated = false;
        this.children = new ArrayList();
    }

    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        this.activityCreated = true;
        Handler handler = new Handler();
        Iterator it = this.runQueue.iterator();
        while (it.hasNext()) {
            handler.post((Runnable) it.next());
        }
    }

    public FragmentManager getInnerFragmentManager() {
        if (this.innerFragmentManager == null) {
            this.innerFragmentManager = new InnerFragmentManager(getFragmentManager());
        }
        return this.innerFragmentManager;
    }

    public void onDetach() {
        if (isRemoving()) {
            Iterator it = this.children.iterator();
            while (it.hasNext()) {
                getFragmentManager().beginTransaction().remove((Fragment) it.next()).commit();
            }
        }
        super.onDetach();
    }
}
