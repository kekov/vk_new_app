package com.vkontakte.android.fragments;

import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.actionbarsherlock.app.SherlockFragment;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.ui.Fonts;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.acra.ACRAConstants;

public class SignupPhoneFragment extends SherlockFragment {
    private OnClickListener btnClickListener;
    private String btnText;
    private List<String> canadianPrefixes;
    private ArrayList<Country> countries;
    private boolean dontUpdateField;
    private String explainText;
    private boolean ignoreSelCallback;
    private int selectedCountry;
    private boolean showForgot;
    private View view;

    /* renamed from: com.vkontakte.android.fragments.SignupPhoneFragment.1 */
    class C08031 implements OnItemSelectedListener {
        C08031() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            SignupPhoneFragment.this.selectedCountry = pos;
            SignupPhoneFragment.this.ignoreSelCallback = true;
            SignupPhoneFragment.this.setCountry(pos);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupPhoneFragment.2 */
    class C08042 implements InputFilter {
        C08042() {
        }

        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            String s = source.toString().replaceAll("[^0-9+]", ACRAConstants.DEFAULT_STRING_VALUE);
            if (start != 0) {
                return s.replace("+", ACRAConstants.DEFAULT_STRING_VALUE);
            }
            return s;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupPhoneFragment.3 */
    class C08053 implements InputFilter {
        C08053() {
        }

        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            return source.toString().replaceAll("[^0-9]", ACRAConstants.DEFAULT_STRING_VALUE);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupPhoneFragment.4 */
    class C08064 implements TextWatcher {
        C08064() {
        }

        public void afterTextChanged(Editable arg0) {
            if (SignupPhoneFragment.this.ignoreSelCallback) {
                SignupPhoneFragment.this.ignoreSelCallback = false;
                return;
            }
            String code = ((EditText) SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_code)).getText().toString().replace("+", ACRAConstants.DEFAULT_STRING_VALUE);
            if (code.length() != 0) {
                int c = SignupPhoneFragment.this.findCountryByCode(code);
                int cut = 0;
                boolean select = true;
                if (c == -1 && (!code.startsWith("1") || code.length() >= 4)) {
                    while (cut < code.length()) {
                        cut++;
                        c = SignupPhoneFragment.this.findCountryByCode(code.substring(0, code.length() - cut));
                        if (c != -1) {
                            break;
                        }
                    }
                }
                if (code.startsWith("1") && (c == -1 || code.length() < ((Country) SignupPhoneFragment.this.countries.get(c)).code.length() || (code.length() == ((Country) SignupPhoneFragment.this.countries.get(c)).code.length() && !code.equals(((Country) SignupPhoneFragment.this.countries.get(c)).code)))) {
                    if (code.length() < 4) {
                        select = false;
                    } else {
                        c = SignupPhoneFragment.this.findCountryByIso("US");
                        cut = code.length() - 1;
                    }
                }
                if (select) {
                    if (cut > 0) {
                        String codePart = code.substring(0, code.length() - cut);
                        String numberPart = code.substring(code.length() - cut);
                        String num = ((EditText) SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_number)).getText().toString();
                        ((EditText) SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_code)).setText("+" + codePart);
                        ((EditText) SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_number)).setText(new StringBuilder(String.valueOf(numberPart)).append(num).toString());
                        SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_number).requestFocus();
                        ((EditText) SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_number)).setSelection(cut);
                    }
                    SignupPhoneFragment.this.selectedCountry = c;
                    if (!code.startsWith("1") && code.length() < ((Country) SignupPhoneFragment.this.countries.get(c)).code.length()) {
                        SignupPhoneFragment.this.dontUpdateField = true;
                    }
                    ((Spinner) SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_countries)).setSelection(c);
                }
            }
        }

        public void beforeTextChanged(CharSequence _s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence _s, int start, int before, int count) {
            String s = _s.toString();
            if (!s.startsWith("+")) {
                ((EditText) SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_code)).setText("+" + s);
                ((EditText) SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_code)).setSelection(1);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupPhoneFragment.5 */
    class C08075 implements TextWatcher {
        C08075() {
        }

        public void afterTextChanged(Editable text) {
            if (SignupPhoneFragment.this.selectedCountry != -1) {
                String code = ((Country) SignupPhoneFragment.this.countries.get(SignupPhoneFragment.this.selectedCountry)).isoCode;
                if ("US".equals(code) || "CA".equals(code)) {
                    String t = text.toString();
                    if (t.length() >= 3) {
                        boolean isCanada = SignupPhoneFragment.this.canadianPrefixes.contains(t.substring(0, 3));
                        if (isCanada && !"CA".equals(code)) {
                            ((Spinner) SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_countries)).setSelection(SignupPhoneFragment.this.findCountryByIso("CA"));
                        }
                        if (!isCanada && !"US".equals(code)) {
                            ((Spinner) SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_countries)).setSelection(SignupPhoneFragment.this.findCountryByIso("US"));
                        }
                    }
                }
            }
        }

        public void beforeTextChanged(CharSequence _s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence _s, int start, int before, int count) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupPhoneFragment.6 */
    class C08086 implements OnEditorActionListener {
        C08086() {
        }

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (SignupPhoneFragment.this.btnClickListener != null) {
                SignupPhoneFragment.this.btnClickListener.onClick(v);
            }
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupPhoneFragment.7 */
    class C08097 implements Runnable {
        C08097() {
        }

        public void run() {
            SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_number).requestFocus();
            ((InputMethodManager) SignupPhoneFragment.this.getActivity().getSystemService("input_method")).showSoftInput(SignupPhoneFragment.this.view.findViewById(C0436R.id.signup_phone_number), 0);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupPhoneFragment.8 */
    class C08108 implements OnClickListener {
        C08108() {
        }

        public void onClick(View v) {
            SignupPhoneFragment.this.getActivity().setResult(1);
            SignupPhoneFragment.this.getActivity().finish();
        }
    }

    private class CountriesAdapter extends BaseAdapter {
        private CountriesAdapter() {
        }

        public int getCount() {
            return SignupPhoneFragment.this.countries.size();
        }

        public Object getItem(int pos) {
            return SignupPhoneFragment.this.countries.get(pos);
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = View.inflate(SignupPhoneFragment.this.getActivity(), C0436R.layout.countries_list_item, null);
                view.setPadding(0, 0, 0, 0);
                view.findViewById(C0436R.id.country_code).setVisibility(8);
            }
            ((TextView) view.findViewById(C0436R.id.country_name)).setText(((Country) SignupPhoneFragment.this.countries.get(position)).name);
            return view;
        }

        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = View.inflate(SignupPhoneFragment.this.getActivity(), C0436R.layout.countries_list_item, null);
                view.findViewById(C0436R.id.dropdown_icon).setVisibility(8);
            }
            ((TextView) view.findViewById(C0436R.id.country_name)).setText(((Country) SignupPhoneFragment.this.countries.get(position)).name);
            ((TextView) view.findViewById(C0436R.id.country_code)).setText("+" + ((Country) SignupPhoneFragment.this.countries.get(position)).code);
            return view;
        }
    }

    private static class Country {
        String code;
        String isoCode;
        String name;

        private Country() {
        }

        public String toString() {
            return this.name + " (+" + this.code + ")";
        }
    }

    public SignupPhoneFragment() {
        this.countries = new ArrayList();
        this.selectedCountry = 0;
        this.ignoreSelCallback = false;
        this.dontUpdateField = false;
        this.canadianPrefixes = Arrays.asList(new String[]{"403", "587", "780", "250", "604", "778", "418", "438", "450", "514", "579", "581", "819", "204", "902", "867", "506", "709", "226", "249", "289", "343", "416", "519", "613", "647", "705", "807", "905", "902", "306", "867"});
    }

    public void setOnNextClickListener(OnClickListener l) {
        this.btnClickListener = l;
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        try {
            InputStream in = getActivity().getAssets().open("countries_" + Global.getDeviceLang() + ".txt");
            byte[] file = new byte[in.available()];
            in.read(file);
            in.close();
            for (String line : new String(file, "UTF-8").split("\n")) {
                String[] info = line.split(",", 4);
                Country c = new Country();
                c.code = info[0];
                c.isoCode = info[2];
                c.name = info[3];
                this.countries.add(c);
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
        TelephonyManager tm = (TelephonyManager) getActivity().getSystemService("phone");
        String userCountry = ACRAConstants.DEFAULT_STRING_VALUE;
        String simCountry = tm.getSimCountryIso().toUpperCase();
        if (simCountry.length() == 2) {
            userCountry = simCountry;
        } else {
            userCountry = Locale.getDefault().getCountry();
        }
        int i = 0;
        Iterator it = this.countries.iterator();
        while (it.hasNext()) {
            if (userCountry.equals(((Country) it.next()).isoCode)) {
                this.selectedCountry = i;
            }
            i++;
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(C0436R.layout.signup_phone, null);
        ((TextView) this.view.findViewById(C0436R.id.signup_phone_text)).setTypeface(Fonts.getRobotoLight());
        Spinner spinner = (Spinner) this.view.findViewById(C0436R.id.signup_phone_countries);
        spinner.setAdapter(new CountriesAdapter());
        spinner.setOnItemSelectedListener(new C08031());
        spinner.setSelection(this.selectedCountry);
        setCountry(this.selectedCountry);
        ((EditText) this.view.findViewById(C0436R.id.signup_phone_code)).getEditableText().setFilters(new InputFilter[]{new C08042()});
        ((EditText) this.view.findViewById(C0436R.id.signup_phone_number)).getEditableText().setFilters(new InputFilter[]{new C08053()});
        ((EditText) this.view.findViewById(C0436R.id.signup_phone_code)).addTextChangedListener(new C08064());
        ((EditText) this.view.findViewById(C0436R.id.signup_phone_number)).addTextChangedListener(new C08075());
        ((EditText) this.view.findViewById(C0436R.id.signup_phone_number)).setOnEditorActionListener(new C08086());
        this.view.findViewById(C0436R.id.signup_btn_next).setOnClickListener(this.btnClickListener);
        this.view.post(new C08097());
        this.view.findViewById(C0436R.id.auth_forgot).setOnClickListener(new C08108());
        ((TextView) this.view.findViewById(C0436R.id.auth_forgot)).setTypeface(Fonts.getRobotoLight());
        if (this.explainText != null) {
            ((TextView) this.view.findViewById(C0436R.id.signup_phone_text)).setText(this.explainText);
        }
        if (this.btnText != null) {
            ((TextView) this.view.findViewById(C0436R.id.signup_btn_next)).setText(this.btnText);
        }
        this.view.findViewById(C0436R.id.auth_forgot).setVisibility(this.showForgot ? 0 : 8);
        return this.view;
    }

    public void setNumber(String _num) {
        String num = ACRAConstants.DEFAULT_STRING_VALUE;
        for (int i = 0; i < _num.length(); i++) {
            char c = _num.charAt(i);
            if (Character.isDigit(c)) {
                num = new StringBuilder(String.valueOf(num)).append(c).toString();
            }
        }
        Country longestCode = null;
        Iterator it = this.countries.iterator();
        while (it.hasNext()) {
            Country c2 = (Country) it.next();
            if (num.startsWith(c2.code) && (longestCode == null || c2.code.length() > longestCode.code.length())) {
                longestCode = c2;
            }
        }
        if (longestCode != null) {
            num = num.substring(longestCode.code.length());
            ((EditText) this.view.findViewById(C0436R.id.signup_phone_code)).setText("+" + longestCode.code);
            ((EditText) this.view.findViewById(C0436R.id.signup_phone_number)).setText(num);
        }
    }

    private int findCountryByIso(String code) {
        int i = 0;
        Iterator it = this.countries.iterator();
        while (it.hasNext()) {
            if (((Country) it.next()).isoCode.equals(code)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    private int findCountryByCode(String code) {
        int i = 0;
        int count = 0;
        Iterator it = this.countries.iterator();
        while (it.hasNext()) {
            Country c = (Country) it.next();
            if (c.code.equals(code) && (!code.equals("1") || c.isoCode.equals("US"))) {
                return i;
            }
            i++;
        }
        i = 0;
        if (code.startsWith("1")) {
            it = this.countries.iterator();
            while (it.hasNext()) {
                if (((Country) it.next()).code.startsWith(code)) {
                    count++;
                }
            }
            if (count > 1 || count == 0) {
                return -1;
            }
        }
        it = this.countries.iterator();
        while (it.hasNext()) {
            if (((Country) it.next()).code.startsWith(code)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    private void setCountry(int pos) {
        if (this.dontUpdateField) {
            this.dontUpdateField = false;
            return;
        }
        Country c = (Country) this.countries.get(pos);
        ((EditText) this.view.findViewById(C0436R.id.signup_phone_code)).setText("+" + c.code);
        ((EditText) this.view.findViewById(C0436R.id.signup_phone_code)).setSelection(c.code.length() + 1);
    }

    public void setExplainText(String text) {
        this.explainText = text;
        if (this.view != null) {
            ((TextView) this.view.findViewById(C0436R.id.signup_phone_text)).setText(text);
        }
    }

    public void setButtonText(String text) {
        this.btnText = text;
        if (this.view != null) {
            ((TextView) this.view.findViewById(C0436R.id.signup_btn_next)).setText(text);
        }
    }

    public void setShowForgitButton(boolean show) {
        this.showForgot = show;
        if (this.view != null) {
            this.view.findViewById(C0436R.id.auth_forgot).setVisibility(show ? 0 : 8);
        }
    }

    public String getNumber() {
        return new StringBuilder(String.valueOf(((EditText) this.view.findViewById(C0436R.id.signup_phone_code)).getText().toString())).append(((EditText) this.view.findViewById(C0436R.id.signup_phone_number)).getText().toString()).toString();
    }

    public boolean isFilled() {
        return ((EditText) this.view.findViewById(C0436R.id.signup_phone_code)).getText().length() > 1 && ((EditText) this.view.findViewById(C0436R.id.signup_phone_number)).getText().length() > 3;
    }
}
