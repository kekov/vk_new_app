package com.vkontakte.android.fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.MultiSectionImageLoaderAdapter;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.SearchIndexer;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.GroupsSearch;
import com.vkontakte.android.api.GroupsSearch.Callback;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.LoadMoreFooterView;
import com.vkontakte.android.ui.MultiSectionAdapter;
import com.vkontakte.android.ui.PinnedHeaderListView;
import com.vkontakte.android.ui.RefreshableListView.OnRefreshListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import org.acra.ACRAConstants;

public class GroupListFragment extends SherlockFragment implements OnRefreshListener {
    private FriendsAdapter adapter;
    private FrameLayout contentView;
    private APIRequest currentSearchReq;
    private EmptyView emptyView;
    private LoadMoreFooterView footerView;
    private ArrayList<Group> groups;
    private ListImageLoaderWrapper imgLoader;
    private SearchIndexer<Group> indexer;
    private boolean isMe;
    private PinnedHeaderListView list;
    private boolean moreAvailable;
    private boolean preloadOnReady;
    private ArrayList<Group> preloadedSearch;
    private Runnable runAfterInit;
    private String searchQuery;
    private ArrayList<Section> sections;
    private SelectionListener selListener;
    private boolean showHints;
    private boolean showOnline;
    private boolean useSections;

    /* renamed from: com.vkontakte.android.fragments.GroupListFragment.1 */
    class C07161 implements OnItemClickListener {
        C07161() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            int[] s = GroupListFragment.this.adapter.resolveIndex(pos - GroupListFragment.this.list.getHeaderViewsCount());
            int uid = ((Group) ((Section) GroupListFragment.this.sections.get(s[0])).list.get(s[1])).id;
            if (GroupListFragment.this.selListener != null) {
                Iterator it = GroupListFragment.this.groups.iterator();
                while (it.hasNext()) {
                    Group p = (Group) it.next();
                    if (p.id == uid) {
                        GroupListFragment.this.selListener.onItemSelected(p);
                        return;
                    }
                }
            }
            Bundle args = new Bundle();
            args.putInt("id", -uid);
            Navigate.to("ProfileFragment", args, GroupListFragment.this.getActivity());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupListFragment.2 */
    class C07172 implements OnClickListener {
        C07172() {
        }

        public void onClick(View v) {
            Navigate.to("SuggestionsRecommendationsFragment", new Bundle(), GroupListFragment.this.getActivity());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupListFragment.4 */
    class C07184 implements Runnable {
        C07184() {
        }

        public void run() {
            if (GroupListFragment.this.adapter != null) {
                GroupListFragment.this.adapter.notifyDataSetChanged();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupListFragment.5 */
    class C07195 implements Runnable {
        C07195() {
        }

        public void run() {
            if (GroupListFragment.this.imgLoader != null) {
                GroupListFragment.this.imgLoader.updateImages();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupListFragment.6 */
    class C07206 implements Comparator<Group> {
        C07206() {
        }

        public int compare(Group lhs, Group rhs) {
            if (lhs.startTime == rhs.startTime) {
                if (lhs.id > rhs.id) {
                    return 1;
                }
                return -1;
            } else if (lhs.startTime <= rhs.startTime) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    private class Section {
        public ArrayList<Group> list;
        public String shortTitle;
        public int startPos;
        public String title;

        private Section() {
            this.shortTitle = null;
        }

        public String toString() {
            return this.shortTitle == null ? this.title : this.shortTitle;
        }
    }

    public interface SelectionListener {
        void onItemSelected(Group group);
    }

    /* renamed from: com.vkontakte.android.fragments.GroupListFragment.3 */
    class C14843 implements Listener {
        C14843() {
        }

        public void onScrolledToLastItem() {
            GroupListFragment.this.loadMore();
        }

        public void onScrollStopped() {
        }

        public void onScrollStarted() {
            Activity act = GroupListFragment.this.getActivity();
            if (act.getCurrentFocus() != null) {
                ((InputMethodManager) act.getSystemService("input_method")).hideSoftInputFromWindow(act.getCurrentFocus().getWindowToken(), 2);
                act.getCurrentFocus().clearFocus();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupListFragment.7 */
    class C14857 implements Callback {

        /* renamed from: com.vkontakte.android.fragments.GroupListFragment.7.1 */
        class C07211 implements Runnable {
            C07211() {
            }

            public void run() {
                GroupListFragment.this.footerView.setVisible(GroupListFragment.this.moreAvailable);
            }
        }

        /* renamed from: com.vkontakte.android.fragments.GroupListFragment.7.2 */
        class C07222 implements Runnable {
            C07222() {
            }

            public void run() {
                GroupListFragment.this.list.refreshDone();
            }
        }

        C14857() {
        }

        public void success(List<Group> results, int total) {
            try {
                boolean z;
                if (results.size() > 50) {
                    ((Section) GroupListFragment.this.sections.get(1)).list.addAll(results.subList(0, 50));
                    GroupListFragment.this.preloadedSearch.addAll(results.subList(50, results.size()));
                } else if (GroupListFragment.this.preloadOnReady) {
                    ((Section) GroupListFragment.this.sections.get(1)).list.addAll(results);
                } else {
                    GroupListFragment.this.preloadedSearch.addAll(results);
                }
                GroupListFragment.this.updateList();
                GroupListFragment.this.currentSearchReq = null;
                if (GroupListFragment.this.preloadOnReady) {
                    GroupListFragment.this.preloadOnReady = false;
                    GroupListFragment.this.loadMore();
                }
                GroupListFragment groupListFragment = GroupListFragment.this;
                if (results.size() > 0) {
                    z = true;
                } else {
                    z = false;
                }
                groupListFragment.moreAvailable = z;
                if (GroupListFragment.this.getActivity() != null) {
                    GroupListFragment.this.getActivity().runOnUiThread(new C07211());
                    if (GroupListFragment.this.list != null && GroupListFragment.this.list.isRefreshing()) {
                        GroupListFragment.this.getActivity().runOnUiThread(new C07222());
                    }
                }
            } catch (Exception e) {
            }
        }

        public void fail(int ecode, String emsg) {
            GroupListFragment.this.currentSearchReq = null;
            if (GroupListFragment.this.getActivity() != null) {
                Toast.makeText(GroupListFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
                if (GroupListFragment.this.list != null && GroupListFragment.this.list.isRefreshing()) {
                    GroupListFragment.this.list.refreshDone();
                }
            }
        }
    }

    private class FriendsAdapter extends MultiSectionAdapter implements SectionIndexer {
        private FriendsAdapter() {
        }

        public View getView(int section, int item, View convertView) {
            View view = convertView;
            if (view == null) {
                view = View.inflate(GroupListFragment.this.getActivity(), C0436R.layout.friend_list_item_subtitle, null);
            }
            Group grp = (Group) ((Section) GroupListFragment.this.sections.get(section)).list.get(item);
            ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(grp.name);
            String subtitle = ACRAConstants.DEFAULT_STRING_VALUE;
            if (grp.type == 0) {
                switch (grp.isClosed) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                        subtitle = GroupListFragment.this.getResources().getString(C0436R.string.open_group);
                        break;
                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                        subtitle = GroupListFragment.this.getResources().getString(C0436R.string.closed_group);
                        break;
                    case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                        subtitle = GroupListFragment.this.getResources().getString(C0436R.string.private_group);
                        break;
                }
            } else if (grp.type == 2) {
                subtitle = GroupListFragment.this.getResources().getString(C0436R.string.public_page);
            } else if (grp.type == 1) {
                subtitle = Global.langDate(GroupListFragment.this.getResources(), grp.startTime);
            }
            ((TextView) view.findViewById(C0436R.id.flist_item_subtext)).setText(subtitle);
            view.findViewById(C0436R.id.flist_item_online).setVisibility(8);
            if (GroupListFragment.this.imgLoader.isAlreadyLoaded(grp.photo)) {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageBitmap(GroupListFragment.this.imgLoader.get(grp.photo));
            } else {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(C0436R.drawable.group_placeholder);
            }
            return view;
        }

        public String getSectionTitle(int section) {
            return ((Section) GroupListFragment.this.sections.get(section)).title;
        }

        public int getSectionCount() {
            return GroupListFragment.this.sections.size();
        }

        public int getItemCount(int section) {
            return ((Section) GroupListFragment.this.sections.get(section)).list.size();
        }

        public long getItemId(int section, int item) {
            return 0;
        }

        public boolean isSectionHeaderVisible(int section) {
            return ((Section) GroupListFragment.this.sections.get(section)).title != null && ((Section) GroupListFragment.this.sections.get(section)).list.size() > 0;
        }

        public int getPositionForSection(int section) {
            if (section >= GroupListFragment.this.sections.size()) {
                return 0;
            }
            return ((Section) GroupListFragment.this.sections.get(section)).startPos;
        }

        public Object[] getSections() {
            return GroupListFragment.this.sections.toArray(new Section[0]);
        }
    }

    private class FriendsPhotosAdapter extends MultiSectionImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.GroupListFragment.FriendsPhotosAdapter.1 */
        class C07231 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C07231(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.flist_item_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private FriendsPhotosAdapter() {
        }

        public int getSectionCount() {
            return GroupListFragment.this.sections.size();
        }

        public int getItemCount(int section) {
            return ((Section) GroupListFragment.this.sections.get(section)).list.size();
        }

        public boolean isSectionHeaderVisible(int section) {
            return ((Section) GroupListFragment.this.sections.get(section)).title != null && ((Section) GroupListFragment.this.sections.get(section)).list.size() > 0;
        }

        public int getImageCountForItem(int section, int item) {
            return 1;
        }

        public String getImageURL(int section, int item, int image) {
            try {
                return ((Group) ((Section) GroupListFragment.this.sections.get(section)).list.get(item)).photo;
            } catch (Exception e) {
                return null;
            }
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += GroupListFragment.this.list.getHeaderViewsCount();
            if (item >= GroupListFragment.this.list.getFirstVisiblePosition() && item <= GroupListFragment.this.list.getLastVisiblePosition()) {
                GroupListFragment.this.getActivity().runOnUiThread(new C07231(GroupListFragment.this.list.getChildAt(item - GroupListFragment.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    public GroupListFragment() {
        this.sections = new ArrayList();
        this.groups = new ArrayList();
        this.showOnline = false;
        this.showHints = true;
        this.indexer = new SearchIndexer();
        this.searchQuery = null;
        this.preloadedSearch = new ArrayList();
        this.preloadOnReady = false;
        this.moreAvailable = true;
        this.useSections = true;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        boolean z;
        this.contentView = new FrameLayout(getActivity());
        this.footerView = new LoadMoreFooterView(getActivity());
        this.contentView.setBackgroundColor(-1);
        this.list = new PinnedHeaderListView(getActivity());
        this.list.addFooterView(this.footerView, null, false);
        this.footerView.setVisible(false);
        PinnedHeaderListView pinnedHeaderListView = this.list;
        ListAdapter friendsAdapter = new FriendsAdapter();
        this.adapter = friendsAdapter;
        pinnedHeaderListView.setAdapter(friendsAdapter);
        this.list.setSelector(C0436R.drawable.highlight);
        this.list.setPadding(getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding), 0, getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding), 0);
        this.list.setScrollBarStyle(33554432);
        this.list.setDivider(new ColorDrawable(-1710619));
        this.list.setDividerHeight(Global.scale(1.0f));
        this.list.setHeaderDividersEnabled(false);
        this.list.setOnRefreshListener(this);
        this.contentView.addView(this.list);
        this.list.setOnItemClickListener(new C07161());
        this.emptyView = EmptyView.create(getActivity());
        this.contentView.addView(this.emptyView);
        this.list.setEmptyView(this.emptyView);
        this.emptyView.setButtonText((int) C0436R.string.empty_find_groups);
        this.emptyView.setOnBtnClickListener(new C07172());
        this.imgLoader = new ListImageLoaderWrapper(new FriendsPhotosAdapter(), this.list, new C14843());
        EmptyView emptyView = this.emptyView;
        int i = this.showHints ? C0436R.string.no_events : this.isMe ? C0436R.string.no_groups_me : C0436R.string.no_groups;
        emptyView.setText(i);
        emptyView = this.emptyView;
        if (this.showHints || !this.isMe) {
            z = false;
        } else {
            z = true;
        }
        emptyView.setButtonVisible(z);
        return this.contentView;
    }

    public void onDestroyView() {
        this.list = null;
        this.adapter = null;
        this.imgLoader = null;
        this.footerView = null;
        this.emptyView = null;
        this.contentView = null;
        super.onDestroyView();
    }

    public void updateList() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new C07184());
            if (this.list != null) {
                this.list.postDelayed(new C07195(), 100);
            }
        }
    }

    public void runAfterInit(Runnable r) {
        this.runAfterInit = r;
    }

    public void setSelectionListener(SelectionListener l) {
        this.selListener = l;
    }

    public void setData(ArrayList<Group> l, boolean showHints, boolean me) {
        this.isMe = me;
        this.groups.clear();
        this.groups.addAll(l);
        this.indexer.bind(this.groups);
        this.indexer.build();
        this.showHints = showHints;
        createSections();
        if (this.list != null) {
            if (this.list.isRefreshing()) {
                this.list.refreshDone();
            }
            updateList();
            EmptyView emptyView = this.emptyView;
            int i = showHints ? C0436R.string.no_events : me ? C0436R.string.no_groups_me : C0436R.string.no_groups;
            emptyView.setText(i);
            emptyView = this.emptyView;
            boolean z = !showHints && me;
            emptyView.setButtonVisible(z);
        }
    }

    public void updateFilter(String filter) {
        if (this.currentSearchReq != null) {
            this.currentSearchReq.cancel();
            this.currentSearchReq = null;
        }
        if (!this.list.isRefreshing()) {
            this.preloadedSearch.clear();
            this.moreAvailable = true;
            this.preloadOnReady = false;
            if (filter == null || filter.length() == 0) {
                boolean z;
                this.searchQuery = null;
                this.footerView.setVisible(false);
                createSections();
                updateList();
                this.list.setSelection(0);
                EmptyView emptyView = this.emptyView;
                int i = this.showHints ? C0436R.string.no_events : this.isMe ? C0436R.string.no_groups_me : C0436R.string.no_groups;
                emptyView.setText(i);
                emptyView = this.emptyView;
                if (this.showHints || !this.isMe) {
                    z = false;
                } else {
                    z = true;
                }
                emptyView.setButtonVisible(z);
                this.list.setDraggingEnabled(true);
                return;
            }
            this.list.setDraggingEnabled(false);
            this.emptyView.setButtonVisible(false);
            this.emptyView.setText((int) C0436R.string.nothing_found);
            this.footerView.setVisible(true);
            this.searchQuery = filter;
            this.list.setFastScrollEnabled(false);
            this.sections.clear();
            Section search = new Section();
            this.sections.add(search);
            search.title = getResources().getString(C0436R.string.search_results);
            List<Group> results = this.indexer.search(filter);
            search.list = new ArrayList();
            search.list.addAll(results);
            Section gsearch = new Section();
            gsearch.title = getResources().getString(C0436R.string.search_global);
            gsearch.list = new ArrayList();
            this.sections.add(gsearch);
            updateList();
            this.list.setSelection(0);
        }
    }

    public void setUserSections(boolean use) {
        this.useSections = use;
        createSections();
    }

    public void createSections() {
        if (this.showHints) {
            Collections.sort(this.groups, new C07206());
            int now = (int) (System.currentTimeMillis() / 1000);
            Section future = new Section();
            future.list = new ArrayList();
            future.title = VKApplication.context.getResources().getString(C0436R.string.groups_upcoming_events);
            Section past = new Section();
            past.list = new ArrayList();
            past.title = VKApplication.context.getResources().getString(C0436R.string.groups_past_events);
            Iterator it = this.groups.iterator();
            while (it.hasNext()) {
                Group g = (Group) it.next();
                if (g.startTime > now) {
                    future.list.add(g);
                } else {
                    past.list.add(0, g);
                }
            }
            this.sections.clear();
            if (future.list.size() > 0) {
                this.sections.add(future);
            }
            if (past.list.size() > 0) {
                this.sections.add(past);
            }
        } else {
            Section s = new Section();
            s.list = this.groups;
            this.sections.clear();
            this.sections.add(s);
        }
        if (this.runAfterInit != null) {
            this.runAfterInit.run();
            this.runAfterInit = null;
        }
    }

    private void loadMore() {
        if (this.searchQuery != null && this.currentSearchReq == null) {
            if (this.preloadedSearch.size() > 0) {
                ((Section) this.sections.get(1)).list.addAll(this.preloadedSearch);
                this.preloadedSearch.clear();
                updateList();
            } else if (((Section) this.sections.get(1)).list.size() > 0) {
                this.preloadOnReady = true;
            }
            if (this.moreAvailable) {
                this.currentSearchReq = new GroupsSearch(this.searchQuery, ((Section) this.sections.get(1)).list.size(), ((Section) this.sections.get(1)).list.size() == 0 ? 100 : 50).setCallback(new C14857()).exec();
            }
        }
    }

    public void onRefresh() {
        if (this.searchQuery != null) {
            updateFilter(this.searchQuery);
        } else {
            Groups.reload(true);
        }
    }

    public String getLastUpdatedTime() {
        return null;
    }

    public void onScrolled(float offset) {
    }
}
