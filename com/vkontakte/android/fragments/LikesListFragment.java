package com.vkontakte.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import org.acra.ACRAConstants;

public class LikesListFragment extends SherlockFragment {
    private boolean friendsLoaded;
    private UserListView friendsView;
    private UserListView likesView;
    private ViewPager pager;
    private boolean repostsLoaded;
    private UserListView repostsView;
    private PagerSlidingTabStrip tabbar;
    private LinearLayout view;

    /* renamed from: com.vkontakte.android.fragments.LikesListFragment.1 */
    class C14911 implements OnPageChangeListener {
        C14911() {
        }

        public void onPageSelected(int pos) {
            if (pos == 1 && !LikesListFragment.this.friendsLoaded) {
                LikesListFragment.this.friendsView.loadData();
                LikesListFragment.this.friendsLoaded = true;
            }
            if (pos == 2 && !LikesListFragment.this.repostsLoaded) {
                LikesListFragment.this.repostsView.loadData();
                LikesListFragment.this.repostsLoaded = true;
            }
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    }

    private class LikesPagerAdapter extends PagerAdapter {
        private LikesPagerAdapter() {
        }

        public int getCount() {
            return 3;
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public Object instantiateItem(ViewGroup container, int position) {
            View v = null;
            switch (position) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    v = LikesListFragment.this.likesView;
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    v = LikesListFragment.this.friendsView;
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    v = LikesListFragment.this.repostsView;
                    break;
            }
            container.addView(v);
            return v;
        }

        public CharSequence getPageTitle(int pos) {
            switch (pos) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return LikesListFragment.this.getString(C0436R.string.liked);
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return LikesListFragment.this.getString(C0436R.string.friends);
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return LikesListFragment.this.getString(C0436R.string.reposters);
                default:
                    return "qwe";
            }
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            View v = null;
            switch (position) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    v = LikesListFragment.this.likesView;
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    v = LikesListFragment.this.friendsView;
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    v = LikesListFragment.this.repostsView;
                    break;
            }
            container.removeView(v);
        }
    }

    public LikesListFragment() {
        this.friendsLoaded = false;
        this.repostsLoaded = false;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        SherlockFragmentActivity sa = getSherlockActivity();
        sa.getSupportActionBar().setDisplayShowTitleEnabled(true);
        sa.getSupportActionBar().setNavigationMode(0);
        String title = ACRAConstants.DEFAULT_STRING_VALUE;
        if (getArguments().containsKey(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)) {
            title = getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE).toString();
        }
        sa.setTitle(title);
        Bundle likesArgs = (Bundle) getArguments().clone();
        Bundle friendsArgs = (Bundle) getArguments().clone();
        Bundle repostsArgs = (Bundle) getArguments().clone();
        friendsArgs.putBoolean("friends_only", true);
        repostsArgs.putString("filter", "copies");
        this.likesView = new UserListView(act, 0, likesArgs);
        this.likesView.loadData();
        this.friendsView = new UserListView(act, 0, friendsArgs);
        this.repostsView = new UserListView(act, 0, repostsArgs);
        this.view = new LinearLayout(getActivity());
        this.view.setOrientation(1);
        this.pager = new ViewPager(getActivity());
        this.pager.setAdapter(new LikesPagerAdapter());
        this.tabbar = new PagerSlidingTabStrip(act);
        this.tabbar.setBackgroundResource(C0436R.color.tab_bg);
        this.tabbar.setIndicatorColorResource(C0436R.color.tab_indicator);
        this.tabbar.setViewPager(this.pager);
        this.tabbar.setOnPageChangeListener(new C14911());
        this.view.addView(this.tabbar, new LayoutParams(-1, Global.scale(GalleryPickerFooterView.SIZE)));
        this.view.addView(this.pager);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.view;
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }
}
