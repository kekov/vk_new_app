package com.vkontakte.android.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnCloseListener;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip.BadgeTabProvider;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.ActivityUtils;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.GroupsGet;
import com.vkontakte.android.api.GroupsGet.Callback;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.fragments.GroupListFragment.SelectionListener;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.PhotoView;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.acra.ACRAConstants;

public class GroupsFragment extends ContainerFragment {
    private ArrayList<Group> allGroups;
    private LinearLayout contentView;
    private FrameLayout contentWrap;
    private APIRequest currentReq;
    private ErrorView errorView;
    private ArrayList<Group> events;
    private GroupListFragment eventsView;
    private ArrayList<Group> groups;
    private GroupListFragment groupsView;
    private ArrayList<Object> lists;
    private int numInvites;
    private ViewPager pager;
    private ProgressBar progress;
    private BroadcastReceiver receiver;
    private boolean reqsLoaded;
    private GroupInvitationsFragment requestsView;
    private SearchView searchView;
    private boolean searching;
    private boolean select;
    private boolean showAdmined;
    private PagerSlidingTabStrip tabbar;
    private ArrayList<CharSequence> titles;
    private int uid;

    /* renamed from: com.vkontakte.android.fragments.GroupsFragment.1 */
    class C07261 extends BroadcastReceiver {
        C07261() {
        }

        public void onReceive(Context context, Intent intent) {
            if (GroupsFragment.this.isAdded()) {
                if (Groups.ACTION_GROUP_LIST_CHANGED.equals(intent.getAction())) {
                    GroupsFragment.this.update();
                }
                if (Groups.ACTION_GROUP_INVITES_CHANGED.equals(intent.getAction())) {
                    GroupsFragment.this.numInvites = LongPollService.numGroupInvitations;
                    GroupsFragment.this.pager.getAdapter().notifyDataSetChanged();
                    if (GroupsFragment.this.pager.getCurrentItem() == 2 && GroupsFragment.this.numInvites == 0) {
                        GroupsFragment.this.pager.setCurrentItem(0, true);
                    }
                    GroupsFragment.this.updateTabs();
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupsFragment.2 */
    class C07272 extends ArrayAdapter<String> {
        C07272(Context $anonymous0, int $anonymous1, List $anonymous2) {
            super($anonymous0, $anonymous1, $anonymous2);
        }

        public View getView(int pos, View view, ViewGroup group) {
            return super.getView(GroupsFragment.this.getSherlockActivity().getSupportActionBar().getSelectedNavigationIndex(), view, group);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupsFragment.7 */
    class C07287 implements OnClickListener {
        C07287() {
        }

        public void onClick(View v) {
            GroupsFragment.this.getSherlockActivity().getSupportActionBar().setNavigationMode(0);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupsFragment.9 */
    class C07299 implements OnClickListener {
        C07299() {
        }

        public void onClick(View v) {
            GroupsFragment.this.update();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupsFragment.3 */
    class C14863 implements OnNavigationListener {
        C14863() {
        }

        public boolean onNavigationItemSelected(int pos, long itemId) {
            GroupsFragment.this.showAdmined = pos == 1;
            GroupsFragment.this.update();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupsFragment.4 */
    class C14874 extends ViewPager {
        C14874(Context $anonymous0) {
            super($anonymous0);
        }

        public boolean onInterceptTouchEvent(MotionEvent ev) {
            if (GroupsFragment.this.searching) {
                return false;
            }
            return super.onInterceptTouchEvent(ev);
        }

        public boolean onTouchEvent(MotionEvent ev) {
            if (GroupsFragment.this.searching) {
                return false;
            }
            return super.onTouchEvent(ev);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupsFragment.5 */
    class C14885 implements OnPageChangeListener {
        C14885() {
        }

        public void onPageSelected(int pos) {
            if (pos == 2 && GroupsFragment.this.uid == Global.uid && !GroupsFragment.this.reqsLoaded) {
                GroupsFragment.this.requestsView.loadData();
                GroupsFragment.this.reqsLoaded = true;
            }
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupsFragment.6 */
    class C14896 implements OnQueryTextListener {
        C14896() {
        }

        public boolean onQueryTextSubmit(String query) {
            View focus = GroupsFragment.this.getActivity().getCurrentFocus();
            if (focus != null) {
                ((InputMethodManager) GroupsFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(focus.getWindowToken(), 0);
                focus.clearFocus();
            }
            return false;
        }

        public boolean onQueryTextChange(String newText) {
            boolean ns;
            if (newText == null || newText.length() <= 0) {
                ns = false;
            } else {
                ns = true;
            }
            if (ns != GroupsFragment.this.searching) {
                GroupsFragment.this.searching = ns;
                if (GroupsFragment.this.searching) {
                    GroupsFragment.this.pager.setCurrentItem(0, false);
                    GroupsFragment.this.tabbar.pageListener.onPageScrolled(0, 0.0f, 0);
                    GroupsFragment.this.tabbar.pageListener.onPageSelected(0);
                    GroupsFragment.this.tabbar.setVisibility(8);
                } else {
                    GroupsFragment.this.tabbar.setVisibility(0);
                }
            }
            GroupsFragment.this.groupsView.updateFilter(newText);
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupsFragment.8 */
    class C14908 implements OnCloseListener {
        C14908() {
        }

        public boolean onClose() {
            GroupsFragment.this.getSherlockActivity().getSupportActionBar().setNavigationMode(1);
            return false;
        }
    }

    private class FriendsPagerAdapter extends FragmentPagerAdapter implements BadgeTabProvider {
        public FriendsPagerAdapter() {
            super(GroupsFragment.this.getInnerFragmentManager());
        }

        public int getCount() {
            return (GroupsFragment.this.uid != Global.uid || GroupsFragment.this.select || GroupsFragment.this.numInvites <= 0) ? 2 : 3;
        }

        public CharSequence getPageTitle(int pos) {
            return pos < GroupsFragment.this.titles.size() ? (CharSequence) GroupsFragment.this.titles.get(pos) : ACRAConstants.DEFAULT_STRING_VALUE;
        }

        public Fragment getItem(int pos) {
            switch (pos) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return GroupsFragment.this.groupsView;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return GroupsFragment.this.eventsView;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return GroupsFragment.this.requestsView;
                default:
                    return null;
            }
        }

        public String getPageBadgeValue(int position) {
            if (position == 2 && Global.uid == GroupsFragment.this.uid && LongPollService.numGroupInvitations > 0) {
                int cnt = LongPollService.numGroupInvitations;
                if (cnt < LocationStatusCodes.GEOFENCE_NOT_AVAILABLE) {
                    return new StringBuilder(String.valueOf(cnt)).toString();
                }
                if (cnt >= LocationStatusCodes.GEOFENCE_NOT_AVAILABLE && cnt < 1000000) {
                    return (cnt / LocationStatusCodes.GEOFENCE_NOT_AVAILABLE) + "K";
                }
                if (cnt >= 1000000) {
                    return String.format("%.1fM", new Object[]{Float.valueOf(((float) cnt) / 1000000.0f)});
                }
            }
            return null;
        }

        public long getItemId(int position) {
            return (long) getItem(position).hashCode();
        }
    }

    public GroupsFragment() {
        this.uid = Global.uid;
        this.reqsLoaded = false;
        this.allGroups = new ArrayList();
        this.groups = new ArrayList();
        this.events = new ArrayList();
        this.titles = new ArrayList();
        this.numInvites = LongPollService.numGroupInvitations;
        this.lists = new ArrayList();
        this.receiver = new C07261();
        this.searching = false;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        this.select = getArguments().getBoolean("select");
        this.uid = getArguments().getInt("uid", Global.uid);
        ActivityUtils.setBeamLink(act, "groups?id=" + this.uid);
        SherlockFragmentActivity sa = getSherlockActivity();
        if ((this.uid == 0 || this.uid == Global.uid) && !getArguments().getBoolean("admin_only")) {
            sa.getSupportActionBar().setNavigationMode(1);
            ArrayAdapter<String> navAdapter = new C07272(sa.getSupportActionBar().getThemedContext(), C0436R.layout.nav_spinner_item, new ArrayList());
            navAdapter.add(getString(C0436R.string.groups));
            navAdapter.add(getString(C0436R.string.groups_mgmt));
            navAdapter.setDropDownViewResource(C0436R.layout.sherlock_spinner_dropdown_item);
            sa.getSupportActionBar().setListNavigationCallbacks(navAdapter, new C14863());
            sa.getSupportActionBar().setDisplayShowTitleEnabled(false);
        } else {
            sa.getSupportActionBar().setNavigationMode(0);
            sa.getSupportActionBar().setDisplayShowTitleEnabled(true);
            if (getArguments().containsKey(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)) {
                sa.setTitle(getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
            } else {
                sa.setTitle(C0436R.string.groups);
            }
        }
        if (getArguments().getBoolean("admin_only")) {
            this.showAdmined = true;
        }
        this.tabbar = new PagerSlidingTabStrip(act);
        this.tabbar.setBackgroundResource(C0436R.color.tab_bg);
        this.tabbar.setIndicatorColorResource(C0436R.color.tab_indicator);
        this.groupsView = new GroupListFragment();
        this.contentView = new LinearLayout(act);
        this.contentView.setOrientation(1);
        this.contentView.addView(this.tabbar, new LayoutParams(-1, Global.scale(GalleryPickerFooterView.SIZE)));
        this.pager = new C14874(act);
        this.pager.setId(C0436R.id.inner_fragment_wrapper);
        this.contentView.addView(this.pager, new LayoutParams(-1, -1));
        this.pager.setAdapter(new FriendsPagerAdapter());
        this.tabbar.setViewPager(this.pager);
        updateTabs();
        this.eventsView = new GroupListFragment();
        if (this.uid == Global.uid) {
            this.requestsView = new GroupInvitationsFragment();
        }
        this.tabbar.setOnPageChangeListener(new C14885());
        this.searchView = new SearchView(sa.getSupportActionBar().getThemedContext());
        this.searchView.setQueryHint(getResources().getString(C0436R.string.search));
        try {
            Field searchField = SearchView.class.getDeclaredField("mSearchButton");
            searchField.setAccessible(true);
            ((ImageView) searchField.get(this.searchView)).setImageResource(C0436R.drawable.ic_ab_search);
            searchField = SearchView.class.getDeclaredField("mSearchPlate");
            searchField.setAccessible(true);
            ((TextView) ((LinearLayout) searchField.get(this.searchView)).getChildAt(0)).setHintTextColor(-2130706433);
        } catch (Exception e) {
        }
        this.searchView.setOnQueryTextListener(new C14896());
        this.searchView.setOnSearchClickListener(new C07287());
        this.searchView.setOnCloseListener(new C14908());
        this.progress = new ProgressBar(act);
        this.contentWrap = new FrameLayout(act);
        this.contentWrap.addView(this.contentView);
        this.contentWrap.setBackgroundColor(-1);
        this.contentWrap.addView(this.progress, new FrameLayout.LayoutParams(Global.scale(40.0f), Global.scale(40.0f), 17));
        this.errorView = (ErrorView) View.inflate(act, C0436R.layout.error, null);
        this.errorView.setVisibility(8);
        this.contentWrap.addView(this.errorView);
        this.errorView.setOnRetryListener(new C07299());
        setHasOptionsMenu(true);
        update();
        if (this.select) {
            SelectionListener sl = new SelectionListener() {
                public void onItemSelected(Group p) {
                    Intent result = new Intent();
                    result.putExtra("gid", p.id);
                    result.putExtra("name", p.name);
                    result.putExtra("photo", p.photo);
                    GroupsFragment.this.getActivity().setResult(-1, result);
                    GroupsFragment.this.getActivity().finish();
                }
            };
            this.groupsView.setSelectionListener(sl);
            this.eventsView.setSelectionListener(sl);
        }
    }

    public void onDetach() {
        getSherlockActivity().getSupportActionBar().setListNavigationCallbacks(null, null);
        getSherlockActivity().getSupportActionBar().setNavigationMode(0);
        super.onDetach();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem search = menu.add((int) C0436R.string.search);
        search.setShowAsAction(2);
        search.setActionView(this.searchView);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void updateTabs() {
        if (getActivity() != null) {
            this.titles.clear();
            this.titles.add(Global.langPlural(C0436R.array.groups, this.groups.size(), getResources()));
            this.titles.add(Global.langPlural(C0436R.array.events, this.events.size(), getResources()));
            if (this.numInvites > 0 && this.uid == Global.uid) {
                this.titles.add(getString(C0436R.string.groups_invitations));
            }
            this.pager.getAdapter().notifyDataSetChanged();
            this.tabbar.notifyDataSetChanged();
            this.tabbar.postInvalidate();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentWrap;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        if (this.uid == 0 || this.uid == Global.uid) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(Groups.ACTION_GROUP_LIST_CHANGED);
            filter.addAction(Groups.ACTION_GROUP_INVITES_CHANGED);
            VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
        if (this.currentReq != null) {
            this.currentReq.cancel();
        }
    }

    private void update() {
        if (this.uid == Global.uid) {
            if (this.groups.size() == 0) {
                this.contentView.setVisibility(4);
                this.progress.setVisibility(0);
            }
            new Thread(new Runnable() {

                /* renamed from: com.vkontakte.android.fragments.GroupsFragment.11.1 */
                class C07251 implements Runnable {

                    /* renamed from: com.vkontakte.android.fragments.GroupsFragment.11.1.1 */
                    class C07241 implements Runnable {
                        C07241() {
                        }

                        public void run() {
                            Global.showViewAnimated(GroupsFragment.this.contentView, true, PhotoView.THUMB_ANIM_DURATION);
                            Global.showViewAnimated(GroupsFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                        }
                    }

                    C07251() {
                    }

                    public void run() {
                        GroupsFragment.this.groups.clear();
                        GroupsFragment.this.events.clear();
                        Iterator it = GroupsFragment.this.allGroups.iterator();
                        while (it.hasNext()) {
                            Group g = (Group) it.next();
                            if (g.type == 1) {
                                GroupsFragment.this.events.add(g);
                            } else {
                                GroupsFragment.this.groups.add(g);
                            }
                        }
                        GroupsFragment.this.eventsView.setData(GroupsFragment.this.events, true, true);
                        if (GroupsFragment.this.contentView.getVisibility() != 0) {
                            GroupsFragment.this.groupsView.runAfterInit(new C07241());
                        }
                        GroupsFragment.this.groupsView.setData(GroupsFragment.this.groups, false, true);
                        GroupsFragment.this.updateTabs();
                    }
                }

                public void run() {
                    if (GroupsFragment.this.getArguments().getBoolean("_from_menu") && GroupsFragment.this.allGroups.size() == 0) {
                        try {
                            Thread.sleep(180);
                        } catch (Exception e) {
                        }
                    }
                    GroupsFragment.this.allGroups.clear();
                    if (GroupsFragment.this.showAdmined) {
                        Groups.getAdminedGroups(GroupsFragment.this.allGroups);
                    } else {
                        Groups.getGroups(GroupsFragment.this.allGroups);
                    }
                    if (GroupsFragment.this.getActivity() != null) {
                        GroupsFragment.this.getActivity().runOnUiThread(new C07251());
                    }
                }
            }).start();
            return;
        }
        this.contentView.setVisibility(4);
        this.progress.setVisibility(0);
        this.currentReq = new GroupsGet(this.uid).setCallback(new Callback() {
            public void success(ArrayList<Group> list) {
                GroupsFragment.this.allGroups.clear();
                GroupsFragment.this.allGroups.addAll(list);
                GroupsFragment.this.groups.clear();
                GroupsFragment.this.events.clear();
                Iterator it = GroupsFragment.this.allGroups.iterator();
                while (it.hasNext()) {
                    Group g = (Group) it.next();
                    if (g.type == 1) {
                        GroupsFragment.this.events.add(g);
                    } else {
                        GroupsFragment.this.groups.add(g);
                    }
                }
                GroupsFragment.this.groupsView.setData(GroupsFragment.this.groups, false, false);
                GroupsFragment.this.eventsView.setData(GroupsFragment.this.events, true, false);
                GroupsFragment.this.updateTabs();
                Global.showViewAnimated(GroupsFragment.this.contentView, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(GroupsFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                GroupsFragment.this.currentReq = null;
            }

            public void fail(int ecode, String emsg) {
                GroupsFragment.this.errorView.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(GroupsFragment.this.errorView, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(GroupsFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                GroupsFragment.this.currentReq = null;
            }
        }).exec(this.contentView);
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }
}
