package com.vkontakte.android.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.google.android.c2dm.C2DMessaging;
import com.vkontakte.android.Auth;
import com.vkontakte.android.BlacklistActivity;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.C2DM;
import com.vkontakte.android.ChangePasswordActivity;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.MainActivity;
import com.vkontakte.android.NewsfeedBanlistActivity;
import com.vkontakte.android.SettingsAdvancedActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.WelcomeActivity;
import com.vkontakte.android.api.Group;
import java.util.Date;
import org.acra.ACRAConstants;

public class SettingsFragment extends PreferenceFragment {
    public static final int REQUEST_SYNC_SETTINGS = 203;
    private Preference cancelDndPref;
    private Preference dnd1Pref;
    private Preference dnd8Pref;
    private SharedPreferences prefs;

    /* renamed from: com.vkontakte.android.fragments.SettingsFragment.17 */
    class AnonymousClass17 implements OnTimeSetListener {
        private final /* synthetic */ boolean[] val$canceled;

        AnonymousClass17(boolean[] zArr) {
            this.val$canceled = zArr;
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            if (!this.val$canceled[0]) {
                Date date = new Date();
                if ((date.getHours() * 60) + date.getMinutes() < (hourOfDay * 60) + minute) {
                    date.setHours(hourOfDay);
                    date.setMinutes(minute);
                    SettingsFragment.this.setDnd(date.getTime());
                    return;
                }
                date.setTime(System.currentTimeMillis() + 86400000);
                date.setHours(hourOfDay);
                date.setMinutes(minute);
                SettingsFragment.this.setDnd(date.getTime());
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SettingsFragment.19 */
    class AnonymousClass19 implements OnClickListener {
        private final /* synthetic */ boolean[] val$canceled;

        AnonymousClass19(boolean[] zArr) {
            this.val$canceled = zArr;
        }

        public void onClick(DialogInterface dialog, int which) {
            this.val$canceled[0] = true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SettingsFragment.1 */
    class C07851 implements OnPreferenceClickListener {
        C07851() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsFragment.this.confirmLogout();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SettingsFragment.2 */
    class C07882 implements OnPreferenceClickListener {
        C07882() {
        }

        public boolean onPreferenceClick(Preference preference) {
            Intent intent = new Intent(SettingsFragment.this.getActivity(), WelcomeActivity.class);
            intent.setAction("syncsettings");
            SettingsFragment.this.startActivityForResult(intent, SettingsFragment.REQUEST_SYNC_SETTINGS);
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SettingsFragment.3 */
    class C07893 implements OnPreferenceClickListener {
        C07893() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsFragment.this.startActivity(new Intent(SettingsFragment.this.getActivity(), NewsfeedBanlistActivity.class));
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SettingsFragment.4 */
    class C07904 implements OnPreferenceClickListener {
        C07904() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsFragment.this.startActivity(new Intent(SettingsFragment.this.getActivity(), ChangePasswordActivity.class));
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SettingsFragment.5 */
    class C07915 implements OnPreferenceClickListener {
        C07915() {
        }

        public boolean onPreferenceClick(Preference preference) {
            VKApplication.context.getSharedPreferences(null, 0).edit().remove("c2dm_regID").commit();
            C2DMessaging.unregister(VKApplication.context);
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SettingsFragment.6 */
    class C07926 implements OnPreferenceClickListener {
        C07926() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsFragment.this.getActivity().stopService(new Intent(SettingsFragment.this.getActivity(), LongPollService.class));
            C2DM.start();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SettingsFragment.7 */
    class C07937 implements OnPreferenceClickListener {
        C07937() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsFragment.this.startActivity(new Intent(SettingsFragment.this.getActivity(), SettingsAdvancedActivity.class));
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SettingsFragment.8 */
    class C07948 implements OnPreferenceClickListener {
        C07948() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsFragment.this.startActivity(new Intent(SettingsFragment.this.getActivity(), BlacklistActivity.class));
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SettingsFragment.9 */
    class C07959 implements OnPreferenceClickListener {
        C07959() {
        }

        public boolean onPreferenceClick(Preference preference) {
            MainActivity.showAbout(SettingsFragment.this.getActivity());
            return true;
        }
    }

    public void onCreate(Bundle b) {
        boolean z;
        super.onCreate(b);
        this.prefs = PreferenceManager.getDefaultSharedPreferences(VKApplication.context);
        addPreferencesFromResource(C0436R.xml.preferences);
        Preference pref = findPreference("logOut");
        pref.setSummary(VKApplication.context.getSharedPreferences(null, 0).getString("username", ACRAConstants.DEFAULT_STRING_VALUE));
        pref.setOnPreferenceClickListener(new C07851());
        pref = findPreference("sync");
        updateSyncLabel(Auth.getCurrentSyncOption(getActivity()));
        pref.setOnPreferenceClickListener(new C07882());
        findPreference("newsBanned").setOnPreferenceClickListener(new C07893());
        findPreference("changePassword").setOnPreferenceClickListener(new C07904());
        pref = findPreference("stopc2dm");
        if (pref != null) {
            pref.setOnPreferenceClickListener(new C07915());
        }
        pref = findPreference("startc2dm");
        if (pref != null) {
            pref.setOnPreferenceClickListener(new C07926());
        }
        findPreference("advanced").setOnPreferenceClickListener(new C07937());
        findPreference("blacklist").setOnPreferenceClickListener(new C07948());
        findPreference("about").setOnPreferenceClickListener(new C07959());
        pref = findPreference("restart");
        if (pref != null) {
            pref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    System.exit(0);
                    return true;
                }
            });
        }
        pref = findPreference("friendsOrderNew");
        if (pref != null) {
            pref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    ((DialogPreference) preference).getDialog().getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
                    return false;
                }
            });
        }
        pref = findPreference("notifyTypes");
        if (pref != null) {
            pref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    ((DialogPreference) preference).getDialog().getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
                    return false;
                }
            });
        }
        pref = findPreference("notifyRingtone");
        if (pref != null) {
            pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    SettingsFragment.this.updateRingtoneName((String) newValue);
                    return true;
                }
            });
        }
        pref = findPreference("useHTTPS");
        if (getActivity().getSharedPreferences(null, 0).getBoolean("forceHTTPS", false)) {
            z = false;
        } else {
            z = true;
        }
        pref.setEnabled(z);
        if (getActivity().getSharedPreferences(null, 0).getBoolean("forceHTTPS", false)) {
            pref.setSummary(C0436R.string.https_only_summary);
        }
        this.cancelDndPref = findPreference("dnd_cancel");
        pref = findPreference("dnd_hour");
        this.dnd1Pref = pref;
        pref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SettingsFragment.this.showDndDialog();
                return true;
            }
        });
        this.cancelDndPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SettingsFragment.this.prefs.edit().putLong("dnd_end", 0).commit();
                PreferenceCategory cat = (PreferenceCategory) SettingsFragment.this.findPreference("cat_notify");
                cat.removePreference(SettingsFragment.this.cancelDndPref);
                cat.addPreference(SettingsFragment.this.dnd1Pref);
                return true;
            }
        });
        if (this.prefs.getLong("dnd_end", 0) > System.currentTimeMillis()) {
            this.cancelDndPref.setSummary(getResources().getString(C0436R.string.sett_dnd_desc, new Object[]{Global.langDate(getResources(), (int) (t / 1000))}));
            ((PreferenceCategory) findPreference("cat_notify")).removePreference(this.dnd1Pref);
        } else {
            ((PreferenceCategory) findPreference("cat_notify")).removePreference(this.cancelDndPref);
        }
        updateRingtoneName(null);
    }

    private void showDndDialog() {
        new Builder(getActivity()).setItems(C0436R.array.sett_dnd_options, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                long t = System.currentTimeMillis();
                switch (which) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                        SettingsFragment.this.setDnd(1800000 + t);
                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                        SettingsFragment.this.setDnd(3600000 + t);
                    case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                        SettingsFragment.this.setDnd(7200000 + t);
                    case Group.ADMIN_LEVEL_ADMIN /*3*/:
                        SettingsFragment.this.setDnd(28800000 + t);
                    case UserListView.TYPE_FAVE /*4*/:
                        SettingsFragment.this.showTimePicker();
                    default:
                }
            }
        }).setTitle(C0436R.string.chat_dnd).show();
    }

    private void showTimePicker() {
        boolean[] canceled = new boolean[1];
        Date date = new Date();
        TimePickerDialog dlg = new TimePickerDialog(getActivity(), new AnonymousClass17(canceled), date.getHours(), date.getMinutes(), true);
        dlg.setButton(-1, getString(C0436R.string.ok), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dlg.setButton(-2, getString(C0436R.string.cancel), new AnonymousClass19(canceled));
        dlg.show();
        dlg.getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
    }

    private void setDnd(long t) {
        this.prefs.edit().putLong("dnd_end", t).commit();
        PreferenceCategory cat = (PreferenceCategory) findPreference("cat_notify");
        if (cat.findPreference("dnd_cancel") == null) {
            cat.addPreference(this.cancelDndPref);
        }
        this.cancelDndPref.setSummary(getResources().getString(C0436R.string.sett_dnd_desc, new Object[]{Global.langDate(getResources(), (int) (t / 1000))}));
        cat.removePreference(this.dnd1Pref);
    }

    private void updateRingtoneName(String uri) {
        Preference pref = findPreference("notifyRingtone");
        String rt = uri != null ? uri : PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("notifyRingtone", "content://settings/system/notification_sound");
        Ringtone ringtone = RingtoneManager.getRingtone(getActivity(), Uri.parse(rt));
        String name = "Unknown";
        if (ringtone != null || rt.length() == 0) {
            name = rt.length() > 0 ? ringtone.getTitle(getActivity()) : getString(C0436R.string.sett_no_sound);
        }
        pref.setSummary(name);
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        ((SherlockFragmentActivity) act).getSupportActionBar().setDisplayShowTitleEnabled(true);
        ((SherlockFragmentActivity) act).getSupportActionBar().setNavigationMode(0);
        act.setTitle(C0436R.string.menu_settings);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.setBackgroundColor(-1);
        return view;
    }

    private void confirmLogout() {
        new Builder(getActivity()).setMessage(C0436R.string.log_out_warning).setTitle(C0436R.string.log_out).setPositiveButton(C0436R.string.yes, new OnClickListener() {

            /* renamed from: com.vkontakte.android.fragments.SettingsFragment.20.1 */
            class C07871 implements Runnable {
                private final /* synthetic */ ProgressDialog val$dlg;

                /* renamed from: com.vkontakte.android.fragments.SettingsFragment.20.1.1 */
                class C07861 implements Runnable {
                    private final /* synthetic */ ProgressDialog val$dlg;

                    C07861(ProgressDialog progressDialog) {
                        this.val$dlg = progressDialog;
                    }

                    public void run() {
                        this.val$dlg.dismiss();
                        ((MainActivity) SettingsFragment.this.getActivity()).restartAfterLogout();
                    }
                }

                C07871(ProgressDialog progressDialog) {
                    this.val$dlg = progressDialog;
                }

                public void run() {
                    LongPollService.logOut(true);
                    SettingsFragment.this.getActivity().runOnUiThread(new C07861(this.val$dlg));
                }
            }

            public void onClick(DialogInterface dialog, int which) {
                ProgressDialog dlg = new ProgressDialog(SettingsFragment.this.getActivity());
                dlg.setMessage(SettingsFragment.this.getResources().getString(C0436R.string.loading));
                dlg.show();
                dlg.getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
                dlg.setCancelable(false);
                new Thread(new C07871(dlg)).start();
            }
        }).setNegativeButton(C0436R.string.no, null).show();
    }

    private void updateSyncLabel(int syncSetting) {
        Log.m528i("vk", "Update sync label " + syncSetting);
        Preference pref = findPreference("sync");
        switch (syncSetting) {
            case BoardTopicsFragment.ORDER_UPDATED_ASC /*-1*/:
                pref.setEnabled(false);
                pref.setSummary(C0436R.string.sync_not_supported);
            case ValidationActivity.VRESULT_NONE /*0*/:
                pref.setSummary(C0436R.string.sync_all);
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                pref.setSummary(C0436R.string.sync_existing);
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                pref.setSummary(C0436R.string.sync_off);
            default:
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SYNC_SETTINGS && resultCode == -1) {
            updateSyncLabel(data.getIntExtra("option", 0));
        }
    }
}
