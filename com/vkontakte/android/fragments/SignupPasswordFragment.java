package com.vkontakte.android.fragments;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.actionbarsherlock.app.SherlockFragment;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.ui.Fonts;

public class SignupPasswordFragment extends SherlockFragment {
    private OnClickListener btnClickListener;
    private String explainText;
    private long initTime;
    private View view;

    /* renamed from: com.vkontakte.android.fragments.SignupPasswordFragment.1 */
    class C08021 implements OnEditorActionListener {
        C08021() {
        }

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (SignupPasswordFragment.this.btnClickListener != null) {
                SignupPasswordFragment.this.btnClickListener.onClick(v);
            }
            return false;
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        if (b != null) {
            this.initTime = b.getLong("init_time");
        } else {
            this.initTime = System.currentTimeMillis();
        }
    }

    public void setOnNextClickListener(OnClickListener l) {
        this.btnClickListener = l;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(C0436R.layout.signup_password, null);
        ((TextView) this.view.findViewById(C0436R.id.signup_pass_text)).setTypeface(Fonts.getRobotoLight());
        if (this.explainText != null) {
            ((TextView) this.view.findViewById(C0436R.id.signup_pass_text)).setText(this.explainText);
        }
        this.view.findViewById(C0436R.id.signup_btn_next).setOnClickListener(this.btnClickListener);
        this.view.findViewById(C0436R.id.signup_pass_edit).requestFocus();
        ((EditText) this.view.findViewById(C0436R.id.signup_pass_edit)).setOnEditorActionListener(new C08021());
        return this.view;
    }

    public void setExplainText(String txt) {
        this.explainText = txt;
        if (this.view != null) {
            ((TextView) this.view.findViewById(C0436R.id.signup_pass_text)).setText(txt);
        }
    }

    public String getPassword() {
        return ((TextView) this.view.findViewById(C0436R.id.signup_pass_edit)).getText().toString();
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.view = null;
    }
}
