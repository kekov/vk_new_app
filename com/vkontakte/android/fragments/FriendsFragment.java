package com.vkontakte.android.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip.BadgeTabProvider;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.ActivityUtils;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.FriendsGet;
import com.vkontakte.android.api.FriendsGet.Callback;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.fragments.FriendListFragment.RefreshListener;
import com.vkontakte.android.fragments.FriendListFragment.SelectionListener;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.PhotoView;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import org.acra.ACRAConstants;

public class FriendsFragment extends ContainerFragment {
    private LinearLayout contentView;
    private FrameLayout contentWrap;
    private APIRequest currentReq;
    private ErrorView errorView;
    private boolean firstUpdate;
    private ArrayList<UserProfile> friends;
    private FriendListFragment friendsView;
    private ArrayList<Object> lists;
    private ArrayList<UserProfile> mutual;
    private FriendListFragment mutualView;
    private int onlineCount;
    private FriendListFragment onlineView;
    private ViewPager pager;
    private int prev;
    private ProgressBar progress;
    private BroadcastReceiver receiver;
    private RefreshListener refreshListener;
    private boolean reqsLoaded;
    private FriendRequestsFragment requestsView;
    private SearchView searchView;
    private boolean searching;
    private int selTab;
    private boolean showOnline;
    private boolean showRequests;
    private PagerSlidingTabStrip tabbar;
    private ArrayList<CharSequence> titles;
    private int uid;

    /* renamed from: com.vkontakte.android.fragments.FriendsFragment.1 */
    class C06961 extends BroadcastReceiver {
        C06961() {
        }

        public void onReceive(Context context, Intent intent) {
            if (FriendsFragment.this.isAdded()) {
                if (Friends.ACTION_FRIEND_LIST_CHANGED.equals(intent.getAction())) {
                    FriendsFragment.this.update();
                }
                if (Friends.ACTION_FRIEND_REQUESTS_CHANGED.equals(intent.getAction())) {
                    FriendsFragment.this.updateTabs();
                }
                if (LongPollService.ACTION_USER_PRESENCE.equals(intent.getAction())) {
                    int u = intent.getIntExtra("uid", 0);
                    Iterator it = FriendsFragment.this.friends.iterator();
                    while (it.hasNext()) {
                        UserProfile user = (UserProfile) it.next();
                        if (user.uid == u) {
                            user.online = intent.getIntExtra("online", 0);
                            break;
                        }
                    }
                    FriendsFragment.this.onlineCount = 0;
                    it = FriendsFragment.this.friends.iterator();
                    while (it.hasNext()) {
                        if (((UserProfile) it.next()).online > 0) {
                            FriendsFragment friendsFragment = FriendsFragment.this;
                            friendsFragment.onlineCount = friendsFragment.onlineCount + 1;
                        }
                    }
                    FriendsFragment.this.onlineView.updateOnline();
                    FriendsFragment.this.updateTabs();
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendsFragment.6 */
    class C06976 implements Runnable {
        C06976() {
        }

        public void run() {
            try {
                FriendsFragment.this.searchView.setMaxWidth(FriendsFragment.this.getActivity().getWindow().getDecorView().getWidth() - ((int) Math.round(((double) FriendsFragment.this.getSherlockActivity().getSupportActionBar().getHeight()) * 2.2d)));
            } catch (Exception e) {
                FriendsFragment.this.searchView.post(this);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendsFragment.7 */
    class C06987 implements OnClickListener {
        C06987() {
        }

        public void onClick(View v) {
            FriendsFragment.this.update();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendsFragment.9 */
    class C07019 implements Runnable {

        /* renamed from: com.vkontakte.android.fragments.FriendsFragment.9.1 */
        class C07001 implements Runnable {

            /* renamed from: com.vkontakte.android.fragments.FriendsFragment.9.1.1 */
            class C06991 implements Runnable {
                private final /* synthetic */ Handler val$h;

                C06991(Handler handler) {
                    this.val$h = handler;
                }

                public void run() {
                    if (FriendsFragment.this.progress.getVisibility() == 0) {
                        Global.showViewAnimated(FriendsFragment.this.contentView, true, PhotoView.THUMB_ANIM_DURATION);
                        Global.showViewAnimated(FriendsFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                    }
                    this.val$h.removeCallbacks(this);
                }
            }

            C07001() {
            }

            public void run() {
                FriendsFragment.this.friends.clear();
                Friends.getFriends(FriendsFragment.this.friends);
                Handler h = new Handler();
                Runnable r = new C06991(h);
                FriendsFragment.this.friendsView.runAfterInit(r);
                FriendListFragment access$11 = FriendsFragment.this.friendsView;
                ArrayList access$2 = FriendsFragment.this.friends;
                boolean z = FriendsFragment.this.uid == 0 || FriendsFragment.this.uid == Global.uid;
                access$11.setData(access$2, z, true);
                FriendsFragment.this.onlineView.setShowOnline(true);
                FriendsFragment.this.onlineView.setData(FriendsFragment.this.friends, false, false);
                FriendsFragment.this.onlineCount = 0;
                Iterator it = FriendsFragment.this.friends.iterator();
                while (it.hasNext()) {
                    if (((UserProfile) it.next()).online > 0) {
                        FriendsFragment access$0 = FriendsFragment.this;
                        access$0.onlineCount = access$0.onlineCount + 1;
                    }
                }
                FriendsFragment.this.updateTabs();
                h.postDelayed(r, 500);
            }
        }

        C07019() {
        }

        public void run() {
            if (FriendsFragment.this.getArguments().getBoolean("_from_menu") && FriendsFragment.this.friends.size() == 0) {
                try {
                    Thread.sleep(180);
                } catch (Exception e) {
                }
            }
            if (FriendsFragment.this.getActivity() == null) {
                Log.e("vk", "NO ACTIVITY!!!");
                FriendsFragment.this.friends.clear();
                Friends.getFriends(FriendsFragment.this.friends);
                return;
            }
            FriendsFragment.this.getActivity().runOnUiThread(new C07001());
        }
    }

    private class NavAdapter<T> extends ArrayAdapter<T> {
        public NavAdapter(Context context, int textViewResourceId, T[] objects) {
            super(context, textViewResourceId, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (getItem(position) != null) {
                return super.getView(position, convertView, parent);
            }
            if (convertView == null) {
                return View.inflate(FriendsFragment.this.getSherlockActivity().getSupportActionBar().getThemedContext(), C0436R.layout.spinner_item_loading, null);
            }
            return convertView;
        }

        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            if (getItem(position) != null) {
                return super.getDropDownView(position, convertView, parent);
            }
            if (convertView == null) {
                return View.inflate(FriendsFragment.this.getSherlockActivity().getSupportActionBar().getThemedContext(), C0436R.layout.spinner_item_loading, null);
            }
            return convertView;
        }

        public int getItemViewType(int pos) {
            return getItem(pos) == null ? 1 : 0;
        }

        public int getViewTypeCount() {
            return 2;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendsFragment.2 */
    class C14742 implements RefreshListener {
        C14742() {
        }

        public void onRefresh() {
            FriendsFragment.this.update();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendsFragment.3 */
    class C14753 extends ViewPager {
        C14753(Context $anonymous0) {
            super($anonymous0);
        }

        public boolean onInterceptTouchEvent(MotionEvent ev) {
            if (FriendsFragment.this.searching) {
                return false;
            }
            return super.onInterceptTouchEvent(ev);
        }

        public boolean onTouchEvent(MotionEvent ev) {
            if (FriendsFragment.this.searching) {
                return false;
            }
            return super.onTouchEvent(ev);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendsFragment.4 */
    class C14764 implements OnPageChangeListener {
        C14764() {
        }

        public void onPageSelected(int pos) {
            boolean z;
            FriendsFragment.this.selTab = pos;
            if (FriendsFragment.this.friends.size() <= 0 || !FriendsFragment.this.showOnline) {
                z = true;
            } else {
                z = true;
            }
            if (pos == z && FriendsFragment.this.uid == Global.uid && !FriendsFragment.this.reqsLoaded) {
                FriendsFragment.this.requestsView.loadData();
                FriendsFragment.this.reqsLoaded = true;
            }
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendsFragment.5 */
    class C14775 implements OnQueryTextListener {
        C14775() {
        }

        public boolean onQueryTextSubmit(String query) {
            View focus = FriendsFragment.this.getActivity().getCurrentFocus();
            if (focus != null) {
                ((InputMethodManager) FriendsFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(focus.getWindowToken(), 0);
                focus.clearFocus();
            }
            return false;
        }

        public boolean onQueryTextChange(String newText) {
            boolean ns;
            if (newText == null || newText.length() <= 0) {
                ns = false;
            } else {
                ns = true;
            }
            if (ns != FriendsFragment.this.searching) {
                FriendsFragment.this.searching = ns;
                if (FriendsFragment.this.searching) {
                    FriendsFragment.this.tabbar.setVisibility(8);
                    FriendsFragment.this.pager.setCurrentItem(0, false);
                    FriendsFragment.this.tabbar.pageListener.onPageScrolled(0, 0.0f, 0);
                    FriendsFragment.this.tabbar.pageListener.onPageSelected(0);
                } else {
                    FriendsFragment.this.updateTabs();
                }
            }
            FriendsFragment.this.friendsView.updateFilter(newText);
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendsFragment.8 */
    class C14788 implements SelectionListener {
        C14788() {
        }

        public void onItemSelected(UserProfile p) {
            Intent res = new Intent();
            res.putExtra("user", p);
            FriendsFragment.this.getActivity().setResult(-1, res);
            FriendsFragment.this.getActivity().finish();
        }
    }

    private class FriendsPagerAdapter extends FragmentPagerAdapter implements BadgeTabProvider {
        public FriendsPagerAdapter() {
            super(FriendsFragment.this.getInnerFragmentManager());
        }

        public int getCount() {
            int cnt = 1;
            if (FriendsFragment.this.friends.size() > 0 && FriendsFragment.this.showOnline) {
                cnt = 1 + 1;
            }
            if ((FriendsFragment.this.uid != Global.uid || !FriendsFragment.this.showRequests) && FriendsFragment.this.mutual.size() <= 0) {
                return cnt;
            }
            return cnt + 1;
        }

        public CharSequence getPageTitle(int pos) {
            return pos < FriendsFragment.this.titles.size() ? (CharSequence) FriendsFragment.this.titles.get(pos) : ACRAConstants.DEFAULT_STRING_VALUE;
        }

        public String getPageBadgeValue(int position) {
            int i = (FriendsFragment.this.friends.size() <= 0 || !FriendsFragment.this.showOnline) ? 1 : 2;
            if (position == i && Global.uid == FriendsFragment.this.uid && LongPollService.numFriendRequests > 0) {
                int cnt = LongPollService.numFriendRequests;
                if (cnt < LocationStatusCodes.GEOFENCE_NOT_AVAILABLE) {
                    return new StringBuilder(String.valueOf(cnt)).toString();
                }
                if (cnt >= LocationStatusCodes.GEOFENCE_NOT_AVAILABLE && cnt < 1000000) {
                    return (cnt / LocationStatusCodes.GEOFENCE_NOT_AVAILABLE) + "K";
                }
                if (cnt >= 1000000) {
                    return String.format("%.1fM", new Object[]{Float.valueOf(((float) cnt) / 1000000.0f)});
                }
            }
            return null;
        }

        public Fragment getItem(int position) {
            switch (position) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return FriendsFragment.this.friendsView;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    if (FriendsFragment.this.friends.size() == 0) {
                        return FriendsFragment.this.uid == Global.uid ? FriendsFragment.this.requestsView : FriendsFragment.this.mutualView;
                    } else {
                        return FriendsFragment.this.onlineView;
                    }
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return FriendsFragment.this.uid == Global.uid ? FriendsFragment.this.requestsView : FriendsFragment.this.mutualView;
                default:
                    return null;
            }
        }

        public int getItemPosition(Object object) {
            if (object == FriendsFragment.this.friendsView) {
                return 0;
            }
            if (object == FriendsFragment.this.onlineView) {
                return 1;
            }
            if (object == FriendsFragment.this.mutualView) {
                return 2;
            }
            if (object != FriendsFragment.this.requestsView) {
                return -2;
            }
            if (FriendsFragment.this.friends.size() <= 0) {
                return 1;
            }
            return 2;
        }

        public long getItemId(int position) {
            return (long) getItem(position).hashCode();
        }
    }

    public FriendsFragment() {
        this.uid = Global.uid;
        this.selTab = 0;
        this.reqsLoaded = false;
        this.friends = new ArrayList();
        this.mutual = new ArrayList();
        this.titles = new ArrayList();
        this.firstUpdate = true;
        this.showOnline = true;
        this.lists = new ArrayList();
        this.receiver = new C06961();
        this.refreshListener = new C14742();
        this.searching = false;
    }

    public void onAttach(Activity act) {
        boolean z;
        boolean z2 = true;
        super.onAttach(act);
        this.uid = getArguments().getInt("uid", Global.uid);
        ActivityUtils.setBeamLink(act, "friends?id=" + this.uid);
        SherlockFragmentActivity sa = getSherlockActivity();
        sa.getSupportActionBar().setNavigationMode(0);
        sa.getSupportActionBar().setDisplayShowTitleEnabled(true);
        if (getArguments().containsKey(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)) {
            sa.setTitle(getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
        } else {
            sa.setTitle(C0436R.string.friends);
        }
        this.tabbar = new PagerSlidingTabStrip(act);
        this.tabbar.setBackgroundResource(C0436R.color.tab_bg);
        this.tabbar.setIndicatorColorResource(C0436R.color.tab_indicator);
        if (getArguments().getBoolean("no_online")) {
            z = false;
        } else {
            z = true;
        }
        this.showOnline = z;
        this.friendsView = new FriendListFragment();
        this.contentView = new LinearLayout(act);
        this.contentView.setOrientation(1);
        this.contentView.addView(this.tabbar, new LayoutParams(-1, Global.scale(GalleryPickerFooterView.SIZE)));
        this.pager = new C14753(act);
        this.pager.setOffscreenPageLimit(3);
        this.pager.setId(C0436R.id.inner_fragment_wrapper);
        this.contentView.addView(this.pager, new LayoutParams(-1, -1));
        this.pager.setAdapter(new FriendsPagerAdapter());
        this.tabbar.setViewPager(this.pager);
        updateTabs();
        this.onlineView = new FriendListFragment();
        if (!getArguments().getBoolean("select")) {
            if (this.uid != Global.uid) {
                this.mutualView = new FriendListFragment();
                this.mutualView.setUserSections(false);
            } else {
                this.requestsView = new FriendRequestsFragment();
                Bundle args = new Bundle();
                args.putBoolean("tab", true);
                this.requestsView.setArguments(args);
            }
        }
        this.tabbar.setOnPageChangeListener(new C14764());
        this.searchView = new SearchView(sa.getSupportActionBar().getThemedContext());
        this.searchView.setQueryHint(getResources().getString(C0436R.string.search));
        try {
            Field searchField = SearchView.class.getDeclaredField("mSearchButton");
            searchField.setAccessible(true);
            ((ImageView) searchField.get(this.searchView)).setImageResource(C0436R.drawable.ic_ab_search);
            searchField = SearchView.class.getDeclaredField("mSearchPlate");
            searchField.setAccessible(true);
            ((TextView) ((LinearLayout) searchField.get(this.searchView)).getChildAt(0)).setHintTextColor(-2130706433);
        } catch (Exception e) {
        }
        this.searchView.setOnQueryTextListener(new C14775());
        if (this.uid == Global.uid) {
            this.searchView.post(new C06976());
        }
        this.progress = new ProgressBar(act);
        this.contentWrap = new FrameLayout(act);
        this.contentWrap.addView(this.contentView);
        this.contentWrap.setBackgroundColor(-1);
        this.contentWrap.addView(this.progress, new FrameLayout.LayoutParams(Global.scale(40.0f), Global.scale(40.0f), 17));
        this.errorView = (ErrorView) View.inflate(act, C0436R.layout.error, null);
        this.errorView.setVisibility(8);
        this.contentWrap.addView(this.errorView);
        this.errorView.setOnRetryListener(new C06987());
        setHasOptionsMenu(true);
        update();
        if (getArguments().getBoolean("select")) {
            SelectionListener sl = new C14788();
            this.friendsView.setSelectionListener(sl);
            this.onlineView.setSelectionListener(sl);
        }
        if (this.uid != Global.uid) {
            this.friendsView.setRefreshListener(this.refreshListener);
            this.onlineView.setRefreshListener(this.refreshListener);
            this.mutualView.setRefreshListener(this.refreshListener);
        }
        this.friendsView.setArguments(getArguments());
        if (LongPollService.numFriendRequests <= 0 || getArguments().getBoolean("select")) {
            z2 = false;
        }
        this.showRequests = z2;
        updateTabs();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem search = menu.add((int) C0436R.string.search);
        search.setShowAsAction(2);
        search.setActionView(this.searchView);
        if (this.uid == Global.uid && !getArguments().getBoolean("select")) {
            MenuItem add = menu.add(0, (int) C0436R.id.add, 0, (int) C0436R.string.add);
            add.setShowAsAction(2);
            add.setIcon((int) C0436R.drawable.ic_ab_add);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0436R.id.add) {
            Navigate.to("SuggestionsFriendsFragment", new Bundle(), getActivity());
        }
        return true;
    }

    private void updateTabs() {
        if (getActivity() != null) {
            int i;
            this.titles.clear();
            if (this.friends.size() <= 0 || !this.showOnline) {
                this.titles.add(getString(C0436R.string.friends));
            } else {
                this.titles.add(Global.langPlural(C0436R.array.friends_tab_all, this.friends.size(), getResources()));
                this.titles.add(Global.langPlural(C0436R.array.friends_tab_online, this.onlineCount, getResources()));
            }
            if (this.mutual.size() > 0) {
                this.titles.add(Global.langPlural(C0436R.array.friends_mutual, this.mutual.size(), getResources()));
            }
            if (this.showRequests && this.uid == Global.uid) {
                this.titles.add(getString(C0436R.string.friend_requests));
            }
            this.pager.getAdapter().notifyDataSetChanged();
            this.tabbar.notifyDataSetChanged();
            this.tabbar.postInvalidate();
            PagerSlidingTabStrip pagerSlidingTabStrip = this.tabbar;
            if (this.titles.size() <= 1 || this.searching) {
                i = 8;
            } else {
                i = 0;
            }
            pagerSlidingTabStrip.setVisibility(i);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentWrap;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        IntentFilter filter = new IntentFilter();
        if (this.uid == 0 || this.uid == Global.uid) {
            filter.addAction(Friends.ACTION_FRIEND_LIST_CHANGED);
            filter.addAction(Friends.ACTION_FRIEND_REQUESTS_CHANGED);
        }
        filter.addAction(LongPollService.ACTION_USER_PRESENCE);
        VKApplication.context.registerReceiver(this.receiver, filter);
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
        if (this.currentReq != null) {
            this.currentReq.cancel();
        }
    }

    private void update() {
        if (this.uid == Global.uid) {
            if (this.friends.size() == 0) {
                this.contentView.setVisibility(4);
                this.errorView.setVisibility(8);
                this.progress.setVisibility(0);
            }
            new Thread(new C07019()).start();
            return;
        }
        if (this.friends.size() == 0) {
            this.contentView.setVisibility(8);
            this.errorView.setVisibility(8);
            this.progress.setVisibility(0);
        }
        this.currentReq = new FriendsGet(this.uid, false).setCallback(new Callback() {
            public void success(ArrayList<UserProfile> list) {
                FriendsFragment.this.friends.clear();
                FriendsFragment.this.friends.addAll(list);
                FriendsFragment.this.friendsView.setData(FriendsFragment.this.friends, false, false);
                FriendsFragment.this.onlineView.setShowOnline(true);
                FriendsFragment.this.onlineView.setData(FriendsFragment.this.friends, false, false);
                FriendsFragment.this.onlineCount = 0;
                Iterator it = FriendsFragment.this.friends.iterator();
                while (it.hasNext()) {
                    if (((UserProfile) it.next()).online > 0) {
                        FriendsFragment friendsFragment = FriendsFragment.this;
                        friendsFragment.onlineCount = friendsFragment.onlineCount + 1;
                    }
                }
                FriendsFragment.this.mutual.clear();
                Friends.intersect(FriendsFragment.this.friends, FriendsFragment.this.mutual);
                FriendsFragment.this.mutualView.setData(FriendsFragment.this.mutual, false, false);
                FriendsFragment.this.updateTabs();
                if (FriendsFragment.this.progress.getVisibility() == 0) {
                    Global.showViewAnimated(FriendsFragment.this.contentView, true, PhotoView.THUMB_ANIM_DURATION);
                    Global.showViewAnimated(FriendsFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                }
                if (FriendsFragment.this.getArguments().getBoolean("mutual", false) && FriendsFragment.this.firstUpdate) {
                    FriendsFragment.this.pager.setCurrentItem(2, false);
                }
                FriendsFragment.this.firstUpdate = false;
                FriendsFragment.this.currentReq = null;
            }

            public void fail(int ecode, String emsg) {
                FriendsFragment.this.errorView.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(FriendsFragment.this.errorView, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(FriendsFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                FriendsFragment.this.currentReq = null;
            }
        }).exec(this.contentWrap);
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }
}
