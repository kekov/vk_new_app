package com.vkontakte.android.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.ExploreByTouchHelper;
import android.text.ClipboardManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.ActivityUtils;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageAttachment;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.NewsItemView;
import com.vkontakte.android.PollAttachView;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.ZhukovLayout;
import com.vkontakte.android.api.BoardAddComment;
import com.vkontakte.android.api.BoardComment;
import com.vkontakte.android.api.BoardDeleteComment;
import com.vkontakte.android.api.BoardGetComments;
import com.vkontakte.android.api.BoardGetComments.Callback;
import com.vkontakte.android.api.PollOption;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.ExtendedListener;
import com.vkontakte.android.ui.PaginationView;
import com.vkontakte.android.ui.PaginationView.Listener;
import com.vkontakte.android.ui.RefreshableListView;
import com.vkontakte.android.ui.RefreshableListView.OnRefreshListener;
import com.vkontakte.android.ui.WriteBar;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.acra.ACRAConstants;

public class BoardTopicViewFragment extends SherlockFragment implements ExtendedListener, OnItemClickListener, OnItemLongClickListener, OnClickListener, Listener, OnRefreshListener {
    private CommentsAdapter adapter;
    private ProgressBar bigProgress;
    private WriteBar commentBar;
    private ArrayList<BoardComment> comments;
    private LinearLayout contentView;
    private TextView createHint;
    private int currentPage;
    private boolean dataLoading;
    private boolean firstLoad;
    private View focusable;
    protected FrameLayout footerView;
    protected FrameLayout headerView;
    private boolean ignoreNextScroll;
    private boolean ignoreScrollEvents;
    private ListImageLoaderWrapper imgLoader;
    private int lastUpdated;
    private RefreshableListView list;
    private APIRequest loadUpReq;
    private boolean moreAvailable;
    protected TextView noNewsView;
    private PaginationView pagination;
    private LinearLayout pollWrap;
    private boolean preloadOnReady;
    private boolean preloadUpOnReady;
    private ArrayList<BoardComment> preloadedComments;
    private ArrayList<BoardComment> preloadedUpComments;
    private boolean preloading;
    private boolean preloadingUp;
    private APIRequest refreshReq;
    private boolean resetScroll;
    private boolean sendingComment;
    private int startOffset;
    private ArrayList<View> visibleViews;
    private FrameLayout wrapView;

    /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.10 */
    class AnonymousClass10 implements DialogInterface.OnClickListener {
        private final /* synthetic */ EditText val$edit;

        AnonymousClass10(EditText editText) {
            this.val$edit = editText;
        }

        public void onClick(DialogInterface dialog, int which) {
            try {
                int n = Integer.parseInt(this.val$edit.getText().toString());
                if (n > 0 && n <= BoardTopicViewFragment.this.pagination.getPageCount()) {
                    BoardTopicViewFragment.this.onPageSelected(n);
                }
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.1 */
    class C05921 implements OnClickListener {
        C05921() {
        }

        public void onClick(View v) {
            if (BoardTopicViewFragment.this.commentBar.isUploading()) {
                BoardTopicViewFragment.this.waitAndSendComment();
            } else {
                BoardTopicViewFragment.this.sendComment();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.2 */
    class C05932 implements Runnable {
        private final /* synthetic */ ProgressDialog val$progress;

        C05932(ProgressDialog progressDialog) {
            this.val$progress = progressDialog;
        }

        public void run() {
            this.val$progress.dismiss();
            BoardTopicViewFragment.this.sendComment();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.3 */
    class C05943 implements Runnable {
        private final /* synthetic */ ProgressDialog val$progress;

        C05943(ProgressDialog progressDialog) {
            this.val$progress = progressDialog;
        }

        public void run() {
            this.val$progress.dismiss();
            Toast.makeText(BoardTopicViewFragment.this.getActivity(), C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.6 */
    class C05976 implements Runnable {
        C05976() {
        }

        public void run() {
            BoardTopicViewFragment.this.adapter.notifyDataSetChanged();
            BoardTopicViewFragment.this.imgLoader.updateImages();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.8 */
    class C06008 implements DialogInterface.OnClickListener {
        private final /* synthetic */ BoardComment val$comment;

        C06008(BoardComment boardComment) {
            this.val$comment = boardComment;
        }

        public void onClick(DialogInterface dialog, int which) {
            if (which < this.val$comment.linkUrls.size()) {
                BoardTopicViewFragment.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse((String) this.val$comment.linkUrls.get(which))));
                return;
            }
            switch (which - this.val$comment.linkUrls.size()) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    ((ClipboardManager) BoardTopicViewFragment.this.getActivity().getSystemService("clipboard")).setText(this.val$comment.text);
                    Toast.makeText(BoardTopicViewFragment.this.getActivity(), C0436R.string.text_copied, 0).show();
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    BoardTopicViewFragment.this.deleteComment(this.val$comment);
                default:
            }
        }
    }

    private class CommentsAdapter extends BaseAdapter {
        private CommentsAdapter() {
        }

        public int getCount() {
            return BoardTopicViewFragment.this.comments.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = View.inflate(BoardTopicViewFragment.this.getActivity(), C0436R.layout.wall_comment, null);
                BoardTopicViewFragment.this.visibleViews.add(view);
                view.findViewById(C0436R.id.poster_photo).setOnClickListener(BoardTopicViewFragment.this);
                view.findViewById(C0436R.id.post_likes).setVisibility(8);
            }
            view.setTag(Integer.valueOf(position));
            BoardComment c = (BoardComment) BoardTopicViewFragment.this.comments.get(position);
            ((TextView) view.findViewById(C0436R.id.poster_name_view)).setText(c.userName);
            ((TextView) view.findViewById(C0436R.id.post_view)).setText(c.text);
            ((TextView) view.findViewById(C0436R.id.post_info_view)).setText(Global.langDateRelative(c.time, BoardTopicViewFragment.this.getResources()));
            view.findViewById(C0436R.id.post_view).setVisibility(c.text.length() > 0 ? 0 : 8);
            if (BoardTopicViewFragment.this.imgLoader.isAlreadyLoaded(c.userPhoto)) {
                ((ImageView) view.findViewById(C0436R.id.poster_photo)).setImageBitmap(BoardTopicViewFragment.this.imgLoader.get(c.userPhoto));
            } else {
                ((ImageView) view.findViewById(C0436R.id.poster_photo)).setImageResource(C0436R.drawable.user_placeholder);
            }
            view.findViewById(C0436R.id.poster_photo).setTag(Integer.valueOf(c.uid));
            ViewGroup attview = (ViewGroup) view.findViewById(C0436R.id.post_attach_container);
            for (int i = 0; i < attview.getChildCount(); i++) {
                View av = attview.getChildAt(i);
                if (av.getTag() != null && (av.getTag() instanceof String)) {
                    Attachment.reuseView(av, av.getTag().toString());
                }
            }
            attview.removeAllViews();
            if (c.attachments.size() > 0) {
                NewsItemView.addAttachments(view, c.attachments, null, C0436R.id.post_attach_container);
                attview.setVisibility(0);
            } else {
                attview.setVisibility(8);
            }
            int idx = 0;
            Iterator it = c.attachments.iterator();
            while (it.hasNext()) {
                Attachment att = (Attachment) it.next();
                if (att instanceof ImageAttachment) {
                    String src = ((ImageAttachment) att).getImageURL();
                    if (BoardTopicViewFragment.this.imgLoader.isAlreadyLoaded(src)) {
                        ((ImageAttachment) att).setImage(((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(idx), BoardTopicViewFragment.this.imgLoader.get(src), true);
                    } else {
                        ((ImageAttachment) att).clearImage(((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(idx));
                    }
                }
                idx++;
            }
            if (position == 0) {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_top);
            } else if (position == getCount() - 1) {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_btm);
            } else {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_mid);
            }
            return view;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.4 */
    class C14354 implements Callback {

        /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.4.1 */
        class C05951 implements Runnable {
            C05951() {
            }

            public void run() {
                BoardTopicViewFragment.this.ignoreNextScroll = true;
                BoardTopicViewFragment.this.ignoreScrollEvents = false;
                BoardTopicViewFragment.this.pagination.setVisibility(0);
                BoardTopicViewFragment.this.pagination.hideNow();
            }
        }

        C14354() {
        }

        public void success(ArrayList<BoardComment> comments, int total, int pollID, String pollQuestion, int pollUserAnswer, ArrayList<PollOption> pollOptions) {
            Iterator it = comments.iterator();
            while (it.hasNext()) {
                BoardComment comment = (BoardComment) it.next();
                DisplayMetrics metrics = VKApplication.context.getResources().getDisplayMetrics();
                int tSize = Math.min(Math.min(metrics.widthPixels, metrics.heightPixels) - Global.scale(95.0f), 604);
                ZhukovLayout.processThumbs(tSize, (int) (((float) tSize) * 0.666f), comment.attachments);
            }
            if (BoardTopicViewFragment.this.preloading) {
                BoardTopicViewFragment.this.preloadedComments.addAll(comments);
            } else if (comments.size() > 20) {
                BoardTopicViewFragment.this.comments.addAll(comments.subList(0, 20));
                BoardTopicViewFragment.this.preloadedComments.addAll(comments.subList(20, comments.size()));
            } else {
                BoardTopicViewFragment.this.comments.addAll(comments);
            }
            BoardTopicViewFragment.this.preloading = false;
            if (BoardTopicViewFragment.this.preloadOnReady) {
                BoardTopicViewFragment.this.preloading = true;
                BoardTopicViewFragment.this.preloadOnReady = false;
                BoardTopicViewFragment.this.loadData();
            }
            BoardTopicViewFragment.this.list.setVisibility(0);
            BoardTopicViewFragment.this.updateList();
            if (BoardTopicViewFragment.this.resetScroll) {
                BoardTopicViewFragment.this.list.setSelection(BoardTopicViewFragment.this.list.getHeaderViewsCount());
                BoardTopicViewFragment.this.resetScroll = false;
            }
            BoardTopicViewFragment.this.moreAvailable = BoardTopicViewFragment.this.comments.size() + BoardTopicViewFragment.this.startOffset < total;
            BoardTopicViewFragment.this.bigProgress.setVisibility(8);
            BoardTopicViewFragment.this.footerView.getChildAt(0).setVisibility(BoardTopicViewFragment.this.moreAvailable ? 0 : 8);
            BoardTopicViewFragment.this.headerView.getChildAt(0).setVisibility(BoardTopicViewFragment.this.startOffset > 0 ? 0 : 8);
            int pages = (int) Math.ceil((double) (((float) total) / 20.0f));
            if (pages > 1) {
                BoardTopicViewFragment.this.pagination.setPageCount(pages);
                if (BoardTopicViewFragment.this.firstLoad) {
                    BoardTopicViewFragment.this.pagination.postDelayed(new C05951(), 300);
                    BoardTopicViewFragment.this.firstLoad = false;
                }
            }
            if (pollQuestion != null && BoardTopicViewFragment.this.pollWrap.getChildCount() == 0) {
                PollAttachView av = new PollAttachView(BoardTopicViewFragment.this.getActivity(), -BoardTopicViewFragment.this.getArguments().getInt("gid", 0), pollID);
                av.isBoard = true;
                av.onLoaded(pollQuestion, pollUserAnswer, (PollOption[]) pollOptions.toArray(new PollOption[0]), false);
                BoardTopicViewFragment.this.pollWrap.addView(av);
                BoardTopicViewFragment.this.pollWrap.setBackgroundResource(C0436R.drawable.bg_post);
            }
            BoardTopicViewFragment.this.refreshReq = null;
            BoardTopicViewFragment.this.dataLoading = false;
            BoardTopicViewFragment.this.lastUpdated = (int) (System.currentTimeMillis() / 1000);
            if (BoardTopicViewFragment.this.list.isRefreshing()) {
                BoardTopicViewFragment.this.list.refreshDone();
            }
        }

        public void fail(int ecode, String emsg) {
            BoardTopicViewFragment.this.refreshReq = null;
            BoardTopicViewFragment.this.dataLoading = false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.5 */
    class C14365 implements Callback {

        /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.5.1 */
        class C05961 implements Runnable {
            C05961() {
            }

            public void run() {
                BoardTopicViewFragment.this.pagination.hide();
            }
        }

        C14365() {
        }

        public void success(ArrayList<BoardComment> comments, int total, int pollID, String pollQuestion, int pollUserAnswer, ArrayList<PollOption> arrayList) {
            Iterator it = comments.iterator();
            while (it.hasNext()) {
                BoardComment comment = (BoardComment) it.next();
                DisplayMetrics metrics = VKApplication.context.getResources().getDisplayMetrics();
                int tSize = Math.min(Math.min(metrics.widthPixels, metrics.heightPixels) - Global.scale(95.0f), 604);
                ZhukovLayout.processThumbs(tSize, (int) (((float) tSize) * 0.666f), comment.attachments);
            }
            if (BoardTopicViewFragment.this.preloadingUp && comments.size() <= 20) {
                BoardTopicViewFragment.this.preloadedUpComments.addAll(comments);
            } else if (comments.size() > 20) {
                BoardTopicViewFragment.this.comments.addAll(0, comments.subList(20, comments.size()));
                BoardTopicViewFragment.this.preloadedUpComments.addAll(comments.subList(0, 20));
            } else {
                BoardTopicViewFragment.this.comments.addAll(0, comments);
            }
            BoardTopicViewFragment.this.preloadingUp = false;
            int firstVisible = BoardTopicViewFragment.this.list.getFirstVisiblePosition();
            int itemOffset = -1;
            if (firstVisible == 0) {
                if (BoardTopicViewFragment.this.list.getChildCount() > 3) {
                    firstVisible += 2;
                    itemOffset = BoardTopicViewFragment.this.list.getChildAt(3).getTop();
                }
            } else if (firstVisible == 1) {
                if (BoardTopicViewFragment.this.list.getChildCount() > 2) {
                    firstVisible++;
                    itemOffset = BoardTopicViewFragment.this.list.getChildAt(2).getTop();
                }
            } else if (BoardTopicViewFragment.this.list.getChildCount() > 1) {
                itemOffset = BoardTopicViewFragment.this.list.getChildAt(1).getTop();
            }
            if (BoardTopicViewFragment.this.preloadUpOnReady) {
                BoardTopicViewFragment.this.preloadingUp = true;
                BoardTopicViewFragment.this.preloadUpOnReady = false;
                BoardTopicViewFragment.this.loadDataUp();
            }
            BoardTopicViewFragment.this.updateList();
            BoardTopicViewFragment.this.moreAvailable = BoardTopicViewFragment.this.comments.size() + BoardTopicViewFragment.this.startOffset < total;
            BoardTopicViewFragment.this.bigProgress.setVisibility(8);
            BoardTopicViewFragment boardTopicViewFragment = BoardTopicViewFragment.this;
            boardTopicViewFragment.startOffset = boardTopicViewFragment.startOffset - comments.size();
            BoardTopicViewFragment.this.footerView.getChildAt(0).setVisibility(BoardTopicViewFragment.this.moreAvailable ? 0 : 8);
            BoardTopicViewFragment.this.headerView.getChildAt(0).setVisibility(BoardTopicViewFragment.this.startOffset > 0 ? 0 : 8);
            int pages = (int) Math.ceil((double) (((float) total) / 20.0f));
            if (pages > 1) {
                BoardTopicViewFragment.this.pagination.setPageCount(pages);
                BoardTopicViewFragment.this.pagination.setVisibility(0);
                BoardTopicViewFragment.this.pagination.show();
                BoardTopicViewFragment.this.pagination.postDelayed(new C05961(), 500);
                BoardTopicViewFragment.this.pagination.setEnabled(true);
            }
            BoardTopicViewFragment.this.list.setSelection(BoardTopicViewFragment.this.list.getHeaderViewsCount());
            BoardTopicViewFragment.this.list.setSelectionFromTop((firstVisible + 1) + Math.min(comments.size(), 20), itemOffset);
            BoardTopicViewFragment.this.dataLoading = false;
            BoardTopicViewFragment.this.loadUpReq = null;
        }

        public void fail(int ecode, String emsg) {
            BoardTopicViewFragment.this.dataLoading = false;
            BoardTopicViewFragment.this.loadUpReq = null;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.7 */
    class C14377 implements BoardAddComment.Callback {
        private final /* synthetic */ ArrayList val$atts;
        private final /* synthetic */ String val$txt;

        /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.7.1 */
        class C05991 implements Runnable {

            /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.7.1.1 */
            class C05981 implements Runnable {
                C05981() {
                }

                public void run() {
                    BoardTopicViewFragment.this.list.scrollDown();
                }
            }

            C05991() {
            }

            public void run() {
                ((InputMethodManager) BoardTopicViewFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(BoardTopicViewFragment.this.getActivity().getCurrentFocus().getWindowToken(), 2);
                BoardTopicViewFragment.this.list.postDelayed(new C05981(), 200);
            }
        }

        C14377(String str, ArrayList arrayList) {
            this.val$txt = str;
            this.val$atts = arrayList;
        }

        public void success(int cid) {
            BoardTopicViewFragment.this.commentBar.setText(ACRAConstants.DEFAULT_STRING_VALUE);
            BoardTopicViewFragment.this.commentBar.clearAttachments();
            BoardTopicViewFragment.this.focusable.requestFocus();
            if (BoardTopicViewFragment.this.getArguments().getInt("tid", 0) == -1) {
                BoardTopicViewFragment.this.createHint.setVisibility(8);
                BoardTopicViewFragment.this.getArguments().putInt("tid", cid);
                BoardTopicViewFragment.this.sendingComment = false;
                BoardTopicViewFragment.this.bigProgress.setVisibility(0);
                BoardTopicViewFragment.this.loadData();
            } else if (BoardTopicViewFragment.this.pagination.getPageCount() <= 1 || (BoardTopicViewFragment.this.startOffset + BoardTopicViewFragment.this.comments.size() >= (BoardTopicViewFragment.this.currentPage - 1) * 20 && (BoardTopicViewFragment.this.startOffset + BoardTopicViewFragment.this.comments.size()) / 20 >= BoardTopicViewFragment.this.pagination.getPageCount())) {
                Pattern ptn1 = Pattern.compile("((?:(?:http|https)://)?[a-zA-Z\u0430-\u044f\u0410-\u042f0-9-]+\\.[a-zA-Z\u0430-\u044f\u0410-\u042f]{2,4}[a-zA-Z/?\\.=#%&-_]+)");
                SharedPreferences prefs = BoardTopicViewFragment.this.getActivity().getSharedPreferences(null, 0);
                BoardComment comment = new BoardComment();
                comment.id = cid;
                comment.text = this.val$txt.replaceAll("\\[post(\\d+)\\|([^\\]]+)\\]", "$2");
                comment.uid = Global.uid;
                comment.userName = prefs.getString("username", "DELETED");
                comment.userPhoto = prefs.getString("userphoto", "http://vkontakte.ru/images/question_b.gif");
                comment.linkTitles = new ArrayList();
                comment.linkUrls = new ArrayList();
                comment.time = (int) (System.currentTimeMillis() / 1000);
                comment.attachments = new ArrayList();
                comment.attachments.addAll(this.val$atts);
                DisplayMetrics metrics = VKApplication.context.getResources().getDisplayMetrics();
                int tSize = Math.min(Math.min(metrics.widthPixels, metrics.heightPixels) - Global.scale(95.0f), 604);
                ZhukovLayout.processThumbs(tSize, (int) (((float) tSize) * 0.666f), comment.attachments);
                Matcher matcher = ptn1.matcher(comment.text);
                while (matcher.find()) {
                    comment.linkUrls.add("vklink://view/?" + matcher.group());
                    comment.linkTitles.add(matcher.group());
                }
                BoardTopicViewFragment.this.comments.add(comment);
                BoardTopicViewFragment.this.updateList();
                BoardTopicViewFragment.this.list.post(new C05991());
                BoardTopicViewFragment.this.sendingComment = false;
            } else {
                Toast.makeText(BoardTopicViewFragment.this.getActivity(), C0436R.string.board_comment_sent, 0).show();
                BoardTopicViewFragment.this.sendingComment = false;
            }
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(BoardTopicViewFragment.this.getActivity(), C0436R.string.err_text, 1).show();
            BoardTopicViewFragment.this.sendingComment = false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.9 */
    class C14389 implements BoardDeleteComment.Callback {
        private final /* synthetic */ BoardComment val$comment;

        C14389(BoardComment boardComment) {
            this.val$comment = boardComment;
        }

        public void success() {
            BoardTopicViewFragment.this.comments.remove(this.val$comment);
            BoardTopicViewFragment.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(BoardTopicViewFragment.this.getActivity(), C0436R.string.error, 0).show();
        }
    }

    private class CommentPhotosAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.BoardTopicViewFragment.CommentPhotosAdapter.1 */
        class C06011 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$image;
            private final /* synthetic */ int val$item;

            C06011(int i, int i2, Bitmap bitmap) {
                this.val$item = i;
                this.val$image = i2;
                this.val$bitmap = bitmap;
            }

            public void run() {
                Iterator it = BoardTopicViewFragment.this.visibleViews.iterator();
                while (it.hasNext()) {
                    View view = (View) it.next();
                    if (((Integer) view.getTag()).intValue() == this.val$item) {
                        try {
                            if (this.val$image == 0) {
                                ((ImageView) view.findViewById(C0436R.id.poster_photo)).setImageBitmap(this.val$bitmap);
                                return;
                            }
                            int imgindex = 0;
                            int index = 0;
                            Iterator it2 = ((BoardComment) BoardTopicViewFragment.this.comments.get(this.val$item)).attachments.iterator();
                            while (it2.hasNext()) {
                                Attachment att = (Attachment) it2.next();
                                if (att instanceof ImageAttachment) {
                                    imgindex++;
                                    if (imgindex == this.val$image) {
                                        ((ImageAttachment) att).setImage(((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(index), this.val$bitmap, false);
                                        return;
                                    }
                                }
                                index++;
                            }
                            return;
                        } catch (Exception e) {
                            return;
                        }
                    }
                }
            }
        }

        private CommentPhotosAdapter() {
        }

        public int getItemCount() {
            return BoardTopicViewFragment.this.comments.size();
        }

        public int getImageCountForItem(int item) {
            int count = 1;
            Iterator it = ((BoardComment) BoardTopicViewFragment.this.comments.get(item)).attachments.iterator();
            while (it.hasNext()) {
                if (((Attachment) it.next()) instanceof ImageAttachment) {
                    count++;
                }
            }
            return count;
        }

        public String getImageURL(int item, int image) {
            switch (image) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return ((BoardComment) BoardTopicViewFragment.this.comments.get(item)).userPhoto;
                default:
                    int imgindex = 0;
                    try {
                        Iterator it = ((BoardComment) BoardTopicViewFragment.this.comments.get(item)).attachments.iterator();
                        while (it.hasNext()) {
                            Attachment att = (Attachment) it.next();
                            if (att instanceof ImageAttachment) {
                                imgindex++;
                                if (imgindex == image) {
                                    return ((ImageAttachment) att).getImageURL();
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                    return null;
            }
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            BoardTopicViewFragment.this.list.post(new C06011(item, image, bitmap));
        }
    }

    public BoardTopicViewFragment() {
        this.comments = new ArrayList();
        this.preloadedComments = new ArrayList();
        this.preloadedUpComments = new ArrayList();
        this.preloading = false;
        this.preloadOnReady = false;
        this.moreAvailable = false;
        this.dataLoading = false;
        this.preloadingUp = false;
        this.preloadUpOnReady = false;
        this.visibleViews = new ArrayList();
        this.currentPage = -1;
        this.startOffset = 0;
        this.resetScroll = false;
        this.sendingComment = false;
        this.refreshReq = null;
        this.loadUpReq = null;
        this.ignoreNextScroll = false;
        this.ignoreScrollEvents = true;
        this.firstLoad = true;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        if (getArguments().containsKey(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)) {
            act.setTitle(getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
        }
        ActivityUtils.setBeamLink(act, "topic-" + getArguments().getInt("gid", 0) + "_" + getArguments().getInt("tid"));
        this.startOffset = getArguments().getInt("offset", 0);
        if (this.startOffset < 0) {
            this.startOffset = 0;
        }
        if (this.startOffset > 0) {
            this.resetScroll = true;
        }
        this.wrapView = new FrameLayout(act);
        this.wrapView.setBackgroundColor(-1);
        this.footerView = new FrameLayout(act);
        ProgressBar pb = new ProgressBar(act);
        LayoutParams lp = new LayoutParams(Global.scale(BitmapDescriptorFactory.HUE_ORANGE), Global.scale(BitmapDescriptorFactory.HUE_ORANGE));
        lp.gravity = 17;
        pb.setLayoutParams(lp);
        this.footerView.setPadding(0, Global.scale(7.0f), 0, Global.scale(7.0f));
        this.footerView.addView(pb);
        pb.setVisibility(8);
        this.headerView = new FrameLayout(act);
        pb = new ProgressBar(act);
        lp = new LayoutParams(Global.scale(BitmapDescriptorFactory.HUE_ORANGE), Global.scale(44.0f));
        lp.gravity = 17;
        pb.setLayoutParams(lp);
        pb.setPadding(0, Global.scale(7.0f), 0, Global.scale(7.0f));
        this.headerView.addView(pb);
        pb.setVisibility(8);
        this.pollWrap = new LinearLayout(act);
        this.pollWrap.setPadding(Global.scale(5.0f), 0, Global.scale(5.0f), 0);
        this.list = new RefreshableListView((Context) act, false);
        this.list.addFooterView(this.footerView, null, false);
        this.list.addHeaderView(this.headerView, null, false);
        this.list.addHeaderView(this.pollWrap, null, false);
        this.list.setFromTop(false);
        RefreshableListView refreshableListView = this.list;
        ListAdapter commentsAdapter = new CommentsAdapter();
        this.adapter = commentsAdapter;
        refreshableListView.setAdapter(commentsAdapter);
        this.list.setDivider(null);
        this.list.setCacheColorHint(-2039584);
        this.list.setBackgroundColor(-2039584);
        this.list.setTopColor(-2039584);
        this.list.setVerticalFadingEdgeEnabled(false);
        this.list.setHighlightAfterClick(false);
        this.list.setVerticalScrollBarEnabled(false);
        this.list.setOnRefreshListener(this);
        this.list.setOnItemClickListener(this);
        this.list.setOnItemLongClickListener(this);
        this.wrapView.addView(this.list);
        this.noNewsView = new TextView(act);
        this.noNewsView.setTextColor(-8947849);
        this.noNewsView.setText(C0436R.string.no_news);
        this.noNewsView.setTextSize(GalleryPickerFooterView.BADGE_SIZE);
        this.noNewsView.setGravity(17);
        LayoutParams lparams = new LayoutParams(-1, -2);
        lparams.gravity = 17;
        this.noNewsView.setLayoutParams(lparams);
        this.noNewsView.setVisibility(8);
        this.wrapView.addView(this.noNewsView);
        this.bigProgress = new ProgressBar(act);
        LayoutParams lp2 = new LayoutParams(-2, -2);
        lp2.gravity = 17;
        this.bigProgress.setLayoutParams(lp2);
        this.bigProgress.setVisibility(0);
        this.wrapView.addView(this.bigProgress);
        this.pagination = new PaginationView(act);
        this.pagination.setVisibility(8);
        this.pagination.setListener(this);
        this.wrapView.addView(this.pagination);
        this.imgLoader = new ListImageLoaderWrapper(new CommentPhotosAdapter(), this.list, this);
        this.contentView = new LinearLayout(act);
        this.contentView.setOrientation(1);
        this.wrapView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        this.contentView.addView(this.wrapView);
        this.focusable = new View(act);
        this.focusable.setFocusable(true);
        this.focusable.setFocusableInTouchMode(true);
        this.focusable.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
        this.focusable.requestFocus();
        this.contentView.addView(this.focusable);
        this.commentBar = new WriteBar(act);
        View shadow = new View(act);
        shadow.setBackgroundResource(C0436R.drawable.bottom_shadow_comments);
        LinearLayout.LayoutParams lpcbs = new LinearLayout.LayoutParams(-1, Global.scale(ImageViewHolder.PaddingSize));
        lpcbs.topMargin = Global.scale(-2.0f);
        this.contentView.addView(shadow, lpcbs);
        this.contentView.addView(this.commentBar);
        if (getArguments().containsKey("is_closed")) {
            this.commentBar.setVisibility(8);
        }
        this.commentBar.findViewById(C0436R.id.writebar_send).setOnClickListener(new C05921());
        this.commentBar.setFragment(this);
        this.commentBar.setUploadType(0, -getArguments().getInt("gid", 0));
        this.commentBar.setAttachLimits(10, false);
        if (getArguments().getInt("tid", 0) != -1) {
            loadData();
            return;
        }
        this.bigProgress.setVisibility(8);
        this.createHint = new TextView(act);
        this.createHint.setText(C0436R.string.create_topic_explain);
        this.createHint.setTextColor(ExploreByTouchHelper.INVALID_ID);
        this.createHint.setTextSize(18.0f);
        this.createHint.setPadding(Global.scale(12.0f), 0, Global.scale(12.0f), 0);
        this.createHint.setGravity(17);
        this.wrapView.addView(this.createHint);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentView;
    }

    private void waitAndSendComment() {
        ProgressDialog progress = new ProgressDialog(getActivity());
        progress.setMessage(getString(C0436R.string.loading));
        progress.setCancelable(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        this.commentBar.waitForUploads(new C05932(progress), new C05943(progress));
    }

    private void loadData() {
        this.dataLoading = true;
        int i = getArguments().getInt("gid", 0);
        int i2 = getArguments().getInt("tid", 0);
        int size = this.startOffset + this.comments.size();
        int i3 = (this.preloading || this.comments.size() == 0) ? 40 : 20;
        this.refreshReq = new BoardGetComments(i, i2, size, i3).setCallback(new C14354()).exec(this.contentView);
    }

    private void loadDataUp() {
        this.dataLoading = true;
        this.loadUpReq = new BoardGetComments(getArguments().getInt("gid", 0), getArguments().getInt("tid", 0), this.startOffset - 20, 20).setCallback(new C14365()).exec(this.contentView);
    }

    public void updateList() {
        getActivity().runOnUiThread(new C05976());
    }

    public void onScrolledToLastItem() {
        if ((this.dataLoading && !this.preloading) || !this.moreAvailable) {
            return;
        }
        if (this.preloading) {
            this.preloading = false;
            this.preloadOnReady = true;
        } else if (this.preloadedComments.size() > 0) {
            this.comments.addAll(this.preloadedComments);
            updateList();
            this.preloadedComments.clear();
            this.preloading = true;
            loadData();
        } else {
            loadData();
        }
    }

    public void onScrollStarted() {
        this.pagination.show();
    }

    public void onScrollStopped() {
        this.pagination.hide();
    }

    private void sendComment() {
        if (!this.sendingComment) {
            this.sendingComment = true;
            String _txt = this.commentBar.getText();
            if (_txt.length() != 0 || this.commentBar.getAttachments().size() != 0) {
                String txt = _txt;
                ArrayList<Attachment> atts = this.commentBar.getAttachments();
                new BoardAddComment(getArguments().getInt("gid", 0), getArguments().getInt("tid", 0), txt, atts, getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE).toString()).setCallback(new C14377(txt, atts)).wrapProgress(getActivity()).exec(this.contentView);
            }
        }
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        super.onActivityResult(reqCode, resCode, data);
        if (reqCode > 10000) {
            this.commentBar.onActivityResult(reqCode, resCode, data);
        }
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id) {
        pos -= this.list.getHeaderViewsCount();
        if (pos < 0 || pos >= this.comments.size()) {
            return false;
        }
        BoardComment comment = (BoardComment) this.comments.get(pos);
        ArrayList<String> options = new ArrayList();
        options.addAll(comment.linkTitles);
        options.add(getResources().getString(C0436R.string.copy_text));
        if ((getArguments().containsKey("is_admin") || comment.uid == Global.uid) && !(pos == 0 && this.startOffset == 0)) {
            options.add(getResources().getString(C0436R.string.delete));
        }
        new Builder(getActivity()).setItems((CharSequence[]) options.toArray(new String[0]), new C06008(comment)).show();
        return true;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        if (!getArguments().containsKey("is_closed")) {
            pos -= this.list.getHeaderViewsCount();
            if (pos >= 0 && pos < this.comments.size()) {
                BoardComment comment = (BoardComment) this.comments.get(pos);
                String text = this.commentBar.getText();
                if (text.indexOf("[post" + comment.id + "|") <= -1) {
                    this.commentBar.setText(new StringBuilder(String.valueOf(text)).append("[post").append(comment.id).append("|").append(comment.userName.split(" ")[0]).append("], ").toString());
                    this.commentBar.focus();
                }
            }
        }
    }

    private void deleteComment(BoardComment comment) {
        new BoardDeleteComment(getArguments().getInt("gid", 0), getArguments().getInt("tid", 0), comment.id).setCallback(new C14389(comment)).wrapProgress(getActivity()).exec(this.contentView);
    }

    public void onClick(View v) {
        if (v.getTag() != null) {
            Bundle args = new Bundle();
            args.putInt("id", ((Integer) v.getTag()).intValue());
            Navigate.to("ProfileFragment", args, getActivity());
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.loadUpReq != null) {
            this.loadUpReq.cancel();
        }
        if (this.refreshReq != null) {
            this.refreshReq.cancel();
        }
    }

    public void onScroll(int firstItem, int visibleCount, int total) {
        if (!this.ignoreScrollEvents) {
            if (this.ignoreNextScroll) {
                this.ignoreNextScroll = false;
            } else {
                this.pagination.show();
                this.pagination.hide();
            }
        }
        int page = ((int) Math.floor((double) (((float) (((firstItem + visibleCount) + this.startOffset) - 1)) / 20.0f))) + 1;
        if (page != this.currentPage) {
            this.currentPage = page;
            this.pagination.setCurrentPage(page);
        }
        if (firstItem > -1) {
            return;
        }
        if ((this.dataLoading && !this.preloadingUp) || this.startOffset <= 0) {
            return;
        }
        if (this.preloadingUp) {
            this.preloadingUp = false;
            this.preloadUpOnReady = true;
        } else if (this.preloadedUpComments.size() > 0) {
            this.comments.addAll(0, this.preloadedUpComments);
            updateList();
            int firstVisible = this.list.getFirstVisiblePosition();
            int itemOffset = -1;
            if (firstVisible == 0) {
                if (this.list.getChildCount() > 2) {
                    firstVisible++;
                    itemOffset = this.list.getChildAt(2).getTop();
                }
            } else if (this.list.getChildCount() > 1) {
                itemOffset = this.list.getChildAt(1).getTop();
            }
            this.list.setSelectionFromTop((firstVisible + 1) + Math.min(this.preloadedUpComments.size(), 20), itemOffset);
            this.preloadedUpComments.clear();
            this.preloadingUp = true;
            loadDataUp();
        } else {
            loadDataUp();
        }
    }

    private void refresh() {
        this.list.setVisibility(8);
        this.comments.clear();
        this.preloadedComments.clear();
        this.preloadedUpComments.clear();
        updateList();
        this.list.setSelection(this.list.getHeaderViewsCount());
        this.bigProgress.setVisibility(0);
        this.resetScroll = true;
        if (this.refreshReq != null) {
            this.refreshReq.handler(null);
            this.refreshReq.cancel();
            this.refreshReq = null;
        }
        if (this.loadUpReq != null) {
            this.loadUpReq.handler(null);
            this.loadUpReq.cancel();
            this.loadUpReq = null;
        }
        loadData();
    }

    public void onPageSelected(int num) {
        if (num > 0) {
            int offset = (num - 1) * 20;
            this.currentPage = num;
            this.pagination.setCurrentPage(num);
            if (offset < this.startOffset || offset >= (this.startOffset + this.comments.size()) + this.preloadedComments.size()) {
                this.startOffset = offset;
                this.pagination.show();
                refresh();
                return;
            }
            this.list.setSelection(this.list.getHeaderViewsCount() + offset);
            this.pagination.hide();
            if (this.refreshReq != null) {
                this.refreshReq.cancel();
                this.refreshReq = null;
                return;
            }
            return;
        }
        TextView tv = new TextView(getActivity());
        tv.setText(getResources().getString(C0436R.string.page_explain, new Object[]{Integer.valueOf(this.pagination.getPageCount())}));
        EditText edit = new EditText(getActivity());
        edit.setInputType(3);
        edit.setWidth(Global.scale(200.0f));
        edit.setText(new StringBuilder(String.valueOf(this.currentPage)).toString());
        LinearLayout ll = new LinearLayout(getActivity());
        ll.setOrientation(1);
        ll.addView(tv);
        ll.addView(edit);
        int padding = Global.scale(10.0f);
        ll.setPadding(padding, padding, padding, padding);
        new Builder(getActivity()).setTitle(C0436R.string.jump_to_page).setView(ll).setPositiveButton(C0436R.string.ok, new AnonymousClass10(edit)).setNegativeButton(C0436R.string.cancel, null).show();
    }

    public void onRefresh() {
        loadData();
    }

    public String getLastUpdatedTime() {
        return this.lastUpdated == 0 ? getString(C0436R.string.not_updated) : getString(C0436R.string.updated) + " " + Global.langDateRelative(this.lastUpdated, getResources());
    }

    public void onPause() {
        super.onPause();
        this.imgLoader.deactivate();
    }

    public void onResume() {
        super.onResume();
        this.imgLoader.activate();
    }

    public void onScrolled(float offset) {
    }
}
