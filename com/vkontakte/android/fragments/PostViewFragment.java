package com.vkontakte.android.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.media.TransportMediator;
import android.text.ClipboardManager;
import android.text.Html;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.WebDialog;
import com.google.android.gms.appstate.AppStateClient;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.DocumentAttachment;
import com.vkontakte.android.EmojiPopup;
import com.vkontakte.android.ExTextView;
import com.vkontakte.android.FragmentWrapperActivity;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageAttachment;
import com.vkontakte.android.LoadMoreCommentsView;
import com.vkontakte.android.Log;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.NewsComment;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.NewsItemView;
import com.vkontakte.android.Photo;
import com.vkontakte.android.PhotoAttachment;
import com.vkontakte.android.PhotoAttachment.FixedSizeImageView;
import com.vkontakte.android.ReportContentActivity;
import com.vkontakte.android.RepostActivity;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.VideoAttachView;
import com.vkontakte.android.VideoAttachment;
import com.vkontakte.android.ViewUtils;
import com.vkontakte.android.ZhukovLayout;
import com.vkontakte.android.api.VideoGetInfo;
import com.vkontakte.android.api.WallAddComment;
import com.vkontakte.android.api.WallDelete;
import com.vkontakte.android.api.WallDeleteComment;
import com.vkontakte.android.api.WallEdit;
import com.vkontakte.android.api.WallEdit.Callback;
import com.vkontakte.android.api.WallGetComments;
import com.vkontakte.android.api.WallLike;
import com.vkontakte.android.api.WallRestoreComment;
import com.vkontakte.android.cache.NewsfeedCache;
import com.vkontakte.android.cache.UserWallCache;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.data.Posts;
import com.vkontakte.android.mediapicker.gl.GLFilterContext;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.RefreshableListView;
import com.vkontakte.android.ui.RefreshableListView.OnRefreshListener;
import com.vkontakte.android.ui.WriteBar;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.acra.ACRAConstants;

public class PostViewFragment extends SherlockFragment implements OnRefreshListener {
    private static final int EDIT_COMMENT_REQUEST = 4329;
    private static final int EDIT_REQUEST = 4328;
    public static final int RESULT_DELETED = 2;
    public static final int TYPE_PHOTO = 1;
    public static final int TYPE_POST = 0;
    private String accessKey;
    private PostCommentsAdapter adapter;
    private boolean canAdmin;
    private WriteBar commentBar;
    private ArrayList<NewsComment> comments;
    private ProgressBar commentsProgress;
    private LinearLayout contentView;
    private APIRequest currentReq;
    private NewsEntry f187e;
    private EmojiPopup emojiPopup;
    private ErrorView error;
    private View headerView;
    private ListImageLoaderWrapper imgLoader;
    private long lastUpdated;
    private ArrayList<String> likePhotos;
    private APIRequest likeReq;
    private boolean liking;
    private RefreshableListView list;
    private LoadMoreCommentsView loadMoreView;
    private boolean loadingComments;
    private int maxLikePhotos;
    private int maxLikeVisible;
    private boolean narrowScreen;
    private ArrayList<Photo> photos;
    private BroadcastReceiver receiver;
    private int replyTo;
    private String replyToName;
    private String replyToRName;
    private int replyToUid;
    private ArrayList<Photo> repostPhotos;
    private int scrollToComment;
    private int type;
    private boolean videoBottomPadding;

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.1 */
    class C07611 extends BroadcastReceiver {
        C07611() {
        }

        public void onReceive(Context context, Intent intent) {
            if (Posts.ACTION_POST_UPDATED_BROADCAST.equals(intent.getAction())) {
                int oid = intent.getIntExtra("owner_id", 0);
                int pid = intent.getIntExtra("post_id", 0);
                if (oid == PostViewFragment.this.f187e.ownerID && pid == PostViewFragment.this.f187e.postID) {
                    Log.m525d("vk", "extras=" + intent.getExtras());
                    PostViewFragment.this.f187e.numLikes = intent.getIntExtra("likes", 0);
                    PostViewFragment.this.f187e.numComments = intent.getIntExtra("comments", 0);
                    PostViewFragment.this.f187e.numRetweets = intent.getIntExtra("retweets", 0);
                    PostViewFragment.this.f187e.flag(4, intent.getBooleanExtra("retweeted", false));
                    PostViewFragment.this.updateButtons();
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.21 */
    class AnonymousClass21 implements OnClickListener {
        private final /* synthetic */ EditText val$edit;

        AnonymousClass21(EditText editText) {
            this.val$edit = editText;
        }

        public void onClick(DialogInterface dialog, int which) {
            PostViewFragment.this.saveRepostComment(this.val$edit.getText().toString());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.24 */
    class AnonymousClass24 implements OnClickListener {
        private final /* synthetic */ ArrayList val$acts;
        private final /* synthetic */ NewsComment val$comm;

        /* renamed from: com.vkontakte.android.fragments.PostViewFragment.24.1 */
        class C07621 implements Runnable {
            C07621() {
            }

            public void run() {
                ((InputMethodManager) PostViewFragment.this.getActivity().getSystemService("input_method")).showSoftInput(PostViewFragment.this.commentBar.findViewById(C0436R.id.writebar_edit), PostViewFragment.TYPE_PHOTO);
            }
        }

        AnonymousClass24(ArrayList arrayList, NewsComment newsComment) {
            this.val$acts = arrayList;
            this.val$comm = newsComment;
        }

        public void onClick(DialogInterface dialog, int which) {
            String act = (String) this.val$acts.get(which);
            Bundle args;
            if ("profile".equals(act)) {
                args = new Bundle();
                args.putInt("id", this.val$comm.uid);
                Navigate.to("ProfileFragment", args, PostViewFragment.this.getActivity());
            } else if ("reply".equals(act)) {
                PostViewFragment.this.replyTo = this.val$comm.cid;
                PostViewFragment.this.replyToUid = this.val$comm.uid;
                PostViewFragment.this.replyToRName = this.val$comm.userRName;
                PostViewFragment.this.replyToName = this.val$comm.userName.split(" ")[0];
                if (PostViewFragment.this.f187e.flag(PostViewFragment.RESULT_DELETED)) {
                    if (PostViewFragment.this.commentBar.isTextEmpty()) {
                        PostViewFragment.this.commentBar.setText(new StringBuilder(String.valueOf(PostViewFragment.this.replyToName)).append(", ").toString());
                    }
                    PostViewFragment.this.commentBar.focus();
                    PostViewFragment.this.list.postDelayed(new C07621(), 200);
                }
            } else if ("copy".equals(act)) {
                ((ClipboardManager) PostViewFragment.this.getActivity().getSystemService("clipboard")).setText(Global.replaceHTML(this.val$comm.text).replace("<br/>", "\n"));
                Toast.makeText(PostViewFragment.this.getActivity(), C0436R.string.text_copied, 0).show();
            } else if ("like".equals(act)) {
                PostViewFragment.this.likeComment(this.val$comm);
            } else if ("liked".equals(act)) {
                args = new Bundle();
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, PostViewFragment.this.getString(C0436R.string.liked));
                args.putInt(WebDialog.DIALOG_PARAM_TYPE, 0);
                args.putInt("ltype", 5);
                args.putInt("lptype", PostViewFragment.this.f187e.type);
                args.putInt("oid", PostViewFragment.this.f187e.ownerID);
                args.putInt("item_id", this.val$comm.cid);
                Navigate.to("LikesListFragment", args, PostViewFragment.this.getActivity());
            } else if ("delete".equals(act)) {
                PostViewFragment.this.deleteComment(this.val$comm.cid);
            } else if ("edit".equals(act)) {
                PostViewFragment.this.editComment(this.val$comm);
            } else if ("report".equals(act)) {
                Intent intent = new Intent(PostViewFragment.this.getActivity(), ReportContentActivity.class);
                intent.putExtra("itemID", this.val$comm.cid);
                intent.putExtra("ownerID", PostViewFragment.this.f187e.ownerID);
                String type = "post_comment";
                if (PostViewFragment.this.f187e.type == PostViewFragment.TYPE_PHOTO) {
                    type = "photo_comment";
                }
                if (PostViewFragment.this.f187e.type == PostViewFragment.RESULT_DELETED) {
                    type = "video_comment";
                }
                intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, type);
                PostViewFragment.this.startActivity(intent);
            } else {
                PostViewFragment.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(act)));
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.27 */
    class AnonymousClass27 implements Runnable {
        private final /* synthetic */ ProgressDialog val$progress;

        AnonymousClass27(ProgressDialog progressDialog) {
            this.val$progress = progressDialog;
        }

        public void run() {
            this.val$progress.dismiss();
            PostViewFragment.this.sendComment();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.28 */
    class AnonymousClass28 implements Runnable {
        private final /* synthetic */ ProgressDialog val$progress;

        AnonymousClass28(ProgressDialog progressDialog) {
            this.val$progress = progressDialog;
        }

        public void run() {
            this.val$progress.dismiss();
            Toast.makeText(PostViewFragment.this.getActivity(), C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.2 */
    class C07652 extends LinearLayout {
        C07652(Context $anonymous0) {
            super($anonymous0);
        }

        public void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            Rect rect = new Rect();
            PostViewFragment.this.getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
            int diff = (PostViewFragment.this.getActivity().getWindow().getDecorView().getHeight() - rect.top) - (h + PostViewFragment.this.getSherlockActivity().getSupportActionBar().getHeight());
            PostViewFragment.this.emojiPopup.onKeyboardStateChanged(diff > Global.scale(100.0f), diff);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.33 */
    class AnonymousClass33 implements OnPreDrawListener {
        private final /* synthetic */ int val$pos;

        /* renamed from: com.vkontakte.android.fragments.PostViewFragment.33.1 */
        class C07661 implements Runnable {
            private final /* synthetic */ TransitionDrawable val$td;

            C07661(TransitionDrawable transitionDrawable) {
                this.val$td = transitionDrawable;
            }

            public void run() {
                this.val$td.startTransition(AppStateClient.STATUS_WRITE_OUT_OF_DATE_VERSION);
            }
        }

        AnonymousClass33(int i) {
            this.val$pos = i;
        }

        public boolean onPreDraw() {
            PostViewFragment.this.list.getViewTreeObserver().removeOnPreDrawListener(this);
            int childPos = this.val$pos - PostViewFragment.this.list.getFirstVisiblePosition();
            if (childPos >= 0 && childPos < PostViewFragment.this.list.getChildCount()) {
                View item = PostViewFragment.this.list.getChildAt(childPos);
                Drawable[] drawableArr = new Drawable[PostViewFragment.RESULT_DELETED];
                drawableArr[0] = new ColorDrawable(563063239);
                drawableArr[PostViewFragment.TYPE_PHOTO] = new ColorDrawable(9415111);
                TransitionDrawable td = new TransitionDrawable(drawableArr);
                item.setBackgroundDrawable(td);
                td.setCrossFadeEnabled(true);
                PostViewFragment.this.list.postDelayed(new C07661(td), 2000);
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.34 */
    class AnonymousClass34 implements AnimationListener {
        private final /* synthetic */ LinearLayout val$lpwrap;

        AnonymousClass34(LinearLayout linearLayout) {
            this.val$lpwrap = linearLayout;
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            this.val$lpwrap.getChildAt(PostViewFragment.this.maxLikeVisible).setVisibility(8);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.35 */
    class AnonymousClass35 implements AnimationListener {
        private final /* synthetic */ LinearLayout val$lpwrap;

        AnonymousClass35(LinearLayout linearLayout) {
            this.val$lpwrap = linearLayout;
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            this.val$lpwrap.getChildAt(0).setVisibility(8);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.3 */
    class C07703 implements View.OnClickListener {
        private final /* synthetic */ int val$idx;

        C07703(int i) {
            this.val$idx = i;
        }

        public void onClick(View v) {
            PostViewFragment.this.openPhotoList(this.val$idx, v, (ViewGroup) PostViewFragment.this.headerView.findViewById(C0436R.id.wall_view_attach), PostViewFragment.this.photos);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.4 */
    class C07714 implements View.OnClickListener {
        private final /* synthetic */ int val$idx;

        C07714(int i) {
            this.val$idx = i;
        }

        public void onClick(View v) {
            PostViewFragment.this.openPhotoList(this.val$idx, v, (ViewGroup) PostViewFragment.this.headerView.findViewById(C0436R.id.wall_view_repost_attach), PostViewFragment.this.repostPhotos);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.5 */
    class C07725 implements View.OnClickListener {
        C07725() {
        }

        public void onClick(View v) {
            Bundle args = new Bundle();
            args.putInt("id", PostViewFragment.this.f187e.userID);
            Navigate.to("ProfileFragment", args, PostViewFragment.this.getActivity());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.6 */
    class C07736 implements View.OnClickListener {
        C07736() {
        }

        public void onClick(View v) {
            String type = "wall";
            if (PostViewFragment.this.f187e.retweetType == PostViewFragment.TYPE_PHOTO) {
                type = "photo";
            }
            if (PostViewFragment.this.f187e.retweetType == PostViewFragment.RESULT_DELETED) {
                type = "video";
            }
            PostViewFragment.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("vkontakte://vk.com/" + type + PostViewFragment.this.f187e.retweetUID + "_" + PostViewFragment.this.f187e.retweetOrigId)));
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.7 */
    class C07747 implements View.OnClickListener {
        C07747() {
        }

        public void onClick(View v) {
            PostViewFragment.this.loadMoreView.showProgress(true);
            PostViewFragment.this.updateList();
            PostViewFragment.this.loadComments(false);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.8 */
    class C07758 implements View.OnClickListener {
        C07758() {
        }

        public void onClick(View v) {
            if (PostViewFragment.this.commentBar.isUploading()) {
                PostViewFragment.this.waitAndSendComment();
            } else {
                PostViewFragment.this.sendComment();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.9 */
    class C07779 implements OnItemLongClickListener {

        /* renamed from: com.vkontakte.android.fragments.PostViewFragment.9.1 */
        class C07761 implements OnClickListener {
            C07761() {
            }

            public void onClick(DialogInterface dialog, int which) {
                ((ClipboardManager) PostViewFragment.this.getActivity().getSystemService("clipboard")).setText(((TextView) PostViewFragment.this.headerView.findViewById(C0436R.id.wall_view_post)).getText().toString());
                Toast.makeText(PostViewFragment.this.getActivity(), C0436R.string.text_copied, 0).show();
            }
        }

        C07779() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id) {
            if (pos != 0) {
                return false;
            }
            Builder builder = new Builder(PostViewFragment.this.getActivity());
            CharSequence[] charSequenceArr = new String[PostViewFragment.TYPE_PHOTO];
            charSequenceArr[0] = PostViewFragment.this.getResources().getString(C0436R.string.copy_text);
            builder.setItems(charSequenceArr, new C07761()).show();
            return true;
        }
    }

    private class PostCommentsAdapter extends BaseAdapter {

        /* renamed from: com.vkontakte.android.fragments.PostViewFragment.PostCommentsAdapter.1 */
        class C07781 implements View.OnClickListener {
            C07781() {
            }

            public void onClick(View v) {
                PostViewFragment.this.restoreComment(((Integer) v.getTag()).intValue());
            }
        }

        /* renamed from: com.vkontakte.android.fragments.PostViewFragment.PostCommentsAdapter.2 */
        class C07792 implements View.OnClickListener {
            C07792() {
            }

            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putInt("id", ((NewsComment) PostViewFragment.this.comments.get(((Integer) ((View) v.getParent()).getTag()).intValue())).uid);
                Navigate.to("ProfileFragment", args, PostViewFragment.this.getActivity());
            }
        }

        private PostCommentsAdapter() {
        }

        public int getCount() {
            return PostViewFragment.this.comments.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public int getViewTypeCount() {
            return PostViewFragment.RESULT_DELETED;
        }

        public int getItemViewType(int pos) {
            return ((NewsComment) PostViewFragment.this.comments.get(pos)).isDeleted ? PostViewFragment.TYPE_PHOTO : 0;
        }

        public boolean isEnabled(int pos) {
            return !((NewsComment) PostViewFragment.this.comments.get(pos)).isDeleted;
        }

        public View getView(int position, View view, ViewGroup parent) {
            NewsComment comment = (NewsComment) PostViewFragment.this.comments.get(position);
            if (comment.isDeleted) {
                if (view == null) {
                    view = View.inflate(PostViewFragment.this.getActivity(), C0436R.layout.deleted_comment, null);
                    view.findViewById(C0436R.id.comment_restore_btn).setOnClickListener(new C07781());
                }
                view.findViewById(C0436R.id.comment_restore_btn).setTag(Integer.valueOf(comment.cid));
                view.setBackgroundDrawable(null);
            } else {
                if (view == null) {
                    view = View.inflate(PostViewFragment.this.getActivity(), C0436R.layout.wall_comment, null);
                    view.findViewById(C0436R.id.poster_photo).setTag(Integer.valueOf(PostViewFragment.TYPE_PHOTO));
                    view.findViewById(C0436R.id.poster_photo).setOnClickListener(new C07792());
                    ((TextView) view.findViewById(C0436R.id.post_view)).setTextSize(PostViewFragment.TYPE_PHOTO, 16.0f + (((float) Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(PostViewFragment.this.getActivity()).getString("fontSize", "0"))) * ImageViewHolder.PaddingSize));
                }
                view.setBackgroundDrawable(null);
                view.setTag(Integer.valueOf(position));
                ((TextView) view.findViewById(C0436R.id.post_view)).setText(comment.displayableText);
                ((TextView) view.findViewById(C0436R.id.poster_name_view)).setText(comment.userName);
                String info = Global.langDate(PostViewFragment.this.getResources(), comment.time);
                if (comment.respToName != null) {
                    info = new StringBuilder(String.valueOf(info)).append(" ").append(comment.respToName).toString();
                }
                ((TextView) view.findViewById(C0436R.id.post_info_view)).setText(info);
                view.findViewById(C0436R.id.post_view).setVisibility(comment.text.length() > 0 ? 0 : 8);
                if (comment.numLikes > 0) {
                    view.findViewById(C0436R.id.post_likes).setVisibility(0);
                    ((TextView) view.findViewById(C0436R.id.post_likes)).setText(new StringBuilder(String.valueOf(comment.numLikes)).toString());
                    ((TextView) view.findViewById(C0436R.id.post_likes)).setTextColor(comment.isLiked ? -13070905 : -5855578);
                    ((TextView) view.findViewById(C0436R.id.post_likes)).setCompoundDrawablesWithIntrinsicBounds(PostViewFragment.this.getResources().getDrawable(comment.isLiked ? C0436R.drawable.ic_comment_liked : C0436R.drawable.ic_comment_like), null, null, null);
                } else {
                    view.findViewById(C0436R.id.post_likes).setVisibility(8);
                }
                ((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).removeAllViews();
                if (comment.attachments.size() > 0) {
                    view.findViewById(C0436R.id.post_attach_container).setVisibility(0);
                    NewsItemView.addAttachments(view, comment.attachments, null, C0436R.id.post_attach_container);
                    int idx = 0;
                    Iterator it = comment.attachments.iterator();
                    while (it.hasNext()) {
                        String src;
                        Attachment att = (Attachment) it.next();
                        if (att instanceof PhotoAttachment) {
                            src = ((PhotoAttachment) att).getThumbURL();
                            if (!PostViewFragment.this.imgLoader.isAlreadyLoaded(src) || PostViewFragment.this.imgLoader.get(src) == null) {
                                ((ImageView) ((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(idx)).setImageResource(C0436R.drawable.photo_placeholder);
                            } else {
                                ((ImageView) ((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(idx)).setImageBitmap(PostViewFragment.this.imgLoader.get(src));
                                ((FixedSizeImageView) ((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(idx)).dontAnimate();
                            }
                        }
                        if (att instanceof VideoAttachment) {
                            src = ((VideoAttachment) att).image;
                            if (PostViewFragment.this.imgLoader.isAlreadyLoaded(src)) {
                                ((VideoAttachView) ((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(idx)).setImageBitmap(PostViewFragment.this.imgLoader.get(src));
                            }
                        }
                        if ((att instanceof DocumentAttachment) && ((DocumentAttachment) att).thumb != null && ((DocumentAttachment) att).thumb.length() > 0) {
                            src = ((DocumentAttachment) att).thumb;
                            if (PostViewFragment.this.imgLoader.isAlreadyLoaded(src)) {
                                ((ImageView) ((ViewGroup) ((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(idx)).findViewById(C0436R.id.att_doc_thumb)).setImageBitmap(PostViewFragment.this.imgLoader.get(src));
                            }
                        }
                        idx += PostViewFragment.TYPE_PHOTO;
                    }
                } else {
                    view.findViewById(C0436R.id.post_attach_container).setVisibility(8);
                }
                if (PostViewFragment.this.imgLoader.isAlreadyLoaded(comment.userPhoto)) {
                    ((ImageView) view.findViewById(C0436R.id.poster_photo)).setImageBitmap(PostViewFragment.this.imgLoader.get(comment.userPhoto));
                } else {
                    ((ImageView) view.findViewById(C0436R.id.poster_photo)).setImageResource(C0436R.drawable.user_placeholder);
                }
            }
            return view;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.22 */
    class AnonymousClass22 implements Callback {
        private final /* synthetic */ String val$newComment;

        AnonymousClass22(String str) {
            this.val$newComment = str;
        }

        public void success() {
            int i = 0;
            Toast.makeText(PostViewFragment.this.getActivity(), C0436R.string.post_edit_saved, 0).show();
            PostViewFragment.this.f187e.retweetText = Global.replaceMentions(this.val$newComment);
            if (PostViewFragment.this.f187e.ownerID == Global.uid) {
                UserWallCache.remove(PostViewFragment.this.f187e.postID, PostViewFragment.this.getActivity());
                UserWallCache.add(PostViewFragment.this.f187e, PostViewFragment.this.getActivity());
            }
            if (PostViewFragment.this.f187e.ownerID == PostViewFragment.this.f187e.userID) {
                NewsfeedCache.remove(PostViewFragment.this.f187e.ownerID, PostViewFragment.this.f187e.postID, PostViewFragment.this.getActivity());
                NewsfeedCache.add(PostViewFragment.this.f187e, PostViewFragment.this.getActivity());
            }
            Intent intent = new Intent(Posts.ACTION_POST_REPLACED_BROADCAST);
            intent.putExtra("entry", PostViewFragment.this.f187e);
            PostViewFragment.this.getActivity().sendBroadcast(intent);
            View findViewById = PostViewFragment.this.headerView.findViewById(C0436R.id.wall_retweet_text);
            if (this.val$newComment.length() <= 0) {
                i = 8;
            }
            findViewById.setVisibility(i);
            ((ExTextView) PostViewFragment.this.contentView.findViewById(C0436R.id.wall_retweet_text)).setText(Global.replaceEmoji(NewsEntry.stripUnderlines((Spannable) Html.fromHtml(PostViewFragment.this.f187e.retweetText.replace("\n", "<br/>")))));
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PostViewFragment.this.getActivity(), C0436R.string.error, 0).show();
            PostViewFragment.this.editRepostComment(this.val$newComment);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.25 */
    class AnonymousClass25 implements WallLike.Callback {
        private final /* synthetic */ NewsComment val$comm;

        AnonymousClass25(NewsComment newsComment) {
            this.val$comm = newsComment;
        }

        public void success(int likes, int retweets, int postID) {
            this.val$comm.isLiked = !this.val$comm.isLiked;
            this.val$comm.numLikes = likes;
            PostViewFragment.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PostViewFragment.this.getActivity(), C0436R.string.error, PostViewFragment.TYPE_PHOTO).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.26 */
    class AnonymousClass26 implements WallDeleteComment.Callback {
        private final /* synthetic */ int val$cid;

        AnonymousClass26(int i) {
            this.val$cid = i;
        }

        public void success() {
            Iterator it = PostViewFragment.this.comments.iterator();
            while (it.hasNext()) {
                NewsComment c = (NewsComment) it.next();
                if (c.cid == this.val$cid) {
                    c.isDeleted = true;
                    NewsEntry access$0 = PostViewFragment.this.f187e;
                    access$0.numComments--;
                    break;
                }
            }
            PostViewFragment.this.updateList();
            PostViewFragment.this.broadcastUpdate();
        }

        public void fail(int ecode, String emsg) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.29 */
    class AnonymousClass29 implements WallAddComment.Callback {
        private final /* synthetic */ ArrayList val$atts;
        private final /* synthetic */ String val$txt;

        /* renamed from: com.vkontakte.android.fragments.PostViewFragment.29.1 */
        class C07641 implements Runnable {

            /* renamed from: com.vkontakte.android.fragments.PostViewFragment.29.1.1 */
            class C07631 implements Runnable {
                C07631() {
                }

                public void run() {
                    PostViewFragment.this.list.setSelection(99999999);
                }
            }

            C07641() {
            }

            public void run() {
                ((InputMethodManager) PostViewFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(PostViewFragment.this.getActivity().getCurrentFocus().getWindowToken(), PostViewFragment.RESULT_DELETED);
                PostViewFragment.this.getActivity().getCurrentFocus().clearFocus();
                if (PostViewFragment.this.emojiPopup.isShowing()) {
                    PostViewFragment.this.emojiPopup.hide();
                }
                PostViewFragment.this.list.postDelayed(new C07631(), 200);
            }
        }

        AnonymousClass29(String str, ArrayList arrayList) {
            this.val$txt = str;
            this.val$atts = arrayList;
        }

        public void success(int cid) {
            PostViewFragment.this.commentBar.setText(ACRAConstants.DEFAULT_STRING_VALUE);
            SharedPreferences prefs = PostViewFragment.this.getActivity().getSharedPreferences(null, 0);
            NewsComment comment = new NewsComment();
            comment.cid = cid;
            comment.setText(this.val$txt.replaceAll("\\[(id|club)([\\d]+)\\|([^\\]]+)\\]", "$3"));
            comment.uid = Global.uid;
            comment.userName = prefs.getString("username", "DELETED");
            comment.userPhoto = prefs.getString("userphoto", "http://vkontakte.ru/images/question_b.gif");
            comment.links = new Vector();
            comment.linkTitles = new Vector();
            comment.attachments = this.val$atts;
            DisplayMetrics metrics = VKApplication.context.getResources().getDisplayMetrics();
            int tSize = Math.min(Math.min(metrics.widthPixels, metrics.heightPixels) - Global.scale(95.0f), 604);
            ZhukovLayout.processThumbs(tSize, (int) (((float) tSize) * 0.666f), comment.attachments);
            Matcher matcher = Pattern.compile("\\[(id|club)([\\d-]+)\\|([^\\]]+)\\]").matcher(this.val$txt);
            while (matcher.find()) {
                comment.links.add("vkontakte://profile/" + ("club".equals(matcher.group(PostViewFragment.TYPE_PHOTO)) ? "-" : ACRAConstants.DEFAULT_STRING_VALUE) + matcher.group(PostViewFragment.RESULT_DELETED));
                comment.linkTitles.add(matcher.group(3));
            }
            ArrayList<String> lnks = Global.extractLinks(comment.text);
            Iterator it = lnks.iterator();
            while (it.hasNext()) {
                comment.links.add("vklink://view/?" + ((String) it.next()));
            }
            comment.linkTitles.addAll(lnks);
            comment.time = (int) (System.currentTimeMillis() / 1000);
            if (PostViewFragment.this.replyTo > 0) {
                comment.resp_to = PostViewFragment.this.replyTo;
                comment.respToName = PostViewFragment.this.replyToRName;
            }
            PostViewFragment.this.comments.add(comment);
            PostViewFragment.this.updateList();
            PostViewFragment.this.replyTo = -1;
            PostViewFragment.this.list.post(new C07641());
            NewsEntry access$0 = PostViewFragment.this.f187e;
            access$0.numComments += PostViewFragment.TYPE_PHOTO;
            Intent intent = new Intent(Posts.ACTION_POST_UPDATED_BROADCAST);
            intent.putExtra("post_id", PostViewFragment.this.f187e.postID);
            intent.putExtra("owner_id", PostViewFragment.this.f187e.ownerID);
            intent.putExtra("comments", PostViewFragment.this.f187e.numComments);
            intent.putExtra("likes", PostViewFragment.this.f187e.numLikes);
            intent.putExtra("liked", PostViewFragment.this.f187e.flag(8));
            PostViewFragment.this.getActivity().sendBroadcast(intent);
            NewsfeedCache.update(VKApplication.context, PostViewFragment.this.f187e.ownerID, PostViewFragment.this.f187e.postID, PostViewFragment.this.f187e.numLikes, PostViewFragment.this.f187e.numComments, PostViewFragment.this.f187e.numRetweets, PostViewFragment.this.f187e.flag(8), PostViewFragment.this.f187e.flag(4));
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PostViewFragment.this.getActivity(), C0436R.string.err_text, PostViewFragment.TYPE_PHOTO).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.32 */
    class AnonymousClass32 implements WallGetComments.Callback {
        private final /* synthetic */ boolean val$clear;

        AnonymousClass32(boolean z) {
            this.val$clear = z;
        }

        public void success(int total, ArrayList<NewsComment> comments, ArrayList<String> likes, int likesCount, boolean canComment) {
            if (PostViewFragment.this.getActivity() != null) {
                int i;
                PostViewFragment.this.currentReq = null;
                PostViewFragment.this.loadingComments = false;
                PostViewFragment.this.lastUpdated = System.currentTimeMillis();
                if (!canComment) {
                    PostViewFragment.this.commentBar.setVisibility(8);
                    PostViewFragment.this.f187e.flag(PostViewFragment.RESULT_DELETED, false);
                    PostViewFragment.this.getSherlockActivity().invalidateOptionsMenu();
                }
                PostViewFragment.this.f187e.numComments = total;
                if (likesCount != -1 && this.val$clear) {
                    PostViewFragment.this.likePhotos.clear();
                    PostViewFragment.this.likePhotos.addAll(likes);
                    PostViewFragment.this.likePhotos.add(0, PostViewFragment.this.getActivity().getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE));
                    PostViewFragment.this.f187e.numLikes = likesCount;
                    PostViewFragment.this.updateLikePhotos();
                    PostViewFragment.this.updateButtons();
                }
                PostViewFragment.this.broadcastUpdate();
                PostViewFragment.this.headerView.findViewById(C0436R.id.divider).setVisibility(total > 0 ? 0 : 8);
                int firstVisible = PostViewFragment.this.list.getFirstVisiblePosition();
                int itemOffset = -1;
                if (firstVisible == 0) {
                    if (PostViewFragment.this.list.getChildCount() > PostViewFragment.RESULT_DELETED) {
                        firstVisible += PostViewFragment.TYPE_PHOTO;
                        itemOffset = PostViewFragment.this.list.getChildAt(PostViewFragment.RESULT_DELETED).getTop();
                    }
                } else if (PostViewFragment.this.list.getChildCount() > PostViewFragment.TYPE_PHOTO) {
                    itemOffset = PostViewFragment.this.list.getChildAt(PostViewFragment.TYPE_PHOTO).getTop();
                }
                DisplayMetrics metrics = VKApplication.context.getResources().getDisplayMetrics();
                int tSize = Math.min(Math.min(metrics.widthPixels, metrics.heightPixels) - Global.scale(95.0f), 604);
                Iterator it = comments.iterator();
                while (it.hasNext()) {
                    ZhukovLayout.processThumbs(tSize, (int) (((float) tSize) * 0.666f), ((NewsComment) it.next()).attachments);
                }
                if (PostViewFragment.this.commentsProgress.getParent() != null) {
                    ((ViewGroup) PostViewFragment.this.commentsProgress.getParent()).removeView(PostViewFragment.this.commentsProgress);
                }
                PostViewFragment.this.list.refreshDone();
                if (this.val$clear) {
                    PostViewFragment.this.comments.clear();
                }
                for (i = 0; i < comments.size(); i += PostViewFragment.TYPE_PHOTO) {
                    NewsComment comm = (NewsComment) comments.get(i);
                    comm.setText(comm.text);
                    comments.set(i, comm);
                }
                PostViewFragment.this.comments.addAll(0, comments);
                if (total > PostViewFragment.this.comments.size()) {
                    PostViewFragment.this.loadMoreView.setVisibility(0);
                    PostViewFragment.this.loadMoreView.showProgress(false);
                    PostViewFragment.this.loadMoreView.setNumComments(total - PostViewFragment.this.comments.size());
                } else {
                    PostViewFragment.this.loadMoreView.setVisibility(8);
                }
                PostViewFragment.this.updateList();
                if (PostViewFragment.this.scrollToComment != 0) {
                    int pos = -1;
                    i = 0;
                    it = PostViewFragment.this.comments.iterator();
                    while (it.hasNext()) {
                        if (((NewsComment) it.next()).cid == PostViewFragment.this.scrollToComment) {
                            pos = i;
                            break;
                        }
                        i += PostViewFragment.TYPE_PHOTO;
                    }
                    if (pos != -1) {
                        pos += PostViewFragment.this.list.getHeaderViewsCount();
                        PostViewFragment.this.list.setSelectionFromTop(pos, Global.scale(50.0f));
                        PostViewFragment.this.highlightComment(pos);
                    }
                    PostViewFragment.this.scrollToComment = 0;
                } else if (PostViewFragment.this.comments.size() > 10) {
                    PostViewFragment.this.list.setSelectionFromTop((firstVisible + PostViewFragment.TYPE_PHOTO) + comments.size(), itemOffset);
                }
            }
        }

        public void fail(int ecode, String emsg) {
            PostViewFragment.this.loadingComments = false;
            PostViewFragment.this.list.refreshDone();
            if (PostViewFragment.this.comments.size() == 0) {
                PostViewFragment.this.error.setErrorInfo(ecode, emsg);
                PostViewFragment.this.commentsProgress.setVisibility(8);
                PostViewFragment.this.error.setVisibility(0);
            } else {
                Toast.makeText(PostViewFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
            }
            PostViewFragment.this.currentReq = null;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.36 */
    class AnonymousClass36 implements WallLike.Callback {
        private final /* synthetic */ boolean val$liked;

        /* renamed from: com.vkontakte.android.fragments.PostViewFragment.36.1 */
        class C07671 implements Runnable {
            C07671() {
            }

            public void run() {
                PostViewFragment.this.like(PostViewFragment.this.f187e.flag(8));
            }
        }

        /* renamed from: com.vkontakte.android.fragments.PostViewFragment.36.2 */
        class C07682 implements Runnable {
            C07682() {
            }

            public void run() {
                PostViewFragment.this.updateButtons();
            }
        }

        /* renamed from: com.vkontakte.android.fragments.PostViewFragment.36.3 */
        class C07693 implements Runnable {
            C07693() {
            }

            public void run() {
                Toast.makeText(PostViewFragment.this.getActivity(), C0436R.string.error, 0).show();
                PostViewFragment.this.updateButtons();
            }
        }

        AnonymousClass36(boolean z) {
            this.val$liked = z;
        }

        public void success(int likes, int retweets, int postID) {
            PostViewFragment.this.f187e.numLikes = likes;
            if (this.val$liked) {
                PostViewFragment.this.f187e.numRetweets = retweets;
            }
            PostViewFragment.this.broadcastUpdate();
            PostViewFragment.this.liking = false;
            if (PostViewFragment.this.f187e.flag(8) != this.val$liked) {
                PostViewFragment.this.contentView.post(new C07671());
            } else {
                PostViewFragment.this.contentView.post(new C07682());
            }
        }

        public void fail(int ecode, String emsg) {
            PostViewFragment.this.f187e.flag(8, !this.val$liked);
            NewsEntry access$0;
            if (this.val$liked) {
                access$0 = PostViewFragment.this.f187e;
                access$0.numLikes--;
            } else {
                access$0 = PostViewFragment.this.f187e;
                access$0.numLikes += PostViewFragment.TYPE_PHOTO;
            }
            PostViewFragment.this.liking = false;
            PostViewFragment.this.contentView.post(new C07693());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PostViewFragment.37 */
    class AnonymousClass37 implements WallRestoreComment.Callback {
        private final /* synthetic */ int val$cid;

        AnonymousClass37(int i) {
            this.val$cid = i;
        }

        public void success() {
            Iterator it = PostViewFragment.this.comments.iterator();
            while (it.hasNext()) {
                NewsComment c = (NewsComment) it.next();
                if (c.cid == this.val$cid) {
                    NewsEntry access$0 = PostViewFragment.this.f187e;
                    access$0.numComments += PostViewFragment.TYPE_PHOTO;
                    c.isDeleted = false;
                    break;
                }
            }
            PostViewFragment.this.updateList();
            PostViewFragment.this.broadcastUpdate();
        }

        public void fail(int ecode, String emsg) {
        }
    }

    private class PostCommentsImagesAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.PostViewFragment.PostCommentsImagesAdapter.1 */
        class C07801 implements Runnable {
            private final /* synthetic */ int val$_image;
            private final /* synthetic */ Bitmap val$bitmap;

            C07801(int i, Bitmap bitmap) {
                this.val$_image = i;
                this.val$bitmap = bitmap;
            }

            public void run() {
                int image = this.val$_image;
                if (image == 0) {
                    try {
                        PostViewFragment.this.headerView.findViewById(C0436R.id.wall_user_photo).setTag(Integer.valueOf(PostViewFragment.TYPE_PHOTO));
                        ((ImageView) PostViewFragment.this.headerView.findViewById(C0436R.id.wall_user_photo)).setImageBitmap(this.val$bitmap);
                    } catch (Throwable x) {
                        Log.m532w("vk", x);
                    }
                } else if (image == PostViewFragment.TYPE_PHOTO && PostViewFragment.this.f187e.flag(32)) {
                    ((ImageView) PostViewFragment.this.headerView.findViewById(C0436R.id.wall_retweet_photo)).setImageBitmap(this.val$bitmap);
                } else {
                    if (PostViewFragment.this.f187e.flag(32)) {
                        image--;
                    }
                    if (image - 1 < PostViewFragment.this.f187e.getImageCount()) {
                        PostViewFragment.this.f187e.getImageAttachment(image - 1).setImage(PostViewFragment.this.getImageAttachView(image - 1), this.val$bitmap, false);
                        return;
                    }
                    ((ImageView) ((LinearLayout) PostViewFragment.this.contentView.findViewById(C0436R.id.wall_view_like_photos)).getChildAt((image - PostViewFragment.this.f187e.getImageCount()) - 1)).setImageBitmap(this.val$bitmap);
                }
            }
        }

        /* renamed from: com.vkontakte.android.fragments.PostViewFragment.PostCommentsImagesAdapter.2 */
        class C07812 implements Runnable {
            private final /* synthetic */ int val$_image;
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$item;

            C07812(int i, int i2, Bitmap bitmap) {
                this.val$item = i;
                this.val$_image = i2;
                this.val$bitmap = bitmap;
            }

            public void run() {
                View view = PostViewFragment.this.list.getChildAt(this.val$item - PostViewFragment.this.list.getFirstVisiblePosition());
                int image = this.val$_image;
                if (image == 0) {
                    try {
                        ((ImageView) view.findViewById(C0436R.id.poster_photo)).setImageBitmap(this.val$bitmap);
                        return;
                    } catch (Throwable x) {
                        Log.m532w("vk", x);
                        return;
                    }
                }
                int imgindex = 0;
                int index = 0;
                Iterator it = ((NewsComment) PostViewFragment.this.comments.get(this.val$item - 1)).attachments.iterator();
                while (it.hasNext()) {
                    Attachment att = (Attachment) it.next();
                    if (att instanceof PhotoAttachment) {
                        imgindex += PostViewFragment.TYPE_PHOTO;
                        if (imgindex == image) {
                            ((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(index).setTag(Integer.valueOf(PostViewFragment.TYPE_PHOTO));
                            ((ImageView) ((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(index)).setImageBitmap(this.val$bitmap);
                            ((FixedSizeImageView) ((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(index)).animateAlpha();
                            return;
                        }
                    }
                    if (att instanceof VideoAttachment) {
                        imgindex += PostViewFragment.TYPE_PHOTO;
                        if (imgindex == image) {
                            ((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(index).setTag(Integer.valueOf(PostViewFragment.TYPE_PHOTO));
                            ((VideoAttachView) ((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(index)).setImageBitmap(this.val$bitmap);
                            return;
                        }
                    }
                    if (att instanceof DocumentAttachment) {
                        if (((DocumentAttachment) att).thumb != null && ((DocumentAttachment) att).thumb.length() > 0) {
                            imgindex += PostViewFragment.TYPE_PHOTO;
                            if (imgindex == image) {
                                ((ViewGroup) PostViewFragment.this.headerView.findViewById(C0436R.id.wall_view_attach)).getChildAt(index).setTag(Integer.valueOf(PostViewFragment.TYPE_PHOTO));
                                ((ImageView) ((ViewGroup) ((ViewGroup) view.findViewById(C0436R.id.post_attach_container)).getChildAt(index)).findViewById(C0436R.id.att_doc_thumb)).setImageBitmap(this.val$bitmap);
                                return;
                            }
                        }
                    }
                    index += PostViewFragment.TYPE_PHOTO;
                }
            }
        }

        private PostCommentsImagesAdapter() {
        }

        public int getItemCount() {
            return PostViewFragment.this.comments.size() + PostViewFragment.TYPE_PHOTO;
        }

        public int getImageCountForItem(int item) {
            int count = PostViewFragment.TYPE_PHOTO;
            if (item == 0) {
                if (PostViewFragment.this.f187e.flag(32)) {
                    count = PostViewFragment.TYPE_PHOTO + PostViewFragment.TYPE_PHOTO;
                }
                return (count + PostViewFragment.this.f187e.getImageCount()) + Math.min(PostViewFragment.this.likePhotos.size(), 10);
            }
            Iterator it = ((NewsComment) PostViewFragment.this.comments.get(item - 1)).attachments.iterator();
            while (it.hasNext()) {
                Attachment att = (Attachment) it.next();
                if ((att instanceof PhotoAttachment) || (att instanceof VideoAttachment) || ((att instanceof DocumentAttachment) && ((DocumentAttachment) att).thumb != null && ((DocumentAttachment) att).thumb.length() > 0)) {
                    count += PostViewFragment.TYPE_PHOTO;
                }
            }
            return count;
        }

        public String getImageURL(int item, int image) {
            if (item == 0) {
                switch (image) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                        return PostViewFragment.this.f187e.userPhotoURL;
                    case PostViewFragment.TYPE_PHOTO /*1*/:
                        if (PostViewFragment.this.f187e.flag(32)) {
                            return PostViewFragment.this.f187e.retweetUserPhoto;
                        }
                        break;
                }
                if (PostViewFragment.this.f187e.flag(32)) {
                    image--;
                }
                if (image - 1 < PostViewFragment.this.f187e.getImageCount()) {
                    return PostViewFragment.this.f187e.getImageAttachment(image - 1).getImageURL();
                }
                return (String) PostViewFragment.this.likePhotos.get((image - PostViewFragment.this.f187e.getImageCount()) - 1);
            }
            item--;
            if (image == 0) {
                return ((NewsComment) PostViewFragment.this.comments.get(item)).userPhoto;
            }
            int imgindex = 0;
            Iterator it = ((NewsComment) PostViewFragment.this.comments.get(item)).attachments.iterator();
            while (it.hasNext()) {
                Attachment att = (Attachment) it.next();
                if (att instanceof PhotoAttachment) {
                    imgindex += PostViewFragment.TYPE_PHOTO;
                    if (imgindex == image) {
                        return ((PhotoAttachment) att).getThumbURL();
                    }
                }
                if (att instanceof VideoAttachment) {
                    imgindex += PostViewFragment.TYPE_PHOTO;
                    if (imgindex == image) {
                        return ((VideoAttachment) att).image;
                    }
                }
                if ((att instanceof DocumentAttachment) && ((DocumentAttachment) att).thumb != null && ((DocumentAttachment) att).thumb.length() > 0) {
                    imgindex += PostViewFragment.TYPE_PHOTO;
                    if (imgindex == image) {
                        return ((DocumentAttachment) att).thumb;
                    }
                }
            }
            return ACRAConstants.DEFAULT_STRING_VALUE;
        }

        public void imageLoaded(int item, int _image, Bitmap bitmap) {
            if (item == 0) {
                PostViewFragment.this.headerView.post(new C07801(_image, bitmap));
            } else if (item >= PostViewFragment.this.list.getFirstVisiblePosition() && item <= PostViewFragment.this.list.getLastVisiblePosition()) {
                PostViewFragment.this.headerView.post(new C07812(item, _image, bitmap));
            }
        }
    }

    public PostViewFragment() {
        this.comments = new ArrayList();
        this.loadingComments = false;
        this.canAdmin = false;
        this.replyTo = -1;
        this.replyToUid = -1;
        this.replyToRName = null;
        this.replyToName = null;
        this.photos = new ArrayList();
        this.repostPhotos = new ArrayList();
        this.type = 0;
        this.videoBottomPadding = false;
        this.likePhotos = new ArrayList();
        this.liking = false;
        this.maxLikePhotos = 0;
        this.maxLikeVisible = 0;
        this.receiver = new C07611();
        this.narrowScreen = false;
        this.accessKey = null;
        this.scrollToComment = 0;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onAttach(android.app.Activity r43) {
        /*
        r42 = this;
        super.onAttach(r43);
        r35 = r42.getArguments();
        r36 = "entry";
        r35 = r35.getParcelable(r36);
        r35 = (com.vkontakte.android.NewsEntry) r35;
        r0 = r35;
        r1 = r42;
        r1.f187e = r0;
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.userID;
        r35 = r0;
        if (r35 != 0) goto L_0x003b;
    L_0x0023:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r42;
        r0 = r0.f187e;
        r36 = r0;
        r0 = r36;
        r0 = r0.ownerID;
        r36 = r0;
        r0 = r36;
        r1 = r35;
        r1.userID = r0;
    L_0x003b:
        r15 = "";
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        if (r35 != 0) goto L_0x014d;
    L_0x004b:
        r35 = new java.lang.StringBuilder;
        r36 = java.lang.String.valueOf(r15);
        r35.<init>(r36);
        r36 = "wall";
        r35 = r35.append(r36);
        r15 = r35.toString();
    L_0x005e:
        r35 = new java.lang.StringBuilder;
        r36 = java.lang.String.valueOf(r15);
        r35.<init>(r36);
        r0 = r42;
        r0 = r0.f187e;
        r36 = r0;
        r0 = r36;
        r0 = r0.ownerID;
        r36 = r0;
        r35 = r35.append(r36);
        r36 = "_";
        r35 = r35.append(r36);
        r0 = r42;
        r0 = r0.f187e;
        r36 = r0;
        r0 = r36;
        r0 = r0.postID;
        r36 = r0;
        r35 = r35.append(r36);
        r15 = r35.toString();
        r0 = r43;
        com.vkontakte.android.ActivityUtils.setBeamLink(r0, r15);
        r35 = r42.getArguments();
        r36 = "type";
        r37 = 0;
        r35 = r35.getInt(r36, r37);
        r0 = r35;
        r1 = r42;
        r1.type = r0;
        r26 = r42.getSherlockActivity();
        r35 = r26.getSupportActionBar();
        r36 = 0;
        r35.setNavigationMode(r36);
        r35 = r26.getSupportActionBar();
        r36 = 1;
        r35.setDisplayShowTitleEnabled(r36);
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        r36 = 1;
        r0 = r35;
        r1 = r36;
        if (r0 != r1) goto L_0x01c8;
    L_0x00d2:
        r35 = 2131230766; // 0x7f08002e float:1.8077594E38 double:1.052967905E-314;
        r0 = r43;
        r1 = r35;
        r0.setTitle(r1);
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.attachments;
        r35 = r0;
        r36 = 0;
        r35 = r35.get(r36);
        r35 = (com.vkontakte.android.PhotoAttachment) r35;
        r0 = r35;
        r0 = r0.accessKey;
        r35 = r0;
        r0 = r35;
        r1 = r42;
        r1.accessKey = r0;
    L_0x00fc:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        if (r35 != 0) goto L_0x0113;
    L_0x010a:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        com.vkontakte.android.data.Posts.trackPostView(r35);
    L_0x0113:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        if (r35 == 0) goto L_0x0214;
    L_0x0121:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        r36 = 1;
        r0 = r35;
        r1 = r36;
        if (r0 == r1) goto L_0x0214;
    L_0x0135:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        r36 = 2;
        r0 = r35;
        r1 = r36;
        if (r0 == r1) goto L_0x0214;
    L_0x0149:
        r43.finish();
    L_0x014c:
        return;
    L_0x014d:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        r36 = 2;
        r0 = r35;
        r1 = r36;
        if (r0 != r1) goto L_0x0176;
    L_0x0161:
        r35 = new java.lang.StringBuilder;
        r36 = java.lang.String.valueOf(r15);
        r35.<init>(r36);
        r36 = "video";
        r35 = r35.append(r36);
        r15 = r35.toString();
        goto L_0x005e;
    L_0x0176:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        r36 = 1;
        r0 = r35;
        r1 = r36;
        if (r0 != r1) goto L_0x019f;
    L_0x018a:
        r35 = new java.lang.StringBuilder;
        r36 = java.lang.String.valueOf(r15);
        r35.<init>(r36);
        r36 = "photo";
        r35 = r35.append(r36);
        r15 = r35.toString();
        goto L_0x005e;
    L_0x019f:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        r36 = 4;
        r0 = r35;
        r1 = r36;
        if (r0 != r1) goto L_0x005e;
    L_0x01b3:
        r35 = new java.lang.StringBuilder;
        r36 = java.lang.String.valueOf(r15);
        r35.<init>(r36);
        r36 = "topic";
        r35 = r35.append(r36);
        r15 = r35.toString();
        goto L_0x005e;
    L_0x01c8:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        r36 = 2;
        r0 = r35;
        r1 = r36;
        if (r0 != r1) goto L_0x0208;
    L_0x01dc:
        r35 = 2131230768; // 0x7f080030 float:1.8077598E38 double:1.052967906E-314;
        r0 = r43;
        r1 = r35;
        r0.setTitle(r1);
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.attachments;
        r35 = r0;
        r36 = 0;
        r35 = r35.get(r36);
        r35 = (com.vkontakte.android.VideoAttachment) r35;
        r0 = r35;
        r0 = r0.accessKey;
        r35 = r0;
        r0 = r35;
        r1 = r42;
        r1.accessKey = r0;
        goto L_0x00fc;
    L_0x0208:
        r35 = 2131230790; // 0x7f080046 float:1.8077643E38 double:1.0529679167E-314;
        r0 = r43;
        r1 = r35;
        r0.setTitle(r1);
        goto L_0x00fc;
    L_0x0214:
        r35 = r42.getArguments();
        r36 = "comment";
        r35 = r35.getInt(r36);
        r0 = r35;
        r1 = r42;
        r1.scrollToComment = r0;
        r35 = com.vkontakte.android.VKApplication.context;
        r35 = r35.getResources();
        r19 = r35.getDisplayMetrics();
        r0 = r19;
        r0 = r0.widthPixels;
        r35 = r0;
        r0 = r19;
        r0 = r0.heightPixels;
        r36 = r0;
        r35 = java.lang.Math.min(r35, r36);
        r36 = 1084227584; // 0x40a00000 float:5.0 double:5.356796015E-315;
        r36 = com.vkontakte.android.Global.scale(r36);
        r36 = r36 * 2;
        r0 = r36;
        r0 = (float) r0;
        r36 = r0;
        r36 = java.lang.Math.round(r36);
        r29 = r35 - r36;
        r0 = r29;
        r0 = (float) r0;
        r35 = r0;
        r36 = 1059749626; // 0x3f2a7efa float:0.666 double:5.235858834E-315;
        r35 = r35 * r36;
        r0 = r35;
        r0 = (int) r0;
        r35 = r0;
        r0 = r42;
        r0 = r0.f187e;
        r36 = r0;
        r0 = r36;
        r0 = r0.attachments;
        r36 = r0;
        r0 = r29;
        r1 = r35;
        r2 = r36;
        com.vkontakte.android.ZhukovLayout.processThumbs(r0, r1, r2);
        r35 = new com.vkontakte.android.fragments.PostViewFragment$2;
        r0 = r35;
        r1 = r42;
        r2 = r43;
        r0.<init>(r2);
        r0 = r35;
        r1 = r42;
        r1.contentView = r0;
        r0 = r42;
        r0 = r0.contentView;
        r35 = r0;
        r36 = 1;
        r35.setOrientation(r36);
        r35 = new com.vkontakte.android.ui.RefreshableListView;
        r36 = 0;
        r0 = r35;
        r1 = r43;
        r2 = r36;
        r0.<init>(r1, r2);
        r0 = r35;
        r1 = r42;
        r1.list = r0;
        r17 = new android.widget.LinearLayout$LayoutParams;
        r35 = -1;
        r36 = -1;
        r0 = r17;
        r1 = r35;
        r2 = r36;
        r0.<init>(r1, r2);
        r35 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r0 = r35;
        r1 = r17;
        r1.weight = r0;
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r0 = r35;
        r1 = r17;
        r0.setLayoutParams(r1);
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = 0;
        r35.setVerticalFadingEdgeEnabled(r36);
        r35 = new com.vkontakte.android.EmojiPopup;
        r36 = r42.getActivity();
        r0 = r42;
        r0 = r0.contentView;
        r37 = r0;
        r38 = 2130837892; // 0x7f020184 float:1.728075E38 double:1.0527737993E-314;
        r39 = 0;
        r35.<init>(r36, r37, r38, r39);
        r0 = r35;
        r1 = r42;
        r1.emojiPopup = r0;
        r0 = r42;
        r0 = r0.contentView;
        r35 = r0;
        r0 = r42;
        r0 = r0.list;
        r36 = r0;
        r37 = new android.widget.LinearLayout$LayoutParams;
        r38 = -1;
        r39 = -1;
        r40 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r37.<init>(r38, r39, r40);
        r35.addView(r36, r37);
        r0 = r42;
        r0 = r0.contentView;
        r35 = r0;
        r36 = -1;
        r35.setBackgroundColor(r36);
        r35 = new com.vkontakte.android.ui.WriteBar;
        r0 = r35;
        r1 = r43;
        r0.<init>(r1);
        r0 = r35;
        r1 = r42;
        r1.commentBar = r0;
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r36 = 2;
        r35 = r35.flag(r36);
        if (r35 == 0) goto L_0x0393;
    L_0x032f:
        r27 = new android.view.View;
        r0 = r27;
        r1 = r43;
        r0.<init>(r1);
        r35 = 2130837686; // 0x7f0200b6 float:1.7280333E38 double:1.0527736975E-314;
        r0 = r27;
        r1 = r35;
        r0.setBackgroundResource(r1);
        r16 = new android.widget.LinearLayout$LayoutParams;
        r35 = -1;
        r36 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r36 = com.vkontakte.android.Global.scale(r36);
        r0 = r16;
        r1 = r35;
        r2 = r36;
        r0.<init>(r1, r2);
        r35 = -1073741824; // 0xffffffffc0000000 float:-2.0 double:NaN;
        r35 = com.vkontakte.android.Global.scale(r35);
        r0 = r35;
        r1 = r16;
        r1.topMargin = r0;
        r0 = r42;
        r0 = r0.contentView;
        r35 = r0;
        r0 = r35;
        r1 = r27;
        r2 = r16;
        r0.addView(r1, r2);
        r0 = r42;
        r0 = r0.contentView;
        r35 = r0;
        r0 = r42;
        r0 = r0.commentBar;
        r36 = r0;
        r35.addView(r36);
        r0 = r42;
        r0 = r0.commentBar;
        r35 = r0;
        r36 = 2131296762; // 0x7f0901fa float:1.821145E38 double:1.053000511E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.widget.TextView) r35;
        r36 = 268435456; // 0x10000000 float:2.5243549E-29 double:1.32624737E-315;
        r35.setImeOptions(r36);
    L_0x0393:
        r35 = 2130903153; // 0x7f030071 float:1.7413116E38 double:1.0528060425E-314;
        r36 = 0;
        r0 = r43;
        r1 = r35;
        r2 = r36;
        r35 = android.view.View.inflate(r0, r1, r2);
        r0 = r35;
        r1 = r42;
        r1.headerView = r0;
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = new android.graphics.drawable.ColorDrawable;
        r37 = -1710619; // 0xffffffffffe5e5e5 float:NaN double:NaN;
        r36.<init>(r37);
        r35.setDivider(r36);
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r36 = com.vkontakte.android.Global.scale(r36);
        r35.setDividerHeight(r36);
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r0 = r42;
        r0 = r0.headerView;
        r36 = r0;
        r37 = 0;
        r38 = 0;
        r35.addHeaderView(r36, r37, r38);
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = new com.vkontakte.android.fragments.PostViewFragment$PostCommentsAdapter;
        r37 = 0;
        r0 = r36;
        r1 = r42;
        r2 = r37;
        r0.<init>(r2);
        r0 = r36;
        r1 = r42;
        r1.adapter = r0;
        r35.setAdapter(r36);
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r0 = r35;
        r1 = r42;
        r0.setOnRefreshListener(r1);
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = -2039584; // 0xffffffffffe0e0e0 float:NaN double:NaN;
        r35.setTopColor(r36);
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = 0;
        r35.setHeaderDividersEnabled(r36);
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = 0;
        r35.setFooterDividersEnabled(r36);
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = 2130837734; // 0x7f0200e6 float:1.728043E38 double:1.052773721E-314;
        r35.setSelector(r36);
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = 1;
        r35.setDrawSelectorOnTop(r36);
        r35 = android.os.Build.VERSION.SDK_INT;
        r36 = 11;
        r0 = r35;
        r1 = r36;
        if (r0 > r1) goto L_0x045d;
    L_0x0447:
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = -1;
        r35.setBackgroundColor(r36);
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = -1;
        r35.setCacheColorHint(r36);
    L_0x045d:
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = 0;
        r35.setFromTop(r36);
        r35 = r42.getArguments();
        r36 = "is_admin";
        r37 = 0;
        r35 = r35.getBoolean(r36, r37);
        if (r35 != 0) goto L_0x0bc1;
    L_0x0476:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r36 = 64;
        r35 = r35.flag(r36);
        if (r35 != 0) goto L_0x0bc1;
    L_0x0484:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.userID;
        r35 = r0;
        r36 = com.vkontakte.android.Global.uid;
        r0 = r35;
        r1 = r36;
        if (r0 == r1) goto L_0x0bc1;
    L_0x0498:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.ownerID;
        r35 = r0;
        r36 = com.vkontakte.android.Global.uid;
        r0 = r35;
        r1 = r36;
        if (r0 == r1) goto L_0x0bc1;
    L_0x04ac:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.ownerID;
        r35 = r0;
        if (r35 == 0) goto L_0x0bc1;
    L_0x04ba:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.ownerID;
        r35 = r0;
        r0 = r35;
        r0 = -r0;
        r35 = r0;
        r35 = com.vkontakte.android.data.Groups.getAdminLevel(r35);
        r36 = 1;
        r0 = r35;
        r1 = r36;
        if (r0 >= r1) goto L_0x0bc1;
    L_0x04d7:
        r35 = 0;
    L_0x04d9:
        r0 = r35;
        r1 = r42;
        r1.canAdmin = r0;
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296589; // 0x7f09014d float:1.8211099E38 double:1.0530004257E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.widget.TextView) r35;
        r36 = android.text.method.LinkMovementMethod.getInstance();
        r35.setMovementMethod(r36);
        r35 = r42.getActivity();
        r35 = android.preference.PreferenceManager.getDefaultSharedPreferences(r35);
        r36 = "fontSize";
        r37 = "0";
        r35 = r35.getString(r36, r37);
        r14 = java.lang.Integer.parseInt(r35);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296592; // 0x7f090150 float:1.8211105E38 double:1.053000427E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.widget.TextView) r35;
        r36 = 1;
        r37 = 1098907648; // 0x41800000 float:16.0 double:5.42932517E-315;
        r0 = (float) r14;
        r38 = r0;
        r39 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r38 = r38 * r39;
        r37 = r37 + r38;
        r35.setTextSize(r36, r37);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296589; // 0x7f09014d float:1.8211099E38 double:1.0530004257E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.widget.TextView) r35;
        r36 = 1;
        r37 = 1098907648; // 0x41800000 float:16.0 double:5.42932517E-315;
        r0 = (float) r14;
        r38 = r0;
        r39 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r38 = r38 * r39;
        r37 = r37 + r38;
        r35.setTextSize(r36, r37);
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.text;
        r35 = r0;
        r35 = r35.length();
        if (r35 <= 0) goto L_0x0bc5;
    L_0x0559:
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296592; // 0x7f090150 float:1.8211105E38 double:1.053000427E-314;
        r35 = r35.findViewById(r36);
        r35 = (com.vkontakte.android.ExTextView) r35;
        r0 = r42;
        r0 = r0.f187e;
        r36 = r0;
        r0 = r36;
        r0 = r0.text;
        r36 = r0;
        r37 = "\n";
        r38 = "<br/>";
        r36 = r36.replace(r37, r38);
        r36 = android.text.Html.fromHtml(r36);
        r36 = (android.text.Spannable) r36;
        r36 = com.vkontakte.android.NewsEntry.stripUnderlines(r36);
        r36 = com.vkontakte.android.Global.replaceEmoji(r36);
        r35.setText(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296592; // 0x7f090150 float:1.8211105E38 double:1.053000427E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.widget.TextView) r35;
        r36 = android.text.method.LinkMovementMethod.getInstance();
        r35.setMovementMethod(r36);
    L_0x05a3:
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296587; // 0x7f09014b float:1.8211095E38 double:1.0530004247E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.widget.TextView) r35;
        r0 = r42;
        r0 = r0.f187e;
        r36 = r0;
        r0 = r36;
        r0 = r0.userName;
        r36 = r0;
        r35.setText(r36);
        r35 = r42.getResources();
        r0 = r42;
        r0 = r0.f187e;
        r36 = r0;
        r0 = r36;
        r0 = r0.time;
        r36 = r0;
        r31 = com.vkontakte.android.Global.langDate(r35, r36);
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r36 = 512; // 0x200 float:7.175E-43 double:2.53E-321;
        r35 = r35.flag(r36);
        if (r35 == 0) goto L_0x0642;
    L_0x05e3:
        r8 = new android.text.SpannableStringBuilder;
        r0 = r31;
        r8.<init>(r0);
        r35 = android.text.Spannable.Factory.getInstance();
        r36 = "F";
        r28 = r35.newSpannable(r36);
        r35 = r42.getResources();
        r36 = 2130837935; // 0x7f0201af float:1.7280838E38 double:1.0527738205E-314;
        r9 = r35.getDrawable(r36);
        r35 = 0;
        r36 = 0;
        r37 = r9.getIntrinsicWidth();
        r38 = r9.getIntrinsicHeight();
        r0 = r35;
        r1 = r36;
        r2 = r37;
        r3 = r38;
        r9.setBounds(r0, r1, r2, r3);
        r35 = new android.text.style.ImageSpan;
        r36 = 1;
        r0 = r35;
        r1 = r36;
        r0.<init>(r9, r1);
        r36 = 0;
        r37 = 1;
        r38 = 0;
        r0 = r28;
        r1 = r35;
        r2 = r36;
        r3 = r37;
        r4 = r38;
        r0.setSpan(r1, r2, r3, r4);
        r35 = " ";
        r0 = r35;
        r8.append(r0);
        r0 = r28;
        r8.append(r0);
        r31 = r8;
    L_0x0642:
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296588; // 0x7f09014c float:1.8211097E38 double:1.053000425E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.widget.TextView) r35;
        r0 = r35;
        r1 = r31;
        r0.setText(r1);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296593; // 0x7f090151 float:1.8211107E38 double:1.0530004277E-314;
        r35 = r35.findViewById(r36);
        r35 = (com.vkontakte.android.ui.FlowLayout) r35;
        r36 = 1084227584; // 0x40a00000 float:5.0 double:5.356796015E-315;
        r36 = com.vkontakte.android.Global.scale(r36);
        r0 = r36;
        r1 = r35;
        r1.pwidth = r0;
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.attachments;
        r35 = r0;
        r35 = r35.size();
        if (r35 <= 0) goto L_0x06e3;
    L_0x0685:
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296593; // 0x7f090151 float:1.8211107E38 double:1.0530004277E-314;
        r35 = r35.findViewById(r36);
        r36 = 0;
        r35.setVisibility(r36);
        r20 = 0;
        r24 = 0;
        r30 = 0;
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.attachments;
        r35 = r0;
        r35 = r35.iterator();
    L_0x06ad:
        r36 = r35.hasNext();
        if (r36 != 0) goto L_0x0c35;
    L_0x06b3:
        r11 = 0;
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.attachments;
        r35 = r0;
        r37 = r35.iterator();
    L_0x06c4:
        r35 = r37.hasNext();
        if (r35 != 0) goto L_0x0c4d;
    L_0x06ca:
        if (r30 == 0) goto L_0x06e3;
    L_0x06cc:
        r16 = r30.getLayoutParams();
        r16 = (com.vkontakte.android.ui.FlowLayout.LayoutParams) r16;
        if (r16 == 0) goto L_0x06e3;
    L_0x06d4:
        r35 = 1;
        r0 = r35;
        r1 = r16;
        r1.breakAfter = r0;
        r0 = r30;
        r1 = r16;
        r0.setLayoutParams(r1);
    L_0x06e3:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.repostAttachments;
        r35 = r0;
        r35 = r35.size();
        if (r35 <= 0) goto L_0x0752;
    L_0x06f5:
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296590; // 0x7f09014e float:1.82111E38 double:1.053000426E-314;
        r35 = r35.findViewById(r36);
        r36 = 0;
        r35.setVisibility(r36);
        r20 = 0;
        r24 = 0;
        r30 = 0;
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.repostAttachments;
        r35 = r0;
        r35 = r35.iterator();
    L_0x071d:
        r36 = r35.hasNext();
        if (r36 != 0) goto L_0x0f39;
    L_0x0723:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.repostAttachments;
        r35 = r0;
        r36 = r35.iterator();
    L_0x0733:
        r35 = r36.hasNext();
        if (r35 != 0) goto L_0x0f51;
    L_0x0739:
        if (r30 == 0) goto L_0x0752;
    L_0x073b:
        r16 = r30.getLayoutParams();
        r16 = (com.vkontakte.android.ui.FlowLayout.LayoutParams) r16;
        if (r16 == 0) goto L_0x0752;
    L_0x0743:
        r35 = 1;
        r0 = r35;
        r1 = r16;
        r1.breakAfter = r0;
        r0 = r30;
        r1 = r16;
        r0.setLayoutParams(r1);
    L_0x0752:
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296585; // 0x7f090149 float:1.821109E38 double:1.0530004237E-314;
        r35 = r35.findViewById(r36);
        r36 = new com.vkontakte.android.fragments.PostViewFragment$5;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.setOnClickListener(r36);
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.retweetUID;
        r35 = r0;
        if (r35 == 0) goto L_0x08b1;
    L_0x0779:
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296591; // 0x7f09014f float:1.8211103E38 double:1.0530004267E-314;
        r35 = r35.findViewById(r36);
        r36 = 0;
        r35.setVisibility(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296592; // 0x7f090150 float:1.8211105E38 double:1.053000427E-314;
        r25 = r35.findViewById(r36);
        r35 = r25.getPaddingLeft();
        r36 = 0;
        r37 = r25.getPaddingRight();
        r38 = r25.getPaddingBottom();
        r0 = r25;
        r1 = r35;
        r2 = r36;
        r3 = r37;
        r4 = r38;
        r0.setPadding(r1, r2, r3, r4);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296591; // 0x7f09014f float:1.8211103E38 double:1.0530004267E-314;
        r35 = r35.findViewById(r36);
        r36 = new com.vkontakte.android.fragments.PostViewFragment$6;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.setOnClickListener(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296731; // 0x7f0901db float:1.8211387E38 double:1.053000496E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.widget.TextView) r35;
        r0 = r42;
        r0 = r0.f187e;
        r36 = r0;
        r0 = r36;
        r0 = r0.retweetUserName;
        r36 = r0;
        r35.setText(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296733; // 0x7f0901dd float:1.821139E38 double:1.053000497E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.widget.TextView) r35;
        r37 = new java.lang.StringBuilder;
        r0 = r42;
        r0 = r0.f187e;
        r36 = r0;
        r0 = r36;
        r0 = r0.retweetOrigTime;
        r36 = r0;
        if (r36 <= 0) goto L_0x0fd8;
    L_0x0809:
        r36 = r42.getResources();
        r0 = r42;
        r0 = r0.f187e;
        r38 = r0;
        r0 = r38;
        r0 = r0.retweetOrigTime;
        r38 = r0;
        r0 = r36;
        r1 = r38;
        r36 = com.vkontakte.android.Global.langDate(r0, r1);
    L_0x0821:
        r36 = java.lang.String.valueOf(r36);
        r0 = r37;
        r1 = r36;
        r0.<init>(r1);
        r0 = r42;
        r0 = r0.f187e;
        r36 = r0;
        r0 = r36;
        r0 = r0.retweetType;
        r36 = r0;
        if (r36 != 0) goto L_0x0fdc;
    L_0x083a:
        r36 = "";
    L_0x083c:
        r0 = r37;
        r1 = r36;
        r36 = r0.append(r1);
        r36 = r36.toString();
        r35.setText(r36);
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.retweetText;
        r35 = r0;
        if (r35 == 0) goto L_0x08b1;
    L_0x0859:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.retweetText;
        r35 = r0;
        r35 = r35.length();
        if (r35 <= 0) goto L_0x08b1;
    L_0x086b:
        r0 = r42;
        r0 = r0.contentView;
        r35 = r0;
        r36 = 2131296589; // 0x7f09014d float:1.8211099E38 double:1.0530004257E-314;
        r35 = r35.findViewById(r36);
        r36 = 0;
        r35.setVisibility(r36);
        r0 = r42;
        r0 = r0.contentView;
        r35 = r0;
        r36 = 2131296589; // 0x7f09014d float:1.8211099E38 double:1.0530004257E-314;
        r35 = r35.findViewById(r36);
        r35 = (com.vkontakte.android.ExTextView) r35;
        r0 = r42;
        r0 = r0.f187e;
        r36 = r0;
        r0 = r36;
        r0 = r0.retweetText;
        r36 = r0;
        r37 = "\n";
        r38 = "<br/>";
        r36 = r36.replace(r37, r38);
        r36 = android.text.Html.fromHtml(r36);
        r36 = (android.text.Spannable) r36;
        r36 = com.vkontakte.android.NewsEntry.stripUnderlines(r36);
        r36 = com.vkontakte.android.Global.replaceEmoji(r36);
        r35.setText(r36);
    L_0x08b1:
        r35 = 2130903117; // 0x7f03004d float:1.7413043E38 double:1.0528060247E-314;
        r36 = 0;
        r0 = r43;
        r1 = r35;
        r2 = r36;
        r35 = android.view.View.inflate(r0, r1, r2);
        r35 = (com.vkontakte.android.LoadMoreCommentsView) r35;
        r0 = r35;
        r1 = r42;
        r1.loadMoreView = r0;
        r0 = r42;
        r0 = r0.loadMoreView;
        r35 = r0;
        r36 = 8;
        r35.setVisibility(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r35 = (android.view.ViewGroup) r35;
        r0 = r42;
        r0 = r0.loadMoreView;
        r36 = r0;
        r35.addView(r36);
        r0 = r42;
        r0 = r0.loadMoreView;
        r35 = r0;
        r36 = new android.widget.LinearLayout$LayoutParams;
        r37 = -1;
        r38 = 1110179840; // 0x422c0000 float:43.0 double:5.485017196E-315;
        r38 = com.vkontakte.android.Global.scale(r38);
        r36.<init>(r37, r38);
        r35.setLayoutParams(r36);
        r0 = r42;
        r0 = r0.loadMoreView;
        r35 = r0;
        r36 = new com.vkontakte.android.fragments.PostViewFragment$7;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.setOnClickListener(r36);
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r36 = 2048; // 0x800 float:2.87E-42 double:1.0118E-320;
        r35 = r35.flag(r36);
        if (r35 != 0) goto L_0x0928;
    L_0x091a:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r36 = 4096; // 0x1000 float:5.74E-42 double:2.0237E-320;
        r35 = r35.flag(r36);
        if (r35 == 0) goto L_0x1000;
    L_0x0928:
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = 0;
        r35.setDraggingEnabled(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296584; // 0x7f090148 float:1.8211089E38 double:1.0530004233E-314;
        r35 = r35.findViewById(r36);
        r36 = 8;
        r35.setVisibility(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296594; // 0x7f090152 float:1.821111E38 double:1.053000428E-314;
        r35 = r35.findViewById(r36);
        r36 = 8;
        r35.setVisibility(r36);
    L_0x0957:
        r35 = new com.vkontakte.android.ui.ListImageLoaderWrapper;
        r36 = new com.vkontakte.android.fragments.PostViewFragment$PostCommentsImagesAdapter;
        r37 = 0;
        r0 = r36;
        r1 = r42;
        r2 = r37;
        r0.<init>(r2);
        r0 = r42;
        r0 = r0.list;
        r37 = r0;
        r38 = 0;
        r35.<init>(r36, r37, r38);
        r0 = r35;
        r1 = r42;
        r1.imgLoader = r0;
        r0 = r42;
        r0 = r0.commentBar;
        r35 = r0;
        r36 = 2131296763; // 0x7f0901fb float:1.8211452E38 double:1.0530005117E-314;
        r35 = r35.findViewById(r36);
        r36 = new com.vkontakte.android.fragments.PostViewFragment$8;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.setOnClickListener(r36);
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r36 = 2048; // 0x800 float:2.87E-42 double:1.0118E-320;
        r35 = r35.flag(r36);
        if (r35 != 0) goto L_0x09f8;
    L_0x099e:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r36 = 4096; // 0x1000 float:5.74E-42 double:2.0237E-320;
        r35 = r35.flag(r36);
        if (r35 != 0) goto L_0x09f8;
    L_0x09ac:
        r35 = new android.widget.ProgressBar;
        r0 = r35;
        r1 = r43;
        r0.<init>(r1);
        r0 = r35;
        r1 = r42;
        r1.commentsProgress = r0;
        r21 = new android.widget.LinearLayout$LayoutParams;
        r35 = 1106247680; // 0x41f00000 float:30.0 double:5.465589745E-315;
        r35 = com.vkontakte.android.Global.scale(r35);
        r36 = 1106247680; // 0x41f00000 float:30.0 double:5.465589745E-315;
        r36 = com.vkontakte.android.Global.scale(r36);
        r0 = r21;
        r1 = r35;
        r2 = r36;
        r0.<init>(r1, r2);
        r35 = 1;
        r0 = r35;
        r1 = r21;
        r1.gravity = r0;
        r0 = r42;
        r0 = r0.commentsProgress;
        r35 = r0;
        r0 = r35;
        r1 = r21;
        r0.setLayoutParams(r1);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r35 = (android.view.ViewGroup) r35;
        r0 = r42;
        r0 = r0.commentsProgress;
        r36 = r0;
        r35.addView(r36);
    L_0x09f8:
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = new com.vkontakte.android.fragments.PostViewFragment$9;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.setOnItemLongClickListener(r36);
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = new com.vkontakte.android.fragments.PostViewFragment$10;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.setOnItemClickListener(r36);
        r0 = r42;
        r0 = r0.contentView;
        r35 = r0;
        r36 = 2131296597; // 0x7f090155 float:1.8211115E38 double:1.0530004297E-314;
        r18 = r35.findViewById(r36);
        r18 = (android.widget.LinearLayout) r18;
        r11 = 0;
    L_0x0a2c:
        r35 = 10;
        r0 = r35;
        if (r11 < r0) goto L_0x100b;
    L_0x0a32:
        r42.updateButtons();
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296595; // 0x7f090153 float:1.8211111E38 double:1.0530004287E-314;
        r35 = r35.findViewById(r36);
        r36 = new com.vkontakte.android.fragments.PostViewFragment$11;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.setOnClickListener(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296598; // 0x7f090156 float:1.8211117E38 double:1.05300043E-314;
        r35 = r35.findViewById(r36);
        r36 = new com.vkontakte.android.fragments.PostViewFragment$12;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.setOnClickListener(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296597; // 0x7f090155 float:1.8211115E38 double:1.0530004297E-314;
        r35 = r35.findViewById(r36);
        r36 = new com.vkontakte.android.fragments.PostViewFragment$13;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.setOnClickListener(r36);
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        r36 = 2;
        r0 = r35;
        r1 = r36;
        if (r0 != r1) goto L_0x0a97;
    L_0x0a94:
        r42.loadVideoInfo();
    L_0x0a97:
        r42.updateLikePhotos();
        r0 = r42;
        r0 = r0.commentBar;
        r35 = r0;
        r0 = r35;
        r1 = r42;
        r0.setFragment(r1);
        r0 = r42;
        r0 = r0.commentBar;
        r35 = r0;
        r36 = 0;
        r0 = r42;
        r0 = r0.f187e;
        r37 = r0;
        r0 = r37;
        r0 = r0.ownerID;
        r37 = r0;
        r35.setUploadType(r36, r37);
        r35 = r42.getArguments();
        r36 = "comment";
        r35 = r35.getBoolean(r36);
        if (r35 == 0) goto L_0x0ae1;
    L_0x0aca:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r36 = 2;
        r35 = r35.flag(r36);
        if (r35 == 0) goto L_0x0ae1;
    L_0x0ad8:
        r0 = r42;
        r0 = r0.commentBar;
        r35 = r0;
        r35.focus();
    L_0x0ae1:
        r0 = r42;
        r0 = r0.commentBar;
        r35 = r0;
        r36 = 2;
        r37 = 0;
        r35.setAttachLimits(r36, r37);
        r0 = r42;
        r0 = r0.commentBar;
        r35 = r0;
        r36 = 2131296764; // 0x7f0901fc float:1.8211454E38 double:1.053000512E-314;
        r35 = r35.findViewById(r36);
        r36 = new com.vkontakte.android.fragments.PostViewFragment$14;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.setOnLongClickListener(r36);
        r0 = r42;
        r0 = r0.commentBar;
        r35 = r0;
        r36 = 2131296764; // 0x7f0901fc float:1.8211454E38 double:1.053000512E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.widget.ImageView) r35;
        r36 = 2130837892; // 0x7f020184 float:1.728075E38 double:1.0527737993E-314;
        r35.setImageResource(r36);
        r0 = r42;
        r0 = r0.commentBar;
        r35 = r0;
        r36 = 2131296764; // 0x7f0901fc float:1.8211454E38 double:1.053000512E-314;
        r35 = r35.findViewById(r36);
        r36 = new com.vkontakte.android.fragments.PostViewFragment$15;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.setOnClickListener(r36);
        r0 = r42;
        r0 = r0.commentBar;
        r35 = r0;
        r36 = 2131296762; // 0x7f0901fa float:1.821145E38 double:1.053000511E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.widget.EditText) r35;
        r36 = new com.vkontakte.android.fragments.PostViewFragment$16;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.addTextChangedListener(r36);
        r35 = 2130903102; // 0x7f03003e float:1.7413012E38 double:1.0528060173E-314;
        r36 = 0;
        r0 = r43;
        r1 = r35;
        r2 = r36;
        r35 = android.view.View.inflate(r0, r1, r2);
        r35 = (com.vkontakte.android.ui.ErrorView) r35;
        r0 = r35;
        r1 = r42;
        r1.error = r0;
        r0 = r42;
        r0 = r0.error;
        r35 = r0;
        r36 = 1;
        r35.setIsInline(r36);
        r0 = r42;
        r0 = r0.error;
        r35 = r0;
        r36 = new com.vkontakte.android.fragments.PostViewFragment$17;
        r0 = r36;
        r1 = r42;
        r0.<init>();
        r35.setOnRetryListener(r36);
        r0 = r42;
        r0 = r0.error;
        r35 = r0;
        r36 = 8;
        r35.setVisibility(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r35 = (android.view.ViewGroup) r35;
        r0 = r42;
        r0 = r0.error;
        r36 = r0;
        r35.addView(r36);
        r0 = r42;
        r0 = r0.scrollToComment;
        r35 = r0;
        if (r35 == 0) goto L_0x0bb3;
    L_0x0ba8:
        r0 = r42;
        r0 = r0.list;
        r35 = r0;
        r36 = 999; // 0x3e7 float:1.4E-42 double:4.936E-321;
        r35.setSelection(r36);
    L_0x0bb3:
        r35 = 1;
        r0 = r42;
        r1 = r35;
        r0.setHasOptionsMenu(r1);
        r42.updatePaddings();
        goto L_0x014c;
    L_0x0bc1:
        r35 = 1;
        goto L_0x04d9;
    L_0x0bc5:
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296592; // 0x7f090150 float:1.8211105E38 double:1.053000427E-314;
        r35 = r35.findViewById(r36);
        r36 = 8;
        r35.setVisibility(r36);
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        r36 = 2;
        r0 = r35;
        r1 = r36;
        if (r0 == r1) goto L_0x05a3;
    L_0x0beb:
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296593; // 0x7f090151 float:1.8211107E38 double:1.0530004277E-314;
        r32 = r35.findViewById(r36);
        r16 = new android.widget.LinearLayout$LayoutParams;
        r35 = -1;
        r36 = -2;
        r0 = r16;
        r1 = r35;
        r2 = r36;
        r0.<init>(r1, r2);
        r35 = 1088421888; // 0x40e00000 float:7.0 double:5.37751863E-315;
        r35 = com.vkontakte.android.Global.scale(r35);
        r36 = 1088421888; // 0x40e00000 float:7.0 double:5.37751863E-315;
        r36 = com.vkontakte.android.Global.scale(r36);
        r37 = 1088421888; // 0x40e00000 float:7.0 double:5.37751863E-315;
        r37 = com.vkontakte.android.Global.scale(r37);
        r38 = 1088421888; // 0x40e00000 float:7.0 double:5.37751863E-315;
        r38 = com.vkontakte.android.Global.scale(r38);
        r0 = r16;
        r1 = r35;
        r2 = r36;
        r3 = r37;
        r4 = r38;
        r0.setMargins(r1, r2, r3, r4);
        r0 = r32;
        r1 = r16;
        r0.setLayoutParams(r1);
        goto L_0x05a3;
    L_0x0c35:
        r6 = r35.next();
        r6 = (com.vkontakte.android.Attachment) r6;
        r0 = r6 instanceof com.vkontakte.android.PhotoAttachment;
        r36 = r0;
        if (r36 == 0) goto L_0x06ad;
    L_0x0c41:
        r6 = (com.vkontakte.android.PhotoAttachment) r6;
        r0 = r6.srcBig;
        r36 = r0;
        if (r36 == 0) goto L_0x06ad;
    L_0x0c49:
        r20 = r20 + 1;
        goto L_0x06ad;
    L_0x0c4d:
        r6 = r37.next();
        r6 = (com.vkontakte.android.Attachment) r6;
        r0 = r43;
        r7 = r6.getFullView(r0);
        r0 = r6 instanceof com.vkontakte.android.PhotoAttachment;
        r35 = r0;
        if (r35 == 0) goto L_0x0cae;
    L_0x0c5f:
        r35 = r6;
        r35 = (com.vkontakte.android.PhotoAttachment) r35;
        r0 = r35;
        r0 = r0.srcBig;
        r35 = r0;
        if (r35 == 0) goto L_0x0c97;
    L_0x0c6b:
        r23 = new com.vkontakte.android.Photo;
        r35 = r6;
        r35 = (com.vkontakte.android.PhotoAttachment) r35;
        r0 = r23;
        r1 = r35;
        r0.<init>(r1);
        r0 = r42;
        r0 = r0.photos;
        r35 = r0;
        r0 = r35;
        r1 = r23;
        r0.add(r1);
        r12 = r24;
        r35 = new com.vkontakte.android.fragments.PostViewFragment$3;
        r0 = r35;
        r1 = r42;
        r0.<init>(r12);
        r0 = r35;
        r7.setOnClickListener(r0);
        r24 = r24 + 1;
    L_0x0c97:
        r35 = 1;
        r0 = r20;
        r1 = r35;
        if (r0 > r1) goto L_0x0cae;
    L_0x0c9f:
        r0 = r7 instanceof android.widget.ImageView;
        r35 = r0;
        if (r35 == 0) goto L_0x0cae;
    L_0x0ca5:
        r35 = r7;
        r35 = (android.widget.ImageView) r35;
        r36 = android.widget.ImageView.ScaleType.FIT_START;
        r35.setScaleType(r36);
    L_0x0cae:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        r36 = 2;
        r0 = r35;
        r1 = r36;
        if (r0 == r1) goto L_0x0cd6;
    L_0x0cc2:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.type;
        r35 = r0;
        r36 = 1;
        r0 = r35;
        r1 = r36;
        if (r0 != r1) goto L_0x0ec3;
    L_0x0cd6:
        r0 = r6 instanceof com.vkontakte.android.VideoAttachment;
        r35 = r0;
        if (r35 == 0) goto L_0x0d57;
    L_0x0cdc:
        r35 = r6;
        r35 = (com.vkontakte.android.VideoAttachment) r35;
        r36 = 1;
        r0 = r35;
        r1 = r43;
        r2 = r36;
        r7 = r0.getView(r1, r2);
        r35 = 2131296336; // 0x7f090050 float:1.8210586E38 double:1.0530003007E-314;
        r0 = r35;
        r35 = r7.findViewById(r0);
        r35 = (android.widget.TextView) r35;
        r36 = com.vkontakte.android.ui.Fonts.getRobotoLight();
        r35.setTypeface(r36);
        r35 = 2131296339; // 0x7f090053 float:1.8210592E38 double:1.053000302E-314;
        r0 = r35;
        r35 = r7.findViewById(r0);
        r35 = (android.widget.TextView) r35;
        r36 = com.vkontakte.android.ui.Fonts.getRobotoLight();
        r35.setTypeface(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296593; // 0x7f090151 float:1.8211107E38 double:1.0530004277E-314;
        r35 = r35.findViewById(r36);
        r16 = r35.getLayoutParams();
        r16 = (android.widget.LinearLayout.LayoutParams) r16;
        r35 = 0;
        r0 = r35;
        r1 = r16;
        r1.rightMargin = r0;
        r0 = r35;
        r1 = r16;
        r1.leftMargin = r0;
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296593; // 0x7f090151 float:1.8211107E38 double:1.0530004277E-314;
        r35 = r35.findViewById(r36);
        r0 = r35;
        r1 = r16;
        r0.setLayoutParams(r1);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296593; // 0x7f090151 float:1.8211107E38 double:1.0530004277E-314;
        r35 = r35.findViewById(r36);
        r36 = -16777216; // 0xffffffffff000000 float:-1.7014118E38 double:NaN;
        r35.setBackgroundColor(r36);
    L_0x0d57:
        r35 = r42.getResources();
        r35 = r35.getDisplayMetrics();
        r0 = r35;
        r0 = r0.widthPixels;
        r35 = r0;
        r36 = r42.getResources();
        r36 = r36.getDisplayMetrics();
        r0 = r36;
        r0 = r0.heightPixels;
        r36 = r0;
        r33 = java.lang.Math.min(r35, r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296593; // 0x7f090151 float:1.8211107E38 double:1.0530004277E-314;
        r5 = r35.findViewById(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r35 = (android.view.ViewGroup) r35;
        r0 = r35;
        r0.removeView(r5);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r35 = (android.view.ViewGroup) r35;
        r36 = 1;
        r0 = r35;
        r1 = r36;
        r0.addView(r5, r1);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296593; // 0x7f090151 float:1.8211107E38 double:1.0530004277E-314;
        r36 = r35.findViewById(r36);
        r38 = 0;
        r39 = 0;
        r40 = 0;
        r0 = r42;
        r0 = r0.videoBottomPadding;
        r35 = r0;
        if (r35 != 0) goto L_0x0dd3;
    L_0x0dbd:
        r35 = r42.getResources();
        r35 = r35.getConfiguration();
        r0 = r35;
        r0 = r0.orientation;
        r35 = r0;
        r41 = 2;
        r0 = r35;
        r1 = r41;
        if (r0 != r1) goto L_0x0f29;
    L_0x0dd3:
        r35 = 1088421888; // 0x40e00000 float:7.0 double:5.37751863E-315;
        r35 = com.vkontakte.android.Global.scale(r35);
    L_0x0dd9:
        r0 = r36;
        r1 = r38;
        r2 = r39;
        r3 = r40;
        r4 = r35;
        r0.setPadding(r1, r2, r3, r4);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296585; // 0x7f090149 float:1.821109E38 double:1.0530004237E-314;
        r22 = r35.findViewById(r36);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r35 = (android.view.ViewGroup) r35;
        r0 = r35;
        r1 = r22;
        r0.removeView(r1);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r35 = (android.view.ViewGroup) r35;
        r0 = r42;
        r0 = r0.f187e;
        r36 = r0;
        r0 = r36;
        r0 = r0.type;
        r36 = r0;
        r38 = 2;
        r0 = r36;
        r1 = r38;
        if (r0 != r1) goto L_0x0f2d;
    L_0x0e1e:
        r36 = 3;
    L_0x0e20:
        r0 = r35;
        r1 = r22;
        r2 = r36;
        r0.addView(r1, r2);
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296592; // 0x7f090150 float:1.8211105E38 double:1.053000427E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.widget.TextView) r35;
        r36 = 0;
        r38 = 0;
        r39 = 0;
        r40 = 0;
        r0 = r35;
        r1 = r36;
        r2 = r38;
        r3 = r39;
        r4 = r40;
        r0.setPadding(r1, r2, r3, r4);
        r0 = r7 instanceof com.vkontakte.android.VideoAttachView;
        r35 = r0;
        if (r35 == 0) goto L_0x0ec3;
    L_0x0e53:
        r35 = com.vkontakte.android.Global.isTablet;
        if (r35 == 0) goto L_0x0f31;
    L_0x0e57:
        r35 = 640; // 0x280 float:8.97E-43 double:3.16E-321;
    L_0x0e59:
        r0 = r35;
        r0 = (float) r0;
        r35 = r0;
        r35 = com.vkontakte.android.Global.scale(r35);
        r0 = r35;
        r1 = r33;
        r34 = java.lang.Math.min(r0, r1);
        r0 = r34;
        r1 = r33;
        if (r0 == r1) goto L_0x0f35;
    L_0x0e70:
        r35 = 1;
    L_0x0e72:
        r0 = r35;
        r1 = r42;
        r1.videoBottomPadding = r0;
        r10 = new com.vkontakte.android.ui.FlowLayout$LayoutParams;
        r35 = 0;
        r36 = 0;
        r0 = r35;
        r1 = r36;
        r10.<init>(r0, r1);
        r0 = r34;
        r10.width = r0;
        r0 = r34;
        r0 = (float) r0;
        r35 = r0;
        r36 = 1061158912; // 0x3f400000 float:0.75 double:5.24282163E-315;
        r35 = r35 * r36;
        r0 = r35;
        r0 = (int) r0;
        r35 = r0;
        r0 = r35;
        r10.height = r0;
        r35 = 1;
        r0 = r35;
        r10.center = r0;
        r7.setLayoutParams(r10);
        r35 = 2131296352; // 0x7f090060 float:1.8210618E38 double:1.0530003086E-314;
        r0 = r35;
        r35 = r7.findViewById(r0);
        r36 = new android.widget.FrameLayout$LayoutParams;
        r0 = r10.width;
        r38 = r0;
        r0 = r10.height;
        r39 = r0;
        r0 = r36;
        r1 = r38;
        r2 = r39;
        r0.<init>(r1, r2);
        r35.setLayoutParams(r36);
    L_0x0ec3:
        r0 = r6 instanceof com.vkontakte.android.ThumbAttachment;
        r35 = r0;
        if (r35 == 0) goto L_0x0ecb;
    L_0x0ec9:
        r30 = r7;
    L_0x0ecb:
        r0 = r6 instanceof com.vkontakte.android.DocumentAttachment;
        r35 = r0;
        if (r35 == 0) goto L_0x0f11;
    L_0x0ed1:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.attachments;
        r35 = r0;
        r35 = r35.size();
        r36 = r11 + 1;
        r0 = r35;
        r1 = r36;
        if (r0 <= r1) goto L_0x0f11;
    L_0x0ee9:
        r0 = r42;
        r0 = r0.f187e;
        r35 = r0;
        r0 = r35;
        r0 = r0.attachments;
        r35 = r0;
        r36 = r11 + 1;
        r35 = r35.get(r36);
        r0 = r35;
        r0 = r0 instanceof com.vkontakte.android.DocumentAttachment;
        r35 = r0;
        if (r35 != 0) goto L_0x0f11;
    L_0x0f03:
        r10 = new com.vkontakte.android.ui.FlowLayout$LayoutParams;
        r10.<init>();
        r35 = 1;
        r0 = r35;
        r10.breakAfter = r0;
        r7.setLayoutParams(r10);
    L_0x0f11:
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r36 = 2131296593; // 0x7f090151 float:1.8211107E38 double:1.0530004277E-314;
        r35 = r35.findViewById(r36);
        r35 = (android.view.ViewGroup) r35;
        r0 = r35;
        r0.addView(r7);
        r11 = r11 + 1;
        goto L_0x06c4;
    L_0x0f29:
        r35 = 0;
        goto L_0x0dd9;
    L_0x0f2d:
        r36 = 0;
        goto L_0x0e20;
    L_0x0f31:
        r35 = 320; // 0x140 float:4.48E-43 double:1.58E-321;
        goto L_0x0e59;
    L_0x0f35:
        r35 = 0;
        goto L_0x0e72;
    L_0x0f39:
        r6 = r35.next();
        r6 = (com.vkontakte.android.Attachment) r6;
        r0 = r6 instanceof com.vkontakte.android.PhotoAttachment;
        r36 = r0;
        if (r36 == 0) goto L_0x071d;
    L_0x0f45:
        r6 = (com.vkontakte.android.PhotoAttachment) r6;
        r0 = r6.srcBig;
        r36 = r0;
        if (r36 == 0) goto L_0x071d;
    L_0x0f4d:
        r20 = r20 + 1;
        goto L_0x071d;
    L_0x0f51:
        r6 = r36.next();
        r6 = (com.vkontakte.android.Attachment) r6;
        r0 = r43;
        r7 = r6.getFullView(r0);
        r0 = r6 instanceof com.vkontakte.android.PhotoAttachment;
        r35 = r0;
        if (r35 == 0) goto L_0x0fb6;
    L_0x0f63:
        r35 = r6;
        r35 = (com.vkontakte.android.PhotoAttachment) r35;
        r0 = r35;
        r0 = r0.srcBig;
        r35 = r0;
        if (r35 == 0) goto L_0x0f9b;
    L_0x0f6f:
        r23 = new com.vkontakte.android.Photo;
        r35 = r6;
        r35 = (com.vkontakte.android.PhotoAttachment) r35;
        r0 = r23;
        r1 = r35;
        r0.<init>(r1);
        r0 = r42;
        r0 = r0.repostPhotos;
        r35 = r0;
        r0 = r35;
        r1 = r23;
        r0.add(r1);
        r12 = r24;
        r35 = new com.vkontakte.android.fragments.PostViewFragment$4;
        r0 = r35;
        r1 = r42;
        r0.<init>(r12);
        r0 = r35;
        r7.setOnClickListener(r0);
        r24 = r24 + 1;
    L_0x0f9b:
        r35 = 1;
        r0 = r20;
        r1 = r35;
        if (r0 > r1) goto L_0x0fb6;
    L_0x0fa3:
        r0 = r7 instanceof android.widget.ImageView;
        r35 = r0;
        if (r35 == 0) goto L_0x0fb6;
    L_0x0fa9:
        r35 = r7;
        r35 = (android.widget.ImageView) r35;
        r37 = android.widget.ImageView.ScaleType.FIT_START;
        r0 = r35;
        r1 = r37;
        r0.setScaleType(r1);
    L_0x0fb6:
        r0 = r6 instanceof com.vkontakte.android.ThumbAttachment;
        r35 = r0;
        if (r35 == 0) goto L_0x0fbe;
    L_0x0fbc:
        r30 = r7;
    L_0x0fbe:
        r0 = r42;
        r0 = r0.headerView;
        r35 = r0;
        r37 = 2131296590; // 0x7f09014e float:1.82111E38 double:1.053000426E-314;
        r0 = r35;
        r1 = r37;
        r35 = r0.findViewById(r1);
        r35 = (android.view.ViewGroup) r35;
        r0 = r35;
        r0.addView(r7);
        goto L_0x0733;
    L_0x0fd8:
        r36 = "";
        goto L_0x0821;
    L_0x0fdc:
        r36 = new java.lang.StringBuilder;
        r38 = " ";
        r0 = r36;
        r1 = r38;
        r0.<init>(r1);
        r38 = 2131231003; // 0x7f08011b float:1.8078075E38 double:1.052968022E-314;
        r0 = r42;
        r1 = r38;
        r38 = r0.getString(r1);
        r0 = r36;
        r1 = r38;
        r36 = r0.append(r1);
        r36 = r36.toString();
        goto L_0x083c;
    L_0x1000:
        r35 = 1;
        r0 = r42;
        r1 = r35;
        r0.loadComments(r1);
        goto L_0x0957;
    L_0x100b:
        r13 = new android.widget.ImageView;
        r0 = r43;
        r13.<init>(r0);
        r35 = 8;
        r0 = r35;
        r13.setVisibility(r0);
        r35 = 2130838132; // 0x7f020274 float:1.7281238E38 double:1.052773918E-314;
        r0 = r35;
        r13.setImageResource(r0);
        r16 = new android.widget.LinearLayout$LayoutParams;
        r35 = 1108082688; // 0x420c0000 float:35.0 double:5.47465589E-315;
        r35 = com.vkontakte.android.Global.scale(r35);
        r36 = 1108082688; // 0x420c0000 float:35.0 double:5.47465589E-315;
        r36 = com.vkontakte.android.Global.scale(r36);
        r0 = r16;
        r1 = r35;
        r2 = r36;
        r0.<init>(r1, r2);
        r35 = 1082130432; // 0x40800000 float:4.0 double:5.34643471E-315;
        r35 = com.vkontakte.android.Global.scale(r35);
        r0 = r35;
        r1 = r16;
        r1.rightMargin = r0;
        r0 = r18;
        r1 = r16;
        r0.addView(r13, r1);
        r11 = r11 + 1;
        goto L_0x0a2c;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.fragments.PostViewFragment.onAttach(android.app.Activity):void");
    }

    public void onCreate(Bundle s) {
        super.onCreate(s);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Posts.ACTION_POST_UPDATED_BROADCAST);
        VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
        if (this.currentReq != null) {
            this.currentReq.cancel();
        }
    }

    public void onPause() {
        super.onPause();
        this.imgLoader.deactivate();
        if (this.emojiPopup != null && this.emojiPopup.isShowing()) {
            this.emojiPopup.hide();
        }
    }

    public void onResume() {
        super.onResume();
        this.imgLoader.activate();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentView;
    }

    public View getImageAttachView(int idx) {
        int cnt = 0;
        int i = 0;
        Iterator it = this.f187e.repostAttachments.iterator();
        while (it.hasNext()) {
            if (((Attachment) it.next()) instanceof ImageAttachment) {
                if (cnt == idx) {
                    return ((ViewGroup) this.headerView.findViewById(C0436R.id.wall_view_repost_attach)).getChildAt(i);
                }
                cnt += TYPE_PHOTO;
            }
            i += TYPE_PHOTO;
        }
        i = 0;
        it = this.f187e.attachments.iterator();
        while (it.hasNext()) {
            if (((Attachment) it.next()) instanceof ImageAttachment) {
                if (cnt == idx) {
                    return ((ViewGroup) this.headerView.findViewById(C0436R.id.wall_view_attach)).getChildAt(i);
                }
                cnt += TYPE_PHOTO;
            }
            i += TYPE_PHOTO;
        }
        return null;
    }

    public void onConfigurationChanged(Configuration cfg) {
        updateLikePhotos();
        updatePaddings();
    }

    private void updatePaddings() {
        DisplayMetrics dm = getResources().getDisplayMetrics();
        this.narrowScreen = true;
        updateHeaderBackground();
        updateList();
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        super.onActivityResult(reqCode, resCode, data);
        if (reqCode > 10000) {
            this.commentBar.onActivityResult(reqCode, resCode, data);
        }
        if (reqCode == EDIT_REQUEST && ((resCode == -1 || resCode == TYPE_PHOTO) && (getActivity() instanceof FragmentWrapperActivity))) {
            getActivity().finish();
        }
        if (reqCode == EDIT_COMMENT_REQUEST && resCode == -1) {
            NewsEntry ce = (NewsEntry) data.getParcelableExtra("comment");
            Iterator it = this.comments.iterator();
            while (it.hasNext()) {
                NewsComment comm = (NewsComment) it.next();
                if (comm.cid == ce.postID) {
                    comm.setText(ce.text);
                    comm.attachments = ce.attachments;
                    DisplayMetrics metrics = VKApplication.context.getResources().getDisplayMetrics();
                    int tSize = Math.min(Math.min(metrics.widthPixels, metrics.heightPixels) - Global.scale(95.0f), 604);
                    it = this.comments.iterator();
                    while (it.hasNext()) {
                        NewsComment c = (NewsComment) it.next();
                        ZhukovLayout.processThumbs(tSize, (int) (((float) tSize) * 0.666f), comm.attachments);
                    }
                    comm.links.clear();
                    comm.linkTitles.clear();
                    Pattern ptn1 = Pattern.compile("((?:(?:http|https)://)?[a-zA-Z\u0430-\u044f\u0410-\u042f0-9-]+\\.[a-zA-Z\u0430-\u044f\u0410-\u042f]{2,4}[a-zA-Z/?\\.=#%&-_]+)");
                    Pattern ptn2 = Pattern.compile("\\[id(\\d+)\\|([^\\]]+)\\]");
                    Pattern ptn3 = Pattern.compile("\\[club(\\d+)\\|([^\\]]+)\\]");
                    Matcher matcher = ptn2.matcher(ce.text);
                    while (matcher.find()) {
                        comm.links.add("vkontakte://profile/" + matcher.group(TYPE_PHOTO));
                        comm.linkTitles.add(matcher.group(RESULT_DELETED));
                    }
                    matcher = ptn3.matcher(ce.text);
                    while (matcher.find()) {
                        comm.links.add("vkontakte://profile/-" + matcher.group(TYPE_PHOTO));
                        comm.linkTitles.add(matcher.group(RESULT_DELETED));
                    }
                    matcher = ptn1.matcher(ce.text);
                    while (matcher.find()) {
                        comm.links.add("vklink://view/?" + matcher.group());
                        comm.linkTitles.add(matcher.group());
                    }
                    updateList();
                    return;
                }
            }
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        boolean z = false;
        inflater.inflate(C0436R.menu.wallview, menu);
        if (this.canAdmin) {
            if (this.f187e.type != 0) {
                menu.findItem(C0436R.id.edit).setVisible(false);
            }
            if (this.f187e.type != 0) {
                menu.removeItem(C0436R.id.edit);
            } else if ((((long) this.f187e.time) < (System.currentTimeMillis() / 1000) - 86400 && !this.f187e.flag(NewsEntry.FLAG_SUGGESTED)) || !this.f187e.flag(TransportMediator.FLAG_KEY_MEDIA_NEXT)) {
                menu.findItem(C0436R.id.edit).setEnabled(false);
            }
        } else {
            menu.findItem(C0436R.id.edit).setVisible(false);
            menu.findItem(C0436R.id.delete).setVisible(false);
        }
        menu.findItem(C0436R.id.show_original_post).setVisible(this.f187e.flag(32));
        menu.findItem(C0436R.id.attach).setVisible(this.f187e.flag(RESULT_DELETED));
        MenuItem findItem = menu.findItem(C0436R.id.report);
        if (!(this.f187e.userID == Global.uid || this.f187e.flag(NewsEntry.FLAG_SUGGESTED) || this.f187e.flag(NewsEntry.FLAG_POSTPONED))) {
            z = true;
        }
        findItem.setVisible(z);
        menu.findItem(C0436R.id.publish_now).setVisible(this.f187e.flag(NewsEntry.FLAG_POSTPONED));
    }

    public void onPrepareOptionsMenu(Menu menu) {
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int i = 0;
        if (item.getItemId() == C0436R.id.delete) {
            int msg = C0436R.string.delete_confirm;
            if (this.f187e.type == TYPE_PHOTO) {
                msg = C0436R.string.delete_photo_confirm;
            }
            if (this.f187e.type == RESULT_DELETED) {
                msg = C0436R.string.delete_video_confirm;
            }
            AlertDialog.Builder negativeButton = new Builder(getActivity()).setTitle(C0436R.string.confirm).setMessage(msg).setPositiveButton(C0436R.string.yes, new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    PostViewFragment.this.deletePost();
                }
            }).setNegativeButton(C0436R.string.no, null);
            if (VERSION.SDK_INT < 11) {
                i = ACRAConstants.DEFAULT_DIALOG_ICON;
            }
            negativeButton.setIcon(i).show();
        } else if (item.getItemId() == C0436R.id.edit) {
            if (this.f187e.flag(32)) {
                editRepostComment(Html.fromHtml(this.f187e.retweetText).toString());
            } else {
                intent = new Intent(getActivity(), NewPostActivity.class);
                intent.putExtra("edit", this.f187e);
                if (this.f187e.flag(NewsEntry.FLAG_POSTPONED) && this.f187e.ownerID < 0) {
                    intent.putExtra("public", true);
                }
                startActivityForResult(intent, EDIT_REQUEST);
            }
        } else if (item.getItemId() == C0436R.id.copy_link) {
            String ln = "http://vk.com/";
            if (this.f187e.type == 0) {
                ln = new StringBuilder(String.valueOf(ln)).append("wall").toString();
            } else if (this.f187e.type == RESULT_DELETED) {
                ln = new StringBuilder(String.valueOf(ln)).append("video").toString();
            } else if (this.f187e.type == TYPE_PHOTO) {
                ln = new StringBuilder(String.valueOf(ln)).append("photo").toString();
            } else if (this.f187e.type == 4) {
                ln = new StringBuilder(String.valueOf(ln)).append("topic").toString();
            }
            ((ClipboardManager) getActivity().getSystemService("clipboard")).setText(new StringBuilder(String.valueOf(ln)).append(this.f187e.ownerID).append("_").append(this.f187e.postID).toString());
            Toast.makeText(getActivity(), C0436R.string.link_copied, 0).show();
        } else if (item.getItemId() == C0436R.id.show_original_post) {
            type = "wall";
            if (this.f187e.retweetType == TYPE_PHOTO) {
                type = "photo";
            }
            if (this.f187e.retweetType == RESULT_DELETED) {
                type = "video";
            }
            getActivity().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("vkontakte://vk.com/" + type + this.f187e.retweetUID + "_" + this.f187e.retweetOrigId)));
        } else if (item.getItemId() == C0436R.id.attach) {
            this.commentBar.openAttachMenu(53, 0, getSherlockActivity().getSupportActionBar().getHeight() - Global.scale(12.0f), C0436R.drawable.attach_dropdown);
        } else if (item.getItemId() == C0436R.id.report) {
            intent = new Intent(getActivity(), ReportContentActivity.class);
            intent.putExtra("itemID", this.f187e.postID);
            intent.putExtra("ownerID", this.f187e.ownerID);
            type = "post";
            if (this.f187e.type == TYPE_PHOTO) {
                type = "photo";
            }
            if (this.f187e.type == RESULT_DELETED) {
                type = "video";
            }
            intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, type);
            startActivity(intent);
        } else if (item.getItemId() == C0436R.id.publish_now) {
            Posts.publishPostponed(this.f187e, getActivity(), new Runnable() {
                public void run() {
                    PostViewFragment.this.getActivity().finish();
                }
            });
        }
        return true;
    }

    private void deletePost() {
        new WallDelete(this.f187e.ownerID, this.f187e.postID, this.f187e.type).setCallback(new WallDelete.Callback() {
            public void success() {
                Intent intent = new Intent(Posts.ACTION_POST_DELETED_BROADCAST);
                intent.putExtra("owner_id", PostViewFragment.this.f187e.ownerID);
                intent.putExtra("post_id", PostViewFragment.this.f187e.postID);
                intent.putExtra("post", PostViewFragment.this.f187e);
                PostViewFragment.this.getActivity().sendBroadcast(intent);
                if (PostViewFragment.this.f187e.ownerID == Global.uid) {
                    UserWallCache.remove(PostViewFragment.this.f187e.postID, PostViewFragment.this.getActivity());
                }
                NewsfeedCache.remove(PostViewFragment.this.f187e.ownerID, PostViewFragment.this.f187e.postID, PostViewFragment.this.getActivity());
                if (PostViewFragment.this.getActivity() instanceof FragmentWrapperActivity) {
                    Intent intent1 = new Intent();
                    intent1.putExtra("oid", PostViewFragment.this.f187e.ownerID);
                    intent1.putExtra("pid", PostViewFragment.this.f187e.postID);
                    PostViewFragment.this.getActivity().setResult(PostViewFragment.RESULT_DELETED, intent1);
                    PostViewFragment.this.getActivity().finish();
                }
            }

            public void fail(int ecode, String emsg) {
                Toast.makeText(PostViewFragment.this.getActivity(), C0436R.string.err_text, PostViewFragment.TYPE_PHOTO).show();
            }
        }).wrapProgress(getActivity()).exec(getActivity());
    }

    private void editRepostComment(String text) {
        EditText edit = new EditText(getActivity());
        edit.setText(text);
        edit.setLines(4);
        edit.setGravity(48);
        new Builder(getActivity()).setTitle(C0436R.string.add_comment_hint).setView(edit).setPositiveButton(C0436R.string.save, new AnonymousClass21(edit)).setNegativeButton(C0436R.string.cancel, null).show();
    }

    private void saveRepostComment(String newComment) {
        new WallEdit(this.f187e.postID, this.f187e.ownerID, newComment).setCallback(new AnonymousClass22(newComment)).wrapProgress(getActivity()).exec(getActivity());
    }

    private void updateHeaderBackground() {
        this.headerView.setBackgroundColor(-1);
        this.headerView.setPadding(0, 0, 0, 0);
    }

    private void updateLikePhotos() {
        this.contentView.postDelayed(new Runnable() {
            public void run() {
                int i;
                Log.m528i("vk", "LikePhotos = " + PostViewFragment.this.likePhotos);
                LinearLayout lpwrap = (LinearLayout) PostViewFragment.this.contentView.findViewById(C0436R.id.wall_view_like_photos);
                PostViewFragment.this.maxLikeVisible = lpwrap.getWidth() / Global.scale(39.0f);
                int num = Math.min(PostViewFragment.this.likePhotos.size() - (PostViewFragment.this.f187e.flag(8) ? 0 : PostViewFragment.TYPE_PHOTO), PostViewFragment.this.maxLikeVisible) + PostViewFragment.TYPE_PHOTO;
                PostViewFragment.this.maxLikePhotos = num - 1;
                for (i = PostViewFragment.TYPE_PHOTO; i < 10; i += PostViewFragment.TYPE_PHOTO) {
                    int i2;
                    View childAt = lpwrap.getChildAt(i);
                    if (i < num) {
                        i2 = 0;
                    } else {
                        i2 = 8;
                    }
                    childAt.setVisibility(i2);
                }
                if (lpwrap.getChildCount() != 0 && PostViewFragment.this.likePhotos.size() != 0) {
                    if (PostViewFragment.this.f187e.flag(8)) {
                        lpwrap.getChildAt(0).setVisibility(0);
                        int numVisible = 0;
                        i = 0;
                        while (i < lpwrap.getChildCount() && lpwrap.getChildAt(i).getVisibility() == 0) {
                            numVisible += PostViewFragment.TYPE_PHOTO;
                            i += PostViewFragment.TYPE_PHOTO;
                        }
                        if (PostViewFragment.this.maxLikePhotos < lpwrap.getChildCount()) {
                            lpwrap.getChildAt(PostViewFragment.this.maxLikePhotos).setVisibility(8);
                            return;
                        }
                        return;
                    }
                    lpwrap.getChildAt(0).setVisibility(8);
                    if (PostViewFragment.this.maxLikeVisible == PostViewFragment.this.maxLikePhotos && PostViewFragment.this.maxLikePhotos < lpwrap.getChildCount()) {
                        lpwrap.getChildAt(PostViewFragment.this.maxLikePhotos).setVisibility(0);
                    }
                }
            }
        }, 100);
    }

    private void showCommentActions(int pos) {
        if (pos >= 0 && pos < this.comments.size()) {
            NewsComment comm = (NewsComment) this.comments.get(pos);
            ArrayList<String> items = new ArrayList();
            ArrayList<String> acts = new ArrayList();
            items.add(getString(C0436R.string.profile));
            acts.add("profile");
            items.add(getString(C0436R.string.reply_to));
            acts.add("reply");
            items.add(getString(C0436R.string.copy_text));
            acts.add("copy");
            items.add(getString(comm.isLiked ? C0436R.string.unlike : C0436R.string.like));
            acts.add("like");
            if (comm.numLikes > 0) {
                items.add(getString(C0436R.string.liked));
                acts.add("liked");
            }
            if (comm.uid == Global.uid || this.f187e.ownerID == Global.uid || this.f187e.userID == Global.uid || ((this.f187e.ownerID < 0 && Groups.getAdminLevel(-this.f187e.ownerID) == TYPE_PHOTO && comm.uid > 0 && comm.uid != 101) || (this.f187e.ownerID < 0 && Groups.getAdminLevel(-this.f187e.ownerID) > TYPE_PHOTO))) {
                items.add(getString(C0436R.string.delete));
                acts.add("delete");
                if ((comm.uid == Global.uid || comm.uid < 0 || comm.uid == 100) && System.currentTimeMillis() / 1000 < ((long) (comm.time + 86400))) {
                    items.add(getString(C0436R.string.edit));
                    acts.add("edit");
                }
            }
            if (comm.uid != Global.uid) {
                items.add(getString(C0436R.string.report_content));
                acts.add("report");
            }
            for (int i = 0; i < comm.links.size(); i += TYPE_PHOTO) {
                items.add((String) comm.linkTitles.elementAt(i));
                acts.add((String) comm.links.get(i));
            }
            new Builder(getActivity()).setItems((CharSequence[]) items.toArray(new String[0]), new AnonymousClass24(acts, comm)).show();
        }
    }

    private void likeComment(NewsComment comm) {
        new WallLike(!comm.isLiked, this.f187e.ownerID, comm.cid, false, 5, this.f187e.type, ACRAConstants.DEFAULT_STRING_VALUE).setCallback(new AnonymousClass25(comm)).exec(this.contentView);
    }

    private void deleteComment(int cid) {
        new WallDeleteComment(this.f187e.ownerID, this.f187e.postID, cid, this.f187e.type, this.accessKey).setCallback(new AnonymousClass26(cid)).wrapProgress(getActivity()).exec(this.contentView);
    }

    private void editComment(NewsComment comm) {
        NewsEntry ce = new NewsEntry();
        ce.text = comm.text;
        ce.attachments = comm.attachments;
        ce.ownerID = this.f187e.ownerID;
        ce.postID = comm.cid;
        ce.userID = comm.uid;
        ce.type = 5;
        switch (this.f187e.type) {
            case TYPE_PHOTO /*1*/:
                ce.retweetText = "photos";
                break;
            case RESULT_DELETED /*2*/:
                ce.retweetText = "video";
                break;
            default:
                ce.retweetText = "wall";
                break;
        }
        Intent intent = new Intent(getActivity(), NewPostActivity.class);
        intent.putExtra("edit", ce);
        startActivityForResult(intent, EDIT_COMMENT_REQUEST);
    }

    private void waitAndSendComment() {
        ProgressDialog progress = new ProgressDialog(getActivity());
        progress.setMessage(getString(C0436R.string.loading));
        progress.setCancelable(true);
        progress.setCanceledOnTouchOutside(false);
        progress.show();
        this.commentBar.waitForUploads(new AnonymousClass27(progress), new AnonymousClass28(progress));
    }

    private void sendComment() {
        String _txt = this.commentBar.getText().replaceAll("\\*((?:id|club)[0-9-]+) \\(([^\\)]+)\\)", "[$1|$2]");
        boolean _r = false;
        if (_txt.startsWith(this.replyToName + ",")) {
            _r = true;
            _txt = _txt.replace(this.replyToName + ",", "[" + (this.replyToUid > 0 ? "id" : "club") + Math.abs(this.replyToUid) + "|" + this.replyToName + "],");
        }
        boolean r = _r;
        String txt = _txt;
        ArrayList<Attachment> atts = this.commentBar.getAttachments();
        this.commentBar.clearAttachments();
        if (txt.length() != 0 || atts.size() != 0) {
            new WallAddComment(this.f187e.ownerID, this.f187e.postID, txt, this.replyTo, this.f187e.type, TextUtils.join(",", atts), this.accessKey).setCallback(new AnonymousClass29(txt, atts)).wrapProgress(getActivity()).exec(this.contentView);
        }
    }

    private void broadcastUpdate() {
        try {
            if (this.f187e.type == 0) {
                Intent intent = new Intent(Posts.ACTION_POST_UPDATED_BROADCAST);
                intent.putExtra("post_id", this.f187e.postID);
                intent.putExtra("owner_id", this.f187e.ownerID);
                intent.putExtra("comments", this.f187e.numComments);
                intent.putExtra("retweets", this.f187e.numRetweets);
                intent.putExtra("likes", this.f187e.numLikes);
                intent.putExtra("liked", this.f187e.flag(8));
                intent.putExtra("retweeted", this.f187e.flag(4));
                getActivity().sendBroadcast(intent);
                NewsfeedCache.update(VKApplication.context, this.f187e.ownerID, this.f187e.postID, this.f187e.numLikes, this.f187e.numComments, this.f187e.numRetweets, this.f187e.flag(8), this.f187e.flag(4));
            }
        } catch (Exception e) {
        }
    }

    public void onRefresh() {
        loadComments(true);
    }

    public String getLastUpdatedTime() {
        if (this.lastUpdated > 0) {
            return new StringBuilder(String.valueOf(getResources().getString(C0436R.string.updated))).append(" ").append(Global.langDateRelativeNoDiff((int) (this.lastUpdated / 1000), getResources())).toString();
        }
        return getResources().getString(C0436R.string.not_updated);
    }

    private void openPhotoList(int index, View view, ViewGroup attachContainer, ArrayList<Photo> photos) {
        if (!getArguments().getBoolean("photo_viewer")) {
            for (int i = 0; i < Math.min(attachContainer.getChildCount(), photos.size()); i += TYPE_PHOTO) {
                int i2;
                View v = attachContainer.getChildAt(i);
                int[] pos = new int[RESULT_DELETED];
                v.getLocationOnScreen(pos);
                ((Photo) photos.get(i)).viewBounds = new Rect(pos[0], pos[TYPE_PHOTO], pos[0] + v.getWidth(), pos[TYPE_PHOTO] + v.getHeight());
                int top = ViewUtils.getViewOffset(v, this.list).y;
                Photo photo = (Photo) photos.get(i);
                if (top < 0) {
                    i2 = -top;
                } else {
                    i2 = 0;
                }
                photo.viewClipTop = i2;
            }
            Drawable d = ((ImageView) view).getDrawable();
            if (d != null && (d instanceof BitmapDrawable)) {
                PhotoViewerFragment.sharedThumb = ((BitmapDrawable) d).getBitmap();
            }
            Bundle args = new Bundle();
            args.putInt("orientation", view.getResources().getConfiguration().orientation);
            args.putParcelableArrayList("list", photos);
            args.putInt(GLFilterContext.AttributePosition, index);
            Navigate.to("PhotoViewerFragment", args, getActivity(), true, 0, 0);
        }
    }

    private void updateList() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                PostViewFragment.this.adapter.notifyDataSetChanged();
                PostViewFragment.this.updateHeaderBackground();
            }
        });
        this.contentView.postDelayed(new Runnable() {
            public void run() {
                PostViewFragment.this.imgLoader.updateImages();
            }
        }, 400);
    }

    private void loadComments(boolean clear) {
        if (!this.loadingComments) {
            int offset;
            int csize = clear ? 0 : this.comments.size();
            int count = 10;
            if (!clear) {
                count = Math.min(100, this.f187e.numComments - csize);
                offset = (this.f187e.numComments - csize) - count;
            }
            if (this.scrollToComment != 0) {
                count = 50;
            }
            offset = csize;
            this.loadingComments = true;
            this.currentReq = new WallGetComments(this.f187e.ownerID, this.f187e.postID, offset, count, this.f187e.type, clear, this.accessKey).setCallback(new AnonymousClass32(clear)).exec(getActivity());
        }
    }

    private void highlightComment(int pos) {
        this.list.getViewTreeObserver().addOnPreDrawListener(new AnonymousClass33(pos));
    }

    private void updateButtons() {
        if (this.f187e.numLikes > 0) {
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_like)).setText(this.f187e.numLikes);
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_like)).setCompoundDrawablePadding(Global.scale(8.0f));
        } else {
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_like)).setText(ACRAConstants.DEFAULT_STRING_VALUE);
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_like)).setCompoundDrawablePadding(0);
        }
        if (this.f187e.flag(8)) {
            this.headerView.findViewById(C0436R.id.wall_view_like_wrap).setBackgroundResource(C0436R.drawable.btn_post_act);
            ((FrameLayout) this.headerView.findViewById(C0436R.id.wall_view_like_wrap)).setForeground(getResources().getDrawable(C0436R.drawable.btn_post_act_hl));
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_like)).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_post_liked, 0, 0, 0);
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_like)).setTextColor(-1);
        } else {
            this.headerView.findViewById(C0436R.id.wall_view_like_wrap).setBackgroundResource(C0436R.drawable.btn_post);
            ((FrameLayout) this.headerView.findViewById(C0436R.id.wall_view_like_wrap)).setForeground(getResources().getDrawable(C0436R.drawable.btn_post_hl));
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_like)).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_post_like, 0, 0, 0);
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_like)).setTextColor(-5000269);
        }
        if (this.f187e.numRetweets > 0) {
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_repost)).setText(this.f187e.numRetweets);
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_repost)).setCompoundDrawablePadding(Global.scale(8.0f));
        } else {
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_repost)).setText(ACRAConstants.DEFAULT_STRING_VALUE);
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_repost)).setCompoundDrawablePadding(0);
        }
        if (this.f187e.flag(4)) {
            this.headerView.findViewById(C0436R.id.wall_view_repost_wrap).setBackgroundResource(C0436R.drawable.btn_post_act);
            ((FrameLayout) this.headerView.findViewById(C0436R.id.wall_view_repost_wrap)).setForeground(getResources().getDrawable(C0436R.drawable.btn_post_act_hl));
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_repost)).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_post_reposted, 0, 0, 0);
            ((TextView) this.headerView.findViewById(C0436R.id.wall_view_repost)).setTextColor(-1);
            return;
        }
        this.headerView.findViewById(C0436R.id.wall_view_repost_wrap).setBackgroundResource(C0436R.drawable.btn_post);
        ((FrameLayout) this.headerView.findViewById(C0436R.id.wall_view_repost_wrap)).setForeground(getResources().getDrawable(C0436R.drawable.btn_post_hl));
        ((TextView) this.headerView.findViewById(C0436R.id.wall_view_repost)).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_post_repost, 0, 0, 0);
        ((TextView) this.headerView.findViewById(C0436R.id.wall_view_repost)).setTextColor(-5000269);
    }

    private void animateLikePhotos() {
        LinearLayout lpwrap = (LinearLayout) this.contentView.findViewById(C0436R.id.wall_view_like_photos);
        if (lpwrap.getChildCount() != 0) {
            TranslateAnimation ta;
            int i;
            AnimationSet set;
            AlphaAnimation aa;
            if (this.f187e.flag(8)) {
                ta = new TranslateAnimation((float) (-Global.scale(39.0f)), 0.0f, 0.0f, 0.0f);
                ta.setDuration(300);
                lpwrap.getChildAt(0).setVisibility(0);
                for (i = 0; i < lpwrap.getChildCount(); i += TYPE_PHOTO) {
                    if (i != this.maxLikeVisible || this.maxLikeVisible >= lpwrap.getChildCount()) {
                        lpwrap.getChildAt(i).startAnimation(ta);
                    } else {
                        set = new AnimationSet(true);
                        set.addAnimation(ta);
                        aa = new AlphaAnimation(1.0f, 0.0f);
                        aa.setDuration(300);
                        set.addAnimation(aa);
                        set.setAnimationListener(new AnonymousClass34(lpwrap));
                        lpwrap.getChildAt(this.maxLikeVisible).startAnimation(set);
                    }
                }
                return;
            }
            if (this.maxLikePhotos == this.maxLikeVisible && this.maxLikeVisible < lpwrap.getChildCount()) {
                lpwrap.getChildAt(this.maxLikePhotos).setVisibility(0);
            }
            ta = new TranslateAnimation(0.0f, (float) (-Global.scale(39.0f)), 0.0f, 0.0f);
            ta.setAnimationListener(new AnonymousClass35(lpwrap));
            ta.setDuration(300);
            for (i = 0; i < Math.min(lpwrap.getChildCount(), this.maxLikeVisible + TYPE_PHOTO); i += TYPE_PHOTO) {
                if (i == this.maxLikePhotos && this.maxLikePhotos == this.maxLikeVisible && this.maxLikeVisible < lpwrap.getChildCount()) {
                    set = new AnimationSet(true);
                    set.addAnimation(ta);
                    aa = new AlphaAnimation(0.0f, 1.0f);
                    aa.setDuration(300);
                    set.addAnimation(aa);
                    lpwrap.getChildAt(this.maxLikePhotos).startAnimation(set);
                    lpwrap.getChildAt(this.maxLikePhotos).setVisibility(0);
                } else {
                    lpwrap.getChildAt(i).startAnimation(ta);
                }
            }
        }
    }

    private void like(boolean liked) {
        this.f187e.flag(8, liked);
        NewsEntry newsEntry;
        if (liked) {
            newsEntry = this.f187e;
            newsEntry.numLikes += TYPE_PHOTO;
        } else {
            newsEntry = this.f187e;
            newsEntry.numLikes--;
        }
        broadcastUpdate();
        updateButtons();
        animateLikePhotos();
        if (!this.liking) {
            this.liking = true;
            new WallLike(liked, this.f187e.ownerID, this.f187e.postID, false, this.f187e.type, 0, this.f187e.type == RESULT_DELETED ? ((VideoAttachment) this.f187e.attachments.get(0)).accessKey : ACRAConstants.DEFAULT_STRING_VALUE).setCallback(new AnonymousClass36(liked)).exec();
        }
    }

    private void repost() {
        Intent intent = new Intent(getActivity(), RepostActivity.class);
        intent.putExtra("post", this.f187e);
        startActivity(intent);
    }

    private void restoreComment(int cid) {
        new WallRestoreComment(this.f187e.ownerID, this.f187e.postID, cid, this.f187e.type, this.accessKey).setCallback(new AnonymousClass37(cid)).wrapProgress(getActivity()).exec(this.contentView);
    }

    private void loadVideoInfo() {
        new VideoGetInfo(this.f187e.ownerID, this.f187e.postID).setCallback(new VideoGetInfo.Callback() {
            public void success(ArrayList<UserProfile> tags, int numLikes, boolean isLiked, int myTagID) {
                PostViewFragment.this.f187e.flag(8, isLiked);
                PostViewFragment.this.f187e.numLikes = numLikes;
                PostViewFragment.this.updateButtons();
                if (tags.size() > 0) {
                    String t = "<font color='#AAAAAA'>" + PostViewFragment.this.getResources().getString(C0436R.string.in_this_video) + " ";
                    ArrayList<String> tt = new ArrayList();
                    Iterator it = tags.iterator();
                    while (it.hasNext()) {
                        UserProfile p = (UserProfile) it.next();
                        tt.add("<a href='vkontakte://profile/" + p.uid + "'>" + p.fullName + "</a>");
                    }
                    t = new StringBuilder(String.valueOf(t)).append(TextUtils.join(", ", tt)).append("</font>").toString();
                    ExTextView exTextView = (ExTextView) PostViewFragment.this.headerView.findViewById(C0436R.id.wall_view_post);
                    StringBuilder stringBuilder = new StringBuilder(String.valueOf(t));
                    String str = (PostViewFragment.this.f187e.text == null || PostViewFragment.this.f187e.text.length() <= 0) ? ACRAConstants.DEFAULT_STRING_VALUE : "<br/><br/>";
                    exTextView.setHTML(stringBuilder.append(str).append(PostViewFragment.this.f187e.text.replace("\n", "<br/>")).toString());
                    ((TextView) PostViewFragment.this.headerView.findViewById(C0436R.id.wall_view_post)).setMovementMethod(LinkMovementMethod.getInstance());
                    PostViewFragment.this.headerView.findViewById(C0436R.id.wall_view_post).setVisibility(0);
                }
            }

            public void fail(int ecode, String emsg) {
            }
        }).exec(this.contentView);
    }

    public void onScrolled(float offset) {
    }
}
