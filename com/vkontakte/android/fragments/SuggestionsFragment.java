package com.vkontakte.android.fragments;

import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.api.SetSubscriptionStatus;
import com.vkontakte.android.api.SetSubscriptionStatus.Callback;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.Fonts;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.PhotoView;
import java.util.ArrayList;

public abstract class SuggestionsFragment extends SherlockFragment {
    private BaseAdapter adapter;
    private ErrorView error;
    private boolean firstLoad;
    protected ListImageLoaderWrapper imgLoader;
    protected ListView list;
    private boolean needUpdateFriends;
    private boolean needUpdateGroups;
    private ProgressBar progress;
    protected ArrayList<UserProfile> users;
    private View view;

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFragment.1 */
    class C08281 implements OnItemClickListener {
        C08281() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long id) {
            SuggestionsFragment.this.onItemClick(pos, id, SuggestionsFragment.this.list.getItemAtPosition(pos));
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFragment.2 */
    class C08292 implements OnClickListener {
        C08292() {
        }

        public void onClick(View v) {
            SuggestionsFragment.this.error.setVisibility(8);
            SuggestionsFragment.this.progress.setVisibility(0);
            SuggestionsFragment.this.loadData();
        }
    }

    protected class UsersAdapter extends BaseAdapter {

        /* renamed from: com.vkontakte.android.fragments.SuggestionsFragment.UsersAdapter.1 */
        class C08311 implements OnClickListener {
            C08311() {
            }

            public void onClick(View v) {
                SuggestionsFragment.this.onSubscribeClick(((Integer) v.getTag()).intValue());
            }
        }

        protected UsersAdapter() {
        }

        public int getCount() {
            return SuggestionsFragment.this.users.size();
        }

        public Object getItem(int position) {
            return SuggestionsFragment.this.users.get(position);
        }

        public long getItemId(int position) {
            return (long) ((UserProfile) SuggestionsFragment.this.users.get(position)).uid;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            boolean z = false;
            View view = convertView;
            if (view == null) {
                view = View.inflate(SuggestionsFragment.this.getActivity(), C0436R.layout.suggest_list_item, null);
                ((TextView) view.findViewById(C0436R.id.flist_item_subtext)).setTypeface(Fonts.getRobotoLight());
                view.findViewById(C0436R.id.flist_item_online).setOnClickListener(new C08311());
            }
            if (position == 0) {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_top);
            } else if (position == SuggestionsFragment.this.users.size() - 1) {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_btm);
            } else {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_mid);
            }
            UserProfile u = (UserProfile) SuggestionsFragment.this.users.get(position);
            ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(u.fullName);
            ((TextView) view.findViewById(C0436R.id.flist_item_subtext)).setText(u.university);
            if (SuggestionsFragment.this.imgLoader.isAlreadyLoaded(u.photo)) {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageBitmap(SuggestionsFragment.this.imgLoader.get(u.photo));
            } else {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(u.uid < 0 ? C0436R.drawable.group_placeholder : C0436R.drawable.user_placeholder);
            }
            view.findViewById(C0436R.id.flist_item_online).setTag(Integer.valueOf(position));
            boolean added = u.online == 0 ? u.isFriend : u.online > 0;
            ImageView imageView = (ImageView) view.findViewById(C0436R.id.flist_item_online);
            int i = (added || u.isFriend) ? C0436R.drawable.ic_suggest_added : C0436R.drawable.ic_suggest_add;
            imageView.setImageResource(i);
            View findViewById = view.findViewById(C0436R.id.flist_item_online);
            if (!u.isFriend) {
                z = true;
            }
            findViewById.setEnabled(z);
            return view;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFragment.3 */
    class C15063 implements Callback {
        private final /* synthetic */ boolean val$needSubscribe;
        private final /* synthetic */ UserProfile val$p;

        C15063(UserProfile userProfile, boolean z) {
            this.val$p = userProfile;
            this.val$needSubscribe = z;
        }

        public void success(int result) {
            this.val$p.online = this.val$needSubscribe ? 1 : 0;
        }

        public void fail(int ecode, String emsg) {
            this.val$p.online = this.val$needSubscribe ? 0 : 1;
            SuggestionsFragment.this.adapter.notifyDataSetChanged();
            Toast.makeText(SuggestionsFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    protected class UserPhotosAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.SuggestionsFragment.UserPhotosAdapter.1 */
        class C08301 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C08301(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.flist_item_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        protected UserPhotosAdapter() {
        }

        public int getItemCount() {
            return SuggestionsFragment.this.users.size();
        }

        public int getImageCountForItem(int item) {
            return 1;
        }

        public String getImageURL(int item, int image) {
            return ((UserProfile) SuggestionsFragment.this.users.get(item)).photo;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            if (SuggestionsFragment.this.list != null) {
                item += SuggestionsFragment.this.list.getHeaderViewsCount();
                if (item >= SuggestionsFragment.this.list.getFirstVisiblePosition() && item <= SuggestionsFragment.this.list.getLastVisiblePosition()) {
                    SuggestionsFragment.this.getActivity().runOnUiThread(new C08301(SuggestionsFragment.this.list.getChildAt(item - SuggestionsFragment.this.list.getFirstVisiblePosition()), bitmap));
                }
            }
        }
    }

    protected abstract String getListTitle();

    protected abstract void loadData();

    protected abstract void onItemClick(int i, long j, Object obj);

    public SuggestionsFragment() {
        this.users = new ArrayList();
        this.firstLoad = true;
        this.needUpdateFriends = false;
        this.needUpdateGroups = false;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        loadData();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FrameLayout wrap = new FrameLayout(getActivity());
        this.list = new ListView(getActivity());
        String listTitle = getListTitle();
        if (listTitle != null) {
            TextView txt = new TextView(getActivity());
            txt.setTextColor(-8815488);
            txt.setTextSize(1, 16.0f);
            txt.setTypeface(Fonts.getRobotoLight());
            txt.setText(listTitle);
            txt.setShadowLayer((float) Global.scale(0.5f), 0.0f, 1.0f, -1);
            txt.setPadding(Global.scale(12.0f), Global.scale(15.0f), Global.scale(12.0f), Global.scale(10.0f));
            this.list.addHeaderView(txt, null, false);
        }
        this.adapter = getAdapter();
        this.list.setAdapter(this.adapter);
        this.list.setDividerHeight(0);
        this.list.setSelector(new ColorDrawable(0));
        this.list.setOnItemClickListener(new C08281());
        this.list.setCacheColorHint(getResources().getColor(C0436R.color.cards_bg));
        wrap.addView(this.list);
        wrap.setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        this.error = (ErrorView) inflater.inflate(C0436R.layout.error, null);
        this.error.setVisibility(8);
        wrap.addView(this.error);
        this.error.setOnRetryListener(new C08292());
        this.progress = new ProgressBar(getActivity());
        wrap.addView(this.progress, new LayoutParams(Global.scale(40.0f), Global.scale(40.0f), 17));
        if (this.list.getCount() - this.list.getHeaderViewsCount() == 0 && this.firstLoad) {
            this.list.setVisibility(8);
        } else {
            this.progress.setVisibility(8);
        }
        this.imgLoader = new ListImageLoaderWrapper(getImageLoaderAdapter(), this.list, null);
        this.view = wrap;
        return this.view;
    }

    protected void onError(int code, String msg) {
        if (this.error != null) {
            this.error.setErrorInfo(code, msg);
            Global.showViewAnimated(this.error, true, PhotoView.THUMB_ANIM_DURATION);
            Global.showViewAnimated(this.progress, false, PhotoView.THUMB_ANIM_DURATION);
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.view = null;
        this.adapter = null;
        this.list = null;
        this.progress = null;
        this.error = null;
        if (this.imgLoader != null) {
            this.imgLoader.deactivate();
        }
        this.imgLoader = null;
    }

    protected void updateList() {
        if (this.adapter != null) {
            this.adapter.notifyDataSetChanged();
        }
        if (this.imgLoader != null) {
            this.imgLoader.updateImages();
        }
        Log.i("vk", "items=" + (this.list.getCount() - this.list.getHeaderViewsCount()));
        if (this.firstLoad && this.list != null && this.progress != null && this.list.getCount() - this.list.getHeaderViewsCount() > 0) {
            Global.showViewAnimated(this.list, true, PhotoView.THUMB_ANIM_DURATION);
            Global.showViewAnimated(this.progress, false, PhotoView.THUMB_ANIM_DURATION);
            this.firstLoad = false;
        }
    }

    protected void onSubscribeClick(int pos) {
        UserProfile p = (UserProfile) this.users.get(pos);
        if (p.online != 2 && p.online != -2) {
            boolean needSubscribe;
            if (p.online == 0) {
                p.online = 2;
                this.adapter.notifyDataSetChanged();
                needSubscribe = true;
            } else {
                p.online = -2;
                this.adapter.notifyDataSetChanged();
                needSubscribe = false;
            }
            new SetSubscriptionStatus(p.uid, needSubscribe).setCallback(new C15063(p, needSubscribe)).exec(getActivity());
        }
    }

    protected BaseAdapter getAdapter() {
        return new UsersAdapter();
    }

    protected ListImageLoaderAdapter getImageLoaderAdapter() {
        return new UserPhotosAdapter();
    }
}
