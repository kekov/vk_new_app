package com.vkontakte.android.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.vkontakte.android.APIController;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.NewsView;
import com.vkontakte.android.NewsfeedList;
import com.vkontakte.android.NewsfeedSearchActivity;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.NewsfeedGetLists;
import com.vkontakte.android.api.NewsfeedGetLists.Callback;
import com.vkontakte.android.cache.NewsfeedCache;
import com.vkontakte.android.data.Posts;
import java.util.ArrayList;
import java.util.Iterator;

public class NewsFragment extends SherlockFragment {
    private boolean firstNav;
    private ArrayList<NewsfeedList> lists;
    private ArrayAdapter<String> navAdapter;
    private int prevNavItem;
    private BroadcastReceiver receiver;
    private NewsView view;

    /* renamed from: com.vkontakte.android.fragments.NewsFragment.1 */
    class C07301 extends BroadcastReceiver {
        C07301() {
        }

        public void onReceive(Context context, Intent intent) {
            NewsEntry post;
            if (Posts.ACTION_NEW_POST_BROADCAST.equals(intent.getAction())) {
                if (NewsFragment.this.view.getList() == 0) {
                    post = (NewsEntry) intent.getParcelableExtra("entry");
                    if (!((post.ownerID != Global.uid && (post.ownerID >= 0 || post.ownerID != post.userID)) || post.flag(NewsEntry.FLAG_POSTPONED) || post.flag(NewsEntry.FLAG_SUGGESTED))) {
                        NewsFragment.this.view.prepend(post);
                    }
                } else {
                    return;
                }
            }
            if (Posts.ACTION_POST_DELETED_BROADCAST.equals(intent.getAction())) {
                NewsFragment.this.view.remove(intent.getIntExtra("owner_id", 0), intent.getIntExtra("post_id", 0));
            }
            if (Posts.ACTION_POST_UPDATED_BROADCAST.equals(intent.getAction())) {
                NewsFragment.this.view.update(intent.getIntExtra("owner_id", 0), intent.getIntExtra("post_id", 0), intent.getIntExtra("likes", 0), intent.getIntExtra("comments", 0), intent.getIntExtra("retweets", 0), intent.getBooleanExtra("liked", false), intent.getBooleanExtra("retweeted", false));
            }
            if (Posts.ACTION_POST_REPLACED_BROADCAST.equals(intent.getAction())) {
                post = (NewsEntry) intent.getParcelableExtra("entry");
                if (NewsFragment.this.getArguments() != null) {
                    String mode = NewsFragment.this.getArguments().getString("mode");
                    if ("postponed".equals(mode) && !post.flag(NewsEntry.FLAG_POSTPONED)) {
                        return;
                    }
                    if ("suggested".equals(mode) && !post.flag(NewsEntry.FLAG_SUGGESTED)) {
                        return;
                    }
                }
                if (post.ownerID == Global.uid || ((post.ownerID < 0 && post.ownerID == post.userID) || (NewsFragment.this.getArguments() != null && NewsFragment.this.getArguments().containsKey("mode")))) {
                    NewsFragment.this.view.replace(post);
                }
            }
            if (Posts.ACTION_RELOAD_FEED.equals(intent.getAction())) {
                NewsFragment.this.view.setList(-9000);
                NewsFragment.this.view.loadData(true);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.NewsFragment.2 */
    class C07312 extends ArrayAdapter<String> {
        C07312(Context $anonymous0, int $anonymous1) {
            super($anonymous0, $anonymous1);
        }

        public View getView(int pos, View view, ViewGroup group) {
            return super.getView(NewsFragment.this.getSherlockActivity().getSupportActionBar().getSelectedNavigationIndex(), view, group);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.NewsFragment.4 */
    class C07334 implements Runnable {
        private final /* synthetic */ Activity val$act;
        private final /* synthetic */ SherlockFragmentActivity val$sa;

        /* renamed from: com.vkontakte.android.fragments.NewsFragment.4.1 */
        class C07321 implements Runnable {
            private final /* synthetic */ Activity val$act;
            private final /* synthetic */ SherlockFragmentActivity val$sa;

            C07321(Activity activity, SherlockFragmentActivity sherlockFragmentActivity) {
                this.val$act = activity;
                this.val$sa = sherlockFragmentActivity;
            }

            public void run() {
                Iterator it = NewsFragment.this.lists.iterator();
                while (it.hasNext()) {
                    NewsFragment.this.navAdapter.add(((NewsfeedList) it.next()).title);
                }
                NewsFragment.this.updateLists();
                int lid = this.val$act.getSharedPreferences(null, 0).getInt("feed_list", 0);
                if (lid == -1) {
                    this.val$sa.getSupportActionBar().setSelectedNavigationItem(1);
                }
                if (lid == -2) {
                    this.val$sa.getSupportActionBar().setSelectedNavigationItem(2);
                }
                if (lid == -3) {
                    this.val$sa.getSupportActionBar().setSelectedNavigationItem(3);
                }
                int i = 5;
                it = NewsFragment.this.lists.iterator();
                while (it.hasNext()) {
                    if (((NewsfeedList) it.next()).id == lid) {
                        this.val$sa.getSupportActionBar().setSelectedNavigationItem(i);
                        return;
                    }
                    i++;
                }
            }
        }

        C07334(Activity activity, SherlockFragmentActivity sherlockFragmentActivity) {
            this.val$act = activity;
            this.val$sa = sherlockFragmentActivity;
        }

        public void run() {
            NewsFragment.this.lists.addAll(NewsfeedCache.getLists());
            this.val$act.runOnUiThread(new C07321(this.val$act, this.val$sa));
        }
    }

    /* renamed from: com.vkontakte.android.fragments.NewsFragment.5 */
    class C07345 implements OnClickListener {
        C07345() {
        }

        public void onClick(View v) {
            NewsFragment.this.view.scrollToTop();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.NewsFragment.7 */
    class C07357 implements Runnable {
        C07357() {
        }

        public void run() {
            if (NewsfeedCache.hasEntries(NewsFragment.this.getActivity()) && PreferenceManager.getDefaultSharedPreferences(NewsFragment.this.getActivity()).getBoolean("refreshOnOpen", true) && NewsFragment.this.view != null && NewsFragment.this.getActivity() != null) {
                if (NewsFragment.this.getArguments() == null || !NewsFragment.this.getArguments().containsKey("owner_id")) {
                    NewsFragment.this.view.preloadNew();
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.NewsFragment.3 */
    class C14923 implements OnNavigationListener {
        C14923() {
        }

        public boolean onNavigationItemSelected(int itemPosition, long itemId) {
            if (NewsFragment.this.firstNav) {
                NewsFragment.this.firstNav = false;
            } else {
                if (itemPosition == 0) {
                    NewsFragment.this.getActivity().getSharedPreferences(null, 0).edit().putInt("feed_list", 0).commit();
                    if (NewsFragment.this.view.setList(0)) {
                        NewsFragment.this.view.loadData(true);
                    }
                }
                if (itemPosition == 1) {
                    NewsFragment.this.getActivity().getSharedPreferences(null, 0).edit().putInt("feed_list", -1).commit();
                    if (NewsFragment.this.view.setList(-1)) {
                        NewsFragment.this.view.loadData(true);
                    }
                }
                if (itemPosition == 2) {
                    NewsFragment.this.getActivity().getSharedPreferences(null, 0).edit().putInt("feed_list", -2).commit();
                    if (NewsFragment.this.view.setList(-2)) {
                        NewsFragment.this.view.loadData(true);
                    }
                }
                if (itemPosition == 3) {
                    NewsFragment.this.getActivity().getSharedPreferences(null, 0).edit().putInt("feed_list", -3).commit();
                    if (NewsFragment.this.view.setList(-3)) {
                        NewsFragment.this.view.loadData(true);
                    }
                }
                if (itemPosition == 4) {
                    NewsFragment.this.getSherlockActivity().getSupportActionBar().setSelectedNavigationItem(NewsFragment.this.prevNavItem);
                    NewsFragment.this.startActivity(new Intent(NewsFragment.this.getActivity(), NewsfeedSearchActivity.class));
                } else {
                    NewsFragment.this.prevNavItem = itemPosition;
                }
                if (itemPosition > 4) {
                    int lid = ((NewsfeedList) NewsFragment.this.lists.get(itemPosition - 5)).id;
                    NewsFragment.this.getActivity().getSharedPreferences(null, 0).edit().putInt("feed_list", lid).commit();
                    if (NewsFragment.this.view.setList(lid)) {
                        NewsFragment.this.view.loadData(true);
                    }
                }
            }
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.NewsFragment.6 */
    class C14936 implements Callback {
        C14936() {
        }

        public void success(ArrayList<NewsfeedList> rlists) {
            if (NewsFragment.this.getActivity() != null) {
                NewsFragment.this.navAdapter.clear();
                NewsFragment.this.navAdapter.add(NewsFragment.this.getString(C0436R.string.newsfeed));
                NewsFragment.this.navAdapter.add(NewsFragment.this.getString(C0436R.string.recommendations));
                NewsFragment.this.navAdapter.add(NewsFragment.this.getString(C0436R.string.friends));
                NewsFragment.this.navAdapter.add(NewsFragment.this.getString(C0436R.string.groups));
                NewsFragment.this.navAdapter.add(NewsFragment.this.getString(C0436R.string.search));
                NewsFragment.this.lists.clear();
                NewsFragment.this.lists.addAll(rlists);
                Iterator it = NewsFragment.this.lists.iterator();
                while (it.hasNext()) {
                    NewsFragment.this.navAdapter.add(((NewsfeedList) it.next()).title);
                }
                NewsfeedCache.setLists(rlists);
            }
        }

        public void fail(int ecode, String emsg) {
        }
    }

    public NewsFragment() {
        this.receiver = new C07301();
        this.firstNav = true;
        this.lists = new ArrayList();
        this.prevNavItem = 0;
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Posts.ACTION_NEW_POST_BROADCAST);
        filter.addAction(Posts.ACTION_POST_DELETED_BROADCAST);
        filter.addAction(Posts.ACTION_POST_UPDATED_BROADCAST);
        filter.addAction(Posts.ACTION_POST_REPLACED_BROADCAST);
        filter.addAction(Posts.ACTION_RELOAD_FEED);
        VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        SherlockFragmentActivity sa = getSherlockActivity();
        if (getArguments() == null || !getArguments().containsKey("owner_id")) {
            sa.getSupportActionBar().setNavigationMode(1);
            this.navAdapter = new C07312(sa.getSupportActionBar().getThemedContext(), C0436R.layout.nav_spinner_item);
            this.navAdapter.add(getString(C0436R.string.newsfeed));
            this.navAdapter.add(getString(C0436R.string.recommendations));
            this.navAdapter.add(getString(C0436R.string.friends));
            this.navAdapter.add(getString(C0436R.string.groups));
            this.navAdapter.add(getString(C0436R.string.search));
            this.navAdapter.setDropDownViewResource(C0436R.layout.sherlock_spinner_dropdown_item);
            sa.getSupportActionBar().setListNavigationCallbacks(this.navAdapter, new C14923());
            sa.getSupportActionBar().setDisplayShowTitleEnabled(false);
            new Thread(new C07334(act, sa)).start();
            int lid = act.getSharedPreferences(null, 0).getInt("feed_list", 0);
            if (this.view == null) {
                this.view = new NewsView(getActivity());
                this.view.setList(lid);
            }
            setHasOptionsMenu(true);
        } else if (this.view == null) {
            this.view = new NewsView(getActivity(), true);
            int oid = getArguments().getInt("owner_id");
            if (getArguments().getString("mode").equals("postponed")) {
                this.view.initPostponed(oid);
                getActivity().setTitle(C0436R.string.postponed_posts_title);
            }
            if (getArguments().getString("mode").equals("suggested")) {
                this.view.initSuggests(oid);
                getActivity().setTitle(C0436R.string.suggested_posts_title);
            }
        }
        this.view.loadData(false);
        int abId = Resources.getSystem().getIdentifier("action_bar_container", "id", "android");
        if (abId == 0) {
            abId = C0436R.id.abs__action_bar_container;
        }
        View actionBarView = getActivity().findViewById(abId);
        if (actionBarView != null) {
            actionBarView.setOnClickListener(new C07345());
        }
    }

    public void onDetach() {
        int abId = Resources.getSystem().getIdentifier("action_bar_container", "id", "android");
        if (abId == 0) {
            abId = C0436R.id.abs__action_bar_container;
        }
        View actionBarView = getActivity().findViewById(abId);
        if (actionBarView != null) {
            actionBarView.setOnClickListener(null);
        }
        getSherlockActivity().getSupportActionBar().setListNavigationCallbacks(null, null);
        getSherlockActivity().getSupportActionBar().setNavigationMode(0);
        super.onDetach();
    }

    private void updateLists() {
        new NewsfeedGetLists().setCallback(new C14936()).exec(getActivity());
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(C0436R.menu.newsfeed, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != C0436R.id.newpost) {
            return false;
        }
        startActivity(new Intent(getActivity(), NewPostActivity.class));
        return true;
    }

    public void onResume() {
        super.onResume();
        APIController.runInBg(new C07357());
        this.view.onResume();
    }

    public void onPause() {
        super.onPause();
        this.view.beforeDestroy();
        this.view.onPause();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (this.view.getParent() != null) {
            ((ViewGroup) this.view.getParent()).removeView(this.view);
        }
        return this.view;
    }
}
