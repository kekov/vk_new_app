package com.vkontakte.android.fragments;

import android.app.Activity;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;

public class DarkBarFragment extends SherlockFragment {
    public void onAttach(Activity act) {
        super.onAttach(act);
        getSherlockActivity().getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(C0436R.drawable.bg_actionbar_black));
        if (getActivity().findViewById(C0436R.id.badge) != null) {
            getActivity().findViewById(C0436R.id.badge).setBackgroundResource(C0436R.drawable.badge_head_black);
            ((TextView) getActivity().findViewById(C0436R.id.badge)).setTextColor(Color.WINDOW_BACKGROUND_COLOR);
            getActivity().findViewById(C0436R.id.badge).setPadding(getActivity().findViewById(C0436R.id.badge).getPaddingLeft(), Global.scale(GroundOverlayOptions.NO_DIMENSION), getActivity().findViewById(C0436R.id.badge).getPaddingRight(), getActivity().findViewById(C0436R.id.badge).getPaddingBottom());
        }
    }

    public void onDetach() {
        getSherlockActivity().getSupportActionBar().removeAllTabs();
        getSherlockActivity().getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(C0436R.drawable.bg_actionbar));
        if (getActivity().findViewById(C0436R.id.badge) != null) {
            getActivity().findViewById(C0436R.id.badge).setBackgroundResource(C0436R.drawable.badge_head);
            ((TextView) getActivity().findViewById(C0436R.id.badge)).setTextColor(-12621929);
            getActivity().findViewById(C0436R.id.badge).setPadding(getActivity().findViewById(C0436R.id.badge).getPaddingLeft(), Global.scale(GroundOverlayOptions.NO_DIMENSION), getActivity().findViewById(C0436R.id.badge).getPaddingRight(), getActivity().findViewById(C0436R.id.badge).getPaddingBottom());
        }
        super.onDetach();
    }
}
