package com.vkontakte.android.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockFragment;
import com.facebook.WebDialog;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.GeoAttachment;
import com.vkontakte.android.GeoPlace;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageCache;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.PlacesGetInfo;
import com.vkontakte.android.api.PlacesGetInfo.Callback;
import java.util.ArrayList;
import org.acra.ACRAConstants;

public class GeoPlaceFragment extends SherlockFragment {
    private GeoAttachment att;
    private APIRequest currentRequest;
    private String groupPhoto;
    private String groupStatus;
    private MapView mapView;
    private GeoPlace place;
    private ArrayList<String> userPhotos;
    private View view;

    /* renamed from: com.vkontakte.android.fragments.GeoPlaceFragment.1 */
    class C07041 implements OnClickListener {
        C07041() {
        }

        public void onClick(DialogInterface dialog, int which) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps"));
            intent.addFlags(268435456);
            GeoPlaceFragment.this.startActivity(intent);
            GeoPlaceFragment.this.getActivity().finish();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GeoPlaceFragment.2 */
    class C07052 implements OnClickListener {
        C07052() {
        }

        public void onClick(DialogInterface dialog, int which) {
            GeoPlaceFragment.this.getActivity().finish();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GeoPlaceFragment.3 */
    class C07063 implements OnCancelListener {
        C07063() {
        }

        public void onCancel(DialogInterface dialog) {
            GeoPlaceFragment.this.getActivity().finish();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GeoPlaceFragment.5 */
    class C07085 implements View.OnClickListener {

        /* renamed from: com.vkontakte.android.fragments.GeoPlaceFragment.5.1 */
        class C07071 implements OnClickListener {
            C07071() {
            }

            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.maps"));
                intent.addFlags(268435456);
                GeoPlaceFragment.this.startActivity(intent);
            }
        }

        C07085() {
        }

        public void onClick(View v) {
            try {
                GeoPlaceFragment.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("geo:" + GeoPlaceFragment.this.place.lat + "," + GeoPlaceFragment.this.place.lon + "?z=18&q=" + GeoPlaceFragment.this.place.lat + "," + GeoPlaceFragment.this.place.lon)));
            } catch (Throwable th) {
                new Builder(GeoPlaceFragment.this.getActivity()).setTitle(C0436R.string.maps_not_available).setMessage(C0436R.string.maps_not_available_descr).setPositiveButton(C0436R.string.open_google_play, new C07071()).setNegativeButton(C0436R.string.close, null).show();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GeoPlaceFragment.6 */
    class C07096 implements View.OnClickListener {
        C07096() {
        }

        public void onClick(View v) {
            Intent intent = new Intent();
            intent.putExtra("point", GeoPlaceFragment.this.att);
            GeoPlaceFragment.this.getActivity().setResult(-1, intent);
            GeoPlaceFragment.this.getActivity().finish();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GeoPlaceFragment.7 */
    class C07107 implements View.OnClickListener {
        C07107() {
        }

        public void onClick(View v) {
            if (GeoPlaceFragment.this.place != null && GeoPlaceFragment.this.place.groupID != 0) {
                Bundle args = new Bundle();
                args.putInt("id", -GeoPlaceFragment.this.place.groupID);
                Navigate.to("ProfileFragment", args, GeoPlaceFragment.this.getActivity());
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GeoPlaceFragment.8 */
    class C07118 implements View.OnClickListener {
        C07118() {
        }

        public void onClick(View v) {
            Bundle args = new Bundle();
            args.putInt("place_id", GeoPlaceFragment.this.att.id);
            args.putInt(WebDialog.DIALOG_PARAM_TYPE, 9);
            args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, GeoPlaceFragment.this.getResources().getString(C0436R.string.checked_in));
            Navigate.to("UserListFragment", args, GeoPlaceFragment.this.getActivity());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GeoPlaceFragment.9 */
    class C07129 implements OnPreDrawListener {
        C07129() {
        }

        public boolean onPreDraw() {
            GeoPlaceFragment.this.view.getViewTreeObserver().removeOnPreDrawListener(this);
            ViewGroup vg = (ViewGroup) GeoPlaceFragment.this.view.findViewById(C0436R.id.place_checkins_wrap);
            for (int i = 0; i < vg.getChildCount(); i++) {
                View v = vg.getChildAt(i);
                if (v.getRight() > vg.getWidth() - Global.scale(10.0f)) {
                    v.setVisibility(4);
                } else {
                    v.setVisibility(0);
                }
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GeoPlaceFragment.4 */
    class C14794 extends MapView {
        C14794(Context $anonymous0, GoogleMapOptions $anonymous1) {
            super($anonymous0, $anonymous1);
        }

        public boolean dispatchTouchEvent(MotionEvent ev) {
            return false;
        }
    }

    public GeoPlaceFragment() {
        this.place = null;
        this.groupPhoto = ACRAConstants.DEFAULT_STRING_VALUE;
        this.groupStatus = ACRAConstants.DEFAULT_STRING_VALUE;
        this.userPhotos = new ArrayList();
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        this.att = (GeoAttachment) getArguments().getParcelable("point");
        act.setTitle(C0436R.string.place);
        if (!Global.isAppInstalled(getActivity(), "com.google.android.apps.maps") || GooglePlayServicesUtil.isGooglePlayServicesAvailable(VKApplication.context) != 0) {
            new Builder(getActivity()).setTitle(C0436R.string.maps_not_available).setMessage(C0436R.string.maps_not_available_descr).setPositiveButton(C0436R.string.open_google_play, new C07041()).setNegativeButton(C0436R.string.close, new C07052()).setOnCancelListener(new C07063()).show();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(C0436R.layout.place_header, null);
        if (!Global.isAppInstalled(getActivity(), "com.google.android.apps.maps") || GooglePlayServicesUtil.isGooglePlayServicesAvailable(VKApplication.context) != 0) {
            return this.view;
        }
        CharSequence charSequence;
        this.mapView = new C14794(getActivity(), new GoogleMapOptions().compassEnabled(false).zoomControlsEnabled(false));
        this.mapView.onCreate(savedInstanceState);
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
        }
        ((ViewGroup) this.view.findViewById(C0436R.id.place_map_wrap)).addView(this.mapView);
        GoogleMap map = this.mapView.getMap();
        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(new LatLng(this.att.lat, this.att.lon)).zoom(16.0f).build()));
        map.addMarker(new MarkerOptions().position(new LatLng(this.att.lat, this.att.lon)));
        ((TextView) this.view.findViewById(C0436R.id.place_title)).setText(this.att.title);
        ((TextView) this.view.findViewById(C0436R.id.place_address)).setText(this.att.address);
        ((TextView) this.view.findViewById(C0436R.id.place_status)).setText(this.groupStatus);
        TextView textView = (TextView) this.view.findViewById(C0436R.id.place_checkins);
        if (this.place == null) {
            charSequence = ACRAConstants.DEFAULT_STRING_VALUE;
        } else {
            charSequence = this.place.checkins;
        }
        textView.setText(charSequence);
        updateUserPhotos();
        updateFieldsVisibility();
        this.view.findViewById(C0436R.id.place_map_wrap).setOnClickListener(new C07085());
        if (getArguments().getBoolean("checkin")) {
            this.view.findViewById(C0436R.id.place_btn_checkin).setOnClickListener(new C07096());
        } else {
            this.view.findViewById(C0436R.id.place_btn_checkin).setVisibility(8);
        }
        View.OnClickListener clickListener = new C07107();
        this.view.findViewById(C0436R.id.place_photo).setOnClickListener(clickListener);
        this.view.findViewById(C0436R.id.place_title).setOnClickListener(clickListener);
        this.view.findViewById(C0436R.id.place_status).setOnClickListener(clickListener);
        this.view.findViewById(C0436R.id.place_checkins_wrap).setOnClickListener(new C07118());
        return this.view;
    }

    public void onPause() {
        super.onPause();
        if (this.mapView != null) {
            this.mapView.onPause();
        }
    }

    public void onResume() {
        super.onResume();
        if (this.mapView != null) {
            this.mapView.onResume();
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        loadData();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.currentRequest != null) {
            this.currentRequest.cancel();
            this.currentRequest = null;
        }
    }

    public void onConfigurationChanged(Configuration cfg) {
        super.onConfigurationChanged(cfg);
        updateVisiblePhotos();
    }

    private void updateVisiblePhotos() {
        this.view.getViewTreeObserver().addOnPreDrawListener(new C07129());
    }

    private void updateFieldsVisibility() {
        View findViewById = this.view.findViewById(C0436R.id.place_checkins_highlight);
        int i = (this.place == null || this.place.checkins <= 0) ? 8 : 0;
        findViewById.setVisibility(i);
        findViewById = this.view.findViewById(C0436R.id.place_address);
        if (this.place == null || this.place.address == null || this.place.address.length() <= 0) {
            i = 8;
        } else {
            i = 0;
        }
        findViewById.setVisibility(i);
        if (this.view.findViewById(C0436R.id.place_checkins_highlight).getVisibility() == 0 && this.view.findViewById(C0436R.id.place_address).getVisibility() == 0) {
            this.view.findViewById(C0436R.id.place_separator).setVisibility(0);
        } else {
            this.view.findViewById(C0436R.id.place_separator).setVisibility(8);
        }
    }

    private void updateUserPhotos() {
        for (int i = 0; i < Math.min(10, this.userPhotos.size()); i++) {
            ImageView iv = new ImageView(getActivity());
            iv.setImageResource(C0436R.drawable.user_placeholder);
            LayoutParams lp = new LayoutParams(Global.scale(35.0f), Global.scale(35.0f));
            lp.rightMargin = Global.scale(4.0f);
            ((ViewGroup) this.view.findViewById(C0436R.id.place_checkins_wrap)).addView(iv, lp);
        }
        updateVisiblePhotos();
        new Thread(new Runnable() {

            /* renamed from: com.vkontakte.android.fragments.GeoPlaceFragment.10.1 */
            class C07021 implements Runnable {
                private final /* synthetic */ Bitmap val$gp;

                C07021(Bitmap bitmap) {
                    this.val$gp = bitmap;
                }

                public void run() {
                    ((ImageView) GeoPlaceFragment.this.view.findViewById(C0436R.id.place_photo)).setImageBitmap(this.val$gp);
                }
            }

            /* renamed from: com.vkontakte.android.fragments.GeoPlaceFragment.10.2 */
            class C07032 implements Runnable {
                private final /* synthetic */ Bitmap val$bmp;
                private final /* synthetic */ ImageView val$iv;

                C07032(ImageView imageView, Bitmap bitmap) {
                    this.val$iv = imageView;
                    this.val$bmp = bitmap;
                }

                public void run() {
                    this.val$iv.setImageBitmap(this.val$bmp);
                }
            }

            public void run() {
                if (GeoPlaceFragment.this.groupPhoto != null) {
                    Bitmap gp = ImageCache.get(GeoPlaceFragment.this.groupPhoto);
                    if (GeoPlaceFragment.this.view != null) {
                        GeoPlaceFragment.this.view.post(new C07021(gp));
                    } else {
                        return;
                    }
                }
                int i = 0;
                while (i < Math.min(10, GeoPlaceFragment.this.userPhotos.size())) {
                    ImageView iv = (ImageView) ((ViewGroup) GeoPlaceFragment.this.view.findViewById(C0436R.id.place_checkins_wrap)).getChildAt(i + 1);
                    Bitmap bmp = ImageCache.get((String) GeoPlaceFragment.this.userPhotos.get(i));
                    if (GeoPlaceFragment.this.view != null) {
                        GeoPlaceFragment.this.view.post(new C07032(iv, bmp));
                        i++;
                    } else {
                        return;
                    }
                }
            }
        }).start();
    }

    private void loadData() {
        this.currentRequest = new PlacesGetInfo(this.att.id).setCallback(new Callback() {
            public void success(GeoPlace _place, ArrayList<String> _userPhotos, String _groupStatus, String _groupPhoto) {
                GeoPlaceFragment.this.currentRequest = null;
                GeoPlaceFragment.this.place = _place;
                GeoPlaceFragment.this.userPhotos = _userPhotos;
                GeoPlaceFragment.this.groupStatus = _groupStatus;
                GeoPlaceFragment.this.groupPhoto = _groupPhoto;
                if (GeoPlaceFragment.this.view != null) {
                    ((TextView) GeoPlaceFragment.this.view.findViewById(C0436R.id.place_status)).setText(GeoPlaceFragment.this.groupStatus);
                    ((TextView) GeoPlaceFragment.this.view.findViewById(C0436R.id.place_checkins)).setText(GeoPlaceFragment.this.place.checkins);
                    GeoPlaceFragment.this.updateUserPhotos();
                    GeoPlaceFragment.this.updateFieldsVisibility();
                }
            }

            public void fail(int ecode, String emsg) {
                GeoPlaceFragment.this.currentRequest = null;
            }
        }).exec(getActivity());
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (this.mapView != null) {
            this.mapView.onDestroy();
        }
        this.view = null;
        this.mapView = null;
    }
}
