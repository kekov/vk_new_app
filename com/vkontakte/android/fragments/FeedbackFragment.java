package com.vkontakte.android.fragments;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.GCMBroadcastReceiver;
import com.vkontakte.android.Global;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.NewsView;
import com.vkontakte.android.NotificationsView;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import java.util.ArrayList;

public class FeedbackFragment extends SherlockFragment {
    private boolean commentsLoaded;
    private NewsView commentsView;
    private LinearLayout contentView;
    private NotificationsView notifyView;
    private ViewPager pager;
    private boolean showFilter;
    private PagerSlidingTabStrip tabs;

    /* renamed from: com.vkontakte.android.fragments.FeedbackFragment.2 */
    class C06782 implements OnMultiChoiceClickListener {
        private final /* synthetic */ boolean[] val$vals;

        C06782(boolean[] zArr) {
            this.val$vals = zArr;
        }

        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
            this.val$vals[which] = isChecked;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FeedbackFragment.3 */
    class C06793 implements OnClickListener {
        private final /* synthetic */ String[] val$optsVals;
        private final /* synthetic */ boolean[] val$vals;

        C06793(boolean[] zArr, String[] strArr) {
            this.val$vals = zArr;
            this.val$optsVals = strArr;
        }

        public void onClick(DialogInterface dialog, int which) {
            ArrayList<String> sett = new ArrayList();
            for (int i = 0; i < this.val$vals.length; i++) {
                if (this.val$vals[i]) {
                    sett.add(this.val$optsVals[i]);
                }
            }
            FeedbackFragment.this.getActivity().getSharedPreferences(null, 0).edit().putString("notifications_filter", TextUtils.join(",", sett)).commit();
            FeedbackFragment.this.notifyView.refresh();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FeedbackFragment.1 */
    class C14661 implements OnPageChangeListener {
        C14661() {
        }

        public void onPageSelected(int page) {
            boolean z = true;
            if (page == 1 && !FeedbackFragment.this.commentsLoaded) {
                FeedbackFragment.this.commentsView.loadData(true);
                FeedbackFragment.this.commentsLoaded = true;
            }
            FeedbackFragment feedbackFragment = FeedbackFragment.this;
            if (page != 0) {
                z = false;
            }
            feedbackFragment.showFilter = z;
            FeedbackFragment.this.getSherlockActivity().invalidateOptionsMenu();
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int page) {
        }
    }

    private class FeedbackPagerAdapter extends PagerAdapter {
        private FeedbackPagerAdapter() {
        }

        public int getCount() {
            return 2;
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public Object instantiateItem(ViewGroup container, int position) {
            View v = null;
            switch (position) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    v = FeedbackFragment.this.notifyView;
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    v = FeedbackFragment.this.commentsView;
                    break;
            }
            container.addView(v);
            return v;
        }

        public CharSequence getPageTitle(int pos) {
            switch (pos) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return FeedbackFragment.this.getString(C0436R.string.replies);
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return FeedbackFragment.this.getString(C0436R.string.comments);
                default:
                    return null;
            }
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            View v = null;
            switch (position) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    v = FeedbackFragment.this.notifyView;
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    v = FeedbackFragment.this.commentsView;
                    break;
            }
            container.removeView(v);
        }
    }

    public FeedbackFragment() {
        this.commentsLoaded = false;
        this.showFilter = true;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        SherlockFragmentActivity sa = getSherlockActivity();
        sa.getSupportActionBar().setNavigationMode(0);
        sa.getSupportActionBar().setDisplayShowTitleEnabled(true);
        act.setTitle(C0436R.string.feedback);
        ((NotificationManager) act.getSystemService("notification")).cancel(GCMBroadcastReceiver.ID_REPLY_NOTIFICATION);
        this.contentView = new LinearLayout(act);
        this.commentsView = new NewsView(act);
        this.commentsView.setCommentsMode(true);
        this.notifyView = new NotificationsView(act);
        boolean fromCache = true;
        if (NotificationsView.notifications == null || NotificationsView.notifications.size() == 0) {
            fromCache = this.notifyView.loadData(false);
        }
        if (fromCache) {
            this.notifyView.refresh();
        }
        int i = LongPollService.numNotifications;
        this.pager = new ViewPager(act);
        this.pager.setAdapter(new FeedbackPagerAdapter());
        this.tabs = new PagerSlidingTabStrip(act);
        this.tabs.setViewPager(this.pager);
        this.tabs.setOnPageChangeListener(new C14661());
        this.tabs.setBackgroundResource(C0436R.color.tab_bg);
        this.tabs.setIndicatorColorResource(C0436R.color.tab_indicator);
        this.contentView.setOrientation(1);
        this.contentView.addView(this.tabs, new LayoutParams(-1, Global.scale(GalleryPickerFooterView.SIZE)));
        this.contentView.addView(this.pager, new LayoutParams(-1, -1));
        setHasOptionsMenu(true);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(C0436R.menu.replies, menu);
    }

    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(C0436R.id.filter).setVisible(this.showFilter);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != C0436R.id.filter) {
            return false;
        }
        showFilter();
        return true;
    }

    private void showFilter() {
        String[] opts = new String[]{getString(C0436R.string.wall), getString(C0436R.string.mentions), getString(C0436R.string.comments), getString(C0436R.string.likes), getString(C0436R.string.reposts), getString(C0436R.string.followers)};
        String[] _setting = getActivity().getSharedPreferences(null, 0).getString("notifications_filter", "wall,mentions,comments,likes,reposts,followers|friends").split(",");
        ArrayList<String> setting = new ArrayList();
        for (String s : _setting) {
            setting.add(s);
        }
        String[] optsVals = new String[]{"wall", "mentions", "comments", "likes", "reposts", "followers|friends"};
        boolean[] vals = new boolean[optsVals.length];
        for (int i = 0; i < vals.length; i++) {
            vals[i] = setting.contains(optsVals[i]);
        }
        new Builder(getActivity()).setTitle(C0436R.string.filter).setMultiChoiceItems(opts, vals, new C06782(vals)).setPositiveButton(C0436R.string.ok, new C06793(vals, optsVals)).setNegativeButton(C0436R.string.cancel, null).show();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentView;
    }

    public void onPause() {
        super.onPause();
        this.notifyView.onPause();
        this.commentsView.onPause();
    }

    public void onResume() {
        super.onResume();
        this.notifyView.onResume();
        this.commentsView.onResume();
    }
}
