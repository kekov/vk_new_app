package com.vkontakte.android.fragments;

import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.actionbarsherlock.app.SherlockFragment;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImagePickerActivity;
import com.vkontakte.android.Log;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.mediapicker.providers.GalleryPickerProvider;
import com.vkontakte.android.ui.Fonts;
import java.io.File;
import java.util.ArrayList;

public class SignupProfileFragment extends SherlockFragment {
    private static final int AVA_RESULT = 101;
    private OnClickListener btnClickListener;
    private int gender;
    private String photo;
    private View view;

    /* renamed from: com.vkontakte.android.fragments.SignupProfileFragment.1 */
    class C08111 implements OnClickListener {
        C08111() {
        }

        public void onClick(View v) {
            SignupProfileFragment.this.view.findViewById(C0436R.id.signup_gender_male).setSelected(true);
            SignupProfileFragment.this.view.findViewById(C0436R.id.signup_gender_female).setSelected(false);
            SignupProfileFragment.this.gender = 2;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupProfileFragment.2 */
    class C08122 implements OnClickListener {
        C08122() {
        }

        public void onClick(View v) {
            SignupProfileFragment.this.view.findViewById(C0436R.id.signup_gender_male).setSelected(false);
            SignupProfileFragment.this.view.findViewById(C0436R.id.signup_gender_female).setSelected(true);
            SignupProfileFragment.this.gender = 1;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupProfileFragment.3 */
    class C08133 implements OnClickListener {
        C08133() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(SignupProfileFragment.this.getActivity(), ImagePickerActivity.class);
            intent.putExtra("allow_album", false);
            intent.putExtra("limit", 1);
            ArrayList<String> acts = new ArrayList();
            if (SignupProfileFragment.this.photo != null) {
                acts.add(SignupProfileFragment.this.getString(C0436R.string.delete));
            }
            intent.putExtra("custom", acts);
            SignupProfileFragment.this.startActivityForResult(intent, SignupProfileFragment.AVA_RESULT);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupProfileFragment.4 */
    class C08144 implements OnEditorActionListener {
        C08144() {
        }

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            SignupProfileFragment.this.onClick(v);
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupProfileFragment.5 */
    class C08155 implements OnClickListener {
        C08155() {
        }

        public void onClick(View v) {
            SignupProfileFragment.this.onClick(v);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupProfileFragment.6 */
    class C08166 implements Runnable {
        C08166() {
        }

        public void run() {
            SignupProfileFragment.this.view.findViewById(C0436R.id.signup_first_name).clearFocus();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupProfileFragment.7 */
    class C08187 implements Runnable {

        /* renamed from: com.vkontakte.android.fragments.SignupProfileFragment.7.1 */
        class C08171 implements Runnable {
            private final /* synthetic */ Bitmap val$bmp;

            C08171(Bitmap bitmap) {
                this.val$bmp = bitmap;
            }

            public void run() {
                Log.m528i("vk", "Set thumb " + this.val$bmp);
                ((ImageView) SignupProfileFragment.this.view.findViewById(C0436R.id.signup_photo)).setImageBitmap(this.val$bmp);
            }
        }

        C08187() {
        }

        public void run() {
            try {
                Bitmap bmp;
                Uri uri = Uri.parse(SignupProfileFragment.this.photo);
                if (uri.getScheme().equals("file") || uri.getScheme().equals("content")) {
                    ParcelFileDescriptor fd = SignupProfileFragment.this.getActivity().getContentResolver().openFileDescriptor(uri, "r");
                    int rotation = 0;
                    if (uri.getScheme().equals("content")) {
                        try {
                            Cursor c = SignupProfileFragment.this.getActivity().getContentResolver().query(uri, new String[]{"orientation"}, null, null, null);
                            if (c.moveToFirst()) {
                                rotation = c.getInt(0);
                            }
                            Log.m528i("vk", "img rotation is " + rotation);
                        } catch (Exception e) {
                        }
                    }
                    if (uri.getScheme().equals("file") || rotation == 0) {
                        String realPath = SignupProfileFragment.this.getRealPathFromURI(uri);
                        if (realPath != null) {
                            try {
                                int o = new ExifInterface(realPath).getAttributeInt("Orientation", 0);
                                Log.m528i("vk", "Exif orientation " + o);
                                switch (o) {
                                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                                        rotation = 0;
                                        break;
                                    case Group.ADMIN_LEVEL_ADMIN /*3*/:
                                        rotation = 180;
                                        break;
                                    case UserListView.TYPE_POLL_VOTERS /*6*/:
                                        rotation = 90;
                                        break;
                                    case UserListView.TYPE_BLACKLIST /*8*/:
                                        rotation = 270;
                                        break;
                                }
                            } catch (Exception e2) {
                            }
                        }
                    }
                    Options opts = new Options();
                    opts.inJustDecodeBounds = true;
                    BitmapFactory.decodeFileDescriptor(fd.getFileDescriptor(), null, opts);
                    int ts = Global.scale(90.0f);
                    int bs = Math.min(opts.outWidth, opts.outHeight);
                    opts.inJustDecodeBounds = false;
                    if (bs > ts) {
                        opts.inSampleSize = (int) Math.floor((double) (((float) bs) / ((float) ts)));
                    }
                    Bitmap _bmp = BitmapFactory.decodeFileDescriptor(fd.getFileDescriptor(), null, opts);
                    if (rotation != 0) {
                        Matrix matrix = new Matrix();
                        matrix.preRotate((float) rotation);
                        _bmp = Bitmap.createBitmap(_bmp, 0, 0, _bmp.getWidth(), _bmp.getHeight(), matrix, false);
                    }
                    bmp = _bmp;
                    fd.close();
                } else {
                    String fname = "thumbs/t" + APIRequest.md5(uri.toString()) + ".jpg";
                    Log.m528i("vk", "Before get styled " + uri + ", fname=" + fname);
                    bmp = GalleryPickerProvider.instance().getThumbnail(uri.toString(), new File(VKApplication.context.getCacheDir(), fname).getAbsolutePath());
                    Log.m528i("vk", "After get, result = " + bmp);
                }
                SignupProfileFragment.this.getActivity().runOnUiThread(new C08171(bmp));
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    public SignupProfileFragment() {
        this.gender = 0;
    }

    public void setOnNextClickListener(OnClickListener l) {
        this.btnClickListener = l;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(C0436R.layout.signup_profile, null);
        ((TextView) this.view.findViewById(C0436R.id.signup_profile_text1)).setTypeface(Fonts.getRobotoLight());
        ((TextView) this.view.findViewById(C0436R.id.signup_profile_text2)).setTypeface(Fonts.getRobotoLight());
        this.view.findViewById(C0436R.id.signup_gender_male).setOnClickListener(new C08111());
        this.view.findViewById(C0436R.id.signup_gender_female).setOnClickListener(new C08122());
        this.view.findViewById(C0436R.id.signup_photo_wrap).setOnClickListener(new C08133());
        ((EditText) this.view.findViewById(C0436R.id.signup_last_name)).setOnEditorActionListener(new C08144());
        if (this.gender > 0) {
            this.view.findViewById(this.gender == 1 ? C0436R.id.signup_gender_female : C0436R.id.signup_gender_male).setSelected(true);
        }
        this.view.findViewById(C0436R.id.signup_btn_next).setOnClickListener(new C08155());
        this.view.post(new C08166());
        if (this.photo != null) {
            this.view.findViewById(C0436R.id.signup_photo_holder).setVisibility(8);
            updatePhoto();
        }
        return this.view;
    }

    private void onClick(View v) {
        if (this.btnClickListener != null) {
            this.btnClickListener.onClick(v);
        }
    }

    public void onDestroyView() {
        if (getActivity().getCurrentFocus() != null) {
            Log.m528i("vk", "Clear focus");
            getActivity().getCurrentFocus().clearFocus();
        }
        super.onDestroyView();
        this.view = null;
    }

    private String getRealPathFromURI(Uri contentUri) {
        if (contentUri.getScheme().equals("file")) {
            return contentUri.getPath();
        }
        Cursor cursor = getActivity().getContentResolver().query(contentUri, new String[]{"_data"}, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode == AVA_RESULT && resCode == -1) {
            this.view.findViewById(C0436R.id.signup_photo_holder).setVisibility(8);
            this.photo = data.getStringExtra("file");
            updatePhoto();
        }
        if (reqCode == AVA_RESULT && resCode == 1) {
            this.photo = null;
            if (this.view != null) {
                this.view.findViewById(C0436R.id.signup_photo_holder).setVisibility(0);
                ((ImageView) this.view.findViewById(C0436R.id.signup_photo)).setImageBitmap(null);
            }
        }
    }

    private void updatePhoto() {
        new Thread(new C08187()).start();
    }

    public String getFirstName() {
        return ((TextView) this.view.findViewById(C0436R.id.signup_first_name)).getText().toString();
    }

    public String getLastName() {
        return ((TextView) this.view.findViewById(C0436R.id.signup_last_name)).getText().toString();
    }

    public int getGender() {
        return this.gender;
    }

    public String getPhoto() {
        return this.photo;
    }

    public int isFilled() {
        int res = (((TextView) this.view.findViewById(C0436R.id.signup_first_name)).getText().length() <= 1 || ((TextView) this.view.findViewById(C0436R.id.signup_last_name)).getText().length() <= 1) ? 0 : 1;
        if (res != 1 || getGender() != 0) {
            return res;
        }
        new Builder(getActivity()).setTitle(C0436R.string.error).setMessage(C0436R.string.signup_gender_not_selected).setPositiveButton(C0436R.string.ok, null).show();
        return -1;
    }
}
