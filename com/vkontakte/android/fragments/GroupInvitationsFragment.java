package com.vkontakte.android.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.GroupInvitation;
import com.vkontakte.android.api.GroupsGetInvites;
import com.vkontakte.android.api.GroupsGetInvites.Callback;
import com.vkontakte.android.api.GroupsJoin;
import com.vkontakte.android.api.GroupsLeave;
import com.vkontakte.android.cache.Cache;
import com.vkontakte.android.cache.GroupsCache;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.MultiSectionAdapter;
import com.vkontakte.android.ui.PhotoView;
import com.vkontakte.android.ui.RefreshableListView;
import com.vkontakte.android.ui.RefreshableListView.OnRefreshListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class GroupInvitationsFragment extends SherlockFragment implements OnClickListener, OnRefreshListener {
    private ProgressBar bigProgress;
    private FrameLayout contentView;
    private APIRequest currentReq;
    private boolean dataLoading;
    private FrameLayout footerView;
    private ListImageLoaderWrapper imgLoader;
    private int lastUpdate;
    private RefreshableListView list;
    private boolean loaded;
    private boolean moreAvailable;
    private boolean refreshing;
    private ArrayList<GroupInvitation> reqs;

    /* renamed from: com.vkontakte.android.fragments.GroupInvitationsFragment.1 */
    class C07131 implements OnItemClickListener {
        C07131() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
            Bundle args = new Bundle();
            args.putInt("id", (int) (-id));
            Navigate.to("ProfileFragment", args, GroupInvitationsFragment.this.getActivity());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupInvitationsFragment.4 */
    class C07144 implements Runnable {
        C07144() {
        }

        public void run() {
            ((BaseAdapter) ((HeaderViewListAdapter) GroupInvitationsFragment.this.list.getAdapter()).getWrappedAdapter()).notifyDataSetChanged();
            GroupInvitationsFragment.this.imgLoader.updateImages();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupInvitationsFragment.2 */
    class C14802 implements Listener {
        C14802() {
        }

        public void onScrolledToLastItem() {
            if (GroupInvitationsFragment.this.moreAvailable) {
                GroupInvitationsFragment.this.loadData();
            }
        }

        public void onScrollStopped() {
        }

        public void onScrollStarted() {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupInvitationsFragment.3 */
    class C14813 implements Callback {
        C14813() {
        }

        public void success(ArrayList<GroupInvitation> invs) {
            GroupInvitationsFragment.this.currentReq = null;
            GroupInvitationsFragment.this.reqs.clear();
            GroupInvitationsFragment.this.reqs.addAll(invs);
            GroupInvitationsFragment.this.updateList();
            GroupInvitationsFragment.this.dataLoading = false;
            Global.showViewAnimated(GroupInvitationsFragment.this.bigProgress, false, PhotoView.THUMB_ANIM_DURATION);
            GroupInvitationsFragment.this.loaded = true;
            if (GroupInvitationsFragment.this.list != null) {
                if (GroupInvitationsFragment.this.refreshing) {
                    GroupInvitationsFragment.this.refreshing = false;
                    GroupInvitationsFragment.this.list.refreshDone();
                }
                VKApplication.context.sendBroadcast(new Intent(Groups.ACTION_GROUP_INVITES_CHANGED), permission.ACCESS_DATA);
            }
        }

        public void fail(int ecode, String emsg) {
            GroupInvitationsFragment.this.currentReq = null;
            GroupInvitationsFragment.this.dataLoading = false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupInvitationsFragment.5 */
    class C14825 implements GroupsJoin.Callback {
        private final /* synthetic */ int val$gid;
        private final /* synthetic */ GroupInvitation val$req;
        private final /* synthetic */ boolean val$unsure;

        C14825(GroupInvitation groupInvitation, int i, boolean z) {
            this.val$req = groupInvitation;
            this.val$gid = i;
            this.val$unsure = z;
        }

        public void success() {
            GroupsCache.add(this.val$req.group, GroupInvitationsFragment.this.getActivity());
            VKApplication.context.sendBroadcast(new Intent(Groups.ACTION_GROUP_INVITES_CHANGED));
            VKApplication.context.sendBroadcast(new Intent(Groups.ACTION_GROUP_LIST_CHANGED));
            LongPollService.numGroupInvitations--;
            VKApplication.context.sendBroadcast(new Intent(LongPollService.ACTION_COUNTERS_UPDATED));
        }

        public void fail(int ecode, String emsg) {
            HashMap<String, String> args = new HashMap();
            args.put("gid", new StringBuilder(String.valueOf(this.val$gid)).toString());
            if (this.val$unsure) {
                args.put("not_sure", "1");
            }
            Cache.putApiRequest("groups.join", args);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.GroupInvitationsFragment.6 */
    class C14836 implements GroupsLeave.Callback {
        private final /* synthetic */ int val$gid;

        C14836(int i) {
            this.val$gid = i;
        }

        public void success() {
            LongPollService.numGroupInvitations--;
            VKApplication.context.sendBroadcast(new Intent(LongPollService.ACTION_COUNTERS_UPDATED));
        }

        public void fail(int ecode, String emsg) {
            HashMap<String, String> args = new HashMap();
            args.put("gid", new StringBuilder(String.valueOf(this.val$gid)).toString());
            Cache.putApiRequest("groups.leave", args);
        }
    }

    private class FriendReqPhotosAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.GroupInvitationsFragment.FriendReqPhotosAdapter.1 */
        class C07151 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$image;
            private final /* synthetic */ int val$item;

            C07151(int i, int i2, Bitmap bitmap) {
                this.val$item = i;
                this.val$image = i2;
                this.val$bitmap = bitmap;
            }

            public void run() {
                View view = GroupInvitationsFragment.this.list.getChildAt((this.val$item - GroupInvitationsFragment.this.list.getFirstVisiblePosition()) + 1);
                if (view != null && view.findViewById(C0436R.id.friend_req_photo) != null) {
                    if (this.val$image == 0) {
                        ((ImageView) view.findViewById(C0436R.id.friend_req_photo)).setImageBitmap(this.val$bitmap);
                    } else {
                        GroupInvitationsFragment.this.getMImageView(this.val$image - 1, view).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private FriendReqPhotosAdapter() {
        }

        public int getItemCount() {
            return GroupInvitationsFragment.this.reqs.size();
        }

        public int getImageCountForItem(int item) {
            return 1;
        }

        public String getImageURL(int item, int image) {
            if (image == 0) {
                return ((GroupInvitation) GroupInvitationsFragment.this.reqs.get(item)).group.photo;
            }
            return null;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            if (GroupInvitationsFragment.this.getActivity() != null && GroupInvitationsFragment.this.list != null) {
                int vend = GroupInvitationsFragment.this.list.getLastVisiblePosition() - GroupInvitationsFragment.this.list.getHeaderViewsCount();
                if (item >= GroupInvitationsFragment.this.list.getFirstVisiblePosition() - GroupInvitationsFragment.this.list.getHeaderViewsCount() && item <= vend) {
                    GroupInvitationsFragment.this.getActivity().runOnUiThread(new C07151(item, image, bitmap));
                }
            }
        }
    }

    private class FriendRequestsAdapter extends MultiSectionAdapter {
        private FriendRequestsAdapter() {
        }

        public View getView(int section, int pos, View view) {
            if (view == null) {
                view = View.inflate(GroupInvitationsFragment.this.getActivity(), C0436R.layout.groups_invite_item, null);
                view.findViewById(C0436R.id.friend_req_btn_add).setTag("add");
                view.findViewById(C0436R.id.friend_req_btn_add).setOnClickListener(GroupInvitationsFragment.this);
                view.findViewById(C0436R.id.friend_req_btn_add2).setTag("unsure");
                view.findViewById(C0436R.id.friend_req_btn_add2).setOnClickListener(GroupInvitationsFragment.this);
                view.findViewById(C0436R.id.friend_req_btn_decline).setTag("decline");
                view.findViewById(C0436R.id.friend_req_btn_decline).setOnClickListener(GroupInvitationsFragment.this);
                view.findViewById(C0436R.id.friend_req_btn_add).setTag(C0436R.id.freqs_tag_parent, view);
                view.findViewById(C0436R.id.friend_req_btn_add2).setTag(C0436R.id.freqs_tag_parent, view);
                view.findViewById(C0436R.id.friend_req_btn_decline).setTag(C0436R.id.freqs_tag_parent, view);
            }
            GroupInvitation req = (GroupInvitation) GroupInvitationsFragment.this.reqs.get(pos);
            ((TextView) view.findViewById(C0436R.id.friend_req_name)).setText(req.group.name);
            if (req.group.type == 1) {
                view.findViewById(C0436R.id.friend_req_btn_add2).setVisibility(0);
                ((TextView) view.findViewById(C0436R.id.req_add_btn_text)).setText(C0436R.string.group_inv_event_accept);
            } else {
                view.findViewById(C0436R.id.friend_req_btn_add2).setVisibility(8);
                ((TextView) view.findViewById(C0436R.id.req_add_btn_text)).setText(C0436R.string.group_inv_accept);
            }
            ((TextView) view.findViewById(C0436R.id.friend_req_info)).setText(GroupInvitationsFragment.this.getResources().getString(C0436R.string.group_invites_you, new Object[]{req.inviter.fullName}));
            ((TextView) view.findViewById(C0436R.id.friend_req_nmutual)).setText(Global.langPlural(C0436R.array.group_members, req.size, GroupInvitationsFragment.this.getResources()));
            ViewFlipper flipper = (ViewFlipper) view.findViewById(C0436R.id.friend_req_flipper);
            flipper.setInAnimation(null);
            flipper.setOutAnimation(null);
            switch (req.state) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    flipper.setDisplayedChild(0);
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    flipper.setDisplayedChild(1);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                case UserListView.TYPE_FAVE /*4*/:
                    flipper.setDisplayedChild(2);
                    break;
            }
            flipper.setInAnimation(GroupInvitationsFragment.this.getActivity(), C0436R.anim.push_up_in);
            flipper.setOutAnimation(GroupInvitationsFragment.this.getActivity(), C0436R.anim.push_up_out);
            if (req.state == 2 || req.state == 4) {
                ((TextView) flipper.findViewById(C0436R.id.friend_req_result_text)).setText(C0436R.string.friend_req_accepted);
            }
            if (req.state == 3) {
                ((TextView) flipper.findViewById(C0436R.id.friend_req_result_text)).setText(C0436R.string.friend_req_declined);
            }
            if (GroupInvitationsFragment.this.imgLoader.isAlreadyLoaded(req.group.photo)) {
                ((ImageView) view.findViewById(C0436R.id.friend_req_photo)).setImageBitmap(GroupInvitationsFragment.this.imgLoader.get(req.group.photo));
            } else {
                ((ImageView) view.findViewById(C0436R.id.friend_req_photo)).setImageResource(C0436R.drawable.group_placeholder);
            }
            view.setTag(Integer.valueOf(req.group.id));
            return view;
        }

        public String getSectionTitle(int section) {
            if (section == 1) {
                return GroupInvitationsFragment.this.getResources().getString(C0436R.string.suggest_friends);
            }
            return null;
        }

        public int getSectionCount() {
            return 1;
        }

        public int getItemCount(int section) {
            return GroupInvitationsFragment.this.reqs.size();
        }

        public long getItemId(int section, int item) {
            try {
                return (long) ((GroupInvitation) GroupInvitationsFragment.this.reqs.get(item)).group.id;
            } catch (Exception e) {
                return 0;
            }
        }

        public boolean isSectionHeaderVisible(int section) {
            return false;
        }
    }

    public GroupInvitationsFragment() {
        this.reqs = new ArrayList();
        this.dataLoading = false;
        this.moreAvailable = false;
        this.refreshing = false;
        this.lastUpdate = (int) (System.currentTimeMillis() / 1000);
        this.loaded = false;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int i = 0;
        this.contentView = new FrameLayout(getActivity());
        this.contentView.setBackgroundColor(-1710619);
        this.footerView = new FrameLayout(getActivity());
        ProgressBar pb = new ProgressBar(getActivity());
        LayoutParams lp = new LayoutParams(Global.scale(BitmapDescriptorFactory.HUE_ORANGE), Global.scale(BitmapDescriptorFactory.HUE_ORANGE));
        lp.gravity = 17;
        pb.setLayoutParams(lp);
        this.footerView.setBackgroundResource(C0436R.drawable.bottom_shadow);
        this.footerView.setPadding(Global.scale(7.0f), Global.scale(10.0f), Global.scale(7.0f), Global.scale(7.0f));
        this.footerView.addView(pb);
        this.footerView.setVisibility(8);
        this.list = new RefreshableListView(getActivity());
        this.list.addFooterView(this.footerView);
        this.list.setAdapter(new FriendRequestsAdapter());
        this.list.setDivider(null);
        if (VERSION.SDK_INT < 11) {
            this.list.setBackgroundColor(-1710619);
        }
        this.list.setCacheColorHint(-1710619);
        this.list.setVerticalFadingEdgeEnabled(false);
        this.list.setOnRefreshListener(this);
        this.list.setSelector(C0436R.drawable.highlight_post);
        this.list.setDrawSelectorOnTop(true);
        this.contentView.addView(this.list);
        this.list.setOnItemClickListener(new C07131());
        this.moreAvailable = false;
        this.imgLoader = new ListImageLoaderWrapper(new FriendReqPhotosAdapter(), this.list, new C14802());
        this.bigProgress = new ProgressBar(getActivity());
        LayoutParams lp2 = new LayoutParams(-2, -2);
        lp2.gravity = 17;
        this.bigProgress.setLayoutParams(lp2);
        ProgressBar progressBar = this.bigProgress;
        if (this.loaded) {
            i = 8;
        }
        progressBar.setVisibility(i);
        this.contentView.addView(this.bigProgress);
        return this.contentView;
    }

    public void loadData() {
        this.dataLoading = true;
        this.currentReq = new GroupsGetInvites().setCallback(new C14813()).exec(getActivity());
    }

    public void updateList() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new C07144());
        }
    }

    public void onClick(View v) {
        GroupInvitation req;
        int gid = ((Integer) ((View) v.getTag(C0436R.id.freqs_tag_parent)).getTag()).intValue();
        GroupInvitation _req = null;
        Iterator it = this.reqs.iterator();
        while (it.hasNext()) {
            req = (GroupInvitation) it.next();
            if (req.group.id == gid) {
                _req = req;
                break;
            }
        }
        req = _req;
        boolean isSuggestion = false;
        req.state = 1;
        ViewFlipper flipper = (ViewFlipper) ((View) v.getTag(C0436R.id.freqs_tag_parent)).findViewById(C0436R.id.friend_req_flipper);
        if ("add".equals(v.getTag().toString()) || "unsure".equals(v.getTag().toString())) {
            animateStateTransition((View) v.getTag(C0436R.id.freqs_tag_parent), true, isSuggestion);
            boolean unsure = "unsure".equals(v.getTag().toString());
            req.state = unsure ? 4 : 2;
            new GroupsJoin(req.group.id, unsure).setCallback(new C14825(req, gid, unsure)).exec();
        }
        if ("decline".equals(v.getTag().toString())) {
            animateStateTransition((View) v.getTag(C0436R.id.freqs_tag_parent), false, isSuggestion);
            req.state = 3;
            new GroupsLeave(gid).setCallback(new C14836(gid)).exec();
        }
    }

    private void animateStateTransition(View view, boolean accepted, boolean suggestion) {
        ViewFlipper flipper = (ViewFlipper) view.findViewById(C0436R.id.friend_req_flipper);
        TextView textView = (TextView) flipper.findViewById(C0436R.id.friend_req_result_text);
        int i = suggestion ? C0436R.string.friend_req_sent : accepted ? C0436R.string.friend_req_accepted : C0436R.string.friend_req_declined;
        textView.setText(i);
        ((TextView) flipper.findViewById(C0436R.id.friend_req_result_text)).setTextColor(-6710887);
        flipper.setDisplayedChild(2);
    }

    private ImageView getMImageView(int n, View view) {
        switch (n) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return (ImageView) view.findViewById(C0436R.id.friend_req_mf1);
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                return (ImageView) view.findViewById(C0436R.id.friend_req_mf2);
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return (ImageView) view.findViewById(C0436R.id.friend_req_mf3);
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                return (ImageView) view.findViewById(C0436R.id.friend_req_mf4);
            case UserListView.TYPE_FAVE /*4*/:
                return (ImageView) view.findViewById(C0436R.id.friend_req_mf5);
            default:
                return null;
        }
    }

    public void onRefresh() {
        this.refreshing = true;
        loadData();
    }

    public String getLastUpdatedTime() {
        if (this.lastUpdate > 0) {
            return new StringBuilder(String.valueOf(getResources().getString(C0436R.string.updated))).append(" ").append(Global.langDateRelativeNoDiff(this.lastUpdate, getResources())).toString();
        }
        return getResources().getString(C0436R.string.not_updated);
    }

    public void onPause() {
        super.onPause();
        this.imgLoader.deactivate();
    }

    public void onResume() {
        super.onResume();
        this.imgLoader.activate();
        ((BaseAdapter) ((HeaderViewListAdapter) this.list.getAdapter()).getWrappedAdapter()).notifyDataSetInvalidated();
    }

    public void onScrolled(float offset) {
    }
}
