package com.vkontakte.android.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.MultiSectionImageLoaderAdapter;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.FriendRequest;
import com.vkontakte.android.api.FriendsAdd;
import com.vkontakte.android.api.FriendsDelete;
import com.vkontakte.android.api.FriendsDeleteAllRequests;
import com.vkontakte.android.api.FriendsGetRequests;
import com.vkontakte.android.api.FriendsGetRequests.Callback;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.cache.Cache;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.MultiSectionAdapter;
import com.vkontakte.android.ui.RefreshableListView;
import com.vkontakte.android.ui.RefreshableListView.OnRefreshListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

public class FriendRequestsFragment extends SherlockFragment implements OnClickListener, OnRefreshListener {
    private FriendRequestsAdapter adapter;
    private ProgressBar bigProgress;
    private FrameLayout contentView;
    private APIRequest currentReq;
    private boolean dataLoading;
    private FrameLayout footerView;
    private ListImageLoaderWrapper imgLoader;
    private int lastUpdate;
    private RefreshableListView list;
    private boolean loaded;
    private View markAllViewedBtn;
    private boolean moreAvailable;
    private EmptyView noReqsView;
    private boolean onlyRecommends;
    ArrayList<FriendRequest> recommends;
    private boolean refreshing;
    private ArrayList<FriendRequest> reqs;
    private boolean showMoreRecommends;
    ArrayList<FriendRequest> suggestions;
    private Vector<Integer> visibleIDs;
    private Vector<View> visibleViews;
    private FrameLayout wrap;

    /* renamed from: com.vkontakte.android.fragments.FriendRequestsFragment.1 */
    class C06901 implements OnClickListener {
        C06901() {
        }

        public void onClick(View v) {
            FriendRequestsFragment.this.markAllAsViewed();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendRequestsFragment.2 */
    class C06912 implements OnItemClickListener {
        C06912() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
            if (id == -1) {
                Bundle args = new Bundle();
                args.putBoolean("recommends", true);
                Navigate.to("FriendRequestsFragment", args, FriendRequestsFragment.this.getActivity());
                return;
            }
            args = new Bundle();
            args.putInt("id", (int) id);
            Navigate.to("ProfileFragment", args, FriendRequestsFragment.this.getActivity());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendRequestsFragment.5 */
    class C06945 implements Runnable {
        C06945() {
        }

        public void run() {
            FriendRequestsFragment.this.adapter.notifyDataSetChanged();
            FriendRequestsFragment.this.imgLoader.updateImages();
            FriendRequestsFragment.this.noReqsView.clearAnimation();
            EmptyView access$20 = FriendRequestsFragment.this.noReqsView;
            int i = (FriendRequestsFragment.this.adapter.getCount() <= 0 || FriendRequestsFragment.this.bigProgress.getVisibility() == 0) ? 0 : 8;
            access$20.setVisibility(i);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendRequestsFragment.3 */
    class C14693 implements Listener {
        C14693() {
        }

        public void onScrolledToLastItem() {
            if (FriendRequestsFragment.this.moreAvailable) {
                FriendRequestsFragment.this.loadData();
            }
        }

        public void onScrollStopped() {
        }

        public void onScrollStarted() {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendRequestsFragment.4 */
    class C14704 implements Callback {

        /* renamed from: com.vkontakte.android.fragments.FriendRequestsFragment.4.1 */
        class C06921 implements Runnable {
            C06921() {
            }

            public void run() {
                int i = 8;
                if (FriendRequestsFragment.this.list != null) {
                    FriendRequestsFragment.this.bigProgress.setVisibility(8);
                    FriendRequestsFragment.this.wrap.setVisibility(0);
                    FriendRequestsFragment.this.updateList();
                    Log.m528i("vk", "more=" + FriendRequestsFragment.this.moreAvailable + " [" + LongPollService.numFriendRequests + " | " + FriendRequestsFragment.this.reqs.size() + "]");
                    View access$16 = FriendRequestsFragment.this.markAllViewedBtn;
                    int i2 = (FriendRequestsFragment.this.moreAvailable || FriendRequestsFragment.this.reqs.size() <= 10) ? 8 : 0;
                    access$16.setVisibility(i2);
                    View childAt = FriendRequestsFragment.this.footerView.getChildAt(0);
                    if (FriendRequestsFragment.this.moreAvailable) {
                        i = 0;
                    }
                    childAt.setVisibility(i);
                    FriendRequestsFragment.this.footerView.setVisibility(FriendRequestsFragment.this.reqs.size() > 0 ? 0 : 4);
                    if (FriendRequestsFragment.this.refreshing) {
                        FriendRequestsFragment.this.list.refreshDone();
                        FriendRequestsFragment.this.refreshing = false;
                    }
                }
            }
        }

        /* renamed from: com.vkontakte.android.fragments.FriendRequestsFragment.4.2 */
        class C06932 implements Runnable {
            C06932() {
            }

            public void run() {
                FriendRequestsFragment.this.bigProgress.setVisibility(8);
                if (FriendRequestsFragment.this.refreshing) {
                    FriendRequestsFragment.this.list.refreshDone();
                    FriendRequestsFragment.this.refreshing = false;
                }
                Toast.makeText(FriendRequestsFragment.this.getActivity(), C0436R.string.err_text, 0).show();
            }
        }

        C14704() {
        }

        public void success(ArrayList<FriendRequest> reqs, ArrayList<FriendRequest> recoms, int numRecoms) {
            boolean z;
            boolean z2 = false;
            if (FriendRequestsFragment.this.reqs.size() == 0 || FriendRequestsFragment.this.refreshing) {
                FriendRequestsFragment.this.reqs.clear();
                FriendRequestsFragment.this.suggestions.clear();
            }
            if (FriendRequestsFragment.this.refreshing) {
                FriendRequestsFragment.this.recommends.clear();
            }
            FriendRequestsFragment.this.recommends.addAll(recoms);
            FriendRequestsFragment friendRequestsFragment = FriendRequestsFragment.this;
            if (numRecoms <= 3 || FriendRequestsFragment.this.onlyRecommends) {
                z = false;
            } else {
                z = true;
            }
            friendRequestsFragment.showMoreRecommends = z;
            ArrayList<FriendRequest> toDisplay = new ArrayList();
            ArrayList<Integer> ids = new ArrayList();
            Iterator it = FriendRequestsFragment.this.reqs.iterator();
            while (it.hasNext()) {
                ids.add(Integer.valueOf(((FriendRequest) it.next()).profile.uid));
            }
            it = reqs.iterator();
            while (it.hasNext()) {
                FriendRequest req = (FriendRequest) it.next();
                if (!ids.contains(Integer.valueOf(req.profile.uid))) {
                    ids.add(Integer.valueOf(req.profile.uid));
                    toDisplay.add(req);
                }
            }
            FriendRequestsFragment.this.reqs.addAll(toDisplay);
            FriendRequestsFragment.this.dataLoading = false;
            FriendRequestsFragment friendRequestsFragment2 = FriendRequestsFragment.this;
            if (reqs.size() > 0 && (LongPollService.numFriendRequests <= 100 || LongPollService.numFriendRequests > FriendRequestsFragment.this.reqs.size())) {
                z2 = true;
            }
            friendRequestsFragment2.moreAvailable = z2;
            FriendRequestsFragment.this.loaded = true;
            if (FriendRequestsFragment.this.getActivity() == null) {
                Log.m530w("vk", "getActivity==null");
                return;
            }
            FriendRequestsFragment.this.getActivity().runOnUiThread(new C06921());
            FriendRequestsFragment.this.currentReq = null;
        }

        public void fail(int ecode, String emsg) {
            FriendRequestsFragment.this.dataLoading = false;
            if (FriendRequestsFragment.this.getActivity() != null) {
                FriendRequestsFragment.this.getActivity().runOnUiThread(new C06932());
                FriendRequestsFragment.this.currentReq = null;
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendRequestsFragment.6 */
    class C14716 implements FriendsDeleteAllRequests.Callback {
        C14716() {
        }

        public void success() {
            Iterator it = FriendRequestsFragment.this.reqs.iterator();
            while (it.hasNext()) {
                FriendRequest r = (FriendRequest) it.next();
                if (r.state == 0) {
                    r.state = 3;
                }
            }
            FriendRequestsFragment.this.updateList();
            FriendRequestsFragment.this.footerView.setVisibility(8);
            VKApplication.context.sendBroadcast(new Intent(Friends.ACTION_FRIEND_REQUESTS_CHANGED));
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(FriendRequestsFragment.this.getActivity(), C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendRequestsFragment.7 */
    class C14727 implements FriendsAdd.Callback {
        private final /* synthetic */ FriendRequest val$req;
        private final /* synthetic */ int val$uid;
        private final /* synthetic */ View val$v;

        C14727(FriendRequest friendRequest, View view, int i) {
            this.val$req = friendRequest;
            this.val$v = view;
            this.val$uid = i;
        }

        public void success(int uid, int result) {
            if (result == 2) {
                Friends.add(this.val$req.profile);
            }
            if (FriendRequestsFragment.this.visibleIDs.contains(Integer.valueOf(uid))) {
                ((Integer) ((View) this.val$v.getParent()).getTag()).intValue();
            }
            if (((Boolean) ((View) this.val$v.getTag(C0436R.id.freqs_tag_parent)).getTag(C0436R.id.freqs_tag_is_req)).booleanValue()) {
                LongPollService.numFriendRequests--;
                VKApplication.context.sendBroadcast(new Intent(Friends.ACTION_FRIEND_REQUESTS_CHANGED));
                VKApplication.context.sendBroadcast(new Intent(LongPollService.ACTION_COUNTERS_UPDATED));
            }
        }

        public void fail(int ecode, String emsg) {
            HashMap<String, String> args = new HashMap();
            args.put("user_id", new StringBuilder(String.valueOf(this.val$uid)).toString());
            Cache.putApiRequest("friends.add", args);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendRequestsFragment.8 */
    class C14738 implements FriendsDelete.Callback {
        private final /* synthetic */ int val$uid;
        private final /* synthetic */ View val$v;

        C14738(View view, int i) {
            this.val$v = view;
            this.val$uid = i;
        }

        public void success(int uid, int result) {
            if (((Boolean) ((View) this.val$v.getTag(C0436R.id.freqs_tag_parent)).getTag(C0436R.id.freqs_tag_is_req)).booleanValue()) {
                LongPollService.numFriendRequests--;
                FriendRequestsFragment.this.getActivity().sendBroadcast(new Intent(Friends.ACTION_FRIEND_REQUESTS_CHANGED));
                FriendRequestsFragment.this.getActivity().sendBroadcast(new Intent(LongPollService.ACTION_COUNTERS_UPDATED));
            }
        }

        public void fail(int ecode, String emsg) {
            HashMap<String, String> args = new HashMap();
            args.put("user_id", new StringBuilder(String.valueOf(this.val$uid)).toString());
            Cache.putApiRequest("friends.delete", args);
        }
    }

    private class FriendReqPhotosAdapter extends MultiSectionImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.FriendRequestsFragment.FriendReqPhotosAdapter.1 */
        class C06951 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$image;
            private final /* synthetic */ View val$view;

            C06951(View view, int i, Bitmap bitmap) {
                this.val$view = view;
                this.val$image = i;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$view != null && this.val$view.findViewById(C0436R.id.friend_req_photo) != null) {
                    if (this.val$image == 0) {
                        ((ImageView) this.val$view.findViewById(C0436R.id.friend_req_photo)).setImageBitmap(this.val$bitmap);
                    } else {
                        FriendRequestsFragment.this.getMImageView(this.val$image - 1, this.val$view).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private FriendReqPhotosAdapter() {
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            if (FriendRequestsFragment.this.getActivity() != null) {
                item += FriendRequestsFragment.this.list.getHeaderViewsCount();
                if (item >= FriendRequestsFragment.this.list.getFirstVisiblePosition() && item <= FriendRequestsFragment.this.list.getLastVisiblePosition()) {
                    FriendRequestsFragment.this.getActivity().runOnUiThread(new C06951(FriendRequestsFragment.this.list.getChildAt(item - FriendRequestsFragment.this.list.getFirstVisiblePosition()), image, bitmap));
                }
            }
        }

        public int getSectionCount() {
            return 3;
        }

        public int getItemCount(int section) {
            return FriendRequestsFragment.this.adapter.getItemCount(section);
        }

        public boolean isSectionHeaderVisible(int section) {
            if (FriendRequestsFragment.this.adapter == null) {
                return false;
            }
            return FriendRequestsFragment.this.adapter.isSectionHeaderVisible(section);
        }

        public int getImageCountForItem(int section, int item) {
            int i = 0;
            if (section == 0 && item == FriendRequestsFragment.this.recommends.size()) {
                return 0;
            }
            FriendRequest req = null;
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    req = (FriendRequest) FriendRequestsFragment.this.recommends.get(item);
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    req = (FriendRequest) FriendRequestsFragment.this.reqs.get(item);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    req = (FriendRequest) FriendRequestsFragment.this.suggestions.get(item);
                    break;
            }
            if (req == null) {
                return 0;
            }
            if (req.mutualFriends != null) {
                i = req.mutualFriends.length;
            }
            return Math.min(i, 5) + 1;
        }

        public String getImageURL(int section, int item, int image) {
            FriendRequest req = null;
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    req = (FriendRequest) FriendRequestsFragment.this.recommends.get(item);
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    req = (FriendRequest) FriendRequestsFragment.this.reqs.get(item);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    req = (FriendRequest) FriendRequestsFragment.this.suggestions.get(item);
                    break;
            }
            if (image == 0) {
                return req.profile.photo;
            }
            return req.mutualFriends[image - 1].photo;
        }
    }

    private class FriendRequestsAdapter extends MultiSectionAdapter {
        private FriendRequestsAdapter() {
        }

        public View getView(int section, int pos, View view) {
            if (section == 0 && pos == FriendRequestsFragment.this.recommends.size()) {
                if (view == null) {
                    view = View.inflate(FriendRequestsFragment.this.getActivity(), C0436R.layout.friend_req_show_all, null);
                }
                return view;
            }
            if (view == null) {
                view = View.inflate(FriendRequestsFragment.this.getActivity(), C0436R.layout.friends_req_item, null);
                view.findViewById(C0436R.id.friend_req_btn_add).setTag("add");
                view.findViewById(C0436R.id.friend_req_btn_add).setOnClickListener(FriendRequestsFragment.this);
                view.findViewById(C0436R.id.friend_req_btn_decline).setTag("decline");
                view.findViewById(C0436R.id.friend_req_btn_decline).setOnClickListener(FriendRequestsFragment.this);
                view.findViewById(C0436R.id.friend_req_btn_add).setTag(C0436R.id.freqs_tag_parent, view);
                view.findViewById(C0436R.id.friend_req_btn_decline).setTag(C0436R.id.freqs_tag_parent, view);
            }
            FriendRequest req = null;
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    req = (FriendRequest) FriendRequestsFragment.this.recommends.get(pos);
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    req = (FriendRequest) FriendRequestsFragment.this.reqs.get(pos);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    req = (FriendRequest) FriendRequestsFragment.this.suggestions.get(pos);
                    break;
            }
            ((TextView) view.findViewById(C0436R.id.friend_req_name)).setText(req.profile.fullName);
            if (req.info != null) {
                ((TextView) view.findViewById(C0436R.id.friend_req_info)).setText(req.info);
                ((TextView) view.findViewById(C0436R.id.friend_req_info)).setMaxHeight(9000);
            } else {
                ((TextView) view.findViewById(C0436R.id.friend_req_info)).setMaxHeight(0);
            }
            if (req.message == null || req.message.length() <= 0) {
                view.findViewById(C0436R.id.friend_req_message).setVisibility(8);
            } else {
                view.findViewById(C0436R.id.friend_req_message).setVisibility(0);
                ((TextView) view.findViewById(C0436R.id.friend_req_message)).setText(req.message);
            }
            int i;
            if (req.numMutualFriends > 0) {
                view.findViewById(C0436R.id.friend_req_nmutual).setVisibility(0);
                ((TextView) view.findViewById(C0436R.id.friend_req_nmutual)).setText(Global.langPlural(C0436R.array.num_mutual_friends_req, req.numMutualFriends, FriendRequestsFragment.this.getResources()));
                view.findViewById(C0436R.id.friend_req_mf1).setVisibility(0);
                view.findViewById(C0436R.id.friend_req_mf2).setVisibility(req.numMutualFriends > 1 ? 0 : 8);
                view.findViewById(C0436R.id.friend_req_mf3).setVisibility(req.numMutualFriends > 2 ? 0 : 8);
                view.findViewById(C0436R.id.friend_req_mf4).setVisibility(req.numMutualFriends > 3 ? 0 : 8);
                view.findViewById(C0436R.id.friend_req_mf5).setVisibility(req.numMutualFriends > 4 ? 0 : 8);
                for (i = 0; i < req.mutualFriends.length; i++) {
                    if (FriendRequestsFragment.this.imgLoader.isAlreadyLoaded(req.mutualFriends[i].photo)) {
                        FriendRequestsFragment.this.getMImageView(i, view).setImageBitmap(FriendRequestsFragment.this.imgLoader.get(req.mutualFriends[i].photo));
                    } else {
                        FriendRequestsFragment.this.getMImageView(i, view).setImageResource(C0436R.drawable.user_placeholder);
                    }
                }
            } else {
                view.findViewById(C0436R.id.friend_req_nmutual).setVisibility(8);
                for (i = 0; i < 5; i++) {
                    FriendRequestsFragment.this.getMImageView(i, view).setImageResource(C0436R.drawable.user_placeholder);
                    FriendRequestsFragment.this.getMImageView(i, view).setVisibility(8);
                }
            }
            ViewFlipper flipper = (ViewFlipper) view.findViewById(C0436R.id.friend_req_flipper);
            flipper.setInAnimation(null);
            flipper.setOutAnimation(null);
            switch (req.state) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    flipper.setDisplayedChild(0);
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    flipper.setDisplayedChild(1);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    flipper.setDisplayedChild(2);
                    break;
            }
            flipper.setInAnimation(FriendRequestsFragment.this.getActivity(), C0436R.anim.push_up_in);
            flipper.setOutAnimation(FriendRequestsFragment.this.getActivity(), C0436R.anim.push_up_out);
            if (req.state == 2) {
                ((TextView) flipper.findViewById(C0436R.id.friend_req_result_text)).setText(section == 1 ? C0436R.string.friend_req_accepted : C0436R.string.friend_req_sent);
            }
            if (req.state == 3) {
                ((TextView) flipper.findViewById(C0436R.id.friend_req_result_text)).setText(C0436R.string.friend_req_declined);
            }
            if (FriendRequestsFragment.this.imgLoader.isAlreadyLoaded(req.profile.photo)) {
                ((ImageView) view.findViewById(C0436R.id.friend_req_photo)).setImageBitmap(FriendRequestsFragment.this.imgLoader.get(req.profile.photo));
            } else {
                ((ImageView) view.findViewById(C0436R.id.friend_req_photo)).setImageResource(C0436R.drawable.user_placeholder);
            }
            view.setTag(Integer.valueOf(req.profile.uid));
            view.setTag(C0436R.id.freqs_tag_is_req, Boolean.valueOf(section == 1));
            return view;
        }

        public String getSectionTitle(int section) {
            if (section == 0) {
                return FriendRequestsFragment.this.getResources().getString(C0436R.string.friends_recommend);
            }
            if (section == 1) {
                return FriendRequestsFragment.this.getResources().getString(C0436R.string.friend_requests);
            }
            if (section == 2) {
                return FriendRequestsFragment.this.getResources().getString(C0436R.string.suggest_friends);
            }
            return null;
        }

        public int getItemViewType(int section, int item) {
            if (section == 0 && item == FriendRequestsFragment.this.recommends.size()) {
                return 2;
            }
            return 0;
        }

        public int getExtraViewTypeCount() {
            return 1;
        }

        public int getSectionCount() {
            return 3;
        }

        public int getItemCount(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return (FriendRequestsFragment.this.showMoreRecommends ? 1 : 0) + FriendRequestsFragment.this.recommends.size();
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return FriendRequestsFragment.this.reqs.size();
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return FriendRequestsFragment.this.suggestions.size();
                default:
                    return FriendRequestsFragment.this.suggestions.size();
            }
        }

        public long getItemId(int section, int item) {
            if (section == 0 && item == FriendRequestsFragment.this.recommends.size()) {
                return -1;
            }
            if (section == 0) {
                try {
                    if (FriendRequestsFragment.this.recommends.size() > 0) {
                        return (long) ((FriendRequest) FriendRequestsFragment.this.recommends.get(item)).profile.uid;
                    }
                } catch (Exception e) {
                    return 0;
                }
            }
            if (section != 1 || FriendRequestsFragment.this.reqs.size() <= 0) {
                return (long) ((FriendRequest) FriendRequestsFragment.this.suggestions.get(item)).profile.uid;
            }
            return (long) ((FriendRequest) FriendRequestsFragment.this.reqs.get(item)).profile.uid;
        }

        public boolean isSectionHeaderVisible(int section) {
            if (section != 0 || FriendRequestsFragment.this.onlyRecommends) {
                if (section == 1) {
                    if (FriendRequestsFragment.this.recommends.size() <= 0 || FriendRequestsFragment.this.reqs.size() <= 0) {
                        return false;
                    }
                    return true;
                } else if (section != 2) {
                    return false;
                } else {
                    if (FriendRequestsFragment.this.reqs.size() <= 0 || FriendRequestsFragment.this.suggestions.size() <= 0) {
                        return false;
                    }
                    return true;
                }
            } else if (FriendRequestsFragment.this.recommends.size() > 0) {
                return true;
            } else {
                return false;
            }
        }

        public int getHeaderLayoutResource() {
            return C0436R.layout.list_cards_section_header;
        }
    }

    public FriendRequestsFragment() {
        this.reqs = new ArrayList();
        this.visibleViews = new Vector();
        this.visibleIDs = new Vector();
        this.dataLoading = false;
        this.moreAvailable = false;
        this.refreshing = false;
        this.loaded = false;
        this.lastUpdate = (int) (System.currentTimeMillis() / 1000);
        this.recommends = new ArrayList();
        this.suggestions = new ArrayList();
        this.showMoreRecommends = false;
        this.onlyRecommends = false;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        Log.m528i("vk", this + " onAttach");
        if (!getArguments().containsKey("tab")) {
            if (getArguments().getBoolean("recommends")) {
                this.onlyRecommends = true;
                act.setTitle(C0436R.string.friends_recommend);
                loadData();
                return;
            }
            act.setTitle(C0436R.string.sett_friend_requests);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.currentReq != null) {
            this.currentReq.cancel();
            this.currentReq = null;
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int i = 8;
        this.contentView = new FrameLayout(getActivity());
        this.contentView.setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        this.footerView = new FrameLayout(getActivity());
        ProgressBar pb = new ProgressBar(getActivity());
        LayoutParams lp = new LayoutParams(Global.scale(BitmapDescriptorFactory.HUE_ORANGE), Global.scale(BitmapDescriptorFactory.HUE_ORANGE));
        lp.gravity = 17;
        int scale = Global.scale(10.0f);
        lp.bottomMargin = scale;
        lp.topMargin = scale;
        pb.setLayoutParams(lp);
        this.footerView.addView(pb);
        this.footerView.setVisibility(8);
        View btn = View.inflate(getActivity(), C0436R.layout.friend_req_show_all, null);
        ((TextView) btn.findViewById(C0436R.id.text)).setText(C0436R.string.friend_reqs_mark_viewed);
        btn.setLayoutParams(new LayoutParams(-1, -2));
        this.footerView.addView(btn);
        btn.setVisibility(4);
        this.markAllViewedBtn = btn;
        btn.setOnClickListener(new C06901());
        this.list = new RefreshableListView(getActivity());
        this.list.addFooterView(this.footerView);
        RefreshableListView refreshableListView = this.list;
        ListAdapter friendRequestsAdapter = new FriendRequestsAdapter();
        this.adapter = friendRequestsAdapter;
        refreshableListView.setAdapter(friendRequestsAdapter);
        this.list.setDivider(null);
        if (VERSION.SDK_INT < 11) {
            this.list.setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
            this.list.setCacheColorHint(getResources().getColor(C0436R.color.cards_bg));
        }
        this.list.setVerticalFadingEdgeEnabled(false);
        this.list.setOnRefreshListener(this);
        this.list.setSelector(C0436R.drawable.highlight_post);
        this.list.setDrawSelectorOnTop(true);
        this.wrap = new FrameLayout(getActivity());
        this.wrap.addView(this.list);
        this.list.setOnItemClickListener(new C06912());
        this.moreAvailable = false;
        this.imgLoader = new ListImageLoaderWrapper(new FriendReqPhotosAdapter(), this.list, new C14693());
        this.noReqsView = EmptyView.create(getActivity());
        this.noReqsView.setText((int) C0436R.string.empty_list);
        this.noReqsView.setVisibility(8);
        this.wrap.addView(this.noReqsView);
        this.contentView.addView(this.wrap);
        FrameLayout frameLayout = this.wrap;
        if (this.loaded) {
            scale = 0;
        } else {
            scale = 8;
        }
        frameLayout.setVisibility(scale);
        this.bigProgress = new ProgressBar(getActivity());
        LayoutParams lp2 = new LayoutParams(-2, -2);
        lp2.gravity = 17;
        this.bigProgress.setLayoutParams(lp2);
        ProgressBar progressBar = this.bigProgress;
        if (!this.loaded) {
            i = 0;
        }
        progressBar.setVisibility(i);
        this.contentView.addView(this.bigProgress);
        return this.contentView;
    }

    public void onDestroyView() {
        this.list = null;
        this.adapter = null;
        this.footerView = null;
        this.noReqsView = null;
        this.imgLoader = null;
        this.bigProgress = null;
        this.markAllViewedBtn = null;
        this.wrap = null;
        this.contentView = null;
        super.onDestroyView();
    }

    public void loadData() {
        this.dataLoading = true;
        this.currentReq = new FriendsGetRequests(this.refreshing ? 0 : this.reqs.size(), 20, this.onlyRecommends).setCallback(new C14704()).exec();
    }

    public void updateList() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new C06945());
        }
    }

    private void markAllAsViewed() {
        new FriendsDeleteAllRequests().setCallback(new C14716()).wrapProgress(getActivity()).exec(getActivity());
    }

    public void onClick(View v) {
        FriendRequest req;
        int uid = ((Integer) ((View) v.getTag(C0436R.id.freqs_tag_parent)).getTag()).intValue();
        boolean _isSuggestion = false;
        FriendRequest _req = null;
        Iterator it = this.reqs.iterator();
        while (it.hasNext()) {
            req = (FriendRequest) it.next();
            if (req.profile.uid == uid) {
                _req = req;
                break;
            }
        }
        if (_req == null) {
            it = this.suggestions.iterator();
            while (it.hasNext()) {
                req = (FriendRequest) it.next();
                if (req.profile.uid == uid) {
                    _req = req;
                    _isSuggestion = true;
                    break;
                }
            }
        }
        if (_req == null) {
            it = this.recommends.iterator();
            while (it.hasNext()) {
                req = (FriendRequest) it.next();
                if (req.profile.uid == uid) {
                    _req = req;
                    _isSuggestion = true;
                    break;
                }
            }
        }
        req = _req;
        boolean isSuggestion = _isSuggestion;
        if (req.state == 0) {
            req.state = 1;
            ViewFlipper flipper = (ViewFlipper) ((View) v.getTag(C0436R.id.freqs_tag_parent)).findViewById(C0436R.id.friend_req_flipper);
            if ("add".equals(v.getTag().toString())) {
                req.state = 2;
                animateStateTransition((View) v.getTag(C0436R.id.freqs_tag_parent), true, isSuggestion);
                new FriendsAdd(uid, null).setCallback(new C14727(req, v, uid)).exec(getActivity());
            }
            if ("decline".equals(v.getTag().toString())) {
                animateStateTransition((View) v.getTag(C0436R.id.freqs_tag_parent), false, isSuggestion);
                req.state = 3;
                new FriendsDelete(uid).setCallback(new C14738(v, uid)).exec(getActivity());
            }
        }
    }

    private void animateStateTransition(View view, boolean accepted, boolean suggestion) {
        int i = C0436R.string.friend_req_declined;
        ViewFlipper flipper = (ViewFlipper) view.findViewById(C0436R.id.friend_req_flipper);
        TextView textView = (TextView) flipper.findViewById(C0436R.id.friend_req_result_text);
        if (suggestion) {
            if (accepted) {
                i = C0436R.string.friend_req_sent;
            }
        } else if (accepted) {
            i = C0436R.string.friend_req_accepted;
        }
        textView.setText(i);
        ((TextView) flipper.findViewById(C0436R.id.friend_req_result_text)).setTextColor(-6710887);
        flipper.setDisplayedChild(2);
    }

    private ImageView getMImageView(int n, View view) {
        switch (n) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return (ImageView) view.findViewById(C0436R.id.friend_req_mf1);
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                return (ImageView) view.findViewById(C0436R.id.friend_req_mf2);
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return (ImageView) view.findViewById(C0436R.id.friend_req_mf3);
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                return (ImageView) view.findViewById(C0436R.id.friend_req_mf4);
            case UserListView.TYPE_FAVE /*4*/:
                return (ImageView) view.findViewById(C0436R.id.friend_req_mf5);
            default:
                return null;
        }
    }

    public void onRefresh() {
        this.refreshing = true;
        loadData();
    }

    public String getLastUpdatedTime() {
        if (this.lastUpdate > 0) {
            return new StringBuilder(String.valueOf(getResources().getString(C0436R.string.updated))).append(" ").append(Global.langDateRelativeNoDiff(this.lastUpdate, getResources())).toString();
        }
        return getResources().getString(C0436R.string.not_updated);
    }

    public void onPause() {
        super.onPause();
        this.imgLoader.deactivate();
    }

    public void onResume() {
        super.onResume();
        this.imgLoader.activate();
        ((BaseAdapter) ((HeaderViewListAdapter) this.list.getAdapter()).getWrappedAdapter()).notifyDataSetInvalidated();
    }

    public void onScrolled(float offset) {
        AlphaAnimation aa;
        if (offset > ((float) Global.scale(10.0f))) {
            if (this.noReqsView.getTag() == null) {
                aa = new AlphaAnimation(1.0f, 0.0f);
                aa.setFillAfter(true);
                aa.setDuration(200);
                if (this.noReqsView.getVisibility() == 0) {
                    this.noReqsView.startAnimation(aa);
                }
                if (this.bigProgress.getVisibility() == 0) {
                    this.bigProgress.startAnimation(aa);
                }
                this.noReqsView.setTag(Integer.valueOf(0));
            }
        } else if (Integer.valueOf(0).equals(this.noReqsView.getTag())) {
            aa = new AlphaAnimation(0.0f, 1.0f);
            aa.setFillAfter(true);
            aa.setDuration(200);
            if (this.noReqsView.getVisibility() == 0) {
                this.noReqsView.startAnimation(aa);
            }
            if (this.bigProgress.getVisibility() == 0) {
                this.bigProgress.startAnimation(aa);
            }
            this.noReqsView.setTag(null);
        }
    }
}
