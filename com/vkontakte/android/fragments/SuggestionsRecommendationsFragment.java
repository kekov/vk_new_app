package com.vkontakte.android.fragments;

import android.app.Activity;
import android.os.Bundle;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.SuggestionsActivity;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.api.NewsfeedGetSuggestedSources;
import com.vkontakte.android.api.NewsfeedGetSuggestedSources.Callback;
import java.util.List;

public class SuggestionsRecommendationsFragment extends SuggestionsFragment {

    /* renamed from: com.vkontakte.android.fragments.SuggestionsRecommendationsFragment.1 */
    class C15151 implements Callback {
        C15151() {
        }

        public void success(List<UserProfile> _users) {
            SuggestionsRecommendationsFragment.this.users.addAll(_users);
            SuggestionsRecommendationsFragment.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            SuggestionsRecommendationsFragment.this.onError(ecode, emsg);
        }
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        if (!(act instanceof SuggestionsActivity)) {
            act.setTitle(C0436R.string.empty_find_groups);
        }
    }

    protected void loadData() {
        new NewsfeedGetSuggestedSources().setCallback(new C15151()).exec(getActivity());
    }

    protected void onItemClick(int pos, long id, Object item) {
        Bundle args = new Bundle();
        args.putInt("id", (int) id);
        Navigate.to("ProfileFragment", args, getActivity());
    }

    protected String getListTitle() {
        return getString(C0436R.string.recom_groups_title);
    }
}
