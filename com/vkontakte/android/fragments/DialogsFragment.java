package com.vkontakte.android.fragments;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnCloseListener;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.ChatUser;
import com.vkontakte.android.DialogEntry;
import com.vkontakte.android.FragmentWrapperActivity;
import com.vkontakte.android.Global;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.Message;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.NetworkStateReceiver;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.MessagesDeleteDialog;
import com.vkontakte.android.api.MessagesDeleteDialog.Callback;
import com.vkontakte.android.data.Messages;
import com.vkontakte.android.data.Messages.GetChatUsersCallback;
import com.vkontakte.android.data.Messages.GetDialogsCallback;
import com.vkontakte.android.data.Messages.SearchCallback;
import com.vkontakte.android.ui.DialogEntryView;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.LoadMoreFooterView;
import com.vkontakte.android.ui.MessagesSearchSuggestionsPopup;
import com.vkontakte.android.ui.PhotoView;
import com.vkontakte.android.ui.RefreshableListView;
import com.vkontakte.android.ui.RefreshableListView.OnRefreshListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import org.acra.ACRAConstants;

public class DialogsFragment extends SherlockFragment implements OnItemClickListener, OnRefreshListener, OnItemLongClickListener {
    private static final int CREATE_CHAT_RESULT = 323;
    private static final int SELECT_MEMBERS_RESULT = 322;
    private static final int WRITE_RESULT = 321;
    private DialogsAdapter adapter;
    private FrameLayout contentView;
    private FrameLayout contentWrap;
    private boolean dataLoading;
    private ArrayList<DialogEntry> displayItems;
    private ArrayList<DialogEntry> dlgs;
    private EmptyView emptyView;
    private ErrorView error;
    private LoadMoreFooterView footerView;
    private ListImageLoaderWrapper imgLoader;
    private RefreshableListView list;
    private boolean moreAvailable;
    private ProgressBar progress;
    private BroadcastReceiver receiver;
    private String searchQuery;
    private ArrayList<DialogEntry> searchResults;
    private SearchView searchView;
    private SelectionListener selListener;
    private ArrayList<UserProfile> selectedTempUsers;
    private MessagesSearchSuggestionsPopup suggester;

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.12 */
    class AnonymousClass12 implements OnClickListener {
        private final /* synthetic */ DialogEntry val$e;

        AnonymousClass12(DialogEntry dialogEntry) {
            this.val$e = dialogEntry;
        }

        public void onClick(DialogInterface dialog, int which) {
            if (which == 0) {
                DialogsFragment.this.confirmAndClearHistory(this.val$e);
            }
            if (which == 1) {
                DialogsFragment.this.createShortcut(this.val$e);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.13 */
    class AnonymousClass13 implements OnClickListener {
        private final /* synthetic */ DialogEntry val$e;

        AnonymousClass13(DialogEntry dialogEntry) {
            this.val$e = dialogEntry;
        }

        public void onClick(DialogInterface dialog, int which) {
            DialogsFragment.this.doClearHistory(this.val$e);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.15 */
    class AnonymousClass15 implements Runnable {
        private final /* synthetic */ DialogEntry val$e;

        AnonymousClass15(DialogEntry dialogEntry) {
            this.val$e = dialogEntry;
        }

        public void run() {
            VKApplication.context.sendBroadcast(Messages.getShortcutIntent(this.val$e.profile));
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.1 */
    class C06621 extends BroadcastReceiver {

        /* renamed from: com.vkontakte.android.fragments.DialogsFragment.1.1 */
        class C14561 implements GetChatUsersCallback {
            private final /* synthetic */ DialogEntry val$e;
            private final /* synthetic */ int val$peer;

            /* renamed from: com.vkontakte.android.fragments.DialogsFragment.1.1.1 */
            class C06591 implements Runnable {
                private final /* synthetic */ DialogEntry val$e;

                C06591(DialogEntry dialogEntry) {
                    this.val$e = dialogEntry;
                }

                public void run() {
                    DialogsFragment.this.dlgs.add(0, this.val$e);
                    DialogsFragment.this.updateList();
                }
            }

            C14561(int i, DialogEntry dialogEntry) {
                this.val$peer = i;
                this.val$e = dialogEntry;
            }

            public void onUsersLoaded(ArrayList<ChatUser> arrayList, String title, String photo) {
                UserProfile p = new UserProfile();
                p.uid = this.val$peer;
                p.photo = photo;
                p.fullName = title;
                this.val$e.profile = p;
                if (DialogsFragment.this.getActivity() != null) {
                    DialogsFragment.this.getActivity().runOnUiThread(new C06591(this.val$e));
                }
            }
        }

        /* renamed from: com.vkontakte.android.fragments.DialogsFragment.1.2 */
        class C14572 implements GetChatUsersCallback {
            private final /* synthetic */ DialogEntry val$e;

            C14572(DialogEntry dialogEntry) {
                this.val$e = dialogEntry;
            }

            public void onUsersLoaded(ArrayList<ChatUser> arrayList, String title, String photo) {
                this.val$e.profile.fullName = title;
                if (photo != null) {
                    this.val$e.profile.photo = photo;
                }
                DialogsFragment.this.updateList();
            }
        }

        C06621() {
        }

        public void onReceive(Context context, Intent intent) {
            int peer;
            Iterator it;
            DialogEntry e;
            if (LongPollService.ACTION_NEW_MESSAGE.equals(intent.getAction())) {
                if (DialogsFragment.this.dlgs != null) {
                    Message msg = (Message) intent.getParcelableExtra(LongPollService.EXTRA_MESSAGE);
                    peer = intent.getIntExtra(LongPollService.EXTRA_PEER_ID, 0);
                    DialogEntry[] entry = new DialogEntry[1];
                    boolean found = false;
                    it = DialogsFragment.this.dlgs.iterator();
                    while (it.hasNext()) {
                        e = (DialogEntry) it.next();
                        if (e.profile.uid == peer) {
                            e.lastMessage = msg;
                            e.lastMessagePhoto = intent.getStringExtra("sender_photo");
                            DialogsFragment.this.dlgs.remove(e);
                            DialogsFragment.this.dlgs.add(0, e);
                            entry[0] = e;
                            DialogsFragment.this.updateList();
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        if (peer < 2000000000) {
                            e = new DialogEntry();
                            e.lastMessage = msg;
                            e.lastMessagePhoto = intent.getStringExtra("sender_photo");
                            e.profile = (UserProfile) intent.getParcelableExtra("peer_profile");
                            DialogsFragment.this.dlgs.add(0, e);
                            DialogsFragment.this.updateList();
                        } else {
                            e = new DialogEntry();
                            e.lastMessage = msg;
                            e.lastMessagePhoto = intent.getStringExtra("sender_photo");
                            Messages.getChatUsers(peer - 2000000000, new C14561(peer, e));
                        }
                    }
                } else {
                    return;
                }
            }
            if (LongPollService.ACTION_MESSAGE_RSTATE_CHANGED.equals(intent.getAction())) {
                int msg2 = intent.getIntExtra(LongPollService.EXTRA_MSG_ID, 0);
                boolean state = intent.getBooleanExtra(LongPollService.EXTRA_READ_STATE, false);
                if (!intent.hasExtra("le")) {
                    it = DialogsFragment.this.dlgs.iterator();
                    while (it.hasNext()) {
                        e = (DialogEntry) it.next();
                        if (e.lastMessage.id == msg2) {
                            e.lastMessage.readState = state;
                            DialogsFragment.this.updateList();
                            break;
                        }
                    }
                }
                peer = intent.getIntExtra(LongPollService.EXTRA_PEER_ID, 0);
                boolean in = intent.getBooleanExtra("in", false);
                boolean needUpdate = false;
                it = DialogsFragment.this.dlgs.iterator();
                while (it.hasNext()) {
                    e = (DialogEntry) it.next();
                    if (e.lastMessage.peer == peer && e.lastMessage.id <= msg2 && e.lastMessage.out != in) {
                        e.lastMessage.readState = state;
                        needUpdate = true;
                    }
                }
                if (needUpdate) {
                    DialogsFragment.this.updateList();
                }
            }
            if (LongPollService.ACTION_REFRESH_DIALOGS_LIST.equals(intent.getAction())) {
                DialogsFragment.this.loadData(true);
            }
            if (LongPollService.ACTION_USER_PRESENCE.equals(intent.getAction())) {
                int uid = intent.getIntExtra("uid", 0);
                int online = intent.getIntExtra("online", 0);
                it = DialogsFragment.this.dlgs.iterator();
                while (it.hasNext()) {
                    e = (DialogEntry) it.next();
                    if (e.profile.uid == uid) {
                        e.profile.online = online;
                        DialogsFragment.this.updateList();
                    }
                }
            }
            if (LongPollService.ACTION_CHAT_CHANGED.equals(intent.getAction())) {
                int cid = intent.getIntExtra("id", 0);
                it = DialogsFragment.this.dlgs.iterator();
                while (it.hasNext()) {
                    e = (DialogEntry) it.next();
                    if (e.lastMessage.peer == 2000000000 + cid) {
                        Messages.getChatUsers(cid, new C14572(e));
                        return;
                    }
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.2 */
    class C06632 implements View.OnClickListener {
        C06632() {
        }

        public void onClick(View v) {
            Bundle args = new Bundle();
            args.putBoolean("select", true);
            Intent intent = new Intent(DialogsFragment.this.getActivity(), FragmentWrapperActivity.class);
            intent.putExtra("class", "CreateChatFragment");
            intent.putExtra("args", args);
            DialogsFragment.this.startActivityForResult(intent, DialogsFragment.WRITE_RESULT);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.7 */
    class C06647 implements Runnable {
        C06647() {
        }

        public void run() {
            try {
                DialogsFragment.this.searchView.setMaxWidth(DialogsFragment.this.getActivity().getWindow().getDecorView().getWidth() - ((int) Math.round(((double) DialogsFragment.this.getSherlockActivity().getSupportActionBar().getHeight()) * 2.2d)));
            } catch (Exception e) {
                DialogsFragment.this.searchView.post(this);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.8 */
    class C06658 implements View.OnClickListener {
        C06658() {
        }

        public void onClick(View v) {
            DialogsFragment.this.error.setVisibility(8);
            DialogsFragment.this.progress.setVisibility(0);
            DialogsFragment.this.loadData(true);
        }
    }

    private class DialogsAdapter extends BaseAdapter {
        private DialogsAdapter() {
        }

        public int getCount() {
            return DialogsFragment.this.displayItems.size();
        }

        public Object getItem(int arg0) {
            return null;
        }

        public long getItemId(int arg0) {
            return 0;
        }

        public View getView(int pos, View convertView, ViewGroup arg2) {
            DialogEntryView v;
            if (convertView == null) {
                v = new DialogEntryView(DialogsFragment.this.getActivity());
            } else {
                v = (DialogEntryView) convertView;
            }
            DialogEntry e = (DialogEntry) DialogsFragment.this.displayItems.get(pos);
            v.setData(e, false);
            if (e.profile == null || !DialogsFragment.this.imgLoader.isAlreadyLoaded(e.profile.photo)) {
                v.setBitmap(null, 0);
            } else {
                v.setBitmap(DialogsFragment.this.imgLoader.get(e.profile.photo), 0);
            }
            if (e.lastMessage.out) {
                if (DialogsFragment.this.imgLoader.isAlreadyLoaded(DialogsFragment.this.getActivity().getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE))) {
                    v.setBitmap(DialogsFragment.this.imgLoader.get(DialogsFragment.this.getActivity().getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE)), 1);
                } else {
                    v.setBitmap(null, 1);
                }
            } else if (e.profile.uid > 2000000000) {
                if (DialogsFragment.this.imgLoader.isAlreadyLoaded(e.lastMessagePhoto)) {
                    v.setBitmap(DialogsFragment.this.imgLoader.get(e.lastMessagePhoto), 1);
                } else {
                    v.setBitmap(null, 1);
                }
            }
            return v;
        }
    }

    public interface SelectionListener {
        void onItemSelected(DialogEntry dialogEntry);
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.10 */
    class AnonymousClass10 implements SearchCallback {
        private final /* synthetic */ boolean val$refresh;

        /* renamed from: com.vkontakte.android.fragments.DialogsFragment.10.1 */
        class C06601 implements Runnable {
            private final /* synthetic */ int val$ecode;
            private final /* synthetic */ String val$emsg;

            C06601(int i, String str) {
                this.val$ecode = i;
                this.val$emsg = str;
            }

            public void run() {
                if (DialogsFragment.this.searchResults.size() == 0) {
                    DialogsFragment.this.error.setErrorInfo(this.val$ecode, this.val$emsg);
                    Global.showViewAnimated(DialogsFragment.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                    Global.showViewAnimated(DialogsFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                    return;
                }
                Toast.makeText(DialogsFragment.this.getActivity(), C0436R.string.err_text, 0).show();
            }
        }

        /* renamed from: com.vkontakte.android.fragments.DialogsFragment.10.2 */
        class C06612 implements Runnable {
            private final /* synthetic */ ArrayList val$dlgs;
            private final /* synthetic */ boolean val$refresh;
            private final /* synthetic */ int val$total;

            C06612(boolean z, ArrayList arrayList, int i) {
                this.val$refresh = z;
                this.val$dlgs = arrayList;
                this.val$total = i;
            }

            public void run() {
                if (this.val$refresh) {
                    DialogsFragment.this.searchResults.clear();
                    DialogsFragment.this.list.refreshDone();
                }
                DialogsFragment.this.searchResults.addAll(this.val$dlgs);
                DialogsFragment.this.updateList();
                if (this.val$dlgs.size() == 0 || DialogsFragment.this.searchResults.size() >= this.val$total) {
                    DialogsFragment.this.moreAvailable = false;
                } else {
                    DialogsFragment.this.moreAvailable = true;
                }
                DialogsFragment.this.footerView.setVisible(DialogsFragment.this.moreAvailable);
                if (DialogsFragment.this.contentWrap.getVisibility() != 0) {
                    Global.showViewAnimated(DialogsFragment.this.contentWrap, true, PhotoView.THUMB_ANIM_DURATION);
                    Global.showViewAnimated(DialogsFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                }
            }
        }

        AnonymousClass10(boolean z) {
            this.val$refresh = z;
        }

        public void onError(int ecode, String emsg) {
            DialogsFragment.this.dataLoading = false;
            if (DialogsFragment.this.getActivity() != null) {
                DialogsFragment.this.getActivity().runOnUiThread(new C06601(ecode, emsg));
            }
        }

        public void onDialogsLoaded(ArrayList<DialogEntry> dlgs, int total) {
            DialogsFragment.this.dataLoading = false;
            if (dlgs != null && DialogsFragment.this.getActivity() != null) {
                DialogsFragment.this.getActivity().runOnUiThread(new C06612(this.val$refresh, dlgs, total));
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.14 */
    class AnonymousClass14 implements Callback {
        private final /* synthetic */ DialogEntry val$e;

        AnonymousClass14(DialogEntry dialogEntry) {
            this.val$e = dialogEntry;
        }

        public void success() {
            DialogsFragment.this.dlgs.remove(this.val$e);
            Messages.removeDialog(this.val$e.profile.uid);
            DialogsFragment.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(DialogsFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.3 */
    class C14583 implements Listener {
        C14583() {
        }

        public void onScrolledToLastItem() {
            DialogsFragment.this.loadData(false);
        }

        public void onScrollStopped() {
        }

        public void onScrollStarted() {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.5 */
    class C14595 implements OnQueryTextListener {
        C14595() {
        }

        public boolean onQueryTextSubmit(String query) {
            if (DialogsFragment.this.getArguments().getBoolean("select", false)) {
                return false;
            }
            InputMethodManager imm = (InputMethodManager) DialogsFragment.this.getActivity().getSystemService("input_method");
            if (DialogsFragment.this.getActivity().getCurrentFocus() != null) {
                imm.hideSoftInputFromWindow(DialogsFragment.this.getActivity().getCurrentFocus().getWindowToken(), 0);
            }
            DialogsFragment.this.getActivity().getCurrentFocus().clearFocus();
            DialogsFragment.this.suggester.updateQuery(ACRAConstants.DEFAULT_STRING_VALUE);
            DialogsFragment.this.search(query);
            return true;
        }

        public boolean onQueryTextChange(String newText) {
            DialogsFragment.this.suggester.updateQuery(newText);
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.6 */
    class C14606 implements OnCloseListener {
        C14606() {
        }

        public boolean onClose() {
            DialogsFragment.this.search(null);
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.9 */
    class C14619 implements GetDialogsCallback {
        private final /* synthetic */ boolean val$refresh;

        /* renamed from: com.vkontakte.android.fragments.DialogsFragment.9.1 */
        class C06661 implements Runnable {
            private final /* synthetic */ ArrayList val$rdlgs;
            private final /* synthetic */ boolean val$refresh;

            C06661(boolean z, ArrayList arrayList) {
                this.val$refresh = z;
                this.val$rdlgs = arrayList;
            }

            public void run() {
                if (this.val$refresh) {
                    DialogsFragment.this.dlgs.clear();
                    DialogsFragment.this.list.refreshDone();
                }
                DialogsFragment.this.dlgs.addAll(this.val$rdlgs);
                DialogsFragment.this.updateList();
                if (this.val$rdlgs.size() == 0) {
                    DialogsFragment.this.moreAvailable = false;
                } else {
                    DialogsFragment.this.moreAvailable = true;
                }
                DialogsFragment.this.footerView.setVisible(DialogsFragment.this.moreAvailable);
                if (DialogsFragment.this.contentWrap.getVisibility() != 0) {
                    Global.showViewAnimated(DialogsFragment.this.contentWrap, true, PhotoView.THUMB_ANIM_DURATION);
                    Global.showViewAnimated(DialogsFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                }
            }
        }

        /* renamed from: com.vkontakte.android.fragments.DialogsFragment.9.2 */
        class C06672 implements Runnable {
            private final /* synthetic */ int val$ecode;
            private final /* synthetic */ String val$emsg;

            C06672(int i, String str) {
                this.val$ecode = i;
                this.val$emsg = str;
            }

            public void run() {
                if (DialogsFragment.this.dlgs.size() == 0) {
                    DialogsFragment.this.error.setErrorInfo(this.val$ecode, this.val$emsg);
                    Global.showViewAnimated(DialogsFragment.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                    Global.showViewAnimated(DialogsFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                    return;
                }
                Toast.makeText(DialogsFragment.this.getActivity(), C0436R.string.err_text, 0).show();
            }
        }

        C14619(boolean z) {
            this.val$refresh = z;
        }

        public void onDialogsLoaded(ArrayList<DialogEntry> rdlgs) {
            DialogsFragment.this.dataLoading = false;
            if (rdlgs != null && DialogsFragment.this.getActivity() != null) {
                DialogsFragment.this.getActivity().runOnUiThread(new C06661(this.val$refresh, rdlgs));
            }
        }

        public void onError(int ecode, String emsg) {
            DialogsFragment.this.dataLoading = false;
            if (DialogsFragment.this.getActivity() != null) {
                DialogsFragment.this.getActivity().runOnUiThread(new C06672(ecode, emsg));
            }
        }
    }

    private class DialogsPhotosAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.DialogsFragment.DialogsPhotosAdapter.1 */
        class C06681 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$image;
            private final /* synthetic */ View val$v;

            C06681(View view, Bitmap bitmap, int i) {
                this.val$v = view;
                this.val$bitmap = bitmap;
                this.val$image = i;
            }

            public void run() {
                if (this.val$v != null && (this.val$v instanceof DialogEntryView)) {
                    ((DialogEntryView) this.val$v).setBitmap(this.val$bitmap, this.val$image);
                }
            }
        }

        private DialogsPhotosAdapter() {
        }

        public int getItemCount() {
            return DialogsFragment.this.displayItems.size();
        }

        public int getImageCountForItem(int item) {
            return (((DialogEntry) DialogsFragment.this.displayItems.get(item)).lastMessage.out || ((DialogEntry) DialogsFragment.this.displayItems.get(item)).profile.uid > 2000000000) ? 2 : 1;
        }

        public String getImageURL(int item, int image) {
            DialogEntry e = (DialogEntry) DialogsFragment.this.displayItems.get(item);
            if (image == 0) {
                return e.profile.photo;
            }
            if (image != 1) {
                return null;
            }
            if (e.lastMessage.out) {
                return DialogsFragment.this.getActivity().getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE);
            }
            if (e.profile.uid > 2000000000) {
                return e.lastMessagePhoto;
            }
            return null;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += DialogsFragment.this.list.getHeaderViewsCount();
            if (item >= DialogsFragment.this.list.getFirstVisiblePosition() && item <= DialogsFragment.this.list.getLastVisiblePosition()) {
                View v = DialogsFragment.this.list.getChildAt(item - DialogsFragment.this.list.getFirstVisiblePosition());
                if (DialogsFragment.this.getActivity() != null) {
                    DialogsFragment.this.getActivity().runOnUiThread(new C06681(v, bitmap, image));
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DialogsFragment.4 */
    class C16204 extends SearchView {
        C16204(Context $anonymous0) {
            super($anonymous0);
        }

        public boolean dispatchKeyEventPreIme(KeyEvent event) {
            return DialogsFragment.this.suggester.onKeyPreIme(event.getKeyCode(), event);
        }
    }

    public DialogsFragment() {
        this.dlgs = new ArrayList();
        this.searchResults = new ArrayList();
        this.selectedTempUsers = new ArrayList();
        this.displayItems = this.dlgs;
        this.dataLoading = false;
        this.receiver = new C06621();
        this.moreAvailable = true;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        IntentFilter filter = new IntentFilter();
        filter.addAction(LongPollService.ACTION_NEW_MESSAGE);
        filter.addAction(LongPollService.ACTION_USER_PRESENCE);
        filter.addAction(LongPollService.ACTION_MESSAGE_RSTATE_CHANGED);
        filter.addAction(LongPollService.ACTION_REFRESH_DIALOGS_LIST);
        filter.addAction(LongPollService.ACTION_CHAT_CHANGED);
        VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
    }

    public void onResume() {
        super.onResume();
        ((NotificationManager) getActivity().getSystemService("notification")).cancel(10);
        this.imgLoader.activate();
        if (this.suggester != null) {
            this.suggester.onResume();
        }
    }

    public void onPause() {
        super.onPause();
        this.imgLoader.deactivate();
        if (this.suggester != null) {
            this.suggester.onPause();
        }
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        this.list = new RefreshableListView(act);
        this.footerView = new LoadMoreFooterView(act);
        this.list.addFooterView(this.footerView, null, false);
        RefreshableListView refreshableListView = this.list;
        ListAdapter dialogsAdapter = new DialogsAdapter();
        this.adapter = dialogsAdapter;
        refreshableListView.setAdapter(dialogsAdapter);
        this.list.setDivider(new ColorDrawable(-2104603));
        this.list.setDividerHeight(Global.scale(1.0f));
        this.list.setOnItemClickListener(this);
        this.list.setOnItemLongClickListener(this);
        this.list.setSelector(C0436R.drawable.highlight);
        this.list.setOnRefreshListener(this);
        this.list.setTopColor(-1);
        this.contentWrap = new FrameLayout(act);
        this.contentWrap.addView(this.list);
        this.emptyView = EmptyView.create(act);
        this.emptyView.setText((int) C0436R.string.no_messages);
        this.emptyView.setButtonText((int) C0436R.string.write_a_message);
        this.emptyView.setButtonVisible(true);
        this.emptyView.setOnBtnClickListener(new C06632());
        this.contentWrap.addView(this.emptyView);
        this.list.setEmptyView(this.emptyView);
        this.progress = new ProgressBar(act);
        this.contentView = new FrameLayout(act);
        this.contentView.setBackgroundColor(-1);
        this.contentView.addView(this.contentWrap);
        this.contentView.addView(this.progress, new LayoutParams(Global.scale(40.0f), Global.scale(40.0f), 17));
        this.contentWrap.setVisibility(8);
        this.imgLoader = new ListImageLoaderWrapper(new DialogsPhotosAdapter(), this.list, new C14583());
        SherlockFragmentActivity sa = getSherlockActivity();
        if (!getArguments().getBoolean("select", false)) {
            sa.getSupportActionBar().setDisplayShowTitleEnabled(true);
            sa.getSupportActionBar().setNavigationMode(0);
            sa.setTitle(C0436R.string.messages);
        }
        this.searchView = new C16204(sa.getSupportActionBar().getThemedContext());
        this.searchView.setQueryHint(getResources().getString(C0436R.string.search));
        try {
            Field searchField = SearchView.class.getDeclaredField("mSearchButton");
            searchField.setAccessible(true);
            ((ImageView) searchField.get(this.searchView)).setImageResource(C0436R.drawable.ic_ab_search);
            searchField = SearchView.class.getDeclaredField("mSearchPlate");
            searchField.setAccessible(true);
            ((TextView) ((LinearLayout) searchField.get(this.searchView)).getChildAt(0)).setHintTextColor(-2130706433);
        } catch (Exception e) {
        }
        this.searchView.setOnQueryTextListener(new C14595());
        this.searchView.setOnCloseListener(new C14606());
        this.suggester = new MessagesSearchSuggestionsPopup(this.searchView, getActivity(), !getArguments().getBoolean("select", false));
        this.contentView.addView(this.suggester.getView());
        if (this.selListener != null) {
            this.suggester.setSelectionListener(this.selListener);
        }
        setHasOptionsMenu(true);
        this.searchView.post(new C06647());
        this.error = (ErrorView) View.inflate(getActivity(), C0436R.layout.error, null);
        this.error.setOnRetryListener(new C06658());
        this.error.setVisibility(8);
        this.contentView.addView(this.error);
        loadData(false);
        if (Global.longPoll == null && NetworkStateReceiver.isConnected()) {
            getActivity().startService(new Intent(getActivity(), LongPollService.class));
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem search = menu.add((int) C0436R.string.search);
        search.setShowAsAction(2);
        search.setActionView(this.searchView);
        if (!getArguments().getBoolean("select", false)) {
            MenuItem write = menu.add(0, (int) C0436R.id.newpost, 0, (int) C0436R.string.open_chat);
            write.setShowAsAction(2);
            write.setIcon((int) C0436R.drawable.ic_ab_write);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0436R.id.newpost) {
            Bundle args = new Bundle();
            args.putBoolean("select", true);
            Intent intent = new Intent(getActivity(), FragmentWrapperActivity.class);
            intent.putExtra("class", "CreateChatFragment");
            intent.putExtra("args", args);
            startActivityForResult(intent, WRITE_RESULT);
        }
        return true;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentView;
    }

    private void search(String q) {
        this.searchQuery = q;
        this.error.setVisibility(8);
        if (q == null) {
            this.displayItems = this.dlgs;
            this.moreAvailable = true;
            updateList();
            this.error.setVisibility(8);
            this.emptyView.setText((int) C0436R.string.no_messages);
            this.emptyView.setButtonVisible(true);
            return;
        }
        this.emptyView.setText((int) C0436R.string.nothing_found);
        this.emptyView.setButtonVisible(false);
        this.searchResults = new ArrayList();
        this.displayItems = this.searchResults;
        updateList();
        loadData(true);
        this.contentWrap.setVisibility(8);
        this.progress.setVisibility(0);
    }

    public void loadData(boolean refresh) {
        int i = 0;
        if (!this.dataLoading) {
            if (this.moreAvailable || refresh) {
                this.dataLoading = true;
                if (this.searchQuery == null) {
                    if (!refresh) {
                        i = this.dlgs.size();
                    }
                    Messages.getDialogs(i, 20, new C14619(refresh));
                    return;
                }
                String str = this.searchQuery;
                if (!refresh) {
                    i = this.searchResults.size();
                }
                Messages.search(str, i, 20, new AnonymousClass10(refresh));
            }
        }
    }

    public void setListener(SelectionListener l) {
        this.selListener = l;
        if (this.suggester != null) {
            this.suggester.setSelectionListener(l);
        }
    }

    private void updateList() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    DialogsFragment.this.preventDuplicates();
                    if (DialogsFragment.this.dlgs.size() > 0) {
                        DialogsFragment.this.error.setVisibility(8);
                    }
                    DialogsFragment.this.adapter.notifyDataSetChanged();
                    DialogsFragment.this.imgLoader.updateImages();
                }
            });
        }
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        Bundle args;
        if (reqCode == WRITE_RESULT) {
            if (resCode == -1) {
                UserProfile profile = (UserProfile) data.getParcelableExtra("user");
                args = new Bundle();
                args.putInt("id", profile.uid);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, profile.fullName);
                if (profile.uid < 2000000000) {
                    args.putCharSequence("photo", profile.photo);
                }
                Navigate.to("ChatFragment", args, getActivity());
            } else if (resCode == 1) {
                Intent intent = new Intent(getActivity(), FragmentWrapperActivity.class);
                intent.putExtra("class", "CreateChatFragment");
                args = new Bundle();
                args.putBoolean("chat", true);
                intent.putExtra("args", args);
                startActivityForResult(intent, SELECT_MEMBERS_RESULT);
            }
        }
        if (reqCode == SELECT_MEMBERS_RESULT) {
            if (resCode == -1) {
                ArrayList<UserProfile> users = data.getParcelableArrayListExtra("users");
                this.selectedTempUsers = users;
                args = new Bundle();
                args.putParcelableArrayList("users", users);
                args.putBoolean("create", true);
                intent = new Intent(getActivity(), FragmentWrapperActivity.class);
                intent.putExtra("class", "ChatMembersFragment");
                intent.putExtra("args", args);
                startActivityForResult(intent, CREATE_CHAT_RESULT);
            } else {
                args = new Bundle();
                args.putBoolean("select", true);
                intent = new Intent(getActivity(), FragmentWrapperActivity.class);
                intent.putExtra("class", "CreateChatFragment");
                intent.putExtra("args", args);
                startActivityForResult(intent, WRITE_RESULT);
                getActivity().overridePendingTransition(C0436R.anim.activity_close_enter, C0436R.anim.activity_close_exit);
            }
        }
        if (reqCode == CREATE_CHAT_RESULT && resCode != -1) {
            intent = new Intent(getActivity(), FragmentWrapperActivity.class);
            intent.putExtra("class", "CreateChatFragment");
            args = new Bundle();
            if (this.selectedTempUsers != null) {
                args.putParcelableArrayList("selected", this.selectedTempUsers);
            }
            args.putBoolean("chat", true);
            intent.putExtra("args", args);
            startActivityForResult(intent, SELECT_MEMBERS_RESULT);
            this.selectedTempUsers = null;
            getActivity().overridePendingTransition(C0436R.anim.activity_close_enter, C0436R.anim.activity_close_exit);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
        pos -= this.list.getHeaderViewsCount();
        if (pos >= 0) {
            DialogEntry e = (DialogEntry) this.displayItems.get(pos);
            if (this.selListener != null) {
                this.selListener.onItemSelected(e);
            } else if (this.searchQuery == null) {
                args = new Bundle();
                args.putInt("id", e.profile.uid);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, e.profile.fullName);
                if (e.profile.uid < 2000000000) {
                    args.putCharSequence("photo", e.profile.photo);
                }
                Navigate.to("ChatFragment", args, getActivity());
            } else {
                args = new Bundle();
                args.putInt("id", e.profile.uid);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, e.profile.fullName);
                if (e.profile.uid < 2000000000) {
                    args.putCharSequence("photo", e.profile.photo);
                }
                args.putParcelable("from_search", e.lastMessage);
                Navigate.to("ChatFragment", args, getActivity());
            }
        }
    }

    public void onRefresh() {
        Messages.reset();
        Messages.resetCache();
        loadData(true);
    }

    public String getLastUpdatedTime() {
        return new StringBuilder(String.valueOf(getResources().getString(C0436R.string.updated))).append(" ").append(Global.langDateRelative((int) (Messages.getLastUpdated() / 1000), getResources())).toString();
    }

    public void onScrolled(float offset) {
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
        if (this.selListener != null || this.searchQuery != null) {
            return false;
        }
        pos -= this.list.getHeaderViewsCount();
        if (pos < 0) {
            return false;
        }
        DialogEntry e = (DialogEntry) this.displayItems.get(pos);
        new Builder(getActivity()).setItems(new String[]{getString(C0436R.string.clear_messages_history), getString(C0436R.string.chat_add_shortcut)}, new AnonymousClass12(e)).show();
        return true;
    }

    private void confirmAndClearHistory(DialogEntry e) {
        new Builder(getActivity()).setTitle(C0436R.string.confirm).setMessage(C0436R.string.clear_messages_history_confirm).setPositiveButton(C0436R.string.yes, new AnonymousClass13(e)).setNegativeButton(C0436R.string.no, null).show();
    }

    private void doClearHistory(DialogEntry e) {
        new MessagesDeleteDialog(e.profile.uid).setCallback(new AnonymousClass14(e)).wrapProgress(getActivity()).exec(getActivity());
    }

    private void createShortcut(DialogEntry e) {
        new Thread(new AnonymousClass15(e)).start();
    }

    private void preventDuplicates() {
        ArrayList<Integer> ids = new ArrayList();
        Iterator<DialogEntry> itr = this.dlgs.iterator();
        while (itr.hasNext()) {
            DialogEntry de = (DialogEntry) itr.next();
            if (ids.contains(Integer.valueOf(de.lastMessage.peer))) {
                itr.remove();
            } else {
                ids.add(Integer.valueOf(de.lastMessage.peer));
            }
        }
    }
}
