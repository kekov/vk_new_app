package com.vkontakte.android.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.ActivityUtils;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.BoardCloseTopic;
import com.vkontakte.android.api.BoardDeleteTopic;
import com.vkontakte.android.api.BoardFixTopic;
import com.vkontakte.android.api.BoardFixTopic.Callback;
import com.vkontakte.android.api.BoardGetTopics;
import com.vkontakte.android.api.BoardTopic;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.LoadMoreFooterView;
import com.vkontakte.android.ui.PhotoView;
import com.vkontakte.android.ui.RefreshableListView;
import com.vkontakte.android.ui.RefreshableListView.OnRefreshListener;
import java.util.ArrayList;
import org.acra.ACRAConstants;

public class BoardTopicsFragment extends BaseListFragment implements OnRefreshListener, Listener, OnItemClickListener, OnItemLongClickListener {
    private static final int CREATE_TOPIC_RESULT = 1;
    public static final int ORDER_CREATED_ASC = -2;
    public static final int ORDER_CREATED_DESC = 2;
    public static final int ORDER_UPDATED_ASC = -1;
    public static final int ORDER_UPDATED_DESC = 1;
    private TopicListAdapter adapter;
    private boolean canCreate;
    private APIRequest currentRequest;
    protected boolean dataLoading;
    private int defaultOrder;
    private LoadMoreFooterView footerView;
    protected ListImageLoaderWrapper imgLoader;
    private long lastUpdate;
    protected boolean moreAvailable;
    protected boolean preloadOnReady;
    protected ArrayList<BoardTopic> preloadedTopics;
    protected boolean preloading;
    private boolean refreshOnResume;
    protected boolean refreshing;
    protected ArrayList<BoardTopic> topics;
    private int type;

    /* renamed from: com.vkontakte.android.fragments.BoardTopicsFragment.1 */
    class C06021 implements OnClickListener {
        C06021() {
        }

        public void onClick(View v) {
            BoardTopicsFragment.this.errorView.setVisibility(8);
            BoardTopicsFragment.this.progress.setVisibility(0);
            BoardTopicsFragment.this.loadData();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicsFragment.3 */
    class C06033 implements DialogInterface.OnClickListener {
        private final /* synthetic */ EditText val$edit;

        C06033(EditText editText) {
            this.val$edit = editText;
        }

        public void onClick(DialogInterface dialog, int which) {
            BoardTopicsFragment.this.refreshOnResume = true;
            String title = this.val$edit.getText().toString();
            if (title.length() != 0) {
                Bundle args = new Bundle();
                args.putInt("tid", BoardTopicsFragment.ORDER_UPDATED_ASC);
                args.putInt("gid", BoardTopicsFragment.this.getArguments().getInt("gid", 0));
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, title);
                Navigate.to("BoardTopicViewFragment", args, BoardTopicsFragment.this.getActivity());
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicsFragment.4 */
    class C06044 implements OnShowListener {
        private final /* synthetic */ EditText val$edit;

        C06044(EditText editText) {
            this.val$edit = editText;
        }

        public void onShow(DialogInterface dialog) {
            ((InputMethodManager) BoardTopicsFragment.this.getActivity().getSystemService("input_method")).showSoftInput(this.val$edit, BoardTopicsFragment.ORDER_UPDATED_DESC);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicsFragment.5 */
    class C06055 implements Runnable {
        C06055() {
        }

        public void run() {
            BoardTopicsFragment.this.adapter.notifyDataSetChanged();
            BoardTopicsFragment.this.imgLoader.updateImages();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicsFragment.6 */
    class C06066 implements DialogInterface.OnClickListener {
        private final /* synthetic */ BoardTopic val$topic;

        C06066(BoardTopic boardTopic) {
            this.val$topic = boardTopic;
        }

        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    BoardTopicsFragment.this.confirmDeleteTopic(this.val$topic);
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    BoardTopicsFragment.this.toggleCloseTopic(this.val$topic);
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    BoardTopicsFragment.this.toggleFixTopic(this.val$topic);
                default:
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicsFragment.7 */
    class C06077 implements DialogInterface.OnClickListener {
        private final /* synthetic */ BoardTopic val$topic;

        C06077(BoardTopic boardTopic) {
            this.val$topic = boardTopic;
        }

        public void onClick(DialogInterface dialog, int which) {
            BoardTopicsFragment.this.deleteTopic(this.val$topic);
        }
    }

    protected class TopicListAdapter extends BaseAdapter {
        protected TopicListAdapter() {
        }

        public int getCount() {
            return BoardTopicsFragment.this.topics.size();
        }

        public Object getItem(int pos) {
            return null;
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int pos, View view, ViewGroup group) {
            if (view == null) {
                view = View.inflate(BoardTopicsFragment.this.getActivity(), C0436R.layout.board_topic_row, null);
            }
            BoardTopic t = (BoardTopic) BoardTopicsFragment.this.topics.get(pos);
            ((TextView) view.findViewById(C0436R.id.board_topic_title)).setText(t.title);
            String info = Global.langPlural(C0436R.array.topic_posts, t.numComments, BoardTopicsFragment.this.getResources());
            if ((t.flags & BoardTopicsFragment.ORDER_UPDATED_DESC) > 0 && (t.flags & BoardTopicsFragment.ORDER_CREATED_DESC) > 0) {
                info = new StringBuilder(String.valueOf(info)).append(", ").append(BoardTopicsFragment.this.getResources().getString(C0436R.string.topic_info_fixed_closed)).toString();
            } else if ((t.flags & BoardTopicsFragment.ORDER_UPDATED_DESC) > 0) {
                info = new StringBuilder(String.valueOf(info)).append(", ").append(BoardTopicsFragment.this.getResources().getString(C0436R.string.topic_info_closed)).toString();
            } else if ((t.flags & BoardTopicsFragment.ORDER_CREATED_DESC) > 0) {
                info = new StringBuilder(String.valueOf(info)).append(", ").append(BoardTopicsFragment.this.getResources().getString(C0436R.string.topic_info_fixed)).toString();
            }
            ((TextView) view.findViewById(C0436R.id.board_topic_info)).setText(info);
            ((TextView) view.findViewById(C0436R.id.board_topic_l_name)).setText(t.updatedBy.fullName);
            ((TextView) view.findViewById(C0436R.id.board_topic_l_text)).setText(t.lastComment);
            ((TextView) view.findViewById(C0436R.id.board_topic_l_updated)).setText(Global.langDateRelative(t.updated, BoardTopicsFragment.this.getResources()));
            if (BoardTopicsFragment.this.imgLoader.isAlreadyLoaded(t.updatedBy.photo)) {
                ((ImageView) view.findViewById(C0436R.id.board_topic_photo)).setImageBitmap(BoardTopicsFragment.this.imgLoader.get(t.updatedBy.photo));
            } else {
                ((ImageView) view.findViewById(C0436R.id.board_topic_photo)).setImageResource(C0436R.drawable.user_placeholder);
            }
            return view;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicsFragment.10 */
    class AnonymousClass10 implements Callback {
        private final /* synthetic */ boolean val$isFixed;
        private final /* synthetic */ BoardTopic val$topic;

        AnonymousClass10(boolean z, BoardTopic boardTopic) {
            this.val$isFixed = z;
            this.val$topic = boardTopic;
        }

        public void success() {
            Toast.makeText(BoardTopicsFragment.this.getActivity(), this.val$isFixed ? C0436R.string.topic_unfixed : C0436R.string.topic_fixed, 0).show();
            BoardTopic boardTopic;
            if (this.val$isFixed) {
                boardTopic = this.val$topic;
                boardTopic.flags &= -3;
            } else {
                boardTopic = this.val$topic;
                boardTopic.flags |= BoardTopicsFragment.ORDER_CREATED_DESC;
            }
            if (this.val$isFixed) {
                BoardTopicsFragment.this.topics.remove(this.val$topic);
                boolean added = false;
                for (int i = 0; i < BoardTopicsFragment.this.topics.size() + BoardTopicsFragment.ORDER_UPDATED_ASC; i += BoardTopicsFragment.ORDER_UPDATED_DESC) {
                    BoardTopic t1 = (BoardTopic) BoardTopicsFragment.this.topics.get(i);
                    BoardTopic t2 = (BoardTopic) BoardTopicsFragment.this.topics.get(i + BoardTopicsFragment.ORDER_UPDATED_DESC);
                    if ((t1.flags & BoardTopicsFragment.ORDER_CREATED_DESC) <= 0 && ((BoardTopicsFragment.this.defaultOrder == BoardTopicsFragment.ORDER_UPDATED_DESC && this.val$topic.updated < t1.updated && this.val$topic.updated >= t2.updated) || ((BoardTopicsFragment.this.defaultOrder == BoardTopicsFragment.ORDER_UPDATED_ASC && this.val$topic.updated < t2.updated && this.val$topic.updated >= t1.updated) || ((BoardTopicsFragment.this.defaultOrder == BoardTopicsFragment.ORDER_CREATED_DESC && this.val$topic.created < t1.created && this.val$topic.created >= t2.created) || (BoardTopicsFragment.this.defaultOrder == BoardTopicsFragment.ORDER_CREATED_ASC && this.val$topic.created < t2.created && this.val$topic.created >= t1.created))))) {
                        added = true;
                        BoardTopicsFragment.this.topics.add(i + BoardTopicsFragment.ORDER_UPDATED_DESC, this.val$topic);
                    }
                }
                if (!added) {
                    BoardTopicsFragment.this.topics.add(this.val$topic);
                }
            } else {
                BoardTopicsFragment.this.topics.remove(this.val$topic);
                BoardTopicsFragment.this.topics.add(0, this.val$topic);
            }
            BoardTopicsFragment.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(BoardTopicsFragment.this.getActivity(), C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicsFragment.2 */
    class C14392 implements BoardGetTopics.Callback {
        C14392() {
        }

        public void success(int count, ArrayList<BoardTopic> topics, boolean canCreate, int order) {
            boolean z;
            if (BoardTopicsFragment.this.refreshing || BoardTopicsFragment.this.topics.size() == 0) {
                BoardTopicsFragment.this.preloadedTopics.clear();
                BoardTopicsFragment.this.topics.clear();
                BoardTopicsFragment.this.lastUpdate = System.currentTimeMillis();
            }
            if (BoardTopicsFragment.this.preloading) {
                BoardTopicsFragment.this.preloadedTopics.addAll(topics);
            } else if (topics.size() > 20) {
                BoardTopicsFragment.this.topics.addAll(topics.subList(0, 20));
                BoardTopicsFragment.this.preloadedTopics.addAll(topics.subList(20, topics.size()));
            } else {
                BoardTopicsFragment.this.topics.addAll(topics);
            }
            BoardTopicsFragment.this.preloading = false;
            if (BoardTopicsFragment.this.preloadOnReady) {
                BoardTopicsFragment.this.preloading = true;
                BoardTopicsFragment.this.preloadOnReady = false;
                BoardTopicsFragment.this.loadData();
            }
            BoardTopicsFragment.this.updateList();
            BoardTopicsFragment boardTopicsFragment = BoardTopicsFragment.this;
            if (BoardTopicsFragment.this.topics.size() < count) {
                z = true;
            } else {
                z = false;
            }
            boardTopicsFragment.moreAvailable = z;
            if (BoardTopicsFragment.this.contentWrap.getVisibility() != 0) {
                Global.showViewAnimated(BoardTopicsFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(BoardTopicsFragment.this.contentWrap, true, PhotoView.THUMB_ANIM_DURATION);
            }
            BoardTopicsFragment.this.footerView.setVisible(BoardTopicsFragment.this.moreAvailable);
            if (BoardTopicsFragment.this.refreshing) {
                ((RefreshableListView) BoardTopicsFragment.this.list).refreshDone();
                BoardTopicsFragment.this.refreshing = false;
            }
            BoardTopicsFragment.this.canCreate = canCreate;
            BoardTopicsFragment.this.getSherlockActivity().invalidateOptionsMenu();
            BoardTopicsFragment.this.defaultOrder = order;
            BoardTopicsFragment.this.currentRequest = null;
        }

        public void fail(int ecode, String emsg) {
            BoardTopicsFragment.this.dataLoading = false;
            BoardTopicsFragment.this.currentRequest = null;
            if (BoardTopicsFragment.this.topics.size() == 0) {
                BoardTopicsFragment.this.errorView.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(BoardTopicsFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(BoardTopicsFragment.this.errorView, true, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(BoardTopicsFragment.this.getActivity(), C0436R.string.err_text, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicsFragment.8 */
    class C14408 implements BoardDeleteTopic.Callback {
        private final /* synthetic */ BoardTopic val$topic;

        C14408(BoardTopic boardTopic) {
            this.val$topic = boardTopic;
        }

        public void success() {
            BoardTopicsFragment.this.topics.remove(this.val$topic);
            BoardTopicsFragment.this.updateList();
        }

        public void fail(int ecode, String emsg) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BoardTopicsFragment.9 */
    class C14419 implements BoardCloseTopic.Callback {
        private final /* synthetic */ boolean val$isClosed;
        private final /* synthetic */ BoardTopic val$topic;

        C14419(boolean z, BoardTopic boardTopic) {
            this.val$isClosed = z;
            this.val$topic = boardTopic;
        }

        public void success() {
            Toast.makeText(BoardTopicsFragment.this.getActivity(), this.val$isClosed ? C0436R.string.topic_opened : C0436R.string.topic_closed, 0).show();
            BoardTopic boardTopic;
            if (this.val$isClosed) {
                boardTopic = this.val$topic;
                boardTopic.flags &= BoardTopicsFragment.ORDER_CREATED_ASC;
            } else {
                boardTopic = this.val$topic;
                boardTopic.flags |= BoardTopicsFragment.ORDER_UPDATED_DESC;
            }
            BoardTopicsFragment.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(BoardTopicsFragment.this.getActivity(), C0436R.string.error, 0).show();
        }
    }

    private class UserPhotosAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.BoardTopicsFragment.UserPhotosAdapter.1 */
        class C06081 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C06081(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.board_topic_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private UserPhotosAdapter() {
        }

        public int getItemCount() {
            return BoardTopicsFragment.this.topics.size();
        }

        public int getImageCountForItem(int item) {
            return BoardTopicsFragment.ORDER_UPDATED_DESC;
        }

        public String getImageURL(int item, int image) {
            return ((BoardTopic) BoardTopicsFragment.this.topics.get(item)).updatedBy.photo;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += BoardTopicsFragment.this.list.getHeaderViewsCount();
            if (item >= BoardTopicsFragment.this.list.getFirstVisiblePosition() && item <= BoardTopicsFragment.this.list.getLastVisiblePosition()) {
                BoardTopicsFragment.this.getActivity().runOnUiThread(new C06081(BoardTopicsFragment.this.list.getChildAt(item - BoardTopicsFragment.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    public BoardTopicsFragment() {
        this.topics = new ArrayList();
        this.preloadedTopics = new ArrayList();
        this.dataLoading = false;
        this.moreAvailable = false;
        this.preloading = false;
        this.preloadOnReady = false;
        this.refreshing = false;
        this.lastUpdate = 0;
        this.refreshOnResume = false;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        ActivityUtils.setBeamLink(act, "board" + getArguments().getInt("gid", 0));
        this.footerView = new LoadMoreFooterView(act);
        this.list.addFooterView(this.footerView, null, false);
        ListView listView = this.list;
        ListAdapter topicListAdapter = new TopicListAdapter();
        this.adapter = topicListAdapter;
        listView.setAdapter(topicListAdapter);
        ((RefreshableListView) this.list).setOnRefreshListener(this);
        this.list.setDrawSelectorOnTop(true);
        this.list.setOnItemClickListener(this);
        this.list.setOnItemLongClickListener(this);
        this.list.setVisibility(8);
        this.imgLoader = new ListImageLoaderWrapper(new UserPhotosAdapter(), this.list, this);
        loadData();
        SherlockFragmentActivity sa = getSherlockActivity();
        sa.getSupportActionBar().setNavigationMode(0);
        sa.getSupportActionBar().setDisplayShowTitleEnabled(true);
        act.setTitle(C0436R.string.topics);
        setHasOptionsMenu(true);
        this.errorView.setOnRetryListener(new C06021());
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(C0436R.menu.topics_list, menu);
        menu.findItem(C0436R.id.create).setVisible(this.canCreate);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0436R.id.create) {
            showCreateBox();
        }
        return true;
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.currentRequest != null) {
            this.currentRequest.cancel();
        }
    }

    public void onScrolledToLastItem() {
        if ((this.dataLoading && !this.preloading) || !this.moreAvailable) {
            return;
        }
        if (this.preloading) {
            this.preloading = false;
            this.preloadOnReady = true;
        } else if (this.preloadedTopics.size() > 0) {
            this.topics.addAll(this.preloadedTopics);
            updateList();
            this.preloadedTopics.clear();
            this.preloading = true;
            loadData();
        } else {
            loadData();
        }
    }

    protected ListView initListView() {
        RefreshableListView l = new RefreshableListView(getActivity());
        l.setSelector(C0436R.drawable.highlight_post);
        l.setDividerHeight(0);
        l.setBackgroundColor(-2039584);
        l.setCacheColorHint(-2039584);
        l.setTopColor(-2039584);
        return l;
    }

    public void onScrollStarted() {
    }

    public void onScrollStopped() {
    }

    public void loadData() {
        int i = 0;
        if (!this.dataLoading) {
            int i2 = getArguments().getInt("gid", 0);
            if (!this.refreshing) {
                i = this.topics.size();
            }
            int i3 = (!this.preloading || this.refreshing) ? 40 : 20;
            this.currentRequest = new BoardGetTopics(i2, i, i3).setCallback(new C14392()).exec(this.contentView);
        }
    }

    private void showCreateBox() {
        EditText edit = new EditText(getActivity());
        edit.setHint(C0436R.string.enter_topic_title);
        FrameLayout editWrap = new FrameLayout(getActivity());
        int pad = Global.scale(10.0f);
        editWrap.setPadding(pad, pad, pad, pad);
        editWrap.addView(edit);
        AlertDialog dlg = new Builder(getActivity()).setTitle(C0436R.string.create_topic).setView(editWrap).setPositiveButton(C0436R.string.ok, new C06033(edit)).setNegativeButton(C0436R.string.cancel, null).create();
        dlg.setOnShowListener(new C06044(edit));
        dlg.show();
    }

    public void updateList() {
        getActivity().runOnUiThread(new C06055());
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        pos -= this.list.getHeaderViewsCount();
        if (pos >= 0 && pos < this.topics.size()) {
            Bundle args = new Bundle();
            args.putInt("tid", ((BoardTopic) this.topics.get(pos)).id);
            args.putInt("gid", getArguments().getInt("gid", 0));
            if (getArguments().containsKey("is_admin")) {
                args.putInt("is_admin", ORDER_UPDATED_DESC);
            }
            args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, ((BoardTopic) this.topics.get(pos)).title);
            if ((((BoardTopic) this.topics.get(pos)).flags & ORDER_UPDATED_DESC) > 0) {
                args.putInt("is_closed", ORDER_UPDATED_DESC);
            }
            Navigate.to("BoardTopicViewFragment", args, getActivity());
        }
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id) {
        pos -= this.list.getHeaderViewsCount();
        if (pos < 0 || pos >= this.topics.size()) {
            return false;
        }
        BoardTopic topic = (BoardTopic) this.topics.get(pos);
        if (!Groups.isGroupAdmin(getArguments().getInt("gid", 0)) && topic.creator != Global.uid) {
            return false;
        }
        int i;
        Builder builder = new Builder(getActivity());
        CharSequence[] charSequenceArr = new String[3];
        charSequenceArr[0] = getResources().getString(C0436R.string.delete);
        Resources resources = getResources();
        if ((topic.flags & ORDER_UPDATED_DESC) > 0) {
            i = C0436R.string.open_topic;
        } else {
            i = C0436R.string.close_topic;
        }
        charSequenceArr[ORDER_UPDATED_DESC] = resources.getString(i);
        Resources resources2 = getResources();
        if ((topic.flags & ORDER_CREATED_DESC) > 0) {
            i = C0436R.string.unfix_topic;
        } else {
            i = C0436R.string.fix_topic;
        }
        charSequenceArr[ORDER_CREATED_DESC] = resources2.getString(i);
        builder.setItems(charSequenceArr, new C06066(topic)).show();
        return true;
    }

    private void confirmDeleteTopic(BoardTopic topic) {
        new Builder(getActivity()).setTitle(C0436R.string.delete_topic_title).setMessage(C0436R.string.delete_topic_confirm).setPositiveButton(C0436R.string.yes, new C06077(topic)).setNegativeButton(C0436R.string.no, null).setIcon(ACRAConstants.DEFAULT_DIALOG_ICON).show();
    }

    private void deleteTopic(BoardTopic topic) {
        new BoardDeleteTopic(getArguments().getInt("gid", 0), topic.id).setCallback(new C14408(topic)).wrapProgress(getActivity()).exec(this.contentView);
    }

    private void toggleCloseTopic(BoardTopic topic) {
        boolean isClosed;
        boolean z = false;
        if ((topic.flags & ORDER_UPDATED_DESC) > 0) {
            isClosed = true;
        } else {
            isClosed = false;
        }
        int i = getArguments().getInt("gid", 0);
        int i2 = topic.id;
        if (!isClosed) {
            z = true;
        }
        new BoardCloseTopic(i, i2, z).setCallback(new C14419(isClosed, topic)).wrapProgress(getActivity()).exec(this.contentView);
    }

    private void toggleFixTopic(BoardTopic topic) {
        boolean isFixed;
        boolean z = false;
        if ((topic.flags & ORDER_CREATED_DESC) > 0) {
            isFixed = true;
        } else {
            isFixed = false;
        }
        int i = getArguments().getInt("gid", 0);
        int i2 = topic.id;
        if (!isFixed) {
            z = true;
        }
        new BoardFixTopic(i, i2, z).setCallback(new AnonymousClass10(isFixed, topic)).wrapProgress(getActivity()).exec(this.contentView);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == ORDER_UPDATED_DESC && resultCode == ORDER_UPDATED_ASC) {
            BoardTopic topic = new BoardTopic();
            topic.id = intent.getIntExtra("id", 0);
            int currentTimeMillis = (int) (System.currentTimeMillis() / 1000);
            topic.created = currentTimeMillis;
            topic.updated = currentTimeMillis;
            topic.title = intent.getStringExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            topic.lastCommentUid = Global.uid;
            topic.numComments = ORDER_UPDATED_DESC;
            topic.creator = Global.uid;
            for (int i = 0; i < this.topics.size(); i += ORDER_UPDATED_DESC) {
                if ((((BoardTopic) this.topics.get(i)).flags & ORDER_CREATED_DESC) == 0) {
                    this.topics.add(i, topic);
                    break;
                }
            }
            updateList();
            Bundle args = new Bundle();
            args.putInt("gid", getArguments().getInt("gid", 0));
            args.putInt("tid", topic.id);
            args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, topic.title);
            Navigate.to("BoardTopicViewActivity", args, getActivity());
        }
    }

    public void onRefresh() {
        this.preloading = false;
        this.refreshing = true;
        loadData();
    }

    public void onResume() {
        super.onResume();
        updateList();
        if (this.refreshOnResume) {
            this.refreshOnResume = false;
            ((RefreshableListView) this.list).refresh();
        }
        this.imgLoader.deactivate();
    }

    public void onPause() {
        super.onPause();
        this.imgLoader.activate();
    }

    public String getLastUpdatedTime() {
        if (this.lastUpdate > 0) {
            return new StringBuilder(String.valueOf(getResources().getString(C0436R.string.updated))).append(" ").append(Global.langDateRelativeNoDiff((int) (this.lastUpdate / 1000), getResources())).toString();
        }
        return getResources().getString(C0436R.string.not_updated);
    }

    public void onScrolled(float offset) {
    }
}
