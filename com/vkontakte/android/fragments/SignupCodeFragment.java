package com.vkontakte.android.fragments;

import android.animation.LayoutTransition;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.SpannableStringBuilder;
import android.text.style.TypefaceSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.actionbarsherlock.app.SherlockFragment;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ui.Fonts;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignupCodeFragment extends SherlockFragment {
    private OnClickListener btnClickListener;
    private long initTime;
    private OnResendListener listener;
    private String phoneNumber;
    private BroadcastReceiver receiver;
    private View view;

    /* renamed from: com.vkontakte.android.fragments.SignupCodeFragment.1 */
    class C07961 extends BroadcastReceiver {
        C07961() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.provider.Telephony.SMS_RECEIVED".equals(intent.getAction())) {
                SmsMessage sms = SmsMessage.createFromPdu((byte[]) ((Object[]) intent.getExtras().get("pdus"))[0]);
                if (sms.getTimestampMillis() > SignupCodeFragment.this.initTime) {
                    Matcher m = Pattern.compile(": ([0-9a-z]+).+(?:VK|\u0412\u041a\u043e\u043d\u0442\u0430\u043a\u0442\u0435)", 40).matcher(sms.getMessageBody());
                    if (m.find()) {
                        ((TextView) SignupCodeFragment.this.view.findViewById(C0436R.id.signup_code_edit)).setText(m.group(1));
                        SignupCodeFragment.this.view.findViewById(C0436R.id.signup_btn_next).performClick();
                    }
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupCodeFragment.2 */
    class C07972 implements OnClickListener {
        C07972() {
        }

        public void onClick(View v) {
            SignupCodeFragment.this.getActivity().onBackPressed();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupCodeFragment.3 */
    class C07993 implements OnClickListener {

        /* renamed from: com.vkontakte.android.fragments.SignupCodeFragment.3.1 */
        class C07981 implements Runnable {
            C07981() {
            }

            public void run() {
                if (SignupCodeFragment.this.view != null) {
                    SignupCodeFragment.this.view.findViewById(C0436R.id.signup_code_call_btn).setVisibility(8);
                    SignupCodeFragment.this.view.findViewById(C0436R.id.signup_code_call_sent).setVisibility(0);
                }
            }
        }

        C07993() {
        }

        public void onClick(View v) {
            if (SignupCodeFragment.this.listener != null) {
                SignupCodeFragment.this.listener.resendCode(true, new C07981());
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SignupCodeFragment.4 */
    class C08004 implements OnEditorActionListener {
        C08004() {
        }

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (SignupCodeFragment.this.btnClickListener != null) {
                SignupCodeFragment.this.btnClickListener.onClick(v);
            }
            return false;
        }
    }

    public interface OnResendListener {
        void resendCode(boolean z, Runnable runnable);
    }

    private class TimeUpdater implements Runnable {

        /* renamed from: com.vkontakte.android.fragments.SignupCodeFragment.TimeUpdater.1 */
        class C08011 implements Runnable {
            C08011() {
            }

            public void run() {
                if (SignupCodeFragment.this.view != null) {
                    ((TextView) SignupCodeFragment.this.view.findViewById(C0436R.id.signup_code_text1)).setText(C0436R.string.signup_code_explain_resent);
                    ((TextView) SignupCodeFragment.this.view.findViewById(C0436R.id.signup_code_text2)).setText(C0436R.string.signup_code_call_explain);
                    SignupCodeFragment.this.view.findViewById(C0436R.id.signup_code_call_btn).setVisibility(0);
                }
            }
        }

        private TimeUpdater() {
        }

        public void run() {
            if (SignupCodeFragment.this.view != null) {
                if (60 - ((System.currentTimeMillis() - SignupCodeFragment.this.initTime) / 1000) >= 0) {
                    SpannableStringBuilder ssb = new SpannableStringBuilder();
                    ssb.append(SignupCodeFragment.this.getString(C0436R.string.signup_code_waiting));
                    ssb.append(" ");
                    Spannable sp = Factory.getInstance().newSpannable(String.format("%d:%02d", new Object[]{Long.valueOf(time / 60), Long.valueOf(time % 60)}));
                    sp.setSpan(new TypefaceSpan("sans-serif"), 0, sp.length(), 0);
                    ssb.append(sp);
                    ((TextView) SignupCodeFragment.this.view.findViewById(C0436R.id.signup_code_text2)).setText(ssb);
                    SignupCodeFragment.this.view.postDelayed(this, 500);
                    return;
                }
                SignupCodeFragment.this.listener.resendCode(false, new C08011());
            }
        }
    }

    public SignupCodeFragment() {
        this.receiver = new C07961();
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        if (b != null) {
            this.initTime = b.getLong("init_time");
        } else {
            this.initTime = System.currentTimeMillis();
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        VKApplication.context.registerReceiver(this.receiver, filter);
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
    }

    public void setNumber(String n) {
        this.phoneNumber = n;
    }

    public void setOnNextClickListener(OnClickListener l) {
        this.btnClickListener = l;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(C0436R.layout.signup_code, null);
        ((TextView) this.view.findViewById(C0436R.id.signup_code_text1)).setTypeface(Fonts.getRobotoLight());
        ((TextView) this.view.findViewById(C0436R.id.signup_code_text2)).setTypeface(Fonts.getRobotoLight());
        ((TextView) this.view.findViewById(C0436R.id.signup_code_call_sent)).setTypeface(Fonts.getRobotoLight());
        if (VERSION.SDK_INT >= 16) {
            LayoutTransition trans = new LayoutTransition();
            trans.enableTransitionType(4);
            ((ViewGroup) ((ViewGroup) this.view).getChildAt(0)).setLayoutTransition(trans);
        }
        this.view.findViewById(C0436R.id.signup_btn_next).setOnClickListener(this.btnClickListener);
        this.view.postDelayed(new TimeUpdater(), 500);
        this.view.findViewById(C0436R.id.signup_code_edit_btn).setOnClickListener(new C07972());
        ((TextView) this.view.findViewById(C0436R.id.signup_code_number)).setText(this.phoneNumber);
        this.view.findViewById(C0436R.id.signup_code_call_btn).setOnClickListener(new C07993());
        this.view.findViewById(C0436R.id.signup_code_edit).requestFocus();
        ((EditText) this.view.findViewById(C0436R.id.signup_code_edit)).setOnEditorActionListener(new C08004());
        return this.view;
    }

    public String getCode() {
        return ((TextView) this.view.findViewById(C0436R.id.signup_code_edit)).getText().toString();
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.view = null;
    }

    public void setOnResendListener(OnResendListener l) {
        this.listener = l;
    }
}
