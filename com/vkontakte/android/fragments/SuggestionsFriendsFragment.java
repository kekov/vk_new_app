package com.vkontakte.android.fragments;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.AuthorizationClient;
import com.facebook.AuthorizationClient.AuthorizationRequest;
import com.facebook.AuthorizationClient.OnCompletedListener;
import com.facebook.AuthorizationClient.Result;
import com.facebook.AuthorizationClient.StartActivityDelegate;
import com.facebook.SessionDefaultAudience;
import com.facebook.SessionLoginBehavior;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusClient.OnPeopleLoadedListener;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.Auth;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.SuggestionsActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKAlertDialog;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.AccountLookupContacts;
import com.vkontakte.android.api.FacebookGetFriends;
import com.vkontakte.android.api.FacebookGetMe;
import com.vkontakte.android.api.FriendsGetSuggestions;
import com.vkontakte.android.api.GmailGetContacts;
import com.vkontakte.android.api.GmailGetContacts.Callback;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.ui.Fonts;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.MergeAdapter;
import com.vkontakte.android.ui.MergeImageLoaderAdapter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.acra.ACRAConstants;

public class SuggestionsFriendsFragment extends SuggestionsFragment {
    private static final int FACEBOOK_RESULT = 101;
    public static final int GMAIL_ERROR_RESULT = 103;
    public static final int GPLUS_ERROR_RESULT = 102;
    private APIRequest currentReq;
    private AuthorizationClient fbAuthClient;
    private Account gmailAccount;
    private ArrayList<Item> importItems;
    private boolean importedContacts;
    private PlusClient plusClient;
    private ProgressDialog progress;

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.10 */
    class AnonymousClass10 implements OnClickListener {
        private final /* synthetic */ Account[] val$accs;

        AnonymousClass10(Account[] accountArr) {
            this.val$accs = accountArr;
        }

        public void onClick(DialogInterface dialog, int which) {
            SuggestionsFriendsFragment.this.progress.show();
            SuggestionsFriendsFragment.this.importGmail(this.val$accs[which]);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.11 */
    class AnonymousClass11 implements Runnable {
        private final /* synthetic */ Account val$acc;

        /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.11.1 */
        class C08321 implements Runnable {
            C08321() {
            }

            public void run() {
                Toast.makeText(SuggestionsFriendsFragment.this.getActivity(), C0436R.string.error, 0).show();
            }
        }

        /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.11.2 */
        class C08332 implements Runnable {
            C08332() {
            }

            public void run() {
                Toast.makeText(SuggestionsFriendsFragment.this.getActivity(), C0436R.string.err_text, 0).show();
            }
        }

        /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.11.3 */
        class C15073 implements Callback {
            private final /* synthetic */ Account val$acc;

            /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.11.3.1 */
            class C08341 implements Runnable {
                C08341() {
                }

                public void run() {
                    new Builder(SuggestionsFriendsFragment.this.getActivity()).setTitle(C0436R.string.error).setMessage(C0436R.string.no_gmail_to_import).setPositiveButton(C0436R.string.ok, null).show();
                    SuggestionsFriendsFragment.this.updateItems();
                }
            }

            C15073(Account account) {
                this.val$acc = account;
            }

            public void success(List<UserProfile> users) {
                if (users.size() != 0) {
                    SuggestionsFriendsFragment.this.doImport(users, this.val$acc.name, "email");
                } else if (SuggestionsFriendsFragment.this.getActivity() != null) {
                    SuggestionsFriendsFragment.this.progress.dismiss();
                    SuggestionsFriendsFragment.this.getActivity().runOnUiThread(new C08341());
                }
            }

            public void fail(int ecode, String emsg) {
                Log.m530w("vk", "error " + ecode + ", " + emsg);
                SuggestionsFriendsFragment.this.progress.dismiss();
                Toast.makeText(SuggestionsFriendsFragment.this.getActivity(), C0436R.string.error, 0).show();
            }
        }

        AnonymousClass11(Account account) {
            this.val$acc = account;
        }

        public void run() {
            String token = null;
            try {
                token = GoogleAuthUtil.getToken(SuggestionsFriendsFragment.this.getActivity(), this.val$acc.name, "oauth2:https://www.google.com/m8/feeds", new Bundle());
            } catch (GooglePlayServicesAvailabilityException e) {
            } catch (Throwable recoverableException) {
                Log.m532w("vk", recoverableException);
                SuggestionsFriendsFragment.this.startActivityForResult(recoverableException.getIntent(), SuggestionsFriendsFragment.GMAIL_ERROR_RESULT);
            } catch (GoogleAuthException authEx) {
                Log.m527e("vk", "Unrecoverable authentication exception: " + authEx.getMessage(), authEx);
                if (SuggestionsFriendsFragment.this.getActivity() != null) {
                    SuggestionsFriendsFragment.this.getActivity().runOnUiThread(new C08321());
                }
                SuggestionsFriendsFragment.this.progress.dismiss();
                return;
            } catch (IOException ioEx) {
                Log.m528i("vk", "transient error encountered: " + ioEx.getMessage());
                if (SuggestionsFriendsFragment.this.getActivity() != null) {
                    SuggestionsFriendsFragment.this.getActivity().runOnUiThread(new C08332());
                }
                SuggestionsFriendsFragment.this.progress.dismiss();
                return;
            }
            if (token != null) {
                new GmailGetContacts(token, this.val$acc.name).setCallback(new C15073(this.val$acc)).exec(SuggestionsFriendsFragment.this.getActivity());
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.2 */
    class C08362 implements OnClickListener {
        C08362() {
        }

        public void onClick(DialogInterface dialog, int which) {
            if (SuggestionsFriendsFragment.this.getActivity() != null) {
                SuggestionsFriendsFragment.this.getActivity().getSharedPreferences(null, 0).edit().putBoolean("agreed_import_contacts", true).commit();
                SuggestionsFriendsFragment.this.importedContacts = true;
                SuggestionsFriendsFragment.this.importContacts();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.3 */
    class C08373 implements OnClickListener {
        C08373() {
        }

        public void onClick(DialogInterface dialog, int which) {
            SuggestionsFriendsFragment.this.updateItems();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.4 */
    class C08384 implements OnCancelListener {
        C08384() {
        }

        public void onCancel(DialogInterface dialog) {
            SuggestionsFriendsFragment.this.updateItems();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.5 */
    class C08405 implements Runnable {

        /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.5.1 */
        class C08391 implements Runnable {
            C08391() {
            }

            public void run() {
                new Builder(SuggestionsFriendsFragment.this.getActivity()).setTitle(C0436R.string.error).setMessage(C0436R.string.no_contacts_to_import).setPositiveButton(C0436R.string.ok, null).show();
                SuggestionsFriendsFragment.this.updateItems();
            }
        }

        C08405() {
        }

        public void run() {
            ArrayList<UserProfile> numbers = new ArrayList();
            ContentResolver resolver = SuggestionsFriendsFragment.this.getActivity().getContentResolver();
            Cursor cursor = null;
            ArrayList<Long> needRawContacts = new ArrayList();
            ArrayList<Long> needContacts = new ArrayList();
            HashMap<Long, Long> rawToContacts = new HashMap();
            HashMap<Long, String> names = new HashMap();
            HashMap<Long, String> photos = new HashMap();
            try {
                cursor = resolver.query(Data.CONTENT_URI, new String[]{"data1", "raw_contact_id"}, "mimetype='vnd.android.cursor.item/phone_v2' AND data2=2", null, null);
                int i;
                Iterator<UserProfile> itr;
                Iterator it;
                UserProfile p;
                Long contactId;
                if (cursor == null || cursor.getCount() == 0) {
                    Log.m530w("vk", "cursor2.getCount = 0");
                    if (cursor != null) {
                        cursor.close();
                    }
                    cursor = null;
                    try {
                        cursor = resolver.query(RawContacts.CONTENT_URI, new String[]{"_id", "contact_id"}, "_id IN (" + TextUtils.join(",", needRawContacts) + ") AND " + "account_type" + "<>'" + Auth.ACCOUNT_TYPE + "'", null, null);
                        if (cursor != null || cursor.getCount() == 0) {
                            Log.m530w("vk", "cursor2.getCount = 0");
                            if (cursor != null) {
                                cursor.close();
                            }
                            cursor = null;
                            try {
                                cursor = resolver.query(Contacts.CONTENT_URI, VERSION.SDK_INT < 11 ? new String[]{"_id", "display_name"} : new String[]{"_id", "display_name", "photo_thumb_uri"}, "_id IN (" + TextUtils.join(",", needContacts) + ")", null, null);
                                if (cursor != null || cursor.getCount() == 0) {
                                    Log.m530w("vk", "cursor2.getCount = 0");
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    i = 0;
                                    itr = numbers.iterator();
                                    it = numbers.iterator();
                                    while (it.hasNext()) {
                                        p = (UserProfile) it.next();
                                        contactId = (Long) rawToContacts.get(needRawContacts.get(i));
                                        p.fullName = (String) names.get(contactId);
                                        p.photo = photos.containsKey(contactId) ? (String) photos.get(contactId) : null;
                                        i++;
                                    }
                                    if (numbers.size() != 0) {
                                        SuggestionsFriendsFragment.this.doImport(numbers, null, "phone");
                                    } else if (SuggestionsFriendsFragment.this.getActivity() == null) {
                                        SuggestionsFriendsFragment.this.progress.dismiss();
                                        SuggestionsFriendsFragment.this.getActivity().runOnUiThread(new C08391());
                                    }
                                }
                                cursor.moveToFirst();
                                do {
                                    names.put(Long.valueOf(cursor.getLong(0)), cursor.getString(1));
                                    if (cursor.getColumnCount() > 2 && cursor.getString(2) != null) {
                                        photos.put(Long.valueOf(cursor.getLong(0)), cursor.getString(2));
                                    }
                                } while (cursor.moveToNext());
                                if (cursor != null) {
                                    cursor.close();
                                }
                                i = 0;
                                itr = numbers.iterator();
                                it = numbers.iterator();
                                while (it.hasNext()) {
                                    p = (UserProfile) it.next();
                                    contactId = (Long) rawToContacts.get(needRawContacts.get(i));
                                    p.fullName = (String) names.get(contactId);
                                    if (photos.containsKey(contactId)) {
                                    }
                                    p.photo = photos.containsKey(contactId) ? (String) photos.get(contactId) : null;
                                    i++;
                                }
                                if (numbers.size() != 0) {
                                    SuggestionsFriendsFragment.this.doImport(numbers, null, "phone");
                                } else if (SuggestionsFriendsFragment.this.getActivity() == null) {
                                    SuggestionsFriendsFragment.this.progress.dismiss();
                                    SuggestionsFriendsFragment.this.getActivity().runOnUiThread(new C08391());
                                }
                            } catch (Exception e) {
                            }
                        } else {
                            cursor.moveToFirst();
                            do {
                                rawToContacts.put(Long.valueOf(cursor.getLong(0)), Long.valueOf(cursor.getLong(1)));
                                needContacts.add(Long.valueOf(cursor.getLong(1)));
                            } while (cursor.moveToNext());
                            if (cursor != null) {
                                cursor.close();
                            }
                            cursor = null;
                            if (VERSION.SDK_INT < 11) {
                            }
                            cursor = resolver.query(Contacts.CONTENT_URI, VERSION.SDK_INT < 11 ? new String[]{"_id", "display_name"} : new String[]{"_id", "display_name", "photo_thumb_uri"}, "_id IN (" + TextUtils.join(",", needContacts) + ")", null, null);
                            if (cursor != null) {
                            }
                            Log.m530w("vk", "cursor2.getCount = 0");
                            if (cursor != null) {
                                cursor.close();
                            }
                            i = 0;
                            itr = numbers.iterator();
                            it = numbers.iterator();
                            while (it.hasNext()) {
                                p = (UserProfile) it.next();
                                contactId = (Long) rawToContacts.get(needRawContacts.get(i));
                                p.fullName = (String) names.get(contactId);
                                if (photos.containsKey(contactId)) {
                                }
                                p.photo = photos.containsKey(contactId) ? (String) photos.get(contactId) : null;
                                i++;
                            }
                            if (numbers.size() != 0) {
                                SuggestionsFriendsFragment.this.doImport(numbers, null, "phone");
                            } else if (SuggestionsFriendsFragment.this.getActivity() == null) {
                                SuggestionsFriendsFragment.this.progress.dismiss();
                                SuggestionsFriendsFragment.this.getActivity().runOnUiThread(new C08391());
                            }
                        }
                    } catch (Exception e2) {
                    }
                } else {
                    cursor.moveToFirst();
                    do {
                        String n = cursor.getString(cursor.getColumnIndex("data1"));
                        UserProfile u = new UserProfile();
                        u.extra = n;
                        needRawContacts.add(Long.valueOf(cursor.getLong(1)));
                        numbers.add(u);
                    } while (cursor.moveToNext());
                    if (cursor != null) {
                        cursor.close();
                    }
                    cursor = null;
                    cursor = resolver.query(RawContacts.CONTENT_URI, new String[]{"_id", "contact_id"}, "_id IN (" + TextUtils.join(",", needRawContacts) + ") AND " + "account_type" + "<>'" + Auth.ACCOUNT_TYPE + "'", null, null);
                    if (cursor != null) {
                    }
                    Log.m530w("vk", "cursor2.getCount = 0");
                    if (cursor != null) {
                        cursor.close();
                    }
                    cursor = null;
                    if (VERSION.SDK_INT < 11) {
                    }
                    cursor = resolver.query(Contacts.CONTENT_URI, VERSION.SDK_INT < 11 ? new String[]{"_id", "display_name"} : new String[]{"_id", "display_name", "photo_thumb_uri"}, "_id IN (" + TextUtils.join(",", needContacts) + ")", null, null);
                    if (cursor != null) {
                    }
                    Log.m530w("vk", "cursor2.getCount = 0");
                    if (cursor != null) {
                        cursor.close();
                    }
                    i = 0;
                    itr = numbers.iterator();
                    it = numbers.iterator();
                    while (it.hasNext()) {
                        p = (UserProfile) it.next();
                        contactId = (Long) rawToContacts.get(needRawContacts.get(i));
                        p.fullName = (String) names.get(contactId);
                        if (photos.containsKey(contactId)) {
                        }
                        p.photo = photos.containsKey(contactId) ? (String) photos.get(contactId) : null;
                        i++;
                    }
                    if (numbers.size() != 0) {
                        SuggestionsFriendsFragment.this.doImport(numbers, null, "phone");
                    } else if (SuggestionsFriendsFragment.this.getActivity() == null) {
                        SuggestionsFriendsFragment.this.progress.dismiss();
                        SuggestionsFriendsFragment.this.getActivity().runOnUiThread(new C08391());
                    }
                }
            } catch (Exception e3) {
            }
        }
    }

    private class ImportTypesAdapter extends BaseAdapter {
        private ImportTypesAdapter() {
        }

        public int getCount() {
            return SuggestionsFriendsFragment.this.importItems.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return (long) (((Item) SuggestionsFriendsFragment.this.importItems.get(position)).type + 2000000001);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = View.inflate(SuggestionsFriendsFragment.this.getActivity(), C0436R.layout.suggest_list_item, null);
                ((TextView) view.findViewById(C0436R.id.flist_item_subtext)).setTypeface(Fonts.getRobotoLight());
                view.findViewById(C0436R.id.flist_item_online).setVisibility(8);
            }
            if (position == 0) {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_top);
            } else if (position == SuggestionsFriendsFragment.this.importItems.size() - 1) {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_btm);
            } else {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_mid);
            }
            Item item = (Item) SuggestionsFriendsFragment.this.importItems.get(position);
            ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(item.titleRes);
            ((TextView) view.findViewById(C0436R.id.flist_item_subtext)).setText(item.descRes);
            ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(item.imgRes);
            return view;
        }
    }

    private class Item {
        int descRes;
        int imgRes;
        int titleRes;
        int type;

        private Item() {
        }
    }

    private class TitleItemAdapter extends BaseAdapter {
        private TitleItemAdapter() {
        }

        public int getCount() {
            return (SuggestionsFriendsFragment.this.users.size() <= 0 || SuggestionsFriendsFragment.this.getArguments().getBoolean("from_signup")) ? 0 : 1;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public boolean isEnabled(int item) {
            return false;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView != null) {
                return convertView;
            }
            TextView txt = (TextView) View.inflate(SuggestionsFriendsFragment.this.getActivity(), C0436R.layout.list_cards_section_header, null);
            txt.setText(SuggestionsFriendsFragment.this.getString(C0436R.string.suggest_friends).toUpperCase());
            return txt;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.14 */
    class AnonymousClass14 implements FacebookGetMe.Callback {
        private final /* synthetic */ String val$token;

        /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.14.1 */
        class C15081 implements FacebookGetFriends.Callback {
            private final /* synthetic */ String val$myId;

            /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.14.1.1 */
            class C08351 implements Runnable {
                C08351() {
                }

                public void run() {
                    new Builder(SuggestionsFriendsFragment.this.getActivity()).setTitle(C0436R.string.error).setMessage(C0436R.string.no_facebook_to_import).setPositiveButton(C0436R.string.ok, null).show();
                    SuggestionsFriendsFragment.this.updateItems();
                }
            }

            C15081(String str) {
                this.val$myId = str;
            }

            public void success(List<UserProfile> friendIds) {
                if (friendIds.size() != 0) {
                    SuggestionsFriendsFragment.this.doImport(friendIds, this.val$myId, "facebook");
                } else if (SuggestionsFriendsFragment.this.getActivity() != null) {
                    SuggestionsFriendsFragment.this.progress.dismiss();
                    SuggestionsFriendsFragment.this.getActivity().runOnUiThread(new C08351());
                }
            }

            public void fail(int ecode, String emsg) {
                Log.m530w("vk", "error " + ecode + ", " + emsg);
                SuggestionsFriendsFragment.this.progress.dismiss();
                Toast.makeText(SuggestionsFriendsFragment.this.getActivity(), C0436R.string.error, 0).show();
            }
        }

        AnonymousClass14(String str) {
            this.val$token = str;
        }

        public void success(String myId) {
            new FacebookGetFriends(this.val$token).setCallback(new C15081(myId)).exec(SuggestionsFriendsFragment.this.getActivity());
        }

        public void fail(int ecode, String emsg) {
            Log.m530w("vk", "error " + ecode + ", " + emsg);
            SuggestionsFriendsFragment.this.progress.dismiss();
            Toast.makeText(SuggestionsFriendsFragment.this.getActivity(), C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.1 */
    class C15091 implements FriendsGetSuggestions.Callback {
        C15091() {
        }

        public void success(ArrayList<UserProfile> _users) {
            SuggestionsFriendsFragment.this.currentReq = null;
            SuggestionsFriendsFragment.this.users.clear();
            SuggestionsFriendsFragment.this.users.addAll(_users);
            SuggestionsFriendsFragment.this.updateItems();
            SuggestionsFriendsFragment.this.updateList();
            SuggestionsFriendsFragment.this.progress.dismiss();
        }

        public void fail(int ecode, String emsg) {
            SuggestionsFriendsFragment.this.currentReq = null;
            if (SuggestionsFriendsFragment.this.getActivity() != null) {
                SuggestionsFriendsFragment.this.progress.dismiss();
                SuggestionsFriendsFragment.this.onError(ecode, emsg);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.6 */
    class C15106 implements AccountLookupContacts.Callback {
        private final /* synthetic */ String val$service;
        private final /* synthetic */ List val$usrs;

        C15106(List list, String str) {
            this.val$usrs = list;
            this.val$service = str;
        }

        public void success(ArrayList<UserProfile> found, ArrayList<UserProfile> other) {
            for (int i = 0; i < other.size(); i++) {
                Iterator it;
                String id = ((UserProfile) other.get(i)).extra;
                for (UserProfile p : this.val$usrs) {
                    if (id.equals(p.extra)) {
                        other.set(i, p);
                        break;
                    }
                }
            }
            SuggestionsFriendsFragment.this.progress.dismiss();
            int srv = -1;
            String title = ACRAConstants.DEFAULT_STRING_VALUE;
            if ("phone".equals(this.val$service)) {
                srv = 0;
                title = SuggestionsFriendsFragment.this.getString(C0436R.string.suggest_type_contacts);
            } else if ("google".equals(this.val$service)) {
                srv = 1;
                title = SuggestionsFriendsFragment.this.getString(C0436R.string.suggest_type_google);
            } else if ("facebook".equals(this.val$service)) {
                srv = 2;
                title = SuggestionsFriendsFragment.this.getString(C0436R.string.suggest_type_facebook);
            } else if ("email".equals(this.val$service)) {
                srv = 3;
                title = SuggestionsFriendsFragment.this.getString(C0436R.string.suggest_type_gmail);
            }
            Friends.saveImportedContacts(srv, found, other);
            if ("phone".equals(this.val$service) && SuggestionsFriendsFragment.this.getArguments().getBoolean("from_signup")) {
                UserProfile user;
                it = found.iterator();
                while (it.hasNext()) {
                    user = (UserProfile) it.next();
                    if (!user.isFriend) {
                        SuggestionsFriendsFragment.this.users.add(user);
                    }
                }
                it = found.iterator();
                while (it.hasNext()) {
                    user = (UserProfile) it.next();
                    if (user.isFriend) {
                        SuggestionsFriendsFragment.this.users.add(user);
                    }
                }
                SuggestionsFriendsFragment.this.updateList();
                SuggestionsFriendsFragment.this.importedContacts = true;
                SuggestionsFriendsFragment.this.updateItems();
            } else if (found.size() != 0 || other.size() != 0 || srv == 0 || srv == 3) {
                Bundle args = new Bundle();
                args.putInt("service", srv);
                args.putString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, title);
                Navigate.to("SuggestionsImportedFragment", args, SuggestionsFriendsFragment.this.getActivity());
            } else {
                Toast.makeText(SuggestionsFriendsFragment.this.getActivity(), C0436R.string.nothing_found, 0).show();
            }
        }

        public void fail(int ecode, String emsg) {
            SuggestionsFriendsFragment.this.progress.dismiss();
            Toast.makeText(SuggestionsFriendsFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
            if ("phone".equals(this.val$service)) {
                SuggestionsFriendsFragment.this.importedContacts = false;
            }
            SuggestionsFriendsFragment.this.updateItems();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.7 */
    class C15117 implements ConnectionCallbacks {
        C15117() {
        }

        public void onConnected(Bundle arg0) {
            SuggestionsFriendsFragment.this.importGoogle();
        }

        public void onDisconnected() {
            SuggestionsFriendsFragment.this.progress.dismiss();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.8 */
    class C15128 implements OnConnectionFailedListener {
        C15128() {
        }

        public void onConnectionFailed(ConnectionResult result) {
            SuggestionsFriendsFragment.this.progress.dismiss();
            if (result.hasResolution()) {
                try {
                    result.startResolutionForResult(SuggestionsFriendsFragment.this.getActivity(), SuggestionsFriendsFragment.GPLUS_ERROR_RESULT);
                } catch (SendIntentException e) {
                    SuggestionsFriendsFragment.this.plusClient.connect();
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsFriendsFragment.9 */
    class C15139 implements OnPeopleLoadedListener {
        C15139() {
        }

        public void onPeopleLoaded(ConnectionResult status, PersonBuffer personBuffer, String nextPage) {
            if (status.getErrorCode() == 0) {
                try {
                    ArrayList<UserProfile> ids = new ArrayList();
                    Iterator it = personBuffer.iterator();
                    while (it.hasNext()) {
                        Person p = (Person) it.next();
                        UserProfile user = new UserProfile();
                        user.extra = p.getId();
                        user.fullName = p.getDisplayName();
                        user.photo = p.getImage().getUrl().replace("sz=50", "sz=" + Global.scale(BitmapDescriptorFactory.HUE_YELLOW));
                        ids.add(user);
                    }
                    SuggestionsFriendsFragment.this.doImport(ids, SuggestionsFriendsFragment.this.plusClient.getCurrentPerson().getId(), "google");
                    personBuffer.close();
                } catch (Throwable th) {
                    SuggestionsFriendsFragment.this.progress.dismiss();
                } finally {
                    personBuffer.close();
                }
            } else {
                Log.m526e("vk", "Error listing people: " + status.getErrorCode());
                Toast.makeText(SuggestionsFriendsFragment.this.getActivity(), C0436R.string.error, 0).show();
                SuggestionsFriendsFragment.this.progress.dismiss();
                if (status.hasResolution()) {
                    try {
                        status.startResolutionForResult(SuggestionsFriendsFragment.this.getActivity(), SuggestionsFriendsFragment.GPLUS_ERROR_RESULT);
                    } catch (SendIntentException e) {
                        SuggestionsFriendsFragment.this.plusClient.connect();
                    }
                }
            }
        }
    }

    private class ImportTypesImageAdapter extends ListImageLoaderAdapter {
        private ImportTypesImageAdapter() {
        }

        public int getItemCount() {
            return SuggestionsFriendsFragment.this.importItems.size();
        }

        public int getImageCountForItem(int item) {
            return 0;
        }

        public String getImageURL(int item, int image) {
            return null;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
        }
    }

    private class TitleItemImageLoaderAdapter extends ListImageLoaderAdapter {
        private TitleItemImageLoaderAdapter() {
        }

        public int getItemCount() {
            return (SuggestionsFriendsFragment.this.users.size() <= 0 || SuggestionsFriendsFragment.this.getArguments().getBoolean("from_signup")) ? 0 : 1;
        }

        public int getImageCountForItem(int item) {
            return 0;
        }

        public String getImageURL(int item, int image) {
            return null;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
        }
    }

    public SuggestionsFriendsFragment() {
        this.importedContacts = false;
        this.importItems = new ArrayList();
    }

    private void updateItems() {
        this.importItems.clear();
        Item item = new Item();
        if (!this.importedContacts || (this.importedContacts && this.users.size() == 0)) {
            item.titleRes = C0436R.string.suggest_type_contacts;
            int i = (this.importedContacts && this.users.size() == 0) ? C0436R.string.no_contacts_found : C0436R.string.suggest_type_contacts_desc;
            item.descRes = i;
            item.imgRes = C0436R.drawable.ic_registration_contacts;
            item.type = 0;
            this.importItems.add(item);
        }
        item = new Item();
        item.titleRes = C0436R.string.suggest_type_gmail;
        item.descRes = C0436R.string.suggest_type_gmail_desc;
        item.imgRes = C0436R.drawable.ic_registration_gmail;
        item.type = 3;
        this.importItems.add(item);
        item = new Item();
        item.titleRes = C0436R.string.suggest_type_facebook;
        item.descRes = C0436R.string.suggest_type_facebook_desc;
        item.imgRes = C0436R.drawable.ic_registration_facebook;
        item.type = 2;
        this.importItems.add(item);
        item = new Item();
        item.titleRes = C0436R.string.suggest_type_ext_search;
        item.descRes = C0436R.string.suggest_type_ext_search_desc;
        item.imgRes = C0436R.drawable.ic_registration_search;
        item.type = 4;
        this.importItems.add(item);
        updateList();
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        this.progress = new ProgressDialog(act);
        this.progress.setCancelable(false);
        this.progress.setMessage(getResources().getString(C0436R.string.loading));
        if (!(getActivity() instanceof SuggestionsActivity)) {
            act.setTitle(C0436R.string.find_friends);
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        if (getArguments().getBoolean("from_signup") && !this.importedContacts) {
            startImportContacts();
        }
    }

    protected void loadData() {
        if (!getArguments().getBoolean("from_signup")) {
            this.currentReq = new FriendsGetSuggestions(!getArguments().getBoolean("from_signup")).setCallback(new C15091()).exec(getActivity());
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.currentReq != null) {
            this.currentReq.cancel();
            this.currentReq = null;
        }
        if (!getArguments().getBoolean("from_signup")) {
            Friends.reload(true);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        if (getArguments().getBoolean("from_signup")) {
            updateList();
        }
        return v;
    }

    protected BaseAdapter getAdapter() {
        MergeAdapter m = new MergeAdapter();
        if (!getArguments().getBoolean("from_signup")) {
            m.addAdapter(new ImportTypesAdapter());
        }
        m.addAdapter(new TitleItemAdapter());
        m.addAdapter(super.getAdapter());
        if (getArguments().getBoolean("from_signup")) {
            m.addAdapter(new ImportTypesAdapter());
        }
        return m;
    }

    protected ListImageLoaderAdapter getImageLoaderAdapter() {
        MergeImageLoaderAdapter m = new MergeImageLoaderAdapter();
        if (!getArguments().getBoolean("from_signup")) {
            m.addAdapter(new ImportTypesImageAdapter());
        }
        m.addAdapter(new TitleItemImageLoaderAdapter());
        m.addAdapter(super.getImageLoaderAdapter());
        if (getArguments().getBoolean("from_signup")) {
            m.addAdapter(new ImportTypesImageAdapter());
        }
        return m;
    }

    protected void onItemClick(int pos, long id, Object item) {
        if (id > 2000000000) {
            switch ((int) (id - 2000000000)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    startImportContacts();
                    return;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    startImportGoogle();
                    return;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    startImportFacebook();
                    return;
                case UserListView.TYPE_FAVE /*4*/:
                    startImportGmail();
                    return;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    openSearch();
                    return;
                default:
                    return;
            }
        }
        Bundle args = new Bundle();
        args.putInt("id", (int) id);
        Navigate.to("ProfileFragment", args, getActivity());
    }

    private void openSearch() {
        Navigate.to("BrowseUsersFragment", new Bundle(), getActivity());
    }

    protected String getListTitle() {
        return null;
    }

    private void startImportContacts() {
        if (!getActivity().getSharedPreferences(null, 0).getBoolean("agreed_import_contacts", false) || getArguments().getBoolean("from_signup")) {
            new VKAlertDialog.Builder(getActivity()).setTitle(C0436R.string.confirm).setMessage(C0436R.string.suggest_contacts_confirm).setPositiveButton(C0436R.string.yes, new C08362()).setNegativeButton(C0436R.string.no, new C08373()).setOnCancelListener(new C08384()).show();
            return;
        }
        this.importedContacts = true;
        importContacts();
    }

    private void importContacts() {
        this.progress.show();
        new Thread(new C08405()).start();
    }

    private void doImport(List<UserProfile> usrs, String myId, String service) {
        ArrayList<String> ids = new ArrayList();
        for (UserProfile u : usrs) {
            ids.add((String) u.extra);
        }
        new AccountLookupContacts(ids, service, myId).setCallback(new C15106(usrs, service)).forceHTTPS(true).exec(getActivity());
    }

    private void startImportGoogle() {
        this.progress.show();
        this.plusClient = new PlusClient.Builder(getActivity(), new C15117(), new C15128()).build();
        this.plusClient.connect();
    }

    private void importGoogle() {
        this.plusClient.loadPeople(new C15139(), 0);
    }

    private void startImportGmail() {
        Account[] accs = AccountManager.get(getActivity()).getAccountsByType(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
        if (accs.length == 0) {
            new Builder(getActivity()).setTitle(C0436R.string.error).setMessage(C0436R.string.no_google_accounts).setPositiveButton(C0436R.string.ok, null).show();
        } else if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()) != 0) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.gms")));
        } else if (accs.length == 1) {
            this.progress.show();
            importGmail(accs[0]);
        } else {
            ArrayList<String> items = new ArrayList();
            for (Account acc : accs) {
                items.add(acc.name);
            }
            new Builder(getActivity()).setTitle(C0436R.string.import_gmail_select_account).setItems((CharSequence[]) items.toArray(new String[0]), new AnonymousClass10(accs)).show();
        }
    }

    private void importGmail(Account acc) {
        this.gmailAccount = acc;
        new Thread(new AnonymousClass11(acc)).start();
    }

    private void startImportFacebook() {
        StartActivityDelegate delegate = new StartActivityDelegate() {
            public void startActivityForResult(Intent intent, int requestCode) {
                SuggestionsFriendsFragment.this.startActivityForResult(intent, requestCode);
            }

            public Activity getActivityContext() {
                return SuggestionsFriendsFragment.this.getActivity();
            }
        };
        this.progress.show();
        AuthorizationRequest req = new AuthorizationRequest(SessionLoginBehavior.SSO_WITH_FALLBACK, FACEBOOK_RESULT, false, new ArrayList(), SessionDefaultAudience.NONE, "529428070481023", null, delegate, UUID.randomUUID().toString());
        AuthorizationClient client = new AuthorizationClient();
        client.setContext(VKApplication.context);
        client.setOnCompletedListener(new OnCompletedListener() {
            public void onCompleted(Result result) {
                Log.m528i("vk", "Got Result, error=" + result.errorCode + "/" + result.errorMessage + ", token=" + result.token);
                if (result.token != null) {
                    SuggestionsFriendsFragment.this.importFacebook(result.token);
                } else {
                    SuggestionsFriendsFragment.this.progress.dismiss();
                }
            }
        });
        client.authorize(req);
        this.fbAuthClient = client;
    }

    private void importFacebook(String token) {
        new FacebookGetMe(token).setCallback(new AnonymousClass14(token)).exec(getActivity());
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode == FACEBOOK_RESULT && this.fbAuthClient != null) {
            this.fbAuthClient.onActivityResult(reqCode, resCode, data);
        }
        if (reqCode == GPLUS_ERROR_RESULT && resCode == -1) {
            this.plusClient.connect();
        }
        if (reqCode != GMAIL_ERROR_RESULT) {
            return;
        }
        if (resCode == -1) {
            importGmail(this.gmailAccount);
        } else {
            this.progress.dismiss();
        }
    }
}
