package com.vkontakte.android.fragments;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.widget.ExploreByTouchHelper;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnCloseListener;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.ActivityUtils;
import com.vkontakte.android.AudioFile;
import com.vkontakte.android.AudioPlayerActivity;
import com.vkontakte.android.AudioPlayerService;
import com.vkontakte.android.AudioPlaylist;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.NetworkStateReceiver;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.AudioGet;
import com.vkontakte.android.api.AudioGetAlbums;
import com.vkontakte.android.api.AudioGetAlbums.Callback;
import com.vkontakte.android.api.AudioGetRecommendations;
import com.vkontakte.android.api.AudioSearch;
import com.vkontakte.android.cache.AlbumArtRetriever;
import com.vkontakte.android.cache.AlbumArtRetriever.ImageLoadCallback;
import com.vkontakte.android.cache.AudioCache;
import com.vkontakte.android.mediapicker.gl.GLFilterContext;
import com.vkontakte.android.ui.MultiSectionAdapter;
import com.vkontakte.android.ui.PhotoView;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.acra.ACRAConstants;

public class AudioListFragment extends BaseListFragment {
    private AudioListAdapter adapter;
    private boolean animatingTransitionOut;
    private ArrayList<AudioFile> audios;
    private int currentPlaylist;
    private APIRequest currentRequest;
    private ArrayList<AudioFile> displayList;
    private APIRequest errorRequest;
    private ArrayList<AudioFile> localSearchResults;
    private ArrayAdapter<String> navAdapter;
    private AudioFile nowPlaying;
    private View playerBar;
    private ArrayList<AudioPlaylist> playlists;
    private BroadcastReceiver receiver;
    private Runnable searchLoader;
    private ArrayList<AudioFile> searchResults;
    private SearchView searchView;
    private boolean searching;
    private boolean select;
    private int uid;

    /* renamed from: com.vkontakte.android.fragments.AudioListFragment.16 */
    class AnonymousClass16 implements AnimatorListener {
        private final /* synthetic */ View val$v;

        AnonymousClass16(View view) {
            this.val$v = view;
        }

        public void onAnimationStart(Animator animation) {
        }

        public void onAnimationRepeat(Animator animation) {
        }

        public void onAnimationEnd(Animator animation) {
            this.val$v.findViewById(C0436R.id.audio_play_icon).setTranslationX(0.0f);
            this.val$v.findViewById(C0436R.id.audio_artist).setTranslationX(0.0f);
            this.val$v.findViewById(C0436R.id.audio_title).setTranslationX(0.0f);
            this.val$v.findViewById(C0436R.id.audio_play_icon).setVisibility(8);
            AudioListFragment.this.animatingTransitionOut = false;
        }

        public void onAnimationCancel(Animator animation) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioListFragment.1 */
    class C05781 extends BroadcastReceiver {

        /* renamed from: com.vkontakte.android.fragments.AudioListFragment.1.1 */
        class C14311 implements ImageLoadCallback {

            /* renamed from: com.vkontakte.android.fragments.AudioListFragment.1.1.1 */
            class C05751 implements Runnable {
                private final /* synthetic */ Bitmap val$bmp;

                /* renamed from: com.vkontakte.android.fragments.AudioListFragment.1.1.1.1 */
                class C05741 implements Runnable {
                    private final /* synthetic */ Bitmap val$bmp;
                    private final /* synthetic */ ViewFlipper val$sw;

                    C05741(ViewFlipper viewFlipper, Bitmap bitmap) {
                        this.val$sw = viewFlipper;
                        this.val$bmp = bitmap;
                    }

                    public void run() {
                        ((ImageView) this.val$sw.getCurrentView().findViewById(C0436R.id.audio_panel_cover)).setImageBitmap(this.val$bmp);
                    }
                }

                C05751(Bitmap bitmap) {
                    this.val$bmp = bitmap;
                }

                public void run() {
                    ViewFlipper sw = (ViewFlipper) AudioListFragment.this.playerBar.findViewById(C0436R.id.audio_panel_switcher);
                    sw.postDelayed(new C05741(sw, this.val$bmp), 500);
                }
            }

            C14311() {
            }

            public void onImageLoaded(Bitmap bmp, int oid, int aid) {
                AudioListFragment.this.playerBar.post(new C05751(bmp));
            }

            public void notAvailable(int oid, int aid) {
            }
        }

        C05781() {
        }

        public void onReceive(Context context, Intent intent) {
            AudioFile file;
            if (AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS.equals(intent.getAction())) {
                if (AudioPlayerService.sharedInstance != null) {
                    file = AudioPlayerService.sharedInstance.getCurrentFile();
                    if (file != null) {
                        int i;
                        if (file.equals(AudioListFragment.this.nowPlaying)) {
                            int i2;
                            AudioListFragment.this.adapter.notifyDataSetChanged();
                            View findViewById = ((ViewFlipper) AudioListFragment.this.playerBar.findViewById(C0436R.id.audio_panel_switcher)).getCurrentView().findViewById(C0436R.id.audio_panel_progress);
                            if (AudioPlayerService.sharedInstance.initing) {
                                i2 = 0;
                            } else {
                                i2 = 8;
                            }
                            findViewById.setVisibility(i2);
                        } else {
                            AudioFile f;
                            int animOut = -1;
                            int animIn = -1;
                            int i3 = 0;
                            int j = 0;
                            while (j < AudioListFragment.this.adapter.getCount()) {
                                f = (AudioFile) AudioListFragment.this.adapter.getItem(j);
                                if (f != null && f.equals(AudioListFragment.this.nowPlaying)) {
                                    animOut = i3;
                                    break;
                                } else {
                                    i3++;
                                    j++;
                                }
                            }
                            i3 = 0;
                            j = 0;
                            while (j < AudioListFragment.this.adapter.getCount()) {
                                f = (AudioFile) AudioListFragment.this.adapter.getItem(j);
                                if (f != null && f.equals(file)) {
                                    animIn = i3;
                                    break;
                                } else {
                                    i3++;
                                    j++;
                                }
                            }
                            boolean fwd = animIn == -1 || animOut == -1 || animOut < animIn;
                            if (AudioListFragment.this.playerBar.getVisibility() == 0) {
                                AudioListFragment.this.animateBottomBar(file, fwd);
                            } else {
                                AudioListFragment.this.showBottomBar(file);
                            }
                            AudioListFragment.this.nowPlaying = file;
                            AudioListFragment.this.animateStateTransition(animIn, true);
                            AudioListFragment.this.animateStateTransition(animOut, false);
                        }
                        ImageView imageView = (ImageView) AudioListFragment.this.playerBar.findViewById(C0436R.id.audio_panel_play);
                        if (AudioPlayerService.sharedInstance.isPlaying()) {
                            i = C0436R.drawable.ic_audio_panel_pause;
                        } else {
                            i = C0436R.drawable.ic_audio_panel_play;
                        }
                        imageView.setImageResource(i);
                    }
                }
            } else if (AudioCache.ACTION_ALBUM_ART_AVAILABLE.equals(intent.getAction())) {
                aid = intent.getIntExtra("aid", 0);
                int oid = intent.getIntExtra("oid", 0);
                Log.m525d("vk", "Album art available " + aid + "_" + oid);
                if (AudioPlayerService.sharedInstance != null && AudioPlayerService.sharedInstance.getCurrentFile() != null) {
                    AudioFile cur = AudioPlayerService.sharedInstance.getCurrentFile();
                    if (cur.aid == aid && cur.oid == oid) {
                        AlbumArtRetriever.getCoverImage(aid, oid, 1, new C14311());
                    }
                }
            } else if (AudioCache.ACTION_FILE_ADDED.equals(intent.getAction())) {
                if (AudioListFragment.this.uid == Global.uid) {
                    AudioFile af = (AudioFile) intent.getParcelableExtra("file");
                    AudioListFragment.this.audios.add(0, af);
                    if (AudioListFragment.this.currentPlaylist == 0) {
                        AudioListFragment.this.displayList.add(0, af);
                        AudioListFragment.this.adapter.notifyDataSetChanged();
                    }
                }
            } else if (AudioCache.ACTION_FILE_DELETED.equals(intent.getAction())) {
                if (AudioListFragment.this.uid == Global.uid) {
                    aid = intent.getIntExtra("aid", 0);
                    Iterator it = AudioListFragment.this.audios.iterator();
                    while (it.hasNext()) {
                        file = (AudioFile) it.next();
                        if (file.aid == aid) {
                            AudioListFragment.this.audios.remove(file);
                            AudioListFragment.this.displayList.remove(file);
                            AudioListFragment.this.adapter.notifyDataSetChanged();
                            return;
                        }
                    }
                }
            } else if (AudioPlayerService.ACTION_SERVICE_STOPPING.equals(intent.getAction())) {
                AudioListFragment.this.playerBar.setVisibility(8);
                AudioListFragment.this.adapter.notifyDataSetChanged();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioListFragment.2 */
    class C05802 implements OnItemClickListener {
        C05802() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            int[] p = AudioListFragment.this.adapter.resolveIndex(pos);
            int section = p[0];
            int position = p[1];
            AudioFile file = null;
            if (AudioListFragment.this.searching) {
                if (section == 0) {
                    file = (AudioFile) AudioListFragment.this.localSearchResults.get(position);
                }
                if (section == 1) {
                    file = (AudioFile) AudioListFragment.this.searchResults.get(position);
                }
            } else {
                file = (AudioFile) AudioListFragment.this.displayList.get(position);
            }
            if (AudioListFragment.this.select) {
                Intent result = new Intent();
                result.putExtra("audio", file);
                AudioListFragment.this.getActivity().setResult(-1, result);
                AudioListFragment.this.getActivity().finish();
            } else if (AudioPlayerService.sharedInstance == null || !file.equals(AudioPlayerService.sharedInstance.getCurrentFile())) {
                ArrayList<AudioFile> al;
                intent = new Intent(AudioListFragment.this.getActivity(), AudioPlayerService.class);
                intent.putExtra("action", 2);
                ArrayList<AudioFile> list = AudioListFragment.this.searching ? section == 0 ? AudioListFragment.this.localSearchResults : AudioListFragment.this.searchResults : AudioListFragment.this.displayList;
                if (list.size() > 500) {
                    al = new ArrayList();
                    AudioPlayerService.listToPlay = list;
                } else {
                    al = list;
                }
                intent.putExtra("list_al", al);
                intent.putExtra(GLFilterContext.AttributePosition, position);
                AudioListFragment.this.getActivity().startService(intent);
            } else {
                intent = new Intent(AudioListFragment.this.getActivity(), AudioPlayerService.class);
                intent.putExtra("action", 3);
                AudioListFragment.this.getActivity().startService(intent);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioListFragment.3 */
    class C05813 implements OnItemLongClickListener {
        C05813() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            int[] p = AudioListFragment.this.adapter.resolveIndex(pos);
            int section = p[0];
            int position = p[1];
            AudioFile file = null;
            if (AudioListFragment.this.searching) {
                if (section == 0) {
                    file = (AudioFile) AudioListFragment.this.localSearchResults.get(position);
                }
                if (section == 1) {
                    file = (AudioFile) AudioListFragment.this.searchResults.get(position);
                }
            } else {
                file = (AudioFile) AudioListFragment.this.displayList.get(position);
            }
            if (AudioListFragment.this.select || AudioPlayerService.sharedInstance == null || !AudioPlayerService.sharedInstance.enqueue(file)) {
                return false;
            }
            Toast.makeText(AudioListFragment.this.getActivity(), C0436R.string.audio_added_to_queue, 0).show();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioListFragment.4 */
    class C05824 implements OnScrollListener {
        C05824() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 1 && AudioListFragment.this.getActivity().getCurrentFocus() != null) {
                ((InputMethodManager) AudioListFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(AudioListFragment.this.getActivity().getCurrentFocus().getWindowToken(), 0);
                AudioListFragment.this.getActivity().getCurrentFocus().clearFocus();
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioListFragment.5 */
    class C05835 implements OnClickListener {
        C05835() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(AudioListFragment.this.getActivity(), AudioPlayerService.class);
            intent.putExtra("action", 3);
            AudioListFragment.this.getActivity().startService(intent);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioListFragment.6 */
    class C05846 implements OnClickListener {
        C05846() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(AudioListFragment.this.getActivity(), AudioPlayerService.class);
            intent.putExtra("action", 6);
            AudioListFragment.this.getActivity().startService(intent);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioListFragment.7 */
    class C05857 implements OnClickListener {
        C05857() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(AudioListFragment.this.getActivity(), AudioPlayerService.class);
            intent.putExtra("action", 5);
            AudioListFragment.this.getActivity().startService(intent);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioListFragment.8 */
    class C05868 implements OnClickListener {
        C05868() {
        }

        public void onClick(View v) {
            AudioListFragment.this.startActivity(new Intent(AudioListFragment.this.getActivity(), AudioPlayerActivity.class));
            AudioListFragment.this.getActivity().overridePendingTransition(C0436R.anim.slide_in, C0436R.anim.noop);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioListFragment.9 */
    class C05879 extends ArrayAdapter<String> {
        C05879(Context $anonymous0, int $anonymous1, List $anonymous2) {
            super($anonymous0, $anonymous1, $anonymous2);
        }

        public View getView(int pos, View view, ViewGroup group) {
            return super.getView(AudioListFragment.this.getSherlockActivity().getSupportActionBar().getSelectedNavigationIndex(), view, group);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioListFragment.17 */
    class AnonymousClass17 implements ImageLoadCallback {
        private final /* synthetic */ int val$aid;
        private final /* synthetic */ ImageView val$iv;
        private final /* synthetic */ int val$oid;

        /* renamed from: com.vkontakte.android.fragments.AudioListFragment.17.1 */
        class C05771 implements Runnable {
            private final /* synthetic */ Bitmap val$bmp;
            private final /* synthetic */ ImageView val$iv;

            C05771(ImageView imageView, Bitmap bitmap) {
                this.val$iv = imageView;
                this.val$bmp = bitmap;
            }

            public void run() {
                this.val$iv.setImageBitmap(this.val$bmp);
            }
        }

        AnonymousClass17(int i, int i2, ImageView imageView) {
            this.val$oid = i;
            this.val$aid = i2;
            this.val$iv = imageView;
        }

        public void onImageLoaded(Bitmap bmp, int _oid, int _aid) {
            Log.m528i("vk", "GET COVER OK " + this.val$oid + ", " + this.val$aid);
            this.val$iv.post(new C05771(this.val$iv, bmp));
        }

        public void notAvailable(int oid, int aid) {
        }
    }

    private class AudioListAdapter extends MultiSectionAdapter {
        private AudioListAdapter() {
        }

        public int getItemCount(int section) {
            if (!AudioListFragment.this.searching) {
                return AudioListFragment.this.displayList.size();
            }
            if (section == 0) {
                return AudioListFragment.this.localSearchResults.size();
            }
            return AudioListFragment.this.searchResults.size();
        }

        public Object getItem(int section, int position) {
            AudioFile file = null;
            if (!AudioListFragment.this.searching) {
                return (AudioFile) AudioListFragment.this.displayList.get(position);
            }
            if (section == 0) {
                file = (AudioFile) AudioListFragment.this.localSearchResults.get(position);
            }
            if (section == 1) {
                return (AudioFile) AudioListFragment.this.searchResults.get(position);
            }
            return file;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int section, int position, View convertView) {
            View v;
            if (convertView == null) {
                v = View.inflate(AudioListFragment.this.getActivity(), C0436R.layout.audio_list_item, null);
            } else {
                v = convertView;
            }
            AudioFile file = null;
            if (AudioListFragment.this.searching) {
                if (section == 0) {
                    file = (AudioFile) AudioListFragment.this.localSearchResults.get(position);
                }
                if (section == 1) {
                    file = (AudioFile) AudioListFragment.this.searchResults.get(position);
                }
            } else {
                file = (AudioFile) AudioListFragment.this.displayList.get(position);
            }
            ((TextView) v.findViewById(C0436R.id.audio_artist)).setText(file.artist);
            ((TextView) v.findViewById(C0436R.id.audio_title)).setText(file.title);
            boolean isCurrent = AudioPlayerService.sharedInstance != null && (file.equals(AudioPlayerService.sharedInstance.getCurrentFile()) || file.equalsAdded(AudioPlayerService.sharedInstance.getCurrentFile()));
            boolean isCached = AudioCache.cachedIDs.contains(file.oid + "_" + file.aid);
            boolean pt = isCurrent && AudioPlayerService.sharedInstance.isPlaying();
            if (!AudioListFragment.this.animatingTransitionOut) {
                v.findViewById(C0436R.id.audio_play_icon).setVisibility(isCurrent ? 0 : 8);
            }
            ((ImageView) v.findViewById(C0436R.id.audio_play_icon)).setImageResource(pt ? C0436R.drawable.ic_audio_play : C0436R.drawable.ic_audio_pause);
            v.findViewById(C0436R.id.audio_saved_icon).setVisibility(isCached ? 0 : 8);
            View findViewById = v.findViewById(C0436R.id.audio_duration);
            int i = (AudioListFragment.this.searching && section == 1) ? 0 : 8;
            findViewById.setVisibility(i);
            if (AudioListFragment.this.searching && section == 1) {
                ((TextView) v.findViewById(C0436R.id.audio_duration)).setText(String.format("%d:%02d", new Object[]{Integer.valueOf(file.duration / 60), Integer.valueOf(file.duration % 60)}));
            }
            return v;
        }

        public String getSectionTitle(int section) {
            return AudioListFragment.this.getString(C0436R.string.search_results);
        }

        public int getSectionCount() {
            return AudioListFragment.this.searching ? 2 : 1;
        }

        public long getItemId(int section, int item) {
            return 0;
        }

        public boolean isSectionHeaderVisible(int section) {
            return section > 0 && AudioListFragment.this.searchResults.size() > 0;
        }
    }

    public AudioListFragment() {
        this.currentPlaylist = 0;
        this.audios = new ArrayList();
        this.displayList = new ArrayList();
        this.searchResults = new ArrayList();
        this.localSearchResults = new ArrayList();
        this.nowPlaying = null;
        this.animatingTransitionOut = false;
        this.playlists = new ArrayList();
        this.searching = false;
        this.receiver = new C05781();
        this.select = false;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        this.select = getArguments().getBoolean("select");
        this.uid = getArguments().getInt("uid", Global.uid);
        ActivityUtils.setBeamLink(getActivity(), "albums" + this.uid);
        ListView listView = this.list;
        ListAdapter audioListAdapter = new AudioListAdapter();
        this.adapter = audioListAdapter;
        listView.setAdapter(audioListAdapter);
        this.list.setOnItemClickListener(new C05802());
        this.list.setOnItemLongClickListener(new C05813());
        this.list.setPadding(getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding), 0, getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding), 0);
        this.list.setScrollBarStyle(33554432);
        this.list.setClipToPadding(false);
        this.list.setOnScrollListener(new C05824());
        getSherlockActivity().getSupportActionBar().setNavigationMode(0);
        act.setTitle(ACRAConstants.DEFAULT_STRING_VALUE);
        this.playerBar = View.inflate(act, C0436R.layout.audio_bottom_panel, null);
        this.playerBar.setVisibility(8);
        this.contentView.addView(this.playerBar, new LayoutParams(-1, Global.scale(58.0f), 80));
        this.playerBar.findViewById(C0436R.id.audio_panel_play).setOnClickListener(new C05835());
        this.playerBar.findViewById(C0436R.id.audio_panel_prev).setOnClickListener(new C05846());
        this.playerBar.findViewById(C0436R.id.audio_panel_next).setOnClickListener(new C05857());
        this.playerBar.findViewById(C0436R.id.audio_panel_switcher).setOnClickListener(new C05868());
        if (AudioPlayerService.sharedInstance != null) {
            this.nowPlaying = AudioPlayerService.sharedInstance.getCurrentFile();
            if (this.nowPlaying != null) {
                this.playerBar.setVisibility(0);
                ViewFlipper vs = (ViewFlipper) this.playerBar.findViewById(C0436R.id.audio_panel_switcher);
                View v = vs.getChildAt(vs.getDisplayedChild());
                ((TextView) v.findViewById(C0436R.id.audio_panel_artist)).setText(this.nowPlaying.artist);
                ((TextView) v.findViewById(C0436R.id.audio_panel_title)).setText(this.nowPlaying.title);
                v.findViewById(C0436R.id.audio_panel_artist).setSelected(true);
                v.findViewById(C0436R.id.audio_panel_title).setSelected(true);
                getAndShowCover(this.nowPlaying.oid, this.nowPlaying.aid, (ImageView) v.findViewById(C0436R.id.audio_panel_cover));
                ((ImageView) this.playerBar.findViewById(C0436R.id.audio_panel_play)).setImageResource(AudioPlayerService.sharedInstance.isPlaying() ? C0436R.drawable.ic_audio_panel_pause : C0436R.drawable.ic_audio_panel_play);
                v.findViewById(C0436R.id.audio_panel_progress).setVisibility(AudioPlayerService.sharedInstance.initing ? 0 : 8);
                this.list.setPadding(this.list.getPaddingLeft(), 0, this.list.getPaddingRight(), Global.scale(58.0f));
            }
        }
        updateBottomBarButtons();
        AudioCache.fillIDs(act);
        SherlockFragmentActivity sa = getSherlockActivity();
        sa.getSupportActionBar().setNavigationMode(1);
        this.navAdapter = new C05879(sa.getSupportActionBar().getThemedContext(), C0436R.layout.nav_spinner_item, new ArrayList());
        this.navAdapter.setDropDownViewResource(C0436R.layout.sherlock_spinner_dropdown_item);
        sa.getSupportActionBar().setListNavigationCallbacks(this.navAdapter, new OnNavigationListener() {
            public boolean onNavigationItemSelected(int pos, long itemId) {
                if (pos == 0) {
                    AudioListFragment.this.setCurrentPlaylist(0);
                } else if (AudioListFragment.this.uid <= 0 || pos != 1) {
                    if (AudioListFragment.this.uid < 0 && pos > 1) {
                        pos++;
                    }
                    pos -= 3;
                    if (pos <= -1) {
                        AudioListFragment.this.currentPlaylist = -2;
                        AudioListFragment.this.loadSaved();
                    } else {
                        AudioListFragment.this.setCurrentPlaylist(((AudioPlaylist) AudioListFragment.this.playlists.get(pos)).id);
                    }
                } else {
                    AudioListFragment.this.currentPlaylist = -1;
                    AudioListFragment.this.loadRecommendations();
                }
                return true;
            }
        });
        sa.getSupportActionBar().setDisplayShowTitleEnabled(false);
        updateNavItems();
        this.searchView = new SearchView(sa.getSupportActionBar().getThemedContext());
        this.searchView.setQueryHint(getResources().getString(C0436R.string.search));
        try {
            Field searchField = SearchView.class.getDeclaredField("mSearchButton");
            searchField.setAccessible(true);
            ((ImageView) searchField.get(this.searchView)).setImageResource(C0436R.drawable.ic_ab_search);
            searchField = SearchView.class.getDeclaredField("mSearchPlate");
            searchField.setAccessible(true);
            ((TextView) ((LinearLayout) searchField.get(this.searchView)).getChildAt(0)).setHintTextColor(-2130706433);
        } catch (Exception e) {
        }
        this.searchView.setOnQueryTextListener(new OnQueryTextListener() {

            /* renamed from: com.vkontakte.android.fragments.AudioListFragment.11.1 */
            class C05761 implements Runnable {
                private final /* synthetic */ String val$newText;

                C05761(String str) {
                    this.val$newText = str;
                }

                public void run() {
                    AudioListFragment.this.searchLoader = null;
                    AudioListFragment.this.loadSearch(this.val$newText);
                }
            }

            public boolean onQueryTextSubmit(String query) {
                if (AudioListFragment.this.searchLoader != null) {
                    AudioListFragment.this.list.removeCallbacks(AudioListFragment.this.searchLoader);
                    AudioListFragment.this.searchLoader = null;
                }
                AudioListFragment.this.loadSearch(query);
                if (AudioListFragment.this.getActivity().getCurrentFocus() != null) {
                    View f = AudioListFragment.this.getActivity().getCurrentFocus();
                    f.clearFocus();
                    ((InputMethodManager) AudioListFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(f.getWindowToken(), 0);
                }
                return true;
            }

            public boolean onQueryTextChange(String newText) {
                AudioListFragment.this.searching = newText.length() > 0;
                AudioListFragment.this.doLocalSearch(newText);
                if (AudioListFragment.this.searchLoader != null) {
                    AudioListFragment.this.list.removeCallbacks(AudioListFragment.this.searchLoader);
                    AudioListFragment.this.searchLoader = null;
                }
                AudioListFragment.this.searchLoader = new C05761(newText);
                AudioListFragment.this.list.postDelayed(AudioListFragment.this.searchLoader, 1500);
                return false;
            }
        });
        this.searchView.setOnSearchClickListener(new OnClickListener() {
            public void onClick(View v) {
                AudioListFragment.this.getSherlockActivity().getSupportActionBar().setNavigationMode(0);
            }
        });
        this.searchView.setOnCloseListener(new OnCloseListener() {
            public boolean onClose() {
                AudioListFragment.this.getSherlockActivity().getSupportActionBar().setNavigationMode(1);
                return false;
            }
        });
        setHasOptionsMenu(true);
        if (getArguments().containsKey("search")) {
            String q = getArguments().getCharSequence("search").toString();
            this.searchView.setIconified(false);
            this.searchView.setQuery(q, true);
            this.searchView.clearFocus();
        } else if (NetworkStateReceiver.isConnected() || this.uid != Global.uid) {
            loadData();
        } else {
            getSherlockActivity().getSupportActionBar().setSelectedNavigationItem(this.uid > 0 ? 2 : 1);
        }
        this.errorView.setOnRetryListener(new OnClickListener() {
            public void onClick(View v) {
                if (AudioListFragment.this.errorRequest != null) {
                    AudioListFragment.this.errorView.setVisibility(8);
                    AudioListFragment.this.progress.setVisibility(0);
                    AudioListFragment.this.currentRequest = AudioListFragment.this.errorRequest;
                    AudioListFragment.this.errorRequest.exec(AudioListFragment.this.getActivity());
                }
            }
        });
    }

    public void onDetach() {
        getSherlockActivity().getSupportActionBar().setListNavigationCallbacks(null, null);
        getSherlockActivity().getSupportActionBar().setNavigationMode(0);
        super.onDetach();
    }

    protected ListView initListView() {
        ListView l = new ListView(getActivity());
        l.setSelector(C0436R.drawable.highlight);
        l.setDivider(new ColorDrawable(-2104603));
        l.setDividerHeight(1);
        return l;
    }

    protected String getEmptyText() {
        return getString(C0436R.string.no_audios);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.m525d("vk", "invalidate menu, removing=" + isRemoving());
        if (!isRemoving()) {
            MenuItem search = menu.add((int) C0436R.string.search);
            search.setShowAsAction(2);
            search.setActionView(this.searchView);
            super.onCreateOptionsMenu(menu, inflater);
        }
    }

    private void updateNavItems() {
        this.navAdapter.clear();
        if (this.uid == Global.uid) {
            this.navAdapter.add(getString(C0436R.string.my_music));
            this.navAdapter.add(getString(C0436R.string.recommendations));
        } else {
            this.navAdapter.add(getString(C0436R.string.users_audio, getArguments().getCharSequence("username")));
            if (this.uid > 0) {
                this.navAdapter.add(getString(C0436R.string.recommendations));
            }
        }
        this.navAdapter.add(getString(C0436R.string.audio_saved));
        new AudioGetAlbums(this.uid).setCallback(new Callback() {
            public void success(ArrayList<AudioPlaylist> lists) {
                AudioListFragment.this.playlists.clear();
                AudioListFragment.this.playlists.addAll(lists);
                Iterator it = lists.iterator();
                while (it.hasNext()) {
                    AudioListFragment.this.navAdapter.add(((AudioPlaylist) it.next()).title);
                }
                AudioListFragment.this.navAdapter.notifyDataSetChanged();
            }

            public void fail(int ecode, String emsg) {
            }
        }).exec(getActivity());
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        IntentFilter filter = new IntentFilter();
        filter.addAction(AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS);
        filter.addAction(AudioPlayerService.ACTION_SERVICE_STOPPING);
        filter.addAction(AudioCache.ACTION_ALBUM_ART_AVAILABLE);
        filter.addAction(AudioCache.ACTION_FILE_ADDED);
        filter.addAction(AudioCache.ACTION_FILE_DELETED);
        VKApplication.context.registerReceiver(this.receiver, filter);
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
        if (this.currentRequest != null) {
            this.currentRequest.cancel();
        }
    }

    public void onResume() {
        super.onResume();
        getActivity().setVolumeControlStream(3);
        getActivity().getWindow().setSoftInputMode(32);
        if (AudioPlayerService.sharedInstance == null && this.playerBar.getVisibility() != 8) {
            this.playerBar.setVisibility(8);
            this.adapter.notifyDataSetChanged();
        }
    }

    public void onPause() {
        super.onPause();
        getActivity().setVolumeControlStream(ExploreByTouchHelper.INVALID_ID);
        getActivity().getWindow().setSoftInputMode(16);
    }

    public void onConfigurationChanged(Configuration cfg) {
        super.onConfigurationChanged(cfg);
        updateBottomBarButtons();
    }

    private void animateStateTransition(int item, boolean in) {
        Log.m528i("vk", "Animate " + item + " " + in);
        if (VERSION.SDK_INT < 11) {
            this.adapter.notifyDataSetChanged();
        } else if (this.list.getHeaderViewsCount() + item >= this.list.getFirstVisiblePosition() && this.list.getHeaderViewsCount() + item <= this.list.getLastVisiblePosition()) {
            View v = this.list.getChildAt((this.list.getHeaderViewsCount() + item) - this.list.getFirstVisiblePosition());
            int iconSize = Global.scale(35.0f);
            ArrayList<Animator> anims;
            AnimatorSet set;
            if (in) {
                v.findViewById(C0436R.id.audio_play_icon).setVisibility(0);
                ((ImageView) v.findViewById(C0436R.id.audio_play_icon)).setImageResource(C0436R.drawable.ic_audio_play);
                if (VERSION.SDK_INT >= 11) {
                    anims = new ArrayList();
                    anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_play_icon), "translationX", new float[]{(float) (-iconSize), 0.0f}).setDuration(200));
                    anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_artist), "translationX", new float[]{(float) (-iconSize), 0.0f}).setDuration(200));
                    anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_title), "translationX", new float[]{(float) (-iconSize), 0.0f}).setDuration(200));
                    if (this.list.getPaddingLeft() > 0) {
                        anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_play_icon), "alpha", new float[]{0.0f, 1.0f}).setDuration(200));
                    }
                    set = new AnimatorSet();
                    set.playTogether(anims);
                    set.start();
                    return;
                }
                return;
            }
            this.animatingTransitionOut = true;
            if (VERSION.SDK_INT >= 11) {
                anims = new ArrayList();
                anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_play_icon), "translationX", new float[]{(float) (-iconSize)}).setDuration(200));
                anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_artist), "translationX", new float[]{(float) (-iconSize)}).setDuration(200));
                anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_title), "translationX", new float[]{(float) (-iconSize)}).setDuration(200));
                if (this.list.getPaddingLeft() > 0) {
                    anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_play_icon), "alpha", new float[]{0.0f}).setDuration(200));
                }
                set = new AnimatorSet();
                set.playTogether(anims);
                set.addListener(new AnonymousClass16(v));
                set.start();
            }
        }
    }

    private void updateBottomBarButtons() {
        boolean showBtns;
        int i;
        int i2 = 0;
        if (((float) getResources().getDisplayMetrics().widthPixels) / getResources().getDisplayMetrics().density >= 500.0f) {
            showBtns = true;
        } else {
            showBtns = false;
        }
        View findViewById = this.playerBar.findViewById(C0436R.id.audio_panel_prev);
        if (showBtns) {
            i = 0;
        } else {
            i = 8;
        }
        findViewById.setVisibility(i);
        View findViewById2 = this.playerBar.findViewById(C0436R.id.audio_panel_next);
        if (!showBtns) {
            i2 = 8;
        }
        findViewById2.setVisibility(i2);
    }

    private void showBottomBar(AudioFile f) {
        int i = 0;
        ViewFlipper vs = (ViewFlipper) this.playerBar.findViewById(C0436R.id.audio_panel_switcher);
        View v = vs.getChildAt(vs.getDisplayedChild());
        ((TextView) v.findViewById(C0436R.id.audio_panel_artist)).setText(f.artist);
        ((TextView) v.findViewById(C0436R.id.audio_panel_title)).setText(f.title);
        v.findViewById(C0436R.id.audio_panel_artist).setSelected(true);
        v.findViewById(C0436R.id.audio_panel_title).setSelected(true);
        getAndShowCover(f.oid, f.aid, (ImageView) v.findViewById(C0436R.id.audio_panel_cover));
        Animation in = new TranslateAnimation(0, 0.0f, 0, 0.0f, 1, 1.0f, 1, 0.0f);
        in.setDuration(300);
        in.setInterpolator(new DecelerateInterpolator());
        this.playerBar.setVisibility(0);
        this.playerBar.startAnimation(in);
        this.list.setPadding(this.list.getPaddingLeft(), 0, this.list.getPaddingRight(), Global.scale(58.0f));
        View findViewById = v.findViewById(C0436R.id.audio_panel_progress);
        if (!AudioPlayerService.sharedInstance.initing) {
            i = 8;
        }
        findViewById.setVisibility(i);
    }

    private void animateBottomBar(AudioFile f, boolean forward) {
        ViewFlipper vs = (ViewFlipper) this.playerBar.findViewById(C0436R.id.audio_panel_switcher);
        View v = vs.getChildAt(vs.getDisplayedChild() == 0 ? 1 : 0);
        ((TextView) v.findViewById(C0436R.id.audio_panel_artist)).setText(f.artist);
        ((TextView) v.findViewById(C0436R.id.audio_panel_title)).setText(f.title);
        v.findViewById(C0436R.id.audio_panel_artist).setSelected(true);
        v.findViewById(C0436R.id.audio_panel_title).setSelected(true);
        getAndShowCover(f.oid, f.aid, (ImageView) v.findViewById(C0436R.id.audio_panel_cover));
        Animation in = new TranslateAnimation(0, 0.0f, 0, 0.0f, 2, (float) (forward ? 1 : -1), 2, 0.0f);
        in.setDuration(300);
        in.setInterpolator(new DecelerateInterpolator());
        Animation out = new TranslateAnimation(0, 0.0f, 0, 0.0f, 2, 0.0f, 2, (float) (forward ? -1 : 1));
        v.findViewById(C0436R.id.audio_panel_progress).setVisibility(AudioPlayerService.sharedInstance.initing ? 0 : 8);
        out.setDuration(300);
        out.setInterpolator(new DecelerateInterpolator());
        vs.setInAnimation(in);
        vs.setOutAnimation(out);
        vs.showNext();
    }

    private void getAndShowCover(int oid, int aid, ImageView iv) {
        iv.setImageResource(C0436R.drawable.audio_panel_placeholder);
        Log.m528i("vk", "GET COVER " + oid + ", " + aid);
        AlbumArtRetriever.getCoverImage(aid, oid, 1, new AnonymousClass17(oid, aid, iv));
    }

    private void setCurrentPlaylist(int id) {
        if (this.audios.size() == 0) {
            loadData();
            return;
        }
        this.currentPlaylist = id;
        this.displayList.clear();
        Iterator it = this.audios.iterator();
        while (it.hasNext()) {
            AudioFile f = (AudioFile) it.next();
            if (id == 0 || f.playlistID == id) {
                this.displayList.add(f);
            }
        }
        this.adapter.notifyDataSetChanged();
        this.list.setSelectionFromTop(0, 0);
    }

    private void loadData() {
        if (this.currentRequest != null) {
            this.currentRequest.cancel();
        }
        this.contentWrap.setVisibility(8);
        this.errorView.setVisibility(8);
        this.progress.setVisibility(0);
        this.currentRequest = new AudioGet(this.uid).setCallback(new AudioGet.Callback() {
            public void success(ArrayList<AudioFile> files) {
                AudioListFragment.this.currentRequest = null;
                AudioListFragment.this.audios.clear();
                AudioListFragment.this.audios.addAll(files);
                Global.showViewAnimated(AudioListFragment.this.contentWrap, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(AudioListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                if (AudioPlayerService.sharedInstance != null) {
                    AudioListFragment.this.nowPlaying = AudioPlayerService.sharedInstance.getCurrentFile();
                }
                if (files.size() > 0) {
                    AudioListFragment.this.setCurrentPlaylist(AudioListFragment.this.currentPlaylist);
                }
            }

            public void fail(int ecode, String emsg) {
                AudioListFragment.this.errorRequest = AudioListFragment.this.currentRequest;
                AudioListFragment.this.currentRequest = null;
                AudioListFragment.this.errorView.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(AudioListFragment.this.errorView, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(AudioListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
            }
        }).exec(this.list);
    }

    private void loadRecommendations() {
        if (this.currentRequest != null) {
            this.currentRequest.cancel();
        }
        this.contentWrap.setVisibility(8);
        this.errorView.setVisibility(8);
        this.progress.setVisibility(0);
        this.currentRequest = new AudioGetRecommendations(this.uid).setCallback(new AudioGetRecommendations.Callback() {
            public void success(ArrayList<AudioFile> files) {
                AudioListFragment.this.displayList.clear();
                AudioListFragment.this.displayList.addAll(files);
                AudioListFragment.this.adapter.notifyDataSetChanged();
                Global.showViewAnimated(AudioListFragment.this.contentWrap, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(AudioListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                AudioListFragment.this.currentRequest = null;
                AudioListFragment.this.list.setSelectionFromTop(0, 0);
            }

            public void fail(int ecode, String emsg) {
                AudioListFragment.this.errorRequest = AudioListFragment.this.currentRequest;
                AudioListFragment.this.currentRequest = null;
                AudioListFragment.this.errorView.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(AudioListFragment.this.errorView, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(AudioListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
            }
        }).exec(this.list);
    }

    private void loadSearch(String q) {
        if (this.currentRequest != null) {
            this.currentRequest.cancel();
        }
        this.errorView.setVisibility(8);
        this.currentRequest = new AudioSearch(q).setCallback(new AudioSearch.Callback() {
            public void success(ArrayList<AudioFile> files) {
                AudioListFragment.this.currentRequest = null;
                AudioListFragment.this.searchResults.clear();
                AudioListFragment.this.searchResults.addAll(files);
                AudioListFragment.this.adapter.notifyDataSetChanged();
                if (AudioListFragment.this.contentWrap.getVisibility() != 0) {
                    Global.showViewAnimated(AudioListFragment.this.contentWrap, true, PhotoView.THUMB_ANIM_DURATION);
                    Global.showViewAnimated(AudioListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                }
            }

            public void fail(int ecode, String emsg) {
                AudioListFragment.this.errorRequest = AudioListFragment.this.currentRequest;
                AudioListFragment.this.currentRequest = null;
                AudioListFragment.this.errorView.setErrorInfo(ecode, emsg);
                if (AudioListFragment.this.contentWrap.getVisibility() != 0) {
                    Global.showViewAnimated(AudioListFragment.this.errorView, true, PhotoView.THUMB_ANIM_DURATION);
                    Global.showViewAnimated(AudioListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                }
            }
        }).exec(this.list);
    }

    private void doLocalSearch(String q) {
        this.localSearchResults.clear();
        q = q.toLowerCase();
        Iterator it = this.displayList.iterator();
        while (it.hasNext()) {
            AudioFile f = (AudioFile) it.next();
            if (f.artist.toLowerCase().indexOf(q) > -1 || f.title.toLowerCase().indexOf(q) > -1) {
                this.localSearchResults.add(f);
            }
        }
        this.adapter.notifyDataSetChanged();
        if (this.searching) {
            this.emptyView.setText((int) C0436R.string.nothing_found);
            if (this.localSearchResults.size() == 0) {
                this.contentWrap.setVisibility(8);
                this.progress.setVisibility(0);
                return;
            }
            return;
        }
        this.emptyView.setText(this.uid == 0 ? C0436R.string.no_audios_me : C0436R.string.no_audios);
        this.contentWrap.setVisibility(0);
        this.progress.setVisibility(8);
    }

    private void loadSaved() {
        if (this.currentRequest != null) {
            this.currentRequest.cancel();
        }
        this.contentWrap.setVisibility(8);
        this.errorView.setVisibility(8);
        this.progress.setVisibility(0);
        new Thread(new Runnable() {

            /* renamed from: com.vkontakte.android.fragments.AudioListFragment.21.1 */
            class C05791 implements Runnable {
                C05791() {
                }

                public void run() {
                    AudioListFragment.this.adapter.notifyDataSetChanged();
                    Global.showViewAnimated(AudioListFragment.this.contentWrap, true, PhotoView.THUMB_ANIM_DURATION);
                    Global.showViewAnimated(AudioListFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                }
            }

            public void run() {
                AudioCache.refillIDs(AudioListFragment.this.getActivity());
                ArrayList<AudioFile> cached = AudioCache.getCachedList(AudioListFragment.this.getActivity());
                AudioListFragment.this.displayList.clear();
                AudioListFragment.this.displayList.addAll(cached);
                AudioListFragment.this.getActivity().runOnUiThread(new C05791());
            }
        }).start();
    }
}
