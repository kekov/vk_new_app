package com.vkontakte.android.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.ActivityUtils;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.FragmentWrapperActivity;
import com.vkontakte.android.Global;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.NewsView;
import com.vkontakte.android.PhotoAlbumsView;
import com.vkontakte.android.PhotoAlbumsView.AlbumActionsCallback;
import com.vkontakte.android.VKAlertDialog;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.PhotoAlbum;
import com.vkontakte.android.api.PhotosCreateAlbum;
import com.vkontakte.android.api.PhotosCreateAlbum.Callback;
import com.vkontakte.android.api.PhotosDeleteAlbum;
import com.vkontakte.android.api.PhotosEditAlbum;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import org.acra.ACRAConstants;

public class PhotoAlbumsListFragment extends SherlockFragment implements AlbumActionsCallback {
    private static final int SELECT_RESULT = 8294;
    private NewsView newsView;
    private ViewPager pager;
    private LinearLayout pagerWrap;
    private boolean showCreate;
    private PagerSlidingTabStrip tabStrip;
    private int uid;
    private PhotoAlbumsView view;

    /* renamed from: com.vkontakte.android.fragments.PhotoAlbumsListFragment.2 */
    class C07362 extends ArrayAdapter<CharSequence> {
        C07362(Context $anonymous0, int $anonymous1, CharSequence[] $anonymous2) {
            super($anonymous0, $anonymous1, $anonymous2);
        }

        public int getCount() {
            return super.getCount() - 1;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoAlbumsListFragment.3 */
    class C07373 implements OnClickListener {
        private final /* synthetic */ PhotoAlbum val$a;
        private final /* synthetic */ View val$view;

        C07373(View view, PhotoAlbum photoAlbum) {
            this.val$view = view;
            this.val$a = photoAlbum;
        }

        public void onClick(DialogInterface dialog, int which) {
            String title = ((TextView) this.val$view.findViewById(C0436R.id.create_album_title)).getText().toString();
            String descr = ((TextView) this.val$view.findViewById(C0436R.id.create_album_descr)).getText().toString();
            int privacy = ((Spinner) this.val$view.findViewById(C0436R.id.create_album_privacy)).getSelectedItemPosition();
            if (this.val$a == null) {
                PhotoAlbumsListFragment.this.doCreateAlbum(PhotoAlbumsListFragment.this.uid, title, descr, privacy);
                return;
            }
            this.val$a.title = title;
            this.val$a.descr = descr;
            this.val$a.privacy = privacy;
            PhotoAlbumsListFragment.this.doEditAlbum(this.val$a);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoAlbumsListFragment.4 */
    class C07384 implements OnShowListener {
        private final /* synthetic */ View val$view;

        C07384(View view) {
            this.val$view = view;
        }

        public void onShow(DialogInterface dialog) {
            ((InputMethodManager) PhotoAlbumsListFragment.this.getActivity().getSystemService("input_method")).showSoftInput((TextView) this.val$view.findViewById(C0436R.id.create_album_title), 1);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoAlbumsListFragment.7 */
    class C07397 implements OnClickListener {
        private final /* synthetic */ int val$aid;

        C07397(int i) {
            this.val$aid = i;
        }

        public void onClick(DialogInterface dialog, int which) {
            PhotoAlbumsListFragment.this.doDelete(this.val$aid);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoAlbumsListFragment.1 */
    class C14941 implements OnPageChangeListener {
        C14941() {
        }

        public void onPageSelected(int page) {
            boolean z = true;
            PhotoAlbumsListFragment photoAlbumsListFragment = PhotoAlbumsListFragment.this;
            if (page != 1) {
                z = false;
            }
            photoAlbumsListFragment.showCreate = z;
            PhotoAlbumsListFragment.this.getSherlockActivity().invalidateOptionsMenu();
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoAlbumsListFragment.5 */
    class C14955 implements Callback {
        C14955() {
        }

        public void success(PhotoAlbum album) {
            PhotoAlbumsListFragment.this.view.addOrReplace(album);
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PhotoAlbumsListFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoAlbumsListFragment.6 */
    class C14966 implements PhotosEditAlbum.Callback {
        private final /* synthetic */ PhotoAlbum val$album;

        C14966(PhotoAlbum photoAlbum) {
            this.val$album = photoAlbum;
        }

        public void success() {
            PhotoAlbumsListFragment.this.view.addOrReplace(this.val$album);
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PhotoAlbumsListFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoAlbumsListFragment.8 */
    class C14978 implements PhotosDeleteAlbum.Callback {
        private final /* synthetic */ int val$aid;

        C14978(int i) {
            this.val$aid = i;
        }

        public void success() {
            PhotoAlbumsListFragment.this.view.remove(this.val$aid);
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PhotoAlbumsListFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    private class PhotosPagerAdapter extends PagerAdapter {
        private PhotosPagerAdapter() {
        }

        public int getCount() {
            return 2;
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public Object instantiateItem(ViewGroup container, int position) {
            View v = null;
            switch (position) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    v = PhotoAlbumsListFragment.this.newsView;
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    v = PhotoAlbumsListFragment.this.view;
                    break;
            }
            container.addView(v);
            return v;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            View v = null;
            switch (position) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    v = PhotoAlbumsListFragment.this.newsView;
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    v = PhotoAlbumsListFragment.this.view;
                    break;
            }
            container.removeView(v);
        }

        public CharSequence getPageTitle(int pos) {
            switch (pos) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return PhotoAlbumsListFragment.this.getString(C0436R.string.friends);
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return PhotoAlbumsListFragment.this.getString(C0436R.string.albums);
                default:
                    return ACRAConstants.DEFAULT_STRING_VALUE;
            }
        }
    }

    public PhotoAlbumsListFragment() {
        this.showCreate = true;
    }

    public void onAttach(Activity act) {
        boolean z = false;
        super.onAttach(act);
        this.uid = getArguments().getInt("uid", 0);
        ActivityUtils.setBeamLink(act, "albums" + this.uid);
        SherlockFragmentActivity sa = getSherlockActivity();
        sa.getSupportActionBar().setDisplayShowTitleEnabled(true);
        if (getArguments().containsKey(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)) {
            sa.setTitle(getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
        } else {
            sa.setTitle(getString(C0436R.string.albums));
        }
        if (getArguments().getBoolean("news")) {
            this.pager = new ViewPager(act);
            this.pager.setAdapter(new PhotosPagerAdapter());
            this.newsView = new NewsView((Context) act, true);
            this.newsView.initPhotos();
            this.newsView.loadData(false);
            this.tabStrip = new PagerSlidingTabStrip(getActivity());
            this.tabStrip.setBackgroundResource(C0436R.color.tab_bg);
            this.tabStrip.setIndicatorColorResource(C0436R.color.tab_indicator);
            this.tabStrip.setViewPager(this.pager);
            this.tabStrip.setOnPageChangeListener(new C14941());
            this.pagerWrap = new LinearLayout(getActivity());
            this.pagerWrap.setOrientation(1);
            this.pagerWrap.addView(this.tabStrip, -1, Global.scale(GalleryPickerFooterView.SIZE));
            this.pagerWrap.addView(this.pager, -1, -1);
            this.showCreate = false;
        }
        int i = this.uid;
        String str = (String) getArguments().getCharSequence("user_name_ins");
        if (!getArguments().getBoolean("select_album")) {
            z = true;
        }
        this.view = new PhotoAlbumsView(act, i, str, z);
        this.view.onActivate();
        this.view.callback = this;
        setHasOptionsMenu(true);
    }

    public void onDetach() {
        getSherlockActivity().getSupportActionBar().removeAllTabs();
        super.onDetach();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(C0436R.menu.photoalbums, menu);
        MenuItem findItem = menu.findItem(C0436R.id.create);
        boolean z = this.showCreate && ((this.uid == 0 || this.uid == Global.uid || (this.uid < 0 && Groups.isGroupAdmin(-this.uid))) && (!getArguments().getBoolean("select") || getArguments().getBoolean("select_album")));
        findItem.setVisible(z);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0436R.id.create) {
            showCreateBox(null);
        }
        return true;
    }

    public void onPause() {
        super.onPause();
        this.view.onDeactivate();
        this.view.onPause();
    }

    public void onResume() {
        super.onResume();
        this.view.onActivate();
        this.view.onResume();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.pagerWrap != null ? this.pagerWrap : this.view;
    }

    private void showCreateBox(PhotoAlbum a) {
        int i;
        View view = View.inflate(getActivity(), C0436R.layout.create_photo_album, null);
        if (this.uid < 0) {
            view.findViewById(C0436R.id.create_album_privacy_wrap).setVisibility(8);
        }
        Spinner sp = (Spinner) view.findViewById(C0436R.id.create_album_privacy);
        ArrayAdapter<CharSequence> adapter = new C07362(getActivity(), 17367048, getResources().getStringArray(C0436R.array.album_privacy_options));
        adapter.setDropDownViewResource(17367049);
        sp.setAdapter(adapter);
        if (a != null) {
            ((TextView) view.findViewById(C0436R.id.create_album_title)).setText(a.title);
            ((TextView) view.findViewById(C0436R.id.create_album_descr)).setText(a.descr);
            sp.setSelection(a.privacy);
        }
        Builder view2 = new VKAlertDialog.Builder(getActivity()).setTitle(a == null ? C0436R.string.create_album : C0436R.string.edit_album).setView(view);
        if (a == null) {
            i = C0436R.string.create;
        } else {
            i = C0436R.string.save;
        }
        AlertDialog dlg = view2.setPositiveButton(i, new C07373(view, a)).setNegativeButton(C0436R.string.cancel, null).create();
        dlg.setOnShowListener(new C07384(view));
        dlg.show();
    }

    private void doCreateAlbum(int uid, String title, String descr, int privacy) {
        if (uid >= 0) {
            uid = 0;
        }
        new PhotosCreateAlbum(title, descr, privacy, uid).setCallback(new C14955()).wrapProgress(getActivity()).exec(getActivity());
    }

    private void doEditAlbum(PhotoAlbum album) {
        new PhotosEditAlbum(album.id, album.title, album.descr, album.privacy, this.uid < 0 ? this.uid : 0).setCallback(new C14966(album)).wrapProgress(getActivity()).exec(getActivity());
    }

    public void deleteAlbum(int aid) {
        new VKAlertDialog.Builder(getActivity()).setTitle(C0436R.string.delete_album).setMessage(C0436R.string.delete_album_confirm).setIcon(VERSION.SDK_INT < 11 ? ACRAConstants.DEFAULT_DIALOG_ICON : 0).setPositiveButton(C0436R.string.yes, new C07397(aid)).setNegativeButton(C0436R.string.no, null).show();
    }

    public void editAlbum(PhotoAlbum album) {
        showCreateBox(album);
    }

    private void doDelete(int aid) {
        new PhotosDeleteAlbum(aid, this.uid < 0 ? -this.uid : 0).setCallback(new C14978(aid)).wrapProgress(getActivity()).exec(getActivity());
    }

    public void openAlbum(PhotoAlbum album) {
        boolean z = true;
        if (getArguments().getBoolean("select_album")) {
            Intent intent = new Intent();
            intent.putExtra("album", album);
            getActivity().setResult(-1, intent);
            getActivity().finish();
            return;
        }
        Bundle args = new Bundle();
        args.putParcelable("album", album);
        if (getArguments().getBoolean("select")) {
            args.putBoolean("select", true);
            intent = new Intent(getActivity(), FragmentWrapperActivity.class);
            intent.putExtra("class", "PhotoListFragment");
            intent.putExtra("args", args);
            if (VERSION.SDK_INT >= 14 && !Build.BRAND.toLowerCase().contains("zte")) {
                intent.putExtra("overlaybar", true);
            }
            startActivityForResult(intent, SELECT_RESULT);
            return;
        }
        String str = "PhotoListFragment";
        Activity activity = getActivity();
        if (VERSION.SDK_INT < 14 || Build.BRAND.toLowerCase().contains("zte")) {
            z = false;
        }
        Navigate.to(str, args, activity, z, -1, -1);
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode == SELECT_RESULT && resCode == -1) {
            getActivity().setResult(-1, data);
            getActivity().finish();
        }
    }
}
