package com.vkontakte.android.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.FragmentWrapperActivity;
import com.vkontakte.android.Global;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.NewsView;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.VideoAttachment;
import com.vkontakte.android.VideoListView;
import com.vkontakte.android.VideoListView.VideoViewCallback;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.PhotoAlbum;
import com.vkontakte.android.api.VideoFile;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;

public class FaveFragment extends SherlockFragment {
    private LinearLayout contentView;
    private UserListView links;
    private ViewPager pager;
    private FrameLayout photos;
    private Runnable postedLoad;
    private NewsView posts;
    private boolean postsLoaded;
    private int prev;
    private PagerSlidingTabStrip tabbar;
    private UserListView users;
    private boolean usersLoaded;
    private VideoListView videos;

    /* renamed from: com.vkontakte.android.fragments.FaveFragment.1 */
    class C14641 implements VideoViewCallback {
        C14641() {
        }

        public void openComments(VideoFile vf, String name, String photo) {
            NewsEntry e = new NewsEntry();
            e.attachments.add(new VideoAttachment(vf));
            e.text = Global.replaceMentions(vf.descr);
            e.time = vf.date;
            e.postID = vf.vid;
            int i = vf.oid;
            e.ownerID = i;
            e.userID = i;
            e.type = 2;
            e.flags |= 2;
            e.userName = name;
            e.userPhotoURL = photo;
            e.numLikes = vf.likes;
            e.flag(8, vf.liked);
            Bundle args = new Bundle();
            args.putParcelable("entry", e);
            Intent intent = new Intent(FaveFragment.this.getActivity(), FragmentWrapperActivity.class);
            intent.putExtra("class", "PostViewFragment");
            intent.putExtra("args", args);
            FaveFragment.this.startActivity(intent);
        }

        public void showAddDialog() {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FaveFragment.2 */
    class C14652 implements OnPageChangeListener {

        /* renamed from: com.vkontakte.android.fragments.FaveFragment.2.1 */
        class C06771 implements Runnable {
            private final /* synthetic */ int val$page;

            C06771(int i) {
                this.val$page = i;
            }

            public void run() {
                FaveFragment.this.postedLoad = null;
                switch (this.val$page) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                        if (!FaveFragment.this.usersLoaded) {
                            FaveFragment.this.users.loadData();
                            FaveFragment.this.usersLoaded = true;
                        }
                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                        if (!FaveFragment.this.postsLoaded) {
                            FaveFragment.this.posts.loadData(true);
                            FaveFragment.this.postsLoaded = true;
                        }
                    case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                        if (FaveFragment.this.links.list.getCount() == FaveFragment.this.links.list.getHeaderViewsCount() + FaveFragment.this.links.list.getFooterViewsCount()) {
                            FaveFragment.this.links.loadData();
                        }
                    case UserListView.TYPE_FAVE /*4*/:
                        if (FaveFragment.this.videos.getCount() == 0) {
                            FaveFragment.this.videos.loadData();
                        }
                    default:
                }
            }
        }

        C14652() {
        }

        public void onPageSelected(int page) {
            if (FaveFragment.this.postedLoad != null) {
                FaveFragment.this.pager.removeCallbacks(FaveFragment.this.postedLoad);
            }
            FaveFragment.this.postedLoad = new C06771(page);
            FaveFragment.this.pager.postDelayed(FaveFragment.this.postedLoad, 300);
            FaveFragment.this.prev = page;
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    }

    private class FavePagerAdapter extends PagerAdapter {
        private FavePagerAdapter() {
        }

        public int getCount() {
            return 5;
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public Object instantiateItem(ViewGroup container, int position) {
            View v = null;
            switch (position) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    v = FaveFragment.this.users;
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    v = FaveFragment.this.posts;
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    v = FaveFragment.this.links;
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    if (FaveFragment.this.photos.getChildCount() == 0) {
                        Fragment pf = new PhotoListFragment();
                        Bundle args = new Bundle();
                        PhotoAlbum a = new PhotoAlbum();
                        a.id = -9001;
                        a.title = FaveFragment.this.getString(C0436R.string.fave_title);
                        a.numPhotos = 9000;
                        args.putParcelable("album", a);
                        args.putBoolean("nohead", true);
                        pf.setArguments(args);
                        FaveFragment.this.getFragmentManager().beginTransaction().add((int) C0436R.id.qwe, pf).commit();
                    }
                    v = FaveFragment.this.photos;
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    v = FaveFragment.this.videos;
                    break;
            }
            container.addView(v);
            return v;
        }

        public CharSequence getPageTitle(int pos) {
            return FaveFragment.this.getResources().getStringArray(C0436R.array.fave_tabs)[pos];
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            View v = null;
            switch (position) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    v = FaveFragment.this.users;
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    v = FaveFragment.this.posts;
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    v = FaveFragment.this.links;
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    v = FaveFragment.this.photos;
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    v = FaveFragment.this.videos;
                    break;
            }
            container.removeView(v);
        }
    }

    public FaveFragment() {
        this.usersLoaded = false;
        this.postsLoaded = false;
        this.prev = 0;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        SherlockFragmentActivity sa = getSherlockActivity();
        sa.getSupportActionBar().setNavigationMode(0);
        sa.getSupportActionBar().setDisplayShowTitleEnabled(true);
        act.setTitle(C0436R.string.fave_title);
        Bundle b1 = new Bundle();
        b1.putString("emptyText", getString(C0436R.string.no_users));
        Bundle b2 = new Bundle();
        b2.putString("emptyText", getString(C0436R.string.no_links));
        this.users = new UserListView(act, 4, b1);
        this.videos = new VideoListView(act, 0, 2, null, null, new C14641());
        this.photos = new FrameLayout(act);
        this.photos.setId(C0436R.id.qwe);
        this.posts = new NewsView((Context) act, true);
        this.links = new UserListView(act, 7, b2);
        this.posts.initFave();
        this.tabbar = new PagerSlidingTabStrip(act);
        this.pager = new ViewPager(act);
        this.pager.setAdapter(new FavePagerAdapter());
        this.tabbar.setViewPager(this.pager);
        this.tabbar.setOnPageChangeListener(new C14652());
        this.tabbar.setBackgroundResource(C0436R.color.tab_bg);
        this.tabbar.setIndicatorColorResource(C0436R.color.tab_indicator);
        this.users.loadData();
        this.contentView = new LinearLayout(act);
        this.contentView.setOrientation(1);
        this.contentView.addView(this.tabbar, new LayoutParams(-1, Global.scale(GalleryPickerFooterView.SIZE)));
        this.contentView.addView(this.pager, new LayoutParams(-1, -1));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentView;
    }
}
