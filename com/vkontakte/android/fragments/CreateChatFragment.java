package com.vkontakte.android.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnCloseListener;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.fragments.FriendListFragment.SelectionListener;
import com.vkontakte.android.ui.RefreshableListView;
import java.lang.reflect.Field;
import java.util.ArrayList;
import org.acra.ACRAConstants;

public class CreateChatFragment extends FriendListFragment {
    private BroadcastReceiver receiver;
    private SearchView searchView;
    private View sendBtn;

    /* renamed from: com.vkontakte.android.fragments.CreateChatFragment.1 */
    class C06471 extends BroadcastReceiver {
        C06471() {
        }

        public void onReceive(Context context, Intent intent) {
            if (Friends.ACTION_FRIEND_LIST_CHANGED.equals(intent.getAction())) {
                ArrayList<UserProfile> fl = new ArrayList();
                Friends.getFriends(fl);
                CreateChatFragment.this.setData(fl, true, false);
            }
            if (LongPollService.ACTION_USER_PRESENCE.equals(intent.getAction())) {
                CreateChatFragment.this.setUserOnline(intent.getIntExtra("uid", 0), intent.getIntExtra("online", 0));
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.CreateChatFragment.3 */
    class C06483 implements OnClickListener {
        C06483() {
        }

        public void onClick(View v) {
            if (CreateChatFragment.this.getSelectedUsers().size() >= 2) {
                Intent intent = new Intent();
                intent.putExtra("users", CreateChatFragment.this.getSelectedUsers());
                CreateChatFragment.this.getActivity().setResult(-1, intent);
                CreateChatFragment.this.getActivity().finish();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.CreateChatFragment.5 */
    class C06495 implements OnClickListener {
        C06495() {
        }

        public void onClick(View v) {
            CreateChatFragment.this.getSherlockActivity().getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.CreateChatFragment.7 */
    class C06507 implements OnClickListener {
        C06507() {
        }

        public void onClick(View v) {
            CreateChatFragment.this.getActivity().setResult(1);
            CreateChatFragment.this.getActivity().finish();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.CreateChatFragment.2 */
    class C14512 implements SelectionListener {
        C14512() {
        }

        public void onItemSelected(UserProfile p) {
            if (CreateChatFragment.this.getArguments().getBoolean("chat")) {
                int cnt = CreateChatFragment.this.getSelectedUsers().size();
                ((TextView) CreateChatFragment.this.sendBtn.findViewById(C0436R.id.ab_done_text)).setText(new StringBuilder(String.valueOf(CreateChatFragment.this.getResources().getString(C0436R.string.welcome_next))).append(cnt > 0 ? " (" + cnt + ")" : ACRAConstants.DEFAULT_STRING_VALUE).toString());
                if (cnt < 2) {
                    CreateChatFragment.this.sendBtn.setEnabled(false);
                    ((TextView) CreateChatFragment.this.sendBtn.findViewById(C0436R.id.ab_done_text)).setTextColor(-2130706433);
                    return;
                }
                CreateChatFragment.this.sendBtn.setEnabled(true);
                ((TextView) CreateChatFragment.this.sendBtn.findViewById(C0436R.id.ab_done_text)).setTextColor(-1);
                return;
            }
            Intent intent = new Intent();
            intent.putExtra("user", p);
            CreateChatFragment.this.getActivity().setResult(-1, intent);
            CreateChatFragment.this.getActivity().finish();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.CreateChatFragment.4 */
    class C14524 implements OnQueryTextListener {
        C14524() {
        }

        public boolean onQueryTextSubmit(String query) {
            View focus = CreateChatFragment.this.getActivity().getCurrentFocus();
            if (focus != null) {
                ((InputMethodManager) CreateChatFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(focus.getWindowToken(), 0);
                focus.clearFocus();
            }
            return false;
        }

        public boolean onQueryTextChange(String newText) {
            if (newText == null || newText.length() <= 0) {
                boolean ns = false;
            }
            CreateChatFragment.this.updateFilter(newText);
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.CreateChatFragment.6 */
    class C14536 implements OnCloseListener {
        C14536() {
        }

        public boolean onClose() {
            CreateChatFragment.this.getSherlockActivity().getSupportActionBar().setDisplayShowTitleEnabled(true);
            return false;
        }
    }

    public CreateChatFragment() {
        this.receiver = new C06471();
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        act.setTitle(!getArguments().getBoolean("chat") ? C0436R.string.open_chat : C0436R.string.create_conversation);
        ArrayList<UserProfile> fl = new ArrayList();
        Friends.getFriends(fl);
        setData(fl, true, false);
        setSelectionListener(new C14512());
        if (getArguments().getBoolean("chat")) {
            setMultiSelection();
            this.sendBtn = View.inflate(act, C0436R.layout.ab_done_right, null);
            ((TextView) this.sendBtn.findViewById(C0436R.id.ab_done_text)).setText(C0436R.string.welcome_next);
            this.sendBtn.setOnClickListener(new C06483());
            this.sendBtn.setEnabled(false);
            ((TextView) this.sendBtn.findViewById(C0436R.id.ab_done_text)).setTextColor(-2130706433);
            ArrayList<UserProfile> sel = getArguments().getParcelableArrayList("selected");
            if (sel != null) {
                setSelectedUsers(sel);
                int cnt = getSelectedUsers().size();
                ((TextView) this.sendBtn.findViewById(C0436R.id.ab_done_text)).setText(new StringBuilder(String.valueOf(getResources().getString(C0436R.string.welcome_next))).append(cnt > 0 ? " (" + cnt + ")" : ACRAConstants.DEFAULT_STRING_VALUE).toString());
                if (cnt < 2) {
                    this.sendBtn.setEnabled(false);
                    ((TextView) this.sendBtn.findViewById(C0436R.id.ab_done_text)).setTextColor(-2130706433);
                } else {
                    this.sendBtn.setEnabled(true);
                    ((TextView) this.sendBtn.findViewById(C0436R.id.ab_done_text)).setTextColor(-1);
                }
            }
        }
        this.searchView = new SearchView(getSherlockActivity().getSupportActionBar().getThemedContext());
        this.searchView.setQueryHint(getResources().getString(C0436R.string.search));
        try {
            Field searchField = SearchView.class.getDeclaredField("mSearchButton");
            searchField.setAccessible(true);
            ((ImageView) searchField.get(this.searchView)).setImageResource(C0436R.drawable.ic_ab_search);
            searchField = SearchView.class.getDeclaredField("mSearchPlate");
            searchField.setAccessible(true);
            ((TextView) ((LinearLayout) searchField.get(this.searchView)).getChildAt(0)).setHintTextColor(-2130706433);
        } catch (Exception e) {
        }
        this.searchView.setOnQueryTextListener(new C14524());
        this.searchView.setOnSearchClickListener(new C06495());
        this.searchView.setOnCloseListener(new C14536());
        setHasOptionsMenu(true);
    }

    public void beforeSetListAdapter(RefreshableListView list) {
        if (!getArguments().getBoolean("chat")) {
            View hdr = View.inflate(getActivity(), C0436R.layout.create_chat, null);
            hdr.setBackgroundResource(C0436R.drawable.highlight);
            list.addHeaderView(hdr, null, false);
            hdr.setOnClickListener(new C06507());
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        IntentFilter filter = new IntentFilter();
        filter.addAction(LongPollService.ACTION_USER_PRESENCE);
        filter.addAction(Friends.ACTION_FRIEND_LIST_CHANGED);
        VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
    }

    public void onDestroy() {
        super.onDestroy();
        VKApplication.context.unregisterReceiver(this.receiver);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem search = menu.add((int) C0436R.string.search);
        search.setShowAsAction(2);
        search.setActionView(this.searchView);
        if (this.sendBtn != null) {
            MenuItem item = menu.add((int) C0436R.string.send);
            item.setActionView(this.sendBtn);
            item.setShowAsAction(2);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }
}
