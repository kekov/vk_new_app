package com.vkontakte.android.fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.ExploreByTouchHelper;
import android.text.ClipboardManager;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.text.style.URLSpan;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.ActionMode.Callback;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.ChatUser;
import com.vkontakte.android.DocumentAttachment;
import com.vkontakte.android.EmojiPopup;
import com.vkontakte.android.EmojiPopup.StickerClickListener;
import com.vkontakte.android.ForwardMessageActivity;
import com.vkontakte.android.FwdMessagesAttachment;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageAttachment;
import com.vkontakte.android.ImageCache;
import com.vkontakte.android.Log;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.Message;
import com.vkontakte.android.Message.FwdMessage;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.PendingDocumentAttachment;
import com.vkontakte.android.PhotoAttachment;
import com.vkontakte.android.PostAttachment;
import com.vkontakte.android.StickerAttachment;
import com.vkontakte.android.UploaderService;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.ZhukovLayout;
import com.vkontakte.android.api.MessagesGetLastActivity;
import com.vkontakte.android.api.MessagesSetActivity;
import com.vkontakte.android.cache.Cache;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Friends.GetUsersCallback;
import com.vkontakte.android.data.Messages;
import com.vkontakte.android.data.Messages.GetChatUsersCallback;
import com.vkontakte.android.data.Messages.GetMessagesCallback;
import com.vkontakte.android.mediapicker.gl.GLFilterContext;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.PendingPhotoAttachment;
import com.vkontakte.android.ui.PhotoView;
import com.vkontakte.android.ui.WriteBar;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import org.acra.ACRAConstants;

public class ChatFragment extends SherlockFragment implements OnItemClickListener, OnItemLongClickListener, BackListener {
    private static final int FORWARD_RESULT = 200;
    public static ChatFragment activeInstance;
    private ActionMode actionMode;
    private Callback actionModeCallback;
    private MessagesAdapter adapter;
    private OnClickListener chatUserClickListener;
    private HashMap<Integer, UserProfile> chatUsers;
    private LinearLayout contentView;
    private boolean dataLoading;
    private EmojiPopup emojiPopup;
    private TextView emptyView;
    private ErrorView errorView;
    private OnClickListener fwdProfileClickListener;
    private ListImageLoaderWrapper imgLoader;
    private boolean isActive;
    private ArrayList<ListItem> items;
    private boolean keyboardVisible;
    private long lastTypingRequest;
    private ListView list;
    private FrameLayout listWrap;
    private View loadMoreView;
    private ArrayList<Message> messages;
    private ArrayList<Message> messagesToForward;
    private boolean moreAvailable;
    private View openChatBtn;
    private int peer;
    private boolean preloadOnReady;
    private ArrayList<Message> preloadedMessages;
    private boolean preloading;
    private ProgressBar progress;
    private BroadcastReceiver receiver;
    private ArrayList<Message> selectedMessages;
    private Timer typingTimer;
    private HashMap<Integer, Timer> typingTimers;
    private ArrayList<Integer> typingUsers;
    private LinearLayout typingView;
    private HashMap<Integer, String> userNamesAcc;
    private WriteBar writeBar;

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.1 */
    class C06171 extends BroadcastReceiver {
        C06171() {
        }

        public void onReceive(Context arg0, Intent intent) {
            if (ChatFragment.this.getActivity() != null) {
                int mid;
                Iterator it;
                Message msg;
                ListItem item;
                if (LongPollService.ACTION_MESSAGE_DELETED.equals(intent.getAction())) {
                    mid = intent.getIntExtra(LongPollService.EXTRA_MSG_ID, 0);
                    it = ChatFragment.this.messages.iterator();
                    while (it.hasNext()) {
                        msg = (Message) it.next();
                        if (msg.id == mid) {
                            ChatFragment.this.messages.remove(msg);
                            break;
                        }
                    }
                    ArrayList<ListItem> toRemove = new ArrayList();
                    it = ChatFragment.this.items.iterator();
                    while (it.hasNext()) {
                        item = (ListItem) it.next();
                        if (item.msgId == mid) {
                            toRemove.add(item);
                        }
                    }
                    ChatFragment.this.items.removeAll(toRemove);
                    ChatFragment.this.removeRepeatingDates();
                    ChatFragment.this.updateList();
                } else if (LongPollService.ACTION_MESSAGE_RSTATE_CHANGED.equals(intent.getAction())) {
                    mid = intent.getIntExtra(LongPollService.EXTRA_MSG_ID, 0);
                    boolean state = intent.getBooleanExtra(LongPollService.EXTRA_READ_STATE, false);
                    if (intent.hasExtra("le")) {
                        boolean in = intent.getBooleanExtra("in", false);
                        if (intent.getIntExtra(LongPollService.EXTRA_PEER_ID, 0) == ChatFragment.this.peer) {
                            it = ChatFragment.this.messages.iterator();
                            while (it.hasNext()) {
                                m = (Message) it.next();
                                if (m.id <= mid && m.out != in) {
                                    m.readState = state;
                                }
                            }
                            i = ChatFragment.this.list.getHeaderViewsCount();
                            r4 = ChatFragment.this.items.iterator();
                            while (r4.hasNext()) {
                                item = (ListItem) r4.next();
                                if (item.msgId <= mid && item.isOut != in) {
                                    item.readState = state;
                                    if (ChatFragment.this.list.getFirstVisiblePosition() <= i && ChatFragment.this.list.getLastVisiblePosition() >= i) {
                                        ChatFragment.this.list.getChildAt(i - ChatFragment.this.list.getFirstVisiblePosition()).setBackgroundColor(state ? 0 : -2694936);
                                    }
                                }
                                i++;
                            }
                            return;
                        }
                        return;
                    }
                    it = ChatFragment.this.messages.iterator();
                    while (it.hasNext()) {
                        m = (Message) it.next();
                        if (m.id == mid) {
                            m.readState = state;
                        }
                    }
                    i = ChatFragment.this.list.getHeaderViewsCount();
                    r4 = ChatFragment.this.items.iterator();
                    while (r4.hasNext()) {
                        item = (ListItem) r4.next();
                        if (item.msgId == mid) {
                            item.readState = state;
                            if (ChatFragment.this.list.getFirstVisiblePosition() <= i && ChatFragment.this.list.getLastVisiblePosition() >= i) {
                                ChatFragment.this.list.getChildAt(i - ChatFragment.this.list.getFirstVisiblePosition()).setBackgroundColor(state ? 0 : -2694936);
                            }
                        }
                        i++;
                    }
                } else if (LongPollService.ACTION_CHAT_CHANGED.equals(intent.getAction())) {
                    if (intent.getIntExtra("id", 0) + 2000000000 == ChatFragment.this.peer) {
                        ChatFragment.this.updateChatUsers(false);
                    }
                } else if (LongPollService.ACTION_NEW_MESSAGE.equals(intent.getAction())) {
                    if (!ChatFragment.this.dataLoading || ChatFragment.this.messages.size() != 0) {
                        if (intent.getIntExtra(LongPollService.EXTRA_PEER_ID, 0) == ChatFragment.this.peer) {
                            m = (Message) intent.getParcelableExtra(LongPollService.EXTRA_MESSAGE);
                            if (m.extras != null && m.extras.containsKey("action_mid")) {
                                ChatFragment.this.userNamesAcc.put(Integer.valueOf(m.extras.getInt("action_mid", 0)), intent.getStringExtra("action_user_name_acc"));
                            }
                            it = ChatFragment.this.messages.iterator();
                            while (it.hasNext()) {
                                if (((Message) it.next()).id == m.id) {
                                    return;
                                }
                            }
                            ChatFragment.this.messages.add(m);
                            ArrayList<Message> msgs = new ArrayList();
                            msgs.add(m);
                            ArrayList<Integer> fuids = new ArrayList();
                            ChatFragment.this.items.addAll(ChatFragment.this.buildItems(msgs, fuids, true, false));
                            ChatFragment.this.updateList();
                            if (ChatFragment.this.isActive) {
                                ChatFragment.this.markAsRead();
                            }
                            ChatFragment.this.loadFwdUsers(fuids);
                            if (ChatFragment.this.peer < 2000000000 && !m.out) {
                                ChatFragment.this.showTyping(false);
                            }
                            if (ChatFragment.this.peer > 2000000000 && !m.out) {
                                ChatFragment.this.typingUsers.remove(Integer.valueOf(m.sender));
                                ChatFragment.this.updateTyping();
                            }
                        }
                    }
                } else if (LongPollService.ACTION_TYPING.equals(intent.getAction())) {
                    if (intent.getIntExtra("uid", 0) != ChatFragment.this.peer) {
                        return;
                    }
                    if (ChatFragment.this.peer < 2000000000) {
                        ChatFragment.this.updateTyping();
                        ChatFragment.this.showTyping(true);
                        ChatFragment.this.restartTypingTimer();
                        return;
                    }
                    uid = intent.getIntExtra("user", 0);
                    if (!ChatFragment.this.typingUsers.contains(Integer.valueOf(uid))) {
                        ChatFragment.this.typingUsers.add(Integer.valueOf(uid));
                    }
                    if (!ChatFragment.this.chatUsers.containsKey(Integer.valueOf(uid)) && ChatFragment.this.chatUsers.size() > 0) {
                        ChatFragment.this.updateChatUsers(true);
                    }
                    ChatFragment.this.restartTypingTimer(uid);
                    ChatFragment.this.updateTyping();
                } else if (LongPollService.ACTION_USER_PRESENCE.equals(intent.getAction())) {
                    uid = intent.getIntExtra("uid", 0);
                    int online = intent.getIntExtra("online", 0);
                    if (uid == ChatFragment.this.peer) {
                        ChatFragment.this.updateOnline(online);
                    }
                } else if (Messages.ACTION_MESSAGE_ID_CHANGED.equals(intent.getAction())) {
                    mid = intent.getIntExtra("oldID", 0);
                    int nid = intent.getIntExtra("newID", 0);
                    it = ChatFragment.this.messages.iterator();
                    while (it.hasNext()) {
                        m = (Message) it.next();
                        if (m.id == mid) {
                            Log.m528i("vk", "set msg id " + mid + " -> " + nid);
                            m.id = nid;
                            ChatFragment.this.updateList();
                            break;
                        }
                    }
                    it = ChatFragment.this.items.iterator();
                    while (it.hasNext()) {
                        item = (ListItem) it.next();
                        if (item.msgId == mid) {
                            item.msgId = nid;
                        }
                    }
                    ChatFragment.this.updateList();
                    if (ChatFragment.this.peer == Global.uid) {
                        ChatFragment.this.showTyping(false);
                    }
                    ChatFragment.this.markAsRead();
                } else if (UploaderService.ACTION_UPLOAD_DONE.equals(intent.getAction())) {
                    Iterator it2;
                    Attachment a;
                    int sz;
                    id = intent.getIntExtra("id", 0);
                    msg = null;
                    r4 = ChatFragment.this.messages.iterator();
                    while (r4.hasNext()) {
                        m = (Message) r4.next();
                        i = 0;
                        it2 = m.attachments.iterator();
                        while (it2.hasNext()) {
                            a = (Attachment) it2.next();
                            if ((a instanceof PendingPhotoAttachment) && ((PendingPhotoAttachment) a).id == id) {
                                m.attachments.set(i, (PhotoAttachment) intent.getParcelableExtra("attachment"));
                                sz = Math.min(ChatFragment.this.contentView.getWidth(), Global.scale(350.0f));
                                ZhukovLayout.processThumbs(sz - Global.scale(100.0f), sz, m.attachments);
                                msg = m;
                                break;
                                continue;
                            } else if ((a instanceof PendingDocumentAttachment) && ((PendingDocumentAttachment) a).did == id) {
                                m.attachments.set(i, (DocumentAttachment) intent.getParcelableExtra("attachment"));
                                sz = Math.min(ChatFragment.this.contentView.getWidth(), Global.scale(350.0f));
                                ZhukovLayout.processThumbs(sz - Global.scale(100.0f), sz, m.attachments);
                                msg = m;
                                break;
                                continue;
                            } else {
                                i++;
                            }
                        }
                        if (msg != null) {
                            break;
                        }
                    }
                    if (msg != null) {
                        r4 = ChatFragment.this.items.iterator();
                        while (r4.hasNext()) {
                            item = (ListItem) r4.next();
                            i = 0;
                            it2 = item.attachments.iterator();
                            while (it2.hasNext()) {
                                a = (Attachment) it2.next();
                                if (!(a instanceof PendingPhotoAttachment) || ((PendingPhotoAttachment) a).id != id) {
                                    if ((a instanceof PendingDocumentAttachment) && ((PendingDocumentAttachment) a).did == id) {
                                        item.attachments.set(i, (DocumentAttachment) intent.getParcelableExtra("attachment"));
                                        sz = Math.min(ChatFragment.this.contentView.getWidth(), Global.scale(350.0f));
                                        ZhukovLayout.processThumbs(sz - Global.scale(100.0f), sz, item.attachments);
                                        break;
                                    }
                                    i++;
                                } else {
                                    item.attachments.set(i, (PhotoAttachment) intent.getParcelableExtra("attachment"));
                                    sz = Math.min(ChatFragment.this.contentView.getWidth(), Global.scale(350.0f));
                                    ZhukovLayout.processThumbs(sz - Global.scale(100.0f), sz, item.attachments);
                                    break;
                                }
                            }
                        }
                        ChatFragment.this.updateList();
                    }
                } else if (Messages.ACTION_SEND_FAILED.equals(intent.getAction())) {
                    id = intent.getIntExtra("id", 0);
                    it = ChatFragment.this.messages.iterator();
                    while (it.hasNext()) {
                        m = (Message) it.next();
                        if (m.id == id) {
                            m.sendFailed = true;
                            break;
                        }
                    }
                    i = 0;
                    it = ChatFragment.this.items.iterator();
                    while (it.hasNext()) {
                        item = (ListItem) it.next();
                        if (item.msgId == id && (item.type == 1 || item.type == 4)) {
                            if (intent.getBooleanExtra("privacy", false)) {
                                new Builder(ChatFragment.this.getActivity()).setTitle(C0436R.string.msg_not_sent_title).setMessage(C0436R.string.msg_not_sent).setPositiveButton(C0436R.string.ok, null).show();
                            } else {
                                Toast.makeText(ChatFragment.this.getActivity(), C0436R.string.msg_error, 0).show();
                            }
                            item.isFailed = true;
                            i += ChatFragment.this.list.getHeaderViewsCount();
                            if (ChatFragment.this.list.getFirstVisiblePosition() <= i && ChatFragment.this.list.getLastVisiblePosition() >= i) {
                                View v = ChatFragment.this.list.getChildAt(i - ChatFragment.this.list.getFirstVisiblePosition());
                                if (v.findViewById(C0436R.id.msg_failed) != null) {
                                    v.findViewById(C0436R.id.msg_time).setVisibility(8);
                                    v.findViewById(C0436R.id.msg_progress).setVisibility(8);
                                    v.findViewById(C0436R.id.msg_failed).setVisibility(0);
                                    if (VERSION.SDK_INT < 11) {
                                        ScaleAnimation sa = new ScaleAnimation(0.01f, 1.0f, 0.01f, 1.0f, 1, 0.5f, 1, 0.5f);
                                        sa.setInterpolator(new OvershootInterpolator());
                                        sa.setDuration(300);
                                        v.findViewById(C0436R.id.msg_failed).startAnimation(sa);
                                        return;
                                    }
                                    Animator animx = ObjectAnimator.ofFloat(v.findViewById(C0436R.id.msg_failed), "scaleX", new float[]{0.01f, 1.0f}).setDuration(300);
                                    Animator animy = ObjectAnimator.ofFloat(v.findViewById(C0436R.id.msg_failed), "scaleY", new float[]{0.01f, 1.0f}).setDuration(300);
                                    AnimatorSet set = new AnimatorSet();
                                    set.playTogether(new Animator[]{animx, animy});
                                    set.setInterpolator(new OvershootInterpolator());
                                    set.start();
                                    return;
                                }
                                return;
                            }
                        }
                        i++;
                    }
                    i += ChatFragment.this.list.getHeaderViewsCount();
                    if (ChatFragment.this.list.getFirstVisiblePosition() <= i) {
                    }
                } else if (LongPollService.ACTION_REFRESH_DIALOGS_LIST.equals(intent.getAction())) {
                    if (intent.hasExtra("affected_chats")) {
                        boolean found = false;
                        r4 = intent.getIntegerArrayListExtra("affected_chats").iterator();
                        while (r4.hasNext()) {
                            if (((Integer) r4.next()).intValue() == ChatFragment.this.peer) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            ArrayList<Integer> markedAsRead = intent.getIntegerArrayListExtra("marked_as_read");
                            ArrayList<Integer> markedAsUnread = intent.getIntegerArrayListExtra("marked_as_unread");
                            ArrayList<Integer> deleted = intent.getIntegerArrayListExtra("deleted");
                            Iterator<Message> itr = ChatFragment.this.messages.iterator();
                            while (itr.hasNext()) {
                                m = (Message) itr.next();
                                if (deleted.contains(Integer.valueOf(m.id))) {
                                    itr.remove();
                                } else {
                                    if (markedAsRead.contains(Integer.valueOf(m.id))) {
                                        m.readState = true;
                                    }
                                    if (markedAsUnread.contains(Integer.valueOf(m.id))) {
                                        m.readState = false;
                                    }
                                }
                            }
                            ChatFragment.this.rebuildItems();
                            ChatFragment.this.updateList();
                            return;
                        }
                    }
                    ChatFragment.this.messages.clear();
                    ChatFragment.this.items.clear();
                    ChatFragment.this.updateList();
                    ChatFragment.this.progress.setVisibility(0);
                    ChatFragment.this.listWrap.setVisibility(8);
                    ChatFragment.this.loadData();
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.21 */
    class AnonymousClass21 extends TimerTask {
        private final /* synthetic */ int val$uid;

        /* renamed from: com.vkontakte.android.fragments.ChatFragment.21.1 */
        class C06191 implements Runnable {
            C06191() {
            }

            public void run() {
                ChatFragment.this.updateTyping();
            }
        }

        AnonymousClass21(int i) {
            this.val$uid = i;
        }

        public void run() {
            ChatFragment.this.typingTimers.remove(Integer.valueOf(this.val$uid));
            ChatFragment.this.typingUsers.remove(Integer.valueOf(this.val$uid));
            ChatFragment.this.typingView.post(new C06191());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.22 */
    class AnonymousClass22 extends ImageView {
        AnonymousClass22(Context $anonymous0) {
            super($anonymous0);
        }

        public void onMeasure(int wms, int hms) {
            int h = MeasureSpec.getSize(hms);
            setMeasuredDimension(h, h);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.24 */
    class AnonymousClass24 implements Runnable {
        private final /* synthetic */ ImageView val$actionView;

        /* renamed from: com.vkontakte.android.fragments.ChatFragment.24.1 */
        class C06201 implements Runnable {
            private final /* synthetic */ ImageView val$actionView;
            private final /* synthetic */ Bitmap val$bmp;

            C06201(ImageView imageView, Bitmap bitmap) {
                this.val$actionView = imageView;
                this.val$bmp = bitmap;
            }

            public void run() {
                this.val$actionView.setImageBitmap(this.val$bmp);
            }
        }

        AnonymousClass24(ImageView imageView) {
            this.val$actionView = imageView;
        }

        public void run() {
            Bitmap bmp = ImageCache.get(ChatFragment.this.getArguments().getCharSequence("photo").toString());
            if (ChatFragment.this.getActivity() != null) {
                ChatFragment.this.getActivity().runOnUiThread(new C06201(this.val$actionView, bmp));
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.29 */
    class AnonymousClass29 implements DialogInterface.OnClickListener {
        private final /* synthetic */ Message val$msg;

        AnonymousClass29(Message message) {
            this.val$msg = message;
        }

        public void onClick(DialogInterface dialog, int which) {
            ArrayList<Message> msgs;
            switch (which) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    if (this.val$msg.sendFailed) {
                        ChatFragment.this.retryFailed(this.val$msg);
                        return;
                    }
                    msgs = new ArrayList();
                    msgs.add(this.val$msg);
                    ChatFragment.this.forward(msgs);
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    ((ClipboardManager) ChatFragment.this.getActivity().getSystemService("clipboard")).setText(this.val$msg.text);
                    Toast.makeText(ChatFragment.this.getActivity(), C0436R.string.text_copied, 0).show();
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    msgs = new ArrayList();
                    msgs.add(this.val$msg);
                    ChatFragment.this.confirmAndDelete(msgs);
                default:
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.2 */
    class C06252 implements OnClickListener {
        C06252() {
        }

        public void onClick(View v) {
            int mid = ((Integer) v.getTag()).intValue();
            Iterator it = ChatFragment.this.messages.iterator();
            while (it.hasNext()) {
                Message msg = (Message) it.next();
                if (msg.id == mid) {
                    Bundle args = new Bundle();
                    args.putInt("id", msg.sender);
                    Navigate.to("ProfileFragment", args, ChatFragment.this.getActivity());
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.30 */
    class AnonymousClass30 implements DialogInterface.OnClickListener {
        private final /* synthetic */ ArrayList val$msgs;

        AnonymousClass30(ArrayList arrayList) {
            this.val$msgs = arrayList;
        }

        public void onClick(DialogInterface dialog, int which) {
            ArrayList<Integer> mids = new ArrayList();
            Iterator it = this.val$msgs.iterator();
            while (it.hasNext()) {
                mids.add(Integer.valueOf(((Message) it.next()).id));
            }
            Messages.delete(mids);
            ChatFragment.this.messages.removeAll(this.val$msgs);
            ArrayList<ListItem> toRemove = new ArrayList();
            it = ChatFragment.this.items.iterator();
            while (it.hasNext()) {
                ListItem item = (ListItem) it.next();
                if (mids.contains(Integer.valueOf(item.msgId))) {
                    toRemove.add(item);
                }
            }
            ChatFragment.this.items.removeAll(toRemove);
            ChatFragment.this.updateList();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.4 */
    class C06274 implements OnClickListener {
        C06274() {
        }

        public void onClick(View v) {
            Bundle args = new Bundle();
            args.putInt("id", ((Integer) v.getTag()).intValue());
            Navigate.to("ProfileFragment", args, ChatFragment.this.getActivity());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.5 */
    class C06285 extends LinearLayout {
        C06285(Context $anonymous0) {
            super($anonymous0);
        }

        public void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            Rect rect = new Rect();
            ChatFragment.this.getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
            int diff = (ChatFragment.this.getActivity().getWindow().getDecorView().getHeight() - rect.top) - (h + ChatFragment.this.getSherlockActivity().getSupportActionBar().getHeight());
            ChatFragment.this.emojiPopup.onKeyboardStateChanged(diff > Global.scale(100.0f), diff);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.7 */
    class C06297 implements OnClickListener {
        C06297() {
        }

        public void onClick(View v) {
            Bundle args = (Bundle) ChatFragment.this.getArguments().clone();
            args.remove("from_search");
            Navigate.to("ChatFragment", args, ChatFragment.this.getActivity());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.8 */
    class C06308 implements OnClickListener {
        C06308() {
        }

        public void onClick(View v) {
            ChatFragment.this.loadMoreView.findViewById(C0436R.id.load_more_progress).setVisibility(0);
            ChatFragment.this.loadMoreView.findViewById(C0436R.id.load_more_btn).setVisibility(4);
            ChatFragment.this.loadData();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.9 */
    class C06319 implements OnScrollListener {
        C06319() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem == 0 && !ChatFragment.this.dataLoading && ChatFragment.this.messages.size() > 0) {
                if ((!ChatFragment.this.dataLoading || ChatFragment.this.preloading) && ChatFragment.this.moreAvailable) {
                    if (ChatFragment.this.preloading) {
                        ChatFragment.this.preloading = false;
                        ChatFragment.this.preloadOnReady = true;
                    } else if (ChatFragment.this.preloadedMessages.size() > 0) {
                        ChatFragment.this.prependMessages(ChatFragment.this.preloadedMessages);
                        ChatFragment.this.preloadedMessages.clear();
                        ChatFragment.this.preloading = true;
                        ChatFragment.this.loadData();
                    } else {
                        ChatFragment.this.loadData();
                    }
                    ChatFragment.this.loadMoreView.findViewById(C0436R.id.load_more_progress).setVisibility(0);
                    ChatFragment.this.loadMoreView.findViewById(C0436R.id.load_more_btn).setVisibility(4);
                }
            }
        }
    }

    private class Image {
        String url;
        int viewId;

        private Image() {
        }
    }

    private class ListItem {
        static final int TYPE_BOTTOM = 4;
        static final int TYPE_FULL = 1;
        static final int TYPE_MIDDLE = 3;
        static final int TYPE_SERVICE = 5;
        static final int TYPE_TOP = 2;
        ArrayList<Attachment> attachments;
        int fwdLevel;
        String fwdName;
        int fwdTime;
        int fwdUid;
        boolean hasLinks;
        ArrayList<Image> images;
        boolean isFailed;
        boolean isOut;
        int msgId;
        boolean readState;
        CharSequence text;
        int time;
        int type;

        private ListItem() {
        }
    }

    private class MessagesAdapter extends BaseAdapter {

        /* renamed from: com.vkontakte.android.fragments.ChatFragment.MessagesAdapter.1 */
        class C06321 implements OnClickListener {
            private final /* synthetic */ int val$photoIdx;
            private final /* synthetic */ ArrayList val$photos;

            C06321(ArrayList arrayList, int i) {
                this.val$photos = arrayList;
                this.val$photoIdx = i;
            }

            public void onClick(View v) {
                ViewGroup attachContainer = (ViewGroup) v.getParent();
                Drawable d = ((ImageView) v).getDrawable();
                if (d != null && (d instanceof BitmapDrawable)) {
                    PhotoViewerFragment.sharedThumb = ((BitmapDrawable) d).getBitmap();
                }
                Bundle args = new Bundle();
                args.putParcelableArrayList("list", this.val$photos);
                args.putInt("orientation", this.val$photoIdx);
                args.putInt(GLFilterContext.AttributePosition, this.val$photoIdx);
                Navigate.to("PhotoViewerFragment", args, ChatFragment.this.getActivity(), true, -1, -1);
            }
        }

        private MessagesAdapter() {
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public boolean isEnabled(int pos) {
            return ((ListItem) ChatFragment.this.items.get(pos)).type != 5;
        }

        public int getCount() {
            return ChatFragment.this.items.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public int getViewTypeCount() {
            return 5;
        }

        public int getItemViewType(int position) {
            ListItem item = (ListItem) ChatFragment.this.items.get(position);
            if (item.type == 5) {
                return 4;
            }
            return item.fwdLevel > 0 ? item.isOut ? 2 : 3 : ((ListItem) ChatFragment.this.items.get(position)).isOut ? 0 : 1;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.view.View getView(int r37, android.view.View r38, android.view.ViewGroup r39) {
            /*
            r36 = this;
            r29 = r38;
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r32 = r32.items;
            r0 = r32;
            r1 = r37;
            r18 = r0.get(r1);
            r18 = (com.vkontakte.android.fragments.ChatFragment.ListItem) r18;
            if (r29 != 0) goto L_0x0039;
        L_0x0018:
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            r33 = 5;
            r0 = r32;
            r1 = r33;
            if (r0 != r1) goto L_0x00e4;
        L_0x0026:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r32 = r32.getActivity();
            r33 = 2130903125; // 0x7f030055 float:1.741306E38 double:1.0528060287E-314;
            r34 = 0;
            r29 = android.view.View.inflate(r32, r33, r34);
        L_0x0039:
            r32 = 2131296496; // 0x7f0900f0 float:1.821091E38 double:1.05300038E-314;
            r0 = r29;
            r1 = r32;
            r7 = r0.findViewById(r1);
            r7 = (android.view.ViewGroup) r7;
            r14 = 0;
        L_0x0047:
            r32 = r7.getChildCount();
            r0 = r32;
            if (r14 < r0) goto L_0x0186;
        L_0x004f:
            r13 = 0;
            r7.removeAllViews();
            r24 = new java.util.ArrayList;
            r24.<init>();
            r8 = new java.util.ArrayList;
            r8.<init>();
            r22 = 0;
            r0 = r18;
            r0 = r0.attachments;
            r32 = r0;
            r33 = r32.iterator();
        L_0x0069:
            r32 = r33.hasNext();
            if (r32 != 0) goto L_0x01ad;
        L_0x006f:
            r32 = r8.size();
            if (r32 <= 0) goto L_0x0086;
        L_0x0075:
            r14 = 0;
            r0 = r18;
            r0 = r0.attachments;
            r32 = r0;
            r33 = r32.iterator();
        L_0x0080:
            r32 = r33.hasNext();
            if (r32 != 0) goto L_0x021d;
        L_0x0086:
            r17 = 0;
            r15 = 0;
            r26 = 0;
            r0 = r18;
            r0 = r0.attachments;
            r32 = r0;
            r33 = r32.iterator();
        L_0x0095:
            r32 = r33.hasNext();
            if (r32 != 0) goto L_0x024b;
        L_0x009b:
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            r33 = 5;
            r0 = r32;
            r1 = r33;
            if (r0 != r1) goto L_0x02fe;
        L_0x00a9:
            r0 = r18;
            r0 = r0.text;
            r32 = r0;
            if (r32 == 0) goto L_0x02bd;
        L_0x00b1:
            r32 = 2131296495; // 0x7f0900ef float:1.8210908E38 double:1.0530003793E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r32 = (android.widget.TextView) r32;
            r0 = r18;
            r0 = r0.text;
            r33 = r0;
            r32.setText(r33);
        L_0x00c7:
            r0 = r18;
            r0 = r0.attachments;
            r32 = r0;
            r32 = r32.size();
            if (r32 <= 0) goto L_0x02d9;
        L_0x00d3:
            r32 = 0;
        L_0x00d5:
            r0 = r32;
            r7.setVisibility(r0);
            r14 = 0;
        L_0x00db:
            r32 = r7.getChildCount();
            r0 = r32;
            if (r14 < r0) goto L_0x02dd;
        L_0x00e3:
            return r29;
        L_0x00e4:
            r0 = r18;
            r0 = r0.fwdLevel;
            r32 = r0;
            if (r32 != 0) goto L_0x013e;
        L_0x00ec:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r33 = r32.getActivity();
            r0 = r18;
            r0 = r0.isOut;
            r32 = r0;
            if (r32 == 0) goto L_0x013a;
        L_0x00fe:
            r32 = 2130903123; // 0x7f030053 float:1.7413055E38 double:1.0528060277E-314;
        L_0x0101:
            r34 = 0;
            r0 = r33;
            r1 = r32;
            r2 = r34;
            r29 = android.view.View.inflate(r0, r1, r2);
        L_0x010d:
            r0 = r18;
            r0 = r0.isOut;
            r32 = r0;
            if (r32 != 0) goto L_0x0164;
        L_0x0115:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r32 = r32.peer;
            r33 = 2000000000; // 0x77359400 float:3.682842E33 double:9.881312917E-315;
            r0 = r32;
            r1 = r33;
            if (r0 >= r1) goto L_0x0164;
        L_0x0128:
            r32 = 2131296497; // 0x7f0900f1 float:1.8210912E38 double:1.0530003803E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 8;
            r32.setVisibility(r33);
            goto L_0x0039;
        L_0x013a:
            r32 = 2130903121; // 0x7f030051 float:1.7413051E38 double:1.0528060267E-314;
            goto L_0x0101;
        L_0x013e:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r33 = r32.getActivity();
            r0 = r18;
            r0 = r0.isOut;
            r32 = r0;
            if (r32 == 0) goto L_0x0160;
        L_0x0150:
            r32 = 2130903124; // 0x7f030054 float:1.7413057E38 double:1.052806028E-314;
        L_0x0153:
            r34 = 0;
            r0 = r33;
            r1 = r32;
            r2 = r34;
            r29 = android.view.View.inflate(r0, r1, r2);
            goto L_0x010d;
        L_0x0160:
            r32 = 2130903122; // 0x7f030052 float:1.7413053E38 double:1.052806027E-314;
            goto L_0x0153;
        L_0x0164:
            r0 = r18;
            r0 = r0.isOut;
            r32 = r0;
            if (r32 != 0) goto L_0x0039;
        L_0x016c:
            r32 = 2131296497; // 0x7f0900f1 float:1.8210912E38 double:1.0530003803E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r33 = r0;
            r33 = r33.chatUserClickListener;
            r32.setOnClickListener(r33);
            goto L_0x0039;
        L_0x0186:
            r9 = r7.getChildAt(r14);
            r32 = r9.getTag();
            if (r32 == 0) goto L_0x01a9;
        L_0x0190:
            r32 = r9.getTag();
            r0 = r32;
            r0 = r0 instanceof java.lang.String;
            r32 = r0;
            if (r32 == 0) goto L_0x01a9;
        L_0x019c:
            r32 = r9.getTag();
            r32 = r32.toString();
            r0 = r32;
            com.vkontakte.android.Attachment.reuseView(r9, r0);
        L_0x01a9:
            r14 = r14 + 1;
            goto L_0x0047;
        L_0x01ad:
            r5 = r33.next();
            r5 = (com.vkontakte.android.Attachment) r5;
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r32 = r32.getActivity();
            r0 = r32;
            r28 = r5.getFullView(r0);
            r0 = r28;
            r7.addView(r0);
            r0 = r5 instanceof com.vkontakte.android.ThumbAttachment;
            r32 = r0;
            if (r32 == 0) goto L_0x01cf;
        L_0x01ce:
            r13 = 1;
        L_0x01cf:
            r0 = r5 instanceof com.vkontakte.android.PhotoAttachment;
            r32 = r0;
            if (r32 == 0) goto L_0x0207;
        L_0x01d5:
            r0 = r5 instanceof com.vkontakte.android.AlbumAttachment;
            r32 = r0;
            if (r32 != 0) goto L_0x0207;
        L_0x01db:
            r23 = r22;
            r22 = r22 + 1;
            r34 = new com.vkontakte.android.Photo;
            r32 = r5;
            r32 = (com.vkontakte.android.PhotoAttachment) r32;
            r0 = r34;
            r1 = r32;
            r0.<init>(r1);
            r0 = r24;
            r1 = r34;
            r0.add(r1);
            r32 = new com.vkontakte.android.fragments.ChatFragment$MessagesAdapter$1;
            r0 = r32;
            r1 = r36;
            r2 = r24;
            r3 = r23;
            r0.<init>(r2, r3);
            r0 = r28;
            r1 = r32;
            r0.setOnClickListener(r1);
        L_0x0207:
            r0 = r5 instanceof com.vkontakte.android.AudioAttachment;
            r32 = r0;
            if (r32 == 0) goto L_0x0069;
        L_0x020d:
            r32 = new com.vkontakte.android.AudioFile;
            r5 = (com.vkontakte.android.AudioAttachment) r5;
            r0 = r32;
            r0.<init>(r5);
            r0 = r32;
            r8.add(r0);
            goto L_0x0069;
        L_0x021d:
            r5 = r33.next();
            r5 = (com.vkontakte.android.Attachment) r5;
            r0 = r5 instanceof com.vkontakte.android.AudioAttachment;
            r32 = r0;
            if (r32 == 0) goto L_0x0080;
        L_0x0229:
            r32 = r5;
            r32 = (com.vkontakte.android.AudioAttachment) r32;
            r0 = r32;
            r0.playlistPos = r14;
            r5 = (com.vkontakte.android.AudioAttachment) r5;
            r32 = 0;
            r0 = r32;
            r0 = new com.vkontakte.android.AudioFile[r0];
            r32 = r0;
            r0 = r32;
            r32 = r8.toArray(r0);
            r32 = (com.vkontakte.android.AudioFile[]) r32;
            r0 = r32;
            r5.playlist = r0;
            r14 = r14 + 1;
            goto L_0x0080;
        L_0x024b:
            r5 = r33.next();
            r5 = (com.vkontakte.android.Attachment) r5;
            r0 = r5 instanceof com.vkontakte.android.ImageAttachment;
            r32 = r0;
            if (r32 == 0) goto L_0x02af;
        L_0x0257:
            r32 = 2131296496; // 0x7f0900f0 float:1.821091E38 double:1.05300038E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r32 = (android.view.ViewGroup) r32;
            r0 = r32;
            r9 = r0.getChildAt(r15);
            r32 = r5;
            r32 = (com.vkontakte.android.ImageAttachment) r32;
            r27 = r32.getImageURL();
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r32 = r32.imgLoader;
            r0 = r32;
            r1 = r27;
            r32 = r0.isAlreadyLoaded(r1);
            if (r32 == 0) goto L_0x02b3;
        L_0x0286:
            r32 = r5;
            r32 = (com.vkontakte.android.ImageAttachment) r32;
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r34 = r0;
            r34 = r34.imgLoader;
            r0 = r34;
            r1 = r27;
            r34 = r0.get(r1);
            r35 = 1;
            r0 = r32;
            r1 = r34;
            r2 = r35;
            r0.setImage(r9, r1, r2);
        L_0x02a7:
            r0 = r5 instanceof com.vkontakte.android.StickerAttachment;
            r32 = r0;
            if (r32 == 0) goto L_0x02af;
        L_0x02ad:
            r26 = r9;
        L_0x02af:
            r15 = r15 + 1;
            goto L_0x0095;
        L_0x02b3:
            r32 = r5;
            r32 = (com.vkontakte.android.ImageAttachment) r32;
            r0 = r32;
            r0.clearImage(r9);
            goto L_0x02a7;
        L_0x02bd:
            r32 = 2131296495; // 0x7f0900ef float:1.8210908E38 double:1.0530003793E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r32 = (android.widget.TextView) r32;
            r0 = r18;
            r0 = r0.time;
            r33 = r0;
            r33 = com.vkontakte.android.Global.langDateDay(r33);
            r32.setText(r33);
            goto L_0x00c7;
        L_0x02d9:
            r32 = 8;
            goto L_0x00d5;
        L_0x02dd:
            r32 = r7.getChildAt(r14);
            r19 = r32.getLayoutParams();
            r19 = (com.vkontakte.android.ui.FlowLayout.LayoutParams) r19;
            r32 = 0;
            r0 = r32;
            r1 = r19;
            r1.vertical_spacing = r0;
            r32 = r7.getChildAt(r14);
            r0 = r32;
            r1 = r19;
            r0.setLayoutParams(r1);
            r14 = r14 + 1;
            goto L_0x00db;
        L_0x02fe:
            r32 = 2131296495; // 0x7f0900ef float:1.8210908E38 double:1.0530003793E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r32 = (android.widget.TextView) r32;
            r0 = r18;
            r0 = r0.text;
            r33 = r0;
            r32.setText(r33);
            r32 = 2131296495; // 0x7f0900ef float:1.8210908E38 double:1.0530003793E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r32 = (android.widget.TextView) r32;
            r0 = r18;
            r0 = r0.hasLinks;
            r33 = r0;
            if (r33 == 0) goto L_0x067e;
        L_0x0329:
            r33 = android.text.method.LinkMovementMethod.getInstance();
        L_0x032d:
            r32.setMovementMethod(r33);
            r32 = 2131296495; // 0x7f0900ef float:1.8210908E38 double:1.0530003793E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 0;
            r32.setFocusable(r33);
            r32 = 2131296499; // 0x7f0900f3 float:1.8210916E38 double:1.0530003813E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r32 = (android.widget.TextView) r32;
            r0 = r18;
            r0 = r0.time;
            r33 = r0;
            r33 = com.vkontakte.android.Global.time(r33);
            r32.setText(r33);
            r0 = r18;
            r0 = r0.isOut;
            r32 = r0;
            if (r32 == 0) goto L_0x0720;
        L_0x0362:
            r0 = r18;
            r0 = r0.time;
            r32 = r0;
            if (r32 <= 0) goto L_0x06ee;
        L_0x036a:
            r0 = r18;
            r0 = r0.msgId;
            r32 = r0;
            if (r32 >= 0) goto L_0x0682;
        L_0x0372:
            r0 = r18;
            r0 = r0.isFailed;
            r32 = r0;
            if (r32 != 0) goto L_0x0682;
        L_0x037a:
            r32 = 2131296499; // 0x7f0900f3 float:1.8210916E38 double:1.0530003813E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 8;
            r32.setVisibility(r33);
            r32 = 2131296501; // 0x7f0900f5 float:1.821092E38 double:1.053000382E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 8;
            r32.setVisibility(r33);
            r32 = 2131296500; // 0x7f0900f4 float:1.8210918E38 double:1.0530003818E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 0;
            r32.setVisibility(r33);
        L_0x03aa:
            r6 = r7.getLayoutParams();
            r6 = (android.view.ViewGroup.MarginLayoutParams) r6;
            if (r13 == 0) goto L_0x075d;
        L_0x03b2:
            r0 = r18;
            r0 = r0.text;
            r32 = r0;
            r32 = r32.length();
            if (r32 != 0) goto L_0x075d;
        L_0x03be:
            r32 = 0;
        L_0x03c0:
            r0 = r32;
            r6.topMargin = r0;
            r32 = 2131296495; // 0x7f0900ef float:1.8210908E38 double:1.0530003793E-314;
            r0 = r29;
            r1 = r32;
            r33 = r0.findViewById(r1);
            r0 = r18;
            r0 = r0.text;
            r32 = r0;
            r32 = r32.length();
            if (r32 <= 0) goto L_0x0765;
        L_0x03db:
            r32 = 0;
        L_0x03dd:
            r0 = r33;
            r1 = r32;
            r0.setVisibility(r1);
            r0 = r18;
            r0 = r0.isOut;
            r32 = r0;
            if (r32 != 0) goto L_0x0404;
        L_0x03ec:
            r32 = 2131296497; // 0x7f0900f1 float:1.8210912E38 double:1.0530003803E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r0 = r18;
            r0 = r0.msgId;
            r33 = r0;
            r33 = java.lang.Integer.valueOf(r33);
            r32.setTag(r33);
        L_0x0404:
            r0 = r18;
            r0 = r0.images;
            r32 = r0;
            r33 = r32.iterator();
        L_0x040e:
            r32 = r33.hasNext();
            if (r32 != 0) goto L_0x0769;
        L_0x0414:
            r0 = r18;
            r0 = r0.attachments;
            r32 = r0;
            r32 = r32.size();
            r33 = 1;
            r0 = r32;
            r1 = r33;
            if (r0 != r1) goto L_0x043c;
        L_0x0426:
            r0 = r18;
            r0 = r0.attachments;
            r32 = r0;
            r33 = 0;
            r32 = r32.get(r33);
            r0 = r32;
            r0 = r0 instanceof com.vkontakte.android.StickerAttachment;
            r32 = r0;
            if (r32 == 0) goto L_0x043c;
        L_0x043a:
            r17 = 1;
        L_0x043c:
            r0 = r18;
            r0 = r0.readState;
            r32 = r0;
            if (r32 == 0) goto L_0x07d2;
        L_0x0444:
            r32 = 0;
        L_0x0446:
            r0 = r29;
            r1 = r32;
            r0.setBackgroundColor(r1);
            r0 = r18;
            r0 = r0.isOut;
            r32 = r0;
            if (r32 == 0) goto L_0x084e;
        L_0x0455:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r0 = r18;
            r0 = r0.msgId;
            r33 = r0;
            r32 = r32.isSelected(r33);
            if (r32 == 0) goto L_0x07d7;
        L_0x0467:
            r10 = 2130837669; // 0x7f0200a5 float:1.7280299E38 double:1.052773689E-314;
        L_0x046a:
            r32 = 1082130432; // 0x40800000 float:4.0 double:5.34643471E-315;
            r21 = com.vkontakte.android.Global.scale(r32);
            r32 = 1082130432; // 0x40800000 float:4.0 double:5.34643471E-315;
            r20 = com.vkontakte.android.Global.scale(r32);
            r31 = -1;
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            switch(r32) {
                case 2: goto L_0x07dc;
                case 3: goto L_0x0827;
                case 4: goto L_0x080a;
                default: goto L_0x0481;
            };
        L_0x0481:
            r32 = 2131296498; // 0x7f0900f2 float:1.8210914E38 double:1.053000381E-314;
            r0 = r29;
            r1 = r32;
            r30 = r0.findViewById(r1);
            r0 = r30;
            r0.setBackgroundResource(r10);
            r33 = r30.getPaddingLeft();
            r34 = r30.getPaddingTop();
            r35 = r30.getPaddingRight();
            r32 = -1;
            r0 = r31;
            r1 = r32;
            if (r0 != r1) goto L_0x0846;
        L_0x04a5:
            r32 = r30.getPaddingBottom();
        L_0x04a9:
            r0 = r30;
            r1 = r33;
            r2 = r34;
            r3 = r35;
            r4 = r32;
            r0.setPadding(r1, r2, r3, r4);
            r32 = 0;
            r33 = 0;
            r0 = r29;
            r1 = r32;
            r2 = r21;
            r3 = r33;
            r4 = r20;
            r0.setPadding(r1, r2, r3, r4);
            r32 = 2131296498; // 0x7f0900f2 float:1.8210914E38 double:1.053000381E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r19 = r32.getLayoutParams();
            r19 = (android.widget.LinearLayout.LayoutParams) r19;
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            r33 = 1;
            r0 = r32;
            r1 = r33;
            if (r0 == r1) goto L_0x084a;
        L_0x04e6:
            r32 = 1;
        L_0x04e8:
            r0 = r32;
            r0 = (float) r0;
            r32 = r0;
            r0 = r32;
            r1 = r19;
            r1.weight = r0;
            r32 = 2131296498; // 0x7f0900f2 float:1.8210914E38 double:1.053000381E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r0 = r32;
            r1 = r19;
            r0.setLayoutParams(r1);
        L_0x0505:
            if (r17 == 0) goto L_0x0541;
        L_0x0507:
            r0 = r18;
            r0 = r0.fwdLevel;
            r32 = r0;
            if (r32 != 0) goto L_0x0541;
        L_0x050f:
            r32 = 2131296498; // 0x7f0900f2 float:1.8210914E38 double:1.053000381E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = new android.graphics.drawable.ColorDrawable;
            r34 = 0;
            r33.<init>(r34);
            r32.setBackgroundDrawable(r33);
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r0 = r18;
            r0 = r0.msgId;
            r33 = r0;
            r32 = r32.isSelected(r33);
            if (r32 == 0) goto L_0x0977;
        L_0x0536:
            r26 = (android.widget.ImageView) r26;
            r32 = 855638016; // 0x33000000 float:2.9802322E-8 double:4.22741349E-315;
            r0 = r26;
            r1 = r32;
            r0.setColorFilter(r1);
        L_0x0541:
            r0 = r18;
            r0 = r0.fwdLevel;
            r32 = r0;
            if (r32 <= 0) goto L_0x0631;
        L_0x0549:
            r32 = 2131296493; // 0x7f0900ed float:1.8210904E38 double:1.0530003783E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r32 = (android.widget.TextView) r32;
            r0 = r18;
            r0 = r0.fwdName;
            r33 = r0;
            r32.setText(r33);
            r32 = 2131296494; // 0x7f0900ee float:1.8210906E38 double:1.053000379E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r32 = (android.widget.TextView) r32;
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r33 = r0;
            r33 = r33.getResources();
            r0 = r18;
            r0 = r0.fwdTime;
            r34 = r0;
            r33 = com.vkontakte.android.Global.langDate(r33, r34);
            r32.setText(r33);
            r32 = 2131296491; // 0x7f0900eb float:1.82109E38 double:1.0530003773E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r32 = (com.vkontakte.android.ui.FwdMessageLevelView) r32;
            r0 = r18;
            r0 = r0.fwdLevel;
            r33 = r0;
            r32.setLevel(r33);
            r32 = 2131296490; // 0x7f0900ea float:1.8210898E38 double:1.053000377E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r0 = r18;
            r0 = r0.fwdUid;
            r33 = r0;
            r33 = java.lang.Integer.valueOf(r33);
            r32.setTag(r33);
            r32 = 2131296490; // 0x7f0900ea float:1.8210898E38 double:1.053000377E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r33 = r0;
            r33 = r33.fwdProfileClickListener;
            r32.setOnClickListener(r33);
            r32 = 2131296491; // 0x7f0900eb float:1.82109E38 double:1.0530003773E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r11 = r32.getLayoutParams();
            r11 = (android.widget.RelativeLayout.LayoutParams) r11;
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            r33 = 4;
            r0 = r32;
            r1 = r33;
            if (r0 != r1) goto L_0x0984;
        L_0x05e8:
            r32 = 0;
            r0 = r32;
            r11.topMargin = r0;
            r32 = 1077936128; // 0x40400000 float:3.0 double:5.325712093E-315;
            r32 = com.vkontakte.android.Global.scale(r32);
            r0 = r32;
            r11.bottomMargin = r0;
        L_0x05f8:
            r32 = 2131296491; // 0x7f0900eb float:1.82109E38 double:1.0530003773E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r0 = r32;
            r0.setLayoutParams(r11);
            r32 = 2131296490; // 0x7f0900ea float:1.8210898E38 double:1.053000377E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r12 = r32.getLayoutParams();
            r12 = (android.widget.RelativeLayout.LayoutParams) r12;
            r0 = r11.topMargin;
            r32 = r0;
            r0 = r32;
            r12.topMargin = r0;
            r32 = 2131296490; // 0x7f0900ea float:1.8210898E38 double:1.053000377E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r0 = r32;
            r0.setLayoutParams(r12);
        L_0x0631:
            r0 = r18;
            r0 = r0.isOut;
            r32 = r0;
            if (r32 != 0) goto L_0x00e3;
        L_0x0639:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r32 = r32.peer;
            r33 = 2000000000; // 0x77359400 float:3.682842E33 double:9.881312917E-315;
            r0 = r32;
            r1 = r33;
            if (r0 <= r1) goto L_0x00e3;
        L_0x064c:
            r32 = 2131296497; // 0x7f0900f1 float:1.8210912E38 double:1.0530003803E-314;
            r0 = r29;
            r1 = r32;
            r33 = r0.findViewById(r1);
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            r34 = 4;
            r0 = r32;
            r1 = r34;
            if (r0 == r1) goto L_0x0673;
        L_0x0665:
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            r34 = 1;
            r0 = r32;
            r1 = r34;
            if (r0 != r1) goto L_0x0a12;
        L_0x0673:
            r32 = 0;
        L_0x0675:
            r0 = r33;
            r1 = r32;
            r0.setVisibility(r1);
            goto L_0x00e3;
        L_0x067e:
            r33 = 0;
            goto L_0x032d;
        L_0x0682:
            r0 = r18;
            r0 = r0.isFailed;
            r32 = r0;
            if (r32 != 0) goto L_0x06bc;
        L_0x068a:
            r32 = 2131296499; // 0x7f0900f3 float:1.8210916E38 double:1.0530003813E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 0;
            r32.setVisibility(r33);
            r32 = 2131296501; // 0x7f0900f5 float:1.821092E38 double:1.053000382E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 8;
            r32.setVisibility(r33);
            r32 = 2131296500; // 0x7f0900f4 float:1.8210918E38 double:1.0530003818E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 8;
            r32.setVisibility(r33);
            goto L_0x03aa;
        L_0x06bc:
            r32 = 2131296499; // 0x7f0900f3 float:1.8210916E38 double:1.0530003813E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 8;
            r32.setVisibility(r33);
            r32 = 2131296501; // 0x7f0900f5 float:1.821092E38 double:1.053000382E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 0;
            r32.setVisibility(r33);
            r32 = 2131296500; // 0x7f0900f4 float:1.8210918E38 double:1.0530003818E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 8;
            r32.setVisibility(r33);
            goto L_0x03aa;
        L_0x06ee:
            r32 = 2131296499; // 0x7f0900f3 float:1.8210916E38 double:1.0530003813E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 4;
            r32.setVisibility(r33);
            r32 = 2131296501; // 0x7f0900f5 float:1.821092E38 double:1.053000382E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 8;
            r32.setVisibility(r33);
            r32 = 2131296500; // 0x7f0900f4 float:1.8210918E38 double:1.0530003818E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r33 = 8;
            r32.setVisibility(r33);
            goto L_0x03aa;
        L_0x0720:
            r32 = 2131296499; // 0x7f0900f3 float:1.8210916E38 double:1.0530003813E-314;
            r0 = r29;
            r1 = r32;
            r33 = r0.findViewById(r1);
            r0 = r18;
            r0 = r0.time;
            r32 = r0;
            if (r32 <= 0) goto L_0x075a;
        L_0x0733:
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            r34 = 4;
            r0 = r32;
            r1 = r34;
            if (r0 == r1) goto L_0x074f;
        L_0x0741:
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            r34 = 1;
            r0 = r32;
            r1 = r34;
            if (r0 != r1) goto L_0x075a;
        L_0x074f:
            r32 = 0;
        L_0x0751:
            r0 = r33;
            r1 = r32;
            r0.setVisibility(r1);
            goto L_0x03aa;
        L_0x075a:
            r32 = 4;
            goto L_0x0751;
        L_0x075d:
            r32 = 1077936128; // 0x40400000 float:3.0 double:5.325712093E-315;
            r32 = com.vkontakte.android.Global.scale(r32);
            goto L_0x03c0;
        L_0x0765:
            r32 = 8;
            goto L_0x03dd;
        L_0x0769:
            r16 = r33.next();
            r16 = (com.vkontakte.android.fragments.ChatFragment.Image) r16;
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r32 = r32.imgLoader;
            r0 = r16;
            r0 = r0.url;
            r34 = r0;
            r0 = r32;
            r1 = r34;
            r32 = r0.isAlreadyLoaded(r1);
            if (r32 == 0) goto L_0x07b6;
        L_0x0789:
            r0 = r16;
            r0 = r0.viewId;
            r32 = r0;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r32 = (android.widget.ImageView) r32;
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r34 = r0;
            r34 = r34.imgLoader;
            r0 = r16;
            r0 = r0.url;
            r35 = r0;
            r34 = r34.get(r35);
            r0 = r32;
            r1 = r34;
            r0.setImageBitmap(r1);
            goto L_0x040e;
        L_0x07b6:
            r0 = r16;
            r0 = r0.viewId;
            r32 = r0;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r32 = (android.widget.ImageView) r32;
            r34 = 2130838133; // 0x7f020275 float:1.728124E38 double:1.0527739184E-314;
            r0 = r32;
            r1 = r34;
            r0.setImageResource(r1);
            goto L_0x040e;
        L_0x07d2:
            r32 = -2694936; // 0xffffffffffd6e0e8 float:NaN double:NaN;
            goto L_0x0446;
        L_0x07d7:
            r10 = 2130837668; // 0x7f0200a4 float:1.7280297E38 double:1.0527736886E-314;
            goto L_0x046a;
        L_0x07dc:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r0 = r18;
            r0 = r0.msgId;
            r33 = r0;
            r32 = r32.isSelected(r33);
            if (r32 == 0) goto L_0x0803;
        L_0x07ee:
            r10 = 2130837673; // 0x7f0200a9 float:1.7280307E38 double:1.052773691E-314;
        L_0x07f1:
            r20 = 0;
            r0 = r18;
            r0 = r0.fwdLevel;
            r32 = r0;
            if (r32 != 0) goto L_0x0807;
        L_0x07fb:
            r32 = 1084227584; // 0x40a00000 float:5.0 double:5.356796015E-315;
            r31 = com.vkontakte.android.Global.scale(r32);
        L_0x0801:
            goto L_0x0481;
        L_0x0803:
            r10 = 2130837672; // 0x7f0200a8 float:1.7280305E38 double:1.0527736906E-314;
            goto L_0x07f1;
        L_0x0807:
            r31 = 0;
            goto L_0x0801;
        L_0x080a:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r0 = r18;
            r0 = r0.msgId;
            r33 = r0;
            r32 = r32.isSelected(r33);
            if (r32 == 0) goto L_0x0823;
        L_0x081c:
            r10 = 2130837667; // 0x7f0200a3 float:1.7280295E38 double:1.052773688E-314;
        L_0x081f:
            r21 = 0;
            goto L_0x0481;
        L_0x0823:
            r10 = 2130837666; // 0x7f0200a2 float:1.7280293E38 double:1.0527736876E-314;
            goto L_0x081f;
        L_0x0827:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r0 = r18;
            r0 = r0.msgId;
            r33 = r0;
            r32 = r32.isSelected(r33);
            if (r32 == 0) goto L_0x0842;
        L_0x0839:
            r10 = 2130837671; // 0x7f0200a7 float:1.7280303E38 double:1.05277369E-314;
        L_0x083c:
            r20 = 0;
            r21 = r20;
            goto L_0x0481;
        L_0x0842:
            r10 = 2130837670; // 0x7f0200a6 float:1.72803E38 double:1.0527736896E-314;
            goto L_0x083c;
        L_0x0846:
            r32 = r31;
            goto L_0x04a9;
        L_0x084a:
            r32 = 0;
            goto L_0x04e8;
        L_0x084e:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r0 = r18;
            r0 = r0.msgId;
            r33 = r0;
            r32 = r32.isSelected(r33);
            if (r32 == 0) goto L_0x0900;
        L_0x0860:
            r10 = 2130837661; // 0x7f02009d float:1.7280282E38 double:1.052773685E-314;
        L_0x0863:
            r32 = 1082130432; // 0x40800000 float:4.0 double:5.34643471E-315;
            r21 = com.vkontakte.android.Global.scale(r32);
            r32 = 1082130432; // 0x40800000 float:4.0 double:5.34643471E-315;
            r20 = com.vkontakte.android.Global.scale(r32);
            r31 = -1;
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            switch(r32) {
                case 2: goto L_0x0905;
                case 3: goto L_0x0950;
                case 4: goto L_0x0933;
                default: goto L_0x087a;
            };
        L_0x087a:
            r32 = 2131296498; // 0x7f0900f2 float:1.8210914E38 double:1.053000381E-314;
            r0 = r29;
            r1 = r32;
            r30 = r0.findViewById(r1);
            r0 = r30;
            r0.setBackgroundResource(r10);
            r33 = r30.getPaddingLeft();
            r34 = r30.getPaddingTop();
            r35 = r30.getPaddingRight();
            r32 = -1;
            r0 = r31;
            r1 = r32;
            if (r0 != r1) goto L_0x096f;
        L_0x089e:
            r32 = r30.getPaddingBottom();
        L_0x08a2:
            r0 = r30;
            r1 = r33;
            r2 = r34;
            r3 = r35;
            r4 = r32;
            r0.setPadding(r1, r2, r3, r4);
            r32 = 0;
            r33 = 0;
            r0 = r29;
            r1 = r32;
            r2 = r21;
            r3 = r33;
            r4 = r20;
            r0.setPadding(r1, r2, r3, r4);
            r32 = 2131296498; // 0x7f0900f2 float:1.8210914E38 double:1.053000381E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r19 = r32.getLayoutParams();
            r19 = (android.widget.LinearLayout.LayoutParams) r19;
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            r33 = 1;
            r0 = r32;
            r1 = r33;
            if (r0 == r1) goto L_0x0973;
        L_0x08df:
            r32 = 1;
        L_0x08e1:
            r0 = r32;
            r0 = (float) r0;
            r32 = r0;
            r0 = r32;
            r1 = r19;
            r1.weight = r0;
            r32 = 2131296498; // 0x7f0900f2 float:1.8210914E38 double:1.053000381E-314;
            r0 = r29;
            r1 = r32;
            r32 = r0.findViewById(r1);
            r0 = r32;
            r1 = r19;
            r0.setLayoutParams(r1);
            goto L_0x0505;
        L_0x0900:
            r10 = 2130837660; // 0x7f02009c float:1.728028E38 double:1.0527736847E-314;
            goto L_0x0863;
        L_0x0905:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r0 = r18;
            r0 = r0.msgId;
            r33 = r0;
            r32 = r32.isSelected(r33);
            if (r32 == 0) goto L_0x092c;
        L_0x0917:
            r10 = 2130837665; // 0x7f0200a1 float:1.728029E38 double:1.052773687E-314;
        L_0x091a:
            r20 = 0;
            r0 = r18;
            r0 = r0.fwdLevel;
            r32 = r0;
            if (r32 != 0) goto L_0x0930;
        L_0x0924:
            r32 = 1084227584; // 0x40a00000 float:5.0 double:5.356796015E-315;
            r31 = com.vkontakte.android.Global.scale(r32);
        L_0x092a:
            goto L_0x087a;
        L_0x092c:
            r10 = 2130837664; // 0x7f0200a0 float:1.7280288E38 double:1.0527736866E-314;
            goto L_0x091a;
        L_0x0930:
            r31 = 0;
            goto L_0x092a;
        L_0x0933:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r0 = r18;
            r0 = r0.msgId;
            r33 = r0;
            r32 = r32.isSelected(r33);
            if (r32 == 0) goto L_0x094c;
        L_0x0945:
            r10 = 2130837659; // 0x7f02009b float:1.7280278E38 double:1.052773684E-314;
        L_0x0948:
            r21 = 0;
            goto L_0x087a;
        L_0x094c:
            r10 = 2130837658; // 0x7f02009a float:1.7280276E38 double:1.0527736837E-314;
            goto L_0x0948;
        L_0x0950:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r0 = r18;
            r0 = r0.msgId;
            r33 = r0;
            r32 = r32.isSelected(r33);
            if (r32 == 0) goto L_0x096b;
        L_0x0962:
            r10 = 2130837663; // 0x7f02009f float:1.7280286E38 double:1.052773686E-314;
        L_0x0965:
            r20 = 0;
            r21 = r20;
            goto L_0x087a;
        L_0x096b:
            r10 = 2130837662; // 0x7f02009e float:1.7280284E38 double:1.0527736857E-314;
            goto L_0x0965;
        L_0x096f:
            r32 = r31;
            goto L_0x08a2;
        L_0x0973:
            r32 = 0;
            goto L_0x08e1;
        L_0x0977:
            r26 = (android.widget.ImageView) r26;
            r32 = 0;
            r0 = r26;
            r1 = r32;
            r0.setColorFilter(r1);
            goto L_0x0541;
        L_0x0984:
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            r33 = 2;
            r0 = r32;
            r1 = r33;
            if (r0 != r1) goto L_0x09a4;
        L_0x0992:
            r32 = 1077936128; // 0x40400000 float:3.0 double:5.325712093E-315;
            r32 = com.vkontakte.android.Global.scale(r32);
            r0 = r32;
            r11.topMargin = r0;
            r32 = 0;
            r0 = r32;
            r11.bottomMargin = r0;
            goto L_0x05f8;
        L_0x09a4:
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            r33 = 1;
            r0 = r32;
            r1 = r33;
            if (r0 != r1) goto L_0x09c8;
        L_0x09b2:
            r32 = 1077936128; // 0x40400000 float:3.0 double:5.325712093E-315;
            r32 = com.vkontakte.android.Global.scale(r32);
            r0 = r32;
            r11.topMargin = r0;
            r32 = 1077936128; // 0x40400000 float:3.0 double:5.325712093E-315;
            r32 = com.vkontakte.android.Global.scale(r32);
            r0 = r32;
            r11.bottomMargin = r0;
            goto L_0x05f8;
        L_0x09c8:
            r0 = r18;
            r0 = r0.type;
            r32 = r0;
            r33 = 3;
            r0 = r32;
            r1 = r33;
            if (r0 != r1) goto L_0x05f8;
        L_0x09d6:
            if (r37 <= 0) goto L_0x05f8;
        L_0x09d8:
            r0 = r36;
            r0 = com.vkontakte.android.fragments.ChatFragment.this;
            r32 = r0;
            r32 = r32.items;
            r33 = r37 + -1;
            r25 = r32.get(r33);
            r25 = (com.vkontakte.android.fragments.ChatFragment.ListItem) r25;
            r0 = r25;
            r0 = r0.fwdLevel;
            r32 = r0;
            if (r32 != 0) goto L_0x0a04;
        L_0x09f2:
            r32 = 0;
            r0 = r32;
            r11.bottomMargin = r0;
            r32 = 1077936128; // 0x40400000 float:3.0 double:5.325712093E-315;
            r32 = com.vkontakte.android.Global.scale(r32);
            r0 = r32;
            r11.topMargin = r0;
            goto L_0x05f8;
        L_0x0a04:
            r32 = 0;
            r0 = r32;
            r11.topMargin = r0;
            r32 = 0;
            r0 = r32;
            r11.bottomMargin = r0;
            goto L_0x05f8;
        L_0x0a12:
            r32 = 4;
            goto L_0x0675;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.fragments.ChatFragment.MessagesAdapter.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.3 */
    class C14473 implements Callback {
        C14473() {
        }

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(C0436R.menu.chat_action_mode, menu);
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return true;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            ArrayList<Message> msgs;
            switch (item.getItemId()) {
                case C0436R.id.copy:
                    ((ClipboardManager) ChatFragment.this.getActivity().getSystemService("clipboard")).setText(((Message) ChatFragment.this.selectedMessages.get(0)).text);
                    Toast.makeText(ChatFragment.this.getActivity(), C0436R.string.text_copied, 0).show();
                    mode.finish();
                    break;
                case C0436R.id.forward:
                    msgs = new ArrayList();
                    msgs.addAll(ChatFragment.this.selectedMessages);
                    ChatFragment.this.forward(msgs);
                    mode.finish();
                    break;
                case C0436R.id.delete:
                    msgs = new ArrayList();
                    msgs.addAll(ChatFragment.this.selectedMessages);
                    ChatFragment.this.confirmAndDelete(msgs);
                    mode.finish();
                    break;
            }
            return false;
        }

        public void onDestroyActionMode(ActionMode mode) {
            ChatFragment.this.actionMode = null;
            ChatFragment.this.selectedMessages.clear();
            ChatFragment.this.adapter.notifyDataSetChanged();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatFragment.6 */
    class C14486 implements StickerClickListener {
        C14486() {
        }

        public void onStickerSelected(StickerAttachment sticker) {
            ChatFragment.this.sendSticker(sticker);
        }
    }

    private class MessagesImagesAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.ChatFragment.MessagesImagesAdapter.1 */
        class C06331 implements Runnable {
            private final /* synthetic */ int val$_image;
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$item;
            private final /* synthetic */ View val$view;

            C06331(int i, int i2, View view, Bitmap bitmap) {
                this.val$_image = i;
                this.val$item = i2;
                this.val$view = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                try {
                    int image = this.val$_image;
                    int i = 0;
                    Iterator it = ((ListItem) ChatFragment.this.items.get(this.val$item)).images.iterator();
                    while (it.hasNext()) {
                        Image img = (Image) it.next();
                        if (i == image) {
                            ((ImageView) this.val$view.findViewById(img.viewId)).setImageBitmap(this.val$bitmap);
                            return;
                        }
                        i++;
                    }
                    image -= ((ListItem) ChatFragment.this.items.get(this.val$item)).images.size();
                    int imgindex = 0;
                    int index = 0;
                    Iterator it2 = ((ListItem) ChatFragment.this.items.get(this.val$item)).attachments.iterator();
                    while (it2.hasNext()) {
                        Attachment att = (Attachment) it2.next();
                        if (att instanceof ImageAttachment) {
                            if (imgindex == image) {
                                ((ImageAttachment) att).setImage(((ViewGroup) this.val$view.findViewById(C0436R.id.msg_attachments)).getChildAt(index), this.val$bitmap, false);
                            }
                            imgindex++;
                        }
                        index++;
                    }
                } catch (Exception e) {
                }
            }
        }

        private MessagesImagesAdapter() {
        }

        public int getItemCount() {
            return ChatFragment.this.items.size();
        }

        public int getImageCountForItem(int item) {
            int count = ((ListItem) ChatFragment.this.items.get(item)).images.size();
            Iterator it = ((ListItem) ChatFragment.this.items.get(item)).attachments.iterator();
            while (it.hasNext()) {
                if (((Attachment) it.next()) instanceof ImageAttachment) {
                    count++;
                }
            }
            return count;
        }

        public String getImageURL(int item, int image) {
            int i = 0;
            Iterator it = ((ListItem) ChatFragment.this.items.get(item)).images.iterator();
            while (it.hasNext()) {
                Image img = (Image) it.next();
                if (i == image) {
                    return img.url;
                }
                i++;
            }
            image -= ((ListItem) ChatFragment.this.items.get(item)).images.size();
            int imgindex = 0;
            it = ((ListItem) ChatFragment.this.items.get(item)).attachments.iterator();
            while (it.hasNext()) {
                Attachment att = (Attachment) it.next();
                if (att instanceof ImageAttachment) {
                    if (imgindex == image) {
                        return ((ImageAttachment) att).getImageURL();
                    }
                    imgindex++;
                }
            }
            return null;
        }

        public void imageLoaded(int item, int _image, Bitmap bitmap) {
            int _item = item + ChatFragment.this.list.getHeaderViewsCount();
            if (_item >= ChatFragment.this.list.getFirstVisiblePosition() && _item <= ChatFragment.this.list.getLastVisiblePosition()) {
                View view = ChatFragment.this.list.getChildAt(_item - ChatFragment.this.list.getFirstVisiblePosition());
                view.post(new C06331(_image, item, view, bitmap));
            }
        }
    }

    public ChatFragment() {
        this.items = new ArrayList();
        this.messages = new ArrayList();
        this.preloadedMessages = new ArrayList();
        this.isActive = false;
        this.typingTimers = new HashMap();
        this.userNamesAcc = new HashMap();
        this.typingUsers = new ArrayList();
        this.receiver = new C06171();
        this.chatUsers = new HashMap();
        this.chatUserClickListener = new C06252();
        this.keyboardVisible = false;
        this.selectedMessages = new ArrayList();
        this.actionModeCallback = new C14473();
        this.messagesToForward = new ArrayList();
        this.fwdProfileClickListener = new C06274();
        this.dataLoading = false;
        this.moreAvailable = true;
        this.preloading = false;
        this.preloadOnReady = false;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        act.setTitle(getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
        this.peer = getArguments().getInt("id", 0);
        this.contentView = new C06285(act);
        this.emojiPopup = new EmojiPopup(getActivity(), this.contentView, C0436R.drawable.ic_msg_panel_smiles, true);
        this.emojiPopup.setStickerClickListener(new C14486());
        this.contentView.setOrientation(1);
        View frameLayout = new FrameLayout(act);
        this.contentView.addView(frameLayout, new LayoutParams(-1, -1, 1.0f));
        this.writeBar = new WriteBar(act);
        this.writeBar.setFragment(this);
        ((ImageView) this.writeBar.findViewById(C0436R.id.writebar_attach)).setImageResource(C0436R.drawable.ic_msg_panel_smiles);
        if (!getArguments().containsKey("from_search")) {
            this.contentView.addView(this.writeBar, new LayoutParams(-1, -2));
        }
        frameLayout.setBackgroundColor(-1643536);
        this.loadMoreView = View.inflate(act, C0436R.layout.messages_load_more, null);
        this.typingView = new LinearLayout(act);
        this.typingView.setPadding(Global.scale(5.0f), Global.scale(5.0f), Global.scale(5.0f), Global.scale(5.0f));
        this.typingView.setGravity(16);
        ImageView tiv1 = new ImageView(act);
        tiv1.setImageResource(C0436R.drawable.ic_typing);
        tiv1.setScaleType(ScaleType.MATRIX);
        tiv1.setLayoutParams(new LayoutParams(Global.scale(12.0f), -2));
        this.typingView.addView(tiv1);
        frameLayout = new ImageView(act);
        frameLayout.setImageResource(C0436R.drawable.ic_typing);
        frameLayout.setScaleType(ScaleType.MATRIX);
        Matrix matrix = new Matrix();
        matrix.setTranslate((float) Global.scale(-12.0f), 0.0f);
        frameLayout.setImageMatrix(matrix);
        frameLayout.setLayoutParams(new LayoutParams(Global.scale(20.0f), -2));
        frameLayout.setBackgroundColor(-1643278);
        this.typingView.addView(frameLayout);
        frameLayout = new TextView(act);
        frameLayout.setTextColor(-7956823);
        frameLayout.setTextSize(1, 14.0f);
        frameLayout.setShadowLayer(1.0E-7f, 0.0f, 1.0f, -985865);
        int i = this.peer;
        if (r0 < 2000000000) {
            frameLayout.setText(getResources().getString(C0436R.string.chat_typing, new Object[]{getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE).toString().split(" ")[0]}));
        }
        frameLayout.setPadding(Global.scale(5.0f), 0, Global.scale(5.0f), 0);
        frameLayout.setLayoutParams(new LayoutParams(-1, -2));
        this.typingView.addView(frameLayout);
        this.typingView.setVisibility(8);
        this.list = new ListView(act);
        if (VERSION.SDK_INT < 11) {
            this.list.setBackgroundColor(-1643536);
        }
        this.list.setCacheColorHint(-1643536);
        if (getArguments().containsKey("from_search")) {
            this.openChatBtn = View.inflate(getActivity(), C0436R.layout.ab_done_right, null);
            ((TextView) this.openChatBtn.findViewById(C0436R.id.ab_done_text)).setText(C0436R.string.open_chat);
            ((TextView) this.openChatBtn.findViewById(C0436R.id.ab_done_text)).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_ab_open_chat, 0, 0, 0);
            this.openChatBtn.findViewById(C0436R.id.view1).setVisibility(4);
            this.openChatBtn.setOnClickListener(new C06297());
        } else {
            this.list.setStackFromBottom(true);
            this.list.addHeaderView(this.loadMoreView, null, false);
            this.list.addFooterView(this.typingView, null, false);
        }
        ListView listView = this.list;
        ListAdapter messagesAdapter = new MessagesAdapter(null);
        this.adapter = messagesAdapter;
        listView.setAdapter(messagesAdapter);
        this.list.setDividerHeight(0);
        this.list.setTranscriptMode(1);
        this.list.setSelector(new ColorDrawable(0));
        this.list.setOnItemClickListener(this);
        this.list.setOnItemLongClickListener(this);
        this.emptyView = new TextView(act);
        this.emptyView.setText(C0436R.string.no_messages);
        this.emptyView.setTextSize(18.0f);
        this.emptyView.setTextColor(ExploreByTouchHelper.INVALID_ID);
        this.emptyView.setGravity(17);
        this.list.setEmptyView(this.emptyView);
        this.listWrap = new FrameLayout(act);
        this.listWrap.addView(this.list);
        this.listWrap.addView(this.emptyView);
        this.listWrap.setVisibility(8);
        frameLayout.addView(this.listWrap);
        this.progress = new ProgressBar(act);
        frameLayout.addView(this.progress, new FrameLayout.LayoutParams(Global.scale(50.0f), Global.scale(50.0f), 17));
        this.errorView = (ErrorView) View.inflate(getActivity(), C0436R.layout.error, null);
        this.errorView.setVisibility(8);
        frameLayout.addView(this.errorView);
        if (!getArguments().containsKey("from_search")) {
            View shadow = new View(act);
            shadow.setLayoutParams(new FrameLayout.LayoutParams(-1, Global.scale(4.0f), 80));
            shadow.setBackgroundResource(C0436R.drawable.bottom_shadow);
            frameLayout.addView(shadow);
        }
        this.imgLoader = new ListImageLoaderWrapper(new MessagesImagesAdapter(null), this.list, null);
        this.loadMoreView.findViewById(C0436R.id.load_more_btn).setOnClickListener(new C06308());
        if (!getArguments().containsKey("from_search")) {
            this.imgLoader.setOnScrollListener(new C06319());
        }
        this.writeBar.findViewById(C0436R.id.writebar_send).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ChatFragment.this.sendMessage();
            }
        });
        this.writeBar.findViewById(C0436R.id.writebar_attach).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                boolean shown;
                boolean z = false;
                if (ChatFragment.this.emojiPopup == null || !ChatFragment.this.emojiPopup.isShowing()) {
                    shown = false;
                } else {
                    shown = true;
                }
                EmojiPopup access$34 = ChatFragment.this.emojiPopup;
                if (!shown) {
                    z = true;
                }
                access$34.showEmojiPopup(z);
            }
        });
        setHasOptionsMenu(true);
        this.writeBar.findViewById(C0436R.id.writebar_edit).setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode != 4 || ChatFragment.this.keyboardVisible || ChatFragment.this.emojiPopup == null || !ChatFragment.this.emojiPopup.isShowing()) {
                    return false;
                }
                if (event.getAction() != 1) {
                    return true;
                }
                ChatFragment.this.emojiPopup.showEmojiPopup(false);
                return true;
            }
        });
        ((EditText) this.writeBar.findViewById(C0436R.id.writebar_edit)).addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                int i = 0;
                ImageSpan[] spans = (ImageSpan[]) s.getSpans(0, s.length(), ImageSpan.class);
                int length = spans.length;
                while (i < length) {
                    s.removeSpan(spans[i]);
                    i++;
                }
                Global.replaceEmoji(s);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    ChatFragment.this.sendTypingIfNeeded();
                }
            }
        });
        ((EditText) this.writeBar.findViewById(C0436R.id.writebar_edit)).setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event == null || event.getAction() != 0 || event.getKeyCode() != 66) {
                    return false;
                }
                boolean sendByEnter = PreferenceManager.getDefaultSharedPreferences(ChatFragment.this.getActivity()).getBoolean("sendByEnter", false);
                if ((!sendByEnter || event.getMetaState() != 0) && (sendByEnter || (event.getMetaState() & NewsEntry.FLAG_SUGGESTED) <= 0)) {
                    return false;
                }
                ChatFragment.this.sendMessage();
                return true;
            }
        });
        if (getArguments().containsKey("fwd")) {
            ArrayList<Message> fwd = getArguments().getParcelableArrayList("fwd");
            this.writeBar.addFwdMessages(fwd);
        }
        if (getArguments().containsKey("post")) {
            NewsEntry p = (NewsEntry) getArguments().getParcelable("post");
            Attachment att = null;
            if (p.type == 0) {
                att = new PostAttachment(p);
            } else {
                if (p.attachments.size() > 0) {
                    att = (Attachment) p.attachments.get(0);
                }
            }
            if (att != null) {
                this.writeBar.addAttachment(att);
            }
        }
        if (getArguments().containsKey("photos")) {
            Iterator it = getArguments().getStringArrayList("photos").iterator();
            while (it.hasNext()) {
                String photo = (String) it.next();
                this.writeBar.addAttachment(new PendingPhotoAttachment(photo));
            }
        }
        this.errorView.setOnRetryListener(new OnClickListener() {
            public void onClick(View v) {
                Global.showViewAnimated(ChatFragment.this.errorView, false, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(ChatFragment.this.progress, true, PhotoView.THUMB_ANIM_DURATION);
                ChatFragment.this.loadData();
            }
        });
        if (getArguments().containsKey("from_search")) {
            Message m = (Message) getArguments().getParcelable("from_search");
            this.messages.add(m);
            ArrayList<Integer> fuids = new ArrayList();
            ArrayList<Message> mm = new ArrayList();
            mm.add(m);
            this.items.addAll(0, buildItems(mm, fuids, true, false));
            removeRepeatingDates();
            updateList();
            loadFwdUsers(fuids);
            this.listWrap.setVisibility(0);
            this.progress.setVisibility(8);
            getActivity().setTitle(C0436R.string.message);
            i = this.peer;
            if (r0 > 2000000000) {
                updateChatUsers(false);
                return;
            }
            return;
        }
        loadData();
        i = this.peer;
        if (r0 < 2000000000 && this.peer > 0) {
            updateOnline(Friends.getOnlineStatus(this.peer));
        }
        i = this.peer;
        if (r0 > 2000000000) {
            updateChatUsers(false);
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        if (!getArguments().containsKey("from_search")) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(LongPollService.ACTION_MESSAGE_DELETED);
            filter.addAction(LongPollService.ACTION_MESSAGE_RSTATE_CHANGED);
            filter.addAction(LongPollService.ACTION_NEW_MESSAGE);
            filter.addAction(LongPollService.ACTION_TYPING);
            filter.addAction(LongPollService.ACTION_USER_PRESENCE);
            filter.addAction(LongPollService.ACTION_CHAT_CHANGED);
            filter.addAction(LongPollService.ACTION_REFRESH_DIALOGS_LIST);
            filter.addAction(Messages.ACTION_MESSAGE_ID_CHANGED);
            filter.addAction(Messages.ACTION_SEND_FAILED);
            filter.addAction(UploaderService.ACTION_UPLOAD_DONE);
            VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (!getArguments().containsKey("from_search")) {
            try {
                VKApplication.context.unregisterReceiver(this.receiver);
            } catch (Exception e) {
            }
        }
    }

    public void onPause() {
        super.onPause();
        this.isActive = false;
        activeInstance = null;
        saveDraft();
        this.imgLoader.deactivate();
    }

    public void onResume() {
        super.onResume();
        this.isActive = true;
        activeInstance = this;
        markAsRead();
        restoreDraft();
        this.emojiPopup.loadRecents();
        this.imgLoader.activate();
    }

    public void onConfigurationChanged(Configuration cfg) {
        super.onConfigurationChanged(cfg);
        if (this.emojiPopup != null && this.emojiPopup.isShowing()) {
            this.emojiPopup.showEmojiPopup(false);
            this.emojiPopup.onKeyboardStateChanged(false, -1);
        }
    }

    public void hideEmojiPopup() {
        this.emojiPopup.hide();
    }

    private void saveDraft() {
        ArrayList<Attachment> atts = this.writeBar.getAttachments();
        SharedPreferences prefs = getActivity().getSharedPreferences("drafts", 0);
        if (atts.size() == 0 && this.writeBar.getText().length() == 0) {
            prefs.edit().remove("text" + this.peer).remove("attach" + this.peer).commit();
            return;
        }
        String satts = null;
        if (atts.size() > 0) {
            try {
                ByteArrayOutputStream buf = new ByteArrayOutputStream();
                DataOutputStream os = new DataOutputStream(buf);
                os.writeInt(atts.size());
                Iterator it = atts.iterator();
                while (it.hasNext()) {
                    ((Attachment) it.next()).serialize(os);
                }
                satts = Base64.encodeToString(buf.toByteArray(), 0);
            } catch (Exception e) {
            }
        }
        Editor ed = prefs.edit().putString("text" + this.peer, this.writeBar.getText());
        if (satts != null) {
            ed.putString("attach" + this.peer, satts);
        }
        ed.commit();
    }

    private void restoreDraft() {
        if (this.writeBar.getAttachments().size() <= 0 && this.writeBar.getText().length() <= 0) {
            SharedPreferences prefs = getActivity().getSharedPreferences("drafts", 0);
            if (prefs.contains("text" + this.peer)) {
                this.writeBar.setText(prefs.getString("text" + this.peer, ACRAConstants.DEFAULT_STRING_VALUE));
                if (prefs.contains("attach" + this.peer)) {
                    try {
                        DataInputStream is = new DataInputStream(new ByteArrayInputStream(Base64.decode(prefs.getString("attach" + this.peer, ACRAConstants.DEFAULT_STRING_VALUE), 0)));
                        int count = is.readInt();
                        for (int i = 0; i < count; i++) {
                            this.writeBar.addAttachment(Attachment.deserialize(is, is.readInt()));
                        }
                    } catch (Exception e) {
                    }
                }
                prefs.edit().remove("text" + this.peer).commit();
                prefs.edit().remove("attach" + this.peer).commit();
            }
        }
    }

    private void updateOnline(int online) {
        getSherlockActivity().getSupportActionBar().setSubtitle(getOnlineString(online));
        if (online == 0) {
            new MessagesGetLastActivity(this.peer).setCallback(new MessagesGetLastActivity.Callback() {
                public void success(int time, int online, boolean f, boolean mobile) {
                    if (time <= 0 || online != 0 || ChatFragment.this.getActivity() == null) {
                        ChatFragment.this.getSherlockActivity().getSupportActionBar().setSubtitle(ChatFragment.this.getOnlineString(online));
                        return;
                    }
                    CharSequence lastSeen = ChatFragment.this.getString(f ? C0436R.string.last_seen_profile_f : C0436R.string.last_seen_profile_m, Global.langDate(ChatFragment.this.getResources(), time));
                    if (mobile) {
                        SpannableStringBuilder bldr = new SpannableStringBuilder(lastSeen);
                        Spannable sp = Factory.getInstance().newSpannable("F");
                        Drawable d = ChatFragment.this.getResources().getDrawable(C0436R.drawable.ic_left_online_mobile);
                        d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                        sp.setSpan(new ImageSpan(d, 1), 0, 1, 0);
                        bldr.append("\u00a0");
                        bldr.append(sp);
                        lastSeen = bldr;
                    }
                    ChatFragment.this.getSherlockActivity().getSupportActionBar().setSubtitle(lastSeen);
                }

                public void fail(int ecode, String emsg) {
                }
            }).exec(getActivity());
        }
    }

    private CharSequence getOnlineString(int online) {
        String base = getString(online > 0 ? C0436R.string.online : C0436R.string.offline);
        if (online == 0 || online == 1) {
            return base;
        }
        CharSequence bldr = new SpannableStringBuilder(base);
        Spannable sp = Factory.getInstance().newSpannable("F");
        Drawable d = getResources().getDrawable(C0436R.drawable.ic_left_online_mobile);
        d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
        sp.setSpan(new ImageSpan(d, 1), 0, 1, 0);
        bldr.append("\u00a0");
        bldr.append(sp);
        return bldr;
    }

    private void updateChatUsers(boolean forceReload) {
        if (forceReload) {
            Cache.setNeedUpdateChat(this.peer - 2000000000);
        }
        Messages.getChatUsers(this.peer - 2000000000, new GetChatUsersCallback() {

            /* renamed from: com.vkontakte.android.fragments.ChatFragment.17.2 */
            class C06162 implements Runnable {
                private final /* synthetic */ String val$title;
                private final /* synthetic */ ArrayList val$users;

                C06162(String str, ArrayList arrayList) {
                    this.val$title = str;
                    this.val$users = arrayList;
                }

                public void run() {
                    if (ChatFragment.this.getActivity() != null) {
                        boolean needRebuild = false;
                        Iterator it = ChatFragment.this.messages.iterator();
                        while (it.hasNext()) {
                            if (((Message) it.next()).isServiceMessage) {
                                needRebuild = true;
                                break;
                            }
                        }
                        if (needRebuild) {
                            ChatFragment.this.rebuildItems();
                            ChatFragment.this.updateList();
                        } else {
                            ChatFragment.this.updateChatUsersPhotos();
                        }
                        if (!ChatFragment.this.getArguments().containsKey("from_search")) {
                            if (this.val$title != null) {
                                ChatFragment.this.getActivity().setTitle(this.val$title);
                            }
                            ChatFragment.this.getSherlockActivity().getSupportActionBar().setSubtitle(Global.langPlural(C0436R.array.chat_members, this.val$users.size(), ChatFragment.this.getResources()));
                            ChatFragment.this.updateTyping();
                        }
                    }
                }
            }

            /* renamed from: com.vkontakte.android.fragments.ChatFragment.17.1 */
            class C14451 implements GetUsersCallback {
                C14451() {
                }

                public void onUsersLoaded(ArrayList<UserProfile> users) {
                    Iterator it = users.iterator();
                    while (it.hasNext()) {
                        UserProfile user = (UserProfile) it.next();
                        ChatFragment.this.chatUsers.put(Integer.valueOf(user.uid), user);
                    }
                    ChatFragment.this.updateChatUsersPhotos();
                }
            }

            public void onUsersLoaded(ArrayList<ChatUser> users, String title, String photo) {
                if (ChatFragment.this.getActivity() != null) {
                    Iterator it = users.iterator();
                    while (it.hasNext()) {
                        ChatUser user = (ChatUser) it.next();
                        ChatFragment.this.chatUsers.put(Integer.valueOf(user.user.uid), user.user);
                    }
                    ArrayList<Integer> needUsers = new ArrayList();
                    it = ChatFragment.this.messages.iterator();
                    while (it.hasNext()) {
                        Message msg = (Message) it.next();
                        if (!(msg.sender == Global.uid || ChatFragment.this.chatUsers.containsKey(Integer.valueOf(msg.sender)) || needUsers.contains(Integer.valueOf(msg.sender)))) {
                            needUsers.add(Integer.valueOf(msg.sender));
                        }
                    }
                    if (needUsers.size() > 0) {
                        Friends.getUsers(needUsers, new C14451());
                    }
                    ChatFragment.this.getActivity().runOnUiThread(new C06162(title, users));
                }
            }
        });
    }

    private void updateChatUsersCount() {
    }

    public int getPeerID() {
        return this.peer;
    }

    private void showTyping(boolean show) {
        boolean visible;
        if (this.typingView.getVisibility() == 0) {
            visible = true;
        } else {
            visible = false;
        }
        if (visible != show) {
            AlphaAnimation aa;
            if (show) {
                this.typingView.setVisibility(0);
                this.typingView.getChildAt(0).setVisibility(0);
                this.typingView.getChildAt(1).setVisibility(0);
                TranslateAnimation ta = new TranslateAnimation((float) Global.scale(-12.0f), 0.0f, 0.0f, 0.0f);
                ta.setDuration(1500);
                ta.setRepeatCount(-1);
                ta.setInterpolator(new Interpolator() {
                    public float getInterpolation(float input) {
                        return Math.min(1.5f * input, 1.0f);
                    }
                });
                this.typingView.getChildAt(1).clearAnimation();
                this.typingView.getChildAt(1).startAnimation(ta);
                aa = new AlphaAnimation(0.0f, 1.0f);
                aa.setDuration(300);
                this.typingView.startAnimation(aa);
                TextView tv = (TextView) this.typingView.getChildAt(2);
                tv.setTextColor(-7956823);
                tv.setGravity(3);
                return;
            }
            aa = new AlphaAnimation(1.0f, 0.0f);
            aa.setDuration(300);
            this.typingView.startAnimation(aa);
            this.typingView.postDelayed(new Runnable() {
                public void run() {
                    ChatFragment.this.typingView.setVisibility(8);
                }
            }, 300);
        }
    }

    private void updateTyping() {
        if (getActivity() != null) {
            if (this.peer < 2000000000) {
                ((TextView) this.typingView.getChildAt(2)).setText(getResources().getString(C0436R.string.chat_typing, new Object[]{getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE).toString().split(" ")[0]}));
                return;
            }
            boolean z;
            if (this.typingUsers.size() > 0) {
                z = true;
            } else {
                z = false;
            }
            showTyping(z);
            if (this.typingUsers.size() > 0) {
                TextView tv = (TextView) this.typingView.getChildAt(2);
                if (this.typingUsers.size() == 1) {
                    tv.setText(getResources().getString(C0436R.string.chat_typing, new Object[]{getChatUserName(((Integer) this.typingUsers.get(0)).intValue())}));
                } else if (this.typingUsers.size() == 2) {
                    tv.setText(getResources().getString(C0436R.string.chat_typing_multiple, new Object[]{getChatUserName(((Integer) this.typingUsers.get(0)).intValue()) + " " + getResources().getString(C0436R.string.ntf_two_users_c) + " " + getChatUserName(((Integer) this.typingUsers.get(1)).intValue())}));
                } else {
                    String str = ACRAConstants.DEFAULT_STRING_VALUE;
                    int i = 0;
                    Iterator it = this.typingUsers.iterator();
                    while (it.hasNext()) {
                        str = new StringBuilder(String.valueOf(str)).append(getChatUserName(((Integer) it.next()).intValue())).toString();
                        if (i + 2 == this.typingUsers.size()) {
                            str = new StringBuilder(String.valueOf(str)).append(" ").append(getResources().getString(C0436R.string.ntf_two_users_c)).append(" ").toString();
                        } else if (i + 1 != this.typingUsers.size()) {
                            str = new StringBuilder(String.valueOf(str)).append(", ").toString();
                        }
                        i++;
                    }
                    tv.setText(getResources().getString(C0436R.string.chat_typing_multiple, new Object[]{str}));
                }
                if (this.list.getLastVisiblePosition() == this.list.getCount() - 1) {
                    this.list.setSelectionFromTop(this.list.getCount() - 1, -9999);
                }
            }
        }
    }

    private String getChatUserName(int uid) {
        if (this.chatUsers.containsKey(Integer.valueOf(uid))) {
            return ((UserProfile) this.chatUsers.get(Integer.valueOf(uid))).fullName;
        }
        return "?";
    }

    private UserProfile getChatUser(int uid) {
        if (this.chatUsers.containsKey(Integer.valueOf(uid))) {
            return (UserProfile) this.chatUsers.get(Integer.valueOf(uid));
        }
        UserProfile p = new UserProfile();
        String str = "...";
        p.fullName = str;
        p.lastName = str;
        p.firstName = str;
        Log.m530w("vk", "getChatUser: unknown user " + uid);
        return p;
    }

    private Message getMessage(int id) {
        Iterator it = this.messages.iterator();
        while (it.hasNext()) {
            Message m = (Message) it.next();
            if (m.id == id) {
                return m;
            }
        }
        return null;
    }

    private void restartTypingTimer() {
        if (this.typingTimer != null) {
            this.typingTimer.cancel();
        }
        this.typingTimer = new Timer();
        this.typingTimer.schedule(new TimerTask() {

            /* renamed from: com.vkontakte.android.fragments.ChatFragment.20.1 */
            class C06181 implements Runnable {
                C06181() {
                }

                public void run() {
                    ChatFragment.this.showTyping(false);
                }
            }

            public void run() {
                ChatFragment.this.typingTimer = null;
                ChatFragment.this.typingView.post(new C06181());
            }
        }, 7000);
    }

    private void restartTypingTimer(int uid) {
        if (this.typingTimers.containsKey(Integer.valueOf(uid))) {
            ((Timer) this.typingTimers.get(Integer.valueOf(uid))).cancel();
        }
        Timer typingTimer = new Timer();
        typingTimer.schedule(new AnonymousClass21(uid), 7000);
        this.typingTimers.put(Integer.valueOf(uid), typingTimer);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getArguments().containsKey("from_search")) {
            MenuItem item = menu.add((int) C0436R.string.send);
            item.setActionView(this.openChatBtn);
            item.setShowAsAction(2);
            return;
        }
        MenuItem attach = menu.add(0, (int) C0436R.id.attach, 0, (int) C0436R.string.attach);
        attach.setShowAsAction(2);
        attach.setIcon((int) C0436R.drawable.ic_ab_attach);
        if (this.peer < 2000000000) {
            item = menu.add(0, 0, 0, (int) C0436R.string.profile);
            item.setShowAsAction(2);
            View actionView = new AnonymousClass22(getActivity());
            actionView.setImageResource(C0436R.drawable.user_placeholder_chat_header);
            item.setActionView(actionView);
            actionView.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (ChatFragment.this.peer >= -2000000000) {
                        Bundle args = new Bundle();
                        args.putInt("id", ChatFragment.this.peer);
                        Navigate.to("ProfileFragment", args, ChatFragment.this.getActivity());
                    }
                }
            });
            new Thread(new AnonymousClass24(actionView)).start();
            return;
        }
        MenuItem members = menu.add(0, (int) C0436R.id.members, 0, (int) C0436R.string.chat_members);
        members.setShowAsAction(2);
        members.setIcon((int) C0436R.drawable.ic_ab_chat_members);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0436R.id.attach) {
            this.writeBar.openAttachMenu(53, 0, getSherlockActivity().getSupportActionBar().getHeight() - Global.scale(12.0f), C0436R.drawable.attach_dropdown);
        }
        if (item.getItemId() == C0436R.id.members) {
            Bundle args = new Bundle();
            args.putInt("id", this.peer - 2000000000);
            args.putInt("admin", Messages.getChatAdmin(this.peer - 2000000000));
            args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
            Navigate.to("ChatMembersFragment", args, getActivity());
        }
        return true;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentView;
    }

    private void loadData() {
        int i;
        this.dataLoading = true;
        int i2 = this.peer;
        int size = this.messages.size();
        if (this.preloading) {
            i = 30;
        } else {
            i = 60;
        }
        Messages.getHistory(i2, size, i, new GetMessagesCallback() {

            /* renamed from: com.vkontakte.android.fragments.ChatFragment.25.1 */
            class C06211 implements Runnable {
                private final /* synthetic */ ArrayList val$msgs;

                C06211(ArrayList arrayList) {
                    this.val$msgs = arrayList;
                }

                public void run() {
                    ArrayList<Integer> mids = new ArrayList();
                    Iterator it = ChatFragment.this.messages.iterator();
                    while (it.hasNext()) {
                        mids.add(Integer.valueOf(((Message) it.next()).id));
                    }
                    it = ChatFragment.this.preloadedMessages.iterator();
                    while (it.hasNext()) {
                        mids.add(Integer.valueOf(((Message) it.next()).id));
                    }
                    Iterator<Message> imsgs = this.val$msgs.iterator();
                    while (imsgs.hasNext()) {
                        Message m = (Message) imsgs.next();
                        if (mids.contains(Integer.valueOf(m.id))) {
                            imsgs.remove();
                        } else {
                            mids.add(Integer.valueOf(m.id));
                        }
                    }
                    if (ChatFragment.this.messages.size() == 0) {
                        ChatFragment.this.errorView.clearAnimation();
                        ChatFragment.this.errorView.setVisibility(8);
                        Global.showViewAnimated(ChatFragment.this.listWrap, true, PhotoView.THUMB_ANIM_DURATION);
                        Global.showViewAnimated(ChatFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                    }
                    if (this.val$msgs.size() == 0) {
                        ChatFragment.this.loadMoreView.findViewById(C0436R.id.load_more_btn).setVisibility(8);
                        ChatFragment.this.loadMoreView.findViewById(C0436R.id.load_more_progress).setVisibility(8);
                        ChatFragment.this.moreAvailable = false;
                        ChatFragment.this.dataLoading = false;
                        return;
                    }
                    ChatFragment.this.loadMoreView.findViewById(C0436R.id.load_more_btn).setVisibility(0);
                    ChatFragment.this.loadMoreView.findViewById(C0436R.id.load_more_progress).setVisibility(8);
                    ChatFragment.this.moreAvailable = true;
                    if (ChatFragment.this.preloading) {
                        ChatFragment.this.preloadedMessages.addAll(this.val$msgs);
                    } else if (this.val$msgs.size() > 30) {
                        ChatFragment.this.prependMessages(this.val$msgs.subList(this.val$msgs.size() - 30, this.val$msgs.size()));
                        ChatFragment.this.preloadedMessages.addAll(this.val$msgs.subList(0, this.val$msgs.size() - 30));
                    } else {
                        ChatFragment.this.prependMessages(this.val$msgs);
                    }
                    ChatFragment.this.updateList();
                    if (ChatFragment.this.isActive) {
                        ChatFragment.this.markAsRead();
                    }
                    ChatFragment.this.dataLoading = false;
                    ChatFragment.this.preloading = false;
                    if (ChatFragment.this.preloadOnReady) {
                        ChatFragment.this.preloading = true;
                        ChatFragment.this.preloadOnReady = false;
                        ChatFragment.this.loadData();
                    }
                }
            }

            /* renamed from: com.vkontakte.android.fragments.ChatFragment.25.2 */
            class C06222 implements Runnable {
                private final /* synthetic */ int val$code;
                private final /* synthetic */ String val$msg;

                C06222(int i, String str) {
                    this.val$code = i;
                    this.val$msg = str;
                }

                public void run() {
                    ChatFragment.this.errorView.setErrorInfo(this.val$code, this.val$msg);
                    ChatFragment.this.list.clearAnimation();
                    ChatFragment.this.listWrap.setVisibility(8);
                    Global.showViewAnimated(ChatFragment.this.errorView, true, PhotoView.THUMB_ANIM_DURATION);
                    Global.showViewAnimated(ChatFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                }
            }

            /* renamed from: com.vkontakte.android.fragments.ChatFragment.25.3 */
            class C06233 implements Runnable {
                C06233() {
                }

                public void run() {
                    Toast.makeText(ChatFragment.this.getActivity(), C0436R.string.err_text, 0).show();
                    ChatFragment.this.loadMoreView.findViewById(C0436R.id.load_more_btn).setVisibility(0);
                    ChatFragment.this.loadMoreView.findViewById(C0436R.id.load_more_progress).setVisibility(8);
                }
            }

            public void onMessagesLoaded(ArrayList<Message> msgs) {
                if (ChatFragment.this.getActivity() == null) {
                    ChatFragment.this.dataLoading = false;
                    return;
                }
                HashSet<Integer> needUsersAcc = new HashSet();
                Iterator it = msgs.iterator();
                while (it.hasNext()) {
                    Message m = (Message) it.next();
                    if (m.isServiceMessage && m.extras != null && m.extras.containsKey("action_mid") && !ChatFragment.this.userNamesAcc.containsKey(Integer.valueOf(m.extras.getInt("action_mid")))) {
                        needUsersAcc.add(Integer.valueOf(m.extras.getInt("action_mid")));
                    }
                }
                if (needUsersAcc.size() > 0) {
                    ArrayList<Integer> uids = new ArrayList();
                    uids.addAll(needUsersAcc);
                    for (UserProfile p : Friends.getUsersBlocking(uids, 3)) {
                        ChatFragment.this.userNamesAcc.put(Integer.valueOf(p.uid), p.fullName);
                    }
                }
                ChatFragment.this.getActivity().runOnUiThread(new C06211(msgs));
            }

            public void onError(int code, String msg) {
                if (ChatFragment.this.messages.size() == 0) {
                    if (ChatFragment.this.getActivity() != null) {
                        ChatFragment.this.contentView.postDelayed(new C06222(code, msg), 300);
                    }
                } else if (ChatFragment.this.getActivity() != null) {
                    ChatFragment.this.getActivity().runOnUiThread(new C06233());
                }
                ChatFragment.this.dataLoading = false;
            }
        });
    }

    private void updateChatUsersPhotos() {
        HashMap<ListItem, ListItem> replace = new HashMap();
        Iterator it = this.items.iterator();
        while (it.hasNext()) {
            ListItem item = (ListItem) it.next();
            if (item.type == 5 && item.msgId > 0) {
                Message m = getMessage(item.msgId);
                if (m.extras != null && m.extras.containsKey("action")) {
                    replace.put(item, (ListItem) buildItems(Arrays.asList(new Message[]{m}), new ArrayList(), false, true).get(0));
                }
            }
            if (!item.isOut && (item.type == 4 || item.type == 1)) {
                Iterator it2 = this.messages.iterator();
                while (it2.hasNext()) {
                    Message msg = (Message) it2.next();
                    if (msg.id == item.msgId) {
                        Image img;
                        it2 = item.images.iterator();
                        while (it2.hasNext()) {
                            img = (Image) it2.next();
                            if (img.viewId == C0436R.id.msg_sender_photo) {
                                item.images.remove(img);
                                break;
                            }
                        }
                        if (this.chatUsers.containsKey(Integer.valueOf(msg.sender))) {
                            img = new Image();
                            img.viewId = C0436R.id.msg_sender_photo;
                            img.url = ((UserProfile) this.chatUsers.get(Integer.valueOf(msg.sender))).photo;
                            item.images.add(img);
                        }
                    }
                }
            }
        }
        if (replace.size() > 0) {
            int i = 0;
            while (i < this.items.size()) {
                if (((ListItem) this.items.get(i)).type == 5 && replace.containsKey(this.items.get(i))) {
                    this.items.set(i, (ListItem) replace.get(this.items.get(i)));
                }
                i++;
            }
        }
        updateList();
    }

    private void rebuildItems() {
        this.items.clear();
        this.items.addAll(buildItems(this.messages, new ArrayList(), true, false));
    }

    private ArrayList<ListItem> buildItems(List<Message> msgs, ArrayList<Integer> fwdUids, boolean append, boolean noDates) {
        int prevDay = 0;
        if (append && !noDates) {
            int i = this.items.size() - 1;
            while (i >= 0) {
                if (((ListItem) this.items.get(i)).type == 5 && ((ListItem) this.items.get(i)).text == null) {
                    prevDay = ((ListItem) this.items.get(i)).time / 86400;
                    break;
                }
                i--;
            }
        }
        ArrayList<ListItem> result = new ArrayList();
        for (Message m : msgs) {
            ChatFragment chatFragment = this;
            ListItem item = new ListItem();
            item.text = m.displayableText;
            if (m.isServiceMessage && m.extras.containsKey("action")) {
                String act = m.extras.getString("action");
                UserProfile p;
                Resources resources;
                int i2;
                Object[] objArr;
                if ("chat_photo_update".equals(act)) {
                    p = getChatUser(m.sender);
                    resources = VKApplication.context.getResources();
                    if (p.f151f) {
                        i2 = C0436R.string.chat_photo_updated_f;
                    } else {
                        i2 = C0436R.string.chat_photo_updated_m;
                    }
                    objArr = new Object[1];
                    objArr[0] = "<b>" + p.fullName.replace("<", "&lt;") + "</b>";
                    item.text = Html.fromHtml(resources.getString(i2, objArr));
                } else if ("chat_photo_remove".equals(act)) {
                    p = getChatUser(m.sender);
                    resources = VKApplication.context.getResources();
                    i2 = p.f151f ? C0436R.string.chat_photo_removed_f : C0436R.string.chat_photo_removed_m;
                    objArr = new Object[1];
                    objArr[0] = "<b>" + p.fullName.replace("<", "&lt;") + "</b>";
                    item.text = Html.fromHtml(resources.getString(i2, objArr));
                } else if ("chat_create".equals(act)) {
                    p = getChatUser(m.sender);
                    resources = VKApplication.context.getResources();
                    i2 = p.f151f ? C0436R.string.serv_created_chat_f : C0436R.string.serv_created_chat_m;
                    objArr = new Object[2];
                    objArr[0] = "<b>" + p.fullName.replace("<", "&lt;") + "</b>";
                    objArr[1] = "<b>" + m.extras.getString("action_text").replace("<", "&lt;") + "</b>";
                    item.text = Html.fromHtml(resources.getString(i2, objArr));
                } else if ("chat_title_update".equals(act)) {
                    p = getChatUser(m.sender);
                    resources = VKApplication.context.getResources();
                    i2 = p.f151f ? C0436R.string.serv_renamed_chat_f : C0436R.string.serv_renamed_chat_m;
                    objArr = new Object[2];
                    objArr[0] = "<b>" + p.fullName.replace("<", "&lt;") + "</b>";
                    objArr[1] = "<b>" + m.extras.getString("action_text").replace("<", "&lt;") + "</b>";
                    item.text = Html.fromHtml(resources.getString(i2, objArr));
                } else if ("chat_invite_user".equals(act)) {
                    uid = m.extras.getInt("action_mid");
                    p = getChatUser(m.sender);
                    if (uid == m.sender) {
                        resources = VKApplication.context.getResources();
                        i2 = p.f151f ? C0436R.string.serv_user_returned_f : C0436R.string.serv_user_returned_m;
                        objArr = new Object[1];
                        objArr[0] = "<b>" + p.fullName.replace("<", "&lt;") + "</b>";
                        item.text = Html.fromHtml(resources.getString(i2, objArr));
                    } else {
                        if (m.extras.containsKey("action_email")) {
                            name = m.extras.getString("action_email");
                        } else {
                            name = this.userNamesAcc.containsKey(Integer.valueOf(uid)) ? (String) this.userNamesAcc.get(Integer.valueOf(uid)) : "...";
                        }
                        resources = VKApplication.context.getResources();
                        i2 = p.f151f ? C0436R.string.serv_user_invited_f : C0436R.string.serv_user_invited_m;
                        objArr = new Object[2];
                        objArr[0] = "<b>" + p.fullName.replace("<", "&lt;") + "</b>";
                        objArr[1] = "<b>" + name.replace("<", "&lt;") + "</b>";
                        item.text = Html.fromHtml(resources.getString(i2, objArr));
                    }
                } else if ("chat_kick_user".equals(act)) {
                    uid = m.extras.getInt("action_mid");
                    p = getChatUser(m.sender);
                    if (uid == m.sender) {
                        resources = VKApplication.context.getResources();
                        i2 = p.f151f ? C0436R.string.serv_user_left_f : C0436R.string.serv_user_left_m;
                        objArr = new Object[1];
                        objArr[0] = "<b>" + p.fullName.replace("<", "&lt;") + "</b>";
                        item.text = Html.fromHtml(resources.getString(i2, objArr));
                    } else {
                        if (m.extras.containsKey("action_email")) {
                            name = m.extras.getString("action_email");
                        } else {
                            name = this.userNamesAcc.containsKey(Integer.valueOf(uid)) ? (String) this.userNamesAcc.get(Integer.valueOf(uid)) : "...";
                        }
                        resources = VKApplication.context.getResources();
                        i2 = p.f151f ? C0436R.string.serv_user_kicked_f : C0436R.string.serv_user_kicked_m;
                        objArr = new Object[2];
                        objArr[0] = "<b>" + p.fullName.replace("<", "&lt;") + "</b>";
                        objArr[1] = "<b>" + name.replace("<", "&lt;") + "</b>";
                        item.text = Html.fromHtml(resources.getString(i2, objArr));
                    }
                } else {
                    Log.m530w("vk", "Unknown message action " + act);
                }
                Iterator it = m.attachments.iterator();
                while (it.hasNext()) {
                    Attachment a = (Attachment) it.next();
                    if (a instanceof PhotoAttachment) {
                        ((PhotoAttachment) a).paddingAfter = false;
                    }
                }
            }
            boolean z = (m.displayableText instanceof Spannable) && ((URLSpan[]) ((Spannable) m.displayableText).getSpans(0, m.displayableText.length() - 1, URLSpan.class)).length > 0;
            item.hasLinks = z;
            item.attachments = m.attachments;
            int sz = Math.min(this.contentView.getWidth(), Global.scale(350.0f));
            ZhukovLayout.processThumbs(sz - Global.scale(115.0f), sz, item.attachments);
            item.fwdLevel = 0;
            item.isOut = m.out;
            item.images = new ArrayList();
            item.msgId = m.id;
            item.readState = m.readState;
            if (!(noDates || (m.time + (TimeZone.getDefault().getRawOffset() / LocationStatusCodes.GEOFENCE_NOT_AVAILABLE)) / 86400 == prevDay)) {
                prevDay = (m.time + (TimeZone.getDefault().getRawOffset() / LocationStatusCodes.GEOFENCE_NOT_AVAILABLE)) / 86400;
                ChatFragment chatFragment2 = this;
                ListItem listItem = new ListItem();
                listItem.type = 5;
                listItem.time = ((m.time + (TimeZone.getDefault().getRawOffset() / LocationStatusCodes.GEOFENCE_NOT_AVAILABLE)) / 86400) * 86400;
                listItem.images = new ArrayList();
                listItem.attachments = new ArrayList();
                result.add(listItem);
            }
            if (m.text.length() > 0 || m.attachments.size() > 0 || item.text.length() > 0) {
                result.add(item);
            }
            Image img;
            if (m.fwdMessages == null || m.fwdMessages.size() <= 0) {
                item.type = m.isServiceMessage ? 5 : 1;
                if (this.peer > 2000000000 && !m.out && this.chatUsers.containsKey(Integer.valueOf(m.sender)) && this.chatUsers.get(Integer.valueOf(m.sender)) != null) {
                    chatFragment = this;
                    img = new Image();
                    img.viewId = C0436R.id.msg_sender_photo;
                    img.url = ((UserProfile) this.chatUsers.get(Integer.valueOf(m.sender))).photo;
                    item.images.add(img);
                }
            } else {
                item.type = 2;
                ArrayList<ListItem> fwdItems = buildItems(m.fwdMessages, 1, m.out, m.id, m.readState, fwdUids);
                if (m.text.length() != 0 || m.attachments.size() != 0) {
                    ((ListItem) fwdItems.get(fwdItems.size() - 1)).type = 4;
                } else if (fwdItems.size() == 1) {
                    ((ListItem) fwdItems.get(0)).type = 1;
                } else if (m.text.length() == 0 && m.attachments.size() == 0) {
                    ((ListItem) fwdItems.get(0)).type = 2;
                    ((ListItem) fwdItems.get(fwdItems.size() - 1)).type = 4;
                }
                result.addAll(fwdItems);
                item = (ListItem) result.get(result.size() - 1);
                if (this.peer > 2000000000 && !m.out && this.chatUsers.containsKey(Integer.valueOf(m.sender))) {
                    chatFragment = this;
                    img = new Image();
                    img.viewId = C0436R.id.msg_sender_photo;
                    img.url = ((UserProfile) this.chatUsers.get(Integer.valueOf(m.sender))).photo;
                    item.images.add(img);
                }
            }
            item.time = m.time;
            item.isFailed = m.sendFailed;
        }
        return result;
    }

    private ArrayList<ListItem> buildItems(ArrayList<FwdMessage> msgs, int level, boolean out, int msgId, boolean rstate, ArrayList<Integer> fwdUids) {
        ArrayList<ListItem> result = new ArrayList();
        int i = 0;
        Iterator it = msgs.iterator();
        while (it.hasNext()) {
            FwdMessage m = (FwdMessage) it.next();
            ListItem item = new ListItem();
            item.text = m.displayableText;
            boolean z = (m.displayableText instanceof Spannable) && ((URLSpan[]) ((Spannable) m.displayableText).getSpans(0, m.displayableText.length() - 1, URLSpan.class)).length > 0;
            item.hasLinks = z;
            item.attachments = m.attachments;
            int sz = Math.min(this.contentView.getWidth(), Global.scale(350.0f));
            ZhukovLayout.processThumbs((sz - Global.scale(115.0f)) - (Global.scale(6.0f) * level), sz, item.attachments);
            item.fwdLevel = level;
            item.type = 3;
            item.isOut = out;
            item.images = new ArrayList();
            item.msgId = msgId;
            item.readState = rstate;
            item.fwdName = m.username;
            if ("DELETED".equals(item.fwdName)) {
                item.fwdName = getString(C0436R.string.loading);
            }
            item.fwdTime = m.time;
            item.fwdUid = m.sender;
            result.add(item);
            if (!fwdUids.contains(Integer.valueOf(m.sender))) {
                fwdUids.add(Integer.valueOf(m.sender));
            }
            if (m.fwdMessages.size() > 0 && level <= 10) {
                result.addAll(buildItems(m.fwdMessages, level + 1, out, msgId, rstate, fwdUids));
            }
            i++;
        }
        return result;
    }

    private int removeRepeatingDates() {
        ArrayList<ListItem> toRemove = new ArrayList();
        int prevDay = 0;
        boolean prevIsDate = false;
        Iterator it = this.items.iterator();
        while (it.hasNext()) {
            ListItem item = (ListItem) it.next();
            if (item.type == 5 && item.text == null) {
                if (item.time == prevDay || prevIsDate) {
                    toRemove.add(item);
                }
                prevDay = item.time;
                prevIsDate = true;
            } else {
                prevIsDate = false;
            }
        }
        this.items.removeAll(toRemove);
        return toRemove.size();
    }

    private void updateList() {
        if (getActivity() == null) {
            Log.m526e("vk", "update list when activity is null!!!");
        } else {
            getActivity().runOnUiThread(new Runnable() {

                /* renamed from: com.vkontakte.android.fragments.ChatFragment.26.1 */
                class C14461 implements GetUsersCallback {

                    /* renamed from: com.vkontakte.android.fragments.ChatFragment.26.1.1 */
                    class C06241 implements Runnable {
                        C06241() {
                        }

                        public void run() {
                            ChatFragment.this.updateChatUsersPhotos();
                        }
                    }

                    C14461() {
                    }

                    public void onUsersLoaded(ArrayList<UserProfile> users) {
                        Log.m528i("vk", "Users loaded " + users);
                        Iterator it = users.iterator();
                        while (it.hasNext()) {
                            UserProfile u = (UserProfile) it.next();
                            ChatFragment.this.chatUsers.put(Integer.valueOf(u.uid), u);
                        }
                        if (ChatFragment.this.contentView != null) {
                            ChatFragment.this.contentView.post(new C06241());
                        }
                    }
                }

                public void run() {
                    try {
                        ChatFragment.this.adapter.notifyDataSetChanged();
                        ChatFragment.this.imgLoader.updateImages();
                        if (ChatFragment.this.peer > 2000000000 && ChatFragment.this.chatUsers.size() > 0) {
                            ArrayList<Integer> needUsers = new ArrayList();
                            Iterator it = ChatFragment.this.messages.iterator();
                            while (it.hasNext()) {
                                Message m = (Message) it.next();
                                if (!ChatFragment.this.chatUsers.containsKey(Integer.valueOf(m.sender))) {
                                    Log.m530w("vk", "Unknown chat user: " + m.sender);
                                    if (!needUsers.contains(Integer.valueOf(m.sender))) {
                                        needUsers.add(Integer.valueOf(m.sender));
                                    }
                                }
                            }
                            if (needUsers.size() > 0) {
                                Friends.getUsers(needUsers, new C14461());
                            }
                        }
                    } catch (Exception e) {
                    }
                }
            });
        }
    }

    private void sendMessage() {
        if (!this.dataLoading || this.messages.size() != 0) {
            String msg = this.writeBar.getText().trim();
            ArrayList<Attachment> atts = this.writeBar.getAttachments();
            ArrayList<Message> fwd = new ArrayList();
            Iterator it = atts.iterator();
            while (it.hasNext()) {
                Attachment a = (Attachment) it.next();
                if (a instanceof FwdMessagesAttachment) {
                    atts.remove(a);
                    fwd.addAll(((FwdMessagesAttachment) a).msgs);
                    break;
                }
            }
            if (msg.length() != 0 || atts.size() != 0 || fwd.size() != 0) {
                Message m = Messages.send(this.peer, msg, atts, fwd);
                this.messages.add(m);
                ArrayList<Message> al = new ArrayList();
                al.add(m);
                ArrayList<Integer> fuids = new ArrayList();
                this.items.addAll(buildItems(al, fuids, true, false));
                removeRepeatingDates();
                updateList();
                loadFwdUsers(fuids);
                this.writeBar.setText(ACRAConstants.DEFAULT_STRING_VALUE);
                this.writeBar.clearAttachments();
                this.messagesToForward.clear();
                this.list.post(new Runnable() {
                    public void run() {
                        ChatFragment.this.list.setSelection(9999999);
                    }
                });
            }
        }
    }

    private void sendSticker(StickerAttachment s) {
        if (!this.dataLoading || this.messages.size() != 0) {
            ArrayList<Attachment> atts = new ArrayList();
            atts.add(s);
            Message m = Messages.send(this.peer, ACRAConstants.DEFAULT_STRING_VALUE, atts, new ArrayList());
            this.messages.add(m);
            ArrayList<Message> al = new ArrayList();
            al.add(m);
            ArrayList<Integer> fuids = new ArrayList();
            this.items.addAll(buildItems(al, fuids, true, false));
            removeRepeatingDates();
            updateList();
            loadFwdUsers(fuids);
            this.list.post(new Runnable() {
                public void run() {
                    ChatFragment.this.list.setSelection(9999999);
                }
            });
        }
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        super.onActivityResult(reqCode, resCode, data);
        if (reqCode > 10000) {
            this.writeBar.onActivityResult(reqCode, resCode, data);
        }
        if (reqCode == FORWARD_RESULT && resCode == -1) {
            UserProfile profile = (UserProfile) data.getParcelableExtra("profile");
            if (profile.uid != this.peer || getArguments().containsKey("from_search")) {
                Bundle args = new Bundle();
                args.putInt("id", profile.uid);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, profile.fullName);
                if (profile.uid < 2000000000) {
                    args.putCharSequence("photo", profile.photo);
                }
                args.putParcelableArrayList("fwd", this.messagesToForward);
                Navigate.to("ChatFragment", args, getActivity());
                return;
            }
            this.writeBar.addFwdMessages(this.messagesToForward);
        }
    }

    private void markAsRead() {
        ArrayList<Integer> ids = new ArrayList();
        Iterator it = this.messages.iterator();
        while (it.hasNext()) {
            Message msg = (Message) it.next();
            if (!(msg.out || msg.readState)) {
                ids.add(Integer.valueOf(msg.id));
            }
        }
        if (ids.size() > 0) {
            Messages.markAsRead(ids);
        }
    }

    private void sendTypingIfNeeded() {
        if (System.currentTimeMillis() - this.lastTypingRequest >= 5000) {
            this.lastTypingRequest = System.currentTimeMillis();
            new MessagesSetActivity(this.peer).exec();
        }
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id) {
        if (this.actionMode != null) {
            return false;
        }
        Message msg = null;
        int mid = ((ListItem) this.items.get(pos - this.list.getHeaderViewsCount())).msgId;
        Iterator it = this.messages.iterator();
        while (it.hasNext()) {
            Message m = (Message) it.next();
            if (m.id == mid) {
                msg = m;
                break;
            }
        }
        if (msg == null) {
            return false;
        }
        this.actionMode = getSherlockActivity().startActionMode(this.actionModeCallback);
        this.selectedMessages.add(msg);
        this.actionMode.setTitle(getString(C0436R.string.selected_n, Integer.valueOf(this.selectedMessages.size())));
        this.adapter.notifyDataSetChanged();
        return true;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        Message msg = null;
        int mid = ((ListItem) this.items.get(pos - this.list.getHeaderViewsCount())).msgId;
        Iterator it = this.messages.iterator();
        while (it.hasNext()) {
            Message m = (Message) it.next();
            if (m.id == mid) {
                msg = m;
                break;
            }
        }
        if (msg != null) {
            if (this.actionMode != null) {
                if (this.selectedMessages.contains(msg)) {
                    this.selectedMessages.remove(msg);
                } else {
                    this.selectedMessages.add(msg);
                }
                this.actionMode.setTitle(getString(C0436R.string.selected_n, Integer.valueOf(this.selectedMessages.size())));
                this.adapter.notifyDataSetChanged();
                if (this.selectedMessages.size() == 0) {
                    this.actionMode.finish();
                    return;
                } else {
                    this.actionMode.getMenu().findItem(C0436R.id.copy).setVisible(this.selectedMessages.size() == 1);
                    return;
                }
            }
            showMessageOptions(msg);
        }
    }

    private void showMessageOptions(Message msg) {
        String[] items = new String[3];
        items[0] = msg.sendFailed ? getString(C0436R.string.retry) : getString(C0436R.string.msg_forward);
        items[1] = getString(C0436R.string.copy_text);
        items[2] = getString(C0436R.string.delete);
        new Builder(getActivity()).setTitle(C0436R.string.message).setItems(items, new AnonymousClass29(msg)).show();
    }

    private void confirmAndDelete(ArrayList<Message> msgs) {
        new Builder(getActivity()).setTitle(C0436R.string.delete_msgs_title).setMessage(getString(C0436R.string.delete_msgs_confirm, Global.langPlural(C0436R.array.qty_msgs, msgs.size(), getResources()))).setPositiveButton(C0436R.string.yes, new AnonymousClass30(msgs)).setNegativeButton(C0436R.string.no, null).show();
    }

    private void forward(ArrayList<Message> msgs) {
        this.messagesToForward.clear();
        this.messagesToForward.addAll(msgs);
        Collections.sort(this.messagesToForward, new Comparator<Message>() {
            public int compare(Message lhs, Message rhs) {
                return lhs.time > rhs.time ? 1 : -1;
            }
        });
        startActivityForResult(new Intent(getActivity(), ForwardMessageActivity.class), FORWARD_RESULT);
    }

    private void loadFwdUsers(ArrayList<Integer> uids) {
        Friends.getUsers(uids, new GetUsersCallback() {

            /* renamed from: com.vkontakte.android.fragments.ChatFragment.32.1 */
            class C06261 implements Runnable {
                private final /* synthetic */ ArrayList val$users;

                C06261(ArrayList arrayList) {
                    this.val$users = arrayList;
                }

                public void run() {
                    HashMap<Integer, UserProfile> p = new HashMap();
                    Iterator it = this.val$users.iterator();
                    while (it.hasNext()) {
                        UserProfile up = (UserProfile) it.next();
                        p.put(Integer.valueOf(up.uid), up);
                    }
                    Iterator it2 = ChatFragment.this.items.iterator();
                    while (it2.hasNext()) {
                        ListItem item = (ListItem) it2.next();
                        if (item.fwdLevel > 0 && p.containsKey(Integer.valueOf(item.fwdUid))) {
                            item.fwdName = ((UserProfile) p.get(Integer.valueOf(item.fwdUid))).fullName;
                            Image im = new Image(null);
                            im.viewId = C0436R.id.msg_fwd_photo;
                            im.url = ((UserProfile) p.get(Integer.valueOf(item.fwdUid))).photo;
                            item.images.add(im);
                        }
                    }
                    ChatFragment.this.updateList();
                }
            }

            public void onUsersLoaded(ArrayList<UserProfile> users) {
                ChatFragment.this.list.post(new C06261(users));
            }
        });
    }

    private boolean isSelected(int mid) {
        Iterator it = this.selectedMessages.iterator();
        while (it.hasNext()) {
            if (((Message) it.next()).id == mid) {
                return true;
            }
        }
        return false;
    }

    private void retryFailed(Message msg) {
        this.messages.remove(msg);
        ArrayList<ListItem> toRemove = new ArrayList();
        Iterator it = this.items.iterator();
        while (it.hasNext()) {
            ListItem item = (ListItem) it.next();
            if (item.msgId == msg.id) {
                toRemove.add(item);
            }
        }
        this.items.removeAll(toRemove);
        ArrayList<Integer> ids = new ArrayList();
        ids.add(Integer.valueOf(msg.id));
        Cache.deleteMessages(ids);
        Message m = Messages.send(this.peer, msg.text, msg.attachments, msg.fwdMessages);
        this.messages.add(m);
        ArrayList<Message> al = new ArrayList();
        al.add(m);
        this.items.addAll(buildItems(al, new ArrayList(), true, false));
        updateList();
    }

    private void prependMessages(List<Message> msgs) {
        if (getActivity() != null) {
            boolean wasDate;
            int itemOffset = -1;
            int firstVisible = this.list.getFirstVisiblePosition();
            if (firstVisible == 0) {
                if (this.list.getChildCount() > 3) {
                    firstVisible += 2;
                    itemOffset = this.list.getChildAt(3).getTop();
                }
            } else if (firstVisible == 1) {
                if (this.list.getChildCount() > 2) {
                    firstVisible++;
                    itemOffset = this.list.getChildAt(2).getTop();
                }
            } else if (this.list.getChildCount() > 1) {
                itemOffset = this.list.getChildAt(0).getTop();
            }
            int itemsBefore = this.items.size();
            this.messages.addAll(0, msgs);
            ArrayList<Integer> fuids = new ArrayList();
            if (this.items.size() > 0 && ((ListItem) this.items.get(0)).type == 5 && ((ListItem) this.items.get(0)).text == null) {
                wasDate = true;
            } else {
                wasDate = false;
            }
            ArrayList<ListItem> litems = buildItems(msgs, fuids, false, false);
            this.items.addAll(0, litems);
            int ndates = removeRepeatingDates();
            if (!wasDate || this.items.size() <= 0 || (((ListItem) this.items.get(itemsBefore)).type == 5 && ((ListItem) this.items.get(itemsBefore)).text == null)) {
                wasDate = false;
            } else {
                wasDate = true;
            }
            updateList();
            this.list.setSelectionFromTop((litems.size() + (firstVisible - ndates)) + 1, itemOffset);
            loadFwdUsers(fuids);
        }
    }

    public boolean onBackPressed() {
        if (!this.emojiPopup.isShowing()) {
            return false;
        }
        hideEmojiPopup();
        return true;
    }
}
