package com.vkontakte.android.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Friends.GetImportedContactsCallback;
import com.vkontakte.android.ui.Fonts;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.MergeAdapter;
import com.vkontakte.android.ui.MergeImageLoaderAdapter;
import java.util.ArrayList;
import java.util.Iterator;

public class SuggestionsImportedFragment extends SuggestionsFragment {
    private ArrayList<UserProfile> invitations;

    protected class InvitationsAdapter extends BaseAdapter {

        /* renamed from: com.vkontakte.android.fragments.SuggestionsImportedFragment.InvitationsAdapter.1 */
        class C08421 implements OnClickListener {
            C08421() {
            }

            public void onClick(View v) {
                SuggestionsImportedFragment.this.onInviteClick(((Integer) v.getTag()).intValue());
            }
        }

        protected InvitationsAdapter() {
        }

        public int getCount() {
            return SuggestionsImportedFragment.this.invitations.size();
        }

        public Object getItem(int position) {
            return SuggestionsImportedFragment.this.invitations.get(position);
        }

        public long getItemId(int position) {
            return (long) ((UserProfile) SuggestionsImportedFragment.this.invitations.get(position)).uid;
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public boolean isEnabled(int item) {
            return false;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = View.inflate(SuggestionsImportedFragment.this.getActivity(), C0436R.layout.suggest_list_item_invite, null);
                ((TextView) view.findViewById(C0436R.id.flist_item_subtext)).setTypeface(Fonts.getRobotoLight());
                view.findViewById(C0436R.id.flist_item_online).setOnClickListener(new C08421());
            }
            if (position == 0) {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_top);
            } else if (position == SuggestionsImportedFragment.this.invitations.size() - 1) {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_btm);
            } else {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_mid);
            }
            UserProfile u = (UserProfile) SuggestionsImportedFragment.this.invitations.get(position);
            ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(u.fullName);
            ((TextView) view.findViewById(C0436R.id.flist_item_subtext)).setText(u.university);
            if (SuggestionsImportedFragment.this.imgLoader.isAlreadyLoaded(u.photo)) {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageBitmap(SuggestionsImportedFragment.this.imgLoader.get(u.photo));
            } else {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(u.uid < 0 ? C0436R.drawable.group_placeholder : C0436R.drawable.user_placeholder);
            }
            view.findViewById(C0436R.id.flist_item_online).setTag(Integer.valueOf(position));
            if (u.online == 0) {
                boolean added = u.isFriend;
            } else if (u.online > 0) {
            }
            return view;
        }
    }

    private class TitleItemAdapter extends BaseAdapter {
        private TitleItemAdapter() {
        }

        public int getCount() {
            return SuggestionsImportedFragment.this.invitations.size() > 0 ? 1 : 0;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public boolean isEnabled(int item) {
            return false;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView != null) {
                return convertView;
            }
            TextView txt = (TextView) View.inflate(SuggestionsImportedFragment.this.getActivity(), C0436R.layout.list_cards_section_header, null);
            txt.setText(SuggestionsImportedFragment.this.getString(C0436R.string.invite_section_title).toUpperCase());
            return txt;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.SuggestionsImportedFragment.1 */
    class C15141 implements GetImportedContactsCallback {

        /* renamed from: com.vkontakte.android.fragments.SuggestionsImportedFragment.1.1 */
        class C08411 implements Runnable {
            private final /* synthetic */ ArrayList val$imported;
            private final /* synthetic */ ArrayList val$notOnVk;

            C08411(ArrayList arrayList, ArrayList arrayList2) {
                this.val$imported = arrayList;
                this.val$notOnVk = arrayList2;
            }

            public void run() {
                int service = SuggestionsImportedFragment.this.getArguments().getInt("service", 0);
                Iterator it = this.val$imported.iterator();
                while (it.hasNext()) {
                    UserProfile p = (UserProfile) it.next();
                    if (!p.isFriend) {
                        SuggestionsImportedFragment.this.users.add(p);
                    }
                }
                it = this.val$imported.iterator();
                while (it.hasNext()) {
                    p = (UserProfile) it.next();
                    if (p.isFriend) {
                        SuggestionsImportedFragment.this.users.add(p);
                    }
                }
                if (service == 0 || service == 3) {
                    SuggestionsImportedFragment.this.invitations.addAll(this.val$notOnVk);
                    Iterator it2 = SuggestionsImportedFragment.this.invitations.iterator();
                    while (it2.hasNext()) {
                        p = (UserProfile) it2.next();
                        p.university = (String) p.extra;
                    }
                }
                SuggestionsImportedFragment.this.updateList();
            }
        }

        C15141() {
        }

        public void onUsersLoaded(ArrayList<UserProfile> imported, ArrayList<UserProfile> notOnVk) {
            if (SuggestionsImportedFragment.this.getActivity() != null) {
                SuggestionsImportedFragment.this.getActivity().runOnUiThread(new C08411(imported, notOnVk));
            }
        }
    }

    protected class InvitationsPhotosAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.SuggestionsImportedFragment.InvitationsPhotosAdapter.1 */
        class C08431 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C08431(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.flist_item_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        protected InvitationsPhotosAdapter() {
        }

        public int getItemCount() {
            return SuggestionsImportedFragment.this.invitations.size();
        }

        public int getImageCountForItem(int item) {
            return ((UserProfile) SuggestionsImportedFragment.this.invitations.get(item)).photo != null ? 1 : 0;
        }

        public String getImageURL(int item, int image) {
            return ((UserProfile) SuggestionsImportedFragment.this.invitations.get(item)).photo;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            if (SuggestionsImportedFragment.this.list != null) {
                item += SuggestionsImportedFragment.this.list.getHeaderViewsCount();
                if (item >= SuggestionsImportedFragment.this.list.getFirstVisiblePosition() && item <= SuggestionsImportedFragment.this.list.getLastVisiblePosition()) {
                    SuggestionsImportedFragment.this.getActivity().runOnUiThread(new C08431(SuggestionsImportedFragment.this.list.getChildAt(item - SuggestionsImportedFragment.this.list.getFirstVisiblePosition()), bitmap));
                }
            }
        }
    }

    private class TitleItemImageLoaderAdapter extends ListImageLoaderAdapter {
        private TitleItemImageLoaderAdapter() {
        }

        public int getItemCount() {
            return SuggestionsImportedFragment.this.invitations.size() > 0 ? 1 : 0;
        }

        public int getImageCountForItem(int item) {
            return 0;
        }

        public String getImageURL(int item, int image) {
            return null;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
        }
    }

    public SuggestionsImportedFragment() {
        this.invitations = new ArrayList();
    }

    protected void loadData() {
        Friends.getImportedContacts(getArguments().getInt("service", 0), new C15141());
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        if (getArguments().containsKey(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)) {
            act.setTitle(getArguments().getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
        }
    }

    protected BaseAdapter getAdapter() {
        MergeAdapter m = new MergeAdapter();
        m.addAdapter(super.getAdapter());
        m.addAdapter(new TitleItemAdapter());
        m.addAdapter(new InvitationsAdapter());
        return m;
    }

    protected ListImageLoaderAdapter getImageLoaderAdapter() {
        MergeImageLoaderAdapter m = new MergeImageLoaderAdapter();
        m.addAdapter(super.getImageLoaderAdapter());
        m.addAdapter(new TitleItemImageLoaderAdapter());
        m.addAdapter(new InvitationsPhotosAdapter());
        return m;
    }

    protected void onItemClick(int pos, long id, Object item) {
        if (id == 0) {
            int service = getArguments().getInt("service", 0);
            UserProfile item2 = (UserProfile) item;
            return;
        }
        Bundle args = new Bundle();
        args.putInt("id", (int) id);
        Navigate.to("ProfileFragment", args, getActivity());
    }

    protected String getListTitle() {
        return null;
    }

    private void onInviteClick(int pos) {
        int service = getArguments().getInt("service", 0);
        UserProfile user = (UserProfile) this.invitations.get(pos);
        if (service == 0) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("sms:" + user.extra));
            intent.putExtra("sms_body", getString(C0436R.string.invitation));
            startActivity(intent);
        }
        if (service == 3) {
            intent = new Intent("android.intent.action.VIEW", Uri.parse("mailto:" + user.extra));
            intent.putExtra("android.intent.extra.SUBJECT", getString(C0436R.string.app_name));
            intent.putExtra("android.intent.extra.TEXT", getString(C0436R.string.invitation));
            startActivity(intent);
        }
    }
}
