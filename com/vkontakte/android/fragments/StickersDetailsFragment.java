package com.vkontakte.android.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.Scroller;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.BillingActivity;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageCache;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.StickerDownloaderService;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.data.Stickers;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.ui.CircularProgressDrawable;
import com.vkontakte.android.ui.ParallaxDrawable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import org.acra.ACRAConstants;

public class StickersDetailsFragment extends SherlockDialogFragment {
    private Runnable autoscroller;
    private boolean closeAfterDownload;
    private int id;
    private String price;
    private BroadcastReceiver receiver;
    private FixedSpeedScroller scroller;
    private Bitmap[] slideBitmaps;
    private ArrayList<String> slides;
    private String storeID;
    private View view;

    /* renamed from: com.vkontakte.android.fragments.StickersDetailsFragment.1 */
    class C08191 extends BroadcastReceiver {
        C08191() {
        }

        public void onReceive(Context arg0, Intent intent) {
            if (Stickers.ACTION_STICKERS_UPDATED.equals(intent.getAction())) {
                StickersDetailsFragment.this.updateState();
            }
            if (Stickers.ACTION_STICKERS_DOWNLOAD_PROGRESS.equals(intent.getAction())) {
                StickersDetailsFragment.this.updateState();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.StickersDetailsFragment.2 */
    class C08202 implements Runnable {
        C08202() {
        }

        public void run() {
            if (StickersDetailsFragment.this.view != null) {
                ViewPager pager = (ViewPager) StickersDetailsFragment.this.view.findViewById(C0436R.id.pager);
                if (pager != null) {
                    int page = (pager.getCurrentItem() + 1) % pager.getAdapter().getCount();
                    StickersDetailsFragment.this.scroller.autoScroll = true;
                    pager.setCurrentItem(page, true);
                    pager.postDelayed(this, 5000);
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.StickersDetailsFragment.3 */
    class C08233 implements OnClickListener {

        /* renamed from: com.vkontakte.android.fragments.StickersDetailsFragment.3.1 */
        class C08211 implements Runnable {
            private final /* synthetic */ View val$v;

            C08211(View view) {
                this.val$v = view;
            }

            public void run() {
                this.val$v.setEnabled(true);
            }
        }

        /* renamed from: com.vkontakte.android.fragments.StickersDetailsFragment.3.2 */
        class C08222 implements Runnable {
            C08222() {
            }

            public void run() {
                if (Stickers.getPackState(StickersDetailsFragment.this.id) == 3) {
                    StickersDetailsFragment.this.downloadPack();
                    StickersDetailsFragment.this.closeAfterDownload = true;
                }
            }
        }

        C08233() {
        }

        public void onClick(View v) {
            v.setEnabled(false);
            v.postDelayed(new C08211(v), 300);
            int state = Stickers.getPackState(StickersDetailsFragment.this.id);
            if (state == 3 || state == 4 || state == 5) {
                StickersDetailsFragment.this.downloadPack();
                StickersDetailsFragment.this.closeAfterDownload = true;
            } else if (StickersDetailsFragment.this.storeID == null || StickersDetailsFragment.this.storeID.length() <= 0) {
                Stickers.activateFreePack(StickersDetailsFragment.this.id, StickersDetailsFragment.this.getActivity(), new C08222());
            } else {
                Intent intent = new Intent(StickersDetailsFragment.this.getActivity(), BillingActivity.class);
                intent.putExtra("product", StickersDetailsFragment.this.id);
                intent.putExtra("store_id", StickersDetailsFragment.this.storeID);
                StickersDetailsFragment.this.startActivityForResult(intent, 101);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.StickersDetailsFragment.5 */
    class C08265 implements Runnable {

        /* renamed from: com.vkontakte.android.fragments.StickersDetailsFragment.5.1 */
        class C08241 implements Runnable {
            private final /* synthetic */ Bitmap val$bg;

            C08241(Bitmap bitmap) {
                this.val$bg = bitmap;
            }

            public void run() {
                if (StickersDetailsFragment.this.view != null) {
                    ViewPager pager = (ViewPager) StickersDetailsFragment.this.view.findViewById(C0436R.id.pager);
                    ParallaxDrawable pd = new ParallaxDrawable();
                    if (VERSION.SDK_INT >= 14) {
                        pd.setCornersRadius(Global.scale(ImageViewHolder.PaddingSize), Global.scale(ImageViewHolder.PaddingSize), 0, 0);
                    }
                    pd.setBitmap(this.val$bg);
                    pd.setOffset(((float) pager.getCurrentItem()) / ((float) pager.getAdapter().getCount()));
                    pager.setBackgroundDrawable(pd);
                }
            }
        }

        /* renamed from: com.vkontakte.android.fragments.StickersDetailsFragment.5.2 */
        class C08252 implements Runnable {
            private final /* synthetic */ ViewPager val$pager;

            C08252(ViewPager viewPager) {
                this.val$pager = viewPager;
            }

            public void run() {
                this.val$pager.setAdapter(this.val$pager.getAdapter());
            }
        }

        C08265() {
        }

        public void run() {
            Bitmap bg = ImageCache.get(StickersDetailsFragment.this.getArguments().getString("background_src"));
            if (StickersDetailsFragment.this.getActivity() != null) {
                StickersDetailsFragment.this.getActivity().runOnUiThread(new C08241(bg));
                int i = 0;
                while (i < StickersDetailsFragment.this.slides.size() && StickersDetailsFragment.this.view != null && StickersDetailsFragment.this.getActivity() != null) {
                    StickersDetailsFragment.this.slideBitmaps[i] = ImageCache.get((String) StickersDetailsFragment.this.slides.get(i));
                    if (StickersDetailsFragment.this.view != null && StickersDetailsFragment.this.getActivity() != null) {
                        ViewPager pager = (ViewPager) StickersDetailsFragment.this.view.findViewById(C0436R.id.pager);
                        int cur = pager.getCurrentItem();
                        if (cur == i || cur == i - 1 || cur == i + 1) {
                            if (StickersDetailsFragment.this.getActivity() != null) {
                                StickersDetailsFragment.this.getActivity().runOnUiThread(new C08252(pager));
                            } else {
                                return;
                            }
                        }
                        i++;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.StickersDetailsFragment.6 */
    class C08276 implements Runnable {
        C08276() {
        }

        public void run() {
            StickersDetailsFragment.this.dismiss();
        }
    }

    private static class FixedSpeedScroller extends Scroller {
        public boolean autoScroll;
        private int mDuration;

        public FixedSpeedScroller(Context context) {
            super(context);
            this.mDuration = LocationStatusCodes.GEOFENCE_NOT_AVAILABLE;
            this.autoScroll = false;
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator) {
            super(context, interpolator);
            this.mDuration = LocationStatusCodes.GEOFENCE_NOT_AVAILABLE;
            this.autoScroll = false;
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator, boolean flywheel) {
            super(context, interpolator, flywheel);
            this.mDuration = LocationStatusCodes.GEOFENCE_NOT_AVAILABLE;
            this.autoScroll = false;
        }

        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            if (this.autoScroll) {
                super.startScroll(startX, startY, dx, dy, this.mDuration);
                this.autoScroll = false;
                return;
            }
            super.startScroll(startX, startY, dx, dy, duration);
        }

        public void startScroll(int startX, int startY, int dx, int dy) {
            super.startScroll(startX, startY, dx, dy, this.mDuration);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.StickersDetailsFragment.4 */
    class C15054 implements OnPageChangeListener {
        private final /* synthetic */ LinearLayout val$dots;
        private final /* synthetic */ ViewPager val$pager;

        C15054(LinearLayout linearLayout, ViewPager viewPager) {
            this.val$dots = linearLayout;
            this.val$pager = viewPager;
        }

        public void onPageSelected(int pos) {
            int i = 0;
            while (i < this.val$dots.getChildCount()) {
                this.val$dots.getChildAt(i).setSelected(i == pos);
                i++;
            }
        }

        public void onPageScrolled(int pos, float offset, int offsetPixels) {
            float position = ((float) pos) + offset;
            if (this.val$pager.getBackground() instanceof ParallaxDrawable) {
                ((ParallaxDrawable) this.val$pager.getBackground()).setOffset(position / ((float) (this.val$pager.getAdapter().getCount() - 1)));
            }
        }

        public void onPageScrollStateChanged(int state) {
            this.val$pager.removeCallbacks(StickersDetailsFragment.this.autoscroller);
            if (state == 0) {
                this.val$pager.postDelayed(StickersDetailsFragment.this.autoscroller, 5000);
            }
        }
    }

    private class SlidePagerAdapter extends PagerAdapter {
        private SlidePagerAdapter() {
        }

        public int getCount() {
            return StickersDetailsFragment.this.slides.size();
        }

        public boolean isViewFromObject(View v, Object o) {
            return v == o;
        }

        public Object instantiateItem(ViewGroup container, int position) {
            ImageView view = new ImageView(StickersDetailsFragment.this.getActivity());
            view.setImageBitmap(StickersDetailsFragment.this.slideBitmaps[position]);
            container.addView(view);
            return view;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    public StickersDetailsFragment() {
        this.receiver = new C08191();
        this.autoscroller = new C08202();
        this.closeAfterDownload = false;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Stickers.ACTION_STICKERS_UPDATED);
        filter.addAction(Stickers.ACTION_STICKERS_DOWNLOAD_PROGRESS);
        VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
    }

    public void onDetach() {
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
        super.onDetach();
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        setStyle(1, 0);
        this.id = getArguments().getInt("id", 0);
        this.price = getArguments().getString("price");
        this.storeID = getArguments().getString("storeID");
        this.slides = getArguments().getStringArrayList("slides");
        this.slideBitmaps = new Bitmap[this.slides.size()];
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(C0436R.layout.sticker_details, null);
        if (getDialog() != null) {
            getDialog().getWindow().setWindowAnimations(C0436R.style.StickerDialogAnim);
        }
        ((TextView) this.view.findViewById(C0436R.id.title)).setText(getArguments().getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
        ((TextView) this.view.findViewById(C0436R.id.count)).setText(getArguments().getString("author"));
        ((TextView) this.view.findViewById(C0436R.id.description)).setText(getArguments().getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION));
        CircularProgressDrawable pd = new CircularProgressDrawable();
        pd.setColors(-986896, -4210753);
        pd.setThickness(2);
        pd.setPad(false);
        pd.setDimBackground(false);
        ((ProgressBar) this.view.findViewById(C0436R.id.sticker_progress)).setProgressDrawable(pd);
        OnClickListener btnClickListener = new C08233();
        this.view.findViewById(C0436R.id.sticker_button).setOnClickListener(btnClickListener);
        this.view.findViewById(C0436R.id.sticker_error).setOnClickListener(btnClickListener);
        ViewPager pager = (ViewPager) this.view.findViewById(C0436R.id.pager);
        pager.setAdapter(new SlidePagerAdapter());
        LinearLayout dots = (LinearLayout) this.view.findViewById(C0436R.id.pager_dots_wrap);
        int pages = pager.getAdapter().getCount();
        LayoutParams lp = new LayoutParams(Global.scale(7.0f), Global.scale(7.0f));
        int scale = Global.scale(5.0f);
        lp.rightMargin = scale;
        lp.leftMargin = scale;
        int i = 0;
        while (i < pages) {
            View dot = new View(getActivity());
            Drawable d = getResources().getDrawable(C0436R.drawable.sticker_pager_dot);
            d.setColorFilter(new ColorMatrixColorFilter(new float[]{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f}));
            dot.setBackgroundDrawable(d);
            dot.setSelected(i == 0);
            dots.addView(dot, lp);
            i++;
        }
        pager.setOnPageChangeListener(new C15054(dots, pager));
        pager.postDelayed(this.autoscroller, 5000);
        updateState();
        loadImages();
        try {
            this.scroller = new FixedSpeedScroller(pager.getContext(), new DecelerateInterpolator());
            Field mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            mScroller.set(pager, this.scroller);
        } catch (Exception e) {
        }
        if (VERSION.SDK_INT < 14) {
            this.view.setBackgroundColor(-1);
        }
        return this.view;
    }

    private void downloadPack() {
        Intent intent = new Intent(getActivity(), StickerDownloaderService.class);
        intent.putExtra("id", this.id);
        intent.putExtra(PlusShare.KEY_CALL_TO_ACTION_URL, getActivity().getSharedPreferences("stickers", 0).getString("content" + this.id, ACRAConstants.DEFAULT_STRING_VALUE));
        intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, getArguments().getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
        getActivity().startService(intent);
    }

    public void onResume() {
        super.onResume();
        Dialog dlg = getDialog();
        if (getResources().getDisplayMetrics().widthPixels >= Global.scale(360.0f)) {
            dlg.getWindow().setLayout(Global.scale(BitmapDescriptorFactory.HUE_MAGENTA), -2);
        }
    }

    private void loadImages() {
        new Thread(new C08265()).start();
    }

    public void onDestroyView() {
        this.view = null;
        super.onDestroyView();
    }

    private void updateState() {
        if (this.view != null) {
            int state = Stickers.getPackState(this.id);
            if (getArguments().containsKey("unavailable")) {
                state = 7;
            }
            if (StickerDownloaderService.currentInstance != null && (StickerDownloaderService.currentInstance.getCurrentPackId() == this.id || StickerDownloaderService.currentInstance.isInQueue(this.id))) {
                this.view.findViewById(C0436R.id.sticker_button).setVisibility(8);
                this.view.findViewById(C0436R.id.sticker_ok).setVisibility(8);
                this.view.findViewById(C0436R.id.sticker_progress).setVisibility(0);
                this.view.findViewById(C0436R.id.sticker_error).setVisibility(8);
                if (StickerDownloaderService.currentInstance.isInQueue(this.id)) {
                    ((ProgressBar) this.view.findViewById(C0436R.id.sticker_progress)).setProgress(0);
                } else {
                    ((ProgressBar) this.view.findViewById(C0436R.id.sticker_progress)).setProgress(Math.round(StickerDownloaderService.currentInstance.getCurrentProgress() * 100.0f));
                }
            } else if (state == 4) {
                this.view.findViewById(C0436R.id.sticker_button).setVisibility(8);
                this.view.findViewById(C0436R.id.sticker_progress).setVisibility(8);
                this.view.findViewById(C0436R.id.sticker_ok).setVisibility(0);
                this.view.findViewById(C0436R.id.sticker_error).setVisibility(8);
            } else if (state == 3 || state == 5) {
                ((TextView) this.view.findViewById(C0436R.id.sticker_button)).setText(C0436R.string.download);
                this.view.findViewById(C0436R.id.sticker_ok).setVisibility(8);
                this.view.findViewById(C0436R.id.sticker_progress).setVisibility(8);
                this.view.findViewById(C0436R.id.sticker_button).setVisibility(0);
                this.view.findViewById(C0436R.id.sticker_error).setVisibility(8);
            } else if (state == 0 || state == 7) {
                ((TextView) this.view.findViewById(C0436R.id.sticker_button)).setText(this.price != null ? this.price : getString(C0436R.string.price_free));
                this.view.findViewById(C0436R.id.sticker_ok).setVisibility(8);
                this.view.findViewById(C0436R.id.sticker_progress).setVisibility(8);
                this.view.findViewById(C0436R.id.sticker_button).setVisibility(0);
                this.view.findViewById(C0436R.id.sticker_error).setVisibility(8);
                if (state == 7) {
                    ((TextView) this.view.findViewById(C0436R.id.sticker_button)).setText(C0436R.string.unavailable);
                    this.view.findViewById(C0436R.id.sticker_button).setEnabled(false);
                    this.view.findViewById(C0436R.id.sticker_button).getBackground().setAlpha(TransportMediator.FLAG_KEY_MEDIA_NEXT);
                } else {
                    this.view.findViewById(C0436R.id.sticker_button).setEnabled(true);
                    this.view.findViewById(C0436R.id.sticker_button).getBackground().setAlpha(MotionEventCompat.ACTION_MASK);
                }
            } else {
                this.view.findViewById(C0436R.id.sticker_ok).setVisibility(8);
                this.view.findViewById(C0436R.id.sticker_progress).setVisibility(8);
                this.view.findViewById(C0436R.id.sticker_button).setVisibility(8);
                this.view.findViewById(C0436R.id.sticker_error).setVisibility(0);
            }
            if (this.closeAfterDownload && state == 4) {
                new Handler(getActivity().getMainLooper()).postDelayed(new C08276(), 500);
            }
        }
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (data != null && reqCode == 101 && resCode == -1) {
            int id = data.getIntExtra("product", 0);
            if (this.id == id) {
                if (Stickers.getPackState(id) == 3) {
                    downloadPack();
                }
                this.closeAfterDownload = true;
                Stickers.broadcastUpdate();
                updateState();
            }
        }
    }
}
