package com.vkontakte.android.fragments;

public interface BackListener {
    boolean onBackPressed();
}
