package com.vkontakte.android.fragments;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.WebDialog;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.DocumentAttachment;
import com.vkontakte.android.DocumentChooserActivity;
import com.vkontakte.android.Global;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.PendingDocumentAttachment;
import com.vkontakte.android.UploaderService;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.DocsDelete;
import com.vkontakte.android.api.DocsDelete.Callback;
import com.vkontakte.android.api.DocsGet;
import com.vkontakte.android.api.Document;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.PaddingColorDrawable;
import com.vkontakte.android.ui.PhotoView;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

public class DocumentsFragment extends SherlockFragment implements Listener {
    private static final int ADD_RESULT = 101;
    private DocsAdapter adapter;
    private ProgressBar bigProgress;
    private boolean canAdd;
    private FrameLayout contentView;
    private APIRequest currentReq;
    protected boolean dataLoading;
    private Vector<Document> docs;
    private TextView emptyView;
    private ErrorView error;
    private FrameLayout footerView;
    private ListImageLoaderWrapper imgWrapper;
    private ListView list;
    protected boolean moreAvailable;
    private int ownerID;
    private BroadcastReceiver receiver;
    private boolean selectMode;

    /* renamed from: com.vkontakte.android.fragments.DocumentsFragment.1 */
    class C06691 extends BroadcastReceiver {
        C06691() {
        }

        public void onReceive(Context arg0, Intent intent) {
            if (UploaderService.ACTION_UPLOAD_DONE.equals(intent.getAction())) {
                Attachment na = (Attachment) intent.getParcelableExtra("attachment");
                if (na instanceof DocumentAttachment) {
                    DocumentAttachment att = (DocumentAttachment) na;
                    if (att.oid == DocumentsFragment.this.ownerID) {
                        Document doc = new Document();
                        doc.did = att.did;
                        doc.oid = att.oid;
                        doc.size = att.size;
                        doc.thumb = att.thumb;
                        doc.title = att.title;
                        doc.url = att.url;
                        String[] sp = att.title.split("\\.");
                        doc.ext = sp[sp.length - 1];
                        DocumentsFragment.this.docs.add(0, doc);
                        DocumentsFragment.this.updateList();
                    }
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DocumentsFragment.2 */
    class C06702 implements OnItemClickListener {
        C06702() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
            pos -= DocumentsFragment.this.list.getHeaderViewsCount();
            if (DocumentsFragment.this.selectMode) {
                Intent intent = new Intent();
                intent.putExtra("document", (Parcelable) DocumentsFragment.this.docs.get(pos));
                DocumentsFragment.this.getActivity().setResult(-1, intent);
                DocumentsFragment.this.getActivity().finish();
                return;
            }
            String thumb = ((Document) DocumentsFragment.this.docs.get(pos)).thumb;
            String url = ((Document) DocumentsFragment.this.docs.get(pos)).url;
            String fileName = ((Document) DocumentsFragment.this.docs.get(pos)).title;
            if (thumb != null && thumb.length() > 0) {
                Bundle args = new Bundle();
                args.putString("doc_url", url);
                args.putString("doc_title", fileName);
                Navigate.to("PhotoViewerFragment", args, DocumentsFragment.this.getActivity(), true, -1, -1);
            } else if (VERSION.SDK_INT >= 14) {
                Uri uri = Uri.parse(url);
                Request req = new Request(uri);
                req.setDestinationUri(Uri.fromFile(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), uri.getLastPathSegment())));
                if (VERSION.SDK_INT >= 14) {
                    req.setNotificationVisibility(1);
                    req.allowScanningByMediaScanner();
                }
                ((DownloadManager) DocumentsFragment.this.getActivity().getSystemService("download")).enqueue(req);
            } else {
                DocumentsFragment.this.getActivity().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DocumentsFragment.3 */
    class C06723 implements OnItemLongClickListener {

        /* renamed from: com.vkontakte.android.fragments.DocumentsFragment.3.1 */
        class C06711 implements OnClickListener {
            private final /* synthetic */ Document val$doc;

            C06711(Document document) {
                this.val$doc = document;
            }

            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                        DocumentsFragment.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.val$doc.url)));
                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                        DocumentsFragment.this.confirmAndDelete(this.val$doc);
                    default:
                }
            }
        }

        C06723() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id) {
            Document doc = (Document) DocumentsFragment.this.docs.get(pos - DocumentsFragment.this.list.getHeaderViewsCount());
            new Builder(DocumentsFragment.this.getActivity()).setItems(new String[]{DocumentsFragment.this.getString(C0436R.string.download), DocumentsFragment.this.getString(C0436R.string.delete)}, new C06711(doc)).show();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DocumentsFragment.4 */
    class C06734 implements View.OnClickListener {
        C06734() {
        }

        public void onClick(View v) {
            DocumentsFragment.this.error.setVisibility(8);
            DocumentsFragment.this.bigProgress.setVisibility(0);
            DocumentsFragment.this.loadData();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DocumentsFragment.5 */
    class C06745 implements OnClickListener {
        private final /* synthetic */ Document val$doc;

        C06745(Document document) {
            this.val$doc = document;
        }

        public void onClick(DialogInterface dialog, int which) {
            DocumentsFragment.this.doDelete(this.val$doc);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DocumentsFragment.8 */
    class C06758 implements Runnable {
        C06758() {
        }

        public void run() {
            DocumentsFragment.this.adapter.notifyDataSetChanged();
            DocumentsFragment.this.imgWrapper.updateImages();
        }
    }

    private class DocsAdapter extends BaseAdapter {
        private DocsAdapter() {
        }

        public int getCount() {
            return DocumentsFragment.this.docs.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = View.inflate(DocumentsFragment.this.getActivity(), C0436R.layout.documents_item, null);
            }
            Document doc = (Document) DocumentsFragment.this.docs.get(position);
            ((TextView) v.findViewById(C0436R.id.docs_item_title)).setText(doc.title);
            ((TextView) v.findViewById(C0436R.id.docs_item_type)).setText(doc.ext.toUpperCase().substring(0, Math.min(doc.ext.length(), 4)));
            ((TextView) v.findViewById(C0436R.id.docs_item_info)).setText(doc.ext + ", " + Global.langFileSize((long) doc.size, DocumentsFragment.this.getResources()));
            if (doc.thumb == null) {
                ((ImageView) v.findViewById(C0436R.id.docs_item_thumb)).setImageBitmap(null);
            } else if (DocumentsFragment.this.imgWrapper.isAlreadyLoaded(doc.thumb)) {
                ((ImageView) v.findViewById(C0436R.id.docs_item_thumb)).setImageBitmap(DocumentsFragment.this.imgWrapper.get(doc.thumb));
            } else {
                ((ImageView) v.findViewById(C0436R.id.docs_item_thumb)).setImageBitmap(null);
            }
            return v;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DocumentsFragment.6 */
    class C14626 implements Callback {
        private final /* synthetic */ Document val$doc;

        C14626(Document document) {
            this.val$doc = document;
        }

        public void success() {
            DocumentsFragment.this.docs.remove(this.val$doc);
            DocumentsFragment.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(DocumentsFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DocumentsFragment.7 */
    class C14637 implements DocsGet.Callback {
        C14637() {
        }

        public void success(int total, Vector<Document> docs, boolean add) {
            int i;
            DocumentsFragment.this.canAdd = add;
            DocumentsFragment.this.getSherlockActivity().invalidateOptionsMenu();
            DocumentsFragment.this.docs.addAll(docs);
            DocumentsFragment.this.bigProgress.setVisibility(8);
            DocumentsFragment.this.list.setVisibility(0);
            DocumentsFragment.this.moreAvailable = DocumentsFragment.this.docs.size() < total;
            View childAt = DocumentsFragment.this.footerView.getChildAt(0);
            if (DocumentsFragment.this.moreAvailable) {
                i = 0;
            } else {
                i = 8;
            }
            childAt.setVisibility(i);
            DocumentsFragment.this.updateList();
            DocumentsFragment.this.dataLoading = false;
            if (total == 0) {
                DocumentsFragment.this.emptyView.setVisibility(0);
            }
            DocumentsFragment.this.currentReq = null;
        }

        public void fail(int ecode, String emsg) {
            DocumentsFragment.this.currentReq = null;
            DocumentsFragment.this.dataLoading = false;
            if (DocumentsFragment.this.docs.size() == 0) {
                DocumentsFragment.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(DocumentsFragment.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(DocumentsFragment.this.bigProgress, false, PhotoView.THUMB_ANIM_DURATION);
                return;
            }
            Toast.makeText(DocumentsFragment.this.getActivity(), C0436R.string.err_text, 0).show();
        }
    }

    private class DocsThumbsAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.DocumentsFragment.DocsThumbsAdapter.1 */
        class C06761 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C06761(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.docs_item_thumb);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private DocsThumbsAdapter() {
        }

        public int getItemCount() {
            return DocumentsFragment.this.docs.size();
        }

        public int getImageCountForItem(int item) {
            return ((Document) DocumentsFragment.this.docs.get(item)).thumb != null ? 1 : 0;
        }

        public String getImageURL(int item, int image) {
            return ((Document) DocumentsFragment.this.docs.get(item)).thumb;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += DocumentsFragment.this.list.getHeaderViewsCount();
            if (item >= DocumentsFragment.this.list.getFirstVisiblePosition() && item <= DocumentsFragment.this.list.getLastVisiblePosition()) {
                DocumentsFragment.this.contentView.post(new C06761(DocumentsFragment.this.list.getChildAt(item - DocumentsFragment.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    public DocumentsFragment() {
        this.dataLoading = false;
        this.moreAvailable = false;
        this.docs = new Vector();
        this.canAdd = false;
        this.receiver = new C06691();
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        act.setTitle(C0436R.string.docs);
        SherlockFragmentActivity sa = getSherlockActivity();
        sa.getSupportActionBar().setDisplayShowTitleEnabled(true);
        sa.getSupportActionBar().setNavigationMode(0);
        this.ownerID = getArguments().getInt("oid", Global.uid);
        this.selectMode = getArguments().getBoolean("select");
        boolean z = this.ownerID == 0 || this.ownerID == Global.uid;
        this.canAdd = z;
        this.contentView = new FrameLayout(act);
        this.contentView.setBackgroundColor(-1);
        this.footerView = new FrameLayout(act);
        ProgressBar pb = new ProgressBar(act);
        LayoutParams lp = new LayoutParams(Global.scale(BitmapDescriptorFactory.HUE_ORANGE), Global.scale(BitmapDescriptorFactory.HUE_ORANGE));
        lp.gravity = 17;
        pb.setLayoutParams(lp);
        this.footerView.setPadding(0, Global.scale(7.0f), 0, Global.scale(7.0f));
        this.footerView.addView(pb);
        pb.setVisibility(8);
        this.list = new ListView(act);
        this.list.addFooterView(this.footerView, null, false);
        if (this.list.getAdapter() == null) {
            ListView listView = this.list;
            ListAdapter docsAdapter = new DocsAdapter();
            this.adapter = docsAdapter;
            listView.setAdapter(docsAdapter);
        }
        this.list.setDivider(null);
        if (VERSION.SDK_INT < 14) {
            this.list.setCacheColorHint(-1);
            this.list.setBackgroundColor(-1);
        }
        this.list.setVerticalFadingEdgeEnabled(false);
        this.list.setSelector(C0436R.drawable.highlight);
        this.list.setDivider(new PaddingColorDrawable(-1710619, Global.scale(6.0f)));
        this.list.setDividerHeight(Global.scale(1.0f));
        this.list.setVisibility(8);
        this.contentView.addView(this.list);
        this.imgWrapper = new ListImageLoaderWrapper(new DocsThumbsAdapter(), this.list, this);
        this.list.setOnItemClickListener(new C06702());
        if (!this.selectMode && (this.ownerID == Global.uid || this.ownerID == 0 || (this.ownerID < 0 && Groups.isGroupAdmin(-this.ownerID)))) {
            this.list.setOnItemLongClickListener(new C06723());
        }
        this.emptyView = new TextView(act);
        this.emptyView.setTextColor(-8947849);
        this.emptyView.setText(C0436R.string.no_docs);
        this.emptyView.setTextSize(GalleryPickerFooterView.BADGE_SIZE);
        this.emptyView.setGravity(17);
        LayoutParams lparams = new LayoutParams(-1, -2);
        lparams.gravity = 17;
        this.emptyView.setLayoutParams(lparams);
        this.emptyView.setVisibility(8);
        this.contentView.addView(this.emptyView);
        this.bigProgress = new ProgressBar(act);
        LayoutParams lp2 = new LayoutParams(-2, -2);
        lp2.gravity = 17;
        this.bigProgress.setLayoutParams(lp2);
        this.bigProgress.setVisibility(0);
        this.contentView.addView(this.bigProgress);
        this.error = (ErrorView) View.inflate(getActivity(), C0436R.layout.error, null);
        this.error.setOnRetryListener(new C06734());
        this.error.setVisibility(8);
        this.contentView.addView(this.error);
        loadData();
        setHasOptionsMenu(true);
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        IntentFilter filter = new IntentFilter();
        filter.addAction(UploaderService.ACTION_UPLOAD_DONE);
        VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (this.ownerID == Global.uid || this.ownerID == 0 || this.canAdd) {
            inflater.inflate(C0436R.menu.docs, menu);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0436R.id.add) {
            Intent intent = new Intent(getActivity(), DocumentChooserActivity.class);
            intent.putExtra("no_my", true);
            intent.putExtra("limit", 100);
            startActivityForResult(intent, ADD_RESULT);
        }
        return true;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentView;
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.currentReq != null) {
            this.currentReq.cancel();
        }
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
    }

    public void onPause() {
        super.onPause();
        this.imgWrapper.deactivate();
    }

    public void onResume() {
        super.onResume();
        this.imgWrapper.activate();
    }

    private void confirmAndDelete(Document doc) {
        new Builder(getActivity()).setTitle(C0436R.string.confirm).setMessage(C0436R.string.document_delete_confirm).setPositiveButton(C0436R.string.yes, new C06745(doc)).setNegativeButton(C0436R.string.no, null).show();
    }

    private void doDelete(Document doc) {
        new DocsDelete(doc.oid, doc.did).setCallback(new C14626(doc)).wrapProgress(getActivity()).exec(getActivity());
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode == ADD_RESULT && resCode == -1) {
            Iterator it = data.getParcelableArrayListExtra("documents").iterator();
            while (it.hasNext()) {
                PendingDocumentAttachment att = (PendingDocumentAttachment) ((Parcelable) it.next());
                Intent intent = new Intent(getActivity(), UploaderService.class);
                intent.putExtra("new", 1);
                intent.putExtra("file", att.url);
                intent.putExtra("id", att.did);
                intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 4);
                if (this.ownerID < 0) {
                    HashMap<String, String> params = new HashMap();
                    params.put("group_id", new StringBuilder(String.valueOf(-this.ownerID)).toString());
                    intent.putExtra("req_params", params);
                }
                getActivity().startService(intent);
            }
        }
    }

    public void onScrolledToLastItem() {
        if (this.moreAvailable && !this.dataLoading) {
            loadData();
        }
    }

    public void onScrollStarted() {
    }

    public void onScrollStopped() {
    }

    public void loadData() {
        this.dataLoading = true;
        this.currentReq = new DocsGet(this.ownerID, this.docs.size(), 100).setCallback(new C14637()).exec(this.contentView);
    }

    public void updateList() {
        getActivity().runOnUiThread(new C06758());
    }
}
