package com.vkontakte.android.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.WebDialog;
import com.vkontakte.android.ActivityUtils;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImagePickerActivity;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.ProfileEditActivity;
import com.vkontakte.android.ProfileView;
import com.vkontakte.android.UploaderService;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.PhotosDeleteAvatar;
import com.vkontakte.android.api.PhotosDeleteAvatar.Callback;
import com.vkontakte.android.data.Posts;
import java.util.ArrayList;

public class ProfileFragment extends SherlockFragment {
    private static final int AVA_RESULT = 3901;
    private static final int EDIT_RESULT = 3902;
    private BroadcastReceiver receiver;
    private int uid;
    private ProfileView view;

    /* renamed from: com.vkontakte.android.fragments.ProfileFragment.1 */
    class C07821 extends BroadcastReceiver {
        C07821() {
        }

        public void onReceive(Context context, Intent intent) {
            NewsEntry post;
            if (Posts.ACTION_NEW_POST_BROADCAST.equals(intent.getAction())) {
                post = (NewsEntry) intent.getParcelableExtra("entry");
                if (post.ownerID == ProfileFragment.this.uid) {
                    if (post.flag(NewsEntry.FLAG_POSTPONED)) {
                        ProfileFragment.this.view.updatePostsButtons(1, 0);
                    } else if (post.flag(NewsEntry.FLAG_SUGGESTED)) {
                        ProfileFragment.this.view.updatePostsButtons(0, 1);
                    } else {
                        ProfileFragment.this.view.prepend(post);
                    }
                }
            }
            if (Posts.ACTION_POST_DELETED_BROADCAST.equals(intent.getAction())) {
                ProfileFragment.this.view.remove(intent.getIntExtra("owner_id", 0), intent.getIntExtra("post_id", 0));
                if (intent.getIntExtra("owner_id", 0) == ProfileFragment.this.uid) {
                    post = (NewsEntry) intent.getParcelableExtra("post");
                    if (post.flag(NewsEntry.FLAG_POSTPONED)) {
                        ProfileFragment.this.view.updatePostsButtons(-1, 0);
                    } else if (post.flag(NewsEntry.FLAG_SUGGESTED)) {
                        ProfileFragment.this.view.updatePostsButtons(0, -1);
                    }
                }
            }
            if (Posts.ACTION_POST_UPDATED_BROADCAST.equals(intent.getAction())) {
                ProfileFragment.this.view.update(intent.getIntExtra("owner_id", 0), intent.getIntExtra("post_id", 0), intent.getIntExtra("likes", 0), intent.getIntExtra("comments", 0), intent.getIntExtra("retweets", 0), intent.getBooleanExtra("liked", false), intent.getBooleanExtra("retweeted", false));
            }
            if (Posts.ACTION_POST_REPLACED_BROADCAST.equals(intent.getAction())) {
                post = (NewsEntry) intent.getParcelableExtra("entry");
                if (!(post.ownerID != ProfileFragment.this.uid || post.flag(NewsEntry.FLAG_POSTPONED) || post.flag(NewsEntry.FLAG_SUGGESTED))) {
                    ProfileFragment.this.view.replace(post);
                }
            }
            if (!Posts.ACTION_USER_PHOTO_CHANGED.equals(intent.getAction())) {
                return;
            }
            if (ProfileFragment.this.uid == 0 || ProfileFragment.this.uid == Global.uid) {
                ProfileFragment.this.view.setUserPhoto(intent.getStringExtra("photo"));
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ProfileFragment.2 */
    class C07832 implements OnClickListener {
        C07832() {
        }

        public void onClick(View v) {
            ProfileFragment.this.view.scrollToTop();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ProfileFragment.3 */
    class C07843 implements DialogInterface.OnClickListener {
        C07843() {
        }

        public void onClick(DialogInterface dialog, int which) {
            ProfileFragment.this.deletePhoto();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ProfileFragment.4 */
    class C15044 implements Callback {
        C15044() {
        }

        public void success(String newPhoto) {
            ProfileFragment.this.view.setUserPhoto(newPhoto);
            Intent intent = new Intent(Posts.ACTION_USER_PHOTO_CHANGED);
            intent.putExtra("photo", newPhoto);
            ProfileFragment.this.getActivity().sendBroadcast(intent, permission.ACCESS_DATA);
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(ProfileFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    public ProfileFragment() {
        this.receiver = new C07821();
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Posts.ACTION_NEW_POST_BROADCAST);
        filter.addAction(Posts.ACTION_POST_DELETED_BROADCAST);
        filter.addAction(Posts.ACTION_POST_UPDATED_BROADCAST);
        filter.addAction(Posts.ACTION_POST_REPLACED_BROADCAST);
        filter.addAction(Posts.ACTION_USER_PHOTO_CHANGED);
        VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        SherlockFragmentActivity a = getSherlockActivity();
        a.getSupportActionBar().setNavigationMode(0);
        a.getSupportActionBar().setDisplayShowTitleEnabled(true);
        int i = getArguments().getInt("id", Global.uid);
        this.uid = i;
        this.view = new ProfileView(act, i, this);
        if (this.uid == 0) {
            this.uid = Global.uid;
        }
        ActivityUtils.setBeamLink(act, this.uid > 0 ? "id" + this.uid : "club" + this.uid);
        this.view.loadInitial();
        setHasOptionsMenu(true);
        a.setTitle(this.uid < 0 ? C0436R.string.group : C0436R.string.profile);
        int abId = Resources.getSystem().getIdentifier("action_bar_container", "id", "android");
        if (abId == 0) {
            abId = C0436R.id.abs__action_bar_container;
        }
        View actionBarView = getActivity().findViewById(abId);
        if (actionBarView != null) {
            actionBarView.setOnClickListener(new C07832());
        }
    }

    public void onDetach() {
        int abId = Resources.getSystem().getIdentifier("action_bar_container", "id", "android");
        if (abId == 0) {
            abId = C0436R.id.abs__action_bar_container;
        }
        View actionBarView = getActivity().findViewById(abId);
        if (actionBarView != null) {
            actionBarView.setOnClickListener(null);
        }
        super.onDetach();
    }

    public void onPause() {
        super.onPause();
        this.view.onPause();
    }

    public void onResume() {
        super.onResume();
        this.view.onResume();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.view.onCreateOptionsMenu(menu, inflater);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return this.view.onOptionsItemSelected(item);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.view;
    }

    public void showUpdatePhotoDlg(boolean havePhotos) {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra("allow_album", false);
        intent.putExtra("limit", 1);
        ArrayList<String> acts = new ArrayList();
        if (havePhotos) {
            acts.add(getString(C0436R.string.open));
            acts.add(getString(C0436R.string.delete));
            intent.putExtra("custom", acts);
        }
        intent.putExtra("no_thumbs", true);
        startActivityForResult(intent, AVA_RESULT);
    }

    public void editProfile() {
        startActivityForResult(new Intent(getActivity(), ProfileEditActivity.class), EDIT_RESULT);
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode == AVA_RESULT && resCode == -1) {
            Intent intent = new Intent(getActivity(), UploaderService.class);
            intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 6);
            intent.putExtra("file", data.getStringExtra("file"));
            getActivity().startService(intent);
        }
        if (reqCode == AVA_RESULT && resCode == 1) {
            int idx = data.getIntExtra("option", 0);
            if (idx == 0) {
                this.view.openProfilePhotos();
            }
            if (idx == 1) {
                new Builder(getActivity()).setTitle(C0436R.string.confirm).setMessage(C0436R.string.delete_photo_confirm).setPositiveButton(C0436R.string.yes, new C07843()).setNegativeButton(C0436R.string.no, null).show();
            }
        }
        if (reqCode == EDIT_RESULT && resCode == -1) {
            Toast.makeText(getActivity(), C0436R.string.profile_saved, 0).show();
            this.view.loadInitial();
        }
    }

    private void deletePhoto() {
        new PhotosDeleteAvatar().setCallback(new C15044()).wrapProgress(getActivity()).exec(getActivity());
    }
}
