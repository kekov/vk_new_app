package com.vkontakte.android.fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockFragment;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.MultiSectionImageLoaderAdapter;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.SearchIndexer;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.UsersSearch;
import com.vkontakte.android.api.UsersSearch.Callback;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.LoadMoreFooterView;
import com.vkontakte.android.ui.MultiSectionAdapter;
import com.vkontakte.android.ui.PinnedHeaderListView;
import com.vkontakte.android.ui.RefreshableListView;
import com.vkontakte.android.ui.RefreshableListView.OnRefreshListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;

public class FriendListFragment extends SherlockFragment implements OnRefreshListener {
    private static final int SORT_FIRST = 1;
    private static final int SORT_HINTS = 0;
    private static final int SORT_LAST = 2;
    private FriendsAdapter adapter;
    private FrameLayout contentView;
    private boolean creatingSections;
    private APIRequest currentSearchReq;
    private EmptyView emptyView;
    private LoadMoreFooterView footerView;
    private ListImageLoaderWrapper imgLoader;
    private SearchIndexer<UserProfile> indexer;
    private boolean isMe;
    private PinnedHeaderListView list;
    private boolean moreAvailable;
    private boolean multiSelect;
    private boolean preloadOnReady;
    private ArrayList<UserProfile> preloadedSearch;
    private RefreshListener refreshListener;
    private Runnable runAfterInit;
    private String searchQuery;
    private ArrayList<Section> sections;
    private Semaphore sectionsLock;
    private SelectionListener selListener;
    private ArrayList<Integer> selected;
    private boolean showHints;
    private boolean showOnline;
    private int sortPref;
    private boolean useSections;
    private ArrayList<UserProfile> users;

    /* renamed from: com.vkontakte.android.fragments.FriendListFragment.1 */
    class C06801 implements OnItemClickListener {
        C06801() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            int[] s = FriendListFragment.this.adapter.resolveIndex(pos - FriendListFragment.this.list.getHeaderViewsCount());
            int uid = ((UserProfile) ((Section) FriendListFragment.this.sections.get(s[FriendListFragment.SORT_HINTS])).list.get(s[FriendListFragment.SORT_FIRST])).uid;
            if (FriendListFragment.this.multiSelect) {
                if (FriendListFragment.this.selected.contains(Integer.valueOf(uid))) {
                    FriendListFragment.this.selected.remove(Integer.valueOf(uid));
                } else {
                    FriendListFragment.this.selected.add(Integer.valueOf(uid));
                }
                if (FriendListFragment.this.selListener != null) {
                    FriendListFragment.this.selListener.onItemSelected(null);
                }
                FriendListFragment.this.adapter.notifyDataSetChanged();
                return;
            }
            if (FriendListFragment.this.selListener != null) {
                Iterator it = FriendListFragment.this.sections.iterator();
                while (it.hasNext()) {
                    Iterator it2 = ((Section) it.next()).list.iterator();
                    while (it2.hasNext()) {
                        UserProfile p = (UserProfile) it2.next();
                        if (p.uid == uid) {
                            FriendListFragment.this.selListener.onItemSelected(p);
                            return;
                        }
                    }
                }
            }
            Bundle args = new Bundle();
            args.putInt("id", uid);
            Navigate.to("ProfileFragment", args, FriendListFragment.this.getActivity());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendListFragment.2 */
    class C06812 implements OnClickListener {
        C06812() {
        }

        public void onClick(View v) {
            Navigate.to("SuggestionsFriendsFragment", new Bundle(), FriendListFragment.this.getActivity());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendListFragment.4 */
    class C06844 implements Runnable {
        private final /* synthetic */ ArrayList val$users;

        /* renamed from: com.vkontakte.android.fragments.FriendListFragment.4.1 */
        class C06821 implements Comparator<UserProfile> {
            C06821() {
            }

            public int compare(UserProfile lhs, UserProfile rhs) {
                return lhs.university.compareToIgnoreCase(rhs.university);
            }
        }

        /* renamed from: com.vkontakte.android.fragments.FriendListFragment.4.2 */
        class C06832 implements Runnable {
            C06832() {
            }

            public void run() {
                FriendListFragment.this.creatingSections = false;
                if (FriendListFragment.this.list != null) {
                    FriendListFragment.this.list.setDraggingEnabled(true);
                }
                if (FriendListFragment.this.runAfterInit != null) {
                    FriendListFragment.this.runAfterInit.run();
                    FriendListFragment.this.runAfterInit = null;
                }
            }
        }

        C06844(ArrayList arrayList) {
            this.val$users = arrayList;
        }

        public void run() {
            Throwable x;
            boolean firstName = true;
            try {
                FriendListFragment.this.sectionsLock.acquire();
            } catch (Exception e) {
            }
            FriendListFragment.this.creatingSections = true;
            Log.m525d("vk", new StringBuilder(String.valueOf(Thread.currentThread().getName())).append(" Create sections thread start, users size=").append(this.val$users.size()).toString());
            if (FriendListFragment.this.sortPref != FriendListFragment.SORT_FIRST) {
                firstName = false;
            }
            Section section = null;
            ArrayList<Section> _sections = new ArrayList();
            if (FriendListFragment.this.showHints) {
                section = new Section(null);
                section.title = VKApplication.context.getResources().getString(C0436R.string.important_friends);
                section.list = new ArrayList();
                section.list.addAll(this.val$users.subList(FriendListFragment.SORT_HINTS, Math.min(this.val$users.size(), 5)));
                section.shortTitle = "\u2605";
                _sections.add(section);
            }
            Iterator it = this.val$users.iterator();
            while (it.hasNext()) {
                UserProfile p = (UserProfile) it.next();
                if (firstName) {
                    p.university = p.firstName + " " + p.lastName;
                } else {
                    p.university = p.lastName + " " + p.firstName;
                }
            }
            Collections.sort(this.val$users, new C06821());
            it = this.val$users.iterator();
            while (it.hasNext()) {
                ((UserProfile) it.next()).university = null;
            }
            char fc = '\u0000';
            int offset = FriendListFragment.SORT_HINTS;
            Iterator it2 = this.val$users.iterator();
            while (it2.hasNext()) {
                String str;
                UserProfile user = (UserProfile) it2.next();
                if (firstName) {
                    try {
                        str = user.firstName;
                    } catch (Exception e2) {
                        x = e2;
                        Log.m532w("vk", x);
                    }
                } else {
                    str = user.lastName;
                }
                if (str.charAt(FriendListFragment.SORT_HINTS) != fc) {
                    int size;
                    fc = (firstName ? user.firstName : user.lastName).charAt(FriendListFragment.SORT_HINTS);
                    if (section != null) {
                        size = section.list.size() + FriendListFragment.SORT_FIRST;
                    } else {
                        size = FriendListFragment.SORT_HINTS;
                    }
                    offset += size;
                    Section curSection = new Section(null);
                    try {
                        curSection.title = new StringBuilder(String.valueOf(fc)).toString().toUpperCase();
                        curSection.list = new ArrayList();
                        curSection.startPos = offset;
                        _sections.add(curSection);
                        section = curSection;
                    } catch (Exception e3) {
                        x = e3;
                        section = curSection;
                        Log.m532w("vk", x);
                    }
                }
                section.list.add(user);
            }
            FriendListFragment.this.sections.clear();
            FriendListFragment.this.sections.addAll(_sections);
            FriendListFragment.this.updateList();
            Log.m525d("vk", new StringBuilder(String.valueOf(Thread.currentThread().getName())).append(" Create sections done, ").append(FriendListFragment.this.sections.size()).toString());
            FriendListFragment.this.sectionsLock.release();
            if (FriendListFragment.this.getActivity() != null) {
                FriendListFragment.this.getActivity().runOnUiThread(new C06832());
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendListFragment.5 */
    class C06855 implements Runnable {
        C06855() {
        }

        public void run() {
            try {
                FriendListFragment.this.sectionsLock.acquire();
            } catch (Exception e) {
            }
            Section s = new Section(null);
            s.list = new ArrayList();
            Iterator it = FriendListFragment.this.users.iterator();
            while (it.hasNext()) {
                UserProfile user = (UserProfile) it.next();
                if (user != null && user.online > 0) {
                    s.list.add(user);
                }
            }
            FriendListFragment.this.sections.clear();
            FriendListFragment.this.sections.add(s);
            FriendListFragment.this.sectionsLock.release();
            FriendListFragment.this.updateList();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendListFragment.7 */
    class C06887 implements Runnable {

        /* renamed from: com.vkontakte.android.fragments.FriendListFragment.7.1 */
        class C06871 implements Runnable {
            C06871() {
            }

            public void run() {
                if (FriendListFragment.this.imgLoader != null) {
                    FriendListFragment.this.imgLoader.updateImages();
                }
            }
        }

        C06887() {
        }

        public void run() {
            if (!FriendListFragment.this.showOnline) {
                Log.m528i("vk", "updateList: sections=" + FriendListFragment.this.sections.size());
            }
            FriendListFragment.this.adapter.notifyDataSetChanged();
            FriendListFragment.this.list.requestLayout();
            FriendListFragment.this.list.postDelayed(new C06871(), 100);
        }
    }

    public interface RefreshListener {
        void onRefresh();
    }

    private class Section {
        public ArrayList<UserProfile> list;
        public String shortTitle;
        public int startPos;
        public String title;

        private Section() {
            this.shortTitle = null;
        }

        public String toString() {
            return this.shortTitle == null ? this.title : this.shortTitle;
        }
    }

    public interface SelectionListener {
        void onItemSelected(UserProfile userProfile);
    }

    /* renamed from: com.vkontakte.android.fragments.FriendListFragment.3 */
    class C14673 implements Listener {
        C14673() {
        }

        public void onScrolledToLastItem() {
            FriendListFragment.this.loadMore();
        }

        public void onScrollStopped() {
        }

        public void onScrollStarted() {
            Activity act = FriendListFragment.this.getActivity();
            if (act.getCurrentFocus() != null) {
                ((InputMethodManager) act.getSystemService("input_method")).hideSoftInputFromWindow(act.getCurrentFocus().getWindowToken(), FriendListFragment.SORT_LAST);
                act.getCurrentFocus().clearFocus();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.FriendListFragment.6 */
    class C14686 implements Callback {

        /* renamed from: com.vkontakte.android.fragments.FriendListFragment.6.1 */
        class C06861 implements Runnable {
            C06861() {
            }

            public void run() {
                if (FriendListFragment.this.footerView != null) {
                    FriendListFragment.this.footerView.setVisible(FriendListFragment.this.moreAvailable);
                }
            }
        }

        C14686() {
        }

        public void success(ArrayList<UserProfile> results, int total) {
            try {
                boolean z;
                if (results.size() > 50) {
                    ((Section) FriendListFragment.this.sections.get(FriendListFragment.SORT_FIRST)).list.addAll(results.subList(FriendListFragment.SORT_HINTS, 50));
                    FriendListFragment.this.preloadedSearch.addAll(results.subList(50, results.size()));
                } else if (FriendListFragment.this.preloadOnReady) {
                    ((Section) FriendListFragment.this.sections.get(FriendListFragment.SORT_FIRST)).list.addAll(results);
                } else {
                    FriendListFragment.this.preloadedSearch.addAll(results);
                }
                FriendListFragment.this.updateList();
                FriendListFragment.this.currentSearchReq = null;
                if (FriendListFragment.this.preloadOnReady) {
                    FriendListFragment.this.preloadOnReady = false;
                    FriendListFragment.this.loadMore();
                }
                FriendListFragment friendListFragment = FriendListFragment.this;
                if (results.size() > 0) {
                    z = true;
                } else {
                    z = false;
                }
                friendListFragment.moreAvailable = z;
                if (FriendListFragment.this.getActivity() != null) {
                    FriendListFragment.this.getActivity().runOnUiThread(new C06861());
                }
            } catch (Exception e) {
            }
        }

        public void fail(int ecode, String emsg) {
            FriendListFragment.this.currentSearchReq = null;
        }
    }

    private class FriendsAdapter extends MultiSectionAdapter implements SectionIndexer {
        private FriendsAdapter() {
        }

        public View getView(int section, int item, View convertView) {
            View view = convertView;
            if (view == null) {
                view = View.inflate(FriendListFragment.this.getActivity(), FriendListFragment.this.multiSelect ? C0436R.layout.friend_list_item_multiselect : C0436R.layout.friend_list_item, null);
            }
            UserProfile user = (UserProfile) ((Section) FriendListFragment.this.sections.get(section)).list.get(item);
            ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(user.fullName);
            view.findViewById(C0436R.id.flist_item_online).setVisibility(user.online > 0 ? FriendListFragment.SORT_HINTS : 8);
            ((ImageView) view.findViewById(C0436R.id.flist_item_online)).setImageResource(user.online == FriendListFragment.SORT_FIRST ? C0436R.drawable.ic_online : C0436R.drawable.ic_online_mobile);
            if (FriendListFragment.this.imgLoader.isAlreadyLoaded(user.photo)) {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageBitmap(FriendListFragment.this.imgLoader.get(user.photo));
            } else {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(C0436R.drawable.user_placeholder);
            }
            if (FriendListFragment.this.multiSelect) {
                ((ImageView) view.findViewById(C0436R.id.flist_item_check)).setImageResource(FriendListFragment.this.selected.contains(Integer.valueOf(user.uid)) ? C0436R.drawable.checkbox_on : C0436R.drawable.checkbox_off);
            }
            return view;
        }

        public String getSectionTitle(int section) {
            return ((Section) FriendListFragment.this.sections.get(section)).title;
        }

        public int getSectionCount() {
            return FriendListFragment.this.sections.size();
        }

        public int getItemCount(int section) {
            return ((Section) FriendListFragment.this.sections.get(section)).list.size();
        }

        public long getItemId(int section, int item) {
            return 0;
        }

        public boolean isSectionHeaderVisible(int section) {
            return ((Section) FriendListFragment.this.sections.get(section)).title != null && ((Section) FriendListFragment.this.sections.get(section)).list.size() > 0;
        }

        public int getPositionForSection(int section) {
            if (section >= FriendListFragment.this.sections.size()) {
                return FriendListFragment.SORT_HINTS;
            }
            return ((Section) FriendListFragment.this.sections.get(section)).startPos;
        }

        public Object[] getSections() {
            return FriendListFragment.this.sections.toArray(new Section[FriendListFragment.SORT_HINTS]);
        }
    }

    private class FriendsPhotosAdapter extends MultiSectionImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.FriendListFragment.FriendsPhotosAdapter.1 */
        class C06891 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C06891(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.flist_item_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private FriendsPhotosAdapter() {
        }

        public int getSectionCount() {
            return FriendListFragment.this.sections.size();
        }

        public int getItemCount(int section) {
            return ((Section) FriendListFragment.this.sections.get(section)).list.size();
        }

        public boolean isSectionHeaderVisible(int section) {
            return ((Section) FriendListFragment.this.sections.get(section)).title != null && ((Section) FriendListFragment.this.sections.get(section)).list.size() > 0;
        }

        public int getImageCountForItem(int section, int item) {
            return FriendListFragment.SORT_FIRST;
        }

        public String getImageURL(int section, int item, int image) {
            try {
                return ((UserProfile) ((Section) FriendListFragment.this.sections.get(section)).list.get(item)).photo;
            } catch (Exception e) {
                return null;
            }
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += FriendListFragment.this.list.getHeaderViewsCount();
            if (item >= FriendListFragment.this.list.getFirstVisiblePosition() && item <= FriendListFragment.this.list.getLastVisiblePosition()) {
                FriendListFragment.this.getActivity().runOnUiThread(new C06891(FriendListFragment.this.list.getChildAt(item - FriendListFragment.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    public FriendListFragment() {
        this.sections = new ArrayList();
        this.users = new ArrayList();
        this.showOnline = false;
        this.showHints = true;
        this.indexer = new SearchIndexer();
        this.searchQuery = null;
        this.preloadedSearch = new ArrayList();
        this.preloadOnReady = false;
        this.moreAvailable = true;
        this.useSections = true;
        this.multiSelect = false;
        this.selected = new ArrayList();
        this.runAfterInit = null;
        this.sectionsLock = new Semaphore(SORT_FIRST, true);
        this.creatingSections = false;
        String pref = PreferenceManager.getDefaultSharedPreferences(VKApplication.context).getString("friendsOrderNew", "firstname");
        if ("hints".equals(pref)) {
            this.sortPref = SORT_HINTS;
        } else if ("firstname".equals(pref)) {
            this.sortPref = SORT_FIRST;
        } else if ("lastname".equals(pref)) {
            this.sortPref = SORT_LAST;
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        Log.m528i("vk", "ON ATTACH " + this.showOnline + " " + act);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.contentView = new FrameLayout(getActivity());
        this.footerView = new LoadMoreFooterView(getActivity());
        this.contentView.setBackgroundColor(-1);
        this.list = new PinnedHeaderListView(getActivity());
        this.list.addFooterView(this.footerView, null, false);
        this.footerView.setVisible(false);
        beforeSetListAdapter(this.list);
        PinnedHeaderListView pinnedHeaderListView = this.list;
        ListAdapter friendsAdapter = new FriendsAdapter();
        this.adapter = friendsAdapter;
        pinnedHeaderListView.setAdapter(friendsAdapter);
        this.list.setSelector(C0436R.drawable.highlight);
        this.list.setPadding(getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding), SORT_HINTS, getResources().getDimensionPixelOffset(C0436R.dimen.list_side_padding), SORT_HINTS);
        this.list.setScrollBarStyle(33554432);
        this.list.setDivider(new ColorDrawable(-1710619));
        this.list.setDividerHeight(Global.scale(1.0f));
        this.list.setHeaderDividersEnabled(false);
        this.list.setOnRefreshListener(this);
        this.contentView.addView(this.list);
        this.list.setOnItemClickListener(new C06801());
        this.emptyView = EmptyView.create(getActivity());
        this.emptyView.setButtonText((int) C0436R.string.find_friends);
        EmptyView emptyView = this.emptyView;
        int i = this.showOnline ? C0436R.string.no_online_friends : this.isMe ? C0436R.string.no_friends_me : C0436R.string.no_friends;
        emptyView.setText(i);
        this.emptyView.setButtonVisible(this.isMe);
        this.emptyView.setOnBtnClickListener(new C06812());
        this.contentView.addView(this.emptyView);
        this.list.setEmptyView(this.emptyView);
        PinnedHeaderListView pinnedHeaderListView2 = this.list;
        boolean z = VERSION.SDK_INT >= 14 && this.useSections && this.sortPref != 0 && !this.showOnline && (getArguments() == null || !getArguments().getBoolean("relation"));
        pinnedHeaderListView2.setFastScrollEnabled(z);
        if (this.creatingSections) {
            this.list.setDraggingEnabled(false);
        }
        this.imgLoader = new ListImageLoaderWrapper(new FriendsPhotosAdapter(), this.list, new C14673());
        return this.contentView;
    }

    public void onDestroyView() {
        this.list = null;
        this.contentView = null;
        this.adapter = null;
        if (this.imgLoader != null) {
            this.imgLoader.deactivate();
        }
        this.imgLoader = null;
        this.footerView = null;
        this.emptyView = null;
        super.onDestroyView();
    }

    public void setMultiSelection() {
        this.multiSelect = true;
    }

    public void onRefresh() {
        if (this.refreshListener != null) {
            this.refreshListener.onRefresh();
        } else {
            Friends.reload(true);
        }
    }

    public ArrayList<UserProfile> getSelectedUsers() {
        ArrayList<UserProfile> result = new ArrayList();
        Iterator it = this.selected.iterator();
        while (it.hasNext()) {
            int uid = ((Integer) it.next()).intValue();
            Iterator it2 = this.users.iterator();
            while (it2.hasNext()) {
                UserProfile p = (UserProfile) it2.next();
                if (p.uid == uid) {
                    result.add(p);
                    break;
                }
            }
        }
        return result;
    }

    public void setSelectedUsers(ArrayList<UserProfile> users) {
        this.selected.clear();
        Iterator it = users.iterator();
        while (it.hasNext()) {
            this.selected.add(Integer.valueOf(((UserProfile) it.next()).uid));
        }
        updateList();
        if (this.selListener != null) {
            this.selListener.onItemSelected(null);
        }
    }

    public void setData(ArrayList<UserProfile> l, boolean showHints, boolean showFind) {
        Log.m528i("vk", "Set data from thread " + Thread.currentThread().getName() + ", count=" + l.size());
        this.isMe = showFind;
        try {
            this.sectionsLock.acquire();
        } catch (Exception e) {
        }
        this.users.clear();
        this.users.addAll(l);
        if (this.list != null && this.list.isRefreshing()) {
            this.list.refreshDone();
        }
        this.sectionsLock.release();
        this.indexer.bind(this.users);
        this.indexer.build();
        this.showHints = showHints;
        if (this.showOnline) {
            updateOnline();
        } else {
            createSections();
        }
        updateList();
        if (this.emptyView != null) {
            EmptyView emptyView = this.emptyView;
            int i = this.showOnline ? C0436R.string.no_online_friends : showFind ? C0436R.string.no_friends_me : C0436R.string.no_friends;
            emptyView.setText(i);
            this.emptyView.setButtonVisible(showFind);
        }
    }

    public void updateFilter(String filter) {
        if (this.currentSearchReq != null) {
            this.currentSearchReq.cancel();
            this.currentSearchReq = null;
        }
        this.preloadedSearch.clear();
        this.moreAvailable = true;
        this.preloadOnReady = false;
        if (filter == null || filter.length() == 0) {
            this.searchQuery = null;
            this.footerView.setVisible(false);
            createSections();
            updateList();
            this.list.setSelection(SORT_HINTS);
            this.emptyView.setText(this.isMe ? C0436R.string.no_friends_me : C0436R.string.no_friends);
            this.emptyView.setButtonVisible(this.isMe);
            this.list.setDraggingEnabled(true);
            return;
        }
        this.list.setDraggingEnabled(false);
        this.emptyView.setText((int) C0436R.string.nothing_found);
        this.emptyView.setButtonVisible(false);
        if (!this.multiSelect) {
            this.footerView.setVisible(true);
        }
        this.searchQuery = filter;
        this.list.setFastScrollEnabled(false);
        this.sections.clear();
        Section search = new Section();
        this.sections.add(search);
        search.title = getResources().getString(C0436R.string.search_results);
        List<UserProfile> results = this.indexer.search(filter);
        search.list = new ArrayList();
        search.list.addAll(results);
        Section gsearch = new Section();
        gsearch.title = getResources().getString(C0436R.string.search_global);
        gsearch.list = new ArrayList();
        this.sections.add(gsearch);
        updateList();
        this.list.setSelection(SORT_HINTS);
    }

    public void setShowOnline(boolean s) {
        this.showOnline = s;
        if (s) {
            updateOnline();
            if (this.list != null) {
                this.list.setFastScrollEnabled(false);
                return;
            }
            return;
        }
        createSections();
    }

    public void setUserSections(boolean use) {
        this.useSections = use;
        createSections();
    }

    public void setUserOnline(int uid, int online) {
        Iterator it = this.users.iterator();
        while (it.hasNext()) {
            UserProfile u = (UserProfile) it.next();
            if (u.uid == uid) {
                u.online = online;
                if (this.showOnline) {
                    updateOnline();
                    return;
                } else {
                    updateList();
                    return;
                }
            }
        }
    }

    public void createSections() {
        boolean relation;
        if (getArguments() == null || !getArguments().getBoolean("relation")) {
            relation = false;
        } else {
            relation = true;
        }
        if (this.useSections && this.sortPref != 0 && !relation) {
            if (VERSION.SDK_INT >= 11 && this.list != null) {
                this.list.setFastScrollEnabled(true);
            }
            ArrayList<UserProfile> users = new ArrayList();
            users.addAll(this.users);
            if (this.list != null) {
                this.list.setDraggingEnabled(false);
            }
            Runnable r = new C06844(users);
            Log.m528i("vk", "Sections size=" + this.sections.size());
            if (this.sections.size() == 0) {
                new Thread(r).start();
            } else {
                r.run();
            }
        } else if (relation) {
            boolean myGender;
            UserProfile p;
            if (this.list != null) {
                this.list.setFastScrollEnabled(false);
            }
            boolean allowSame = getArguments().getBoolean("show_same_gender");
            if (getArguments().getInt("my_gender") == SORT_FIRST) {
                myGender = true;
            } else {
                myGender = false;
            }
            Log.m525d("vk", "same=" + allowSame + ", my=" + myGender);
            s = new Section();
            s.list = new ArrayList();
            Iterator it = this.users.iterator();
            while (it.hasNext()) {
                p = (UserProfile) it.next();
                if (p.f151f != myGender) {
                    s.list.add(p);
                }
            }
            if (allowSame) {
                it = this.users.iterator();
                while (it.hasNext()) {
                    p = (UserProfile) it.next();
                    if (p.f151f == myGender) {
                        s.list.add(p);
                    }
                }
            }
            this.sections.clear();
            this.sections.add(s);
            updateList();
            if (this.runAfterInit != null) {
                this.runAfterInit.run();
                this.runAfterInit = null;
            }
        } else {
            if (this.list != null) {
                this.list.setFastScrollEnabled(false);
            }
            s = new Section();
            s.list = this.users;
            this.sections.clear();
            this.sections.add(s);
            updateList();
            if (this.runAfterInit != null) {
                this.runAfterInit.run();
                this.runAfterInit = null;
            }
        }
    }

    public void updateOnline() {
        if (this.showOnline) {
            new Thread(new C06855()).start();
        }
    }

    protected void beforeSetListAdapter(RefreshableListView lv) {
    }

    private void loadMore() {
        if (!this.multiSelect && this.searchQuery != null && this.currentSearchReq == null) {
            if (this.preloadedSearch.size() > 0) {
                ((Section) this.sections.get(SORT_FIRST)).list.addAll(this.preloadedSearch);
                this.preloadedSearch.clear();
                updateList();
            } else if (((Section) this.sections.get(SORT_FIRST)).list.size() > 0) {
                this.preloadOnReady = true;
            }
            if (this.moreAvailable && !this.multiSelect) {
                this.currentSearchReq = new UsersSearch(this.searchQuery, ((Section) this.sections.get(SORT_FIRST)).list.size(), ((Section) this.sections.get(SORT_FIRST)).list.size() == 0 ? 100 : 50).setCallback(new C14686()).exec();
            }
        }
    }

    public void updateList() {
        if (this.list != null && this.adapter != null) {
            getActivity().runOnUiThread(new C06887());
        }
    }

    public void setSelectionListener(SelectionListener l) {
        this.selListener = l;
    }

    public void setRefreshListener(RefreshListener l) {
        this.refreshListener = l;
    }

    public void runAfterInit(Runnable r) {
        this.runAfterInit = r;
        if (this.sections.size() > 0 && ((Section) this.sections.get(SORT_HINTS)).list.size() > 0) {
            r.run();
        }
    }

    public String getLastUpdatedTime() {
        return null;
    }

    public void onScrolled(float offset) {
    }
}
