package com.vkontakte.android.fragments;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.widget.ExploreByTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.Window.Callback;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.AudioFile;
import com.vkontakte.android.AudioPlayerService;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.cache.AudioCache;
import com.vkontakte.android.ui.ReorderableListView;
import com.vkontakte.android.ui.ReorderableListView.Swappable;
import com.vkontakte.android.ui.SwipeDismissListViewTouchListener;
import com.vkontakte.android.ui.SwipeDismissListViewTouchListener.DismissCallbacks;
import com.vkontakte.android.ui.WindowCallbackDelegate;
import java.util.ArrayList;

public class AudioPlaylistFragment extends SherlockDialogFragment {
    private AudioListAdapter adapter;
    private boolean animatingTransitionOut;
    private ArrayList<AudioFile> files;
    private ListView list;
    private AudioFile nowPlaying;
    private BroadcastReceiver receiver;
    private SwipeDismissListViewTouchListener swipeDismiss;
    private boolean usePopUp;

    /* renamed from: com.vkontakte.android.fragments.AudioPlaylistFragment.1 */
    class C05881 extends BroadcastReceiver {
        C05881() {
        }

        public void onReceive(Context arg0, Intent intent) {
            if (AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS.equals(intent.getAction())) {
                if (AudioPlayerService.sharedInstance != null && AudioPlaylistFragment.this.list != null) {
                    AudioFile file = AudioPlayerService.sharedInstance.getCurrentFile();
                    if (file == null) {
                        return;
                    }
                    if (file.equals(AudioPlaylistFragment.this.nowPlaying)) {
                        AudioPlaylistFragment.this.adapter.notifyDataSetChanged();
                        return;
                    }
                    AudioFile f;
                    int animOut = -1;
                    int animIn = -1;
                    int i = 0;
                    int j = 0;
                    while (j < AudioPlaylistFragment.this.adapter.getCount()) {
                        f = (AudioFile) AudioPlaylistFragment.this.adapter.getItem(j);
                        if (f != null && f.equals(AudioPlaylistFragment.this.nowPlaying)) {
                            animOut = i;
                            break;
                        } else {
                            i++;
                            j++;
                        }
                    }
                    i = 0;
                    j = 0;
                    while (j < AudioPlaylistFragment.this.adapter.getCount()) {
                        f = (AudioFile) AudioPlaylistFragment.this.adapter.getItem(j);
                        if (f != null && f.equals(file)) {
                            animIn = i;
                            break;
                        } else {
                            i++;
                            j++;
                        }
                    }
                    boolean z;
                    if (animIn == -1 || animOut == -1 || animOut < animIn) {
                        z = true;
                    } else {
                        z = false;
                    }
                    AudioPlaylistFragment.this.nowPlaying = file;
                    AudioPlaylistFragment.this.animateStateTransition(animIn, true);
                    AudioPlaylistFragment.this.animateStateTransition(animOut, false);
                }
            } else if (AudioCache.ACTION_FILE_ADDED.equals(intent.getAction()) || AudioCache.ACTION_FILE_DELETED.equals(intent.getAction())) {
                AudioPlaylistFragment.this.update();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioPlaylistFragment.3 */
    class C05893 extends ListView {
        C05893(Context $anonymous0) {
            super($anonymous0);
        }

        public void onMeasure(int wms, int hms) {
            if (AudioPlaylistFragment.this.getDialog() == null || AudioPlaylistFragment.this.getDialog().getWindow() == null) {
                setMeasuredDimension(20, 20);
                setAdapter(null);
                return;
            }
            Drawable wbg = AudioPlaylistFragment.this.getDialog().getWindow().getDecorView().getBackground();
            int pad = 0;
            if (wbg != null) {
                Rect r = new Rect();
                wbg.getPadding(r);
                pad = r.top + r.bottom;
            }
            if (!AudioPlaylistFragment.this.usePopUp) {
                r = new Rect();
                AudioPlaylistFragment.this.getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
                pad += r.top;
            }
            int h = (MeasureSpec.getSize(hms) - AudioPlaylistFragment.this.getSherlockActivity().getSupportActionBar().getHeight()) - pad;
            if (AudioPlaylistFragment.this.usePopUp) {
                super.onMeasure(wms, MeasureSpec.makeMeasureSpec(h, ExploreByTouchHelper.INVALID_ID));
                if (getMeasuredHeight() < Global.scale(BitmapDescriptorFactory.HUE_MAGENTA)) {
                    setMeasuredDimension(getMeasuredWidth(), Global.scale(BitmapDescriptorFactory.HUE_MAGENTA));
                    return;
                }
                return;
            }
            super.onMeasure(wms, MeasureSpec.makeMeasureSpec(h, 1073741824));
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioPlaylistFragment.4 */
    class C05904 implements OnItemClickListener {
        C05904() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            if (AudioPlayerService.sharedInstance == null) {
                return;
            }
            if (AudioPlayerService.sharedInstance.getPlaylistPosition() == pos) {
                AudioPlayerService.sharedInstance.togglePlayPause();
            } else {
                AudioPlayerService.sharedInstance.jumpToTrack(pos);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioPlaylistFragment.7 */
    class C05917 implements AnimatorListener {
        private final /* synthetic */ View val$v;

        C05917(View view) {
            this.val$v = view;
        }

        public void onAnimationStart(Animator animation) {
        }

        public void onAnimationRepeat(Animator animation) {
        }

        public void onAnimationEnd(Animator animation) {
            this.val$v.findViewById(C0436R.id.audio_play_icon).setTranslationX(0.0f);
            this.val$v.findViewById(C0436R.id.audio_artist).setTranslationX(0.0f);
            this.val$v.findViewById(C0436R.id.audio_title).setTranslationX(0.0f);
            this.val$v.findViewById(C0436R.id.audio_play_icon).setVisibility(8);
            AudioPlaylistFragment.this.animatingTransitionOut = false;
        }

        public void onAnimationCancel(Animator animation) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioPlaylistFragment.2 */
    class C14322 extends ReorderableListView {
        C14322(Context $anonymous0) {
            super($anonymous0);
        }

        public void onMeasure(int wms, int hms) {
            if (AudioPlaylistFragment.this.getDialog() == null || AudioPlaylistFragment.this.getDialog().getWindow() == null) {
                setMeasuredDimension(20, 20);
                setAdapter(null);
                return;
            }
            Drawable wbg = AudioPlaylistFragment.this.getDialog().getWindow().getDecorView().getBackground();
            int pad = 0;
            if (wbg != null) {
                Rect r = new Rect();
                wbg.getPadding(r);
                pad = r.top + r.bottom;
            }
            if (!AudioPlaylistFragment.this.usePopUp) {
                r = new Rect();
                AudioPlaylistFragment.this.getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
                pad += r.top;
            }
            int h = (MeasureSpec.getSize(hms) - AudioPlaylistFragment.this.getSherlockActivity().getSupportActionBar().getHeight()) - pad;
            if (AudioPlaylistFragment.this.usePopUp) {
                super.onMeasure(wms, MeasureSpec.makeMeasureSpec(h, ExploreByTouchHelper.INVALID_ID));
                if (getMeasuredHeight() < Global.scale(BitmapDescriptorFactory.HUE_MAGENTA)) {
                    setMeasuredDimension(getMeasuredWidth(), Global.scale(BitmapDescriptorFactory.HUE_MAGENTA));
                    return;
                }
                return;
            }
            super.onMeasure(wms, MeasureSpec.makeMeasureSpec(h, 1073741824));
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioPlaylistFragment.5 */
    class C14335 implements DismissCallbacks {
        C14335() {
        }

        public void onDismiss(ListView listView, int[] reverseSortedPositions) {
            if (AudioPlayerService.sharedInstance != null) {
                for (int pos : reverseSortedPositions) {
                    AudioPlayerService.sharedInstance.removePlaylistItem(pos);
                    AudioPlaylistFragment.this.files.remove(pos);
                    AudioPlaylistFragment.this.adapter.notifyDataSetChanged();
                }
            }
        }

        public boolean canDismiss(int position) {
            return (AudioPlayerService.sharedInstance == null || position == AudioPlayerService.sharedInstance.getPlaylistPosition()) ? false : true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.AudioPlaylistFragment.6 */
    class C14346 extends WindowCallbackDelegate {
        C14346(Callback $anonymous0) {
            super($anonymous0);
        }

        public boolean dispatchTouchEvent(MotionEvent ev) {
            if (ev.getY() >= 0.0f) {
                return super.dispatchTouchEvent(ev);
            }
            if (AudioPlaylistFragment.this.getDialog() != null) {
                AudioPlaylistFragment.this.getDialog().dismiss();
            }
            return true;
        }
    }

    private class AudioListAdapter extends BaseAdapter implements Swappable {
        private AudioListAdapter() {
        }

        public int getCount() {
            return AudioPlaylistFragment.this.files.size();
        }

        public Object getItem(int pos) {
            return AudioPlaylistFragment.this.files.get(pos);
        }

        public long getItemId(int position) {
            if (position < 0 || position >= AudioPlaylistFragment.this.files.size()) {
                return -1;
            }
            AudioFile file = (AudioFile) AudioPlaylistFragment.this.files.get(position);
            return (((long) file.oid) << 32) | ((long) file.aid);
        }

        public boolean hasStableIds() {
            return true;
        }

        public View getView(int position, View convertView, ViewGroup q) {
            View v = null;
            int i = 8;
            if (AudioPlaylistFragment.this.getActivity() != null) {
                boolean isCurrent;
                boolean pt;
                if (convertView == null) {
                    v = View.inflate(AudioPlaylistFragment.this.getActivity(), C0436R.layout.audio_list_item, null);
                } else {
                    v = convertView;
                }
                AudioFile file = (AudioFile) AudioPlaylistFragment.this.files.get(position);
                ((TextView) v.findViewById(C0436R.id.audio_artist)).setText(file.artist);
                ((TextView) v.findViewById(C0436R.id.audio_title)).setText(file.title);
                if (AudioPlayerService.sharedInstance == null || !(file.equals(AudioPlayerService.sharedInstance.getCurrentFile()) || file.equalsAdded(AudioPlayerService.sharedInstance.getCurrentFile()))) {
                    isCurrent = false;
                } else {
                    isCurrent = true;
                }
                boolean isCached = AudioCache.cachedIDs.contains(file.oid + "_" + file.aid);
                if (isCurrent && AudioPlayerService.sharedInstance.isPlaying()) {
                    pt = true;
                } else {
                    pt = false;
                }
                if (!AudioPlaylistFragment.this.animatingTransitionOut) {
                    int i2;
                    View findViewById = v.findViewById(C0436R.id.audio_play_icon);
                    if (isCurrent) {
                        i2 = 0;
                    } else {
                        i2 = 8;
                    }
                    findViewById.setVisibility(i2);
                }
                ((ImageView) v.findViewById(C0436R.id.audio_play_icon)).setImageResource(pt ? C0436R.drawable.ic_audio_play : C0436R.drawable.ic_audio_pause);
                View findViewById2 = v.findViewById(C0436R.id.audio_saved_icon);
                if (isCached) {
                    i = 0;
                }
                findViewById2.setVisibility(i);
                ((TextView) v.findViewById(C0436R.id.audio_duration)).setText(String.format("%d:%02d", new Object[]{Integer.valueOf(file.duration / 60), Integer.valueOf(file.duration % 60)}));
            }
            return v;
        }

        public void swapItems(int first, int second) {
            AudioFile tmp = (AudioFile) AudioPlaylistFragment.this.files.get(first);
            AudioPlaylistFragment.this.files.set(first, (AudioFile) AudioPlaylistFragment.this.files.get(second));
            AudioPlaylistFragment.this.files.set(second, tmp);
            if (AudioPlayerService.sharedInstance != null) {
                AudioPlayerService.sharedInstance.swapPlaylistItems(first, second);
            }
        }
    }

    public AudioPlaylistFragment() {
        this.files = new ArrayList();
        this.animatingTransitionOut = false;
        this.usePopUp = false;
        this.receiver = new C05881();
    }

    public void onCreate(Bundle b) {
        boolean z;
        super.onCreate(b);
        update();
        IntentFilter filter = new IntentFilter();
        filter.addAction(AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS);
        filter.addAction(AudioCache.ACTION_FILE_ADDED);
        filter.addAction(AudioCache.ACTION_FILE_DELETED);
        VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
        if (AudioPlayerService.sharedInstance != null) {
            this.nowPlaying = AudioPlayerService.sharedInstance.getCurrentFile();
        }
        int scrSize = VKApplication.context.getResources().getConfiguration().screenLayout & 15;
        if (VERSION.SDK_INT < 14 || !(scrSize == 3 || scrSize == 4)) {
            z = false;
        } else {
            z = true;
        }
        this.usePopUp = z;
        setStyle(1, 0);
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            getSherlockActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (VERSION.SDK_INT >= 14) {
            this.list = new C14322(getActivity());
        } else {
            this.list = new C05893(getActivity());
        }
        this.list.setSelector(C0436R.drawable.highlight);
        this.list.setDivider(new ColorDrawable(-2104603));
        this.list.setDividerHeight(1);
        ListView listView = this.list;
        ListAdapter audioListAdapter = new AudioListAdapter();
        this.adapter = audioListAdapter;
        listView.setAdapter(audioListAdapter);
        this.list.setOnItemClickListener(new C05904());
        if (AudioPlayerService.sharedInstance != null) {
            this.list.setSelectionFromTop(Math.max(0, AudioPlayerService.sharedInstance.getPlaylistPosition() - 1), 0);
        }
        if (VERSION.SDK_INT >= 14) {
            this.swipeDismiss = new SwipeDismissListViewTouchListener(this.list, new C14335());
            this.list.setOnTouchListener(this.swipeDismiss);
        } else {
            this.list.setOnItemLongClickListener(null);
        }
        getDialog().setCanceledOnTouchOutside(true);
        return this.list;
    }

    public void onResume() {
        super.onResume();
        Dialog dlg = getDialog();
        if (Global.isTablet) {
            dlg.getWindow().setFlags(0, 2);
        } else {
            dlg.getWindow().setFlags(2, 2);
            if (VERSION.SDK_INT >= 14) {
                dlg.getWindow().setDimAmount(0.7f);
            }
        }
        LayoutParams lp = dlg.getWindow().getAttributes();
        lp.horizontalMargin = 0.0f;
        Rect r = new Rect();
        if (!this.usePopUp) {
            getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(r);
        }
        lp.y = getSherlockActivity().getSupportActionBar().getHeight() + r.top;
        lp.gravity = 53;
        lp.width = -1;
        if (this.usePopUp) {
            lp.width = Global.scale(400.0f);
        } else {
            dlg.getWindow().setBackgroundDrawable(new ColorDrawable(-1));
        }
        dlg.getWindow().setAttributes(lp);
        dlg.getWindow().setCallback(new C14346(dlg.getWindow().getCallback()));
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.list = null;
        this.adapter = null;
    }

    private void update() {
        if (AudioPlayerService.sharedInstance != null) {
            this.files.clear();
            AudioPlayerService.sharedInstance.getPlaylist(this.files);
            if (this.adapter != null) {
                this.adapter.notifyDataSetChanged();
            }
        }
    }

    private void animateStateTransition(int item, boolean in) {
        if (VERSION.SDK_INT < 11) {
            this.adapter.notifyDataSetChanged();
        } else if (this.list.getHeaderViewsCount() + item >= this.list.getFirstVisiblePosition() && this.list.getHeaderViewsCount() + item <= this.list.getLastVisiblePosition()) {
            View v = this.list.getChildAt((this.list.getHeaderViewsCount() + item) - this.list.getFirstVisiblePosition());
            int iconSize = Global.scale(35.0f);
            ArrayList<Animator> anims;
            AnimatorSet set;
            if (in) {
                v.findViewById(C0436R.id.audio_play_icon).setVisibility(0);
                ((ImageView) v.findViewById(C0436R.id.audio_play_icon)).setImageResource(C0436R.drawable.ic_audio_play);
                if (VERSION.SDK_INT >= 11) {
                    anims = new ArrayList();
                    anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_play_icon), "translationX", new float[]{(float) (-iconSize), 0.0f}).setDuration(200));
                    anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_artist), "translationX", new float[]{(float) (-iconSize), 0.0f}).setDuration(200));
                    anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_title), "translationX", new float[]{(float) (-iconSize), 0.0f}).setDuration(200));
                    if (this.list.getPaddingLeft() > 0) {
                        anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_play_icon), "alpha", new float[]{0.0f, 1.0f}).setDuration(200));
                    }
                    set = new AnimatorSet();
                    set.playTogether(anims);
                    set.start();
                    return;
                }
                return;
            }
            this.animatingTransitionOut = true;
            if (VERSION.SDK_INT >= 11) {
                anims = new ArrayList();
                anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_play_icon), "translationX", new float[]{(float) (-iconSize)}).setDuration(200));
                anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_artist), "translationX", new float[]{(float) (-iconSize)}).setDuration(200));
                anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_title), "translationX", new float[]{(float) (-iconSize)}).setDuration(200));
                if (this.list.getPaddingLeft() > 0) {
                    anims.add(ObjectAnimator.ofFloat(v.findViewById(C0436R.id.audio_play_icon), "alpha", new float[]{0.0f}).setDuration(200));
                }
                set = new AnimatorSet();
                set.playTogether(anims);
                set.addListener(new C05917(v));
                set.start();
            }
        }
    }
}
