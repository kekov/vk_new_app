package com.vkontakte.android.fragments;

import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com.vkontakte.android.Global;

public abstract class DatabaseSearchFragment<T> extends SherlockDialogFragment {
    private ListAdapter adapter;
    private Callback<T> callback;

    /* renamed from: com.vkontakte.android.fragments.DatabaseSearchFragment.1 */
    class C06511 implements TextWatcher {
        C06511() {
        }

        public void afterTextChanged(Editable ed) {
            ((Filterable) DatabaseSearchFragment.this.adapter).getFilter().filter(ed);
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.DatabaseSearchFragment.2 */
    class C06522 implements OnItemClickListener {
        C06522() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            T item = DatabaseSearchFragment.this.adapter.getItem(pos);
            if (DatabaseSearchFragment.this.callback != null) {
                DatabaseSearchFragment.this.callback.onItemSelected(item);
            }
            DatabaseSearchFragment.this.dismiss();
        }
    }

    public interface Callback<T> {
        void onItemSelected(T t);
    }

    public abstract ListAdapter getAdapter();

    public void onCreate(Bundle b) {
        super.onCreate(b);
        setStyle(1, 0);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getDialog() != null) {
            getDialog().getWindow().setSoftInputMode(16);
            getDialog().getWindow().setGravity(49);
        }
        LinearLayout content = new LinearLayout(getActivity());
        content.setOrientation(1);
        EditText filter = new EditText(getActivity());
        filter.setInputType(524289);
        if (getArguments() != null && getArguments().containsKey("hint")) {
            filter.setHint(getArguments().getString("hint"));
        }
        LayoutParams flp = new LayoutParams(-1, -2);
        int scale = Global.scale(10.0f);
        flp.rightMargin = scale;
        flp.leftMargin = scale;
        flp.bottomMargin = scale;
        flp.topMargin = scale;
        content.addView(filter, flp);
        ListView list = new ListView(getActivity());
        if (VERSION.SDK_INT < 14) {
            list.setBackgroundColor(-1);
            list.setCacheColorHint(-1);
        }
        content.addView(list);
        this.adapter = getAdapter();
        list.setAdapter(this.adapter);
        filter.addTextChangedListener(new C06511());
        ((Filterable) this.adapter).getFilter().filter(null);
        list.setOnItemClickListener(new C06522());
        return content;
    }

    public void setCallback(Callback<T> c) {
        this.callback = c;
    }
}
