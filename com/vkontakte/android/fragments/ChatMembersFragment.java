package com.vkontakte.android.fragments;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.ChatUser;
import com.vkontakte.android.FragmentWrapperActivity;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImagePickerActivity;
import com.vkontakte.android.Log;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.MultiSectionImageLoaderAdapter;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.UploaderService;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKAlertDialog;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.GetUserOnlines;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.MessagesAddChatUser;
import com.vkontakte.android.api.MessagesCreateChat;
import com.vkontakte.android.api.MessagesDeleteChatPhoto;
import com.vkontakte.android.api.MessagesEditChat;
import com.vkontakte.android.api.MessagesRemoveChatUser;
import com.vkontakte.android.api.MessagesRemoveChatUser.Callback;
import com.vkontakte.android.cache.Cache;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Messages;
import com.vkontakte.android.data.Messages.GetChatUsersCallback;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.MultiSectionAdapter;
import com.vkontakte.android.ui.OverlayTextView;
import de.ankri.views.Switch;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.acra.ACRAConstants;

public class ChatMembersFragment extends SherlockFragment {
    private static final int INVITE_RESULT = 38920;
    private static final int PHOTO_RESULT = 38919;
    private ChatMembersAdapter adapter;
    private int admin;
    private String chatPhoto;
    private ArrayList<ChatUser> chatUsers;
    private boolean create;
    private TextView dndText;
    private FrameLayout dndView;
    private int id;
    private ListImageLoaderWrapper imgLoader;
    private OverlayTextView inviteBtn;
    private OverlayTextView leaveBtn;
    private ListView list;
    private Switch notifySwitch;
    private FrameLayout notifyView;
    private BroadcastReceiver receiver;
    private View sendBtn;
    private EditText titleEdit;
    private View titleView;
    private int uploadID;
    private ProgressDialog uploadProgress;

    /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.14 */
    class AnonymousClass14 implements OnClickListener {
        private final /* synthetic */ int val$idx;

        AnonymousClass14(int i) {
            this.val$idx = i;
        }

        public void onClick(DialogInterface dialog, int which) {
            ChatMembersFragment.this.doRemoveUser(this.val$idx);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.1 */
    class C06341 extends BroadcastReceiver {
        C06341() {
        }

        public void onReceive(Context context, Intent intent) {
            if (LongPollService.ACTION_CHAT_CHANGED.equals(intent.getAction())) {
                int cid = intent.getIntExtra("id", 0);
                Log.m528i("vk", "Received chat changed: " + cid);
                if (cid == ChatMembersFragment.this.id) {
                    ChatMembersFragment.this.updateChatUsers();
                }
            }
            if (UploaderService.ACTION_UPLOAD_PROGRESS.equals(intent.getAction()) && intent.getIntExtra("id", 0) == ChatMembersFragment.this.uploadID) {
                int done = intent.getIntExtra("done", 0);
                int total = intent.getIntExtra("total", 0);
                if (ChatMembersFragment.this.uploadProgress != null) {
                    ChatMembersFragment.this.uploadProgress.setIndeterminate(false);
                    ChatMembersFragment.this.uploadProgress.setMax(total);
                    ChatMembersFragment.this.uploadProgress.setProgress(done);
                }
            }
            if (UploaderService.ACTION_UPLOAD_DONE.equals(intent.getAction()) && intent.getIntExtra("id", 0) == ChatMembersFragment.this.uploadID && ChatMembersFragment.this.uploadProgress != null) {
                ChatMembersFragment.this.uploadProgress.dismiss();
                ChatMembersFragment.this.uploadProgress = null;
            }
            if (UploaderService.ACTION_UPLOAD_FAILED.equals(intent.getAction()) && intent.getIntExtra("id", 0) == ChatMembersFragment.this.uploadID) {
                if (ChatMembersFragment.this.uploadProgress != null) {
                    ChatMembersFragment.this.uploadProgress.dismiss();
                    ChatMembersFragment.this.uploadProgress = null;
                }
                new Builder(ChatMembersFragment.this.getActivity()).setTitle(C0436R.string.error).setMessage(C0436R.string.uploading_photo_err_notify).setPositiveButton(C0436R.string.ok, null).show();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.2 */
    class C06352 extends View {
        C06352(Context $anonymous0) {
            super($anonymous0);
        }

        public void onMeasure(int wms, int hms) {
            setMeasuredDimension(5, Global.scale(10.0f));
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.3 */
    class C06363 implements OnItemClickListener {
        C06363() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            boolean z = false;
            int[] p = ChatMembersFragment.this.adapter.resolveIndex(pos);
            int section = p[0];
            int item = p[1];
            if (section == 1) {
                if (item == 0) {
                    Switch access$18 = ChatMembersFragment.this.notifySwitch;
                    if (!ChatMembersFragment.this.notifySwitch.isChecked()) {
                        z = true;
                    }
                    access$18.setChecked(z);
                }
                if (item == 1) {
                    ChatMembersFragment.this.showDndDialog();
                }
            }
            if (section == 2) {
                if (item < ChatMembersFragment.this.chatUsers.size()) {
                    Bundle args = new Bundle();
                    args.putInt("id", ((ChatUser) ChatMembersFragment.this.chatUsers.get(item)).user.uid);
                    Navigate.to("ProfileFragment", args, ChatMembersFragment.this.getActivity());
                } else {
                    ChatMembersFragment.this.showFriendPicker();
                }
            }
            if (section == 3) {
                ChatMembersFragment.this.leaveChat();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.4 */
    class C06374 implements OnEditorActionListener {
        C06374() {
        }

        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (ChatMembersFragment.this.create) {
                ChatMembersFragment.this.createChat();
            } else {
                ChatMembersFragment.this.setChatTitle(ChatMembersFragment.this.titleEdit.getText().toString());
                ((InputMethodManager) ChatMembersFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(ChatMembersFragment.this.getActivity().getCurrentFocus().getWindowToken(), 0);
                ChatMembersFragment.this.getActivity().getCurrentFocus().clearFocus();
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.5 */
    class C06385 implements OnCheckedChangeListener {
        C06385() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            ChatMembersFragment.this.setMute(!isChecked);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.6 */
    class C06396 implements View.OnClickListener {
        C06396() {
        }

        public void onClick(View v) {
            ChatMembersFragment.this.createChat();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.7 */
    class C06407 implements View.OnClickListener {
        C06407() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(ChatMembersFragment.this.getActivity(), ImagePickerActivity.class);
            intent.putExtra("allow_album", false);
            intent.putExtra("limit", 1);
            if (!(ChatMembersFragment.this.chatPhoto == null || ChatMembersFragment.this.chatPhoto.length() <= 0 || ChatMembersFragment.this.chatPhoto.startsWith("M"))) {
                ArrayList<String> al = new ArrayList();
                al.add(ChatMembersFragment.this.getString(C0436R.string.delete));
                intent.putExtra("custom", al);
            }
            intent.putExtra("no_thumbs", true);
            ChatMembersFragment.this.startActivityForResult(intent, ChatMembersFragment.PHOTO_RESULT);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.15 */
    class AnonymousClass15 implements Callback {
        private final /* synthetic */ int val$idx;

        AnonymousClass15(int i) {
            this.val$idx = i;
        }

        public void success() {
            ChatMembersFragment.this.chatUsers.remove(this.val$idx);
            ChatMembersFragment.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(ChatMembersFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.17 */
    class AnonymousClass17 implements MessagesAddChatUser.Callback {
        private final /* synthetic */ UserProfile val$p;

        AnonymousClass17(UserProfile userProfile) {
            this.val$p = userProfile;
        }

        public void success() {
            ChatUser cu = new ChatUser();
            cu.inviter = new UserProfile();
            cu.inviter.uid = Global.uid;
            cu.inviter.fullName = ChatMembersFragment.this.getActivity().getSharedPreferences(null, 0).getString("username", ACRAConstants.DEFAULT_STRING_VALUE);
            cu.user = this.val$p;
            ChatMembersFragment.this.chatUsers.add(cu);
            ChatMembersFragment.this.updateList();
        }

        public void fail(int ecode, String emsg) {
            int i = C0436R.string.error;
            if (ecode == 15) {
                String err = null;
                if (emsg.contains("already left")) {
                    err = ChatMembersFragment.this.getString(C0436R.string.chat_invite_error_left);
                } else if (emsg.contains("already in")) {
                    err = ChatMembersFragment.this.getString(C0436R.string.chat_invite_error_already_in);
                }
                if (err != null) {
                    new Builder(ChatMembersFragment.this.getActivity()).setTitle(C0436R.string.error).setMessage(err).setPositiveButton(C0436R.string.ok, null).show();
                    return;
                }
            }
            Context activity = ChatMembersFragment.this.getActivity();
            if (ecode == -1) {
                i = C0436R.string.err_text;
            }
            Toast.makeText(activity, i, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.8 */
    class C14498 implements GetChatUsersCallback {

        /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.8.1 */
        class C06411 implements Runnable {
            private final /* synthetic */ ArrayList val$users;

            C06411(ArrayList arrayList) {
                this.val$users = arrayList;
            }

            public void run() {
                ChatMembersFragment.this.chatUsers.clear();
                ChatMembersFragment.this.chatUsers.addAll(this.val$users);
                ChatMembersFragment.this.updateList();
                ChatMembersFragment.this.loadOnlines();
            }
        }

        C14498() {
        }

        public void onUsersLoaded(ArrayList<ChatUser> users, String title, String photo) {
            ChatMembersFragment.this.chatPhoto = photo;
            if (ChatMembersFragment.this.chatPhoto != null && ChatMembersFragment.this.chatPhoto.startsWith("M")) {
                ChatMembersFragment.this.chatPhoto = ACRAConstants.DEFAULT_STRING_VALUE;
            }
            ChatMembersFragment.this.getActivity().runOnUiThread(new C06411(users));
        }
    }

    /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.9 */
    class C14509 implements GetUserOnlines.Callback {
        C14509() {
        }

        public void success(HashMap<Integer, Integer> onlines) {
            Iterator it = ChatMembersFragment.this.chatUsers.iterator();
            while (it.hasNext()) {
                ChatUser user = (ChatUser) it.next();
                if (onlines.containsKey(Integer.valueOf(user.user.uid))) {
                    user.user.online = ((Integer) onlines.get(Integer.valueOf(user.user.uid))).intValue();
                }
            }
            ChatMembersFragment.this.updateList();
        }

        public void fail(int ecode, String emsg) {
        }
    }

    private class ChatMembersAdapter extends MultiSectionAdapter {

        /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.ChatMembersAdapter.1 */
        class C06421 implements View.OnClickListener {
            C06421() {
            }

            public void onClick(View v) {
                ChatMembersFragment.this.removeUser(((Integer) v.getTag()).intValue());
            }
        }

        private ChatMembersAdapter() {
        }

        public View getView(int section, int item, View convertView) {
            int i = 0;
            if (section == 0) {
                return ChatMembersFragment.this.titleView;
            }
            if (section == 1) {
                if (item == 0) {
                    return ChatMembersFragment.this.notifyView;
                }
                if (item == 1) {
                    return ChatMembersFragment.this.dndView;
                }
            }
            if (section == 2) {
                if (item >= ChatMembersFragment.this.chatUsers.size()) {
                    return ChatMembersFragment.this.inviteBtn;
                }
                View v = convertView;
                if (v == null) {
                    v = View.inflate(ChatMembersFragment.this.getActivity(), C0436R.layout.chat_members_item, null);
                    v.findViewById(C0436R.id.chat_member_remove).setOnClickListener(new C06421());
                }
                ChatUser user = (ChatUser) ChatMembersFragment.this.chatUsers.get(item);
                ((TextView) v.findViewById(C0436R.id.chat_member_name)).setText(user.user.fullName);
                if (user.inviter.uid == user.user.uid || ChatMembersFragment.this.admin == user.user.uid) {
                    ((TextView) v.findViewById(C0436R.id.chat_member_inv)).setText(C0436R.string.chat_admin);
                } else {
                    ((TextView) v.findViewById(C0436R.id.chat_member_inv)).setText(ChatMembersFragment.this.getString(user.inviter.f151f ? C0436R.string.invited_by_f : C0436R.string.invited_by_m, user.inviter.fullName));
                }
                if (ChatMembersFragment.this.imgLoader.isAlreadyLoaded(user.user.photo)) {
                    ((ImageView) v.findViewById(C0436R.id.chat_member_photo)).setImageBitmap(ChatMembersFragment.this.imgLoader.get(user.user.photo));
                } else {
                    ((ImageView) v.findViewById(C0436R.id.chat_member_photo)).setImageResource(C0436R.drawable.user_placeholder);
                }
                View findViewById = v.findViewById(C0436R.id.chat_member_remove);
                int i2 = (user.user.uid == Global.uid || (!(user.inviter.uid == Global.uid || ChatMembersFragment.this.admin == Global.uid) || ChatMembersFragment.this.create)) ? 4 : 0;
                findViewById.setVisibility(i2);
                v.findViewById(C0436R.id.chat_member_remove).setTag(Integer.valueOf(item));
                View findViewById2 = v.findViewById(C0436R.id.chat_member_online);
                if (user.user.online <= 0) {
                    i = 8;
                }
                findViewById2.setVisibility(i);
                ((ImageView) v.findViewById(C0436R.id.chat_member_online)).setImageResource(user.user.online == 1 ? C0436R.drawable.ic_online : C0436R.drawable.ic_online_mobile);
                i2 = item == 0 ? C0436R.drawable.bg_post_comments_top : (ChatMembersFragment.this.create && item == ChatMembersFragment.this.chatUsers.size() - 1) ? C0436R.drawable.bg_post_comments_btm : C0436R.drawable.bg_post_comments_mid;
                v.setBackgroundResource(i2);
                return v;
            } else if (section == 3) {
                return ChatMembersFragment.this.leaveBtn;
            } else {
                return null;
            }
        }

        public String getSectionTitle(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return ChatMembersFragment.this.getString(C0436R.string.chat_title);
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return ChatMembersFragment.this.getString(C0436R.string.sett_notifications);
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return ChatMembersFragment.this.getString(C0436R.string.chat_members);
                default:
                    return ACRAConstants.DEFAULT_STRING_VALUE;
            }
        }

        public boolean isEnabled(int section, int item) {
            return true;
        }

        public int getSectionCount() {
            return 4;
        }

        public int getItemCount(int section) {
            int i = 0;
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return 1;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    if (ChatMembersFragment.this.create) {
                        return 0;
                    }
                    return 2;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    int size = ChatMembersFragment.this.chatUsers.size();
                    if (!ChatMembersFragment.this.create) {
                        i = 1;
                    }
                    return i + size;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return !ChatMembersFragment.this.create ? 1 : 0;
                default:
                    return 0;
            }
        }

        public long getItemId(int section, int item) {
            return 0;
        }

        public boolean isSectionHeaderVisible(int section) {
            if (ChatMembersFragment.this.create || section == 3 || section == 0) {
                return false;
            }
            return true;
        }

        public int getItemViewType(int section, int item) {
            if (section == 0 && item == 0) {
                return 2;
            }
            if (section != 1) {
                return section == 2 ? item == ChatMembersFragment.this.chatUsers.size() ? 5 : 0 : section == 3 ? 6 : 0;
            } else {
                if (item == 0) {
                    return 3;
                }
                return 4;
            }
        }

        public int getExtraViewTypeCount() {
            return 7;
        }

        public int getHeaderLayoutResource() {
            return C0436R.layout.list_cards_section_header;
        }
    }

    private class ChatMembersPhotosAdapter extends MultiSectionImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.ChatMembersPhotosAdapter.1 */
        class C06431 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;

            C06431(Bitmap bitmap) {
                this.val$bitmap = bitmap;
            }

            public void run() {
                ((ImageView) ChatMembersFragment.this.titleView.findViewById(C0436R.id.chat_edit_photo)).setImageBitmap(this.val$bitmap);
            }
        }

        /* renamed from: com.vkontakte.android.fragments.ChatMembersFragment.ChatMembersPhotosAdapter.2 */
        class C06442 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C06442(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.chat_member_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private ChatMembersPhotosAdapter() {
        }

        public int getSectionCount() {
            return ChatMembersFragment.this.adapter.getSectionCount();
        }

        public int getItemCount(int section) {
            return ChatMembersFragment.this.adapter.getItemCount(section);
        }

        public boolean isSectionHeaderVisible(int section) {
            return ChatMembersFragment.this.adapter.isSectionHeaderVisible(section);
        }

        public int getImageCountForItem(int section, int item) {
            if (section == 0) {
                return 1;
            }
            if (section != 2 || item >= ChatMembersFragment.this.chatUsers.size()) {
                return 0;
            }
            return 1;
        }

        public String getImageURL(int section, int item, int image) {
            if (section == 0) {
                return ChatMembersFragment.this.chatPhoto;
            }
            if (section != 2 || item >= ChatMembersFragment.this.chatUsers.size()) {
                return null;
            }
            return ((ChatUser) ChatMembersFragment.this.chatUsers.get(item)).user.photo;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            item += ChatMembersFragment.this.list.getHeaderViewsCount();
            if (item == 0) {
                ChatMembersFragment.this.getActivity().runOnUiThread(new C06431(bitmap));
            } else if (item >= ChatMembersFragment.this.list.getFirstVisiblePosition() && item <= ChatMembersFragment.this.list.getLastVisiblePosition()) {
                ChatMembersFragment.this.getActivity().runOnUiThread(new C06442(ChatMembersFragment.this.list.getChildAt(item - ChatMembersFragment.this.list.getFirstVisiblePosition()), bitmap));
            }
        }
    }

    public ChatMembersFragment() {
        this.chatUsers = new ArrayList();
        this.uploadID = -1;
        this.receiver = new C06341();
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        this.create = getArguments().getBoolean("create");
        this.list = new ListView(act);
        if (!this.create) {
            this.list.addFooterView(new C06352(act), null, false);
        }
        ListView listView = this.list;
        ListAdapter chatMembersAdapter = new ChatMembersAdapter(null);
        this.adapter = chatMembersAdapter;
        listView.setAdapter(chatMembersAdapter);
        this.list.setDividerHeight(0);
        this.list.setSelector(new ColorDrawable(0));
        this.list.setBackgroundColor(-2039584);
        this.list.setCacheColorHint(-2039584);
        this.id = getArguments().getInt("id");
        this.admin = getArguments().getInt("admin");
        getActivity().getWindow().setSoftInputMode(32);
        this.list.setOnItemClickListener(new C06363());
        this.imgLoader = new ListImageLoaderWrapper(new ChatMembersPhotosAdapter(null), this.list, null);
        this.titleView = View.inflate(getActivity(), C0436R.layout.chat_title_edit, null);
        this.titleEdit = (EditText) this.titleView.findViewById(C0436R.id.chat_edit_title);
        this.titleEdit.setText(getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
        this.titleEdit.setImeOptions(6);
        this.titleEdit.setOnEditorActionListener(new C06374());
        this.titleEdit.setPadding(this.titleEdit.getPaddingLeft() + Global.scale(16.0f), this.titleEdit.getPaddingTop() + Global.scale(8.0f), this.titleEdit.getPaddingRight() + Global.scale(16.0f), this.titleEdit.getPaddingBottom() + Global.scale(8.0f));
        this.inviteBtn = new OverlayTextView(act);
        this.inviteBtn.setText(C0436R.string.chat_add_member);
        this.inviteBtn.setTextSize(1, 18.0f);
        this.inviteBtn.setGravity(17);
        this.inviteBtn.setBackgroundResource(C0436R.drawable.bg_post_comments_btm);
        this.inviteBtn.setOverlayPadding(this.inviteBtn.getPaddingLeft(), this.inviteBtn.getPaddingTop(), this.inviteBtn.getPaddingRight(), this.inviteBtn.getPaddingBottom());
        this.inviteBtn.setPadding(this.inviteBtn.getPaddingLeft() + Global.scale(16.0f), this.inviteBtn.getPaddingTop() + Global.scale(10.0f), this.inviteBtn.getPaddingRight() + Global.scale(16.0f), this.inviteBtn.getPaddingBottom() + Global.scale(10.0f));
        this.inviteBtn.setTextColor(-13070905);
        this.inviteBtn.setOverlay((int) C0436R.drawable.highlight);
        this.leaveBtn = new OverlayTextView(act);
        this.leaveBtn.setText(C0436R.string.chat_leave);
        this.leaveBtn.setTextSize(1, 18.0f);
        this.leaveBtn.setGravity(17);
        this.leaveBtn.setBackgroundResource(C0436R.drawable.bg_post);
        this.leaveBtn.setOverlayPadding(this.leaveBtn.getPaddingLeft(), this.leaveBtn.getPaddingTop(), this.leaveBtn.getPaddingRight(), this.leaveBtn.getPaddingBottom());
        this.leaveBtn.setPadding(this.leaveBtn.getPaddingLeft() + Global.scale(16.0f), this.leaveBtn.getPaddingTop() + Global.scale(10.0f), this.leaveBtn.getPaddingRight() + Global.scale(16.0f), this.leaveBtn.getPaddingBottom() + Global.scale(10.0f));
        this.leaveBtn.setTextColor(Color.WINDOW_BACKGROUND_COLOR);
        this.leaveBtn.setOverlay((int) C0436R.drawable.highlight);
        LinearLayout nview = new LinearLayout(act);
        nview.setOrientation(0);
        TextView nv = new TextView(act);
        nv.setTextSize(1, 18.0f);
        nv.setTextColor(Color.WINDOW_BACKGROUND_COLOR);
        nv.setText(C0436R.string.chat_sound);
        nview.addView(nv, new LayoutParams(-1, Global.scale(28.0f), 1.0f));
        View view = new Switch(act);
        view.setTextOn(getString(C0436R.string.on).toUpperCase());
        view.setTextOff(getString(C0436R.string.off).toUpperCase());
        view.setTextColor(-1);
        view.setTrackDrawable(getResources().getDrawable(C0436R.drawable.switch_track));
        view.setThumbDrawable(getResources().getDrawable(C0436R.drawable.switch_thumb));
        view.setOnCheckedChangeListener(new C06385());
        int[][] iArr = new int[1][];
        iArr[0] = new int[0];
        view.setSwitchTextColor(new ColorStateList(iArr, new int[]{-1}));
        this.notifySwitch = view;
        nview.addView(view, new LayoutParams(-2, Global.scale(24.0f)));
        nview.setPadding(nview.getPaddingLeft() + Global.scale(16.0f), nview.getPaddingTop() + Global.scale(10.0f), nview.getPaddingRight() + Global.scale(16.0f), nview.getPaddingBottom() + Global.scale(10.0f));
        this.notifyView = new FrameLayout(act);
        this.notifyView.setBackgroundResource(C0436R.drawable.bg_post_comments_top);
        this.notifyView.addView(nview);
        View hl1 = new View(act);
        hl1.setBackgroundResource(C0436R.drawable.highlight);
        this.notifyView.addView(hl1);
        LinearLayout dview = new LinearLayout(act);
        dview.setOrientation(0);
        TextView dv = new TextView(act);
        dv.setTextSize(1, 18.0f);
        dv.setTextColor(Color.WINDOW_BACKGROUND_COLOR);
        dv.setText(C0436R.string.chat_dnd);
        this.dndText = new TextView(act);
        this.dndText.setTextSize(1, 18.0f);
        this.dndText.setTextColor(-13070905);
        this.dndText.setText("qwe");
        dview.addView(dv, new LayoutParams(-1, Global.scale(28.0f), 1.0f));
        dview.addView(this.dndText, new LayoutParams(-2, Global.scale(28.0f)));
        dview.setPadding(dview.getPaddingLeft() + Global.scale(16.0f), dview.getPaddingTop() + Global.scale(10.0f), dview.getPaddingRight() + Global.scale(16.0f), dview.getPaddingBottom() + Global.scale(10.0f));
        this.dndView = new FrameLayout(act);
        this.dndView.setBackgroundResource(C0436R.drawable.bg_post_comments_btm);
        this.dndView.addView(dview);
        View hl2 = new View(act);
        hl2.setBackgroundResource(C0436R.drawable.highlight);
        this.dndView.addView(hl2);
        act.setTitle(this.create ? C0436R.string.create_conversation : C0436R.string.chat_members);
        if (this.create) {
            ArrayList<UserProfile> users = getArguments().getParcelableArrayList("users");
            UserProfile cur = new UserProfile();
            cur.uid = Global.uid;
            cur.fullName = act.getSharedPreferences(null, 0).getString("username", ACRAConstants.DEFAULT_STRING_VALUE);
            cur.photo = act.getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE);
            Iterator it = users.iterator();
            while (it.hasNext()) {
                UserProfile p = (UserProfile) it.next();
                ChatUser cu = new ChatUser();
                cu.user = p;
                cu.inviter = cur;
                this.chatUsers.add(cu);
                updateList();
            }
            this.sendBtn = View.inflate(act, C0436R.layout.ab_done_right, null);
            ((TextView) this.sendBtn.findViewById(C0436R.id.ab_done_text)).setText(C0436R.string.create);
            this.sendBtn.setOnClickListener(new C06396());
            setHasOptionsMenu(true);
        } else {
            updateChatUsers();
        }
        updateSettings();
        this.titleView.findViewById(C0436R.id.chat_edit_photo).setOnClickListener(new C06407());
        if (this.create) {
            this.titleView.findViewById(C0436R.id.chat_edit_photo).setVisibility(8);
            this.titleView.findViewById(C0436R.id.chat_edit_stitle).setVisibility(8);
            return;
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(LongPollService.ACTION_CHAT_CHANGED);
        filter.addAction(UploaderService.ACTION_UPLOAD_DONE);
        filter.addAction(UploaderService.ACTION_UPLOAD_PROGRESS);
        filter.addAction(UploaderService.ACTION_UPLOAD_FAILED);
        VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
    }

    public void onDestroy() {
        super.onDestroy();
        if (!this.create) {
            try {
                VKApplication.context.unregisterReceiver(this.receiver);
            } catch (Exception e) {
            }
        }
    }

    public void onPause() {
        super.onPause();
        this.imgLoader.deactivate();
    }

    public void onResume() {
        super.onResume();
        this.imgLoader.activate();
    }

    private void updateChatUsers() {
        Messages.getChatUsers(this.id, new C14498());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.list;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (this.sendBtn != null) {
            MenuItem item = menu.add((int) C0436R.string.send);
            item.setActionView(this.sendBtn);
            item.setShowAsAction(2);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void loadOnlines() {
        ArrayList<Integer> needOnlines = new ArrayList();
        Iterator it = this.chatUsers.iterator();
        while (it.hasNext()) {
            ChatUser user = (ChatUser) it.next();
            if (Friends.isFriend(user.user.uid)) {
                user.user.online = Friends.getOnlineStatus(user.user.uid);
            } else {
                needOnlines.add(Integer.valueOf(user.user.uid));
            }
        }
        updateList();
        if (needOnlines.size() != 0) {
            new GetUserOnlines(needOnlines).setCallback(new C14509()).exec(getActivity());
        }
    }

    private void updateList() {
        this.adapter.notifyDataSetChanged();
        this.imgLoader.updateImages();
    }

    private void createChat() {
        String title = this.titleEdit.getText().toString();
        if (title.length() == 0) {
            ArrayList<String> ttl = new ArrayList();
            for (int i = 0; i < Math.min(3, this.chatUsers.size()); i++) {
                ttl.add(((ChatUser) this.chatUsers.get(i)).user.firstName);
            }
            title = TextUtils.join(", ", ttl);
        }
        ArrayList<UserProfile> users = new ArrayList();
        Iterator it = this.chatUsers.iterator();
        while (it.hasNext()) {
            users.add(((ChatUser) it.next()).user);
        }
        new MessagesCreateChat(users, title).setCallback(new MessagesCreateChat.Callback() {
            public void success(int chatID) {
                UserProfile profile = new UserProfile();
                profile.uid = 2000000000 + chatID;
                profile.fullName = ChatMembersFragment.this.titleEdit.getText().toString();
                profile.online = Global.uid;
                ArrayList<String> ph = new ArrayList();
                ph.add("M");
                for (int i = 0; i < Math.min(ChatMembersFragment.this.chatUsers.size(), 4); i++) {
                    ph.add(((ChatUser) ChatMembersFragment.this.chatUsers.get(i)).user.photo);
                }
                profile.photo = TextUtils.join("|", ph);
                ArrayList<UserProfile> al = new ArrayList();
                al.add(profile);
                Cache.updatePeers(al, false);
                Bundle args = new Bundle();
                args.putInt("id", profile.uid);
                args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, profile.fullName);
                Navigate.to("ChatFragment", args, ChatMembersFragment.this.getActivity());
                ChatMembersFragment.this.getActivity().setResult(-1);
                ChatMembersFragment.this.getActivity().finish();
            }

            public void fail(int ecode, String emsg) {
                Toast.makeText(ChatMembersFragment.this.getActivity(), C0436R.string.err_text, 0).show();
            }
        }).wrapProgress(getActivity()).exec(getActivity());
    }

    private void updateSettings() {
        SharedPreferences prefs = getActivity().getSharedPreferences("notify", 0);
        this.notifySwitch.setChecked(!prefs.contains(new StringBuilder("mute").append(this.id + 2000000000).toString()));
        int dnd = prefs.getInt("dnd" + (this.id + 2000000000), 0);
        if (dnd > ((int) (System.currentTimeMillis() / 1000))) {
            this.dndText.setVisibility(0);
            this.dndText.setText(dnd == ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED ? getResources().getStringArray(C0436R.array.dnd_options)[3] : Global.langDate(getResources(), dnd));
            return;
        }
        this.dndText.setVisibility(8);
    }

    private void setMute(boolean mute) {
        SharedPreferences prefs = getActivity().getSharedPreferences("notify", 0);
        if (mute) {
            prefs.edit().putBoolean("mute" + (this.id + 2000000000), true).commit();
        } else {
            prefs.edit().remove("mute" + (this.id + 2000000000)).commit();
        }
    }

    private void setDnd(int time) {
        Log.m525d("vk", "set dnd " + time);
        getActivity().getSharedPreferences("notify", 0).edit().putInt("dnd" + (2000000000 + this.id), time).commit();
    }

    private void showDndDialog() {
        if (getActivity().getSharedPreferences("notify", 0).getInt("dnd" + (2000000000 + this.id), 0) > ((int) (System.currentTimeMillis() / 1000))) {
            new VKAlertDialog.Builder(getActivity()).setTitle(C0436R.string.chat_dnd).setMessage(C0436R.string.dnd_cancel).setPositiveButton(C0436R.string.yes, new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    ChatMembersFragment.this.setDnd(0);
                    ChatMembersFragment.this.updateSettings();
                }
            }).setNegativeButton(C0436R.string.no, null).show();
        } else {
            new VKAlertDialog.Builder(getActivity()).setItems(C0436R.array.dnd_options, new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    int t = 0;
                    int now = (int) (System.currentTimeMillis() / 1000);
                    switch (which) {
                        case ValidationActivity.VRESULT_NONE /*0*/:
                            t = now + 3600;
                            break;
                        case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                            t = now + 86400;
                            break;
                        case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                            t = now + 604800;
                            break;
                        case Group.ADMIN_LEVEL_ADMIN /*3*/:
                            t = ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
                            break;
                    }
                    ChatMembersFragment.this.setDnd(t);
                    ChatMembersFragment.this.updateSettings();
                }
            }).setTitle(C0436R.string.chat_dnd).show();
        }
    }

    private void showFriendPicker() {
        Bundle args = new Bundle();
        args.putBoolean("select", true);
        Intent intent = new Intent(getActivity(), FragmentWrapperActivity.class);
        intent.putExtra("class", "FriendsFragment");
        intent.putExtra("args", args);
        startActivityForResult(intent, INVITE_RESULT);
    }

    private void setChatTitle(String newTitle) {
        if (!getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE).toString().equals(newTitle) && newTitle.trim().length() != 0) {
            new MessagesEditChat(this.id, newTitle).setCallback(new MessagesEditChat.Callback() {
                public void success() {
                    Toast.makeText(ChatMembersFragment.this.getActivity(), C0436R.string.chat_title_change_ok, 0).show();
                }

                public void fail(int ecode, String emsg) {
                    Toast.makeText(ChatMembersFragment.this.getActivity(), C0436R.string.err_text, 0).show();
                }
            }).wrapProgress(getActivity()).exec(getActivity());
        }
    }

    private void removeUser(int idx) {
        new VKAlertDialog.Builder(getActivity()).setTitle(C0436R.string.confirm).setMessage(C0436R.string.delete_chat_user_confirm).setPositiveButton(C0436R.string.yes, new AnonymousClass14(idx)).setNegativeButton(C0436R.string.no, null).show();
    }

    private void doRemoveUser(int idx) {
        new MessagesRemoveChatUser(this.id, ((ChatUser) this.chatUsers.get(idx)).user.uid).setCallback(new AnonymousClass15(idx)).wrapProgress(getActivity()).exec(getActivity());
    }

    private void leaveChat() {
        new VKAlertDialog.Builder(getActivity()).setTitle(C0436R.string.confirm).setMessage(C0436R.string.chat_leave_confirm).setPositiveButton(C0436R.string.yes, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int idx = -1;
                for (int i = 0; i < ChatMembersFragment.this.chatUsers.size(); i++) {
                    if (((ChatUser) ChatMembersFragment.this.chatUsers.get(i)).user.uid == Global.uid) {
                        idx = i;
                        break;
                    }
                }
                if (idx != -1) {
                    ChatMembersFragment.this.doRemoveUser(idx);
                }
            }
        }).setNegativeButton(C0436R.string.no, null).show();
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (resCode == -1) {
            if (reqCode == INVITE_RESULT) {
                UserProfile p = (UserProfile) data.getParcelableExtra("user");
                new MessagesAddChatUser(this.id, p.uid).setCallback(new AnonymousClass17(p)).wrapProgress(getActivity()).exec(getActivity());
            }
            if (reqCode == PHOTO_RESULT) {
                this.uploadProgress = new ProgressDialog(getActivity());
                this.uploadProgress.setProgressStyle(1);
                this.uploadProgress.setTitle(getString(C0436R.string.uploading_photo));
                this.uploadProgress.setIndeterminate(true);
                this.uploadProgress.setButton(-3, getString(C0436R.string.cancel), new OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                this.uploadProgress.setOnCancelListener(new OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        if (UploaderService.currentInstance != null) {
                            UploaderService.currentInstance.cancel(ChatMembersFragment.this.uploadID);
                        }
                    }
                });
                this.uploadProgress.show();
                this.uploadID = UploaderService.getNewID();
                Intent intent = new Intent(getActivity(), UploaderService.class);
                intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 7);
                intent.putExtra("file", data.getStringExtra("file"));
                intent.putExtra("id", this.uploadID);
                intent.putExtra("new", true);
                HashMap<String, String> rp = new HashMap();
                rp.put("chat_id", new StringBuilder(String.valueOf(this.id)).toString());
                intent.putExtra("req_params", rp);
                getActivity().startService(intent);
            }
        }
        if (resCode == 1 && reqCode == PHOTO_RESULT) {
            new MessagesDeleteChatPhoto(this.id).setCallback(new MessagesDeleteChatPhoto.Callback() {
                public void success(int mid) {
                    ((ImageView) ChatMembersFragment.this.titleView.findViewById(C0436R.id.chat_edit_photo)).setImageResource(C0436R.drawable.ic_chat_multi);
                }

                public void fail(int ecode, String emsg) {
                    Toast.makeText(ChatMembersFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
                }
            }).wrapProgress(getActivity()).exec(getActivity());
        }
    }
}
