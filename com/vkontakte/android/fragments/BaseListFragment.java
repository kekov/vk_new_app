package com.vkontakte.android.fragments;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.ErrorView;
import org.acra.ACRAConstants;

public abstract class BaseListFragment extends SherlockFragment {
    protected FrameLayout contentView;
    protected FrameLayout contentWrap;
    protected EmptyView emptyView;
    protected ErrorView errorView;
    protected ListView list;
    protected ProgressBar progress;

    protected ListView initListView() {
        ListView l = new ListView(getActivity());
        l.setSelector(C0436R.drawable.highlight);
        l.setDivider(new ColorDrawable(-2104603));
        l.setDividerHeight(1);
        return l;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        this.contentView = new FrameLayout(act);
        this.contentWrap = new FrameLayout(act);
        this.list = initListView();
        this.contentWrap.addView(this.list);
        this.contentView.addView(this.contentWrap);
        this.emptyView = EmptyView.create(act);
        this.emptyView.setText(getEmptyText());
        this.contentWrap.addView(this.emptyView);
        this.list.setEmptyView(this.emptyView);
        this.progress = new ProgressBar(act);
        this.contentView.addView(this.progress, new LayoutParams(Global.scale(40.0f), Global.scale(40.0f), 17));
        this.errorView = (ErrorView) View.inflate(act, C0436R.layout.error, null);
        this.contentView.addView(this.errorView);
        this.contentWrap.setVisibility(8);
        this.errorView.setVisibility(8);
        this.contentView.setBackgroundColor(-1);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentView;
    }

    protected String getEmptyText() {
        return ACRAConstants.DEFAULT_STRING_VALUE;
    }
}
