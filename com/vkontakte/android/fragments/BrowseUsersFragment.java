package com.vkontakte.android.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewPropertyAnimator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnCloseListener;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.MultiSectionImageLoaderAdapter;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.api.UsersSearch;
import com.vkontakte.android.api.UsersSearch.Callback;
import com.vkontakte.android.data.database.City;
import com.vkontakte.android.data.database.Country;
import com.vkontakte.android.fragments.CitySelectFragment.CityCallback;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.Fonts;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.LoadMoreFooterView;
import com.vkontakte.android.ui.MultiSectionAdapter;
import com.vkontakte.android.ui.OverlayTextView;
import com.vkontakte.android.ui.PhotoView;
import java.lang.reflect.Field;
import java.util.ArrayList;
import org.acra.ACRAConstants;

public class BrowseUsersFragment extends SherlockFragment implements BackListener {
    private UsersAdapter adapter;
    private int ageFrom;
    private int ageTo;
    private int city;
    private FrameLayout content;
    private int country;
    private APIRequest currentReq;
    private boolean dataLoading;
    private Runnable delayedSearch;
    private EmptyView empty;
    private View extParamsView;
    private Handler handler;
    private ListImageLoaderWrapper imgLoader;
    private boolean isEmpty;
    private ListView list;
    private LoadMoreFooterView loadMoreView;
    private boolean moreAvailable;
    private int offset;
    private OverlayTextView paramsBtn;
    private boolean preloadOnReady;
    private ArrayList<UserProfile> preloadedResults;
    private boolean preloading;
    private int prevHash;
    private ProgressBar progress;
    private String query;
    private int relation;
    private ArrayList<UserProfile> searchResults;
    private SearchView searchView;
    private int total;
    private FrameLayout wrap;

    /* renamed from: com.vkontakte.android.fragments.BrowseUsersFragment.3 */
    class C06093 implements OnItemClickListener {
        C06093() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long id) {
            Bundle args = new Bundle();
            args.putInt("id", (int) id);
            Navigate.to("ProfileFragment", args, BrowseUsersFragment.this.getActivity());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BrowseUsersFragment.4 */
    class C06104 extends LinearLayout {
        C06104(Context $anonymous0) {
            super($anonymous0);
        }

        public boolean onTouchEvent(MotionEvent ev) {
            if (ev.getAction() == 0) {
                BrowseUsersFragment.this.toggleExtParams();
                requestDisallowInterceptTouchEvent(true);
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BrowseUsersFragment.5 */
    class C06115 implements OnClickListener {
        C06115() {
        }

        public void onClick(View v) {
            BrowseUsersFragment.this.toggleExtParams();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BrowseUsersFragment.7 */
    class C06127 extends ArrayAdapter<Country> {
        C06127(Context $anonymous0, int $anonymous1, Country[] $anonymous2) {
            super($anonymous0, $anonymous1, $anonymous2);
        }

        public long getItemId(int pos) {
            return (long) ((Country) getItem(pos)).id;
        }

        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View v = super.getDropDownView(position, convertView, parent);
            if (v instanceof TextView) {
                ((TextView) v).setTypeface(((Country) getItem(position)).important ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            }
            return v;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BrowseUsersFragment.8 */
    class C06138 implements OnItemSelectedListener {
        C06138() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long id) {
            if (((long) BrowseUsersFragment.this.country) != id) {
                boolean z;
                BrowseUsersFragment.this.country = (int) id;
                BrowseUsersFragment.this.city = 0;
                ((TextView) BrowseUsersFragment.this.extParamsView.findViewById(C0436R.id.city_btn)).setText(C0436R.string.edit_choose_city);
                View findViewById = BrowseUsersFragment.this.extParamsView.findViewById(C0436R.id.city_btn);
                if (BrowseUsersFragment.this.country > 0) {
                    z = true;
                } else {
                    z = false;
                }
                findViewById.setEnabled(z);
                BrowseUsersFragment.this.searchDelayed();
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BrowseUsersFragment.9 */
    class C06149 implements OnClickListener {
        C06149() {
        }

        public void onClick(View v) {
            for (View view : new View[]{BrowseUsersFragment.this.extParamsView.findViewById(C0436R.id.gender_any), BrowseUsersFragment.this.extParamsView.findViewById(C0436R.id.gender_male), BrowseUsersFragment.this.extParamsView.findViewById(C0436R.id.gender_female)}) {
                boolean z;
                if (view == v) {
                    z = true;
                } else {
                    z = false;
                }
                view.setSelected(z);
            }
            if (Global.isTablet) {
                BrowseUsersFragment.this.searchDelayed();
            }
        }
    }

    private class SearchRunner implements Runnable {
        private SearchRunner() {
        }

        public void run() {
            BrowseUsersFragment.this.delayedSearch = null;
            if (BrowseUsersFragment.this.getActivity() != null && BrowseUsersFragment.this.loadData(true)) {
                BrowseUsersFragment.this.searchResults.clear();
                BrowseUsersFragment.this.preloadedResults.clear();
                BrowseUsersFragment.this.wrap.setVisibility(8);
                BrowseUsersFragment.this.progress.setVisibility(0);
                BrowseUsersFragment.this.updateList();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BrowseUsersFragment.17 */
    class AnonymousClass17 implements Callback {
        private final /* synthetic */ boolean val$reload;

        AnonymousClass17(boolean z) {
            this.val$reload = z;
        }

        public void success(ArrayList<UserProfile> results, int total) {
            int i = 100;
            BrowseUsersFragment.this.dataLoading = false;
            BrowseUsersFragment.this.currentReq = null;
            if (BrowseUsersFragment.this.getActivity() != null) {
                boolean z;
                BrowseUsersFragment browseUsersFragment;
                if (this.val$reload) {
                    browseUsersFragment = BrowseUsersFragment.this;
                    if (BrowseUsersFragment.this.preloading) {
                        i = 50;
                    }
                    browseUsersFragment.offset = i;
                } else {
                    browseUsersFragment = BrowseUsersFragment.this;
                    int access$37 = browseUsersFragment.offset;
                    if (BrowseUsersFragment.this.preloading) {
                        i = 50;
                    }
                    browseUsersFragment.offset = i + access$37;
                }
                Global.showViewAnimated(BrowseUsersFragment.this.wrap, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(BrowseUsersFragment.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                BrowseUsersFragment.this.total = total;
                if (BrowseUsersFragment.this.preloading) {
                    BrowseUsersFragment.this.preloadedResults.addAll(results);
                } else if (results.size() > 50) {
                    BrowseUsersFragment.this.searchResults.addAll(results.subList(0, 50));
                    BrowseUsersFragment.this.preloadedResults.addAll(results.subList(50, results.size()));
                } else {
                    BrowseUsersFragment.this.searchResults.addAll(results);
                }
                BrowseUsersFragment.this.preloading = false;
                if (BrowseUsersFragment.this.preloadOnReady) {
                    BrowseUsersFragment.this.preloading = true;
                    BrowseUsersFragment.this.preloadOnReady = false;
                    BrowseUsersFragment.this.loadData(false);
                }
                total = Math.min(LocationStatusCodes.GEOFENCE_NOT_AVAILABLE, total);
                BrowseUsersFragment browseUsersFragment2 = BrowseUsersFragment.this;
                if (total <= BrowseUsersFragment.this.searchResults.size() || results.size() <= 0) {
                    z = false;
                } else {
                    z = true;
                }
                browseUsersFragment2.moreAvailable = z;
                BrowseUsersFragment.this.loadMoreView.setVisible(BrowseUsersFragment.this.moreAvailable);
                BrowseUsersFragment.this.updateList();
            }
        }

        public void fail(int ecode, String emsg) {
            BrowseUsersFragment.this.dataLoading = false;
            BrowseUsersFragment.this.currentReq = null;
            if (BrowseUsersFragment.this.getActivity() != null) {
                BrowseUsersFragment.this.wrap.setVisibility(0);
                BrowseUsersFragment.this.progress.setVisibility(8);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BrowseUsersFragment.1 */
    class C14421 implements OnQueryTextListener {
        C14421() {
        }

        public boolean onQueryTextSubmit(String query) {
            View focus = BrowseUsersFragment.this.getActivity().getCurrentFocus();
            BrowseUsersFragment.this.query = query;
            if (focus != null) {
                ((InputMethodManager) BrowseUsersFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(focus.getWindowToken(), 0);
                focus.clearFocus();
            }
            if (BrowseUsersFragment.this.delayedSearch != null) {
                BrowseUsersFragment.this.handler.removeCallbacks(BrowseUsersFragment.this.delayedSearch);
                BrowseUsersFragment.this.delayedSearch.run();
                BrowseUsersFragment.this.delayedSearch = null;
            } else {
                new SearchRunner(null).run();
            }
            return true;
        }

        public boolean onQueryTextChange(String newText) {
            if (newText.length() == 0 && !BrowseUsersFragment.this.dataLoading) {
                BrowseUsersFragment.this.query = newText;
                BrowseUsersFragment.this.searchDelayed();
            }
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BrowseUsersFragment.2 */
    class C14432 implements OnCloseListener {
        C14432() {
        }

        public boolean onClose() {
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.BrowseUsersFragment.6 */
    class C14446 implements Listener {
        C14446() {
        }

        public void onScrolledToLastItem() {
            if ((BrowseUsersFragment.this.dataLoading && !BrowseUsersFragment.this.preloading) || !BrowseUsersFragment.this.moreAvailable) {
                return;
            }
            if (BrowseUsersFragment.this.preloading) {
                BrowseUsersFragment.this.preloading = false;
                BrowseUsersFragment.this.preloadOnReady = true;
            } else if (BrowseUsersFragment.this.preloadedResults.size() > 0) {
                BrowseUsersFragment.this.searchResults.addAll(BrowseUsersFragment.this.preloadedResults);
                BrowseUsersFragment.this.updateList();
                BrowseUsersFragment.this.preloadedResults.clear();
                BrowseUsersFragment.this.preloading = true;
                BrowseUsersFragment.this.loadData(false);
            } else {
                BrowseUsersFragment.this.loadData(false);
            }
        }

        public void onScrollStopped() {
        }

        public void onScrollStarted() {
            ((InputMethodManager) BrowseUsersFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(BrowseUsersFragment.this.getActivity().getCurrentFocus().getWindowToken(), 0);
            BrowseUsersFragment.this.getActivity().getCurrentFocus().clearFocus();
        }
    }

    protected class UserPhotosAdapter extends MultiSectionImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.fragments.BrowseUsersFragment.UserPhotosAdapter.1 */
        class C06151 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ View val$v;

            C06151(View view, Bitmap bitmap) {
                this.val$v = view;
                this.val$bitmap = bitmap;
            }

            public void run() {
                if (this.val$v != null) {
                    View vv = this.val$v.findViewById(C0436R.id.flist_item_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        protected UserPhotosAdapter() {
        }

        public int getItemCount(int section) {
            return BrowseUsersFragment.this.searchResults.size();
        }

        public int getImageCountForItem(int section, int item) {
            return 1;
        }

        public String getImageURL(int section, int item, int image) {
            return ((UserProfile) BrowseUsersFragment.this.searchResults.get(item)).photo;
        }

        public void imageLoaded(int item, int image, Bitmap bitmap) {
            if (BrowseUsersFragment.this.list != null) {
                item += BrowseUsersFragment.this.list.getHeaderViewsCount();
                if (item >= BrowseUsersFragment.this.list.getFirstVisiblePosition() && item <= BrowseUsersFragment.this.list.getLastVisiblePosition()) {
                    BrowseUsersFragment.this.getActivity().runOnUiThread(new C06151(BrowseUsersFragment.this.list.getChildAt(item - BrowseUsersFragment.this.list.getFirstVisiblePosition()), bitmap));
                }
            }
        }

        public int getSectionCount() {
            return 1;
        }

        public boolean isSectionHeaderVisible(int section) {
            return BrowseUsersFragment.this.adapter.isSectionHeaderVisible(section);
        }
    }

    protected class UsersAdapter extends MultiSectionAdapter {
        protected UsersAdapter() {
        }

        public int getItemCount(int section) {
            return BrowseUsersFragment.this.searchResults.size();
        }

        public Object getItem(int section, int position) {
            return BrowseUsersFragment.this.searchResults.get(position);
        }

        public long getItemId(int section, int position) {
            if (position < 0 || position >= BrowseUsersFragment.this.searchResults.size()) {
                return 0;
            }
            return (long) ((UserProfile) BrowseUsersFragment.this.searchResults.get(position)).uid;
        }

        public View getView(int section, int position, View convertView) {
            View view = convertView;
            if (view == null) {
                view = View.inflate(BrowseUsersFragment.this.getActivity(), C0436R.layout.suggest_list_item, null);
                ((TextView) view.findViewById(C0436R.id.flist_item_subtext)).setTypeface(Fonts.getRobotoLight());
                view.findViewById(C0436R.id.flist_item_online).setVisibility(8);
            }
            if (position == 0) {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_top);
            } else if (position == BrowseUsersFragment.this.searchResults.size() - 1) {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_btm);
            } else {
                view.setBackgroundResource(C0436R.drawable.bg_post_comments_mid);
            }
            UserProfile u = (UserProfile) BrowseUsersFragment.this.searchResults.get(position);
            CharSequence name = u.fullName;
            if ((u.extra instanceof Boolean) && ((Boolean) u.extra).booleanValue()) {
                SpannableStringBuilder bldr = new SpannableStringBuilder(name);
                Spannable sp = Factory.getInstance().newSpannable("F");
                Drawable d = BrowseUsersFragment.this.getResources().getDrawable(C0436R.drawable.ic_user_verified);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                sp.setSpan(new ImageSpan(d, 1), 0, 1, 0);
                bldr.append("\u00a0");
                bldr.append(sp);
                name = bldr;
            }
            ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(name);
            ((TextView) view.findViewById(C0436R.id.flist_item_subtext)).setText(u.university);
            if (BrowseUsersFragment.this.imgLoader.isAlreadyLoaded(u.photo)) {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageBitmap(BrowseUsersFragment.this.imgLoader.get(u.photo));
            } else {
                ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(u.uid < 0 ? C0436R.drawable.group_placeholder : C0436R.drawable.user_placeholder);
            }
            return view;
        }

        public String getSectionTitle(int section) {
            if (BrowseUsersFragment.this.isEmpty) {
                return BrowseUsersFragment.this.getString(C0436R.string.now_on_site);
            }
            return Global.langPlural(C0436R.array.people_found, BrowseUsersFragment.this.total, BrowseUsersFragment.this.getResources());
        }

        public int getSectionCount() {
            return 1;
        }

        public boolean isSectionHeaderVisible(int section) {
            return BrowseUsersFragment.this.searchResults.size() > 0;
        }

        public int getHeaderLayoutResource() {
            return C0436R.layout.list_cards_section_header;
        }
    }

    public BrowseUsersFragment() {
        this.searchResults = new ArrayList();
        this.preloadedResults = new ArrayList();
        this.isEmpty = false;
        this.query = ACRAConstants.DEFAULT_STRING_VALUE;
        this.prevHash = -1;
        this.handler = new Handler();
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        getSherlockActivity().getSupportActionBar().setNavigationMode(0);
        getSherlockActivity().getSupportActionBar().setDisplayShowTitleEnabled(false);
        act.setTitle(C0436R.string.search);
        this.searchView = new SearchView(getSherlockActivity().getSupportActionBar().getThemedContext());
        this.searchView.setQueryHint(getString(C0436R.string.search_people));
        this.searchView.setIconified(false);
        try {
            Field searchField = SearchView.class.getDeclaredField("mSearchButton");
            searchField.setAccessible(true);
            ((ImageView) searchField.get(this.searchView)).setImageResource(C0436R.drawable.ic_ab_search);
            searchField = SearchView.class.getDeclaredField("mSearchPlate");
            searchField.setAccessible(true);
            ((TextView) ((LinearLayout) searchField.get(this.searchView)).getChildAt(0)).setHintTextColor(-2130706433);
        } catch (Exception e) {
        }
        this.searchView.setOnQueryTextListener(new C14421());
        this.searchView.setOnCloseListener(new C14432());
        this.searchView.setMaxWidth(2147483637);
        this.searchView.onActionViewExpanded();
        getSherlockActivity().getSupportActionBar().setCustomView(this.searchView);
        getSherlockActivity().getSupportActionBar().setDisplayShowCustomEnabled(true);
        setHasOptionsMenu(true);
        if (getArguments() != null && getArguments().containsKey("q")) {
            this.query = getArguments().getString("q");
            this.searchView.setQuery(this.query, false);
        }
        loadData(true);
    }

    public void onDetach() {
        getSherlockActivity().getSupportActionBar().setCustomView(null);
        getSherlockActivity().getSupportActionBar().setDisplayShowCustomEnabled(false);
        super.onDetach();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void onConfigurationChanged(Configuration cfg) {
        int i = 1;
        super.onConfigurationChanged(cfg);
        if (Global.isTablet) {
            int i2;
            DisplayMetrics dm = getResources().getDisplayMetrics();
            LinearLayout linearLayout = (LinearLayout) this.extParamsView.findViewById(C0436R.id.region_wrap);
            if (dm.widthPixels < dm.heightPixels) {
                i2 = 1;
            } else {
                i2 = 0;
            }
            linearLayout.setOrientation(i2);
            linearLayout = (LinearLayout) this.extParamsView.findViewById(C0436R.id.gender_wrap);
            if (dm.widthPixels >= dm.heightPixels) {
                i = 0;
            }
            linearLayout.setOrientation(i);
            int size = dm.widthPixels < dm.heightPixels ? Global.scale(200.0f) : Global.scale(370.0f);
            LayoutParams wlp = new LayoutParams(-1, -1, 3);
            wlp.rightMargin = size;
            this.wrap.setLayoutParams(wlp);
            this.extParamsView.setLayoutParams(new LayoutParams(size, -1, 53));
            ((MarginLayoutParams) this.progress.getLayoutParams()).rightMargin = size / 2;
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.content = new FrameLayout(getActivity());
        this.content.setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        this.wrap = new FrameLayout(getActivity());
        this.list = new ListView(getActivity());
        this.wrap.addView(this.list);
        this.empty = EmptyView.create(getActivity());
        this.empty.setText((int) C0436R.string.nothing_found);
        this.empty.setButtonVisible(false);
        this.wrap.addView(this.empty);
        this.list.setEmptyView(this.empty);
        this.list.setDividerHeight(0);
        this.list.setSelector(new ColorDrawable(0));
        this.content.addView(this.wrap);
        this.loadMoreView = new LoadMoreFooterView(getActivity());
        this.list.addFooterView(this.loadMoreView, null, false);
        this.progress = new ProgressBar(getActivity());
        this.progress.setVisibility(this.dataLoading ? 0 : 8);
        this.wrap.setVisibility(this.dataLoading ? 8 : 0);
        this.content.addView(this.progress, new LayoutParams(Global.scale(40.0f), Global.scale(40.0f), 17));
        this.list.setOnItemClickListener(new C06093());
        this.extParamsView = inflater.inflate(C0436R.layout.search_ext_params, null);
        this.extParamsView.setBackgroundColor(-1);
        ViewGroup.LayoutParams layoutParams;
        if (Global.isTablet) {
            int size;
            DisplayMetrics dm = getResources().getDisplayMetrics();
            ((LinearLayout) this.extParamsView.findViewById(C0436R.id.region_wrap)).setOrientation(dm.widthPixels < dm.heightPixels ? 1 : 0);
            ((LinearLayout) this.extParamsView.findViewById(C0436R.id.gender_wrap)).setOrientation(dm.widthPixels < dm.heightPixels ? 1 : 0);
            if (dm.widthPixels < dm.heightPixels) {
                size = Global.scale(200.0f);
            } else {
                size = Global.scale(370.0f);
            }
            layoutParams = new LayoutParams(-1, -1, 3);
            layoutParams.rightMargin = size;
            this.wrap.setLayoutParams(layoutParams);
            layoutParams = new LayoutParams(size, -1, 53);
            ((MarginLayoutParams) this.progress.getLayoutParams()).rightMargin = size / 2;
            this.content.addView(this.extParamsView, layoutParams);
        } else {
            View c06104 = new C06104(getActivity());
            c06104.setOrientation(1);
            c06104.addView(this.extParamsView, -1, -2);
            c06104 = new ImageView(getActivity());
            c06104.setImageResource(C0436R.drawable.full_width_shadow);
            c06104.setScaleType(ScaleType.FIT_XY);
            c06104.addView(c06104, -1, -2);
            c06104.setPadding(0, Global.scale(GalleryPickerFooterView.SIZE), 0, 0);
            this.extParamsView = c06104;
            this.extParamsView.setVisibility(8);
            ImageView btnShadow = new ImageView(getActivity());
            btnShadow.setImageResource(C0436R.drawable.full_width_shadow);
            btnShadow.setScaleType(ScaleType.FIT_XY);
            LayoutParams bslp = new LayoutParams(-1, -2, 48);
            bslp.topMargin = Global.scale(GalleryPickerFooterView.SIZE);
            btnShadow.setLayoutParams(bslp);
            this.paramsBtn = new OverlayTextView(getActivity());
            this.paramsBtn.setText(C0436R.string.search_params);
            this.paramsBtn.setTextColor(-7960438);
            this.paramsBtn.setBackgroundColor(-986638);
            this.paramsBtn.setTextSize(1, 16.0f);
            this.paramsBtn.setGravity(19);
            this.paramsBtn.setPadding(Global.scale(12.0f), 0, Global.scale(12.0f), 0);
            this.paramsBtn.setOverlay((int) C0436R.drawable.highlight);
            Drawable arrow = getResources().getDrawable(C0436R.drawable.ic_search_arrow_down);
            arrow.setBounds(0, 0, arrow.getIntrinsicWidth(), arrow.getIntrinsicHeight());
            this.paramsBtn.setCompoundDrawables(null, null, arrow, null);
            layoutParams = new LayoutParams(-1, -1, 3);
            layoutParams.topMargin = Global.scale(GalleryPickerFooterView.SIZE);
            this.wrap.setLayoutParams(layoutParams);
            this.paramsBtn.setOnClickListener(new C06115());
            this.content.addView(btnShadow);
            this.content.addView(this.extParamsView, new LayoutParams(-1, -1));
            this.content.addView(this.paramsBtn, new LayoutParams(-1, Global.scale(GalleryPickerFooterView.SIZE), 48));
        }
        ListView listView = this.list;
        ListAdapter usersAdapter = new UsersAdapter();
        this.adapter = usersAdapter;
        listView.setAdapter(usersAdapter);
        this.imgLoader = new ListImageLoaderWrapper(new UserPhotosAdapter(), this.list, new C14446());
        Spinner countrySpinner = (Spinner) this.extParamsView.findViewById(C0436R.id.country_spinner);
        ArrayAdapter<Country> countryAdapter = new C06127(getActivity(), 17367048, (Country[]) Country.getCountries(true, true, getString(C0436R.string.edit_choose_country)).toArray(new Country[0]));
        countryAdapter.setDropDownViewResource(17367049);
        countrySpinner.setAdapter(countryAdapter);
        countrySpinner.setOnItemSelectedListener(new C06138());
        OnClickListener genderClickListener = new C06149();
        this.extParamsView.findViewById(C0436R.id.gender_any).setSelected(true);
        this.extParamsView.findViewById(C0436R.id.gender_any).setOnClickListener(genderClickListener);
        this.extParamsView.findViewById(C0436R.id.gender_male).setOnClickListener(genderClickListener);
        this.extParamsView.findViewById(C0436R.id.gender_female).setOnClickListener(genderClickListener);
        ArrayAdapter<String> ageAdapterFrom = new ArrayAdapter(getActivity(), 17367048);
        ageAdapterFrom.setDropDownViewResource(17367049);
        ageAdapterFrom.add(getResources().getString(C0436R.string.from));
        ArrayAdapter<String> ageAdapterTo = new ArrayAdapter(getActivity(), 17367048);
        ageAdapterTo.setDropDownViewResource(17367049);
        ageAdapterTo.add(getResources().getString(C0436R.string.to));
        for (int i = 14; i <= 80; i++) {
            ageAdapterFrom.add(getString(C0436R.string.age_from, Integer.valueOf(i)));
            ageAdapterTo.add(getString(C0436R.string.age_to, Integer.valueOf(i)));
        }
        ((Spinner) this.extParamsView.findViewById(C0436R.id.age_from)).setAdapter(ageAdapterFrom);
        ((Spinner) this.extParamsView.findViewById(C0436R.id.age_to)).setAdapter(ageAdapterTo);
        OnItemSelectedListener ageSelListener = new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> v, View arg1, int pos, long arg3) {
                int age = pos > 0 ? pos + 13 : 0;
                if (v.getId() == C0436R.id.age_from) {
                    if (BrowseUsersFragment.this.ageFrom != age) {
                        BrowseUsersFragment.this.ageFrom = age;
                        if (BrowseUsersFragment.this.ageTo < BrowseUsersFragment.this.ageFrom && BrowseUsersFragment.this.ageTo > 0) {
                            ((Spinner) BrowseUsersFragment.this.extParamsView.findViewById(C0436R.id.age_to)).setSelection(BrowseUsersFragment.this.ageFrom - 13);
                        }
                    } else {
                        return;
                    }
                } else if (BrowseUsersFragment.this.ageTo != age) {
                    BrowseUsersFragment.this.ageTo = age;
                    if (BrowseUsersFragment.this.ageFrom > BrowseUsersFragment.this.ageTo && BrowseUsersFragment.this.ageTo > 0) {
                        ((Spinner) BrowseUsersFragment.this.extParamsView.findViewById(C0436R.id.age_from)).setSelection(BrowseUsersFragment.this.ageTo - 13);
                    }
                } else {
                    return;
                }
                if (Global.isTablet) {
                    BrowseUsersFragment.this.searchDelayed();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        };
        ((Spinner) this.extParamsView.findViewById(C0436R.id.age_from)).setOnItemSelectedListener(ageSelListener);
        ((Spinner) this.extParamsView.findViewById(C0436R.id.age_to)).setOnItemSelectedListener(ageSelListener);
        ArrayAdapter<CharSequence> relationAdapter = ArrayAdapter.createFromResource(getActivity(), C0436R.array.edit_relation_m, 17367048);
        relationAdapter.setDropDownViewResource(17367049);
        ((Spinner) this.extParamsView.findViewById(C0436R.id.relation_spinner)).setAdapter(relationAdapter);
        ((Spinner) this.extParamsView.findViewById(C0436R.id.relation_spinner)).setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
                if (BrowseUsersFragment.this.relation != pos) {
                    BrowseUsersFragment.this.relation = pos;
                    if (Global.isTablet) {
                        BrowseUsersFragment.this.searchDelayed();
                    }
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.extParamsView.findViewById(C0436R.id.city_btn).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                BrowseUsersFragment.this.selectCity();
            }
        });
        return this.content;
    }

    private void selectCity() {
        CitySelectFragment fragment = new CitySelectFragment();
        Bundle args = new Bundle();
        args.putString("hint", getString(C0436R.string.edit_choose_city));
        args.putInt("country", this.country);
        args.putBoolean("show_none", this.city > 0);
        fragment.setArguments(args);
        fragment.setCallback(new CityCallback() {
            public void onItemSelected(City item) {
                BrowseUsersFragment.this.city = item.id;
                if (BrowseUsersFragment.this.city > 0) {
                    ((TextView) BrowseUsersFragment.this.extParamsView.findViewById(C0436R.id.city_btn)).setText(item.title);
                } else {
                    ((TextView) BrowseUsersFragment.this.extParamsView.findViewById(C0436R.id.city_btn)).setText(C0436R.string.edit_choose_city);
                }
                if (Global.isTablet) {
                    BrowseUsersFragment.this.searchDelayed();
                }
            }
        });
        fragment.show(getFragmentManager(), "city");
    }

    private void searchDelayed() {
        if (this.delayedSearch != null) {
            this.handler.removeCallbacks(this.delayedSearch);
        } else {
            this.delayedSearch = new SearchRunner();
        }
        this.handler.postDelayed(this.delayedSearch, 700);
    }

    private void toggleExtParams() {
        if (this.extParamsView != null) {
            ViewPropertyAnimator a;
            Drawable arrow;
            if (this.extParamsView.getVisibility() == 8) {
                this.extParamsView.setVisibility(0);
                if (VERSION.SDK_INT >= 14) {
                    this.extParamsView.setAlpha(0.0f);
                    this.extParamsView.setTranslationY((float) Global.scale(-100.0f));
                    a = this.extParamsView.animate().alpha(1.0f).translationY(0.0f).setDuration(250);
                    if (VERSION.SDK_INT >= 16) {
                        a.withLayer();
                    }
                    a.start();
                    a = this.wrap.animate().alpha(0.15f).setDuration(250);
                    if (VERSION.SDK_INT >= 16) {
                        a.withLayer();
                    }
                    a.start();
                }
                arrow = getResources().getDrawable(C0436R.drawable.ic_search_arrow_up);
                arrow.setBounds(0, 0, arrow.getIntrinsicWidth(), arrow.getIntrinsicHeight());
                this.paramsBtn.setCompoundDrawables(null, null, arrow, null);
                if (getActivity().getCurrentFocus() != null) {
                    ((InputMethodManager) getActivity().getSystemService("input_method")).hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                    getActivity().getCurrentFocus().clearFocus();
                    return;
                }
                return;
            }
            if (VERSION.SDK_INT >= 14) {
                a = this.extParamsView.animate().alpha(0.0f).translationY((float) Global.scale(-100.0f)).setDuration(200);
                if (VERSION.SDK_INT >= 16) {
                    a.withLayer();
                }
                a.start();
                a = this.wrap.animate().alpha(1.0f).setDuration(200);
                if (VERSION.SDK_INT >= 16) {
                    a.withEndAction(new Runnable() {
                        public void run() {
                            BrowseUsersFragment.this.extParamsView.setVisibility(8);
                        }
                    }).withLayer();
                } else {
                    this.extParamsView.postDelayed(new Runnable() {
                        public void run() {
                            BrowseUsersFragment.this.extParamsView.setVisibility(8);
                        }
                    }, 200);
                }
                a.start();
            } else {
                this.extParamsView.setVisibility(8);
            }
            arrow = getResources().getDrawable(C0436R.drawable.ic_search_arrow_down);
            arrow.setBounds(0, 0, arrow.getIntrinsicWidth(), arrow.getIntrinsicHeight());
            this.paramsBtn.setCompoundDrawables(null, null, arrow, null);
            new SearchRunner().run();
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.imgLoader.deactivate();
        this.list = null;
        this.wrap = null;
        this.content = null;
        this.extParamsView = null;
        this.imgLoader = null;
        this.adapter = null;
        this.progress = null;
        this.loadMoreView = null;
        this.empty = null;
        if (this.currentReq != null) {
            this.currentReq.cancel();
            this.currentReq = null;
        }
    }

    private void updateList() {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (BrowseUsersFragment.this.adapter != null) {
                    BrowseUsersFragment.this.adapter.notifyDataSetChanged();
                    BrowseUsersFragment.this.imgLoader.updateImages();
                }
            }
        });
    }

    private boolean loadData(boolean reload) {
        int i = 0;
        Bundle xparams = new Bundle();
        if (this.extParamsView != null) {
            int country = (int) ((Spinner) this.extParamsView.findViewById(C0436R.id.country_spinner)).getSelectedItemId();
            if (country > 0) {
                xparams.putInt("country", country);
                if (this.city > 0) {
                    xparams.putInt("city", this.city);
                }
            }
            if (this.ageFrom > 0) {
                xparams.putInt("age_from", this.ageFrom);
            }
            if (this.ageTo > 0) {
                xparams.putInt("age_to", this.ageTo);
            }
            if (this.extParamsView.findViewById(C0436R.id.gender_male).isSelected()) {
                xparams.putInt("sex", 2);
            } else if (this.extParamsView.findViewById(C0436R.id.gender_female).isSelected()) {
                xparams.putInt("sex", 1);
            }
            if (this.relation > 0) {
                xparams.putInt("status", this.relation);
            }
        }
        if (reload) {
            int hash = this.query.hashCode();
            for (String key : xparams.keySet()) {
                hash += key.hashCode() + xparams.get(key).hashCode();
            }
            if (hash == this.prevHash) {
                return false;
            }
            this.prevHash = hash;
        }
        boolean z = this.query.length() == 0 && xparams.size() == 0;
        this.isEmpty = z;
        this.dataLoading = true;
        String charSequence = this.searchView.getQuery().toString();
        if (!reload) {
            i = this.offset;
        }
        this.currentReq = new UsersSearch(charSequence, xparams, i, this.preloading ? 50 : 100).setCallback(new AnonymousClass17(reload)).exec(getActivity());
        return true;
    }

    public boolean onBackPressed() {
        if (Global.isTablet || this.extParamsView.getVisibility() != 0) {
            return false;
        }
        toggleExtParams();
        return true;
    }
}
