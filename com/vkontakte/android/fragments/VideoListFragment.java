package com.vkontakte.android.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnCloseListener;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.ActivityUtils;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.FragmentWrapperActivity;
import com.vkontakte.android.Global;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.SendActivity;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.VideoAttachment;
import com.vkontakte.android.VideoListView;
import com.vkontakte.android.VideoListView.VideoViewCallback;
import com.vkontakte.android.api.VideoAlbum;
import com.vkontakte.android.api.VideoFile;
import com.vkontakte.android.api.VideoGetAlbums;
import com.vkontakte.android.api.VideoGetAlbums.Callback;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import org.acra.ACRAConstants;

public class VideoListFragment extends SherlockFragment implements VideoViewCallback {
    private static final int COMMENTS_RESULT = 236;
    private static final int VIDEO_EXISTING_RESULT = 235;
    private static final int VIDEO_NEW_RESULT = 234;
    private MenuItem addItem;
    private LinearLayout contentView;
    private LinearLayout filterBar;
    private boolean firstNav;
    private ArrayAdapter<String> navAdapter;
    private int oid;
    private ArrayList<VideoAlbum> playlists;
    private Runnable searchLoader;
    private SearchView searchView;
    private int type;
    private VideoListView view;

    /* renamed from: com.vkontakte.android.fragments.VideoListFragment.1 */
    class C08441 extends ArrayAdapter<String> {
        C08441(Context $anonymous0, int $anonymous1, List $anonymous2) {
            super($anonymous0, $anonymous1, $anonymous2);
        }

        public View getView(int pos, View view, ViewGroup group) {
            return super.getView(VideoListFragment.this.getSherlockActivity().getSupportActionBar().getSelectedNavigationIndex(), view, group);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.VideoListFragment.4 */
    class C08464 implements OnClickListener {
        C08464() {
        }

        public void onClick(View v) {
            VideoListFragment.this.getSherlockActivity().getSupportActionBar().setNavigationMode(0);
            VideoListFragment.this.filterBar.setVisibility(0);
            if (VideoListFragment.this.addItem != null) {
                VideoListFragment.this.addItem.setVisible(false);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.VideoListFragment.6 */
    class C08476 implements OnItemSelectedListener {
        C08476() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            VideoListFragment.this.view.searchLength = pos;
            VideoListFragment.this.loadSearch(VideoListFragment.this.searchView.getQuery().toString());
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.VideoListFragment.7 */
    class C08487 implements OnItemSelectedListener {
        C08487() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            boolean z = true;
            VideoListView access$4 = VideoListFragment.this.view;
            if (pos != 1) {
                z = false;
            }
            access$4.searchHd = z;
            VideoListFragment.this.loadSearch(VideoListFragment.this.searchView.getQuery().toString());
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.VideoListFragment.8 */
    class C08498 implements DialogInterface.OnClickListener {
        C08498() {
        }

        public void onClick(DialogInterface dialog, int which) {
            if (which == 0) {
                Intent intent = new Intent("android.media.action.VIDEO_CAPTURE");
                intent.putExtra("android.intent.extra.sizeLimit", 2147483648L);
                VideoListFragment.this.startActivityForResult(intent, VideoListFragment.VIDEO_NEW_RESULT);
            }
            if (which == 1) {
                intent = new Intent("android.intent.action.PICK");
                intent.setType("video/*");
                VideoListFragment.this.startActivityForResult(intent, VideoListFragment.VIDEO_EXISTING_RESULT);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.VideoListFragment.2 */
    class C15162 implements OnNavigationListener {
        C15162() {
        }

        public boolean onNavigationItemSelected(int pos, long itemId) {
            if (VideoListFragment.this.firstNav) {
                VideoListFragment.this.firstNav = false;
            } else if (pos == 0) {
                VideoListFragment.this.setCurrentPlaylist(0);
            } else if (pos != 1 || VideoListFragment.this.oid <= 0) {
                VideoListFragment.this.type = 0;
                if (VideoListFragment.this.oid > 0) {
                    pos--;
                }
                VideoListFragment.this.setCurrentPlaylist(((VideoAlbum) VideoListFragment.this.playlists.get(pos - 1)).id);
                VideoListFragment.this.getSherlockActivity().invalidateOptionsMenu();
            } else {
                VideoListView access$4 = VideoListFragment.this.view;
                VideoListFragment.this.type = 1;
                access$4.setType(1);
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.VideoListFragment.3 */
    class C15173 implements OnQueryTextListener {

        /* renamed from: com.vkontakte.android.fragments.VideoListFragment.3.1 */
        class C08451 implements Runnable {
            private final /* synthetic */ String val$newText;

            C08451(String str) {
                this.val$newText = str;
            }

            public void run() {
                VideoListFragment.this.searchLoader = null;
                VideoListFragment.this.loadSearch(this.val$newText);
            }
        }

        C15173() {
        }

        public boolean onQueryTextSubmit(String query) {
            if (VideoListFragment.this.searchLoader != null) {
                VideoListFragment.this.view.removeCallbacks(VideoListFragment.this.searchLoader);
                VideoListFragment.this.searchLoader = null;
            }
            VideoListFragment.this.loadSearch(query);
            ((InputMethodManager) VideoListFragment.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(VideoListFragment.this.getActivity().getCurrentFocus().getWindowToken(), 2);
            VideoListFragment.this.getActivity().getCurrentFocus().clearFocus();
            return true;
        }

        public boolean onQueryTextChange(String newText) {
            if (VideoListFragment.this.searchLoader != null) {
                VideoListFragment.this.view.removeCallbacks(VideoListFragment.this.searchLoader);
                VideoListFragment.this.searchLoader = null;
            }
            VideoListFragment.this.searchLoader = new C08451(newText);
            VideoListFragment.this.view.postDelayed(VideoListFragment.this.searchLoader, 1500);
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.VideoListFragment.5 */
    class C15185 implements OnCloseListener {
        C15185() {
        }

        public boolean onClose() {
            VideoListFragment.this.getSherlockActivity().getSupportActionBar().setNavigationMode(1);
            VideoListFragment.this.filterBar.setVisibility(8);
            VideoListFragment.this.view.localSearch(ACRAConstants.DEFAULT_STRING_VALUE);
            if (VideoListFragment.this.addItem != null) {
                VideoListFragment.this.addItem.setVisible(true);
            }
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.fragments.VideoListFragment.9 */
    class C15199 implements Callback {
        C15199() {
        }

        public void success(List<VideoAlbum> albums) {
            VideoListFragment.this.playlists.addAll(albums);
            for (VideoAlbum a : albums) {
                VideoListFragment.this.navAdapter.add(a.title);
            }
        }

        public void fail(int ecode, String emsg) {
        }
    }

    public VideoListFragment() {
        this.playlists = new ArrayList();
        this.firstNav = true;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        this.oid = getArguments().getInt("uid", Global.uid);
        ActivityUtils.setBeamLink(act, "video" + this.oid);
        SherlockFragmentActivity sa = getSherlockActivity();
        sa.getSupportActionBar().setNavigationMode(0);
        sa.getSupportActionBar().setDisplayShowTitleEnabled(true);
        sa.getSupportActionBar().setNavigationMode(1);
        this.navAdapter = new C08441(sa.getSupportActionBar().getThemedContext(), C0436R.layout.nav_spinner_item, new ArrayList());
        this.navAdapter.setDropDownViewResource(C0436R.layout.sherlock_spinner_dropdown_item);
        this.navAdapter.add(getArguments().containsKey(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE) ? getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE).toString() : getString(C0436R.string.videos));
        if (this.oid > 0) {
            Object string;
            ArrayAdapter arrayAdapter = this.navAdapter;
            if (this.oid == Global.uid) {
                string = getString(C0436R.string.videos_of_me);
            } else {
                string = getString(C0436R.string.videos_of_user, getArguments().getCharSequence("username_ins"));
            }
            arrayAdapter.add(string);
        }
        sa.getSupportActionBar().setListNavigationCallbacks(this.navAdapter, new C15162());
        sa.getSupportActionBar().setDisplayShowTitleEnabled(false);
        int i = this.oid;
        int i2 = getArguments().getInt(WebDialog.DIALOG_PARAM_TYPE, 0);
        this.type = i2;
        this.view = new VideoListView(act, i, i2, getArguments().getString("groupName"), getArguments().getString("groupPhoto"), this);
        updateNavItems();
        this.searchView = new SearchView(sa.getSupportActionBar().getThemedContext());
        this.searchView.setQueryHint(getResources().getString(C0436R.string.search));
        try {
            Field searchField = SearchView.class.getDeclaredField("mSearchButton");
            searchField.setAccessible(true);
            ((ImageView) searchField.get(this.searchView)).setImageResource(C0436R.drawable.ic_ab_search);
            searchField = SearchView.class.getDeclaredField("mSearchPlate");
            searchField.setAccessible(true);
            ((TextView) ((LinearLayout) searchField.get(this.searchView)).getChildAt(0)).setHintTextColor(-2130706433);
        } catch (Exception e) {
        }
        this.searchView.setImeOptions(this.searchView.getImeOptions() | 268435456);
        this.searchView.setOnQueryTextListener(new C15173());
        this.searchView.setOnSearchClickListener(new C08464());
        this.searchView.setOnCloseListener(new C15185());
        setHasOptionsMenu(true);
        this.filterBar = new LinearLayout(act);
        this.filterBar.setOrientation(0);
        this.filterBar.setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        Spinner qualitySelector = new Spinner(getActivity());
        if (VERSION.SDK_INT < 14) {
            qualitySelector.setBackgroundResource(C0436R.drawable.abs__spinner_ab_holo_light);
        }
        ArrayAdapter<CharSequence> aa = ArrayAdapter.createFromResource(getActivity(), C0436R.array.video_search_quality, C0436R.layout.sherlock_spinner_item);
        aa.setDropDownViewResource(17367049);
        qualitySelector.setAdapter(aa);
        this.filterBar.addView(qualitySelector);
        Spinner lengthSelector = new Spinner(getActivity());
        if (VERSION.SDK_INT < 14) {
            lengthSelector.setBackgroundResource(C0436R.drawable.abs__spinner_ab_holo_light);
        }
        ArrayAdapter<CharSequence> aal = ArrayAdapter.createFromResource(getActivity(), C0436R.array.video_search_length, C0436R.layout.sherlock_spinner_item);
        aal.setDropDownViewResource(17367049);
        lengthSelector.setAdapter(aal);
        this.filterBar.addView(lengthSelector);
        this.filterBar.setVisibility(8);
        lengthSelector.setOnItemSelectedListener(new C08476());
        qualitySelector.setOnItemSelectedListener(new C08487());
        this.view.selectMode = getArguments().getBoolean("select");
        this.contentView = new LinearLayout(act);
        this.contentView.setOrientation(1);
        this.contentView.addView(this.filterBar);
        this.contentView.addView(this.view, new LayoutParams(-1, -1));
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        MenuItem search = menu.add((int) C0436R.string.search);
        search.setShowAsAction(2);
        search.setActionView(this.searchView);
        if ((this.oid == Global.uid || this.oid == 0) && this.type == 0) {
            MenuItem add = menu.add(0, (int) C0436R.id.create, 0, (int) C0436R.string.add);
            add.setIcon((int) C0436R.drawable.ic_ab_add);
            add.setShowAsAction(2);
            this.addItem = add;
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void onDetach() {
        getSherlockActivity().getSupportActionBar().setListNavigationCallbacks(null, null);
        getSherlockActivity().getSupportActionBar().setNavigationMode(0);
        super.onDetach();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0436R.id.create) {
            showAddVideoBox();
        }
        return true;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentView;
    }

    private void showAddVideoBox() {
        new Builder(getActivity()).setTitle(C0436R.string.share_video_title).setItems(new CharSequence[]{getString(C0436R.string.add_video_new), getString(C0436R.string.add_video_existing)}, new C08498()).show();
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode == COMMENTS_RESULT) {
            if (resCode == 2) {
                this.view.removeItem(data.getIntExtra("oid", 0), data.getIntExtra("pid", 0));
            }
        } else if (resCode == -1) {
            Intent intent = new Intent(getActivity(), SendActivity.class);
            intent.putExtra("android.intent.extra.STREAM", data.getData());
            startActivity(intent);
        }
    }

    private void updateNavItems() {
        new VideoGetAlbums(this.oid).setCallback(new C15199()).exec(getActivity());
    }

    private void setCurrentPlaylist(int pl) {
        this.view.setAlbum(pl);
    }

    private void loadSearch(String q) {
        if (q.length() > 0) {
            this.view.globalSearch(q);
        } else {
            this.view.localSearch(ACRAConstants.DEFAULT_STRING_VALUE);
        }
    }

    public void openComments(VideoFile vf, String name, String photo) {
        NewsEntry e = new NewsEntry();
        e.attachments.add(new VideoAttachment(vf));
        e.text = Global.replaceMentions(vf.descr);
        e.time = vf.date;
        e.postID = vf.vid;
        int i = vf.oid;
        e.ownerID = i;
        e.userID = i;
        e.type = 2;
        e.flags |= 2;
        e.userName = name;
        e.userPhotoURL = photo;
        e.numLikes = vf.likes;
        e.flag(8, vf.liked);
        Bundle args = new Bundle();
        args.putParcelable("entry", e);
        Intent intent = new Intent(getActivity(), FragmentWrapperActivity.class);
        intent.putExtra("class", "PostViewFragment");
        intent.putExtra("args", args);
        startActivityForResult(intent, COMMENTS_RESULT);
    }

    public void showAddDialog() {
        showAddVideoBox();
    }
}
