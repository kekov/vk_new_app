package com.vkontakte.android.fragments;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.ExploreByTouchHelper;
import android.text.ClipboardManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.ActivityUtils;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageCache;
import com.vkontakte.android.ImageCache.ProgressCallback;
import com.vkontakte.android.ImageCache.RequestWrapper;
import com.vkontakte.android.Log;
import com.vkontakte.android.Navigate;
import com.vkontakte.android.NetworkStateReceiver;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.Photo;
import com.vkontakte.android.Photo.Image;
import com.vkontakte.android.PhotoAttachment;
import com.vkontakte.android.PhotoTag;
import com.vkontakte.android.PhotoViewerActivity;
import com.vkontakte.android.ReportContentActivity;
import com.vkontakte.android.RepostActivity;
import com.vkontakte.android.UploaderService;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.FaveGetPhotos;
import com.vkontakte.android.api.GetFullPhotoList;
import com.vkontakte.android.api.PhotosCopy;
import com.vkontakte.android.api.PhotosDelete;
import com.vkontakte.android.api.PhotosGet;
import com.vkontakte.android.api.PhotosGetAll;
import com.vkontakte.android.api.PhotosGetInfo;
import com.vkontakte.android.api.PhotosGetInfo.Callback;
import com.vkontakte.android.api.PhotosGetTags;
import com.vkontakte.android.api.PhotosGetUserPhotos;
import com.vkontakte.android.api.WallLike;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Friends.GetUsersCallback;
import com.vkontakte.android.mediapicker.gl.GLFilterContext;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import com.vkontakte.android.ui.PhotoView;
import com.vkontakte.android.ui.PhotoView.DismissListener;
import com.vkontakte.android.ui.PhotoView.NavigationListener;
import com.vkontakte.android.ui.PhotoView.PhotoViewerAdapter;
import com.vkontakte.android.ui.PhotoView.RunnableFuture;
import com.vkontakte.android.ui.PhotoView.ZoomListener;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.acra.ACRAConstants;

public class PhotoViewerFragment extends SherlockFragment implements NavigationListener, DismissListener, ZoomListener {
    public static ArrayList<Photo> sharedList;
    public static Bitmap sharedThumb;
    private int aid;
    private int allUid;
    private int aoid;
    private View buttonBar;
    private FrameLayout contentView;
    private Photo curPhoto;
    private boolean dataLoading;
    private boolean descrVisible;
    private boolean dismissed;
    private String docTitle;
    private String docUrl;
    private boolean liking;
    private boolean loaded;
    private int maxLikePhotos;
    private int maxLikeVisible;
    private boolean photoLoaded;
    private PhotoView photoView;
    private ArrayList<Photo> photos;
    private String title;
    private int total;
    boolean useBig;
    boolean useSmall;

    /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.2 */
    class C07532 implements Runnable {
        C07532() {
        }

        public void run() {
            PhotoViewerFragment.this.showControls(true, true);
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.3 */
    class C07543 implements OnClickListener {
        C07543() {
        }

        public void onClick(View v) {
            if (PhotoViewerFragment.this.curPhoto.albumID != ExploreByTouchHelper.INVALID_ID) {
                NewsEntry e = new NewsEntry();
                e.type = 1;
                e.postID = PhotoViewerFragment.this.curPhoto.id;
                e.ownerID = PhotoViewerFragment.this.curPhoto.ownerID;
                e.userID = PhotoViewerFragment.this.curPhoto.userID;
                e.attachments.add(new PhotoAttachment(PhotoViewerFragment.this.curPhoto));
                e.text = Global.replaceMentions(PhotoViewerFragment.this.curPhoto.descr);
                e.time = PhotoViewerFragment.this.curPhoto.date;
                e.numLikes = PhotoViewerFragment.this.curPhoto.nLikes;
                e.numComments = PhotoViewerFragment.this.curPhoto.nComments;
                if (PhotoViewerFragment.this.curPhoto.user != null) {
                    e.userName = PhotoViewerFragment.this.curPhoto.user.fullName;
                    e.userPhotoURL = PhotoViewerFragment.this.curPhoto.user.photo;
                }
                e.flag(8, PhotoViewerFragment.this.curPhoto.isLiked);
                e.flag(2, PhotoViewerFragment.this.curPhoto.canComment);
                Bundle args = new Bundle();
                args.putParcelable("entry", e);
                args.putBoolean("photo_viewer", true);
                Navigate.to("PostViewFragment", args, PhotoViewerFragment.this.getActivity());
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.4 */
    class C07554 implements OnClickListener {
        C07554() {
        }

        public void onClick(View v) {
            if (PhotoViewerFragment.this.curPhoto.albumID != ExploreByTouchHelper.INVALID_ID) {
                PhotoViewerFragment.this.like(!PhotoViewerFragment.this.curPhoto.isLiked);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.5 */
    class C07565 implements OnClickListener {
        C07565() {
        }

        public void onClick(View v) {
            if (PhotoViewerFragment.this.curPhoto.albumID != ExploreByTouchHelper.INVALID_ID) {
                PhotoViewerFragment.this.showTags();
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.6 */
    class C07576 implements OnClickListener {
        C07576() {
        }

        public void onClick(View v) {
            if (PhotoViewerFragment.this.getSherlockActivity() != null) {
                PhotoViewerFragment.this.showControls(!PhotoViewerFragment.this.getSherlockActivity().getSupportActionBar().isShowing(), true);
            }
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.7 */
    class C07587 implements DialogInterface.OnClickListener {
        C07587() {
        }

        public void onClick(DialogInterface dialog, int which) {
            PhotoViewerFragment.this.deletePhoto();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.13 */
    class AnonymousClass13 implements Callback {
        private final /* synthetic */ Photo val$p;

        AnonymousClass13(Photo photo) {
            this.val$p = photo;
        }

        public void success(int likes, int comments, int tags, boolean liked, boolean canComment) {
            Log.m528i("vk", "Can comment " + canComment);
            this.val$p.nLikes = likes;
            this.val$p.nComments = comments;
            this.val$p.nTags = tags;
            this.val$p.isLiked = liked;
            this.val$p.canComment = canComment;
            this.val$p.infoLoaded = true;
            PhotoViewerFragment.this.updateBottomBar();
        }

        public void fail(int ecode, String emsg) {
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.14 */
    class AnonymousClass14 implements WallLike.Callback {
        private final /* synthetic */ boolean val$liked;

        /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.14.1 */
        class C07501 implements Runnable {
            C07501() {
            }

            public void run() {
                PhotoViewerFragment.this.like(PhotoViewerFragment.this.curPhoto.isLiked);
            }
        }

        /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.14.2 */
        class C07512 implements Runnable {
            C07512() {
            }

            public void run() {
                PhotoViewerFragment.this.updateBottomBar();
            }
        }

        /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.14.3 */
        class C07523 implements Runnable {
            C07523() {
            }

            public void run() {
                Toast.makeText(PhotoViewerFragment.this.getActivity(), C0436R.string.error, 0).show();
                PhotoViewerFragment.this.updateBottomBar();
            }
        }

        AnonymousClass14(boolean z) {
            this.val$liked = z;
        }

        public void success(int likes, int retweets, int postID) {
            PhotoViewerFragment.this.curPhoto.nLikes = likes;
            PhotoViewerFragment.this.liking = false;
            if (PhotoViewerFragment.this.curPhoto.isLiked != this.val$liked) {
                PhotoViewerFragment.this.contentView.post(new C07501());
            } else {
                PhotoViewerFragment.this.contentView.post(new C07512());
            }
        }

        public void fail(int ecode, String emsg) {
            PhotoViewerFragment.this.curPhoto.isLiked = !this.val$liked;
            Photo access$9;
            if (this.val$liked) {
                access$9 = PhotoViewerFragment.this.curPhoto;
                access$9.nLikes--;
            } else {
                access$9 = PhotoViewerFragment.this.curPhoto;
                access$9.nLikes++;
            }
            PhotoViewerFragment.this.liking = false;
            PhotoViewerFragment.this.contentView.post(new C07523());
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.1 */
    class C15011 implements GetFullPhotoList.Callback {
        C15011() {
        }

        public void success(ArrayList<Photo> photos) {
            ArrayList<Photo> oldPhotos = PhotoViewerFragment.this.photos;
            Iterator it = oldPhotos.iterator();
            while (it.hasNext()) {
                Photo op = (Photo) it.next();
                Iterator it2 = photos.iterator();
                while (it2.hasNext()) {
                    Photo p = (Photo) it2.next();
                    if (p.id == op.id && p.ownerID == op.ownerID) {
                        photos.remove(p);
                        break;
                    }
                }
            }
            photos.addAll(0, oldPhotos);
            int pos = PhotoViewerFragment.this.getArguments().getInt(GLFilterContext.AttributePosition, 0);
            PhotoViewerFragment.sharedThumb = null;
            PhotoViewerFragment.this.photos = photos;
            PhotoViewerFragment.this.onPositionChanged(pos);
            PhotoViewerFragment.this.showControls(true, true);
            PhotoViewerFragment.this.loadUsers();
            PhotoViewerFragment.this.loaded = true;
            PhotoViewerFragment.this.photoView.setEnabled(true);
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PhotoViewerFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
            ((PhotoViewerActivity) PhotoViewerFragment.this.getActivity()).forceFinish();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.8 */
    class C15028 implements PhotosCopy.Callback {
        C15028() {
        }

        public void success(int id) {
            Toast.makeText(PhotoViewerFragment.this.getActivity(), C0436R.string.saved_to_album, 0).show();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PhotoViewerFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.9 */
    class C15039 implements PhotosDelete.Callback {
        C15039() {
        }

        public void success() {
            Intent intent = new Intent(UploaderService.ACTION_PHOTO_REMOVED);
            intent.putExtra("aid", PhotoViewerFragment.this.curPhoto.albumID);
            intent.putExtra("pid", PhotoViewerFragment.this.curPhoto.id);
            PhotoViewerFragment.this.getActivity().sendBroadcast(intent);
            PhotoViewerFragment.this.getActivity().finish();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(PhotoViewerFragment.this.getActivity(), ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    private class DocPhotoLoader implements RunnableFuture<Bitmap> {
        private Bitmap bmp;
        private boolean canceled;
        InputStream in;
        private Photo photo;
        private ProgressCallback progr;

        /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.DocPhotoLoader.1 */
        class C07591 implements Runnable {
            C07591() {
            }

            public void run() {
                if (PhotoViewerFragment.this.getSherlockActivity() != null) {
                    PhotoViewerFragment.this.getSherlockActivity().invalidateOptionsMenu();
                }
            }
        }

        /* renamed from: com.vkontakte.android.fragments.PhotoViewerFragment.DocPhotoLoader.2 */
        class C07602 implements Runnable {
            C07602() {
            }

            public void run() {
                if (PhotoViewerFragment.this.photoView != null) {
                    PhotoViewerFragment.this.photoView.setErrorText(PhotoViewerFragment.this.getString(C0436R.string.no_sd_card));
                }
            }
        }

        public DocPhotoLoader() {
            this.canceled = false;
            this.in = null;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r20 = this;
            r17 = 0;
            r0 = r17;
            r1 = r20;
            r1.canceled = r0;
            r11 = 0;
            r15 = new java.io.File;	 Catch:{ Exception -> 0x027f }
            r17 = android.os.Environment.getExternalStorageDirectory();	 Catch:{ Exception -> 0x027f }
            r18 = ".vkontakte";
            r0 = r17;
            r1 = r18;
            r15.<init>(r0, r1);	 Catch:{ Exception -> 0x027f }
            r17 = r15.exists();	 Catch:{ Exception -> 0x027f }
            if (r17 != 0) goto L_0x0021;
        L_0x001e:
            r15.mkdirs();	 Catch:{ Exception -> 0x027f }
        L_0x0021:
            r6 = new java.io.File;	 Catch:{ Exception -> 0x027f }
            r17 = android.os.Environment.getExternalStorageDirectory();	 Catch:{ Exception -> 0x027f }
            r18 = ".vkontakte/temp_doc";
            r0 = r17;
            r1 = r18;
            r6.<init>(r0, r1);	 Catch:{ Exception -> 0x027f }
            r6.createNewFile();	 Catch:{ Exception -> 0x027f }
            r12 = new java.io.FileOutputStream;	 Catch:{ Exception -> 0x027f }
            r12.<init>(r6);	 Catch:{ Exception -> 0x027f }
            r14 = new java.net.URL;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r20;
            r0 = com.vkontakte.android.fragments.PhotoViewerFragment.this;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r17 = r17.docUrl;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r17;
            r14.<init>(r0);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r4 = r14.openConnection();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r4 = (java.net.HttpURLConnection) r4;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r4.connect();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r4.getInputStream();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r17;
            r1 = r20;
            r1.in = r0;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r7 = r4.getContentLength();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = "vk";
            r18 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r19 = "url=";
            r18.<init>(r19);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r18;
            r18 = r0.append(r14);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r18 = r18.toString();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            com.vkontakte.android.Log.m525d(r17, r18);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = "vk";
            r18 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r19 = "Len=";
            r18.<init>(r19);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r18;
            r18 = r0.append(r7);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r18 = r18.toString();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            com.vkontakte.android.Log.m525d(r17, r18);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r20;
            r0 = r0.progr;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            if (r17 == 0) goto L_0x00a3;
        L_0x0094:
            r0 = r20;
            r0 = r0.progr;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r18 = 0;
            r0 = r17;
            r1 = r18;
            r0.onProgressChanged(r1, r7);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
        L_0x00a3:
            r13 = 0;
            r9 = 0;
            r3 = 0;
            r17 = 10240; // 0x2800 float:1.4349E-41 double:5.059E-320;
            r0 = r17;
            r2 = new byte[r0];	 Catch:{ Exception -> 0x0146, all -> 0x027c }
        L_0x00ac:
            r0 = r20;
            r0 = r0.in;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r0 = r17;
            r13 = r0.read(r2);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            if (r13 > 0) goto L_0x0123;
        L_0x00ba:
            r0 = r20;
            r0 = r0.in;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r17.close();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r12.close();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r4.disconnect();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = "vk";
            r18 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r19 = "Downloaded ";
            r18.<init>(r19);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r18;
            r18 = r0.append(r9);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r19 = " / ";
            r18 = r18.append(r19);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r18;
            r18 = r0.append(r7);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r18 = r18.toString();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            com.vkontakte.android.Log.m525d(r17, r18);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r10 = new android.graphics.BitmapFactory$Options;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r10.<init>();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = 1;
            r0 = r17;
            r10.inJustDecodeBounds = r0;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r6.getAbsolutePath();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r17;
            android.graphics.BitmapFactory.decodeFile(r0, r10);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r10.outWidth;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            if (r17 <= 0) goto L_0x010b;
        L_0x0105:
            r0 = r10.outHeight;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            if (r17 > 0) goto L_0x0198;
        L_0x010b:
            if (r12 == 0) goto L_0x0110;
        L_0x010d:
            r12.close();	 Catch:{ Exception -> 0x0270 }
        L_0x0110:
            r0 = r20;
            r0 = r0.in;	 Catch:{ Exception -> 0x0282 }
            r17 = r0;
            if (r17 == 0) goto L_0x0121;
        L_0x0118:
            r0 = r20;
            r0 = r0.in;	 Catch:{ Exception -> 0x0282 }
            r17 = r0;
            r17.close();	 Catch:{ Exception -> 0x0282 }
        L_0x0121:
            r11 = r12;
        L_0x0122:
            return;
        L_0x0123:
            r17 = 0;
            r0 = r17;
            r12.write(r2, r0, r13);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r9 = r9 + r13;
            r3 = r3 + 1;
            r17 = r3 % 5;
            if (r17 != 0) goto L_0x00ac;
        L_0x0131:
            r0 = r20;
            r0 = r0.progr;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            if (r17 == 0) goto L_0x00ac;
        L_0x0139:
            r0 = r20;
            r0 = r0.progr;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r0 = r17;
            r0.onProgressChanged(r9, r7);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            goto L_0x00ac;
        L_0x0146:
            r16 = move-exception;
            r11 = r12;
        L_0x0148:
            r17 = "vk";
            r0 = r17;
            r1 = r16;
            com.vkontakte.android.Log.m532w(r0, r1);	 Catch:{ all -> 0x0254 }
            r17 = "mounted";
            r18 = android.os.Environment.getExternalStorageState();	 Catch:{ all -> 0x0254 }
            r17 = r17.equals(r18);	 Catch:{ all -> 0x0254 }
            if (r17 != 0) goto L_0x017f;
        L_0x015d:
            r0 = r20;
            r0 = com.vkontakte.android.fragments.PhotoViewerFragment.this;	 Catch:{ all -> 0x0254 }
            r17 = r0;
            r17 = r17.getActivity();	 Catch:{ all -> 0x0254 }
            if (r17 == 0) goto L_0x017f;
        L_0x0169:
            r0 = r20;
            r0 = com.vkontakte.android.fragments.PhotoViewerFragment.this;	 Catch:{ all -> 0x0254 }
            r17 = r0;
            r17 = r17.getActivity();	 Catch:{ all -> 0x0254 }
            r18 = new com.vkontakte.android.fragments.PhotoViewerFragment$DocPhotoLoader$2;	 Catch:{ all -> 0x0254 }
            r0 = r18;
            r1 = r20;
            r0.<init>();	 Catch:{ all -> 0x0254 }
            r17.runOnUiThread(r18);	 Catch:{ all -> 0x0254 }
        L_0x017f:
            if (r11 == 0) goto L_0x0184;
        L_0x0181:
            r11.close();	 Catch:{ Exception -> 0x0273 }
        L_0x0184:
            r0 = r20;
            r0 = r0.in;	 Catch:{ Exception -> 0x0196 }
            r17 = r0;
            if (r17 == 0) goto L_0x0122;
        L_0x018c:
            r0 = r20;
            r0 = r0.in;	 Catch:{ Exception -> 0x0196 }
            r17 = r0;
            r17.close();	 Catch:{ Exception -> 0x0196 }
            goto L_0x0122;
        L_0x0196:
            r17 = move-exception;
            goto L_0x0122;
        L_0x0198:
            r0 = r20;
            r0 = com.vkontakte.android.fragments.PhotoViewerFragment.this;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r17 = r17.getResources();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r5 = r17.getDisplayMetrics();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r20;
            r0 = com.vkontakte.android.fragments.PhotoViewerFragment.this;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r0 = r17;
            r0 = r0.useSmall;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            if (r17 == 0) goto L_0x0250;
        L_0x01b4:
            r17 = 512; // 0x200 float:7.175E-43 double:2.53E-321;
        L_0x01b6:
            r0 = r5.widthPixels;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r18 = r0;
            r0 = r5.heightPixels;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r19 = r0;
            r18 = java.lang.Math.max(r18, r19);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r8 = java.lang.Math.max(r17, r18);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = 0;
            r0 = r17;
            r10.inJustDecodeBounds = r0;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r10.outWidth;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r0 = r17;
            if (r0 > r8) goto L_0x01dc;
        L_0x01d4:
            r0 = r10.outHeight;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r0 = r17;
            if (r0 <= r8) goto L_0x01fa;
        L_0x01dc:
            r0 = r10.outWidth;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r0 = r10.outHeight;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r18 = r0;
            r17 = java.lang.Math.max(r17, r18);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r17;
            r0 = (float) r0;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r0 = (float) r8;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r18 = r0;
            r17 = r17 / r18;
            r17 = java.lang.Math.round(r17);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r17;
            r10.inSampleSize = r0;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
        L_0x01fa:
            r17 = r6.getAbsolutePath();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r17;
            r17 = android.graphics.BitmapFactory.decodeFile(r0, r10);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r17;
            r1 = r20;
            r1.bmp = r0;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r20;
            r0 = com.vkontakte.android.fragments.PhotoViewerFragment.this;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r18 = 1;
            r17.photoLoaded = r18;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r20;
            r0 = com.vkontakte.android.fragments.PhotoViewerFragment.this;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r17 = r17.getActivity();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            if (r17 == 0) goto L_0x0237;
        L_0x0221:
            r0 = r20;
            r0 = com.vkontakte.android.fragments.PhotoViewerFragment.this;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17 = r0;
            r17 = r17.getActivity();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r18 = new com.vkontakte.android.fragments.PhotoViewerFragment$DocPhotoLoader$1;	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r0 = r18;
            r1 = r20;
            r0.<init>();	 Catch:{ Exception -> 0x0146, all -> 0x027c }
            r17.runOnUiThread(r18);	 Catch:{ Exception -> 0x0146, all -> 0x027c }
        L_0x0237:
            if (r12 == 0) goto L_0x023c;
        L_0x0239:
            r12.close();	 Catch:{ Exception -> 0x0278 }
        L_0x023c:
            r0 = r20;
            r0 = r0.in;	 Catch:{ Exception -> 0x026c }
            r17 = r0;
            if (r17 == 0) goto L_0x0285;
        L_0x0244:
            r0 = r20;
            r0 = r0.in;	 Catch:{ Exception -> 0x026c }
            r17 = r0;
            r17.close();	 Catch:{ Exception -> 0x026c }
            r11 = r12;
            goto L_0x0122;
        L_0x0250:
            r17 = 1024; // 0x400 float:1.435E-42 double:5.06E-321;
            goto L_0x01b6;
        L_0x0254:
            r17 = move-exception;
        L_0x0255:
            if (r11 == 0) goto L_0x025a;
        L_0x0257:
            r11.close();	 Catch:{ Exception -> 0x0276 }
        L_0x025a:
            r0 = r20;
            r0 = r0.in;	 Catch:{ Exception -> 0x027a }
            r18 = r0;
            if (r18 == 0) goto L_0x026b;
        L_0x0262:
            r0 = r20;
            r0 = r0.in;	 Catch:{ Exception -> 0x027a }
            r18 = r0;
            r18.close();	 Catch:{ Exception -> 0x027a }
        L_0x026b:
            throw r17;
        L_0x026c:
            r17 = move-exception;
            r11 = r12;
            goto L_0x0122;
        L_0x0270:
            r17 = move-exception;
            goto L_0x0110;
        L_0x0273:
            r17 = move-exception;
            goto L_0x0184;
        L_0x0276:
            r18 = move-exception;
            goto L_0x025a;
        L_0x0278:
            r17 = move-exception;
            goto L_0x023c;
        L_0x027a:
            r18 = move-exception;
            goto L_0x026b;
        L_0x027c:
            r17 = move-exception;
            r11 = r12;
            goto L_0x0255;
        L_0x027f:
            r16 = move-exception;
            goto L_0x0148;
        L_0x0282:
            r17 = move-exception;
            goto L_0x0121;
        L_0x0285:
            r11 = r12;
            goto L_0x0122;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.fragments.PhotoViewerFragment.DocPhotoLoader.run():void");
        }

        public boolean cancel(boolean mayInterruptIfRunning) {
            if (this.in != null) {
                try {
                    this.in.close();
                } catch (Exception e) {
                }
                this.canceled = true;
            }
            return true;
        }

        public Bitmap get() throws InterruptedException, ExecutionException {
            return this.bmp;
        }

        public Bitmap get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return null;
        }

        public boolean isCancelled() {
            return this.canceled;
        }

        public boolean isDone() {
            return this.bmp != null;
        }

        public void setProgressCallback(ProgressCallback c) {
            this.progr = c;
        }
    }

    private class PhotoAdapter implements PhotoViewerAdapter {
        private PhotoAdapter() {
        }

        public int getCount() {
            if (PhotoViewerFragment.this.docUrl != null) {
                return 1;
            }
            return PhotoViewerFragment.this.photos.size();
        }

        public boolean isPhotoLoaded(int num) {
            return false;
        }

        public RunnableFuture<Bitmap> getPhoto(int num) {
            if (PhotoViewerFragment.this.docUrl != null) {
                return new DocPhotoLoader();
            }
            return new PhotoLoader((Photo) PhotoViewerFragment.this.photos.get(num));
        }

        public void requestPhotoDownload(int num) {
        }

        public float getPhotoLoadProgress(int num) {
            return 0.0f;
        }

        public boolean shouldPreload() {
            return !NetworkStateReceiver.isMobile();
        }

        public boolean allowZoom(int num) {
            return true;
        }

        public Bitmap getThumb(int num) {
            if (!PhotoViewerFragment.this.loaded) {
                return PhotoViewerFragment.sharedThumb;
            }
            if (PhotoViewerFragment.this.docUrl != null || num < 0 || num >= getCount()) {
                return null;
            }
            Iterator it = ((Photo) PhotoViewerFragment.this.photos.get(num)).sizes.iterator();
            while (it.hasNext()) {
                Image im = (Image) it.next();
                if (ImageCache.isInCache(im.url)) {
                    return ImageCache.get(im.url);
                }
            }
            return null;
        }

        public boolean isCached(int num) {
            if (PhotoViewerFragment.this.docUrl != null || num < 0 || num >= PhotoViewerFragment.this.photos.size()) {
                return false;
            }
            Photo photo = (Photo) PhotoViewerFragment.this.photos.get(num);
            return ImageCache.isInCache(PhotoViewerFragment.this.useSmall ? photo.getImage('x').url : photo.getImage('y', 'x').url);
        }
    }

    private class PhotoLoader implements RunnableFuture<Bitmap> {
        private Bitmap bmp;
        private boolean canceled;
        private Photo photo;
        private ProgressCallback progr;
        private RequestWrapper wrapper;

        public PhotoLoader(Photo p) {
            this.wrapper = new RequestWrapper();
            this.canceled = false;
            this.photo = p;
        }

        public void run() {
            this.canceled = false;
            String url = PhotoViewerFragment.this.useSmall ? this.photo.getImage('x').url : PhotoViewerFragment.this.useBig ? this.photo.getImage(new char[]{'z', 'y', 'x'}).url : this.photo.getImage('y', 'x').url;
            this.bmp = ImageCache.get(url, this.wrapper, this.progr, true);
        }

        public boolean cancel(boolean mayInterruptIfRunning) {
            if (!(this.wrapper == null || this.wrapper.request == null)) {
                this.wrapper.request.abort();
                this.canceled = true;
            }
            return true;
        }

        public Bitmap get() throws InterruptedException, ExecutionException {
            return this.bmp;
        }

        public Bitmap get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return null;
        }

        public boolean isCancelled() {
            return this.canceled;
        }

        public boolean isDone() {
            return this.bmp != null;
        }

        public void setProgressCallback(ProgressCallback c) {
            this.progr = c;
        }
    }

    public PhotoViewerFragment() {
        boolean z = true;
        this.photos = new ArrayList();
        this.total = 0;
        this.allUid = 0;
        this.aid = 0;
        this.aoid = 0;
        this.dataLoading = false;
        this.loaded = false;
        this.descrVisible = true;
        this.useSmall = ((ActivityManager) VKApplication.context.getSystemService("activity")).getMemoryClass() < 20;
        if (((ActivityManager) VKApplication.context.getSystemService("activity")).getMemoryClass() < 100 || !(Global.displayDensity > ImageViewHolder.PaddingSize || (VKApplication.context.getResources().getConfiguration().screenLayout & 15) == 3 || (VKApplication.context.getResources().getConfiguration().screenLayout & 15) == 4)) {
            z = false;
        }
        this.useBig = z;
        this.dismissed = false;
        this.photoLoaded = false;
    }

    static {
        sharedThumb = null;
    }

    public void onAttach(Activity act) {
        super.onAttach(act);
        getSherlockActivity().getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(C0436R.drawable.bg_actionbar_grey_transparent));
        if (VERSION.SDK_INT < 14) {
            act.getWindow().addFlags(GLRenderBuffer.EGL_SURFACE_SIZE);
        }
        if (getArguments().containsKey(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)) {
            this.title = getArguments().getCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE).toString();
            act.setTitle(this.title);
        }
        if (getArguments().containsKey("all_uid")) {
            this.allUid = getArguments().getInt("all_uid");
            this.total = getArguments().getInt("total");
        }
        if (getArguments().containsKey("aid")) {
            this.aid = getArguments().getInt("aid");
            this.aoid = getArguments().getInt("oid");
            this.total = getArguments().getInt("total");
            if (this.aid == 0) {
                this.allUid = this.aoid;
            }
        }
        if (getArguments().containsKey("list") && !getArguments().containsKey("feed_entry")) {
            this.photos = getArguments().getParcelableArrayList("list");
            this.curPhoto = (Photo) this.photos.get(getArguments().getInt(GLFilterContext.AttributePosition, 0));
            this.loaded = true;
        } else if (getArguments().containsKey("shared_list")) {
            this.photos.addAll(sharedList);
            this.curPhoto = (Photo) this.photos.get(getArguments().getInt(GLFilterContext.AttributePosition, 0));
            this.loaded = true;
        } else if (getArguments().containsKey("doc_url")) {
            this.docUrl = getArguments().getString("doc_url");
            this.docTitle = getArguments().getString("doc_title");
            getActivity().setTitle(this.docTitle);
            this.curPhoto = new Photo();
            this.loaded = true;
        } else {
            this.photos = getArguments().getParcelableArrayList("list");
            this.curPhoto = (Photo) this.photos.get(getArguments().getInt(GLFilterContext.AttributePosition, 0));
            new GetFullPhotoList((NewsEntry) getArguments().getParcelable("feed_entry")).setCallback(new C15011()).exec(act);
        }
        this.buttonBar = View.inflate(act, C0436R.layout.photo_viewer_bottom, null);
        this.contentView = new FrameLayout(act);
        this.photoView = new PhotoView(act);
        this.contentView.addView(this.photoView);
        this.contentView.addView(this.photoView.getOverlayView());
        this.contentView.addView(this.buttonBar, new LayoutParams(-1, -2, 80));
        this.photoView.setNavigationListener(this);
        this.photoView.setDismissListener(this);
        this.photoView.setZoomListener(this);
        if (this.photos.size() > 0) {
            Photo photo = (Photo) this.photos.get(getArguments().getInt(GLFilterContext.AttributePosition, 0));
            this.photoView.setThumb(sharedThumb, !ImageCache.isInCache(this.useSmall ? photo.getImage('x').url : photo.getImage('y', 'x').url));
        } else {
            this.photoView.setThumb(sharedThumb, true);
        }
        if (!(this.curPhoto == null || this.curPhoto.viewBounds == null || VERSION.SDK_INT < 14)) {
            getActivity().overridePendingTransition(0, 0);
            this.photoView.animateIn(this.curPhoto.viewBounds, this.curPhoto.viewClipTop);
        }
        this.photoView.setAdapter(new PhotoAdapter());
        if (((getArguments().containsKey("list") || getArguments().containsKey("shared_list")) && !getArguments().containsKey("feed_entry")) || this.docUrl != null) {
            this.photoView.setPosition(getArguments().getInt(GLFilterContext.AttributePosition, 0));
            onPositionChanged(getArguments().getInt(GLFilterContext.AttributePosition, 0));
            sharedThumb = null;
            showControls(false, false);
            this.photoView.postDelayed(new C07532(), 200);
        } else {
            showControls(false, false);
            this.photoView.setEnabled(false);
            this.photoView.setPosition(getArguments().getInt(GLFilterContext.AttributePosition, 0));
        }
        loadUsers();
        this.buttonBar.findViewById(C0436R.id.photo_viewer_comments).setOnClickListener(new C07543());
        this.buttonBar.findViewById(C0436R.id.photo_viewer_likes).setOnClickListener(new C07554());
        this.buttonBar.findViewById(C0436R.id.photo_viewer_tags).setOnClickListener(new C07565());
        if (this.curPhoto.id == 0) {
            this.buttonBar.setVisibility(8);
        }
        this.contentView.setFocusable(true);
        this.contentView.setFocusableInTouchMode(true);
        this.contentView.requestFocus();
        this.photoView.setOnClickListener(new C07576());
        this.photoView.setBgColor(getArguments().getInt("bg_color", -1));
        setHasOptionsMenu(true);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        boolean z = true;
        inflater.inflate(C0436R.menu.photo_viewer, menu);
        MenuItem findItem = menu.findItem(C0436R.id.delete);
        boolean z2 = this.curPhoto != null && this.curPhoto.ownerID == Global.uid;
        findItem.setVisible(z2);
        findItem = menu.findItem(C0436R.id.save_to_album);
        if (this.curPhoto == null || this.curPhoto.ownerID == Global.uid || this.curPhoto.id == 0) {
            z2 = false;
        } else {
            z2 = true;
        }
        findItem.setVisible(z2);
        findItem = menu.findItem(C0436R.id.copy_link);
        if (this.curPhoto.id != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        findItem.setVisible(z2);
        findItem = menu.findItem(C0436R.id.send_to_friend);
        if (this.curPhoto.id != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        findItem.setVisible(z2);
        findItem = menu.findItem(C0436R.id.go_to_album);
        if (this.curPhoto.albumID > 0 || this.curPhoto.albumID == -6 || this.curPhoto.albumID == -7 || this.curPhoto.albumID == -15) {
            z2 = true;
        } else {
            z2 = false;
        }
        findItem.setVisible(z2);
        MenuItem findItem2 = menu.findItem(C0436R.id.report);
        if (this.curPhoto.ownerID == Global.uid || this.curPhoto.id == 0) {
            z = false;
        }
        findItem2.setVisible(z);
        if (this.docUrl != null) {
            menu.findItem(C0436R.id.save).setEnabled(this.photoLoaded);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0436R.id.save) {
            savePhoto();
        }
        if (item.getItemId() == C0436R.id.delete) {
            new Builder(getActivity()).setTitle(C0436R.string.confirm).setMessage(C0436R.string.delete_photo_confirm).setPositiveButton(C0436R.string.yes, new C07587()).setNegativeButton(C0436R.string.no, null).show();
        }
        if (item.getItemId() == C0436R.id.copy_link) {
            ((ClipboardManager) getActivity().getSystemService("clipboard")).setText("http://vk.com/photo" + this.curPhoto.ownerID + "_" + this.curPhoto.id);
            Toast.makeText(getActivity(), C0436R.string.link_copied, 0).show();
        }
        if (item.getItemId() == C0436R.id.send_to_friend) {
            NewsEntry e = new NewsEntry();
            e.attachments = new ArrayList();
            e.attachments.add(new PhotoAttachment(this.curPhoto));
            e.type = 1;
            Intent intent = new Intent(getActivity(), RepostActivity.class);
            intent.putExtra("post", e);
            intent.putExtra("msg", true);
            startActivity(intent);
        }
        if (item.getItemId() == C0436R.id.save_to_album) {
            new PhotosCopy(this.curPhoto.ownerID, this.curPhoto.id, this.curPhoto.accessKey).setCallback(new C15028()).wrapProgress(getActivity()).exec(this.contentView);
        }
        if (item.getItemId() == C0436R.id.go_to_album) {
            if (getArguments().getInt("from_album", 0) == this.curPhoto.albumID && getArguments().getInt("from_album_oid", 0) == this.curPhoto.ownerID) {
                getActivity().finish();
            } else {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("vkontakte://vk.com/album" + this.curPhoto.ownerID + "_" + this.curPhoto.albumID)));
            }
        }
        if (item.getItemId() == C0436R.id.report) {
            intent = new Intent(getActivity(), ReportContentActivity.class);
            intent.putExtra("itemID", this.curPhoto.id);
            intent.putExtra("ownerID", this.curPhoto.ownerID);
            intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, "photo");
            startActivity(intent);
        }
        return true;
    }

    private void deletePhoto() {
        new PhotosDelete(this.curPhoto.ownerID, this.curPhoto.id).setCallback(new C15039()).wrapProgress(getActivity()).exec(getActivity());
    }

    private void savePhoto() {
        if (this.docUrl != null) {
            File file = new File(Environment.getExternalStorageDirectory(), ".vkontakte/temp_doc");
            if (file.exists()) {
                file.renameTo(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), this.docTitle));
                Toast.makeText(getActivity(), getString(C0436R.string.file_saved, path.getAbsoluteFile()), 0).show();
                return;
            }
            return;
        }
        File dir = new File(Environment.getExternalStorageDirectory(), "VK");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String url;
        if (VERSION.SDK_INT >= 11) {
            if (this.curPhoto.getImage('w') != null) {
                url = this.curPhoto.getImage('w').url;
            } else if (this.curPhoto.getImage('z') != null) {
                url = this.curPhoto.getImage('z').url;
            } else if (this.curPhoto.getImage('y') != null) {
                url = this.curPhoto.getImage('y').url;
            } else {
                url = this.curPhoto.getImage('x').url;
            }
            if (url != null) {
                Uri uri = Uri.parse(url);
                Request req = new Request(uri);
                req.setDestinationUri(Uri.fromFile(new File(dir, uri.getLastPathSegment())));
                if (VERSION.SDK_INT >= 14) {
                    req.setNotificationVisibility(1);
                    req.allowScanningByMediaScanner();
                }
                ((DownloadManager) getActivity().getSystemService("download")).enqueue(req);
                return;
            }
            return;
        }
        url = this.curPhoto.getImage('y', 'x').url;
        ImageCache.save(url, new File(dir, Uri.parse(url).getLastPathSegment()).getAbsolutePath());
        Toast.makeText(getActivity(), C0436R.string.photo_save_ok, 0).show();
        MediaScannerConnection.scanFile(getActivity(), new String[]{new File(dir, Uri.parse(url).getLastPathSegment()).getAbsolutePath()}, new String[]{"image/jpeg"}, null);
    }

    private void showTags() {
        if (this.curPhoto.nTags > 0 && this.curPhoto.tags.size() == 0) {
            new PhotosGetTags(this.curPhoto.ownerID, this.curPhoto.id, this.curPhoto.accessKey).setCallback(new PhotosGetTags.Callback() {
                public void success(ArrayList<PhotoTag> tags) {
                    PhotoViewerFragment.this.curPhoto.tags = tags;
                    PhotoViewerFragment.this.doShowTags();
                }

                public void fail(int ecode, String emsg) {
                    Toast.makeText(PhotoViewerFragment.this.getActivity(), C0436R.string.err_text, 0).show();
                }
            }).wrapProgress(getActivity()).exec(getActivity());
        } else if (this.curPhoto.tags.size() > 0) {
            doShowTags();
        } else {
            Toast.makeText(getActivity(), C0436R.string.photo_no_tags, 0).show();
        }
    }

    private void doShowTags() {
        String[] items = new String[this.curPhoto.tags.size()];
        for (int i = 0; i < this.curPhoto.tags.size(); i++) {
            items[i] = ((PhotoTag) this.curPhoto.tags.get(i)).userName;
        }
        new Builder(getActivity()).setTitle(C0436R.string.tags).setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                PhotoTag tag = (PhotoTag) PhotoViewerFragment.this.curPhoto.tags.get(which);
                if (tag.userID > 0) {
                    Bundle args = new Bundle();
                    args.putInt("id", tag.userID);
                    Navigate.to("ProfileFragment", args, PhotoViewerFragment.this.getActivity());
                }
            }
        }).show();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.contentView;
    }

    public void onConfigurationChanged(Configuration cfg) {
        super.onConfigurationChanged(cfg);
    }

    private void loadUsers() {
        ArrayList<Integer> ids = new ArrayList();
        Iterator it = this.photos.iterator();
        while (it.hasNext()) {
            Photo p = (Photo) it.next();
            if (p.user == null && !ids.contains(Integer.valueOf(p.userID))) {
                ids.add(Integer.valueOf(p.userID));
            }
        }
        Log.m528i("vk", "LOAD USERS");
        Friends.getUsers(ids, new GetUsersCallback() {
            public void onUsersLoaded(ArrayList<UserProfile> users) {
                HashMap<Integer, UserProfile> h = new HashMap();
                Iterator it = users.iterator();
                while (it.hasNext()) {
                    UserProfile p = (UserProfile) it.next();
                    h.put(Integer.valueOf(p.uid), p);
                }
                Iterator it2 = PhotoViewerFragment.this.photos.iterator();
                while (it2.hasNext()) {
                    Photo p2 = (Photo) it2.next();
                    if (p2.user == null) {
                        p2.user = (UserProfile) h.get(Integer.valueOf(p2.userID));
                    }
                }
            }
        });
    }

    private void loadPhotoInfo() {
        if (!this.curPhoto.infoLoaded) {
            Photo p = this.curPhoto;
            new PhotosGetInfo(p.ownerID, p.id, p.accessKey).setCallback(new AnonymousClass13(p)).exec(getActivity());
        }
    }

    private void like(boolean liked) {
        this.curPhoto.isLiked = liked;
        Photo photo;
        if (liked) {
            photo = this.curPhoto;
            photo.nLikes++;
        } else {
            photo = this.curPhoto;
            photo.nLikes--;
        }
        if (!this.liking) {
            this.liking = true;
            new WallLike(liked, this.curPhoto.ownerID, this.curPhoto.id, false, 1, 0, this.curPhoto.accessKey).setCallback(new AnonymousClass14(liked)).exec();
        }
    }

    private void showControls(boolean show, boolean anim) {
        int i = 0;
        if (getSherlockActivity() != null) {
            if (show) {
                getSherlockActivity().getSupportActionBar().show();
            } else {
                getSherlockActivity().getSupportActionBar().hide();
            }
            if (this.curPhoto.id != 0) {
                if (anim) {
                    Global.showViewAnimated(this.buttonBar, show, PhotoView.THUMB_ANIM_DURATION);
                } else {
                    this.buttonBar.setVisibility(show ? 0 : 4);
                }
                if (VERSION.SDK_INT >= 14) {
                    PhotoView photoView = this.photoView;
                    if (!show) {
                        i = 1;
                    }
                    photoView.setSystemUiVisibility(i);
                }
            }
        }
    }

    public void onPositionChanged(int pos) {
        if (getActivity() != null && this.docUrl == null) {
            if (pos >= this.photos.size()) {
                pos = this.photos.size() - 1;
            }
            this.curPhoto = (Photo) this.photos.get(pos);
            getSherlockActivity().invalidateOptionsMenu();
            Object[] objArr;
            if (this.title != null) {
                ActionBar supportActionBar = getSherlockActivity().getSupportActionBar();
                objArr = new Object[2];
                objArr[0] = Integer.valueOf(pos + 1);
                objArr[1] = Integer.valueOf(this.total != 0 ? this.total : this.photos.size());
                supportActionBar.setSubtitle(getString(C0436R.string.player_num, objArr));
            } else {
                FragmentActivity activity = getActivity();
                objArr = new Object[2];
                objArr[0] = Integer.valueOf(pos + 1);
                objArr[1] = Integer.valueOf(this.total != 0 ? this.total : this.photos.size());
                activity.setTitle(getString(C0436R.string.player_num, objArr));
            }
            if (!this.curPhoto.infoLoaded) {
                loadPhotoInfo();
            }
            updateBottomBar();
            if (!(this.allUid == 0 && this.aid == 0) && this.photos.size() < this.total && pos >= this.photos.size() - 2) {
                Log.m526e("vk", "LOAD MORE!");
                loadMore();
            }
        }
    }

    private void loadMore() {
        if (!this.dataLoading) {
            this.dataLoading = true;
            if (this.allUid != 0) {
                new PhotosGetAll(this.allUid, this.photos.size(), 100).setCallback(new PhotosGetAll.Callback() {
                    public void success(int total, Vector<Photo> photos) {
                        PhotoViewerFragment.this.photos.addAll(photos);
                        PhotoViewerFragment.this.dataLoading = false;
                    }

                    public void fail(int ecode, String emsg) {
                        PhotoViewerFragment.this.dataLoading = false;
                    }
                }).exec(getActivity());
            } else if (this.aid > -9000) {
                new PhotosGet(this.aoid, this.aid, this.photos.size(), 500).setCallback(new PhotosGet.Callback() {
                    public void success(int total, Vector<Photo> photos) {
                        PhotoViewerFragment.this.photos.addAll(photos);
                        PhotoViewerFragment.this.dataLoading = false;
                    }

                    public void fail(int ecode, String emsg) {
                        PhotoViewerFragment.this.dataLoading = false;
                    }
                }).exec(getActivity());
            } else if (this.aid == -9000) {
                this.dataLoading = true;
                new PhotosGetUserPhotos(this.aoid, this.photos.size(), 500).setCallback(new PhotosGetUserPhotos.Callback() {
                    public void success(int total, Vector<Photo> photos) {
                        PhotoViewerFragment.this.photos.addAll(photos);
                        PhotoViewerFragment.this.dataLoading = false;
                    }

                    public void fail(int ecode, String emsg) {
                        PhotoViewerFragment.this.dataLoading = false;
                    }
                }).exec(getActivity());
            } else if (this.aid == -9001) {
                new FaveGetPhotos(this.photos.size(), 500).setCallback(new FaveGetPhotos.Callback() {
                    public void success(int total, Vector<Photo> photos) {
                        if (PhotoViewerFragment.this.photos.size() != 0) {
                            boolean needAnim = false;
                        }
                        PhotoViewerFragment.this.photos.addAll(photos);
                        PhotoViewerFragment.this.dataLoading = false;
                    }

                    public void fail(int ecode, String emsg) {
                        PhotoViewerFragment.this.dataLoading = false;
                    }
                }).exec(getActivity());
            }
        }
    }

    private void updateBottomBar() {
        if (this.curPhoto != null) {
            ActivityUtils.setBeamLink(getActivity(), "photo" + this.curPhoto.ownerID + "_" + this.curPhoto.id);
            if (this.curPhoto.infoLoaded) {
                setBottomBarData(this.curPhoto.nLikes, this.curPhoto.nComments, this.curPhoto.nTags, this.curPhoto.isLiked, this.curPhoto.descr);
                return;
            }
            setBottomBarData(0, 0, 0, false, ACRAConstants.DEFAULT_STRING_VALUE);
        }
    }

    public void animateOut(Runnable afterDone) {
        if (this.curPhoto.viewBounds == null || getResources().getConfiguration().orientation != getArguments().getInt("orientation", 0) || VERSION.SDK_INT < 14) {
            afterDone.run();
            getActivity().overridePendingTransition(0, C0436R.anim.fade_out_fast);
            return;
        }
        if (this.buttonBar.getVisibility() != 8) {
            showControls(false, false);
        }
        this.photoView.animateOut(this.curPhoto.viewBounds, this.curPhoto.viewClipTop, afterDone);
    }

    private void setBottomBarData(int likes, int comments, int tags, boolean liked, String descr) {
        this.descrVisible = true;
        this.buttonBar.findViewById(C0436R.id.photo_viewer_descr).setVisibility(0);
        ((TextView) this.buttonBar.findViewById(C0436R.id.photo_viewer_descr)).setText(Global.replaceEmoji(Global.unwrapMentions(descr)));
        ((TextView) this.buttonBar.findViewById(C0436R.id.photo_viewer_likes_t)).setCompoundDrawablesWithIntrinsicBounds(liked ? C0436R.drawable.ic_photoview_liked : C0436R.drawable.ic_photoview_like, 0, 0, 0);
        ((TextView) this.buttonBar.findViewById(C0436R.id.photo_viewer_likes_t)).setCompoundDrawablePadding(Global.scale(5.0f));
        ((TextView) this.buttonBar.findViewById(C0436R.id.photo_viewer_likes_t)).setText(likes > 0 ? new StringBuilder(String.valueOf(likes)).toString() : ACRAConstants.DEFAULT_STRING_VALUE);
        ((TextView) this.buttonBar.findViewById(C0436R.id.photo_viewer_comments_t)).setText(comments > 0 ? new StringBuilder(String.valueOf(comments)).toString() : ACRAConstants.DEFAULT_STRING_VALUE);
        ((TextView) this.buttonBar.findViewById(C0436R.id.photo_viewer_tags_t)).setText(tags > 0 ? new StringBuilder(String.valueOf(tags)).toString() : ACRAConstants.DEFAULT_STRING_VALUE);
    }

    public void onDismiss() {
        if (!this.dismissed) {
            this.dismissed = true;
            getActivity().finish();
        }
    }

    public void onPrepareDismiss() {
        if (this.curPhoto.viewBounds != null) {
            Rect r = new Rect(this.curPhoto.viewBounds);
            r.top += this.curPhoto.viewClipTop;
            this.photoView.setThumbBounds(r);
        } else {
            this.photoView.setThumbBounds(null);
        }
        showControls(false, false);
    }

    public void onZoomChanged(float factor, float max, float min) {
        boolean show = factor <= min;
        if (show != this.descrVisible) {
            this.descrVisible = show;
            this.buttonBar.findViewById(C0436R.id.photo_viewer_descr).clearAnimation();
            Global.showViewAnimated(this.buttonBar.findViewById(C0436R.id.photo_viewer_descr), show, PhotoView.THUMB_ANIM_DURATION);
        }
    }
}
