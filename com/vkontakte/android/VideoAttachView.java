package com.vkontakte.android;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import com.vkontakte.android.api.VideoFile;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.ui.PhotoView;
import java.io.ByteArrayOutputStream;

public class VideoAttachView extends FrameLayout implements OnClickListener {
    public VideoFile file;

    /* renamed from: com.vkontakte.android.VideoAttachView.1 */
    class C05291 implements AnimatorListener {
        boolean removed;
        private final /* synthetic */ View val$black;
        private final /* synthetic */ ImageView val$iv;

        /* renamed from: com.vkontakte.android.VideoAttachView.1.1 */
        class C05281 implements Runnable {
            private final /* synthetic */ View val$black;
            private final /* synthetic */ ImageView val$iv;

            C05281(ImageView imageView, View view) {
                this.val$iv = imageView;
                this.val$black = view;
            }

            public void run() {
                ((ViewGroup) this.val$iv.getParent()).removeView(this.val$iv);
                ((ViewGroup) this.val$black.getParent()).removeView(this.val$black);
            }
        }

        C05291(ImageView imageView, View view) {
            this.val$iv = imageView;
            this.val$black = view;
            this.removed = false;
        }

        public void onAnimationCancel(Animator arg0) {
        }

        public void onAnimationEnd(Animator arg0) {
            if (!this.removed) {
                VideoAttachView.this.postDelayed(new C05281(this.val$iv, this.val$black), 1500);
                this.removed = true;
            }
        }

        public void onAnimationRepeat(Animator arg0) {
        }

        public void onAnimationStart(Animator arg0) {
        }
    }

    public VideoAttachView(Context context) {
        super(context);
        init();
    }

    public VideoAttachView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setOnClickListener(this);
        setFocusable(false);
        setFocusableInTouchMode(false);
    }

    public void onClick(View arg0) {
        Intent intent = new Intent(getContext(), NewVideoPlayerActivity.class);
        intent.putExtra("file", this.file);
        if (VERSION.SDK_INT >= 11) {
            ImageView thumb = (ImageView) findViewById(C0436R.id.video_preview);
            ImageView iv = new ImageView(getContext());
            iv.setImageDrawable(thumb.getDrawable());
            iv.setLayoutParams(new LayoutParams(thumb.getWidth(), thumb.getHeight()));
            View black = new View(getContext());
            black.setBackgroundColor(Color.WINDOW_BACKGROUND_COLOR);
            black.setLayoutParams(new LayoutParams(-1, -1));
            black.setAlpha(0.0f);
            Activity act = (Activity) getContext();
            if (act.getParent() == null) {
                ((ViewGroup) act.getWindow().getDecorView()).addView(black);
                ((ViewGroup) act.getWindow().getDecorView()).addView(iv);
            } else {
                act = act.getParent();
                ((ViewGroup) act.getWindow().getDecorView()).addView(black);
                ((ViewGroup) act.getWindow().getDecorView()).addView(iv);
            }
            int[] coords = new int[2];
            thumb.getLocationInWindow(coords);
            iv.setTranslationX((float) coords[0]);
            iv.setTranslationY((float) coords[1]);
            iv.setPivotX((float) (thumb.getWidth() / 2));
            iv.setPivotY((float) (thumb.getHeight() / 2));
            int r = act.getWindowManager().getDefaultDisplay().getRotation();
            int o = getResources().getConfiguration().orientation;
            boolean rv = r == 0 || r == 2;
            if (rv != (o == 1)) {
                r++;
                if (r > 3) {
                    r -= 4;
                }
            }
            int scrW = Math.max(act.getWindow().getDecorView().getWidth(), act.getWindow().getDecorView().getHeight());
            int scrH = Math.min(act.getWindow().getDecorView().getWidth(), act.getWindow().getDecorView().getHeight());
            float xdst = (float) ((scrW / 2) - (thumb.getWidth() / 2));
            float ydst = (float) ((scrH / 2) - (thumb.getHeight() / 2));
            ObjectAnimator rotation = null;
            float t;
            if (r == 0) {
                rotation = ObjectAnimator.ofFloat(iv, "rotation", new float[]{90.0f});
                t = xdst;
                xdst = (((float) (thumb.getHeight() / 2)) + ydst) - ((float) (thumb.getWidth() / 2));
                ydst = (((float) (thumb.getWidth() / 2)) + t) - ((float) (thumb.getHeight() / 2));
            } else if (r == 3) {
                rotation = ObjectAnimator.ofFloat(iv, "rotationX", new float[]{180.0f});
            } else if (r == 2) {
                rotation = ObjectAnimator.ofFloat(iv, "rotation", new float[]{-90.0f});
                t = xdst;
                xdst = (((float) (thumb.getHeight() / 2)) + ydst) - ((float) (thumb.getWidth() / 2));
                ydst = (((float) (thumb.getWidth() / 2)) + t) - ((float) (thumb.getHeight() / 2));
            }
            if (rotation != null) {
                rotation.setInterpolator(new DecelerateInterpolator());
                rotation.setDuration(300);
            }
            Animator tX = ObjectAnimator.ofFloat(iv, "translationX", new float[]{xdst});
            tX.setInterpolator(new DecelerateInterpolator());
            tX.setDuration(300);
            Animator tY = ObjectAnimator.ofFloat(iv, "translationY", new float[]{ydst});
            tY.setInterpolator(new DecelerateInterpolator());
            tY.setDuration(300);
            float[] fArr = new float[1];
            fArr[0] = ((float) scrH) / ((float) thumb.getHeight());
            ObjectAnimator sX = ObjectAnimator.ofFloat(iv, "scaleX", fArr);
            sX.setInterpolator(new DecelerateInterpolator());
            sX.setDuration(300);
            fArr = new float[1];
            fArr[0] = ((float) scrH) / ((float) thumb.getHeight());
            Animator sY = ObjectAnimator.ofFloat(iv, "scaleY", fArr);
            sY.setInterpolator(new DecelerateInterpolator());
            sY.setDuration(300);
            ObjectAnimator fade = ObjectAnimator.ofFloat(black, "alpha", new float[]{1.0f});
            fade.setInterpolator(new DecelerateInterpolator());
            fade.setDuration(300);
            AnimatorSet set = new AnimatorSet();
            set.play(tX).with(tY);
            set.play(tY).with(sX);
            set.play(sX).with(sY);
            set.play(sY).with(fade);
            if (rotation != null) {
                set.play(fade).with(rotation);
            }
            set.addListener(new C05291(iv, black));
            set.start();
            intent.putExtra("anim_start_time", System.currentTimeMillis());
            intent.putExtra("anim_duration", PhotoView.THUMB_ANIM_DURATION);
        }
        if (((ImageView) findViewById(C0436R.id.video_preview)).getDrawable() != null) {
            try {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                ((BitmapDrawable) ((ImageView) findViewById(C0436R.id.video_preview)).getDrawable()).getBitmap().compress(CompressFormat.JPEG, 95, os);
                intent.putExtra("thumb", os.toByteArray());
            } catch (Exception e) {
            }
        }
        try {
            getContext().startActivity(intent);
            if (VERSION.SDK_INT >= 5) {
                ((Activity) getContext()).overridePendingTransition(17432576, 17432577);
            }
        } catch (Exception e2) {
            intent.removeExtra("thumb");
            getContext().startActivity(intent);
            if (VERSION.SDK_INT >= 5) {
                ((Activity) getContext()).overridePendingTransition(17432576, 17432577);
            }
        }
    }

    public void setImageBitmap(Bitmap bmp) {
        ((ImageView) findViewById(C0436R.id.video_preview)).setImageBitmap(bmp);
    }
}
