package com.vkontakte.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.vkontakte.android.PhotoAttachment.FixedSizeImageView;
import com.vkontakte.android.PhotoAttachment.Image;
import com.vkontakte.android.api.PhotoAlbum;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.ui.FixedSizeFrameLayout;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import org.acra.ACRAConstants;

public class AlbumAttachment extends PhotoAttachment {
    public int size;

    /* renamed from: com.vkontakte.android.AlbumAttachment.1 */
    class C01601 implements OnClickListener {
        C01601() {
        }

        public void onClick(View v) {
            PhotoAlbum album = new PhotoAlbum();
            album.title = AlbumAttachment.this.descr;
            album.oid = AlbumAttachment.this.oid;
            album.id = AlbumAttachment.this.aid;
            album.thumbURL = ((Image) AlbumAttachment.this.images.get("m")).url;
            album.numPhotos = AlbumAttachment.this.size;
            Bundle args = new Bundle();
            args.putParcelable("album", album);
            String str = "PhotoListFragment";
            Activity activity = (Activity) v.getContext();
            boolean z = VERSION.SDK_INT >= 14 && !Build.BRAND.toLowerCase().contains("zte");
            Navigate.to(str, args, activity, z, -1, -1);
        }
    }

    public AlbumAttachment(HashMap<String, Image> _images, int _oid, int _pid, int _aid, String _descr, int _size) {
        super((HashMap) _images, _oid, _pid, _aid, _descr);
        this.size = _size;
    }

    public AlbumAttachment(Image[] _images, int _oid, int _pid, int _aid, String _descr, int _size) {
        super(_images, _oid, _pid, _aid, _descr, 0, 0, ACRAConstants.DEFAULT_STRING_VALUE);
        this.size = _size;
    }

    public View getViewForList(Context context, View reuse) {
        int i;
        int i2 = 10;
        View iv = super.getViewForList(context, reuse);
        iv.setOnClickListener(new C01601());
        iv.setId(C0436R.id.attach_album_image);
        View view = Attachment.getReusableView(context, "album");
        ((TextView) view.findViewById(C0436R.id.attach_title)).setText(this.descr);
        ((TextView) view.findViewById(C0436R.id.attach_subtitle)).setText(Global.langPlural(C0436R.array.num_attach_photo, this.size, context.getResources()));
        if (((ViewGroup) view).getChildAt(0) instanceof ImageView) {
            ((ViewGroup) view).removeViewAt(0);
        }
        ((ViewGroup) view).addView(iv, 0);
        FixedSizeFrameLayout fixedSizeFrameLayout = (FixedSizeFrameLayout) view;
        int i3 = this.displayW;
        int i4 = this.displayH;
        if (this.paddingAfter) {
            i = 10;
        } else {
            i = 0;
        }
        fixedSizeFrameLayout.setSize(i3, Global.scale((float) i) + i4);
        if (!this.paddingAfter) {
            i2 = 0;
        }
        view.setPadding(0, 0, 0, Global.scale((float) i2));
        ((ImageView) view.findViewById(C0436R.id.attach_album_image)).setImageDrawable(new ColorDrawable(Color.LIGHT_GRAY));
        return view;
    }

    public void serialize(DataOutputStream s) throws IOException {
        s.writeInt(13);
        Set<String> keys = this.images.keySet();
        s.writeInt(keys.size());
        for (String k : keys) {
            Image im = (Image) this.images.get(k);
            s.writeChar(k.charAt(0));
            s.writeUTF(im.url);
            s.writeInt(im.width);
            s.writeInt(im.height);
        }
        s.writeInt(this.oid);
        s.writeInt(this.pid);
        s.writeInt(this.aid);
        s.writeUTF(this.descr != null ? this.descr : ACRAConstants.DEFAULT_STRING_VALUE);
        s.writeInt(this.size);
    }

    public String toString() {
        return "album" + this.oid + "_" + this.aid;
    }

    public void setImage(View view, Bitmap img, boolean fromCache) {
        ((ImageView) view.findViewById(C0436R.id.attach_album_image)).setImageBitmap(img);
        if (fromCache) {
            ((FixedSizeImageView) view.findViewById(C0436R.id.attach_album_image)).dontAnimate();
        } else {
            ((FixedSizeImageView) view.findViewById(C0436R.id.attach_album_image)).animateAlpha();
        }
    }

    public void clearImage(View view) {
        ((ImageView) view.findViewById(C0436R.id.attach_album_image)).setImageDrawable(new ColorDrawable(Color.LIGHT_GRAY));
    }
}
