package com.vkontakte.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.api.AccountSetInfo;
import com.vkontakte.android.api.AccountSetInfo.Callback;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.data.Posts;
import com.vkontakte.android.fragments.SuggestionsFriendsFragment;
import com.vkontakte.android.fragments.SuggestionsRecommendationsFragment;
import com.vkontakte.android.ui.ActionBarHacks;
import com.vkontakte.android.ui.ActionBarProgressDrawable;

public class SuggestionsActivity extends VKFragmentActivity {
    private FrameLayout contentView;
    private View doneBtn;
    private SuggestionsFriendsFragment friends;
    private int page;
    private ActionBarProgressDrawable progress;
    private SuggestionsRecommendationsFragment recommendations;

    /* renamed from: com.vkontakte.android.SuggestionsActivity.1 */
    class C05211 implements OnClickListener {
        C05211() {
        }

        public void onClick(View v) {
            if (SuggestionsActivity.this.page == 0) {
                SuggestionsActivity.this.switchScreen();
                SuggestionsActivity.this.updateIntroFlags(1);
                return;
            }
            SuggestionsActivity.this.sendBroadcast(new Intent(Posts.ACTION_RELOAD_FEED), permission.ACCESS_DATA);
            SuggestionsActivity.this.updateIntroFlags(2);
            SuggestionsActivity.this.setResult(-1);
            SuggestionsActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.SuggestionsActivity.2 */
    class C13642 extends ActionBarProgressDrawable {
        C13642() {
        }

        public void invalidateSelf() {
            super.invalidateSelf();
            View abv = ActionBarHacks.getActionBar(SuggestionsActivity.this);
            if (abv != null) {
                abv.postInvalidate();
            }
        }
    }

    /* renamed from: com.vkontakte.android.SuggestionsActivity.3 */
    class C13653 implements Callback {
        C13653() {
        }

        public void success() {
        }

        public void fail(int ecode, String emsg) {
        }
    }

    public SuggestionsActivity() {
        this.page = 0;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        this.contentView = new FrameLayout(this);
        this.contentView.setId(C0436R.id.fragment_wrapper);
        setContentView(this.contentView);
        this.doneBtn = View.inflate(this, C0436R.layout.ab_done_right, null);
        ((TextView) this.doneBtn.findViewById(C0436R.id.ab_done_text)).setText(C0436R.string.welcome_next);
        this.doneBtn.setOnClickListener(new C05211());
        this.progress = new C13642();
        this.progress.setStepCount(2);
        this.progress.setStep(0);
        getSupportActionBar().setBackgroundDrawable(this.progress);
        this.page = getIntent().getBooleanExtra("groups", false) ? 0 : 1;
        switchScreen();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add((int) C0436R.string.send);
        item.setActionView(this.doneBtn);
        item.setShowAsAction(2);
        return true;
    }

    public void finish() {
        Friends.reload(true);
        Groups.reload(true);
        super.finish();
    }

    private void switchScreen() {
        if (this.page == 0) {
            this.page = 1;
            if (this.recommendations == null) {
                this.recommendations = new SuggestionsRecommendationsFragment();
            }
            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, this.recommendations).commit();
            setTitle((int) C0436R.string.recommended_pages);
            if (getIntent().getBooleanExtra("groups", false)) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setHomeButtonEnabled(false);
            } else {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
            }
        } else if (this.page == 1) {
            this.page = 0;
            if (this.friends == null) {
                Bundle extras = new Bundle();
                extras.putBoolean("from_signup", true);
                this.friends = new SuggestionsFriendsFragment();
                this.friends.setArguments(extras);
            }
            getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, this.friends).commit();
            setTitle((int) C0436R.string.find_friends);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }
        this.progress.setStepAnimated(this.page);
    }

    private void updateIntroFlags(int remove) {
        int intro = getSharedPreferences(null, 0).getInt("intro", 0) & (remove ^ -1);
        getSharedPreferences(null, 0).edit().putInt("intro", intro).commit();
        new AccountSetInfo(intro).setCallback(new C13653()).exec();
    }

    public void onBackPressed() {
        if (this.page != 1 || getIntent().getBooleanExtra("groups", false)) {
            setResult(0);
            finish();
            return;
        }
        switchScreen();
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        super.onActivityResult(reqCode, resCode, data);
        if (reqCode == SuggestionsFriendsFragment.GPLUS_ERROR_RESULT) {
            this.friends.onActivityResult(reqCode, resCode, data);
        }
    }
}
