package com.vkontakte.android;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ProgressBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Photo.Image;
import com.vkontakte.android.api.NotesGetById;
import com.vkontakte.android.api.PagesGetHTML;
import com.vkontakte.android.api.PagesGetHTML.Callback;
import com.vkontakte.android.mediapicker.gl.GLFilterContext;
import com.vkontakte.android.mediapicker.providers.GalleryPickerProvider;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.acra.ACRAConstants;

public class WikiViewActivity extends SherlockActivity {
    private Bundle curMeta;
    private ArrayList<HistoryEntry> history;
    private boolean isLoaded;
    private ProgressBar progress;
    private APIRequest req;
    private ShareActionProvider shareActionProvider;
    private String template;
    private WebView webView;

    /* renamed from: com.vkontakte.android.WikiViewActivity.7 */
    class C05487 implements OnClickListener {
        C05487() {
        }

        public void onClick(DialogInterface dialog, int which) {
            if (!WikiViewActivity.this.isLoaded) {
                WikiViewActivity.this.onBackPressed();
            }
        }
    }

    /* renamed from: com.vkontakte.android.WikiViewActivity.8 */
    class C05498 implements OnCancelListener {
        C05498() {
        }

        public void onCancel(DialogInterface dialog) {
            if (!WikiViewActivity.this.isLoaded) {
                WikiViewActivity.this.onBackPressed();
            }
        }
    }

    private static class HistoryEntry {
        int oid;
        int pid;
        String title;

        private HistoryEntry() {
        }
    }

    private class WebCallback {
        private WebCallback() {
        }

        @JavascriptInterface
        public void playAudio(int oid, int aid, String artist, String title, String url, int duration) {
            AudioFile af = new AudioFile(aid, oid, artist, title, duration, url);
            Intent intent = new Intent(WikiViewActivity.this, AudioPlayerService.class);
            intent.putExtra("action", 1);
            intent.putExtra("file", af);
            WikiViewActivity.this.startService(intent);
            Intent intent2 = new Intent(WikiViewActivity.this, AudioPlayerService.class);
            intent2.putExtra("action", 4);
            WikiViewActivity.this.startService(intent2);
        }

        @JavascriptInterface
        public void openPage(int oid, int pid) {
            WikiViewActivity.this.loadPage(oid, pid, true);
        }

        @JavascriptInterface
        public void openPhotos(String[] urls, int pos) {
            ArrayList<Photo> photos = new ArrayList();
            for (String url : urls) {
                Photo p = new Photo();
                Image im = new Image();
                im.type = 'x';
                im.url = url;
                p.sizes.add(im);
                photos.add(p);
            }
            Bundle args = new Bundle();
            args.putParcelableArrayList("list", photos);
            args.putInt(GLFilterContext.AttributePosition, pos);
            Navigate.to("PhotoViewerFragment", args, WikiViewActivity.this, true, -1, -1);
        }
    }

    private class WebChrome extends WebChromeClient {
        private WebChrome() {
        }

        public void onProgressChanged(WebView view, int newProgress) {
            WikiViewActivity.this.setProgress(newProgress * 100);
            if (newProgress > 0 && WikiViewActivity.this.progress.getVisibility() != 8) {
                WikiViewActivity.this.progress.setVisibility(8);
            }
        }

        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            Log.m528i("vk", "ALERT -> " + message);
            return true;
        }
    }

    private class WebClient extends WebViewClient {
        private WebClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            url = url.replace(GalleryPickerProvider.UNSTYLED_URI_SCHEME, "http://vkontakte.ru");
            try {
                Uri uri = Uri.parse(url);
                if (("vkontakte.ru".equals(uri.getHost()) | "vk.com".equals(uri.getHost())) != 0) {
                    if ("/pages".equals(uri.getPath())) {
                        int oid = Integer.parseInt(uri.getQueryParameter("oid"));
                        String title = uri.getQueryParameter("p").replace('_', ' ');
                        Log.m525d("vk", "page '" + title + "' " + oid);
                        WikiViewActivity.this.loadPage(oid, title, true);
                    } else if (uri.getPath() != null && uri.getPath().matches("/page[-0-9]+_[0-9]+")) {
                        Matcher m = Pattern.compile("/page([-0-9]+)_([0-9]+)").matcher(uri.getPath());
                        m.find();
                        WikiViewActivity.this.loadPage(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), true);
                    }
                    return true;
                }
            } catch (Exception e) {
            }
            WikiViewActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("vklink://view/?" + url)));
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.WikiViewActivity.1 */
    class C13871 implements Callback {
        C13871() {
        }

        public void success(String html, String title, Bundle meta) {
            WikiViewActivity.this.curMeta = meta;
            WikiViewActivity.this.displayPage(html);
            WikiViewActivity.this.pushHistory(0, 0, WikiViewActivity.this.getIntent().getStringExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
            WikiViewActivity.this.req = null;
        }

        public void fail(int ecode, String emsg) {
            WikiViewActivity.this.showError(ecode);
            WikiViewActivity.this.req = null;
        }
    }

    /* renamed from: com.vkontakte.android.WikiViewActivity.2 */
    class C13882 implements Callback {
        C13882() {
        }

        public void success(String html, String title, Bundle meta) {
            WikiViewActivity.this.curMeta = meta;
            WikiViewActivity.this.displayPage(html);
            WikiViewActivity.this.pushHistory(WikiViewActivity.this.getIntent().getIntExtra("oid", 0), 0, WikiViewActivity.this.getIntent().getStringExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
            WikiViewActivity.this.req = null;
        }

        public void fail(int ecode, String emsg) {
            WikiViewActivity.this.showError(ecode);
            WikiViewActivity.this.req = null;
        }
    }

    /* renamed from: com.vkontakte.android.WikiViewActivity.3 */
    class C13893 implements Callback {
        C13893() {
        }

        public void success(String html, String title, Bundle meta) {
            WikiViewActivity.this.curMeta = meta;
            WikiViewActivity.this.displayPage(html);
            WikiViewActivity.this.pushHistory(WikiViewActivity.this.getIntent().getIntExtra("oid", 0), WikiViewActivity.this.getIntent().getIntExtra("pid", 0), null);
            WikiViewActivity.this.setTitle(title);
            WikiViewActivity.this.req = null;
        }

        public void fail(int ecode, String emsg) {
            WikiViewActivity.this.showError(ecode);
            WikiViewActivity.this.req = null;
        }
    }

    /* renamed from: com.vkontakte.android.WikiViewActivity.4 */
    class C13904 implements Callback {
        private final /* synthetic */ int val$oid;
        private final /* synthetic */ boolean val$useHistory;

        C13904(boolean z, int i) {
            this.val$useHistory = z;
            this.val$oid = i;
        }

        public void success(String html, String title, Bundle meta) {
            WikiViewActivity.this.curMeta = meta;
            WikiViewActivity.this.setTitle(title);
            WikiViewActivity.this.displayPage(html);
            if (this.val$useHistory) {
                WikiViewActivity.this.pushHistory(this.val$oid, 0, title);
            }
            WikiViewActivity.this.req = null;
        }

        public void fail(int ecode, String emsg) {
            WikiViewActivity.this.showError(ecode);
            WikiViewActivity.this.req = null;
        }
    }

    /* renamed from: com.vkontakte.android.WikiViewActivity.5 */
    class C13915 implements Callback {
        private final /* synthetic */ int val$id;
        private final /* synthetic */ int val$oid;
        private final /* synthetic */ boolean val$useHistory;

        C13915(boolean z, int i, int i2) {
            this.val$useHistory = z;
            this.val$oid = i;
            this.val$id = i2;
        }

        public void success(String html, String title, Bundle meta) {
            WikiViewActivity.this.curMeta = meta;
            WikiViewActivity.this.setTitle(title);
            WikiViewActivity.this.displayPage(html);
            if (this.val$useHistory) {
                WikiViewActivity.this.pushHistory(this.val$oid, this.val$id, null);
            }
            WikiViewActivity.this.req = null;
        }

        public void fail(int ecode, String emsg) {
            WikiViewActivity.this.showError(ecode);
            WikiViewActivity.this.req = null;
        }
    }

    /* renamed from: com.vkontakte.android.WikiViewActivity.6 */
    class C13926 implements NotesGetById.Callback {
        C13926() {
        }

        public void success(String html) {
            WikiViewActivity.this.displayPage(html);
            WikiViewActivity.this.req = null;
        }

        public void fail(int ecode, String emsg) {
            WikiViewActivity.this.showError(ecode);
            WikiViewActivity.this.req = null;
        }
    }

    public WikiViewActivity() {
        this.history = new ArrayList();
        this.isLoaded = false;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        setTitle(getIntent().getStringExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE));
        requestWindowFeature(2);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.webView = new WebView(this);
        this.webView.setInitialScale((int) (Global.displayDensity * 100.0f));
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setWebViewClient(new WebClient());
        this.webView.setWebChromeClient(new WebChrome());
        this.webView.addJavascriptInterface(new WebCallback(), "vk");
        FrameLayout wrap = new FrameLayout(this);
        wrap.addView(this.webView);
        this.progress = new ProgressBar(this);
        wrap.addView(this.progress, new LayoutParams(Global.scale(40.0f), Global.scale(40.0f), 17));
        setContentView((View) wrap);
        try {
            InputStream is = getResources().getAssets().open(getIntent().getBooleanExtra("site", false) ? "wiki_template_article.htm" : "wiki_template.htm");
            byte[] buf = new byte[is.available()];
            is.read(buf);
            this.template = new String(buf, "UTF-8");
        } catch (Exception e) {
        }
        if (getIntent().hasExtra("nid")) {
            loadNote(getIntent().getIntExtra("oid", 0), getIntent().getIntExtra("nid", 0));
        } else {
            loadPage();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (getIntent().getBooleanExtra("site", false)) {
            MenuItem item = menu.add(0, (int) C0436R.id.attach, 0, ACRAConstants.DEFAULT_STRING_VALUE);
            item.setShowAsAction(2);
            this.shareActionProvider = new ShareActionProvider(getSupportActionBar().getThemedContext());
            item.setActionProvider(this.shareActionProvider);
        }
        return true;
    }

    public void onPause() {
        super.onPause();
        AppStateTracker.onActivityPaused();
    }

    public void onResume() {
        super.onResume();
        AppStateTracker.onActivityResumed(this);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.req != null) {
            this.req.cancel();
            this.req = null;
        }
    }

    private void loadPage() {
        if (getIntent().getIntExtra("oid", 0) == 0) {
            this.req = new PagesGetHTML(getIntent().getStringExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)).setCallback(new C13871()).exec((Activity) this);
        } else if (getIntent().getIntExtra("pid", 0) == 0) {
            this.req = new PagesGetHTML(getIntent().getIntExtra("oid", 0), getIntent().getStringExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)).setCallback(new C13882()).exec((Activity) this);
        } else {
            this.req = new PagesGetHTML(getIntent().getIntExtra("oid", 0), getIntent().getIntExtra("pid", 0), getIntent().getBooleanExtra("site", false)).setCallback(new C13893()).exec((Activity) this);
        }
    }

    private void loadPage(int oid, String title, boolean useHistory) {
        this.req = new PagesGetHTML(oid, title).setCallback(new C13904(useHistory, oid)).exec((Activity) this);
    }

    private void loadPage(int oid, int id, boolean useHistory) {
        this.req = new PagesGetHTML(oid, id, getIntent().getBooleanExtra("site", false)).setCallback(new C13915(useHistory, oid, id)).exec((Activity) this);
    }

    private void loadNote(int oid, int nid) {
        this.req = new NotesGetById(oid, nid).setCallback(new C13926()).exec((Activity) this);
    }

    private void showError(int code) {
        int msg = C0436R.string.err_text;
        if (code == 100) {
            msg = C0436R.string.wiki_page_not_found;
        } else if (code == 15) {
            msg = C0436R.string.wiki_page_no_access;
        }
        try {
            new Builder(this).setTitle(C0436R.string.error).setMessage(msg).setPositiveButton(C0436R.string.ok, new C05487()).setOnCancelListener(new C05498()).show();
        } catch (Exception e) {
        }
    }

    private void displayPage(String html) {
        this.isLoaded = true;
        String s = this.template.replace("{CONTENT}", html.replace("src=\"/images/", "src=\"http://vk.com/images/"));
        if (getIntent().getBooleanExtra("site", false)) {
            CharSequence string;
            String replace = s.replace("{TITLE}", this.curMeta.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE)).replace("{GROUP_NAME}", this.curMeta.getString("group_name")).replace("{GROUP_PHOTO}", this.curMeta.getString("group_photo")).replace("{ORIG_URL}", this.curMeta.getString(PlusShare.KEY_CALL_TO_ACTION_URL).replaceAll("(http|https)\\://(www\\.){0,1}", ACRAConstants.DEFAULT_STRING_VALUE)).replace("{GROUP_ID}", new StringBuilder(String.valueOf(this.curMeta.getInt("group_id"))).toString()).replace("{PROFILE_ID}", new StringBuilder(String.valueOf(-this.curMeta.getInt("group_id"))).toString()).replace("{VIEWS}", Global.langPlural(C0436R.array.video_views, this.curMeta.getInt("views"), getResources())).replace("{DATE}", Global.langDate(getResources(), this.curMeta.getInt("date"))).replace("{L_SOURCE}", getString(C0436R.string.article_source)).replace("{L_PREVIEW}", getString(C0436R.string.article_preview));
            CharSequence charSequence = "{L_GO_TO_GROUP}";
            if (this.curMeta.getInt("group_id") > 0) {
                string = getString(C0436R.string.article_go_to_group, new Object[]{this.curMeta.getString("group_name")});
            } else {
                string = getString(C0436R.string.article_go_to_profile, new Object[]{this.curMeta.getString("name_gen")});
            }
            s = replace.replace(charSequence, string);
            if (this.shareActionProvider != null) {
                Intent intent = new Intent("android.intent.action.SEND");
                intent.putExtra("android.intent.extra.TEXT", new StringBuilder(String.valueOf(this.curMeta.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE))).append("\n").append(this.curMeta.getString(PlusShare.KEY_CALL_TO_ACTION_URL)).toString());
                intent.setType("text/plain");
                this.shareActionProvider.setShareIntent(intent);
            }
        }
        try {
            File f = new File(getCacheDir(), "wiki_temp.htm");
            FileOutputStream os = new FileOutputStream(f);
            os.write(s.getBytes("UTF-8"));
            os.close();
            this.webView.loadUrl(new StringBuilder(GalleryPickerProvider.UNSTYLED_URI_SCHEME).append(f.getAbsolutePath()).toString());
            this.webView.scrollTo(0, 0);
        } catch (Exception e) {
        }
    }

    private void pushHistory(int oid, int pid, String title) {
        HistoryEntry he = new HistoryEntry();
        he.oid = oid;
        he.pid = pid;
        he.title = title;
        this.history.add(he);
    }

    public void onBackPressed() {
        if (this.history.size() == 0) {
            super.onBackPressed();
            return;
        }
        HistoryEntry e = (HistoryEntry) this.history.remove(this.history.size() - 1);
        if (this.history.size() == 0) {
            super.onBackPressed();
            return;
        }
        e = (HistoryEntry) this.history.get(this.history.size() - 1);
        if (e.title == null) {
            loadPage(e.oid, e.pid, false);
        } else if (e.pid == 0) {
            loadPage(e.oid, e.title, false);
        } else {
            super.onBackPressed();
        }
    }
}
