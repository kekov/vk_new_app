package com.vkontakte.android;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.ExploreByTouchHelper;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.AudioPlayerService.PlayerCallback;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.AudioAdd;
import com.vkontakte.android.api.AudioAdd.Callback;
import com.vkontakte.android.api.AudioDelete;
import com.vkontakte.android.api.AudioGetLyrics;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.cache.AlbumArtRetriever;
import com.vkontakte.android.cache.AlbumArtRetriever.ImageLoadCallback;
import com.vkontakte.android.cache.AudioCache;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.fragments.AudioPlaylistFragment;
import com.vkontakte.android.ui.AlbumScrollView;
import com.vkontakte.android.ui.DepthPageTransformer;
import com.vkontakte.android.ui.Fonts;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.acra.ACRAConstants;

public class AudioPlayerActivity extends VKFragmentActivity implements PlayerCallback {
    private static Drawable pauseIcon;
    private static Drawable playIcon;
    ImageView addBtn;
    private String animArtist;
    private String animTitle;
    private View aview;
    boolean canUpdateProgress;
    private int coverAid;
    private int coverOid;
    private HashMap<Integer, View> coverViews;
    AudioFile file;
    private boolean firstInfoUpdate;
    public boolean isRegistered;
    private BroadcastReceiver receiver;
    private int viewPagerScrollState;
    boolean wasTouching;

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.13 */
    class AnonymousClass13 implements OnClickListener {
        private final /* synthetic */ AudioFile val$file;

        AnonymousClass13(AudioFile audioFile) {
            this.val$file = audioFile;
        }

        public void onClick(DialogInterface dialog, int which) {
            AudioPlayerActivity.this.deleteFile(this.val$file, true);
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.15 */
    class AnonymousClass15 extends AnimatorListenerAdapter {
        private final /* synthetic */ TextView val$artist;
        private final /* synthetic */ AudioFile val$f;
        private final /* synthetic */ TextView val$title;

        AnonymousClass15(TextView textView, AudioFile audioFile, TextView textView2) {
            this.val$artist = textView;
            this.val$f = audioFile;
            this.val$title = textView2;
        }

        public void onAnimationRepeat(Animator animation) {
            this.val$artist.setText(this.val$f.artist);
            this.val$title.setText(this.val$f.title);
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.16 */
    class AnonymousClass16 implements Runnable {
        private final /* synthetic */ ViewPager val$pager;

        /* renamed from: com.vkontakte.android.AudioPlayerActivity.16.1 */
        class C01701 implements Runnable {
            C01701() {
            }

            public void run() {
                AudioPlayerActivity.this.forceUpdateCover();
            }
        }

        AnonymousClass16(ViewPager viewPager) {
            this.val$pager = viewPager;
        }

        public void run() {
            if (Math.abs((AudioPlayerService.sharedInstance.getPlaylistPosition() + 1) - this.val$pager.getCurrentItem()) <= 1) {
                this.val$pager.setCurrentItem(AudioPlayerService.sharedInstance.getPlaylistPosition() + 1, true);
                return;
            }
            this.val$pager.setAdapter(this.val$pager.getAdapter());
            this.val$pager.setCurrentItem(AudioPlayerService.sharedInstance.getPlaylistPosition() + 1, false);
            this.val$pager.postDelayed(new C01701(), 100);
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.17 */
    class AnonymousClass17 implements OnMultiChoiceClickListener {
        private final /* synthetic */ boolean[] val$checked;

        AnonymousClass17(boolean[] zArr) {
            this.val$checked = zArr;
        }

        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
            this.val$checked[which] = isChecked;
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.18 */
    class AnonymousClass18 implements OnClickListener {
        private final /* synthetic */ boolean[] val$checked;
        private final /* synthetic */ ArrayList val$ids;

        AnonymousClass18(boolean[] zArr, ArrayList arrayList) {
            this.val$checked = zArr;
            this.val$ids = arrayList;
        }

        public void onClick(DialogInterface dialog, int which) {
            ArrayList<Integer> newTargets = new ArrayList();
            for (int i = 0; i < this.val$checked.length; i++) {
                if (this.val$checked[i]) {
                    newTargets.add((Integer) this.val$ids.get(i));
                }
            }
            if (AudioPlayerService.sharedInstance != null) {
                AudioPlayerService.sharedInstance.setBroadcast(newTargets);
            }
            AudioPlayerActivity.this.findViewById(C0436R.id.aplayer_broadcast).setSelected(newTargets.size() > 0);
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.1 */
    class C01741 extends BroadcastReceiver {
        C01741() {
        }

        public void onReceive(Context context, Intent intent) {
            if (AudioCache.ACTION_ALBUM_ART_AVAILABLE.equals(intent.getAction())) {
                int oid = intent.getIntExtra("oid", 0);
                int aid = intent.getIntExtra("aid", 0);
                if (AudioPlayerService.sharedInstance != null) {
                    AudioFile file = AudioPlayerService.sharedInstance.getCurrentFile();
                    if (file != null && file.aid == aid && file.oid == oid) {
                        AudioPlayerActivity.this.forceUpdateCover();
                    }
                }
                AudioPlayerActivity.this.updatePager();
            } else if (AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS.equals(intent.getAction())) {
                AudioPlayerActivity.this.invalidateOptionsMenu();
                AudioPlayerActivity.this.updatePager();
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.23 */
    class AnonymousClass23 implements Runnable {
        private final /* synthetic */ boolean val$enable;

        AnonymousClass23(boolean z) {
            this.val$enable = z;
        }

        public void run() {
            AudioPlayerActivity.this.findViewById(C0436R.id.aplayer_broadcast).setSelected(this.val$enable);
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.24 */
    class AnonymousClass24 implements Runnable {
        private final /* synthetic */ int val$current;
        private final /* synthetic */ int val$total;

        AnonymousClass24(int i, int i2) {
            this.val$current = i;
            this.val$total = i2;
        }

        public void run() {
            AudioPlayerActivity.this.getSupportActionBar().setSubtitle(AudioPlayerActivity.this.getResources().getString(C0436R.string.player_num, new Object[]{Integer.valueOf(this.val$current), Integer.valueOf(this.val$total)}));
            AudioPlayerActivity.this.updatePager();
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.25 */
    class AnonymousClass25 implements Runnable {
        private final /* synthetic */ boolean val$playing;

        AnonymousClass25(boolean z) {
            this.val$playing = z;
        }

        public void run() {
            AudioPlayerActivity.this.updatePager();
            ((ImageView) AudioPlayerActivity.this.findViewById(C0436R.id.aplayer_play)).setImageDrawable(this.val$playing ? AudioPlayerActivity.pauseIcon : AudioPlayerActivity.playIcon);
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.26 */
    class AnonymousClass26 implements Runnable {
        private final /* synthetic */ long val$ms;
        private final /* synthetic */ int val$progr;

        AnonymousClass26(int i, long j) {
            this.val$progr = i;
            this.val$ms = j;
        }

        public void run() {
            ((SeekBar) AudioPlayerActivity.this.aview.findViewById(C0436R.id.aplayer_progress)).setProgress(this.val$progr);
            ((TextView) AudioPlayerActivity.this.aview.findViewById(C0436R.id.aplayer_time)).setText(String.format("%d:%02d", new Object[]{Long.valueOf((this.val$ms / 1000) / 60), Long.valueOf((this.val$ms / 1000) % 60)}));
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.27 */
    class AnonymousClass27 implements Runnable {
        private final /* synthetic */ int val$progr;

        AnonymousClass27(int i) {
            this.val$progr = i;
        }

        public void run() {
            ((SeekBar) AudioPlayerActivity.this.aview.findViewById(C0436R.id.aplayer_progress)).setSecondaryProgress(this.val$progr);
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.2 */
    class C01772 implements Runnable {
        C01772() {
        }

        public void run() {
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.3 */
    class C01783 implements OnPreDrawListener {
        C01783() {
        }

        public boolean onPreDraw() {
            int id = C0436R.id.aplayer_cover_pager;
            AudioPlayerActivity.this.aview.getViewTreeObserver().removeOnPreDrawListener(this);
            int add = 0;
            if (AudioPlayerActivity.this.findViewById(C0436R.id.aplayer_cover_pager) == null) {
                ((ViewGroup) AudioPlayerActivity.this.findViewById(C0436R.id.audio_player_cover_scroll)).getChildAt(0).setPadding(0, AudioPlayerActivity.this.getSupportActionBar().getHeight() + Global.scale(8.0f), 0, 0);
                LayoutParams lp = (LayoutParams) AudioPlayerActivity.this.findViewById(C0436R.id.audio_player_cover_scroll).getLayoutParams();
                lp.width = (AudioPlayerActivity.this.aview.getHeight() - AudioPlayerActivity.this.getSupportActionBar().getHeight()) - Global.scale((float) (VERSION.SDK_INT < 19 ? 0 : 16));
                AudioPlayerActivity.this.findViewById(C0436R.id.audio_player_cover_scroll).setLayoutParams(lp);
                AudioPlayerActivity.this.aview.forceLayout();
            }
            if (AudioPlayerActivity.this.findViewById(C0436R.id.padder) != null) {
                if (VERSION.SDK_INT < 19) {
                    ViewGroup.LayoutParams lp2 = AudioPlayerActivity.this.findViewById(C0436R.id.padder).getLayoutParams();
                    lp2.height = AudioPlayerActivity.this.getSupportActionBar().getHeight();
                    AudioPlayerActivity.this.findViewById(C0436R.id.padder).setLayoutParams(lp2);
                }
                add = Global.scale(8.0f);
            }
            if (VERSION.SDK_INT >= 19) {
                if (AudioPlayerActivity.this.findViewById(C0436R.id.aplayer_cover_pager) == null) {
                    id = C0436R.id.audio_player_cover_scroll;
                }
                lp = (LayoutParams) AudioPlayerActivity.this.findViewById(id).getLayoutParams();
                lp.topMargin = -(AudioPlayerActivity.this.getSupportActionBar().getHeight() + add);
                AudioPlayerActivity.this.findViewById(id).setLayoutParams(lp);
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.4 */
    class C01794 implements View.OnClickListener {
        C01794() {
        }

        public void onClick(View v) {
            if (AudioPlayerService.sharedInstance != null) {
                if (AudioPlayerService.sharedInstance.isPlaying()) {
                    AudioPlayerService.sharedInstance.unregisterRemoteControl();
                } else {
                    AudioPlayerService.sharedInstance.registerRemoteControl();
                }
                AudioPlayerService.sharedInstance.togglePlayPause();
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.5 */
    class C01805 implements View.OnClickListener {
        C01805() {
        }

        public void onClick(View v) {
            if (AudioPlayerActivity.this.viewPagerScrollState == 0) {
                AudioPlayerService.sharedInstance.nextTrack();
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.6 */
    class C01816 implements View.OnClickListener {
        C01816() {
        }

        public void onClick(View v) {
            if (AudioPlayerActivity.this.viewPagerScrollState == 0) {
                AudioPlayerService.sharedInstance.prevTrack();
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.7 */
    class C01827 implements OnSeekBarChangeListener {
        C01827() {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            AudioPlayerService.sharedInstance.seek(seekBar.getProgress());
            AudioPlayerActivity.this.canUpdateProgress = true;
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            AudioPlayerActivity.this.canUpdateProgress = false;
        }

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.8 */
    class C01838 implements View.OnClickListener {
        C01838() {
        }

        public void onClick(View arg0) {
            if (AudioPlayerService.sharedInstance != null) {
                AudioPlayerService.sharedInstance.setLoop(!AudioPlayerService.sharedInstance.isLoop());
                AudioPlayerActivity.this.findViewById(C0436R.id.aplayer_repeat).setSelected(AudioPlayerService.sharedInstance.isLoop());
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.9 */
    class C01849 implements View.OnClickListener {
        C01849() {
        }

        public void onClick(View arg0) {
            if (AudioPlayerService.sharedInstance != null) {
                AudioPlayerService.sharedInstance.setRandom(!AudioPlayerService.sharedInstance.isRandom());
                AudioPlayerActivity.this.findViewById(C0436R.id.aplayer_shuffle).setSelected(AudioPlayerService.sharedInstance.isRandom());
            }
        }
    }

    private class CoverInfoHolder {
        Bitmap cover;
        String lyrics;

        private CoverInfoHolder() {
        }
    }

    private static class NoPaddingTransitionDrawable extends TransitionDrawable {
        public NoPaddingTransitionDrawable(Drawable[] layers) {
            super(layers);
        }

        public boolean getPadding(Rect pad) {
            pad.set(0, 0, 0, 0);
            return false;
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.11 */
    class AnonymousClass11 implements OnPageChangeListener {
        int curPage;
        int lastState;
        private final /* synthetic */ ViewPager val$pager;

        /* renamed from: com.vkontakte.android.AudioPlayerActivity.11.1 */
        class C01681 implements Runnable {
            private final /* synthetic */ ViewPager val$pager;

            C01681(ViewPager viewPager) {
                this.val$pager = viewPager;
            }

            public void run() {
                this.val$pager.postInvalidate();
                AnonymousClass11.this.onPageScrollStateChanged(0);
                this.val$pager.setAdapter(this.val$pager.getAdapter());
                this.val$pager.setCurrentItem(this.val$pager.getAdapter().getCount() - 2, false);
            }
        }

        /* renamed from: com.vkontakte.android.AudioPlayerActivity.11.2 */
        class C01692 implements Runnable {
            private final /* synthetic */ ViewPager val$pager;

            C01692(ViewPager viewPager) {
                this.val$pager = viewPager;
            }

            public void run() {
                this.val$pager.postInvalidate();
                AnonymousClass11.this.onPageScrollStateChanged(0);
                this.val$pager.setAdapter(this.val$pager.getAdapter());
                this.val$pager.setCurrentItem(1, false);
            }
        }

        AnonymousClass11(ViewPager viewPager) {
            this.val$pager = viewPager;
        }

        public void onPageSelected(int pos) {
        }

        public void onPageScrolled(int page, float offset, int offsetPx) {
            if (this.lastState != 0 && AudioPlayerActivity.this.wasTouching) {
                if (page < this.curPage) {
                    offset = 1.0f - offset;
                }
                if (!(offset == 0.0f && page != this.curPage && this.lastState == 2) && offset >= 0.0f) {
                    AudioPlayerService.sharedInstance.setVolume(1.0f - offset);
                }
            }
        }

        public void onPageScrollStateChanged(int state) {
            Log.m528i("vk", "state changed " + state);
            AudioPlayerActivity.this.viewPagerScrollState = state;
            this.lastState = state;
            if (state == 0) {
                int pos = this.val$pager.getCurrentItem();
                realPos = pos <= 0 ? AudioPlayerService.sharedInstance.getPlaylistLength() - 1 : pos > AudioPlayerService.sharedInstance.getPlaylistLength() ? 0 : pos - 1;
                AudioPlayerActivity.this.wasTouching = false;
                if (pos == 0) {
                    this.val$pager.postDelayed(new C01681(this.val$pager), 10);
                }
                if (pos >= this.val$pager.getAdapter().getCount() - 1) {
                    this.val$pager.postDelayed(new C01692(this.val$pager), 10);
                }
                if (AudioPlayerService.sharedInstance.getPlaylistPosition() != realPos && pos - 1 == realPos) {
                    AudioPlayerService.sharedInstance.jumpToTrack(realPos);
                }
            }
            if (state == 1) {
                this.curPage = this.val$pager.getCurrentItem();
                AudioPlayerActivity.this.wasTouching = true;
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.12 */
    class AnonymousClass12 implements Callback {
        private final /* synthetic */ AudioFile val$file;

        AnonymousClass12(AudioFile audioFile) {
            this.val$file = audioFile;
        }

        public void success(int id) {
            Toast.makeText(AudioPlayerActivity.this, C0436R.string.audio_added, 0).show();
            Intent intent = new Intent(AudioCache.ACTION_FILE_ADDED);
            AudioPlayerService.sharedInstance.setCurrentFileIDs(Global.uid, id);
            this.val$file.oid = Global.uid;
            this.val$file.aid = id;
            intent.putExtra("file", this.val$file);
            AudioPlayerActivity.this.sendBroadcast(intent, permission.ACCESS_DATA);
            AudioPlayerActivity.this.invalidateOptionsMenu();
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(AudioPlayerActivity.this, C0436R.string.audio_add_error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.14 */
    class AnonymousClass14 implements AudioDelete.Callback {
        private final /* synthetic */ AudioFile val$f;
        private final /* synthetic */ boolean val$updateCover;

        AnonymousClass14(AudioFile audioFile, boolean z) {
            this.val$f = audioFile;
            this.val$updateCover = z;
        }

        public void success() {
            Toast.makeText(AudioPlayerActivity.this, C0436R.string.audio_deleted, 0).show();
            Intent intent = new Intent(AudioCache.ACTION_FILE_DELETED);
            intent.putExtra("aid", this.val$f.aid);
            AudioPlayerActivity.this.sendBroadcast(intent, permission.ACCESS_DATA);
            if (this.val$f.oldAid != 0) {
                AudioPlayerService.sharedInstance.resetCurrentFileIDs();
            } else {
                AudioPlayerService.sharedInstance.removeCurrentFile();
            }
            AudioPlayerActivity.this.invalidateOptionsMenu();
            if (this.val$updateCover) {
                AudioPlayerActivity.this.invalidatePager();
            }
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(AudioPlayerActivity.this, C0436R.string.error, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.19 */
    class AnonymousClass19 implements ImageLoadCallback {
        private final /* synthetic */ boolean val$force;

        /* renamed from: com.vkontakte.android.AudioPlayerActivity.19.1 */
        class C01721 implements Runnable {
            private final /* synthetic */ Bitmap val$bmp;
            private final /* synthetic */ boolean val$force;

            /* renamed from: com.vkontakte.android.AudioPlayerActivity.19.1.1 */
            class C01711 extends AnimatorListenerAdapter {
                boolean canceled;

                C01711() {
                    this.canceled = false;
                }

                public void onAnimationCancel(Animator anim) {
                    this.canceled = true;
                }

                public void onAnimationEnd(Animator animation) {
                    animation.removeListener(this);
                    if (!this.canceled) {
                        AudioPlayerActivity.this.updateCoverImage();
                    }
                }
            }

            C01721(Bitmap bitmap, boolean z) {
                this.val$bmp = bitmap;
                this.val$force = z;
            }

            public void run() {
                Drawable bg = AudioPlayerActivity.this.aview.getBackground();
                CoverBgDrawable bd = new CoverBgDrawable(this.val$bmp);
                AudioPlayerActivity.this.aview.setBackgroundDrawable(bd);
                bd.fadeIn(bg);
                if (VERSION.SDK_INT >= 19) {
                    AudioPlayerActivity.this.aview.setFitsSystemWindows(true);
                }
                if ((this.val$force || !AudioPlayerActivity.this.usePager()) && AudioPlayerActivity.this.getCurrentCoverScroller() != null) {
                    ImageView iv = (ImageView) AudioPlayerActivity.this.getCurrentCoverScroller().findViewById(C0436R.id.audio_player_cover);
                    if (iv.getDrawable() instanceof ColorDrawable) {
                        iv.setImageResource(C0436R.drawable.aplayer_cover_placeholder);
                    }
                    if (VERSION.SDK_INT >= 15) {
                        iv.animate().alpha(0.0f).setDuration(300).setListener(new C01711()).start();
                    } else {
                        AudioPlayerActivity.this.updateCoverImage();
                    }
                }
            }
        }

        /* renamed from: com.vkontakte.android.AudioPlayerActivity.19.2 */
        class C01732 implements Runnable {
            private final /* synthetic */ Drawable val$bg;

            C01732(Drawable drawable) {
                this.val$bg = drawable;
            }

            public void run() {
                if (Global.isTouchwiz()) {
                    AudioPlayerActivity.this.aview.setBackgroundResource(C0436R.drawable.bg_audio_player);
                } else {
                    TransitionDrawable td = new NoPaddingTransitionDrawable(new Drawable[]{this.val$bg, AudioPlayerActivity.this.getResources().getDrawable(C0436R.drawable.bg_audio_player)});
                    td.setCrossFadeEnabled(false);
                    AudioPlayerActivity.this.aview.setBackgroundDrawable(td);
                    td.startTransition(LocationStatusCodes.GEOFENCE_NOT_AVAILABLE);
                }
                if (VERSION.SDK_INT >= 19) {
                    AudioPlayerActivity.this.aview.setFitsSystemWindows(true);
                }
            }
        }

        AnonymousClass19(boolean z) {
            this.val$force = z;
        }

        public void onImageLoaded(Bitmap bmp, int oid, int aid) {
            if (AudioPlayerActivity.this.file.aid == aid && AudioPlayerActivity.this.file.oid == oid) {
                AudioPlayerActivity.this.runOnUiThread(new C01721(bmp, this.val$force));
            }
        }

        public void notAvailable(int oid, int aid) {
            if (AudioPlayerActivity.this.file.aid == aid && AudioPlayerActivity.this.file.oid == oid) {
                if (!AudioPlayerActivity.this.usePager()) {
                    AudioPlayerActivity.this.updateCoverImage();
                }
                Drawable bg = AudioPlayerActivity.this.aview.getBackground();
                if (!(bg instanceof LayerDrawable)) {
                    AudioPlayerActivity.this.runOnUiThread(new C01732(bg));
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.AudioPlayerActivity.22 */
    class AnonymousClass22 implements AudioGetLyrics.Callback {
        private final /* synthetic */ View val$cur;

        AnonymousClass22(View view) {
            this.val$cur = view;
        }

        public void success(String text, int lid) {
            if (AudioPlayerActivity.this.file.lyricsID == lid) {
                ((TextView) this.val$cur.findViewById(C0436R.id.audio_player_lyrics)).setText(text);
                this.val$cur.findViewById(C0436R.id.audio_player_cover_scroll).setEnabled(true);
                AudioCache.saveLyrics(AudioPlayerActivity.this.file.oid, AudioPlayerActivity.this.file.aid, text);
                AudioPlayerActivity.this.invalidateOptionsMenu();
            }
        }

        public void fail(int ecode, String emsg) {
        }
    }

    private class CoverPagerAdapter extends PagerAdapter {

        /* renamed from: com.vkontakte.android.AudioPlayerActivity.CoverPagerAdapter.2 */
        class C01872 implements OnPreDrawListener {
            private final /* synthetic */ View val$v;

            C01872(View view) {
                this.val$v = view;
            }

            public boolean onPreDraw() {
                this.val$v.getViewTreeObserver().removeOnPreDrawListener(this);
                ((ViewGroup) this.val$v.findViewById(C0436R.id.audio_player_cover_scroll)).getChildAt(0).setPadding(0, AudioPlayerActivity.this.getSupportActionBar().getHeight() + Global.scale(5.0f), 0, 0);
                return true;
            }
        }

        /* renamed from: com.vkontakte.android.AudioPlayerActivity.CoverPagerAdapter.1 */
        class C12671 implements ImageLoadCallback {
            private final /* synthetic */ View val$v;

            /* renamed from: com.vkontakte.android.AudioPlayerActivity.CoverPagerAdapter.1.1 */
            class C01851 implements Runnable {
                private final /* synthetic */ Bitmap val$bmp;
                private final /* synthetic */ View val$v;

                C01851(View view, Bitmap bitmap) {
                    this.val$v = view;
                    this.val$bmp = bitmap;
                }

                public void run() {
                    ((ImageView) this.val$v.findViewById(C0436R.id.audio_player_cover)).setImageBitmap(this.val$bmp);
                }
            }

            /* renamed from: com.vkontakte.android.AudioPlayerActivity.CoverPagerAdapter.1.2 */
            class C01862 implements Runnable {
                private final /* synthetic */ View val$v;

                C01862(View view) {
                    this.val$v = view;
                }

                public void run() {
                    ((ImageView) this.val$v.findViewById(C0436R.id.audio_player_cover)).setImageResource(C0436R.drawable.aplayer_cover_placeholder);
                }
            }

            C12671(View view) {
                this.val$v = view;
            }

            public void onImageLoaded(Bitmap bmp, int oid, int aid) {
                AudioPlayerActivity.this.runOnUiThread(new C01851(this.val$v, bmp));
            }

            public void notAvailable(int oid, int aid) {
                AudioPlayerActivity.this.runOnUiThread(new C01862(this.val$v));
            }
        }

        private CoverPagerAdapter() {
        }

        public int getCount() {
            if (AudioPlayerService.sharedInstance != null) {
                return AudioPlayerService.sharedInstance.getPlaylistLength() + 2;
            }
            return 0;
        }

        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public Object instantiateItem(ViewGroup container, int position) {
            Log.m528i("vk", "instantiate " + position);
            realPos = position <= 0 ? AudioPlayerService.sharedInstance.getPlaylistLength() - 1 : position > AudioPlayerService.sharedInstance.getPlaylistLength() ? 0 : position - 1;
            position--;
            View v = View.inflate(AudioPlayerActivity.this, C0436R.layout.audio_player_cover_scroll, null);
            container.addView(v);
            AudioPlayerActivity.this.coverViews.put(Integer.valueOf(position), v);
            AudioFile file = AudioPlayerService.sharedInstance.getPlaylistItem(realPos);
            if (file == null) {
                file = new AudioFile();
            }
            ((AlbumScrollView) v).setEnabled(false);
            AlbumArtRetriever.getCoverImage(file.aid, file.oid, 0, new C12671(v));
            if (AudioPlayerActivity.this.getSupportActionBar().getHeight() == 0) {
                v.getViewTreeObserver().addOnPreDrawListener(new C01872(v));
            } else {
                ((ViewGroup) v.findViewById(C0436R.id.audio_player_cover_scroll)).getChildAt(0).setPadding(0, AudioPlayerActivity.this.getSupportActionBar().getHeight() + Global.scale(5.0f), 0, 0);
            }
            return v;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            Log.m528i("vk", "destroy " + position);
            View v = (View) object;
            AudioPlayerActivity.this.coverViews.remove(Integer.valueOf(position - 1));
            container.removeView(v);
        }
    }

    public AudioPlayerActivity() {
        this.canUpdateProgress = true;
        this.isRegistered = false;
        this.coverViews = new HashMap();
        this.firstInfoUpdate = true;
        this.animArtist = ACRAConstants.DEFAULT_STRING_VALUE;
        this.animTitle = ACRAConstants.DEFAULT_STRING_VALUE;
        this.receiver = new C01741();
    }

    public void onCreate(Bundle b) {
        boolean isTouchWiz;
        requestWindowFeature(9);
        super.onCreate(b);
        if (VERSION.SDK_INT >= 19) {
            getWindow().addFlags(134217728);
        }
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(C0436R.drawable.bg_actionbar_black_transparent));
        setTitle((int) C0436R.string.now_playing);
        IntentFilter filter = new IntentFilter();
        filter.addAction(AudioCache.ACTION_ALBUM_ART_AVAILABLE);
        filter.addAction(AudioPlayerService.ACTION_UPDATE_AUDIO_LISTS);
        VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
        this.viewPagerScrollState = 0;
        this.aview = View.inflate(this, C0436R.layout.audio_player, null);
        this.aview.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.aview.post(new C01772());
        this.aview.getViewTreeObserver().addOnPreDrawListener(new C01783());
        setContentView(this.aview);
        ((ViewGroup) this.aview).setClipToPadding(false);
        if (VERSION.SDK_INT >= 19) {
            this.aview.setFitsSystemWindows(true);
        }
        findViewById(C0436R.id.aplayer_play).setOnClickListener(new C01794());
        findViewById(C0436R.id.aplayer_next).setOnClickListener(new C01805());
        findViewById(C0436R.id.aplayer_prev).setOnClickListener(new C01816());
        ((SeekBar) findViewById(C0436R.id.aplayer_progress)).setOnSeekBarChangeListener(new C01827());
        ((TextView) findViewById(C0436R.id.aplayer_artist)).setText(ACRAConstants.DEFAULT_STRING_VALUE);
        ((TextView) findViewById(C0436R.id.aplayer_title)).setText(ACRAConstants.DEFAULT_STRING_VALUE);
        if (AudioPlayerService.sharedInstance != null) {
            findViewById(C0436R.id.aplayer_repeat).setSelected(AudioPlayerService.sharedInstance.isLoop());
            findViewById(C0436R.id.aplayer_shuffle).setSelected(AudioPlayerService.sharedInstance.isRandom());
        }
        findViewById(C0436R.id.aplayer_repeat).setOnClickListener(new C01838());
        findViewById(C0436R.id.aplayer_shuffle).setOnClickListener(new C01849());
        findViewById(C0436R.id.aplayer_broadcast).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (AudioPlayerService.sharedInstance != null) {
                    AudioPlayerActivity.this.showBroadcastDialog();
                }
            }
        });
        if (playIcon == null) {
            playIcon = getResources().getDrawable(C0436R.drawable.ic_aplayer_play);
            pauseIcon = getResources().getDrawable(C0436R.drawable.ic_aplayer_pause);
        }
        ((TextView) findViewById(C0436R.id.aplayer_artist)).setTypeface(Fonts.getRobotoLight());
        ((TextView) findViewById(C0436R.id.aplayer_title)).setTypeface(Fonts.getRobotoLight());
        if (new File("/system/app/SecLauncher2.apk").exists() || new File("/system/app/SecLauncher3.apk").exists()) {
            isTouchWiz = true;
        } else {
            isTouchWiz = false;
        }
        if (isTouchWiz) {
            ((TextView) findViewById(C0436R.id.aplayer_artist)).setMovementMethod(new ScrollingMovementMethod());
            ((TextView) findViewById(C0436R.id.aplayer_title)).setMovementMethod(new ScrollingMovementMethod());
        } else {
            findViewById(C0436R.id.aplayer_artist).setSelected(true);
            findViewById(C0436R.id.aplayer_title).setSelected(true);
        }
        TextView titleView = (TextView) findViewById(C0436R.id.aplayer_title);
        ((TextView) findViewById(C0436R.id.aplayer_time)).setTypeface(Fonts.getRobotoLightItalic());
        ((TextView) findViewById(C0436R.id.aplayer_duration)).setTypeface(Fonts.getRobotoLightItalic());
        if (AudioPlayerService.sharedInstance != null) {
            findViewById(C0436R.id.aplayer_broadcast).setSelected(AudioPlayerService.sharedInstance.isBroadcast());
        }
        this.coverOid = 0;
        this.coverAid = 0;
        ViewPager pager = (ViewPager) this.aview.findViewById(C0436R.id.aplayer_cover_pager);
        if (pager != null) {
            pager.setAdapter(new CoverPagerAdapter());
            if (AudioPlayerService.sharedInstance != null) {
                pager.setCurrentItem(AudioPlayerService.sharedInstance.getPlaylistPosition() + 1, false);
            }
            pager.setOnPageChangeListener(new AnonymousClass11(pager));
            if (VERSION.SDK_INT >= 14) {
                pager.setPageTransformer(true, new DepthPageTransformer());
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
    }

    public void onResume() {
        super.onResume();
        setVolumeControlStream(3);
        if (AudioPlayerService.sharedInstance == null) {
            finish();
        } else {
            AudioPlayerService.sharedInstance.registerPlayerView(this);
        }
    }

    public void onPause() {
        super.onPause();
        setVolumeControlStream(ExploreByTouchHelper.INVALID_ID);
        if (AudioPlayerService.sharedInstance != null) {
            if (this.viewPagerScrollState != 0) {
                AudioPlayerService.sharedInstance.setVolume(1.0f);
            }
            AudioPlayerService.sharedInstance.unregisterPlayerView(this);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(C0436R.menu.audio, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z = false;
        if (AudioPlayerService.sharedInstance != null) {
            AudioFile f = AudioPlayerService.sharedInstance.getCurrentFile();
            if (f != null) {
                boolean z2;
                boolean cached = AudioCache.isCached(f.oid, f.aid);
                Log.m528i("vk", "IS CACHED = " + cached);
                menu.findItem(C0436R.id.audio_cache).setIcon(cached ? C0436R.drawable.ic_aplayer_cache_active : C0436R.drawable.ic_aplayer_cache_normal);
                menu.findItem(C0436R.id.audio_cache).setChecked(cached);
                MenuItem findItem = menu.findItem(C0436R.id.audio_cache);
                if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("enableAudioCache", true) || cached) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                findItem.setEnabled(z2);
                if (f.oid == Global.uid) {
                    findItem = menu.findItem(C0436R.id.audio_add);
                    if (f.oldAid != 0) {
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    findItem.setVisible(z2);
                    if (f.oldAid != 0) {
                        menu.findItem(C0436R.id.audio_add).setIcon((int) C0436R.drawable.ic_aplayer_added);
                    }
                    MenuItem findItem2 = menu.findItem(C0436R.id.audio_delete);
                    if (!f.fromAttachment) {
                        z = true;
                    }
                    findItem2.setVisible(z);
                } else {
                    menu.findItem(C0436R.id.audio_add).setVisible(true);
                    menu.findItem(C0436R.id.audio_add).setIcon((int) C0436R.drawable.ic_ab_add);
                    menu.findItem(C0436R.id.audio_delete).setVisible(false);
                }
                menu.findItem(C0436R.id.audio_lyrics).setEnabled(haveLyrics());
                Log.m528i("vk", "lyrics enabled=" + menu.findItem(C0436R.id.audio_lyrics).isEnabled());
            }
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case C0436R.id.audio_add:
                addCurrent();
                return true;
            case C0436R.id.audio_list:
                AudioPlaylistFragment pf = new AudioPlaylistFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.addToBackStack(null);
                pf.show(ft, "dialog");
                return true;
            case C0436R.id.audio_cache:
                toggleCachedState();
                return true;
            case C0436R.id.audio_lyrics:
                showLyrics();
                return true;
            case C0436R.id.audio_delete:
                confirmAndDelete();
                return true;
            case C0436R.id.audio_search_artist:
                searchArtist();
                return true;
            case C0436R.id.send_to_friend:
                NewsEntry e = new NewsEntry();
                e.attachments = new ArrayList();
                e.attachments.add(new AudioAttachment(AudioPlayerService.sharedInstance.getCurrentFile()));
                e.type = -1;
                Intent intent = new Intent(this, RepostActivity.class);
                intent.putExtra("post", e);
                intent.putExtra("msg", true);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onNavigateUp() {
        finish();
        return true;
    }

    private void toggleCachedState() {
        if (AudioPlayerService.sharedInstance == null) {
            return;
        }
        if (AudioPlayerService.sharedInstance.isEnoughSpaceToCache()) {
            AudioFile file = AudioPlayerService.sharedInstance.getCurrentFile();
            if (file != null) {
                if (AudioCache.isCached(file.oid, file.aid)) {
                    AudioCache.deleteCurrent();
                } else {
                    AudioCache.saveCurrent(true);
                }
                invalidateOptionsMenu();
                return;
            }
            return;
        }
        Toast.makeText(this, C0436R.string.no_space_to_cache, 0).show();
    }

    private void addCurrent() {
        if (AudioPlayerService.sharedInstance != null) {
            AudioFile file = AudioPlayerService.sharedInstance.getCurrentFile();
            if (file.oid == Global.uid) {
                deleteFile(file, false);
            } else if (file != null) {
                new AudioAdd(file.oid, file.aid, 0).setCallback(new AnonymousClass12(file)).exec((Activity) this);
            }
        }
    }

    private void confirmAndDelete() {
        if (AudioPlayerService.sharedInstance != null) {
            AudioFile file = AudioPlayerService.sharedInstance.getCurrentFile();
            if (file != null) {
                new Builder(this).setTitle(C0436R.string.confirm).setMessage(C0436R.string.delete_audio_confirm).setPositiveButton(C0436R.string.yes, new AnonymousClass13(file)).setNegativeButton(C0436R.string.no, null).show();
            }
        }
    }

    private void deleteFile(AudioFile f, boolean updateCover) {
        new AudioDelete(f.oid, f.aid).setCallback(new AnonymousClass14(f, updateCover)).exec((Activity) this);
    }

    private void searchArtist() {
        if (AudioPlayerService.sharedInstance != null) {
            AudioFile file = AudioPlayerService.sharedInstance.getCurrentFile();
            if (file != null) {
                Bundle args = new Bundle();
                args.putCharSequence("search", file.artist);
                Navigate.to("AudioListFragment", args, this);
            }
        }
    }

    private View getCurrentCoverScroller() {
        if (((ViewPager) this.aview.findViewById(C0436R.id.aplayer_cover_pager)) == null || AudioPlayerService.sharedInstance == null) {
            return this.aview.findViewById(C0436R.id.audio_player_cover_scroll);
        }
        return (View) this.coverViews.get(Integer.valueOf(AudioPlayerService.sharedInstance.getPlaylistPosition()));
    }

    private boolean usePager() {
        return this.aview.findViewById(C0436R.id.aplayer_cover_pager) != null;
    }

    public void updatePager() {
        ViewPager pager = (ViewPager) this.aview.findViewById(C0436R.id.aplayer_cover_pager);
        if (pager != null) {
            pager.getAdapter().notifyDataSetChanged();
        }
    }

    public void invalidatePager() {
        ViewPager pager = (ViewPager) this.aview.findViewById(C0436R.id.aplayer_cover_pager);
        if (pager != null) {
            pager.setAdapter(pager.getAdapter());
        }
    }

    public void displayInfo(AudioFile f) {
        this.file = f;
        if (VERSION.SDK_INT < 14 || this.firstInfoUpdate) {
            ((TextView) findViewById(C0436R.id.aplayer_artist)).setText(f.artist);
            ((TextView) findViewById(C0436R.id.aplayer_title)).setText(f.title);
            this.firstInfoUpdate = false;
        } else {
            TextView artist = (TextView) findViewById(C0436R.id.aplayer_artist);
            TextView title = (TextView) findViewById(C0436R.id.aplayer_title);
            String curArtist = artist.getText().toString();
            String curTitle = title.getText().toString();
            if (!((curArtist.equals(f.artist) && curTitle.equals(f.title)) || (f.artist.equals(this.animArtist) && f.title.equals(this.animTitle)))) {
                ObjectAnimator oa;
                this.animArtist = f.artist;
                this.animTitle = f.title;
                AnimatorListenerAdapter listener = new AnonymousClass15(artist, f, title);
                ArrayList<Animator> anims = new ArrayList();
                if (!curArtist.equals(f.artist)) {
                    oa = ObjectAnimator.ofFloat(artist, "alpha", new float[]{1.0f, 0.0f});
                    oa.setDuration(200);
                    oa.setRepeatMode(2);
                    oa.setRepeatCount(1);
                    oa.addListener(listener);
                    anims.add(oa);
                }
                if (!curTitle.equals(f.title)) {
                    oa = ObjectAnimator.ofFloat(title, "alpha", new float[]{1.0f, 0.0f});
                    oa.setDuration(200);
                    oa.setRepeatMode(2);
                    oa.setRepeatCount(1);
                    if (curArtist.equals(f.artist)) {
                        oa.addListener(listener);
                    }
                    anims.add(oa);
                }
                AnimatorSet set = new AnimatorSet();
                set.playTogether(anims);
                set.start();
            }
        }
        TextView textView = (TextView) findViewById(C0436R.id.aplayer_duration);
        r15 = new Object[2];
        r15[0] = Integer.valueOf(f.duration / 60);
        r15[1] = Integer.valueOf(f.duration % 60);
        textView.setText(String.format("%d:%02d", r15));
        findViewById(C0436R.id.aplayer_artist).setSelected(true);
        findViewById(C0436R.id.aplayer_title).setSelected(true);
        ViewPager pager = (ViewPager) this.aview.findViewById(C0436R.id.aplayer_cover_pager);
        if (pager != null) {
            pager.setEnabled(AudioPlayerService.sharedInstance.getPlaylistLength() > 1);
            int position = pager.getCurrentItem();
            realPos = position <= 0 ? AudioPlayerService.sharedInstance.getPlaylistLength() - 1 : position > AudioPlayerService.sharedInstance.getPlaylistLength() ? 0 : position - 1;
            if (this.viewPagerScrollState == 0 && realPos != AudioPlayerService.sharedInstance.getPlaylistPosition()) {
                this.aview.postDelayed(new AnonymousClass16(pager), 300);
            }
        }
        updateCover(false);
        updateLyrics();
    }

    public void forceUpdateCover() {
        this.coverOid = 0;
        this.coverAid = 0;
        updateCover(true);
    }

    private void showBroadcastDialog() {
        ArrayList<Group> grps = new ArrayList();
        Groups.getAdminedGroups(grps);
        ArrayList<String> items = new ArrayList();
        ArrayList<Integer> ids = new ArrayList();
        ArrayList<Integer> targets = AudioPlayerService.sharedInstance.getBroadcastTargets();
        items.add(getResources().getString(C0436R.string.my_page));
        Iterator it = grps.iterator();
        while (it.hasNext()) {
            items.add(((Group) it.next()).name);
        }
        boolean[] checked = new boolean[items.size()];
        checked[0] = targets.contains(Integer.valueOf(Global.uid));
        ids.add(Integer.valueOf(Global.uid));
        int i = 1;
        it = grps.iterator();
        while (it.hasNext()) {
            Group g = (Group) it.next();
            checked[i] = targets.contains(Integer.valueOf(-g.id));
            ids.add(Integer.valueOf(-g.id));
            i++;
        }
        new Builder(this).setTitle(C0436R.string.audio_broadcast).setMultiChoiceItems((CharSequence[]) items.toArray(new String[0]), checked, new AnonymousClass17(checked)).setPositiveButton(C0436R.string.ok, new AnonymousClass18(checked, ids)).setNegativeButton(C0436R.string.cancel, null).show();
    }

    public void updateCover(boolean force) {
        if (this.file != null) {
            Log.m525d("vk", "updateCover " + force + " " + this.coverAid + "==" + this.file.aid + ", " + this.coverOid + "==" + this.file.oid);
        }
        if (this.file == null) {
            return;
        }
        if (this.coverAid != this.file.aid || this.coverOid != this.file.oid) {
            this.coverAid = this.file.aid;
            this.coverOid = this.file.oid;
            doUpdateCover(force);
        }
    }

    private void doUpdateCover(boolean force) {
        AlbumArtRetriever.getCoverImage(this.file.aid, this.file.oid, 2, new AnonymousClass19(force));
    }

    private void updateCoverImage() {
        AlbumArtRetriever.getCoverImage(this.file.aid, this.file.oid, 0, new ImageLoadCallback() {

            /* renamed from: com.vkontakte.android.AudioPlayerActivity.20.1 */
            class C01751 implements Runnable {
                private final /* synthetic */ int val$aid;
                private final /* synthetic */ Bitmap val$bmp;
                private final /* synthetic */ int val$oid;

                C01751(int i, int i2, Bitmap bitmap) {
                    this.val$aid = i;
                    this.val$oid = i2;
                    this.val$bmp = bitmap;
                }

                public void run() {
                    if (AudioPlayerActivity.this.file.aid == this.val$aid && AudioPlayerActivity.this.file.oid == this.val$oid) {
                        View scroller = AudioPlayerActivity.this.getCurrentCoverScroller();
                        if (scroller != null) {
                            ImageView iv = (ImageView) scroller.findViewById(C0436R.id.audio_player_cover);
                            if (iv != null) {
                                iv.setImageBitmap(this.val$bmp);
                                if (VERSION.SDK_INT >= 14) {
                                    iv.animate().alpha(1.0f).setDuration(300).start();
                                }
                            }
                        }
                    }
                }
            }

            /* renamed from: com.vkontakte.android.AudioPlayerActivity.20.2 */
            class C01762 implements Runnable {
                private final /* synthetic */ int val$aid;
                private final /* synthetic */ int val$oid;

                C01762(int i, int i2) {
                    this.val$aid = i;
                    this.val$oid = i2;
                }

                public void run() {
                    if (AudioPlayerActivity.this.file.aid == this.val$aid && AudioPlayerActivity.this.file.oid == this.val$oid && AudioPlayerActivity.this.getCurrentCoverScroller() != null) {
                        ImageView iv = (ImageView) AudioPlayerActivity.this.getCurrentCoverScroller().findViewById(C0436R.id.audio_player_cover);
                        iv.setImageResource(C0436R.drawable.aplayer_cover_placeholder);
                        if (VERSION.SDK_INT >= 14) {
                            iv.animate().alpha(1.0f).setDuration(300).start();
                        }
                    }
                }
            }

            public void onImageLoaded(Bitmap bmp, int oid, int aid) {
                AudioPlayerActivity.this.runOnUiThread(new C01751(aid, oid, bmp));
            }

            public void notAvailable(int oid, int aid) {
                AudioPlayerActivity.this.runOnUiThread(new C01762(aid, oid));
            }
        });
    }

    public void updateLyrics() {
        View cur = getCurrentCoverScroller();
        if (cur == null) {
            this.aview.postDelayed(new Runnable() {
                public void run() {
                    AudioPlayerActivity.this.updateLyrics();
                }
            }, 10);
            return;
        }
        ((ScrollView) cur.findViewById(C0436R.id.audio_player_cover_scroll)).scrollTo(0, 0);
        cur.findViewById(C0436R.id.audio_player_cover_scroll).setEnabled(false);
        if (this.file.lyricsID > 0) {
            String l = AudioCache.getLyrics(this.file.oid, this.file.aid);
            if (l != null) {
                ((TextView) cur.findViewById(C0436R.id.audio_player_lyrics)).setText(l);
                cur.findViewById(C0436R.id.audio_player_cover_scroll).setEnabled(true);
                invalidateOptionsMenu();
                return;
            }
            new AudioGetLyrics(this.file.lyricsID).setCallback(new AnonymousClass22(cur)).exec((Activity) this);
        }
    }

    public boolean haveLyrics() {
        View cur = getCurrentCoverScroller();
        if (cur == null) {
            return false;
        }
        return cur.findViewById(C0436R.id.audio_player_cover_scroll).isEnabled();
    }

    public void showLyrics() {
        ScrollView sv = (ScrollView) getCurrentCoverScroller().findViewById(C0436R.id.audio_player_cover_scroll);
        if (sv.getScrollY() < sv.getHeight() / 2) {
            sv.smoothScrollTo(0, sv.getHeight() / 2);
        }
    }

    public void enableBroadcast(boolean enable) {
        runOnUiThread(new AnonymousClass23(enable));
    }

    public void enableControlButtons(boolean enable) {
        findViewById(C0436R.id.aplayer_play).setEnabled(enable);
        findViewById(C0436R.id.aplayer_next).setEnabled(enable);
        findViewById(C0436R.id.aplayer_prev).setEnabled(enable);
    }

    public void setNumber(int current, int total) {
        runOnUiThread(new AnonymousClass24(current, total));
    }

    public void setPlaying(boolean playing) {
        runOnUiThread(new AnonymousClass25(playing));
    }

    public void setPosition(int progr, long ms) {
        if (this.canUpdateProgress && this.aview != null) {
            runOnUiThread(new AnonymousClass26(progr, ms));
        }
    }

    public void setBuffered(int progr) {
        runOnUiThread(new AnonymousClass27(progr));
    }
}
