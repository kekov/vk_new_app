package com.vkontakte.android;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.widget.Toast;
import com.facebook.NativeProtocol;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.api.APIUtils;
import com.vkontakte.android.api.AudioGetById;
import com.vkontakte.android.api.BoardGetTopics;
import com.vkontakte.android.api.BoardTopic;
import com.vkontakte.android.api.PhotoAlbum;
import com.vkontakte.android.api.PhotosGetAlbums;
import com.vkontakte.android.api.PhotosGetAlbums.Callback;
import com.vkontakte.android.api.VideoFile;
import com.vkontakte.android.api.VideoGetById;
import com.vkontakte.android.api.WallGetById;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Friends.GetUsersCallback;
import com.vkontakte.android.data.Messages;
import com.vkontakte.android.data.Messages.GetChatUsersCallback;
import com.vkontakte.android.mediapicker.gl.GLFilterContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public class LinkRedirActivity extends Activity {
    boolean otherApp;

    /* renamed from: com.vkontakte.android.LinkRedirActivity.9 */
    class C02899 implements Runnable {
        C02899() {
        }

        public void run() {
            Toast.makeText(LinkRedirActivity.this, C0436R.string.link_not_supported, 0).show();
        }
    }

    /* renamed from: com.vkontakte.android.LinkRedirActivity.12 */
    class AnonymousClass12 implements GetChatUsersCallback {
        private final /* synthetic */ int val$uid;

        AnonymousClass12(int i) {
            this.val$uid = i;
        }

        public void onUsersLoaded(ArrayList<ChatUser> arrayList, String title, String photo) {
            Bundle args = new Bundle();
            args.putInt("id", this.val$uid);
            args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, title);
            Navigate.to("ChatFragment", args, LinkRedirActivity.this);
        }
    }

    /* renamed from: com.vkontakte.android.LinkRedirActivity.1 */
    class C12931 implements Callback {
        C12931() {
        }

        public void success(ArrayList<PhotoAlbum> albums, ArrayList<PhotoAlbum> system) {
            albums.addAll(system);
            Iterator it = albums.iterator();
            while (it.hasNext()) {
                PhotoAlbum a = (PhotoAlbum) it.next();
                if (a.id == -9000) {
                    Bundle args = new Bundle();
                    args.putParcelable("album", a);
                    Navigate.to("PhotoListFragment", args, LinkRedirActivity.this);
                }
            }
            LinkRedirActivity.this.finish();
        }

        public void fail(int ecode, String emsg) {
            LinkRedirActivity.this.finish();
        }
    }

    /* renamed from: com.vkontakte.android.LinkRedirActivity.2 */
    class C12942 extends APIHandler {
        private final /* synthetic */ int val$aid;
        private final /* synthetic */ Uri val$uri;

        C12942(int i, Uri uri) {
            this.val$aid = i;
            this.val$uri = uri;
        }

        public void success(JSONObject o) {
            boolean z = false;
            try {
                JSONArray ja = APIUtils.unwrapArray(o, "response").array;
                PhotoAlbum a = null;
                for (int i = 0; i < ja.length(); i++) {
                    if (ja.getJSONObject(i).getInt("id") == this.val$aid) {
                        a = new PhotoAlbum(ja.getJSONObject(i));
                        break;
                    }
                }
                if (a == null) {
                    Toast.makeText(LinkRedirActivity.this, C0436R.string.access_error, 0).show();
                    LinkRedirActivity.this.finish();
                    return;
                }
                LinkRedirActivity.this.finish();
                Bundle args = new Bundle();
                args.putParcelable("album", a);
                String str = "PhotoListFragment";
                Activity activity = LinkRedirActivity.this;
                if (VERSION.SDK_INT >= 14 && !Build.BRAND.toLowerCase().contains("zte")) {
                    z = true;
                }
                Navigate.to(str, args, activity, z, -1, -1);
            } catch (Throwable x) {
                Log.m532w("vk", x);
                LinkRedirActivity.this.finish();
                LinkRedirActivity.this.openBrowser(this.val$uri);
            }
        }

        public void fail(int ecode, String emsg) {
            LinkRedirActivity.this.finish();
            LinkRedirActivity.this.openBrowser(this.val$uri);
        }
    }

    /* renamed from: com.vkontakte.android.LinkRedirActivity.3 */
    class C12953 implements VideoGetById.Callback {
        private final /* synthetic */ Uri val$uri;

        C12953(Uri uri) {
            this.val$uri = uri;
        }

        public void success(VideoFile video) {
            LinkRedirActivity.this.finish();
            Intent intent = new Intent(LinkRedirActivity.this, NewVideoPlayerActivity.class);
            intent.putExtra("file", video);
            LinkRedirActivity.this.startActivity(intent);
        }

        public void fail(int ecode, String emsg) {
            LinkRedirActivity.this.finish();
            LinkRedirActivity.this.openBrowser(this.val$uri);
        }
    }

    /* renamed from: com.vkontakte.android.LinkRedirActivity.4 */
    class C12964 implements BoardGetTopics.Callback {
        private final /* synthetic */ int val$oid;
        private final /* synthetic */ int val$tid;
        private final /* synthetic */ Uri val$uri;

        C12964(int i, int i2, Uri uri) {
            this.val$oid = i;
            this.val$tid = i2;
            this.val$uri = uri;
        }

        public void success(int total, ArrayList<BoardTopic> topics, boolean canCreate, int order) {
            LinkRedirActivity.this.finish();
            BoardTopic topic = (BoardTopic) topics.get(0);
            Bundle args = new Bundle();
            args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, topic.title);
            args.putInt("gid", this.val$oid);
            args.putInt("tid", this.val$tid);
            if ((topic.flags & 1) > 0) {
                args.putInt("is_closed", 1);
            }
            Navigate.to("BoardTopicViewFragment", args, LinkRedirActivity.this);
        }

        public void fail(int ecode, String emsg) {
            LinkRedirActivity.this.finish();
            LinkRedirActivity.this.openBrowser(this.val$uri);
        }
    }

    /* renamed from: com.vkontakte.android.LinkRedirActivity.5 */
    class C12975 extends APIHandler {
        private final /* synthetic */ Uri val$uri;

        /* renamed from: com.vkontakte.android.LinkRedirActivity.5.1 */
        class C02851 implements Runnable {
            C02851() {
            }

            public void run() {
                Toast.makeText(LinkRedirActivity.this, C0436R.string.error, 0).show();
            }
        }

        C12975(Uri uri) {
            this.val$uri = uri;
        }

        public void success(JSONObject o) {
            try {
                JSONObject jp = o.getJSONArray("response").getJSONObject(0);
                LinkRedirActivity.this.finish();
                Photo photo = new Photo(jp);
                ArrayList<Photo> al = new ArrayList();
                al.add(photo);
                Bundle args = new Bundle();
                args.putParcelableArrayList("list", al);
                args.putInt(GLFilterContext.AttributePosition, 0);
                Navigate.to("PhotoViewerFragment", args, LinkRedirActivity.this, true, 0, 0);
            } catch (Exception e) {
                LinkRedirActivity.this.runOnUiThread(new C02851());
            }
        }

        public void fail(int ecode, String emsg) {
            LinkRedirActivity.this.finish();
            LinkRedirActivity.this.openBrowser(this.val$uri);
        }
    }

    /* renamed from: com.vkontakte.android.LinkRedirActivity.6 */
    class C12986 implements WallGetById.Callback {
        private final /* synthetic */ Uri val$uri;

        /* renamed from: com.vkontakte.android.LinkRedirActivity.6.1 */
        class C02861 implements Runnable {
            C02861() {
            }

            public void run() {
                Toast.makeText(LinkRedirActivity.this, C0436R.string.post_not_found, 0).show();
            }
        }

        /* renamed from: com.vkontakte.android.LinkRedirActivity.6.2 */
        class C02872 implements Runnable {
            C02872() {
            }

            public void run() {
                Toast.makeText(LinkRedirActivity.this, C0436R.string.err_text, 0).show();
            }
        }

        C12986(Uri uri) {
            this.val$uri = uri;
        }

        public void success(NewsEntry[] news) {
            LinkRedirActivity.this.finish();
            if (news.length == 0) {
                LinkRedirActivity.this.runOnUiThread(new C02861());
                return;
            }
            Bundle args = new Bundle();
            args.putParcelable("entry", news[0]);
            if (this.val$uri.getQueryParameter("reply") != null) {
                args.putInt("comment", LinkRedirActivity.this.safeParseInt(this.val$uri.getQueryParameter("reply")));
            }
            Navigate.to("PostViewFragment", args, LinkRedirActivity.this);
        }

        public void fail(int ecode, String emsg) {
            LinkRedirActivity.this.finish();
            if (ecode == -1) {
                LinkRedirActivity.this.runOnUiThread(new C02872());
            } else {
                LinkRedirActivity.this.openBrowser(this.val$uri);
            }
        }
    }

    /* renamed from: com.vkontakte.android.LinkRedirActivity.7 */
    class C12997 implements AudioGetById.Callback {
        private final /* synthetic */ Uri val$uri;

        /* renamed from: com.vkontakte.android.LinkRedirActivity.7.1 */
        class C02881 implements Runnable {
            C02881() {
            }

            public void run() {
                Toast.makeText(LinkRedirActivity.this, C0436R.string.post_not_found, 0).show();
            }
        }

        C12997(Uri uri) {
            this.val$uri = uri;
        }

        public void success(ArrayList<AudioFile> res) {
            LinkRedirActivity.this.finish();
            if (res.size() == 0) {
                LinkRedirActivity.this.runOnUiThread(new C02881());
                return;
            }
            AudioFile af = (AudioFile) res.get(0);
            Intent intent = new Intent(LinkRedirActivity.this, AudioPlayerService.class);
            intent.putExtra("action", 1);
            intent.putExtra("file", af);
            LinkRedirActivity.this.startService(intent);
            Intent intent2 = new Intent(LinkRedirActivity.this, AudioPlayerService.class);
            intent2.putExtra("action", 4);
            LinkRedirActivity.this.startService(intent2);
        }

        public void fail(int ecode, String emsg) {
            LinkRedirActivity.this.finish();
            LinkRedirActivity.this.openBrowser(this.val$uri);
        }
    }

    /* renamed from: com.vkontakte.android.LinkRedirActivity.8 */
    class C13008 extends APIHandler {
        private final /* synthetic */ Uri val$uri;

        C13008(Uri uri) {
            this.val$uri = uri;
        }

        public void success(JSONObject o) {
            try {
                JSONObject r = o.optJSONObject("response");
                if (r == null || !r.has(WebDialog.DIALOG_PARAM_TYPE)) {
                    LinkRedirActivity.this.openBrowser(this.val$uri);
                    LinkRedirActivity.this.finish();
                    return;
                }
                String type = r.getString(WebDialog.DIALOG_PARAM_TYPE);
                Bundle args;
                if ("user".equals(type)) {
                    args = new Bundle();
                    args.putInt("id", r.getInt("object_id"));
                    Navigate.to("ProfileFragment", args, LinkRedirActivity.this);
                    LinkRedirActivity.this.finish();
                } else if ("group".equals(type)) {
                    args = new Bundle();
                    args.putInt("id", -r.getInt("object_id"));
                    Navigate.to("ProfileFragment", args, LinkRedirActivity.this);
                    LinkRedirActivity.this.finish();
                } else {
                    LinkRedirActivity.this.openBrowser(this.val$uri);
                    LinkRedirActivity.this.finish();
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }

        public void fail(int ecode, String emsg) {
            LinkRedirActivity.this.openBrowser(this.val$uri);
            LinkRedirActivity.this.finish();
        }
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        if (getIntent() == null) {
            finish();
            return;
        }
        Uri data = getIntent().getData();
        if (getIntent().getData() == null) {
            Toast.makeText(this, C0436R.string.error, 0).show();
            finish();
            return;
        }
        Log.m528i("vk", "DATA = " + data);
        String url = data.toString();
        int uid;
        if ("content".equals(data.getScheme())) {
            try {
                Cursor cursor = getContentResolver().query(getIntent().getData(), new String[]{"data1"}, null, null, null);
                cursor.moveToFirst();
                uid = cursor.getInt(cursor.getColumnIndex("data1"));
                String ctype = getIntent().getType();
                if ("vnd.android.cursor.item/vnd.com.vkontakte.android.profile".equals(ctype)) {
                    Bundle args = new Bundle();
                    args.putInt("id", uid);
                    Navigate.to("ProfileFragment", args, this);
                } else if ("vnd.android.cursor.item/vnd.com.vkontakte.android.sendmsg".equals(ctype)) {
                    openChat(uid);
                }
            } catch (Exception e) {
            }
            finish();
            return;
        }
        if ("vklink".equals(data.getScheme())) {
            url = data.toString().split("\\?", 2)[1];
        } else if (!"vkontakte".equals(data.getScheme())) {
            this.otherApp = true;
        } else if ("profile".equals(data.getHost())) {
            uid = safeParseInt((String) data.getPathSegments().get(0));
            args = new Bundle();
            args.putInt("id", uid);
            finish();
            Navigate.to("ProfileFragment", args, this);
            return;
        } else if ("search".equals(data.getHost())) {
            String q = data.toString().split("/", 4)[3];
            Intent intent = new Intent(this, NewsfeedSearchActivity.class);
            intent.putExtra("q", q);
            finish();
            startActivity(intent);
            return;
        }
        if (!url.startsWith("http://")) {
            if (!url.startsWith("https://")) {
                if (!url.startsWith("vkontakte://")) {
                    url = "http://" + url;
                }
            }
        }
        Uri uri = Uri.parse(url);
        if (uri.getHost().equals("vkontakte.ru") || uri.getHost().equals("vk.com") || uri.getHost().equals("m.vk.com")) {
            String path = uri.getPath();
            if (uri.getQueryParameter("z") != null) {
                path = "/" + uri.getQueryParameter("z").split("\\/")[0];
                url = uri.getScheme() + "://vk.com" + path;
            } else {
                if (uri.getQueryParameter("w") != null) {
                    path = "/" + uri.getQueryParameter("w").split("\\/")[0];
                    url = uri.getScheme() + "://vk.com" + path;
                }
            }
            int id;
            if (path.equals("/friends")) {
                args = new Bundle();
                if (uri.getQueryParameter("id") != null) {
                    id = safeParseInt(uri.getQueryParameter("id"));
                    if (id > 0) {
                        args.putInt("uid", id);
                    }
                }
                Navigate.to("FriendsFragment", args, this);
                finish();
                return;
            }
            if (path.equals("/groups")) {
                args = new Bundle();
                if (uri.getQueryParameter("id") != null) {
                    id = safeParseInt(uri.getQueryParameter("id"));
                    if (id > 0) {
                        args.putInt("uid", id);
                    }
                }
                Navigate.to("GroupsFragment", args, this);
                finish();
                return;
            }
            if (path.equals("/search")) {
                if ("communities".equals(data.getQueryParameter("section"))) {
                    Navigate.to("GroupsFragment", new Bundle(), this);
                }
                if ("statuses".equals(data.getQueryParameter("section"))) {
                    startActivity(new Intent(this, NewsfeedSearchActivity.class));
                }
                finish();
                return;
            }
            if (path.equals("/fave")) {
                args = new Bundle();
                if ("likes_posts".equals(data.getQueryParameter("section"))) {
                    args.putInt("tab", 3);
                }
                if ("likes_photo".equals(data.getQueryParameter("section"))) {
                    args.putInt("tab", 1);
                }
                if ("likes_video".equals(data.getQueryParameter("section"))) {
                    args.putInt("tab", 0);
                }
                if ("links".equals(data.getQueryParameter("section"))) {
                    args.putInt("tab", 4);
                }
                Navigate.to("FaveFragment", args, this);
                finish();
                return;
            }
            if (!path.equals("/mail")) {
                if (!path.equals("/im")) {
                    if (path.equals("/feed")) {
                        if ("notifications".equals(uri.getQueryParameter("section"))) {
                            Navigate.to("FeedbackFragment", new Bundle(), this);
                        } else {
                            Navigate.to("NewsfeedFragment", new Bundle(), this);
                        }
                        finish();
                        return;
                    }
                    Matcher m;
                    if (path.matches("/tag[0-9]+")) {
                        finish();
                        m = Pattern.compile("/tag([0-9]+)").matcher(url);
                        m.find();
                        new PhotosGetAlbums(safeParseInt(m.group(1)), true).setCallback(new C12931()).wrapProgress(this).exec((Activity) this);
                        return;
                    }
                    if (path.matches("/(photos|albums)[-0-9]+")) {
                        finish();
                        m = Pattern.compile("/(?:photos|albums)([-0-9]+)").matcher(url);
                        m.find();
                        uid = safeParseInt(m.group(1));
                        args = new Bundle();
                        args.putInt("uid", uid);
                        args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, getResources().getString(C0436R.string.albums));
                        Navigate.to("PhotoAlbumsListFragment", args, this);
                        return;
                    }
                    if (path.matches("/(videos)[-0-9]+")) {
                        finish();
                        m = Pattern.compile("/(?:videos)([-0-9]+)").matcher(url);
                        m.find();
                        uid = safeParseInt(m.group(1));
                        args = new Bundle();
                        args.putInt("uid", uid);
                        Navigate.to("VideoListFragment", args, this);
                        return;
                    }
                    if (path.matches("/(id|wall)[-0-9]+")) {
                        finish();
                        m = Pattern.compile("/(?:id|wall)([-0-9]+)").matcher(url);
                        m.find();
                        uid = safeParseInt(m.group(1));
                        args = new Bundle();
                        args.putInt("id", uid);
                        Navigate.to("ProfileFragment", args, this);
                        return;
                    }
                    if (path.matches("/write[-0-9]+")) {
                        m = Pattern.compile("/write([-0-9]+)").matcher(url);
                        m.find();
                        openChat(safeParseInt(m.group(1)));
                        finish();
                        return;
                    }
                    if (path.matches("/(club|event|public)[0-9]+")) {
                        finish();
                        m = Pattern.compile("/(?:club|event|public)([0-9]+)").matcher(url);
                        m.find();
                        uid = -safeParseInt(m.group(1));
                        args = new Bundle();
                        args.putInt("id", uid);
                        Navigate.to("ProfileFragment", args, this);
                        return;
                    }
                    if (path.matches("/board[0-9]+")) {
                        finish();
                        m = Pattern.compile("/board([0-9]+)").matcher(url);
                        m.find();
                        uid = safeParseInt(m.group(1));
                        args = new Bundle();
                        args.putInt("gid", uid);
                        Navigate.to("BoardTopicsFragment", args, this);
                        return;
                    }
                    int oid;
                    if (path.matches("/album[-0-9]+_[-0-9]+")) {
                        overridePendingTransition(0, 0);
                        m = Pattern.compile("/album([-0-9]+)_([-0-9]+)").matcher(url);
                        m.find();
                        oid = safeParseInt(m.group(1));
                        int aid = safeParseInt(m.group(2));
                        new APIRequest("execute.getPhotoAlbum").param("owner_id", oid).param("album_id", aid).handler(new C12942(aid, uri)).wrapProgress(this).exec((Activity) this);
                        return;
                    }
                    if (path.matches("/video[-0-9]+_[0-9]+")) {
                        try {
                            Activity.class.getMethod("overridePendingTransition", new Class[]{Integer.TYPE, Integer.TYPE}).invoke(this, new Object[]{Integer.valueOf(0), Integer.valueOf(0)});
                        } catch (Exception e2) {
                        }
                        m = Pattern.compile("/video([-0-9]+)_([0-9]+)").matcher(url);
                        m.find();
                        new VideoGetById(safeParseInt(m.group(1)), safeParseInt(m.group(2)), null).setCallback(new C12953(uri)).wrapProgress(this).exec();
                        return;
                    }
                    if (path.matches("/topic[-0-9]+_[0-9]+")) {
                        overridePendingTransition(0, 0);
                        m = Pattern.compile("/topic([-0-9]+)_([0-9]+)").matcher(url);
                        m.find();
                        oid = -safeParseInt(m.group(1));
                        int tid = safeParseInt(m.group(2));
                        new BoardGetTopics(oid, tid).setCallback(new C12964(oid, tid, uri)).wrapProgress(this).exec();
                        return;
                    }
                    if (path.matches("/page[-0-9]+_[0-9]+")) {
                        m = Pattern.compile("/page([-0-9]+)_([0-9]+)").matcher(url);
                        m.find();
                        oid = safeParseInt(m.group(1));
                        int pid = safeParseInt(m.group(2));
                        intent = new Intent(this, WikiViewActivity.class);
                        intent.putExtra("oid", oid);
                        intent.putExtra("pid", pid);
                        startActivity(intent);
                        finish();
                        return;
                    }
                    if (path.matches("/photo[-_0-9]+")) {
                        try {
                            Activity.class.getMethod("overridePendingTransition", new Class[]{Integer.TYPE, Integer.TYPE}).invoke(this, new Object[]{Integer.valueOf(0), Integer.valueOf(0)});
                        } catch (Exception e3) {
                        }
                        m = Pattern.compile("/photo([-_0-9]+)").matcher(url);
                        m.find();
                        new APIRequest("photos.getById").param("photos", m.group(1)).param("extended", 1).param("photo_sizes", 1).handler(new C12975(uri)).wrapProgress(this).exec();
                        return;
                    }
                    if (path.matches("/wall[-0-9]+_[0-9]+")) {
                        overridePendingTransition(0, 0);
                        Pattern.compile("wall([-0-9]+_[0-9]+)").matcher(url).find();
                        new WallGetById(new String[]{m.group(1)}).setCallback(new C12986(uri)).wrapProgress(this).exec();
                        return;
                    }
                    if (path.matches("/audio[-0-9]+_[0-9]+")) {
                        overridePendingTransition(0, 0);
                        m = Pattern.compile("audio([-0-9]+_[0-9]+)").matcher(url);
                        m.find();
                        String post = m.group(1);
                        Vector<String> al = new Vector();
                        al.add(post);
                        new AudioGetById(al).setCallback(new C12997(uri)).wrapProgress(this).exec();
                        return;
                    }
                    if (path.matches("/[A-Za-z0-9\\._]+")) {
                        try {
                            Activity.class.getMethod("overridePendingTransition", new Class[]{Integer.TYPE, Integer.TYPE}).invoke(this, new Object[]{Integer.valueOf(0), Integer.valueOf(0)});
                        } catch (Exception e4) {
                        }
                        m = Pattern.compile("/([A-Za-z0-9\\._]+)").matcher(path);
                        m.find();
                        new APIRequest("resolveScreenName").param("screen_name", m.group(1)).handler(new C13008(uri)).wrapProgress(this).exec();
                        return;
                    }
                }
            }
            Navigate.to("DialogsFragment", new Bundle(), this);
            finish();
            return;
        }
        openBrowser(uri);
        finish();
    }

    private void openBrowser(Uri uri) {
        Log.m526e("vk", "unrecognized link: " + uri);
        if ("vkontakte".equals(uri.getScheme())) {
            runOnUiThread(new C02899());
            return;
        }
        Uri orig = uri;
        if (!"vk.com".equals(uri.getAuthority())) {
            uri = new Builder().scheme("http").authority("m.vk.com").path("/away").appendQueryParameter("to", orig.toString()).build();
        }
        if (this.otherApp) {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(LinkRedirActivity.this, C0436R.string.link_not_supported, 0).show();
                }
            });
            List<ResolveInfo> resolveInfo = getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", uri), NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST);
            if (resolveInfo != null && resolveInfo.size() <= 2) {
                Intent intent = new Intent("android.intent.action.VIEW", uri);
                for (ResolveInfo info : resolveInfo) {
                    if (!"com.vkontakte.android".equals(info.activityInfo.packageName)) {
                        intent.setClassName(info.activityInfo.packageName, info.activityInfo.name);
                        startActivity(intent);
                        return;
                    }
                }
            }
            startActivity(Intent.createChooser(new Intent("android.intent.action.VIEW", uri), null));
            return;
        }
        startActivity(new Intent("android.intent.action.VIEW", uri));
    }

    private int safeParseInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return 0;
        }
    }

    private void openChat(int uid) {
        if (uid < 2000000000) {
            ArrayList<Integer> al = new ArrayList();
            al.add(Integer.valueOf(uid));
            Friends.getUsers(al, new GetUsersCallback() {
                public void onUsersLoaded(ArrayList<UserProfile> users) {
                    UserProfile p = (UserProfile) users.get(0);
                    Bundle args = new Bundle();
                    args.putInt("id", p.uid);
                    args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, p.fullName);
                    args.putCharSequence("photo", p.photo);
                    Navigate.to("ChatFragment", args, LinkRedirActivity.this);
                }
            });
            return;
        }
        Messages.getChatUsers(uid - 2000000000, new AnonymousClass12(uid));
    }
}
