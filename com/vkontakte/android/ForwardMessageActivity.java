package com.vkontakte.android;

import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.vkontakte.android.data.Messages;
import com.vkontakte.android.fragments.DialogsFragment;
import com.vkontakte.android.fragments.DialogsFragment.SelectionListener;

public class ForwardMessageActivity extends SherlockFragmentActivity {
    private DialogsFragment dialogs;
    private FrameLayout dialogsWrap;

    /* renamed from: com.vkontakte.android.ForwardMessageActivity.1 */
    class C12861 implements SelectionListener {
        C12861() {
        }

        public void onItemSelected(DialogEntry e) {
            if ("android.intent.action.CREATE_SHORTCUT".equals(ForwardMessageActivity.this.getIntent().getAction())) {
                ForwardMessageActivity.this.setResult(-1, Messages.getShortcutIntent(e.profile));
                ForwardMessageActivity.this.finish();
                return;
            }
            Intent intent = new Intent();
            intent.putExtra("profile", e.profile);
            ForwardMessageActivity.this.setResult(-1, intent);
            ForwardMessageActivity.this.finish();
        }
    }

    public void onCreate(Bundle b) {
        boolean z = true;
        super.onCreate(b);
        this.dialogsWrap = new FrameLayout(this);
        this.dialogsWrap.setId(C0436R.id.fragment_wrapper);
        setContentView(this.dialogsWrap);
        if ("android.intent.action.CREATE_SHORTCUT".equals(getIntent().getAction())) {
            setTitle(C0436R.string.add_shortcut_title);
        }
        this.dialogs = new DialogsFragment();
        Bundle args = new Bundle();
        args.putBoolean("select", true);
        this.dialogs.setArguments(args);
        this.dialogs.setListener(new C12861());
        getSupportFragmentManager().beginTransaction().add((int) C0436R.id.fragment_wrapper, this.dialogs).commit();
        ActionBar supportActionBar = getSupportActionBar();
        if (isTaskRoot()) {
            z = false;
        }
        supportActionBar.setDisplayHomeAsUpEnabled(z);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        }
        onBackPressed();
        return true;
    }
}
