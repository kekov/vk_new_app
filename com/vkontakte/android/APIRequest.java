package com.vkontakte.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import com.google.android.gcm.GCMConstants;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.concurrent.Semaphore;
import org.acra.ACRAConstants;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONObject;

public class APIRequest {
    public static final int ERROR_ACCESS = 15;
    public static final int ERROR_PARAM = 100;
    private static HttpClient httpclient;
    private static Semaphore reauthSemaphore;
    public boolean background;
    private boolean cancel;
    boolean forceHTTPS;
    private APIHandler handler;
    HttpPost httppost;
    public long initTime;
    public Hashtable<String, String> params;
    ProgressDialog progressDialog;
    public Handler uiHandler;

    /* renamed from: com.vkontakte.android.APIRequest.1 */
    class C01581 implements Runnable {
        C01581() {
        }

        public void run() {
            try {
                APIRequest.this.httppost.abort();
            } catch (Throwable th) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.APIRequest.2 */
    class C01592 implements OnCancelListener {
        C01592() {
        }

        public void onCancel(DialogInterface dialog) {
            APIRequest.this.cancel();
        }
    }

    public static class APIHandler {
        public void success(JSONObject resp) {
        }

        public void fail(int code, String msg) {
        }
    }

    public static class ErrorResponse {
        public int errorCode;
        public String errorMessage;

        public ErrorResponse(int code, String msg) {
            this.errorCode = code;
            this.errorMessage = msg;
        }
    }

    static {
        reauthSemaphore = new Semaphore(1, true);
    }

    public APIRequest(String method) {
        this.params = new Hashtable();
        this.handler = null;
        this.forceHTTPS = false;
        this.cancel = false;
        this.progressDialog = null;
        this.background = false;
        this.params.put("method", method);
        this.params.put("v", "5.14");
        this.params.put("lang", Global.getDeviceLang());
    }

    private static String convert(byte[] b) {
        String ret = new String();
        for (int i = 0; i < b.length; i++) {
            char[] hex = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
            ret = new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(ret)).append(String.valueOf(hex[(b[i] & 240) >> 4])).toString())).append(String.valueOf(hex[b[i] & ERROR_ACCESS])).toString();
        }
        return ret;
    }

    public static String md5(String h) {
        try {
            return convert(MessageDigest.getInstance("MD5").digest(h.getBytes("UTF-8")));
        } catch (Exception e) {
            return ACRAConstants.DEFAULT_STRING_VALUE;
        }
    }

    public String getSig() {
        String src = "/method/" + ((String) this.params.get("method")) + "?";
        Enumeration<String> e = this.params.keys();
        ArrayList<String> parts = new ArrayList();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            if (!key.equals("method")) {
                parts.add(new StringBuilder(String.valueOf(key)).append("=").append((String) this.params.get(key)).toString());
            }
        }
        return md5(new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(src)).append(TextUtils.join("&", parts)).toString())).append(Global.secret).toString());
    }

    public APIRequest param(String name, String value) {
        if (value != null) {
            this.params.put(name, value);
        }
        return this;
    }

    public APIRequest param(String name, int value) {
        if (value != 0) {
            this.params.put(name, Integer.toString(value));
        }
        return this;
    }

    public APIRequest handler(APIHandler h) {
        this.handler = h;
        return this;
    }

    public APIRequest forceHTTPS(boolean f) {
        this.forceHTTPS = f;
        return this;
    }

    public APIRequest setBackground(boolean bg) {
        this.background = bg;
        return this;
    }

    public APIRequest exec() {
        APIController.executeRequest(this);
        return this;
    }

    public APIRequest exec(View v) {
        if (!(v == null || v.getContext() == null)) {
            this.uiHandler = new Handler(v.getContext().getMainLooper());
            APIController.executeRequest(this);
        }
        return this;
    }

    public APIRequest exec(Activity v) {
        if (v != null) {
            this.uiHandler = new Handler(v.getMainLooper());
            APIController.executeRequest(this);
        }
        return this;
    }

    public boolean execSync() {
        JSONObject o = APIController.runRequest(this);
        if (o == null) {
            invokeCallback(new ErrorResponse(-2, "Response parse failed"));
            return false;
        }
        Object result = parseResponse(o);
        try {
            invokeCallback(result);
            if (APIController.API_DEBUG) {
                Log.m529v("vk", "[" + ((String) this.params.get("method")) + "] Request done in " + (System.currentTimeMillis() - this.initTime));
            }
            if (result == null || (result instanceof ErrorResponse)) {
                return false;
            }
            return true;
        } catch (Exception e) {
            invokeCallback(new ErrorResponse(-3, "Callback invocation failed (parse error?)"));
            return false;
        }
    }

    public void cancel() {
        this.cancel = true;
        if (APIController.API_DEBUG) {
            Log.m528i("vk", "Cancel request " + ((String) this.params.get("method")));
        }
        if (this.httppost != null) {
            Runnable r = new C01581();
            if ("main".equals(Thread.currentThread().getName())) {
                new Thread(r).start();
            } else {
                r.run();
            }
        }
    }

    protected JSONObject doExec() {
        return null;
    }

    public Object parse(JSONObject r) {
        return r;
    }

    Object parseResponse(JSONObject r) {
        try {
            if (this.cancel) {
                return null;
            }
            if (r == null) {
                return new ErrorResponse(-1, "I/O error");
            }
            if (!r.has("response") && r.has(GCMConstants.EXTRA_ERROR)) {
                return new ErrorResponse(r.getJSONObject(GCMConstants.EXTRA_ERROR).getInt("error_code"), r.getJSONObject(GCMConstants.EXTRA_ERROR).getString("error_msg"));
            }
            long time = System.currentTimeMillis();
            Object result = parse(r);
            if (!APIController.API_DEBUG) {
                return result;
            }
            Log.m529v("vk", "[" + ((String) this.params.get("method")) + "] Parse Data: " + (System.currentTimeMillis() - time));
            time = System.currentTimeMillis();
            return result;
        } catch (Throwable x) {
            Log.m532w("vk", x);
            return new ErrorResponse(-2, "Parse error");
        }
    }

    public void invokeCallback(Object result) {
        if (this.handler != null && result != null) {
            if (result instanceof JSONObject) {
                this.handler.success((JSONObject) result);
                return;
            }
            try {
                ErrorResponse er = (ErrorResponse) result;
                this.handler.fail(er.errorCode, er.errorMessage);
            } catch (Exception e) {
            }
        }
    }

    public boolean isCanceled() {
        return this.cancel;
    }

    public APIRequest wrapProgress(Context context) {
        return wrapProgress(context, C0436R.string.loading, true, false);
    }

    public APIRequest wrapProgress(Context context, int strRes, boolean cancelable, boolean cancelByClick) {
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setMessage(context.getResources().getString(strRes));
        this.progressDialog.setOnCancelListener(new C01592());
        this.progressDialog.setCancelable(cancelable);
        this.progressDialog.setCanceledOnTouchOutside(cancelByClick);
        this.progressDialog.show();
        this.progressDialog.getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
        return this;
    }
}
