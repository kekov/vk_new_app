package com.vkontakte.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.NewsItemView.OnInfoUpdateListener;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.NotificationEntry;
import com.vkontakte.android.api.NotificationsGet;
import com.vkontakte.android.api.WallLike;
import com.vkontakte.android.api.WallLike.Callback;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.ListImageLoaderAdapter;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.ListImageLoaderWrapper.Listener;
import com.vkontakte.android.ui.OverlayTextView;
import com.vkontakte.android.ui.PhotoStripView;
import com.vkontakte.android.ui.PhotoStripView.OnPhotoClickListener;
import com.vkontakte.android.ui.PhotoView;
import com.vkontakte.android.ui.RefreshableListView;
import com.vkontakte.android.ui.RefreshableListView.OnRefreshListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.acra.ACRAConstants;

public class NotificationsView extends FrameLayout implements Listener, OnRefreshListener {
    private static final int ID_REPLY_OVERLAY_ICON = 500;
    private static int lastUpdateTime;
    public static Vector<NotificationEntry> notifications;
    public static Vector<NotificationEntry> preloaded;
    private NotificationsAdapter adapter;
    private Runnable afterLoad;
    private APIRequest currentReq;
    private boolean dataLoading;
    private OnClickListener entryClickListener;
    private ErrorView error;
    private FrameLayout footerView;
    private String from;
    private ListImageLoaderWrapper imgWrapper;
    private String[][][] langInfo;
    private RefreshableListView list;
    private boolean moreAvailable;
    private EmptyView noNewsView;
    private int offset;
    private OnPhotoClickListener photoStripClickListener;
    private boolean preloadOnReady;
    private boolean preloading;
    private OnClickListener profileClickListener;
    private ProgressBar progress;

    /* renamed from: com.vkontakte.android.NotificationsView.1 */
    class C03701 implements OnClickListener {
        C03701() {
        }

        public void onClick(View v) {
            Object tag = v.getTag();
            if (tag != null && (tag instanceof Integer)) {
                Bundle args = new Bundle();
                args.putInt("id", ((Integer) tag).intValue());
                Navigate.to("ProfileFragment", args, (Activity) NotificationsView.this.getContext());
            }
        }
    }

    /* renamed from: com.vkontakte.android.NotificationsView.2 */
    class C03712 implements OnClickListener {
        C03712() {
        }

        public void onClick(View v) {
            Object tag = v.getTag();
            if (tag != null && (tag instanceof Integer)) {
                NewsEntry e = ((NotificationEntry) NotificationsView.notifications.get(((Integer) tag).intValue())).parent;
                if (e.type == 5 && ((NotificationEntry) NotificationsView.notifications.get(((Integer) tag).intValue())).ppost != null) {
                    e = ((NotificationEntry) NotificationsView.notifications.get(((Integer) tag).intValue())).ppost;
                }
                Log.m528i("vk", "Entry = " + e);
                if (e == null) {
                    return;
                }
                Bundle args;
                if (e.type == 4) {
                    args = new Bundle();
                    args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, e.text);
                    args.putInt("gid", -e.ownerID);
                    args.putInt("tid", e.postID);
                    args.putInt("offset", e.numComments - (e.numComments % 20));
                    Navigate.to("BoardTopicViewFragment", args, (Activity) NotificationsView.this.getContext());
                    return;
                }
                args = new Bundle();
                args.putParcelable("entry", e);
                Navigate.to("PostViewFragment", args, (Activity) NotificationsView.this.getContext());
            }
        }
    }

    /* renamed from: com.vkontakte.android.NotificationsView.4 */
    class C03724 implements OnItemClickListener {
        C03724() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int _pos, long id) {
            int pos = _pos - NotificationsView.this.list.getHeaderViewsCount();
            if (pos >= 0 && pos < NotificationsView.notifications.size()) {
                NotificationEntry _e = (NotificationEntry) NotificationsView.notifications.get(pos);
                if (_e.feedbackType == 6) {
                    ArrayList<NotificationEntry> list = _e.extra;
                    NotificationsView.notifications.remove(_e);
                    NotificationsView.notifications.addAll(pos, list);
                    NotificationsView.this.updateList();
                    return;
                }
                NewsEntry e = (_e.parentType == 1 || _e.ppost == null) ? _e.parent : _e.ppost;
                Bundle args;
                if (e == null) {
                    args = new Bundle();
                    args.putInt("id", ((UserProfile) _e.users.get(0)).uid);
                    Navigate.to("ProfileFragment", args, (Activity) NotificationsView.this.getContext());
                } else if (e.type == 4) {
                    args = new Bundle();
                    args.putCharSequence(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, e.text);
                    args.putInt("gid", -e.ownerID);
                    args.putInt("tid", e.postID);
                    args.putInt("offset", e.numComments - (e.numComments % 20));
                    Navigate.to("BoardTopicViewFragment", args, (Activity) NotificationsView.this.getContext());
                } else {
                    args = new Bundle();
                    args.putParcelable("entry", e);
                    if (_e.parentType == 4) {
                        args.putInt("comment", _e.parent.postID);
                    }
                    if (_e.feedbackType == 3) {
                        args.putInt("comment", _e.commentID);
                    }
                    Navigate.to("PostViewFragment", args, (Activity) NotificationsView.this.getContext());
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.NotificationsView.5 */
    class C03735 implements OnClickListener {
        C03735() {
        }

        public void onClick(View v) {
            NotificationsView.this.error.setVisibility(8);
            NotificationsView.this.progress.setVisibility(0);
            NotificationsView.this.loadData(true);
        }
    }

    /* renamed from: com.vkontakte.android.NotificationsView.7 */
    class C03747 implements Runnable {
        C03747() {
        }

        public void run() {
            NotificationsView.this.list.refresh();
        }
    }

    /* renamed from: com.vkontakte.android.NotificationsView.9 */
    class C03759 implements Runnable {
        C03759() {
        }

        public void run() {
            NotificationsView.this.adapter.notifyDataSetChanged();
            NotificationsView.this.imgWrapper.updateImages();
        }
    }

    private class NotificationsAdapter extends BaseAdapter {

        /* renamed from: com.vkontakte.android.NotificationsView.NotificationsAdapter.1 */
        class C13331 implements OnInfoUpdateListener {
            C13331() {
            }

            public void onInfoUpdate(NewsItemView view, NewsEntry e) {
                int i;
                int i2 = 10;
                if (e.flag(8)) {
                    ((OverlayTextView) view.findViewById(C0436R.id.post_likes)).setBackgroundResource(C0436R.drawable.btn_post_act);
                    ((OverlayTextView) view.findViewById(C0436R.id.post_likes)).setOverlay((int) C0436R.drawable.btn_post_act_hl);
                    ((OverlayTextView) view.findViewById(C0436R.id.post_likes)).setTextColor(-1);
                    ((OverlayTextView) view.findViewById(C0436R.id.post_likes)).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_post_liked, 0, 0, 0);
                } else {
                    ((OverlayTextView) view.findViewById(C0436R.id.post_likes)).setBackgroundResource(C0436R.drawable.btn_post);
                    ((OverlayTextView) view.findViewById(C0436R.id.post_likes)).setOverlay((int) C0436R.drawable.btn_post_hl);
                    ((OverlayTextView) view.findViewById(C0436R.id.post_likes)).setTextColor(-6710887);
                    ((OverlayTextView) view.findViewById(C0436R.id.post_likes)).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_post_like, 0, 0, 0);
                }
                View findViewById = view.findViewById(C0436R.id.post_likes);
                if (e.numLikes > 0) {
                    i = 10;
                } else {
                    i = 15;
                }
                findViewById.setPadding(Global.scale((float) i), 0, Global.scale((float) (e.numLikes > 0 ? 10 : 15)), 0);
                if (e.flag(4)) {
                    ((OverlayTextView) view.findViewById(C0436R.id.post_reposts)).setBackgroundResource(C0436R.drawable.btn_post_act);
                    ((OverlayTextView) view.findViewById(C0436R.id.post_reposts)).setOverlay((int) C0436R.drawable.btn_post_act_hl);
                    ((OverlayTextView) view.findViewById(C0436R.id.post_reposts)).setTextColor(-1);
                    ((OverlayTextView) view.findViewById(C0436R.id.post_reposts)).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_post_reposted, 0, 0, 0);
                } else {
                    ((OverlayTextView) view.findViewById(C0436R.id.post_reposts)).setBackgroundResource(C0436R.drawable.btn_post);
                    ((OverlayTextView) view.findViewById(C0436R.id.post_reposts)).setOverlay((int) C0436R.drawable.btn_post_hl);
                    ((OverlayTextView) view.findViewById(C0436R.id.post_reposts)).setTextColor(-6710887);
                    ((OverlayTextView) view.findViewById(C0436R.id.post_reposts)).setCompoundDrawablesWithIntrinsicBounds(C0436R.drawable.ic_post_repost, 0, 0, 0);
                }
                findViewById = view.findViewById(C0436R.id.post_reposts);
                if (e.numRetweets > 0) {
                    i = 10;
                } else {
                    i = 15;
                }
                i = Global.scale((float) i);
                if (e.numRetweets <= 0) {
                    i2 = 15;
                }
                findViewById.setPadding(i, 0, Global.scale((float) i2), 0);
            }
        }

        private NotificationsAdapter() {
        }

        public int getCount() {
            return NotificationsView.notifications.size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public int getViewTypeCount() {
            return 10;
        }

        public int getItemViewType(int pos) {
            switch (((NotificationEntry) NotificationsView.notifications.get(pos)).feedbackType) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                case UserListView.TYPE_FAVE /*4*/:
                    return 1;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return 2;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return 0;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    return 3;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    return 4;
                default:
                    return 5;
            }
        }

        public boolean isEnabled(int pos) {
            return (((NotificationEntry) NotificationsView.notifications.get(pos)).feedbackType == 5 || ((NotificationEntry) NotificationsView.notifications.get(pos)).action == 3) ? false : true;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public android.view.View getView(int r43, android.view.View r44, android.view.ViewGroup r45) {
            /*
            r42 = this;
            r34 = r44;
            r37 = com.vkontakte.android.NotificationsView.notifications;	 Catch:{ Exception -> 0x008c }
            r0 = r37;
            r1 = r43;
            r7 = r0.get(r1);	 Catch:{ Exception -> 0x008c }
            r7 = (com.vkontakte.android.api.NotificationEntry) r7;	 Catch:{ Exception -> 0x008c }
            r0 = r7.feedbackType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 5;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x0099;
        L_0x001a:
            if (r34 == 0) goto L_0x001f;
        L_0x001c:
            r33 = r34;
        L_0x001e:
            return r33;
        L_0x001f:
            r33 = new android.widget.TextView;	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.getContext();	 Catch:{ Exception -> 0x008c }
            r0 = r33;
            r1 = r37;
            r0.<init>(r1);	 Catch:{ Exception -> 0x008c }
            r37 = -8026747; // 0xffffffffff858585 float:NaN double:NaN;
            r0 = r33;
            r1 = r37;
            r0.setTextColor(r1);	 Catch:{ Exception -> 0x008c }
            r37 = android.graphics.Typeface.DEFAULT_BOLD;	 Catch:{ Exception -> 0x008c }
            r0 = r33;
            r1 = r37;
            r0.setTypeface(r1);	 Catch:{ Exception -> 0x008c }
            r37 = 1099431936; // 0x41880000 float:17.0 double:5.431915495E-315;
            r37 = com.vkontakte.android.Global.scale(r37);	 Catch:{ Exception -> 0x008c }
            r38 = 1092616192; // 0x41200000 float:10.0 double:5.398241246E-315;
            r38 = com.vkontakte.android.Global.scale(r38);	 Catch:{ Exception -> 0x008c }
            r39 = 0;
            r40 = 0;
            r0 = r33;
            r1 = r37;
            r2 = r38;
            r3 = r39;
            r4 = r40;
            r0.setPadding(r1, r2, r3, r4);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.getResources();	 Catch:{ Exception -> 0x008c }
            r38 = 2131231503; // 0x7f08030f float:1.8079089E38 double:1.052968269E-314;
            r37 = r37.getString(r38);	 Catch:{ Exception -> 0x008c }
            r37 = r37.toUpperCase();	 Catch:{ Exception -> 0x008c }
            r0 = r33;
            r1 = r37;
            r0.setText(r1);	 Catch:{ Exception -> 0x008c }
            r37 = 1;
            r38 = 1096810496; // 0x41600000 float:14.0 double:5.41896386E-315;
            r0 = r33;
            r1 = r37;
            r2 = r38;
            r0.setTextSize(r1, r2);	 Catch:{ Exception -> 0x008c }
            goto L_0x001e;
        L_0x008c:
            r36 = move-exception;
        L_0x008d:
            r37 = "vk";
            r0 = r37;
            r1 = r36;
            com.vkontakte.android.Log.m532w(r0, r1);
        L_0x0096:
            r33 = r34;
            goto L_0x001e;
        L_0x0099:
            r0 = r7.feedbackType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 6;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x015c;
        L_0x00a5:
            if (r34 == 0) goto L_0x0147;
        L_0x00a7:
            r0 = r34;
            r0 = (android.widget.TextView) r0;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r33 = r37;
        L_0x00af:
            r38 = 2131558503; // 0x7f0d0067 float:1.8742324E38 double:1.0531298284E-314;
            r0 = r7.extra;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = (java.util.ArrayList) r37;	 Catch:{ Exception -> 0x008c }
            r37 = r37.size();	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r39 = r39.getResources();	 Catch:{ Exception -> 0x008c }
            r0 = r38;
            r1 = r37;
            r2 = r39;
            r37 = com.vkontakte.android.Global.langPlural(r0, r1, r2);	 Catch:{ Exception -> 0x008c }
            r0 = r33;
            r1 = r37;
            r0.setText(r1);	 Catch:{ Exception -> 0x008c }
            if (r34 != 0) goto L_0x001e;
        L_0x00d9:
            r37 = 2130837679; // 0x7f0200af float:1.7280319E38 double:1.052773694E-314;
            r0 = r33;
            r1 = r37;
            r0.setBackgroundResource(r1);	 Catch:{ Exception -> 0x008c }
            r0 = r33;
            r0 = (com.vkontakte.android.ui.OverlayTextView) r0;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 2130837734; // 0x7f0200e6 float:1.728043E38 double:1.052773721E-314;
            r37.setOverlay(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r33;
            r0 = (com.vkontakte.android.ui.OverlayTextView) r0;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = r33.getPaddingLeft();	 Catch:{ Exception -> 0x008c }
            r39 = 0;
            r40 = r33.getPaddingRight();	 Catch:{ Exception -> 0x008c }
            r41 = 0;
            r37.setOverlayPadding(r38, r39, r40, r41);	 Catch:{ Exception -> 0x008c }
            r37 = r33.getPaddingLeft();	 Catch:{ Exception -> 0x008c }
            r38 = 1092616192; // 0x41200000 float:10.0 double:5.398241246E-315;
            r38 = com.vkontakte.android.Global.scale(r38);	 Catch:{ Exception -> 0x008c }
            r39 = r33.getPaddingRight();	 Catch:{ Exception -> 0x008c }
            r40 = 1092616192; // 0x41200000 float:10.0 double:5.398241246E-315;
            r40 = com.vkontakte.android.Global.scale(r40);	 Catch:{ Exception -> 0x008c }
            r0 = r33;
            r1 = r37;
            r2 = r38;
            r3 = r39;
            r4 = r40;
            r0.setPadding(r1, r2, r3, r4);	 Catch:{ Exception -> 0x008c }
            r37 = 17;
            r0 = r33;
            r1 = r37;
            r0.setGravity(r1);	 Catch:{ Exception -> 0x008c }
            r37 = -13398839; // 0xffffffffff338cc9 float:-2.386628E38 double:NaN;
            r0 = r33;
            r1 = r37;
            r0.setTextColor(r1);	 Catch:{ Exception -> 0x008c }
            r37 = 1;
            r38 = 1097859072; // 0x41700000 float:15.0 double:5.424144515E-315;
            r0 = r33;
            r1 = r37;
            r2 = r38;
            r0.setTextSize(r1, r2);	 Catch:{ Exception -> 0x008c }
            goto L_0x001e;
        L_0x0147:
            r33 = new com.vkontakte.android.ui.OverlayTextView;	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.getContext();	 Catch:{ Exception -> 0x008c }
            r0 = r33;
            r1 = r37;
            r0.<init>(r1);	 Catch:{ Exception -> 0x008c }
            goto L_0x00af;
        L_0x015c:
            r15 = 0;
            r0 = r7.feedbackType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 3;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x042a;
        L_0x0169:
            if (r34 != 0) goto L_0x01cb;
        L_0x016b:
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.getContext();	 Catch:{ Exception -> 0x008c }
            r38 = 2130903140; // 0x7f030064 float:1.741309E38 double:1.052806036E-314;
            r39 = 0;
            r34 = com.vkontakte.android.NotificationsView.inflate(r37, r38, r39);	 Catch:{ Exception -> 0x008c }
            r9 = new com.vkontakte.android.NotificationsView$ViewHolder;	 Catch:{ Exception -> 0x008c }
            r0 = r34;
            r9.<init>(r0);	 Catch:{ Exception -> 0x008c }
            r0 = r34;
            r0.setTag(r9);	 Catch:{ Exception -> 0x008c }
            r0 = r9.userPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.profileClickListener;	 Catch:{ Exception -> 0x008c }
            r37.setOnClickListener(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.getContext();	 Catch:{ Exception -> 0x008c }
            r37 = android.preference.PreferenceManager.getDefaultSharedPreferences(r37);	 Catch:{ Exception -> 0x008c }
            r38 = "fontSize";
            r39 = "0";
            r37 = r37.getString(r38, r39);	 Catch:{ Exception -> 0x008c }
            r17 = java.lang.Integer.parseInt(r37);	 Catch:{ Exception -> 0x008c }
            r0 = r9.text;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 1;
            r39 = 1098907648; // 0x41800000 float:16.0 double:5.42932517E-315;
            r0 = r17;
            r0 = (float) r0;	 Catch:{ Exception -> 0x008c }
            r40 = r0;
            r41 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
            r40 = r40 * r41;
            r39 = r39 + r40;
            r37.setTextSize(r38, r39);	 Catch:{ Exception -> 0x008c }
        L_0x01cb:
            r9 = r34.getTag();	 Catch:{ Exception -> 0x008c }
            r9 = (com.vkontakte.android.NotificationsView.ViewHolder) r9;	 Catch:{ Exception -> 0x008c }
            r0 = r9.name;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r7.commentUser;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r0 = r38;
            r0 = r0.fullName;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r37.setText(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r9.text;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r7.displayableText;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r37.setText(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r7.commentUser;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r0 = r38;
            r0 = r0.photo;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r37 = r37.isAlreadyLoaded(r38);	 Catch:{ Exception -> 0x008c }
            if (r37 == 0) goto L_0x041e;
        L_0x0207:
            r0 = r9.userPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r7.commentUser;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r0 = r39;
            r0 = r0.photo;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r38 = r38.get(r39);	 Catch:{ Exception -> 0x008c }
            r37.setImageBitmap(r38);	 Catch:{ Exception -> 0x008c }
        L_0x0226:
            r0 = r9.userPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r7.commentUser;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r0 = r38;
            r0 = r0.uid;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = java.lang.Integer.valueOf(r38);	 Catch:{ Exception -> 0x008c }
            r37.setTag(r38);	 Catch:{ Exception -> 0x008c }
        L_0x023b:
            r9 = r34.getTag();	 Catch:{ Exception -> 0x008c }
            r9 = (com.vkontakte.android.NotificationsView.ViewHolder) r9;	 Catch:{ Exception -> 0x008c }
            r0 = r7.parentType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 <= 0) goto L_0x0c27;
        L_0x0247:
            if (r15 != 0) goto L_0x0ab1;
        L_0x0249:
            r23 = 1;
        L_0x024b:
            if (r23 == 0) goto L_0x025f;
        L_0x024d:
            r0 = r7.time;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.getResources();	 Catch:{ Exception -> 0x008c }
            r15 = com.vkontakte.android.Global.langDateRelative(r37, r38);	 Catch:{ Exception -> 0x008c }
        L_0x025f:
            r18 = "";
            r0 = r7.parentType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            switch(r37) {
                case 1: goto L_0x0ab5;
                case 2: goto L_0x0aeb;
                case 3: goto L_0x0b17;
                case 4: goto L_0x0b5b;
                case 5: goto L_0x0b91;
                default: goto L_0x0268;
            };	 Catch:{ Exception -> 0x008c }
        L_0x0268:
            r0 = r7.feedbackType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 1;
            r0 = r37;
            r1 = r38;
            if (r0 == r1) goto L_0x0280;
        L_0x0274:
            r0 = r7.feedbackType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 4;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x0282;
        L_0x0280:
            r18 = 0;
        L_0x0282:
            r0 = r9.link;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x02ac;
        L_0x0288:
            r0 = r9.link;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            if (r18 == 0) goto L_0x0bc7;
        L_0x028e:
            r37 = r18;
        L_0x0290:
            r0 = r38;
            r1 = r37;
            r0.setText(r1);	 Catch:{ Exception -> 0x008c }
            r0 = r9.link;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            if (r18 == 0) goto L_0x0bcb;
        L_0x029d:
            r37 = r18.length();	 Catch:{ Exception -> 0x008c }
            if (r37 <= 0) goto L_0x0bcb;
        L_0x02a3:
            r37 = 999; // 0x3e7 float:1.4E-42 double:4.936E-321;
        L_0x02a5:
            r0 = r38;
            r1 = r37;
            r0.setMaxHeight(r1);	 Catch:{ Exception -> 0x008c }
        L_0x02ac:
            r27 = 0;
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0 = r0.attachments;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.iterator();	 Catch:{ Exception -> 0x008c }
        L_0x02bc:
            r38 = r37.hasNext();	 Catch:{ Exception -> 0x008c }
            if (r38 != 0) goto L_0x0bcf;
        L_0x02c2:
            if (r27 != 0) goto L_0x0be3;
        L_0x02c4:
            r0 = r9.postPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 8;
            r37.setVisibility(r38);	 Catch:{ Exception -> 0x008c }
        L_0x02cd:
            r0 = r9.postPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x02fa;
        L_0x02d3:
            r0 = r9.postPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = java.lang.Integer.valueOf(r43);	 Catch:{ Exception -> 0x008c }
            r37.setTag(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r9.info;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = java.lang.Integer.valueOf(r43);	 Catch:{ Exception -> 0x008c }
            r37.setTag(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r9.link;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x02fa;
        L_0x02ef:
            r0 = r9.link;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = java.lang.Integer.valueOf(r43);	 Catch:{ Exception -> 0x008c }
            r37.setTag(r38);	 Catch:{ Exception -> 0x008c }
        L_0x02fa:
            if (r9 == 0) goto L_0x030b;
        L_0x02fc:
            r0 = r9.info;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x030b;
        L_0x0302:
            r0 = r9.info;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0.setText(r15);	 Catch:{ Exception -> 0x008c }
        L_0x030b:
            r37 = 500; // 0x1f4 float:7.0E-43 double:2.47E-321;
            r0 = r34;
            r1 = r37;
            r37 = r0.findViewById(r1);	 Catch:{ Exception -> 0x008c }
            if (r37 == 0) goto L_0x0326;
        L_0x0317:
            r37 = 500; // 0x1f4 float:7.0E-43 double:2.47E-321;
            r0 = r34;
            r1 = r37;
            r37 = r0.findViewById(r1);	 Catch:{ Exception -> 0x008c }
            r38 = 8;
            r37.setVisibility(r38);	 Catch:{ Exception -> 0x008c }
        L_0x0326:
            r0 = r7.replyID;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 > 0) goto L_0x0332;
        L_0x032c:
            r0 = r7.isLiked;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x03bc;
        L_0x0332:
            r16 = 0;
            r37 = 500; // 0x1f4 float:7.0E-43 double:2.47E-321;
            r0 = r34;
            r1 = r37;
            r37 = r0.findViewById(r1);	 Catch:{ Exception -> 0x008c }
            if (r37 != 0) goto L_0x0c8c;
        L_0x0340:
            r16 = new android.widget.ImageView;	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.getContext();	 Catch:{ Exception -> 0x008c }
            r0 = r16;
            r1 = r37;
            r0.<init>(r1);	 Catch:{ Exception -> 0x008c }
            r37 = android.widget.ImageView.ScaleType.FIT_END;	 Catch:{ Exception -> 0x008c }
            r0 = r16;
            r1 = r37;
            r0.setScaleType(r1);	 Catch:{ Exception -> 0x008c }
            r19 = new android.widget.RelativeLayout$LayoutParams;	 Catch:{ Exception -> 0x008c }
            r37 = -2;
            r38 = -1;
            r0 = r19;
            r1 = r37;
            r2 = r38;
            r0.<init>(r1, r2);	 Catch:{ Exception -> 0x008c }
            r37 = 11;
            r0 = r19;
            r1 = r37;
            r0.addRule(r1);	 Catch:{ Exception -> 0x008c }
            r37 = 12;
            r0 = r19;
            r1 = r37;
            r0.addRule(r1);	 Catch:{ Exception -> 0x008c }
            r37 = 10;
            r0 = r19;
            r1 = r37;
            r0.addRule(r1);	 Catch:{ Exception -> 0x008c }
            r0 = r16;
            r1 = r19;
            r0.setLayoutParams(r1);	 Catch:{ Exception -> 0x008c }
            r37 = 500; // 0x1f4 float:7.0E-43 double:2.47E-321;
            r0 = r16;
            r1 = r37;
            r0.setId(r1);	 Catch:{ Exception -> 0x008c }
            r0 = r34;
            r0 = (android.view.ViewGroup) r0;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r1 = r16;
            r0.addView(r1);	 Catch:{ Exception -> 0x008c }
        L_0x03a3:
            r0 = r7.replyID;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 <= 0) goto L_0x0c9a;
        L_0x03a9:
            r37 = 2130837957; // 0x7f0201c5 float:1.7280883E38 double:1.0527738314E-314;
        L_0x03ac:
            r0 = r16;
            r1 = r37;
            r0.setImageResource(r1);	 Catch:{ Exception -> 0x008c }
            r37 = 0;
            r0 = r16;
            r1 = r37;
            r0.setVisibility(r1);	 Catch:{ Exception -> 0x008c }
        L_0x03bc:
            if (r43 <= 0) goto L_0x0c9f;
        L_0x03be:
            r37 = com.vkontakte.android.NotificationsView.notifications;	 Catch:{ Exception -> 0x008c }
            r38 = r43 + -1;
            r37 = r37.get(r38);	 Catch:{ Exception -> 0x008c }
            r37 = (com.vkontakte.android.api.NotificationEntry) r37;	 Catch:{ Exception -> 0x008c }
            r0 = r37;
            r0 = r0.feedbackType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 5;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x0c9f;
        L_0x03d6:
            r28 = 1;
        L_0x03d8:
            r37 = com.vkontakte.android.NotificationsView.notifications;	 Catch:{ Exception -> 0x008c }
            r37 = r37.size();	 Catch:{ Exception -> 0x008c }
            r37 = r37 + -1;
            r0 = r43;
            r1 = r37;
            if (r0 >= r1) goto L_0x0ca3;
        L_0x03e6:
            r37 = com.vkontakte.android.NotificationsView.notifications;	 Catch:{ Exception -> 0x008c }
            r38 = r43 + 1;
            r37 = r37.get(r38);	 Catch:{ Exception -> 0x008c }
            r37 = (com.vkontakte.android.api.NotificationEntry) r37;	 Catch:{ Exception -> 0x008c }
            r0 = r37;
            r0 = r0.feedbackType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 5;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x0ca3;
        L_0x03fe:
            r22 = 1;
        L_0x0400:
            if (r43 == 0) goto L_0x0404;
        L_0x0402:
            if (r28 == 0) goto L_0x0ca7;
        L_0x0404:
            r37 = r42.getCount();	 Catch:{ Exception -> 0x008c }
            r37 = r37 + -1;
            r0 = r43;
            r1 = r37;
            if (r0 == r1) goto L_0x0412;
        L_0x0410:
            if (r22 == 0) goto L_0x0ca7;
        L_0x0412:
            r37 = 2130837675; // 0x7f0200ab float:1.728031E38 double:1.052773692E-314;
            r0 = r34;
            r1 = r37;
            r0.setBackgroundResource(r1);	 Catch:{ Exception -> 0x008c }
            goto L_0x0096;
        L_0x041e:
            r0 = r9.userPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 2130838132; // 0x7f020274 float:1.7281238E38 double:1.052773918E-314;
            r37.setImageResource(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x0226;
        L_0x042a:
            r0 = r7.feedbackType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 1;
            r0 = r37;
            r1 = r38;
            if (r0 == r1) goto L_0x0442;
        L_0x0436:
            r0 = r7.feedbackType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 4;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x0858;
        L_0x0442:
            if (r34 != 0) goto L_0x0485;
        L_0x0444:
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.getContext();	 Catch:{ Exception -> 0x008c }
            r38 = 2130903141; // 0x7f030065 float:1.7413092E38 double:1.0528060366E-314;
            r39 = 0;
            r34 = com.vkontakte.android.NotificationsView.inflate(r37, r38, r39);	 Catch:{ Exception -> 0x008c }
            r9 = new com.vkontakte.android.NotificationsView$ViewHolder;	 Catch:{ Exception -> 0x008c }
            r0 = r34;
            r9.<init>(r0);	 Catch:{ Exception -> 0x008c }
            r0 = r34;
            r0.setTag(r9);	 Catch:{ Exception -> 0x008c }
            r0 = r9.userPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.profileClickListener;	 Catch:{ Exception -> 0x008c }
            r37.setOnClickListener(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r9.photoStrip;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.photoStripClickListener;	 Catch:{ Exception -> 0x008c }
            r37.setListener(r38);	 Catch:{ Exception -> 0x008c }
        L_0x0485:
            r9 = r34.getTag();	 Catch:{ Exception -> 0x008c }
            r9 = (com.vkontakte.android.NotificationsView.ViewHolder) r9;	 Catch:{ Exception -> 0x008c }
            r0 = r7.time;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.getResources();	 Catch:{ Exception -> 0x008c }
            r15 = com.vkontakte.android.Global.langDateRelative(r37, r38);	 Catch:{ Exception -> 0x008c }
            r0 = r7.users;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r24 = r37.size();	 Catch:{ Exception -> 0x008c }
            r32 = new android.text.SpannableStringBuilder;	 Catch:{ Exception -> 0x008c }
            r0 = r7.users;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 0;
            r37 = r37.get(r38);	 Catch:{ Exception -> 0x008c }
            r37 = (com.vkontakte.android.UserProfile) r37;	 Catch:{ Exception -> 0x008c }
            r0 = r37;
            r0 = r0.fullName;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r32;
            r1 = r37;
            r0.<init>(r1);	 Catch:{ Exception -> 0x008c }
            r21 = r32.length();	 Catch:{ Exception -> 0x008c }
            r0 = r7.users;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.size();	 Catch:{ Exception -> 0x008c }
            r38 = 1;
            r0 = r37;
            r1 = r38;
            if (r0 <= r1) goto L_0x054f;
        L_0x04d4:
            r37 = " ";
            r0 = r32;
            r1 = r37;
            r0.append(r1);	 Catch:{ Exception -> 0x008c }
            r37 = 2131558447; // 0x7f0d002f float:1.874221E38 double:1.053129801E-314;
            r0 = r7.users;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.size();	 Catch:{ Exception -> 0x008c }
            r38 = r38 + -1;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r39 = r39.getResources();	 Catch:{ Exception -> 0x008c }
            r29 = com.vkontakte.android.Global.langPlural(r37, r38, r39);	 Catch:{ Exception -> 0x008c }
            r37 = "<b>";
            r0 = r29;
            r1 = r37;
            r31 = r0.indexOf(r1);	 Catch:{ Exception -> 0x008c }
            r37 = "<b>";
            r38 = "";
            r0 = r29;
            r1 = r37;
            r2 = r38;
            r29 = r0.replace(r1, r2);	 Catch:{ Exception -> 0x008c }
            r37 = "</b>";
            r0 = r29;
            r1 = r37;
            r8 = r0.indexOf(r1);	 Catch:{ Exception -> 0x008c }
            r37 = "</b>";
            r38 = "";
            r0 = r29;
            r1 = r37;
            r2 = r38;
            r29 = r0.replace(r1, r2);	 Catch:{ Exception -> 0x008c }
            r37 = android.text.Spannable.Factory.getInstance();	 Catch:{ Exception -> 0x008c }
            r0 = r37;
            r1 = r29;
            r30 = r0.newSpannable(r1);	 Catch:{ Exception -> 0x008c }
            r37 = new android.text.style.StyleSpan;	 Catch:{ Exception -> 0x008c }
            r38 = 1;
            r37.<init>(r38);	 Catch:{ Exception -> 0x008c }
            r38 = 0;
            r0 = r30;
            r1 = r37;
            r2 = r31;
            r3 = r38;
            r0.setSpan(r1, r2, r8, r3);	 Catch:{ Exception -> 0x008c }
            r0 = r32;
            r1 = r30;
            r0.append(r1);	 Catch:{ Exception -> 0x008c }
        L_0x054f:
            r12 = 0;
            r13 = 0;
            r14 = 0;
            r0 = r7.action;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            switch(r37) {
                case 1: goto L_0x06db;
                case 2: goto L_0x06e8;
                case 3: goto L_0x06f5;
                case 4: goto L_0x0559;
                case 5: goto L_0x0559;
                case 6: goto L_0x0702;
                default: goto L_0x0559;
            };	 Catch:{ Exception -> 0x008c }
        L_0x0559:
            r0 = r7.parentType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            switch(r37) {
                case 1: goto L_0x070f;
                case 2: goto L_0x0712;
                case 3: goto L_0x0715;
                case 4: goto L_0x0718;
                default: goto L_0x0560;
            };	 Catch:{ Exception -> 0x008c }
        L_0x0560:
            r37 = 1;
            r0 = r24;
            r1 = r37;
            if (r0 != r1) goto L_0x071e;
        L_0x0568:
            r0 = r7.users;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 0;
            r37 = r37.get(r38);	 Catch:{ Exception -> 0x008c }
            r37 = (com.vkontakte.android.UserProfile) r37;	 Catch:{ Exception -> 0x008c }
            r0 = r37;
            r0 = r0.f151f;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x071b;
        L_0x057c:
            r13 = 1;
        L_0x057d:
            r37 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x008c }
            r38 = " ";
            r37.<init>(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.langInfo;	 Catch:{ Exception -> 0x008c }
            r38 = r38[r12];	 Catch:{ Exception -> 0x008c }
            r38 = r38[r13];	 Catch:{ Exception -> 0x008c }
            r38 = r38[r14];	 Catch:{ Exception -> 0x008c }
            r37 = r37.append(r38);	 Catch:{ Exception -> 0x008c }
            r37 = r37.toString();	 Catch:{ Exception -> 0x008c }
            r0 = r32;
            r1 = r37;
            r0.append(r1);	 Catch:{ Exception -> 0x008c }
            r18 = 0;
            r0 = r7.action;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 1;
            r0 = r37;
            r1 = r38;
            if (r0 == r1) goto L_0x05bd;
        L_0x05b1:
            r0 = r7.action;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 2;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x05f1;
        L_0x05bd:
            r0 = r7.parentType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 3;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x0721;
        L_0x05c9:
            r38 = android.text.Spannable.Factory.getInstance();	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0 = r0.attachments;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r39 = 0;
            r0 = r37;
            r1 = r39;
            r37 = r0.get(r1);	 Catch:{ Exception -> 0x008c }
            r37 = (com.vkontakte.android.VideoAttachment) r37;	 Catch:{ Exception -> 0x008c }
            r0 = r37;
            r0 = r0.title;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r38;
            r1 = r37;
            r18 = r0.newSpannable(r1);	 Catch:{ Exception -> 0x008c }
        L_0x05f1:
            r20 = 0;
            r0 = r9.photoStrip;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 6;
            r0 = r7.users;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r39 = r39.size();	 Catch:{ Exception -> 0x008c }
            r38 = java.lang.Math.min(r38, r39);	 Catch:{ Exception -> 0x008c }
            r38 = r38 + -1;
            r37.setCount(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r9.photoStrip;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r0 = r7.users;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.size();	 Catch:{ Exception -> 0x008c }
            r39 = 1;
            r0 = r37;
            r1 = r39;
            if (r0 <= r1) goto L_0x07b3;
        L_0x061e:
            r37 = 0;
        L_0x0620:
            r0 = r38;
            r1 = r37;
            r0.setVisibility(r1);	 Catch:{ Exception -> 0x008c }
            r0 = r7.users;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.iterator();	 Catch:{ Exception -> 0x008c }
        L_0x062f:
            r38 = r37.hasNext();	 Catch:{ Exception -> 0x008c }
            if (r38 != 0) goto L_0x07b7;
        L_0x0635:
            r0 = r7.users;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.size();	 Catch:{ Exception -> 0x008c }
            r38 = 1;
            r0 = r37;
            r1 = r38;
            if (r0 <= r1) goto L_0x065e;
        L_0x0645:
            r0 = r9.photoStrip;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r7.users;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r39 = 1;
            r0 = r7.users;	 Catch:{ Exception -> 0x008c }
            r40 = r0;
            r40 = r40.size();	 Catch:{ Exception -> 0x008c }
            r38 = r38.subList(r39, r40);	 Catch:{ Exception -> 0x008c }
            r37.setTag(r38);	 Catch:{ Exception -> 0x008c }
        L_0x065e:
            if (r18 == 0) goto L_0x0669;
        L_0x0660:
            r37 = " ";
            r0 = r32;
            r1 = r37;
            r0.append(r1);	 Catch:{ Exception -> 0x008c }
        L_0x0669:
            r37 = new android.text.style.ForegroundColorSpan;	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.getResources();	 Catch:{ Exception -> 0x008c }
            r39 = 2131099687; // 0x7f060027 float:1.7811734E38 double:1.052903143E-314;
            r38 = r38.getColorStateList(r39);	 Catch:{ Exception -> 0x008c }
            r38 = r38.getDefaultColor();	 Catch:{ Exception -> 0x008c }
            r37.<init>(r38);	 Catch:{ Exception -> 0x008c }
            r38 = r32.length();	 Catch:{ Exception -> 0x008c }
            r39 = 0;
            r0 = r32;
            r1 = r37;
            r2 = r21;
            r3 = r38;
            r4 = r39;
            r0.setSpan(r1, r2, r3, r4);	 Catch:{ Exception -> 0x008c }
            if (r18 == 0) goto L_0x06ce;
        L_0x0698:
            r37 = new android.text.style.ForegroundColorSpan;	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.getResources();	 Catch:{ Exception -> 0x008c }
            r39 = 2131099689; // 0x7f060029 float:1.7811738E38 double:1.052903144E-314;
            r38 = r38.getColorStateList(r39);	 Catch:{ Exception -> 0x008c }
            r38 = r38.getDefaultColor();	 Catch:{ Exception -> 0x008c }
            r37.<init>(r38);	 Catch:{ Exception -> 0x008c }
            r38 = 0;
            r39 = r18.length();	 Catch:{ Exception -> 0x008c }
            r40 = 0;
            r0 = r18;
            r1 = r37;
            r2 = r38;
            r3 = r39;
            r4 = r40;
            r0.setSpan(r1, r2, r3, r4);	 Catch:{ Exception -> 0x008c }
            r0 = r32;
            r1 = r18;
            r0.append(r1);	 Catch:{ Exception -> 0x008c }
        L_0x06ce:
            r0 = r9.name;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r1 = r32;
            r0.setText(r1);	 Catch:{ Exception -> 0x008c }
            goto L_0x023b;
        L_0x06db:
            r12 = 0;
            r0 = r9.actionIcon;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 2130837835; // 0x7f02014b float:1.7280635E38 double:1.052773771E-314;
            r37.setImageResource(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x0559;
        L_0x06e8:
            r12 = 1;
            r0 = r9.actionIcon;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 2130837836; // 0x7f02014c float:1.7280637E38 double:1.0527737716E-314;
            r37.setImageResource(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x0559;
        L_0x06f5:
            r12 = 2;
            r0 = r9.actionIcon;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 2130837833; // 0x7f020149 float:1.7280631E38 double:1.05277377E-314;
            r37.setImageResource(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x0559;
        L_0x0702:
            r12 = 3;
            r0 = r9.actionIcon;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 2130837834; // 0x7f02014a float:1.7280633E38 double:1.0527737706E-314;
            r37.setImageResource(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x0559;
        L_0x070f:
            r14 = 0;
            goto L_0x0560;
        L_0x0712:
            r14 = 1;
            goto L_0x0560;
        L_0x0715:
            r14 = 2;
            goto L_0x0560;
        L_0x0718:
            r14 = 3;
            goto L_0x0560;
        L_0x071b:
            r13 = 0;
            goto L_0x057d;
        L_0x071e:
            r13 = 2;
            goto L_0x057d;
        L_0x0721:
            r0 = r7.parentType;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 2;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x0731;
        L_0x072d:
            r18 = 0;
            goto L_0x05f1;
        L_0x0731:
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0 = r0.displayableRetweetText;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x0761;
        L_0x073d:
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0 = r0.displayableRetweetText;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.length();	 Catch:{ Exception -> 0x008c }
            if (r37 <= 0) goto L_0x0761;
        L_0x074d:
            r37 = android.text.Spannable.Factory.getInstance();	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r0 = r38;
            r0 = r0.displayableRetweetText;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r18 = r37.newSpannable(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x05f1;
        L_0x0761:
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0 = r0.displayablePreviewText;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x0791;
        L_0x076d:
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0 = r0.displayablePreviewText;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.length();	 Catch:{ Exception -> 0x008c }
            if (r37 <= 0) goto L_0x0791;
        L_0x077d:
            r37 = android.text.Spannable.Factory.getInstance();	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r0 = r38;
            r0 = r0.displayablePreviewText;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r18 = r37.newSpannable(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x05f1;
        L_0x0791:
            r37 = android.text.Spannable.Factory.getInstance();	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.getResources();	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r0 = r39;
            r0 = r0.time;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r38 = com.vkontakte.android.Global.langDate(r38, r39);	 Catch:{ Exception -> 0x008c }
            r18 = r37.newSpannable(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x05f1;
        L_0x07b3:
            r37 = 8;
            goto L_0x0620;
        L_0x07b7:
            r26 = r37.next();	 Catch:{ Exception -> 0x008c }
            r26 = (com.vkontakte.android.UserProfile) r26;	 Catch:{ Exception -> 0x008c }
            if (r20 != 0) goto L_0x0818;
        L_0x07bf:
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r26;
            r0 = r0.photo;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r38 = r38.isAlreadyLoaded(r39);	 Catch:{ Exception -> 0x008c }
            if (r38 == 0) goto L_0x080d;
        L_0x07d5:
            r0 = r9.userPhoto;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r39 = r39.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r26;
            r0 = r0.photo;	 Catch:{ Exception -> 0x008c }
            r40 = r0;
            r39 = r39.get(r40);	 Catch:{ Exception -> 0x008c }
            r38.setImageBitmap(r39);	 Catch:{ Exception -> 0x008c }
        L_0x07f0:
            r0 = r9.userPhoto;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r0 = r26;
            r0 = r0.uid;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r39 = java.lang.Integer.valueOf(r39);	 Catch:{ Exception -> 0x008c }
            r38.setTag(r39);	 Catch:{ Exception -> 0x008c }
        L_0x0801:
            r20 = r20 + 1;
            r38 = 6;
            r0 = r20;
            r1 = r38;
            if (r0 < r1) goto L_0x062f;
        L_0x080b:
            goto L_0x0635;
        L_0x080d:
            r0 = r9.userPhoto;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r39 = 2130838132; // 0x7f020274 float:1.7281238E38 double:1.052773918E-314;
            r38.setImageResource(r39);	 Catch:{ Exception -> 0x008c }
            goto L_0x07f0;
        L_0x0818:
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r26;
            r0 = r0.photo;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r38 = r38.isAlreadyLoaded(r39);	 Catch:{ Exception -> 0x008c }
            if (r38 == 0) goto L_0x084c;
        L_0x082e:
            r0 = r9.photoStrip;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r39 = r20 + -1;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r40 = r0;
            r40 = r40.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r26;
            r0 = r0.photo;	 Catch:{ Exception -> 0x008c }
            r41 = r0;
            r40 = r40.get(r41);	 Catch:{ Exception -> 0x008c }
            r38.setBitmap(r39, r40);	 Catch:{ Exception -> 0x008c }
            goto L_0x0801;
        L_0x084c:
            r0 = r9.photoStrip;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r39 = r20 + -1;
            r40 = 0;
            r38.setBitmap(r39, r40);	 Catch:{ Exception -> 0x008c }
            goto L_0x0801;
        L_0x0858:
            r0 = r7.action;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 4;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x086a;
        L_0x0864:
            r0 = r7.ppost;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x0876;
        L_0x086a:
            r0 = r7.action;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 5;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x0a8d;
        L_0x0876:
            r25 = 0;
            if (r34 == 0) goto L_0x0880;
        L_0x087a:
            r0 = r34;
            r0 = (com.vkontakte.android.NewsItemView) r0;	 Catch:{ Exception -> 0x008c }
            r25 = r0;
        L_0x0880:
            if (r25 != 0) goto L_0x08a7;
        L_0x0882:
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.getContext();	 Catch:{ Exception -> 0x008c }
            r38 = 2130903132; // 0x7f03005c float:1.7413073E38 double:1.052806032E-314;
            r39 = 0;
            r25 = com.vkontakte.android.NotificationsView.inflate(r37, r38, r39);	 Catch:{ Exception -> 0x008c }
            r25 = (com.vkontakte.android.NewsItemView) r25;	 Catch:{ Exception -> 0x008c }
            r37 = new com.vkontakte.android.NotificationsView$NotificationsAdapter$1;	 Catch:{ Exception -> 0x008c }
            r0 = r37;
            r1 = r42;
            r0.<init>();	 Catch:{ Exception -> 0x008c }
            r0 = r25;
            r1 = r37;
            r0.setOnInfoUpdateListener(r1);	 Catch:{ Exception -> 0x008c }
        L_0x08a7:
            r25.reset();	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 0;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r39 = r39.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r25;
            r1 = r37;
            r2 = r38;
            r3 = r39;
            r0.setData(r1, r2, r3);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r0 = r38;
            r0 = r0.userPhotoURL;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r37 = r37.isAlreadyLoaded(r38);	 Catch:{ Exception -> 0x008c }
            if (r37 == 0) goto L_0x099d;
        L_0x08df:
            r37 = 2131296519; // 0x7f090107 float:1.8210957E38 double:1.053000391E-314;
            r0 = r25;
            r1 = r37;
            r37 = r0.findViewById(r1);	 Catch:{ Exception -> 0x008c }
            r37 = (android.widget.ImageView) r37;	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r0 = r39;
            r0 = r0.userPhotoURL;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r38 = r38.get(r39);	 Catch:{ Exception -> 0x008c }
            r37.setImageBitmap(r38);	 Catch:{ Exception -> 0x008c }
        L_0x0907:
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 32;
            r37 = r37.flag(r38);	 Catch:{ Exception -> 0x008c }
            if (r37 == 0) goto L_0x0955;
        L_0x0913:
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r0 = r38;
            r0 = r0.retweetUserPhoto;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r37 = r37.isAlreadyLoaded(r38);	 Catch:{ Exception -> 0x008c }
            if (r37 == 0) goto L_0x09c2;
        L_0x092d:
            r37 = 2131296517; // 0x7f090105 float:1.8210953E38 double:1.05300039E-314;
            r0 = r25;
            r1 = r37;
            r37 = r0.findViewById(r1);	 Catch:{ Exception -> 0x008c }
            r37 = (android.widget.ImageView) r37;	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r0 = r39;
            r0 = r0.retweetUserPhoto;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r38 = r38.get(r39);	 Catch:{ Exception -> 0x008c }
            r37.setImageBitmap(r38);	 Catch:{ Exception -> 0x008c }
        L_0x0955:
            r10 = 0;
            r37 = 2131296520; // 0x7f090108 float:1.821096E38 double:1.0530003916E-314;
            r0 = r25;
            r1 = r37;
            r5 = r0.findViewById(r1);	 Catch:{ Exception -> 0x008c }
            r5 = (android.view.ViewGroup) r5;	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0 = r0.repostAttachments;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.iterator();	 Catch:{ Exception -> 0x008c }
        L_0x0971:
            r38 = r37.hasNext();	 Catch:{ Exception -> 0x008c }
            if (r38 != 0) goto L_0x09e7;
        L_0x0977:
            r10 = 0;
            r37 = 2131296410; // 0x7f09009a float:1.8210736E38 double:1.0530003373E-314;
            r0 = r25;
            r1 = r37;
            r5 = r0.findViewById(r1);	 Catch:{ Exception -> 0x008c }
            r5 = (android.view.ViewGroup) r5;	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0 = r0.attachments;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.iterator();	 Catch:{ Exception -> 0x008c }
        L_0x0993:
            r38 = r37.hasNext();	 Catch:{ Exception -> 0x008c }
            if (r38 != 0) goto L_0x0a3a;
        L_0x0999:
            r34 = r25;
            goto L_0x023b;
        L_0x099d:
            r37 = 2131296519; // 0x7f090107 float:1.8210957E38 double:1.053000391E-314;
            r0 = r25;
            r1 = r37;
            r37 = r0.findViewById(r1);	 Catch:{ Exception -> 0x008c }
            r37 = (android.widget.ImageView) r37;	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r0 = r38;
            r0 = r0.userID;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            if (r38 <= 0) goto L_0x09be;
        L_0x09b6:
            r38 = 2130838132; // 0x7f020274 float:1.7281238E38 double:1.052773918E-314;
        L_0x09b9:
            r37.setImageResource(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x0907;
        L_0x09be:
            r38 = 2130837731; // 0x7f0200e3 float:1.7280424E38 double:1.0527737197E-314;
            goto L_0x09b9;
        L_0x09c2:
            r37 = 2131296517; // 0x7f090105 float:1.8210953E38 double:1.05300039E-314;
            r0 = r25;
            r1 = r37;
            r37 = r0.findViewById(r1);	 Catch:{ Exception -> 0x008c }
            r37 = (android.widget.ImageView) r37;	 Catch:{ Exception -> 0x008c }
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r0 = r38;
            r0 = r0.userID;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            if (r38 <= 0) goto L_0x09e3;
        L_0x09db:
            r38 = 2130838132; // 0x7f020274 float:1.7281238E38 double:1.052773918E-314;
        L_0x09de:
            r37.setImageResource(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x0955;
        L_0x09e3:
            r38 = 2130837731; // 0x7f0200e3 float:1.7280424E38 double:1.0527737197E-314;
            goto L_0x09de;
        L_0x09e7:
            r6 = r37.next();	 Catch:{ Exception -> 0x008c }
            r6 = (com.vkontakte.android.Attachment) r6;	 Catch:{ Exception -> 0x008c }
            r0 = r6 instanceof com.vkontakte.android.ImageAttachment;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            if (r38 == 0) goto L_0x0a2c;
        L_0x09f3:
            r0 = r6;
            r0 = (com.vkontakte.android.ImageAttachment) r0;	 Catch:{ Exception -> 0x008c }
            r11 = r0;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r39 = r11.getImageURL();	 Catch:{ Exception -> 0x008c }
            r38 = r38.isAlreadyLoaded(r39);	 Catch:{ Exception -> 0x008c }
            if (r38 == 0) goto L_0x0a30;
        L_0x0a0b:
            r38 = r5.getChildAt(r10);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r39 = r39.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r40 = r11.getImageURL();	 Catch:{ Exception -> 0x008c }
            r39 = r39.get(r40);	 Catch:{ Exception -> 0x008c }
            r40 = 1;
            r0 = r38;
            r1 = r39;
            r2 = r40;
            r11.setImage(r0, r1, r2);	 Catch:{ Exception -> 0x008c }
        L_0x0a2c:
            r10 = r10 + 1;
            goto L_0x0971;
        L_0x0a30:
            r38 = r5.getChildAt(r10);	 Catch:{ Exception -> 0x008c }
            r0 = r38;
            r11.clearImage(r0);	 Catch:{ Exception -> 0x008c }
            goto L_0x0a2c;
        L_0x0a3a:
            r6 = r37.next();	 Catch:{ Exception -> 0x008c }
            r6 = (com.vkontakte.android.Attachment) r6;	 Catch:{ Exception -> 0x008c }
            r0 = r6 instanceof com.vkontakte.android.ImageAttachment;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            if (r38 == 0) goto L_0x0a7f;
        L_0x0a46:
            r0 = r6;
            r0 = (com.vkontakte.android.ImageAttachment) r0;	 Catch:{ Exception -> 0x008c }
            r11 = r0;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r39 = r11.getImageURL();	 Catch:{ Exception -> 0x008c }
            r38 = r38.isAlreadyLoaded(r39);	 Catch:{ Exception -> 0x008c }
            if (r38 == 0) goto L_0x0a83;
        L_0x0a5e:
            r38 = r5.getChildAt(r10);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r39 = r0;
            r39 = r39.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r40 = r11.getImageURL();	 Catch:{ Exception -> 0x008c }
            r39 = r39.get(r40);	 Catch:{ Exception -> 0x008c }
            r40 = 1;
            r0 = r38;
            r1 = r39;
            r2 = r40;
            r11.setImage(r0, r1, r2);	 Catch:{ Exception -> 0x008c }
        L_0x0a7f:
            r10 = r10 + 1;
            goto L_0x0993;
        L_0x0a83:
            r38 = r5.getChildAt(r10);	 Catch:{ Exception -> 0x008c }
            r0 = r38;
            r11.clearImage(r0);	 Catch:{ Exception -> 0x008c }
            goto L_0x0a7f;
        L_0x0a8d:
            if (r34 != 0) goto L_0x023b;
        L_0x0a8f:
            r35 = new android.widget.TextView;	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.getContext();	 Catch:{ Exception -> 0x008c }
            r0 = r35;
            r1 = r37;
            r0.<init>(r1);	 Catch:{ Exception -> 0x008c }
            r0 = r35;
            r0 = (android.widget.TextView) r0;	 Catch:{ Exception -> 0x0cdd }
            r37 = r0;
            r38 = "UNKNOWN!!";
            r37.setText(r38);	 Catch:{ Exception -> 0x0cdd }
            r34 = r35;
            goto L_0x023b;
        L_0x0ab1:
            r23 = 0;
            goto L_0x024b;
        L_0x0ab5:
            if (r23 == 0) goto L_0x0adf;
        L_0x0ab7:
            r37 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x008c }
            r38 = java.lang.String.valueOf(r15);	 Catch:{ Exception -> 0x008c }
            r37.<init>(r38);	 Catch:{ Exception -> 0x008c }
            r38 = " ";
            r37 = r37.append(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.getResources();	 Catch:{ Exception -> 0x008c }
            r39 = 2131231003; // 0x7f08011b float:1.8078075E38 double:1.052968022E-314;
            r38 = r38.getString(r39);	 Catch:{ Exception -> 0x008c }
            r37 = r37.append(r38);	 Catch:{ Exception -> 0x008c }
            r15 = r37.toString();	 Catch:{ Exception -> 0x008c }
        L_0x0adf:
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0 = r0.displayablePreviewText;	 Catch:{ Exception -> 0x008c }
            r18 = r0;
            goto L_0x0268;
        L_0x0aeb:
            if (r23 == 0) goto L_0x0268;
        L_0x0aed:
            r37 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x008c }
            r38 = java.lang.String.valueOf(r15);	 Catch:{ Exception -> 0x008c }
            r37.<init>(r38);	 Catch:{ Exception -> 0x008c }
            r38 = " ";
            r37 = r37.append(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.getResources();	 Catch:{ Exception -> 0x008c }
            r39 = 2131231004; // 0x7f08011c float:1.8078077E38 double:1.0529680224E-314;
            r38 = r38.getString(r39);	 Catch:{ Exception -> 0x008c }
            r37 = r37.append(r38);	 Catch:{ Exception -> 0x008c }
            r15 = r37.toString();	 Catch:{ Exception -> 0x008c }
            goto L_0x0268;
        L_0x0b17:
            if (r23 == 0) goto L_0x0b41;
        L_0x0b19:
            r37 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x008c }
            r38 = java.lang.String.valueOf(r15);	 Catch:{ Exception -> 0x008c }
            r37.<init>(r38);	 Catch:{ Exception -> 0x008c }
            r38 = " ";
            r37 = r37.append(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.getResources();	 Catch:{ Exception -> 0x008c }
            r39 = 2131231005; // 0x7f08011d float:1.8078079E38 double:1.052968023E-314;
            r38 = r38.getString(r39);	 Catch:{ Exception -> 0x008c }
            r37 = r37.append(r38);	 Catch:{ Exception -> 0x008c }
            r15 = r37.toString();	 Catch:{ Exception -> 0x008c }
        L_0x0b41:
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0 = r0.attachments;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 0;
            r37 = r37.get(r38);	 Catch:{ Exception -> 0x008c }
            r37 = (com.vkontakte.android.VideoAttachment) r37;	 Catch:{ Exception -> 0x008c }
            r0 = r37;
            r0 = r0.title;	 Catch:{ Exception -> 0x008c }
            r18 = r0;
            goto L_0x0268;
        L_0x0b5b:
            if (r23 == 0) goto L_0x0b85;
        L_0x0b5d:
            r37 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x008c }
            r38 = java.lang.String.valueOf(r15);	 Catch:{ Exception -> 0x008c }
            r37.<init>(r38);	 Catch:{ Exception -> 0x008c }
            r38 = " ";
            r37 = r37.append(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.getResources();	 Catch:{ Exception -> 0x008c }
            r39 = 2131231006; // 0x7f08011e float:1.807808E38 double:1.0529680234E-314;
            r38 = r38.getString(r39);	 Catch:{ Exception -> 0x008c }
            r37 = r37.append(r38);	 Catch:{ Exception -> 0x008c }
            r15 = r37.toString();	 Catch:{ Exception -> 0x008c }
        L_0x0b85:
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0 = r0.displayablePreviewText;	 Catch:{ Exception -> 0x008c }
            r18 = r0;
            goto L_0x0268;
        L_0x0b91:
            if (r23 == 0) goto L_0x0bbb;
        L_0x0b93:
            r37 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x008c }
            r38 = java.lang.String.valueOf(r15);	 Catch:{ Exception -> 0x008c }
            r37.<init>(r38);	 Catch:{ Exception -> 0x008c }
            r38 = " ";
            r37 = r37.append(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.getResources();	 Catch:{ Exception -> 0x008c }
            r39 = 2131231007; // 0x7f08011f float:1.8078083E38 double:1.052968024E-314;
            r38 = r38.getString(r39);	 Catch:{ Exception -> 0x008c }
            r37 = r37.append(r38);	 Catch:{ Exception -> 0x008c }
            r15 = r37.toString();	 Catch:{ Exception -> 0x008c }
        L_0x0bbb:
            r0 = r7.parent;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r37;
            r0 = r0.displayablePreviewText;	 Catch:{ Exception -> 0x008c }
            r18 = r0;
            goto L_0x0268;
        L_0x0bc7:
            r37 = "";
            goto L_0x0290;
        L_0x0bcb:
            r37 = 0;
            goto L_0x02a5;
        L_0x0bcf:
            r6 = r37.next();	 Catch:{ Exception -> 0x008c }
            r6 = (com.vkontakte.android.Attachment) r6;	 Catch:{ Exception -> 0x008c }
            r0 = r6 instanceof com.vkontakte.android.ImageAttachment;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            if (r38 == 0) goto L_0x02bc;
        L_0x0bdb:
            r6 = (com.vkontakte.android.ImageAttachment) r6;	 Catch:{ Exception -> 0x008c }
            r27 = r6.getImageURL();	 Catch:{ Exception -> 0x008c }
            goto L_0x02c2;
        L_0x0be3:
            r0 = r9.postPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 0;
            r37.setVisibility(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r37 = r37.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r37;
            r1 = r27;
            r37 = r0.isAlreadyLoaded(r1);	 Catch:{ Exception -> 0x008c }
            if (r37 == 0) goto L_0x0c1b;
        L_0x0c00:
            r0 = r9.postPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r0 = r42;
            r0 = com.vkontakte.android.NotificationsView.this;	 Catch:{ Exception -> 0x008c }
            r38 = r0;
            r38 = r38.imgWrapper;	 Catch:{ Exception -> 0x008c }
            r0 = r38;
            r1 = r27;
            r38 = r0.get(r1);	 Catch:{ Exception -> 0x008c }
            r37.setImageBitmap(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x02cd;
        L_0x0c1b:
            r0 = r9.postPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 2130838064; // 0x7f020230 float:1.72811E38 double:1.0527738843E-314;
            r37.setImageResource(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x02cd;
        L_0x0c27:
            r0 = r7.action;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 4;
            r0 = r37;
            r1 = r38;
            if (r0 != r1) goto L_0x0c39;
        L_0x0c33:
            r0 = r7.ppost;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x02fa;
        L_0x0c39:
            r0 = r7.action;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 5;
            r0 = r37;
            r1 = r38;
            if (r0 == r1) goto L_0x02fa;
        L_0x0c45:
            r0 = r9.link;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x0c54;
        L_0x0c4b:
            r0 = r9.link;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 8;
            r37.setVisibility(r38);	 Catch:{ Exception -> 0x008c }
        L_0x0c54:
            r0 = r9.postPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x0c63;
        L_0x0c5a:
            r0 = r9.postPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 8;
            r37.setVisibility(r38);	 Catch:{ Exception -> 0x008c }
        L_0x0c63:
            r0 = r9.postPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x02fa;
        L_0x0c69:
            r0 = r9.postPhoto;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 0;
            r37.setTag(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r9.info;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 0;
            r37.setTag(r38);	 Catch:{ Exception -> 0x008c }
            r0 = r9.link;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            if (r37 == 0) goto L_0x02fa;
        L_0x0c81:
            r0 = r9.link;	 Catch:{ Exception -> 0x008c }
            r37 = r0;
            r38 = 0;
            r37.setTag(r38);	 Catch:{ Exception -> 0x008c }
            goto L_0x02fa;
        L_0x0c8c:
            r37 = 500; // 0x1f4 float:7.0E-43 double:2.47E-321;
            r0 = r34;
            r1 = r37;
            r16 = r0.findViewById(r1);	 Catch:{ Exception -> 0x008c }
            r16 = (android.widget.ImageView) r16;	 Catch:{ Exception -> 0x008c }
            goto L_0x03a3;
        L_0x0c9a:
            r37 = 2130837959; // 0x7f0201c7 float:1.7280887E38 double:1.0527738324E-314;
            goto L_0x03ac;
        L_0x0c9f:
            r28 = 0;
            goto L_0x03d8;
        L_0x0ca3:
            r22 = 0;
            goto L_0x0400;
        L_0x0ca7:
            if (r43 == 0) goto L_0x0cab;
        L_0x0ca9:
            if (r28 == 0) goto L_0x0cb7;
        L_0x0cab:
            r37 = 2130837681; // 0x7f0200b1 float:1.7280323E38 double:1.052773695E-314;
            r0 = r34;
            r1 = r37;
            r0.setBackgroundResource(r1);	 Catch:{ Exception -> 0x008c }
            goto L_0x0096;
        L_0x0cb7:
            r37 = r42.getCount();	 Catch:{ Exception -> 0x008c }
            r37 = r37 + -1;
            r0 = r43;
            r1 = r37;
            if (r0 == r1) goto L_0x0cc5;
        L_0x0cc3:
            if (r22 == 0) goto L_0x0cd1;
        L_0x0cc5:
            r37 = 2130837677; // 0x7f0200ad float:1.7280315E38 double:1.052773693E-314;
            r0 = r34;
            r1 = r37;
            r0.setBackgroundResource(r1);	 Catch:{ Exception -> 0x008c }
            goto L_0x0096;
        L_0x0cd1:
            r37 = 2130837679; // 0x7f0200af float:1.7280319E38 double:1.052773694E-314;
            r0 = r34;
            r1 = r37;
            r0.setBackgroundResource(r1);	 Catch:{ Exception -> 0x008c }
            goto L_0x0096;
        L_0x0cdd:
            r36 = move-exception;
            r34 = r35;
            goto L_0x008d;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.NotificationsView.NotificationsAdapter.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
        }
    }

    private static class ViewHolder {
        ImageView actionIcon;
        TextView info;
        TextView link;
        TextView name;
        PhotoStripView photoStrip;
        ImageView postPhoto;
        TextView text;
        ImageView userPhoto;

        public ViewHolder(View v) {
            this.userPhoto = (ImageView) v.findViewById(C0436R.id.nc_user_photo);
            this.postPhoto = (ImageView) v.findViewById(C0436R.id.nc_post_photo);
            this.actionIcon = (ImageView) v.findViewById(C0436R.id.nc_action_icon);
            this.name = (TextView) v.findViewById(C0436R.id.nc_user_name);
            this.text = (TextView) v.findViewById(C0436R.id.nc_comment_text);
            this.info = (TextView) v.findViewById(C0436R.id.nc_info);
            this.link = (TextView) v.findViewById(C0436R.id.nc_link);
            this.photoStrip = (PhotoStripView) v.findViewById(C0436R.id.nc_user_image1);
        }
    }

    /* renamed from: com.vkontakte.android.NotificationsView.3 */
    class C13303 implements OnPhotoClickListener {
        C13303() {
        }

        public void onPhotoClick(PhotoStripView view, int index) {
            List<UserProfile> list = (List) view.getTag();
            Bundle args = new Bundle();
            args.putInt("id", ((UserProfile) list.get(index)).uid);
            Navigate.to("ProfileFragment", args, (Activity) NotificationsView.this.getContext());
        }
    }

    /* renamed from: com.vkontakte.android.NotificationsView.6 */
    class C13316 implements Callback {
        private final /* synthetic */ NotificationEntry val$e;

        C13316(NotificationEntry notificationEntry) {
            this.val$e = notificationEntry;
        }

        public void success(int likes, int retweets, int postID) {
            this.val$e.numLikes = likes;
            this.val$e.isLiked = !this.val$e.isLiked;
            NotificationsView.this.updateList();
        }

        public void fail(int ecode, String emsg) {
        }
    }

    /* renamed from: com.vkontakte.android.NotificationsView.8 */
    class C13328 implements NotificationsGet.Callback {
        private final /* synthetic */ boolean val$fromCache;
        private final /* synthetic */ boolean val$refresh;

        C13328(boolean z, boolean z2) {
            this.val$fromCache = z;
            this.val$refresh = z2;
        }

        public void success(List<NotificationEntry> n, int count, int newOffset, String newFrom) {
            int i;
            boolean z = true;
            int i2 = 0;
            NotificationsView.this.list.setDraggingEnabled(true);
            if (!this.val$fromCache && NotificationsView.this.offset == 0) {
                LongPollService.numNotifications = 0;
                VKApplication.context.sendBroadcast(new Intent(LongPollService.ACTION_COUNTERS_UPDATED), permission.ACCESS_DATA);
            }
            NotificationsView.this.offset = newOffset;
            NotificationsView.this.from = newFrom;
            if (this.val$refresh) {
                NotificationsView.notifications.clear();
                NotificationsView.preloaded.clear();
                NotificationsView.lastUpdateTime = (int) (System.currentTimeMillis() / 1000);
                NotificationsView.this.getContext().getSharedPreferences(null, 0).edit().putInt("notifications_updated", NotificationsView.lastUpdateTime).commit();
            }
            if (NotificationsView.this.preloading) {
                NotificationsView.preloaded.addAll(n);
            } else if (n.size() > 20) {
                NotificationsView.notifications.addAll(n.subList(0, 20));
                NotificationsView.preloaded.addAll(n.subList(20, n.size()));
            } else {
                NotificationsView.notifications.addAll(n);
            }
            NotificationsView.this.preloading = false;
            if (NotificationsView.this.preloadOnReady) {
                NotificationsView.this.preloading = true;
                NotificationsView.this.preloadOnReady = false;
                NotificationsView.this.loadData(false);
            }
            NotificationsView.this.updateList();
            if (this.val$refresh) {
                NotificationsView.this.list.refreshDone();
            }
            NotificationsView notificationsView = NotificationsView.this;
            if (NotificationsView.notifications.size() >= count || n.size() <= 0) {
                z = false;
            }
            notificationsView.moreAvailable = z;
            NotificationsView.this.progress.setVisibility(8);
            View childAt = NotificationsView.this.footerView.getChildAt(0);
            if (NotificationsView.this.moreAvailable) {
                i = 0;
            } else {
                i = 8;
            }
            childAt.setVisibility(i);
            NotificationsView.this.dataLoading = false;
            EmptyView access$20 = NotificationsView.this.noNewsView;
            if (count != 0) {
                i2 = 8;
            }
            access$20.setVisibility(i2);
            NotificationsView.this.currentReq = null;
            if (NotificationsView.this.afterLoad != null && this.val$fromCache) {
                NotificationsView.this.afterLoad.run();
            }
            NotificationsView.this.afterLoad = null;
        }

        public void fail(int ecode, String emsg) {
            NotificationsView.this.dataLoading = false;
            if (this.val$refresh) {
                NotificationsView.this.list.refreshDone();
            }
            if (NotificationsView.notifications.size() == 0) {
                NotificationsView.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(NotificationsView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(NotificationsView.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
            } else {
                Toast.makeText(NotificationsView.this.getContext(), C0436R.string.err_text, 0).show();
            }
            NotificationsView.this.currentReq = null;
        }
    }

    private class NotificationsImagesAdapter extends ListImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.NotificationsView.NotificationsImagesAdapter.1 */
        class C03761 implements Runnable {
            private final /* synthetic */ int val$_image;
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$item;
            private final /* synthetic */ View val$view;

            C03761(int i, View view, int i2, Bitmap bitmap) {
                this.val$item = i;
                this.val$view = view;
                this.val$_image = i2;
                this.val$bitmap = bitmap;
            }

            public void run() {
                try {
                    View v;
                    NotificationEntry e = (NotificationEntry) NotificationsView.notifications.get(this.val$item);
                    if (e.feedbackType == 3 && this.val$view != null) {
                        if (this.val$_image == 0) {
                            v = this.val$view.findViewById(C0436R.id.nc_user_photo);
                            if (v != null) {
                                ((ImageView) v).setImageBitmap(this.val$bitmap);
                            }
                        }
                        if (this.val$_image == 1) {
                            v = this.val$view.findViewById(C0436R.id.nc_post_photo);
                            if (v != null) {
                                ((ImageView) v).setImageBitmap(this.val$bitmap);
                            }
                        }
                    }
                    if ((e.feedbackType == 1 || e.feedbackType == 4) && this.val$view != null) {
                        if (this.val$_image >= Math.min(e.users.size(), 6)) {
                            v = this.val$view.findViewById(C0436R.id.nc_post_photo);
                            if (v != null) {
                                ((ImageView) v).setImageBitmap(this.val$bitmap);
                            }
                        } else if (this.val$_image == 0) {
                            ImageView v2 = (ImageView) this.val$view.findViewById(C0436R.id.nc_user_photo);
                            if (v2 != null) {
                                v2.setImageBitmap(this.val$bitmap);
                            }
                        } else {
                            ((PhotoStripView) this.val$view.findViewById(C0436R.id.nc_user_image1)).setBitmap(this.val$_image - 1, this.val$bitmap);
                        }
                    }
                    if (e.feedbackType == 2) {
                        try {
                            int image = this.val$_image;
                            if (image == 0) {
                                ((ImageView) this.val$view.findViewById(C0436R.id.user_photo)).setImageBitmap(this.val$bitmap);
                                return;
                            }
                            Attachment att;
                            if (image == 1) {
                                if (e.parent.flag(32)) {
                                    ((ImageView) this.val$view.findViewById(C0436R.id.post_retweet_photo)).setImageBitmap(this.val$bitmap);
                                    return;
                                }
                            }
                            if (e.parent.flag(32)) {
                                image--;
                            }
                            image--;
                            int imgindex = 0;
                            int index = 0;
                            Iterator it = e.parent.repostAttachments.iterator();
                            while (it.hasNext()) {
                                att = (Attachment) it.next();
                                if (att instanceof ImageAttachment) {
                                    if (imgindex == image) {
                                        ((ImageAttachment) att).setImage(((ViewGroup) this.val$view.findViewById(C0436R.id.post_repost_attach_container)).getChildAt(index), this.val$bitmap, false);
                                    }
                                    imgindex++;
                                }
                                index++;
                            }
                            index = 0;
                            it = e.parent.attachments.iterator();
                            while (it.hasNext()) {
                                att = (Attachment) it.next();
                                if (att instanceof ImageAttachment) {
                                    if (imgindex == image) {
                                        ((ImageAttachment) att).setImage(((ViewGroup) this.val$view.findViewById(C0436R.id.post_attach_container)).getChildAt(index), this.val$bitmap, false);
                                    }
                                    imgindex++;
                                }
                                index++;
                            }
                        } catch (Exception e2) {
                        }
                    }
                } catch (Exception e3) {
                }
            }
        }

        private NotificationsImagesAdapter() {
        }

        public int getItemCount() {
            return NotificationsView.notifications.size();
        }

        public int getImageCountForItem(int item) {
            int i = 0;
            NotificationEntry e = (NotificationEntry) NotificationsView.notifications.get(item);
            if (e.feedbackType == 5) {
                return 0;
            }
            if (e.feedbackType != 2) {
                String postphoto = null;
                if (e.parent != null) {
                    Iterator it = e.parent.attachments.iterator();
                    while (it.hasNext()) {
                        Attachment att = (Attachment) it.next();
                        if (att instanceof ImageAttachment) {
                            postphoto = ((ImageAttachment) att).getImageURL();
                            break;
                        }
                    }
                }
                if (e.feedbackType == 3) {
                    if (postphoto != null) {
                        i = 1;
                    }
                    return i + 1;
                } else if (e.feedbackType != 1 && e.feedbackType != 4) {
                    return 0;
                } else {
                    int min = Math.min(e.users.size(), 6);
                    if (postphoto != null) {
                        i = 1;
                    }
                    return i + min;
                }
            }
            int count = 1;
            Iterator it2 = e.parent.attachments.iterator();
            while (it2.hasNext()) {
                if (((Attachment) it2.next()) instanceof ImageAttachment) {
                    count++;
                }
            }
            it2 = e.parent.repostAttachments.iterator();
            while (it2.hasNext()) {
                if (((Attachment) it2.next()) instanceof ImageAttachment) {
                    count++;
                }
            }
            if (e.parent.flag(32)) {
                count++;
            }
            return count;
        }

        public String getImageURL(int item, int image) {
            Iterator it;
            Attachment att;
            NotificationEntry e = (NotificationEntry) NotificationsView.notifications.get(item);
            if (e.feedbackType == 3) {
                if (image == 0) {
                    return e.commentUser.photo;
                }
                if (image == 1) {
                    if (e.parent == null) {
                        return null;
                    }
                    it = e.parent.attachments.iterator();
                    while (it.hasNext()) {
                        att = (Attachment) it.next();
                        if (att instanceof ImageAttachment) {
                            return ((ImageAttachment) att).getImageURL();
                        }
                    }
                    return null;
                }
            }
            if (e.feedbackType != 1 && e.feedbackType != 4) {
                if (e.feedbackType == 2) {
                    switch (image) {
                        case ValidationActivity.VRESULT_NONE /*0*/:
                            return e.parent.userPhotoURL;
                        case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                            if (e.parent.flag(32)) {
                                return e.parent.retweetUserPhoto;
                            }
                            break;
                    }
                    if (e.parent.flag(32)) {
                        image--;
                    }
                    image--;
                    int i = 0;
                    it = e.parent.repostAttachments.iterator();
                    while (it.hasNext()) {
                        att = (Attachment) it.next();
                        if (att instanceof ImageAttachment) {
                            if (i == image) {
                                return ((ImageAttachment) att).getImageURL();
                            }
                            i++;
                        }
                    }
                    it = e.parent.attachments.iterator();
                    while (it.hasNext()) {
                        att = (Attachment) it.next();
                        if (att instanceof ImageAttachment) {
                            if (i == image) {
                                return ((ImageAttachment) att).getImageURL();
                            }
                            i++;
                        }
                    }
                }
                return null;
            } else if (image != Math.min(e.users.size(), 6)) {
                return ((UserProfile) e.users.get(image)).photo;
            } else {
                if (e.parent == null) {
                    return null;
                }
                it = e.parent.attachments.iterator();
                while (it.hasNext()) {
                    att = (Attachment) it.next();
                    if (att instanceof ImageAttachment) {
                        return ((ImageAttachment) att).getImageURL();
                    }
                }
                return null;
            }
        }

        public void imageLoaded(int _item, int _image, Bitmap bitmap) {
            int item = _item;
            if (NotificationsView.this.list.getHeaderViewsCount() + item >= NotificationsView.this.list.getFirstVisiblePosition() && NotificationsView.this.list.getHeaderViewsCount() + item <= NotificationsView.this.list.getLastVisiblePosition()) {
                NotificationsView.this.post(new C03761(item, NotificationsView.this.list.getChildAt((NotificationsView.this.list.getHeaderViewsCount() + item) - NotificationsView.this.list.getFirstVisiblePosition()), _image, bitmap));
            }
        }
    }

    static {
        notifications = new Vector();
        preloaded = new Vector();
        lastUpdateTime = 0;
    }

    public NotificationsView(Context context) {
        super(context);
        init();
    }

    public NotificationsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public NotificationsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        r3 = new String[4][][];
        r3[0] = new String[][]{getResources().getStringArray(C0436R.array.ntf_like_m), getResources().getStringArray(C0436R.array.ntf_like_f), getResources().getStringArray(C0436R.array.ntf_like_x)};
        r3[1] = new String[][]{getResources().getStringArray(C0436R.array.ntf_rt_m), getResources().getStringArray(C0436R.array.ntf_rt_f), getResources().getStringArray(C0436R.array.ntf_rt_x)};
        r3[2] = new String[][]{getResources().getStringArray(C0436R.array.ntf_follow_m), getResources().getStringArray(C0436R.array.ntf_follow_f), getResources().getStringArray(C0436R.array.ntf_follow_x)};
        r3[3] = new String[][]{getResources().getStringArray(C0436R.array.ntf_accepted_m), getResources().getStringArray(C0436R.array.ntf_accepted_f), getResources().getStringArray(C0436R.array.ntf_accepted_x)};
        this.langInfo = r3;
        setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        this.footerView = new FrameLayout(getContext());
        ProgressBar pb = new ProgressBar(getContext());
        LayoutParams lp = new LayoutParams(Global.scale(BitmapDescriptorFactory.HUE_ORANGE), Global.scale(BitmapDescriptorFactory.HUE_ORANGE));
        lp.gravity = 17;
        pb.setLayoutParams(lp);
        this.footerView.setPadding(0, Global.scale(7.0f), 0, Global.scale(7.0f));
        this.footerView.addView(pb);
        pb.setVisibility(8);
        this.list = new RefreshableListView(getContext());
        this.list.addFooterView(this.footerView);
        if (this.list.getAdapter() == null) {
            RefreshableListView refreshableListView = this.list;
            ListAdapter notificationsAdapter = new NotificationsAdapter();
            this.adapter = notificationsAdapter;
            refreshableListView.setAdapter(notificationsAdapter);
        }
        this.list.setDivider(null);
        if (VERSION.SDK_INT < 11) {
            this.list.setCacheColorHint(0);
            this.list.setBackgroundColor(0);
        }
        this.list.setTopColor(getResources().getColor(C0436R.color.cards_bg));
        this.list.setVerticalFadingEdgeEnabled(false);
        this.list.setOnRefreshListener(this);
        addView(this.list);
        this.imgWrapper = new ListImageLoaderWrapper(new NotificationsImagesAdapter(), this.list, this);
        this.progress = new ProgressBar(getContext());
        LayoutParams lp2 = new LayoutParams(-2, -2);
        lp2.gravity = 17;
        this.progress.setLayoutParams(lp2);
        this.progress.setVisibility(getCount() == 0 ? 0 : 8);
        addView(this.progress);
        this.noNewsView = EmptyView.create(getContext());
        this.noNewsView.setText((int) C0436R.string.no_replies);
        this.noNewsView.setVisibility(8);
        addView(this.noNewsView);
        this.profileClickListener = new C03701();
        this.entryClickListener = new C03712();
        this.photoStripClickListener = new C13303();
        this.list.setOnItemClickListener(new C03724());
        this.error = (ErrorView) inflate(getContext(), C0436R.layout.error, null);
        this.error.setVisibility(8);
        this.error.setOnRetryListener(new C03735());
        addView(this.error);
        this.list.setDraggingEnabled(false);
        lastUpdateTime = getContext().getSharedPreferences(null, 0).getInt("notifications_updated", 0);
    }

    public static void reset() {
        notifications.clear();
        preloaded.clear();
    }

    public int getCount() {
        return notifications.size() + preloaded.size();
    }

    private void like(int pos) {
        NotificationEntry e = (NotificationEntry) notifications.get(pos);
        new WallLike(!e.isLiked, e.parent.ownerID, e.feedbackType == 3 ? e.commentID : e.parent.postID, false, e.feedbackType == 3 ? 5 : e.parent.type, e.ppost != null ? e.ppost.type : 0, ACRAConstants.DEFAULT_STRING_VALUE).setCallback(new C13316(e)).exec((View) this);
    }

    public void onDetachedFromWindow() {
        if (this.currentReq != null) {
            this.currentReq.cancel();
        }
    }

    public void onPause() {
        this.imgWrapper.deactivate();
    }

    public void onResume() {
        this.imgWrapper.activate();
        updateList();
    }

    public void refresh() {
        Runnable r = new C03747();
        if (this.dataLoading) {
            this.afterLoad = r;
        } else {
            postDelayed(r, 300);
        }
    }

    public boolean loadData(boolean refresh) {
        if (this.dataLoading) {
            return false;
        }
        boolean fromCache;
        this.dataLoading = true;
        if (refresh) {
            this.offset = 0;
            this.from = "0";
        }
        if (notifications.size() == 0 && LongPollService.numNotifications == 0 && !this.list.isRefreshing()) {
            fromCache = true;
        } else {
            fromCache = false;
        }
        this.currentReq = new NotificationsGet(this.offset, this.from, this.preloading ? 20 : 40, fromCache).setCallback(new C13328(fromCache, refresh)).exec((View) this);
        if (notifications.size() == 0 && LongPollService.numNotifications == 0 && !this.list.isRefreshing()) {
            return true;
        }
        return false;
    }

    public void updateList() {
        ((Activity) getContext()).runOnUiThread(new C03759());
    }

    public void onScrolledToLastItem() {
        if ((this.dataLoading && !this.preloading) || !this.moreAvailable) {
            return;
        }
        if (this.preloading) {
            this.preloading = false;
            this.preloadOnReady = true;
        } else if (preloaded.size() > 0) {
            notifications.addAll(preloaded);
            updateList();
            preloaded.clear();
            this.preloading = true;
            loadData(false);
        } else {
            loadData(false);
        }
    }

    public void onScrollStarted() {
    }

    public void onScrollStopped() {
    }

    public void onRefresh() {
        loadData(true);
    }

    public String getLastUpdatedTime() {
        if (lastUpdateTime > 0) {
            return new StringBuilder(String.valueOf(getResources().getString(C0436R.string.updated))).append(" ").append(Global.langDateRelativeNoDiff(lastUpdateTime, getResources())).toString();
        }
        return getResources().getString(C0436R.string.not_updated);
    }

    public void onScrolled(float offset) {
        AlphaAnimation aa;
        if (offset > ((float) Global.scale(10.0f))) {
            if (this.noNewsView.getTag() == null) {
                aa = new AlphaAnimation(1.0f, 0.0f);
                aa.setFillAfter(true);
                aa.setDuration(200);
                if (this.noNewsView.getVisibility() == 0) {
                    this.noNewsView.startAnimation(aa);
                }
                if (this.progress.getVisibility() == 0) {
                    this.progress.startAnimation(aa);
                }
                this.noNewsView.setTag(Integer.valueOf(0));
            }
        } else if (Integer.valueOf(0).equals(this.noNewsView.getTag())) {
            aa = new AlphaAnimation(0.0f, 1.0f);
            aa.setFillAfter(true);
            aa.setDuration(200);
            if (this.noNewsView.getVisibility() == 0) {
                this.noNewsView.startAnimation(aa);
            }
            if (this.progress.getVisibility() == 0) {
                this.progress.startAnimation(aa);
            }
            this.noNewsView.setTag(null);
        }
    }
}
