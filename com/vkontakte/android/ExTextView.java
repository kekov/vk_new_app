package com.vkontakte.android;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.util.AttributeSet;
import android.widget.RemoteViews.RemoteView;
import android.widget.TextView;

@RemoteView
public class ExTextView extends TextView {

    private class ResourceImageGetter implements ImageGetter {
        private ResourceImageGetter() {
        }

        public Drawable getDrawable(String source) {
            if (source.startsWith("empty,")) {
                String[] t = source.split(",");
                ColorDrawable cd = new ColorDrawable(-2147418368);
                cd.setBounds(0, 0, Integer.parseInt(t[1]), Integer.parseInt(t[2]));
                return cd;
            }
            try {
                Drawable drawable = ExTextView.this.getResources().getDrawable(Integer.parseInt(source));
                drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
                return drawable;
            } catch (Exception e) {
                return null;
            }
        }
    }

    private class URLSpanNoUnderline extends URLSpan {
        public URLSpanNoUnderline(String url) {
            super(url);
        }

        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            if (ds.drawableState != null) {
                if (ExTextView.this.isPressed() || ExTextView.this.isSelected()) {
                    ds.setColor(-1);
                } else {
                    ds.setColor(-13936518);
                }
            }
        }
    }

    public ExTextView(Context context) {
        super(context);
    }

    public ExTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void onFinishInflate() {
        setFocusable(false);
        setFocusableInTouchMode(false);
    }

    public void setTextEx(CharSequence s) {
        try {
            super.setText(doIt((Spannable) s));
        } catch (Exception e) {
            super.setText(s);
        }
    }

    public void setHTML(String h) {
        try {
            super.setText(Global.replaceEmoji(doIt((Spannable) Html.fromHtml(h, new ResourceImageGetter(), null))));
        } catch (Exception e) {
            super.setText(Html.fromHtml(h, new ResourceImageGetter(), null));
        }
    }

    public void setHTML(String h, ImageGetter igetter) {
        try {
            super.setText(doIt((Spannable) Html.fromHtml(h, igetter, null)));
        } catch (Exception e) {
            super.setText(Html.fromHtml(h, new ResourceImageGetter(), null));
        }
    }

    private Spannable doIt(Spannable s) {
        for (URLSpan span : (URLSpan[]) s.getSpans(0, s.length(), URLSpan.class)) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            s.setSpan(new URLSpanNoUnderline(span.getURL()), start, end, 0);
        }
        return s;
    }
}
