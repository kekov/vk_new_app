package com.vkontakte.android;

import android.graphics.Bitmap;
import android.view.View;

public interface ImageAttachment {
    void clearImage(View view);

    String getImageURL();

    void setImage(View view, Bitmap bitmap, boolean z);
}
