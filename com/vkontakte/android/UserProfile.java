package com.vkontakte.android;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.acra.ACRAConstants;

public class UserProfile implements Parcelable, Indexable {
    public static final Creator<UserProfile> CREATOR;
    public static final int OFFLINE = 0;
    public static final int ONLINE_MOBILE_APP = 3;
    public static final int ONLINE_MOBILE_SITE = 2;
    public static final int ONLINE_SITE = 1;
    public String bdate;
    public int city;
    public int country;
    public Object extra;
    public boolean f151f;
    public String firstName;
    public String fullName;
    public boolean isFriend;
    public String lastName;
    public int online;
    public String photo;
    public int uid;
    public String university;

    /* renamed from: com.vkontakte.android.UserProfile.1 */
    class C05241 implements Creator<UserProfile> {
        C05241() {
        }

        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    }

    public UserProfile(Parcel in) {
        boolean z;
        boolean z2 = true;
        this.firstName = "DELETED";
        this.fullName = "DELETED";
        this.lastName = "DELETED";
        this.photo = "http://vkontakte.ru/images/question_c.gif";
        this.online = OFFLINE;
        this.university = ACRAConstants.DEFAULT_STRING_VALUE;
        this.bdate = null;
        this.uid = in.readInt();
        this.firstName = in.readString();
        this.fullName = in.readString();
        this.photo = in.readString();
        this.online = in.readInt();
        if (in.readInt() == ONLINE_SITE) {
            z = true;
        } else {
            z = false;
        }
        this.f151f = z;
        if (in.readInt() != ONLINE_SITE) {
            z2 = false;
        }
        this.isFriend = z2;
    }

    public UserProfile() {
        this.firstName = "DELETED";
        this.fullName = "DELETED";
        this.lastName = "DELETED";
        this.photo = "http://vkontakte.ru/images/question_c.gif";
        this.online = OFFLINE;
        this.university = ACRAConstants.DEFAULT_STRING_VALUE;
        this.bdate = null;
    }

    static {
        CREATOR = new C05241();
    }

    public int describeContents() {
        return OFFLINE;
    }

    public void writeToParcel(Parcel p, int arg1) {
        int i;
        int i2 = ONLINE_SITE;
        p.writeInt(this.uid);
        p.writeString(this.firstName);
        p.writeString(this.fullName);
        p.writeString(this.photo);
        p.writeInt(this.online);
        if (this.f151f) {
            i = ONLINE_SITE;
        } else {
            i = OFFLINE;
        }
        p.writeInt(i);
        if (!this.isFriend) {
            i2 = OFFLINE;
        }
        p.writeInt(i2);
    }

    public String toString() {
        if (this == null) {
            return "<NULL>";
        }
        return "User {id=" + this.uid + ", name=" + this.fullName + " [" + this.firstName + "/" + this.lastName + "], photo=" + this.photo + ", online=" + this.online + ", extra=" + this.extra + ", gender=" + this.f151f + "}";
    }

    public int hashCode() {
        return this.uid;
    }

    public boolean equals(Object o) {
        if (!(o instanceof UserProfile)) {
            return false;
        }
        UserProfile p = (UserProfile) o;
        if (this.uid == p.uid && this.fullName.equals(p.fullName) && this.photo.equals(p.photo)) {
            return true;
        }
        return false;
    }

    public char[] getIndexChars() {
        char[] cArr = new char[ONLINE_MOBILE_SITE];
        cArr[OFFLINE] = (this.firstName + " ").toLowerCase().charAt(OFFLINE);
        cArr[ONLINE_SITE] = (this.lastName + " ").toLowerCase().charAt(OFFLINE);
        return cArr;
    }

    public boolean matches(String q) {
        return this.fullName.toLowerCase().startsWith(q) || this.firstName.toLowerCase().startsWith(q) || this.lastName.toLowerCase().startsWith(q);
    }
}
