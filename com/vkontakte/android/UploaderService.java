package com.vkontakte.android;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.DisplayMetrics;
import android.widget.RemoteViews;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.PhotoAttachment.Image;
import com.vkontakte.android.api.Document;
import com.vkontakte.android.cache.UserWallCache;
import com.vkontakte.android.data.Messages;
import com.vkontakte.android.data.Posts;
import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import com.vkontakte.android.mediapicker.providers.GalleryPickerProvider;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;
import org.acra.ACRAConstants;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class UploaderService extends Service implements Runnable {
    public static final String ACTION_PHOTO_ADDED = "com.vkontakte.android.PHOTO_ADDED";
    public static final String ACTION_PHOTO_REMOVED = "com.vkontakte.android.PHOTO_REMOVED";
    public static final String ACTION_UPLOAD_DONE = "com.vkontakte.android.UPLOAD_DONE";
    public static final String ACTION_UPLOAD_FAILED = "com.vkontakte.android.UPLOAD_FAILED";
    public static final String ACTION_UPLOAD_PROGRESS = "com.vkontakte.android.UPLOAD_PROGRESS";
    public static final String ACTION_UPLOAD_STARTED = "com.vkontakte.android.UPLOAD_STARTED";
    private static final int ID_NOTIFICATION_DONE = 324;
    private static final int ID_NOTIFICATION_FAIL = 325;
    private static final int ID_NOTIFICATION_PROGRESS = 323;
    private static final int TASK_STATE_DONE = 4;
    private static final int TASK_STATE_FINISHING = 3;
    private static final int TASK_STATE_NEW = 0;
    private static final int TASK_STATE_SAVING = 2;
    private static final int TASK_STATE_UPLOADING = 1;
    public static final int TYPE_ALBUM_PHOTO = 1;
    public static final int TYPE_AUDIO = 3;
    public static final int TYPE_CHAT_PHOTO = 7;
    public static final int TYPE_DOCUMENT = 4;
    public static final int TYPE_MESSAGE_PHOTO = 5;
    public static final int TYPE_PROFILE_PHOTO = 6;
    public static final int TYPE_VIDEO = 2;
    public static final int TYPE_WALL_PHOTO = 0;
    public static UploaderService currentInstance;
    private static Vector<UploadTask> failedTasks;
    private static int lastID;
    private static Vector<UploadTask> taskQueue;
    private HttpPost currentRequest;
    private UploadTask currentTask;
    private int nDone;
    private boolean needCancelCurrent;
    private boolean needUpdateNumber;
    private Notification notification;
    private CharSequence notificationTitle;
    private Builder notifyBuilder;
    private boolean running;
    private RemoteViews uploadView;

    public interface UploadCallback {
        void setProgress(int i, int i2);

        void uploadDone(String str, Attachment attachment);

        boolean uploadFailed();
    }

    public class UploadEntity implements HttpEntity {
        String field;
        String file;
        final String fileHeaderTemplate;
        byte[] footer;
        byte[] header;
        int offset;

        public UploadEntity(String _file, String _field) {
            this.offset = UploaderService.TASK_STATE_NEW;
            this.header = "\r\n--VK-FILE-UPLOAD-BOUNDARY\r\nContent-Disposition: form-data; name=\"photo\"; filename=\"photo.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n".getBytes();
            this.fileHeaderTemplate = "\r\n--VK-FILE-UPLOAD-BOUNDARY\r\nContent-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\nContent-Type: %s\r\n\r\n";
            this.footer = "\r\n--VK-FILE-UPLOAD-BOUNDARY--\r\n".getBytes();
            try {
                this.file = _file;
                this.field = _field;
                if (this.file.startsWith("/")) {
                    this.file = new Uri.Builder().scheme("file").path(this.file).build().toString();
                }
                Object[] objArr = new Object[UploaderService.TYPE_AUDIO];
                objArr[UploaderService.TASK_STATE_NEW] = this.field;
                objArr[UploaderService.TYPE_ALBUM_PHOTO] = UploaderService.this.getFileName(Uri.parse(this.file));
                objArr[UploaderService.TYPE_VIDEO] = UploaderService.this.getContentResolver().getType(Uri.parse(this.file));
                this.header = String.format(Locale.US, "\r\n--VK-FILE-UPLOAD-BOUNDARY\r\nContent-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\nContent-Type: %s\r\n\r\n", objArr).getBytes("UTF-8");
                if (APIController.API_DEBUG) {
                    Log.m525d("vk", "Will upload " + this.file);
                }
            } catch (Exception e) {
            }
        }

        public void consumeContent() throws IOException {
        }

        public InputStream getContent() throws IOException, IllegalStateException {
            return null;
        }

        public Header getContentEncoding() {
            return null;
        }

        public long getContentLength() {
            try {
                AssetFileDescriptor f = UploaderService.this.getContentResolver().openAssetFileDescriptor(Uri.parse(this.file), "r");
                long l = ((long) (this.header.length + this.footer.length)) + f.getLength();
                f.close();
                return l;
            } catch (Exception e) {
                return 0;
            }
        }

        public Header getContentType() {
            return new BasicHeader("Content-Type", "multipart/form-data; boundary=\"VK-FILE-UPLOAD-BOUNDARY\"");
        }

        public boolean isChunked() {
            return false;
        }

        public boolean isRepeatable() {
            return false;
        }

        public boolean isStreaming() {
            return false;
        }

        public void writeTo(OutputStream os) throws IOException {
            FileInputStream is = null;
            AssetFileDescriptor f = null;
            try {
                Intent intent;
                f = UploaderService.this.getContentResolver().openAssetFileDescriptor(Uri.parse(this.file), "r");
                int total = (int) Math.ceil(((double) f.getLength()) / 1024.0d);
                int loaded = UploaderService.TASK_STATE_NEW;
                long last = 0;
                byte[] buffer = new byte[GLRenderBuffer.EGL_SURFACE_SIZE];
                os.write(this.header);
                is = f.createInputStream();
                while (is.available() > 0) {
                    int nread = is.read(buffer);
                    if (nread == -1) {
                        break;
                    }
                    os.write(buffer, UploaderService.TASK_STATE_NEW, nread);
                    if (System.currentTimeMillis() - last >= 800) {
                        int _total = total;
                        int _loaded = loaded;
                        if (!(UploaderService.this.currentTask.type == UploaderService.TYPE_MESSAGE_PHOTO || UploaderService.this.currentTask.type == 0 || UploaderService.this.currentTask.type == UploaderService.TYPE_CHAT_PHOTO || !UploaderService.this.currentTask.showNotification)) {
                            UploaderService.this.updateProgress(_loaded, _total, false);
                        }
                        last = System.currentTimeMillis();
                    }
                    if (UploaderService.this.currentTask.callback != null) {
                        UploaderService.this.currentTask.callback.setProgress(loaded, total);
                    }
                    if (loaded % 10 == 0) {
                        intent = new Intent(UploaderService.ACTION_UPLOAD_PROGRESS);
                        intent.putExtra("done", loaded);
                        intent.putExtra("total", total);
                        intent.putExtra("id", UploaderService.this.currentTask.id);
                        UploaderService.this.sendBroadcast(intent, permission.ACCESS_DATA);
                    }
                    this.offset += GLRenderBuffer.EGL_SURFACE_SIZE;
                    loaded += UploaderService.TYPE_ALBUM_PHOTO;
                }
                if (!(UploaderService.this.currentTask.type == UploaderService.TYPE_MESSAGE_PHOTO || UploaderService.this.currentTask.type == 0 || UploaderService.this.currentTask.type == UploaderService.TYPE_CHAT_PHOTO || !UploaderService.this.currentTask.showNotification)) {
                    UploaderService.this.updateProgress(10, 10, true);
                }
                if (UploaderService.this.currentTask.callback != null) {
                    UploaderService.this.currentTask.callback.setProgress(-1, UploaderService.TYPE_ALBUM_PHOTO);
                }
                intent = new Intent(UploaderService.ACTION_UPLOAD_PROGRESS);
                intent.putExtra("done", loaded);
                intent.putExtra("total", total);
                intent.putExtra("id", UploaderService.this.currentTask.id);
                UploaderService.this.sendBroadcast(intent, permission.ACCESS_DATA);
                os.write(this.footer);
                if (is != null) {
                    try {
                        is.close();
                    } catch (Exception e) {
                    }
                }
                f.close();
            } catch (Throwable x) {
                Log.m532w("vk", x);
                if (is != null) {
                    try {
                        is.close();
                    } catch (Exception e2) {
                    }
                }
                f.close();
            } catch (Throwable th) {
                if (is != null) {
                    try {
                        is.close();
                    } catch (Exception e3) {
                    }
                }
                f.close();
            }
        }
    }

    public static class UploadTask {
        HashMap<String, Attachment> attcahments;
        UploadCallback callback;
        String file;
        int id;
        Object info;
        boolean isTemp;
        String mediaIdString;
        HashMap<String, String> requestParams;
        JSONObject resultObj;
        boolean showNotification;
        int state;
        Vector<UploadTask> sub;
        int type;
        String uploadResponse;
        String uploadServer;

        public UploadTask() {
            this.showNotification = true;
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.10 */
    class AnonymousClass10 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        AnonymousClass10(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                this.val$ut.resultObj = o.getJSONArray("response").getJSONObject(UploaderService.TASK_STATE_NEW);
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.11 */
    class AnonymousClass11 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        AnonymousClass11(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                this.val$ut.resultObj = o.getJSONArray("response").getJSONObject(UploaderService.TASK_STATE_NEW);
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.12 */
    class AnonymousClass12 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        AnonymousClass12(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                JSONObject j = o.getJSONArray("response").getJSONObject(UploaderService.TASK_STATE_NEW);
                this.val$ut.resultObj = j;
                UploadTask uploadTask = this.val$ut;
                String str = "photo" + j.getInt("owner_id") + "_" + j.getInt("pid");
                uploadTask.mediaIdString = str;
                this.val$ut.requestParams.put("attachment", str);
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.13 */
    class AnonymousClass13 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        AnonymousClass13(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                Photo ph = new Photo(o.getJSONArray("response").getJSONObject(UploaderService.TASK_STATE_NEW));
                Intent intent = new Intent(UploaderService.ACTION_PHOTO_ADDED);
                intent.putExtra("photo", ph);
                intent.putExtra("aid", ph.albumID);
                UploaderService.this.sendBroadcast(intent, permission.ACCESS_DATA);
                this.val$ut.id = ph.id;
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.15 */
    class AnonymousClass15 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        AnonymousClass15(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                this.val$ut.resultObj = o.getJSONArray("response").getJSONObject(UploaderService.TASK_STATE_NEW);
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.16 */
    class AnonymousClass16 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        AnonymousClass16(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                NewsEntry e;
                int i;
                Iterator it;
                UploadTask t;
                Attachment att;
                JSONArray sizes;
                ArrayList<Image> photos;
                JSONObject so;
                DisplayMetrics metrics;
                int tSize;
                Intent intent;
                if (this.val$ut.requestParams.containsKey("_edit")) {
                    e = this.val$ut.info;
                    int photoInsertPos = UploaderService.TASK_STATE_NEW;
                    for (i = UploaderService.TASK_STATE_NEW; i < e.attachments.size(); i += UploaderService.TYPE_ALBUM_PHOTO) {
                        if (e.attachments.get(i) instanceof PhotoAttachment) {
                            photoInsertPos = i;
                        }
                    }
                    it = this.val$ut.sub.iterator();
                    while (it.hasNext()) {
                        t = (UploadTask) it.next();
                        if (t.resultObj == null || !t.resultObj.has("sizes")) {
                            att = (Attachment) this.val$ut.attcahments.get(t.file);
                            if (att != null) {
                                e.attachments.add(att);
                            }
                        } else {
                            sizes = t.resultObj.optJSONArray("sizes");
                            photos = new ArrayList();
                            for (i = UploaderService.TASK_STATE_NEW; i < sizes.length(); i += UploaderService.TYPE_ALBUM_PHOTO) {
                                so = sizes.getJSONObject(i);
                                photos.add(new Image(so.optString(WebDialog.DIALOG_PARAM_TYPE, "?").charAt(UploaderService.TASK_STATE_NEW), so.getString("src"), so.getInt("width"), so.getInt("height")));
                            }
                            e.attachments.add(photoInsertPos, new PhotoAttachment((Image[]) photos.toArray(new Image[UploaderService.TASK_STATE_NEW]), t.resultObj.getInt("owner_id"), t.resultObj.optInt("pid", -1), t.resultObj.optInt("aid", -7), t.resultObj.optString("text"), t.resultObj.optInt("user_id"), t.resultObj.optInt("created"), ACRAConstants.DEFAULT_STRING_VALUE));
                            photoInsertPos += UploaderService.TYPE_ALBUM_PHOTO;
                        }
                    }
                    if (Global.displayDensity > 1.0f || Global.isTablet) {
                        metrics = VKApplication.context.getResources().getDisplayMetrics();
                        tSize = Math.min(Math.min(metrics.widthPixels, metrics.heightPixels) - Global.scale(66.0f), 604);
                        ZhukovLayout.processThumbs(tSize, (int) (((float) tSize) * 0.666f), e.attachments);
                    }
                    e.text = (String) this.val$ut.requestParams.get(LongPollService.EXTRA_MESSAGE);
                    intent = new Intent(Posts.ACTION_POST_REPLACED_BROADCAST);
                    intent.putExtra("entry", e);
                    UploaderService.this.sendBroadcast(intent);
                    return;
                }
                String str;
                SharedPreferences prefs = UploaderService.this.getSharedPreferences(null, UploaderService.TASK_STATE_NEW);
                e = new NewsEntry();
                e.postID = o.optJSONObject("response").optInt("post_id");
                e.text = Global.replaceMentions((String) this.val$ut.requestParams.get(LongPollService.EXTRA_MESSAGE));
                e.time = ((int) (System.currentTimeMillis() / 1000)) + Global.timeDiff;
                e.userID = this.val$ut.requestParams.containsKey("from_group") ? Integer.parseInt((String) this.val$ut.requestParams.get("owner_id")) : Global.uid;
                e.ownerID = Integer.parseInt((String) this.val$ut.requestParams.get("owner_id"));
                e.userName = this.val$ut.requestParams.containsKey("from_group") ? (String) this.val$ut.requestParams.get("_group_title") : prefs.getString("username", "DELETED");
                if (this.val$ut.requestParams.containsKey("from_group")) {
                    str = (String) this.val$ut.requestParams.get("_group_photo");
                } else {
                    str = prefs.getString("userphoto", "http://vkontakte.ru/images/question_b.gif");
                }
                e.userPhotoURL = str;
                e.flags = 66;
                if (Global.displayDensity > 1.0f || Global.isTablet) {
                    metrics = VKApplication.context.getResources().getDisplayMetrics();
                    tSize = Math.min(Math.min(metrics.widthPixels, metrics.heightPixels) - Global.scale(66.0f), 604);
                    ZhukovLayout.processThumbs(tSize, (int) (((float) tSize) * 0.666f), e.attachments);
                }
                if (this.val$ut.sub == null) {
                    sizes = this.val$ut.resultObj.optJSONArray("sizes");
                    photos = new ArrayList();
                    for (i = UploaderService.TASK_STATE_NEW; i < sizes.length(); i += UploaderService.TYPE_ALBUM_PHOTO) {
                        so = sizes.getJSONObject(i);
                        photos.add(new Image(so.optString(WebDialog.DIALOG_PARAM_TYPE, "?").charAt(UploaderService.TASK_STATE_NEW), so.getString("src"), so.getInt("width"), so.getInt("height")));
                    }
                    e.attachments.add(new PhotoAttachment((Image[]) photos.toArray(new Image[UploaderService.TASK_STATE_NEW]), this.val$ut.resultObj.getInt("owner_id"), this.val$ut.resultObj.optInt("pid", -1), this.val$ut.resultObj.optInt("aid", -7), this.val$ut.resultObj.optString("text"), this.val$ut.resultObj.optInt("user_id"), this.val$ut.resultObj.optInt("created"), ACRAConstants.DEFAULT_STRING_VALUE));
                } else {
                    it = this.val$ut.sub.iterator();
                    while (it.hasNext()) {
                        t = (UploadTask) it.next();
                        if (t.resultObj == null || !t.resultObj.has("sizes")) {
                            att = (Attachment) this.val$ut.attcahments.get(t.file);
                            if (att != null) {
                                e.attachments.add(att);
                            }
                        } else {
                            sizes = t.resultObj.optJSONArray("sizes");
                            photos = new ArrayList();
                            for (i = UploaderService.TASK_STATE_NEW; i < sizes.length(); i += UploaderService.TYPE_ALBUM_PHOTO) {
                                so = sizes.getJSONObject(i);
                                photos.add(new Image(so.optString(WebDialog.DIALOG_PARAM_TYPE, "?").charAt(UploaderService.TASK_STATE_NEW), so.getString("src"), so.getInt("width"), so.getInt("height")));
                            }
                            e.attachments.add(new PhotoAttachment((Image[]) photos.toArray(new Image[UploaderService.TASK_STATE_NEW]), t.resultObj.getInt("owner_id"), t.resultObj.optInt("pid", -1), t.resultObj.optInt("aid", -7), t.resultObj.optString("text"), t.resultObj.optInt("user_id"), t.resultObj.optInt("created"), ACRAConstants.DEFAULT_STRING_VALUE));
                        }
                    }
                }
                if (this.val$ut.requestParams.containsKey("lat")) {
                    GeoAttachment gp = new GeoAttachment();
                    gp.lat = Double.parseDouble((String) this.val$ut.requestParams.get("lat"));
                    gp.lon = Double.parseDouble((String) this.val$ut.requestParams.get("long"));
                    e.attachments.add(gp);
                }
                if (this.val$ut.requestParams.containsKey("signed")) {
                    e.attachments.add(new LinkAttachment("http://vkontakte.ru/id" + Global.uid, "- " + prefs.getString("username", "DELETED"), ACRAConstants.DEFAULT_STRING_VALUE));
                }
                this.val$ut.info = e;
                intent = new Intent(Posts.ACTION_NEW_POST_BROADCAST);
                intent.putExtra("entry", e);
                UploaderService.this.sendBroadcast(intent);
                if (e.ownerID == Global.uid) {
                    UserWallCache.add(e, UploaderService.this);
                }
            } catch (Throwable x) {
                Log.m532w("vk", x);
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.1 */
    class C13661 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        C13661(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                this.val$ut.uploadServer = o.getJSONObject("response").getString("upload_url");
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.2 */
    class C13672 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        C13672(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                this.val$ut.uploadServer = o.getJSONObject("response").getString("upload_url");
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.3 */
    class C13683 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        C13683(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                this.val$ut.uploadServer = o.getJSONObject("response").getString("upload_url");
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.4 */
    class C13694 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        C13694(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                this.val$ut.uploadServer = o.getJSONObject("response").getString("upload_url");
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.5 */
    class C13705 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        C13705(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                this.val$ut.uploadServer = o.getJSONObject("response").getString("upload_url");
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.6 */
    class C13716 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        C13716(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                this.val$ut.uploadServer = o.getJSONObject("response").getString("upload_url");
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.7 */
    class C13727 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        C13727(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                this.val$ut.uploadServer = o.getJSONObject("response").getString("upload_url");
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.8 */
    class C13738 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        C13738(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                this.val$ut.uploadServer = o.getJSONObject("response").getString("upload_url");
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.UploaderService.9 */
    class C13749 extends APIHandler {
        private final /* synthetic */ UploadTask val$ut;

        C13749(UploadTask uploadTask) {
            this.val$ut = uploadTask;
        }

        public void success(JSONObject o) {
            try {
                JSONObject j = o.getJSONArray("response").getJSONObject(UploaderService.TASK_STATE_NEW);
                this.val$ut.resultObj = j;
                UploadTask uploadTask = this.val$ut;
                String str = "photo" + j.getInt("owner_id") + "_" + j.getInt("pid");
                uploadTask.mediaIdString = str;
                this.val$ut.requestParams.put("_attachment", str);
            } catch (Exception e) {
            }
        }
    }

    public UploaderService() {
        this.running = false;
        this.nDone = TASK_STATE_NEW;
        this.needUpdateNumber = false;
        this.currentTask = null;
        this.needCancelCurrent = false;
    }

    static {
        taskQueue = new Vector();
        failedTasks = new Vector();
        lastID = (int) (System.currentTimeMillis() / 1000);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onStart(Intent intent, int startId) {
        onStartCommand(intent, TASK_STATE_NEW, startId);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        currentInstance = this;
        if ("RETRY".equals(intent.getAction())) {
            ((NotificationManager) getSystemService("notification")).cancel(ID_NOTIFICATION_FAIL);
            retryAll();
        } else if ("NOTHING".equals(intent.getAction())) {
            if (!this.running) {
                stopSelf();
            }
        } else if ("CANCEL".equals(intent.getAction())) {
            cancel(intent.getIntExtra("id", -1));
        } else if (!"android.intent.action.SEND".equals(intent.getAction())) {
            UploadTask task;
            if (intent.hasExtra("new")) {
                task = new UploadTask();
                task.id = intent.getIntExtra("id", TASK_STATE_NEW);
                task.file = intent.getStringExtra("file");
                task.type = intent.getIntExtra(WebDialog.DIALOG_PARAM_TYPE, TASK_STATE_NEW);
                if (intent.hasExtra("req_params")) {
                    task.requestParams = (HashMap) intent.getSerializableExtra("req_params");
                }
                if (intent.hasExtra("no_notify")) {
                    task.showNotification = false;
                }
                taskQueue.add(task);
            } else if (intent.hasExtra("multiattach")) {
                task = new UploadTask();
                String[] files = intent.getStringArrayExtra("files");
                int[] types = intent.getIntArrayExtra("types");
                task.requestParams = (HashMap) intent.getSerializableExtra("req_params");
                task.sub = new Vector();
                task.attcahments = (HashMap) intent.getSerializableExtra("attachments");
                if (intent.hasExtra("info")) {
                    task.info = intent.getParcelableExtra("info");
                }
                for (int i = TASK_STATE_NEW; i < files.length; i += TYPE_ALBUM_PHOTO) {
                    UploadTask st = new UploadTask();
                    st.file = files[i];
                    st.type = types[i];
                    st.requestParams = task.requestParams;
                    task.sub.add(st);
                }
                taskQueue.add(task);
            } else if (intent.hasExtra("files")) {
                Iterator it = intent.getParcelableArrayListExtra("files").iterator();
                while (it.hasNext()) {
                    Parcelable p = (Parcelable) it.next();
                    task = new UploadTask();
                    if (intent.hasExtra("info")) {
                        task.info = intent.getStringExtra("info");
                    }
                    task.file = p.toString();
                    task.type = intent.getIntExtra(WebDialog.DIALOG_PARAM_TYPE, TASK_STATE_NEW);
                    task.requestParams = (HashMap) intent.getSerializableExtra("req_params");
                    taskQueue.add(task);
                }
            } else {
                task = new UploadTask();
                if (intent.hasExtra("info")) {
                    task.info = intent.getStringExtra("info");
                }
                task.file = intent.getStringExtra("file");
                task.type = intent.getIntExtra(WebDialog.DIALOG_PARAM_TYPE, TASK_STATE_NEW);
                task.requestParams = (HashMap) intent.getSerializableExtra("req_params");
                taskQueue.add(task);
            }
            if (this.running) {
                this.needUpdateNumber = true;
            }
            startThread();
        }
        return TYPE_VIDEO;
    }

    public void setCallbackForFile(String fileURI, UploadCallback callback) {
        if (this.currentTask == null || !this.currentTask.file.equals(fileURI)) {
            Iterator it = taskQueue.iterator();
            while (it.hasNext()) {
                UploadTask ut = (UploadTask) it.next();
                if (ut != null && ut.file != null && ut.file.equals(fileURI)) {
                    ut.callback = callback;
                    return;
                }
            }
            return;
        }
        this.currentTask.callback = callback;
    }

    private void startThread() {
        if (!this.running) {
            new Thread(this).start();
            this.running = true;
        }
    }

    public void run() {
        Log.m528i("vk", "UploaderService started");
        while (taskQueue.size() > 0) {
            try {
                processTask((UploadTask) taskQueue.remove(TASK_STATE_NEW), false);
                this.nDone += TYPE_ALBUM_PHOTO;
                if (taskQueue.size() == 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                    }
                }
            } catch (Exception x) {
                Log.m527e("vk", "Error uploading!", x);
            }
        }
        stopSelf();
        this.nDone = TASK_STATE_NEW;
        Log.m528i("vk", "UploaderService stopped");
        currentInstance = null;
    }

    private void updateProgress(int loaded, int total, boolean indeterminate) {
        Log.m525d("vk", "UploaderService: progress = " + loaded + "/" + total + " [ " + ((int) ((((float) loaded) / ((float) total)) * 100.0f)) + "% ]");
        if (VERSION.SDK_INT < 14) {
            this.uploadView.setProgressBar(C0436R.id.notify_progress, total, loaded, indeterminate);
            if (this.needUpdateNumber) {
                this.uploadView.setTextViewText(C0436R.id.notify_num, new StringBuilder(String.valueOf(this.nDone + TYPE_ALBUM_PHOTO)).append("/").append((this.nDone + taskQueue.size()) + TYPE_ALBUM_PHOTO).toString());
                this.needUpdateNumber = false;
            }
        } else {
            Intent intent = new Intent(this, getClass());
            intent.setAction("NOTHING");
            Builder ongoing;
            Object[] objArr;
            if (this.notifyBuilder == null) {
                ongoing = new Builder(this).setContentTitle(this.notificationTitle).setOngoing(true);
                objArr = new Object[TYPE_VIDEO];
                objArr[TASK_STATE_NEW] = Integer.valueOf(this.nDone + TYPE_ALBUM_PHOTO);
                objArr[TYPE_ALBUM_PHOTO] = Integer.valueOf((this.nDone + taskQueue.size()) + TYPE_ALBUM_PHOTO);
                this.notifyBuilder = ongoing.setContentText(getString(C0436R.string.player_num, objArr)).setProgress(total, loaded, indeterminate).setContentIntent(PendingIntent.getService(this, TASK_STATE_NEW, intent, TASK_STATE_NEW)).setSmallIcon(17301640);
            } else {
                ongoing = this.notifyBuilder;
                objArr = new Object[TYPE_VIDEO];
                objArr[TASK_STATE_NEW] = Integer.valueOf(this.nDone + TYPE_ALBUM_PHOTO);
                objArr[TYPE_ALBUM_PHOTO] = Integer.valueOf((this.nDone + taskQueue.size()) + TYPE_ALBUM_PHOTO);
                ongoing.setContentText(getString(C0436R.string.player_num, objArr)).setProgress(total, loaded, indeterminate);
            }
            this.notification = this.notifyBuilder.getNotification();
        }
        ((NotificationManager) getSystemService("notification")).notify(ID_NOTIFICATION_PROGRESS, this.notification);
    }

    private boolean processTask(UploadTask ut, boolean isSub) {
        if (!isSub) {
            this.currentTask = ut;
        }
        if (ut.sub != null) {
            Log.m528i("vk", "UploaderService: multiattach upload");
            Iterator it = ut.sub.iterator();
            while (it.hasNext()) {
                UploadTask u = (UploadTask) it.next();
                Log.m528i("vk", "UploaderService: uploading subTask = " + u.file);
                if (u.file.startsWith("A")) {
                    u.mediaIdString = u.file.substring(TYPE_ALBUM_PHOTO);
                } else if (!processTask(u, true)) {
                    return false;
                }
            }
            if (post(ut)) {
                ut.state = TYPE_DOCUMENT;
                showDone(ut);
                Log.m528i("vk", "UploaderService: multiattach upload OK!");
                return true;
            }
            if (!this.needCancelCurrent) {
                uploadFailed(this.currentTask);
            }
            this.needCancelCurrent = false;
            return false;
        }
        if (!(ut.type == TYPE_MESSAGE_PHOTO || ut.type == 0 || ut.type == TYPE_CHAT_PHOTO || !ut.showNotification)) {
            showProgress(ut.type);
        }
        Log.m528i("vk", "UploaderService: taskState = NEW");
        if (!(ut.type == TYPE_DOCUMENT || ut.type == TYPE_AUDIO || ut.type == TYPE_VIDEO)) {
            try {
                resizeImageIfNeeded(ut);
            } catch (Exception e) {
                return false;
            }
        }
        if (ut.state == 0) {
            if (getUploadServer(ut)) {
                ut.state = TYPE_ALBUM_PHOTO;
            } else {
                if (!this.needCancelCurrent) {
                    uploadFailed(this.currentTask);
                }
                this.needCancelCurrent = false;
                return false;
            }
        }
        Log.m528i("vk", "UploaderService: taskState = UPLOADING");
        if (ut.state == TYPE_ALBUM_PHOTO) {
            if (doUpload(ut)) {
                ut.state = TYPE_VIDEO;
            } else {
                if (!this.needCancelCurrent) {
                    uploadFailed(this.currentTask);
                }
                this.needCancelCurrent = false;
                return false;
            }
        }
        if (!(ut.type == TYPE_MESSAGE_PHOTO || ut.type == 0 || ut.type == TYPE_CHAT_PHOTO || !ut.showNotification)) {
            updateProgress(TASK_STATE_NEW, TYPE_ALBUM_PHOTO, true);
        }
        if (this.currentTask.callback != null) {
            this.currentTask.callback.setProgress(-1, TYPE_ALBUM_PHOTO);
        }
        Log.m528i("vk", "UploaderService: taskState = SAVING");
        if (ut.state == TYPE_VIDEO) {
            if (saveAfterUpload(ut)) {
                ut.state = TYPE_AUDIO;
            } else {
                if (!this.needCancelCurrent) {
                    uploadFailed(this.currentTask);
                }
                this.needCancelCurrent = false;
                return false;
            }
        }
        Log.m528i("vk", "UploaderService: taskState = FINISHING");
        if (ut.state == TYPE_AUDIO && !isSub) {
            if (post(ut)) {
                ut.state = TYPE_DOCUMENT;
            } else {
                if (!this.needCancelCurrent) {
                    uploadFailed(this.currentTask);
                }
                this.needCancelCurrent = false;
                return false;
            }
        }
        Log.m528i("vk", "UploaderService: taskState = DONE");
        if (!(ut.type == TYPE_MESSAGE_PHOTO || ut.type == 0 || ut.type == TYPE_CHAT_PHOTO || isSub || !ut.showNotification)) {
            showDone(ut);
        }
        if (ut.isTemp) {
            new File(ut.file).delete();
        }
        return true;
    }

    private static String getRealPathFromURI(Uri contentUri) {
        if ("file".equals(contentUri.getScheme())) {
            return contentUri.getPath();
        }
        String[] proj = new String[TYPE_ALBUM_PHOTO];
        proj[TASK_STATE_NEW] = "_data";
        Cursor cursor = VKApplication.context.getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();
        return path;
    }

    private void resizeImageIfNeeded(UploadTask ut) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        File file;
        if (ut.file.startsWith(GalleryPickerProvider.STYLED_IMAGE_URI_START)) {
            String filePath = new File(Environment.getExternalStorageDirectory(), "VK/" + (System.currentTimeMillis() / 100) + ".jpg").getAbsolutePath();
            file = new File(Environment.getExternalStorageDirectory(), "VK");
            if (!file.exists()) {
                file.mkdir();
            }
            String result = GalleryPickerProvider.instance().getPathForStyledImage(ut.file, Global.displayDensity >= ImageViewHolder.PaddingSize ? NewsEntry.FLAG_POSTPONED : GLRenderBuffer.EGL_SURFACE_SIZE, 92, filePath);
            String[] strArr = new String[TYPE_ALBUM_PHOTO];
            strArr[TASK_STATE_NEW] = filePath;
            String[] strArr2 = new String[TYPE_ALBUM_PHOTO];
            strArr2[TASK_STATE_NEW] = "image/jpeg";
            MediaScannerConnection.scanFile(this, strArr, strArr2, null);
            ut.file = result;
            return;
        }
        if (prefs.getBoolean("compressPhotos", true)) {
            Options opts = new Options();
            opts.inJustDecodeBounds = true;
            AssetFileDescriptor f = getContentResolver().openAssetFileDescriptor(Uri.parse(ut.file), "r");
            InputStream is = f.createInputStream();
            BitmapFactory.decodeStream(is, null, opts);
            is.getChannel().position(0);
            Log.m528i("vk", "in img size " + opts.outWidth + "x" + opts.outHeight);
            if ((opts.outWidth > opts.outHeight && opts.outWidth > 1280) || (opts.outWidth <= opts.outHeight && opts.outHeight > GLRenderBuffer.EGL_SURFACE_SIZE)) {
                float sample;
                Bitmap bmp;
                if (opts.outWidth > opts.outHeight) {
                    sample = ((float) Math.max(opts.outWidth, opts.outHeight)) / 1280.0f;
                } else {
                    sample = ((float) Math.max(opts.outWidth, opts.outHeight)) / 1024.0f;
                }
                Log.m525d("vk", "sample size=" + sample);
                sample = (float) Math.round(sample);
                Log.m525d("vk", "new size=" + (((float) opts.outWidth) / sample) + "x" + (((float) opts.outHeight) / sample));
                opts.inJustDecodeBounds = false;
                opts.inSampleSize = (int) sample;
                try {
                    bmp = BitmapFactory.decodeStream(is, null, opts);
                } catch (OutOfMemoryError e) {
                    ImageCache.clearTopLevel();
                    bmp = BitmapFactory.decodeStream(is, null, opts);
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                    return;
                }
                file = new File(Environment.getExternalStorageDirectory(), ".vkontakte");
                if (!file.exists()) {
                    file.mkdirs();
                }
                file = new File(Environment.getExternalStorageDirectory(), ".vkontakte/temp_upload.jpg");
                int rotation = TASK_STATE_NEW;
                if (ut.file.startsWith("content:")) {
                    try {
                        String[] projection = new String[TYPE_ALBUM_PHOTO];
                        projection[TASK_STATE_NEW] = "orientation";
                        Cursor c = getContentResolver().query(Uri.parse(ut.file), projection, null, null, null);
                        if (c.moveToFirst()) {
                            rotation = c.getInt(TASK_STATE_NEW);
                        }
                        Log.m528i("vk", "img rotation is " + rotation);
                    } catch (Exception e2) {
                    }
                }
                if (!ut.file.startsWith("content:") || rotation == 0) {
                    int o = new ExifInterface(getRealPathFromURI(Uri.parse(ut.file))).getAttributeInt("Orientation", TASK_STATE_NEW);
                    Log.m528i("vk", "Exif orientation " + o);
                    switch (o) {
                        case TYPE_ALBUM_PHOTO /*1*/:
                            rotation = TASK_STATE_NEW;
                            break;
                        case TYPE_AUDIO /*3*/:
                            rotation = 180;
                            break;
                        case TYPE_PROFILE_PHOTO /*6*/:
                            rotation = 90;
                            break;
                        case UserListView.TYPE_BLACKLIST /*8*/:
                            rotation = 270;
                            break;
                    }
                }
                if (rotation != 0) {
                    Matrix matrix = new Matrix();
                    matrix.preRotate((float) rotation);
                    try {
                        bmp = Bitmap.createBitmap(bmp, TASK_STATE_NEW, TASK_STATE_NEW, bmp.getWidth(), bmp.getHeight(), matrix, false);
                    } catch (OutOfMemoryError e3) {
                        ImageCache.clearTopLevel();
                        bmp = Bitmap.createBitmap(bmp, TASK_STATE_NEW, TASK_STATE_NEW, bmp.getWidth(), bmp.getHeight(), matrix, false);
                    }
                }
                OutputStream out = new FileOutputStream(file);
                bmp.compress(CompressFormat.JPEG, 92, out);
                bmp.recycle();
                out.close();
                if (Integer.parseInt(VERSION.SDK) >= 9) {
                    String path = ut.file;
                    ExifInterface exifInterface = new ExifInterface(getRealPathFromURI(Uri.parse(ut.file)));
                    ExifInterface dstExif = new ExifInterface(file.getAbsolutePath());
                    dstExif.setAttribute("GPSAltitude", exifInterface.getAttribute("GPSAltitude"));
                    dstExif.setAttribute("GPSAltitudeRef", exifInterface.getAttribute("GPSAltitudeRef"));
                    dstExif.setAttribute("GPSLatitude", exifInterface.getAttribute("GPSLatitude"));
                    dstExif.setAttribute("GPSLatitudeRef", exifInterface.getAttribute("GPSLatitudeRef"));
                    dstExif.setAttribute("GPSLongitude", exifInterface.getAttribute("GPSLongitude"));
                    dstExif.setAttribute("GPSLongitudeRef", exifInterface.getAttribute("GPSLongitudeRef"));
                    dstExif.setAttribute("Make", exifInterface.getAttribute("Make"));
                    dstExif.setAttribute("Model", exifInterface.getAttribute("Model"));
                    dstExif.saveAttributes();
                }
                Log.m525d("vk", "file size = " + file.length());
                ut.file = file.getAbsolutePath();
                ut.isTemp = true;
            }
            is.close();
            f.close();
        }
    }

    private boolean getUploadServer(UploadTask ut) {
        APIRequest req;
        if (ut.type == 0) {
            req = new APIRequest("photos.getWallUploadServer");
            req.param("owner_id", Integer.parseInt((String) ut.requestParams.get("owner_id")));
            return req.handler(new C13661(ut)).execSync();
        } else if (ut.type == TYPE_PROFILE_PHOTO) {
            return new APIRequest("photos.getProfileUploadServer").handler(new C13672(ut)).execSync();
        } else {
            if (ut.type == TYPE_CHAT_PHOTO) {
                req = new APIRequest("photos.getChatUploadServer");
                req.param("chat_id", (String) ut.requestParams.get("chat_id"));
                return req.handler(new C13683(ut)).execSync();
            } else if (ut.type == TYPE_ALBUM_PHOTO) {
                return new APIRequest("photos.getUploadServer").param("album_id", (String) ut.requestParams.get("aid")).param("group_id", ut.requestParams.containsKey("gid") ? (String) ut.requestParams.get("gid") : null).handler(new C13694(ut)).execSync();
            } else if (ut.type == TYPE_MESSAGE_PHOTO) {
                return new APIRequest("photos.getMessagesUploadServer").handler(new C13705(ut)).execSync();
            } else {
                if (ut.type == TYPE_AUDIO) {
                    return new APIRequest("audio.getUploadServer").handler(new C13716(ut)).execSync();
                }
                if (ut.type == TYPE_VIDEO) {
                    return new APIRequest("video.save").param("name", (String) ut.requestParams.get("name")).param(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, (String) ut.requestParams.get(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION)).handler(new C13727(ut)).execSync();
                }
                if (ut.type != TYPE_DOCUMENT) {
                    return false;
                }
                String str;
                String method = (ut.requestParams == null || !ut.requestParams.containsKey("_method")) ? "docs.getUploadServer" : (String) ut.requestParams.get("method");
                APIRequest aPIRequest = new APIRequest(method);
                String str2 = "group_id";
                if (ut.requestParams == null || !ut.requestParams.containsKey("group_id")) {
                    str = null;
                } else {
                    str = (String) ut.requestParams.get("group_id");
                }
                return aPIRequest.param(str2, str).handler(new C13738(ut)).execSync();
            }
        }
    }

    private boolean doUpload(UploadTask ut) {
        HttpResponse response = null;
        InputStream is = null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpParams hParams = httpclient.getParams();
            HttpProtocolParams.setUseExpectContinue(hParams, false);
            HttpProtocolParams.setUserAgent(hParams, APIController.USER_AGENT);
            HttpConnectionParams.setSocketBufferSize(hParams, ACRAConstants.DEFAULT_BUFFER_SIZE_IN_BYTES);
            HttpConnectionParams.setConnectionTimeout(hParams, 30000);
            HttpConnectionParams.setSoTimeout(hParams, 30000);
            HttpPost post = new HttpPost(ut.uploadServer);
            this.currentRequest = post;
            Log.m525d("vk", "FILE=" + ut.file);
            if (ut.type == 0 || ut.type == TYPE_MESSAGE_PHOTO || ut.type == TYPE_PROFILE_PHOTO) {
                post.setEntity(new UploadEntity(ut.file, "photo"));
            } else if (ut.type == TYPE_ALBUM_PHOTO) {
                post.setEntity(new UploadEntity(ut.file, "file1"));
            } else if (ut.type == TYPE_AUDIO) {
                post.setEntity(new UploadEntity(ut.file, "file"));
            } else if (ut.type == TYPE_VIDEO) {
                post.setEntity(new UploadEntity(ut.file, "video_file"));
            } else {
                post.setEntity(new UploadEntity(ut.file, "file"));
            }
            response = httpclient.execute(post);
            is = response.getEntity().getContent();
            if (this.needCancelCurrent) {
                if (is != null) {
                    try {
                        is.close();
                    } catch (Exception e) {
                    }
                }
                if (response != null) {
                    try {
                        response.getEntity().consumeContent();
                    } catch (Exception e2) {
                    }
                }
                return false;
            }
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            byte[] rd = new byte[GLRenderBuffer.EGL_SURFACE_SIZE];
            while (true) {
                int l = is.read(rd);
                if (l <= 0) {
                    String s = new String(buf.toByteArray(), "UTF-8");
                    Log.m525d("vk", s);
                    ut.uploadResponse = s;
                    this.currentRequest = null;
                    is.close();
                    return true;
                }
                buf.write(rd, TASK_STATE_NEW, l);
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e3) {
                }
            }
            if (response != null) {
                try {
                    response.getEntity().consumeContent();
                } catch (Exception e4) {
                }
            }
            this.currentRequest = null;
            return false;
        }
    }

    private boolean saveAfterUpload(UploadTask ut) {
        APIRequest req;
        JSONObject obj;
        Iterator<String> keys;
        String k;
        if (ut.type == 0) {
            req = new APIRequest("photos.saveWallPhoto");
            req.param("photo_sizes", (int) TYPE_ALBUM_PHOTO);
            req.param("device", Build.BRAND + ":" + Build.MANUFACTURER + ":" + Build.MODEL + ":" + Build.PRODUCT);
            try {
                obj = (JSONObject) new JSONTokener(ut.uploadResponse).nextValue();
                keys = obj.keys();
                while (keys.hasNext()) {
                    k = (String) keys.next();
                    req.param(k, obj.getString(k));
                }
            } catch (Exception e) {
            }
            req.param("owner_id", Integer.parseInt((String) ut.requestParams.get("owner_id")));
            return req.handler(new C13749(ut)).execSync();
        } else if (ut.type == TYPE_PROFILE_PHOTO) {
            req = new APIRequest("photos.saveProfilePhoto");
            req.param("device", Build.BRAND + ":" + Build.MANUFACTURER + ":" + Build.MODEL + ":" + Build.PRODUCT);
            try {
                obj = (JSONObject) new JSONTokener(ut.uploadResponse).nextValue();
                keys = obj.keys();
                while (keys.hasNext()) {
                    k = (String) keys.next();
                    req.param(k, obj.getString(k));
                }
            } catch (Exception e2) {
            }
            return req.handler(new AnonymousClass10(ut)).execSync();
        } else if (ut.type == TYPE_CHAT_PHOTO) {
            req = new APIRequest("messages.setChatPhoto");
            try {
                req.param("file", ((JSONObject) new JSONTokener(ut.uploadResponse).nextValue()).getString("response"));
            } catch (Exception e3) {
            }
            return req.handler(new AnonymousClass11(ut)).execSync();
        } else if (ut.type == TYPE_MESSAGE_PHOTO) {
            req = new APIRequest("photos.saveMessagesPhoto");
            try {
                obj = (JSONObject) new JSONTokener(ut.uploadResponse).nextValue();
                keys = obj.keys();
                while (keys.hasNext()) {
                    k = (String) keys.next();
                    req.param(k, obj.getString(k));
                }
            } catch (Exception e4) {
            }
            req.param("photo_sizes", (int) TYPE_ALBUM_PHOTO);
            return req.handler(new AnonymousClass12(ut)).execSync();
        } else if (ut.type == TYPE_ALBUM_PHOTO) {
            req = new APIRequest("photos.save");
            req.param("device", Build.BRAND + ":" + Build.MANUFACTURER + ":" + Build.MODEL + ":" + Build.PRODUCT);
            try {
                obj = (JSONObject) new JSONTokener(ut.uploadResponse).nextValue();
                keys = obj.keys();
                while (keys.hasNext()) {
                    k = (String) keys.next();
                    req.param(k, obj.getString(k));
                }
            } catch (Exception e5) {
            }
            if (ut.requestParams.containsKey("gid")) {
                req.param("group_id", (String) ut.requestParams.get("gid"));
            }
            if (req.params.containsKey("aid")) {
                req.params.put("album_id", (String) req.params.remove("aid"));
            }
            if (ut.requestParams.containsKey("text")) {
                req.param("caption", (String) ut.requestParams.get("text"));
            }
            req.param("photo_sizes", (int) TYPE_ALBUM_PHOTO);
            return req.handler(new AnonymousClass13(ut)).execSync();
        } else if (ut.type == TYPE_AUDIO) {
            req = new APIRequest("audio.save");
            try {
                obj = (JSONObject) new JSONTokener(ut.uploadResponse).nextValue();
                req.param("server", obj.getString("server"));
                req.param("audio", obj.getString("audio"));
                req.param("hash", obj.getString("hash"));
            } catch (Exception e6) {
            }
            return req.handler(new APIHandler() {
                public void success(JSONObject o) {
                }
            }).execSync();
        } else if (ut.type == TYPE_VIDEO) {
            return true;
        } else {
            if (ut.type != TYPE_DOCUMENT) {
                return false;
            }
            req = new APIRequest("docs.save");
            try {
                req.param("file", ((JSONObject) new JSONTokener(ut.uploadResponse).nextValue()).getString("file"));
            } catch (Exception e7) {
            }
            return req.handler(new AnonymousClass15(ut)).execSync();
        }
    }

    private boolean post(UploadTask ut) {
        JSONObject o;
        PhotoAttachment att;
        Intent intent;
        if (ut.type == 0) {
            if (ut.requestParams.containsKey("_nopost")) {
                try {
                    o = new JSONObject();
                    o.put(WebDialog.DIALOG_PARAM_TYPE, "photo");
                    o.put("photo", ut.resultObj);
                    att = (PhotoAttachment) Attachment.parse(o);
                    intent = new Intent(ACTION_UPLOAD_DONE);
                    intent.putExtra("id", ut.id);
                    intent.putExtra("attachment", att);
                    sendBroadcast(intent, permission.ACCESS_DATA);
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                }
                return true;
            }
            APIRequest req = new APIRequest(ut.requestParams.containsKey("_edit") ? "wall.edit" : "wall.post");
            req.param("device", Build.BRAND + ":" + Build.MANUFACTURER + ":" + Build.MODEL + ":" + Build.PRODUCT);
            for (String key : ut.requestParams.keySet()) {
                if (!key.startsWith("_")) {
                    req.param(key, (String) ut.requestParams.get(key));
                }
            }
            String atts = ACRAConstants.DEFAULT_STRING_VALUE;
            Iterator it = ut.sub.iterator();
            while (it.hasNext()) {
                atts = new StringBuilder(String.valueOf(atts)).append(",").append(((UploadTask) it.next()).mediaIdString.split("\\|")[TASK_STATE_NEW]).toString();
            }
            req.param("attachments", atts.substring(TYPE_ALBUM_PHOTO));
            return req.handler(new AnonymousClass16(ut)).execSync();
        } else if (ut.type == TYPE_ALBUM_PHOTO) {
            return true;
        } else {
            if (ut.type == TYPE_MESSAGE_PHOTO) {
                if (ut.callback != null) {
                    try {
                        o = new JSONObject();
                        o.put(WebDialog.DIALOG_PARAM_TYPE, "photo");
                        o.put("photo", ut.resultObj);
                        ut.callback.uploadDone("photo" + Global.uid + "_" + ut.resultObj.optString("pid"), Attachment.parse(o));
                    } catch (Exception e) {
                    }
                }
                try {
                    o = new JSONObject();
                    o.put(WebDialog.DIALOG_PARAM_TYPE, "photo");
                    o.put("photo", ut.resultObj);
                    att = (PhotoAttachment) Attachment.parse(o);
                    intent = new Intent(ACTION_UPLOAD_DONE);
                    intent.putExtra("id", ut.id);
                    intent.putExtra("attachment", att);
                    sendBroadcast(intent, permission.ACCESS_DATA);
                    Messages.uploadDone(ut.id, att);
                } catch (Exception e2) {
                }
                return true;
            } else if (ut.type == TYPE_AUDIO) {
                return true;
            } else {
                if (ut.type == TYPE_VIDEO) {
                    return true;
                }
                if (ut.type == TYPE_DOCUMENT) {
                    Document doc = new Document(ut.resultObj);
                    intent = new Intent(ACTION_UPLOAD_DONE);
                    intent.putExtra("id", ut.id);
                    intent.putExtra("attachment", new DocumentAttachment(doc));
                    sendBroadcast(intent, permission.ACCESS_DATA);
                    Messages.uploadDone(ut.id, new DocumentAttachment(doc));
                    return true;
                } else if (ut.type == TYPE_CHAT_PHOTO) {
                    intent = new Intent(ACTION_UPLOAD_DONE);
                    intent.putExtra("id", ut.id);
                    sendBroadcast(intent, permission.ACCESS_DATA);
                    return true;
                } else if (ut.type != TYPE_PROFILE_PHOTO) {
                    return false;
                } else {
                    return new APIRequest("getProfiles").param("user_ids", Global.uid).param("fields", Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec").handler(new APIHandler() {
                        public void success(JSONObject r) {
                            try {
                                String photo = r.getJSONArray("response").getJSONObject(UploaderService.TASK_STATE_NEW).getString(Global.displayDensity > 1.0f ? "photo_medium_rec" : "photo_rec");
                                UploaderService.this.getSharedPreferences(null, UploaderService.TASK_STATE_NEW).edit().putString("userphoto", photo).commit();
                                Intent intent = new Intent(Posts.ACTION_USER_PHOTO_CHANGED);
                                intent.putExtra("photo", photo);
                                UploaderService.this.sendBroadcast(intent);
                                Log.m526e("vk", "SENT BROADCAST!!!");
                            } catch (Throwable x) {
                                Log.m532w("vk", x);
                            }
                        }
                    }).execSync();
                }
            }
        }
    }

    public void cancel(int id) {
        if (this.currentTask != null && this.currentTask.id == id) {
            this.needCancelCurrent = true;
            if (this.currentRequest != null) {
                this.currentRequest.abort();
            }
        }
        Iterator it = taskQueue.iterator();
        while (it.hasNext()) {
            UploadTask ut = (UploadTask) it.next();
            if (ut.id == id) {
                taskQueue.remove(ut);
                return;
            }
        }
    }

    private void uploadFailed(UploadTask ut) {
        Log.m526e("vk", "UploaderService: FAILED");
        if (ut.callback != null && ut.callback.uploadFailed()) {
            processTask(ut, false);
        }
        if (ut.type == TYPE_MESSAGE_PHOTO) {
            Messages.uploadFailed(ut.id);
        }
        if (ut.id != 0) {
            Intent intent = new Intent(ACTION_UPLOAD_FAILED);
            intent.putExtra("id", ut.id);
            sendBroadcast(intent);
        }
        if (ut.type != TYPE_MESSAGE_PHOTO && ut.type != 0) {
            failedTasks.add(ut);
            NotificationManager mNotificationManager = (NotificationManager) getSystemService("notification");
            mNotificationManager.cancel(ID_NOTIFICATION_PROGRESS);
            Notification notification1 = new Notification(ACRAConstants.DEFAULT_NOTIFICATION_ICON, getResources().getString(C0436R.string.uploading_photo_err), System.currentTimeMillis());
            Context context = getApplicationContext();
            Intent notificationIntent = new Intent(this, getClass());
            notificationIntent.setAction("RETRY");
            notification1.setLatestEventInfo(context, getResources().getString(C0436R.string.app_name), getResources().getString(C0436R.string.uploading_photo_err_notify), PendingIntent.getService(this, TASK_STATE_NEW, notificationIntent, TASK_STATE_NEW));
            mNotificationManager.notify(ID_NOTIFICATION_FAIL, notification1);
        }
    }

    public static boolean hasTaskWithId(int id) {
        if (currentInstance != null && currentInstance.currentTask != null && currentInstance.currentTask.id == id) {
            return true;
        }
        Iterator it = taskQueue.iterator();
        while (it.hasNext()) {
            if (((UploadTask) it.next()).id == id) {
                return true;
            }
        }
        return false;
    }

    private int getStringResForProgress(int type) {
        switch (type) {
            case TASK_STATE_NEW /*0*/:
            case TYPE_ALBUM_PHOTO /*1*/:
                return C0436R.string.uploading_photo;
            case TYPE_VIDEO /*2*/:
                return C0436R.string.uploading_video;
            case TYPE_AUDIO /*3*/:
                return C0436R.string.uploading_audio;
            default:
                return C0436R.string.uploading_document;
        }
    }

    private void showProgress(int type) {
        Intent intent = new Intent(this, getClass());
        intent.setAction("NOTHING");
        this.notificationTitle = Html.fromHtml("<b>" + getResources().getString(C0436R.string.app_name) + "</b> - " + getResources().getString(getStringResForProgress(type)));
        if (VERSION.SDK_INT < 14) {
            this.notification = new Notification(17301640, null, System.currentTimeMillis());
            Notification notification = this.notification;
            notification.flags |= 34;
            this.uploadView = new RemoteViews(getPackageName(), C0436R.layout.notify_with_progress);
            this.uploadView.setTextViewText(C0436R.id.notify_title, this.notificationTitle);
            this.uploadView.setTextViewText(C0436R.id.notify_num, new StringBuilder(String.valueOf(this.nDone + TYPE_ALBUM_PHOTO)).append("/").append((this.nDone + taskQueue.size()) + TYPE_ALBUM_PHOTO).toString());
            this.notification.setLatestEventInfo(getApplicationContext(), null, null, PendingIntent.getService(this, TASK_STATE_NEW, intent, TASK_STATE_NEW));
            this.notification.contentView = this.uploadView;
        } else {
            Builder contentTitle = new Builder(this).setSmallIcon(17301640).setContentTitle(this.notificationTitle);
            Object[] objArr = new Object[TYPE_VIDEO];
            objArr[TASK_STATE_NEW] = Integer.valueOf(this.nDone + TYPE_ALBUM_PHOTO);
            objArr[TYPE_ALBUM_PHOTO] = Integer.valueOf((this.nDone + taskQueue.size()) + TYPE_ALBUM_PHOTO);
            this.notifyBuilder = contentTitle.setContentText(getString(C0436R.string.player_num, objArr)).setProgress(10, TASK_STATE_NEW, true).setOngoing(true).setContentIntent(PendingIntent.getService(this, TASK_STATE_NEW, intent, TASK_STATE_NEW));
            this.notification = this.notifyBuilder.getNotification();
        }
        ((NotificationManager) getSystemService("notification")).notify(ID_NOTIFICATION_PROGRESS, this.notification);
    }

    private void showDone(UploadTask ut) {
        PendingIntent contentIntent;
        int rIdOkLong = C0436R.string.wall_ok_long;
        int rIdOkShort = C0436R.string.wall_ok;
        switch (ut.type) {
            case TYPE_ALBUM_PHOTO /*1*/:
                if (this.nDone <= TYPE_ALBUM_PHOTO) {
                    rIdOkLong = C0436R.string.photo_upload_ok_long;
                    rIdOkShort = C0436R.string.photo_upload_ok;
                    break;
                }
                rIdOkLong = C0436R.string.photos_upload_ok_long;
                rIdOkShort = C0436R.string.photos_upload_ok;
                break;
            case TYPE_VIDEO /*2*/:
                rIdOkLong = C0436R.string.video_upload_ok_long;
                rIdOkShort = C0436R.string.video_upload_ok;
                break;
            case TYPE_AUDIO /*3*/:
                rIdOkLong = C0436R.string.audio_upload_ok_long;
                rIdOkShort = C0436R.string.audio_upload_ok;
                break;
            case TYPE_DOCUMENT /*4*/:
                rIdOkLong = C0436R.string.doc_upload_ok_long;
                rIdOkShort = C0436R.string.doc_upload_ok;
                break;
            case TYPE_PROFILE_PHOTO /*6*/:
                rIdOkLong = C0436R.string.photo_upload_ok_long;
                rIdOkShort = C0436R.string.photo_upload_ok;
                break;
        }
        Intent notificationIntent;
        switch (ut.type) {
            case TASK_STATE_NEW /*0*/:
                Bundle args = new Bundle();
                args.putParcelable("entry", (Parcelable) ut.info);
                notificationIntent = new Intent(this, FragmentWrapperActivity.class);
                notificationIntent.putExtra("args", args);
                notificationIntent.putExtra("class", "PostViewFragment");
                contentIntent = PendingIntent.getActivity(this, TASK_STATE_NEW, notificationIntent, 134217728);
                break;
            default:
                notificationIntent = new Intent(this, getClass());
                notificationIntent.setAction("NOTHING");
                contentIntent = PendingIntent.getService(this, TASK_STATE_NEW, notificationIntent, 134217728);
                break;
        }
        NotificationManager mNotificationManager = (NotificationManager) getSystemService("notification");
        mNotificationManager.cancel(ID_NOTIFICATION_PROGRESS);
        Notification notification1 = new Notification(C0436R.drawable.ic_stat_notify_ok, getResources().getString(rIdOkShort), System.currentTimeMillis());
        notification1.setLatestEventInfo(getApplicationContext(), getResources().getString(C0436R.string.app_name), getResources().getString(rIdOkLong), contentIntent);
        notification1.flags |= 16;
        mNotificationManager.notify(ID_NOTIFICATION_DONE, notification1);
    }

    private void retryAll() {
        taskQueue.addAll(failedTasks);
        failedTasks.clear();
        startThread();
    }

    private String getFileName(Uri uri) {
        if (uri.getScheme().equals("content")) {
            String ctype = getContentResolver().getType(uri);
            if (ctype.startsWith("image/")) {
                return "image.jpg";
            }
            if (ctype.startsWith("audio/")) {
                return "audio.mp3";
            }
            if (ctype.startsWith("video/")) {
                return "video.mp4";
            }
        }
        return uri.getLastPathSegment();
    }

    public static int getNewID() {
        int i = lastID;
        lastID = i + TYPE_ALBUM_PHOTO;
        return i;
    }
}
