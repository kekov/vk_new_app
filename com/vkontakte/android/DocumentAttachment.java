package com.vkontakte.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.vkontakte.android.api.Document;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import com.vkontakte.android.ui.GifView;
import java.io.DataOutputStream;
import java.io.IOException;
import org.acra.ACRAConstants;

public class DocumentAttachment extends Attachment implements ImageAttachment {
    public static final Creator<DocumentAttachment> CREATOR;
    public int did;
    public GifView gifView;
    public int oid;
    public int size;
    public String thumb;
    public String title;
    public String url;

    /* renamed from: com.vkontakte.android.DocumentAttachment.1 */
    class C02381 implements Creator<DocumentAttachment> {
        C02381() {
        }

        public DocumentAttachment createFromParcel(Parcel in) {
            return new DocumentAttachment(in);
        }

        public DocumentAttachment[] newArray(int size) {
            return new DocumentAttachment[size];
        }
    }

    /* renamed from: com.vkontakte.android.DocumentAttachment.2 */
    class C02392 implements OnClickListener {
        private final /* synthetic */ Context val$context;

        C02392(Context context) {
            this.val$context = context;
        }

        public void onClick(View v) {
            if (DocumentAttachment.this.url != null && DocumentAttachment.this.url.length() > 0) {
                try {
                    this.val$context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(DocumentAttachment.this.url)));
                } catch (Exception e) {
                }
            }
        }
    }

    public DocumentAttachment(String _title, String _url, int _size, String _thumb, int _oid, int _did) {
        this.title = _title;
        this.url = _url;
        this.size = _size;
        this.thumb = _thumb;
        this.oid = _oid;
        this.did = _did;
    }

    public DocumentAttachment(Document doc) {
        this.title = doc.title;
        this.url = doc.url;
        this.size = doc.size;
        this.oid = doc.oid;
        this.did = doc.did;
        this.thumb = doc.thumb;
        if (!doc.title.toLowerCase().endsWith("." + doc.ext)) {
            this.title += "." + doc.ext;
        }
    }

    public DocumentAttachment(Parcel in) {
        this(in.readString(), in.readString(), in.readInt(), in.readString(), in.readInt(), in.readInt());
    }

    static {
        CREATOR = new C02381();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.url);
        dest.writeInt(this.size);
        dest.writeString(this.thumb);
        dest.writeInt(this.oid);
        dest.writeInt(this.did);
    }

    public View getFullView(Context context) {
        return getViewForList(context, null);
    }

    public View getViewForList(Context context, View reuse) {
        if (this.thumb == null || this.thumb.length() <= 0) {
            View v;
            if (reuse == null) {
                v = Attachment.getReusableView(context, "common");
            } else {
                v = reuse;
            }
            ((ImageView) v.findViewById(C0436R.id.attach_icon)).setImageResource(C0436R.drawable.ic_attach_document);
            ((TextView) v.findViewById(C0436R.id.attach_title)).setText(this.title);
            ((TextView) v.findViewById(C0436R.id.attach_subtitle)).setText(new StringBuilder(String.valueOf(context.getResources().getString(C0436R.string.doc))).append(" ").append(Global.langFileSize((long) this.size, context.getResources())).toString());
            if (this.url != null && this.url.length() > 0) {
                v.setOnClickListener(new C02392(context));
            }
            return v;
        }
        DocAttachView av = (DocAttachView) Attachment.getReusableView(context, "doc_thumb");
        av.setData(this.title, this.url, this.size, this.thumb);
        return av;
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(9);
        os.writeUTF(this.title);
        os.writeUTF(this.url == null ? ACRAConstants.DEFAULT_STRING_VALUE : this.url);
        os.writeInt(this.size);
        if (this.thumb != null) {
            os.writeUTF(this.thumb);
        } else {
            os.writeUTF(ACRAConstants.DEFAULT_STRING_VALUE);
        }
        os.writeInt(this.oid);
        os.writeInt(this.did);
    }

    public String toString() {
        return "doc" + this.oid + "_" + this.did;
    }

    public LayoutParams getViewLayoutParams() {
        LayoutParams lp = new LayoutParams();
        lp.width = -1;
        lp.height = Global.scale(54.0f);
        return lp;
    }

    public String getImageURL() {
        return this.thumb;
    }

    public void setImage(View view, Bitmap img, boolean fromCache) {
        if (this.thumb != null) {
            ((ImageView) view.findViewById(C0436R.id.att_doc_thumb)).setImageBitmap(img);
        }
    }

    public void clearImage(View view) {
        if (this.thumb != null && view.findViewById(C0436R.id.att_doc_thumb) != null) {
            ((ImageView) view.findViewById(C0436R.id.att_doc_thumb)).setImageBitmap(null);
        }
    }
}
