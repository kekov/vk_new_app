package com.vkontakte.android;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import java.io.DataOutputStream;
import java.io.IOException;

public class AudioAttachment extends Attachment {
    public static final Creator<AudioAttachment> CREATOR;
    public int aid;
    public String artist;
    public int duration;
    public int oid;
    public transient AudioFile[] playlist;
    public transient int playlistPos;
    public String title;
    public String url;

    /* renamed from: com.vkontakte.android.AudioAttachment.1 */
    class C01661 implements Creator<AudioAttachment> {
        C01661() {
        }

        public AudioAttachment createFromParcel(Parcel in) {
            return new AudioAttachment(in.readString(), in.readString(), in.readInt(), in.readInt(), in.readInt());
        }

        public AudioAttachment[] newArray(int size) {
            return new AudioAttachment[size];
        }
    }

    public AudioAttachment(String _artist, String _title, int _duration, int _oid, int _aid) {
        this.artist = _artist;
        this.title = _title;
        this.duration = _duration;
        this.oid = _oid;
        this.aid = _aid;
    }

    public AudioAttachment(AudioFile af) {
        this.artist = af.artist;
        this.title = af.title;
        this.duration = af.duration;
        this.oid = af.oid;
        this.aid = af.aid;
    }

    static {
        CREATOR = new C01661();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.artist);
        dest.writeString(this.title);
        dest.writeInt(this.duration);
        dest.writeInt(this.oid);
        dest.writeInt(this.aid);
    }

    public View getFullView(Context context) {
        return getViewForList(context, null);
    }

    public View getViewForList(Context context, View reuse) {
        AudioAttachView aav;
        if (reuse == null) {
            aav = Attachment.getReusableView(context, "audio");
        } else {
            View aav2 = reuse;
        }
        aav = aav;
        aav.setData(this.artist, this.title, this.oid, this.aid, this.duration);
        if (this.playlist != null) {
            aav.playlist = this.playlist;
            aav.playlistPos = this.playlistPos;
        }
        return aav;
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(3);
        os.writeUTF(this.artist);
        os.writeUTF(this.title);
        os.writeInt(this.duration);
        os.writeInt(this.oid);
        os.writeInt(this.aid);
    }

    public String toString() {
        return "audio" + this.oid + "_" + this.aid;
    }

    public LayoutParams getViewLayoutParams() {
        LayoutParams lp = new LayoutParams();
        lp.width = -1;
        lp.height = Global.scale(54.0f);
        return lp;
    }
}
