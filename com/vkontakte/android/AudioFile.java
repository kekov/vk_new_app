package com.vkontakte.android;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.plus.PlusShare;
import org.json.JSONObject;

public class AudioFile implements Parcelable {
    public static final Creator<AudioFile> CREATOR;
    public int aid;
    public String artist;
    public int duration;
    public String durationS;
    public int fileSize;
    public boolean fromAttachment;
    public int lyricsID;
    public int oid;
    public int oldAid;
    public int oldOid;
    public int playlistID;
    public boolean retried;
    public String title;
    public String url;

    /* renamed from: com.vkontakte.android.AudioFile.1 */
    class C01671 implements Creator<AudioFile> {
        C01671() {
        }

        public AudioFile createFromParcel(Parcel in) {
            return new AudioFile(null);
        }

        public AudioFile[] newArray(int size) {
            return new AudioFile[size];
        }
    }

    public AudioFile(int aid, int oid, String artist, String title, int duration, String url) {
        this.aid = aid;
        this.oid = oid;
        this.artist = artist;
        this.title = title;
        this.duration = duration;
        this.url = url;
        this.durationS = String.format("%d:%02d", new Object[]{Integer.valueOf(duration / 60), Integer.valueOf(duration % 60)});
    }

    public AudioFile(JSONObject o) {
        try {
            this.aid = o.optInt("id", o.optInt("aid"));
            this.oid = o.getInt("owner_id");
            this.artist = o.getString("artist");
            this.title = o.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            this.duration = o.getInt("duration");
            this.url = o.getString(PlusShare.KEY_CALL_TO_ACTION_URL);
            this.lyricsID = o.optInt("lyrics_id");
            this.playlistID = o.optInt("album_id");
        } catch (Exception x) {
            Log.m531w("vk", "Error parsing audio " + o, x);
        }
    }

    public AudioFile(AudioAttachment att) {
        this(att.aid, att.oid, att.artist, att.title, att.duration, null);
    }

    private AudioFile(Parcel p) {
        boolean z = true;
        try {
            this.artist = p.readString();
            this.title = p.readString();
            this.duration = p.readInt();
            this.durationS = p.readString();
            this.url = p.readString();
            this.aid = p.readInt();
            this.oid = p.readInt();
            this.lyricsID = p.readInt();
            if (p.readInt() != 1) {
                z = false;
            }
            this.fromAttachment = z;
        } catch (Exception e) {
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.artist);
        out.writeString(this.title);
        out.writeInt(this.duration);
        out.writeString(this.durationS);
        out.writeString(this.url);
        out.writeInt(this.aid);
        out.writeInt(this.oid);
        out.writeInt(this.lyricsID);
        out.writeInt(this.fromAttachment ? 1 : 0);
    }

    static {
        CREATOR = new C01671();
    }

    public boolean equals(AudioFile f) {
        if (f != null && f.oid == this.oid && f.aid == this.aid) {
            return true;
        }
        return false;
    }

    public boolean equalsAdded(AudioFile f) {
        if (f == null) {
            return false;
        }
        if ((f.oid == this.oldOid && f.aid == this.oldAid) || (f.oldOid == this.oid && f.oldAid == this.aid)) {
            return true;
        }
        return false;
    }

    public String toString() {
        return "AudioFile {" + this.oid + "_" + this.aid + ", " + this.duration + ", " + this.artist + ", " + this.title + "}";
    }
}
