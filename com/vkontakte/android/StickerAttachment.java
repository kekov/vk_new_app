package com.vkontakte.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v4.media.TransportMediator;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import java.io.DataOutputStream;
import java.io.IOException;

public class StickerAttachment extends Attachment implements ImageAttachment {
    public static final Creator<StickerAttachment> CREATOR;
    public int height;
    public int id;
    public String[] images;
    public int width;

    /* renamed from: com.vkontakte.android.StickerAttachment.1 */
    class C05001 implements Creator<StickerAttachment> {
        C05001() {
        }

        public StickerAttachment createFromParcel(Parcel in) {
            return new StickerAttachment(in);
        }

        public StickerAttachment[] newArray(int size) {
            return new StickerAttachment[size];
        }
    }

    /* renamed from: com.vkontakte.android.StickerAttachment.2 */
    class C05012 extends ImageView {
        C05012(Context $anonymous0) {
            super($anonymous0);
        }

        public void onMeasure(int wms, int hms) {
            setMeasuredDimension(Global.scale((float) StickerAttachment.this.width) / 2, (Global.scale((float) StickerAttachment.this.height) / 2) + Global.scale(8.0f));
        }
    }

    public StickerAttachment(int _id, String[] _images, int _width, int _height) {
        this.id = _id;
        this.images = _images;
        this.width = _width;
        this.height = _height;
    }

    public StickerAttachment(Parcel in) {
        this.id = in.readInt();
        this.images = in.createStringArray();
        this.width = in.readInt();
        this.height = in.readInt();
    }

    static {
        CREATOR = new C05001();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel p, int arg1) {
        p.writeInt(this.id);
        p.writeStringArray(this.images);
        p.writeInt(this.width);
        p.writeInt(this.height);
    }

    public View getFullView(Context context) {
        ImageView iv = new C05012(context);
        iv.setScaleType(ScaleType.FIT_START);
        return iv;
    }

    public LayoutParams getViewLayoutParams() {
        LayoutParams lp = new LayoutParams();
        lp.width = Global.scale((float) this.width) / 2;
        lp.height = (Global.scale((float) this.height) / 2) + Global.scale(8.0f);
        return lp;
    }

    public View getViewForList(Context context, View reuse) {
        return getFullView(context);
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(16);
        os.writeInt(this.id);
        os.writeInt(this.images.length);
        for (String s : this.images) {
            os.writeUTF(s);
        }
        os.writeInt(this.width);
        os.writeInt(this.height);
    }

    public String getImageURL() {
        return this.images[Global.displayDensity > 1.0f ? 2 : 1];
    }

    public String getKeyboardImageURL(int width) {
        int size = 0;
        if (width > 64) {
            size = 1;
        }
        if (width > TransportMediator.FLAG_KEY_MEDIA_NEXT) {
            size = 2;
        }
        String url = this.images[size];
        if (url.startsWith("A")) {
            return url.split("\\|")[1];
        }
        return url;
    }

    public void setImage(View view, Bitmap img, boolean fromCache) {
        ((ImageView) view).setImageBitmap(img);
    }

    public void clearImage(View view) {
        ((ImageView) view).setImageResource(C0436R.drawable.sticker_placeholder);
    }
}
