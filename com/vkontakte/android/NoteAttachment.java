package com.vkontakte.android;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.ui.FlowLayout.LayoutParams;
import java.io.DataOutputStream;
import java.io.IOException;

public class NoteAttachment extends Attachment {
    public static final Creator<NoteAttachment> CREATOR;
    int nid;
    int oid;
    String title;

    /* renamed from: com.vkontakte.android.NoteAttachment.1 */
    class C03641 implements Creator<NoteAttachment> {
        C03641() {
        }

        public NoteAttachment createFromParcel(Parcel in) {
            return new NoteAttachment(in.readString(), in.readInt(), in.readInt());
        }

        public NoteAttachment[] newArray(int size) {
            return new NoteAttachment[size];
        }
    }

    /* renamed from: com.vkontakte.android.NoteAttachment.2 */
    class C03652 implements OnClickListener {
        private final /* synthetic */ Context val$context;

        C03652(Context context) {
            this.val$context = context;
        }

        public void onClick(View v) {
            Intent intent = new Intent(this.val$context, WikiViewActivity.class);
            intent.putExtra("oid", NoteAttachment.this.oid);
            intent.putExtra("nid", NoteAttachment.this.nid);
            intent.putExtra(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, NoteAttachment.this.title);
            this.val$context.startActivity(intent);
        }
    }

    public NoteAttachment(String _title, int _oid, int _nid) {
        this.title = _title;
        this.oid = _oid;
        this.nid = _nid;
    }

    static {
        CREATOR = new C03641();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeInt(this.oid);
        dest.writeInt(this.nid);
    }

    public View getFullView(Context context) {
        return getViewForList(context, null);
    }

    public View getViewForList(Context context, View reuse) {
        View v;
        if (reuse == null) {
            v = Attachment.getReusableView(context, "common");
        } else {
            v = reuse;
        }
        ((ImageView) v.findViewById(C0436R.id.attach_icon)).setImageResource(C0436R.drawable.ic_attach_note);
        ((TextView) v.findViewById(C0436R.id.attach_title)).setText(this.title);
        ((TextView) v.findViewById(C0436R.id.attach_subtitle)).setText(C0436R.string.attach_note);
        v.setOnClickListener(new C03652(context));
        return v;
    }

    public LayoutParams getViewLayoutParams() {
        LayoutParams lp = new LayoutParams();
        lp.width = -1;
        lp.height = Global.scale(54.0f);
        return lp;
    }

    public void serialize(DataOutputStream os) throws IOException {
        os.writeInt(7);
        os.writeUTF(this.title);
        os.writeInt(this.oid);
        os.writeInt(this.nid);
    }
}
