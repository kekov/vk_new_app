package com.vkontakte.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.text.ClipboardManager;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.MultiThumbPhotoAlbum;
import com.vkontakte.android.api.PhotoAlbum;
import com.vkontakte.android.api.PhotosGetAlbums;
import com.vkontakte.android.api.PhotosGetAlbums.Callback;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.ui.EmptyView;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.PhotoView;
import java.util.ArrayList;
import java.util.Iterator;

public class PhotoAlbumsView extends FrameLayout implements OnItemClickListener {
    private PhotoAlbumsAdapter adapter;
    private ArrayList<PhotoAlbum> albums;
    public AlbumActionsCallback callback;
    private APIRequest currentReq;
    private ErrorView error;
    private ListImageLoaderWrapper imgLoader;
    private int itemHeight;
    private StickyGridHeadersGridView list;
    private boolean loaded;
    private OnClickListener menuClickListener;
    private boolean needSystem;
    private EmptyView noAlbumsView;
    private int numColumns;
    private ProgressBar progress;
    private BroadcastReceiver receiver;
    private ArrayList<PhotoAlbum> system;
    private int uid;
    private String userName;
    private FrameLayout wrap;

    /* renamed from: com.vkontakte.android.PhotoAlbumsView.1 */
    class C03811 extends BroadcastReceiver {
        C03811() {
        }

        public void onReceive(Context arg0, Intent intent) {
            int aid;
            Iterator it;
            PhotoAlbum album;
            if (UploaderService.ACTION_PHOTO_ADDED.equals(intent.getAction())) {
                aid = intent.getIntExtra("aid", 0);
                it = PhotoAlbumsView.this.albums.iterator();
                while (it.hasNext()) {
                    album = (PhotoAlbum) it.next();
                    if (aid == album.id) {
                        album.numPhotos++;
                        PhotoAlbumsView.this.updateList();
                    }
                }
            }
            if (UploaderService.ACTION_PHOTO_REMOVED.equals(intent.getAction())) {
                aid = intent.getIntExtra("aid", 0);
                it = PhotoAlbumsView.this.albums.iterator();
                while (it.hasNext()) {
                    album = (PhotoAlbum) it.next();
                    if (aid == album.id) {
                        album.numPhotos--;
                        PhotoAlbumsView.this.updateList();
                    }
                }
            }
        }
    }

    /* renamed from: com.vkontakte.android.PhotoAlbumsView.2 */
    class C03842 implements OnClickListener {

        /* renamed from: com.vkontakte.android.PhotoAlbumsView.2.1 */
        class C03821 implements DialogInterface.OnClickListener {
            private final /* synthetic */ int val$pos;

            C03821(int i) {
                this.val$pos = i;
            }

            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                        PhotoAlbumsView.this.editAlbum(this.val$pos);
                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                        PhotoAlbumsView.this.deleteAlbum(this.val$pos);
                    case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                        PhotoAlbumsView.this.copyLink(this.val$pos);
                    default:
                }
            }
        }

        /* renamed from: com.vkontakte.android.PhotoAlbumsView.2.2 */
        class C03832 implements OnMenuItemClickListener {
            private final /* synthetic */ int val$pos;

            C03832(int i) {
                this.val$pos = i;
            }

            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                        PhotoAlbumsView.this.editAlbum(this.val$pos);
                        break;
                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                        PhotoAlbumsView.this.deleteAlbum(this.val$pos);
                        break;
                    case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                        PhotoAlbumsView.this.copyLink(this.val$pos);
                        break;
                }
                return true;
            }
        }

        C03842() {
        }

        public void onClick(View v) {
            int pos = ((Integer) v.getTag()).intValue();
            if (((PhotoAlbum) PhotoAlbumsView.this.albums.get(pos)).id >= 0) {
                if (VERSION.SDK_INT < 14) {
                    new Builder(PhotoAlbumsView.this.getContext()).setItems(new String[]{PhotoAlbumsView.this.getResources().getString(C0436R.string.edit), PhotoAlbumsView.this.getResources().getString(C0436R.string.delete), PhotoAlbumsView.this.getResources().getString(C0436R.string.copy_link)}, new C03821(pos)).show();
                    return;
                }
                PopupMenu pm = new PopupMenu(PhotoAlbumsView.this.getContext(), v);
                pm.getMenu().add(0, 0, 0, PhotoAlbumsView.this.getResources().getString(C0436R.string.edit));
                pm.getMenu().add(0, 1, 0, PhotoAlbumsView.this.getResources().getString(C0436R.string.delete));
                pm.getMenu().add(0, 2, 0, PhotoAlbumsView.this.getResources().getString(C0436R.string.copy_link));
                pm.setOnMenuItemClickListener(new C03832(pos));
                pm.show();
            }
        }
    }

    /* renamed from: com.vkontakte.android.PhotoAlbumsView.3 */
    class C03853 implements OnClickListener {
        C03853() {
        }

        public void onClick(View v) {
            if (PhotoAlbumsView.this.callback != null) {
                PhotoAlbumsView.this.callback.editAlbum(null);
            }
        }
    }

    /* renamed from: com.vkontakte.android.PhotoAlbumsView.4 */
    class C03864 implements OnClickListener {
        C03864() {
        }

        public void onClick(View v) {
            PhotoAlbumsView.this.error.setVisibility(8);
            PhotoAlbumsView.this.progress.setVisibility(0);
            PhotoAlbumsView.this.loadData();
        }
    }

    /* renamed from: com.vkontakte.android.PhotoAlbumsView.5 */
    class C03875 implements Runnable {
        private final /* synthetic */ int val$w;

        C03875(int i) {
            this.val$w = i;
        }

        public void run() {
            PhotoAlbumsView.this.updateSizes(this.val$w);
            PhotoAlbumsView.this.updateList();
        }
    }

    /* renamed from: com.vkontakte.android.PhotoAlbumsView.7 */
    class C03897 implements Runnable {

        /* renamed from: com.vkontakte.android.PhotoAlbumsView.7.1 */
        class C03881 implements Runnable {
            C03881() {
            }

            public void run() {
                PhotoAlbumsView.this.imgLoader.updateImages();
            }
        }

        C03897() {
        }

        public void run() {
            PhotoAlbumsView.this.adapter.notifyDataSetChanged();
            PhotoAlbumsView.this.postDelayed(new C03881(), 200);
        }
    }

    /* renamed from: com.vkontakte.android.PhotoAlbumsView.8 */
    class C03908 implements Runnable {
        C03908() {
        }

        public void run() {
            PhotoAlbumsView.this.adapter.notifyDataSetInvalidated();
        }
    }

    public interface AlbumActionsCallback {
        void deleteAlbum(int i);

        void editAlbum(PhotoAlbum photoAlbum);

        void openAlbum(PhotoAlbum photoAlbum);
    }

    /* renamed from: com.vkontakte.android.PhotoAlbumsView.6 */
    class C13346 implements Callback {
        C13346() {
        }

        public void success(ArrayList<PhotoAlbum> _albums, ArrayList<PhotoAlbum> _system) {
            PhotoAlbumsView.this.albums.addAll(_albums);
            PhotoAlbumsView.this.system.addAll(_system);
            PhotoAlbumsView.this.error.setVisibility(8);
            Global.showViewAnimated(PhotoAlbumsView.this.wrap, true, PhotoView.THUMB_ANIM_DURATION);
            Global.showViewAnimated(PhotoAlbumsView.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
            PhotoAlbumsView.this.updateList();
            PhotoAlbumsView.this.currentReq = null;
        }

        public void fail(int ecode, String emsg) {
            PhotoAlbumsView.this.loaded = false;
            PhotoAlbumsView.this.error.setErrorInfo(ecode, emsg);
            Global.showViewAnimated(PhotoAlbumsView.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
            Global.showViewAnimated(PhotoAlbumsView.this.error, true, PhotoView.THUMB_ANIM_DURATION);
            PhotoAlbumsView.this.currentReq = null;
        }
    }

    private class PhotoAlbumsAdapter extends BaseAdapter implements StickyGridHeadersBaseAdapter {
        private PhotoAlbumsAdapter() {
        }

        public int getCount() {
            return PhotoAlbumsView.this.albums.size() + PhotoAlbumsView.this.system.size();
        }

        public Object getItem(int arg0) {
            return null;
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public int getViewTypeCount() {
            return 2;
        }

        public int getItemViewType(int pos) {
            PhotoAlbum a;
            if (pos < PhotoAlbumsView.this.system.size()) {
                a = (PhotoAlbum) PhotoAlbumsView.this.system.get(pos);
            } else {
                a = (PhotoAlbum) PhotoAlbumsView.this.albums.get(pos - PhotoAlbumsView.this.system.size());
            }
            if (a instanceof MultiThumbPhotoAlbum) {
                return 1;
            }
            return 0;
        }

        public View getView(int pos, View view, ViewGroup group) {
            PhotoAlbum a;
            int realPos = pos;
            if (pos < PhotoAlbumsView.this.system.size()) {
                a = (PhotoAlbum) PhotoAlbumsView.this.system.get(pos);
            } else {
                pos -= PhotoAlbumsView.this.system.size();
                a = (PhotoAlbum) PhotoAlbumsView.this.albums.get(pos);
            }
            if (view == null) {
                view = PhotoAlbumsView.inflate(PhotoAlbumsView.this.getContext(), a instanceof MultiThumbPhotoAlbum ? C0436R.layout.photoalbums_item_multithumb : C0436R.layout.photoalbums_item, null);
                view.findViewById(C0436R.id.album_actions).setOnClickListener(PhotoAlbumsView.this.menuClickListener);
            }
            if (!(a instanceof MultiThumbPhotoAlbum)) {
                view.setLayoutParams(new LayoutParams(-1, PhotoAlbumsView.this.itemHeight));
            }
            view.setTag(Integer.valueOf(realPos));
            ((TextView) view.findViewById(C0436R.id.album_title)).setText(a.title);
            ((TextView) view.findViewById(C0436R.id.album_qty)).setText(a.numPhotos > 0 ? Global.langPlural(C0436R.array.album_numphotos, a.numPhotos, PhotoAlbumsView.this.getResources()) : PhotoAlbumsView.this.getResources().getString(C0436R.string.no_photos));
            View findViewById = view.findViewById(C0436R.id.album_actions);
            int i = ((PhotoAlbumsView.this.uid == Global.uid || PhotoAlbumsView.this.uid == 0) && a.id > 0) ? 0 : 8;
            findViewById.setVisibility(i);
            view.findViewById(C0436R.id.album_actions).setTag(Integer.valueOf(pos));
            if (a instanceof MultiThumbPhotoAlbum) {
                MultiThumbPhotoAlbum mt = (MultiThumbPhotoAlbum) a;
                for (int i2 = 0; i2 < mt.thumbs.size(); i2++) {
                    if (PhotoAlbumsView.this.imgLoader.isAlreadyLoaded((String) mt.thumbs.get(i2))) {
                        ((ImageView) view.findViewById(PhotoAlbumsView.this.getMultiThumbViewId(i2))).setImageBitmap(PhotoAlbumsView.this.imgLoader.get((String) mt.thumbs.get(i2)));
                    } else {
                        ((ImageView) view.findViewById(PhotoAlbumsView.this.getMultiThumbViewId(i2))).setImageDrawable(new ColorDrawable(Color.LIGHT_GRAY));
                    }
                }
            } else if (a.thumbURL == null || a.thumbURL.length() <= 0 || a.thumbURL.endsWith("gif") || !PhotoAlbumsView.this.imgLoader.isAlreadyLoaded(a.thumbURL)) {
                ((ImageView) view.findViewById(C0436R.id.album_thumb)).setImageDrawable(new ColorDrawable(Color.LIGHT_GRAY));
            } else {
                ((ImageView) view.findViewById(C0436R.id.album_thumb)).setImageBitmap(PhotoAlbumsView.this.imgLoader.get(a.thumbURL));
            }
            return view;
        }

        public int getCountForHeader(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return PhotoAlbumsView.this.system.size();
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return PhotoAlbumsView.this.albums.size();
                default:
                    return 0;
            }
        }

        public View getHeaderView(int pos, View view, ViewGroup arg2) {
            if (pos != 0) {
                if (!(view instanceof TextView)) {
                    view = null;
                }
                View v = (TextView) view;
                if (v == null) {
                    v = new TextView(PhotoAlbumsView.this.getContext());
                    v.setTextColor(-8026747);
                    v.setTypeface(Typeface.DEFAULT_BOLD);
                    v.setPadding(Global.scale(GalleryPickerFooterView.BADGE_SIZE), Global.scale(10.0f), 0, 0);
                    v.setText(Global.langPlural(C0436R.array.albums, PhotoAlbumsView.this.albums.size(), PhotoAlbumsView.this.getResources()).toUpperCase());
                    v.setTextSize(1, 14.0f);
                }
                return v;
            } else if (view == null) {
                return new View(PhotoAlbumsView.this.getContext());
            } else {
                return view;
            }
        }

        public int getNumHeaders() {
            if (PhotoAlbumsView.this.albums.size() <= 0 && PhotoAlbumsView.this.system.size() <= 0) {
                return 0;
            }
            if (PhotoAlbumsView.this.albums.size() == 0) {
                return 1;
            }
            return 2;
        }
    }

    private class AlbumCoversAdapter extends MultiSectionImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.PhotoAlbumsView.AlbumCoversAdapter.1 */
        class C03911 implements Runnable {
            private final /* synthetic */ PhotoAlbum val$a;
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$image;
            private final /* synthetic */ int val$item;

            C03911(int i, PhotoAlbum photoAlbum, int i2, Bitmap bitmap) {
                this.val$item = i;
                this.val$a = photoAlbum;
                this.val$image = i2;
                this.val$bitmap = bitmap;
            }

            public void run() {
                int pos = -1;
                int i = 0;
                while (i < PhotoAlbumsView.this.list.getChildCount()) {
                    if ((PhotoAlbumsView.this.list.getChildAt(i).getTag() instanceof Integer) && ((Integer) PhotoAlbumsView.this.list.getChildAt(i).getTag()).intValue() == this.val$item) {
                        pos = i;
                    }
                    i++;
                }
                if (pos != -1) {
                    View it = PhotoAlbumsView.this.list.getChildAt(pos);
                    if (this.val$a instanceof MultiThumbPhotoAlbum) {
                        int id = PhotoAlbumsView.this.getMultiThumbViewId(this.val$image);
                        if (id != 0 && it != null && it.findViewById(id) != null) {
                            ((ImageView) it.findViewById(id)).setImageBitmap(this.val$bitmap);
                        }
                    } else if (it != null && it.findViewById(C0436R.id.album_thumb) != null) {
                        ((ImageView) it.findViewById(C0436R.id.album_thumb)).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private AlbumCoversAdapter() {
        }

        public int getItemCount(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return PhotoAlbumsView.this.system.size();
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return PhotoAlbumsView.this.albums.size();
                default:
                    return 0;
            }
        }

        public int getImageCountForItem(int section, int item) {
            PhotoAlbum a;
            if (section == 0) {
                a = (PhotoAlbum) PhotoAlbumsView.this.system.get(item);
            } else {
                a = (PhotoAlbum) PhotoAlbumsView.this.albums.get(item);
            }
            if (a instanceof MultiThumbPhotoAlbum) {
                return ((MultiThumbPhotoAlbum) a).thumbs.size();
            }
            return (a.thumbURL == null || a.thumbURL.length() <= 0 || a.thumbURL.endsWith("gif")) ? 0 : 1;
        }

        public String getImageURL(int section, int item, int image) {
            PhotoAlbum a;
            if (section == 0) {
                a = (PhotoAlbum) PhotoAlbumsView.this.system.get(item);
            } else {
                a = (PhotoAlbum) PhotoAlbumsView.this.albums.get(item);
            }
            if (a instanceof MultiThumbPhotoAlbum) {
                return (String) ((MultiThumbPhotoAlbum) a).thumbs.get(image);
            }
            return a.thumbURL;
        }

        public void imageLoaded(int _item, int image, Bitmap bitmap) {
            PhotoAlbum a;
            int item = _item;
            if (item < PhotoAlbumsView.this.system.size()) {
                a = (PhotoAlbum) PhotoAlbumsView.this.system.get(item);
            } else {
                a = (PhotoAlbum) PhotoAlbumsView.this.albums.get(item - PhotoAlbumsView.this.system.size());
            }
            PhotoAlbumsView.this.post(new C03911(item, a, image, bitmap));
        }

        public int getSectionCount() {
            return 2;
        }

        public boolean isSectionHeaderVisible(int section) {
            return false;
        }
    }

    public PhotoAlbumsView(Context context, int uid, String userName, boolean needSystem) {
        super(context);
        this.albums = new ArrayList();
        this.system = new ArrayList();
        this.loaded = false;
        this.itemHeight = 0;
        this.needSystem = true;
        this.numColumns = 1;
        this.receiver = new C03811();
        this.menuClickListener = new C03842();
        this.uid = uid;
        this.userName = userName;
        this.needSystem = needSystem;
        init();
    }

    public PhotoAlbumsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.albums = new ArrayList();
        this.system = new ArrayList();
        this.loaded = false;
        this.itemHeight = 0;
        this.needSystem = true;
        this.numColumns = 1;
        this.receiver = new C03811();
        this.menuClickListener = new C03842();
        init();
    }

    public PhotoAlbumsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.albums = new ArrayList();
        this.system = new ArrayList();
        this.loaded = false;
        this.itemHeight = 0;
        this.needSystem = true;
        this.numColumns = 1;
        this.receiver = new C03811();
        this.menuClickListener = new C03842();
        init();
    }

    private void init() {
        boolean isMe = false;
        this.wrap = new FrameLayout(getContext());
        setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        this.list = new StickyGridHeadersGridView(getContext());
        StickyGridHeadersGridView stickyGridHeadersGridView = this.list;
        ListAdapter photoAlbumsAdapter = new PhotoAlbumsAdapter();
        this.adapter = photoAlbumsAdapter;
        stickyGridHeadersGridView.setAdapter(photoAlbumsAdapter);
        if (VERSION.SDK_INT < 14) {
            this.list.setCacheColorHint(getResources().getColor(C0436R.color.cards_bg));
            this.list.setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        }
        this.list.setVerticalFadingEdgeEnabled(false);
        this.list.setOnItemClickListener(this);
        this.list.setVerticalSpacing(Global.scale(4.0f));
        this.list.setSelector(C0436R.drawable.highlight_post);
        this.list.setDrawSelectorOnTop(true);
        this.list.setPadding(Global.scale(4.0f), 0, Global.scale(4.0f), Global.scale(10.0f));
        this.list.setClipToPadding(false);
        this.list.setScrollBarStyle(33554432);
        this.list.setAreHeadersSticky(false);
        this.wrap.addView(this.list);
        this.imgLoader = new ListImageLoaderWrapper(new AlbumCoversAdapter(), this.list, null);
        this.progress = new ProgressBar(getContext());
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(Global.scale(40.0f), Global.scale(40.0f));
        lp.gravity = 17;
        this.progress.setLayoutParams(lp);
        addView(this.progress);
        this.noAlbumsView = EmptyView.create(getContext());
        if (Global.uid == this.uid || this.uid == 0) {
            isMe = true;
        }
        this.noAlbumsView.setText(isMe ? C0436R.string.no_albums_me : C0436R.string.no_albums);
        this.noAlbumsView.setButtonText((int) C0436R.string.create_album);
        this.noAlbumsView.setButtonVisible(isMe);
        this.noAlbumsView.setOnBtnClickListener(new C03853());
        this.list.setEmptyView(this.noAlbumsView);
        this.wrap.addView(this.noAlbumsView);
        this.wrap.setVisibility(8);
        addView(this.wrap);
        this.error = (ErrorView) inflate(getContext(), C0436R.layout.error, null);
        this.error.setOnRetryListener(new C03864());
        this.error.setVisibility(8);
        addView(this.error);
    }

    public void onActivate() {
        if (!this.loaded) {
            loadData();
        }
    }

    public void onDeactivate() {
    }

    public void onAttachedToWindow() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UploaderService.ACTION_PHOTO_ADDED);
        filter.addAction(UploaderService.ACTION_PHOTO_REMOVED);
        VKApplication.context.registerReceiver(this.receiver, filter, permission.ACCESS_DATA, null);
    }

    public void onDetachedFromWindow() {
        if (this.currentReq != null) {
            this.currentReq.cancel();
        }
        try {
            VKApplication.context.unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
    }

    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        post(new C03875(w));
    }

    private void updateSizes(int sw) {
        int colCount = Math.round(((float) sw) / ((float) Global.scale(350.0f)));
        this.list.setNumColumns(colCount);
        this.itemHeight = Math.round(((float) ((sw / colCount) - Global.scale(4.0f))) / 1.5f);
        this.numColumns = colCount;
    }

    private void loadData() {
        this.loaded = true;
        this.currentReq = new PhotosGetAlbums(this.uid, this.needSystem).setCallback(new C13346()).exec((View) this);
    }

    private void updateList() {
        post(new C03897());
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        PhotoAlbum a;
        if (pos < this.system.size()) {
            a = (PhotoAlbum) this.system.get(pos);
        } else {
            pos -= this.system.size();
            if (pos >= 0) {
                a = (PhotoAlbum) this.albums.get(pos);
            } else {
                return;
            }
        }
        if (this.callback != null) {
            this.callback.openAlbum(a);
        }
    }

    public void addOrReplace(PhotoAlbum album) {
        for (int i = 0; i < this.albums.size(); i++) {
            if (((PhotoAlbum) this.albums.get(i)).id == album.id) {
                this.albums.set(i, album);
                updateList();
                return;
            }
        }
        this.albums.add(album);
        updateList();
    }

    private void editAlbum(int pos) {
        if (this.callback != null) {
            this.callback.editAlbum((PhotoAlbum) this.albums.get(pos));
        }
    }

    private void deleteAlbum(int pos) {
        if (this.callback != null) {
            this.callback.deleteAlbum(((PhotoAlbum) this.albums.get(pos)).id);
        }
    }

    private void copyLink(int pos) {
        PhotoAlbum a = (PhotoAlbum) this.albums.get(pos);
        ((ClipboardManager) getContext().getSystemService("clipboard")).setText("http://vk.com/album" + a.oid + "_" + a.id);
        Toast.makeText(getContext(), C0436R.string.link_copied, 0).show();
    }

    public void remove(int id) {
        for (int i = 0; i < this.albums.size(); i++) {
            if (((PhotoAlbum) this.albums.get(i)).id == id) {
                this.albums.remove(i);
                updateList();
                return;
            }
        }
    }

    public void invalidateList() {
        postDelayed(new C03908(), 10);
    }

    private int getMultiThumbViewId(int index) {
        switch (index) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return C0436R.id.album_thumb1;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                return C0436R.id.album_thumb2;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return C0436R.id.album_thumb3;
            default:
                return -1;
        }
    }

    public void onPause() {
        this.imgLoader.deactivate();
    }

    public void onResume() {
        this.imgLoader.activate();
    }
}
