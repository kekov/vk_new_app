package com.vkontakte.android;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TimePicker;
import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.c2dm.C2DMessaging;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.Date;
import org.acra.ACRAConstants;

public class SettingsActivity extends SherlockPreferenceActivity {
    public static final int REQUEST_SYNC_SETTINGS = 203;
    public static final int RESULT_LOGGED_OUT = 2;
    private Preference cancelDndPref;
    private Preference dnd1Pref;
    private Preference dnd8Pref;
    private SharedPreferences prefs;

    /* renamed from: com.vkontakte.android.SettingsActivity.1 */
    class C04751 implements OnPreferenceClickListener {
        C04751() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsActivity.this.confirmLogout();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.SettingsActivity.2 */
    class C04762 implements OnPreferenceClickListener {
        C04762() {
        }

        public boolean onPreferenceClick(Preference preference) {
            Intent intent = new Intent(SettingsActivity.this, WelcomeActivity.class);
            intent.setAction("syncsettings");
            SettingsActivity.this.startActivityForResult(intent, SettingsActivity.REQUEST_SYNC_SETTINGS);
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.SettingsActivity.3 */
    class C04773 implements OnPreferenceClickListener {
        C04773() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsActivity.this.startActivity(new Intent(SettingsActivity.this, NewsfeedBanlistActivity.class));
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.SettingsActivity.4 */
    class C04784 implements OnPreferenceClickListener {
        C04784() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsActivity.this.startActivity(new Intent(SettingsActivity.this, ChangePasswordActivity.class));
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.SettingsActivity.5 */
    class C04795 implements OnPreferenceClickListener {
        C04795() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsActivity.this.startActivity(new Intent(SettingsActivity.this, BlacklistActivity.class));
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.SettingsActivity.6 */
    class C04806 implements OnPreferenceClickListener {
        C04806() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsActivity.this.getSharedPreferences(null, 0).edit().remove("c2dm_regID").commit();
            C2DMessaging.unregister(SettingsActivity.this.getApplicationContext());
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.SettingsActivity.7 */
    class C04817 implements OnPreferenceClickListener {
        C04817() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsActivity.this.stopService(new Intent(SettingsActivity.this, LongPollService.class));
            C2DM.start();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.SettingsActivity.8 */
    class C04828 implements OnPreferenceClickListener {
        C04828() {
        }

        public boolean onPreferenceClick(Preference preference) {
            SettingsActivity.this.startActivity(new Intent(SettingsActivity.this, SettingsAdvancedActivity.class));
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.SettingsActivity.9 */
    class C04839 implements OnPreferenceClickListener {
        C04839() {
        }

        public boolean onPreferenceClick(Preference preference) {
            MainActivity.showAbout(SettingsActivity.this);
            return true;
        }
    }

    private class AdapterWrapper extends BaseAdapter {
        ListAdapter f46a;

        public AdapterWrapper(ListAdapter aa) {
            this.f46a = aa;
        }

        public int getCount() {
            return this.f46a.getCount();
        }

        public Object getItem(int position) {
            return this.f46a.getItem(position);
        }

        public long getItemId(int position) {
            return this.f46a.getItemId(position);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            return this.f46a.getView(position, convertView, parent);
        }

        public boolean isEnabled(int pos) {
            return this.f46a.isEnabled(pos);
        }

        public int getViewTypeCount() {
            return this.f46a.getViewTypeCount();
        }

        public int getItemViewType(int pos) {
            return this.f46a.getItemViewType(pos);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        boolean z;
        this.prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addPreferencesFromResource(C0436R.xml.preferences);
        Preference pref = findPreference("logOut");
        pref.setSummary(VKApplication.context.getSharedPreferences(null, 0).getString("username", ACRAConstants.DEFAULT_STRING_VALUE));
        pref.setOnPreferenceClickListener(new C04751());
        pref = findPreference("sync");
        updateSyncLabel(Auth.getCurrentSyncOption(this));
        pref.setOnPreferenceClickListener(new C04762());
        findPreference("newsBanned").setOnPreferenceClickListener(new C04773());
        findPreference("changePassword").setOnPreferenceClickListener(new C04784());
        findPreference("blacklist").setOnPreferenceClickListener(new C04795());
        pref = findPreference("stopc2dm");
        if (pref != null) {
            pref.setOnPreferenceClickListener(new C04806());
        }
        pref = findPreference("startc2dm");
        if (pref != null) {
            pref.setOnPreferenceClickListener(new C04817());
        }
        findPreference("advanced").setOnPreferenceClickListener(new C04828());
        findPreference("about").setOnPreferenceClickListener(new C04839());
        pref = findPreference("useHTTPS");
        if (getSharedPreferences(null, 0).getBoolean("forceHTTPS", false)) {
            z = false;
        } else {
            z = true;
        }
        pref.setEnabled(z);
        if (getSharedPreferences(null, 0).getBoolean("forceHTTPS", false)) {
            pref.setSummary(C0436R.string.https_only_summary);
        }
        this.cancelDndPref = findPreference("dnd_cancel");
        pref = findPreference("dnd_hour");
        this.dnd1Pref = pref;
        pref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SettingsActivity.this.showDndDialog();
                return true;
            }
        });
        this.cancelDndPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SettingsActivity.this.prefs.edit().putLong("dnd_end", 0).commit();
                PreferenceCategory cat = (PreferenceCategory) SettingsActivity.this.findPreference("cat_notify");
                cat.removePreference(SettingsActivity.this.cancelDndPref);
                cat.addPreference(SettingsActivity.this.dnd1Pref);
                return true;
            }
        });
        if (this.prefs.getLong("dnd_end", 0) > System.currentTimeMillis()) {
            this.cancelDndPref.setSummary(getResources().getString(C0436R.string.sett_dnd_desc, new Object[]{Global.langDateShort((int) (t / 1000))}));
            ((PreferenceCategory) findPreference("cat_notify")).removePreference(this.dnd1Pref);
        } else {
            ((PreferenceCategory) findPreference("cat_notify")).removePreference(this.cancelDndPref);
        }
        pref = findPreference("notifyRingtone");
        if (pref != null) {
            pref.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    SettingsActivity.this.updateRingtoneName((String) newValue);
                    return true;
                }
            });
        }
        getListView().setVerticalFadingEdgeEnabled(false);
        updateRingtoneName(null);
    }

    private void showDndDialog() {
        new Builder(this).setItems(C0436R.array.sett_dnd_options, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                long t = System.currentTimeMillis();
                switch (which) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                        SettingsActivity.this.setDnd(1800000 + t);
                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                        SettingsActivity.this.setDnd(3600000 + t);
                    case SettingsActivity.RESULT_LOGGED_OUT /*2*/:
                        SettingsActivity.this.setDnd(7200000 + t);
                    case Group.ADMIN_LEVEL_ADMIN /*3*/:
                        SettingsActivity.this.setDnd(28800000 + t);
                    case UserListView.TYPE_FAVE /*4*/:
                        SettingsActivity.this.showTimePicker();
                    default:
                }
            }
        }).setTitle(C0436R.string.chat_dnd).show();
    }

    private void showTimePicker() {
        Date date = new Date();
        new TimePickerDialog(this, new OnTimeSetListener() {
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Date date = new Date();
                if ((date.getHours() * 60) + date.getMinutes() < (hourOfDay * 60) + minute) {
                    date.setHours(hourOfDay);
                    date.setMinutes(minute);
                    SettingsActivity.this.setDnd(date.getTime());
                    return;
                }
                date.setTime(System.currentTimeMillis() + 86400000);
                date.setHours(hourOfDay);
                date.setMinutes(minute);
                SettingsActivity.this.setDnd(date.getTime());
            }
        }, date.getHours(), date.getMinutes(), true).show();
    }

    private void setDnd(long t) {
        this.prefs.edit().putLong("dnd_end", t).commit();
        PreferenceCategory cat = (PreferenceCategory) findPreference("cat_notify");
        if (cat.findPreference("dnd_cancel") == null) {
            cat.addPreference(this.cancelDndPref);
        }
        this.cancelDndPref.setSummary(getResources().getString(C0436R.string.sett_dnd_desc, new Object[]{Global.langDate(getResources(), (int) (t / 1000))}));
        cat.removePreference(this.dnd1Pref);
    }

    private void updateRingtoneName(String uri) {
        Preference pref = findPreference("notifyRingtone");
        String rt = uri != null ? uri : PreferenceManager.getDefaultSharedPreferences(this).getString("notifyRingtone", "content://settings/system/notification_sound");
        Ringtone ringtone = RingtoneManager.getRingtone(this, Uri.parse(rt));
        String name = "Unknown";
        if (ringtone != null || rt.length() == 0) {
            name = rt.length() > 0 ? ringtone.getTitle(this) : getString(C0436R.string.sett_no_sound);
        }
        pref.setSummary(name);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        }
        onBackPressed();
        return true;
    }

    private void updateSyncLabel(int syncSetting) {
        Preference pref = findPreference("sync");
        switch (syncSetting) {
            case BoardTopicsFragment.ORDER_UPDATED_ASC /*-1*/:
                pref.setEnabled(false);
                pref.setSummary(C0436R.string.sync_not_supported);
            case ValidationActivity.VRESULT_NONE /*0*/:
                pref.setSummary(C0436R.string.sync_all);
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                pref.setSummary(C0436R.string.sync_existing);
            case RESULT_LOGGED_OUT /*2*/:
                pref.setSummary(C0436R.string.sync_off);
            default:
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SYNC_SETTINGS && resultCode == -1) {
            updateSyncLabel(data.getIntExtra("option", 0));
        }
    }

    private void confirmLogout() {
        new Builder(this).setMessage(C0436R.string.log_out_warning).setTitle(C0436R.string.log_out).setPositiveButton(C0436R.string.yes, new OnClickListener() {

            /* renamed from: com.vkontakte.android.SettingsActivity.15.1 */
            class C04741 implements Runnable {
                private final /* synthetic */ ProgressDialog val$dlg;

                /* renamed from: com.vkontakte.android.SettingsActivity.15.1.1 */
                class C04731 implements Runnable {
                    private final /* synthetic */ ProgressDialog val$dlg;

                    C04731(ProgressDialog progressDialog) {
                        this.val$dlg = progressDialog;
                    }

                    public void run() {
                        this.val$dlg.dismiss();
                        SettingsActivity.this.setResult(SettingsActivity.RESULT_LOGGED_OUT);
                        SettingsActivity.this.finish();
                        Intent intent = new Intent(SettingsActivity.this.getApplicationContext(), MainActivity.class);
                        intent.addFlags(67108864);
                        SettingsActivity.this.startActivity(intent);
                    }
                }

                C04741(ProgressDialog progressDialog) {
                    this.val$dlg = progressDialog;
                }

                public void run() {
                    LongPollService.logOut(true);
                    SettingsActivity.this.runOnUiThread(new C04731(this.val$dlg));
                }
            }

            public void onClick(DialogInterface dialog, int which) {
                ProgressDialog dlg = new ProgressDialog(SettingsActivity.this);
                dlg.setMessage(SettingsActivity.this.getResources().getString(C0436R.string.loading));
                dlg.show();
                dlg.setCancelable(false);
                new Thread(new C04741(dlg)).start();
            }
        }).setNegativeButton(C0436R.string.no, null).setIcon(ACRAConstants.DEFAULT_DIALOG_ICON).show();
    }
}
