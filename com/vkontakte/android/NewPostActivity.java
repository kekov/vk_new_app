package com.vkontakte.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DialogFragment;
import android.app.Notification;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings.Secure;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.MotionEventCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.vkontakte.android.APIRequest.APIHandler;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.VideoFile;
import com.vkontakte.android.cache.NewsfeedCache;
import com.vkontakte.android.cache.UserWallCache;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.data.Posts;
import com.vkontakte.android.fragments.DateTimePickerDialogFragment;
import com.vkontakte.android.fragments.DateTimePickerDialogFragment.OnSelectedListener;
import com.vkontakte.android.ui.AttachmentsEditorView;
import com.vkontakte.android.ui.AttachmentsEditorView.Callback;
import com.vkontakte.android.ui.PendingPhotoAttachment;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class NewPostActivity extends SherlockFragmentActivity {
    private static final int AUDIO_RESULT = 4;
    private static final int DOCUMENT_RESULT = 6;
    private static final int LOCATION_RESULT = 3;
    private static final int MENTION_RESULT = 7;
    public static final int NOTIFY_ID = 1;
    public static final int NOTIFY_ID_PROGR = 2;
    private static final int PHOTO_RESULT = 1;
    public static final int POLL_EDIT_RESULT = 9;
    private static final int POLL_RESULT = 8;
    private static final int VIDEO_RESULT = 5;
    private AttachmentsEditorView attachView;
    Intent camIntent;
    private boolean changed;
    boolean exportToFacebook;
    boolean exportToTwitter;
    boolean friendsOnly;
    boolean fromGroup;
    private GeoAttachment geoAttach;
    Uri imageUri;
    private boolean isComment;
    private boolean isSuggest;
    boolean mdSetup;
    boolean needPostAfterLocation;
    Notification notification;
    private ListView optionsAlertView;
    private Calendar postponeTo;
    ProgressDialog progrDlg;
    ProgressDialog progrDlg2;
    ProgressDialog progrDlg3;
    boolean publishing;
    private boolean saveDraft;
    private View sendBtn;
    private Attachment sigAttach;
    boolean signedFromGroup;
    int uid;
    RemoteViews uploadView;

    /* renamed from: com.vkontakte.android.NewPostActivity.15 */
    class AnonymousClass15 implements OnMultiChoiceClickListener {
        private final /* synthetic */ ArrayList val$acts;
        private final /* synthetic */ boolean[] val$enabled;
        private final /* synthetic */ boolean[] val$opts;

        AnonymousClass15(ArrayList arrayList, boolean[] zArr, boolean[] zArr2) {
            this.val$acts = arrayList;
            this.val$enabled = zArr;
            this.val$opts = zArr2;
        }

        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
            boolean z = true;
            boolean z2 = false;
            int w = -1;
            String s = (String) this.val$acts.get(which);
            boolean z3;
            ListView listView;
            if ("friendsonly".equals(s)) {
                boolean[] zArr;
                int indexOf;
                w = 0;
                if (this.val$acts.contains("twitter")) {
                    zArr = this.val$enabled;
                    indexOf = this.val$acts.indexOf("twitter");
                    if (isChecked) {
                        z3 = false;
                    } else {
                        z3 = true;
                    }
                    zArr[indexOf] = z3;
                    zArr = this.val$opts;
                    indexOf = this.val$acts.indexOf("twitter");
                    if (isChecked) {
                        z3 = false;
                    } else {
                        z3 = true;
                    }
                    zArr[indexOf] = z3;
                    listView = ((AlertDialog) dialog).getListView();
                    indexOf = this.val$acts.indexOf("twitter");
                    if (isChecked) {
                        z3 = false;
                    } else {
                        z3 = true;
                    }
                    listView.setItemChecked(indexOf, z3);
                }
                if (this.val$acts.contains("fb")) {
                    zArr = this.val$enabled;
                    indexOf = this.val$acts.indexOf("fb");
                    if (isChecked) {
                        z3 = false;
                    } else {
                        z3 = true;
                    }
                    zArr[indexOf] = z3;
                    zArr = this.val$opts;
                    indexOf = this.val$acts.indexOf("fb");
                    if (isChecked) {
                        z3 = false;
                    } else {
                        z3 = true;
                    }
                    zArr[indexOf] = z3;
                    ListView listView2 = ((AlertDialog) dialog).getListView();
                    int indexOf2 = this.val$acts.indexOf("fb");
                    if (!isChecked) {
                        z2 = true;
                    }
                    listView2.setItemChecked(indexOf2, z2);
                }
                ((BaseAdapter) ((AlertDialog) dialog).getListView().getAdapter()).notifyDataSetChanged();
            } else if ("twitter".equals(s)) {
                w = NewPostActivity.PHOTO_RESULT;
            } else if ("fb".equals(s)) {
                w = NewPostActivity.NOTIFY_ID_PROGR;
            } else if ("timer".equals(s)) {
                NewPostActivity.this.showTimerDlg();
                listView = NewPostActivity.this.optionsAlertView;
                if (NewPostActivity.this.postponeTo != null) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                listView.setItemChecked(which, z3);
                boolean[] zArr2 = this.val$opts;
                if (NewPostActivity.this.postponeTo == null) {
                    z = false;
                }
                zArr2[which] = z;
            }
            if (w != -1) {
                this.val$opts[w] = isChecked;
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.16 */
    class AnonymousClass16 implements OnClickListener {
        private final /* synthetic */ boolean[] val$opts;

        AnonymousClass16(boolean[] zArr) {
            this.val$opts = zArr;
        }

        public void onClick(DialogInterface dialog, int which) {
            NewPostActivity.this.friendsOnly = this.val$opts[0];
            NewPostActivity.this.exportToTwitter = this.val$opts[NewPostActivity.PHOTO_RESULT];
            NewPostActivity.this.exportToFacebook = this.val$opts[NewPostActivity.NOTIFY_ID_PROGR];
            NewPostActivity.this.getSharedPreferences(null, 0).edit().putBoolean("export_twitter", this.val$opts[NewPostActivity.PHOTO_RESULT]).putBoolean("export_facebook", this.val$opts[NewPostActivity.NOTIFY_ID_PROGR]).commit();
            NewPostActivity.this.updateExportIcons();
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.18 */
    class AnonymousClass18 implements OnMultiChoiceClickListener {
        private final /* synthetic */ boolean[] val$enabled;
        private final /* synthetic */ boolean[] val$opts;

        AnonymousClass18(boolean[] zArr, boolean[] zArr2) {
            this.val$enabled = zArr;
            this.val$opts = zArr2;
        }

        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
            boolean z = true;
            if (which == 0) {
                boolean[] zArr = this.val$enabled;
                this.val$enabled[NewPostActivity.NOTIFY_ID_PROGR] = isChecked;
                zArr[NewPostActivity.PHOTO_RESULT] = isChecked;
                ((BaseAdapter) ((AlertDialog) dialog).getListView().getAdapter()).notifyDataSetChanged();
                if (!isChecked) {
                    this.val$opts[NewPostActivity.PHOTO_RESULT] = false;
                    ((AlertDialog) dialog).getListView().setItemChecked(NewPostActivity.PHOTO_RESULT, false);
                    ((AlertDialog) dialog).getListView().setItemChecked(NewPostActivity.NOTIFY_ID_PROGR, false);
                    NewPostActivity.this.postponeTo = null;
                    NewPostActivity.this.updateTimer();
                }
            }
            if (which == NewPostActivity.NOTIFY_ID_PROGR) {
                NewPostActivity.this.showTimerDlg();
                NewPostActivity.this.optionsAlertView.setItemChecked(which, NewPostActivity.this.postponeTo != null);
                zArr = this.val$opts;
                if (NewPostActivity.this.postponeTo == null) {
                    z = false;
                }
                zArr[which] = z;
                return;
            }
            this.val$opts[which] = isChecked;
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.19 */
    class AnonymousClass19 implements OnClickListener {
        private final /* synthetic */ boolean[] val$opts;

        AnonymousClass19(boolean[] zArr) {
            this.val$opts = zArr;
        }

        public void onClick(DialogInterface dialog, int which) {
            int i = 0;
            NewPostActivity.this.fromGroup = this.val$opts[0];
            NewPostActivity.this.signedFromGroup = this.val$opts[NewPostActivity.PHOTO_RESULT];
            NewPostActivity.this.updateExportIcons();
            View findViewById = NewPostActivity.this.findViewById(C0436R.id.newpost_signature);
            if (!NewPostActivity.this.signedFromGroup) {
                i = NewPostActivity.POLL_RESULT;
            }
            findViewById.setVisibility(i);
            NewPostActivity.this.updateFieldSize();
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.1 */
    class C03191 implements Runnable {
        C03191() {
        }

        public void run() {
            ((InputMethodManager) NewPostActivity.this.getSystemService("input_method")).showSoftInput(NewPostActivity.this.findViewById(C0436R.id.status_text_edit), 0);
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.20 */
    class AnonymousClass20 implements OnTouchListener {
        private final /* synthetic */ AlertDialog val$dlg;
        private final /* synthetic */ boolean[] val$enabled;

        AnonymousClass20(AlertDialog alertDialog, boolean[] zArr) {
            this.val$dlg = alertDialog;
            this.val$enabled = zArr;
        }

        public boolean onTouch(View v, MotionEvent event) {
            int idx = this.val$dlg.getListView().pointToPosition((int) event.getX(), (int) event.getY());
            if (idx < 0 || idx >= this.val$enabled.length || this.val$enabled[idx]) {
                return false;
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.22 */
    class AnonymousClass22 extends BaseAdapter {
        private final /* synthetic */ boolean[] val$enabled;
        private final /* synthetic */ ArrayAdapter val$wrapped;

        AnonymousClass22(ArrayAdapter arrayAdapter, boolean[] zArr) {
            this.val$wrapped = arrayAdapter;
            this.val$enabled = zArr;
        }

        public int getCount() {
            return this.val$wrapped.getCount();
        }

        public Object getItem(int position) {
            return this.val$wrapped.getItem(position);
        }

        public long getItemId(int position) {
            return this.val$wrapped.getItemId(position);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view = this.val$wrapped.getView(position, convertView, parent);
            if (this.val$enabled[position]) {
                if (VERSION.SDK_INT >= 14) {
                    view.setAlpha(1.0f);
                } else {
                    view.clearAnimation();
                }
            } else if (VERSION.SDK_INT >= 14) {
                view.setAlpha(0.3f);
            } else {
                AlphaAnimation aa = new AlphaAnimation(0.3f, 0.3f);
                aa.setFillAfter(true);
                view.startAnimation(aa);
            }
            return view;
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.23 */
    class AnonymousClass23 implements Runnable {
        private final /* synthetic */ Intent val$intent;
        private final /* synthetic */ int val$requestCode;

        AnonymousClass23(int i, Intent intent) {
            this.val$requestCode = i;
            this.val$intent = intent;
        }

        public void run() {
            Iterator it;
            if (this.val$requestCode == NewPostActivity.PHOTO_RESULT) {
                if (this.val$intent.hasExtra("attachment")) {
                    NewPostActivity.this.attachView.add((PhotoAttachment) this.val$intent.getParcelableExtra("attachment"));
                } else if (this.val$intent.hasExtra("files")) {
                    it = this.val$intent.getStringArrayListExtra("files").iterator();
                    while (it.hasNext()) {
                        NewPostActivity.this.attachView.add(new PendingPhotoAttachment((String) it.next()));
                    }
                } else {
                    NewPostActivity.this.attachView.add(new PendingPhotoAttachment(this.val$intent.getStringExtra("file")));
                }
                NewPostActivity.this.showAttachView(true);
            }
            if (this.val$requestCode == NewPostActivity.AUDIO_RESULT) {
                NewPostActivity.this.attachView.add(new AudioAttachment((AudioFile) this.val$intent.getParcelableExtra("audio")));
                NewPostActivity.this.showAttachView(true);
            }
            if (this.val$requestCode == NewPostActivity.VIDEO_RESULT) {
                NewPostActivity.this.attachView.add(new VideoAttachment((VideoFile) this.val$intent.getParcelableExtra("video")));
                NewPostActivity.this.showAttachView(true);
            }
            if (this.val$requestCode == NewPostActivity.DOCUMENT_RESULT) {
                it = this.val$intent.getParcelableArrayListExtra("documents").iterator();
                while (it.hasNext()) {
                    NewPostActivity.this.attachView.add((Attachment) ((Parcelable) it.next()));
                }
                NewPostActivity.this.showAttachView(true);
            }
            if (this.val$requestCode == NewPostActivity.MENTION_RESULT) {
                UserProfile p = (UserProfile) this.val$intent.getParcelableExtra("user");
                EditText txt = (EditText) NewPostActivity.this.findViewById(C0436R.id.status_text_edit);
                if (txt.getSelectionEnd() != txt.getSelectionStart()) {
                    String s = txt.getText().subSequence(txt.getSelectionStart(), txt.getSelectionEnd()).toString();
                    txt.getText().replace(txt.getSelectionStart(), txt.getSelectionEnd(), "*id" + p.uid + " (" + txt + ")");
                } else {
                    txt.getText().insert(txt.getSelectionEnd(), new StringBuilder(String.valueOf(txt.getSelectionStart() == 0 ? ACRAConstants.DEFAULT_STRING_VALUE : " ")).append("*id").append(p.uid).append(" (").append(p.fullName).append(") ").toString());
                }
            }
            if (this.val$requestCode == NewPostActivity.POLL_RESULT) {
                NewPostActivity.this.attachView.add((PollAttachment) this.val$intent.getParcelableExtra("poll"));
                NewPostActivity.this.showAttachView(true);
            }
            if (this.val$requestCode == NewPostActivity.POLL_EDIT_RESULT) {
                Attachment poll = (PollAttachment) this.val$intent.getParcelableExtra("poll");
                NewPostActivity.this.attachView.add(poll);
                it = NewPostActivity.this.attachView.getAll().iterator();
                while (it.hasNext()) {
                    Attachment att = (Attachment) it.next();
                    if ((att instanceof PollAttachment) && att != poll) {
                        NewPostActivity.this.attachView.remove(att);
                        break;
                    }
                }
            }
            NewPostActivity.this.updateSendButton();
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.2 */
    class C03202 implements View.OnClickListener {
        C03202() {
        }

        public void onClick(View arg0) {
            NewPostActivity.this.publishing = true;
            NewPostActivity.this.saveDraft = false;
            NewPostActivity.this.post();
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.3 */
    class C03213 implements View.OnClickListener {
        C03213() {
        }

        public void onClick(View arg0) {
            if (NewPostActivity.this.checkAttachLimit()) {
                Intent intent = new Intent(NewPostActivity.this, ImagePickerActivity.class);
                intent.putExtra("limit", (NewPostActivity.this.isComment ? NewPostActivity.NOTIFY_ID_PROGR : 10) - NewPostActivity.this.attachView.getCount());
                NewPostActivity.this.startActivityForResult(intent, NewPostActivity.PHOTO_RESULT);
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.4 */
    class C03234 implements View.OnClickListener {

        /* renamed from: com.vkontakte.android.NewPostActivity.4.1 */
        class C03221 implements OnClickListener {
            C03221() {
            }

            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    NewPostActivity.this.startLocationChooser();
                }
                if (which == NewPostActivity.PHOTO_RESULT) {
                    NewPostActivity.this.removeLocation();
                }
            }
        }

        C03234() {
        }

        public void onClick(View v) {
            if (NewPostActivity.this.geoAttach == null) {
                NewPostActivity.this.startLocationChooser();
                return;
            }
            Builder title = new VKAlertDialog.Builder(NewPostActivity.this).setTitle(C0436R.string.place);
            CharSequence[] charSequenceArr = new String[NewPostActivity.NOTIFY_ID_PROGR];
            charSequenceArr[0] = NewPostActivity.this.getString(C0436R.string.edit);
            charSequenceArr[NewPostActivity.PHOTO_RESULT] = NewPostActivity.this.getString(C0436R.string.delete);
            title.setItems(charSequenceArr, new C03221()).show();
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.5 */
    class C03245 implements View.OnClickListener {
        C03245() {
        }

        public void onClick(View v) {
            NewPostActivity.this.showOptions();
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.6 */
    class C03256 implements View.OnClickListener {
        C03256() {
        }

        public void onClick(View arg0) {
            NewPostActivity.this.startMentionChooser();
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.7 */
    class C03267 implements View.OnClickListener {
        C03267() {
        }

        public void onClick(View arg0) {
            NewPostActivity.this.showExtendedAttachMenu();
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.8 */
    class C03278 implements TextWatcher {
        C03278() {
        }

        public void afterTextChanged(Editable s) {
            int i = 0;
            ImageSpan[] spans = (ImageSpan[]) s.getSpans(0, s.length(), ImageSpan.class);
            int length = spans.length;
            while (i < length) {
                s.removeSpan(spans[i]);
                i += NewPostActivity.PHOTO_RESULT;
            }
            Global.replaceEmoji(s);
        }

        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        public void onTextChanged(CharSequence str, int arg1, int arg2, int len) {
            NewPostActivity.this.updateSendButton();
            NewPostActivity.this.showAttachView(NewPostActivity.this.attachView.getVisibility() == 0);
            NewPostActivity.this.changed = true;
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.26 */
    class AnonymousClass26 extends APIHandler {
        private final /* synthetic */ ArrayList val$atts;
        private final /* synthetic */ NewsEntry val$edit;
        private final /* synthetic */ String val$ptext;

        AnonymousClass26(String str, NewsEntry newsEntry, ArrayList arrayList) {
            this.val$ptext = str;
            this.val$edit = newsEntry;
            this.val$atts = arrayList;
        }

        public void success(JSONObject r) {
            SharedPreferences prefs = NewPostActivity.this.getSharedPreferences(null, 0);
            NewsEntry e;
            Intent intent;
            if (!NewPostActivity.this.getIntent().hasExtra("edit") || NewPostActivity.this.isSuggest) {
                if (NewPostActivity.this.postponeTo != null) {
                    Context applicationContext = NewPostActivity.this.getApplicationContext();
                    Resources resources = NewPostActivity.this.getResources();
                    Object[] objArr = new Object[NewPostActivity.PHOTO_RESULT];
                    objArr[0] = Global.langDate(NewPostActivity.this.getResources(), (int) (NewPostActivity.this.postponeTo.getTimeInMillis() / 1000));
                    Toast.makeText(applicationContext, resources.getString(C0436R.string.wall_postponed, objArr), 0).show();
                } else {
                    Toast.makeText(NewPostActivity.this.getApplicationContext(), NewPostActivity.this.getResources().getString(NewPostActivity.this.getIntent().getBooleanExtra("suggest", false) ? C0436R.string.post_edit_saved : C0436R.string.wall_ok), 0).show();
                }
                e = new NewsEntry();
                e.postID = r.optJSONObject("response").optInt("post_id");
                e.text = Global.replaceMentions(this.val$ptext);
                e.time = (int) (System.currentTimeMillis() / 1000);
                e.userID = NewPostActivity.this.fromGroup ? NewPostActivity.this.uid : Global.uid;
                e.ownerID = NewPostActivity.this.uid;
                e.userName = NewPostActivity.this.fromGroup ? NewPostActivity.this.getIntent().getStringExtra("group_title") : prefs.getString("username", "DELETED");
                e.userPhotoURL = NewPostActivity.this.fromGroup ? NewPostActivity.this.getIntent().getStringExtra("group_photo") : prefs.getString("userphoto", "http://vkontakte.ru/images/question_b.gif");
                e.flags = 194;
                if (NewPostActivity.this.uid < 0 && NewPostActivity.this.fromGroup) {
                    Group g = Groups.getById(-NewPostActivity.this.uid);
                    if (g == null || g.isClosed == 0) {
                        e.flags |= NewPostActivity.PHOTO_RESULT;
                    }
                }
                if (NewPostActivity.this.friendsOnly) {
                    e.flags |= NewsEntry.FLAG_FRIENDS_ONLY;
                }
                if (NewPostActivity.this.postponeTo != null) {
                    e.flags |= NewsEntry.FLAG_POSTPONED;
                }
                if (NewPostActivity.this.getIntent().getBooleanExtra("suggest", false)) {
                    e.flags |= NewsEntry.FLAG_SUGGESTED;
                }
                e.attachments.addAll(this.val$atts);
                NewPostActivity.this.attachView.clear();
                if (NewPostActivity.this.geoAttach != null) {
                    e.attachments.add(NewPostActivity.this.geoAttach);
                }
                if (NewPostActivity.this.signedFromGroup) {
                    e.attachments.add(new SignatureLinkAttachment("http://vkontakte.ru/id" + Global.uid, prefs.getString("username", "DELETED")));
                }
                intent = new Intent(Posts.ACTION_NEW_POST_BROADCAST);
                intent.putExtra("entry", e);
                NewPostActivity.this.sendBroadcast(intent);
                if (NewPostActivity.this.isSuggest || (this.val$edit != null && this.val$edit.flag(NewsEntry.FLAG_POSTPONED) && NewPostActivity.this.postponeTo == null)) {
                    intent = new Intent(Posts.ACTION_POST_DELETED_BROADCAST);
                    intent.putExtra("owner_id", this.val$edit.ownerID);
                    intent.putExtra("post_id", this.val$edit.postID);
                    intent.putExtra("post", this.val$edit);
                    NewPostActivity.this.sendBroadcast(intent);
                }
                if (NewPostActivity.this.uid == Global.uid && UserWallCache.hasEntries(VKApplication.context) && NewPostActivity.this.postponeTo == null) {
                    UserWallCache.add(e, NewPostActivity.this);
                }
                if ((NewPostActivity.this.uid == Global.uid || (NewPostActivity.this.uid < 0 && NewPostActivity.this.fromGroup)) && NewPostActivity.this.postponeTo == null && !NewPostActivity.this.getIntent().hasExtra("suggest") && NewsfeedCache.hasEntries(VKApplication.context)) {
                    NewsfeedCache.add(e, NewPostActivity.this);
                }
                NewPostActivity.this.setResult(-1);
                NewPostActivity.this.finish();
                return;
            }
            String str;
            Toast.makeText(NewPostActivity.this.getApplicationContext(), C0436R.string.post_edit_saved, 0).show();
            e = (NewsEntry) NewPostActivity.this.getIntent().getParcelableExtra("edit");
            if (NewPostActivity.this.postponeTo != null) {
                e.time = (int) (NewPostActivity.this.postponeTo.getTimeInMillis() / 1000);
            }
            if (e.flag(NewsEntry.FLAG_POSTPONED) && NewPostActivity.this.postponeTo == null) {
                intent = new Intent(Posts.ACTION_POST_DELETED_BROADCAST);
                intent.putExtra("owner_id", e.ownerID);
                intent.putExtra("post_id", e.postID);
                intent.putExtra("post", e);
                NewPostActivity.this.sendBroadcast(intent);
                e.time = (int) (System.currentTimeMillis() / 1000);
                e.flag(NewsEntry.FLAG_POSTPONED, false);
            }
            if (NewPostActivity.this.isComment) {
                str = this.val$ptext;
            } else {
                str = Global.replaceMentions(this.val$ptext);
            }
            e.text = str;
            e.attachments.clear();
            e.attachments.addAll(NewPostActivity.this.attachView.getAll());
            NewPostActivity.this.attachView.clear();
            if (NewPostActivity.this.isComment) {
                intent = new Intent();
                intent.putExtra("comment", e);
                NewPostActivity.this.setResult(-1, intent);
                NewPostActivity.this.finish();
                return;
            }
            if (NewPostActivity.this.geoAttach != null) {
                e.attachments.add(NewPostActivity.this.geoAttach);
            }
            if (NewPostActivity.this.signedFromGroup) {
                if (NewPostActivity.this.sigAttach == null) {
                    if (this.val$edit.userID > 0) {
                        NewPostActivity.this.sigAttach = new SignatureLinkAttachment("http://vkontakte.ru/id" + this.val$edit.userID, this.val$edit.userName);
                    } else {
                        NewPostActivity.this.sigAttach = new SignatureLinkAttachment("http://vkontakte.ru/id" + Global.uid, prefs.getString("username", "DELETED"));
                    }
                }
                e.attachments.add(NewPostActivity.this.sigAttach);
            }
            e.flag(NewsEntry.FLAG_EXPORT_FACEBOOK, NewPostActivity.this.exportToFacebook);
            e.flag(ACRAConstants.DEFAULT_BUFFER_SIZE_IN_BYTES, NewPostActivity.this.exportToTwitter);
            e.flag(NewsEntry.FLAG_FRIENDS_ONLY, NewPostActivity.this.friendsOnly);
            intent = new Intent(Posts.ACTION_POST_REPLACED_BROADCAST);
            intent.putExtra("entry", e);
            NewPostActivity.this.sendBroadcast(intent);
            NewPostActivity.this.setResult(-1);
            NewPostActivity.this.finish();
            if (e.ownerID == Global.uid && UserWallCache.hasEntries(VKApplication.context) && NewPostActivity.this.postponeTo == null && !e.flag(NewsEntry.FLAG_SUGGESTED)) {
                UserWallCache.remove(e.postID, NewPostActivity.this);
                UserWallCache.add(e, NewPostActivity.this);
            }
            if (e.ownerID == e.userID && NewPostActivity.this.postponeTo == null) {
                NewsfeedCache.remove(e.ownerID, e.postID, NewPostActivity.this);
                NewsfeedCache.add(e, NewPostActivity.this);
            }
        }

        public void fail(int ecode, String emsg) {
            if (NewPostActivity.this.postponeTo != null && ecode == 214) {
                String err = null;
                if (emsg.contains("already scheduled for this time")) {
                    err = NewPostActivity.this.getString(C0436R.string.postpone_error_already_exists);
                }
                if (emsg.contains("posts on a day")) {
                    err = NewPostActivity.this.getString(C0436R.string.postpone_error_per_day);
                }
                if (emsg.contains("schedule more than")) {
                    err = NewPostActivity.this.getString(C0436R.string.postpone_error_too_many);
                }
                if (err != null) {
                    Toast.makeText(NewPostActivity.this.getApplicationContext(), err, 0).show();
                }
            } else if (NewPostActivity.this.postponeTo != null && ecode == 100 && emsg.contains("publish_date")) {
                Toast.makeText(NewPostActivity.this.getApplicationContext(), NewPostActivity.this.getResources().getString(C0436R.string.invalid_date), 0).show();
            } else {
                Toast.makeText(NewPostActivity.this.getApplicationContext(), NewPostActivity.this.getResources().getString(C0436R.string.error), 0).show();
            }
        }
    }

    /* renamed from: com.vkontakte.android.NewPostActivity.9 */
    class C13169 implements Callback {
        C13169() {
        }

        public void onUploadFailed() {
            NewPostActivity.this.updateSendButton();
        }

        public void onAttachmentRemoved(Attachment att) {
            NewPostActivity.this.updateSendButton();
            if (NewPostActivity.this.attachView.getCount() == 0) {
                NewPostActivity.this.showAttachView(false);
            }
            NewPostActivity.this.changed = true;
        }

        public void onAllUploadsDone() {
            NewPostActivity.this.updateSendButton();
        }
    }

    public NewPostActivity() {
        this.uid = 0;
        this.mdSetup = false;
        this.publishing = false;
        this.fromGroup = false;
        this.signedFromGroup = false;
        this.needPostAfterLocation = false;
        this.saveDraft = true;
        this.isComment = false;
        this.changed = false;
        this.friendsOnly = false;
        this.exportToTwitter = false;
        this.exportToFacebook = false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r23) {
        /*
        r22 = this;
        super.onCreate(r23);
        r17 = r22.getIntent();
        r18 = "edit";
        r17 = r17.hasExtra(r18);
        if (r17 == 0) goto L_0x0019;
    L_0x000f:
        r17 = 2131231127; // 0x7f080197 float:1.8078326E38 double:1.052968083E-314;
        r0 = r22;
        r1 = r17;
        r0.setTitle(r1);
    L_0x0019:
        r17 = r22.getIntent();
        r18 = "suggest";
        r17 = r17.hasExtra(r18);
        if (r17 == 0) goto L_0x002f;
    L_0x0025:
        r17 = 2131231488; // 0x7f080300 float:1.8079058E38 double:1.0529682616E-314;
        r0 = r22;
        r1 = r17;
        r0.setTitle(r1);
    L_0x002f:
        r17 = com.vkontakte.android.Global.isTablet;
        if (r17 == 0) goto L_0x06ed;
    L_0x0033:
        r17 = 8;
        r0 = r22;
        r1 = r17;
        r0.requestWindowFeature(r1);
        r17 = r22.getWindow();
        r18 = 2;
        r19 = 2;
        r17.setFlags(r18, r19);
        r17 = r22.getWindow();
        r8 = r17.getAttributes();
        r17 = 1140457472; // 0x43fa0000 float:500.0 double:5.634608575E-315;
        r17 = com.vkontakte.android.Global.scale(r17);
        r0 = r17;
        r8.width = r0;
        r17 = 1134723072; // 0x43a28000 float:325.0 double:5.606276874E-315;
        r17 = com.vkontakte.android.Global.scale(r17);
        r0 = r17;
        r8.height = r0;
        r17 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r0 = r17;
        r8.alpha = r0;
        r17 = 1056964608; // 0x3f000000 float:0.5 double:5.222099017E-315;
        r0 = r17;
        r8.dimAmount = r0;
        r17 = 32;
        r0 = r17;
        r8.softInputMode = r0;
        r17 = 49;
        r0 = r17;
        r8.gravity = r0;
        r17 = 1109393408; // 0x42200000 float:40.0 double:5.481131706E-315;
        r17 = com.vkontakte.android.Global.scale(r17);
        r0 = r17;
        r8.y = r0;
        r17 = r22.getWindow();
        r0 = r17;
        r0.setAttributes(r8);
        r17 = r22.getWindow();
        r18 = 32;
        r17.setSoftInputMode(r18);
    L_0x0098:
        r17 = 2130903040; // 0x7f030000 float:1.7412887E38 double:1.0528059867E-314;
        r18 = 0;
        r0 = r22;
        r1 = r17;
        r2 = r18;
        r17 = android.view.View.inflate(r0, r1, r2);
        r0 = r17;
        r1 = r22;
        r1.sendBtn = r0;
        r17 = r22.getIntent();
        r18 = "uid";
        r19 = com.vkontakte.android.Global.uid;
        r17 = r17.getIntExtra(r18, r19);
        r0 = r17;
        r1 = r22;
        r1.uid = r0;
        r17 = 2130903182; // 0x7f03008e float:1.7413175E38 double:1.052806057E-314;
        r0 = r22;
        r1 = r17;
        r0.setContentView(r1);
        r17 = android.os.Build.VERSION.SDK_INT;
        r18 = 14;
        r0 = r17;
        r1 = r18;
        if (r0 < r1) goto L_0x00e0;
    L_0x00d2:
        r17 = 2131296701; // 0x7f0901bd float:1.8211326E38 double:1.053000481E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        com.vkontakte.android.ViewUtils.setNoClipRecursive(r17);
    L_0x00e0:
        r17 = 2131296703; // 0x7f0901bf float:1.821133E38 double:1.053000482E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = new com.vkontakte.android.NewPostActivity$1;
        r0 = r18;
        r1 = r22;
        r0.<init>();
        r19 = 100;
        r17.postDelayed(r18, r19);
        r0 = r22;
        r0 = r0.sendBtn;
        r17 = r0;
        r18 = new com.vkontakte.android.NewPostActivity$2;
        r0 = r18;
        r1 = r22;
        r0.<init>();
        r17.setOnClickListener(r18);
        r17 = 2131296712; // 0x7f0901c8 float:1.8211348E38 double:1.0530004865E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = new com.vkontakte.android.NewPostActivity$3;
        r0 = r18;
        r1 = r22;
        r0.<init>();
        r17.setOnClickListener(r18);
        r17 = 2131296710; // 0x7f0901c6 float:1.8211344E38 double:1.0530004855E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = new com.vkontakte.android.NewPostActivity$4;
        r0 = r18;
        r1 = r22;
        r0.<init>();
        r17.setOnClickListener(r18);
        r17 = 2131296714; // 0x7f0901ca float:1.8211352E38 double:1.0530004875E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = new com.vkontakte.android.NewPostActivity$5;
        r0 = r18;
        r1 = r22;
        r0.<init>();
        r17.setOnClickListener(r18);
        r17 = 2131296711; // 0x7f0901c7 float:1.8211346E38 double:1.053000486E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = new com.vkontakte.android.NewPostActivity$6;
        r0 = r18;
        r1 = r22;
        r0.<init>();
        r17.setOnClickListener(r18);
        r17 = 2131296713; // 0x7f0901c9 float:1.821135E38 double:1.053000487E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = new com.vkontakte.android.NewPostActivity$7;
        r0 = r18;
        r1 = r22;
        r0.<init>();
        r17.setOnClickListener(r18);
        r17 = 2131296706; // 0x7f0901c2 float:1.8211336E38 double:1.0530004835E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 8;
        r17.setVisibility(r18);
        r17 = 2131296703; // 0x7f0901bf float:1.821133E38 double:1.053000482E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r17 = (android.widget.EditText) r17;
        r18 = new com.vkontakte.android.NewPostActivity$8;
        r0 = r18;
        r1 = r22;
        r0.<init>();
        r17.addTextChangedListener(r18);
        r17 = 2131296704; // 0x7f0901c0 float:1.8211332E38 double:1.0530004825E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r17 = (com.vkontakte.android.ui.AttachmentsEditorView) r17;
        r0 = r17;
        r1 = r22;
        r1.attachView = r0;
        r0 = r22;
        r0 = r0.attachView;
        r17 = r0;
        r18 = 0;
        r0 = r18;
        r1 = r17;
        r1.uploadType = r0;
        r0 = r22;
        r0 = r0.attachView;
        r17 = r0;
        r0 = r22;
        r0 = r0.uid;
        r18 = r0;
        r0 = r18;
        r1 = r17;
        r1.uploadOwnerId = r0;
        r0 = r22;
        r0 = r0.attachView;
        r17 = r0;
        r18 = new com.vkontakte.android.NewPostActivity$9;
        r0 = r18;
        r1 = r22;
        r0.<init>();
        r17.setCallback(r18);
        r17 = "android.intent.action.SEND";
        r18 = r22.getIntent();
        r18 = r18.getAction();
        r17 = r17.equals(r18);
        if (r17 == 0) goto L_0x02e6;
    L_0x01fc:
        r17 = r22.getIntent();
        r18 = "text";
        r17 = r17.hasExtra(r18);
        if (r17 != 0) goto L_0x0214;
    L_0x0208:
        r17 = r22.getIntent();
        r18 = "link_title";
        r17 = r17.hasExtra(r18);
        if (r17 == 0) goto L_0x02e6;
    L_0x0214:
        r17 = r22.getIntent();	 Catch:{ Exception -> 0x0711 }
        r18 = "text";
        r14 = r17.getStringExtra(r18);	 Catch:{ Exception -> 0x0711 }
        r6 = 0;
        r17 = "((?:(?:http|https)://)[a-zA-Z\u0430-\u044f\u0410-\u042f0-9-]+\\.[a-zA-Z\u0430-\u044f\u0410-\u042f]{2,4}[0-9a-zA-Z/?\\.=#!%&_-]*(?<!\\.)(?<!!))";
        r11 = java.util.regex.Pattern.compile(r17);	 Catch:{ Exception -> 0x0711 }
        if (r14 == 0) goto L_0x0245;
    L_0x0227:
        r7 = r11.matcher(r14);	 Catch:{ Exception -> 0x0711 }
        r17 = r7.find();	 Catch:{ Exception -> 0x0711 }
        if (r17 == 0) goto L_0x0245;
    L_0x0231:
        r17 = 1;
        r0 = r17;
        r6 = r7.group(r0);	 Catch:{ Exception -> 0x0711 }
        r17 = "";
        r0 = r17;
        r14 = r7.replaceFirst(r0);	 Catch:{ Exception -> 0x0711 }
        r14 = r14.trim();	 Catch:{ Exception -> 0x0711 }
    L_0x0245:
        r17 = "vk";
        r18 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0711 }
        r19 = "LINK ";
        r18.<init>(r19);	 Catch:{ Exception -> 0x0711 }
        r0 = r18;
        r18 = r0.append(r6);	 Catch:{ Exception -> 0x0711 }
        r18 = r18.toString();	 Catch:{ Exception -> 0x0711 }
        com.vkontakte.android.Log.m525d(r17, r18);	 Catch:{ Exception -> 0x0711 }
        if (r6 == 0) goto L_0x027e;
    L_0x025d:
        r0 = r22;
        r0 = r0.attachView;	 Catch:{ Exception -> 0x0711 }
        r17 = r0;
        r18 = new com.vkontakte.android.LinkAttachment;	 Catch:{ Exception -> 0x0711 }
        r19 = "";
        r20 = "";
        r0 = r18;
        r1 = r19;
        r2 = r20;
        r0.<init>(r6, r1, r2);	 Catch:{ Exception -> 0x0711 }
        r17.add(r18);	 Catch:{ Exception -> 0x0711 }
        r17 = 1;
        r0 = r22;
        r1 = r17;
        r0.showAttachView(r1);	 Catch:{ Exception -> 0x0711 }
    L_0x027e:
        if (r14 == 0) goto L_0x06f9;
    L_0x0280:
        r17 = r14.length();	 Catch:{ Exception -> 0x0711 }
        if (r17 <= 0) goto L_0x06f9;
    L_0x0286:
        r17 = r22.getIntent();	 Catch:{ Exception -> 0x0711 }
        r18 = "link_title";
        r17 = r17.hasExtra(r18);	 Catch:{ Exception -> 0x0711 }
        if (r17 == 0) goto L_0x06f9;
    L_0x0292:
        r17 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x0711 }
        r18 = r22.getIntent();	 Catch:{ Exception -> 0x0711 }
        r19 = "link_title";
        r18 = r18.getStringExtra(r19);	 Catch:{ Exception -> 0x0711 }
        r18 = java.lang.String.valueOf(r18);	 Catch:{ Exception -> 0x0711 }
        r17.<init>(r18);	 Catch:{ Exception -> 0x0711 }
        r18 = "\n\n";
        r17 = r17.append(r18);	 Catch:{ Exception -> 0x0711 }
        r0 = r17;
        r17 = r0.append(r14);	 Catch:{ Exception -> 0x0711 }
        r14 = r17.toString();	 Catch:{ Exception -> 0x0711 }
    L_0x02b5:
        r17 = 2131296703; // 0x7f0901bf float:1.821133E38 double:1.053000482E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);	 Catch:{ Exception -> 0x0711 }
        r17 = (android.widget.EditText) r17;	 Catch:{ Exception -> 0x0711 }
        r0 = r17;
        r0.setText(r14);	 Catch:{ Exception -> 0x0711 }
        r17 = 2131296703; // 0x7f0901bf float:1.821133E38 double:1.053000482E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);	 Catch:{ Exception -> 0x0711 }
        r17 = (android.widget.EditText) r17;	 Catch:{ Exception -> 0x0711 }
        r18 = r14.length();	 Catch:{ Exception -> 0x0711 }
        r17.setSelection(r18);	 Catch:{ Exception -> 0x0711 }
        r0 = r22;
        r0 = r0.sendBtn;	 Catch:{ Exception -> 0x0711 }
        r17 = r0;
        r18 = 1;
        r17.setEnabled(r18);	 Catch:{ Exception -> 0x0711 }
    L_0x02e6:
        r17 = r22.getIntent();
        r18 = "photoURI";
        r17 = r17.hasExtra(r18);
        if (r17 == 0) goto L_0x0322;
    L_0x02f2:
        r0 = r22;
        r0 = r0.attachView;
        r17 = r0;
        r18 = new com.vkontakte.android.ui.PendingPhotoAttachment;
        r19 = r22.getIntent();
        r20 = "photoURI";
        r19 = r19.getParcelableExtra(r20);
        r19 = r19.toString();
        r18.<init>(r19);
        r17.add(r18);
        r0 = r22;
        r0 = r0.sendBtn;
        r17 = r0;
        r18 = 1;
        r17.setEnabled(r18);
        r17 = 1;
        r0 = r22;
        r1 = r17;
        r0.showAttachView(r1);
    L_0x0322:
        r17 = r22.getIntent();
        r18 = "photos";
        r17 = r17.hasExtra(r18);
        if (r17 == 0) goto L_0x0356;
    L_0x032e:
        r17 = r22.getIntent();
        r18 = "photos";
        r10 = r17.getStringArrayListExtra(r18);
        r17 = r10.iterator();
    L_0x033c:
        r18 = r17.hasNext();
        if (r18 != 0) goto L_0x071b;
    L_0x0342:
        r0 = r22;
        r0 = r0.sendBtn;
        r17 = r0;
        r18 = 1;
        r17.setEnabled(r18);
        r17 = 1;
        r0 = r22;
        r1 = r17;
        r0.showAttachView(r1);
    L_0x0356:
        r0 = r22;
        r0 = r0.uid;
        r17 = r0;
        r18 = com.vkontakte.android.Global.uid;
        r0 = r17;
        r1 = r18;
        if (r0 != r1) goto L_0x0733;
    L_0x0364:
        r17 = r22.getIntent();
        r18 = "edit";
        r17 = r17.hasExtra(r18);
        if (r17 != 0) goto L_0x0733;
    L_0x0370:
        r17 = 0;
        r18 = 0;
        r0 = r22;
        r1 = r17;
        r2 = r18;
        r17 = r0.getSharedPreferences(r1, r2);
        r18 = "export_twitter_avail";
        r19 = 0;
        r17 = r17.getBoolean(r18, r19);
        if (r17 == 0) goto L_0x0390;
    L_0x0388:
        r17 = 1;
        r0 = r17;
        r1 = r22;
        r1.exportToTwitter = r0;
    L_0x0390:
        r17 = 0;
        r18 = 0;
        r0 = r22;
        r1 = r17;
        r2 = r18;
        r17 = r0.getSharedPreferences(r1, r2);
        r18 = "export_facebook_avail";
        r19 = 0;
        r17 = r17.getBoolean(r18, r19);
        if (r17 == 0) goto L_0x03b0;
    L_0x03a8:
        r17 = 1;
        r0 = r17;
        r1 = r22;
        r1.exportToFacebook = r0;
    L_0x03b0:
        r22.updateExportIcons();
    L_0x03b3:
        r17 = r22.getIntent();
        r18 = "public";
        r17 = r17.hasExtra(r18);
        if (r17 == 0) goto L_0x03c7;
    L_0x03bf:
        r17 = 1;
        r0 = r17;
        r1 = r22;
        r1.fromGroup = r0;
    L_0x03c7:
        r17 = r22.getIntent();
        r18 = "edit";
        r17 = r17.hasExtra(r18);
        if (r17 == 0) goto L_0x05d6;
    L_0x03d3:
        r17 = r22.getIntent();
        r18 = "edit";
        r5 = r17.getParcelableExtra(r18);
        r5 = (com.vkontakte.android.NewsEntry) r5;
        r13 = r5.text;
        r17 = "<a href='vkontakte://profile/([0-9]+)'>([^<]+)</a>";
        r18 = "*id$1 ($2)";
        r0 = r17;
        r1 = r18;
        r13 = r13.replaceAll(r0, r1);
        r17 = "<a href='vkontakte://profile/-([0-9]+)'>([^<]+)</a>";
        r18 = "*club$1 ($2)";
        r0 = r17;
        r1 = r18;
        r13 = r13.replaceAll(r0, r1);
        r17 = "<a href='vklink://view/[^']+'>([^<]+)</a>";
        r18 = "$1";
        r0 = r17;
        r1 = r18;
        r13 = r13.replaceAll(r0, r1);
        r17 = "<a href='vkontakte://search/[^']+'>([^<]+)</a>";
        r18 = "$1";
        r0 = r17;
        r1 = r18;
        r13 = r13.replaceAll(r0, r1);
        r17 = 2131296703; // 0x7f0901bf float:1.821133E38 double:1.053000482E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r17 = (android.widget.EditText) r17;
        r0 = r17;
        r0.setText(r13);
        r17 = 2131296703; // 0x7f0901bf float:1.821133E38 double:1.053000482E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r17 = (android.widget.EditText) r17;
        r18 = r13.length();
        r17.setSelection(r18);
        r0 = r5.ownerID;
        r17 = r0;
        r0 = r17;
        r1 = r22;
        r1.uid = r0;
        r12 = 0;
        r0 = r5.attachments;
        r17 = r0;
        r17 = r17.iterator();
    L_0x044a:
        r18 = r17.hasNext();
        if (r18 != 0) goto L_0x0765;
    L_0x0450:
        r0 = r5.attachments;
        r17 = r0;
        r17 = r17.iterator();
    L_0x0458:
        r18 = r17.hasNext();
        if (r18 != 0) goto L_0x07bd;
    L_0x045e:
        r0 = r22;
        r0.showAttachView(r12);
        r0 = r5.ownerID;
        r17 = r0;
        if (r17 >= 0) goto L_0x047d;
    L_0x0469:
        r0 = r5.userID;
        r17 = r0;
        r18 = com.vkontakte.android.Global.uid;
        r0 = r17;
        r1 = r18;
        if (r0 == r1) goto L_0x047d;
    L_0x0475:
        r17 = 1;
        r0 = r17;
        r1 = r22;
        r1.fromGroup = r0;
    L_0x047d:
        r0 = r5.type;
        r17 = r0;
        r18 = 5;
        r0 = r17;
        r1 = r18;
        if (r0 != r1) goto L_0x04bb;
    L_0x0489:
        r17 = 2131296714; // 0x7f0901ca float:1.8211352E38 double:1.0530004875E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 4;
        r17.setVisibility(r18);
        r17 = 2131296710; // 0x7f0901c6 float:1.8211344E38 double:1.0530004855E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 4;
        r17.setVisibility(r18);
        r17 = 2131230944; // 0x7f0800e0 float:1.8077955E38 double:1.052967993E-314;
        r0 = r22;
        r1 = r17;
        r0.setTitle(r1);
        r17 = 1;
        r0 = r17;
        r1 = r22;
        r1.isComment = r0;
    L_0x04bb:
        r17 = 4096; // 0x1000 float:5.74E-42 double:2.0237E-320;
        r0 = r17;
        r17 = r5.flag(r0);
        if (r17 == 0) goto L_0x07e0;
    L_0x04c5:
        r0 = r5.ownerID;
        r17 = r0;
        r0 = r17;
        r0 = -r0;
        r17 = r0;
        r17 = com.vkontakte.android.data.Groups.getAdminLevel(r17);
        r18 = 2;
        r0 = r17;
        r1 = r18;
        if (r0 < r1) goto L_0x07e0;
    L_0x04da:
        r17 = 1;
    L_0x04dc:
        r0 = r17;
        r1 = r22;
        r1.isSuggest = r0;
        r17 = 2048; // 0x800 float:2.87E-42 double:1.0118E-320;
        r0 = r17;
        r17 = r5.flag(r0);
        if (r17 == 0) goto L_0x0551;
    L_0x04ec:
        r17 = java.util.Calendar.getInstance();
        r0 = r17;
        r1 = r22;
        r1.postponeTo = r0;
        r0 = r22;
        r0 = r0.postponeTo;
        r17 = r0;
        r0 = r5.time;
        r18 = r0;
        r0 = r18;
        r0 = (long) r0;
        r18 = r0;
        r20 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
        r18 = r18 * r20;
        r17.setTimeInMillis(r18);
        r22.updateTimer();
        r0 = r22;
        r0 = r0.sendBtn;
        r17 = r0;
        r18 = 2131296299; // 0x7f09002b float:1.821051E38 double:1.0530002824E-314;
        r17 = r17.findViewById(r18);
        r17 = (android.widget.TextView) r17;
        r18 = 2131231481; // 0x7f0802f9 float:1.8079044E38 double:1.052968258E-314;
        r17.setText(r18);
        r17 = 512; // 0x200 float:7.175E-43 double:2.53E-321;
        r0 = r17;
        r17 = r5.flag(r0);
        r0 = r17;
        r1 = r22;
        r1.friendsOnly = r0;
        r17 = 8192; // 0x2000 float:1.14794E-41 double:4.0474E-320;
        r0 = r17;
        r17 = r5.flag(r0);
        r0 = r17;
        r1 = r22;
        r1.exportToTwitter = r0;
        r17 = 16384; // 0x4000 float:2.2959E-41 double:8.0948E-320;
        r0 = r17;
        r17 = r5.flag(r0);
        r0 = r17;
        r1 = r22;
        r1.exportToFacebook = r0;
        r22.updateExportIcons();
    L_0x0551:
        r0 = r22;
        r0 = r0.isSuggest;
        r17 = r0;
        if (r17 == 0) goto L_0x058f;
    L_0x0559:
        r17 = 2131296705; // 0x7f0901c1 float:1.8211334E38 double:1.053000483E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 2131296336; // 0x7f090050 float:1.8210586E38 double:1.0530003007E-314;
        r17 = r17.findViewById(r18);
        r17 = (android.widget.TextView) r17;
        r0 = r5.userName;
        r18 = r0;
        r17.setText(r18);
        r17 = 1;
        r0 = r17;
        r1 = r22;
        r1.signedFromGroup = r0;
        r17 = 2131296705; // 0x7f0901c1 float:1.8211334E38 double:1.053000483E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 0;
        r17.setVisibility(r18);
        r22.updateFieldSize();
    L_0x058f:
        r0 = r5.createdBy;
        r17 = r0;
        if (r17 <= 0) goto L_0x05d6;
    L_0x0595:
        r0 = r22;
        r0 = r0.sigAttach;
        r17 = r0;
        if (r17 != 0) goto L_0x05d6;
    L_0x059d:
        r17 = 2131296705; // 0x7f0901c1 float:1.8211334E38 double:1.053000483E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 2131296336; // 0x7f090050 float:1.8210586E38 double:1.0530003007E-314;
        r17 = r17.findViewById(r18);
        r17 = (android.widget.TextView) r17;
        r18 = "...";
        r17.setText(r18);
        r15 = new java.util.ArrayList;
        r15.<init>();
        r0 = r5.createdBy;
        r17 = r0;
        r17 = java.lang.Integer.valueOf(r17);
        r0 = r17;
        r15.add(r0);
        r17 = new com.vkontakte.android.NewPostActivity$10;
        r0 = r17;
        r1 = r22;
        r0.<init>();
        r0 = r17;
        com.vkontakte.android.data.Friends.getUsers(r15, r0);
    L_0x05d6:
        r17 = r22.getSupportActionBar();
        r18 = 1;
        r17.setDisplayHomeAsUpEnabled(r18);
        r0 = r22;
        r0 = r0.uid;
        r17 = r0;
        if (r17 >= 0) goto L_0x05fe;
    L_0x05e7:
        r0 = r22;
        r0 = r0.uid;
        r17 = r0;
        r0 = r17;
        r0 = -r0;
        r17 = r0;
        r17 = com.vkontakte.android.data.Groups.getAdminLevel(r17);
        r18 = 2;
        r0 = r17;
        r1 = r18;
        if (r0 < r1) goto L_0x063c;
    L_0x05fe:
        r17 = r22.getIntent();
        r18 = "edit";
        r17 = r17.hasExtra(r18);
        if (r17 == 0) goto L_0x0626;
    L_0x060a:
        r0 = r22;
        r0 = r0.isSuggest;
        r17 = r0;
        if (r17 != 0) goto L_0x0626;
    L_0x0612:
        r17 = r22.getIntent();
        r18 = "edit";
        r17 = r17.getParcelableExtra(r18);
        r17 = (com.vkontakte.android.NewsEntry) r17;
        r18 = 2048; // 0x800 float:2.87E-42 double:1.0118E-320;
        r17 = r17.flag(r18);
        if (r17 == 0) goto L_0x063c;
    L_0x0626:
        r0 = r22;
        r0 = r0.uid;
        r17 = r0;
        if (r17 <= 0) goto L_0x0683;
    L_0x062e:
        r0 = r22;
        r0 = r0.uid;
        r17 = r0;
        r18 = com.vkontakte.android.Global.uid;
        r0 = r17;
        r1 = r18;
        if (r0 == r1) goto L_0x0683;
    L_0x063c:
        r17 = 2131296714; // 0x7f0901ca float:1.8211352E38 double:1.0530004875E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r17 = r17.getVisibility();
        if (r17 != 0) goto L_0x0683;
    L_0x064d:
        r17 = 2131296714; // 0x7f0901ca float:1.8211352E38 double:1.0530004875E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 0;
        r17.setEnabled(r18);
        r3 = new android.view.animation.AlphaAnimation;
        r17 = 1053609165; // 0x3ecccccd float:0.4 double:5.205520926E-315;
        r18 = 1053609165; // 0x3ecccccd float:0.4 double:5.205520926E-315;
        r0 = r17;
        r1 = r18;
        r3.<init>(r0, r1);
        r17 = 1;
        r0 = r17;
        r3.setFillAfter(r0);
        r17 = 2131296714; // 0x7f0901ca float:1.8211352E38 double:1.0530004875E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r0 = r17;
        r0.startAnimation(r3);
    L_0x0683:
        r22.updateSendButton();
        r0 = r22;
        r0 = r0.attachView;
        r17 = r0;
        r17 = r17.getCount();
        if (r17 != 0) goto L_0x069b;
    L_0x0692:
        r17 = 0;
        r0 = r22;
        r1 = r17;
        r0.showAttachView(r1);
    L_0x069b:
        r0 = r22;
        r0 = r0.attachView;
        r17 = r0;
        r18 = new com.vkontakte.android.NewPostActivity$11;
        r0 = r18;
        r1 = r22;
        r0.<init>();
        r19 = 100;
        r17.postDelayed(r18, r19);
        r0 = r22;
        r0 = r0.isSuggest;
        r17 = r0;
        if (r17 != 0) goto L_0x06ec;
    L_0x06b7:
        r0 = r22;
        r0 = r0.sigAttach;
        r17 = r0;
        if (r17 != 0) goto L_0x06ec;
    L_0x06bf:
        r17 = 2131296705; // 0x7f0901c1 float:1.8211334E38 double:1.053000483E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 2131296336; // 0x7f090050 float:1.8210586E38 double:1.0530003007E-314;
        r17 = r17.findViewById(r18);
        r17 = (android.widget.TextView) r17;
        r18 = 0;
        r19 = 0;
        r0 = r22;
        r1 = r18;
        r2 = r19;
        r18 = r0.getSharedPreferences(r1, r2);
        r19 = "username";
        r20 = "DELETED";
        r18 = r18.getString(r19, r20);
        r17.setText(r18);
    L_0x06ec:
        return;
    L_0x06ed:
        r17 = r22.getWindow();
        r18 = 2130838130; // 0x7f020272 float:1.7281234E38 double:1.052773917E-314;
        r17.setBackgroundDrawableResource(r18);
        goto L_0x0098;
    L_0x06f9:
        r17 = r22.getIntent();	 Catch:{ Exception -> 0x0711 }
        r18 = "link_title";
        r17 = r17.hasExtra(r18);	 Catch:{ Exception -> 0x0711 }
        if (r17 == 0) goto L_0x02b5;
    L_0x0705:
        r17 = r22.getIntent();	 Catch:{ Exception -> 0x0711 }
        r18 = "link_title";
        r14 = r17.getStringExtra(r18);	 Catch:{ Exception -> 0x0711 }
        goto L_0x02b5;
    L_0x0711:
        r16 = move-exception;
        r17 = "vk";
        r18 = "WTF you just did?!";
        com.vkontakte.android.Log.m530w(r17, r18);
        goto L_0x02e6;
    L_0x071b:
        r9 = r17.next();
        r9 = (java.lang.String) r9;
        r0 = r22;
        r0 = r0.attachView;
        r18 = r0;
        r19 = new com.vkontakte.android.ui.PendingPhotoAttachment;
        r0 = r19;
        r0.<init>(r9);
        r18.add(r19);
        goto L_0x033c;
    L_0x0733:
        r17 = 2131296709; // 0x7f0901c5 float:1.8211342E38 double:1.053000485E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 8;
        r17.setVisibility(r18);
        r17 = 2131296708; // 0x7f0901c4 float:1.821134E38 double:1.0530004845E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 8;
        r17.setVisibility(r18);
        r17 = 2131296707; // 0x7f0901c3 float:1.8211338E38 double:1.053000484E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 8;
        r17.setVisibility(r18);
        goto L_0x03b3;
    L_0x0765:
        r4 = r17.next();
        r4 = (com.vkontakte.android.Attachment) r4;
        r0 = r4 instanceof com.vkontakte.android.SignatureLinkAttachment;
        r18 = r0;
        if (r18 == 0) goto L_0x044a;
    L_0x0771:
        r17 = 1;
        r0 = r17;
        r1 = r22;
        r1.signedFromGroup = r0;
        r0 = r22;
        r0.sigAttach = r4;
        r0 = r5.attachments;
        r17 = r0;
        r0 = r17;
        r0.remove(r4);
        r17 = 2131296705; // 0x7f0901c1 float:1.8211334E38 double:1.053000483E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 2131296336; // 0x7f090050 float:1.8210586E38 double:1.0530003007E-314;
        r17 = r17.findViewById(r18);
        r17 = (android.widget.TextView) r17;
        r0 = r22;
        r0 = r0.sigAttach;
        r18 = r0;
        r18 = (com.vkontakte.android.LinkAttachment) r18;
        r0 = r18;
        r0 = r0.title;
        r18 = r0;
        r17.setText(r18);
        r17 = 2131296705; // 0x7f0901c1 float:1.8211334E38 double:1.053000483E-314;
        r0 = r22;
        r1 = r17;
        r17 = r0.findViewById(r1);
        r18 = 0;
        r17.setVisibility(r18);
        goto L_0x0450;
    L_0x07bd:
        r4 = r17.next();
        r4 = (com.vkontakte.android.Attachment) r4;
        r0 = r4 instanceof com.vkontakte.android.GeoAttachment;
        r18 = r0;
        if (r18 != 0) goto L_0x07d7;
    L_0x07c9:
        r0 = r22;
        r0 = r0.attachView;
        r18 = r0;
        r0 = r18;
        r0.add(r4);
        r12 = 1;
        goto L_0x0458;
    L_0x07d7:
        r4 = (com.vkontakte.android.GeoAttachment) r4;
        r0 = r22;
        r0.attachLocation(r4);
        goto L_0x0458;
    L_0x07e0:
        r17 = 0;
        goto L_0x04dc;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.NewPostActivity.onCreate(android.os.Bundle):void");
    }

    public void onConfigurationChanged(Configuration cfg) {
        super.onConfigurationChanged(cfg);
        if (VERSION.SDK_INT >= 14) {
            DialogFragment f = (DialogFragment) getFragmentManager().findFragmentByTag("datepicker");
            if (f != null) {
                f.dismiss();
                getFragmentManager().executePendingTransactions();
                f.show(getFragmentManager(), "datepicker");
            }
            f = (DialogFragment) getFragmentManager().findFragmentByTag("timepicker");
            if (f != null) {
                f.dismiss();
                getFragmentManager().executePendingTransactions();
                f.show(getFragmentManager(), "timepicker");
            }
        }
    }

    private void showAttachView(boolean show) {
        this.attachView.setVisibility(show ? 0 : POLL_RESULT);
        updateFieldSize();
    }

    private void updateFieldSize() {
        int i;
        View findViewById = findViewById(C0436R.id.status_text_edit);
        if (this.attachView.getVisibility() == 0 || this.signedFromGroup) {
            i = -2;
        } else {
            i = -1;
        }
        findViewById.setLayoutParams(new LayoutParams(-1, i));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add((int) C0436R.string.send);
        item.setActionView(this.sendBtn);
        item.setShowAsAction(NOTIFY_ID_PROGR);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            onBackPressed();
        }
        return true;
    }

    private boolean checkAttachLimit() {
        int i;
        int i2 = NOTIFY_ID_PROGR;
        int count = this.attachView.getCount();
        if (this.isComment) {
            i = NOTIFY_ID_PROGR;
        } else {
            i = 10;
        }
        if (count < i) {
            return true;
        }
        Resources resources = getResources();
        Object[] objArr = new Object[PHOTO_RESULT];
        if (!this.isComment) {
            i2 = 10;
        }
        objArr[0] = Integer.valueOf(i2);
        Toast.makeText(this, resources.getString(C0436R.string.attachments_limit, objArr), 0).show();
        return false;
    }

    private void updateExportIcons() {
        int i;
        int i2 = 0;
        View findViewById = findViewById(C0436R.id.newpost_fb_icon);
        if (this.exportToFacebook) {
            i = 0;
        } else {
            i = POLL_RESULT;
        }
        findViewById.setVisibility(i);
        findViewById = findViewById(C0436R.id.newpost_twitter_icon);
        if (this.exportToTwitter) {
            i = 0;
        } else {
            i = POLL_RESULT;
        }
        findViewById.setVisibility(i);
        View findViewById2 = findViewById(C0436R.id.newpost_friends_only);
        if (!this.friendsOnly) {
            i2 = POLL_RESULT;
        }
        findViewById2.setVisibility(i2);
    }

    private void showExtendedAttachMenu() {
        ArrayList<String> opts = new ArrayList();
        opts.add(getString(C0436R.string.audio));
        opts.add(getString(C0436R.string.video));
        opts.add(getString(C0436R.string.doc));
        if (this.uid == Global.uid || this.uid < 0) {
            boolean hasPoll = false;
            Iterator it = this.attachView.getAll().iterator();
            while (it.hasNext()) {
                if (((Attachment) it.next()) instanceof PollAttachment) {
                    hasPoll = true;
                    break;
                }
            }
            if (!hasPoll) {
                opts.add(getString(C0436R.string.attach_poll));
            }
        }
        new VKAlertDialog.Builder(this).setItems((CharSequence[]) opts.toArray(new String[0]), new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                        if (NewPostActivity.this.checkAttachLimit()) {
                            NewPostActivity.this.startAudioSelector();
                        }
                    case NewPostActivity.PHOTO_RESULT /*1*/:
                        if (NewPostActivity.this.checkAttachLimit()) {
                            NewPostActivity.this.startVideoSelector();
                        }
                    case NewPostActivity.NOTIFY_ID_PROGR /*2*/:
                        if (NewPostActivity.this.checkAttachLimit()) {
                            NewPostActivity.this.startDocumentSelector();
                        }
                    case NewPostActivity.LOCATION_RESULT /*3*/:
                        if (NewPostActivity.this.checkAttachLimit()) {
                            NewPostActivity.this.startPollSelector();
                        }
                    default:
                }
            }
        }).setTitle(C0436R.string.attach).show().setCanceledOnTouchOutside(true);
    }

    private void showTimerDlg() {
        DateTimePickerDialogFragment p = new DateTimePickerDialogFragment();
        if (this.postponeTo != null) {
            Bundle args = new Bundle();
            args.putLong("date", this.postponeTo.getTimeInMillis());
            p.setArguments(args);
        }
        p.setOnSelectedListener(new OnSelectedListener() {
            public void onDateSelected(Calendar date) {
                NewPostActivity.this.postponeTo = date;
                NewPostActivity.this.updateTimer();
            }
        });
        p.show(getSupportFragmentManager(), "picker");
    }

    private void updateTimer() {
        boolean z = true;
        if (this.optionsAlertView != null) {
            this.optionsAlertView.setItemChecked(this.optionsAlertView.getCount() - 1, this.postponeTo != null);
            boolean[] opts = (boolean[]) this.optionsAlertView.getTag();
            int count = this.optionsAlertView.getCount() - 1;
            if (this.postponeTo == null) {
                z = false;
            }
            opts[count] = z;
        }
        ((TextView) this.sendBtn.findViewById(C0436R.id.ab_done_text)).setText(this.postponeTo == null ? C0436R.string.send : C0436R.string.timer_post_btn);
        updateBottomLine();
    }

    private void startAudioSelector() {
        Bundle args = new Bundle();
        args.putBoolean("select", true);
        Intent intent = new Intent(this, FragmentWrapperActivity.class);
        intent.putExtra("class", "AudioListFragment");
        intent.putExtra("args", args);
        startActivityForResult(intent, AUDIO_RESULT);
    }

    private void startVideoSelector() {
        Bundle args = new Bundle();
        args.putBoolean("select", true);
        Intent intent = new Intent(this, FragmentWrapperActivity.class);
        intent.putExtra("class", "VideoListFragment");
        intent.putExtra("args", args);
        startActivityForResult(intent, VIDEO_RESULT);
    }

    private void startDocumentSelector() {
        Intent intent = new Intent(this, DocumentChooserActivity.class);
        intent.putExtra("limit", (this.isComment ? NOTIFY_ID_PROGR : 10) - this.attachView.getCount());
        startActivityForResult(intent, DOCUMENT_RESULT);
    }

    private void startPollSelector() {
        Intent intent = new Intent(this, PollEditorActivity.class);
        intent.putExtra("oid", this.uid);
        startActivityForResult(intent, POLL_RESULT);
    }

    public void startLocationChooser() {
        String locationProviders = Secure.getString(getContentResolver(), "location_providers_allowed");
        if (locationProviders == null || locationProviders.length() == 0) {
            new VKAlertDialog.Builder(this).setTitle(C0436R.string.location_disabled_title).setMessage(C0436R.string.location_disabled).setPositiveButton(C0436R.string.open_settings, new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    NewPostActivity.this.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
                }
            }).setNegativeButton(C0436R.string.cancel, null).show();
        } else {
            startActivityForResult(new Intent(this, this.uid == Global.uid ? CheckInActivity.class : SelectGeoPointActivity.class), LOCATION_RESULT);
        }
    }

    public void startMentionChooser() {
        Bundle args = new Bundle();
        args.putBoolean("select", true);
        Intent intent = new Intent(this, FragmentWrapperActivity.class);
        intent.putExtra("class", "FriendsFragment");
        intent.putExtra("args", args);
        startActivityForResult(intent, MENTION_RESULT);
    }

    public void showOptions() {
        boolean[] opts;
        boolean[] enabled;
        AlertDialog dlg;
        boolean z = true;
        ArrayList<String> items;
        ArrayList<String> acts;
        boolean z2;
        if (this.uid > 0) {
            items = new ArrayList();
            acts = new ArrayList();
            opts = new boolean[AUDIO_RESULT];
            opts[0] = this.friendsOnly;
            enabled = new boolean[]{true, true, true, true};
            int o = PHOTO_RESULT;
            items.add(getResources().getString(C0436R.string.newpost_friends_only));
            acts.add("friendsonly");
            if (getSharedPreferences(null, 0).getBoolean("export_twitter_avail", false)) {
                items.add(getResources().getString(C0436R.string.newpost_export_twitter));
                acts.add("twitter");
                opts[PHOTO_RESULT] = this.exportToTwitter;
                if (this.friendsOnly) {
                    enabled[PHOTO_RESULT] = false;
                }
                o = PHOTO_RESULT + PHOTO_RESULT;
            }
            if (getSharedPreferences(null, 0).getBoolean("export_facebook_avail", false)) {
                items.add(getResources().getString(C0436R.string.newpost_export_fb));
                acts.add("fb");
                opts[o] = this.exportToFacebook;
                if (this.friendsOnly) {
                    enabled[o] = false;
                }
                o += PHOTO_RESULT;
            }
            if ((!getIntent().hasExtra("edit") || ((NewsEntry) getIntent().getParcelableExtra("edit")).flag(NewsEntry.FLAG_POSTPONED)) && (this.uid == Global.uid || this.uid == 0 || (this.uid < 0 && this.fromGroup))) {
                items.add(getString(C0436R.string.timer));
                acts.add("timer");
                if (this.postponeTo != null) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                opts[o] = z2;
                o += PHOTO_RESULT;
            }
            dlg = new VKAlertDialog.Builder(this).setMultiChoiceItems((CharSequence[]) items.toArray(new String[0]), opts, new AnonymousClass15(acts, enabled, opts)).setPositiveButton(C0436R.string.ok, new AnonymousClass16(opts)).setTitle(C0436R.string.post_options).show();
            this.optionsAlertView = dlg.getListView();
            this.optionsAlertView.setTag(opts);
            dlg.setOnDismissListener(new OnDismissListener() {
                public void onDismiss(DialogInterface dialog) {
                    NewPostActivity.this.optionsAlertView = null;
                }
            });
        } else if (Groups.isGroupAdmin(-this.uid)) {
            opts = new boolean[LOCATION_RESULT];
            opts[0] = this.fromGroup;
            opts[PHOTO_RESULT] = this.signedFromGroup;
            if (this.postponeTo != null) {
                z2 = true;
            } else {
                z2 = false;
            }
            opts[NOTIFY_ID_PROGR] = z2;
            enabled = new boolean[LOCATION_RESULT];
            if (getIntent().hasExtra("public")) {
                z2 = false;
            } else {
                z2 = true;
            }
            enabled[0] = z2;
            z2 = getIntent().hasExtra("public") || this.fromGroup;
            enabled[PHOTO_RESULT] = z2;
            z2 = getIntent().hasExtra("public") || this.fromGroup;
            enabled[NOTIFY_ID_PROGR] = z2;
            items = new ArrayList();
            acts = new ArrayList();
            items.add(getString(C0436R.string.post_from_group));
            items.add(getString(C0436R.string.post_from_group_signed));
            acts.add("from_group");
            acts.add("signed");
            if ((!getIntent().hasExtra("edit") || ((NewsEntry) getIntent().getParcelableExtra("edit")).flag(NewsEntry.FLAG_POSTPONED) || this.isSuggest) && (this.uid == Global.uid || this.uid <= 0)) {
                items.add(getString(C0436R.string.timer));
                acts.add("timer");
                if (this.postponeTo == null) {
                    z = false;
                }
                opts[NOTIFY_ID_PROGR] = z;
            }
            dlg = new VKAlertDialog.Builder(this).setMultiChoiceItems((CharSequence[]) items.toArray(new String[0]), opts, new AnonymousClass18(enabled, opts)).setPositiveButton(C0436R.string.ok, new AnonymousClass19(opts)).setTitle(C0436R.string.post_options).show();
        } else {
            return;
        }
        dlg.getListView().setOnTouchListener(new AnonymousClass20(dlg, enabled));
        this.optionsAlertView = dlg.getListView();
        this.optionsAlertView.setTag(opts);
        dlg.setOnDismissListener(new OnDismissListener() {
            public void onDismiss(DialogInterface dialog) {
                NewPostActivity.this.optionsAlertView = null;
            }
        });
        dlg.getListView().setAdapter(new AnonymousClass22((ArrayAdapter) dlg.getListView().getAdapter(), enabled));
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == -1) {
            this.changed = true;
            if (requestCode == LOCATION_RESULT) {
                attachLocation((GeoAttachment) intent.getParcelableExtra("point"));
            } else {
                this.attachView.postDelayed(new AnonymousClass23(requestCode, intent), 300);
            }
        }
    }

    private void attachLocation(GeoAttachment att) {
        this.geoAttach = att;
        ((ImageView) findViewById(C0436R.id.newpost_btn_location)).setImageResource(C0436R.drawable.ic_newpost_location_active);
        updateBottomLine();
        updateSendButton();
    }

    private void removeLocation() {
        this.geoAttach = null;
        ((ImageView) findViewById(C0436R.id.newpost_btn_location)).setImageResource(C0436R.drawable.ic_newpost_location);
        updateBottomLine();
        updateSendButton();
        this.changed = true;
    }

    private void updateBottomLine() {
        ArrayList<String> lines = new ArrayList();
        if (this.postponeTo != null) {
            lines.add(getString(C0436R.string.timer_attach_text) + " " + Global.langDate(getResources(), (int) (this.postponeTo.getTimeInMillis() / 1000)));
        }
        if (this.geoAttach != null) {
            lines.add(this.geoAttach.title != null ? this.geoAttach.title : this.geoAttach.address);
        }
        if (lines.size() > 0) {
            findViewById(C0436R.id.newpost_location_address).setVisibility(0);
            ((TextView) findViewById(C0436R.id.newpost_location_address)).setText(TextUtils.join("\n", lines));
            return;
        }
        findViewById(C0436R.id.newpost_location_address).setVisibility(POLL_RESULT);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    private void updateSendButton() {
        boolean enable;
        if ((((TextView) findViewById(C0436R.id.status_text_edit)).getText().toString().trim().length() > 0 || this.attachView.getCount() > 0 || this.geoAttach != null) && !this.attachView.isUploading()) {
            enable = true;
        } else {
            enable = false;
        }
        this.sendBtn.setEnabled(enable);
        if (VERSION.SDK_INT >= 11) {
            this.sendBtn.findViewById(C0436R.id.ab_done_text).setAlpha(enable ? 1.0f : 0.5f);
            return;
        }
        ((TextView) this.sendBtn.findViewById(C0436R.id.ab_done_text)).getCompoundDrawables()[0].setAlpha(enable ? MotionEventCompat.ACTION_MASK : TransportMediator.FLAG_KEY_MEDIA_NEXT);
        ((TextView) this.sendBtn.findViewById(C0436R.id.ab_done_text)).setTextColor(enable ? -1 : -2130706433);
    }

    public void onBackPressed() {
        if (!(getIntent().hasExtra("edit") && this.changed) && (getIntent().hasExtra("edit") || (((TextView) findViewById(C0436R.id.status_text_edit)).length() <= 0 && this.attachView.getCount() <= 0 && this.geoAttach == null))) {
            super.onBackPressed();
        } else {
            new VKAlertDialog.Builder(this).setTitle(C0436R.string.confirm).setMessage(getIntent().hasExtra("edit") ? C0436R.string.confirm_close_post_edit : C0436R.string.confirm_close_post).setPositiveButton(getIntent().hasExtra("edit") ? C0436R.string.reg_continue : C0436R.string.delete, new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    super.onBackPressed();
                }
            }).setNegativeButton(C0436R.string.cancel, null).show();
        }
    }

    public void onResume() {
        super.onResume();
        this.sendBtn.postDelayed(new Runnable() {
            public void run() {
                ((InputMethodManager) NewPostActivity.this.getSystemService("input_method")).showSoftInput(NewPostActivity.this.findViewById(C0436R.id.status_text_edit), NewPostActivity.PHOTO_RESULT);
            }
        }, 200);
    }

    public void post() {
        if (!this.attachView.isUploading()) {
            NewsEntry edit;
            ArrayList<Attachment> atts = this.attachView.getAll();
            Iterator it = atts.iterator();
            while (it.hasNext()) {
                if (((Attachment) it.next()) instanceof PendingPhotoAttachment) {
                    return;
                }
            }
            String exportParam = null;
            if (this.exportToFacebook && this.exportToTwitter) {
                exportParam = "twitter,facebook";
            } else if (this.exportToTwitter) {
                exportParam = "twitter";
            } else if (this.exportToFacebook) {
                exportParam = "facebook";
            }
            String ptext = ((EditText) findViewById(C0436R.id.status_text_edit)).getText().toString().replaceAll("\\*((?:id|club)[0-9-]+) \\(([^\\)]+)\\)", "[$1|$2]");
            String str = (!getIntent().hasExtra("edit") || this.isSuggest) ? "wall.post" : this.isComment ? ACRAConstants.DEFAULT_STRING_VALUE : "wall.edit";
            APIRequest req = new APIRequest(str);
            req.param("device", Build.BRAND + ":" + Build.MANUFACTURER + ":" + Build.MODEL + ":" + Build.PRODUCT);
            req.param("owner_id", this.uid);
            if (this.postponeTo != null) {
                req.param("publish_date", (int) (this.postponeTo.getTimeInMillis() / 1000));
            }
            if (getIntent().hasExtra("edit")) {
                NewsEntry e = (NewsEntry) getIntent().getParcelableExtra("edit");
                req.param(this.isComment ? "comment_id" : "post_id", e.postID);
                if (this.isComment) {
                    req.param("method", e.retweetText + ".editComment");
                }
                edit = e;
            } else {
                edit = null;
            }
            if (this.fromGroup) {
                req.param("from_group", (int) PHOTO_RESULT);
            }
            if (this.signedFromGroup) {
                req.param("signed", (int) PHOTO_RESULT);
            } else if (this.fromGroup) {
                req.param("signed", "0");
            }
            if (this.geoAttach != null) {
                if (this.geoAttach.id <= 0) {
                    req.param("lat", new StringBuilder(String.valueOf(this.geoAttach.lat)).toString()).param("long", new StringBuilder(String.valueOf(this.geoAttach.lon)).toString());
                } else {
                    req.param("place_id", this.geoAttach.id);
                }
            }
            Attachment.sort(atts);
            if (atts.size() > 0) {
                req.param("attachments", TextUtils.join(",", atts));
            } else {
                req.param("attachments", " ");
            }
            req.param(LongPollService.EXTRA_MESSAGE, ptext);
            req.param("friends_only", this.friendsOnly ? "1" : null);
            req.param("services", exportParam).handler(new AnonymousClass26(ptext, edit, atts)).wrapProgress(this).exec((Activity) this);
        }
    }
}
