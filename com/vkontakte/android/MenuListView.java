package com.vkontakte.android;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.vkontakte.android.AudioPlayerService.AttachViewCallback;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Groups;
import com.vkontakte.android.data.Posts;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.ui.ListImageLoaderWrapper;
import com.vkontakte.android.ui.MultiSectionAdapter;
import com.vkontakte.android.ui.PaddingColorDrawable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.acra.ACRAConstants;

public class MenuListView extends FrameLayout {
    public static HashMap<String, Integer> counters;
    public static MenuListView lastInstance;
    public static Bundle reminderInfo;
    private static String reminderText;
    private MenuAdapter adapter;
    private AttachViewCallback audioCallback;
    private ArrayList<UserProfile> friends;
    private ArrayList<Group> groups;
    private int[] icons;
    private ListImageLoaderWrapper imgLoader;
    public int itemPadding;
    private List<String> items;
    public ListView list;
    private Listener listener;
    private Drawable playerShadow;
    private View playerView;
    private BroadcastReceiver receiver;
    private View searchBox;
    private boolean touchInPlayer;
    private String userName;
    private String userPhoto;

    /* renamed from: com.vkontakte.android.MenuListView.10 */
    class AnonymousClass10 implements Runnable {
        private final /* synthetic */ List val$_friends;
        private final /* synthetic */ List val$_groups;

        AnonymousClass10(List list, List list2) {
            this.val$_friends = list;
            this.val$_groups = list2;
        }

        public void run() {
            MenuListView.this.userName = MenuListView.this.getContext().getSharedPreferences(null, 0).getString("username", "DELETED");
            MenuListView.this.userPhoto = MenuListView.this.getContext().getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE);
            MenuListView.this.friends.clear();
            MenuListView.this.friends.addAll(this.val$_friends);
            MenuListView.this.groups.clear();
            MenuListView.this.groups.addAll(this.val$_groups);
            MenuListView.this.adapter.notifyDataSetChanged();
        }
    }

    /* renamed from: com.vkontakte.android.MenuListView.1 */
    class C03021 extends BroadcastReceiver {
        C03021() {
        }

        public void onReceive(Context context, Intent intent) {
            if (Friends.ACTION_FRIEND_LIST_CHANGED.equals(intent.getAction())) {
                MenuListView.this.friends.clear();
                Friends.getFriends(MenuListView.this.friends);
                if (MenuListView.this.friends.size() > 5) {
                    MenuListView.this.friends.subList(5, MenuListView.this.friends.size()).clear();
                }
                MenuListView.this.updateList();
            }
            if (Groups.ACTION_GROUP_LIST_CHANGED.equals(intent.getAction())) {
                MenuListView.this.groups.clear();
                Groups.getGroups(MenuListView.this.groups);
                if (MenuListView.this.groups.size() > 5) {
                    MenuListView.this.groups.subList(5, MenuListView.this.groups.size()).clear();
                }
                MenuListView.this.updateList();
            }
            if (LongPollService.ACTION_USER_PRESENCE.equals(intent.getAction())) {
                int uid = intent.getIntExtra("uid", 0);
                Iterator it = MenuListView.this.friends.iterator();
                while (it.hasNext()) {
                    UserProfile p = (UserProfile) it.next();
                    if (p.uid == uid) {
                        p.online = intent.getIntExtra("online", 0);
                        MenuListView.this.updateList();
                    }
                }
            }
            if (NetworkStateReceiver.ACTION_GROUPS_UPDATED.equals(intent.getAction())) {
                MenuListView.this.groups.clear();
                Groups.getGroups(MenuListView.this.groups);
                if (MenuListView.this.groups.size() > 5) {
                    MenuListView.this.groups.subList(5, MenuListView.this.groups.size()).clear();
                }
                MenuListView.this.updateList();
            }
            if (Posts.ACTION_USER_PHOTO_CHANGED.equals(intent.getAction())) {
                MenuListView.this.userPhoto = intent.getStringExtra("photo");
                MenuListView.this.updateList();
                MenuListView.this.imgLoader.updateImages();
            }
            if (Posts.ACTION_USER_NAME_CHANGED.equals(intent.getAction())) {
                MenuListView.this.userName = intent.getStringExtra("name");
                MenuListView.this.updateList();
                MenuListView.this.imgLoader.updateImages();
            }
            if (LongPollService.ACTION_COUNTERS_UPDATED.equals(intent.getAction())) {
                MenuListView.counters.clear();
                MenuListView.counters.put("friends", Integer.valueOf(LongPollService.numFriendRequests));
                MenuListView.counters.put("messages", Integer.valueOf(LongPollService.numNewMessages));
                MenuListView.counters.put("groups", Integer.valueOf(LongPollService.numGroupInvitations));
                MenuListView.counters.put("notifications", Integer.valueOf(LongPollService.numNotifications));
                MenuListView.this.updateList();
            }
            if (AudioPlayerService.ACTION_SERVICE_STOPPING.equals(intent.getAction())) {
                MenuListView.this.playerView.setVisibility(8);
                MenuListView.this.updateList();
            }
        }
    }

    /* renamed from: com.vkontakte.android.MenuListView.3 */
    class C03043 implements OnClickListener {
        C03043() {
        }

        public void onClick(View v) {
            MenuListView.this.getContext().startActivity(new Intent(MenuListView.this.getContext(), QuickSearchActivity.class));
        }
    }

    /* renamed from: com.vkontakte.android.MenuListView.4 */
    class C03054 implements OnItemClickListener {
        C03054() {
        }

        public void onItemClick(AdapterView<?> adapterView, View itemview, int item, long id) {
            if (id > 0) {
                if (MenuListView.this.listener != null) {
                    MenuListView.this.listener.onUserSelected((int) id, false);
                }
            } else if (id >= 0 || id <= -2000000000) {
                if (MenuListView.this.listener != null) {
                    item -= MenuListView.this.list.getHeaderViewsCount();
                    if (item > MenuListView.this.adapter.getItemCount(0)) {
                        if (MenuListView.this.playerView.getVisibility() == 0) {
                            item++;
                        }
                        item -= MenuListView.this.adapter.getCount();
                    }
                    MenuListView.this.listener.onMenuItemSelected(id == 0 ? item : (int) id, false);
                }
            } else if (MenuListView.this.listener != null) {
                MenuListView.this.listener.onCommunitySelected((int) (-id), false);
            }
        }
    }

    /* renamed from: com.vkontakte.android.MenuListView.5 */
    class C03065 implements OnItemLongClickListener {
        C03065() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View itemview, int item, long id) {
            if (id > 0) {
                if (MenuListView.this.listener != null) {
                    MenuListView.this.listener.onUserSelected((int) id, true);
                }
            } else if (id >= 0 || id <= -2000000000) {
                if (MenuListView.this.listener != null) {
                    item -= MenuListView.this.list.getHeaderViewsCount();
                    if (item > MenuListView.this.adapter.getItemCount(0)) {
                        if (MenuListView.this.playerView.getVisibility() == 0) {
                            item++;
                        }
                        item -= MenuListView.this.adapter.getCount();
                    }
                    if (item == 10) {
                        item = 11;
                    }
                    MenuListView.this.listener.onMenuItemSelected(id == 0 ? item : (int) id, true);
                }
            } else if (MenuListView.this.listener != null) {
                MenuListView.this.listener.onCommunitySelected((int) (-id), true);
            }
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.MenuListView.6 */
    class C03076 implements OnClickListener {
        C03076() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(MenuListView.this.getContext(), AudioPlayerService.class);
            intent.putExtra("action", 3);
            MenuListView.this.getContext().startService(intent);
        }
    }

    /* renamed from: com.vkontakte.android.MenuListView.7 */
    class C03087 implements OnClickListener {
        C03087() {
        }

        public void onClick(View v) {
            MenuListView.this.getContext().startActivity(new Intent(MenuListView.this.getContext(), AudioPlayerActivity.class));
            ((Activity) MenuListView.this.getContext()).overridePendingTransition(C0436R.anim.slide_in, C0436R.anim.noop);
            if (MenuListView.this.listener != null) {
                MenuListView.this.listener.onMenuItemSelected(-20, false);
            }
        }
    }

    /* renamed from: com.vkontakte.android.MenuListView.8 */
    class C03108 implements Runnable {

        /* renamed from: com.vkontakte.android.MenuListView.8.1 */
        class C03091 implements Runnable {
            private final /* synthetic */ Bundle val$info;
            private final /* synthetic */ String val$rtext;

            C03091(Bundle bundle, String str) {
                this.val$info = bundle;
                this.val$rtext = str;
            }

            public void run() {
                MenuListView.reminderInfo = this.val$info;
                MenuListView.reminderText = this.val$rtext;
                MenuListView.this.updateList();
            }
        }

        C03108() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r25 = this;
            r20 = java.lang.Thread.currentThread();
            r21 = "Update birthdays";
            r20.setName(r21);
            r4 = 0;
            r20 = java.lang.System.currentTimeMillis();
            r6 = com.vkontakte.android.cache.Cache.getBirthdays(r20);
            r3 = 0;
            r20 = r6.size();
            if (r20 <= 0) goto L_0x0105;
        L_0x0019:
            r20 = 1109393408; // 0x42200000 float:40.0 double:5.481131706E-315;
            r13 = com.vkontakte.android.Global.scale(r20);
            r11 = new java.util.ArrayList;
            r11.<init>();
            r17 = new java.util.ArrayList;
            r17.<init>();
            r7 = new java.util.Date;
            r20 = java.lang.System.currentTimeMillis();
            r0 = r20;
            r7.<init>(r0);
            r20 = new java.lang.StringBuilder;
            r21 = r7.getDate();
            r21 = java.lang.String.valueOf(r21);
            r20.<init>(r21);
            r21 = ".";
            r20 = r20.append(r21);
            r21 = r7.getMonth();
            r21 = r21 + 1;
            r20 = r20.append(r21);
            r21 = ".";
            r20 = r20.append(r21);
            r16 = r20.toString();
            r8 = new java.util.Date;
            r20 = java.lang.System.currentTimeMillis();
            r22 = 86400000; // 0x5265c00 float:7.82218E-36 double:4.2687272E-316;
            r20 = r20 + r22;
            r0 = r20;
            r8.<init>(r0);
            r20 = new java.lang.StringBuilder;
            r21 = r8.getDate();
            r21 = java.lang.String.valueOf(r21);
            r20.<init>(r21);
            r21 = ".";
            r20 = r20.append(r21);
            r21 = r8.getMonth();
            r21 = r21 + 1;
            r20 = r20.append(r21);
            r21 = ".";
            r20 = r20.append(r21);
            r19 = r20.toString();
            r9 = 0;
            r20 = r6.iterator();
        L_0x0097:
            r21 = r20.hasNext();
            if (r21 != 0) goto L_0x0120;
        L_0x009d:
            r0 = r25;
            r0 = com.vkontakte.android.MenuListView.this;
            r20 = r0;
            r20 = r20.getContext();
            r21 = r20.getResources();
            if (r9 == 0) goto L_0x0252;
        L_0x00ad:
            r20 = 2131231071; // 0x7f08015f float:1.8078213E38 double:1.0529680555E-314;
        L_0x00b0:
            r22 = 1;
            r0 = r22;
            r0 = new java.lang.Object[r0];
            r22 = r0;
            r23 = 0;
            r24 = ", ";
            r0 = r24;
            r24 = android.text.TextUtils.join(r0, r11);
            r22[r23] = r24;
            r0 = r21;
            r1 = r20;
            r2 = r22;
            r4 = r0.getString(r1, r2);
            r3 = new android.os.Bundle;
            r3.<init>();
            r20 = r17.size();
            r21 = 1;
            r0 = r20;
            r1 = r21;
            if (r0 != r1) goto L_0x0257;
        L_0x00df:
            r20 = "_class";
            r21 = "ProfileFragment";
            r0 = r20;
            r1 = r21;
            r3.putCharSequence(r0, r1);
            r21 = "id";
            r20 = 0;
            r0 = r17;
            r1 = r20;
            r20 = r0.get(r1);
            r20 = (com.vkontakte.android.UserProfile) r20;
            r0 = r20;
            r0 = r0.uid;
            r20 = r0;
            r0 = r21;
            r1 = r20;
            r3.putInt(r0, r1);
        L_0x0105:
            r10 = r3;
            r14 = r4;
            r0 = r25;
            r0 = com.vkontakte.android.MenuListView.this;
            r20 = r0;
            r20 = r20.getContext();
            r20 = (android.app.Activity) r20;
            r21 = new com.vkontakte.android.MenuListView$8$1;
            r0 = r21;
            r1 = r25;
            r0.<init>(r10, r14);
            r20.runOnUiThread(r21);
            return;
        L_0x0120:
            r12 = r20.next();
            r12 = (com.vkontakte.android.UserProfile) r12;
            r0 = r12.bdate;
            r21 = r0;
            r0 = r21;
            r1 = r16;
            r21 = r0.startsWith(r1);
            if (r21 == 0) goto L_0x0135;
        L_0x0134:
            r9 = 1;
        L_0x0135:
            if (r9 == 0) goto L_0x0145;
        L_0x0137:
            r0 = r12.bdate;
            r21 = r0;
            r0 = r21;
            r1 = r16;
            r21 = r0.startsWith(r1);
            if (r21 != 0) goto L_0x0147;
        L_0x0145:
            if (r9 != 0) goto L_0x0150;
        L_0x0147:
            r0 = r12.university;
            r21 = r0;
            r0 = r21;
            r11.add(r0);
        L_0x0150:
            r0 = r12.bdate;
            r21 = r0;
            r22 = "\\.";
            r15 = r21.split(r22);
            r0 = r15.length;
            r21 = r0;
            r22 = 2;
            r0 = r21;
            r1 = r22;
            if (r0 <= r1) goto L_0x01e5;
        L_0x0165:
            r21 = 2;
            r21 = r15[r21];
            r18 = java.lang.Integer.parseInt(r21);
        L_0x016d:
            r0 = r12.bdate;
            r21 = r0;
            r0 = r21;
            r1 = r16;
            r21 = r0.startsWith(r1);
            if (r21 == 0) goto L_0x01e8;
        L_0x017b:
            r0 = r25;
            r0 = com.vkontakte.android.MenuListView.this;
            r21 = r0;
            r21 = r21.getContext();
            r21 = r21.getResources();
            r22 = 2131230775; // 0x7f080037 float:1.8077612E38 double:1.0529679093E-314;
            r21 = r21.getString(r22);
            r0 = r21;
            r12.firstName = r0;
        L_0x0194:
            if (r18 <= 0) goto L_0x01de;
        L_0x0196:
            r21 = r7.getYear();
            r0 = r21;
            r0 = r0 + 1900;
            r21 = r0;
            r5 = r21 - r18;
            r0 = r12.firstName;
            r21 = r0;
            r22 = new java.lang.StringBuilder;
            r21 = java.lang.String.valueOf(r21);
            r0 = r22;
            r1 = r21;
            r0.<init>(r1);
            r21 = ", ";
            r0 = r22;
            r1 = r21;
            r21 = r0.append(r1);
            r22 = 2131558456; // 0x7f0d0038 float:1.8742228E38 double:1.053129805E-314;
            r0 = r25;
            r0 = com.vkontakte.android.MenuListView.this;
            r23 = r0;
            r23 = r23.getResources();
            r0 = r22;
            r1 = r23;
            r22 = com.vkontakte.android.Global.langPlural(r0, r5, r1);
            r21 = r21.append(r22);
            r21 = r21.toString();
            r0 = r21;
            r12.firstName = r0;
        L_0x01de:
            r0 = r17;
            r0.add(r12);
            goto L_0x0097;
        L_0x01e5:
            r18 = 0;
            goto L_0x016d;
        L_0x01e8:
            r0 = r12.bdate;
            r21 = r0;
            r0 = r21;
            r1 = r19;
            r21 = r0.startsWith(r1);
            if (r21 == 0) goto L_0x0210;
        L_0x01f6:
            r0 = r25;
            r0 = com.vkontakte.android.MenuListView.this;
            r21 = r0;
            r21 = r21.getContext();
            r21 = r21.getResources();
            r22 = 2131231075; // 0x7f080163 float:1.807822E38 double:1.0529680575E-314;
            r21 = r21.getString(r22);
            r0 = r21;
            r12.firstName = r0;
            goto L_0x0194;
        L_0x0210:
            r21 = new java.lang.StringBuilder;
            r22 = 0;
            r22 = r15[r22];
            r22 = java.lang.String.valueOf(r22);
            r21.<init>(r22);
            r22 = " ";
            r21 = r21.append(r22);
            r0 = r25;
            r0 = com.vkontakte.android.MenuListView.this;
            r22 = r0;
            r22 = r22.getContext();
            r22 = r22.getResources();
            r23 = 2131558401; // 0x7f0d0001 float:1.8742117E38 double:1.053129778E-314;
            r22 = r22.getStringArray(r23);
            r23 = 1;
            r23 = r15[r23];
            r23 = java.lang.Integer.parseInt(r23);
            r23 = r23 + -1;
            r22 = r22[r23];
            r21 = r21.append(r22);
            r21 = r21.toString();
            r0 = r21;
            r12.firstName = r0;
            goto L_0x0194;
        L_0x0252:
            r20 = 2131231072; // 0x7f080160 float:1.8078215E38 double:1.052968056E-314;
            goto L_0x00b0;
        L_0x0257:
            r20 = "_class";
            r21 = "UserListFragment";
            r0 = r20;
            r1 = r21;
            r3.putCharSequence(r0, r1);
            r20 = "type";
            r21 = 3;
            r0 = r20;
            r1 = r21;
            r3.putInt(r0, r1);
            r20 = "users";
            r0 = r20;
            r1 = r17;
            r3.putParcelableArrayList(r0, r1);
            r20 = "extended";
            r21 = 1;
            r0 = r20;
            r1 = r21;
            r3.putBoolean(r0, r1);
            r20 = "title";
            r0 = r25;
            r0 = com.vkontakte.android.MenuListView.this;
            r21 = r0;
            r21 = r21.getContext();
            r21 = r21.getResources();
            r22 = 2131231074; // 0x7f080162 float:1.8078219E38 double:1.052968057E-314;
            r21 = r21.getString(r22);
            r0 = r20;
            r1 = r21;
            r3.putCharSequence(r0, r1);
            goto L_0x0105;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.MenuListView.8.run():void");
        }
    }

    /* renamed from: com.vkontakte.android.MenuListView.9 */
    class C03119 implements Runnable {
        C03119() {
        }

        public void run() {
            try {
                ArrayList<UserProfile> friends = new ArrayList();
                Friends.getFriends(friends);
                if (friends.size() > 5) {
                    friends.subList(5, friends.size()).clear();
                }
                ArrayList<Group> groups = new ArrayList();
                Groups.getGroups(groups);
                if (groups.size() > 5) {
                    groups.subList(5, groups.size()).clear();
                }
                MenuListView.this.update(friends, groups);
            } catch (Exception e) {
            }
        }
    }

    public interface Listener {
        void onCommunitySelected(int i, boolean z);

        void onMenuItemSelected(int i, boolean z);

        void onUserSelected(int i, boolean z);
    }

    /* renamed from: com.vkontakte.android.MenuListView.2 */
    class C13112 implements AttachViewCallback {

        /* renamed from: com.vkontakte.android.MenuListView.2.1 */
        class C03031 implements Runnable {
            private final /* synthetic */ int val$state;

            C03031(int i) {
                this.val$state = i;
            }

            public void run() {
                if (MenuListView.this.playerView.getVisibility() != 0) {
                    MenuListView.this.playerView.setVisibility(0);
                    MenuListView.this.updateList();
                }
                if (AudioPlayerService.sharedInstance != null) {
                    AudioFile af = AudioPlayerService.sharedInstance.getCurrentFile();
                    if (af != null) {
                        ((TextView) MenuListView.this.playerView.findViewById(C0436R.id.player_view_artist)).setText(af.artist);
                        ((TextView) MenuListView.this.playerView.findViewById(C0436R.id.player_view_title)).setText(af.title);
                    }
                    ((ImageView) MenuListView.this.playerView.findViewById(C0436R.id.player_view_button)).setImageResource(this.val$state == 1 ? C0436R.drawable.ic_audio_panel_pause : C0436R.drawable.ic_audio_panel_play);
                    MenuListView.this.postInvalidate();
                }
            }
        }

        C13112() {
        }

        public void onPlayStateChanged(int oid, int aid, int state) {
            ((Activity) MenuListView.this.getContext()).runOnUiThread(new C03031(state));
        }
    }

    private class MenuAdapter extends MultiSectionAdapter {

        /* renamed from: com.vkontakte.android.MenuListView.MenuAdapter.1 */
        class C03121 implements OnClickListener {
            C03121() {
            }

            public void onClick(View v) {
                MenuListView.this.getContext().startActivity(new Intent(MenuListView.this.getContext(), PostPhotoActivity.class));
            }
        }

        /* renamed from: com.vkontakte.android.MenuListView.MenuAdapter.2 */
        class C03132 extends TextView {
            C03132(Context $anonymous0) {
                super($anonymous0);
            }

            public int getSuggestedMinimumHeight() {
                return 1;
            }
        }

        private MenuAdapter() {
        }

        public View getView(int section, int item, View view) {
            if (section == 0) {
                if (item == 0) {
                    if (view == null) {
                        view = MenuListView.inflate(MenuListView.this.getContext(), C0436R.layout.left_menu_item_me, null);
                        view.setLayoutParams(new LayoutParams(-1, Global.scale(55.0f)));
                        view.setBackgroundResource(C0436R.drawable.highlight_left_menu_first);
                        ((ImageView) view.findViewById(C0436R.id.flist_item_online)).setImageResource(C0436R.drawable.ic_left_post_photo);
                        view.findViewById(C0436R.id.flist_item_online).setOnClickListener(new C03121());
                    }
                    TextPaint tp = new TextPaint();
                    tp.setTypeface(Typeface.DEFAULT);
                    tp.setTextSize((float) Global.scale(GalleryPickerFooterView.BADGE_SIZE));
                    Rect bnds = new Rect();
                    tp.getTextBounds(MenuListView.this.userName, 0, MenuListView.this.userName.length(), bnds);
                    if (bnds.width() <= MenuListView.this.list.getWidth() - Global.scale(BitmapDescriptorFactory.HUE_GREEN)) {
                        ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(MenuListView.this.userName);
                    } else {
                        ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(MenuListView.this.userName.split(" ")[0]);
                    }
                    if (MenuListView.this.imgLoader.isAlreadyLoaded(MenuListView.this.userPhoto)) {
                        ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageBitmap(MenuListView.this.imgLoader.get(MenuListView.this.userPhoto));
                    } else {
                        ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(C0436R.drawable.user_placeholder_dark);
                    }
                } else {
                    if (view == null) {
                        view = MenuListView.inflate(MenuListView.this.getContext(), C0436R.layout.left_menu_item, null);
                        view.setLayoutParams(new LayoutParams(-1, Global.scale(47.0f)));
                    }
                    view.setBackgroundResource(item == 0 ? C0436R.drawable.highlight_left_menu_first : C0436R.drawable.highlight_left_menu);
                    ((TextView) view.findViewById(C0436R.id.leftmenu_text)).setText((CharSequence) MenuListView.this.items.get(item));
                    int counter = 0;
                    String key = null;
                    switch (item) {
                        case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                            key = "notifications";
                            break;
                        case Group.ADMIN_LEVEL_ADMIN /*3*/:
                            key = "messages";
                            break;
                        case UserListView.TYPE_FAVE /*4*/:
                            key = "friends";
                            break;
                        case UserListView.TYPE_FOLLOWERS /*5*/:
                            key = "groups";
                            break;
                    }
                    if (key != null) {
                        try {
                            if (MenuListView.counters.containsKey(key)) {
                                counter = ((Integer) MenuListView.counters.get(key)).intValue();
                            }
                        } catch (Exception e) {
                        }
                    }
                    if (counter == 0) {
                        try {
                            view.findViewById(C0436R.id.leftmenu_counter).setVisibility(8);
                        } catch (Exception e2) {
                        }
                    } else {
                        view.findViewById(C0436R.id.leftmenu_counter).setVisibility(0);
                        if (counter > LocationStatusCodes.GEOFENCE_NOT_AVAILABLE) {
                            ((TextView) view.findViewById(C0436R.id.leftmenu_counter)).setText((counter / LocationStatusCodes.GEOFENCE_NOT_AVAILABLE) + "K");
                        } else {
                            ((TextView) view.findViewById(C0436R.id.leftmenu_counter)).setText(new StringBuilder(String.valueOf(counter)).toString());
                        }
                    }
                    if (item != 0) {
                        ((ImageView) view.findViewById(C0436R.id.leftmenu_icon)).setImageResource(MenuListView.this.icons[item]);
                    }
                }
                return view;
            } else if (section == 1) {
                if (view == null) {
                    view = new C03132(MenuListView.this.getContext());
                    view.setLayoutParams(new LayoutParams(-1, -2));
                    view.setBackgroundResource(C0436R.drawable.highlight_left_menu_first);
                }
                TextView v = (TextView) view;
                v.setGravity(1);
                v.setTextColor(-1);
                v.setPadding(Global.scale(5.0f), Global.scale(7.0f), Global.scale(5.0f), Global.scale(7.0f));
                v.setText(MenuListView.reminderText);
                return v;
            } else if (section == 2) {
                if (view == null) {
                    view = MenuListView.inflate(MenuListView.this.getContext(), C0436R.layout.left_menu_item2, null);
                    view.setLayoutParams(new LayoutParams(-1, Global.scale(47.0f)));
                }
                view.setBackgroundResource(item == 0 ? C0436R.drawable.highlight_left_menu_first : C0436R.drawable.highlight_left_menu);
                UserProfile p = (UserProfile) MenuListView.this.friends.get(item);
                ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(p.fullName);
                view.findViewById(C0436R.id.flist_item_online).setVisibility(p.online > 0 ? 0 : 8);
                ((ImageView) view.findViewById(C0436R.id.flist_item_online)).setImageResource(p.online == 1 ? C0436R.drawable.ic_left_online : C0436R.drawable.ic_left_online_mobile);
                if (MenuListView.this.imgLoader.isAlreadyLoaded(p.photo)) {
                    ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageBitmap(MenuListView.this.imgLoader.get(p.photo));
                } else {
                    ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(C0436R.drawable.user_placeholder_dark);
                }
                return view;
            } else if (section == 3) {
                if (view == null) {
                    view = MenuListView.inflate(MenuListView.this.getContext(), C0436R.layout.left_menu_item2, null);
                    view.setLayoutParams(new LayoutParams(-1, Global.scale(47.0f)));
                }
                view.setBackgroundResource(item == 0 ? C0436R.drawable.highlight_left_menu_first : C0436R.drawable.highlight_left_menu);
                Group g = (Group) MenuListView.this.groups.get(item);
                ((TextView) view.findViewById(C0436R.id.flist_item_text)).setText(g.name);
                view.findViewById(C0436R.id.flist_item_online).setVisibility(8);
                if (MenuListView.this.imgLoader.isAlreadyLoaded(g.photo)) {
                    ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageBitmap(MenuListView.this.imgLoader.get(g.photo));
                } else {
                    ((ImageView) view.findViewById(C0436R.id.flist_item_photo)).setImageResource(C0436R.drawable.group_placeholder_dark);
                }
                return view;
            } else if (section != 4) {
                return null;
            } else {
                if (view == null) {
                    view = new View(MenuListView.this.getContext());
                    view.setLayoutParams(new LayoutParams(-1, Global.scale(57.0f)));
                }
                return view;
            }
        }

        public String getSectionTitle(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return ACRAConstants.DEFAULT_STRING_VALUE;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return MenuListView.this.getResources().getString(C0436R.string.reminder);
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return MenuListView.this.getResources().getString(C0436R.string.friends);
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return MenuListView.this.getResources().getString(C0436R.string.groups);
                default:
                    return ACRAConstants.DEFAULT_STRING_VALUE;
            }
        }

        public int getSectionCount() {
            return 5;
        }

        public int getItemCount(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return MenuListView.this.items.size();
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    if (MenuListView.reminderText == null) {
                        return 0;
                    }
                    return 1;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return Math.min(5, MenuListView.this.friends.size());
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return Math.min(5, MenuListView.this.groups.size());
                case UserListView.TYPE_FAVE /*4*/:
                    return MenuListView.this.playerView.getVisibility() != 0 ? 0 : 1;
                default:
                    return 0;
            }
        }

        public long getItemId(int section, int item) {
            switch (section) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    return -2000000000;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    try {
                        return (long) ((UserProfile) MenuListView.this.friends.get(item)).uid;
                    } catch (ArrayIndexOutOfBoundsException e) {
                        break;
                    }
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return (long) (-((Group) MenuListView.this.groups.get(item)).id);
            }
            return 0;
        }

        public boolean isSectionHeaderVisible(int section) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                case UserListView.TYPE_FAVE /*4*/:
                    return false;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    if (MenuListView.reminderText != null) {
                        return true;
                    }
                    return false;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    if (MenuListView.this.friends.size() > 0) {
                        return true;
                    }
                    return false;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return MenuListView.this.groups.size() > 0;
                default:
                    if (section != 0) {
                        return true;
                    }
                    return false;
            }
        }

        public int getExtraViewTypeCount() {
            return 5;
        }

        public int getItemViewType(int section, int item) {
            if (section == 0) {
                if (item == 0) {
                    return 2;
                }
                return 0;
            } else if (section == 4) {
                return 4;
            } else {
                if (section == 1) {
                    return 5;
                }
                if (section == 5) {
                    return 6;
                }
                return 3;
            }
        }

        public int getHeaderLayoutResource() {
            return C0436R.layout.list_menu_section_header;
        }
    }

    private class MenuImagesAdapter extends MultiSectionImageLoaderAdapter {

        /* renamed from: com.vkontakte.android.MenuListView.MenuImagesAdapter.1 */
        class C03141 implements Runnable {
            private final /* synthetic */ Bitmap val$bitmap;
            private final /* synthetic */ int val$item;

            C03141(int i, Bitmap bitmap) {
                this.val$item = i;
                this.val$bitmap = bitmap;
            }

            public void run() {
                View v = MenuListView.this.list.getChildAt(this.val$item - MenuListView.this.list.getFirstVisiblePosition());
                if (v != null) {
                    View vv = v.findViewById(C0436R.id.flist_item_photo);
                    if (vv != null) {
                        ((ImageView) vv).setImageBitmap(this.val$bitmap);
                    }
                }
            }
        }

        private MenuImagesAdapter() {
        }

        public int getSectionCount() {
            return MenuListView.this.adapter.getSectionCount();
        }

        public int getItemCount(int section) {
            return MenuListView.this.adapter.getItemCount(section);
        }

        public boolean isSectionHeaderVisible(int section) {
            return MenuListView.this.adapter.isSectionHeaderVisible(section);
        }

        public int getImageCountForItem(int section, int item) {
            if (section == 2 || section == 3) {
                return 1;
            }
            if (section == 0 && item == 0) {
                return 1;
            }
            return 0;
        }

        public String getImageURL(int section, int item, int image) {
            switch (section) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    return MenuListView.this.userPhoto;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return ((UserProfile) MenuListView.this.friends.get(item)).photo;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return ((Group) MenuListView.this.groups.get(item)).photo;
                default:
                    return null;
            }
        }

        public void imageLoaded(int _item, int image, Bitmap bitmap) {
            if (_item >= 0) {
                MenuListView.this.post(new C03141(_item + MenuListView.this.list.getHeaderViewsCount(), bitmap));
            }
        }
    }

    static {
        counters = new HashMap();
        reminderText = null;
        reminderInfo = new Bundle();
    }

    public MenuListView(Context context) {
        super(context);
        this.groups = new ArrayList();
        this.friends = new ArrayList();
        this.touchInPlayer = false;
        int[] iArr = new int[12];
        iArr[1] = C0436R.drawable.ic_left_news;
        iArr[2] = C0436R.drawable.ic_left_feedback;
        iArr[3] = C0436R.drawable.ic_left_messages;
        iArr[4] = C0436R.drawable.ic_left_friends;
        iArr[5] = C0436R.drawable.ic_left_groups;
        iArr[6] = C0436R.drawable.ic_left_photos;
        iArr[7] = C0436R.drawable.ic_left_video;
        iArr[8] = C0436R.drawable.ic_left_music;
        iArr[9] = C0436R.drawable.ic_left_fave;
        iArr[10] = C0436R.drawable.ic_left_search;
        iArr[11] = C0436R.drawable.ic_left_settings;
        this.icons = iArr;
        this.receiver = new C03021();
        this.audioCallback = new C13112();
        init();
    }

    public MenuListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.groups = new ArrayList();
        this.friends = new ArrayList();
        this.touchInPlayer = false;
        int[] iArr = new int[12];
        iArr[1] = C0436R.drawable.ic_left_news;
        iArr[2] = C0436R.drawable.ic_left_feedback;
        iArr[3] = C0436R.drawable.ic_left_messages;
        iArr[4] = C0436R.drawable.ic_left_friends;
        iArr[5] = C0436R.drawable.ic_left_groups;
        iArr[6] = C0436R.drawable.ic_left_photos;
        iArr[7] = C0436R.drawable.ic_left_video;
        iArr[8] = C0436R.drawable.ic_left_music;
        iArr[9] = C0436R.drawable.ic_left_fave;
        iArr[10] = C0436R.drawable.ic_left_search;
        iArr[11] = C0436R.drawable.ic_left_settings;
        this.icons = iArr;
        this.receiver = new C03021();
        this.audioCallback = new C13112();
        init();
    }

    public MenuListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.groups = new ArrayList();
        this.friends = new ArrayList();
        this.touchInPlayer = false;
        int[] iArr = new int[12];
        iArr[1] = C0436R.drawable.ic_left_news;
        iArr[2] = C0436R.drawable.ic_left_feedback;
        iArr[3] = C0436R.drawable.ic_left_messages;
        iArr[4] = C0436R.drawable.ic_left_friends;
        iArr[5] = C0436R.drawable.ic_left_groups;
        iArr[6] = C0436R.drawable.ic_left_photos;
        iArr[7] = C0436R.drawable.ic_left_video;
        iArr[8] = C0436R.drawable.ic_left_music;
        iArr[9] = C0436R.drawable.ic_left_fave;
        iArr[10] = C0436R.drawable.ic_left_search;
        iArr[11] = C0436R.drawable.ic_left_settings;
        this.icons = iArr;
        this.receiver = new C03021();
        this.audioCallback = new C13112();
        init();
    }

    private void init() {
        this.items = Arrays.asList(getResources().getStringArray(C0436R.array.leftmenu));
        this.playerView = inflate(getContext(), C0436R.layout.menu_audio_player, null);
        this.searchBox = View.inflate(getContext(), C0436R.layout.left_search, null);
        this.list = new ListView(getContext());
        this.searchBox.findViewById(C0436R.id.left_quick_search_btn).setOnClickListener(new C03043());
        this.list.addHeaderView(this.searchBox, null, false);
        ListView listView = this.list;
        ListAdapter menuAdapter = new MenuAdapter();
        this.adapter = menuAdapter;
        listView.setAdapter(menuAdapter);
        this.list.setCacheColorHint(-14473165);
        this.list.setBackgroundColor(-14473165);
        this.list.setDivider(new PaddingColorDrawable(1056964608, Global.scale(6.0f)));
        this.list.setDividerHeight(Global.scale(1.0f));
        this.list.setVerticalFadingEdgeEnabled(false);
        this.list.setSelector(new ColorDrawable(0));
        this.list.setHeaderDividersEnabled(false);
        this.list.setVerticalScrollBarEnabled(false);
        this.list.setOnItemClickListener(new C03054());
        this.list.setOnItemLongClickListener(new C03065());
        this.imgLoader = new ListImageLoaderWrapper(new MenuImagesAdapter(), this.list, null);
        this.userName = getContext().getSharedPreferences(null, 0).getString("username", "DELETED");
        this.userPhoto = getContext().getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE);
        updateBirthdays();
        this.playerView.setVisibility(8);
        this.playerView.findViewById(C0436R.id.player_view_button).setOnClickListener(new C03076());
        this.playerView.findViewById(C0436R.id.player_view_content).setOnClickListener(new C03087());
        this.list.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.playerView.setLayoutParams(new FrameLayout.LayoutParams(-1, Global.scale(58.0f), 80));
        addView(this.list);
        addView(this.playerView);
        this.playerShadow = getResources().getDrawable(C0436R.drawable.mini_player_shadow);
        this.playerShadow.setAlpha(100);
    }

    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.playerView.getVisibility() == 0) {
            this.playerShadow.setBounds(0, (getHeight() - this.playerView.getHeight()) - (this.playerShadow.getIntrinsicHeight() / 2), getWidth(), getHeight() - this.playerView.getHeight());
            this.playerShadow.draw(canvas);
        }
    }

    public void updateUserInfo() {
        this.userName = getContext().getSharedPreferences(null, 0).getString("username", "DELETED");
        this.userPhoto = getContext().getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE);
        updateList();
    }

    public void updateBirthdays() {
        new Thread(new C03108()).start();
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        new Thread(new C03119()).start();
        lastInstance = this;
        IntentFilter filter = new IntentFilter();
        filter.addAction(Friends.ACTION_FRIEND_LIST_CHANGED);
        filter.addAction(Groups.ACTION_GROUP_LIST_CHANGED);
        filter.addAction(LongPollService.ACTION_USER_PRESENCE);
        filter.addAction(NetworkStateReceiver.ACTION_GROUPS_UPDATED);
        filter.addAction(LongPollService.ACTION_COUNTERS_UPDATED);
        filter.addAction(Posts.ACTION_USER_PHOTO_CHANGED);
        filter.addAction(Posts.ACTION_USER_NAME_CHANGED);
        filter.addAction(AudioPlayerService.ACTION_SERVICE_STOPPING);
        getContext().registerReceiver(this.receiver, filter);
        AudioPlayerService.addAttachViewCallback(this.audioCallback);
        if (!(AudioPlayerService.sharedInstance == null || AudioPlayerService.sharedInstance.getCurrentFile() == null)) {
            this.audioCallback.onPlayStateChanged(AudioPlayerService.sharedInstance.getOid(), AudioPlayerService.sharedInstance.getAid(), AudioPlayerService.sharedInstance.isPlaying() ? 1 : 2);
        }
        this.imgLoader.activate();
        this.imgLoader.updateImages();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Log.m528i("vk", "on detached");
        lastInstance = null;
        try {
            getContext().unregisterReceiver(this.receiver);
        } catch (Exception e) {
        }
        AudioPlayerService.removeAttachViewCallback(this.audioCallback);
    }

    public void update(List<UserProfile> _friends, List<Group> _groups) {
        ((Activity) getContext()).runOnUiThread(new AnonymousClass10(_friends, _groups));
    }

    public void updateList() {
        ((Activity) getContext()).runOnUiThread(new Runnable() {
            public void run() {
                MenuListView.this.adapter.notifyDataSetChanged();
                MenuListView.this.imgLoader.updateImages();
            }
        });
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
