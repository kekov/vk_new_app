package com.vkontakte.android;

import com.google.android.gms.plus.PlusShare;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class GeoPlace {
    public String address;
    public int checkins;
    public int distance;
    public int groupID;
    public int id;
    public double lat;
    public double lon;
    public String photo;
    public String title;

    public GeoPlace() {
        this.id = 0;
        this.title = ACRAConstants.DEFAULT_STRING_VALUE;
        this.photo = ACRAConstants.DEFAULT_STRING_VALUE;
        this.address = ACRAConstants.DEFAULT_STRING_VALUE;
    }

    public GeoPlace(JSONObject o) {
        this.id = 0;
        this.title = ACRAConstants.DEFAULT_STRING_VALUE;
        this.photo = ACRAConstants.DEFAULT_STRING_VALUE;
        this.address = ACRAConstants.DEFAULT_STRING_VALUE;
        try {
            this.id = o.getInt("id");
            this.title = o.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            this.lat = o.getDouble("latitude");
            this.lon = o.getDouble("longitude");
            this.checkins = o.optInt("checkins");
            this.photo = o.optString("group_photo");
            this.groupID = o.optInt("group_id");
            this.distance = o.optInt("distance");
            this.address = o.optString("address", ACRAConstants.DEFAULT_STRING_VALUE);
        } catch (Exception x) {
            Log.m531w("vk", "Error parsing GeoPlace " + o, x);
        }
    }
}
