package com.vkontakte.android;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.widget.Toast;
import com.android.vending.billing.IInAppBillingService;
import com.android.vending.billing.IInAppBillingService.Stub;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.api.StorePurchase;
import com.vkontakte.android.api.StorePurchase.Callback;
import com.vkontakte.android.data.StickerPack;
import com.vkontakte.android.data.Stickers;
import java.util.ArrayList;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class BillingActivity extends Activity {
    private IInAppBillingService service;
    ServiceConnection serviceConn;

    /* renamed from: com.vkontakte.android.BillingActivity.1 */
    class C02051 implements ServiceConnection {
        C02051() {
        }

        public void onServiceDisconnected(ComponentName name) {
            BillingActivity.this.service = null;
        }

        public void onServiceConnected(ComponentName name, IBinder binder) {
            BillingActivity.this.service = Stub.asInterface(binder);
            if (BillingActivity.this.getIntent().hasExtra("restore")) {
                BillingActivity.this.processRestore();
            } else {
                BillingActivity.this.processPurchase();
            }
        }
    }

    /* renamed from: com.vkontakte.android.BillingActivity.2 */
    class C02082 implements Runnable {
        private final /* synthetic */ ProgressDialog val$progress;

        /* renamed from: com.vkontakte.android.BillingActivity.2.1 */
        class C02061 implements Runnable {
            private final /* synthetic */ ArrayList val$dataList;
            private final /* synthetic */ ProgressDialog val$progress;

            C02061(ArrayList arrayList, ProgressDialog progressDialog) {
                this.val$dataList = arrayList;
                this.val$progress = progressDialog;
            }

            public void run() {
                if (this.val$dataList.size() == 0) {
                    BillingActivity.this.setResult(-1);
                    BillingActivity.this.finish();
                    return;
                }
                try {
                    JSONObject o = new JSONObject((String) this.val$dataList.remove(0));
                    String dp = o.getString("developerPayload");
                    String token = o.getString("purchaseToken");
                    String orderId = o.getString("orderId");
                    String[] t = dp.split(",");
                    if (Integer.parseInt(t[0]) != Global.uid) {
                        new Handler().post(this);
                        return;
                    }
                    int id = Integer.parseInt(t[2]);
                    String productId = o.getString("productId");
                    BillingActivity.this.getSharedPreferences("stickers", 0).edit().putBoolean("owned" + id, true).putString("token" + id, token).putString("order" + id, orderId).commit();
                    BillingActivity.this.pollForResult(id, orderId, productId, token, this.val$progress, false, this);
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                }
            }
        }

        /* renamed from: com.vkontakte.android.BillingActivity.2.2 */
        class C02072 implements Runnable {
            C02072() {
            }

            public void run() {
                Toast.makeText(BillingActivity.this, C0436R.string.error, 0).show();
                BillingActivity.this.setResult(0);
                BillingActivity.this.finish();
            }
        }

        C02082(ProgressDialog progressDialog) {
            this.val$progress = progressDialog;
        }

        public void run() {
            try {
                new C02061(BillingActivity.this.service.getPurchases(3, BillingActivity.this.getPackageName(), "inapp", null).getStringArrayList("INAPP_PURCHASE_DATA_LIST"), this.val$progress).run();
                BillingActivity.this.finish();
            } catch (Throwable x) {
                Log.m532w("vk", x);
                BillingActivity.this.runOnUiThread(new C02072());
            }
        }
    }

    /* renamed from: com.vkontakte.android.BillingActivity.3 */
    class C12723 implements Callback {
        private final /* synthetic */ int[] val$count;
        private final /* synthetic */ boolean val$finish;
        private final /* synthetic */ int val$id;
        private final /* synthetic */ Runnable val$onDone;
        private final /* synthetic */ String val$orderId;
        private final /* synthetic */ SharedPreferences val$prefs;
        private final /* synthetic */ String val$productId;
        private final /* synthetic */ ProgressDialog val$progress;
        private final /* synthetic */ String val$token;

        /* renamed from: com.vkontakte.android.BillingActivity.3.1 */
        class C02091 implements Runnable {
            private final /* synthetic */ boolean val$finish;
            private final /* synthetic */ int val$id;
            private final /* synthetic */ Runnable val$onDone;
            private final /* synthetic */ String val$orderId;
            private final /* synthetic */ String val$productId;
            private final /* synthetic */ ProgressDialog val$progress;
            private final /* synthetic */ String val$token;

            C02091(int i, String str, String str2, String str3, ProgressDialog progressDialog, boolean z, Runnable runnable) {
                this.val$id = i;
                this.val$orderId = str;
                this.val$productId = str2;
                this.val$token = str3;
                this.val$progress = progressDialog;
                this.val$finish = z;
                this.val$onDone = runnable;
            }

            public void run() {
                BillingActivity.this.pollForResult(this.val$id, this.val$orderId, this.val$productId, this.val$token, this.val$progress, this.val$finish, this.val$onDone);
            }
        }

        /* renamed from: com.vkontakte.android.BillingActivity.3.2 */
        class C02102 implements Runnable {
            private final /* synthetic */ boolean val$fatal;
            private final /* synthetic */ boolean val$finish;
            private final /* synthetic */ int val$id;
            private final /* synthetic */ String val$message;
            private final /* synthetic */ Runnable val$onDone;
            private final /* synthetic */ SharedPreferences val$prefs;
            private final /* synthetic */ String val$token;

            C02102(String str, SharedPreferences sharedPreferences, int i, boolean z, boolean z2, String str2, Runnable runnable) {
                this.val$token = str;
                this.val$prefs = sharedPreferences;
                this.val$id = i;
                this.val$fatal = z;
                this.val$finish = z2;
                this.val$message = str2;
                this.val$onDone = runnable;
            }

            public void run() {
                try {
                    BillingActivity.this.service.consumePurchase(3, BillingActivity.this.getPackageName(), this.val$token);
                } catch (Exception e) {
                }
                this.val$prefs.edit().remove("owned" + this.val$id).commit();
                if (this.val$fatal) {
                    this.val$prefs.edit().remove("token" + this.val$id).remove("order" + this.val$id).commit();
                }
                if (this.val$finish) {
                    Intent intent = new Intent();
                    intent.putExtra("fatal", this.val$fatal);
                    intent.putExtra(LongPollService.EXTRA_MESSAGE, this.val$message);
                    BillingActivity.this.setResult(-1, intent);
                    BillingActivity.this.finish();
                }
                if (this.val$onDone != null) {
                    BillingActivity.this.runOnUiThread(this.val$onDone);
                }
            }
        }

        /* renamed from: com.vkontakte.android.BillingActivity.3.3 */
        class C02113 implements Runnable {
            private final /* synthetic */ boolean val$finish;
            private final /* synthetic */ int val$id;
            private final /* synthetic */ String val$message;
            private final /* synthetic */ Runnable val$onDone;
            private final /* synthetic */ StickerPack val$pack;
            private final /* synthetic */ SharedPreferences val$prefs;
            private final /* synthetic */ String val$token;

            C02113(String str, SharedPreferences sharedPreferences, int i, StickerPack stickerPack, boolean z, String str2, Runnable runnable) {
                this.val$token = str;
                this.val$prefs = sharedPreferences;
                this.val$id = i;
                this.val$pack = stickerPack;
                this.val$finish = z;
                this.val$message = str2;
                this.val$onDone = runnable;
            }

            public void run() {
                Intent intent;
                try {
                    if (BillingActivity.this.service.consumePurchase(3, BillingActivity.this.getPackageName(), this.val$token) == 0) {
                        this.val$prefs.edit().remove("token" + this.val$id).remove("order" + this.val$id).putString("content" + this.val$id, new StringBuilder(String.valueOf(this.val$pack.baseURL)).append("content.zip").toString()).putString("ordering" + this.val$id, TextUtils.join(",", this.val$pack.ids)).commit();
                        ArrayList<Integer> order = new ArrayList();
                        order.addAll(Global.stringToIntArray(this.val$prefs.getString("order", ACRAConstants.DEFAULT_STRING_VALUE)));
                        order.add(Integer.valueOf(this.val$id));
                        this.val$prefs.edit().putString("order", TextUtils.join(",", order)).commit();
                        if (this.val$finish) {
                            intent = new Intent();
                            intent.putExtra("product", this.val$id);
                            intent.putExtra(LongPollService.EXTRA_MESSAGE, this.val$message);
                            BillingActivity.this.setResult(-1, intent);
                            BillingActivity.this.finish();
                        }
                    } else {
                        this.val$prefs.edit().putBoolean("incomplete" + this.val$id, true).commit();
                        if (this.val$finish) {
                            intent = new Intent();
                            intent.putExtra("product", this.val$id);
                            intent.putExtra("incomplete", true);
                            intent.putExtra(LongPollService.EXTRA_MESSAGE, this.val$message);
                            BillingActivity.this.setResult(-1, intent);
                            BillingActivity.this.finish();
                        }
                    }
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                    this.val$prefs.edit().putBoolean("incomplete" + this.val$id, true).commit();
                    if (this.val$finish) {
                        intent = new Intent();
                        intent.putExtra("product", this.val$id);
                        intent.putExtra("incomplete", true);
                        intent.putExtra(LongPollService.EXTRA_MESSAGE, this.val$message);
                        BillingActivity.this.setResult(-1, intent);
                        BillingActivity.this.finish();
                    }
                }
                if (this.val$onDone != null) {
                    this.val$onDone.run();
                }
            }
        }

        C12723(int[] iArr, boolean z, int i, Runnable runnable, SharedPreferences sharedPreferences, String str, String str2, String str3, ProgressDialog progressDialog) {
            this.val$count = iArr;
            this.val$finish = z;
            this.val$id = i;
            this.val$onDone = runnable;
            this.val$prefs = sharedPreferences;
            this.val$orderId = str;
            this.val$productId = str2;
            this.val$token = str3;
            this.val$progress = progressDialog;
        }

        public void success(int state, StickerPack pack, String message, boolean fatal) {
            int[] iArr = this.val$count;
            iArr[0] = iArr[0] + 1;
            if (state == 0) {
                if (this.val$count[0] >= 30) {
                    if (this.val$finish) {
                        Intent intent = new Intent();
                        intent.putExtra("product", this.val$id);
                        intent.putExtra("incomplete", true);
                        intent.putExtra(LongPollService.EXTRA_MESSAGE, message);
                        BillingActivity.this.setResult(-1, intent);
                        BillingActivity.this.finish();
                    }
                    if (this.val$onDone != null) {
                        this.val$onDone.run();
                        return;
                    }
                    return;
                }
                new Handler().postDelayed(new C02091(this.val$id, this.val$orderId, this.val$productId, this.val$token, this.val$progress, this.val$finish, this.val$onDone), 1000);
            } else if (state < 0 || state != 1) {
                APIController.runInBg(new C02102(this.val$token, this.val$prefs, this.val$id, fatal, this.val$finish, message, this.val$onDone));
            } else {
                this.val$prefs.edit().putBoolean("confirmed" + this.val$id, true).commit();
                APIController.runInBg(new C02113(this.val$token, this.val$prefs, this.val$id, pack, this.val$finish, message, this.val$onDone));
            }
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(BillingActivity.this, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
            this.val$prefs.edit().putBoolean("incomplete" + this.val$id, true).commit();
            if (this.val$finish) {
                Intent intent = new Intent();
                intent.putExtra("product", this.val$id);
                intent.putExtra("incomplete", true);
                BillingActivity.this.setResult(-1, intent);
                BillingActivity.this.finish();
            }
            if (this.val$onDone != null) {
                this.val$onDone.run();
            }
        }
    }

    public BillingActivity() {
        this.serviceConn = new C02051();
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        if (Stickers.isPlayStoreInstalled()) {
            bindService(new Intent("com.android.vending.billing.InAppBillingService.BIND"), this.serviceConn, 1);
            return;
        }
        Toast.makeText(this, C0436R.string.sticker_pack_not_available, 0).show();
        setResult(0);
        finish();
    }

    private void processPurchase() {
        try {
            Bundle buyIntentBundle = this.service.getBuyIntent(3, getPackageName(), getIntent().getStringExtra("store_id"), "inapp", Global.uid + "," + 1 + "," + getIntent().getIntExtra("product", 0));
            if (buyIntentBundle.getInt("RESPONSE_CODE") == 7) {
                processRestore();
                return;
            }
            startIntentSenderForResult(((PendingIntent) buyIntentBundle.getParcelable("BUY_INTENT")).getIntentSender(), LocationStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES, new Intent(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue());
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }

    private void processRestore() {
        ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(C0436R.string.loading));
        progress.setCancelable(false);
        progress.show();
        APIController.runInBg(new C02082(progress));
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.service != null) {
            unbindService(this.serviceConn);
        }
    }

    private void confirmAndConsume(int id, String orderId, String productId, String token) {
        getSharedPreferences("stickers", 0).edit().putBoolean("owned" + id, true).putString("token" + id, token).putString("order" + id, orderId).commit();
        ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(C0436R.string.completing_purchase));
        progress.setCancelable(false);
        progress.show();
        pollForResult(id, orderId, productId, token, progress, true, null);
    }

    private void pollForResult(int id, String orderId, String productId, String token, ProgressDialog progress, boolean finish, Runnable onDone) {
        int[] count = new int[1];
        new StorePurchase(id, productId, orderId, token).setCallback(new C12723(count, finish, id, onDone, getSharedPreferences("stickers", 0), orderId, productId, token, progress)).exec((Activity) this);
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode == LocationStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");
            if (resCode == -1) {
                try {
                    JSONObject jo = new JSONObject(purchaseData);
                    confirmAndConsume(getIntent().getIntExtra("product", 0), jo.getString("orderId"), jo.getString("productId"), jo.getString("purchaseToken"));
                    return;
                } catch (Throwable e) {
                    Log.m532w("vk", e);
                    return;
                }
            }
            finish();
        }
    }
}
