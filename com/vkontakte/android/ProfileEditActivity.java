package com.vkontakte.android;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.fourmob.datetimepicker.date.CalendarDatePickerDialog;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.api.AccountGetProfileInfo;
import com.vkontakte.android.api.AccountSaveProfileInfo;
import com.vkontakte.android.api.AccountSaveProfileInfo.Callback;
import com.vkontakte.android.data.Posts;
import com.vkontakte.android.data.database.City;
import com.vkontakte.android.data.database.Country;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.fragments.CitySelectFragment;
import com.vkontakte.android.fragments.CitySelectFragment.CityCallback;
import com.vkontakte.android.ui.ErrorView;
import com.vkontakte.android.ui.Fonts;
import com.vkontakte.android.ui.PhotoView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.acra.ACRAConstants;

public class ProfileEditActivity extends VKFragmentActivity {
    private static final List<Integer> RELATIONS_SAME_GENDER;
    private static final List<Integer> RELATIONS_WITH_PARTNER;
    private static final int RELATION_PARTNER_RESULT = 101;
    private Spinner bdateVisSpinner;
    private int bday;
    private int bmonth;
    private int byear;
    private int cityID;
    private TextView citySelector;
    private FrameLayout content;
    private ArrayAdapter<Country> countryAdapter;
    private Spinner countrySpinner;
    private Bundle currentInfo;
    private APIRequest currentReq;
    private ErrorView error;
    private View form;
    private int gender;
    private boolean ignoreCountryChange;
    private ProgressBar progress;
    private ArrayAdapter<CharSequence> relationAdapter;
    private UserProfile relationPartner;
    private Spinner relationSpinner;
    private View sendBtn;

    /* renamed from: com.vkontakte.android.ProfileEditActivity.1 */
    class C04061 extends ArrayAdapter<Country> {
        C04061(Context $anonymous0, int $anonymous1) {
            super($anonymous0, $anonymous1);
        }

        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View v = super.getDropDownView(position, convertView, parent);
            if (v instanceof TextView) {
                ((TextView) v).setTypeface(((Country) getItem(position)).important ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            }
            return v;
        }
    }

    /* renamed from: com.vkontakte.android.ProfileEditActivity.20 */
    class AnonymousClass20 implements OnDismissListener {
        private final /* synthetic */ boolean val$finish;

        AnonymousClass20(boolean z) {
            this.val$finish = z;
        }

        public void onDismiss(DialogInterface dialog) {
            if (this.val$finish) {
                ProfileEditActivity.this.setResult(-1);
                ProfileEditActivity.this.finish();
            }
        }
    }

    /* renamed from: com.vkontakte.android.ProfileEditActivity.2 */
    class C04072 implements OnItemSelectedListener {
        C04072() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            boolean z = false;
            if (ProfileEditActivity.this.ignoreCountryChange) {
                ProfileEditActivity.this.ignoreCountryChange = false;
                return;
            }
            ProfileEditActivity.this.cityID = 0;
            Country c = (Country) ProfileEditActivity.this.countryAdapter.getItem(pos);
            ProfileEditActivity.this.citySelector.setText(ACRAConstants.DEFAULT_STRING_VALUE);
            TextView access$4 = ProfileEditActivity.this.citySelector;
            if (c.id > 0) {
                z = true;
            }
            access$4.setEnabled(z);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.vkontakte.android.ProfileEditActivity.3 */
    class C04083 implements OnClickListener {
        C04083() {
        }

        public void onClick(View v) {
            ProfileEditActivity.this.selectCity();
        }
    }

    /* renamed from: com.vkontakte.android.ProfileEditActivity.4 */
    class C04094 implements OnClickListener {
        C04094() {
        }

        public void onClick(View v) {
            ProfileEditActivity.this.selectBirthDate();
        }
    }

    /* renamed from: com.vkontakte.android.ProfileEditActivity.5 */
    class C04105 implements OnClickListener {
        C04105() {
        }

        public void onClick(View v) {
            ProfileEditActivity.this.findViewById(C0436R.id.signup_gender_male).setSelected(true);
            ProfileEditActivity.this.findViewById(C0436R.id.signup_gender_female).setSelected(false);
            ProfileEditActivity.this.gender = 2;
            ProfileEditActivity.this.updateRelationOptions();
        }
    }

    /* renamed from: com.vkontakte.android.ProfileEditActivity.6 */
    class C04116 implements OnClickListener {
        C04116() {
        }

        public void onClick(View v) {
            ProfileEditActivity.this.findViewById(C0436R.id.signup_gender_male).setSelected(false);
            ProfileEditActivity.this.findViewById(C0436R.id.signup_gender_female).setSelected(true);
            ProfileEditActivity.this.gender = 1;
            ProfileEditActivity.this.updateRelationOptions();
        }
    }

    /* renamed from: com.vkontakte.android.ProfileEditActivity.7 */
    class C04127 implements OnClickListener {
        C04127() {
        }

        public void onClick(View v) {
            ProfileEditActivity.this.error.setVisibility(8);
            ProfileEditActivity.this.progress.setVisibility(0);
            ProfileEditActivity.this.loadData();
        }
    }

    /* renamed from: com.vkontakte.android.ProfileEditActivity.8 */
    class C04138 implements OnItemSelectedListener {
        C04138() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            int i;
            int i2 = 0;
            boolean show = ProfileEditActivity.RELATIONS_WITH_PARTNER.contains(Integer.valueOf(pos));
            View findViewById = ProfileEditActivity.this.findViewById(C0436R.id.edit_relation_divider);
            if (show) {
                i = 0;
            } else {
                i = 8;
            }
            findViewById.setVisibility(i);
            View findViewById2 = ProfileEditActivity.this.findViewById(C0436R.id.edit_relation_partner);
            if (!show) {
                i2 = 8;
            }
            findViewById2.setVisibility(i2);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.vkontakte.android.ProfileEditActivity.9 */
    class C04149 implements OnClickListener {
        C04149() {
        }

        public void onClick(View v) {
            ProfileEditActivity.this.setRelationPartner(null);
        }
    }

    /* renamed from: com.vkontakte.android.ProfileEditActivity.19 */
    class AnonymousClass19 implements Callback {
        private final /* synthetic */ Bundle val$newInfo;

        AnonymousClass19(Bundle bundle) {
            this.val$newInfo = bundle;
        }

        public void success(int nameStatus, int nameRetryIn, String newFirst, String newLast) {
            if (nameStatus == 0) {
                if (this.val$newInfo.containsKey("first_name") || this.val$newInfo.containsKey("last_name")) {
                    Intent intent = new Intent(Posts.ACTION_USER_NAME_CHANGED);
                    intent.putExtra("name", new StringBuilder(String.valueOf(newFirst)).append(" ").append(newLast).toString());
                    ProfileEditActivity.this.sendBroadcast(intent, permission.ACCESS_DATA);
                }
                ProfileEditActivity.this.setResult(-1);
                ProfileEditActivity.this.finish();
                return;
            }
            if (nameStatus == 1) {
                ProfileEditActivity.this.showNameInfoDialog(ProfileEditActivity.this.getString(C0436R.string.edit_name_processing), true);
            }
            if (nameStatus == 2) {
                ProfileEditActivity.this.showNameInfoDialog(ProfileEditActivity.this.getString(C0436R.string.edit_name_declined), false);
            }
            if (nameStatus == 3) {
                ProfileEditActivity.this.showNameInfoDialog(ProfileEditActivity.this.getString(C0436R.string.edit_name_was_accepted, new Object[]{Global.langDate(ProfileEditActivity.this.getResources(), nameRetryIn)}), false);
            }
            if (nameStatus == 4) {
                ProfileEditActivity.this.showNameInfoDialog(ProfileEditActivity.this.getString(C0436R.string.edit_name_was_declined, new Object[]{Global.langDate(ProfileEditActivity.this.getResources(), nameRetryIn)}), false);
            }
        }

        public void fail(int ecode, String emsg) {
            int i = C0436R.string.error;
            if (ecode == 100) {
                new Builder(ProfileEditActivity.this).setMessage(ProfileEditActivity.this.getString(C0436R.string.signup_invalid_name)).setTitle(C0436R.string.error).setPositiveButton(C0436R.string.ok, null).show();
                return;
            }
            Context context = ProfileEditActivity.this;
            if (ecode == -1) {
                i = C0436R.string.err_text;
            }
            Toast.makeText(context, i, 0).show();
        }
    }

    public ProfileEditActivity() {
        this.ignoreCountryChange = false;
    }

    static {
        RELATIONS_WITH_PARTNER = Arrays.asList(new Integer[]{Integer.valueOf(2), Integer.valueOf(3), Integer.valueOf(4), Integer.valueOf(5), Integer.valueOf(7)});
        RELATIONS_SAME_GENDER = Arrays.asList(new Integer[]{Integer.valueOf(2), Integer.valueOf(5), Integer.valueOf(7)});
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        this.content = new FrameLayout(this);
        this.content.setBackgroundColor(getResources().getColor(C0436R.color.cards_bg));
        this.progress = new ProgressBar(this);
        this.content.addView(this.progress, new LayoutParams(Global.scale(40.0f), Global.scale(40.0f), 17));
        this.error = (ErrorView) View.inflate(this, C0436R.layout.error, null);
        this.content.addView(this.error);
        this.form = View.inflate(this, C0436R.layout.profile_edit, null);
        this.content.addView(this.form);
        this.error.setVisibility(8);
        this.form.setVisibility(8);
        setContentView(this.content);
        ((TextView) findViewById(C0436R.id.info_message)).setTypeface(Fonts.getRobotoCondensed(2));
        ((TextView) findViewById(C0436R.id.info_new_name)).setTypeface(Fonts.getRobotoCondensed(3));
        this.bdateVisSpinner = (Spinner) findViewById(C0436R.id.edit_bdate_visibility);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, C0436R.array.edit_bdate_visibility, C0436R.layout.card_spinner_item);
        adapter.setDropDownViewResource(17367049);
        this.bdateVisSpinner.setAdapter(adapter);
        this.relationSpinner = (Spinner) findViewById(C0436R.id.edit_relation);
        this.countrySpinner = (Spinner) findViewById(C0436R.id.edit_country);
        this.countryAdapter = new C04061(this, C0436R.layout.card_spinner_item);
        this.countryAdapter.setDropDownViewResource(17367049);
        for (Country c : Country.getCountries(true, true, null)) {
            this.countryAdapter.add(c);
        }
        this.countrySpinner.setAdapter(this.countryAdapter);
        this.countrySpinner.setOnItemSelectedListener(new C04072());
        this.citySelector = (TextView) findViewById(C0436R.id.edit_city);
        this.citySelector.setOnClickListener(new C04083());
        findViewById(C0436R.id.edit_bdate_chooser).setOnClickListener(new C04094());
        findViewById(C0436R.id.signup_gender_male).setOnClickListener(new C04105());
        findViewById(C0436R.id.signup_gender_female).setOnClickListener(new C04116());
        this.error.setOnRetryListener(new C04127());
        this.relationSpinner.setOnItemSelectedListener(new C04138());
        findViewById(C0436R.id.edit_relation_partner_remove).setOnClickListener(new C04149());
        findViewById(C0436R.id.edit_relation_partner).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putBoolean("select", true);
                args.putBoolean("relation", true);
                args.putBoolean("no_online", true);
                args.putInt("my_gender", ProfileEditActivity.this.gender);
                args.putBoolean("show_same_gender", ProfileEditActivity.RELATIONS_SAME_GENDER.contains(Integer.valueOf(ProfileEditActivity.this.relationSpinner.getSelectedItemPosition())));
                Intent intent = new Intent(ProfileEditActivity.this, FragmentWrapperActivity.class);
                intent.putExtra("class", "FriendsFragment");
                intent.putExtra("args", args);
                ProfileEditActivity.this.startActivityForResult(intent, ProfileEditActivity.RELATION_PARTNER_RESULT);
            }
        });
        findViewById(C0436R.id.info_cancel_btn).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ProfileEditActivity.this.cancelNameRequest();
            }
        });
        this.sendBtn = View.inflate(this, C0436R.layout.ab_done_right, null);
        ((TextView) this.sendBtn.findViewById(C0436R.id.ab_done_text)).setText(C0436R.string.save);
        this.sendBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ProfileEditActivity.this.save();
            }
        });
        loadData();
    }

    private void selectCity() {
        CitySelectFragment fragment = new CitySelectFragment();
        Bundle args = new Bundle();
        args.putString("hint", getString(C0436R.string.edit_choose_city));
        args.putInt("country", ((Country) this.countrySpinner.getSelectedItem()).id);
        args.putBoolean("show_none", this.cityID > 0);
        fragment.setArguments(args);
        fragment.setCallback(new CityCallback() {
            public void onItemSelected(City item) {
                ProfileEditActivity.this.cityID = item.id;
                if (ProfileEditActivity.this.cityID > 0) {
                    ProfileEditActivity.this.citySelector.setText(item.title);
                } else {
                    ProfileEditActivity.this.citySelector.setText(ACRAConstants.DEFAULT_STRING_VALUE);
                }
            }
        });
        fragment.show(getSupportFragmentManager(), "city");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem item = menu.add((int) C0436R.string.save);
        item.setActionView(this.sendBtn);
        item.setShowAsAction(2);
        return true;
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.currentReq != null) {
            this.currentReq.cancel();
            this.currentReq = null;
        }
    }

    public void onActivityResult(int reqCode, int resCode, Intent intent) {
        if (reqCode == RELATION_PARTNER_RESULT && resCode == -1) {
            setRelationPartner((UserProfile) intent.getParcelableExtra("user"));
        }
    }

    private void updateRelationOptions() {
        int selected = this.relationSpinner.getSelectedItemPosition();
        this.relationAdapter = ArrayAdapter.createFromResource(this, this.gender == 1 ? C0436R.array.edit_relation_f : C0436R.array.edit_relation_m, C0436R.layout.card_spinner_item);
        this.relationAdapter.setDropDownViewResource(17367049);
        this.relationSpinner.setAdapter(this.relationAdapter);
        this.relationSpinner.setSelection(selected);
    }

    private void loadData() {
        this.currentReq = new AccountGetProfileInfo().setCallback(new AccountGetProfileInfo.Callback() {
            public void success(Bundle info) {
                int status;
                ProfileEditActivity.this.currentReq = null;
                ProfileEditActivity.this.currentInfo = info;
                ((TextView) ProfileEditActivity.this.findViewById(C0436R.id.edit_first_name)).setText(info.getString("first_name"));
                ((TextView) ProfileEditActivity.this.findViewById(C0436R.id.edit_last_name)).setText(info.getString("last_name"));
                ProfileEditActivity.this.gender = info.getInt("gender");
                ProfileEditActivity.this.findViewById(C0436R.id.signup_gender_male).setSelected(ProfileEditActivity.this.gender == 2);
                ProfileEditActivity.this.findViewById(C0436R.id.signup_gender_female).setSelected(ProfileEditActivity.this.gender == 1);
                ProfileEditActivity.this.updateRelationOptions();
                ProfileEditActivity.this.bday = info.getInt("bday");
                ProfileEditActivity.this.bmonth = info.getInt("bmonth");
                ProfileEditActivity.this.byear = info.getInt("byear");
                if (ProfileEditActivity.this.bday <= 0 || ProfileEditActivity.this.bday >= 32 || ProfileEditActivity.this.bmonth <= 0 || ProfileEditActivity.this.bmonth >= 13) {
                    ((TextView) ProfileEditActivity.this.findViewById(C0436R.id.edit_bdate_chooser)).setText(C0436R.string.not_specified);
                } else {
                    String date = new StringBuilder(String.valueOf(ProfileEditActivity.this.bday)).append(" ").append(ProfileEditActivity.this.getResources().getStringArray(C0436R.array.months_full)[ProfileEditActivity.this.bmonth - 1]).toString();
                    if (ProfileEditActivity.this.byear > 0) {
                        date = new StringBuilder(String.valueOf(date)).append(" ").append(ProfileEditActivity.this.byear).toString();
                    }
                    ((TextView) ProfileEditActivity.this.findViewById(C0436R.id.edit_bdate_chooser)).setText(date);
                }
                switch (info.getInt("bdate_vis")) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                        ProfileEditActivity.this.bdateVisSpinner.setSelection(2);
                        break;
                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                        ProfileEditActivity.this.bdateVisSpinner.setSelection(0);
                        break;
                    case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                        ProfileEditActivity.this.bdateVisSpinner.setSelection(1);
                        break;
                }
                ProfileEditActivity.this.relationSpinner.setSelection(Math.min(info.getInt("relation"), ProfileEditActivity.this.relationAdapter.getCount() - 1));
                if (info.containsKey("relation_partner")) {
                    ProfileEditActivity.this.setRelationPartner((UserProfile) info.getParcelable("relation_partner"));
                } else {
                    boolean show = ProfileEditActivity.RELATIONS_WITH_PARTNER.contains(Integer.valueOf(info.getInt("relation")));
                    ProfileEditActivity.this.findViewById(C0436R.id.edit_relation_divider).setVisibility(show ? 0 : 8);
                    ProfileEditActivity.this.findViewById(C0436R.id.edit_relation_partner).setVisibility(show ? 0 : 8);
                    ProfileEditActivity.this.setRelationPartner(null);
                }
                ProfileEditActivity.this.ignoreCountryChange = true;
                int countryID = info.getInt("country_id");
                boolean found = false;
                for (int i = 0; i < ProfileEditActivity.this.countryAdapter.getCount(); i++) {
                    if (((Country) ProfileEditActivity.this.countryAdapter.getItem(i)).id == countryID) {
                        found = true;
                        ProfileEditActivity.this.countrySpinner.setSelection(i);
                        if (!found) {
                            Country c = new Country();
                            c.id = countryID;
                            c.name = info.getString("country_name");
                            ProfileEditActivity.this.countryAdapter.add(c);
                            ProfileEditActivity.this.countrySpinner.setSelection(ProfileEditActivity.this.countryAdapter.getCount() - 1);
                        }
                        ProfileEditActivity.this.citySelector.setEnabled(countryID <= 0);
                        if (info.getInt("city_id") > 0) {
                            ProfileEditActivity.this.citySelector.setText(info.getString("city_name"));
                            ProfileEditActivity.this.cityID = info.getInt("city_id");
                        }
                        if (info.containsKey("name_req_status")) {
                            ProfileEditActivity.this.findViewById(C0436R.id.edit_info_box).setVisibility(8);
                        } else {
                            status = info.getInt("name_req_status");
                            if (status != 1) {
                                ((TextView) ProfileEditActivity.this.findViewById(C0436R.id.info_new_name)).setText(info.getString("name_req_name"));
                                ((TextView) ProfileEditActivity.this.findViewById(C0436R.id.info_message)).setText(C0436R.string.edit_name_req_processing);
                                ProfileEditActivity.this.findViewById(C0436R.id.info_cancel_btn).setVisibility(status != 1 ? 0 : 8);
                            } else {
                                ((TextView) ProfileEditActivity.this.findViewById(C0436R.id.info_message)).setText(Html.fromHtml(ProfileEditActivity.this.getString(C0436R.string.edit_name_declined)));
                                ProfileEditActivity.this.findViewById(C0436R.id.info_cancel_btn).setVisibility(8);
                                ProfileEditActivity.this.findViewById(C0436R.id.info_new_name).setVisibility(8);
                            }
                        }
                        Global.showViewAnimated(ProfileEditActivity.this.form, true, PhotoView.THUMB_ANIM_DURATION);
                        Global.showViewAnimated(ProfileEditActivity.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                        ProfileEditActivity.this.invalidateOptionsMenu();
                    }
                }
                if (found) {
                    Country c2 = new Country();
                    c2.id = countryID;
                    c2.name = info.getString("country_name");
                    ProfileEditActivity.this.countryAdapter.add(c2);
                    ProfileEditActivity.this.countrySpinner.setSelection(ProfileEditActivity.this.countryAdapter.getCount() - 1);
                }
                if (countryID <= 0) {
                }
                ProfileEditActivity.this.citySelector.setEnabled(countryID <= 0);
                if (info.getInt("city_id") > 0) {
                    ProfileEditActivity.this.citySelector.setText(info.getString("city_name"));
                    ProfileEditActivity.this.cityID = info.getInt("city_id");
                }
                if (info.containsKey("name_req_status")) {
                    ProfileEditActivity.this.findViewById(C0436R.id.edit_info_box).setVisibility(8);
                } else {
                    status = info.getInt("name_req_status");
                    if (status != 1) {
                        ((TextView) ProfileEditActivity.this.findViewById(C0436R.id.info_message)).setText(Html.fromHtml(ProfileEditActivity.this.getString(C0436R.string.edit_name_declined)));
                        ProfileEditActivity.this.findViewById(C0436R.id.info_cancel_btn).setVisibility(8);
                        ProfileEditActivity.this.findViewById(C0436R.id.info_new_name).setVisibility(8);
                    } else {
                        ((TextView) ProfileEditActivity.this.findViewById(C0436R.id.info_new_name)).setText(info.getString("name_req_name"));
                        ((TextView) ProfileEditActivity.this.findViewById(C0436R.id.info_message)).setText(C0436R.string.edit_name_req_processing);
                        if (status != 1) {
                        }
                        ProfileEditActivity.this.findViewById(C0436R.id.info_cancel_btn).setVisibility(status != 1 ? 0 : 8);
                    }
                }
                Global.showViewAnimated(ProfileEditActivity.this.form, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(ProfileEditActivity.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
                ProfileEditActivity.this.invalidateOptionsMenu();
            }

            public void fail(int ecode, String emsg) {
                ProfileEditActivity.this.currentReq = null;
                ProfileEditActivity.this.error.setErrorInfo(ecode, emsg);
                Global.showViewAnimated(ProfileEditActivity.this.error, true, PhotoView.THUMB_ANIM_DURATION);
                Global.showViewAnimated(ProfileEditActivity.this.progress, false, PhotoView.THUMB_ANIM_DURATION);
            }
        }).exec((Activity) this);
    }

    private void cancelNameRequest() {
        new AccountSaveProfileInfo(this.currentInfo.getInt("name_req_id")).setCallback(new Callback() {
            public void success(int nameStatus, int nameRetryIn, String nf, String nl) {
                Toast.makeText(ProfileEditActivity.this, C0436R.string.name_request_canceled, 1).show();
                ProfileEditActivity.this.findViewById(C0436R.id.edit_info_box).setVisibility(8);
            }

            public void fail(int ecode, String emsg) {
                Toast.makeText(ProfileEditActivity.this, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
            }
        }).wrapProgress(this).exec((Activity) this);
    }

    private void setRelationPartner(UserProfile p) {
        this.relationPartner = p;
        if (p != null) {
            ((TextView) findViewById(C0436R.id.edit_relation_partner_name)).setText(p.fullName);
            findViewById(C0436R.id.edit_relation_partner_remove).setVisibility(0);
            ((ImageView) findViewById(C0436R.id.edit_relation_partner_photo)).setImageResource(C0436R.drawable.user_placeholder);
            APIController.runInBg(new Runnable() {

                /* renamed from: com.vkontakte.android.ProfileEditActivity.16.1 */
                class C04051 implements Runnable {
                    private final /* synthetic */ Bitmap val$bmp;

                    C04051(Bitmap bitmap) {
                        this.val$bmp = bitmap;
                    }

                    public void run() {
                        ((ImageView) ProfileEditActivity.this.findViewById(C0436R.id.edit_relation_partner_photo)).setImageBitmap(this.val$bmp);
                    }
                }

                public void run() {
                    ProfileEditActivity.this.runOnUiThread(new C04051(ImageCache.get(ProfileEditActivity.this.relationPartner.photo)));
                }
            });
            return;
        }
        ((TextView) findViewById(C0436R.id.edit_relation_partner_name)).setText(C0436R.string.edit_relation_partner);
        ((ImageView) findViewById(C0436R.id.edit_relation_partner_photo)).setImageResource(C0436R.drawable.user_placeholder);
        findViewById(C0436R.id.edit_relation_partner_remove).setVisibility(8);
    }

    private void selectBirthDate() {
        int i;
        int i2;
        if (VERSION.SDK_INT < 14) {
            int i3;
            OnDateSetListener anonymousClass17 = new OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    ProfileEditActivity.this.byear = year;
                    ProfileEditActivity.this.bmonth = monthOfYear + 1;
                    ProfileEditActivity.this.bday = dayOfMonth;
                    String date = new StringBuilder(String.valueOf(ProfileEditActivity.this.bday)).append(" ").append(ProfileEditActivity.this.getResources().getStringArray(C0436R.array.months_full)[ProfileEditActivity.this.bmonth - 1]).toString();
                    if (ProfileEditActivity.this.byear > 0) {
                        date = new StringBuilder(String.valueOf(date)).append(" ").append(ProfileEditActivity.this.byear).toString();
                    }
                    ((TextView) ProfileEditActivity.this.findViewById(C0436R.id.edit_bdate_chooser)).setText(date);
                }
            };
            if (this.byear > 1900) {
                i = this.byear;
            } else {
                i = Calendar.getInstance().get(1) - 14;
            }
            if (this.bmonth > 0) {
                i2 = this.bmonth - 1;
            } else {
                i2 = 1;
            }
            if (this.bday > 0) {
                i3 = this.bday;
            } else {
                i3 = 1;
            }
            new DatePickerDialog(this, anonymousClass17, i, i2, i3).show();
            return;
        }
        int i4;
        CalendarDatePickerDialog.OnDateSetListener anonymousClass18 = new CalendarDatePickerDialog.OnDateSetListener() {
            public void onDateSet(CalendarDatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                ProfileEditActivity.this.byear = year;
                ProfileEditActivity.this.bmonth = monthOfYear + 1;
                ProfileEditActivity.this.bday = dayOfMonth;
                String date = new StringBuilder(String.valueOf(ProfileEditActivity.this.bday)).append(" ").append(ProfileEditActivity.this.getResources().getStringArray(C0436R.array.months_full)[ProfileEditActivity.this.bmonth - 1]).toString();
                if (ProfileEditActivity.this.byear > 0) {
                    date = new StringBuilder(String.valueOf(date)).append(" ").append(ProfileEditActivity.this.byear).toString();
                }
                ((TextView) ProfileEditActivity.this.findViewById(C0436R.id.edit_bdate_chooser)).setText(date);
            }
        };
        if (this.byear > 1900) {
            i4 = this.byear;
        } else {
            i4 = Calendar.getInstance().get(1) - 14;
        }
        if (this.bmonth > 0) {
            i = this.bmonth - 1;
        } else {
            i = 1;
        }
        if (this.bday > 0) {
            i2 = this.bday;
        } else {
            i2 = 1;
        }
        CalendarDatePickerDialog dlg = CalendarDatePickerDialog.newInstance(anonymousClass18, i4, i, i2);
        dlg.setYearRange(1905, Calendar.getInstance().get(1) - 14);
        dlg.setDoneButtonText(getString(C0436R.string.done));
        dlg.show(getFragmentManager(), "_datepicker_");
    }

    private void save() {
        Bundle newInfo = new Bundle();
        String firstName = ((TextView) findViewById(C0436R.id.edit_first_name)).getText().toString();
        String lastName = ((TextView) findViewById(C0436R.id.edit_last_name)).getText().toString();
        if (firstName.length() < 2 || lastName.length() < 2) {
            Toast.makeText(this, C0436R.string.signup_invalid_name, 0).show();
            return;
        }
        int newRelPartnerId;
        int relPartnerId;
        if (!(firstName.equals(this.currentInfo.getString("first_name")) && lastName.equals(this.currentInfo.getString("last_name")))) {
            newInfo.putString("first_name", firstName);
            newInfo.putString("last_name", lastName);
        }
        if (this.gender != this.currentInfo.getInt("gender")) {
            newInfo.putInt("gender", this.gender);
        }
        int relation = this.relationSpinner.getSelectedItemPosition();
        if (relation != this.currentInfo.getInt("relation")) {
            newInfo.putInt("relation", relation);
        }
        UserProfile relPartner = (UserProfile) this.currentInfo.getParcelable("relation_partner");
        if (relPartner != null) {
            newRelPartnerId = relPartner.uid;
        } else {
            newRelPartnerId = 0;
        }
        if (this.relationPartner != null) {
            relPartnerId = this.relationPartner.uid;
        } else {
            relPartnerId = 0;
        }
        if (relPartnerId != newRelPartnerId) {
            newInfo.putParcelable("relation_partner", this.relationPartner);
        }
        if (!(this.bday == this.currentInfo.getInt("bday") && this.bmonth == this.currentInfo.getInt("bmonth") && this.byear == this.currentInfo.getInt("byear"))) {
            newInfo.putInt("bday", this.bday);
            newInfo.putInt("bmonth", this.bmonth);
            newInfo.putInt("byear", this.byear);
        }
        int bdateVis = -1;
        switch (this.bdateVisSpinner.getSelectedItemPosition()) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                bdateVis = 1;
                break;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                bdateVis = 2;
                break;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                bdateVis = 0;
                break;
        }
        if (bdateVis != this.currentInfo.getInt("bdate_vis")) {
            newInfo.putInt("bdate_vis", bdateVis);
        }
        int country = ((Country) this.countrySpinner.getSelectedItem()).id;
        if (country != this.currentInfo.getInt("country_id")) {
            newInfo.putInt("country_id", country);
        }
        if (this.cityID != this.currentInfo.getInt("city_id")) {
            newInfo.putInt("city_id", this.cityID);
        }
        if (newInfo.size() == 0) {
            Log.m525d("vk", "Nothing to save.");
            finish();
            return;
        }
        new AccountSaveProfileInfo(newInfo).setCallback(new AnonymousClass19(newInfo)).wrapProgress(this).exec((Activity) this);
    }

    private void showNameInfoDialog(String text, boolean finish) {
        new Builder(this).setTitle(C0436R.string.edit_name_dialog_title).setMessage(Html.fromHtml(text)).setPositiveButton(C0436R.string.ok, null).show().setOnDismissListener(new AnonymousClass20(finish));
    }
}
