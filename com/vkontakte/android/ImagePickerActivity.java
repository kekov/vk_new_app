package com.vkontakte.android;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import com.facebook.WebDialog;
import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.mediapicker.GalleryPickerActivity;
import java.util.ArrayList;

public class ImagePickerActivity extends Activity {
    private static final int ALBUM_RESULT = 103;
    private static final int CAMERA_RESULT = 101;
    private static final int CROP_RESULT = 102;
    private static final int FILTER_RESULT = 104;
    private static final int GALLERY_RESULT = 100;
    private boolean crop;
    private Uri tempPhotoURI;

    /* renamed from: com.vkontakte.android.ImagePickerActivity.1 */
    class C02801 implements OnClickListener {
        private final /* synthetic */ ArrayList val$acts;

        C02801(ArrayList arrayList) {
            this.val$acts = arrayList;
        }

        public void onClick(DialogInterface dialog, int which) {
            if (which < this.val$acts.size()) {
                String a = (String) this.val$acts.get(which);
                if ("g".equals(a)) {
                    ImagePickerActivity.this.startGallery();
                    return;
                } else if ("c".equals(a)) {
                    ImagePickerActivity.this.startCamera();
                    return;
                } else if ("a".equals(a)) {
                    ImagePickerActivity.this.startAlbum();
                    return;
                } else {
                    return;
                }
            }
            ImagePickerActivity.this.customSelected(which - 2);
        }
    }

    /* renamed from: com.vkontakte.android.ImagePickerActivity.2 */
    class C02812 implements OnCancelListener {
        C02812() {
        }

        public void onCancel(DialogInterface dialog) {
            ImagePickerActivity.this.finish();
        }
    }

    public ImagePickerActivity() {
        this.crop = false;
    }

    public void onCreate(Bundle b) {
        super.onCreate(b);
        overridePendingTransition(0, 0);
        this.crop = getIntent().getBooleanExtra("crop", false);
        ArrayList<String> items = new ArrayList();
        ArrayList<String> acts = new ArrayList();
        items.add(getResources().getString(C0436R.string.add_photo_gallery));
        acts.add("g");
        items.add(getResources().getString(C0436R.string.add_photo_camera));
        acts.add("c");
        if (getIntent().getBooleanExtra("allow_album", true)) {
            items.add(getResources().getString(C0436R.string.add_photo_from_album));
            acts.add("a");
        }
        if (getIntent().hasExtra("custom")) {
            items.addAll(getIntent().getStringArrayListExtra("custom"));
        }
        if (getIntent().getIntExtra(WebDialog.DIALOG_PARAM_TYPE, -1) == 0) {
            startCamera();
        } else if (getIntent().getIntExtra(WebDialog.DIALOG_PARAM_TYPE, -1) == 1) {
            startGallery();
        } else {
            new Builder(this).setTitle(C0436R.string.attach_photo).setItems((CharSequence[]) items.toArray(new String[0]), new C02801(acts)).setOnCancelListener(new C02812()).show();
        }
    }

    private void startCamera() {
        try {
            ContentValues values = new ContentValues();
            values.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, "tmp");
            this.tempPhotoURI = getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, values);
            Intent camIntent = new Intent("android.media.action.IMAGE_CAPTURE");
            camIntent.putExtra("output", this.tempPhotoURI);
            startActivityForResult(camIntent, CAMERA_RESULT);
        } catch (Exception e) {
        }
    }

    private void startGallery() {
        Intent intent = new Intent(this, GalleryPickerActivity.class);
        intent.putExtra("selection_limit", getIntent().getIntExtra("limit", GALLERY_RESULT));
        if (getIntent().getIntExtra("limit", 0) == 1) {
            intent.putExtra("single_mode", true);
        }
        if (intent.getBooleanExtra("no_thumbs", false)) {
            intent.putExtra("no_thumbs", true);
        }
        startActivityForResult(intent, GALLERY_RESULT);
    }

    private void startAlbum() {
        Bundle args = new Bundle();
        args.putBoolean("select", true);
        Intent intent = new Intent(this, FragmentWrapperActivity.class);
        intent.putExtra("class", "PhotoAlbumsListFragment");
        intent.putExtra("args", args);
        startActivityForResult(intent, ALBUM_RESULT);
    }

    private void customSelected(int idx) {
        Intent intent = new Intent();
        intent.putExtra("option", idx);
        setResult(1, intent);
        finish();
    }

    public void onActivityResult(int reqCode, int result, Intent data) {
        if (result == -1) {
            ArrayList<String> files;
            Intent intent;
            if (reqCode == GALLERY_RESULT && data != null && data.hasExtra("images")) {
                files = data.getStringArrayListExtra("images");
                Intent res = new Intent();
                if (files.size() == 1) {
                    res.putExtra("file", (String) files.get(0));
                } else {
                    res.putExtra("files", files);
                }
                setResult(-1, res);
                finish();
            }
            if (reqCode == CAMERA_RESULT && this.tempPhotoURI != null) {
                intent = new Intent(this, GalleryPickerActivity.class);
                intent.setAction("android.intent.action.SEND");
                intent.putExtra("android.intent.extra.STREAM", this.tempPhotoURI);
                startActivityForResult(intent, FILTER_RESULT);
            }
            if (reqCode == FILTER_RESULT && data != null && data.hasExtra("images")) {
                files = data.getStringArrayListExtra("images");
                intent = new Intent();
                intent.putExtra("file", (String) files.get(0));
                setResult(-1, intent);
                finish();
            }
            if (reqCode == CROP_RESULT) {
                intent = new Intent();
                intent.putExtra("file", this.tempPhotoURI.toString());
                setResult(-1, intent);
                finish();
            }
            if (reqCode == ALBUM_RESULT) {
                Photo photo = (Photo) data.getParcelableExtra("photo");
                intent = new Intent();
                intent.putExtra("attachment", new PhotoAttachment(photo));
                setResult(-1, intent);
                finish();
                return;
            }
            return;
        }
        setResult(0);
        finish();
    }
}
