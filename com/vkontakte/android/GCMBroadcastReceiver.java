package com.vkontakte.android;

import android.app.Notification;
import android.app.Notification.BigTextStyle;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings.System;
import com.facebook.WebDialog;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.vkontakte.android.cache.Cache;
import com.vkontakte.android.data.Friends;
import com.vkontakte.android.data.Friends.GetUsersCallback;
import com.vkontakte.android.data.Stickers;
import java.util.ArrayList;
import java.util.Arrays;
import org.acra.ACRAConstants;

public class GCMBroadcastReceiver extends BroadcastReceiver {
    public static final int ID_FRIEND_FOUND_NOTIFICATION = 503;
    public static final int ID_FRIEND_NOTIFICATION = 501;
    public static final int ID_REPLY_NOTIFICATION = 502;

    /* renamed from: com.vkontakte.android.GCMBroadcastReceiver.1 */
    class C02711 implements Runnable {
        private final /* synthetic */ Intent val$_intent;
        private final /* synthetic */ Context val$context;

        /* renamed from: com.vkontakte.android.GCMBroadcastReceiver.1.1 */
        class C12881 implements GetUsersCallback {
            private final /* synthetic */ int val$cnt;
            private final /* synthetic */ Context val$context;
            private final /* synthetic */ Intent val$intent;
            private final /* synthetic */ SharedPreferences val$prefs;

            C12881(Context context, Intent intent, int i, SharedPreferences sharedPreferences) {
                this.val$context = context;
                this.val$intent = intent;
                this.val$cnt = i;
                this.val$prefs = sharedPreferences;
            }

            public void onUsersLoaded(ArrayList<UserProfile> users) {
                if (users.size() >= 1) {
                    UserProfile user = (UserProfile) users.get(0);
                    PendingIntent pIntent = PendingIntent.getActivity(this.val$context, 0, this.val$intent, 134217728);
                    Intent intentAccept = new Intent(NotificationButtonsReceiver.ACTION_ACCEPT_FRIEND);
                    intentAccept.putExtra("uid", user.uid);
                    PendingIntent pIntentAccept = PendingIntent.getBroadcast(this.val$context, 1, intentAccept, 1342177280);
                    Intent intentDecline = new Intent(NotificationButtonsReceiver.ACTION_DECLINE_FRIEND);
                    intentDecline.putExtra("uid", user.uid);
                    PendingIntent pIntentDecline = PendingIntent.getBroadcast(this.val$context, 2, intentDecline, 1342177280);
                    Builder ntfb = new Builder(this.val$context).setContentTitle(user.fullName).setContentText(this.val$context.getResources().getString(C0436R.string.add_friend_notify)).setContentIntent(pIntent).setLargeIcon(Bitmap.createScaledBitmap(ImageCache.get(user.photo), Global.scale(64.0f), Global.scale(64.0f), true)).setSmallIcon(C0436R.drawable.ic_stat_notify_request).setNumber(this.val$cnt).setTicker(user.fullName + " " + this.val$context.getResources().getString(C0436R.string.add_friend_notify));
                    if (VERSION.SDK_INT >= 16) {
                        ntfb.addAction(C0436R.drawable.ic_ab_done, this.val$context.getResources().getString(C0436R.string.friends_add), pIntentAccept).addAction(C0436R.drawable.ic_ab_cancel, this.val$context.getResources().getString(C0436R.string.friends_decline), pIntentDecline);
                    }
                    Notification ntf = ntfb.getNotification();
                    ntf.flags |= 16;
                    if (this.val$prefs.getString("notifyRingtone", System.DEFAULT_NOTIFICATION_URI.toString()).length() > 0) {
                        ntf.sound = Uri.parse(this.val$prefs.getString("notifyRingtone", System.DEFAULT_NOTIFICATION_URI.toString()));
                    }
                    if (this.val$prefs.getBoolean("notifyVibrate", true)) {
                        ntf.defaults |= 2;
                    }
                    if (this.val$prefs.getBoolean("notifyLED", true)) {
                        ntf.defaults |= 4;
                        ntf.flags |= 1;
                    }
                    ((NotificationManager) this.val$context.getSystemService("notification")).notify(GCMBroadcastReceiver.ID_FRIEND_NOTIFICATION, ntf);
                }
            }
        }

        /* renamed from: com.vkontakte.android.GCMBroadcastReceiver.1.2 */
        class C12892 implements GetUsersCallback {
            private final /* synthetic */ Context val$context;
            private final /* synthetic */ Message val$msg;

            C12892(Message message, Context context) {
                this.val$msg = message;
                this.val$context = context;
            }

            public void onUsersLoaded(ArrayList<UserProfile> users) {
                UserProfile user = (UserProfile) users.get(0);
                String notifyText = this.val$msg.text;
                if (this.val$msg.isServiceMessage && this.val$msg.extras.containsKey("action")) {
                    notifyText = this.val$msg.getServiceMessageText(user, this.val$msg.extras.getString("action_user_name_acc"));
                }
                if (notifyText == null || notifyText.length() == 0) {
                    if (this.val$msg.attachments.size() > 0) {
                        Attachment att = (Attachment) this.val$msg.attachments.get(0);
                        if (att instanceof PhotoAttachment) {
                            notifyText = this.val$context.getResources().getString(C0436R.string.photo);
                        } else if (att instanceof VideoAttachment) {
                            notifyText = this.val$context.getResources().getString(C0436R.string.video);
                        } else if (att instanceof AudioAttachment) {
                            notifyText = this.val$context.getResources().getString(C0436R.string.audio);
                        } else if (att instanceof DocumentAttachment) {
                            notifyText = this.val$context.getResources().getString(((DocumentAttachment) att).url == null ? C0436R.string.gift : C0436R.string.doc);
                        } else if (att instanceof PostAttachment) {
                            notifyText = this.val$context.getResources().getString(C0436R.string.attach_wall_post);
                        } else if (att instanceof GeoAttachment) {
                            notifyText = this.val$context.getResources().getString(C0436R.string.place);
                        }
                    } else if (this.val$msg.fwdMessages != null && this.val$msg.fwdMessages.size() > 0) {
                        notifyText = Global.langPlural(C0436R.array.num_attach_fwd_message, this.val$msg.fwdMessages.size(), this.val$context.getResources());
                    }
                }
                LongPollService.updateNotification(notifyText, this.val$msg.title, user.fullName, user.photo, true, this.val$msg.peer);
            }
        }

        /* renamed from: com.vkontakte.android.GCMBroadcastReceiver.1.3 */
        class C12903 implements GetUsersCallback {
            private final /* synthetic */ Context val$context;
            private final /* synthetic */ SharedPreferences val$prefs;
            private final /* synthetic */ String val$service;
            private final /* synthetic */ int val$uid;

            C12903(int i, String str, Context context, SharedPreferences sharedPreferences) {
                this.val$uid = i;
                this.val$service = str;
                this.val$context = context;
                this.val$prefs = sharedPreferences;
            }

            public void onUsersLoaded(ArrayList<UserProfile> users) {
                if (users.size() >= 1) {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("vkontakte://profile/" + this.val$uid));
                    UserProfile user = (UserProfile) users.get(0);
                    int serviceStr = 0;
                    if ("email".equals(this.val$service)) {
                        serviceStr = C0436R.string.friend_search_email;
                    } else if ("phone".equals(this.val$service)) {
                        serviceStr = C0436R.string.friend_search_phone;
                    } else if ("twitter".equals(this.val$service)) {
                        serviceStr = C0436R.string.friend_search_twitter;
                    } else if ("facebook".equals(this.val$service)) {
                        serviceStr = C0436R.string.friend_search_facebook;
                    } else if ("odnoklassniki".equals(this.val$service)) {
                        serviceStr = C0436R.string.friend_search_odnoklassniki;
                    } else if ("instagram".equals(this.val$service)) {
                        serviceStr = C0436R.string.friend_search_instagram;
                    } else if ("google".equals(this.val$service)) {
                        serviceStr = C0436R.string.friend_search_gplus;
                    }
                    Context context = this.val$context;
                    int i = user.f151f ? C0436R.string.notify_friend_found_f : C0436R.string.notify_friend_found_m;
                    Object[] objArr = new Object[2];
                    objArr[0] = user.fullName;
                    objArr[1] = serviceStr != 0 ? this.val$context.getString(serviceStr) : this.val$service;
                    String text = context.getString(i, objArr);
                    Notification ntf;
                    if (VERSION.SDK_INT < 14) {
                        ntf = new Notification(C0436R.drawable.ic_stat_notify_request, text, System.currentTimeMillis());
                        ntf.setLatestEventInfo(this.val$context, this.val$context.getString(user.f151f ? C0436R.string.notify_friend_found_title_f : C0436R.string.notify_friend_found_title_m), text, PendingIntent.getActivity(this.val$context, 0, intent, 134217728));
                        ntf.flags |= 16;
                        ((NotificationManager) this.val$context.getSystemService("notification")).notify(GCMBroadcastReceiver.ID_FRIEND_FOUND_NOTIFICATION, ntf);
                        return;
                    }
                    PendingIntent pIntent = PendingIntent.getActivity(this.val$context, 0, intent, 134217728);
                    Intent intentAccept = new Intent(NotificationButtonsReceiver.ACTION_ACCEPT_FRIEND);
                    intentAccept.putExtra("uid", user.uid);
                    intentAccept.putExtra("notifyId", GCMBroadcastReceiver.ID_FRIEND_FOUND_NOTIFICATION);
                    PendingIntent pIntentAccept = PendingIntent.getBroadcast(this.val$context, 1, intentAccept, 1342177280);
                    Builder ntfb = new Builder(this.val$context).setContentTitle(this.val$context.getString(user.f151f ? C0436R.string.notify_friend_found_title_f : C0436R.string.notify_friend_found_title_m)).setContentText(text).setContentIntent(pIntent).setLargeIcon(Bitmap.createScaledBitmap(ImageCache.get(user.photo), Global.scale(64.0f), Global.scale(64.0f), true)).setSmallIcon(C0436R.drawable.ic_stat_notify_request).setTicker(text);
                    if (VERSION.SDK_INT >= 16) {
                        ntfb.addAction(C0436R.drawable.ic_ab_add, this.val$context.getResources().getString(C0436R.string.profile_add_friend), pIntentAccept);
                        ntf = new BigTextStyle(ntfb).bigText(text).build();
                    } else {
                        ntf = ntfb.getNotification();
                    }
                    ntf.flags |= 16;
                    if (this.val$prefs.getString("notifyRingtone", System.DEFAULT_NOTIFICATION_URI.toString()).length() > 0) {
                        ntf.sound = Uri.parse(this.val$prefs.getString("notifyRingtone", System.DEFAULT_NOTIFICATION_URI.toString()));
                    }
                    if (this.val$prefs.getBoolean("notifyVibrate", true)) {
                        ntf.defaults |= 2;
                    }
                    if (this.val$prefs.getBoolean("notifyLED", true)) {
                        ntf.defaults |= 4;
                        ntf.flags |= 1;
                    }
                    ((NotificationManager) this.val$context.getSystemService("notification")).notify(GCMBroadcastReceiver.ID_FRIEND_FOUND_NOTIFICATION, ntf);
                }
            }
        }

        /* renamed from: com.vkontakte.android.GCMBroadcastReceiver.1.4 */
        class C12914 implements GetUsersCallback {
            private final /* synthetic */ Context val$context;
            private final /* synthetic */ Intent val$intent;
            private final /* synthetic */ boolean val$isReply;
            private final /* synthetic */ String val$place;
            private final /* synthetic */ SharedPreferences val$prefs;
            private final /* synthetic */ String val$text;

            C12914(Context context, Intent intent, String str, boolean z, String str2, SharedPreferences sharedPreferences) {
                this.val$context = context;
                this.val$intent = intent;
                this.val$place = str;
                this.val$isReply = z;
                this.val$text = str2;
                this.val$prefs = sharedPreferences;
            }

            public void onUsersLoaded(ArrayList<UserProfile> users) {
                if (users.size() != 0) {
                    Notification ntf;
                    UserProfile user = (UserProfile) users.get(0);
                    Log.m528i("vk", "Got user " + user);
                    PendingIntent pIntent = PendingIntent.getActivity(this.val$context, 0, this.val$intent, 134217728);
                    String shortTitle = this.val$context.getResources().getString(C0436R.string.commented_post_s);
                    String longTitle = this.val$context.getResources().getString(user.f151f ? C0436R.string.commented_post_f : C0436R.string.commented_post_m, new Object[]{user.fullName});
                    if (this.val$place.startsWith("photo")) {
                        shortTitle = this.val$context.getResources().getString(C0436R.string.commented_photo_s);
                        longTitle = this.val$context.getResources().getString(user.f151f ? C0436R.string.commented_photo_f : C0436R.string.commented_photo_m, new Object[]{user.fullName});
                    }
                    if (this.val$place.startsWith("video")) {
                        shortTitle = this.val$context.getResources().getString(C0436R.string.commented_video_s);
                        longTitle = this.val$context.getResources().getString(user.f151f ? C0436R.string.commented_video_f : C0436R.string.commented_video_m, new Object[]{user.fullName});
                    }
                    if (this.val$isReply) {
                        longTitle = this.val$context.getResources().getString(user.f151f ? C0436R.string.replied_f : C0436R.string.replied_m, new Object[]{user.fullName});
                        shortTitle = this.val$context.getResources().getString(C0436R.string.replied_s);
                    }
                    if (VERSION.SDK_INT >= 14) {
                        Bitmap photo = ImageCache.get(user.photo);
                        Builder ticker = new Builder(this.val$context).setContentTitle(user.fullName + " " + shortTitle).setContentText(this.val$text).setContentIntent(pIntent).setTicker(longTitle);
                        if (photo == null) {
                            photo = Global.getResBitmap(this.val$context.getResources(), C0436R.drawable.user_placeholder);
                        }
                        Builder bldr = ticker.setLargeIcon(Bitmap.createScaledBitmap(photo, Global.scale(64.0f), Global.scale(64.0f), true)).setSmallIcon(C0436R.drawable.ic_stat_notify_reply);
                        if (VERSION.SDK_INT >= 16) {
                            ntf = new BigTextStyle(bldr).setBigContentTitle(user.fullName + " " + shortTitle).bigText(this.val$text).build();
                        } else {
                            ntf = bldr.getNotification();
                        }
                    } else {
                        ntf = new Notification(C0436R.drawable.ic_stat_notify_reply, longTitle, System.currentTimeMillis());
                        ntf.setLatestEventInfo(this.val$context, user.fullName + " " + shortTitle, this.val$text, pIntent);
                    }
                    ntf.flags |= 16;
                    if (this.val$prefs.getString("notifyRingtone", System.DEFAULT_NOTIFICATION_URI.toString()).length() > 0) {
                        ntf.sound = Uri.parse(this.val$prefs.getString("notifyRingtone", System.DEFAULT_NOTIFICATION_URI.toString()));
                    }
                    if (this.val$prefs.getBoolean("notifyVibrate", true)) {
                        ntf.defaults |= 2;
                    }
                    if (this.val$prefs.getBoolean("notifyLED", true)) {
                        ntf.defaults |= 4;
                        ntf.flags |= 1;
                    }
                    ((NotificationManager) this.val$context.getSystemService("notification")).notify(GCMBroadcastReceiver.ID_REPLY_NOTIFICATION, ntf);
                }
            }
        }

        C02711(Context context, Intent intent) {
            this.val$context = context;
            this.val$_intent = intent;
        }

        public void run() {
            String messageType = GoogleCloudMessaging.getInstance(this.val$context).getMessageType(this.val$_intent);
            Bundle extras = this.val$_intent.getExtras();
            SharedPreferences sprefs = VKApplication.context.getSharedPreferences(null, 0);
            Global.secret = sprefs.getString("secret", null);
            Global.accessToken = sprefs.getString("sid", null);
            Global.uid = sprefs.getInt("uid", 0);
            String l = ACRAConstants.DEFAULT_STRING_VALUE;
            if (Global.uid == 1708231) {
                Log.m528i("vk", "!!!onmessage!!!");
                for (String k : extras.keySet()) {
                    Log.m528i("vk", new StringBuilder(String.valueOf(k)).append(" = ").append(extras.getString(k)).toString());
                    l = new StringBuilder(String.valueOf(l)).append(k).append("=").append(extras.getString(k)).append(";").toString();
                }
                Log.m528i("vk", "===============");
            }
            sprefs.edit().putInt("push_counter", sprefs.getInt("push_counter", 0) + 1).commit();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.val$context);
            String ckey = ACRAConstants.DEFAULT_STRING_VALUE;
            ckey = extras.getString("collapse_key");
            if ("update_stickers".equals(ckey)) {
                Stickers.updateInfo();
            } else if (!prefs.getBoolean("notifications", true)) {
                Log.m525d("vk", "all notifications disabled");
            } else if (prefs.getLong("dnd_end", 0) > System.currentTimeMillis()) {
                Log.m525d("vk", "global dnd is active");
            } else {
                String enabled;
                int uid;
                Intent intent;
                ArrayList<Integer> uids;
                ArrayList<Integer> al;
                if ("friend".equals(ckey)) {
                    enabled = prefs.getString("notifyTypes", null);
                    if (enabled != null) {
                        if (!Arrays.asList(enabled.split(";")).contains("friends")) {
                            Log.m525d("vk", "friend notifications disabled");
                            return;
                        }
                    }
                    String username = extras.getString("first_name") + " " + extras.getString("last_name");
                    int cnt = Integer.parseInt(extras.getString("badge"));
                    uid = Integer.parseInt(extras.getString("uid"));
                    intent = new Intent("android.intent.action.VIEW", Uri.parse("vkontakte://profile/" + uid));
                    if (VERSION.SDK_INT < 14) {
                        Notification notification = new Notification(C0436R.drawable.ic_stat_notify_request, new StringBuilder(String.valueOf(username)).append(" ").append(this.val$context.getResources().getString(C0436R.string.add_friend_notify)).toString(), System.currentTimeMillis());
                        notification.setLatestEventInfo(this.val$context, username, this.val$context.getResources().getString(C0436R.string.add_friend_notify), PendingIntent.getActivity(this.val$context, 0, intent, 134217728));
                        notification.flags |= 16;
                        ((NotificationManager) this.val$context.getSystemService("notification")).notify(GCMBroadcastReceiver.ID_FRIEND_NOTIFICATION, notification);
                    } else {
                        uids = new ArrayList();
                        uids.add(Integer.valueOf(uid));
                        Friends.getUsers(uids, new C12881(this.val$context, intent, cnt, prefs));
                    }
                }
                if ("msg".equals(ckey)) {
                    if (LongPollService.longPollRunning) {
                        Log.m528i("vk", "LongPoll running, push message discarded");
                        return;
                    }
                    enabled = prefs.getString("notifyTypes", null);
                    if (enabled != null) {
                        if (!Arrays.asList(enabled.split(";")).contains("messages")) {
                            Log.m525d("vk", "message notifications disabled");
                            return;
                        }
                    }
                    int num = Integer.parseInt(extras.getString("badge"));
                    int mid = Integer.parseInt(extras.getString(LongPollService.EXTRA_MSG_ID));
                    LongPollService.numNewMessages = num;
                    LongPollService.syncStateWithServer();
                    Message msg = Cache.getMessageByID(mid);
                    if (msg == null) {
                        Log.m526e("vk", "WTF?!?!");
                        return;
                    } else if (!msg.readState) {
                        al = new ArrayList();
                        al.add(Integer.valueOf(msg.sender));
                        Friends.getUsers(al, new C12892(msg, this.val$context));
                    } else {
                        return;
                    }
                }
                if ("friend_found".equals(ckey)) {
                    enabled = prefs.getString("notifyTypes", null);
                    if (enabled != null) {
                        if (!Arrays.asList(enabled.split(";")).contains("friends")) {
                            Log.m525d("vk", "friend notifications disabled");
                            return;
                        }
                    }
                    uid = Integer.parseInt(extras.getString("uid"));
                    String service = extras.getString("service");
                    uids = new ArrayList();
                    uids.add(Integer.valueOf(uid));
                    Friends.getUsers(uids, new C12903(uid, service, this.val$context, prefs));
                }
                if ("reply".equals(ckey) || "comment".equals(ckey) || "mention".equals(ckey)) {
                    enabled = prefs.getString("notifyTypes", null);
                    if (enabled != null) {
                        if (!Arrays.asList(enabled.split(";")).contains("mention".equals(ckey) ? "mentions" : "replies")) {
                            Log.m525d("vk", "reply/mention notifications disabled");
                            return;
                        }
                    }
                    String link = "vkontakte://vk.com/feed?section=notifications";
                    String text = Global.unwrapMentions(extras.getString("text"));
                    uid = Integer.parseInt(extras.getString("from_id"));
                    boolean isReply = "reply".equals(extras.getString(WebDialog.DIALOG_PARAM_TYPE));
                    al = new ArrayList();
                    al.add(Integer.valueOf(uid));
                    intent = new Intent("android.intent.action.VIEW", Uri.parse("vkontakte://vk.com/feed?section=notifications"));
                    intent.addFlags(268435456);
                    Friends.getUsers(al, new C12914(this.val$context, intent, extras.getString("place"), isReply, text, prefs));
                }
            }
        }
    }

    public void onReceive(Context context, Intent _intent) {
        setResultCode(-1);
        new Thread(new C02711(context, _intent)).start();
    }
}
