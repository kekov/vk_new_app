package com.vkontakte.android;

import android.app.Activity;
import android.content.Intent;
import java.util.Timer;
import java.util.TimerTask;

public class AppStateTracker {
    private static Activity currentActivity;
    private static boolean isInBG;
    private static Timer timer;

    private static class BgTrackerTask extends TimerTask {
        private BgTrackerTask() {
        }

        public void run() {
            AppStateTracker.timer = null;
            AppStateTracker.isInBG = true;
            Log.m528i("vk", "==== ENTER BACKGROUND ====");
            if (Global.longPoll != null) {
                Global.longPoll.stopDelayed();
            }
        }
    }

    static {
        isInBG = false;
    }

    public static void onActivityPaused() {
        if (timer == null) {
            timer = new Timer();
            timer.schedule(new BgTrackerTask(), 2000);
        }
        currentActivity = null;
    }

    public static void onActivityResumed(Activity act) {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (isInBG || act.isTaskRoot()) {
            Log.m528i("vk", "==== LEAVE BACKGROUND ====");
            isInBG = false;
            if (Global.longPoll != null) {
                Global.longPoll.cancelDelayedStop();
            } else {
                VKApplication.context.startService(new Intent(VKApplication.context, LongPollService.class));
            }
        }
        currentActivity = act;
    }

    public static Activity getCurrentActivity() {
        return currentActivity;
    }
}
