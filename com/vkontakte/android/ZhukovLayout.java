package com.vkontakte.android;

import java.util.List;

public class ZhukovLayout {
    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void processThumbs(int r66, int r67, java.util.ArrayList<com.vkontakte.android.Attachment> r68) {
        /*
        r52 = new java.util.ArrayList;
        r52.<init>();
        r60 = r68.iterator();
    L_0x0009:
        r61 = r60.hasNext();
        if (r61 != 0) goto L_0x0016;
    L_0x000f:
        r60 = r52.size();
        if (r60 != 0) goto L_0x002a;
    L_0x0015:
        return;
    L_0x0016:
        r5 = r60.next();
        r5 = (com.vkontakte.android.Attachment) r5;
        r0 = r5 instanceof com.vkontakte.android.ThumbAttachment;
        r61 = r0;
        if (r61 == 0) goto L_0x0009;
    L_0x0022:
        r5 = (com.vkontakte.android.ThumbAttachment) r5;
        r0 = r52;
        r0.add(r5);
        goto L_0x0009;
    L_0x002a:
        r41 = "";
        r60 = 3;
        r0 = r60;
        r0 = new int[r0];
        r42 = r0;
        r44 = new java.util.ArrayList;
        r44.<init>();
        r10 = r52.size();
        r7 = 0;
        r60 = r52.iterator();
    L_0x0042:
        r61 = r60.hasNext();
        if (r61 != 0) goto L_0x0071;
    L_0x0048:
        if (r7 == 0) goto L_0x0100;
    L_0x004a:
        r60 = "vk";
        r61 = "BAD!";
        com.vkontakte.android.Log.m526e(r60, r61);
        r60 = r52.iterator();
    L_0x0055:
        r61 = r60.hasNext();
        if (r61 != 0) goto L_0x00d1;
    L_0x005b:
        r60 = r52.size();
        r60 = r60 + -1;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 1;
        r60.setPaddingAfter(r61);
        goto L_0x0015;
    L_0x0071:
        r50 = r60.next();
        r50 = (com.vkontakte.android.ThumbAttachment) r50;
        r43 = r50.getRatio();
        r61 = -1082130432; // 0xffffffffbf800000 float:-1.0 double:NaN;
        r61 = (r43 > r61 ? 1 : (r43 == r61 ? 0 : -1));
        if (r61 != 0) goto L_0x0082;
    L_0x0081:
        r7 = 1;
    L_0x0082:
        r0 = r43;
        r0 = (double) r0;
        r61 = r0;
        r63 = 4608083138725491507; // 0x3ff3333333333333 float:4.172325E-8 double:1.2;
        r61 = (r61 > r63 ? 1 : (r61 == r63 ? 0 : -1));
        if (r61 <= 0) goto L_0x00bd;
    L_0x0090:
        r40 = 119; // 0x77 float:1.67E-43 double:5.9E-322;
    L_0x0092:
        r61 = new java.lang.StringBuilder;
        r62 = java.lang.String.valueOf(r41);
        r61.<init>(r62);
        r0 = r61;
        r1 = r40;
        r61 = r0.append(r1);
        r41 = r61.toString();
        r61 = oi(r40);
        r62 = r42[r61];
        r62 = r62 + 1;
        r42[r61] = r62;
        r61 = java.lang.Float.valueOf(r43);
        r0 = r44;
        r1 = r61;
        r0.add(r1);
        goto L_0x0042;
    L_0x00bd:
        r0 = r43;
        r0 = (double) r0;
        r61 = r0;
        r63 = 4605380978949069210; // 0x3fe999999999999a float:-1.5881868E-23 double:0.8;
        r61 = (r61 > r63 ? 1 : (r61 == r63 ? 0 : -1));
        if (r61 >= 0) goto L_0x00ce;
    L_0x00cb:
        r40 = 110; // 0x6e float:1.54E-43 double:5.43E-322;
        goto L_0x0092;
    L_0x00ce:
        r40 = 113; // 0x71 float:1.58E-43 double:5.6E-322;
        goto L_0x0092;
    L_0x00d1:
        r48 = r60.next();
        r48 = (com.vkontakte.android.ThumbAttachment) r48;
        r61 = 1124532224; // 0x43070000 float:135.0 double:5.555927395E-315;
        r61 = com.vkontakte.android.Global.scale(r61);
        r0 = r61;
        r0 = (float) r0;
        r61 = r0;
        r62 = 1120403456; // 0x42c80000 float:100.0 double:5.53552857E-315;
        r62 = com.vkontakte.android.Global.scale(r62);
        r0 = r62;
        r0 = (float) r0;
        r62 = r0;
        r63 = 0;
        r64 = 0;
        r0 = r48;
        r1 = r61;
        r2 = r62;
        r3 = r63;
        r4 = r64;
        r0.setViewSize(r1, r2, r3, r4);
        goto L_0x0055;
    L_0x0100:
        r60 = r52.iterator();
    L_0x0104:
        r61 = r60.hasNext();
        if (r61 != 0) goto L_0x01d2;
    L_0x010a:
        r60 = r52.size();
        r60 = r60 + -1;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 1;
        r60.setPaddingAfter(r61);
        r60 = r44.isEmpty();
        if (r60 != 0) goto L_0x01e3;
    L_0x0125:
        r60 = sum(r44);
        r61 = r44.size();
        r0 = r61;
        r0 = (float) r0;
        r61 = r0;
        r6 = r60 / r61;
    L_0x0134:
        r60 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r60 = com.vkontakte.android.Global.scale(r60);
        r0 = r60;
        r0 = (float) r0;
        r32 = r0;
        r60 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r60 = com.vkontakte.android.Global.scale(r60);
        r0 = r60;
        r0 = (float) r0;
        r31 = r0;
        if (r66 <= 0) goto L_0x01e7;
    L_0x014c:
        r0 = r66;
        r0 = (float) r0;
        r35 = r0;
        r0 = r67;
        r0 = (float) r0;
        r33 = r0;
    L_0x0156:
        r34 = r35 / r33;
        r60 = 1;
        r0 = r60;
        if (r10 != r0) goto L_0x0210;
    L_0x015e:
        r60 = 0;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 120; // 0x78 float:1.68E-43 double:5.93E-322;
        r60 = r60.getWidth(r61);
        r0 = r60;
        r0 = (float) r0;
        r60 = r0;
        r60 = com.vkontakte.android.Global.scale(r60);
        r0 = r60;
        r0 = (float) r0;
        r60 = r0;
        r0 = r35;
        r1 = r60;
        r35 = java.lang.Math.min(r0, r1);
        r60 = 0;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r0 = r60;
        r0 = (double) r0;
        r60 = r0;
        r62 = 4602678819172646912; // 0x3fe0000000000000 float:0.0 double:0.5;
        r60 = (r60 > r62 ? 1 : (r60 == r62 ? 0 : -1));
        if (r60 <= 0) goto L_0x01ed;
    L_0x01a1:
        r60 = 0;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 0;
        r0 = r44;
        r1 = r61;
        r61 = r0.get(r1);
        r61 = (java.lang.Float) r61;
        r61 = r61.floatValue();
        r61 = r35 / r61;
        r62 = 1;
        r63 = 0;
        r0 = r60;
        r1 = r35;
        r2 = r61;
        r3 = r62;
        r4 = r63;
        r0.setViewSize(r1, r2, r3, r4);
        goto L_0x0015;
    L_0x01d2:
        r48 = r60.next();
        r48 = (com.vkontakte.android.ThumbAttachment) r48;
        r61 = 0;
        r0 = r48;
        r1 = r61;
        r0.setPaddingAfter(r1);
        goto L_0x0104;
    L_0x01e3:
        r6 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        goto L_0x0134;
    L_0x01e7:
        r35 = 1134559232; // 0x43a00000 float:320.0 double:5.605467397E-315;
        r33 = 1129447424; // 0x43520000 float:210.0 double:5.58021171E-315;
        goto L_0x0156;
    L_0x01ed:
        r60 = 0;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r61 = r61 * r35;
        r62 = 1;
        r63 = 0;
        r0 = r60;
        r1 = r35;
        r2 = r61;
        r3 = r62;
        r4 = r63;
        r0.setViewSize(r1, r2, r3, r4);
        goto L_0x0015;
    L_0x0210:
        r60 = 2;
        r0 = r60;
        if (r10 != r0) goto L_0x041f;
    L_0x0216:
        r60 = "ww";
        r0 = r41;
        r1 = r60;
        r60 = r0.equals(r1);
        if (r60 == 0) goto L_0x02dd;
    L_0x0222:
        r0 = (double) r6;
        r60 = r0;
        r62 = 4608983858650965606; // 0x3ff6666666666666 float:2.720083E23 double:1.4;
        r0 = r34;
        r0 = (double) r0;
        r64 = r0;
        r62 = r62 * r64;
        r60 = (r60 > r62 ? 1 : (r60 == r62 ? 0 : -1));
        if (r60 <= 0) goto L_0x02dd;
    L_0x0235:
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r61 = r60.floatValue();
        r60 = 0;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r61 - r60;
        r0 = r60;
        r0 = (double) r0;
        r60 = r0;
        r62 = 4596373779694328218; // 0x3fc999999999999a float:-1.5881868E-23 double:0.2;
        r60 = (r60 > r62 ? 1 : (r60 == r62 ? 0 : -1));
        if (r60 >= 0) goto L_0x02dd;
    L_0x0265:
        r55 = r35;
        r60 = 0;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r61 = r55 / r60;
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r55 / r60;
        r62 = r33 - r31;
        r63 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r62 = r62 / r63;
        r0 = r60;
        r1 = r62;
        r60 = java.lang.Math.min(r0, r1);
        r0 = r61;
        r1 = r60;
        r16 = java.lang.Math.min(r0, r1);
        r60 = 0;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 1;
        r62 = 0;
        r0 = r60;
        r1 = r55;
        r2 = r16;
        r3 = r61;
        r4 = r62;
        r0.setViewSize(r1, r2, r3, r4);
        r60 = 1;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 0;
        r62 = 0;
        r0 = r60;
        r1 = r55;
        r2 = r16;
        r3 = r61;
        r4 = r62;
        r0.setViewSize(r1, r2, r3, r4);
        goto L_0x0015;
    L_0x02dd:
        r60 = "ww";
        r0 = r41;
        r1 = r60;
        r60 = r0.equals(r1);
        if (r60 != 0) goto L_0x02f5;
    L_0x02e9:
        r60 = "qq";
        r0 = r41;
        r1 = r60;
        r60 = r0.equals(r1);
        if (r60 == 0) goto L_0x036b;
    L_0x02f5:
        r60 = r35 - r32;
        r61 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r55 = r60 / r61;
        r60 = 0;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r61 = r55 / r60;
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r55 / r60;
        r0 = r60;
        r1 = r33;
        r60 = java.lang.Math.min(r0, r1);
        r0 = r61;
        r1 = r60;
        r16 = java.lang.Math.min(r0, r1);
        r60 = 0;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 0;
        r62 = 0;
        r0 = r60;
        r1 = r55;
        r2 = r16;
        r3 = r61;
        r4 = r62;
        r0.setViewSize(r1, r2, r3, r4);
        r60 = 1;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 0;
        r62 = 0;
        r0 = r60;
        r1 = r55;
        r2 = r16;
        r3 = r61;
        r4 = r62;
        r0.setViewSize(r1, r2, r3, r4);
        goto L_0x0015;
    L_0x036b:
        r61 = r35 - r32;
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r61 = r61 / r60;
        r62 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r60 = 0;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r62 = r62 / r60;
        r63 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r63 / r60;
        r60 = r60 + r62;
        r56 = r61 / r60;
        r60 = r35 - r56;
        r57 = r60 - r32;
        r60 = 0;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r61 = r56 / r60;
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r57 / r60;
        r0 = r61;
        r1 = r60;
        r60 = java.lang.Math.min(r0, r1);
        r0 = r33;
        r1 = r60;
        r16 = java.lang.Math.min(r0, r1);
        r60 = 0;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 0;
        r62 = 0;
        r0 = r60;
        r1 = r56;
        r2 = r16;
        r3 = r61;
        r4 = r62;
        r0.setViewSize(r1, r2, r3, r4);
        r60 = 1;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 0;
        r62 = 0;
        r0 = r60;
        r1 = r57;
        r2 = r16;
        r3 = r61;
        r4 = r62;
        r0.setViewSize(r1, r2, r3, r4);
        goto L_0x0015;
    L_0x041f:
        r60 = 3;
        r0 = r60;
        if (r10 != r0) goto L_0x05ee;
    L_0x0425:
        r60 = "www";
        r0 = r41;
        r1 = r60;
        r60 = r0.equals(r1);
        if (r60 == 0) goto L_0x04e7;
    L_0x0431:
        r55 = r35;
        r60 = 0;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r55 / r60;
        r61 = r33 - r31;
        r62 = 1059648963; // 0x3f28f5c3 float:0.66 double:5.235361493E-315;
        r61 = r61 * r62;
        r20 = java.lang.Math.min(r60, r61);
        r60 = 0;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 1;
        r62 = 0;
        r0 = r60;
        r1 = r55;
        r2 = r20;
        r3 = r61;
        r4 = r62;
        r0.setViewSize(r1, r2, r3, r4);
        r60 = r35 - r32;
        r61 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r55 = r60 / r61;
        r60 = r33 - r20;
        r61 = r60 - r31;
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r62 = r55 / r60;
        r60 = 2;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r55 / r60;
        r0 = r62;
        r1 = r60;
        r60 = java.lang.Math.min(r0, r1);
        r0 = r61;
        r1 = r60;
        r16 = java.lang.Math.min(r0, r1);
        r60 = 1;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 0;
        r62 = 0;
        r0 = r60;
        r1 = r55;
        r2 = r16;
        r3 = r61;
        r4 = r62;
        r0.setViewSize(r1, r2, r3, r4);
        r60 = 2;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 0;
        r62 = 0;
        r0 = r60;
        r1 = r55;
        r2 = r16;
        r3 = r61;
        r4 = r62;
        r0.setViewSize(r1, r2, r3, r4);
        goto L_0x0015;
    L_0x04e7:
        r0 = r33;
        r0 = (int) r0;
        r16 = r0;
        r0 = r16;
        r0 = (float) r0;
        r61 = r0;
        r60 = 0;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r60 * r61;
        r0 = r60;
        r0 = (double) r0;
        r60 = r0;
        r62 = r35 - r32;
        r0 = r62;
        r0 = (double) r0;
        r62 = r0;
        r64 = 4604930618986332160; // 0x3fe8000000000000 float:0.0 double:0.75;
        r62 = r62 * r64;
        r60 = java.lang.Math.min(r60, r62);
        r0 = r60;
        r0 = (int) r0;
        r59 = r0;
        r60 = 0;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r0 = r59;
        r0 = (float) r0;
        r61 = r0;
        r0 = r16;
        r0 = (float) r0;
        r62 = r0;
        r63 = 0;
        r64 = 0;
        r60.setViewSize(r61, r62, r63, r64);
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r61 = r33 - r31;
        r61 = r61 * r60;
        r60 = 2;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r62 = r60.floatValue();
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r60 + r62;
        r18 = r61 / r60;
        r60 = r33 - r18;
        r17 = r60 - r31;
        r0 = r59;
        r0 = (float) r0;
        r60 = r0;
        r60 = r35 - r60;
        r61 = r60 - r32;
        r60 = 2;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r62 = r18 * r60;
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r60 * r17;
        r0 = r62;
        r1 = r60;
        r60 = java.lang.Math.min(r0, r1);
        r0 = r61;
        r1 = r60;
        r55 = java.lang.Math.min(r0, r1);
        r60 = 1;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 0;
        r62 = 1;
        r0 = r60;
        r1 = r55;
        r2 = r17;
        r3 = r61;
        r4 = r62;
        r0.setViewSize(r1, r2, r3, r4);
        r60 = 2;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r61 = 0;
        r62 = 1;
        r0 = r60;
        r1 = r55;
        r2 = r18;
        r3 = r61;
        r4 = r62;
        r0.setViewSize(r1, r2, r3, r4);
        goto L_0x0015;
    L_0x05ee:
        r60 = 4;
        r0 = r60;
        if (r10 != r0) goto L_0x08c2;
    L_0x05f4:
        r60 = "wwww";
        r0 = r41;
        r1 = r60;
        r60 = r0.equals(r1);
        if (r60 == 0) goto L_0x075a;
    L_0x0600:
        r0 = r35;
        r0 = (int) r0;
        r55 = r0;
        r0 = r55;
        r0 = (float) r0;
        r61 = r0;
        r60 = 0;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r61 / r60;
        r0 = r60;
        r0 = (double) r0;
        r60 = r0;
        r62 = r33 - r31;
        r0 = r62;
        r0 = (double) r0;
        r62 = r0;
        r64 = 4604119971053405471; // 0x3fe51eb851eb851f float:1.26443839E11 double:0.66;
        r62 = r62 * r64;
        r60 = java.lang.Math.min(r60, r62);
        r0 = r60;
        r0 = (int) r0;
        r20 = r0;
        r60 = 0;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r0 = r55;
        r0 = (float) r0;
        r61 = r0;
        r0 = r20;
        r0 = (float) r0;
        r62 = r0;
        r63 = 1;
        r64 = 0;
        r60.setViewSize(r61, r62, r63, r64);
        r60 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r60 = r60 * r32;
        r61 = r35 - r60;
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r62 = r60.floatValue();
        r60 = 2;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r62 = r62 + r60;
        r60 = 3;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r60 + r62;
        r60 = r61 / r60;
        r0 = r60;
        r0 = (int) r0;
        r16 = r0;
        r0 = r16;
        r0 = (float) r0;
        r61 = r0;
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r60 * r61;
        r0 = r60;
        r0 = (int) r0;
        r56 = r0;
        r0 = r16;
        r0 = (float) r0;
        r61 = r0;
        r60 = 2;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r60 * r61;
        r0 = r60;
        r0 = (int) r0;
        r57 = r0;
        r0 = r16;
        r0 = (float) r0;
        r61 = r0;
        r60 = 3;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r60 * r61;
        r0 = r60;
        r0 = (int) r0;
        r58 = r0;
        r0 = r20;
        r0 = (float) r0;
        r60 = r0;
        r60 = r33 - r60;
        r60 = r60 - r31;
        r0 = r16;
        r0 = (float) r0;
        r61 = r0;
        r60 = java.lang.Math.min(r60, r61);
        r0 = r60;
        r0 = (int) r0;
        r16 = r0;
        r60 = 1;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r0 = r56;
        r0 = (float) r0;
        r61 = r0;
        r0 = r16;
        r0 = (float) r0;
        r62 = r0;
        r63 = 0;
        r64 = 0;
        r60.setViewSize(r61, r62, r63, r64);
        r60 = 2;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r0 = r57;
        r0 = (float) r0;
        r61 = r0;
        r0 = r16;
        r0 = (float) r0;
        r62 = r0;
        r63 = 0;
        r64 = 0;
        r60.setViewSize(r61, r62, r63, r64);
        r60 = 3;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r0 = r58;
        r0 = (float) r0;
        r61 = r0;
        r0 = r16;
        r0 = (float) r0;
        r62 = r0;
        r63 = 0;
        r64 = 0;
        r60.setViewSize(r61, r62, r63, r64);
        goto L_0x0015;
    L_0x075a:
        r0 = r33;
        r0 = (int) r0;
        r16 = r0;
        r0 = r16;
        r0 = (float) r0;
        r61 = r0;
        r60 = 0;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r60 * r61;
        r0 = r60;
        r0 = (double) r0;
        r60 = r0;
        r62 = r35 - r32;
        r0 = r62;
        r0 = (double) r0;
        r62 = r0;
        r64 = 4604119971053405471; // 0x3fe51eb851eb851f float:1.26443839E11 double:0.66;
        r62 = r62 * r64;
        r60 = java.lang.Math.min(r60, r62);
        r0 = r60;
        r0 = (int) r0;
        r59 = r0;
        r60 = 0;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r0 = r59;
        r0 = (float) r0;
        r61 = r0;
        r0 = r16;
        r0 = (float) r0;
        r62 = r0;
        r63 = 0;
        r64 = 0;
        r60.setViewSize(r61, r62, r63, r64);
        r60 = 1073741824; // 0x40000000 float:2.0 double:5.304989477E-315;
        r60 = r60 * r31;
        r61 = r33 - r60;
        r62 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r62 = r62 / r60;
        r63 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r60 = 2;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r63 / r60;
        r62 = r62 + r60;
        r63 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r60 = 3;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r63 / r60;
        r60 = r60 + r62;
        r60 = r61 / r60;
        r0 = r60;
        r0 = (int) r0;
        r55 = r0;
        r0 = r55;
        r0 = (float) r0;
        r61 = r0;
        r60 = 1;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r61 / r60;
        r0 = r60;
        r0 = (int) r0;
        r17 = r0;
        r0 = r55;
        r0 = (float) r0;
        r61 = r0;
        r60 = 2;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r61 / r60;
        r0 = r60;
        r0 = (int) r0;
        r18 = r0;
        r0 = r55;
        r0 = (float) r0;
        r61 = r0;
        r60 = 3;
        r0 = r44;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (java.lang.Float) r60;
        r60 = r60.floatValue();
        r60 = r61 / r60;
        r60 = r60 + r31;
        r0 = r60;
        r0 = (int) r0;
        r19 = r0;
        r0 = r59;
        r0 = (float) r0;
        r60 = r0;
        r60 = r35 - r60;
        r60 = r60 - r32;
        r0 = r55;
        r0 = (float) r0;
        r61 = r0;
        r60 = java.lang.Math.min(r60, r61);
        r0 = r60;
        r0 = (int) r0;
        r55 = r0;
        r60 = 1;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r0 = r55;
        r0 = (float) r0;
        r61 = r0;
        r0 = r17;
        r0 = (float) r0;
        r62 = r0;
        r63 = 0;
        r64 = 1;
        r60.setViewSize(r61, r62, r63, r64);
        r60 = 2;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r0 = r55;
        r0 = (float) r0;
        r61 = r0;
        r0 = r18;
        r0 = (float) r0;
        r62 = r0;
        r63 = 0;
        r64 = 1;
        r60.setViewSize(r61, r62, r63, r64);
        r60 = 3;
        r0 = r52;
        r1 = r60;
        r60 = r0.get(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r0 = r55;
        r0 = (float) r0;
        r61 = r0;
        r0 = r19;
        r0 = (float) r0;
        r62 = r0;
        r63 = 0;
        r64 = 1;
        r60.setViewSize(r61, r62, r63, r64);
        goto L_0x0015;
    L_0x08c2:
        r45 = new java.util.ArrayList;
        r45.<init>();
        r0 = (double) r6;
        r60 = r0;
        r62 = 4607632778762754458; // 0x3ff199999999999a float:-1.5881868E-23 double:1.1;
        r60 = (r60 > r62 ? 1 : (r60 == r62 ? 0 : -1));
        if (r60 <= 0) goto L_0x09b1;
    L_0x08d3:
        r61 = r44.iterator();
    L_0x08d7:
        r60 = r61.hasNext();
        if (r60 != 0) goto L_0x0990;
    L_0x08dd:
        r54 = new java.util.HashMap;
        r54.<init>();
        r60 = new java.lang.StringBuilder;
        r15 = r10;
        r61 = java.lang.String.valueOf(r10);
        r60.<init>(r61);
        r60 = r60.toString();
        r61 = 1;
        r0 = r61;
        r0 = new float[r0];
        r61 = r0;
        r62 = 0;
        r0 = r45;
        r1 = r35;
        r2 = r32;
        r63 = calculateMultiThumbsHeight(r0, r1, r2);
        r61[r62] = r63;
        r0 = r54;
        r1 = r60;
        r2 = r61;
        r0.put(r1, r2);
        r15 = 1;
    L_0x0910:
        r60 = r10 + -1;
        r0 = r60;
        if (r15 <= r0) goto L_0x09db;
    L_0x0916:
        r15 = 1;
    L_0x0917:
        r60 = r10 + -2;
        r0 = r60;
        if (r15 <= r0) goto L_0x0a3f;
    L_0x091d:
        r36 = 0;
        r37 = 0;
        r38 = 0;
        r25 = r54.keySet();
        r61 = r25.iterator();
    L_0x092b:
        r60 = r61.hasNext();
        if (r60 != 0) goto L_0x0adf;
    L_0x0931:
        r8 = 0;
        r53 = r52.clone();
        r53 = (java.util.ArrayList) r53;
        r46 = r45.clone();
        r46 = (java.util.ArrayList) r46;
        r60 = ",";
        r0 = r36;
        r1 = r60;
        r9 = r0.split(r1);
        r0 = r54;
        r1 = r36;
        r39 = r0.get(r1);
        r39 = (float[]) r39;
        r0 = r9.length;
        r60 = r0;
        r27 = r60 + -1;
        r24 = 0;
        r22 = 0;
    L_0x095b:
        r0 = r9.length;
        r60 = r0;
        r0 = r22;
        r1 = r60;
        if (r0 >= r1) goto L_0x0015;
    L_0x0964:
        r60 = r9[r22];
        r28 = java.lang.Integer.parseInt(r60);
        r30 = new java.util.ArrayList;
        r30.<init>();
        r23 = 0;
    L_0x0971:
        r0 = r23;
        r1 = r28;
        if (r0 < r1) goto L_0x0b7d;
    L_0x0977:
        r29 = r39[r24];
        r24 = r24 + 1;
        r60 = r30.size();
        r26 = r60 + -1;
        r23 = 0;
    L_0x0983:
        r60 = r30.size();
        r0 = r23;
        r1 = r60;
        if (r0 < r1) goto L_0x0b94;
    L_0x098d:
        r22 = r22 + 1;
        goto L_0x095b;
    L_0x0990:
        r60 = r61.next();
        r60 = (java.lang.Float) r60;
        r43 = r60.floatValue();
        r60 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r0 = r60;
        r1 = r43;
        r60 = java.lang.Math.max(r0, r1);
        r60 = java.lang.Float.valueOf(r60);
        r0 = r45;
        r1 = r60;
        r0.add(r1);
        goto L_0x08d7;
    L_0x09b1:
        r61 = r44.iterator();
    L_0x09b5:
        r60 = r61.hasNext();
        if (r60 == 0) goto L_0x08dd;
    L_0x09bb:
        r60 = r61.next();
        r60 = (java.lang.Float) r60;
        r43 = r60.floatValue();
        r60 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r0 = r60;
        r1 = r43;
        r60 = java.lang.Math.min(r0, r1);
        r60 = java.lang.Float.valueOf(r60);
        r0 = r45;
        r1 = r60;
        r0.add(r1);
        goto L_0x09b5;
    L_0x09db:
        r60 = new java.lang.StringBuilder;
        r61 = java.lang.String.valueOf(r15);
        r60.<init>(r61);
        r61 = ",";
        r60 = r60.append(r61);
        r47 = r10 - r15;
        r0 = r60;
        r1 = r47;
        r60 = r0.append(r1);
        r60 = r60.toString();
        r61 = 2;
        r0 = r61;
        r0 = new float[r0];
        r61 = r0;
        r62 = 0;
        r63 = 0;
        r0 = r45;
        r1 = r63;
        r63 = r0.subList(r1, r15);
        r0 = r63;
        r1 = r35;
        r2 = r32;
        r63 = calculateMultiThumbsHeight(r0, r1, r2);
        r61[r62] = r63;
        r62 = 1;
        r63 = r45.size();
        r0 = r45;
        r1 = r63;
        r63 = r0.subList(r15, r1);
        r0 = r63;
        r1 = r35;
        r2 = r32;
        r63 = calculateMultiThumbsHeight(r0, r1, r2);
        r61[r62] = r63;
        r0 = r54;
        r1 = r60;
        r2 = r61;
        r0.put(r1, r2);
        r15 = r15 + 1;
        goto L_0x0910;
    L_0x0a3f:
        r47 = 1;
    L_0x0a41:
        r60 = r10 - r15;
        r60 = r60 + -1;
        r0 = r47;
        r1 = r60;
        if (r0 <= r1) goto L_0x0a4f;
    L_0x0a4b:
        r15 = r15 + 1;
        goto L_0x0917;
    L_0x0a4f:
        r60 = new java.lang.StringBuilder;
        r61 = java.lang.String.valueOf(r15);
        r60.<init>(r61);
        r61 = ",";
        r60 = r60.append(r61);
        r0 = r60;
        r1 = r47;
        r60 = r0.append(r1);
        r61 = ",";
        r60 = r60.append(r61);
        r61 = r10 - r15;
        r49 = r61 - r47;
        r0 = r60;
        r1 = r49;
        r60 = r0.append(r1);
        r60 = r60.toString();
        r61 = 3;
        r0 = r61;
        r0 = new float[r0];
        r61 = r0;
        r62 = 0;
        r63 = 0;
        r0 = r45;
        r1 = r63;
        r63 = r0.subList(r1, r15);
        r0 = r63;
        r1 = r35;
        r2 = r32;
        r63 = calculateMultiThumbsHeight(r0, r1, r2);
        r61[r62] = r63;
        r62 = 1;
        r63 = r15 + r47;
        r0 = r45;
        r1 = r63;
        r63 = r0.subList(r15, r1);
        r0 = r63;
        r1 = r35;
        r2 = r32;
        r63 = calculateMultiThumbsHeight(r0, r1, r2);
        r61[r62] = r63;
        r62 = 2;
        r63 = r15 + r47;
        r64 = r45.size();
        r0 = r45;
        r1 = r63;
        r2 = r64;
        r63 = r0.subList(r1, r2);
        r0 = r63;
        r1 = r35;
        r2 = r32;
        r63 = calculateMultiThumbsHeight(r0, r1, r2);
        r61[r62] = r63;
        r0 = r54;
        r1 = r60;
        r2 = r61;
        r0.put(r1, r2);
        r47 = r47 + 1;
        goto L_0x0a41;
    L_0x0adf:
        r11 = r61.next();
        r11 = (java.lang.String) r11;
        r0 = r54;
        r21 = r0.get(r11);
        r21 = (float[]) r21;
        r0 = r21;
        r0 = r0.length;
        r60 = r0;
        r60 = r60 + -1;
        r0 = r60;
        r0 = (float) r0;
        r60 = r0;
        r13 = r31 * r60;
        r0 = r21;
        r0 = r0.length;
        r62 = r0;
        r60 = 0;
    L_0x0b02:
        r0 = r60;
        r1 = r62;
        if (r0 < r1) goto L_0x0b76;
    L_0x0b08:
        r60 = r13 - r33;
        r12 = java.lang.Math.abs(r60);
        r60 = 44;
        r0 = r60;
        r60 = r11.indexOf(r0);
        r62 = -1;
        r0 = r60;
        r1 = r62;
        if (r0 == r1) goto L_0x0b6a;
    L_0x0b1e:
        r60 = ",";
        r0 = r60;
        r14 = r11.split(r0);
        r60 = 0;
        r60 = r14[r60];
        r60 = java.lang.Integer.parseInt(r60);
        r62 = 1;
        r62 = r14[r62];
        r62 = java.lang.Integer.parseInt(r62);
        r0 = r60;
        r1 = r62;
        if (r0 > r1) goto L_0x0b5d;
    L_0x0b3c:
        r0 = r14.length;
        r60 = r0;
        r62 = 2;
        r0 = r60;
        r1 = r62;
        if (r0 <= r1) goto L_0x0b6a;
    L_0x0b47:
        r60 = 1;
        r60 = r14[r60];
        r60 = java.lang.Integer.parseInt(r60);
        r62 = 2;
        r62 = r14[r62];
        r62 = java.lang.Integer.parseInt(r62);
        r0 = r60;
        r1 = r62;
        if (r0 <= r1) goto L_0x0b6a;
    L_0x0b5d:
        r0 = (double) r12;
        r62 = r0;
        r64 = 4607632778762754458; // 0x3ff199999999999a float:-1.5881868E-23 double:1.1;
        r62 = r62 * r64;
        r0 = r62;
        r12 = (float) r0;
    L_0x0b6a:
        if (r36 == 0) goto L_0x0b70;
    L_0x0b6c:
        r60 = (r12 > r37 ? 1 : (r12 == r37 ? 0 : -1));
        if (r60 >= 0) goto L_0x092b;
    L_0x0b70:
        r36 = r11;
        r37 = r12;
        goto L_0x092b;
    L_0x0b76:
        r16 = r21[r60];
        r13 = r13 + r16;
        r60 = r60 + 1;
        goto L_0x0b02;
    L_0x0b7d:
        r60 = 0;
        r0 = r53;
        r1 = r60;
        r60 = r0.remove(r1);
        r60 = (com.vkontakte.android.ThumbAttachment) r60;
        r0 = r30;
        r1 = r60;
        r0.add(r1);
        r23 = r23 + 1;
        goto L_0x0971;
    L_0x0b94:
        r0 = r30;
        r1 = r23;
        r50 = r0.get(r1);
        r50 = (com.vkontakte.android.ThumbAttachment) r50;
        r60 = 0;
        r0 = r46;
        r1 = r60;
        r60 = r0.remove(r1);
        r60 = (java.lang.Float) r60;
        r51 = r60.floatValue();
        r60 = r51 * r29;
        r0 = r60;
        r0 = (int) r0;
        r60 = r0;
        r0 = r60;
        r0 = (float) r0;
        r61 = r0;
        r0 = r29;
        r0 = (int) r0;
        r60 = r0;
        r0 = r60;
        r0 = (float) r0;
        r62 = r0;
        r0 = r23;
        r1 = r26;
        if (r0 != r1) goto L_0x0bdf;
    L_0x0bca:
        r60 = 1;
    L_0x0bcc:
        r63 = 0;
        r0 = r50;
        r1 = r61;
        r2 = r62;
        r3 = r60;
        r4 = r63;
        r0.setViewSize(r1, r2, r3, r4);
        r23 = r23 + 1;
        goto L_0x0983;
    L_0x0bdf:
        r60 = 0;
        goto L_0x0bcc;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.ZhukovLayout.processThumbs(int, int, java.util.ArrayList):void");
    }

    private static int oi(char o) {
        switch (o) {
            case 'n':
                return 1;
            case 'q':
                return 2;
            default:
                return 0;
        }
    }

    private static float sum(List<Float> a) {
        float sum = 0.0f;
        for (Float floatValue : a) {
            sum += floatValue.floatValue();
        }
        return sum;
    }

    private static float calculateMultiThumbsHeight(List<Float> ratios, float width, float margin) {
        return (width - (((float) (ratios.size() - 1)) * margin)) / sum(ratios);
    }
}
