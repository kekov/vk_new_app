package com.vkontakte.android;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.vkontakte.android.MenuListView.Listener;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BackListener;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.fragments.SettingsFragment;
import com.vkontakte.android.ui.MenuOverlayView;
import net.hockeyapp.android.Strings;
import org.acra.ACRAConstants;

public class FragmentWrapperActivity extends SherlockFragmentActivity {
    FrameLayout contentView;
    private MenuOverlayView menu;

    /* renamed from: com.vkontakte.android.FragmentWrapperActivity.2 */
    class C02702 implements Runnable {
        C02702() {
        }

        public void run() {
            try {
                int btnId = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
                if (btnId == 0) {
                    btnId = C0436R.id.abs__action_bar_title;
                }
                if (btnId != 0) {
                    FragmentWrapperActivity.this.setTextViewMarquee((TextView) FragmentWrapperActivity.this.findViewById(btnId));
                }
                btnId = Resources.getSystem().getIdentifier("action_bar_subtitle", "id", "android");
                if (btnId == 0) {
                    btnId = C0436R.id.abs__action_bar_subtitle;
                }
                if (btnId != 0) {
                    FragmentWrapperActivity.this.setTextViewMarquee((TextView) FragmentWrapperActivity.this.findViewById(btnId));
                }
                if (VERSION.SDK_INT < 17) {
                    btnId = Resources.getSystem().getIdentifier("action_bar", "id", "android");
                    if (btnId == 0) {
                        btnId = C0436R.id.abs__action_bar;
                    }
                    if (btnId != 0) {
                        ViewGroup vg = (ViewGroup) FragmentWrapperActivity.this.findViewById(btnId);
                        for (int i = 0; i < vg.getChildCount(); i++) {
                            if (vg.getChildAt(i) instanceof LinearLayout) {
                                vg.getChildAt(i).setBackgroundDrawable(null);
                                return;
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    /* renamed from: com.vkontakte.android.FragmentWrapperActivity.1 */
    class C12871 implements Listener {
        C12871() {
        }

        public void onUserSelected(int id, boolean allowBack) {
            Bundle args = new Bundle();
            args.putInt("id", id);
            FragmentWrapperActivity.this.openFromMenu("ProfileFragment", args, allowBack);
        }

        public void onMenuItemSelected(int item, boolean allowBack) {
            String fragment = ACRAConstants.DEFAULT_STRING_VALUE;
            Bundle args = new Bundle();
            switch (item) {
                case -2000000000:
                    try {
                        fragment = MenuListView.reminderInfo.getCharSequence("_class").toString();
                        args = (Bundle) MenuListView.reminderInfo.clone();
                        break;
                    } catch (Throwable x) {
                        Log.m532w("vk", x);
                        break;
                    }
                case ValidationActivity.VRESULT_NONE /*0*/:
                    fragment = "ProfileFragment";
                    args.putInt("id", Global.uid);
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    fragment = "NewsFragment";
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    fragment = "FeedbackFragment";
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    fragment = "DialogsFragment";
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    fragment = "FriendsFragment";
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    fragment = "GroupsFragment";
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    fragment = "PhotoAlbumsListFragment";
                    args.putBoolean("news", true);
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    fragment = "VideoListFragment";
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    fragment = "AudioListFragment";
                    break;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    fragment = "FaveFragment";
                    break;
                case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                    fragment = "BrowseUsersFragment";
                    break;
                case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                    if (VERSION.SDK_INT >= 11) {
                        fragment = "_settings";
                        break;
                    }
                    FragmentWrapperActivity.this.startActivity(new Intent(FragmentWrapperActivity.this, SettingsActivity.class));
                    FragmentWrapperActivity.this.menu.closeMenu();
                    return;
            }
            if (fragment != null && fragment.length() > 0) {
                FragmentWrapperActivity.this.openFromMenu(fragment, args, allowBack);
            }
        }

        public void onCommunitySelected(int id, boolean allowBack) {
            Bundle args = new Bundle();
            args.putInt("id", -id);
            FragmentWrapperActivity.this.openFromMenu("ProfileFragment", args, allowBack);
        }
    }

    public void onCreate(Bundle b) {
        if (getIntent().getBooleanExtra("overlaybar", false)) {
            requestWindowFeature(9);
        }
        super.onCreate(b);
        String fn = getIntent().getStringExtra("class");
        getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
        if (VERSION.SDK_INT < 9) {
            getWindow().setFormat(1);
        }
        if (getIntent().hasExtra("in_anim") || getIntent().hasExtra("out_anim")) {
            overridePendingTransition(getIntent().getIntExtra("in_anim", 0), getIntent().getIntExtra("out_anim", 0));
        }
        this.contentView = new FrameLayout(this);
        this.contentView.setId(C0436R.id.fragment_wrapper);
        setContentView(this.contentView);
        initFragment();
        if (!isTaskRoot()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if (!("PhotoViewerFragment".equals(fn) || "AudioPlayerFragment".equals(fn))) {
            setupMenu();
        }
        setTitleMarquee();
    }

    protected void initFragment() {
        try {
            String fn = getIntent().getStringExtra("class");
            if (!"_settings".equals(fn)) {
                SherlockFragment fragment = (SherlockFragment) Class.forName("com.vkontakte.android.fragments." + fn).newInstance();
                fragment.setArguments(getIntent().getBundleExtra("args"));
                getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, fragment, "news").commit();
            } else if (VERSION.SDK_INT < 14) {
                startActivity(new Intent(this, SettingsActivity.class));
                finish();
            } else {
                getSupportFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, new SherlockFragment(), "content").commit();
                getFragmentManager().beginTransaction().replace(C0436R.id.fragment_wrapper, new SettingsFragment(), "content").commit();
            }
        } catch (Throwable x) {
            Log.m532w("vk", x);
            Toast.makeText(this, C0436R.string.error, 0).show();
            finish();
        }
    }

    public void onConfigurationChanged(Configuration cfg) {
        super.onConfigurationChanged(cfg);
        setTitleMarquee();
        updateMenuMode();
    }

    private void setupMenu() {
        if ((getSharedPreferences(null, 0).getInt("intro", 0) & 3) <= 0) {
            this.menu = new MenuOverlayView(this);
            FrameLayout decorView = (FrameLayout) getWindow().getDecorView();
            View vr = decorView.getChildAt(0);
            decorView.removeViewAt(0);
            this.menu.addView(vr);
            decorView.addView(this.menu);
            this.menu.getListView().setListener(new C12871());
            updateMenuMode();
        }
    }

    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(C0436R.id.fragment_wrapper);
        if (f == null || !(f instanceof BackListener) || !((BackListener) f).onBackPressed()) {
            super.onBackPressed();
        }
    }

    private void openFromMenu(String fclass, Bundle args, boolean back) {
        if (back) {
            this.menu.closeMenu();
            Navigate.to(fclass, args, this);
            return;
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("class", fclass);
        intent.putExtra("args", args);
        intent.addFlags(67108864);
        startActivity(intent);
    }

    private void updateMenuMode() {
        if (Global.isTablet && this.menu != null) {
            if (getResources().getDisplayMetrics().widthPixels <= getResources().getDisplayMetrics().heightPixels || (getResources().getConfiguration().screenLayout & 15) != 4) {
                this.menu.setMode(0);
            } else {
                this.menu.setMode(2);
            }
        }
    }

    public void setTitle(CharSequence title) {
        super.setTitle(title);
        setTitleMarquee();
    }

    public void setTitle(int res) {
        super.setTitle(res);
        setTitleMarquee();
    }

    public void onPause() {
        super.onPause();
        AppStateTracker.onActivityPaused();
    }

    public void onResume() {
        super.onResume();
        AppStateTracker.onActivityResumed(this);
    }

    private void setTitleMarquee() {
        if (this.contentView != null) {
            this.contentView.postDelayed(new C02702(), 100);
        }
    }

    private void setTextViewMarquee(TextView t) {
        t.setEllipsize(TruncateAt.MARQUEE);
        t.setSelected(true);
        t.setHorizontalFadingEdgeEnabled(true);
        t.setFadingEdgeLength(Global.scale(10.0f));
        t.setMarqueeRepeatLimit(2);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        }
        onBackPressed();
        return true;
    }
}
