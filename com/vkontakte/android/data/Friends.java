package com.vkontakte.android.data;

import android.content.Intent;
import android.support.v4.util.LruCache;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.NetworkStateReceiver;
import com.vkontakte.android.SearchIndexer;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.FriendsGet;
import com.vkontakte.android.api.FriendsGet.Callback;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.UsersGet;
import com.vkontakte.android.cache.Cache;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;
import org.acra.ACRAConstants;

public class Friends {
    public static final String ACTION_FRIEND_LIST_CHANGED = "com.vkontakte.android.FRIEND_LIST_CHANGED";
    public static final String ACTION_FRIEND_REQUESTS_CHANGED = "com.vkontakte.android.FRIEND_REQUESTS_CHANGED";
    public static final int CASE_ABL = 5;
    public static final int CASE_ACC = 3;
    public static final int CASE_DAT = 2;
    public static final int CASE_GEN = 1;
    public static final int CASE_INS = 4;
    public static final int CASE_NOM = 0;
    private static final boolean DEBUG = false;
    public static final int IMPORT_SERVICE_CONTACTS = 0;
    public static final int IMPORT_SERVICE_FACEBOOK = 2;
    public static final int IMPORT_SERVICE_GOOGLE = 1;
    public static final int IMPORT_SERVICE_MAIL = 3;
    private static ArrayList<UserProfile> friends;
    private static ArrayList<Integer> hints;
    private static SearchIndexer<UserProfile> index;
    private static ArrayList<Folder> lists;
    private static boolean loadedOnlines;
    private static boolean loading;
    private static Semaphore semaphore;
    private static LruCache<Integer, UserProfile> users;

    /* renamed from: com.vkontakte.android.data.Friends.1 */
    class C05611 implements Runnable {
        private final /* synthetic */ boolean val$forceNetwork;

        /* renamed from: com.vkontakte.android.data.Friends.1.1 */
        class C14131 implements Callback {
            C14131() {
            }

            public void success(ArrayList<UserProfile> list) {
                try {
                    Friends.semaphore.acquire();
                } catch (Exception e) {
                }
                Friends.friends.clear();
                Friends.friends.addAll(list);
                Friends.semaphore.release();
                Friends.hints.clear();
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    Friends.hints.add(Integer.valueOf(((UserProfile) it.next()).uid));
                }
                Cache.updateFriends(list, true);
                Friends.index.bind(Friends.friends);
                Friends.updateIndex();
                VKApplication.context.sendBroadcast(new Intent(Friends.ACTION_FRIEND_LIST_CHANGED));
                Friends.loadedOnlines = true;
                Friends.loading = Friends.DEBUG;
            }

            public void fail(int ecode, String emsg) {
                Friends.friends = Cache.getFriends();
                Log.m530w("vk", "reload friends failed, got " + Friends.friends.size() + " from cache");
                Friends.index.bind(Friends.friends);
                Friends.updateIndex();
                VKApplication.context.sendBroadcast(new Intent(Friends.ACTION_FRIEND_LIST_CHANGED));
                Friends.loadedOnlines = true;
                Friends.loading = Friends.DEBUG;
            }
        }

        C05611(boolean z) {
            this.val$forceNetwork = z;
        }

        public void run() {
            ArrayList<UserProfile> result = new ArrayList();
            if (!this.val$forceNetwork) {
                result = Cache.getFriends();
            }
            if (this.val$forceNetwork || result.size() == 0) {
                Friends.loading = true;
                new FriendsGet(Global.uid, Friends.DEBUG).setCallback(new C14131()).setBackground(true).exec();
                return;
            }
            try {
                Friends.semaphore.acquire();
            } catch (Exception e) {
            }
            HashMap<Integer, Integer> onlines = new HashMap();
            Iterator it = Friends.friends.iterator();
            while (it.hasNext()) {
                UserProfile p = (UserProfile) it.next();
                onlines.put(Integer.valueOf(p.uid), Integer.valueOf(p.online));
            }
            Friends.friends = result;
            Iterator it2 = Friends.friends.iterator();
            while (it2.hasNext()) {
                p = (UserProfile) it2.next();
                if (onlines.containsKey(Integer.valueOf(p.uid))) {
                    p.online = ((Integer) onlines.get(Integer.valueOf(p.uid))).intValue();
                }
            }
            Friends.semaphore.release();
            Friends.index.bind(Friends.friends);
            Friends.updateIndex();
            VKApplication.context.sendBroadcast(new Intent(Friends.ACTION_FRIEND_LIST_CHANGED));
        }
    }

    /* renamed from: com.vkontakte.android.data.Friends.2 */
    class C05622 implements Runnable {
        private final /* synthetic */ GetUsersCallback val$callback;
        private final /* synthetic */ ArrayList val$ids;
        private final /* synthetic */ int val$nameCase;
        private final /* synthetic */ ArrayList val$profiles;

        /* renamed from: com.vkontakte.android.data.Friends.2.1 */
        class C14141 implements UsersGet.Callback {
            private final /* synthetic */ ArrayList val$ids;
            private final /* synthetic */ int val$nameCase;
            private final /* synthetic */ ArrayList val$profiles;

            C14141(int i, ArrayList arrayList, ArrayList arrayList2) {
                this.val$nameCase = i;
                this.val$profiles = arrayList;
                this.val$ids = arrayList2;
            }

            public void success(ArrayList<UserProfile> r) {
                Cache.updatePeers(r, true, this.val$nameCase);
                Iterator it = r.iterator();
                while (it.hasNext()) {
                    UserProfile p = (UserProfile) it.next();
                    Friends.users.put(Integer.valueOf(p.uid | (this.val$nameCase << 24)), p);
                }
                this.val$profiles.addAll(r);
            }

            public void fail(int ecode, String emsg) {
                this.val$profiles.addAll(Cache.getUsers(this.val$ids, true, this.val$nameCase));
            }
        }

        C05622(ArrayList arrayList, int i, ArrayList arrayList2, GetUsersCallback getUsersCallback) {
            this.val$ids = arrayList;
            this.val$nameCase = i;
            this.val$profiles = arrayList2;
            this.val$callback = getUsersCallback;
        }

        public void run() {
            if (this.val$ids.size() > 0) {
                ArrayList<UserProfile> fromCache = Cache.getUsers(this.val$ids, Friends.DEBUG, this.val$nameCase);
                this.val$profiles.addAll(fromCache);
                Iterator it = fromCache.iterator();
                while (it.hasNext()) {
                    UserProfile p = (UserProfile) it.next();
                    this.val$ids.remove(Integer.valueOf(p.uid));
                    Friends.users.put(Integer.valueOf(p.uid | (this.val$nameCase << 24)), p);
                }
            }
            if (this.val$ids.size() > 0) {
                new UsersGet(this.val$ids, this.val$nameCase).setCallback(new C14141(this.val$nameCase, this.val$profiles, this.val$ids)).execSync();
            }
            this.val$callback.onUsersLoaded(this.val$profiles);
        }
    }

    /* renamed from: com.vkontakte.android.data.Friends.4 */
    class C05634 implements Runnable {
        private final /* synthetic */ GetImportedContactsCallback val$callback;
        private final /* synthetic */ int val$service;

        C05634(int i, GetImportedContactsCallback getImportedContactsCallback) {
            this.val$service = i;
            this.val$callback = getImportedContactsCallback;
        }

        public void run() {
            ArrayList<UserProfile> users = Cache.getImportedContacts(this.val$service);
            HashMap<Integer, String> descs = new HashMap();
            ArrayList<Integer> reqs = new ArrayList();
            ArrayList<Integer> needProfiles = new ArrayList();
            Iterator it = users.iterator();
            while (it.hasNext()) {
                UserProfile u = (UserProfile) it.next();
                if (u.uid > 0) {
                    needProfiles.add(Integer.valueOf(u.uid));
                    descs.put(Integer.valueOf(u.uid), u.university);
                    if (u.isFriend) {
                        reqs.add(Integer.valueOf(u.uid));
                    }
                }
            }
            ArrayList<UserProfile> profiles = Cache.getUsers(needProfiles, true);
            Iterator it2 = profiles.iterator();
            while (it2.hasNext()) {
                UserProfile profile = (UserProfile) it2.next();
                profile.university = (String) descs.get(Integer.valueOf(profile.uid));
            }
            Iterator<UserProfile> itr = users.iterator();
            while (itr.hasNext()) {
                if (((UserProfile) itr.next()).uid > 0) {
                    itr.remove();
                }
            }
            this.val$callback.onUsersLoaded(profiles, users);
        }
    }

    public static class Folder {
        public int id;
        public int name;
    }

    public interface GetImportedContactsCallback {
        void onUsersLoaded(ArrayList<UserProfile> arrayList, ArrayList<UserProfile> arrayList2);
    }

    public interface GetUsersCallback {
        void onUsersLoaded(ArrayList<UserProfile> arrayList);
    }

    /* renamed from: com.vkontakte.android.data.Friends.3 */
    class C14153 implements GetUsersCallback {
        private final /* synthetic */ boolean[] val$done;
        private final /* synthetic */ Object val$lock;
        private final /* synthetic */ ArrayList val$result;

        C14153(ArrayList arrayList, boolean[] zArr, Object obj) {
            this.val$result = arrayList;
            this.val$done = zArr;
            this.val$lock = obj;
        }

        public void onUsersLoaded(ArrayList<UserProfile> users) {
            this.val$result.addAll(users);
            this.val$done[Friends.IMPORT_SERVICE_CONTACTS] = true;
            synchronized (this.val$lock) {
                this.val$lock.notify();
            }
        }
    }

    static {
        friends = Cache.getFriends();
        friends = new ArrayList();
        hints = new ArrayList();
        lists = new ArrayList();
        users = new LruCache(100);
        index = new SearchIndexer();
        loadedOnlines = DEBUG;
        semaphore = new Semaphore(IMPORT_SERVICE_GOOGLE, true);
        loading = DEBUG;
    }

    public static void reload(boolean forceNetwork) {
        if (!loading) {
            new Thread(new C05611(forceNetwork)).start();
        }
    }

    private static void updateIndex() {
        index.build();
        UserProfile me = new UserProfile();
        me.uid = Global.uid;
        String[] sp = VKApplication.context.getSharedPreferences(null, IMPORT_SERVICE_CONTACTS).getString("username", ACRAConstants.DEFAULT_STRING_VALUE).split(" ", IMPORT_SERVICE_FACEBOOK);
        me.firstName = sp[IMPORT_SERVICE_CONTACTS];
        me.lastName = sp[IMPORT_SERVICE_GOOGLE];
        me.fullName = me.firstName + " " + me.lastName;
        me.photo = VKApplication.context.getSharedPreferences(null, IMPORT_SERVICE_CONTACTS).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE);
        index.add(me);
    }

    public static boolean isFriend(int uid) {
        Iterator it = friends.iterator();
        while (it.hasNext()) {
            if (((UserProfile) it.next()).uid == uid) {
                return true;
            }
        }
        return DEBUG;
    }

    public static void getFriends(ArrayList<UserProfile> out) {
        if (!loadedOnlines && NetworkStateReceiver.isConnected()) {
            reload(true);
        }
        out.addAll(friends);
    }

    public static int getOnlineStatus(int uid) {
        try {
            semaphore.acquire();
        } catch (Exception e) {
        }
        Iterator it = friends.iterator();
        while (it.hasNext()) {
            UserProfile user = (UserProfile) it.next();
            if (user.uid == uid) {
                semaphore.release();
                return user.online;
            }
        }
        semaphore.release();
        return IMPORT_SERVICE_CONTACTS;
    }

    public static UserProfile get(int uid) {
        try {
            semaphore.acquire();
        } catch (Exception e) {
        }
        Iterator it = friends.iterator();
        while (it.hasNext()) {
            UserProfile user = (UserProfile) it.next();
            if (user.uid == uid) {
                semaphore.release();
                return user;
            }
        }
        semaphore.release();
        return null;
    }

    public static UserProfile getFromAll(int uid) {
        try {
            semaphore.acquire();
        } catch (Exception e) {
        }
        Iterator it = friends.iterator();
        while (it.hasNext()) {
            UserProfile user = (UserProfile) it.next();
            if (user.uid == uid) {
                semaphore.release();
                return user;
            }
        }
        semaphore.release();
        UserProfile p = (UserProfile) users.get(Integer.valueOf(uid));
        if (p != null) {
            return p;
        }
        Integer[] numArr = new Integer[IMPORT_SERVICE_GOOGLE];
        numArr[IMPORT_SERVICE_CONTACTS] = Integer.valueOf(uid);
        ArrayList<UserProfile> fromCache = Cache.getUsers(Arrays.asList(numArr), DEBUG);
        if (fromCache.size() > 0) {
            p = (UserProfile) fromCache.get(IMPORT_SERVICE_CONTACTS);
            users.put(Integer.valueOf(p.uid), p);
            return p;
        }
        numArr = new Integer[IMPORT_SERVICE_GOOGLE];
        numArr[IMPORT_SERVICE_CONTACTS] = Integer.valueOf(uid);
        ArrayList<UserProfile> pp = Cache.getUsers(Arrays.asList(numArr), true);
        if (pp.size() <= 0) {
            return null;
        }
        p = (UserProfile) pp.get(IMPORT_SERVICE_CONTACTS);
        users.put(Integer.valueOf(p.uid), p);
        return p;
    }

    public static void add(UserProfile user) {
        try {
            semaphore.acquire();
        } catch (Exception e) {
        }
        friends.add(user);
        semaphore.release();
        ArrayList<UserProfile> al = new ArrayList();
        al.add(user);
        hints.add(Integer.valueOf(user.uid));
        Cache.updateFriends(al, DEBUG);
        index.add(user);
        VKApplication.context.sendBroadcast(new Intent(ACTION_FRIEND_LIST_CHANGED));
    }

    public static void remove(int uid) {
        UserProfile p = get(uid);
        try {
            semaphore.acquire();
        } catch (Exception e) {
        }
        friends.remove(p);
        hints.remove(Integer.valueOf(uid));
        semaphore.release();
        Cache.removeFriend(uid);
        updateIndex();
        VKApplication.context.sendBroadcast(new Intent(ACTION_FRIEND_LIST_CHANGED));
    }

    public static void setOnlines(ArrayList<Integer> onlines, ArrayList<Integer> mobileOnlines) {
        try {
            semaphore.acquire();
        } catch (Exception e) {
        }
        Iterator it = friends.iterator();
        while (it.hasNext()) {
            UserProfile user = (UserProfile) it.next();
            if (onlines.contains(Integer.valueOf(user.uid))) {
                user.online = IMPORT_SERVICE_GOOGLE;
            } else if (mobileOnlines.contains(Integer.valueOf(user.uid))) {
                user.online = IMPORT_SERVICE_MAIL;
            } else {
                user.online = IMPORT_SERVICE_CONTACTS;
            }
        }
        semaphore.release();
    }

    public static void setOnlineStatus(int uid, int status) {
        try {
            semaphore.acquire();
        } catch (Exception e) {
        }
        Iterator it = friends.iterator();
        while (it.hasNext()) {
            UserProfile user = (UserProfile) it.next();
            if (user.uid == uid) {
                user.online = status;
                break;
            }
        }
        semaphore.release();
    }

    public static void intersect(ArrayList<UserProfile> list, ArrayList<UserProfile> result) {
        try {
            semaphore.acquire();
        } catch (Exception e) {
        }
        ArrayList<Integer> uids = new ArrayList();
        Iterator it = friends.iterator();
        while (it.hasNext()) {
            uids.add(Integer.valueOf(((UserProfile) it.next()).uid));
        }
        semaphore.release();
        it = list.iterator();
        while (it.hasNext()) {
            UserProfile p = (UserProfile) it.next();
            if (uids.contains(Integer.valueOf(p.uid))) {
                result.add(p);
            }
        }
    }

    private static UserProfile getGroup(int gid) {
        Group g = Groups.getById(Math.abs(gid));
        if (g == null) {
            return null;
        }
        UserProfile user = new UserProfile();
        user.uid = -g.id;
        user.fullName = g.name;
        user.photo = g.photo;
        return user;
    }

    public static void getUsers(List<Integer> uids, GetUsersCallback callback) {
        getUsers(uids, callback, IMPORT_SERVICE_CONTACTS);
    }

    public static void getUsers(List<Integer> uids, GetUsersCallback callback, int nameCase) {
        ArrayList<Integer> ids = new ArrayList();
        ids.addAll(uids);
        ids.remove(Integer.valueOf(IMPORT_SERVICE_CONTACTS));
        ArrayList<UserProfile> profiles = new ArrayList();
        for (Integer intValue : uids) {
            int uid = intValue.intValue();
            UserProfile p = (uid > 0 || uid < -2000000000) ? (UserProfile) users.get(Integer.valueOf((nameCase << 24) | uid)) : getGroup(uid);
            if (p != null) {
                profiles.add(p);
                ids.remove(Integer.valueOf(p.uid));
            }
        }
        if (nameCase == 0) {
            for (Integer intValue2 : uids) {
                uid = intValue2.intValue();
                if (uid >= 0) {
                    Iterator it = friends.iterator();
                    while (it.hasNext()) {
                        p = (UserProfile) it.next();
                        if (p.uid == uid) {
                            profiles.add(p);
                            ids.remove(Integer.valueOf(p.uid));
                            break;
                        }
                    }
                }
            }
        }
        if (ids.size() == 0) {
            callback.onUsersLoaded(profiles);
        } else {
            new Thread(new C05622(ids, nameCase, profiles, callback)).start();
        }
    }

    public static ArrayList<UserProfile> getUsersBlocking(List<Integer> uids) {
        return getUsersBlocking(uids, IMPORT_SERVICE_CONTACTS);
    }

    public static ArrayList<UserProfile> getUsersBlocking(List<Integer> uids, int nameCase) {
        ArrayList<UserProfile> result = new ArrayList();
        if (uids.size() != 0) {
            Object lock = new Object();
            boolean[] done = new boolean[IMPORT_SERVICE_GOOGLE];
            getUsers(uids, new C14153(result, done, lock), nameCase);
            synchronized (lock) {
                if (!done[IMPORT_SERVICE_CONTACTS]) {
                    try {
                        lock.wait();
                    } catch (Exception e) {
                    }
                }
            }
        }
        return result;
    }

    public static void getImportedContacts(int service, GetImportedContactsCallback callback) {
        new Thread(new C05634(service, callback)).start();
    }

    public static void saveImportedContacts(int service, List<UserProfile> users, List<UserProfile> notOnVk) {
        Cache.updatePeers(users, DEBUG);
        Cache.saveImportedContacts(users, service, true);
        Cache.saveImportedContacts(notOnVk, service, DEBUG);
    }

    public static void reset() {
        try {
            semaphore.acquire();
        } catch (Exception e) {
        }
        friends.clear();
        hints.clear();
        lists.clear();
        users.evictAll();
        semaphore.release();
    }

    public static List<UserProfile> search(String q) {
        return index.search(q);
    }
}
