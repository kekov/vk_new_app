package com.vkontakte.android.data;

import android.app.ActivityManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Parcelable;
import android.text.TextUtils;
import com.facebook.WebDialog;
import com.vkontakte.android.APIController;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.Attachment;
import com.vkontakte.android.ChatUser;
import com.vkontakte.android.DialogEntry;
import com.vkontakte.android.GeoAttachment;
import com.vkontakte.android.Global;
import com.vkontakte.android.ImageCache;
import com.vkontakte.android.Log;
import com.vkontakte.android.LongPollService;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.Message;
import com.vkontakte.android.Message.FwdMessage;
import com.vkontakte.android.PendingDocumentAttachment;
import com.vkontakte.android.UploaderService;
import com.vkontakte.android.UserProfile;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.MessagesDelete;
import com.vkontakte.android.api.MessagesGetChat;
import com.vkontakte.android.api.MessagesGetDialogs;
import com.vkontakte.android.api.MessagesGetDialogs.Callback;
import com.vkontakte.android.api.MessagesGetHistory;
import com.vkontakte.android.api.MessagesMarkAsRead;
import com.vkontakte.android.api.MessagesSearch;
import com.vkontakte.android.api.MessagesSend;
import com.vkontakte.android.cache.Cache;
import com.vkontakte.android.cache.MessagesAction;
import com.vkontakte.android.data.Friends.GetUsersCallback;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;
import com.vkontakte.android.ui.PendingPhotoAttachment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.Semaphore;
import org.acra.ACRAConstants;

public class Messages {
    public static final String ACTION_MESSAGE_ID_CHANGED = "com.vkontakte.android.MESSAGE_ID_CHANGED";
    public static final String ACTION_SEND_FAILED = "com.vkontakte.android.MESSAGE_SEND_FAILED";
    private static final boolean DEBUG = false;
    private static ArrayList<DialogEntry> dialogs;
    private static HashMap<Integer, ArrayList<Message>> histories;
    private static long lastUpdated;
    private static APIRequest loadDlgsReq;
    private static ArrayList<Message> pendingUploads;
    public static Semaphore updateLock;

    /* renamed from: com.vkontakte.android.data.Messages.11 */
    class AnonymousClass11 implements Runnable {
        private final /* synthetic */ int val$chatID;
        private final /* synthetic */ GetChatUsersCallback val$getChatUsersCallback;

        AnonymousClass11(int i, GetChatUsersCallback getChatUsersCallback) {
            this.val$chatID = i;
            this.val$getChatUsersCallback = getChatUsersCallback;
        }

        public void run() {
            ArrayList<ChatUser> users = Cache.getChatUsers(this.val$chatID);
            Log.m525d("vk", "get users from cache, size=" + users.size());
            if (users.size() == 0) {
                Log.m525d("vk", "cache returned empty list, fallback to api");
                Messages.getChatUsersFromApi(this.val$chatID, this.val$getChatUsersCallback);
                return;
            }
            String[] info = Cache.getChatInfo(this.val$chatID);
            this.val$getChatUsersCallback.onUsersLoaded(users, info[0], info[1]);
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.3 */
    class C05653 implements Runnable {
        private final /* synthetic */ Message val$msg;

        C05653(Message message) {
            this.val$msg = message;
        }

        public void run() {
            try {
                Messages.updateLock.acquire();
            } catch (Exception e) {
            }
            ArrayList<Message> al = new ArrayList();
            al.add(this.val$msg);
            Cache.addMessages(al);
            Messages.updateLock.release();
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.4 */
    class C05664 implements Runnable {
        private final /* synthetic */ GetDialogsCallback val$callback;
        private final /* synthetic */ int val$count;
        private final /* synthetic */ int val$offset;

        /* renamed from: com.vkontakte.android.data.Messages.4.1 */
        class C14201 implements GetUsersCallback {
            private final /* synthetic */ GetDialogsCallback val$callback;
            private final /* synthetic */ ArrayList val$result;

            C14201(ArrayList arrayList, GetDialogsCallback getDialogsCallback) {
                this.val$result = arrayList;
                this.val$callback = getDialogsCallback;
            }

            public void onUsersLoaded(ArrayList<UserProfile> users) {
                Log.m528i("vk", "On users loaded " + users);
                HashMap<Integer, UserProfile> u = new HashMap();
                Iterator it = users.iterator();
                while (it.hasNext()) {
                    UserProfile p = (UserProfile) it.next();
                    u.put(Integer.valueOf(p.uid), p);
                }
                Log.m525d("vk", "U=" + u);
                Iterator it2 = this.val$result.iterator();
                while (it2.hasNext()) {
                    DialogEntry e = (DialogEntry) it2.next();
                    Log.m528i("vk", "sender=" + e.lastMessage.sender);
                    if (e.profile.uid > 2000000000 && !e.lastMessage.out) {
                        if (u.containsKey(Integer.valueOf(e.lastMessage.sender))) {
                            e.lastMessagePhoto = ((UserProfile) u.get(Integer.valueOf(e.lastMessage.sender))).photo;
                        } else {
                            Log.m526e("vk", "Can't find photo for " + e.lastMessage.sender);
                        }
                    }
                }
                if (this.val$callback != null) {
                    this.val$callback.onDialogsLoaded(this.val$result);
                }
            }
        }

        /* renamed from: com.vkontakte.android.data.Messages.4.2 */
        class C14222 implements Callback {
            private final /* synthetic */ GetDialogsCallback val$callback;
            private final /* synthetic */ int val$count;

            /* renamed from: com.vkontakte.android.data.Messages.4.2.1 */
            class C14211 implements GetUsersCallback {
                private final /* synthetic */ GetDialogsCallback val$callback;
                private final /* synthetic */ int val$count;
                private final /* synthetic */ ArrayList val$results;

                C14211(ArrayList arrayList, GetDialogsCallback getDialogsCallback, int i) {
                    this.val$results = arrayList;
                    this.val$callback = getDialogsCallback;
                    this.val$count = i;
                }

                public void onUsersLoaded(ArrayList<UserProfile> users) {
                    HashMap<Integer, UserProfile> u = new HashMap();
                    Iterator it = users.iterator();
                    while (it.hasNext()) {
                        UserProfile p = (UserProfile) it.next();
                        u.put(Integer.valueOf(p.uid), p);
                    }
                    Iterator it2 = this.val$results.iterator();
                    while (it2.hasNext()) {
                        DialogEntry e = (DialogEntry) it2.next();
                        if (e.profile.uid > 2000000000 && !e.lastMessage.out && u.containsKey(Integer.valueOf(e.lastMessage.sender))) {
                            e.lastMessagePhoto = ((UserProfile) u.get(Integer.valueOf(e.lastMessage.sender))).photo;
                        }
                    }
                    try {
                        Messages.updateLock.acquire();
                    } catch (Exception e2) {
                    }
                    Messages.dialogs.addAll(this.val$results);
                    Messages.updateLock.release();
                    if (this.val$callback != null) {
                        ArrayList<DialogEntry> res = new ArrayList();
                        res.addAll(this.val$results.subList(0, Math.min(this.val$results.size(), this.val$count)));
                        this.val$callback.onDialogsLoaded(res);
                    }
                }
            }

            C14222(GetDialogsCallback getDialogsCallback, int i) {
                this.val$callback = getDialogsCallback;
                this.val$count = i;
            }

            public void success(int total, ArrayList<DialogEntry> results) {
                Messages.lastUpdated = System.currentTimeMillis();
                VKApplication.context.getSharedPreferences("msg", 0).edit().putLong("updated", Messages.lastUpdated).commit();
                ArrayList<Integer> uids = new ArrayList();
                Iterator it = results.iterator();
                while (it.hasNext()) {
                    DialogEntry e = (DialogEntry) it.next();
                    if (!(e.profile.uid <= 2000000000 || e.lastMessage.out || uids.contains(Integer.valueOf(e.lastMessage.sender)))) {
                        uids.add(Integer.valueOf(e.lastMessage.sender));
                    }
                }
                ArrayList<Message> msgs = new ArrayList();
                ArrayList<UserProfile> usrs = new ArrayList();
                it = results.iterator();
                while (it.hasNext()) {
                    DialogEntry dlg = (DialogEntry) it.next();
                    msgs.add(dlg.lastMessage);
                    usrs.add(dlg.profile);
                }
                Cache.addMessages(msgs);
                Cache.updatePeers(usrs, false);
                Friends.getUsers(uids, new C14211(results, this.val$callback, this.val$count));
            }

            public void fail(int ecode, String emsg) {
                if (this.val$callback != null) {
                    this.val$callback.onError(ecode, emsg);
                }
            }
        }

        C05664(int i, int i2, GetDialogsCallback getDialogsCallback) {
            this.val$count = i;
            this.val$offset = i2;
            this.val$callback = getDialogsCallback;
        }

        public void run() {
            int cacheCount = Cache.getDialogsCount();
            if (Messages.dialogs.size() < cacheCount) {
                try {
                    Messages.updateLock.acquire();
                } catch (Exception e) {
                }
                Messages.dialogs.addAll(Cache.getDialogs(Messages.dialogs.size(), Math.min(cacheCount - Messages.dialogs.size(), this.val$count)));
                if (Messages.dialogs.size() >= this.val$offset + this.val$count) {
                    ArrayList<DialogEntry> result = new ArrayList();
                    result.addAll(Messages.dialogs.subList(this.val$offset, this.val$offset + this.val$count));
                    ArrayList<Integer> uids = new ArrayList();
                    Iterator it = result.iterator();
                    while (it.hasNext()) {
                        DialogEntry e2 = (DialogEntry) it.next();
                        if (!(e2.profile.uid <= 2000000000 || e2.lastMessage.out || uids.contains(Integer.valueOf(e2.lastMessage.sender)))) {
                            uids.add(Integer.valueOf(e2.lastMessage.sender));
                        }
                    }
                    Friends.getUsers(uids, new C14201(result, this.val$callback));
                    Messages.updateLock.release();
                    return;
                }
                Messages.updateLock.release();
            }
            Messages.loadDlgsReq = new MessagesGetDialogs(this.val$offset, this.val$count * 2).setCallback(new C14222(this.val$callback, this.val$count));
            Messages.loadDlgsReq.execSync();
            Messages.loadDlgsReq = null;
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.5 */
    class C05675 implements Runnable {
        private final /* synthetic */ int val$mid;
        private final /* synthetic */ boolean val$state;

        C05675(int i, boolean z) {
            this.val$mid = i;
            this.val$state = z;
        }

        public void run() {
            try {
                Messages.updateLock.acquire();
            } catch (Exception e) {
            }
            Cache.setMessageReadState(this.val$mid, this.val$state);
            Iterator it = Messages.dialogs.iterator();
            while (it.hasNext()) {
                DialogEntry e2 = (DialogEntry) it.next();
                if (e2.lastMessage.id == this.val$mid) {
                    e2.lastMessage.readState = this.val$state;
                    break;
                }
            }
            for (Integer intValue : Messages.histories.keySet()) {
                it = ((ArrayList) Messages.histories.get(Integer.valueOf(intValue.intValue()))).iterator();
                while (it.hasNext()) {
                    Message msg = (Message) it.next();
                    if (msg.id == this.val$mid) {
                        msg.readState = this.val$state;
                        Messages.updateLock.release();
                        return;
                    }
                }
            }
            Messages.updateLock.release();
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.6 */
    class C05686 implements Runnable {
        private final /* synthetic */ GetMessagesCallback val$callback;
        private final /* synthetic */ int val$count;
        private final /* synthetic */ int val$offset;
        private final /* synthetic */ int val$peer;

        /* renamed from: com.vkontakte.android.data.Messages.6.1 */
        class C14231 implements MessagesGetHistory.Callback {
            private final /* synthetic */ GetMessagesCallback val$callback;
            private final /* synthetic */ int val$peer;
            private final /* synthetic */ ArrayList val$result;

            C14231(ArrayList arrayList, GetMessagesCallback getMessagesCallback, int i) {
                this.val$result = arrayList;
                this.val$callback = getMessagesCallback;
                this.val$peer = i;
            }

            public void success(int total, Vector<Message> msgs) {
                this.val$result.addAll(msgs);
                this.val$callback.onMessagesLoaded(this.val$result);
                Cache.addMessages(msgs);
            }

            public void fail(int ecode, String emsg) {
                this.val$callback.onError(ecode, emsg);
            }
        }

        C05686(int i, int i2, int i3, GetMessagesCallback getMessagesCallback) {
            this.val$peer = i;
            this.val$offset = i2;
            this.val$count = i3;
            this.val$callback = getMessagesCallback;
        }

        public void run() {
            Thread.currentThread().setName("GetHistory " + this.val$peer);
            try {
                LongPollService.addMessage.acquire();
            } catch (Exception e) {
            }
            ArrayList<Message> result = new ArrayList();
            if (Messages.histories.containsKey(Integer.valueOf(this.val$peer))) {
                ArrayList<Message> h = (ArrayList) Messages.histories.get(Integer.valueOf(this.val$peer));
                if (h != null && h.size() < this.val$offset && h.size() >= this.val$offset + this.val$count) {
                    result.addAll(h.subList(this.val$offset, this.val$offset + this.val$count));
                    this.val$callback.onMessagesLoaded(result);
                    Log.m528i("vk", "Returinig peer " + this.val$peer + " history from RAM");
                    LongPollService.addMessage.release();
                    return;
                }
            }
            LongPollService.addMessage.release();
            try {
                LongPollService.addMessage.acquire();
            } catch (Exception e2) {
            }
            if (Cache.getMessagesHistoryCount(this.val$peer) >= this.val$offset + this.val$count) {
                result.addAll(Cache.getMessagesHistory(this.val$peer, this.val$offset, this.val$count));
                this.val$callback.onMessagesLoaded(result);
                LongPollService.addMessage.release();
                return;
            }
            new MessagesGetHistory(this.val$peer, this.val$offset, this.val$count).setCallback(new C14231(result, this.val$callback, this.val$peer)).execSync();
            LongPollService.addMessage.release();
        }
    }

    public interface GetChatUsersCallback {
        void onUsersLoaded(ArrayList<ChatUser> arrayList, String str, String str2);
    }

    public interface GetDialogsCallback {
        void onDialogsLoaded(ArrayList<DialogEntry> arrayList);

        void onError(int i, String str);
    }

    public interface GetMessagesCallback {
        void onError(int i, String str);

        void onMessagesLoaded(ArrayList<Message> arrayList);
    }

    public interface SearchCallback {
        void onDialogsLoaded(ArrayList<DialogEntry> arrayList, int i);

        void onError(int i, String str);
    }

    /* renamed from: com.vkontakte.android.data.Messages.10 */
    class AnonymousClass10 implements GetChatUsersCallback {
        private final /* synthetic */ Message val$m;

        AnonymousClass10(Message message) {
            this.val$m = message;
        }

        public void onUsersLoaded(ArrayList<ChatUser> users, String title, String photo) {
            UserProfile p = new UserProfile();
            p.fullName = title;
            p.uid = this.val$m.peer;
            if (photo != null) {
                p.photo = photo;
            } else {
                ArrayList<String> ph = new ArrayList();
                ph.add("M");
                for (int i = 0; i < Math.min(users.size(), 4); i++) {
                    ph.add(((ChatUser) users.get(i)).user.photo);
                }
                p.photo = TextUtils.join("|", ph);
            }
            Intent intent = new Intent(LongPollService.ACTION_NEW_MESSAGE);
            intent.putExtra(LongPollService.EXTRA_MESSAGE, this.val$m);
            intent.putExtra(LongPollService.EXTRA_PEER_ID, this.val$m.peer);
            intent.putExtra("peer_profile", p);
            intent.putExtra("sender_photo", VKApplication.context.getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE));
            VKApplication.context.sendBroadcast(intent, permission.ACCESS_DATA);
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.12 */
    class AnonymousClass12 implements MessagesGetChat.Callback {
        private final /* synthetic */ int val$chatID;
        private final /* synthetic */ GetChatUsersCallback val$getChatUsersCallback;

        AnonymousClass12(int i, GetChatUsersCallback getChatUsersCallback) {
            this.val$chatID = i;
            this.val$getChatUsersCallback = getChatUsersCallback;
        }

        public void success(ArrayList<ChatUser> users, String title, String photo, int adminID) {
            Log.m528i("vk", "chat users loaded");
            UserProfile p = new UserProfile();
            p.uid = this.val$chatID + 2000000000;
            p.fullName = title;
            p.photo = photo;
            p.online = adminID;
            ArrayList<UserProfile> al = new ArrayList();
            al.add(p);
            Cache.updatePeers(al, false);
            Cache.updateChat(this.val$chatID, title, users, photo);
            this.val$getChatUsersCallback.onUsersLoaded(users, title, photo);
        }

        public void fail(int ecode, String emsg) {
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.13 */
    class AnonymousClass13 implements MessagesMarkAsRead.Callback {
        private final /* synthetic */ ArrayList val$ids;

        AnonymousClass13(ArrayList arrayList) {
            this.val$ids = arrayList;
        }

        public void success() {
            Iterator it = this.val$ids.iterator();
            while (it.hasNext()) {
                int id = ((Integer) it.next()).intValue();
                Intent intent = new Intent(LongPollService.ACTION_MESSAGE_RSTATE_CHANGED);
                intent.putExtra(LongPollService.EXTRA_MSG_ID, id);
                intent.putExtra(LongPollService.EXTRA_READ_STATE, true);
                VKApplication.context.sendBroadcast(intent, permission.ACCESS_DATA);
            }
        }

        public void fail(int ecode, String emsg) {
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.14 */
    class AnonymousClass14 implements MessagesDelete.Callback {
        private final /* synthetic */ ArrayList val$ids;

        AnonymousClass14(ArrayList arrayList) {
            this.val$ids = arrayList;
        }

        public void success() {
            Cache.deleteMessages(this.val$ids);
        }

        public void fail(int ecode, String emsg) {
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.15 */
    class AnonymousClass15 implements MessagesSearch.Callback {
        private final /* synthetic */ SearchCallback val$callback;

        /* renamed from: com.vkontakte.android.data.Messages.15.1 */
        class C14171 implements GetUsersCallback {
            private final /* synthetic */ SearchCallback val$callback;
            private final /* synthetic */ ArrayList val$chats;
            private final /* synthetic */ List val$msgs;
            private final /* synthetic */ int val$total;

            C14171(ArrayList arrayList, List list, SearchCallback searchCallback, int i) {
                this.val$chats = arrayList;
                this.val$msgs = list;
                this.val$callback = searchCallback;
                this.val$total = i;
            }

            public void onUsersLoaded(ArrayList<UserProfile> users) {
                HashMap<Integer, UserProfile> u = new HashMap();
                Iterator it = users.iterator();
                while (it.hasNext()) {
                    UserProfile p = (UserProfile) it.next();
                    u.put(Integer.valueOf(p.uid), p);
                }
                Iterator it2 = this.val$chats.iterator();
                while (it2.hasNext()) {
                    UserProfile c = (UserProfile) it2.next();
                    if (c.photo.startsWith("M")) {
                        String[] ids = c.photo.split(",");
                        ArrayList<String> ph = new ArrayList();
                        ph.add("M");
                        for (int i = 1; i < ids.length; i++) {
                            try {
                                ph.add(((UserProfile) u.get(Integer.valueOf(Integer.parseInt(ids[i])))).photo);
                            } catch (Exception e) {
                            }
                        }
                        c.photo = TextUtils.join("|", ph);
                    }
                    u.put(Integer.valueOf(c.uid), c);
                }
                ArrayList<DialogEntry> result = new ArrayList();
                for (Message m : this.val$msgs) {
                    DialogEntry e2 = new DialogEntry();
                    e2.lastMessage = m;
                    e2.lastMessagePhoto = ((UserProfile) u.get(Integer.valueOf(m.sender))).photo;
                    e2.profile = (UserProfile) u.get(Integer.valueOf(m.peer));
                    result.add(e2);
                }
                if (this.val$callback != null) {
                    this.val$callback.onDialogsLoaded(result, this.val$total);
                }
            }
        }

        AnonymousClass15(SearchCallback searchCallback) {
            this.val$callback = searchCallback;
        }

        public void success(List<Message> msgs, int total, ArrayList<UserProfile> chats) {
            ArrayList<Integer> uids = new ArrayList();
            for (Message m : msgs) {
                if (!uids.contains(Integer.valueOf(m.peer)) && m.peer < 2000000000) {
                    uids.add(Integer.valueOf(m.peer));
                }
                if (!uids.contains(Integer.valueOf(m.sender))) {
                    uids.add(Integer.valueOf(m.sender));
                }
            }
            Iterator it = chats.iterator();
            while (it.hasNext()) {
                UserProfile c = (UserProfile) it.next();
                if (c.photo.startsWith("M")) {
                    String[] ids = c.photo.split(",");
                    for (int i = 1; i < ids.length; i++) {
                        try {
                            int uid = Integer.parseInt(ids[i]);
                            if (!uids.contains(Integer.valueOf(uid))) {
                                uids.add(Integer.valueOf(uid));
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }
            Friends.getUsers(uids, new C14171(chats, msgs, this.val$callback, total));
        }

        public void fail(int ecode, String emsg) {
            if (this.val$callback != null) {
                this.val$callback.onError(ecode, emsg);
            }
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.1 */
    class C14181 implements GetUsersCallback {
        private final /* synthetic */ Message val$msg;
        private final /* synthetic */ String val$senderPhoto;

        C14181(Message message, String str) {
            this.val$msg = message;
            this.val$senderPhoto = str;
        }

        public void onUsersLoaded(ArrayList<UserProfile> users) {
            DialogEntry e = new DialogEntry();
            e.lastMessage = this.val$msg;
            e.lastMessagePhoto = this.val$senderPhoto;
            e.profile = (UserProfile) users.get(0);
            try {
                Messages.updateLock.acquire();
            } catch (Exception e2) {
            }
            Messages.dialogs.add(0, e);
            Messages.updateLock.release();
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.2 */
    class C14192 implements GetChatUsersCallback {
        private final /* synthetic */ Message val$msg;
        private final /* synthetic */ String val$senderPhoto;

        C14192(Message message, String str) {
            this.val$msg = message;
            this.val$senderPhoto = str;
        }

        public void onUsersLoaded(ArrayList<ChatUser> users, String title, String photo) {
            UserProfile p = new UserProfile();
            p.fullName = title;
            p.uid = this.val$msg.peer;
            if (photo != null) {
                p.photo = photo;
            } else {
                p.photo = Messages.createChatPhoto(users);
            }
            DialogEntry e = new DialogEntry();
            e.lastMessage = this.val$msg;
            e.lastMessagePhoto = this.val$senderPhoto;
            e.profile = p;
            try {
                Messages.updateLock.acquire();
            } catch (Exception e2) {
            }
            Messages.dialogs.add(0, e);
            Messages.updateLock.release();
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.7 */
    class C14247 implements MessagesSend.Callback {
        private final /* synthetic */ Message val$m;

        C14247(Message message) {
            this.val$m = message;
        }

        public void success(int mid) {
            Cache.setMessageID(this.val$m.id, mid);
            Intent intent = new Intent(Messages.ACTION_MESSAGE_ID_CHANGED);
            intent.putExtra("oldID", this.val$m.id);
            intent.putExtra("newID", mid);
            VKApplication.context.sendBroadcast(intent, permission.ACCESS_DATA);
            this.val$m.id = mid;
            Messages.removeFromSending(this.val$m);
            Messages.broadcastNewMessage(this.val$m);
        }

        public void fail(int ecode, String emsg) {
            Messages.removeFromSending(this.val$m);
            this.val$m.sendFailed = true;
            Messages.add(this.val$m, null, VKApplication.context.getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE));
            Intent intent = new Intent(Messages.ACTION_SEND_FAILED);
            intent.putExtra("id", this.val$m.id);
            if (ecode == 7) {
                intent.putExtra("privacy", true);
            }
            VKApplication.context.sendBroadcast(intent, permission.ACCESS_DATA);
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.8 */
    class C14258 implements MessagesSend.Callback {
        private final /* synthetic */ Message val$m;

        C14258(Message message) {
            this.val$m = message;
        }

        public void success(int mid) {
            Cache.setMessageID(this.val$m.id, mid);
            Intent intent = new Intent(Messages.ACTION_MESSAGE_ID_CHANGED);
            intent.putExtra("oldID", this.val$m.id);
            intent.putExtra("newID", mid);
            VKApplication.context.sendBroadcast(intent, permission.ACCESS_DATA);
            this.val$m.id = mid;
            Messages.removeFromSending(this.val$m);
            Messages.broadcastNewMessage(this.val$m);
        }

        public void fail(int ecode, String emsg) {
            Messages.removeFromSending(this.val$m);
            this.val$m.sendFailed = true;
            Messages.add(this.val$m, null, VKApplication.context.getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE));
            Intent intent = new Intent(Messages.ACTION_SEND_FAILED);
            intent.putExtra("id", this.val$m.id);
            if (ecode == 7) {
                intent.putExtra("privacy", true);
            }
            VKApplication.context.sendBroadcast(intent, permission.ACCESS_DATA);
        }
    }

    /* renamed from: com.vkontakte.android.data.Messages.9 */
    class C14269 implements GetUsersCallback {
        private final /* synthetic */ Message val$m;

        C14269(Message message) {
            this.val$m = message;
        }

        public void onUsersLoaded(ArrayList<UserProfile> users) {
            Intent intent = new Intent(LongPollService.ACTION_NEW_MESSAGE);
            intent.putExtra(LongPollService.EXTRA_MESSAGE, this.val$m);
            intent.putExtra(LongPollService.EXTRA_PEER_ID, this.val$m.peer);
            intent.putExtra("peer_profile", (Parcelable) users.get(0));
            intent.putExtra("sender_photo", VKApplication.context.getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE));
            VKApplication.context.sendBroadcast(intent, permission.ACCESS_DATA);
        }
    }

    static {
        histories = new HashMap();
        dialogs = new ArrayList();
        pendingUploads = new ArrayList();
        lastUpdated = -1;
        updateLock = new Semaphore(1, true);
    }

    public static void add(Message msg, UserProfile peerProfile, String senderPhoto) {
        lastUpdated = System.currentTimeMillis();
        VKApplication.context.getSharedPreferences("msg", 0).edit().putLong("updated", lastUpdated).commit();
        if (!histories.containsKey(Integer.valueOf(msg.peer))) {
            histories.put(Integer.valueOf(msg.peer), new ArrayList());
        }
        ((ArrayList) histories.get(Integer.valueOf(msg.peer))).add(msg);
        try {
            updateLock.acquire();
        } catch (Exception e) {
        }
        boolean found = false;
        Iterator it = dialogs.iterator();
        while (it.hasNext()) {
            DialogEntry e2 = (DialogEntry) it.next();
            if (e2.profile.uid == msg.peer) {
                e2.lastMessage = msg;
                e2.lastMessagePhoto = senderPhoto;
                dialogs.remove(e2);
                dialogs.add(0, e2);
                found = true;
                break;
            }
        }
        updateLock.release();
        if (!found) {
            if (peerProfile != null) {
                e2 = new DialogEntry();
                e2.lastMessage = msg;
                e2.lastMessagePhoto = senderPhoto;
                e2.profile = peerProfile;
                try {
                    updateLock.acquire();
                } catch (Exception e3) {
                }
                dialogs.add(0, e2);
                updateLock.release();
            } else {
                ArrayList<Integer> ids = new ArrayList();
                ids.add(Integer.valueOf(msg.peer));
                if (msg.peer < 2000000000) {
                    Friends.getUsers(ids, new C14181(msg, senderPhoto));
                } else {
                    getChatUsers(msg.peer - 2000000000, new C14192(msg, senderPhoto));
                }
            }
        }
        APIController.runInBg(new C05653(msg));
    }

    public static void getAllLoadedDialogs(ArrayList<DialogEntry> out) {
        out.addAll(dialogs);
    }

    public static void getDialogs(int offset, int count, GetDialogsCallback callback) {
        if (dialogs.size() >= offset + count) {
            ArrayList<DialogEntry> result = new ArrayList();
            result.addAll(dialogs.subList(offset, offset + count));
            callback.onDialogsLoaded(result);
            return;
        }
        new Thread(new C05664(count, offset, callback)).start();
    }

    public static void setReadState(int mid, boolean state) {
        APIController.runInBg(new C05675(mid, state));
    }

    public static void setReadStateUpto(int peer, int mid, boolean in) {
        try {
            updateLock.acquire();
        } catch (Exception e) {
        }
        Cache.setMessageReadStateUpto(mid, peer, true, in);
        Iterator it = dialogs.iterator();
        while (it.hasNext()) {
            DialogEntry e2 = (DialogEntry) it.next();
            if (e2.lastMessage.peer == peer && e2.lastMessage.id <= mid && e2.lastMessage.out != in) {
                e2.lastMessage.readState = true;
                break;
            }
        }
        ArrayList<Message> msgs = (ArrayList) histories.get(Integer.valueOf(peer));
        if (msgs != null) {
            it = msgs.iterator();
            while (it.hasNext()) {
                Message msg = (Message) it.next();
                if (msg.id <= mid && msg.out != in) {
                    msg.readState = true;
                }
            }
        }
        updateLock.release();
    }

    public static void getHistory(int peer, int offset, int count, GetMessagesCallback callback) {
        new Thread(new C05686(peer, offset, count, callback)).start();
    }

    public static Message send(int peer, String text, ArrayList<Attachment> attachments, ArrayList<?> fwd) {
        Iterator it;
        Message m = new Message();
        int id = VKApplication.context.getSharedPreferences("longpoll", 0).getInt("tmp_msg_id", -1);
        m.id = id;
        m.peer = peer;
        m.out = peer != Global.uid;
        m.sender = Global.uid;
        m.time = ((int) (System.currentTimeMillis() / 1000)) - Global.timeDiff;
        Log.m528i("vk", "time diff = " + Global.timeDiff);
        VKApplication.context.getSharedPreferences("longpoll", 0).edit().putInt("tmp_msg_id", id - 1).commit();
        m.setText(text);
        m.attachments = new ArrayList();
        m.attachments.addAll(attachments);
        m.fwdMessages = new ArrayList();
        ArrayList<Integer> fwdIds = new ArrayList();
        if (fwd != null) {
            it = fwd.iterator();
            while (it.hasNext()) {
                Object fm = it.next();
                if (fm instanceof Message) {
                    fwdIds.add(Integer.valueOf(((Message) fm).id));
                    m.fwdMessages.add(((Message) fm).forward());
                }
                if (fm instanceof FwdMessage) {
                    fwdIds.add(Integer.valueOf(((FwdMessage) fm).id));
                    m.fwdMessages.add((FwdMessage) fm);
                }
            }
        }
        GeoAttachment geo = null;
        Iterator it2 = attachments.iterator();
        while (it2.hasNext()) {
            Attachment att = (Attachment) it2.next();
            if (att instanceof GeoAttachment) {
                attachments.remove(att);
                geo = (GeoAttachment) att;
                break;
            }
        }
        int uploadId = -1;
        it = attachments.iterator();
        while (it.hasNext()) {
            att = (Attachment) it.next();
            if (att instanceof PendingPhotoAttachment) {
                uploadId = ((PendingPhotoAttachment) att).id;
            }
            if (att instanceof PendingDocumentAttachment) {
                uploadId = ((PendingDocumentAttachment) att).did;
            }
        }
        if (uploadId == -1) {
            new MessagesSend(peer, text, attachments, fwdIds, geo, id).setCallback(new C14247(m)).exec();
            LongPollService.sendingMessages.add(m);
        } else {
            it2 = m.attachments.iterator();
            while (it2.hasNext()) {
                Intent intent;
                att = (Attachment) it2.next();
                if (att instanceof PendingPhotoAttachment) {
                    PendingPhotoAttachment pa = (PendingPhotoAttachment) att;
                    if (!UploaderService.hasTaskWithId(pa.id)) {
                        intent = new Intent(VKApplication.context, UploaderService.class);
                        intent.putExtra("new", 1);
                        intent.putExtra("file", pa.fileUri);
                        intent.putExtra("id", pa.id);
                        intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 5);
                        VKApplication.context.startService(intent);
                    }
                }
                if (att instanceof PendingDocumentAttachment) {
                    PendingDocumentAttachment pa2 = (PendingDocumentAttachment) att;
                    if (!UploaderService.hasTaskWithId(pa2.did)) {
                        intent = new Intent(VKApplication.context, UploaderService.class);
                        intent.putExtra("new", 1);
                        intent.putExtra("file", pa2.url);
                        intent.putExtra("id", pa2.did);
                        intent.putExtra(WebDialog.DIALOG_PARAM_TYPE, 4);
                        intent.putExtra("no_notify", true);
                        HashMap<String, String> params = new HashMap();
                        params.put("_mehod", "docs.getWallUploadServer");
                        intent.putExtra("req_params", params);
                        VKApplication.context.startService(intent);
                    }
                }
            }
            pendingUploads.add(new Message(m));
        }
        add(m, null, VKApplication.context.getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE));
        return m;
    }

    public static void uploadDone(int id, Attachment result) {
        Message msg = null;
        Iterator it = pendingUploads.iterator();
        while (it.hasNext()) {
            Message m = (Message) it.next();
            int i = 0;
            Iterator it2 = m.attachments.iterator();
            while (it2.hasNext()) {
                Attachment a = (Attachment) it2.next();
                if ((a instanceof PendingPhotoAttachment) && ((PendingPhotoAttachment) a).id == id) {
                    m.attachments.set(i, result);
                    add(m, null, VKApplication.context.getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE));
                    msg = m;
                    break;
                    continue;
                } else if ((a instanceof PendingDocumentAttachment) && ((PendingDocumentAttachment) a).did == id) {
                    m.attachments.set(i, result);
                    add(m, null, VKApplication.context.getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE));
                    msg = m;
                    break;
                    continue;
                } else {
                    i++;
                }
            }
            if (msg != null) {
                break;
            }
        }
        if (msg != null) {
            Attachment att;
            Iterator it3 = msg.attachments.iterator();
            while (it3.hasNext()) {
                att = (Attachment) it3.next();
                if (!(att instanceof PendingPhotoAttachment)) {
                    if (att instanceof PendingDocumentAttachment) {
                        return;
                    }
                }
                return;
            }
            pendingUploads.remove(msg);
            GeoAttachment geo = null;
            ArrayList<Attachment> attachments = new ArrayList();
            attachments.addAll(msg.attachments);
            it3 = attachments.iterator();
            while (it3.hasNext()) {
                att = (Attachment) it3.next();
                if (att instanceof GeoAttachment) {
                    attachments.remove(att);
                    geo = (GeoAttachment) att;
                    break;
                }
            }
            ArrayList<Integer> fwdIds = new ArrayList();
            it3 = msg.fwdMessages.iterator();
            while (it3.hasNext()) {
                fwdIds.add(Integer.valueOf(((FwdMessage) it3.next()).id));
            }
            m = msg;
            new MessagesSend(msg.peer, msg.text, attachments, fwdIds, geo, msg.id).setCallback(new C14258(m)).exec();
            LongPollService.sendingMessages.add(m);
        }
    }

    public static void uploadFailed(int id) {
        Message msg = null;
        Iterator it = pendingUploads.iterator();
        while (it.hasNext()) {
            Message m = (Message) it.next();
            Iterator it2 = m.attachments.iterator();
            while (it2.hasNext()) {
                Attachment a = (Attachment) it2.next();
                if ((a instanceof PendingPhotoAttachment) && ((PendingPhotoAttachment) a).id == id) {
                    msg = m;
                    break;
                    continue;
                }
            }
            if (msg != null) {
                break;
            }
        }
        if (msg != null) {
            pendingUploads.remove(msg);
            removeFromSending(msg);
            msg.sendFailed = true;
            add(msg, null, VKApplication.context.getSharedPreferences(null, 0).getString("userphoto", ACRAConstants.DEFAULT_STRING_VALUE));
            Intent intent = new Intent(ACTION_SEND_FAILED);
            intent.putExtra("id", msg.id);
            VKApplication.context.sendBroadcast(intent, permission.ACCESS_DATA);
        }
    }

    private static void removeFromSending(Message m) {
        Log.m525d("vk", "remove from sending: " + m.id);
        LongPollService.sendingMessages.remove(m);
        if (LongPollService.sendingMessages.size() == 0 && LongPollService.pendingReceivedMessages.size() > 0) {
            Message msg;
            Iterator it = LongPollService.pendingReceivedMessages.iterator();
            while (it.hasNext()) {
                msg = (Message) it.next();
                if (msg.id == m.id) {
                    LongPollService.pendingReceivedMessages.remove(msg);
                    break;
                }
            }
            ArrayList<Message> toRemove = new ArrayList();
            it = LongPollService.pendingReceivedMessages.iterator();
            while (it.hasNext()) {
                msg = (Message) it.next();
                if (Cache.containsMessage(msg.id)) {
                    toRemove.add(msg);
                }
            }
            LongPollService.pendingReceivedMessages.removeAll(toRemove);
            if (LongPollService.pendingReceivedMessages.size() != 0) {
                Cache.addMessages(LongPollService.pendingReceivedMessages);
                it = LongPollService.pendingReceivedMessages.iterator();
                while (it.hasNext()) {
                    broadcastNewMessage((Message) it.next());
                }
            }
        }
    }

    private static void broadcastNewMessage(Message m) {
        ArrayList<Integer> ids = new ArrayList();
        ids.add(Integer.valueOf(m.peer));
        if (m.peer < 2000000000) {
            Friends.getUsers(ids, new C14269(m));
        } else {
            getChatUsers(m.peer - 2000000000, new AnonymousClass10(m));
        }
    }

    public static void getChatUsers(int chatID, GetChatUsersCallback getChatUsersCallback) {
        if (Cache.needUpdateChat(chatID)) {
            Log.m530w("vk", "need update chat " + chatID);
            getChatUsersFromApi(chatID, getChatUsersCallback);
            return;
        }
        new Thread(new AnonymousClass11(chatID, getChatUsersCallback)).start();
    }

    private static void getChatUsersFromApi(int chatID, GetChatUsersCallback getChatUsersCallback) {
        new MessagesGetChat(chatID).setCallback(new AnonymousClass12(chatID, getChatUsersCallback)).exec();
    }

    public static void markAsRead(ArrayList<Integer> ids) {
        new MessagesMarkAsRead(ids).setCallback(new AnonymousClass13(ids)).exec();
    }

    public static void delete(ArrayList<Integer> ids) {
        ArrayList<Integer> failed = new ArrayList();
        Iterator it = ids.iterator();
        while (it.hasNext()) {
            int id = ((Integer) it.next()).intValue();
            if (id < 0) {
                failed.add(Integer.valueOf(id));
            }
        }
        Cache.deleteMessages(failed);
        it = failed.iterator();
        while (it.hasNext()) {
            id = ((Integer) it.next()).intValue();
            Intent intent = new Intent(LongPollService.ACTION_MESSAGE_DELETED);
            intent.putExtra(LongPollService.EXTRA_MSG_ID, id);
            VKApplication.context.sendBroadcast(intent, permission.ACCESS_DATA);
        }
        ids.removeAll(failed);
        if (ids.size() > 0) {
            new MessagesDelete(ids).setCallback(new AnonymousClass14(ids)).exec();
        }
    }

    public static int getChatAdmin(int chatID) {
        return Cache.getChatAdmin(chatID);
    }

    public static void applyActions(ArrayList<MessagesAction> actions) {
        Cache.applyMessagesActions(actions);
        histories.clear();
        try {
            updateLock.acquire();
        } catch (Exception e) {
        }
        dialogs.clear();
        updateLock.release();
    }

    public static void reset() {
        VKApplication.context.getSharedPreferences("longpoll", 0).edit().putInt("pts", 0).commit();
        try {
            updateLock.acquire();
        } catch (Exception e) {
        }
        dialogs.clear();
        updateLock.release();
        histories.clear();
        pendingUploads.clear();
    }

    public static void removeDialog(int peer) {
        Cache.deleteDialog(peer);
        histories.remove(Integer.valueOf(peer));
        try {
            updateLock.acquire();
        } catch (Exception e) {
        }
        Iterator<DialogEntry> itr = dialogs.iterator();
        while (itr.hasNext()) {
            if (((DialogEntry) itr.next()).profile.uid == peer) {
                itr.remove();
                break;
            }
        }
        updateLock.release();
    }

    public static void resetCache() {
        Cache.deleteAllMessages();
        if (loadDlgsReq != null) {
            loadDlgsReq.cancel();
            loadDlgsReq = null;
        }
    }

    public static long getLastUpdated() {
        if (lastUpdated == -1) {
            lastUpdated = VKApplication.context.getSharedPreferences("msg", 0).getLong("updated", lastUpdated);
        }
        return lastUpdated;
    }

    public static void search(String q, int offset, int count, SearchCallback callback) {
        new MessagesSearch(q, offset, count).setCallback(new AnonymousClass15(callback)).exec();
    }

    private static Bitmap createShortcutIcon(String url) {
        int iconSize = VERSION.SDK_INT < 11 ? Global.scale(GalleryPickerFooterView.SIZE) : ((ActivityManager) VKApplication.context.getSystemService("activity")).getLauncherLargeIconSize();
        Bitmap photo = ImageCache.get(url);
        Bitmap icon = Bitmap.createBitmap(iconSize, iconSize, Config.ARGB_8888);
        Canvas c = new Canvas(icon);
        Paint paint = new Paint();
        paint.setColor(Color.WINDOW_BACKGROUND_COLOR);
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        c.drawColor(Color.WINDOW_BACKGROUND_COLOR, Mode.CLEAR);
        c.drawRoundRect(new RectF(0.0f, 0.0f, (float) c.getWidth(), (float) c.getHeight()), (float) (c.getWidth() / 15), (float) (c.getWidth() / 15), paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        c.drawBitmap(photo, null, new Rect(0, 0, c.getWidth(), c.getHeight()), paint);
        return icon;
    }

    public static Intent getShortcutIntent(UserProfile profile) {
        Bitmap icon = createShortcutIcon(profile.photo);
        Intent sIntent = new Intent("android.intent.action.VIEW", Uri.parse("vkontakte://vk.com/write" + profile.uid));
        sIntent.addFlags(268435456);
        Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        intent.putExtra("android.intent.extra.shortcut.INTENT", sIntent);
        intent.putExtra("android.intent.extra.shortcut.NAME", profile.fullName);
        intent.putExtra("android.intent.extra.shortcut.ICON", icon);
        return intent;
    }

    public static String createChatPhoto(ArrayList<ChatUser> users) {
        ArrayList<String> pp = new ArrayList();
        pp.add("M");
        for (int i = 0; i < users.size(); i++) {
            ChatUser cu = (ChatUser) users.get(i);
            if (!(cu.user.uid == Global.uid || pp.contains(cu.user.photo))) {
                pp.add(cu.user.photo);
                if (pp.size() == 5) {
                    break;
                }
            }
        }
        return TextUtils.join("|", pp);
    }
}
