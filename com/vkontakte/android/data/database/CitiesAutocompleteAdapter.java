package com.vkontakte.android.data.database;

import android.graphics.Typeface;
import android.os.Handler;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filter.FilterResults;
import android.widget.Filterable;
import android.widget.TextView;
import com.vkontakte.android.APIRequest;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.DatabaseGetCities;
import com.vkontakte.android.api.DatabaseGetCities.Callback;
import java.util.ArrayList;
import java.util.List;

public class CitiesAutocompleteAdapter extends BaseAdapter implements Filterable {
    private List<City> cities;
    private int country;
    private Runnable delaying;
    private CitiesFilter filter;
    private Handler handler;
    private List<City> hints;
    private City none;
    private String query;
    private APIRequest req;
    private List<City> results;
    private boolean showNone;

    private class CitiesFilter extends Filter {

        /* renamed from: com.vkontakte.android.data.database.CitiesAutocompleteAdapter.CitiesFilter.1 */
        class C05731 implements Runnable {
            private final /* synthetic */ String val$query;

            C05731(String str) {
                this.val$query = str;
            }

            public void run() {
                CitiesAutocompleteAdapter.this.delaying = null;
                CitiesAutocompleteAdapter.this.loadSearch(this.val$query);
            }
        }

        private CitiesFilter() {
        }

        protected FilterResults performFiltering(CharSequence constraint) {
            CitiesAutocompleteAdapter.this.query = constraint;
            return new FilterResults();
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            String query = null;
            if (CitiesAutocompleteAdapter.this.delaying != null) {
                CitiesAutocompleteAdapter.this.handler.removeCallbacks(CitiesAutocompleteAdapter.this.delaying);
                CitiesAutocompleteAdapter.this.delaying = null;
            }
            if (CitiesAutocompleteAdapter.this.req != null) {
                CitiesAutocompleteAdapter.this.req.cancel();
                CitiesAutocompleteAdapter.this.req = null;
            }
            if (constraint != null && constraint.length() > 0) {
                query = constraint.toString();
            }
            Handler access$2 = CitiesAutocompleteAdapter.this.handler;
            CitiesAutocompleteAdapter citiesAutocompleteAdapter = CitiesAutocompleteAdapter.this;
            Runnable c05731 = new C05731(query);
            citiesAutocompleteAdapter.delaying = c05731;
            access$2.postDelayed(c05731, 500);
        }
    }

    /* renamed from: com.vkontakte.android.data.database.CitiesAutocompleteAdapter.1 */
    class C14301 implements Callback {
        private final /* synthetic */ String val$q;

        /* renamed from: com.vkontakte.android.data.database.CitiesAutocompleteAdapter.1.1 */
        class C05721 implements Runnable {
            private final /* synthetic */ String val$q;
            private final /* synthetic */ List val$res;

            C05721(List list, String str) {
                this.val$res = list;
                this.val$q = str;
            }

            public void run() {
                if (CitiesAutocompleteAdapter.this.showNone) {
                    this.val$res.add(0, CitiesAutocompleteAdapter.this.none);
                }
                if (this.val$q == null) {
                    CitiesAutocompleteAdapter.this.hints.addAll(this.val$res);
                    CitiesAutocompleteAdapter.this.cities = CitiesAutocompleteAdapter.this.hints;
                } else {
                    CitiesAutocompleteAdapter.this.results.addAll(this.val$res);
                    CitiesAutocompleteAdapter.this.cities = CitiesAutocompleteAdapter.this.results;
                }
                CitiesAutocompleteAdapter.this.notifyDataSetChanged();
            }
        }

        C14301(String str) {
            this.val$q = str;
        }

        public void success(List<City> res) {
            CitiesAutocompleteAdapter.this.handler.post(new C05721(res, this.val$q));
        }

        public void fail(int ecode, String emsg) {
        }
    }

    public CitiesAutocompleteAdapter() {
        this.filter = new CitiesFilter();
        this.country = 0;
        this.query = null;
        this.hints = new ArrayList();
        this.results = new ArrayList();
        this.cities = this.hints;
        this.handler = new Handler();
        this.none = new City();
        this.none.id = 0;
        this.none.title = VKApplication.context.getResources().getString(C0436R.string.not_specified);
    }

    public int getCount() {
        return this.cities.size();
    }

    public Object getItem(int position) {
        return this.cities.get(position);
    }

    public long getItemId(int position) {
        return (long) ((City) this.cities.get(position)).id;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        CharSequence title;
        View v = convertView;
        if (v == null) {
            v = View.inflate(parent.getContext(), C0436R.layout.city_list_item, null);
        }
        City city = (City) this.cities.get(position);
        if (this.query != null) {
            int pos = city.title.toLowerCase().indexOf(this.query);
            if (pos != -1) {
                Spannable sp = Factory.getInstance().newSpannable(city.title);
                sp.setSpan(new ForegroundColorSpan(parent.getResources().getColorStateList(C0436R.color.btn_link).getDefaultColor()), pos, this.query.length() + pos, 0);
                title = sp;
            } else {
                title = city.title;
            }
        } else {
            title = city.title;
        }
        ((TextView) v.findViewById(C0436R.id.city_title)).setText(title);
        ((TextView) v.findViewById(C0436R.id.city_title)).setTypeface(city.important ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
        if (city.region == null || city.area == null || city.region.length() <= 0 || city.area.length() <= 0) {
            v.findViewById(C0436R.id.city_subtitle).setVisibility(8);
        } else {
            v.findViewById(C0436R.id.city_subtitle).setVisibility(0);
            ((TextView) v.findViewById(C0436R.id.city_subtitle)).setText(city.area + ", " + city.region);
        }
        return v;
    }

    public void setCountry(int c) {
        this.country = c;
        this.hints.clear();
        this.results.clear();
        notifyDataSetChanged();
        this.filter.filter(null);
    }

    public void setShowNone(boolean show) {
        this.showNone = show;
    }

    public Filter getFilter() {
        return this.filter;
    }

    private void loadSearch(String q) {
        this.query = q != null ? q.toLowerCase() : null;
        if (q != null || this.hints.size() <= 0) {
            if (q != null) {
                this.cities = this.results;
                this.results.clear();
                notifyDataSetChanged();
            }
            this.req = new DatabaseGetCities(this.country, q).setCallback(new C14301(q)).exec();
            return;
        }
        this.cities = this.hints;
        notifyDataSetChanged();
    }
}
