package com.vkontakte.android.data.database;

public class Country {
    public String code;
    public int id;
    public boolean important;
    public String name;
    public String phoneCode;

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.util.ArrayList<com.vkontakte.android.data.database.Country> getCountries(boolean r21, boolean r22, java.lang.String r23) {
        /*
        r10 = new java.util.ArrayList;
        r10.<init>();
        r17 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x012a }
        r18 = 0;
        r19 = 0;
        r17 = r17.getSharedPreferences(r18, r19);	 Catch:{ Exception -> 0x012a }
        r18 = "usercountry";
        r19 = 0;
        r13 = r17.getInt(r18, r19);	 Catch:{ Exception -> 0x012a }
        r14 = 0;
        if (r13 != 0) goto L_0x0039;
    L_0x001a:
        r17 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x012a }
        r18 = "phone";
        r12 = r17.getSystemService(r18);	 Catch:{ Exception -> 0x012a }
        r12 = (android.telephony.TelephonyManager) r12;	 Catch:{ Exception -> 0x012a }
        r17 = r12.getSimCountryIso();	 Catch:{ Exception -> 0x012a }
        r11 = r17.toUpperCase();	 Catch:{ Exception -> 0x012a }
        r17 = r11.length();	 Catch:{ Exception -> 0x012a }
        r18 = 2;
        r0 = r17;
        r1 = r18;
        if (r0 != r1) goto L_0x00a5;
    L_0x0038:
        r14 = r11;
    L_0x0039:
        if (r22 == 0) goto L_0x00ae;
    L_0x003b:
        r15 = 1;
    L_0x003c:
        if (r22 == 0) goto L_0x0052;
    L_0x003e:
        r9 = new com.vkontakte.android.data.database.Country;	 Catch:{ Exception -> 0x012a }
        r9.<init>();	 Catch:{ Exception -> 0x012a }
        r17 = 0;
        r0 = r17;
        r9.id = r0;	 Catch:{ Exception -> 0x012a }
        if (r23 == 0) goto L_0x00b0;
    L_0x004b:
        r0 = r23;
        r9.name = r0;	 Catch:{ Exception -> 0x012a }
        r10.add(r9);	 Catch:{ Exception -> 0x012a }
    L_0x0052:
        r17 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x012a }
        r17 = r17.getAssets();	 Catch:{ Exception -> 0x012a }
        r18 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x012a }
        r19 = "countries_";
        r18.<init>(r19);	 Catch:{ Exception -> 0x012a }
        r19 = com.vkontakte.android.Global.getDeviceLang();	 Catch:{ Exception -> 0x012a }
        r18 = r18.append(r19);	 Catch:{ Exception -> 0x012a }
        r19 = ".txt";
        r18 = r18.append(r19);	 Catch:{ Exception -> 0x012a }
        r18 = r18.toString();	 Catch:{ Exception -> 0x012a }
        r5 = r17.open(r18);	 Catch:{ Exception -> 0x012a }
        r17 = r5.available();	 Catch:{ Exception -> 0x012a }
        r0 = r17;
        r4 = new byte[r0];	 Catch:{ Exception -> 0x012a }
        r5.read(r4);	 Catch:{ Exception -> 0x012a }
        r5.close();	 Catch:{ Exception -> 0x012a }
        r17 = new java.lang.String;	 Catch:{ Exception -> 0x012a }
        r18 = "UTF-8";
        r0 = r17;
        r1 = r18;
        r0.<init>(r4, r1);	 Catch:{ Exception -> 0x012a }
        r18 = "\n";
        r8 = r17.split(r18);	 Catch:{ Exception -> 0x012a }
        r3 = new java.util.HashSet;	 Catch:{ Exception -> 0x012a }
        r3.<init>();	 Catch:{ Exception -> 0x012a }
        r0 = r8.length;	 Catch:{ Exception -> 0x012a }
        r18 = r0;
        r17 = 0;
    L_0x009e:
        r0 = r17;
        r1 = r18;
        if (r0 < r1) goto L_0x00be;
    L_0x00a4:
        return r10;
    L_0x00a5:
        r17 = java.util.Locale.getDefault();	 Catch:{ Exception -> 0x012a }
        r14 = r17.getCountry();	 Catch:{ Exception -> 0x012a }
        goto L_0x0039;
    L_0x00ae:
        r15 = 0;
        goto L_0x003c;
    L_0x00b0:
        r17 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x012a }
        r17 = r17.getResources();	 Catch:{ Exception -> 0x012a }
        r18 = 2131231519; // 0x7f08031f float:1.8079121E38 double:1.052968277E-314;
        r23 = r17.getString(r18);	 Catch:{ Exception -> 0x012a }
        goto L_0x004b;
    L_0x00be:
        r7 = r8[r17];	 Catch:{ Exception -> 0x012a }
        r19 = ",";
        r20 = 4;
        r0 = r19;
        r1 = r20;
        r6 = r7.split(r0, r1);	 Catch:{ Exception -> 0x012a }
        r2 = new com.vkontakte.android.data.database.Country;	 Catch:{ Exception -> 0x012a }
        r2.<init>();	 Catch:{ Exception -> 0x012a }
        r19 = 1;
        r19 = r6[r19];	 Catch:{ Exception -> 0x012a }
        r19 = java.lang.Integer.parseInt(r19);	 Catch:{ Exception -> 0x012a }
        r0 = r19;
        r2.id = r0;	 Catch:{ Exception -> 0x012a }
        r19 = 2;
        r19 = r6[r19];	 Catch:{ Exception -> 0x012a }
        r0 = r19;
        r2.code = r0;	 Catch:{ Exception -> 0x012a }
        r19 = 3;
        r19 = r6[r19];	 Catch:{ Exception -> 0x012a }
        r0 = r19;
        r2.name = r0;	 Catch:{ Exception -> 0x012a }
        r19 = 0;
        r19 = r6[r19];	 Catch:{ Exception -> 0x012a }
        r0 = r19;
        r2.phoneCode = r0;	 Catch:{ Exception -> 0x012a }
        if (r21 == 0) goto L_0x0103;
    L_0x00f7:
        r0 = r2.code;	 Catch:{ Exception -> 0x012a }
        r19 = r0;
        r0 = r19;
        r19 = r3.add(r0);	 Catch:{ Exception -> 0x012a }
        if (r19 == 0) goto L_0x0122;
    L_0x0103:
        r0 = r2.id;	 Catch:{ Exception -> 0x012a }
        r19 = r0;
        r0 = r19;
        if (r0 == r13) goto L_0x0119;
    L_0x010b:
        if (r14 == 0) goto L_0x0126;
    L_0x010d:
        r0 = r2.code;	 Catch:{ Exception -> 0x012a }
        r19 = r0;
        r0 = r19;
        r19 = r0.equals(r14);	 Catch:{ Exception -> 0x012a }
        if (r19 == 0) goto L_0x0126;
    L_0x0119:
        r19 = 1;
        r0 = r19;
        r2.important = r0;	 Catch:{ Exception -> 0x012a }
        r10.add(r15, r2);	 Catch:{ Exception -> 0x012a }
    L_0x0122:
        r17 = r17 + 1;
        goto L_0x009e;
    L_0x0126:
        r10.add(r2);	 Catch:{ Exception -> 0x012a }
        goto L_0x0122;
    L_0x012a:
        r16 = move-exception;
        r17 = "vk";
        r0 = r17;
        r1 = r16;
        com.vkontakte.android.Log.m532w(r0, r1);
        goto L_0x00a4;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.data.database.Country.getCountries(boolean, boolean, java.lang.String):java.util.ArrayList<com.vkontakte.android.data.database.Country>");
    }

    public String toString() {
        return this.name;
    }
}
