package com.vkontakte.android.data.database;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Log;
import org.json.JSONObject;

public class City {
    public String area;
    public int id;
    public boolean important;
    public String region;
    public String title;

    public City(JSONObject o) {
        boolean z = true;
        try {
            this.id = o.getInt("id");
            this.title = o.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            this.area = o.optString("area");
            this.region = o.optString("region");
            if (o.optInt("important") != 1) {
                z = false;
            }
            this.important = z;
        } catch (Exception x) {
            Log.m531w("vk", "Error parsing city", x);
        }
    }

    public String toString() {
        return this.title;
    }
}
