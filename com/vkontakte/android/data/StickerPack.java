package com.vkontakte.android.data;

import com.google.android.gms.plus.PlusShare;
import com.vkontakte.android.Log;
import com.vkontakte.android.StickerAttachment;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class StickerPack {
    public static final int STATE_CONFIRMED = 2;
    public static final int STATE_DOWNLOADED = 4;
    public static final int STATE_INACTIVE = 5;
    public static final int STATE_NOT_OWNED = 0;
    public static final int STATE_OWNED = 3;
    public static final int STATE_PAID = 1;
    public static final int STATE_PROMOTED = 6;
    public static final int STATE_UNAVAILABLE = 7;
    public String author;
    public String baseURL;
    public int count;
    public String description;
    public String downloadLink;
    public int id;
    public List<Integer> ids;
    public boolean isNew;
    public ArrayList<String> previews;
    public String price;
    public int state;
    public ArrayList<StickerAttachment> stickers;
    public String stickersBaseURL;
    public String storeID;
    public String thumb;
    public String title;

    public StickerPack(JSONObject o) {
        boolean promoted = true;
        try {
            boolean purchased;
            boolean active;
            this.id = o.getInt("id");
            this.title = o.getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE);
            this.downloadLink = o.getString("base_url") + "content.zip";
            this.ids = new ArrayList();
            JSONArray jids = o.getJSONObject("stickers").getJSONArray("sticker_ids");
            this.count = jids.length();
            for (int i = STATE_NOT_OWNED; i < jids.length(); i += STATE_PAID) {
                this.ids.add(Integer.valueOf(jids.getInt(i)));
            }
            if (o.getInt("purchased") == STATE_PAID) {
                purchased = true;
            } else {
                purchased = false;
            }
            if (o.getInt("active") == STATE_PAID) {
                active = true;
            } else {
                active = false;
            }
            if (o.optInt("promoted") != STATE_PAID) {
                promoted = false;
            }
            this.baseURL = o.getString("base_url");
            this.stickersBaseURL = o.getJSONObject("stickers").getString("base_url");
            if (purchased) {
                if (active) {
                    this.state = Stickers.getPackState(this.id);
                    if (this.state != STATE_DOWNLOADED) {
                        this.state = STATE_OWNED;
                    }
                } else {
                    this.state = STATE_INACTIVE;
                }
            } else if (promoted) {
                this.state = STATE_PROMOTED;
            }
            Log.m528i("vk", this.title + ", state=" + this.state);
        } catch (Throwable x) {
            Log.m532w("vk", x);
        }
    }
}
