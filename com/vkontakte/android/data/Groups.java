package com.vkontakte.android.data;

import android.content.Intent;
import com.vkontakte.android.APIController;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.SearchIndexer;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.api.GroupsGet;
import com.vkontakte.android.api.GroupsGet.Callback;
import com.vkontakte.android.cache.GroupsCache;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Groups {
    public static final String ACTION_GROUP_INVITES_CHANGED = "com.vkontakte.android.GROUP_INVITES_CHANGED";
    public static final String ACTION_GROUP_LIST_CHANGED = "com.vkontakte.android.GROUP_LIST_CHANGED";
    private static ArrayList<Group> groups;
    private static SearchIndexer<Group> index;

    /* renamed from: com.vkontakte.android.data.Groups.1 */
    class C05641 implements Runnable {
        private final /* synthetic */ boolean val$forceNetwork;

        /* renamed from: com.vkontakte.android.data.Groups.1.1 */
        class C14161 implements Callback {
            C14161() {
            }

            public void success(ArrayList<Group> list) {
                Groups.groups.clear();
                Groups.groups.addAll(list);
                GroupsCache.replace(list);
                Groups.index.bind(Groups.groups);
                Groups.index.build();
            }

            public void fail(int ecode, String emsg) {
                Groups.groups.clear();
                Groups.groups.addAll(GroupsCache.get());
                Groups.index.bind(Groups.groups);
                Groups.index.build();
            }
        }

        C05641(boolean z) {
            this.val$forceNetwork = z;
        }

        public void run() {
            ArrayList<Group> result = new ArrayList();
            if (!this.val$forceNetwork) {
                result.addAll(GroupsCache.get());
            }
            if (this.val$forceNetwork || result.size() == 0) {
                new GroupsGet(Global.uid).setCallback(new C14161()).execSync();
            } else {
                Groups.groups = result;
            }
            Groups.index.bind(Groups.groups);
            Groups.index.build();
            VKApplication.context.sendBroadcast(new Intent(Groups.ACTION_GROUP_LIST_CHANGED));
        }
    }

    static {
        groups = new ArrayList();
        index = new SearchIndexer();
    }

    public static void reload(boolean forceNetwork) {
        Log.m528i("vk", "RELOAD GROUPS " + forceNetwork);
        APIController.runInBg(new C05641(forceNetwork));
    }

    public static void getGroups(ArrayList<Group> out) {
        out.addAll(groups);
    }

    public static void getAdminedGroups(ArrayList<Group> out) {
        Iterator it = groups.iterator();
        while (it.hasNext()) {
            Group g = (Group) it.next();
            if (g.isAdmin) {
                out.add(g);
            }
        }
    }

    public static boolean isGroupMember(int gid) {
        Iterator it = groups.iterator();
        while (it.hasNext()) {
            if (((Group) it.next()).id == gid) {
                return true;
            }
        }
        return false;
    }

    public static boolean isGroupAdmin(int gid) {
        Iterator it = groups.iterator();
        while (it.hasNext()) {
            Group g = (Group) it.next();
            if (g.id == gid) {
                Log.m528i("vk", "is group admin " + gid + " -> " + g.isAdmin);
                return g.isAdmin;
            }
        }
        return false;
    }

    public static Group getById(int gid) {
        Iterator it = groups.iterator();
        while (it.hasNext()) {
            Group g = (Group) it.next();
            if (g.id == gid) {
                return g;
            }
        }
        return null;
    }

    public static int getAdminLevel(int gid) {
        Iterator it = groups.iterator();
        while (it.hasNext()) {
            Group g = (Group) it.next();
            if (g.id == gid) {
                return g.adminLevel;
            }
        }
        return 0;
    }

    public static void reset() {
        groups.clear();
        index = new SearchIndexer();
    }

    public static List<Group> search(String q) {
        return index.search(q);
    }
}
