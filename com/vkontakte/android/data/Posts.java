package com.vkontakte.android.data;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.NewsEntry;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.WallPost;
import com.vkontakte.android.api.WallPost.Callback;
import com.vkontakte.android.background.WorkerThread;
import com.vkontakte.android.mediapicker.entries.CancellableRunnable;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Vector;
import java.util.concurrent.Semaphore;

public class Posts {
    public static final String ACTION_NEW_POST_BROADCAST = "com.vkontakte.android.NEW_POST_ADDED";
    public static final String ACTION_POST_DELETED_BROADCAST = "com.vkontakte.android.POST_DELETED";
    public static final String ACTION_POST_REPLACED_BROADCAST = "com.vkontakte.android.POST_REPLACED";
    public static final String ACTION_POST_UPDATED_BROADCAST = "com.vkontakte.android.POST_UPDATED";
    public static final String ACTION_RELOAD_FEED = "com.vkontakte.android.RELOAD_FEED";
    public static final String ACTION_USER_NAME_CHANGED = "com.vkontakte.android.USER_NAME_CHANGED";
    public static final String ACTION_USER_PHOTO_CHANGED = "com.vkontakte.android.USER_PHOTO_CHANGED";
    public static ArrayList<NewsEntry> feed;
    public static String feedFrom;
    public static int feedItem;
    public static int feedItemOffset;
    public static int feedOffset;
    private static Vector<String> logWriteQueue;
    private static CancellableRunnable postedRunner;
    public static ArrayList<NewsEntry> preloadedFeed;
    private static Semaphore queueAccess;
    private static WorkerThread thread;
    private static HashSet<String> viewedPosts;
    private static HashSet<String> viewedReposts;

    /* renamed from: com.vkontakte.android.data.Posts.1 */
    class C05691 implements Runnable {
        C05691() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r10 = this;
            r0 = new java.io.File;	 Catch:{ Exception -> 0x002e }
            r6 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x002e }
            r6 = r6.getFilesDir();	 Catch:{ Exception -> 0x002e }
            r7 = "posts.log";
            r0.<init>(r6, r7);	 Catch:{ Exception -> 0x002e }
            r6 = r0.exists();	 Catch:{ Exception -> 0x002e }
            if (r6 != 0) goto L_0x0014;
        L_0x0013:
            return;
        L_0x0014:
            r2 = new java.io.BufferedReader;	 Catch:{ Exception -> 0x002e }
            r6 = new java.io.FileReader;	 Catch:{ Exception -> 0x002e }
            r6.<init>(r0);	 Catch:{ Exception -> 0x002e }
            r2.<init>(r6);	 Catch:{ Exception -> 0x002e }
        L_0x001e:
            r1 = r2.readLine();	 Catch:{ Exception -> 0x002e }
            if (r1 == 0) goto L_0x002a;
        L_0x0024:
            r6 = r1.length();	 Catch:{ Exception -> 0x002e }
            if (r6 > 0) goto L_0x0035;
        L_0x002a:
            r2.close();	 Catch:{ Exception -> 0x002e }
            goto L_0x0013;
        L_0x002e:
            r5 = move-exception;
            r6 = "vk";
            android.util.Log.w(r6, r5);
            goto L_0x0013;
        L_0x0035:
            r6 = ",";
            r5 = r1.split(r6);	 Catch:{ Exception -> 0x002e }
            r6 = r5.length;	 Catch:{ Exception -> 0x002e }
            r7 = 3;
            if (r6 < r7) goto L_0x002a;
        L_0x003f:
            r6 = 2;
            r6 = r5[r6];	 Catch:{ Exception -> 0x002e }
            r3 = java.lang.Long.parseLong(r6);	 Catch:{ Exception -> 0x002e }
            r6 = java.lang.System.currentTimeMillis();	 Catch:{ Exception -> 0x002e }
            r6 = r6 - r3;
            r8 = 86400000; // 0x5265c00 float:7.82218E-36 double:4.2687272E-316;
            r6 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1));
            if (r6 >= 0) goto L_0x001e;
        L_0x0052:
            r6 = "0";
            r7 = 1;
            r7 = r5[r7];	 Catch:{ Exception -> 0x002e }
            r6 = r6.equals(r7);	 Catch:{ Exception -> 0x002e }
            if (r6 == 0) goto L_0x0068;
        L_0x005d:
            r6 = com.vkontakte.android.data.Posts.viewedPosts;	 Catch:{ Exception -> 0x002e }
            r7 = 0;
            r7 = r5[r7];	 Catch:{ Exception -> 0x002e }
            r6.add(r7);	 Catch:{ Exception -> 0x002e }
            goto L_0x001e;
        L_0x0068:
            r6 = com.vkontakte.android.data.Posts.viewedReposts;	 Catch:{ Exception -> 0x002e }
            r7 = 0;
            r7 = r5[r7];	 Catch:{ Exception -> 0x002e }
            r6.add(r7);	 Catch:{ Exception -> 0x002e }
            goto L_0x001e;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.data.Posts.1.run():void");
        }
    }

    /* renamed from: com.vkontakte.android.data.Posts.2 */
    class C14272 implements Callback {
        private final /* synthetic */ Activity val$context;
        private final /* synthetic */ NewsEntry val$e;
        private final /* synthetic */ Runnable val$runAfter;

        C14272(NewsEntry newsEntry, Activity activity, Runnable runnable) {
            this.val$e = newsEntry;
            this.val$context = activity;
            this.val$runAfter = runnable;
        }

        public void success(int id) {
            Intent intent = new Intent(Posts.ACTION_POST_DELETED_BROADCAST);
            intent.putExtra("owner_id", this.val$e.ownerID);
            intent.putExtra("post_id", this.val$e.postID);
            intent.putExtra("post", this.val$e);
            this.val$context.sendBroadcast(intent, permission.ACCESS_DATA);
            NewsEntry n = new NewsEntry(this.val$e);
            n.flag(NewsEntry.FLAG_POSTPONED, false);
            n.flags |= 2;
            n.postID = id;
            n.time = (int) (System.currentTimeMillis() / 1000);
            intent = new Intent(Posts.ACTION_NEW_POST_BROADCAST);
            intent.putExtra("entry", n);
            this.val$context.sendBroadcast(intent, permission.ACCESS_DATA);
            Toast.makeText(this.val$context, C0436R.string.wall_ok, 0).show();
            if (this.val$runAfter != null) {
                this.val$runAfter.run();
            }
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(this.val$context, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    private static class StatsBackgroundRunner extends CancellableRunnable {
        private StatsBackgroundRunner() {
        }

        public void run() {
            try {
                File file = new File(VKApplication.context.getFilesDir(), "posts.log");
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileOutputStream os = new FileOutputStream(file, true);
                Posts.queueAccess.acquire();
                String data = new StringBuilder(String.valueOf(TextUtils.join("\n", Posts.logWriteQueue))).append("\n").toString();
                Posts.logWriteQueue.clear();
                Posts.queueAccess.release();
                os.write(data.getBytes());
                os.close();
            } catch (Exception x) {
                Log.w("vk", x);
            }
            Posts.postedRunner = null;
        }
    }

    static {
        feed = new ArrayList();
        preloadedFeed = new ArrayList();
        viewedPosts = new HashSet();
        viewedReposts = new HashSet();
        logWriteQueue = new Vector();
        queueAccess = new Semaphore(1, true);
        thread = new WorkerThread("Posts stats background");
        thread.start();
        thread.postRunnable(new C05691(), 0);
    }

    public static void trackPostView(NewsEntry e) {
        if (e != null && e.type == 0) {
            String repostID;
            String postID = e.ownerID + "_" + e.postID;
            if (e.flag(32)) {
                repostID = e.retweetUID + "_" + e.retweetOrigId;
            } else {
                repostID = null;
            }
            boolean needWrite = false;
            try {
                queueAccess.acquire();
            } catch (Exception e2) {
            }
            if (viewedPosts.add(postID)) {
                logWriteQueue.add(new StringBuilder(String.valueOf(postID)).append(",0,").append(System.currentTimeMillis()).toString());
                needWrite = true;
            }
            if (repostID != null && viewedReposts.add(repostID)) {
                logWriteQueue.add(new StringBuilder(String.valueOf(repostID)).append(",1,").append(System.currentTimeMillis()).toString());
                needWrite = true;
            }
            queueAccess.release();
            if (needWrite && postedRunner == null) {
                postedRunner = new StatsBackgroundRunner();
                thread.postRunnable(postedRunner.toRunnable(), 10000);
            }
        }
    }

    public static void getViewedPosts(ArrayList<String> posts, ArrayList<String> reposts) {
        posts.addAll(viewedPosts);
        reposts.addAll(viewedReposts);
    }

    public static void clearViewedPosts() {
        viewedPosts.clear();
        viewedReposts.clear();
        try {
            new File(VKApplication.context.getFilesDir(), "posts.log").delete();
        } catch (Exception e) {
        }
        if (postedRunner != null) {
            postedRunner.cancel();
            postedRunner = null;
        }
    }

    public static void publishPostponed(NewsEntry e, Activity context, Runnable runAfter) {
        new WallPost(e.ownerID, e.postID).setCallback(new C14272(e, context, runAfter)).wrapProgress(context).exec(context);
    }
}
