package com.vkontakte.android.data;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.media.TransportMediator;
import android.text.TextUtils;
import android.widget.Toast;
import com.android.vending.billing.IInAppBillingService;
import com.android.vending.billing.IInAppBillingService.Stub;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.vkontakte.android.APIController;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.Global;
import com.vkontakte.android.Log;
import com.vkontakte.android.Manifest.permission;
import com.vkontakte.android.StickerAttachment;
import com.vkontakte.android.StickerDownloaderService;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.VKApplication;
import com.vkontakte.android.api.StoreGetPurchases;
import com.vkontakte.android.api.StoreGetPurchases.Callback;
import com.vkontakte.android.api.StorePurchase;
import com.vkontakte.android.fragments.SuggestionsFriendsFragment;
import com.vkontakte.android.mediapicker.providers.GalleryPickerProvider;
import com.vkontakte.android.mediapicker.providers.ResourceProvider.Views;
import com.vkontakte.android.mediapicker.ui.holders.ImageViewHolder;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.acra.ACRAConstants;
import org.json.JSONObject;

public class Stickers {
    public static final String ACTION_STICKERS_DOWNLOAD_PROGRESS = "com.vkontakte.android.STICKERS_DOWNLOAD_PROGRESS";
    public static final String ACTION_STICKERS_UPDATED = "com.vkontakte.android.STICKERS_UPDATED";

    /* renamed from: com.vkontakte.android.data.Stickers.3 */
    class C05713 implements ServiceConnection {
        private final /* synthetic */ Activity val$act;
        private final /* synthetic */ GetPricesCallback val$callback;
        private final /* synthetic */ List val$ids;

        /* renamed from: com.vkontakte.android.data.Stickers.3.1 */
        class C05701 implements Runnable {
            private final /* synthetic */ Activity val$act;
            private final /* synthetic */ GetPricesCallback val$callback;
            private final /* synthetic */ ServiceConnection val$conn;
            private final /* synthetic */ List val$ids;
            private final /* synthetic */ IInAppBillingService val$service;

            C05701(List list, IInAppBillingService iInAppBillingService, Activity activity, GetPricesCallback getPricesCallback, ServiceConnection serviceConnection) {
                this.val$ids = list;
                this.val$service = iInAppBillingService;
                this.val$act = activity;
                this.val$callback = getPricesCallback;
                this.val$conn = serviceConnection;
            }

            public void run() {
                Bundle bundle = new Bundle();
                ArrayList<String> skuList = new ArrayList();
                skuList.addAll(this.val$ids);
                Log.m525d("vk", "Getting prices for " + skuList);
                bundle.putStringArrayList("ITEM_ID_LIST", skuList);
                try {
                    Bundle details = this.val$service.getSkuDetails(3, this.val$act.getPackageName(), "inapp", bundle);
                    if (details.getInt("RESPONSE_CODE") == 0) {
                        HashMap<String, String> prices = new HashMap();
                        Iterator it = details.getStringArrayList("DETAILS_LIST").iterator();
                        while (it.hasNext()) {
                            JSONObject object = new JSONObject((String) it.next());
                            prices.put(object.getString("productId"), object.getString("price"));
                        }
                        this.val$callback.onSuccess(prices);
                        this.val$act.unbindService(this.val$conn);
                        return;
                    }
                    throw new Exception("error getting prices");
                } catch (Throwable x) {
                    Log.m532w("vk", x);
                    this.val$callback.onError(-1, x.getMessage());
                }
            }
        }

        C05713(List list, Activity activity, GetPricesCallback getPricesCallback) {
            this.val$ids = list;
            this.val$act = activity;
            this.val$callback = getPricesCallback;
        }

        public void onServiceDisconnected(ComponentName name) {
        }

        public void onServiceConnected(ComponentName name, IBinder binder) {
            IInAppBillingService service = Stub.asInterface(binder);
            APIController.runInBg(new C05701(this.val$ids, service, this.val$act, this.val$callback, this));
        }
    }

    public interface GetPricesCallback {
        void onError(int i, String str);

        void onSuccess(HashMap<String, String> hashMap);
    }

    /* renamed from: com.vkontakte.android.data.Stickers.1 */
    class C14281 implements Callback {
        C14281() {
        }

        public void success(List<StickerPack> packs) {
            Stickers.doUpdateInfo(packs);
        }

        public void fail(int ecode, String emsg) {
        }
    }

    /* renamed from: com.vkontakte.android.data.Stickers.2 */
    class C14292 implements StorePurchase.Callback {
        private final /* synthetic */ Activity val$act;
        private final /* synthetic */ int val$id;
        private final /* synthetic */ Runnable val$onSuccess;

        C14292(Activity activity, int i, Runnable runnable) {
            this.val$act = activity;
            this.val$id = i;
            this.val$onSuccess = runnable;
        }

        public void success(int state, StickerPack pack, String msg, boolean fatal) {
            if (msg != null) {
                new Builder(this.val$act).setTitle(C0436R.string.error).setMessage(msg).setPositiveButton(C0436R.string.ok, null).show();
            }
            if (state == 1) {
                SharedPreferences prefs = VKApplication.context.getSharedPreferences("stickers", 0);
                prefs.edit().putBoolean("owned" + this.val$id, true).putBoolean("confirmed" + this.val$id, true).putString("content" + this.val$id, pack.baseURL + "content.zip").putString("ordering" + this.val$id, TextUtils.join(",", pack.ids)).commit();
                ArrayList<String> order = new ArrayList();
                order.addAll(Arrays.asList(prefs.getString("order", ACRAConstants.DEFAULT_STRING_VALUE).split(",")));
                order.add(new StringBuilder(String.valueOf(this.val$id)).toString());
                prefs.edit().putString("order", TextUtils.join(",", order)).commit();
                Stickers.broadcastUpdate();
                if (this.val$onSuccess != null) {
                    this.val$onSuccess.run();
                }
            }
        }

        public void fail(int ecode, String emsg) {
            Toast.makeText(this.val$act, ecode == -1 ? C0436R.string.err_text : C0436R.string.error, 0).show();
        }
    }

    public static int getPackState(int id) {
        SharedPreferences prefs = VKApplication.context.getSharedPreferences("stickers", 0);
        if (!prefs.getBoolean("owned" + id, false)) {
            return 0;
        }
        if (!prefs.getBoolean("confirmed" + id, false)) {
            return 1;
        }
        if (prefs.contains("token" + id)) {
            return 2;
        }
        if (!Arrays.asList(prefs.getString("order", ACRAConstants.DEFAULT_STRING_VALUE).split(",")).contains(new StringBuilder(String.valueOf(id)).toString())) {
            return 5;
        }
        if (new File(VKApplication.context.getFilesDir(), "stickers/" + id + "/").exists()) {
            return 4;
        }
        return 3;
    }

    public static void updateInfo() {
        new StoreGetPurchases().setCallback(new C14281()).setBackground(true).exec();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void doUpdateInfo(java.util.List<com.vkontakte.android.data.StickerPack> r29) {
        /*
        r5 = new java.util.ArrayList;
        r5.<init>();
        r20 = com.vkontakte.android.VKApplication.context;	 Catch:{ Exception -> 0x00a9 }
        r20 = r20.getAssets();	 Catch:{ Exception -> 0x00a9 }
        r21 = "stickers";
        r4 = r20.list(r21);	 Catch:{ Exception -> 0x00a9 }
        r0 = r4.length;	 Catch:{ Exception -> 0x00a9 }
        r21 = r0;
        r20 = 0;
    L_0x0016:
        r0 = r20;
        r1 = r21;
        if (r0 < r1) goto L_0x008a;
    L_0x001c:
        r20 = com.vkontakte.android.VKApplication.context;
        r21 = "stickers";
        r22 = 0;
        r15 = r20.getSharedPreferences(r21, r22);
        r3 = r15.getAll();
        r6 = new java.util.ArrayList;
        r6.<init>();
        r12 = r3.keySet();
        r20 = r12.iterator();
    L_0x0037:
        r21 = r20.hasNext();
        if (r21 != 0) goto L_0x00b5;
    L_0x003d:
        r16 = new java.util.ArrayList;
        r16.<init>();
        r13 = new java.util.ArrayList;
        r13.<init>();
        r21 = r29.iterator();
    L_0x004b:
        r20 = r21.hasNext();
        if (r20 != 0) goto L_0x00ec;
    L_0x0051:
        r20 = r15.edit();
        r21 = "promoted";
        r22 = ",";
        r0 = r22;
        r1 = r16;
        r22 = android.text.TextUtils.join(r0, r1);
        r20 = r20.putString(r21, r22);
        r21 = "order";
        r22 = ",";
        r0 = r22;
        r22 = android.text.TextUtils.join(r0, r13);
        r20 = r20.putString(r21, r22);
        r20.commit();
        r20 = r6.size();
        if (r20 <= 0) goto L_0x0086;
    L_0x007c:
        r21 = r6.iterator();
    L_0x0080:
        r20 = r21.hasNext();
        if (r20 != 0) goto L_0x03c0;
    L_0x0086:
        broadcastUpdate();
        return;
    L_0x008a:
        r8 = r4[r20];	 Catch:{ Exception -> 0x00a9 }
        r22 = "\\.";
        r0 = r22;
        r22 = r8.split(r0);	 Catch:{ NumberFormatException -> 0x0437 }
        r23 = 0;
        r22 = r22[r23];	 Catch:{ NumberFormatException -> 0x0437 }
        r22 = java.lang.Integer.parseInt(r22);	 Catch:{ NumberFormatException -> 0x0437 }
        r22 = java.lang.Integer.valueOf(r22);	 Catch:{ NumberFormatException -> 0x0437 }
        r0 = r22;
        r5.add(r0);	 Catch:{ NumberFormatException -> 0x0437 }
    L_0x00a5:
        r20 = r20 + 1;
        goto L_0x0016;
    L_0x00a9:
        r19 = move-exception;
        r20 = "vk";
        r0 = r20;
        r1 = r19;
        com.vkontakte.android.Log.m532w(r0, r1);
        goto L_0x001c;
    L_0x00b5:
        r11 = r20.next();
        r11 = (java.lang.String) r11;
        r21 = "owned";
        r0 = r21;
        r21 = r11.startsWith(r0);
        if (r21 == 0) goto L_0x0037;
    L_0x00c5:
        r21 = "owned";
        r22 = "";
        r0 = r21;
        r1 = r22;
        r21 = r11.replace(r0, r1);	 Catch:{ Exception -> 0x00e0 }
        r9 = java.lang.Integer.parseInt(r21);	 Catch:{ Exception -> 0x00e0 }
        r21 = java.lang.Integer.valueOf(r9);	 Catch:{ Exception -> 0x00e0 }
        r0 = r21;
        r6.add(r0);	 Catch:{ Exception -> 0x00e0 }
        goto L_0x0037;
    L_0x00e0:
        r19 = move-exception;
        r21 = "vk";
        r0 = r21;
        r1 = r19;
        com.vkontakte.android.Log.m532w(r0, r1);
        goto L_0x0037;
    L_0x00ec:
        r14 = r21.next();
        r14 = (com.vkontakte.android.data.StickerPack) r14;
        r0 = r14.state;
        r20 = r0;
        r22 = 6;
        r0 = r20;
        r1 = r22;
        if (r0 == r1) goto L_0x03af;
    L_0x00fe:
        r20 = r15.edit();
        r22 = new java.lang.StringBuilder;
        r23 = "owned";
        r22.<init>(r23);
        r0 = r14.id;
        r23 = r0;
        r22 = r22.append(r23);
        r22 = r22.toString();
        r23 = 1;
        r0 = r20;
        r1 = r22;
        r2 = r23;
        r20 = r0.putBoolean(r1, r2);
        r22 = new java.lang.StringBuilder;
        r23 = "confirmed";
        r22.<init>(r23);
        r0 = r14.id;
        r23 = r0;
        r22 = r22.append(r23);
        r22 = r22.toString();
        r23 = 1;
        r0 = r20;
        r1 = r22;
        r2 = r23;
        r20 = r0.putBoolean(r1, r2);
        r22 = new java.lang.StringBuilder;
        r23 = "content";
        r22.<init>(r23);
        r0 = r14.id;
        r23 = r0;
        r22 = r22.append(r23);
        r22 = r22.toString();
        r0 = r14.downloadLink;
        r23 = r0;
        r0 = r20;
        r1 = r22;
        r2 = r23;
        r20 = r0.putString(r1, r2);
        r22 = new java.lang.StringBuilder;
        r23 = "base_url";
        r22.<init>(r23);
        r0 = r14.id;
        r23 = r0;
        r22 = r22.append(r23);
        r22 = r22.toString();
        r0 = r14.baseURL;
        r23 = r0;
        r0 = r20;
        r1 = r22;
        r2 = r23;
        r20 = r0.putString(r1, r2);
        r22 = new java.lang.StringBuilder;
        r23 = "s_base_url";
        r22.<init>(r23);
        r0 = r14.id;
        r23 = r0;
        r22 = r22.append(r23);
        r22 = r22.toString();
        r0 = r14.stickersBaseURL;
        r23 = r0;
        r0 = r20;
        r1 = r22;
        r2 = r23;
        r20 = r0.putString(r1, r2);
        r22 = new java.lang.StringBuilder;
        r23 = "ordering";
        r22.<init>(r23);
        r0 = r14.id;
        r23 = r0;
        r22 = r22.append(r23);
        r22 = r22.toString();
        r23 = ",";
        r0 = r14.ids;
        r24 = r0;
        r23 = android.text.TextUtils.join(r23, r24);
        r0 = r20;
        r1 = r22;
        r2 = r23;
        r20 = r0.putString(r1, r2);
        r20.commit();
        r0 = r14.id;
        r20 = r0;
        r20 = java.lang.Integer.valueOf(r20);
        r0 = r20;
        r6.remove(r0);
        r0 = r14.state;
        r20 = r0;
        r22 = 5;
        r0 = r20;
        r1 = r22;
        if (r0 == r1) goto L_0x01f3;
    L_0x01e6:
        r0 = r14.id;
        r20 = r0;
        r20 = java.lang.Integer.valueOf(r20);
        r0 = r20;
        r13.add(r0);
    L_0x01f3:
        r0 = r14.state;
        r20 = r0;
        r22 = 4;
        r0 = r20;
        r1 = r22;
        if (r0 != r1) goto L_0x022f;
    L_0x01ff:
        r7 = new java.io.File;
        r20 = com.vkontakte.android.VKApplication.context;
        r20 = r20.getFilesDir();
        r22 = new java.lang.StringBuilder;
        r23 = "stickers/";
        r22.<init>(r23);
        r0 = r14.id;
        r23 = r0;
        r22 = r22.append(r23);
        r22 = r22.toString();
        r0 = r20;
        r1 = r22;
        r7.<init>(r0, r1);
        r0 = r14.ids;
        r20 = r0;
        r22 = r20.iterator();
    L_0x0229:
        r20 = r22.hasNext();
        if (r20 != 0) goto L_0x02b2;
    L_0x022f:
        r0 = r14.state;
        r20 = r0;
        r22 = 3;
        r0 = r20;
        r1 = r22;
        if (r0 != r1) goto L_0x004b;
    L_0x023b:
        r0 = r14.id;
        r20 = r0;
        r20 = java.lang.Integer.valueOf(r20);
        r0 = r20;
        r20 = r5.contains(r0);
        if (r20 == 0) goto L_0x004b;
    L_0x024b:
        r10 = new android.content.Intent;
        r20 = com.vkontakte.android.VKApplication.context;
        r22 = com.vkontakte.android.StickerDownloaderService.class;
        r0 = r20;
        r1 = r22;
        r10.<init>(r0, r1);
        r20 = "id";
        r0 = r14.id;
        r22 = r0;
        r0 = r20;
        r1 = r22;
        r10.putExtra(r0, r1);
        r20 = "url";
        r22 = com.vkontakte.android.VKApplication.context;
        r23 = "stickers";
        r24 = 0;
        r22 = r22.getSharedPreferences(r23, r24);
        r23 = new java.lang.StringBuilder;
        r24 = "content";
        r23.<init>(r24);
        r0 = r14.id;
        r24 = r0;
        r23 = r23.append(r24);
        r23 = r23.toString();
        r24 = "";
        r22 = r22.getString(r23, r24);
        r0 = r20;
        r1 = r22;
        r10.putExtra(r0, r1);
        r20 = "title";
        r0 = r14.title;
        r22 = r0;
        r0 = r20;
        r1 = r22;
        r10.putExtra(r0, r1);
        r20 = "silent";
        r22 = 1;
        r0 = r20;
        r1 = r22;
        r10.putExtra(r0, r1);
        r20 = com.vkontakte.android.VKApplication.context;
        r0 = r20;
        r0.startService(r10);
        goto L_0x004b;
    L_0x02b2:
        r20 = r22.next();
        r20 = (java.lang.Integer) r20;
        r9 = r20.intValue();
        r20 = new java.io.File;
        r23 = new java.lang.StringBuilder;
        r24 = java.lang.String.valueOf(r9);
        r23.<init>(r24);
        r24 = "_64b.png";
        r23 = r23.append(r24);
        r23 = r23.toString();
        r0 = r20;
        r1 = r23;
        r0.<init>(r7, r1);
        r20 = r20.exists();
        if (r20 != 0) goto L_0x0229;
    L_0x02de:
        r20 = "vk";
        r23 = new java.lang.StringBuilder;
        r24 = "image for sticker ";
        r23.<init>(r24);
        r0 = r23;
        r23 = r0.append(r9);
        r24 = " is missing!";
        r23 = r23.append(r24);
        r23 = r23.toString();
        r0 = r20;
        r1 = r23;
        com.vkontakte.android.Log.m525d(r0, r1);
        r20 = 3;
        r0 = r20;
        r0 = new int[r0];
        r18 = r0;
        r18 = {64, 128, 256};
        r0 = r18;
        r0 = r0.length;
        r23 = r0;
        r20 = 0;
    L_0x0310:
        r0 = r20;
        r1 = r23;
        if (r0 >= r1) goto L_0x0229;
    L_0x0316:
        r17 = r18[r20];
        r10 = new android.content.Intent;
        r24 = com.vkontakte.android.VKApplication.context;
        r25 = com.vkontakte.android.StickerDownloaderService.class;
        r0 = r24;
        r1 = r25;
        r10.<init>(r0, r1);
        r24 = "url";
        r25 = new java.lang.StringBuilder;
        r0 = r14.stickersBaseURL;
        r26 = r0;
        r26 = java.lang.String.valueOf(r26);
        r25.<init>(r26);
        r0 = r25;
        r25 = r0.append(r9);
        r26 = "/";
        r25 = r25.append(r26);
        r0 = r25;
        r1 = r17;
        r25 = r0.append(r1);
        r26 = "b.png";
        r25 = r25.append(r26);
        r25 = r25.toString();
        r0 = r24;
        r1 = r25;
        r10.putExtra(r0, r1);
        r24 = "dest_path";
        r25 = new java.io.File;
        r26 = com.vkontakte.android.VKApplication.context;
        r26 = r26.getFilesDir();
        r27 = new java.lang.StringBuilder;
        r28 = "stickers/";
        r27.<init>(r28);
        r0 = r14.id;
        r28 = r0;
        r27 = r27.append(r28);
        r28 = "/";
        r27 = r27.append(r28);
        r0 = r27;
        r27 = r0.append(r9);
        r28 = "_";
        r27 = r27.append(r28);
        r0 = r27;
        r1 = r17;
        r27 = r0.append(r1);
        r28 = "b.png";
        r27 = r27.append(r28);
        r27 = r27.toString();
        r25.<init>(r26, r27);
        r25 = r25.getAbsolutePath();
        r0 = r24;
        r1 = r25;
        r10.putExtra(r0, r1);
        r24 = com.vkontakte.android.VKApplication.context;
        r0 = r24;
        r0.startService(r10);
        r20 = r20 + 1;
        goto L_0x0310;
    L_0x03af:
        r0 = r14.id;
        r20 = r0;
        r20 = java.lang.Integer.valueOf(r20);
        r0 = r16;
        r1 = r20;
        r0.add(r1);
        goto L_0x004b;
    L_0x03c0:
        r20 = r21.next();
        r20 = (java.lang.Integer) r20;
        r9 = r20.intValue();
        r20 = r15.edit();
        r22 = new java.lang.StringBuilder;
        r23 = "owned";
        r22.<init>(r23);
        r0 = r22;
        r22 = r0.append(r9);
        r22 = r22.toString();
        r0 = r20;
        r1 = r22;
        r20 = r0.remove(r1);
        r22 = new java.lang.StringBuilder;
        r23 = "confirmed";
        r22.<init>(r23);
        r0 = r22;
        r22 = r0.append(r9);
        r22 = r22.toString();
        r0 = r20;
        r1 = r22;
        r20 = r0.remove(r1);
        r22 = new java.lang.StringBuilder;
        r23 = "content";
        r22.<init>(r23);
        r0 = r22;
        r22 = r0.append(r9);
        r22 = r22.toString();
        r0 = r20;
        r1 = r22;
        r20 = r0.remove(r1);
        r22 = new java.lang.StringBuilder;
        r23 = "token";
        r22.<init>(r23);
        r0 = r22;
        r22 = r0.append(r9);
        r22 = r22.toString();
        r0 = r20;
        r1 = r22;
        r20 = r0.remove(r1);
        r20.commit();
        goto L_0x0080;
    L_0x0437:
        r22 = move-exception;
        goto L_0x00a5;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vkontakte.android.data.Stickers.doUpdateInfo(java.util.List):void");
    }

    public static boolean hasNewStockItems() {
        return VKApplication.context.getSharedPreferences("stickers", 0).getBoolean("has_new", false);
    }

    public static void activateFreePack(int id, Activity act, Runnable onSuccess) {
        new StorePurchase(id, null, null, null).setCallback(new C14292(act, id, onSuccess)).wrapProgress(act).exec(act);
    }

    public static List<Integer> getActivePacks() {
        List<Integer> ls = Global.stringToIntArray(VKApplication.context.getSharedPreferences("stickers", 0).getString("order", ACRAConstants.DEFAULT_STRING_VALUE));
        ArrayList<Integer> result = new ArrayList();
        for (Integer intValue : ls) {
            int id = intValue.intValue();
            int state = getPackState(id);
            if ((state == 4 || state == 3) && !result.contains(Integer.valueOf(id))) {
                result.add(Integer.valueOf(id));
            }
        }
        return result;
    }

    public static List<Integer> getPromotedPacks() {
        List<Integer> ls = Global.stringToIntArray(VKApplication.context.getSharedPreferences("stickers", 0).getString("promoted", ACRAConstants.DEFAULT_STRING_VALUE));
        ArrayList<Integer> result = new ArrayList();
        for (Integer intValue : ls) {
            int id = intValue.intValue();
            if (getPackState(id) == 0) {
                result.add(Integer.valueOf(id));
            }
        }
        return result;
    }

    public static List<StickerAttachment> getStickers(int pack) {
        SharedPreferences prefs = VKApplication.context.getSharedPreferences("stickers", 0);
        ArrayList<StickerAttachment> res = new ArrayList();
        File dir = new File(VKApplication.context.getFilesDir(), "stickers/" + pack);
        if (!dir.exists()) {
            return res;
        }
        int i = 3;
        int[] sizes = new int[]{64, TransportMediator.FLAG_KEY_MEDIA_NEXT, Views.PHOTO_VIEW};
        List<Integer> ids = Global.stringToIntArray(prefs.getString("ordering" + pack, ACRAConstants.DEFAULT_STRING_VALUE));
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(new File(dir, "sizes")));
            HashMap<Integer, Point> fsizes = new HashMap();
            if (in.readInt() != 1) {
                in.close();
                StickerDownloaderService.generateSizesFile(dir);
                return getStickers(pack);
            }
            int i2;
            int id;
            int count = in.readInt();
            for (i2 = 0; i2 < count; i2++) {
                id = in.readInt();
                Point pt = new Point();
                pt.x = in.readInt();
                pt.y = in.readInt();
                fsizes.put(Integer.valueOf(id), pt);
            }
            in.close();
            for (Integer intValue : ids) {
                id = intValue.intValue();
                StickerAttachment att = new StickerAttachment();
                att.id = id;
                att.images = new String[3];
                for (i2 = 0; i2 < 3; i2++) {
                    att.images[i2] = "A|file://" + new File(dir, new StringBuilder(String.valueOf(id)).append("_").append(sizes[i2]).append("b.png").toString()).getAbsolutePath() + "|" + prefs.getString("s_base_url" + pack, ACRAConstants.DEFAULT_STRING_VALUE) + id + "/" + sizes[i2] + "b.png";
                }
                Point p = (Point) fsizes.get(Integer.valueOf(id));
                if (p != null) {
                    att.width = p.x;
                    att.height = p.y;
                } else {
                    att.height = Views.PHOTO_VIEW;
                    att.width = Views.PHOTO_VIEW;
                    Log.m530w("vk", "Size for sticker " + id + " is unknown!");
                }
                res.add(att);
            }
            return res;
        } catch (Exception e) {
            return res;
        }
    }

    public static String getPackBackgroundImage(int pack) {
        return new StringBuilder(GalleryPickerProvider.UNSTYLED_URI_SCHEME).append(new File(VKApplication.context.getFilesDir(), "stickers/" + pack + "/background.png").getAbsolutePath()).toString();
    }

    public static void deleteDownloadedPack(int id) {
        try {
            File dir = new File(VKApplication.context.getFilesDir(), "stickers/" + id);
            if (dir.exists()) {
                for (File f : dir.listFiles()) {
                    if (!(f.getName().equals(".") || f.getName().equals(".."))) {
                        f.delete();
                    }
                }
                dir.delete();
            }
        } catch (Exception e) {
        }
    }

    public static void getPrices(Activity act, List<String> ids, GetPricesCallback callback) {
        if (!act.bindService(new Intent("com.android.vending.billing.InAppBillingService.BIND"), new C05713(ids, act, callback), 1)) {
            callback.onError(-2, "Google Play not available");
        }
    }

    public static boolean isPlayStoreInstalled() {
        if (!Global.isAppInstalled(VKApplication.context, GooglePlayServicesUtil.GOOGLE_PLAY_STORE_PACKAGE)) {
            return false;
        }
        if (VKApplication.context.getPackageManager().resolveService(new Intent("com.android.vending.billing.InAppBillingService.BIND"), 0) != null) {
            return true;
        }
        return false;
    }

    public static String[] getTabIcon(int pack) {
        int res = 34;
        if (Global.displayDensity > 1.0f) {
            res = 51;
        }
        if (Global.displayDensity > 1.5f) {
            res = 68;
        }
        if (Global.displayDensity > ImageViewHolder.PaddingSize) {
            res = SuggestionsFriendsFragment.GPLUS_ERROR_RESULT;
        }
        if (getPackState(pack) == 4) {
            return new String[]{new StringBuilder(GalleryPickerProvider.UNSTYLED_URI_SCHEME).append(new File(VKApplication.context.getFilesDir(), "stickers/" + pack + "/thumb_" + res + ".png").getAbsolutePath()).toString(), new StringBuilder(GalleryPickerProvider.UNSTYLED_URI_SCHEME).append(new File(VKApplication.context.getFilesDir(), "stickers/" + pack + "/thumb_" + res + "b.png").getAbsolutePath()).toString()};
        }
        return new String[]{"http://vk.com/images/store/stickers/" + pack + "/thumb_" + res + ".png", "http://vk.com/images/store/stickers/" + pack + "/thumb_" + res + "b.png"};
    }

    public static void broadcastUpdate() {
        VKApplication.context.sendBroadcast(new Intent(ACTION_STICKERS_UPDATED), permission.ACCESS_DATA);
    }
}
