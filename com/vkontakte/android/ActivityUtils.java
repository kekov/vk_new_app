package com.vkontakte.android;

import android.app.Activity;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Build.VERSION;

public class ActivityUtils {
    public static void setBeamLink(Activity act, String link) {
        if (VERSION.SDK_INT >= 14) {
            NfcAdapter nfc = NfcAdapter.getDefaultAdapter(act);
            if (nfc != null) {
                nfc.setNdefPushMessage(new NdefMessage(new NdefRecord[]{NdefRecord.createUri(Uri.parse("http://m.vk.com/" + link))}), act, new Activity[0]);
            }
        }
    }
}
