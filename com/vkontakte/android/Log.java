package com.vkontakte.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.Locale;
import org.acra.ACRAConstants;

public class Log {
    public static File logFile;
    private static FileOutputStream os;

    public static void m529v(String tag, String msg) {
        android.util.Log.d(tag, msg);
        lf("V", tag, msg);
    }

    public static void m525d(String tag, String msg) {
        android.util.Log.d(tag, msg);
        lf("D", tag, msg);
    }

    public static void m528i(String tag, String msg) {
        android.util.Log.i(tag, msg);
        lf("I", tag, msg);
    }

    public static void m530w(String tag, String msg) {
        android.util.Log.w(tag, msg);
        lf("W", tag, msg);
    }

    public static void m531w(String tag, String msg, Throwable x) {
        android.util.Log.w(tag, msg, x);
        StringWriter sw = new StringWriter();
        x.printStackTrace(new PrintWriter(sw));
        lf("W", tag, new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(msg)).append("\n").append(x.getClass()).append(": ").append(x.getMessage()).append("\n").toString())).append(sw.toString()).toString());
    }

    public static void m532w(String tag, Throwable x) {
        android.util.Log.w(tag, x);
        StringWriter sw = new StringWriter();
        x.printStackTrace(new PrintWriter(sw));
        lf("W", tag, new StringBuilder(String.valueOf(ACRAConstants.DEFAULT_STRING_VALUE + x.getClass() + ": " + x.getMessage() + "\n")).append(sw.toString()).toString());
    }

    public static void m526e(String tag, String msg) {
        android.util.Log.e(tag, msg);
        lf("E", tag, msg);
    }

    public static void m527e(String tag, String msg, Throwable x) {
        android.util.Log.e(tag, msg, x);
        StringWriter sw = new StringWriter();
        x.printStackTrace(new PrintWriter(sw));
        lf("E", tag, new StringBuilder(String.valueOf(new StringBuilder(String.valueOf(msg)).append("\n").append(x.getClass()).append(": ").append(x.getMessage()).append("\n").toString())).append(sw.toString()).toString());
    }

    private static void lf(String lvl, String tag, String msg) {
        if (logFile != null) {
            try {
                if (os == null) {
                    logFile.createNewFile();
                    os = new FileOutputStream(logFile);
                }
                Date d = new Date(System.currentTimeMillis());
                for (String line : msg.split("\n")) {
                    os.write(String.format(Locale.US, "%d:%02d:%02d.%03d\t%s\t%s\t%s\n", new Object[]{Integer.valueOf(d.getHours()), Integer.valueOf(d.getMinutes()), Integer.valueOf(d.getSeconds()), Long.valueOf(t % 1000), lvl, tag, line}).getBytes("UTF-8"));
                    os.flush();
                }
            } catch (Exception e) {
            }
        }
    }
}
