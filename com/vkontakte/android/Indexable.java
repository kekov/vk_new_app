package com.vkontakte.android;

public interface Indexable {
    char[] getIndexChars();

    boolean matches(String str);
}
