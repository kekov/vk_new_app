package com.vkontakte.android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;
import org.acra.ACRAConstants;

public class SearchIndexer<T extends Indexable> {
    private static final String[] translit1;
    private static final String[] translit2;
    private HashMap<String, ArrayList<T>> index;
    private List<T> objects;
    private Semaphore semaphore;

    /* renamed from: com.vkontakte.android.SearchIndexer.1 */
    class C04571 implements Runnable {
        C04571() {
        }

        public void run() {
            try {
                SearchIndexer.this.semaphore.acquire();
                SearchIndexer.this.index.clear();
                for (Indexable obj : SearchIndexer.this.objects) {
                    SearchIndexer.this.add(obj);
                }
                SearchIndexer.this.semaphore.release();
            } catch (Exception e) {
                SearchIndexer.this.index.clear();
            }
        }
    }

    static {
        translit1 = new String[]{"\u0449", "\u0436", "\u0447", "\u0448", "\u044e", "\u044f", "\u0430", "\u0431", "\u0432", "\u0433", "\u0434", "\u0435", "\u0437", "\u0438", "\u0439", "\u043a", "\u043b", "\u043c", "\u043d", "\u043e", "\u043f", "\u0440", "\u0441", "\u0442", "\u0443", "\u0444", "\u0445", "\u0446", "\u044a", "\u044b", "\u044c", "\u044d"};
        translit2 = new String[]{"sch", "zh", "ch", "sh", "yu", "ya", "a", "b", "v", "g", "d", "e", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "c", ACRAConstants.DEFAULT_STRING_VALUE, "y", ACRAConstants.DEFAULT_STRING_VALUE, "e"};
    }

    public SearchIndexer() {
        this.index = new HashMap();
        this.semaphore = new Semaphore(1, true);
    }

    public void bind(List<T> list) {
        this.objects = list;
    }

    public void build() {
        if (this.objects == null) {
            throw new IllegalStateException("Object list is null");
        }
        new Thread(new C04571()).start();
    }

    public void add(T obj) {
        for (char c : obj.getIndexChars()) {
            String s = Character.toString(c);
            try {
                if (!this.index.containsKey(s) || this.index.get(s) == null) {
                    this.index.put(s, new ArrayList());
                }
                if (!((ArrayList) this.index.get(s)).contains(obj)) {
                    ((ArrayList) this.index.get(s)).add(obj);
                }
            } catch (Exception e) {
            }
        }
    }

    public List<T> search(String q) {
        String qTranslit;
        q = q.toLowerCase();
        String qTranslit1 = q;
        String qTranslit2 = q;
        for (int i = 0; i < translit1.length; i++) {
            qTranslit1 = qTranslit1.replace(translit1[i], translit2[i]);
            if (translit2[i].length() > 0) {
                qTranslit2 = qTranslit2.replace(translit2[i], translit1[i]);
            }
        }
        if (qTranslit1.equals(q)) {
            qTranslit = qTranslit2;
        } else {
            qTranslit = qTranslit1;
        }
        ArrayList<T> result = new ArrayList();
        if (q == null || q.length() <= 0) {
            result.addAll(this.objects);
        } else {
            Iterator it;
            Indexable obj;
            ArrayList<T> m = (ArrayList) this.index.get(q.substring(0, 1));
            if (m != null) {
                try {
                    this.semaphore.acquire();
                } catch (Exception e) {
                }
                it = m.iterator();
                while (it.hasNext()) {
                    obj = (Indexable) it.next();
                    if (obj != null && obj.matches(q)) {
                        result.add(obj);
                    }
                }
                this.semaphore.release();
            }
            if (qTranslit.length() > 0) {
                m = (ArrayList) this.index.get(qTranslit.substring(0, 1));
                if (m != null) {
                    try {
                        this.semaphore.acquire();
                    } catch (Exception e2) {
                    }
                    it = m.iterator();
                    while (it.hasNext()) {
                        obj = (Indexable) it.next();
                        if (!(obj == null || !obj.matches(qTranslit) || result.contains(obj))) {
                            result.add(obj);
                        }
                    }
                    this.semaphore.release();
                }
            }
        }
        return result;
    }
}
