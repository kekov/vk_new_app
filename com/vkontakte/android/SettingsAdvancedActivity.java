package com.vkontakte.android;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceManager;
import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.MenuItem;
import com.vkontakte.android.VKAlertDialog.Builder;
import com.vkontakte.android.cache.AudioCache;
import org.acra.ACRAConstants;

public class SettingsAdvancedActivity extends SherlockPreferenceActivity {
    private String initialCacheLocation;

    /* renamed from: com.vkontakte.android.SettingsAdvancedActivity.1 */
    class C04861 implements OnPreferenceClickListener {

        /* renamed from: com.vkontakte.android.SettingsAdvancedActivity.1.1 */
        class C04851 implements Runnable {
            private final /* synthetic */ ProgressDialog val$dlg;

            /* renamed from: com.vkontakte.android.SettingsAdvancedActivity.1.1.1 */
            class C04841 implements Runnable {
                private final /* synthetic */ ProgressDialog val$dlg;

                C04841(ProgressDialog progressDialog) {
                    this.val$dlg = progressDialog;
                }

                public void run() {
                    this.val$dlg.dismiss();
                }
            }

            C04851(ProgressDialog progressDialog) {
                this.val$dlg = progressDialog;
            }

            public void run() {
                ImageCache.clear();
                SettingsAdvancedActivity.this.runOnUiThread(new C04841(this.val$dlg));
            }
        }

        C04861() {
        }

        public boolean onPreferenceClick(Preference preference) {
            ProgressDialog dlg = new ProgressDialog(SettingsAdvancedActivity.this);
            dlg.setMessage(SettingsAdvancedActivity.this.getResources().getString(C0436R.string.loading));
            dlg.show();
            dlg.setCancelable(false);
            new Thread(new C04851(dlg)).start();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.SettingsAdvancedActivity.2 */
    class C04902 implements OnPreferenceClickListener {

        /* renamed from: com.vkontakte.android.SettingsAdvancedActivity.2.1 */
        class C04891 implements OnClickListener {

            /* renamed from: com.vkontakte.android.SettingsAdvancedActivity.2.1.1 */
            class C04881 implements Runnable {
                private final /* synthetic */ ProgressDialog val$dlg;

                /* renamed from: com.vkontakte.android.SettingsAdvancedActivity.2.1.1.1 */
                class C04871 implements Runnable {
                    private final /* synthetic */ ProgressDialog val$dlg;

                    C04871(ProgressDialog progressDialog) {
                        this.val$dlg = progressDialog;
                    }

                    public void run() {
                        this.val$dlg.dismiss();
                    }
                }

                C04881(ProgressDialog progressDialog) {
                    this.val$dlg = progressDialog;
                }

                public void run() {
                    AudioCache.clear();
                    SettingsAdvancedActivity.this.runOnUiThread(new C04871(this.val$dlg));
                }
            }

            C04891() {
            }

            public void onClick(DialogInterface dialog, int which) {
                ProgressDialog dlg = new ProgressDialog(SettingsAdvancedActivity.this);
                dlg.setMessage(SettingsAdvancedActivity.this.getResources().getString(C0436R.string.loading));
                dlg.show();
                dlg.setCancelable(false);
                new Thread(new C04881(dlg)).start();
            }
        }

        C04902() {
        }

        public boolean onPreferenceClick(Preference preference) {
            new Builder(SettingsAdvancedActivity.this).setTitle(C0436R.string.confirm).setMessage(C0436R.string.clear_audio_cache_confirm).setIcon(ACRAConstants.DEFAULT_DIALOG_ICON).setPositiveButton(C0436R.string.yes, new C04891()).setNegativeButton(C0436R.string.no, null).show();
            return true;
        }
    }

    /* renamed from: com.vkontakte.android.SettingsAdvancedActivity.3 */
    class C04913 implements OnPreferenceClickListener {
        C04913() {
        }

        public boolean onPreferenceClick(Preference preference) {
            ((DialogPreference) preference).getDialog().getWindow().setBackgroundDrawableResource(C0436R.drawable.transparent);
            return false;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        this.initialCacheLocation = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).getString("imgCacheLocation", "internal");
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(C0436R.xml.preferences_adv);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getResources().getConfiguration().keyboard != 2) {
            getPreferenceScreen().removePreference(findPreference("sendByEnter"));
        }
        if (!Global.maybeTablet) {
            getPreferenceScreen().removePreference(findPreference("forceTabletUI"));
        }
        findPreference("clearImageCache").setOnPreferenceClickListener(new C04861());
        findPreference("clearAudioCache").setOnPreferenceClickListener(new C04902());
        Preference pref = findPreference("fontSize");
        if (pref != null) {
            pref.setOnPreferenceClickListener(new C04913());
        }
        getListView().setVerticalFadingEdgeEnabled(false);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return super.onOptionsItemSelected(item);
        }
        onBackPressed();
        return true;
    }
}
