package com.tonicartos.widget.stickygridheaders;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StickyGridHeadersBaseAdapterWrapper extends BaseAdapter {
    protected static final int ID_FILLER = -2;
    protected static final int ID_HEADER = -1;
    protected static final int POSITION_FILLER = -1;
    protected static final int POSITION_HEADER = -2;
    protected static final int VIEW_TYPE_FILLER = 0;
    protected static final int VIEW_TYPE_HEADER = 1;
    private static boolean sCurrentlySizingRow = false;
    private static final int sNumViewTypes = 2;
    private final Context mContext;
    private int mCount;
    private DataSetObserver mDataSetObserver;
    private final StickyGridHeadersBaseAdapter mDelegate;
    private StickyGridHeadersGridView mGridView;
    private final List<View> mHeaderCache;
    private int mNumColumns;
    private View[] mRowSiblings;

    /* renamed from: com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapterWrapper.1 */
    class C01521 extends DataSetObserver {
        C01521() {
        }

        public void onChanged() {
            StickyGridHeadersBaseAdapterWrapper.this.updateCount();
            StickyGridHeadersBaseAdapterWrapper.this.notifyDataSetChanged();
        }

        public void onInvalidated() {
            StickyGridHeadersBaseAdapterWrapper.this.mHeaderCache.clear();
            StickyGridHeadersBaseAdapterWrapper.this.notifyDataSetInvalidated();
        }
    }

    protected class FillerView extends View {
        public FillerView(Context context) {
            super(context);
        }

        public FillerView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public FillerView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }
    }

    protected class HeaderFillerView extends FrameLayout {
        private int mHeaderId;
        private int mHeaderWidth;

        public HeaderFillerView(Context context) {
            super(context);
        }

        public HeaderFillerView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public HeaderFillerView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        public int getHeaderId() {
            return this.mHeaderId;
        }

        public void setHeaderId(int headerId) {
            this.mHeaderId = headerId;
        }

        public void setHeaderWidth(int width) {
            this.mHeaderWidth = width;
        }

        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            View v = (View) getTag();
            if (v.getLayoutParams() == null) {
                v.setLayoutParams(generateDefaultLayoutParams());
            }
            if (v.getVisibility() != 8 && v.getMeasuredHeight() == 0) {
                v.measure(MeasureSpec.makeMeasureSpec(this.mHeaderWidth, 1073741824), MeasureSpec.makeMeasureSpec(StickyGridHeadersBaseAdapterWrapper.VIEW_TYPE_FILLER, StickyGridHeadersBaseAdapterWrapper.VIEW_TYPE_FILLER));
            }
            setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), v.getMeasuredHeight());
        }

        protected LayoutParams generateDefaultLayoutParams() {
            return new LayoutParams(StickyGridHeadersBaseAdapterWrapper.POSITION_FILLER, StickyGridHeadersBaseAdapterWrapper.POSITION_FILLER);
        }
    }

    protected class HeaderHolder {
        protected View mHeaderView;

        protected HeaderHolder() {
        }
    }

    protected class Position {
        protected int mHeader;
        protected int mPosition;

        protected Position(int position, int header) {
            this.mPosition = position;
            this.mHeader = header;
        }
    }

    protected class ReferenceView extends FrameLayout {
        private boolean mForceMeasureDisabled;
        private int mNumColumns;
        private int mPosition;
        private View[] mRowSiblings;

        public ReferenceView(Context context) {
            super(context);
        }

        public ReferenceView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @TargetApi(11)
        public ReferenceView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        public Object getTag() {
            return getChildAt(StickyGridHeadersBaseAdapterWrapper.VIEW_TYPE_FILLER).getTag();
        }

        public Object getTag(int key) {
            return getChildAt(StickyGridHeadersBaseAdapterWrapper.VIEW_TYPE_FILLER).getTag(key);
        }

        public View getView() {
            return getChildAt(StickyGridHeadersBaseAdapterWrapper.VIEW_TYPE_FILLER);
        }

        public void setNumColumns(int numColumns) {
            this.mNumColumns = numColumns;
        }

        public void setPosition(int position) {
            this.mPosition = position;
        }

        @SuppressLint({"NewApi"})
        public void setRowSiblings(View[] rowSiblings) {
            this.mRowSiblings = rowSiblings;
        }

        public void setTag(int key, Object tag) {
            getChildAt(StickyGridHeadersBaseAdapterWrapper.VIEW_TYPE_FILLER).setTag(key, tag);
        }

        public void setTag(Object tag) {
            getChildAt(StickyGridHeadersBaseAdapterWrapper.VIEW_TYPE_FILLER).setTag(tag);
        }

        private void forceRowMeasurement(int widthMeasureSpec, int heightMeasureSpec) {
            if (!this.mForceMeasureDisabled) {
                this.mForceMeasureDisabled = true;
                View[] viewArr = this.mRowSiblings;
                int length = viewArr.length;
                for (int i = StickyGridHeadersBaseAdapterWrapper.VIEW_TYPE_FILLER; i < length; i += StickyGridHeadersBaseAdapterWrapper.VIEW_TYPE_HEADER) {
                    viewArr[i].measure(widthMeasureSpec, heightMeasureSpec);
                }
                this.mForceMeasureDisabled = false;
            }
        }

        protected LayoutParams generateDefaultLayoutParams() {
            return new LayoutParams(StickyGridHeadersBaseAdapterWrapper.POSITION_FILLER, StickyGridHeadersBaseAdapterWrapper.POSITION_FILLER);
        }

        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            if (this.mNumColumns != StickyGridHeadersBaseAdapterWrapper.VIEW_TYPE_HEADER && StickyGridHeadersBaseAdapterWrapper.this.mRowSiblings != null) {
                if (this.mPosition % this.mNumColumns == 0) {
                    forceRowMeasurement(widthMeasureSpec, heightMeasureSpec);
                }
                int measuredHeight = getMeasuredHeight();
                int maxHeight = measuredHeight;
                View[] viewArr = this.mRowSiblings;
                int length = viewArr.length;
                for (int i = StickyGridHeadersBaseAdapterWrapper.VIEW_TYPE_FILLER; i < length; i += StickyGridHeadersBaseAdapterWrapper.VIEW_TYPE_HEADER) {
                    View rowSibling = viewArr[i];
                    if (rowSibling != null) {
                        maxHeight = Math.max(maxHeight, rowSibling.getMeasuredHeight());
                    }
                }
                if (maxHeight != measuredHeight) {
                    super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(maxHeight, 1073741824));
                }
            }
        }
    }

    public StickyGridHeadersBaseAdapterWrapper(Context context, StickyGridHeadersGridView gridView, StickyGridHeadersBaseAdapter delegate) {
        this.mDataSetObserver = new C01521();
        this.mHeaderCache = new ArrayList();
        this.mNumColumns = VIEW_TYPE_HEADER;
        this.mContext = context;
        this.mDelegate = delegate;
        this.mGridView = gridView;
        delegate.registerDataSetObserver(this.mDataSetObserver);
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public int getCount() {
        this.mCount = VIEW_TYPE_FILLER;
        int numHeaders = this.mDelegate.getNumHeaders();
        if (numHeaders == 0) {
            return this.mDelegate.getCount();
        }
        for (int i = VIEW_TYPE_FILLER; i < numHeaders; i += VIEW_TYPE_HEADER) {
            this.mCount += (this.mDelegate.getCountForHeader(i) + unFilledSpacesInHeaderGroup(i)) + this.mNumColumns;
        }
        return this.mCount;
    }

    public Object getItem(int position) throws ArrayIndexOutOfBoundsException {
        Position adapterPosition = translatePosition(position);
        if (adapterPosition.mPosition == POSITION_FILLER || adapterPosition.mPosition == POSITION_HEADER) {
            return null;
        }
        return this.mDelegate.getItem(adapterPosition.mPosition);
    }

    public long getItemId(int position) {
        Position adapterPosition = translatePosition(position);
        if (adapterPosition.mPosition == POSITION_HEADER) {
            return -1;
        }
        if (adapterPosition.mPosition == POSITION_FILLER) {
            return -2;
        }
        return this.mDelegate.getItemId(adapterPosition.mPosition);
    }

    public int getItemViewType(int position) {
        Position adapterPosition = translatePosition(position);
        if (adapterPosition.mPosition == POSITION_HEADER) {
            return VIEW_TYPE_HEADER;
        }
        if (adapterPosition.mPosition == POSITION_FILLER) {
            return VIEW_TYPE_FILLER;
        }
        int itemViewType = this.mDelegate.getItemViewType(adapterPosition.mPosition);
        return itemViewType != POSITION_FILLER ? itemViewType + sNumViewTypes : itemViewType;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ReferenceView container = null;
        if (convertView instanceof ReferenceView) {
            container = (ReferenceView) convertView;
            convertView = container.getChildAt(VIEW_TYPE_FILLER);
        }
        Position adapterPosition = translatePosition(position);
        if (adapterPosition.mPosition == POSITION_HEADER) {
            View v = getHeaderFillerView(adapterPosition.mHeader, convertView, parent);
            ((HeaderFillerView) v).setHeaderId(adapterPosition.mHeader);
            v.setTag(this.mDelegate.getHeaderView(adapterPosition.mHeader, (View) v.getTag(), parent));
            convertView = v;
        } else if (adapterPosition.mPosition == POSITION_FILLER) {
            convertView = getFillerView(convertView, parent);
        } else {
            convertView = this.mDelegate.getView(adapterPosition.mPosition, convertView, parent);
        }
        if (container == null) {
            container = new ReferenceView(this.mContext);
        }
        container.removeAllViews();
        container.addView(convertView);
        container.setPosition(position);
        container.setNumColumns(this.mNumColumns);
        this.mRowSiblings[position % this.mNumColumns] = container;
        if (position % this.mNumColumns == 0) {
            sCurrentlySizingRow = true;
            for (int i = VIEW_TYPE_HEADER; i < this.mRowSiblings.length; i += VIEW_TYPE_HEADER) {
                this.mRowSiblings[i] = getView(position + i, null, parent);
            }
            sCurrentlySizingRow = false;
        }
        container.setRowSiblings(this.mRowSiblings);
        if (!sCurrentlySizingRow && (position % this.mNumColumns == this.mNumColumns + POSITION_FILLER || position == getCount() + POSITION_FILLER)) {
            initRowSiblings(this.mNumColumns);
        }
        return container;
    }

    public StickyGridHeadersBaseAdapter getWrappedAdapter() {
        return this.mDelegate;
    }

    public int getViewTypeCount() {
        return this.mDelegate.getViewTypeCount() + sNumViewTypes;
    }

    public boolean hasStableIds() {
        return this.mDelegate.hasStableIds();
    }

    public boolean isEmpty() {
        return this.mDelegate.isEmpty();
    }

    public boolean isEnabled(int position) {
        Position adapterPosition = translatePosition(position);
        if (adapterPosition.mPosition == POSITION_FILLER || adapterPosition.mPosition == POSITION_HEADER) {
            return false;
        }
        return this.mDelegate.isEnabled(adapterPosition.mPosition);
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.mDelegate.registerDataSetObserver(observer);
    }

    public void setNumColumns(int numColumns) {
        this.mNumColumns = numColumns;
        initRowSiblings(numColumns);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        this.mDelegate.unregisterDataSetObserver(observer);
    }

    private FillerView getFillerView(View convertView, ViewGroup parent) {
        FillerView fillerView = (FillerView) convertView;
        if (fillerView == null) {
            return new FillerView(this.mContext);
        }
        return fillerView;
    }

    private View getHeaderFillerView(int headerPosition, View convertView, ViewGroup parent) {
        HeaderFillerView headerFillerView = (HeaderFillerView) convertView;
        headerFillerView = new HeaderFillerView(this.mContext);
        headerFillerView.setHeaderWidth(this.mGridView.getWidth());
        return headerFillerView;
    }

    private void initRowSiblings(int numColumns) {
        this.mRowSiblings = new View[numColumns];
        Arrays.fill(this.mRowSiblings, null);
    }

    private int unFilledSpacesInHeaderGroup(int header) {
        int remainder = this.mDelegate.getCountForHeader(header) % this.mNumColumns;
        return remainder == 0 ? VIEW_TYPE_FILLER : this.mNumColumns - remainder;
    }

    protected long getHeaderId(int position) {
        return (long) translatePosition(position).mHeader;
    }

    protected View getHeaderView(int position, View convertView, ViewGroup parent) {
        if (this.mDelegate.getNumHeaders() == 0) {
            return null;
        }
        return this.mDelegate.getHeaderView(translatePosition(position).mHeader, convertView, parent);
    }

    protected Position translatePosition(int position) {
        int numHeaders = this.mDelegate.getNumHeaders();
        if (numHeaders != 0) {
            int adapterPosition = position;
            int place = position;
            int i = VIEW_TYPE_FILLER;
            while (i < numHeaders) {
                int sectionCount = this.mDelegate.getCountForHeader(i);
                if (place == 0) {
                    return new Position(POSITION_HEADER, i);
                }
                place -= this.mNumColumns;
                if (place < 0) {
                    return new Position(POSITION_FILLER, i);
                }
                adapterPosition -= this.mNumColumns;
                if (place < sectionCount) {
                    return new Position(adapterPosition, i);
                }
                int filler = unFilledSpacesInHeaderGroup(i);
                adapterPosition -= filler;
                place -= sectionCount + filler;
                i += VIEW_TYPE_HEADER;
            }
            return new Position(POSITION_FILLER, i);
        } else if (position >= this.mDelegate.getCount()) {
            return new Position(POSITION_FILLER, VIEW_TYPE_FILLER);
        } else {
            return new Position(position, VIEW_TYPE_FILLER);
        }
    }

    protected void updateCount() {
        this.mCount = VIEW_TYPE_FILLER;
        int numHeaders = this.mDelegate.getNumHeaders();
        if (numHeaders == 0) {
            this.mCount = this.mDelegate.getCount();
            return;
        }
        for (int i = VIEW_TYPE_FILLER; i < numHeaders; i += VIEW_TYPE_HEADER) {
            this.mCount += this.mDelegate.getCountForHeader(i) + this.mNumColumns;
        }
    }
}
