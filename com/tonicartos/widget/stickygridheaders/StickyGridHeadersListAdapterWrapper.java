package com.tonicartos.widget.stickygridheaders;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;

public class StickyGridHeadersListAdapterWrapper extends BaseAdapter implements StickyGridHeadersBaseAdapter {
    private DataSetObserver mDataSetObserver;
    private ListAdapter mDelegate;

    /* renamed from: com.tonicartos.widget.stickygridheaders.StickyGridHeadersListAdapterWrapper.1 */
    class C01561 extends DataSetObserver {
        C01561() {
        }

        public void onChanged() {
            StickyGridHeadersListAdapterWrapper.this.notifyDataSetChanged();
        }

        public void onInvalidated() {
            StickyGridHeadersListAdapterWrapper.this.notifyDataSetInvalidated();
        }
    }

    public StickyGridHeadersListAdapterWrapper(ListAdapter adapter) {
        this.mDataSetObserver = new C01561();
        this.mDelegate = adapter;
        adapter.registerDataSetObserver(this.mDataSetObserver);
    }

    public int getCount() {
        return this.mDelegate.getCount();
    }

    public Object getItem(int position) {
        return this.mDelegate.getItem(position);
    }

    public long getItemId(int position) {
        return this.mDelegate.getItemId(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return this.mDelegate.getView(position, convertView, parent);
    }

    public int getCountForHeader(int header) {
        return 0;
    }

    public int getNumHeaders() {
        return 0;
    }

    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
