package com.google.android.gms.gcm;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import com.google.android.gcm.GCMConstants;
import com.google.android.gms.common.GooglePlayServicesUtil;
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class GoogleCloudMessaging {
    public static final String ERROR_MAIN_THREAD = "MAIN_THREAD";
    public static final String ERROR_SERVICE_NOT_AVAILABLE = "SERVICE_NOT_AVAILABLE";
    public static final String MESSAGE_TYPE_DELETED = "deleted_messages";
    public static final String MESSAGE_TYPE_MESSAGE = "gcm";
    public static final String MESSAGE_TYPE_SEND_ERROR = "send_error";
    static GoogleCloudMessaging ef;
    private Context eg;
    private PendingIntent eh;
    final BlockingQueue<Intent> ei;
    private Handler ej;
    private Messenger ek;

    /* renamed from: com.google.android.gms.gcm.GoogleCloudMessaging.1 */
    class C01051 extends Handler {
        final /* synthetic */ GoogleCloudMessaging el;

        C01051(GoogleCloudMessaging googleCloudMessaging, Looper looper) {
            this.el = googleCloudMessaging;
            super(looper);
        }

        public void handleMessage(Message msg) {
            this.el.ei.add((Intent) msg.obj);
        }
    }

    public GoogleCloudMessaging() {
        this.ei = new LinkedBlockingQueue();
        this.ej = new C01051(this, Looper.getMainLooper());
        this.ek = new Messenger(this.ej);
    }

    private void aw() {
        Intent intent = new Intent(GCMConstants.INTENT_TO_GCM_UNREGISTRATION);
        intent.setPackage(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE);
        this.ei.clear();
        intent.putExtra("google.messenger", this.ek);
        m42b(intent);
        this.eg.startService(intent);
    }

    private void m41b(String... strArr) {
        String c = m43c(strArr);
        Intent intent = new Intent(GCMConstants.INTENT_TO_GCM_REGISTRATION);
        intent.setPackage(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE);
        intent.putExtra("google.messenger", this.ek);
        m42b(intent);
        intent.putExtra(GCMConstants.EXTRA_SENDER, c);
        this.eg.startService(intent);
    }

    public static synchronized GoogleCloudMessaging getInstance(Context context) {
        GoogleCloudMessaging googleCloudMessaging;
        synchronized (GoogleCloudMessaging.class) {
            if (ef == null) {
                ef = new GoogleCloudMessaging();
                ef.eg = context;
            }
            googleCloudMessaging = ef;
        }
        return googleCloudMessaging;
    }

    synchronized void ax() {
        if (this.eh != null) {
            this.eh.cancel();
            this.eh = null;
        }
    }

    synchronized void m42b(Intent intent) {
        if (this.eh == null) {
            this.eh = PendingIntent.getBroadcast(this.eg, 0, new Intent(), 0);
        }
        intent.putExtra(GCMConstants.EXTRA_APPLICATION_PENDING_INTENT, this.eh);
    }

    String m43c(String... strArr) {
        if (strArr == null || strArr.length == 0) {
            throw new IllegalArgumentException("No senderIds");
        }
        StringBuilder stringBuilder = new StringBuilder(strArr[0]);
        for (int i = 1; i < strArr.length; i++) {
            stringBuilder.append(',').append(strArr[i]);
        }
        return stringBuilder.toString();
    }

    public void close() {
        ax();
    }

    public String getMessageType(Intent intent) {
        if (!GCMConstants.INTENT_FROM_GCM_MESSAGE.equals(intent.getAction())) {
            return null;
        }
        String stringExtra = intent.getStringExtra(GCMConstants.EXTRA_SPECIAL_MESSAGE);
        return stringExtra == null ? MESSAGE_TYPE_MESSAGE : stringExtra;
    }

    public String register(String... senderIds) throws IOException {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            throw new IOException(ERROR_MAIN_THREAD);
        }
        this.ei.clear();
        m41b(senderIds);
        try {
            Intent intent = (Intent) this.ei.poll(5000, TimeUnit.MILLISECONDS);
            if (intent == null) {
                throw new IOException(ERROR_SERVICE_NOT_AVAILABLE);
            }
            String stringExtra = intent.getStringExtra(GCMConstants.EXTRA_REGISTRATION_ID);
            if (stringExtra != null) {
                return stringExtra;
            }
            intent.getStringExtra(GCMConstants.EXTRA_ERROR);
            String stringExtra2 = intent.getStringExtra(GCMConstants.EXTRA_ERROR);
            if (stringExtra2 != null) {
                throw new IOException(stringExtra2);
            }
            throw new IOException(ERROR_SERVICE_NOT_AVAILABLE);
        } catch (InterruptedException e) {
            throw new IOException(e.getMessage());
        }
    }

    public void send(String to, String msgId, long timeToLive, Bundle data) throws IOException {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            throw new IOException(ERROR_MAIN_THREAD);
        } else if (to == null) {
            throw new IllegalArgumentException("Missing 'to'");
        } else {
            Intent intent = new Intent("com.google.android.gcm.intent.SEND");
            intent.putExtras(data);
            m42b(intent);
            intent.putExtra("google.to", to);
            intent.putExtra("google.message_id", msgId);
            intent.putExtra("google.ttl", Long.toString(timeToLive));
            this.eg.sendOrderedBroadcast(intent, null);
        }
    }

    public void send(String to, String msgId, Bundle data) throws IOException {
        send(to, msgId, -1, data);
    }

    public void unregister() throws IOException {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            throw new IOException(ERROR_MAIN_THREAD);
        }
        aw();
        try {
            Intent intent = (Intent) this.ei.poll(5000, TimeUnit.MILLISECONDS);
            if (intent == null) {
                throw new IOException(ERROR_SERVICE_NOT_AVAILABLE);
            } else if (intent.getStringExtra(GCMConstants.EXTRA_UNREGISTERED) == null) {
                String stringExtra = intent.getStringExtra(GCMConstants.EXTRA_ERROR);
                if (stringExtra != null) {
                    throw new IOException(stringExtra);
                }
                throw new IOException(ERROR_SERVICE_NOT_AVAILABLE);
            }
        } catch (InterruptedException e) {
            throw new IOException(e.getMessage());
        }
    }
}
