package com.google.android.gms.maps;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.cl;
import com.google.android.gms.internal.cw;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.RuntimeRemoteException;

public final class MapsInitializer {
    private MapsInitializer() {
    }

    public static void initialize(Context context) throws GooglePlayServicesNotAvailableException {
        C0142x.m495d(context);
        cl g = cw.m314g(context);
        try {
            CameraUpdateFactory.m503a(g.aR());
            BitmapDescriptorFactory.m505a(g.aS());
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
