package com.google.android.gms.maps;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.google.android.gms.maps.model.CameraPosition;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import net.hockeyapp.android.Strings;

public class GoogleMapOptionsCreator implements Creator<GoogleMapOptions> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m504a(GoogleMapOptions googleMapOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, googleMapOptions.m978u());
        ad.m88a(parcel, 2, googleMapOptions.aG());
        ad.m88a(parcel, 3, googleMapOptions.aH());
        ad.m106c(parcel, 4, googleMapOptions.getMapType());
        ad.m95a(parcel, 5, googleMapOptions.getCamera(), i, false);
        ad.m88a(parcel, 6, googleMapOptions.aI());
        ad.m88a(parcel, 7, googleMapOptions.aJ());
        ad.m88a(parcel, 8, googleMapOptions.aK());
        ad.m88a(parcel, 9, googleMapOptions.aL());
        ad.m88a(parcel, 10, googleMapOptions.aM());
        ad.m88a(parcel, 11, googleMapOptions.aN());
        ad.m87C(parcel, d);
    }

    public GoogleMapOptions createFromParcel(Parcel parcel) {
        byte b = (byte) 0;
        int c = ac.m58c(parcel);
        CameraPosition cameraPosition = null;
        byte b2 = (byte) 0;
        byte b3 = (byte) 0;
        byte b4 = (byte) 0;
        byte b5 = (byte) 0;
        byte b6 = (byte) 0;
        int i = 0;
        byte b7 = (byte) 0;
        byte b8 = (byte) 0;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b9 = ac.m55b(parcel);
            switch (ac.m68j(b9)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i2 = ac.m63f(parcel, b9);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    b8 = ac.m61d(parcel, b9);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    b7 = ac.m61d(parcel, b9);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    i = ac.m63f(parcel, b9);
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    cameraPosition = (CameraPosition) ac.m52a(parcel, b9, CameraPosition.CREATOR);
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    b6 = ac.m61d(parcel, b9);
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    b5 = ac.m61d(parcel, b9);
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    b4 = ac.m61d(parcel, b9);
                    break;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    b3 = ac.m61d(parcel, b9);
                    break;
                case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                    b2 = ac.m61d(parcel, b9);
                    break;
                case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                    b = ac.m61d(parcel, b9);
                    break;
                default:
                    ac.m56b(parcel, b9);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new GoogleMapOptions(i2, b8, b7, i, cameraPosition, b6, b5, b4, b3, b2, b);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public GoogleMapOptions[] newArray(int size) {
        return new GoogleMapOptions[size];
    }
}
