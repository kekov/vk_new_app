package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.bc.C1151a;
import com.google.android.gms.internal.cx;
import com.google.android.gms.internal.da;

public final class GroundOverlayOptions implements ae {
    public static final GroundOverlayOptionsCreator CREATOR;
    public static final float NO_DIMENSION = -1.0f;
    private final int f140T;
    private float fN;
    private float fU;
    private boolean fV;
    private BitmapDescriptor fX;
    private LatLng fY;
    private float fZ;
    private float ga;
    private LatLngBounds gb;
    private float gc;
    private float gd;
    private float ge;

    static {
        CREATOR = new GroundOverlayOptionsCreator();
    }

    public GroundOverlayOptions() {
        this.fV = true;
        this.gc = 0.0f;
        this.gd = 0.5f;
        this.ge = 0.5f;
        this.f140T = 1;
    }

    GroundOverlayOptions(int versionCode, IBinder wrappedImage, LatLng location, float width, float height, LatLngBounds bounds, float bearing, float zIndex, boolean visible, float transparency, float anchorU, float anchorV) {
        this.fV = true;
        this.gc = 0.0f;
        this.gd = 0.5f;
        this.ge = 0.5f;
        this.f140T = versionCode;
        this.fX = new BitmapDescriptor(C1151a.m631j(wrappedImage));
        this.fY = location;
        this.fZ = width;
        this.ga = height;
        this.gb = bounds;
        this.fN = bearing;
        this.fU = zIndex;
        this.fV = visible;
        this.gc = transparency;
        this.gd = anchorU;
        this.ge = anchorV;
    }

    private GroundOverlayOptions m993a(LatLng latLng, float f, float f2) {
        this.fY = latLng;
        this.fZ = f;
        this.ga = f2;
        return this;
    }

    public IBinder aX() {
        return this.fX.aD().asBinder();
    }

    public GroundOverlayOptions anchor(float u, float v) {
        this.gd = u;
        this.ge = v;
        return this;
    }

    public GroundOverlayOptions bearing(float bearing) {
        this.fN = ((bearing % 360.0f) + 360.0f) % 360.0f;
        return this;
    }

    public int describeContents() {
        return 0;
    }

    public float getAnchorU() {
        return this.gd;
    }

    public float getAnchorV() {
        return this.ge;
    }

    public float getBearing() {
        return this.fN;
    }

    public LatLngBounds getBounds() {
        return this.gb;
    }

    public float getHeight() {
        return this.ga;
    }

    public BitmapDescriptor getImage() {
        return this.fX;
    }

    public LatLng getLocation() {
        return this.fY;
    }

    public float getTransparency() {
        return this.gc;
    }

    public float getWidth() {
        return this.fZ;
    }

    public float getZIndex() {
        return this.fU;
    }

    public GroundOverlayOptions image(BitmapDescriptor image) {
        this.fX = image;
        return this;
    }

    public boolean isVisible() {
        return this.fV;
    }

    public GroundOverlayOptions position(LatLng location, float width) {
        boolean z = true;
        C0142x.m490a(this.gb == null, "Position has already been set using positionFromBounds");
        C0142x.m493b(location != null, (Object) "Location must be specified");
        if (width < 0.0f) {
            z = false;
        }
        C0142x.m493b(z, (Object) "Width must be non-negative");
        return m993a(location, width, NO_DIMENSION);
    }

    public GroundOverlayOptions position(LatLng location, float width, float height) {
        boolean z = true;
        C0142x.m490a(this.gb == null, "Position has already been set using positionFromBounds");
        C0142x.m493b(location != null, (Object) "Location must be specified");
        C0142x.m493b(width >= 0.0f, (Object) "Width must be non-negative");
        if (height < 0.0f) {
            z = false;
        }
        C0142x.m493b(z, (Object) "Height must be non-negative");
        return m993a(location, width, height);
    }

    public GroundOverlayOptions positionFromBounds(LatLngBounds bounds) {
        C0142x.m490a(this.fY == null, "Position has already been set using position: " + this.fY);
        this.gb = bounds;
        return this;
    }

    public GroundOverlayOptions transparency(float transparency) {
        boolean z = transparency >= 0.0f && transparency <= 1.0f;
        C0142x.m493b(z, (Object) "Transparency must be in the range [0..1]");
        this.gc = transparency;
        return this;
    }

    public int m994u() {
        return this.f140T;
    }

    public GroundOverlayOptions visible(boolean visible) {
        this.fV = visible;
        return this;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            da.m320a(this, out, flags);
        } else {
            GroundOverlayOptionsCreator.m508a(this, out, flags);
        }
    }

    public GroundOverlayOptions zIndex(float zIndex) {
        this.fU = zIndex;
        return this;
    }
}
