package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class VisibleRegionCreator implements Creator<VisibleRegion> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m517a(VisibleRegion visibleRegion, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, visibleRegion.m1011u());
        ad.m95a(parcel, 2, visibleRegion.nearLeft, i, false);
        ad.m95a(parcel, 3, visibleRegion.nearRight, i, false);
        ad.m95a(parcel, 4, visibleRegion.farLeft, i, false);
        ad.m95a(parcel, 5, visibleRegion.farRight, i, false);
        ad.m95a(parcel, 6, visibleRegion.latLngBounds, i, false);
        ad.m87C(parcel, d);
    }

    public VisibleRegion createFromParcel(Parcel parcel) {
        LatLngBounds latLngBounds = null;
        int c = ac.m58c(parcel);
        int i = 0;
        LatLng latLng = null;
        LatLng latLng2 = null;
        LatLng latLng3 = null;
        LatLng latLng4 = null;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    latLng4 = (LatLng) ac.m52a(parcel, b, LatLng.CREATOR);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    latLng3 = (LatLng) ac.m52a(parcel, b, LatLng.CREATOR);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    latLng2 = (LatLng) ac.m52a(parcel, b, LatLng.CREATOR);
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    latLng = (LatLng) ac.m52a(parcel, b, LatLng.CREATOR);
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    latLngBounds = (LatLngBounds) ac.m52a(parcel, b, LatLngBounds.CREATOR);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new VisibleRegion(i, latLng4, latLng3, latLng2, latLng, latLngBounds);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public VisibleRegion[] newArray(int size) {
        return new VisibleRegion[size];
    }
}
