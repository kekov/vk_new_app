package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import net.hockeyapp.android.Strings;

public class GroundOverlayOptionsCreator implements Creator<GroundOverlayOptions> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m508a(GroundOverlayOptions groundOverlayOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, groundOverlayOptions.m994u());
        ad.m93a(parcel, 2, groundOverlayOptions.aX(), false);
        ad.m95a(parcel, 3, groundOverlayOptions.getLocation(), i, false);
        ad.m90a(parcel, 4, groundOverlayOptions.getWidth());
        ad.m90a(parcel, 5, groundOverlayOptions.getHeight());
        ad.m95a(parcel, 6, groundOverlayOptions.getBounds(), i, false);
        ad.m90a(parcel, 7, groundOverlayOptions.getBearing());
        ad.m90a(parcel, 8, groundOverlayOptions.getZIndex());
        ad.m99a(parcel, 9, groundOverlayOptions.isVisible());
        ad.m90a(parcel, 10, groundOverlayOptions.getTransparency());
        ad.m90a(parcel, 11, groundOverlayOptions.getAnchorU());
        ad.m90a(parcel, 12, groundOverlayOptions.getAnchorV());
        ad.m87C(parcel, d);
    }

    public GroundOverlayOptions createFromParcel(Parcel parcel) {
        int c = ac.m58c(parcel);
        int i = 0;
        IBinder iBinder = null;
        LatLng latLng = null;
        float f = 0.0f;
        float f2 = 0.0f;
        LatLngBounds latLngBounds = null;
        float f3 = 0.0f;
        float f4 = 0.0f;
        boolean z = false;
        float f5 = 0.0f;
        float f6 = 0.0f;
        float f7 = 0.0f;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    iBinder = ac.m71m(parcel, b);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    latLng = (LatLng) ac.m52a(parcel, b, LatLng.CREATOR);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    f = ac.m66i(parcel, b);
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    f2 = ac.m66i(parcel, b);
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    latLngBounds = (LatLngBounds) ac.m52a(parcel, b, LatLngBounds.CREATOR);
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    f3 = ac.m66i(parcel, b);
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    f4 = ac.m66i(parcel, b);
                    break;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    z = ac.m60c(parcel, b);
                    break;
                case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                    f5 = ac.m66i(parcel, b);
                    break;
                case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                    f6 = ac.m66i(parcel, b);
                    break;
                case Strings.UPDATE_DIALOG_POSITIVE_BUTTON_ID /*12*/:
                    f7 = ac.m66i(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new GroundOverlayOptions(i, iBinder, latLng, f, f2, latLngBounds, f3, f4, z, f5, f6, f7);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public GroundOverlayOptions[] newArray(int size) {
        return new GroundOverlayOptions[size];
    }
}
