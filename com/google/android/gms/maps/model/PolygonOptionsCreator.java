package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.ArrayList;
import java.util.List;

public class PolygonOptionsCreator implements Creator<PolygonOptions> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m513a(PolygonOptions polygonOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, polygonOptions.m1004u());
        ad.m105b(parcel, 2, polygonOptions.getPoints(), false);
        ad.m107c(parcel, 3, polygonOptions.aZ(), false);
        ad.m90a(parcel, 4, polygonOptions.getStrokeWidth());
        ad.m106c(parcel, 5, polygonOptions.getStrokeColor());
        ad.m106c(parcel, 6, polygonOptions.getFillColor());
        ad.m90a(parcel, 7, polygonOptions.getZIndex());
        ad.m99a(parcel, 8, polygonOptions.isVisible());
        ad.m99a(parcel, 9, polygonOptions.isGeodesic());
        ad.m87C(parcel, d);
    }

    public PolygonOptions createFromParcel(Parcel parcel) {
        float f = 0.0f;
        boolean z = false;
        int c = ac.m58c(parcel);
        List list = null;
        List arrayList = new ArrayList();
        boolean z2 = false;
        int i = 0;
        int i2 = 0;
        float f2 = 0.0f;
        int i3 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i3 = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    list = ac.m59c(parcel, b, LatLng.CREATOR);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    ac.m54a(parcel, b, arrayList, getClass().getClassLoader());
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    f2 = ac.m66i(parcel, b);
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    i2 = ac.m63f(parcel, b);
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    i = ac.m63f(parcel, b);
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    f = ac.m66i(parcel, b);
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    z2 = ac.m60c(parcel, b);
                    break;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    z = ac.m60c(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new PolygonOptions(i3, list, arrayList, f2, i2, i, f, z2, z);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public PolygonOptions[] newArray(int size) {
        return new PolygonOptions[size];
    }
}
