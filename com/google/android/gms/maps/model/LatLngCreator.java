package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class LatLngCreator implements Creator<LatLng> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m511a(LatLng latLng, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, latLng.m995u());
        ad.m89a(parcel, 2, latLng.latitude);
        ad.m89a(parcel, 3, latLng.longitude);
        ad.m87C(parcel, d);
    }

    public LatLng createFromParcel(Parcel parcel) {
        double d = 0.0d;
        int c = ac.m58c(parcel);
        int i = 0;
        double d2 = 0.0d;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    d2 = ac.m67j(parcel, b);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    d = ac.m67j(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new LatLng(i, d2, d);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public LatLng[] newArray(int size) {
        return new LatLng[size];
    }
}
