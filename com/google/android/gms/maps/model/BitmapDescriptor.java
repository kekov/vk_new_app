package com.google.android.gms.maps.model;

import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.bc;

public final class BitmapDescriptor {
    private final bc eR;

    public BitmapDescriptor(bc remoteObject) {
        this.eR = (bc) C0142x.m495d(remoteObject);
    }

    public bc aD() {
        return this.eR;
    }
}
