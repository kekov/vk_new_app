package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.C0141w;
import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.cx;
import com.google.android.gms.internal.db;

public final class LatLngBounds implements ae {
    public static final LatLngBoundsCreator CREATOR;
    private final int f142T;
    public final LatLng northeast;
    public final LatLng southwest;

    public static final class Builder {
        private double gf;
        private double gg;
        private double gh;
        private double gi;

        public Builder() {
            this.gf = Double.POSITIVE_INFINITY;
            this.gg = Double.NEGATIVE_INFINITY;
            this.gh = Double.NaN;
            this.gi = Double.NaN;
        }

        private boolean m509b(double d) {
            boolean z = false;
            if (this.gh <= this.gi) {
                return this.gh <= d && d <= this.gi;
            } else {
                if (this.gh <= d || d <= this.gi) {
                    z = true;
                }
                return z;
            }
        }

        public LatLngBounds build() {
            C0142x.m490a(!Double.isNaN(this.gh), "no included points");
            return new LatLngBounds(new LatLng(this.gf, this.gh), new LatLng(this.gg, this.gi));
        }

        public Builder include(LatLng point) {
            this.gf = Math.min(this.gf, point.latitude);
            this.gg = Math.max(this.gg, point.latitude);
            double d = point.longitude;
            if (Double.isNaN(this.gh)) {
                this.gh = d;
                this.gi = d;
            } else if (!m509b(d)) {
                if (LatLngBounds.m997b(this.gh, d) < LatLngBounds.m999c(this.gi, d)) {
                    this.gh = d;
                } else {
                    this.gi = d;
                }
            }
            return this;
        }
    }

    static {
        CREATOR = new LatLngBoundsCreator();
    }

    LatLngBounds(int versionCode, LatLng southwest, LatLng northeast) {
        C0142x.m492b((Object) southwest, (Object) "null southwest");
        C0142x.m492b((Object) northeast, (Object) "null northeast");
        C0142x.m491a(northeast.latitude >= southwest.latitude, "southern latitude exceeds northern latitude (%s > %s)", Double.valueOf(southwest.latitude), Double.valueOf(northeast.latitude));
        this.f142T = versionCode;
        this.southwest = southwest;
        this.northeast = northeast;
    }

    public LatLngBounds(LatLng southwest, LatLng northeast) {
        this(1, southwest, northeast);
    }

    private boolean m996a(double d) {
        return this.southwest.latitude <= d && d <= this.northeast.latitude;
    }

    private static double m997b(double d, double d2) {
        return ((d - d2) + 360.0d) % 360.0d;
    }

    private boolean m998b(double d) {
        boolean z = false;
        if (this.southwest.longitude <= this.northeast.longitude) {
            return this.southwest.longitude <= d && d <= this.northeast.longitude;
        } else {
            if (this.southwest.longitude <= d || d <= this.northeast.longitude) {
                z = true;
            }
            return z;
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    private static double m999c(double d, double d2) {
        return ((d2 - d) + 360.0d) % 360.0d;
    }

    public boolean contains(LatLng point) {
        return m996a(point.latitude) && m998b(point.longitude);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LatLngBounds)) {
            return false;
        }
        LatLngBounds latLngBounds = (LatLngBounds) o;
        return this.southwest.equals(latLngBounds.southwest) && this.northeast.equals(latLngBounds.northeast);
    }

    public int hashCode() {
        return C0141w.hashCode(this.southwest, this.northeast);
    }

    public LatLngBounds including(LatLng point) {
        double min = Math.min(this.southwest.latitude, point.latitude);
        double max = Math.max(this.northeast.latitude, point.latitude);
        double d = this.northeast.longitude;
        double d2 = this.southwest.longitude;
        double d3 = point.longitude;
        if (m998b(d3)) {
            d3 = d2;
            d2 = d;
        } else if (m997b(d2, d3) < m999c(d, d3)) {
            d2 = d;
        } else {
            double d4 = d2;
            d2 = d3;
            d3 = d4;
        }
        return new LatLngBounds(new LatLng(min, d3), new LatLng(max, d2));
    }

    public String toString() {
        return C0141w.m488c(this).m486a("southwest", this.southwest).m486a("northeast", this.northeast).toString();
    }

    public int m1002u() {
        return this.f142T;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            db.m321a(this, out, flags);
        } else {
            LatLngBoundsCreator.m510a(this, out, flags);
        }
    }
}
