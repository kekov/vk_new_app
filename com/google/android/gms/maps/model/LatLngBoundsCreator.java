package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class LatLngBoundsCreator implements Creator<LatLngBounds> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m510a(LatLngBounds latLngBounds, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, latLngBounds.m1002u());
        ad.m95a(parcel, 2, latLngBounds.southwest, i, false);
        ad.m95a(parcel, 3, latLngBounds.northeast, i, false);
        ad.m87C(parcel, d);
    }

    public LatLngBounds createFromParcel(Parcel parcel) {
        LatLng latLng = null;
        int c = ac.m58c(parcel);
        int i = 0;
        LatLng latLng2 = null;
        while (parcel.dataPosition() < c) {
            int f;
            LatLng latLng3;
            int b = ac.m55b(parcel);
            LatLng latLng4;
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    latLng4 = latLng;
                    latLng = latLng2;
                    f = ac.m63f(parcel, b);
                    latLng3 = latLng4;
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    f = i;
                    latLng4 = (LatLng) ac.m52a(parcel, b, LatLng.CREATOR);
                    latLng3 = latLng;
                    latLng = latLng4;
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    latLng3 = (LatLng) ac.m52a(parcel, b, LatLng.CREATOR);
                    latLng = latLng2;
                    f = i;
                    break;
                default:
                    ac.m56b(parcel, b);
                    latLng3 = latLng;
                    latLng = latLng2;
                    f = i;
                    break;
            }
            i = f;
            latLng2 = latLng;
            latLng = latLng3;
        }
        if (parcel.dataPosition() == c) {
            return new LatLngBounds(i, latLng2, latLng);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public LatLngBounds[] newArray(int size) {
        return new LatLngBounds[size];
    }
}
