package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class TileCreator implements Creator<Tile> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m515a(Tile tile, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, tile.m1006u());
        ad.m106c(parcel, 2, tile.width);
        ad.m106c(parcel, 3, tile.height);
        ad.m100a(parcel, 4, tile.data, false);
        ad.m87C(parcel, d);
    }

    public Tile createFromParcel(Parcel parcel) {
        int i = 0;
        int c = ac.m58c(parcel);
        byte[] bArr = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i3 = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    i2 = ac.m63f(parcel, b);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    i = ac.m63f(parcel, b);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    bArr = ac.m73o(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new Tile(i3, i2, i, bArr);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public Tile[] newArray(int size) {
        return new Tile[size];
    }
}
