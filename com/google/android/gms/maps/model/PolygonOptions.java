package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.cx;
import com.google.android.gms.internal.de;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class PolygonOptions implements ae {
    public static final PolygonOptionsCreator CREATOR;
    private final int f144T;
    private float fR;
    private int fS;
    private int fT;
    private float fU;
    private boolean fV;
    private final List<LatLng> gq;
    private final List<List<LatLng>> gr;
    private boolean gs;

    static {
        CREATOR = new PolygonOptionsCreator();
    }

    public PolygonOptions() {
        this.fR = 10.0f;
        this.fS = Color.WINDOW_BACKGROUND_COLOR;
        this.fT = 0;
        this.fU = 0.0f;
        this.fV = true;
        this.gs = false;
        this.f144T = 1;
        this.gq = new ArrayList();
        this.gr = new ArrayList();
    }

    PolygonOptions(int versionCode, List<LatLng> points, List holes, float strokeWidth, int strokeColor, int fillColor, float zIndex, boolean visible, boolean geodesic) {
        this.fR = 10.0f;
        this.fS = Color.WINDOW_BACKGROUND_COLOR;
        this.fT = 0;
        this.fU = 0.0f;
        this.fV = true;
        this.gs = false;
        this.f144T = versionCode;
        this.gq = points;
        this.gr = holes;
        this.fR = strokeWidth;
        this.fS = strokeColor;
        this.fT = fillColor;
        this.fU = zIndex;
        this.fV = visible;
        this.gs = geodesic;
    }

    public List aZ() {
        return this.gr;
    }

    public PolygonOptions add(LatLng point) {
        this.gq.add(point);
        return this;
    }

    public PolygonOptions add(LatLng... points) {
        this.gq.addAll(Arrays.asList(points));
        return this;
    }

    public PolygonOptions addAll(Iterable<LatLng> points) {
        for (LatLng add : points) {
            this.gq.add(add);
        }
        return this;
    }

    public PolygonOptions addHole(Iterable<LatLng> points) {
        ArrayList arrayList = new ArrayList();
        for (LatLng add : points) {
            arrayList.add(add);
        }
        this.gr.add(arrayList);
        return this;
    }

    public int describeContents() {
        return 0;
    }

    public PolygonOptions fillColor(int color) {
        this.fT = color;
        return this;
    }

    public PolygonOptions geodesic(boolean geodesic) {
        this.gs = geodesic;
        return this;
    }

    public int getFillColor() {
        return this.fT;
    }

    public List<List<LatLng>> getHoles() {
        return this.gr;
    }

    public List<LatLng> getPoints() {
        return this.gq;
    }

    public int getStrokeColor() {
        return this.fS;
    }

    public float getStrokeWidth() {
        return this.fR;
    }

    public float getZIndex() {
        return this.fU;
    }

    public boolean isGeodesic() {
        return this.gs;
    }

    public boolean isVisible() {
        return this.fV;
    }

    public PolygonOptions strokeColor(int color) {
        this.fS = color;
        return this;
    }

    public PolygonOptions strokeWidth(float width) {
        this.fR = width;
        return this;
    }

    public int m1004u() {
        return this.f144T;
    }

    public PolygonOptions visible(boolean visible) {
        this.fV = visible;
        return this;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            de.m324a(this, out, flags);
        } else {
            PolygonOptionsCreator.m513a(this, out, flags);
        }
    }

    public PolygonOptions zIndex(float zIndex) {
        this.fU = zIndex;
        return this;
    }
}
