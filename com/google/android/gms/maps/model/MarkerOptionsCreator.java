package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class MarkerOptionsCreator implements Creator<MarkerOptions> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m512a(MarkerOptions markerOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, markerOptions.m1003u());
        ad.m95a(parcel, 2, markerOptions.getPosition(), i, false);
        ad.m96a(parcel, 3, markerOptions.getTitle(), false);
        ad.m96a(parcel, 4, markerOptions.getSnippet(), false);
        ad.m93a(parcel, 5, markerOptions.aY(), false);
        ad.m90a(parcel, 6, markerOptions.getAnchorU());
        ad.m90a(parcel, 7, markerOptions.getAnchorV());
        ad.m99a(parcel, 8, markerOptions.isDraggable());
        ad.m99a(parcel, 9, markerOptions.isVisible());
        ad.m87C(parcel, d);
    }

    public MarkerOptions createFromParcel(Parcel parcel) {
        float f = 0.0f;
        boolean z = false;
        IBinder iBinder = null;
        int c = ac.m58c(parcel);
        boolean z2 = false;
        float f2 = 0.0f;
        String str = null;
        String str2 = null;
        LatLng latLng = null;
        int i = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    latLng = (LatLng) ac.m52a(parcel, b, LatLng.CREATOR);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    str2 = ac.m70l(parcel, b);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    str = ac.m70l(parcel, b);
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    iBinder = ac.m71m(parcel, b);
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    f2 = ac.m66i(parcel, b);
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    f = ac.m66i(parcel, b);
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    z2 = ac.m60c(parcel, b);
                    break;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    z = ac.m60c(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new MarkerOptions(i, latLng, str2, str, iBinder, f2, f, z2, z);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public MarkerOptions[] newArray(int size) {
        return new MarkerOptions[size];
    }
}
