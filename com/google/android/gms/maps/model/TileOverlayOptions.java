package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.cx;
import com.google.android.gms.internal.dh;
import com.google.android.gms.internal.dp;
import com.google.android.gms.internal.dp.C1206a;

public final class TileOverlayOptions implements ae {
    public static final TileOverlayOptionsCreator CREATOR;
    private final int f148T;
    private float fU;
    private boolean fV;
    private dp gv;
    private TileProvider gw;

    /* renamed from: com.google.android.gms.maps.model.TileOverlayOptions.1 */
    class C12621 implements TileProvider {
        private final dp gx;
        final /* synthetic */ TileOverlayOptions gy;

        C12621(TileOverlayOptions tileOverlayOptions) {
            this.gy = tileOverlayOptions;
            this.gx = this.gy.gv;
        }

        public Tile getTile(int x, int y, int zoom) {
            try {
                return this.gx.getTile(x, y, zoom);
            } catch (RemoteException e) {
                return null;
            }
        }
    }

    /* renamed from: com.google.android.gms.maps.model.TileOverlayOptions.2 */
    class C16172 extends C1206a {
        final /* synthetic */ TileOverlayOptions gy;
        final /* synthetic */ TileProvider gz;

        C16172(TileOverlayOptions tileOverlayOptions, TileProvider tileProvider) {
            this.gy = tileOverlayOptions;
            this.gz = tileProvider;
        }

        public Tile getTile(int x, int y, int zoom) {
            return this.gz.getTile(x, y, zoom);
        }
    }

    static {
        CREATOR = new TileOverlayOptionsCreator();
    }

    public TileOverlayOptions() {
        this.fV = true;
        this.f148T = 1;
    }

    TileOverlayOptions(int versionCode, IBinder delegate, boolean visible, float zIndex) {
        this.fV = true;
        this.f148T = versionCode;
        this.gv = C1206a.m816Q(delegate);
        this.gw = this.gv == null ? null : new C12621(this);
        this.fV = visible;
        this.fU = zIndex;
    }

    public IBinder ba() {
        return this.gv.asBinder();
    }

    public int describeContents() {
        return 0;
    }

    public TileProvider getTileProvider() {
        return this.gw;
    }

    public float getZIndex() {
        return this.fU;
    }

    public boolean isVisible() {
        return this.fV;
    }

    public TileOverlayOptions tileProvider(TileProvider tileProvider) {
        this.gw = tileProvider;
        this.gv = this.gw == null ? null : new C16172(this, tileProvider);
        return this;
    }

    public int m1008u() {
        return this.f148T;
    }

    public TileOverlayOptions visible(boolean visible) {
        this.fV = visible;
        return this;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            dh.m327a(this, out, flags);
        } else {
            TileOverlayOptionsCreator.m516a(this, out, flags);
        }
    }

    public TileOverlayOptions zIndex(float zIndex) {
        this.fU = zIndex;
        return this;
    }
}
