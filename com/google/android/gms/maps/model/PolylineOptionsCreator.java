package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.List;

public class PolylineOptionsCreator implements Creator<PolylineOptions> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m514a(PolylineOptions polylineOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, polylineOptions.m1005u());
        ad.m105b(parcel, 2, polylineOptions.getPoints(), false);
        ad.m90a(parcel, 3, polylineOptions.getWidth());
        ad.m106c(parcel, 4, polylineOptions.getColor());
        ad.m90a(parcel, 5, polylineOptions.getZIndex());
        ad.m99a(parcel, 6, polylineOptions.isVisible());
        ad.m99a(parcel, 7, polylineOptions.isGeodesic());
        ad.m87C(parcel, d);
    }

    public PolylineOptions createFromParcel(Parcel parcel) {
        float f = 0.0f;
        boolean z = false;
        int c = ac.m58c(parcel);
        List list = null;
        boolean z2 = false;
        int i = 0;
        float f2 = 0.0f;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i2 = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    list = ac.m59c(parcel, b, LatLng.CREATOR);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    f2 = ac.m66i(parcel, b);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    i = ac.m63f(parcel, b);
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    f = ac.m66i(parcel, b);
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    z2 = ac.m60c(parcel, b);
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    z = ac.m60c(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new PolylineOptions(i2, list, f2, i, f, z2, z);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public PolylineOptions[] newArray(int size) {
        return new PolylineOptions[size];
    }
}
