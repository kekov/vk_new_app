package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.C0141w;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.cx;
import com.google.android.gms.internal.di;

public final class VisibleRegion implements ae {
    public static final VisibleRegionCreator CREATOR;
    private final int f149T;
    public final LatLng farLeft;
    public final LatLng farRight;
    public final LatLngBounds latLngBounds;
    public final LatLng nearLeft;
    public final LatLng nearRight;

    static {
        CREATOR = new VisibleRegionCreator();
    }

    VisibleRegion(int versionCode, LatLng nearLeft, LatLng nearRight, LatLng farLeft, LatLng farRight, LatLngBounds latLngBounds) {
        this.f149T = versionCode;
        this.nearLeft = nearLeft;
        this.nearRight = nearRight;
        this.farLeft = farLeft;
        this.farRight = farRight;
        this.latLngBounds = latLngBounds;
    }

    public VisibleRegion(LatLng nearLeft, LatLng nearRight, LatLng farLeft, LatLng farRight, LatLngBounds latLngBounds) {
        this(1, nearLeft, nearRight, farLeft, farRight, latLngBounds);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof VisibleRegion)) {
            return false;
        }
        VisibleRegion visibleRegion = (VisibleRegion) o;
        return this.nearLeft.equals(visibleRegion.nearLeft) && this.nearRight.equals(visibleRegion.nearRight) && this.farLeft.equals(visibleRegion.farLeft) && this.farRight.equals(visibleRegion.farRight) && this.latLngBounds.equals(visibleRegion.latLngBounds);
    }

    public int hashCode() {
        return C0141w.hashCode(this.nearLeft, this.nearRight, this.farLeft, this.farRight, this.latLngBounds);
    }

    public String toString() {
        return C0141w.m488c(this).m486a("nearLeft", this.nearLeft).m486a("nearRight", this.nearRight).m486a("farLeft", this.farLeft).m486a("farRight", this.farRight).m486a("latLngBounds", this.latLngBounds).toString();
    }

    public int m1011u() {
        return this.f149T;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            di.m328a(this, out, flags);
        } else {
            VisibleRegionCreator.m517a(this, out, flags);
        }
    }
}
