package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.cx;
import com.google.android.gms.internal.cz;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;

public final class CircleOptions implements ae {
    public static final CircleOptionsCreator CREATOR;
    private final int f139T;
    private LatLng fP;
    private double fQ;
    private float fR;
    private int fS;
    private int fT;
    private float fU;
    private boolean fV;

    static {
        CREATOR = new CircleOptionsCreator();
    }

    public CircleOptions() {
        this.fP = null;
        this.fQ = 0.0d;
        this.fR = 10.0f;
        this.fS = Color.WINDOW_BACKGROUND_COLOR;
        this.fT = 0;
        this.fU = 0.0f;
        this.fV = true;
        this.f139T = 1;
    }

    CircleOptions(int versionCode, LatLng center, double radius, float strokeWidth, int strokeColor, int fillColor, float zIndex, boolean visible) {
        this.fP = null;
        this.fQ = 0.0d;
        this.fR = 10.0f;
        this.fS = Color.WINDOW_BACKGROUND_COLOR;
        this.fT = 0;
        this.fU = 0.0f;
        this.fV = true;
        this.f139T = versionCode;
        this.fP = center;
        this.fQ = radius;
        this.fR = strokeWidth;
        this.fS = strokeColor;
        this.fT = fillColor;
        this.fU = zIndex;
        this.fV = visible;
    }

    public CircleOptions center(LatLng center) {
        this.fP = center;
        return this;
    }

    public int describeContents() {
        return 0;
    }

    public CircleOptions fillColor(int color) {
        this.fT = color;
        return this;
    }

    public LatLng getCenter() {
        return this.fP;
    }

    public int getFillColor() {
        return this.fT;
    }

    public double getRadius() {
        return this.fQ;
    }

    public int getStrokeColor() {
        return this.fS;
    }

    public float getStrokeWidth() {
        return this.fR;
    }

    public float getZIndex() {
        return this.fU;
    }

    public boolean isVisible() {
        return this.fV;
    }

    public CircleOptions radius(double radius) {
        this.fQ = radius;
        return this;
    }

    public CircleOptions strokeColor(int color) {
        this.fS = color;
        return this;
    }

    public CircleOptions strokeWidth(float width) {
        this.fR = width;
        return this;
    }

    public int m992u() {
        return this.f139T;
    }

    public CircleOptions visible(boolean visible) {
        this.fV = visible;
        return this;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            cz.m319a(this, out, flags);
        } else {
            CircleOptionsCreator.m507a(this, out, flags);
        }
    }

    public CircleOptions zIndex(float zIndex) {
        this.fU = zIndex;
        return this;
    }
}
