package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class CircleOptionsCreator implements Creator<CircleOptions> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m507a(CircleOptions circleOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, circleOptions.m992u());
        ad.m95a(parcel, 2, circleOptions.getCenter(), i, false);
        ad.m89a(parcel, 3, circleOptions.getRadius());
        ad.m90a(parcel, 4, circleOptions.getStrokeWidth());
        ad.m106c(parcel, 5, circleOptions.getStrokeColor());
        ad.m106c(parcel, 6, circleOptions.getFillColor());
        ad.m90a(parcel, 7, circleOptions.getZIndex());
        ad.m99a(parcel, 8, circleOptions.isVisible());
        ad.m87C(parcel, d);
    }

    public CircleOptions createFromParcel(Parcel parcel) {
        float f = 0.0f;
        boolean z = false;
        int c = ac.m58c(parcel);
        LatLng latLng = null;
        double d = 0.0d;
        int i = 0;
        int i2 = 0;
        float f2 = 0.0f;
        int i3 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i3 = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    latLng = (LatLng) ac.m52a(parcel, b, LatLng.CREATOR);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    d = ac.m67j(parcel, b);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    f2 = ac.m66i(parcel, b);
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    i2 = ac.m63f(parcel, b);
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    i = ac.m63f(parcel, b);
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    f = ac.m66i(parcel, b);
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    z = ac.m60c(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new CircleOptions(i3, latLng, d, f2, i2, i, f, z);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public CircleOptions[] newArray(int size) {
        return new CircleOptions[size];
    }
}
