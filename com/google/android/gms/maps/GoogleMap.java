package com.google.android.gms.maps;

import android.location.Location;
import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.internal.C0115do;
import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.bc;
import com.google.android.gms.internal.bd;
import com.google.android.gms.internal.ck.C1172a;
import com.google.android.gms.internal.cm.C1176a;
import com.google.android.gms.internal.cn.C1178a;
import com.google.android.gms.internal.co.C1180a;
import com.google.android.gms.internal.cp;
import com.google.android.gms.internal.cq.C1184a;
import com.google.android.gms.internal.cr.C1186a;
import com.google.android.gms.internal.cs.C1188a;
import com.google.android.gms.internal.ct.C1190a;
import com.google.android.gms.internal.cu.C1192a;
import com.google.android.gms.internal.dl;
import com.google.android.gms.internal.dm;
import com.google.android.gms.maps.LocationSource.OnLocationChangedListener;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.internal.ILocationSourceDelegate.C1253a;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RuntimeRemoteException;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;

public final class GoogleMap {
    public static final int MAP_TYPE_HYBRID = 4;
    public static final int MAP_TYPE_NONE = 0;
    public static final int MAP_TYPE_NORMAL = 1;
    public static final int MAP_TYPE_SATELLITE = 2;
    public static final int MAP_TYPE_TERRAIN = 3;
    private final IGoogleMapDelegate eT;
    private UiSettings eU;

    public interface CancelableCallback {
        void onCancel();

        void onFinish();
    }

    public interface InfoWindowAdapter {
        View getInfoContents(Marker marker);

        View getInfoWindow(Marker marker);
    }

    public interface OnCameraChangeListener {
        void onCameraChange(CameraPosition cameraPosition);
    }

    public interface OnInfoWindowClickListener {
        void onInfoWindowClick(Marker marker);
    }

    public interface OnMapClickListener {
        void onMapClick(LatLng latLng);
    }

    public interface OnMapLongClickListener {
        void onMapLongClick(LatLng latLng);
    }

    public interface OnMarkerClickListener {
        boolean onMarkerClick(Marker marker);
    }

    public interface OnMarkerDragListener {
        void onMarkerDrag(Marker marker);

        void onMarkerDragEnd(Marker marker);

        void onMarkerDragStart(Marker marker);
    }

    public interface OnMyLocationChangeListener {
        void onMyLocationChange(Location location);
    }

    /* renamed from: com.google.android.gms.maps.GoogleMap.1 */
    class C16071 extends C1253a {
        final /* synthetic */ LocationSource eV;
        final /* synthetic */ GoogleMap eW;

        /* renamed from: com.google.android.gms.maps.GoogleMap.1.1 */
        class C12411 implements OnLocationChangedListener {
            final /* synthetic */ cp eX;
            final /* synthetic */ C16071 eY;

            C12411(C16071 c16071, cp cpVar) {
                this.eY = c16071;
                this.eX = cpVar;
            }

            public void onLocationChanged(Location location) {
                try {
                    this.eX.m305e(bd.m1049f(location));
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                }
            }
        }

        C16071(GoogleMap googleMap, LocationSource locationSource) {
            this.eW = googleMap;
            this.eV = locationSource;
        }

        public void activate(cp listener) {
            this.eV.activate(new C12411(this, listener));
        }

        public void deactivate() {
            this.eV.deactivate();
        }
    }

    /* renamed from: com.google.android.gms.maps.GoogleMap.2 */
    class C16082 extends C1178a {
        final /* synthetic */ GoogleMap eW;
        final /* synthetic */ OnCameraChangeListener eZ;

        C16082(GoogleMap googleMap, OnCameraChangeListener onCameraChangeListener) {
            this.eW = googleMap;
            this.eZ = onCameraChangeListener;
        }

        public void onCameraChange(CameraPosition position) {
            this.eZ.onCameraChange(position);
        }
    }

    /* renamed from: com.google.android.gms.maps.GoogleMap.3 */
    class C16093 extends C1184a {
        final /* synthetic */ GoogleMap eW;
        final /* synthetic */ OnMapClickListener fa;

        C16093(GoogleMap googleMap, OnMapClickListener onMapClickListener) {
            this.eW = googleMap;
            this.fa = onMapClickListener;
        }

        public void onMapClick(LatLng point) {
            this.fa.onMapClick(point);
        }
    }

    /* renamed from: com.google.android.gms.maps.GoogleMap.4 */
    class C16104 extends C1186a {
        final /* synthetic */ GoogleMap eW;
        final /* synthetic */ OnMapLongClickListener fb;

        C16104(GoogleMap googleMap, OnMapLongClickListener onMapLongClickListener) {
            this.eW = googleMap;
            this.fb = onMapLongClickListener;
        }

        public void onMapLongClick(LatLng point) {
            this.fb.onMapLongClick(point);
        }
    }

    /* renamed from: com.google.android.gms.maps.GoogleMap.5 */
    class C16115 extends C1188a {
        final /* synthetic */ GoogleMap eW;
        final /* synthetic */ OnMarkerClickListener fc;

        C16115(GoogleMap googleMap, OnMarkerClickListener onMarkerClickListener) {
            this.eW = googleMap;
            this.fc = onMarkerClickListener;
        }

        public boolean m1265a(dm dmVar) {
            return this.fc.onMarkerClick(new Marker(dmVar));
        }
    }

    /* renamed from: com.google.android.gms.maps.GoogleMap.6 */
    class C16126 extends C1190a {
        final /* synthetic */ GoogleMap eW;
        final /* synthetic */ OnMarkerDragListener fd;

        C16126(GoogleMap googleMap, OnMarkerDragListener onMarkerDragListener) {
            this.eW = googleMap;
            this.fd = onMarkerDragListener;
        }

        public void m1266b(dm dmVar) {
            this.fd.onMarkerDragStart(new Marker(dmVar));
        }

        public void m1267c(dm dmVar) {
            this.fd.onMarkerDragEnd(new Marker(dmVar));
        }

        public void m1268d(dm dmVar) {
            this.fd.onMarkerDrag(new Marker(dmVar));
        }
    }

    /* renamed from: com.google.android.gms.maps.GoogleMap.7 */
    class C16137 extends C1180a {
        final /* synthetic */ GoogleMap eW;
        final /* synthetic */ OnInfoWindowClickListener fe;

        C16137(GoogleMap googleMap, OnInfoWindowClickListener onInfoWindowClickListener) {
            this.eW = googleMap;
            this.fe = onInfoWindowClickListener;
        }

        public void m1269e(dm dmVar) {
            this.fe.onInfoWindowClick(new Marker(dmVar));
        }
    }

    /* renamed from: com.google.android.gms.maps.GoogleMap.8 */
    class C16148 extends C1176a {
        final /* synthetic */ GoogleMap eW;
        final /* synthetic */ InfoWindowAdapter ff;

        C16148(GoogleMap googleMap, InfoWindowAdapter infoWindowAdapter) {
            this.eW = googleMap;
            this.ff = infoWindowAdapter;
        }

        public bc m1270f(dm dmVar) {
            return bd.m1049f(this.ff.getInfoWindow(new Marker(dmVar)));
        }

        public bc m1271g(dm dmVar) {
            return bd.m1049f(this.ff.getInfoContents(new Marker(dmVar)));
        }
    }

    /* renamed from: com.google.android.gms.maps.GoogleMap.9 */
    class C16159 extends C1192a {
        final /* synthetic */ GoogleMap eW;
        final /* synthetic */ OnMyLocationChangeListener fg;

        C16159(GoogleMap googleMap, OnMyLocationChangeListener onMyLocationChangeListener) {
            this.eW = googleMap;
            this.fg = onMyLocationChangeListener;
        }

        public void m1272b(bc bcVar) {
            this.fg.onMyLocationChange((Location) bd.m1048a(bcVar));
        }
    }

    /* renamed from: com.google.android.gms.maps.GoogleMap.a */
    static final class C1616a extends C1172a {
        private final CancelableCallback fh;

        C1616a(CancelableCallback cancelableCallback) {
            this.fh = cancelableCallback;
        }

        public void onCancel() {
            this.fh.onCancel();
        }

        public void onFinish() {
            this.fh.onFinish();
        }
    }

    protected GoogleMap(IGoogleMapDelegate map) {
        this.eT = (IGoogleMapDelegate) C0142x.m495d(map);
    }

    IGoogleMapDelegate aF() {
        return this.eT;
    }

    public final Circle addCircle(CircleOptions options) {
        try {
            return new Circle(this.eT.addCircle(options));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final GroundOverlay addGroundOverlay(GroundOverlayOptions options) {
        try {
            dl addGroundOverlay = this.eT.addGroundOverlay(options);
            return addGroundOverlay != null ? new GroundOverlay(addGroundOverlay) : null;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final Marker addMarker(MarkerOptions options) {
        try {
            dm addMarker = this.eT.addMarker(options);
            return addMarker != null ? new Marker(addMarker) : null;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final Polygon addPolygon(PolygonOptions options) {
        try {
            return new Polygon(this.eT.addPolygon(options));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final Polyline addPolyline(PolylineOptions options) {
        try {
            return new Polyline(this.eT.addPolyline(options));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final TileOverlay addTileOverlay(TileOverlayOptions options) {
        try {
            C0115do addTileOverlay = this.eT.addTileOverlay(options);
            return addTileOverlay != null ? new TileOverlay(addTileOverlay) : null;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final void animateCamera(CameraUpdate update) {
        try {
            this.eT.animateCamera(update.aD());
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final void animateCamera(CameraUpdate update, int durationMs, CancelableCallback callback) {
        try {
            this.eT.animateCameraWithDurationAndCallback(update.aD(), durationMs, callback == null ? null : new C1616a(callback));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final void animateCamera(CameraUpdate update, CancelableCallback callback) {
        try {
            this.eT.animateCameraWithCallback(update.aD(), callback == null ? null : new C1616a(callback));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final void clear() {
        try {
            this.eT.clear();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final CameraPosition getCameraPosition() {
        try {
            return this.eT.getCameraPosition();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final int getMapType() {
        try {
            return this.eT.getMapType();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final float getMaxZoomLevel() {
        try {
            return this.eT.getMaxZoomLevel();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final float getMinZoomLevel() {
        try {
            return this.eT.getMinZoomLevel();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final Location getMyLocation() {
        try {
            return this.eT.getMyLocation();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final Projection getProjection() {
        try {
            return new Projection(this.eT.getProjection());
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final UiSettings getUiSettings() {
        try {
            if (this.eU == null) {
                this.eU = new UiSettings(this.eT.getUiSettings());
            }
            return this.eU;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final boolean isIndoorEnabled() {
        try {
            return this.eT.isIndoorEnabled();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final boolean isMyLocationEnabled() {
        try {
            return this.eT.isMyLocationEnabled();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final boolean isTrafficEnabled() {
        try {
            return this.eT.isTrafficEnabled();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final void moveCamera(CameraUpdate update) {
        try {
            this.eT.moveCamera(update.aD());
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final boolean setIndoorEnabled(boolean enabled) {
        try {
            return this.eT.setIndoorEnabled(enabled);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final void setInfoWindowAdapter(InfoWindowAdapter adapter) {
        if (adapter == null) {
            try {
                this.eT.setInfoWindowAdapter(null);
                return;
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
        this.eT.setInfoWindowAdapter(new C16148(this, adapter));
    }

    public final void setLocationSource(LocationSource source) {
        if (source == null) {
            try {
                this.eT.setLocationSource(null);
                return;
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
        this.eT.setLocationSource(new C16071(this, source));
    }

    public final void setMapType(int type) {
        try {
            this.eT.setMapType(type);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final void setMyLocationEnabled(boolean enabled) {
        try {
            this.eT.setMyLocationEnabled(enabled);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final void setOnCameraChangeListener(OnCameraChangeListener listener) {
        if (listener == null) {
            try {
                this.eT.setOnCameraChangeListener(null);
                return;
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
        this.eT.setOnCameraChangeListener(new C16082(this, listener));
    }

    public final void setOnInfoWindowClickListener(OnInfoWindowClickListener listener) {
        if (listener == null) {
            try {
                this.eT.setOnInfoWindowClickListener(null);
                return;
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
        this.eT.setOnInfoWindowClickListener(new C16137(this, listener));
    }

    public final void setOnMapClickListener(OnMapClickListener listener) {
        if (listener == null) {
            try {
                this.eT.setOnMapClickListener(null);
                return;
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
        this.eT.setOnMapClickListener(new C16093(this, listener));
    }

    public final void setOnMapLongClickListener(OnMapLongClickListener listener) {
        if (listener == null) {
            try {
                this.eT.setOnMapLongClickListener(null);
                return;
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
        this.eT.setOnMapLongClickListener(new C16104(this, listener));
    }

    public final void setOnMarkerClickListener(OnMarkerClickListener listener) {
        if (listener == null) {
            try {
                this.eT.setOnMarkerClickListener(null);
                return;
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
        this.eT.setOnMarkerClickListener(new C16115(this, listener));
    }

    public final void setOnMarkerDragListener(OnMarkerDragListener listener) {
        if (listener == null) {
            try {
                this.eT.setOnMarkerDragListener(null);
                return;
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
        this.eT.setOnMarkerDragListener(new C16126(this, listener));
    }

    public final void setOnMyLocationChangeListener(OnMyLocationChangeListener listener) {
        if (listener == null) {
            try {
                this.eT.setOnMyLocationChangeListener(null);
                return;
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
        this.eT.setOnMyLocationChangeListener(new C16159(this, listener));
    }

    public final void setTrafficEnabled(boolean enabled) {
        try {
            this.eT.setTrafficEnabled(enabled);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public final void stopAnimation() {
        try {
            this.eT.stopAnimation();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
