package com.google.android.gms.maps;

import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.bc;

public final class CameraUpdate {
    private final bc eR;

    CameraUpdate(bc remoteObject) {
        this.eR = (bc) C0142x.m495d(remoteObject);
    }

    public bc aD() {
        return this.eR;
    }
}
