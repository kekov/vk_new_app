package com.google.android.gms.maps;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.dynamic.LifecycleDelegate;
import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.bb;
import com.google.android.gms.internal.bd;
import com.google.android.gms.internal.be;
import com.google.android.gms.internal.cv;
import com.google.android.gms.internal.cw;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.internal.IMapFragmentDelegate;
import com.google.android.gms.maps.model.RuntimeRemoteException;

public class SupportMapFragment extends Fragment {
    private final C1247b fE;
    private GoogleMap ft;

    /* renamed from: com.google.android.gms.maps.SupportMapFragment.a */
    static class C1246a implements LifecycleDelegate {
        private final Fragment fF;
        private final IMapFragmentDelegate fv;

        public C1246a(Fragment fragment, IMapFragmentDelegate iMapFragmentDelegate) {
            this.fv = (IMapFragmentDelegate) C0142x.m495d(iMapFragmentDelegate);
            this.fF = (Fragment) C0142x.m495d(fragment);
        }

        public IMapFragmentDelegate aO() {
            return this.fv;
        }

        public void onCreate(Bundle savedInstanceState) {
            if (savedInstanceState == null) {
                try {
                    savedInstanceState = new Bundle();
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                }
            }
            Bundle arguments = this.fF.getArguments();
            if (arguments != null && arguments.containsKey("MapOptions")) {
                cv.m311a(savedInstanceState, "MapOptions", arguments.getParcelable("MapOptions"));
            }
            this.fv.onCreate(savedInstanceState);
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            try {
                return (View) bd.m1048a(this.fv.onCreateView(bd.m1049f(inflater), bd.m1049f(container), savedInstanceState));
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onDestroy() {
            try {
                this.fv.onDestroy();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onDestroyView() {
            try {
                this.fv.onDestroyView();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onInflate(Activity activity, Bundle attrs, Bundle savedInstanceState) {
            try {
                this.fv.onInflate(bd.m1049f(activity), (GoogleMapOptions) attrs.getParcelable("MapOptions"), savedInstanceState);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onLowMemory() {
            try {
                this.fv.onLowMemory();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onPause() {
            try {
                this.fv.onPause();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onResume() {
            try {
                this.fv.onResume();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onSaveInstanceState(Bundle outState) {
            try {
                this.fv.onSaveInstanceState(outState);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
    }

    /* renamed from: com.google.android.gms.maps.SupportMapFragment.b */
    static class C1247b extends bb<C1246a> {
        private Activity ar;
        private final Fragment fF;
        protected be<C1246a> fw;

        C1247b(Fragment fragment) {
            this.fF = fragment;
        }

        private void setActivity(Activity activity) {
            this.ar = activity;
            aP();
        }

        protected void m983a(be<C1246a> beVar) {
            this.fw = beVar;
            aP();
        }

        public void aP() {
            if (this.ar != null && this.fw != null && ag() == null) {
                try {
                    MapsInitializer.initialize(this.ar);
                    this.fw.m175a(new C1246a(this.fF, cw.m314g(this.ar).m301d(bd.m1049f(this.ar))));
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                } catch (GooglePlayServicesNotAvailableException e2) {
                }
            }
        }
    }

    public SupportMapFragment() {
        this.fE = new C1247b(this);
    }

    public static SupportMapFragment newInstance() {
        return new SupportMapFragment();
    }

    public static SupportMapFragment newInstance(GoogleMapOptions options) {
        SupportMapFragment supportMapFragment = new SupportMapFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("MapOptions", options);
        supportMapFragment.setArguments(bundle);
        return supportMapFragment;
    }

    protected IMapFragmentDelegate aO() {
        this.fE.aP();
        return this.fE.ag() == null ? null : ((C1246a) this.fE.ag()).aO();
    }

    public final GoogleMap getMap() {
        IMapFragmentDelegate aO = aO();
        if (aO == null) {
            return null;
        }
        try {
            IGoogleMapDelegate map = aO.getMap();
            if (map == null) {
                return null;
            }
            if (this.ft == null || this.ft.aF().asBinder() != map.asBinder()) {
                this.ft = new GoogleMap(map);
            }
            return this.ft;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.fE.setActivity(activity);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.fE.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.fE.onCreateView(inflater, container, savedInstanceState);
    }

    public void onDestroy() {
        this.fE.onDestroy();
        super.onDestroy();
    }

    public void onDestroyView() {
        this.fE.onDestroyView();
        super.onDestroyView();
    }

    public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState) {
        super.onInflate(activity, attrs, savedInstanceState);
        this.fE.setActivity(activity);
        Parcelable createFromAttributes = GoogleMapOptions.createFromAttributes(activity, attrs);
        Bundle bundle = new Bundle();
        bundle.putParcelable("MapOptions", createFromAttributes);
        this.fE.onInflate(activity, bundle, savedInstanceState);
    }

    public void onLowMemory() {
        this.fE.onLowMemory();
        super.onLowMemory();
    }

    public void onPause() {
        this.fE.onPause();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.fE.onResume();
    }

    public void onSaveInstanceState(Bundle outState) {
        this.fE.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    public void setArguments(Bundle args) {
        super.setArguments(args);
    }
}
