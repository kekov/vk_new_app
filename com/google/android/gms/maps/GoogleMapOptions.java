package com.google.android.gms.maps;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.util.AttributeSet;
import com.google.android.gms.R;
import com.google.android.gms.internal.ae;
import com.google.android.gms.internal.ci;
import com.google.android.gms.internal.cj;
import com.google.android.gms.internal.cx;
import com.google.android.gms.maps.model.CameraPosition;

public final class GoogleMapOptions implements ae {
    public static final GoogleMapOptionsCreator CREATOR;
    private final int f130T;
    private Boolean fi;
    private Boolean fj;
    private int fk;
    private CameraPosition fl;
    private Boolean fm;
    private Boolean fn;
    private Boolean fo;
    private Boolean fp;
    private Boolean fq;
    private Boolean fr;

    static {
        CREATOR = new GoogleMapOptionsCreator();
    }

    public GoogleMapOptions() {
        this.fk = -1;
        this.f130T = 1;
    }

    GoogleMapOptions(int versionCode, byte zOrderOnTop, byte useViewLifecycleInFragment, int mapType, CameraPosition camera, byte zoomControlsEnabled, byte compassEnabled, byte scrollGesturesEnabled, byte zoomGesturesEnabled, byte tiltGesturesEnabled, byte rotateGesturesEnabled) {
        this.fk = -1;
        this.f130T = versionCode;
        this.fi = cj.m296a(zOrderOnTop);
        this.fj = cj.m296a(useViewLifecycleInFragment);
        this.fk = mapType;
        this.fl = camera;
        this.fm = cj.m296a(zoomControlsEnabled);
        this.fn = cj.m296a(compassEnabled);
        this.fo = cj.m296a(scrollGesturesEnabled);
        this.fp = cj.m296a(zoomGesturesEnabled);
        this.fq = cj.m296a(tiltGesturesEnabled);
        this.fr = cj.m296a(rotateGesturesEnabled);
    }

    public static GoogleMapOptions createFromAttributes(Context context, AttributeSet attrs) {
        if (attrs == null) {
            return null;
        }
        TypedArray obtainAttributes = context.getResources().obtainAttributes(attrs, R.styleable.MapAttrs);
        GoogleMapOptions googleMapOptions = new GoogleMapOptions();
        if (obtainAttributes.hasValue(0)) {
            googleMapOptions.mapType(obtainAttributes.getInt(0, -1));
        }
        if (obtainAttributes.hasValue(13)) {
            googleMapOptions.zOrderOnTop(obtainAttributes.getBoolean(13, false));
        }
        if (obtainAttributes.hasValue(12)) {
            googleMapOptions.useViewLifecycleInFragment(obtainAttributes.getBoolean(12, false));
        }
        if (obtainAttributes.hasValue(6)) {
            googleMapOptions.compassEnabled(obtainAttributes.getBoolean(6, true));
        }
        if (obtainAttributes.hasValue(7)) {
            googleMapOptions.rotateGesturesEnabled(obtainAttributes.getBoolean(7, true));
        }
        if (obtainAttributes.hasValue(8)) {
            googleMapOptions.scrollGesturesEnabled(obtainAttributes.getBoolean(8, true));
        }
        if (obtainAttributes.hasValue(9)) {
            googleMapOptions.tiltGesturesEnabled(obtainAttributes.getBoolean(9, true));
        }
        if (obtainAttributes.hasValue(11)) {
            googleMapOptions.zoomGesturesEnabled(obtainAttributes.getBoolean(11, true));
        }
        if (obtainAttributes.hasValue(10)) {
            googleMapOptions.zoomControlsEnabled(obtainAttributes.getBoolean(10, true));
        }
        googleMapOptions.camera(CameraPosition.createFromAttributes(context, attrs));
        obtainAttributes.recycle();
        return googleMapOptions;
    }

    public byte aG() {
        return cj.m297b(this.fi);
    }

    public byte aH() {
        return cj.m297b(this.fj);
    }

    public byte aI() {
        return cj.m297b(this.fm);
    }

    public byte aJ() {
        return cj.m297b(this.fn);
    }

    public byte aK() {
        return cj.m297b(this.fo);
    }

    public byte aL() {
        return cj.m297b(this.fp);
    }

    public byte aM() {
        return cj.m297b(this.fq);
    }

    public byte aN() {
        return cj.m297b(this.fr);
    }

    public GoogleMapOptions camera(CameraPosition camera) {
        this.fl = camera;
        return this;
    }

    public GoogleMapOptions compassEnabled(boolean enabled) {
        this.fn = Boolean.valueOf(enabled);
        return this;
    }

    public int describeContents() {
        return 0;
    }

    public CameraPosition getCamera() {
        return this.fl;
    }

    public Boolean getCompassEnabled() {
        return this.fn;
    }

    public int getMapType() {
        return this.fk;
    }

    public Boolean getRotateGesturesEnabled() {
        return this.fr;
    }

    public Boolean getScrollGesturesEnabled() {
        return this.fo;
    }

    public Boolean getTiltGesturesEnabled() {
        return this.fq;
    }

    public Boolean getUseViewLifecycleInFragment() {
        return this.fj;
    }

    public Boolean getZOrderOnTop() {
        return this.fi;
    }

    public Boolean getZoomControlsEnabled() {
        return this.fm;
    }

    public Boolean getZoomGesturesEnabled() {
        return this.fp;
    }

    public GoogleMapOptions mapType(int mapType) {
        this.fk = mapType;
        return this;
    }

    public GoogleMapOptions rotateGesturesEnabled(boolean enabled) {
        this.fr = Boolean.valueOf(enabled);
        return this;
    }

    public GoogleMapOptions scrollGesturesEnabled(boolean enabled) {
        this.fo = Boolean.valueOf(enabled);
        return this;
    }

    public GoogleMapOptions tiltGesturesEnabled(boolean enabled) {
        this.fq = Boolean.valueOf(enabled);
        return this;
    }

    public int m978u() {
        return this.f130T;
    }

    public GoogleMapOptions useViewLifecycleInFragment(boolean useViewLifecycleInFragment) {
        this.fj = Boolean.valueOf(useViewLifecycleInFragment);
        return this;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (cx.aV()) {
            ci.m295a(this, out, flags);
        } else {
            GoogleMapOptionsCreator.m504a(this, out, flags);
        }
    }

    public GoogleMapOptions zOrderOnTop(boolean zOrderOnTop) {
        this.fi = Boolean.valueOf(zOrderOnTop);
        return this;
    }

    public GoogleMapOptions zoomControlsEnabled(boolean enabled) {
        this.fm = Boolean.valueOf(enabled);
        return this;
    }

    public GoogleMapOptions zoomGesturesEnabled(boolean enabled) {
        this.fp = Boolean.valueOf(enabled);
        return this;
    }
}
