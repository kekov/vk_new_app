package com.google.android.gms.maps;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.dynamic.LifecycleDelegate;
import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.bb;
import com.google.android.gms.internal.bd;
import com.google.android.gms.internal.be;
import com.google.android.gms.internal.cv;
import com.google.android.gms.internal.cw;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.internal.IMapFragmentDelegate;
import com.google.android.gms.maps.model.RuntimeRemoteException;

public class MapFragment extends Fragment {
    private final C1243b fs;
    private GoogleMap ft;

    /* renamed from: com.google.android.gms.maps.MapFragment.a */
    static class C1242a implements LifecycleDelegate {
        private final Fragment fu;
        private final IMapFragmentDelegate fv;

        public C1242a(Fragment fragment, IMapFragmentDelegate iMapFragmentDelegate) {
            this.fv = (IMapFragmentDelegate) C0142x.m495d(iMapFragmentDelegate);
            this.fu = (Fragment) C0142x.m495d(fragment);
        }

        public IMapFragmentDelegate aO() {
            return this.fv;
        }

        public void onCreate(Bundle savedInstanceState) {
            if (savedInstanceState == null) {
                try {
                    savedInstanceState = new Bundle();
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                }
            }
            Bundle arguments = this.fu.getArguments();
            if (arguments != null && arguments.containsKey("MapOptions")) {
                cv.m311a(savedInstanceState, "MapOptions", arguments.getParcelable("MapOptions"));
            }
            this.fv.onCreate(savedInstanceState);
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            try {
                return (View) bd.m1048a(this.fv.onCreateView(bd.m1049f(inflater), bd.m1049f(container), savedInstanceState));
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onDestroy() {
            try {
                this.fv.onDestroy();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onDestroyView() {
            try {
                this.fv.onDestroyView();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onInflate(Activity activity, Bundle attrs, Bundle savedInstanceState) {
            try {
                this.fv.onInflate(bd.m1049f(activity), (GoogleMapOptions) attrs.getParcelable("MapOptions"), savedInstanceState);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onLowMemory() {
            try {
                this.fv.onLowMemory();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onPause() {
            try {
                this.fv.onPause();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onResume() {
            try {
                this.fv.onResume();
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }

        public void onSaveInstanceState(Bundle outState) {
            try {
                this.fv.onSaveInstanceState(outState);
            } catch (RemoteException e) {
                throw new RuntimeRemoteException(e);
            }
        }
    }

    /* renamed from: com.google.android.gms.maps.MapFragment.b */
    static class C1243b extends bb<C1242a> {
        private Activity ar;
        private final Fragment fu;
        protected be<C1242a> fw;

        C1243b(Fragment fragment) {
            this.fu = fragment;
        }

        private void setActivity(Activity activity) {
            this.ar = activity;
            aP();
        }

        protected void m980a(be<C1242a> beVar) {
            this.fw = beVar;
            aP();
        }

        public void aP() {
            if (this.ar != null && this.fw != null && ag() == null) {
                try {
                    MapsInitializer.initialize(this.ar);
                    this.fw.m175a(new C1242a(this.fu, cw.m314g(this.ar).m301d(bd.m1049f(this.ar))));
                } catch (RemoteException e) {
                    throw new RuntimeRemoteException(e);
                } catch (GooglePlayServicesNotAvailableException e2) {
                }
            }
        }
    }

    public MapFragment() {
        this.fs = new C1243b(this);
    }

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    public static MapFragment newInstance(GoogleMapOptions options) {
        MapFragment mapFragment = new MapFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("MapOptions", options);
        mapFragment.setArguments(bundle);
        return mapFragment;
    }

    protected IMapFragmentDelegate aO() {
        this.fs.aP();
        return this.fs.ag() == null ? null : ((C1242a) this.fs.ag()).aO();
    }

    public final GoogleMap getMap() {
        IMapFragmentDelegate aO = aO();
        if (aO == null) {
            return null;
        }
        try {
            IGoogleMapDelegate map = aO.getMap();
            if (map == null) {
                return null;
            }
            if (this.ft == null || this.ft.aF().asBinder() != map.asBinder()) {
                this.ft = new GoogleMap(map);
            }
            return this.ft;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.fs.setActivity(activity);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.fs.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.fs.onCreateView(inflater, container, savedInstanceState);
    }

    public void onDestroy() {
        this.fs.onDestroy();
        super.onDestroy();
    }

    public void onDestroyView() {
        this.fs.onDestroyView();
        super.onDestroyView();
    }

    public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState) {
        super.onInflate(activity, attrs, savedInstanceState);
        this.fs.setActivity(activity);
        Parcelable createFromAttributes = GoogleMapOptions.createFromAttributes(activity, attrs);
        Bundle bundle = new Bundle();
        bundle.putParcelable("MapOptions", createFromAttributes);
        this.fs.onInflate(activity, bundle, savedInstanceState);
    }

    public void onLowMemory() {
        this.fs.onLowMemory();
        super.onLowMemory();
    }

    public void onPause() {
        this.fs.onPause();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.fs.onResume();
    }

    public void onSaveInstanceState(Bundle outState) {
        this.fs.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    public void setArguments(Bundle args) {
        super.setArguments(args);
    }
}
