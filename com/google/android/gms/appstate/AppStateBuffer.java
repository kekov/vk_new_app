package com.google.android.gms.appstate;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.C1230k;
import com.google.android.gms.internal.C1585c;

public final class AppStateBuffer extends DataBuffer<AppState> {
    public AppStateBuffer(C1230k dataHolder) {
        super(dataHolder);
    }

    public AppState get(int position) {
        return new C1585c(this.O, position);
    }
}
