package com.google.android.gms.internal;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.C0136t.C1236a;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import net.hockeyapp.android.Strings;

/* renamed from: com.google.android.gms.internal.u */
public interface C0137u extends IInterface {

    /* renamed from: com.google.android.gms.internal.u.a */
    public static abstract class C1238a extends Binder implements C0137u {

        /* renamed from: com.google.android.gms.internal.u.a.a */
        static class C1237a implements C0137u {
            private IBinder f125a;

            C1237a(IBinder iBinder) {
                this.f125a = iBinder;
            }

            public void m959a(C0136t c0136t, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(c0136t != null ? c0136t.asBinder() : null);
                    obtain.writeInt(i);
                    this.f125a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m960a(C0136t c0136t, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(c0136t != null ? c0136t.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.f125a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m961a(C0136t c0136t, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(c0136t != null ? c0136t.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f125a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m962a(C0136t c0136t, int i, String str, String str2, String[] strArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(c0136t != null ? c0136t.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeStringArray(strArr);
                    this.f125a.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m963a(C0136t c0136t, int i, String str, String str2, String[] strArr, String str3, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(c0136t != null ? c0136t.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeStringArray(strArr);
                    obtain.writeString(str3);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f125a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m964a(C0136t c0136t, int i, String str, String str2, String[] strArr, String str3, IBinder iBinder, String str4, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(c0136t != null ? c0136t.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeStringArray(strArr);
                    obtain.writeString(str3);
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str4);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f125a.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f125a;
            }

            public void m965b(C0136t c0136t, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(c0136t != null ? c0136t.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f125a.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m966c(C0136t c0136t, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(c0136t != null ? c0136t.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f125a.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m967d(C0136t c0136t, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(c0136t != null ? c0136t.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f125a.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m968e(C0136t c0136t, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(c0136t != null ? c0136t.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f125a.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m969f(C0136t c0136t, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(c0136t != null ? c0136t.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f125a.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public static C0137u m970h(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof C0137u)) ? new C1237a(iBinder) : (C0137u) queryLocalInterface;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            Bundle bundle = null;
            C0136t g;
            int readInt;
            String readString;
            switch (code) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    m478a(C1236a.m958g(data.readStrongBinder()), data.readInt(), data.readString(), data.readString(), data.createStringArray(), data.readString(), data.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(data) : null);
                    reply.writeNoException();
                    return true;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    g = C1236a.m958g(data.readStrongBinder());
                    readInt = data.readInt();
                    readString = data.readString();
                    if (data.readInt() != 0) {
                        bundle = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    m476a(g, readInt, readString, bundle);
                    reply.writeNoException();
                    return true;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    m475a(C1236a.m958g(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case UserListView.TYPE_FAVE /*4*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    m474a(C1236a.m958g(data.readStrongBinder()), data.readInt());
                    reply.writeNoException();
                    return true;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    g = C1236a.m958g(data.readStrongBinder());
                    readInt = data.readInt();
                    readString = data.readString();
                    if (data.readInt() != 0) {
                        bundle = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    m480b(g, readInt, readString, bundle);
                    reply.writeNoException();
                    return true;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    g = C1236a.m958g(data.readStrongBinder());
                    readInt = data.readInt();
                    readString = data.readString();
                    if (data.readInt() != 0) {
                        bundle = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    m481c(g, readInt, readString, bundle);
                    reply.writeNoException();
                    return true;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    g = C1236a.m958g(data.readStrongBinder());
                    readInt = data.readInt();
                    readString = data.readString();
                    if (data.readInt() != 0) {
                        bundle = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    m482d(g, readInt, readString, bundle);
                    reply.writeNoException();
                    return true;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    g = C1236a.m958g(data.readStrongBinder());
                    readInt = data.readInt();
                    readString = data.readString();
                    if (data.readInt() != 0) {
                        bundle = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    m483e(g, readInt, readString, bundle);
                    reply.writeNoException();
                    return true;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    m479a(C1236a.m958g(data.readStrongBinder()), data.readInt(), data.readString(), data.readString(), data.createStringArray(), data.readString(), data.readStrongBinder(), data.readString(), data.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(data) : null);
                    reply.writeNoException();
                    return true;
                case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    m477a(C1236a.m958g(data.readStrongBinder()), data.readInt(), data.readString(), data.readString(), data.createStringArray());
                    reply.writeNoException();
                    return true;
                case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    g = C1236a.m958g(data.readStrongBinder());
                    readInt = data.readInt();
                    readString = data.readString();
                    if (data.readInt() != 0) {
                        bundle = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    m484f(g, readInt, readString, bundle);
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.common.internal.IGmsServiceBroker");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void m474a(C0136t c0136t, int i) throws RemoteException;

    void m475a(C0136t c0136t, int i, String str) throws RemoteException;

    void m476a(C0136t c0136t, int i, String str, Bundle bundle) throws RemoteException;

    void m477a(C0136t c0136t, int i, String str, String str2, String[] strArr) throws RemoteException;

    void m478a(C0136t c0136t, int i, String str, String str2, String[] strArr, String str3, Bundle bundle) throws RemoteException;

    void m479a(C0136t c0136t, int i, String str, String str2, String[] strArr, String str3, IBinder iBinder, String str4, Bundle bundle) throws RemoteException;

    void m480b(C0136t c0136t, int i, String str, Bundle bundle) throws RemoteException;

    void m481c(C0136t c0136t, int i, String str, Bundle bundle) throws RemoteException;

    void m482d(C0136t c0136t, int i, String str, Bundle bundle) throws RemoteException;

    void m483e(C0136t c0136t, int i, String str, Bundle bundle) throws RemoteException;

    void m484f(C0136t c0136t, int i, String str, Bundle bundle) throws RemoteException;
}
