package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.plus.model.moments.ItemScope;
import com.google.android.gms.plus.model.moments.Moment;

public final class eh extends C0123j implements Moment {
    private ef iS;

    public eh(C1230k c1230k, int i) {
        super(c1230k, i);
    }

    private ef bS() {
        synchronized (this) {
            if (this.iS == null) {
                byte[] byteArray = getByteArray("momentImpl");
                Parcel obtain = Parcel.obtain();
                obtain.unmarshall(byteArray, 0, byteArray.length);
                obtain.setDataPosition(0);
                this.iS = ef.CREATOR.m390w(obtain);
                obtain.recycle();
            }
        }
        return this.iS;
    }

    public ef bR() {
        return bS();
    }

    public /* synthetic */ Object freeze() {
        return bR();
    }

    public String getId() {
        return bS().getId();
    }

    public ItemScope getResult() {
        return bS().getResult();
    }

    public String getStartDate() {
        return bS().getStartDate();
    }

    public ItemScope getTarget() {
        return bS().getTarget();
    }

    public String getType() {
        return bS().getType();
    }

    public boolean hasId() {
        return bS().hasId();
    }

    public boolean hasResult() {
        return bS().hasId();
    }

    public boolean hasStartDate() {
        return bS().hasStartDate();
    }

    public boolean hasTarget() {
        return bS().hasTarget();
    }

    public boolean hasType() {
        return bS().hasType();
    }
}
