package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.GroundOverlayOptions;

public class da {
    public static void m320a(GroundOverlayOptions groundOverlayOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, groundOverlayOptions.m994u());
        ad.m93a(parcel, 2, groundOverlayOptions.aX(), false);
        ad.m95a(parcel, 3, groundOverlayOptions.getLocation(), i, false);
        ad.m90a(parcel, 4, groundOverlayOptions.getWidth());
        ad.m90a(parcel, 5, groundOverlayOptions.getHeight());
        ad.m95a(parcel, 6, groundOverlayOptions.getBounds(), i, false);
        ad.m90a(parcel, 7, groundOverlayOptions.getBearing());
        ad.m90a(parcel, 8, groundOverlayOptions.getZIndex());
        ad.m99a(parcel, 9, groundOverlayOptions.isVisible());
        ad.m90a(parcel, 10, groundOverlayOptions.getTransparency());
        ad.m90a(parcel, 11, groundOverlayOptions.getAnchorU());
        ad.m90a(parcel, 12, groundOverlayOptions.getAnchorV());
        ad.m87C(parcel, d);
    }
}
