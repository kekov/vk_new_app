package com.google.android.gms.internal;

import android.os.Parcel;
import com.facebook.WebDialog;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationEntity;
import com.google.android.gms.games.multiplayer.Participant;
import java.util.ArrayList;

public final class bw extends C0123j implements Invitation {
    private final ArrayList<Participant> dJ;
    private final Game dK;
    private final bx dL;

    public bw(C1230k c1230k, int i, int i2) {
        super(c1230k, i);
        this.dK = new bf(c1230k, i);
        this.dJ = new ArrayList(i2);
        String string = getString("external_inviter_id");
        Object obj = null;
        for (int i3 = 0; i3 < i2; i3++) {
            bx bxVar = new bx(this.O, this.R + i3);
            if (bxVar.getParticipantId().equals(string)) {
                obj = bxVar;
            }
            this.dJ.add(bxVar);
        }
        this.dL = (bx) C0142x.m492b(obj, (Object) "Must have a valid inviter!");
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return InvitationEntity.m1036a(this, obj);
    }

    public Invitation freeze() {
        return new InvitationEntity(this);
    }

    public long getCreationTimestamp() {
        return getLong("creation_timestamp");
    }

    public Game getGame() {
        return this.dK;
    }

    public String getInvitationId() {
        return getString("external_invitation_id");
    }

    public int getInvitationType() {
        return getInteger(WebDialog.DIALOG_PARAM_TYPE);
    }

    public Participant getInviter() {
        return this.dL;
    }

    public ArrayList<Participant> getParticipants() {
        return this.dJ;
    }

    public int hashCode() {
        return InvitationEntity.m1035a(this);
    }

    public String toString() {
        return InvitationEntity.m1037b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        ((InvitationEntity) freeze()).writeToParcel(dest, flags);
    }
}
