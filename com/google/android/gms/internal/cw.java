package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.cl.C1174a;
import com.google.android.gms.maps.model.RuntimeRemoteException;

public class cw {
    private static Context aZ;
    private static cl fH;

    private static <T> T m312a(ClassLoader classLoader, String str) {
        try {
            return m313b(((ClassLoader) C0142x.m495d(classLoader)).loadClass(str));
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Unable to find dynamic class " + str);
        }
    }

    public static boolean aT() {
        return aU() != null;
    }

    private static Class<?> aU() {
        try {
            return Class.forName("com.google.android.gms.maps.internal.CreatorImpl");
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    private static <T> T m313b(Class<?> cls) {
        try {
            return cls.newInstance();
        } catch (InstantiationException e) {
            throw new IllegalStateException("Unable to instantiate the dynamic class " + cls.getName());
        } catch (IllegalAccessException e2) {
            throw new IllegalStateException("Unable to call the default constructor of " + cls.getName());
        }
    }

    public static cl m314g(Context context) throws GooglePlayServicesNotAvailableException {
        C0142x.m495d(context);
        m316i(context);
        if (fH == null) {
            m317j(context);
        }
        if (fH != null) {
            return fH;
        }
        fH = C1174a.m778t((IBinder) m312a(getRemoteContext(context).getClassLoader(), "com.google.android.gms.maps.internal.CreatorImpl"));
        m315h(context);
        return fH;
    }

    private static Context getRemoteContext(Context context) {
        if (aZ == null) {
            if (aU() != null) {
                aZ = context;
            } else {
                aZ = GooglePlayServicesUtil.getRemoteContext(context);
            }
        }
        return aZ;
    }

    private static void m315h(Context context) {
        try {
            fH.m299a(bd.m1049f(getRemoteContext(context).getResources()), (int) GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    public static void m316i(Context context) throws GooglePlayServicesNotAvailableException {
        if (!aT()) {
            int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
            if (isGooglePlayServicesAvailable != 0) {
                throw new GooglePlayServicesNotAvailableException(isGooglePlayServicesAvailable);
            }
        }
    }

    private static void m317j(Context context) {
        Class aU = aU();
        if (aU != null) {
            Log.i(cw.class.getSimpleName(), "Making Creator statically");
            fH = (cl) m313b(aU);
            m315h(context);
        }
    }
}
