package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.internal.an.C0108b;

public class ai implements ae {
    public static final aj CREATOR;
    private final int f66T;
    private final ak bo;

    static {
        CREATOR = new aj();
    }

    ai(int i, ak akVar) {
        this.f66T = i;
        this.bo = akVar;
    }

    private ai(ak akVar) {
        this.f66T = 1;
        this.bo = akVar;
    }

    public static ai m560a(C0108b<?, ?> c0108b) {
        if (c0108b instanceof ak) {
            return new ai((ak) c0108b);
        }
        throw new IllegalArgumentException("Unsupported safe parcelable field converter class.");
    }

    ak m561B() {
        return this.bo;
    }

    public C0108b<?, ?> m562C() {
        if (this.bo != null) {
            return this.bo;
        }
        throw new IllegalStateException("There was no converter wrapped in this ConverterWrapper.");
    }

    public int describeContents() {
        aj ajVar = CREATOR;
        return 0;
    }

    int m563u() {
        return this.f66T;
    }

    public void writeToParcel(Parcel out, int flags) {
        aj ajVar = CREATOR;
        aj.m113a(this, out, flags);
    }
}
