package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.eq.C1599b.C1598b;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashSet;
import java.util.Set;

public class el implements Creator<C1598b> {
    static void m400a(C1598b c1598b, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        Set by = c1598b.by();
        if (by.contains(Integer.valueOf(1))) {
            ad.m106c(parcel, 1, c1598b.m1214u());
        }
        if (by.contains(Integer.valueOf(2))) {
            ad.m106c(parcel, 2, c1598b.getHeight());
        }
        if (by.contains(Integer.valueOf(3))) {
            ad.m96a(parcel, 3, c1598b.getUrl(), true);
        }
        if (by.contains(Integer.valueOf(4))) {
            ad.m106c(parcel, 4, c1598b.getWidth());
        }
        ad.m87C(parcel, d);
    }

    public C1598b m401A(Parcel parcel) {
        int i = 0;
        int c = ac.m58c(parcel);
        Set hashSet = new HashSet();
        String str = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i3 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    i2 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(2));
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    str = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(3));
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    i = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(4));
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new C1598b(hashSet, i3, i2, str, i);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public C1598b[] m402U(int i) {
        return new C1598b[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m401A(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m402U(x0);
    }
}
