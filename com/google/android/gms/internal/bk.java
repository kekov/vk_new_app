package com.google.android.gms.internal;

public final class bk {
    private static final C0135s dg;

    static {
        dg = new C0135s("Games");
    }

    public static void m176a(String str, String str2) {
        dg.m467a(str, str2);
    }

    public static void m177a(String str, String str2, Throwable th) {
        dg.m468a(str, str2, th);
    }

    public static void m178b(String str, String str2) {
        dg.m469b(str, str2);
    }

    public static void m179c(String str, String str2) {
        dg.m470c(str, str2);
    }

    public static void m180d(String str, String str2) {
        dg.m471d(str, str2);
    }
}
