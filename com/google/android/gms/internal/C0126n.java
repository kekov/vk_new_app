package com.google.android.gms.internal;

/* renamed from: com.google.android.gms.internal.n */
public final class C0126n {
    public static void m442a(boolean z) {
        if (!z) {
            throw new IllegalStateException();
        }
    }

    public static void m443a(boolean z, Object obj) {
        if (!z) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    public static void m444b(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("null reference");
        }
    }
}
