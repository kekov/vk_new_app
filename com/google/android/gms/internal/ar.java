package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.aq.C1143a;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.ArrayList;

public class ar implements Creator<aq> {
    static void m143a(aq aqVar, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, aqVar.m606u());
        ad.m105b(parcel, 2, aqVar.m601V(), false);
        ad.m96a(parcel, 3, aqVar.m602W(), false);
        ad.m87C(parcel, d);
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m144k(x0);
    }

    public aq m144k(Parcel parcel) {
        String str = null;
        int c = ac.m58c(parcel);
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    arrayList = ac.m59c(parcel, b, C1143a.CREATOR);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    str = ac.m70l(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new aq(i, arrayList, str);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m145q(x0);
    }

    public aq[] m145q(int i) {
        return new aq[i];
    }
}
