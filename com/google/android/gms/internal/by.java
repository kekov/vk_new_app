package com.google.android.gms.internal;

import com.google.android.gms.games.multiplayer.realtime.Room;

public final class by extends C1231m<Room> {
    public by(C1230k c1230k) {
        super(c1230k);
    }

    protected /* synthetic */ Object m1128a(int i, int i2) {
        return m1129b(i, i2);
    }

    protected Room m1129b(int i, int i2) {
        return new bz(this.O, i, i2);
    }

    protected String getPrimaryDataMarkerColumn() {
        return "external_match_id";
    }
}
