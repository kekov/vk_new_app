package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class aj implements Creator<ai> {
    static void m113a(ai aiVar, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, aiVar.m563u());
        ad.m95a(parcel, 2, aiVar.m561B(), i, false);
        ad.m87C(parcel, d);
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m114f(x0);
    }

    public ai m114f(Parcel parcel) {
        int c = ac.m58c(parcel);
        int i = 0;
        ak akVar = null;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    akVar = (ak) ac.m52a(parcel, b, ak.CREATOR);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new ai(i, akVar);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public ai[] m115l(int i) {
        return new ai[i];
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m115l(x0);
    }
}
