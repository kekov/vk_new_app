package com.google.android.gms.internal;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class du {
    Bundle dq;

    public du(Bundle bundle) {
        this.dq = bundle;
    }

    public boolean bd() {
        return this.dq.getBoolean("has_plus_one", false);
    }

    public String be() {
        return this.dq.getString("bubble_text");
    }

    public String[] bf() {
        return this.dq.getStringArray("inline_annotations");
    }

    public Uri[] bg() {
        Object parcelableArray = this.dq.getParcelableArray("profile_photo_uris");
        if (parcelableArray == null) {
            return null;
        }
        Object obj = new Uri[parcelableArray.length];
        System.arraycopy(parcelableArray, 0, obj, 0, parcelableArray.length);
        return obj;
    }

    public Intent getIntent() {
        return (Intent) this.dq.getParcelable("intent");
    }
}
