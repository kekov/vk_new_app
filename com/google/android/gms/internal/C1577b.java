package com.google.android.gms.internal;

import com.google.android.gms.appstate.AppState;

/* renamed from: com.google.android.gms.internal.b */
public final class C1577b implements AppState {
    private final int f159h;
    private final String f160i;
    private final byte[] f161j;
    private final boolean f162k;
    private final String f163l;
    private final byte[] f164m;

    public C1577b(AppState appState) {
        this.f159h = appState.getKey();
        this.f160i = appState.getLocalVersion();
        this.f161j = appState.getLocalData();
        this.f162k = appState.hasConflict();
        this.f163l = appState.getConflictVersion();
        this.f164m = appState.getConflictData();
    }

    static int m1044a(AppState appState) {
        return C0141w.hashCode(Integer.valueOf(appState.getKey()), appState.getLocalVersion(), appState.getLocalData(), Boolean.valueOf(appState.hasConflict()), appState.getConflictVersion(), appState.getConflictData());
    }

    static boolean m1045a(AppState appState, Object obj) {
        if (!(obj instanceof AppState)) {
            return false;
        }
        if (appState == obj) {
            return true;
        }
        AppState appState2 = (AppState) obj;
        return C0141w.m487a(Integer.valueOf(appState2.getKey()), Integer.valueOf(appState.getKey())) && C0141w.m487a(appState2.getLocalVersion(), appState.getLocalVersion()) && C0141w.m487a(appState2.getLocalData(), appState.getLocalData()) && C0141w.m487a(Boolean.valueOf(appState2.hasConflict()), Boolean.valueOf(appState.hasConflict())) && C0141w.m487a(appState2.getConflictVersion(), appState.getConflictVersion()) && C0141w.m487a(appState2.getConflictData(), appState.getConflictData());
    }

    static String m1046b(AppState appState) {
        return C0141w.m488c(appState).m486a("Key", Integer.valueOf(appState.getKey())).m486a("LocalVersion", appState.getLocalVersion()).m486a("LocalData", appState.getLocalData()).m486a("HasConflict", Boolean.valueOf(appState.hasConflict())).m486a("ConflictVersion", appState.getConflictVersion()).m486a("ConflictData", appState.getConflictData()).toString();
    }

    public AppState m1047a() {
        return this;
    }

    public boolean equals(Object obj) {
        return C1577b.m1045a(this, obj);
    }

    public /* synthetic */ Object freeze() {
        return m1047a();
    }

    public byte[] getConflictData() {
        return this.f164m;
    }

    public String getConflictVersion() {
        return this.f163l;
    }

    public int getKey() {
        return this.f159h;
    }

    public byte[] getLocalData() {
        return this.f161j;
    }

    public String getLocalVersion() {
        return this.f160i;
    }

    public boolean hasConflict() {
        return this.f162k;
    }

    public int hashCode() {
        return C1577b.m1044a(this);
    }

    public String toString() {
        return C1577b.m1046b(this);
    }
}
