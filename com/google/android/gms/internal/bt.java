package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.leaderboard.LeaderboardScore;

public final class bt implements LeaderboardScore {
    private final long dr;
    private final String ds;
    private final String dt;
    private final long du;
    private final long dv;
    private final String dw;
    private final Uri dx;
    private final Uri dy;
    private final PlayerEntity dz;

    public bt(LeaderboardScore leaderboardScore) {
        this.dr = leaderboardScore.getRank();
        this.ds = (String) C0142x.m495d(leaderboardScore.getDisplayRank());
        this.dt = (String) C0142x.m495d(leaderboardScore.getDisplayScore());
        this.du = leaderboardScore.getRawScore();
        this.dv = leaderboardScore.getTimestampMillis();
        this.dw = leaderboardScore.getScoreHolderDisplayName();
        this.dx = leaderboardScore.getScoreHolderIconImageUri();
        this.dy = leaderboardScore.getScoreHolderHiResImageUri();
        Player scoreHolder = leaderboardScore.getScoreHolder();
        this.dz = scoreHolder == null ? null : (PlayerEntity) scoreHolder.freeze();
    }

    static int m1125a(LeaderboardScore leaderboardScore) {
        return C0141w.hashCode(Long.valueOf(leaderboardScore.getRank()), leaderboardScore.getDisplayRank(), Long.valueOf(leaderboardScore.getRawScore()), leaderboardScore.getDisplayScore(), Long.valueOf(leaderboardScore.getTimestampMillis()), leaderboardScore.getScoreHolderDisplayName(), leaderboardScore.getScoreHolderIconImageUri(), leaderboardScore.getScoreHolderHiResImageUri(), leaderboardScore.getScoreHolder());
    }

    static boolean m1126a(LeaderboardScore leaderboardScore, Object obj) {
        if (!(obj instanceof LeaderboardScore)) {
            return false;
        }
        if (leaderboardScore == obj) {
            return true;
        }
        LeaderboardScore leaderboardScore2 = (LeaderboardScore) obj;
        return C0141w.m487a(Long.valueOf(leaderboardScore2.getRank()), Long.valueOf(leaderboardScore.getRank())) && C0141w.m487a(leaderboardScore2.getDisplayRank(), leaderboardScore.getDisplayRank()) && C0141w.m487a(Long.valueOf(leaderboardScore2.getRawScore()), Long.valueOf(leaderboardScore.getRawScore())) && C0141w.m487a(leaderboardScore2.getDisplayScore(), leaderboardScore.getDisplayScore()) && C0141w.m487a(Long.valueOf(leaderboardScore2.getTimestampMillis()), Long.valueOf(leaderboardScore.getTimestampMillis())) && C0141w.m487a(leaderboardScore2.getScoreHolderDisplayName(), leaderboardScore.getScoreHolderDisplayName()) && C0141w.m487a(leaderboardScore2.getScoreHolderIconImageUri(), leaderboardScore.getScoreHolderIconImageUri()) && C0141w.m487a(leaderboardScore2.getScoreHolderHiResImageUri(), leaderboardScore.getScoreHolderHiResImageUri()) && C0141w.m487a(leaderboardScore2.getScoreHolder(), leaderboardScore.getScoreHolder());
    }

    static String m1127b(LeaderboardScore leaderboardScore) {
        return C0141w.m488c(leaderboardScore).m486a("Rank", Long.valueOf(leaderboardScore.getRank())).m486a("DisplayRank", leaderboardScore.getDisplayRank()).m486a("Score", Long.valueOf(leaderboardScore.getRawScore())).m486a("DisplayScore", leaderboardScore.getDisplayScore()).m486a("Timestamp", Long.valueOf(leaderboardScore.getTimestampMillis())).m486a("DisplayName", leaderboardScore.getScoreHolderDisplayName()).m486a("IconImageUri", leaderboardScore.getScoreHolderIconImageUri()).m486a("HiResImageUri", leaderboardScore.getScoreHolderHiResImageUri()).m486a("Player", leaderboardScore.getScoreHolder() == null ? null : leaderboardScore.getScoreHolder()).toString();
    }

    public LeaderboardScore as() {
        return this;
    }

    public boolean equals(Object obj) {
        return m1126a(this, obj);
    }

    public /* synthetic */ Object freeze() {
        return as();
    }

    public String getDisplayRank() {
        return this.ds;
    }

    public void getDisplayRank(CharArrayBuffer dataOut) {
        ax.m162b(this.ds, dataOut);
    }

    public String getDisplayScore() {
        return this.dt;
    }

    public void getDisplayScore(CharArrayBuffer dataOut) {
        ax.m162b(this.dt, dataOut);
    }

    public long getRank() {
        return this.dr;
    }

    public long getRawScore() {
        return this.du;
    }

    public Player getScoreHolder() {
        return this.dz;
    }

    public String getScoreHolderDisplayName() {
        return this.dz == null ? this.dw : this.dz.getDisplayName();
    }

    public void getScoreHolderDisplayName(CharArrayBuffer dataOut) {
        if (this.dz == null) {
            ax.m162b(this.dw, dataOut);
        } else {
            this.dz.getDisplayName(dataOut);
        }
    }

    public Uri getScoreHolderHiResImageUri() {
        return this.dz == null ? this.dy : this.dz.getHiResImageUri();
    }

    public Uri getScoreHolderIconImageUri() {
        return this.dz == null ? this.dx : this.dz.getIconImageUri();
    }

    public long getTimestampMillis() {
        return this.dv;
    }

    public int hashCode() {
        return m1125a(this);
    }

    public String toString() {
        return m1127b(this);
    }
}
