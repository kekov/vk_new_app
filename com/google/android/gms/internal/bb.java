package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.dynamic.LifecycleDelegate;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class bb<T extends LifecycleDelegate> {
    private T bQ;
    private Bundle bR;
    private LinkedList<C0110a> bS;
    private final be<T> bT;

    /* renamed from: com.google.android.gms.internal.bb.5 */
    class C01095 implements OnClickListener {
        final /* synthetic */ bb bU;
        final /* synthetic */ Context cb;
        final /* synthetic */ int cc;

        C01095(bb bbVar, Context context, int i) {
            this.bU = bbVar;
            this.cb = context;
            this.cc = i;
        }

        public void onClick(View v) {
            this.cb.startActivity(GooglePlayServicesUtil.m9a(this.cb, this.cc, -1));
        }
    }

    /* renamed from: com.google.android.gms.internal.bb.a */
    interface C0110a {
        void m166b(LifecycleDelegate lifecycleDelegate);

        int getState();
    }

    /* renamed from: com.google.android.gms.internal.bb.1 */
    class C11451 implements be<T> {
        final /* synthetic */ bb bU;

        C11451(bb bbVar) {
            this.bU = bbVar;
        }

        public void m626a(T t) {
            this.bU.bQ = t;
            Iterator it = this.bU.bS.iterator();
            while (it.hasNext()) {
                ((C0110a) it.next()).m166b(this.bU.bQ);
            }
            this.bU.bS.clear();
            this.bU.bR = null;
        }
    }

    /* renamed from: com.google.android.gms.internal.bb.2 */
    class C11462 implements C0110a {
        final /* synthetic */ bb bU;
        final /* synthetic */ Activity bV;
        final /* synthetic */ Bundle bW;
        final /* synthetic */ Bundle bX;

        C11462(bb bbVar, Activity activity, Bundle bundle, Bundle bundle2) {
            this.bU = bbVar;
            this.bV = activity;
            this.bW = bundle;
            this.bX = bundle2;
        }

        public void m627b(LifecycleDelegate lifecycleDelegate) {
            this.bU.bQ.onInflate(this.bV, this.bW, this.bX);
        }

        public int getState() {
            return 0;
        }
    }

    /* renamed from: com.google.android.gms.internal.bb.3 */
    class C11473 implements C0110a {
        final /* synthetic */ bb bU;
        final /* synthetic */ Bundle bX;

        C11473(bb bbVar, Bundle bundle) {
            this.bU = bbVar;
            this.bX = bundle;
        }

        public void m628b(LifecycleDelegate lifecycleDelegate) {
            this.bU.bQ.onCreate(this.bX);
        }

        public int getState() {
            return 1;
        }
    }

    /* renamed from: com.google.android.gms.internal.bb.4 */
    class C11484 implements C0110a {
        final /* synthetic */ bb bU;
        final /* synthetic */ Bundle bX;
        final /* synthetic */ FrameLayout bY;
        final /* synthetic */ LayoutInflater bZ;
        final /* synthetic */ ViewGroup ca;

        C11484(bb bbVar, FrameLayout frameLayout, LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            this.bU = bbVar;
            this.bY = frameLayout;
            this.bZ = layoutInflater;
            this.ca = viewGroup;
            this.bX = bundle;
        }

        public void m629b(LifecycleDelegate lifecycleDelegate) {
            this.bY.removeAllViews();
            this.bY.addView(this.bU.bQ.onCreateView(this.bZ, this.ca, this.bX));
        }

        public int getState() {
            return 2;
        }
    }

    /* renamed from: com.google.android.gms.internal.bb.6 */
    class C11496 implements C0110a {
        final /* synthetic */ bb bU;

        C11496(bb bbVar) {
            this.bU = bbVar;
        }

        public void m630b(LifecycleDelegate lifecycleDelegate) {
            this.bU.bQ.onResume();
        }

        public int getState() {
            return 3;
        }
    }

    public bb() {
        this.bT = new C11451(this);
    }

    private void m170a(Bundle bundle, C0110a c0110a) {
        if (this.bQ != null) {
            c0110a.m166b(this.bQ);
            return;
        }
        if (this.bS == null) {
            this.bS = new LinkedList();
        }
        this.bS.add(c0110a);
        if (bundle != null) {
            if (this.bR == null) {
                this.bR = (Bundle) bundle.clone();
            } else {
                this.bR.putAll(bundle);
            }
        }
        m174a(this.bT);
    }

    private void m172u(int i) {
        while (!this.bS.isEmpty() && ((C0110a) this.bS.getLast()).getState() >= i) {
            this.bS.removeLast();
        }
    }

    public void m173a(FrameLayout frameLayout) {
        Context context = frameLayout.getContext();
        int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        CharSequence b = GooglePlayServicesUtil.m14b(context, isGooglePlayServicesAvailable, -1);
        CharSequence a = GooglePlayServicesUtil.m10a(context, isGooglePlayServicesAvailable);
        View linearLayout = new LinearLayout(frameLayout.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LayoutParams(-2, -2));
        frameLayout.addView(linearLayout);
        View textView = new TextView(frameLayout.getContext());
        textView.setLayoutParams(new LayoutParams(-2, -2));
        textView.setText(b);
        linearLayout.addView(textView);
        if (a != null) {
            View button = new Button(context);
            button.setLayoutParams(new LayoutParams(-2, -2));
            button.setText(a);
            linearLayout.addView(button);
            button.setOnClickListener(new C01095(this, context, isGooglePlayServicesAvailable));
        }
    }

    protected abstract void m174a(be<T> beVar);

    public T ag() {
        return this.bQ;
    }

    public void onCreate(Bundle savedInstanceState) {
        m170a(savedInstanceState, new C11473(this, savedInstanceState));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FrameLayout frameLayout = new FrameLayout(inflater.getContext());
        m170a(savedInstanceState, new C11484(this, frameLayout, inflater, container, savedInstanceState));
        if (this.bQ == null) {
            m173a(frameLayout);
        }
        return frameLayout;
    }

    public void onDestroy() {
        if (this.bQ != null) {
            this.bQ.onDestroy();
        } else {
            m172u(1);
        }
    }

    public void onDestroyView() {
        if (this.bQ != null) {
            this.bQ.onDestroyView();
        } else {
            m172u(2);
        }
    }

    public void onInflate(Activity activity, Bundle attrs, Bundle savedInstanceState) {
        m170a(savedInstanceState, new C11462(this, activity, attrs, savedInstanceState));
    }

    public void onLowMemory() {
        if (this.bQ != null) {
            this.bQ.onLowMemory();
        }
    }

    public void onPause() {
        if (this.bQ != null) {
            this.bQ.onPause();
        } else {
            m172u(3);
        }
    }

    public void onResume() {
        m170a(null, new C11496(this));
    }

    public void onSaveInstanceState(Bundle outState) {
        if (this.bQ != null) {
            this.bQ.onSaveInstanceState(outState);
        } else if (this.bR != null) {
            outState.putAll(this.bR);
        }
    }
}
