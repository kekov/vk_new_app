package com.google.android.gms.internal;

import android.os.Bundle;

public final class bs {
    private final Bundle dq;

    public bs(Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        this.dq = bundle;
    }

    public Bundle ar() {
        return this.dq;
    }
}
