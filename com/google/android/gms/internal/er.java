package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v4.media.TransportMediator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.eq.C1596a;
import com.google.android.gms.internal.eq.C1599b;
import com.google.android.gms.internal.eq.C1600c;
import com.google.android.gms.internal.eq.C1601d;
import com.google.android.gms.internal.eq.C1602e;
import com.google.android.gms.internal.eq.C1603g;
import com.google.android.gms.internal.eq.C1604h;
import com.google.android.gms.internal.eq.C1605i;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.providers.LangProvider.StringKeys;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.hockeyapp.android.Strings;

public class er implements Creator<eq> {
    static void m416a(eq eqVar, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        Set by = eqVar.by();
        if (by.contains(Integer.valueOf(1))) {
            ad.m106c(parcel, 1, eqVar.m1263u());
        }
        if (by.contains(Integer.valueOf(2))) {
            ad.m96a(parcel, 2, eqVar.getAboutMe(), true);
        }
        if (by.contains(Integer.valueOf(3))) {
            ad.m95a(parcel, 3, eqVar.bT(), i, true);
        }
        if (by.contains(Integer.valueOf(4))) {
            ad.m96a(parcel, 4, eqVar.getBirthday(), true);
        }
        if (by.contains(Integer.valueOf(5))) {
            ad.m96a(parcel, 5, eqVar.getBraggingRights(), true);
        }
        if (by.contains(Integer.valueOf(6))) {
            ad.m106c(parcel, 6, eqVar.getCircledByCount());
        }
        if (by.contains(Integer.valueOf(7))) {
            ad.m95a(parcel, 7, eqVar.bU(), i, true);
        }
        if (by.contains(Integer.valueOf(8))) {
            ad.m96a(parcel, 8, eqVar.getCurrentLocation(), true);
        }
        if (by.contains(Integer.valueOf(9))) {
            ad.m96a(parcel, 9, eqVar.getDisplayName(), true);
        }
        if (by.contains(Integer.valueOf(10))) {
            ad.m105b(parcel, 10, eqVar.bV(), true);
        }
        if (by.contains(Integer.valueOf(11))) {
            ad.m96a(parcel, 11, eqVar.bW(), true);
        }
        if (by.contains(Integer.valueOf(12))) {
            ad.m106c(parcel, 12, eqVar.getGender());
        }
        if (by.contains(Integer.valueOf(13))) {
            ad.m99a(parcel, 13, eqVar.isHasApp());
        }
        if (by.contains(Integer.valueOf(14))) {
            ad.m96a(parcel, 14, eqVar.getId(), true);
        }
        if (by.contains(Integer.valueOf(15))) {
            ad.m95a(parcel, 15, eqVar.bX(), i, true);
        }
        if (by.contains(Integer.valueOf(16))) {
            ad.m99a(parcel, 16, eqVar.isPlusUser());
        }
        if (by.contains(Integer.valueOf(19))) {
            ad.m95a(parcel, 19, eqVar.bY(), i, true);
        }
        if (by.contains(Integer.valueOf(18))) {
            ad.m96a(parcel, 18, eqVar.getLanguage(), true);
        }
        if (by.contains(Integer.valueOf(21))) {
            ad.m106c(parcel, 21, eqVar.getObjectType());
        }
        if (by.contains(Integer.valueOf(20))) {
            ad.m96a(parcel, 20, eqVar.getNickname(), true);
        }
        if (by.contains(Integer.valueOf(23))) {
            ad.m105b(parcel, 23, eqVar.ca(), true);
        }
        if (by.contains(Integer.valueOf(22))) {
            ad.m105b(parcel, 22, eqVar.bZ(), true);
        }
        if (by.contains(Integer.valueOf(25))) {
            ad.m106c(parcel, 25, eqVar.getRelationshipStatus());
        }
        if (by.contains(Integer.valueOf(24))) {
            ad.m106c(parcel, 24, eqVar.getPlusOneCount());
        }
        if (by.contains(Integer.valueOf(27))) {
            ad.m96a(parcel, 27, eqVar.getUrl(), true);
        }
        if (by.contains(Integer.valueOf(26))) {
            ad.m96a(parcel, 26, eqVar.getTagline(), true);
        }
        if (by.contains(Integer.valueOf(29))) {
            ad.m99a(parcel, 29, eqVar.isVerified());
        }
        if (by.contains(Integer.valueOf(28))) {
            ad.m105b(parcel, 28, eqVar.cb(), true);
        }
        ad.m87C(parcel, d);
    }

    public eq m417F(Parcel parcel) {
        int c = ac.m58c(parcel);
        Set hashSet = new HashSet();
        int i = 0;
        String str = null;
        C1596a c1596a = null;
        String str2 = null;
        String str3 = null;
        int i2 = 0;
        C1599b c1599b = null;
        String str4 = null;
        String str5 = null;
        List list = null;
        String str6 = null;
        int i3 = 0;
        boolean z = false;
        String str7 = null;
        C1601d c1601d = null;
        boolean z2 = false;
        String str8 = null;
        C1602e c1602e = null;
        String str9 = null;
        int i4 = 0;
        List list2 = null;
        List list3 = null;
        int i5 = 0;
        int i6 = 0;
        String str10 = null;
        String str11 = null;
        List list4 = null;
        boolean z3 = false;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    str = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(2));
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    C1596a c1596a2 = (C1596a) ac.m52a(parcel, b, C1596a.CREATOR);
                    hashSet.add(Integer.valueOf(3));
                    c1596a = c1596a2;
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    str2 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(4));
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    str3 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(5));
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    i2 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(6));
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    C1599b c1599b2 = (C1599b) ac.m52a(parcel, b, C1599b.CREATOR);
                    hashSet.add(Integer.valueOf(7));
                    c1599b = c1599b2;
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    str4 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(8));
                    break;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    str5 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(9));
                    break;
                case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                    list = ac.m59c(parcel, b, C1600c.CREATOR);
                    hashSet.add(Integer.valueOf(10));
                    break;
                case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                    str6 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(11));
                    break;
                case Strings.UPDATE_DIALOG_POSITIVE_BUTTON_ID /*12*/:
                    i3 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(12));
                    break;
                case Strings.EXPIRY_INFO_TITLE_ID /*13*/:
                    z = ac.m60c(parcel, b);
                    hashSet.add(Integer.valueOf(13));
                    break;
                case Strings.EXPIRY_INFO_TEXT_ID /*14*/:
                    str7 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(14));
                    break;
                case Strings.FEEDBACK_FAILED_TITLE_ID /*15*/:
                    C1601d c1601d2 = (C1601d) ac.m52a(parcel, b, C1601d.CREATOR);
                    hashSet.add(Integer.valueOf(15));
                    c1601d = c1601d2;
                    break;
                case TransportMediator.FLAG_KEY_MEDIA_PAUSE /*16*/:
                    z2 = ac.m60c(parcel, b);
                    hashSet.add(Integer.valueOf(16));
                    break;
                case StringKeys.SELECT_IMAGES /*18*/:
                    str8 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(18));
                    break;
                case StringKeys.SELECT_IMAGE /*19*/:
                    C1602e c1602e2 = (C1602e) ac.m52a(parcel, b, C1602e.CREATOR);
                    hashSet.add(Integer.valueOf(19));
                    c1602e = c1602e2;
                    break;
                case StringKeys.CAMERA_FOLDER /*20*/:
                    str9 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(20));
                    break;
                case C0436R.styleable.View_android_fitsSystemWindows /*21*/:
                    i4 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(21));
                    break;
                case C0436R.styleable.View_android_scrollbars /*22*/:
                    list2 = ac.m59c(parcel, b, C1603g.CREATOR);
                    hashSet.add(Integer.valueOf(22));
                    break;
                case C0436R.styleable.View_android_fadingEdge /*23*/:
                    list3 = ac.m59c(parcel, b, C1604h.CREATOR);
                    hashSet.add(Integer.valueOf(23));
                    break;
                case C0436R.styleable.View_android_fadingEdgeLength /*24*/:
                    i5 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(24));
                    break;
                case C0436R.styleable.View_android_nextFocusLeft /*25*/:
                    i6 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(25));
                    break;
                case C0436R.styleable.View_android_nextFocusRight /*26*/:
                    str10 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(26));
                    break;
                case C0436R.styleable.View_android_nextFocusUp /*27*/:
                    str11 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(27));
                    break;
                case C0436R.styleable.View_android_nextFocusDown /*28*/:
                    list4 = ac.m59c(parcel, b, C1605i.CREATOR);
                    hashSet.add(Integer.valueOf(28));
                    break;
                case C0436R.styleable.View_android_clickable /*29*/:
                    z3 = ac.m60c(parcel, b);
                    hashSet.add(Integer.valueOf(29));
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new eq(hashSet, i, str, c1596a, str2, str3, i2, c1599b, str4, str5, list, str6, i3, z, str7, c1601d, z2, str8, c1602e, str9, i4, list2, list3, i5, i6, str10, str11, list4, z3);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public eq[] m418Z(int i) {
        return new eq[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m417F(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m418Z(x0);
    }
}
