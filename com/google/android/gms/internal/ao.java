package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.an.C1142a;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class ao implements Creator<C1142a> {
    static void m137a(C1142a c1142a, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, c1142a.m595u());
        ad.m106c(parcel, 2, c1142a.m581E());
        ad.m99a(parcel, 3, c1142a.m584K());
        ad.m106c(parcel, 4, c1142a.m582F());
        ad.m99a(parcel, 5, c1142a.m585L());
        ad.m96a(parcel, 6, c1142a.m586M(), false);
        ad.m106c(parcel, 7, c1142a.m587N());
        ad.m96a(parcel, 8, c1142a.m589P(), false);
        ad.m95a(parcel, 9, c1142a.m591R(), i, false);
        ad.m87C(parcel, d);
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m138i(x0);
    }

    public C1142a m138i(Parcel parcel) {
        ai aiVar = null;
        int i = 0;
        int c = ac.m58c(parcel);
        String str = null;
        String str2 = null;
        boolean z = false;
        int i2 = 0;
        boolean z2 = false;
        int i3 = 0;
        int i4 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i4 = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    i3 = ac.m63f(parcel, b);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    z2 = ac.m60c(parcel, b);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    i2 = ac.m63f(parcel, b);
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    z = ac.m60c(parcel, b);
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    str2 = ac.m70l(parcel, b);
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    i = ac.m63f(parcel, b);
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    str = ac.m70l(parcel, b);
                    break;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    aiVar = (ai) ac.m52a(parcel, b, ai.CREATOR);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new C1142a(i4, i3, z2, i2, z, str2, i, str, aiVar);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m139o(x0);
    }

    public C1142a[] m139o(int i) {
        return new C1142a[i];
    }
}
