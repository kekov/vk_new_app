package com.google.android.gms.internal;

import android.os.Parcel;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class an {

    /* renamed from: com.google.android.gms.internal.an.b */
    public interface C0108b<I, O> {
        int m122E();

        int m123F();

        I m124e(O o);
    }

    /* renamed from: com.google.android.gms.internal.an.a */
    public static class C1142a<I, O> implements ae {
        public static final ao CREATOR;
        private final int f68T;
        protected final Class<? extends an> bA;
        protected final String bB;
        private aq bC;
        private C0108b<I, O> bD;
        protected final int bu;
        protected final boolean bv;
        protected final int bw;
        protected final boolean bx;
        protected final String by;
        protected final int bz;

        static {
            CREATOR = new ao();
        }

        C1142a(int i, int i2, boolean z, int i3, boolean z2, String str, int i4, String str2, ai aiVar) {
            this.f68T = i;
            this.bu = i2;
            this.bv = z;
            this.bw = i3;
            this.bx = z2;
            this.by = str;
            this.bz = i4;
            if (str2 == null) {
                this.bA = null;
                this.bB = null;
            } else {
                this.bA = at.class;
                this.bB = str2;
            }
            if (aiVar == null) {
                this.bD = null;
            } else {
                this.bD = aiVar.m562C();
            }
        }

        protected C1142a(int i, boolean z, int i2, boolean z2, String str, int i3, Class<? extends an> cls, C0108b<I, O> c0108b) {
            this.f68T = 1;
            this.bu = i;
            this.bv = z;
            this.bw = i2;
            this.bx = z2;
            this.by = str;
            this.bz = i3;
            this.bA = cls;
            if (cls == null) {
                this.bB = null;
            } else {
                this.bB = cls.getCanonicalName();
            }
            this.bD = c0108b;
        }

        public static C1142a m572a(String str, int i, C0108b<?, ?> c0108b, boolean z) {
            return new C1142a(c0108b.m122E(), z, c0108b.m123F(), false, str, i, null, c0108b);
        }

        public static <T extends an> C1142a<T, T> m573a(String str, int i, Class<T> cls) {
            return new C1142a(11, false, 11, false, str, i, cls, null);
        }

        public static <T extends an> C1142a<ArrayList<T>, ArrayList<T>> m574b(String str, int i, Class<T> cls) {
            return new C1142a(11, true, 11, true, str, i, cls, null);
        }

        public static C1142a<Integer, Integer> m575c(String str, int i) {
            return new C1142a(0, false, 0, false, str, i, null, null);
        }

        public static C1142a<Double, Double> m577d(String str, int i) {
            return new C1142a(4, false, 4, false, str, i, null, null);
        }

        public static C1142a<Boolean, Boolean> m578e(String str, int i) {
            return new C1142a(6, false, 6, false, str, i, null, null);
        }

        public static C1142a<String, String> m579f(String str, int i) {
            return new C1142a(7, false, 7, false, str, i, null, null);
        }

        public static C1142a<ArrayList<String>, ArrayList<String>> m580g(String str, int i) {
            return new C1142a(7, true, 7, true, str, i, null, null);
        }

        public int m581E() {
            return this.bu;
        }

        public int m582F() {
            return this.bw;
        }

        public C1142a<I, O> m583J() {
            return new C1142a(this.f68T, this.bu, this.bv, this.bw, this.bx, this.by, this.bz, this.bB, m591R());
        }

        public boolean m584K() {
            return this.bv;
        }

        public boolean m585L() {
            return this.bx;
        }

        public String m586M() {
            return this.by;
        }

        public int m587N() {
            return this.bz;
        }

        public Class<? extends an> m588O() {
            return this.bA;
        }

        String m589P() {
            return this.bB == null ? null : this.bB;
        }

        public boolean m590Q() {
            return this.bD != null;
        }

        ai m591R() {
            return this.bD == null ? null : ai.m560a(this.bD);
        }

        public HashMap<String, C1142a<?, ?>> m592S() {
            C0142x.m495d(this.bB);
            C0142x.m495d(this.bC);
            return this.bC.m605n(this.bB);
        }

        public void m593a(aq aqVar) {
            this.bC = aqVar;
        }

        public int describeContents() {
            ao aoVar = CREATOR;
            return 0;
        }

        public I m594e(O o) {
            return this.bD.m124e(o);
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Field\n");
            stringBuilder.append("            versionCode=").append(this.f68T).append('\n');
            stringBuilder.append("                 typeIn=").append(this.bu).append('\n');
            stringBuilder.append("            typeInArray=").append(this.bv).append('\n');
            stringBuilder.append("                typeOut=").append(this.bw).append('\n');
            stringBuilder.append("           typeOutArray=").append(this.bx).append('\n');
            stringBuilder.append("        outputFieldName=").append(this.by).append('\n');
            stringBuilder.append("      safeParcelFieldId=").append(this.bz).append('\n');
            stringBuilder.append("       concreteTypeName=").append(m589P()).append('\n');
            if (m588O() != null) {
                stringBuilder.append("     concreteType.class=").append(m588O().getCanonicalName()).append('\n');
            }
            stringBuilder.append("          converterName=").append(this.bD == null ? "null" : this.bD.getClass().getCanonicalName()).append('\n');
            return stringBuilder.toString();
        }

        public int m595u() {
            return this.f68T;
        }

        public void writeToParcel(Parcel out, int flags) {
            ao aoVar = CREATOR;
            ao.m137a(this, out, flags);
        }
    }

    private void m125a(StringBuilder stringBuilder, C1142a c1142a, Object obj) {
        if (c1142a.m581E() == 11) {
            stringBuilder.append(((an) c1142a.m588O().cast(obj)).toString());
        } else if (c1142a.m581E() == 7) {
            stringBuilder.append("\"");
            stringBuilder.append(ay.m163o((String) obj));
            stringBuilder.append("\"");
        } else {
            stringBuilder.append(obj);
        }
    }

    private void m126a(StringBuilder stringBuilder, C1142a c1142a, ArrayList<Object> arrayList) {
        stringBuilder.append("[");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                stringBuilder.append(",");
            }
            Object obj = arrayList.get(i);
            if (obj != null) {
                m125a(stringBuilder, c1142a, obj);
            }
        }
        stringBuilder.append("]");
    }

    public abstract HashMap<String, C1142a<?, ?>> m127G();

    public HashMap<String, Object> m128H() {
        return null;
    }

    public HashMap<String, Object> m129I() {
        return null;
    }

    protected <O, I> I m130a(C1142a<I, O> c1142a, Object obj) {
        return c1142a.bD != null ? c1142a.m594e(obj) : obj;
    }

    protected boolean m131a(C1142a c1142a) {
        return c1142a.m582F() == 11 ? c1142a.m585L() ? m136m(c1142a.m586M()) : m135l(c1142a.m586M()) : m134k(c1142a.m586M());
    }

    protected Object m132b(C1142a c1142a) {
        boolean z = true;
        String M = c1142a.m586M();
        if (c1142a.m588O() == null) {
            return m133j(c1142a.m586M());
        }
        if (m133j(c1142a.m586M()) != null) {
            z = false;
        }
        C0142x.m490a(z, "Concrete field shouldn't be value object: " + c1142a.m586M());
        Map I = c1142a.m585L() ? m129I() : m128H();
        if (I != null) {
            return I.get(M);
        }
        try {
            return getClass().getMethod("get" + Character.toUpperCase(M.charAt(0)) + M.substring(1), new Class[0]).invoke(this, new Object[0]);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    protected abstract Object m133j(String str);

    protected abstract boolean m134k(String str);

    protected boolean m135l(String str) {
        throw new UnsupportedOperationException("Concrete types not supported");
    }

    protected boolean m136m(String str) {
        throw new UnsupportedOperationException("Concrete type arrays not supported");
    }

    public String toString() {
        HashMap G = m127G();
        StringBuilder stringBuilder = new StringBuilder(100);
        for (String str : G.keySet()) {
            C1142a c1142a = (C1142a) G.get(str);
            if (m131a(c1142a)) {
                Object a = m130a(c1142a, m132b(c1142a));
                if (stringBuilder.length() == 0) {
                    stringBuilder.append("{");
                } else {
                    stringBuilder.append(",");
                }
                stringBuilder.append("\"").append(str).append("\":");
                if (a != null) {
                    switch (c1142a.m582F()) {
                        case UserListView.TYPE_BLACKLIST /*8*/:
                            stringBuilder.append("\"").append(aw.m160a((byte[]) a)).append("\"");
                            break;
                        case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                            stringBuilder.append("\"").append(aw.m161b((byte[]) a)).append("\"");
                            break;
                        case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                            az.m164a(stringBuilder, (HashMap) a);
                            break;
                        default:
                            if (!c1142a.m584K()) {
                                m125a(stringBuilder, c1142a, a);
                                break;
                            }
                            m126a(stringBuilder, c1142a, (ArrayList) a);
                            break;
                    }
                }
                stringBuilder.append("null");
            }
        }
        if (stringBuilder.length() > 0) {
            stringBuilder.append("}");
        } else {
            stringBuilder.append("{}");
        }
        return stringBuilder.toString();
    }
}
