package com.google.android.gms.internal;

import com.google.android.gms.common.data.DataBuffer;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.internal.m */
public abstract class C1231m<T> extends DataBuffer<T> {
    private boolean ag;
    private ArrayList<Integer> ah;

    public C1231m(C1230k c1230k) {
        super(c1230k);
        this.ag = false;
    }

    private int m926g(int i) {
        boolean z = i >= 0 && i < this.ah.size();
        C0126n.m443a(z, "Position " + i + " is out of bounds for this buffer");
        return ((Integer) this.ah.get(i)).intValue();
    }

    private int m927h(int i) {
        return (i < 0 || i == this.ah.size()) ? 0 : i == this.ah.size() + -1 ? this.O.getCount() - ((Integer) this.ah.get(i)).intValue() : ((Integer) this.ah.get(i + 1)).intValue() - ((Integer) this.ah.get(i)).intValue();
    }

    private void m928i() {
        synchronized (this) {
            if (!this.ag) {
                int count = this.O.getCount();
                this.ah = new ArrayList();
                if (count > 0) {
                    this.ah.add(Integer.valueOf(0));
                    String primaryDataMarkerColumn = getPrimaryDataMarkerColumn();
                    String c = this.O.m918c(primaryDataMarkerColumn, 0, this.O.m919d(0));
                    int i = 1;
                    while (i < count) {
                        String c2 = this.O.m918c(primaryDataMarkerColumn, i, this.O.m919d(i));
                        if (c2.equals(c)) {
                            c2 = c;
                        } else {
                            this.ah.add(Integer.valueOf(i));
                        }
                        i++;
                        c = c2;
                    }
                }
                this.ag = true;
            }
        }
    }

    protected abstract T m929a(int i, int i2);

    public final T get(int position) {
        m928i();
        return m929a(m926g(position), m927h(position));
    }

    public int getCount() {
        m928i();
        return this.ah.size();
    }

    protected abstract String getPrimaryDataMarkerColumn();
}
