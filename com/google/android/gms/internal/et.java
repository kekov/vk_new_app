package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.eq.C1604h;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashSet;
import java.util.Set;

public class et implements Creator<C1604h> {
    static void m419a(C1604h c1604h, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        Set by = c1604h.by();
        if (by.contains(Integer.valueOf(1))) {
            ad.m106c(parcel, 1, c1604h.m1250u());
        }
        if (by.contains(Integer.valueOf(2))) {
            ad.m99a(parcel, 2, c1604h.isPrimary());
        }
        if (by.contains(Integer.valueOf(3))) {
            ad.m96a(parcel, 3, c1604h.getValue(), true);
        }
        ad.m87C(parcel, d);
    }

    public C1604h m420G(Parcel parcel) {
        boolean z = false;
        int c = ac.m58c(parcel);
        Set hashSet = new HashSet();
        String str = null;
        int i = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    z = ac.m60c(parcel, b);
                    hashSet.add(Integer.valueOf(2));
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    str = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(3));
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new C1604h(hashSet, i, z, str);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public C1604h[] aa(int i) {
        return new C1604h[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m420G(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return aa(x0);
    }
}
