package com.google.android.gms.internal;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.widget.ImageView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusClient.C0147a;

public class dt extends ImageView implements ConnectionCallbacks, C0147a {
    private int gU;
    private boolean gV;
    private boolean gW;
    private Bitmap gX;
    private PlusClient gY;
    private Uri mUri;

    /* renamed from: com.google.android.gms.internal.dt.a */
    class C0116a extends AsyncTask<ParcelFileDescriptor, Void, Bitmap> {
        private final int gU;
        final /* synthetic */ dt gZ;

        C0116a(dt dtVar, int i) {
            this.gZ = dtVar;
            this.gU = i;
        }

        protected Bitmap m344a(ParcelFileDescriptor... parcelFileDescriptorArr) {
            ParcelFileDescriptor parcelFileDescriptor = parcelFileDescriptorArr[0];
            try {
                Bitmap decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
                if (this.gU > 0) {
                    decodeFileDescriptor = dt.m825a(decodeFileDescriptor, this.gU);
                } else {
                    try {
                        parcelFileDescriptor.close();
                    } catch (Throwable e) {
                        Log.e("PlusImageView", "closed failed", e);
                    }
                }
                return decodeFileDescriptor;
            } finally {
                try {
                    parcelFileDescriptor.close();
                } catch (Throwable e2) {
                    Log.e("PlusImageView", "closed failed", e2);
                }
            }
        }

        protected void m345b(Bitmap bitmap) {
            this.gZ.gX = bitmap;
            if (this.gZ.gV) {
                this.gZ.setImageBitmap(this.gZ.gX);
            }
        }

        protected /* synthetic */ Object doInBackground(Object[] x0) {
            return m344a((ParcelFileDescriptor[]) x0);
        }

        protected /* synthetic */ void onPostExecute(Object x0) {
            m345b((Bitmap) x0);
        }
    }

    public dt(Context context) {
        super(context);
    }

    private static Bitmap m825a(Bitmap bitmap, int i) {
        double width = (double) bitmap.getWidth();
        double height = (double) bitmap.getHeight();
        double d = width > height ? ((double) i) / width : ((double) i) / height;
        return Bitmap.createScaledBitmap(bitmap, (int) ((width * d) + 0.5d), (int) ((d * height) + 0.5d), true);
    }

    private void bc() {
        boolean z = this.mUri != null && "android.resource".equals(this.mUri.getScheme());
        if (!this.gW) {
            return;
        }
        if (this.mUri == null) {
            setImageBitmap(null);
        } else if (z || (this.gY != null && this.gY.isConnected())) {
            if (z) {
                setImageURI(this.mUri);
            } else {
                this.gY.m1014a(this, this.mUri, this.gU);
            }
            this.gW = false;
        }
    }

    public void m830a(Uri uri, int i) {
        boolean z = false;
        boolean equals = this.mUri == null ? uri == null : this.mUri.equals(uri);
        if (this.gU == i) {
            z = true;
        }
        if (!equals || !r2) {
            this.mUri = uri;
            this.gU = i;
            this.gW = true;
            bc();
        }
    }

    public void m831a(ConnectionResult connectionResult, ParcelFileDescriptor parcelFileDescriptor) {
        if (connectionResult.isSuccess()) {
            this.gW = false;
            if (parcelFileDescriptor != null) {
                new C0116a(this, this.gU).execute(new ParcelFileDescriptor[]{parcelFileDescriptor});
            }
        }
    }

    public void m832a(PlusClient plusClient) {
        if (plusClient != this.gY) {
            if (this.gY != null && this.gY.isConnectionCallbacksRegistered(this)) {
                this.gY.unregisterConnectionCallbacks(this);
            }
            this.gY = plusClient;
            this.gY.registerConnectionCallbacks(this);
        }
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.gV = true;
        if (!(this.gY == null || this.gY.isConnectionCallbacksRegistered(this))) {
            this.gY.registerConnectionCallbacks(this);
        }
        if (this.gX != null) {
            setImageBitmap(this.gX);
        }
    }

    public void onConnected(Bundle connectionHint) {
        bc();
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.gV = false;
        if (this.gY != null && this.gY.isConnectionCallbacksRegistered(this)) {
            this.gY.unregisterConnectionCallbacks(this);
        }
    }

    public void onDisconnected() {
    }
}
