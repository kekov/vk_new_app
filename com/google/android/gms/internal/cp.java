package com.google.android.gms.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.bc.C1151a;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public interface cp extends IInterface {

    /* renamed from: com.google.android.gms.internal.cp.a */
    public static abstract class C1182a extends Binder implements cp {

        /* renamed from: com.google.android.gms.internal.cp.a.a */
        static class C1181a implements cp {
            private IBinder f89a;

            C1181a(IBinder iBinder) {
                this.f89a = iBinder;
            }

            public IBinder asBinder() {
                return this.f89a;
            }

            public void m785e(bc bcVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.internal.IOnLocationChangeListener");
                    obtain.writeStrongBinder(bcVar != null ? bcVar.asBinder() : null);
                    this.f89a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public static cp m786B(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.internal.IOnLocationChangeListener");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof cp)) ? new C1181a(iBinder) : (cp) queryLocalInterface;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    data.enforceInterface("com.google.android.gms.maps.internal.IOnLocationChangeListener");
                    m305e(C1151a.m631j(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.maps.internal.IOnLocationChangeListener");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void m305e(bc bcVar) throws RemoteException;
}
