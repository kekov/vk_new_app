package com.google.android.gms.internal;

import android.os.IBinder;
import com.google.android.gms.internal.bc.C1151a;
import java.lang.reflect.Field;

public final class bd<T> extends C1151a {
    private final T cd;

    private bd(T t) {
        this.cd = t;
    }

    public static <T> T m1048a(bc bcVar) {
        if (bcVar instanceof bd) {
            return ((bd) bcVar).cd;
        }
        IBinder asBinder = bcVar.asBinder();
        Field[] declaredFields = asBinder.getClass().getDeclaredFields();
        if (declaredFields.length == 1) {
            Field field = declaredFields[0];
            if (field.isAccessible()) {
                throw new IllegalArgumentException("The concrete class implementing IObjectWrapper must have exactly one declared *private* field for the wrapped object. Preferably, this is an instance of the ObjectWrapper<T> class.");
            }
            field.setAccessible(true);
            try {
                return field.get(asBinder);
            } catch (Throwable e) {
                throw new IllegalArgumentException("Binder object is null.", e);
            } catch (Throwable e2) {
                throw new IllegalArgumentException("remoteBinder is the wrong class.", e2);
            } catch (Throwable e22) {
                throw new IllegalArgumentException("Could not access the field in remoteBinder.", e22);
            }
        }
        throw new IllegalArgumentException("The concrete class implementing IObjectWrapper must have exactly *one* declared private field for the wrapped object.  Preferably, this is an instance of the ObjectWrapper<T> class.");
    }

    public static <T> bc m1049f(T t) {
        return new bd(t);
    }
}
