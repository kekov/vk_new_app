package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.internal.an.C1142a;
import java.util.ArrayList;
import java.util.HashMap;

public class aq implements ae {
    public static final ar CREATOR;
    private final int f69T;
    private final HashMap<String, HashMap<String, C1142a<?, ?>>> bE;
    private final ArrayList<C1143a> bF;
    private final String bG;

    /* renamed from: com.google.android.gms.internal.aq.a */
    public static class C1143a implements ae {
        public static final as CREATOR;
        final ArrayList<C1144b> bH;
        final String className;
        final int versionCode;

        static {
            CREATOR = new as();
        }

        C1143a(int i, String str, ArrayList<C1144b> arrayList) {
            this.versionCode = i;
            this.className = str;
            this.bH = arrayList;
        }

        C1143a(String str, HashMap<String, C1142a<?, ?>> hashMap) {
            this.versionCode = 1;
            this.className = str;
            this.bH = C1143a.m596a(hashMap);
        }

        private static ArrayList<C1144b> m596a(HashMap<String, C1142a<?, ?>> hashMap) {
            if (hashMap == null) {
                return null;
            }
            ArrayList<C1144b> arrayList = new ArrayList();
            for (String str : hashMap.keySet()) {
                arrayList.add(new C1144b(str, (C1142a) hashMap.get(str)));
            }
            return arrayList;
        }

        HashMap<String, C1142a<?, ?>> m597X() {
            HashMap<String, C1142a<?, ?>> hashMap = new HashMap();
            int size = this.bH.size();
            for (int i = 0; i < size; i++) {
                C1144b c1144b = (C1144b) this.bH.get(i);
                hashMap.put(c1144b.bI, c1144b.bJ);
            }
            return hashMap;
        }

        public int describeContents() {
            as asVar = CREATOR;
            return 0;
        }

        public void writeToParcel(Parcel out, int flags) {
            as asVar = CREATOR;
            as.m146a(this, out, flags);
        }
    }

    /* renamed from: com.google.android.gms.internal.aq.b */
    public static class C1144b implements ae {
        public static final ap CREATOR;
        final String bI;
        final C1142a<?, ?> bJ;
        final int versionCode;

        static {
            CREATOR = new ap();
        }

        C1144b(int i, String str, C1142a<?, ?> c1142a) {
            this.versionCode = i;
            this.bI = str;
            this.bJ = c1142a;
        }

        C1144b(String str, C1142a<?, ?> c1142a) {
            this.versionCode = 1;
            this.bI = str;
            this.bJ = c1142a;
        }

        public int describeContents() {
            ap apVar = CREATOR;
            return 0;
        }

        public void writeToParcel(Parcel out, int flags) {
            ap apVar = CREATOR;
            ap.m140a(this, out, flags);
        }
    }

    static {
        CREATOR = new ar();
    }

    aq(int i, ArrayList<C1143a> arrayList, String str) {
        this.f69T = i;
        this.bF = null;
        this.bE = m598b(arrayList);
        this.bG = (String) C0142x.m495d(str);
        m599T();
    }

    public aq(Class<? extends an> cls) {
        this.f69T = 1;
        this.bF = null;
        this.bE = new HashMap();
        this.bG = cls.getCanonicalName();
    }

    private static HashMap<String, HashMap<String, C1142a<?, ?>>> m598b(ArrayList<C1143a> arrayList) {
        HashMap<String, HashMap<String, C1142a<?, ?>>> hashMap = new HashMap();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            C1143a c1143a = (C1143a) arrayList.get(i);
            hashMap.put(c1143a.className, c1143a.m597X());
        }
        return hashMap;
    }

    public void m599T() {
        for (String str : this.bE.keySet()) {
            HashMap hashMap = (HashMap) this.bE.get(str);
            for (String str2 : hashMap.keySet()) {
                ((C1142a) hashMap.get(str2)).m593a(this);
            }
        }
    }

    public void m600U() {
        for (String str : this.bE.keySet()) {
            HashMap hashMap = (HashMap) this.bE.get(str);
            HashMap hashMap2 = new HashMap();
            for (String str2 : hashMap.keySet()) {
                hashMap2.put(str2, ((C1142a) hashMap.get(str2)).m583J());
            }
            this.bE.put(str, hashMap2);
        }
    }

    ArrayList<C1143a> m601V() {
        ArrayList<C1143a> arrayList = new ArrayList();
        for (String str : this.bE.keySet()) {
            arrayList.add(new C1143a(str, (HashMap) this.bE.get(str)));
        }
        return arrayList;
    }

    public String m602W() {
        return this.bG;
    }

    public void m603a(Class<? extends an> cls, HashMap<String, C1142a<?, ?>> hashMap) {
        this.bE.put(cls.getCanonicalName(), hashMap);
    }

    public boolean m604a(Class<? extends an> cls) {
        return this.bE.containsKey(cls.getCanonicalName());
    }

    public int describeContents() {
        ar arVar = CREATOR;
        return 0;
    }

    public HashMap<String, C1142a<?, ?>> m605n(String str) {
        return (HashMap) this.bE.get(str);
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (String str : this.bE.keySet()) {
            stringBuilder.append(str).append(":\n");
            HashMap hashMap = (HashMap) this.bE.get(str);
            for (String str2 : hashMap.keySet()) {
                stringBuilder.append("  ").append(str2).append(": ");
                stringBuilder.append(hashMap.get(str2));
            }
        }
        return stringBuilder.toString();
    }

    int m606u() {
        return this.f69T;
    }

    public void writeToParcel(Parcel out, int flags) {
        ar arVar = CREATOR;
        ar.m143a(this, out, flags);
    }
}
