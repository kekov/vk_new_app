package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ak.C1141a;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.ArrayList;

public class al implements Creator<ak> {
    static void m116a(ak akVar, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, akVar.m571u());
        ad.m105b(parcel, 2, akVar.m565D(), false);
        ad.m87C(parcel, d);
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m117g(x0);
    }

    public ak m117g(Parcel parcel) {
        int c = ac.m58c(parcel);
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    arrayList = ac.m59c(parcel, b, C1141a.CREATOR);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new ak(i, arrayList);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public ak[] m118m(int i) {
        return new ak[i];
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m118m(x0);
    }
}
