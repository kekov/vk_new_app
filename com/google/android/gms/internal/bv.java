package com.google.android.gms.internal;

import com.google.android.gms.games.leaderboard.LeaderboardVariant;

public final class bv extends C0123j implements LeaderboardVariant {
    bv(C1230k c1230k, int i) {
        super(c1230k, i);
    }

    public String at() {
        return getString("top_page_token_next");
    }

    public String au() {
        return getString("window_page_token_prev");
    }

    public String av() {
        return getString("window_page_token_next");
    }

    public int getCollection() {
        return getInteger("collection");
    }

    public String getDisplayPlayerRank() {
        return getString("player_display_rank");
    }

    public String getDisplayPlayerScore() {
        return getString("player_display_score");
    }

    public long getNumScores() {
        return m436d("total_scores") ? -1 : getLong("total_scores");
    }

    public long getPlayerRank() {
        return m436d("player_rank") ? -1 : getLong("player_rank");
    }

    public long getRawPlayerScore() {
        return m436d("player_raw_score") ? -1 : getLong("player_raw_score");
    }

    public int getTimeSpan() {
        return getInteger("timespan");
    }

    public boolean hasPlayerInfo() {
        return !m436d("player_raw_score");
    }

    public String toString() {
        return C0141w.m488c(this).m486a("TimeSpan", bq.m280B(getTimeSpan())).m486a("Collection", bp.m279B(getCollection())).m486a("RawPlayerScore", hasPlayerInfo() ? Long.valueOf(getRawPlayerScore()) : "none").m486a("DisplayPlayerScore", hasPlayerInfo() ? getDisplayPlayerScore() : "none").m486a("PlayerRank", hasPlayerInfo() ? Long.valueOf(getPlayerRank()) : "none").m486a("DisplayPlayerRank", hasPlayerInfo() ? getDisplayPlayerRank() : "none").m486a("NumScores", Long.valueOf(getNumScores())).m486a("TopPageNextToken", at()).m486a("WindowPageNextToken", av()).m486a("WindowPagePrevToken", au()).toString();
    }
}
