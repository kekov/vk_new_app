package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.database.CursorIndexOutOfBoundsException;
import android.database.CursorWindow;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.k */
public final class C1230k implements ae {
    public static final C0125l CREATOR;
    private static final C0124a aa;
    int f114T;
    String[] f115U;
    Bundle f116V;
    CursorWindow[] f117W;
    Bundle f118X;
    int[] f119Y;
    int f120Z;
    boolean mClosed;
    int f121p;

    /* renamed from: com.google.android.gms.internal.k.a */
    public static class C0124a {
        private final String[] f37U;
        private final ArrayList<HashMap<String, Object>> ab;
        private final String ac;
        private final HashMap<Object, Integer> ad;
        private boolean ae;
        private String af;

        private C0124a(String[] strArr, String str) {
            this.f37U = (String[]) C0142x.m495d(strArr);
            this.ab = new ArrayList();
            this.ac = str;
            this.ad = new HashMap();
            this.ae = false;
            this.af = null;
        }
    }

    /* renamed from: com.google.android.gms.internal.k.1 */
    static class C12291 extends C0124a {
        C12291(String[] strArr, String str) {
            super(str, null);
        }
    }

    static {
        CREATOR = new C0125l();
        aa = new C12291(new String[0], null);
    }

    C1230k() {
        this.mClosed = false;
    }

    private C1230k(C0124a c0124a, int i, Bundle bundle) {
        this(c0124a.f37U, C1230k.m913a(c0124a), i, bundle);
    }

    public C1230k(String[] strArr, CursorWindow[] cursorWindowArr, int i, Bundle bundle) {
        this.mClosed = false;
        this.f114T = 1;
        this.f115U = (String[]) C0142x.m495d(strArr);
        this.f117W = (CursorWindow[]) C0142x.m495d(cursorWindowArr);
        this.f121p = i;
        this.f118X = bundle;
        m923g();
    }

    public static C1230k m911a(int i, Bundle bundle) {
        return new C1230k(aa, i, bundle);
    }

    private void m912a(String str, int i) {
        if (this.f116V == null || !this.f116V.containsKey(str)) {
            throw new IllegalArgumentException("No such column: " + str);
        } else if (isClosed()) {
            throw new IllegalArgumentException("Buffer is closed.");
        } else if (i < 0 || i >= this.f120Z) {
            throw new CursorIndexOutOfBoundsException(i, this.f120Z);
        }
    }

    private static CursorWindow[] m913a(C0124a c0124a) {
        if (c0124a.f37U.length == 0) {
            return new CursorWindow[0];
        }
        ArrayList c = c0124a.ab;
        int size = c.size();
        CursorWindow cursorWindow = new CursorWindow(false);
        CursorWindow[] cursorWindowArr = new CursorWindow[]{cursorWindow};
        cursorWindow.setNumColumns(c0124a.f37U.length);
        int i = 0;
        while (i < size) {
            try {
                if (cursorWindow.allocRow()) {
                    Map map = (Map) c.get(i);
                    for (int i2 = 0; i2 < c0124a.f37U.length; i2++) {
                        String str = c0124a.f37U[i2];
                        Object obj = map.get(str);
                        if (obj == null) {
                            cursorWindow.putNull(i, i2);
                        } else if (obj instanceof String) {
                            cursorWindow.putString((String) obj, i, i2);
                        } else if (obj instanceof Long) {
                            cursorWindow.putLong(((Long) obj).longValue(), i, i2);
                        } else if (obj instanceof Integer) {
                            cursorWindow.putLong((long) ((Integer) obj).intValue(), i, i2);
                        } else if (obj instanceof Boolean) {
                            cursorWindow.putLong(((Boolean) obj).booleanValue() ? 1 : 0, i, i2);
                        } else if (obj instanceof byte[]) {
                            cursorWindow.putBlob((byte[]) obj, i, i2);
                        } else {
                            throw new IllegalArgumentException("Unsupported object for column " + str + ": " + obj);
                        }
                    }
                    i++;
                } else {
                    throw new RuntimeException("Cursor window out of memory");
                }
            } catch (RuntimeException e) {
                cursorWindow.close();
                throw e;
            }
        }
        return cursorWindowArr;
    }

    public static C1230k m914e(int i) {
        return C1230k.m911a(i, null);
    }

    public long m915a(String str, int i, int i2) {
        m912a(str, i);
        return this.f117W[i2].getLong(i - this.f119Y[i2], this.f116V.getInt(str));
    }

    public void m916a(String str, int i, int i2, CharArrayBuffer charArrayBuffer) {
        m912a(str, i);
        this.f117W[i2].copyStringToBuffer(i - this.f119Y[i2], this.f116V.getInt(str), charArrayBuffer);
    }

    public int m917b(String str, int i, int i2) {
        m912a(str, i);
        return this.f117W[i2].getInt(i - this.f119Y[i2], this.f116V.getInt(str));
    }

    public String m918c(String str, int i, int i2) {
        m912a(str, i);
        return this.f117W[i2].getString(i - this.f119Y[i2], this.f116V.getInt(str));
    }

    public void close() {
        synchronized (this) {
            if (!this.mClosed) {
                this.mClosed = true;
                for (CursorWindow close : this.f117W) {
                    close.close();
                }
            }
        }
    }

    public int m919d(int i) {
        int i2 = 0;
        boolean z = i >= 0 && i < this.f120Z;
        C0142x.m489a(z);
        while (i2 < this.f119Y.length) {
            if (i < this.f119Y[i2]) {
                i2--;
                break;
            }
            i2++;
        }
        return i2 == this.f119Y.length ? i2 - 1 : i2;
    }

    public boolean m920d(String str, int i, int i2) {
        m912a(str, i);
        return Long.valueOf(this.f117W[i2].getLong(i - this.f119Y[i2], this.f116V.getInt(str))).longValue() == 1;
    }

    public int describeContents() {
        C0125l c0125l = CREATOR;
        return 0;
    }

    public byte[] m921e(String str, int i, int i2) {
        m912a(str, i);
        return this.f117W[i2].getBlob(i - this.f119Y[i2], this.f116V.getInt(str));
    }

    public Uri m922f(String str, int i, int i2) {
        String c = m918c(str, i, i2);
        return c == null ? null : Uri.parse(c);
    }

    public void m923g() {
        int i;
        int i2 = 0;
        this.f116V = new Bundle();
        for (i = 0; i < this.f115U.length; i++) {
            this.f116V.putInt(this.f115U[i], i);
        }
        this.f119Y = new int[this.f117W.length];
        i = 0;
        while (i2 < this.f117W.length) {
            this.f119Y[i2] = i;
            i += this.f117W[i2].getNumRows();
            i2++;
        }
        this.f120Z = i;
    }

    public boolean m924g(String str, int i, int i2) {
        m912a(str, i);
        return this.f117W[i2].isNull(i - this.f119Y[i2], this.f116V.getInt(str));
    }

    public int getCount() {
        return this.f120Z;
    }

    public int getStatusCode() {
        return this.f121p;
    }

    public Bundle m925h() {
        return this.f118X;
    }

    public boolean isClosed() {
        boolean z;
        synchronized (this) {
            z = this.mClosed;
        }
        return z;
    }

    public void writeToParcel(Parcel dest, int flags) {
        C0125l c0125l = CREATOR;
        C0125l.m439a(this, dest, flags);
    }
}
