package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.support.v4.media.TransportMediator;
import com.google.android.gms.internal.ac.C0107a;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.cache.CacheTables;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.providers.LangProvider.StringKeys;
import com.vkontakte.android.mediapicker.ui.GalleryPickerHeaderView;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.hockeyapp.android.Strings;

public class ee implements Creator<ed> {
    static void m385a(ed edVar, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        Set by = edVar.by();
        if (by.contains(Integer.valueOf(1))) {
            ad.m106c(parcel, 1, edVar.m1190u());
        }
        if (by.contains(Integer.valueOf(2))) {
            ad.m95a(parcel, 2, edVar.bz(), i, true);
        }
        if (by.contains(Integer.valueOf(3))) {
            ad.m97a(parcel, 3, edVar.getAdditionalName(), true);
        }
        if (by.contains(Integer.valueOf(4))) {
            ad.m95a(parcel, 4, edVar.bA(), i, true);
        }
        if (by.contains(Integer.valueOf(5))) {
            ad.m96a(parcel, 5, edVar.getAddressCountry(), true);
        }
        if (by.contains(Integer.valueOf(6))) {
            ad.m96a(parcel, 6, edVar.getAddressLocality(), true);
        }
        if (by.contains(Integer.valueOf(7))) {
            ad.m96a(parcel, 7, edVar.getAddressRegion(), true);
        }
        if (by.contains(Integer.valueOf(8))) {
            ad.m105b(parcel, 8, edVar.bB(), true);
        }
        if (by.contains(Integer.valueOf(9))) {
            ad.m106c(parcel, 9, edVar.getAttendeeCount());
        }
        if (by.contains(Integer.valueOf(10))) {
            ad.m105b(parcel, 10, edVar.bC(), true);
        }
        if (by.contains(Integer.valueOf(11))) {
            ad.m95a(parcel, 11, edVar.bD(), i, true);
        }
        if (by.contains(Integer.valueOf(12))) {
            ad.m105b(parcel, 12, edVar.bE(), true);
        }
        if (by.contains(Integer.valueOf(13))) {
            ad.m96a(parcel, 13, edVar.getBestRating(), true);
        }
        if (by.contains(Integer.valueOf(14))) {
            ad.m96a(parcel, 14, edVar.getBirthDate(), true);
        }
        if (by.contains(Integer.valueOf(15))) {
            ad.m95a(parcel, 15, edVar.bF(), i, true);
        }
        if (by.contains(Integer.valueOf(17))) {
            ad.m96a(parcel, 17, edVar.getContentSize(), true);
        }
        if (by.contains(Integer.valueOf(16))) {
            ad.m96a(parcel, 16, edVar.getCaption(), true);
        }
        if (by.contains(Integer.valueOf(19))) {
            ad.m105b(parcel, 19, edVar.bG(), true);
        }
        if (by.contains(Integer.valueOf(18))) {
            ad.m96a(parcel, 18, edVar.getContentUrl(), true);
        }
        if (by.contains(Integer.valueOf(21))) {
            ad.m96a(parcel, 21, edVar.getDateModified(), true);
        }
        if (by.contains(Integer.valueOf(20))) {
            ad.m96a(parcel, 20, edVar.getDateCreated(), true);
        }
        if (by.contains(Integer.valueOf(23))) {
            ad.m96a(parcel, 23, edVar.getDescription(), true);
        }
        if (by.contains(Integer.valueOf(22))) {
            ad.m96a(parcel, 22, edVar.getDatePublished(), true);
        }
        if (by.contains(Integer.valueOf(25))) {
            ad.m96a(parcel, 25, edVar.getEmbedUrl(), true);
        }
        if (by.contains(Integer.valueOf(24))) {
            ad.m96a(parcel, 24, edVar.getDuration(), true);
        }
        if (by.contains(Integer.valueOf(27))) {
            ad.m96a(parcel, 27, edVar.getFamilyName(), true);
        }
        if (by.contains(Integer.valueOf(26))) {
            ad.m96a(parcel, 26, edVar.getEndDate(), true);
        }
        if (by.contains(Integer.valueOf(29))) {
            ad.m95a(parcel, 29, edVar.bH(), i, true);
        }
        if (by.contains(Integer.valueOf(28))) {
            ad.m96a(parcel, 28, edVar.getGender(), true);
        }
        if (by.contains(Integer.valueOf(31))) {
            ad.m96a(parcel, 31, edVar.getHeight(), true);
        }
        if (by.contains(Integer.valueOf(30))) {
            ad.m96a(parcel, 30, edVar.getGivenName(), true);
        }
        if (by.contains(Integer.valueOf(34))) {
            ad.m95a(parcel, 34, edVar.bI(), i, true);
        }
        if (by.contains(Integer.valueOf(32))) {
            ad.m96a(parcel, 32, edVar.getId(), true);
        }
        if (by.contains(Integer.valueOf(33))) {
            ad.m96a(parcel, 33, edVar.getImage(), true);
        }
        if (by.contains(Integer.valueOf(38))) {
            ad.m89a(parcel, 38, edVar.getLongitude());
        }
        if (by.contains(Integer.valueOf(39))) {
            ad.m96a(parcel, 39, edVar.getName(), true);
        }
        if (by.contains(Integer.valueOf(36))) {
            ad.m89a(parcel, 36, edVar.getLatitude());
        }
        if (by.contains(Integer.valueOf(37))) {
            ad.m95a(parcel, 37, edVar.bJ(), i, true);
        }
        if (by.contains(Integer.valueOf(42))) {
            ad.m96a(parcel, 42, edVar.getPlayerType(), true);
        }
        if (by.contains(Integer.valueOf(43))) {
            ad.m96a(parcel, 43, edVar.getPostOfficeBoxNumber(), true);
        }
        if (by.contains(Integer.valueOf(40))) {
            ad.m95a(parcel, 40, edVar.bK(), i, true);
        }
        if (by.contains(Integer.valueOf(41))) {
            ad.m105b(parcel, 41, edVar.bL(), true);
        }
        if (by.contains(Integer.valueOf(46))) {
            ad.m95a(parcel, 46, edVar.bM(), i, true);
        }
        if (by.contains(Integer.valueOf(47))) {
            ad.m96a(parcel, 47, edVar.getStartDate(), true);
        }
        if (by.contains(Integer.valueOf(44))) {
            ad.m96a(parcel, 44, edVar.getPostalCode(), true);
        }
        if (by.contains(Integer.valueOf(45))) {
            ad.m96a(parcel, 45, edVar.getRatingValue(), true);
        }
        if (by.contains(Integer.valueOf(51))) {
            ad.m96a(parcel, 51, edVar.getThumbnailUrl(), true);
        }
        if (by.contains(Integer.valueOf(50))) {
            ad.m95a(parcel, 50, edVar.bN(), i, true);
        }
        if (by.contains(Integer.valueOf(49))) {
            ad.m96a(parcel, 49, edVar.getText(), true);
        }
        if (by.contains(Integer.valueOf(48))) {
            ad.m96a(parcel, 48, edVar.getStreetAddress(), true);
        }
        if (by.contains(Integer.valueOf(55))) {
            ad.m96a(parcel, 55, edVar.getWidth(), true);
        }
        if (by.contains(Integer.valueOf(54))) {
            ad.m96a(parcel, 54, edVar.getUrl(), true);
        }
        if (by.contains(Integer.valueOf(53))) {
            ad.m96a(parcel, 53, edVar.getType(), true);
        }
        if (by.contains(Integer.valueOf(52))) {
            ad.m96a(parcel, 52, edVar.getTickerSymbol(), true);
        }
        if (by.contains(Integer.valueOf(56))) {
            ad.m96a(parcel, 56, edVar.getWorstRating(), true);
        }
        ad.m87C(parcel, d);
    }

    public ed[] m386P(int i) {
        return new ed[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m387v(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m386P(x0);
    }

    public ed m387v(Parcel parcel) {
        int c = ac.m58c(parcel);
        Set hashSet = new HashSet();
        int i = 0;
        ed edVar = null;
        List list = null;
        ed edVar2 = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        List list2 = null;
        int i2 = 0;
        List list3 = null;
        ed edVar3 = null;
        List list4 = null;
        String str4 = null;
        String str5 = null;
        ed edVar4 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        List list5 = null;
        String str9 = null;
        String str10 = null;
        String str11 = null;
        String str12 = null;
        String str13 = null;
        String str14 = null;
        String str15 = null;
        String str16 = null;
        String str17 = null;
        ed edVar5 = null;
        String str18 = null;
        String str19 = null;
        String str20 = null;
        String str21 = null;
        ed edVar6 = null;
        double d = 0.0d;
        ed edVar7 = null;
        double d2 = 0.0d;
        String str22 = null;
        ed edVar8 = null;
        List list6 = null;
        String str23 = null;
        String str24 = null;
        String str25 = null;
        String str26 = null;
        ed edVar9 = null;
        String str27 = null;
        String str28 = null;
        String str29 = null;
        ed edVar10 = null;
        String str30 = null;
        String str31 = null;
        String str32 = null;
        String str33 = null;
        String str34 = null;
        String str35 = null;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            ed edVar11;
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    edVar11 = (ed) ac.m52a(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(2));
                    edVar = edVar11;
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    list = ac.m82x(parcel, b);
                    hashSet.add(Integer.valueOf(3));
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    edVar11 = (ed) ac.m52a(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(4));
                    edVar2 = edVar11;
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    str = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(5));
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    str2 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(6));
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    str3 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(7));
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    list2 = ac.m59c(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(8));
                    break;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    i2 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(9));
                    break;
                case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                    list3 = ac.m59c(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(10));
                    break;
                case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                    edVar11 = (ed) ac.m52a(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(11));
                    edVar3 = edVar11;
                    break;
                case Strings.UPDATE_DIALOG_POSITIVE_BUTTON_ID /*12*/:
                    list4 = ac.m59c(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(12));
                    break;
                case Strings.EXPIRY_INFO_TITLE_ID /*13*/:
                    str4 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(13));
                    break;
                case Strings.EXPIRY_INFO_TEXT_ID /*14*/:
                    str5 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(14));
                    break;
                case Strings.FEEDBACK_FAILED_TITLE_ID /*15*/:
                    edVar11 = (ed) ac.m52a(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(15));
                    edVar4 = edVar11;
                    break;
                case TransportMediator.FLAG_KEY_MEDIA_PAUSE /*16*/:
                    str6 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(16));
                    break;
                case StringKeys.SELECT_ALBUM /*17*/:
                    str7 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(17));
                    break;
                case StringKeys.SELECT_IMAGES /*18*/:
                    str8 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(18));
                    break;
                case StringKeys.SELECT_IMAGE /*19*/:
                    list5 = ac.m59c(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(19));
                    break;
                case StringKeys.CAMERA_FOLDER /*20*/:
                    str9 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(20));
                    break;
                case C0436R.styleable.View_android_fitsSystemWindows /*21*/:
                    str10 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(21));
                    break;
                case C0436R.styleable.View_android_scrollbars /*22*/:
                    str11 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(22));
                    break;
                case C0436R.styleable.View_android_fadingEdge /*23*/:
                    str12 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(23));
                    break;
                case C0436R.styleable.View_android_fadingEdgeLength /*24*/:
                    str13 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(24));
                    break;
                case C0436R.styleable.View_android_nextFocusLeft /*25*/:
                    str14 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(25));
                    break;
                case C0436R.styleable.View_android_nextFocusRight /*26*/:
                    str15 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(26));
                    break;
                case C0436R.styleable.View_android_nextFocusUp /*27*/:
                    str16 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(27));
                    break;
                case C0436R.styleable.View_android_nextFocusDown /*28*/:
                    str17 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(28));
                    break;
                case C0436R.styleable.View_android_clickable /*29*/:
                    edVar11 = (ed) ac.m52a(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(29));
                    edVar5 = edVar11;
                    break;
                case C0436R.styleable.View_android_longClickable /*30*/:
                    str18 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(30));
                    break;
                case CacheTables.MAIN_DB_VERSION /*31*/:
                    str19 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(31));
                    break;
                case TransportMediator.FLAG_KEY_MEDIA_STOP /*32*/:
                    str20 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(32));
                    break;
                case StringKeys.NO_SD_CARD /*33*/:
                    str21 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(33));
                    break;
                case StringKeys.ERROR /*34*/:
                    edVar11 = (ed) ac.m52a(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(34));
                    edVar6 = edVar11;
                    break;
                case C0436R.styleable.View_android_soundEffectsEnabled /*36*/:
                    d = ac.m67j(parcel, b);
                    hashSet.add(Integer.valueOf(36));
                    break;
                case C0436R.styleable.View_android_keepScreenOn /*37*/:
                    edVar11 = (ed) ac.m52a(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(37));
                    edVar7 = edVar11;
                    break;
                case C0436R.styleable.View_android_isScrollContainer /*38*/:
                    d2 = ac.m67j(parcel, b);
                    hashSet.add(Integer.valueOf(38));
                    break;
                case C0436R.styleable.View_android_hapticFeedbackEnabled /*39*/:
                    str22 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(39));
                    break;
                case C0436R.styleable.View_android_onClick /*40*/:
                    edVar11 = (ed) ac.m52a(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(40));
                    edVar8 = edVar11;
                    break;
                case C0436R.styleable.View_android_contentDescription /*41*/:
                    list6 = ac.m59c(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(41));
                    break;
                case C0436R.styleable.View_android_scrollbarFadeDuration /*42*/:
                    str23 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(42));
                    break;
                case C0436R.styleable.View_android_scrollbarDefaultDelayBeforeFade /*43*/:
                    str24 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(43));
                    break;
                case C0436R.styleable.View_android_fadeScrollbars /*44*/:
                    str25 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(44));
                    break;
                case C0436R.styleable.SherlockTheme_listPreferredItemPaddingLeft /*45*/:
                    str26 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(45));
                    break;
                case C0436R.styleable.SherlockTheme_listPreferredItemPaddingRight /*46*/:
                    edVar11 = (ed) ac.m52a(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(46));
                    edVar9 = edVar11;
                    break;
                case C0436R.styleable.SherlockTheme_textAppearanceListItemSmall /*47*/:
                    str27 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(47));
                    break;
                case GalleryPickerHeaderView.SIZE /*48*/:
                    str28 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(48));
                    break;
                case StringKeys.CORRUPTED_IMAGE_1 /*49*/:
                    str29 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(49));
                    break;
                case StringKeys.CORRUPTED_IMAGE_2 /*50*/:
                    edVar11 = (ed) ac.m52a(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(50));
                    edVar10 = edVar11;
                    break;
                case StringKeys.FILTERS_UNSUPPORTED /*51*/:
                    str30 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(51));
                    break;
                case StringKeys.SHARE_PHOTO /*52*/:
                    str31 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(52));
                    break;
                case StringKeys.SHARE_PHOTOS /*53*/:
                    str32 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(53));
                    break;
                case StringKeys.CLOSE /*54*/:
                    str33 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(54));
                    break;
                case C0436R.styleable.SherlockTheme_popupMenuStyle /*55*/:
                    str34 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(55));
                    break;
                case C0436R.styleable.SherlockTheme_dropdownListPreferredItemHeight /*56*/:
                    str35 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(56));
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new ed(hashSet, i, edVar, list, edVar2, str, str2, str3, list2, i2, list3, edVar3, list4, str4, str5, edVar4, str6, str7, str8, list5, str9, str10, str11, str12, str13, str14, str15, str16, str17, edVar5, str18, str19, str20, str21, edVar6, d, edVar7, d2, str22, edVar8, list6, str23, str24, str25, str26, edVar9, str27, str28, str29, edVar10, str30, str31, str32, str33, str34, str35);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }
}
