package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.eq.C1599b.C1597a;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashSet;
import java.util.Set;

public class ek implements Creator<C1597a> {
    static void m397a(C1597a c1597a, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        Set by = c1597a.by();
        if (by.contains(Integer.valueOf(1))) {
            ad.m106c(parcel, 1, c1597a.m1208u());
        }
        if (by.contains(Integer.valueOf(2))) {
            ad.m106c(parcel, 2, c1597a.getLeftImageOffset());
        }
        if (by.contains(Integer.valueOf(3))) {
            ad.m106c(parcel, 3, c1597a.getTopImageOffset());
        }
        ad.m87C(parcel, d);
    }

    public C1597a[] m398T(int i) {
        return new C1597a[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m399z(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m398T(x0);
    }

    public C1597a m399z(Parcel parcel) {
        int i = 0;
        int c = ac.m58c(parcel);
        Set hashSet = new HashSet();
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i3 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    i2 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(2));
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    i = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(3));
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new C1597a(hashSet, i3, i2, i);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }
}
