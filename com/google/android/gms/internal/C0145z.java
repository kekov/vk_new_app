package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.view.View;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.C0138v.C1240a;

/* renamed from: com.google.android.gms.internal.z */
public final class C0145z {
    private static Context aZ;
    private static C0138v ba;

    /* renamed from: com.google.android.gms.internal.z.a */
    public static class C0144a extends Exception {
        public C0144a(String str) {
            super(str);
        }
    }

    public static View m497d(Context context, int i, int i2) throws C0144a {
        try {
            return (View) bd.m1048a(C0145z.m498f(context).m485a(bd.m1049f(context), i, i2));
        } catch (Exception e) {
            throw new C0144a("Could not get button with size " + i + " and color " + i2);
        }
    }

    private static C0138v m498f(Context context) throws C0144a {
        C0142x.m495d(context);
        if (ba == null) {
            if (aZ == null) {
                aZ = GooglePlayServicesUtil.getRemoteContext(context);
                if (aZ == null) {
                    throw new C0144a("Could not get remote context.");
                }
            }
            try {
                ba = C1240a.m972i((IBinder) aZ.getClassLoader().loadClass("com.google.android.gms.common.ui.SignInButtonCreatorImpl").newInstance());
            } catch (ClassNotFoundException e) {
                throw new C0144a("Could not load creator class.");
            } catch (InstantiationException e2) {
                throw new C0144a("Could not instantiate creator.");
            } catch (IllegalAccessException e3) {
                throw new C0144a("Could not access creator.");
            }
        }
        return ba;
    }
}
