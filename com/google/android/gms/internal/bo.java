package com.google.android.gms.internal;

import android.net.LocalSocket;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import com.google.android.gms.games.RealTimeSocket;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

final class bo implements RealTimeSocket {
    private final LocalSocket dl;
    private final String dm;
    private ParcelFileDescriptor dn;

    bo(LocalSocket localSocket, String str) {
        this.dl = localSocket;
        this.dm = str;
    }

    public void close() throws IOException {
        this.dl.close();
    }

    public InputStream getInputStream() throws IOException {
        return this.dl.getInputStream();
    }

    public OutputStream getOutputStream() throws IOException {
        return this.dl.getOutputStream();
    }

    public ParcelFileDescriptor getParcelFileDescriptor() throws IOException {
        if (this.dn == null && !isClosed()) {
            Parcel obtain = Parcel.obtain();
            obtain.writeFileDescriptor(this.dl.getFileDescriptor());
            obtain.setDataPosition(0);
            this.dn = obtain.readFileDescriptor();
        }
        return this.dn;
    }

    public boolean isClosed() {
        return (this.dl.isConnected() || this.dl.isBound()) ? false : true;
    }
}
