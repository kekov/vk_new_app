package com.google.android.gms.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: com.google.android.gms.internal.w */
public final class C0141w {

    /* renamed from: com.google.android.gms.internal.w.a */
    public static final class C0140a {
        private final List<String> aW;
        private final Object aX;

        private C0140a(Object obj) {
            this.aX = C0142x.m495d(obj);
            this.aW = new ArrayList();
        }

        public C0140a m486a(String str, Object obj) {
            this.aW.add(((String) C0142x.m495d(str)) + "=" + String.valueOf(obj));
            return this;
        }

        public String toString() {
            StringBuilder append = new StringBuilder(100).append(this.aX.getClass().getSimpleName()).append('{');
            int size = this.aW.size();
            for (int i = 0; i < size; i++) {
                append.append((String) this.aW.get(i));
                if (i < size - 1) {
                    append.append(", ");
                }
            }
            return append.append('}').toString();
        }
    }

    public static boolean m487a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public static C0140a m488c(Object obj) {
        return new C0140a(null);
    }

    public static int hashCode(Object... objects) {
        return Arrays.hashCode(objects);
    }
}
