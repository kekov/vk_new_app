package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.eq.C1599b;
import com.google.android.gms.internal.eq.C1599b.C1597a;
import com.google.android.gms.internal.eq.C1599b.C1598b;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashSet;
import java.util.Set;

public class ej implements Creator<C1599b> {
    static void m394a(C1599b c1599b, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        Set by = c1599b.by();
        if (by.contains(Integer.valueOf(1))) {
            ad.m106c(parcel, 1, c1599b.m1220u());
        }
        if (by.contains(Integer.valueOf(2))) {
            ad.m95a(parcel, 2, c1599b.ce(), i, true);
        }
        if (by.contains(Integer.valueOf(3))) {
            ad.m95a(parcel, 3, c1599b.cf(), i, true);
        }
        if (by.contains(Integer.valueOf(4))) {
            ad.m106c(parcel, 4, c1599b.getLayout());
        }
        ad.m87C(parcel, d);
    }

    public C1599b[] m395S(int i) {
        return new C1599b[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m396y(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m395S(x0);
    }

    public C1599b m396y(Parcel parcel) {
        C1598b c1598b = null;
        int i = 0;
        int c = ac.m58c(parcel);
        Set hashSet = new HashSet();
        C1597a c1597a = null;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i2 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    C1597a c1597a2 = (C1597a) ac.m52a(parcel, b, C1597a.CREATOR);
                    hashSet.add(Integer.valueOf(2));
                    c1597a = c1597a2;
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    C1598b c1598b2 = (C1598b) ac.m52a(parcel, b, C1598b.CREATOR);
                    hashSet.add(Integer.valueOf(3));
                    c1598b = c1598b2;
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    i = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(4));
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new C1599b(hashSet, i2, c1597a, c1598b, i);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }
}
