package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.eq.C1605i;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashSet;
import java.util.Set;

public class eu implements Creator<C1605i> {
    static void m421a(C1605i c1605i, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        Set by = c1605i.by();
        if (by.contains(Integer.valueOf(1))) {
            ad.m106c(parcel, 1, c1605i.m1256u());
        }
        if (by.contains(Integer.valueOf(2))) {
            ad.m99a(parcel, 2, c1605i.isPrimary());
        }
        if (by.contains(Integer.valueOf(3))) {
            ad.m106c(parcel, 3, c1605i.getType());
        }
        if (by.contains(Integer.valueOf(4))) {
            ad.m96a(parcel, 4, c1605i.getValue(), true);
        }
        ad.m87C(parcel, d);
    }

    public C1605i m422H(Parcel parcel) {
        int i = 0;
        int c = ac.m58c(parcel);
        Set hashSet = new HashSet();
        String str = null;
        boolean z = false;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i2 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    z = ac.m60c(parcel, b);
                    hashSet.add(Integer.valueOf(2));
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    i = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(3));
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    str = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(4));
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new C1605i(hashSet, i2, z, i, str);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public C1605i[] ab(int i) {
        return new C1605i[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m422H(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return ab(x0);
    }
}
