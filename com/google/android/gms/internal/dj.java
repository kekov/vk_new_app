package com.google.android.gms.internal;

import android.graphics.Bitmap;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.bc.C1151a;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public interface dj extends IInterface {

    /* renamed from: com.google.android.gms.internal.dj.a */
    public static abstract class C1194a extends Binder implements dj {

        /* renamed from: com.google.android.gms.internal.dj.a.a */
        static class C1193a implements dj {
            private IBinder f95a;

            C1193a(IBinder iBinder) {
                this.f95a = iBinder;
            }

            public bc m797N(int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    obtain.writeInt(i);
                    this.f95a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    bc j = C1151a.m631j(obtain2.readStrongBinder());
                    return j;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public bc m798a(Bitmap bitmap) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    if (bitmap != null) {
                        obtain.writeInt(1);
                        bitmap.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f95a.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                    bc j = C1151a.m631j(obtain2.readStrongBinder());
                    return j;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f95a;
            }

            public bc bb() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    this.f95a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    bc j = C1151a.m631j(obtain2.readStrongBinder());
                    return j;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public bc m799c(float f) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    obtain.writeFloat(f);
                    this.f95a.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    bc j = C1151a.m631j(obtain2.readStrongBinder());
                    return j;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public bc m800x(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    obtain.writeString(str);
                    this.f95a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    bc j = C1151a.m631j(obtain2.readStrongBinder());
                    return j;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public bc m801y(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    obtain.writeString(str);
                    this.f95a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    bc j = C1151a.m631j(obtain2.readStrongBinder());
                    return j;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public bc m802z(String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    obtain.writeString(str);
                    this.f95a.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                    bc j = C1151a.m631j(obtain2.readStrongBinder());
                    return j;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public static dj m803J(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof dj)) ? new C1193a(iBinder) : (dj) queryLocalInterface;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            IBinder iBinder = null;
            bc N;
            switch (code) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    data.enforceInterface("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    N = m329N(data.readInt());
                    reply.writeNoException();
                    reply.writeStrongBinder(N != null ? N.asBinder() : null);
                    return true;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    data.enforceInterface("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    N = m332x(data.readString());
                    reply.writeNoException();
                    if (N != null) {
                        iBinder = N.asBinder();
                    }
                    reply.writeStrongBinder(iBinder);
                    return true;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    data.enforceInterface("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    N = m333y(data.readString());
                    reply.writeNoException();
                    if (N != null) {
                        iBinder = N.asBinder();
                    }
                    reply.writeStrongBinder(iBinder);
                    return true;
                case UserListView.TYPE_FAVE /*4*/:
                    data.enforceInterface("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    N = bb();
                    reply.writeNoException();
                    if (N != null) {
                        iBinder = N.asBinder();
                    }
                    reply.writeStrongBinder(iBinder);
                    return true;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    data.enforceInterface("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    N = m331c(data.readFloat());
                    reply.writeNoException();
                    if (N != null) {
                        iBinder = N.asBinder();
                    }
                    reply.writeStrongBinder(iBinder);
                    return true;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    data.enforceInterface("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    N = m330a(data.readInt() != 0 ? (Bitmap) Bitmap.CREATOR.createFromParcel(data) : null);
                    reply.writeNoException();
                    if (N != null) {
                        iBinder = N.asBinder();
                    }
                    reply.writeStrongBinder(iBinder);
                    return true;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    data.enforceInterface("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    N = m334z(data.readString());
                    reply.writeNoException();
                    if (N != null) {
                        iBinder = N.asBinder();
                    }
                    reply.writeStrongBinder(iBinder);
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    bc m329N(int i) throws RemoteException;

    bc m330a(Bitmap bitmap) throws RemoteException;

    bc bb() throws RemoteException;

    bc m331c(float f) throws RemoteException;

    bc m332x(String str) throws RemoteException;

    bc m333y(String str) throws RemoteException;

    bc m334z(String str) throws RemoteException;
}
