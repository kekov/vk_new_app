package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.ArrayList;

public class ec implements Creator<eb> {
    static void m382a(eb ebVar, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m96a(parcel, 1, ebVar.getDescription(), false);
        ad.m106c(parcel, LocationStatusCodes.GEOFENCE_NOT_AVAILABLE, ebVar.m898u());
        ad.m105b(parcel, 2, ebVar.bv(), false);
        ad.m105b(parcel, 3, ebVar.bw(), false);
        ad.m99a(parcel, 4, ebVar.bx());
        ad.m87C(parcel, d);
    }

    public eb[] m383O(int i) {
        return new eb[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m384u(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m383O(x0);
    }

    public eb m384u(Parcel parcel) {
        boolean z = false;
        ArrayList arrayList = null;
        int c = ac.m58c(parcel);
        ArrayList arrayList2 = null;
        String str = null;
        int i = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    str = ac.m70l(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    arrayList2 = ac.m59c(parcel, b, ag.CREATOR);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    arrayList = ac.m59c(parcel, b, ag.CREATOR);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    z = ac.m60c(parcel, b);
                    break;
                case LocationStatusCodes.GEOFENCE_NOT_AVAILABLE /*1000*/:
                    i = ac.m63f(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new eb(i, str, arrayList2, arrayList, z);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }
}
