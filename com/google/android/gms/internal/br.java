package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.games.leaderboard.Leaderboard;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import java.util.ArrayList;

public final class br extends C0123j implements Leaderboard {
    private final int f77do;

    public br(C1230k c1230k, int i, int i2) {
        super(c1230k, i);
        this.f77do = i2;
    }

    public String getDisplayName() {
        return getString("name");
    }

    public void getDisplayName(CharArrayBuffer dataOut) {
        m434a("name", dataOut);
    }

    public Uri getIconImageUri() {
        return m435c("board_icon_image_uri");
    }

    public String getLeaderboardId() {
        return getString("external_leaderboard_id");
    }

    public int getScoreOrder() {
        return getInteger("score_order");
    }

    public ArrayList<LeaderboardVariant> getVariants() {
        ArrayList<LeaderboardVariant> arrayList = new ArrayList(this.f77do);
        for (int i = 0; i < this.f77do; i++) {
            arrayList.add(new bv(this.O, this.R + i));
        }
        return arrayList;
    }

    public String toString() {
        return C0141w.m488c(this).m486a("ID", getLeaderboardId()).m486a("DisplayName", getDisplayName()).m486a("IconImageURI", getIconImageUri()).m486a("ScoreOrder", Integer.valueOf(getScoreOrder())).toString();
    }
}
