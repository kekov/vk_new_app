package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class ah implements Creator<ag> {
    static void m110a(ag agVar, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, agVar.getType());
        ad.m106c(parcel, LocationStatusCodes.GEOFENCE_NOT_AVAILABLE, agVar.m554u());
        ad.m106c(parcel, 2, agVar.m555v());
        ad.m96a(parcel, 3, agVar.m556w(), false);
        ad.m96a(parcel, 4, agVar.m557x(), false);
        ad.m96a(parcel, 5, agVar.getDisplayName(), false);
        ad.m96a(parcel, 6, agVar.m558y(), false);
        ad.m87C(parcel, d);
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m111e(x0);
    }

    public ag m111e(Parcel parcel) {
        int i = 0;
        String str = null;
        int c = ac.m58c(parcel);
        String str2 = null;
        String str3 = null;
        String str4 = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i2 = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    i = ac.m63f(parcel, b);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    str4 = ac.m70l(parcel, b);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    str3 = ac.m70l(parcel, b);
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    str2 = ac.m70l(parcel, b);
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    str = ac.m70l(parcel, b);
                    break;
                case LocationStatusCodes.GEOFENCE_NOT_AVAILABLE /*1000*/:
                    i3 = ac.m63f(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new ag(i3, i2, i, str4, str3, str2, str);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public ag[] m112k(int i) {
        return new ag[i];
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m112k(x0);
    }
}
