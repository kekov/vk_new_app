package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.CameraPosition;

public class cy {
    public static void m318a(CameraPosition cameraPosition, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, cameraPosition.m991u());
        ad.m95a(parcel, 2, cameraPosition.target, i, false);
        ad.m90a(parcel, 3, cameraPosition.zoom);
        ad.m90a(parcel, 4, cameraPosition.tilt);
        ad.m90a(parcel, 5, cameraPosition.bearing);
        ad.m87C(parcel, d);
    }
}
