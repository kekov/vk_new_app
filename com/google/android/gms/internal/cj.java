package com.google.android.gms.internal;

import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public final class cj {
    public static Boolean m296a(byte b) {
        switch (b) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return Boolean.FALSE;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                return Boolean.TRUE;
            default:
                return null;
        }
    }

    public static byte m297b(Boolean bool) {
        return bool != null ? bool.booleanValue() ? (byte) 1 : (byte) 0 : (byte) -1;
    }
}
