package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.eq.C1601d;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashSet;
import java.util.Set;

public class en implements Creator<C1601d> {
    static void m406a(C1601d c1601d, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        Set by = c1601d.by();
        if (by.contains(Integer.valueOf(1))) {
            ad.m106c(parcel, 1, c1601d.m1232u());
        }
        if (by.contains(Integer.valueOf(2))) {
            ad.m96a(parcel, 2, c1601d.getUrl(), true);
        }
        ad.m87C(parcel, d);
    }

    public C1601d m407C(Parcel parcel) {
        int c = ac.m58c(parcel);
        Set hashSet = new HashSet();
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    str = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(2));
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new C1601d(hashSet, i, str);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public C1601d[] m408W(int i) {
        return new C1601d[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m407C(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m408W(x0);
    }
}
