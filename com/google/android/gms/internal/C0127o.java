package com.google.android.gms.internal;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.util.Log;

/* renamed from: com.google.android.gms.internal.o */
public class C0127o implements OnClickListener {
    private final Activity ar;
    private final int as;
    private final Intent mIntent;

    public C0127o(Activity activity, Intent intent, int i) {
        this.ar = activity;
        this.mIntent = intent;
        this.as = i;
    }

    public void onClick(DialogInterface dialog, int which) {
        try {
            if (this.mIntent != null) {
                this.ar.startActivityForResult(this.mIntent, this.as);
            }
            dialog.dismiss();
        } catch (ActivityNotFoundException e) {
            Log.e("SettingsRedirect", "Can't redirect to app settings for Google Play services");
        }
    }
}
