package com.google.android.gms.internal;

import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.v4.media.TransportMediator;
import com.google.android.gms.internal.dw.C1214a;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.providers.LangProvider.StringKeys;
import java.util.List;
import net.hockeyapp.android.Strings;

public interface dx extends IInterface {

    /* renamed from: com.google.android.gms.internal.dx.a */
    public static abstract class C1216a extends Binder implements dx {

        /* renamed from: com.google.android.gms.internal.dx.a.a */
        static class C1215a implements dx {
            private IBinder f105a;

            C1215a(IBinder iBinder) {
                this.f105a = iBinder;
            }

            public void m846a(at atVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    if (atVar != null) {
                        obtain.writeInt(1);
                        atVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f105a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m847a(dw dwVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    this.f105a.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m848a(dw dwVar, int i, int i2, int i3, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeInt(i2);
                    obtain.writeInt(i3);
                    obtain.writeString(str);
                    this.f105a.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m849a(dw dwVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.f105a.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m850a(dw dwVar, int i, String str, Uri uri, String str2, String str3) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    this.f105a.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m851a(dw dwVar, Uri uri, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f105a.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m852a(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.f105a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m853a(dw dwVar, String str, eb ebVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    if (ebVar != null) {
                        obtain.writeInt(1);
                        ebVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f105a.transact(25, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m854a(dw dwVar, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.f105a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m855a(dw dwVar, String str, String str2, int i, String str3) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeInt(i);
                    obtain.writeString(str3);
                    this.f105a.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m856a(dw dwVar, String str, List<String> list, List<String> list2, List<String> list3) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeStringList(list);
                    obtain.writeStringList(list2);
                    obtain.writeStringList(list3);
                    this.f105a.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m857a(dw dwVar, String str, boolean z) throws RemoteException {
                int i = 0;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    if (z) {
                        i = 1;
                    }
                    obtain.writeInt(i);
                    this.f105a.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m858a(dw dwVar, boolean z, boolean z2) throws RemoteException {
                int i = 1;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeInt(z ? 1 : 0);
                    if (!z2) {
                        i = 0;
                    }
                    obtain.writeInt(i);
                    this.f105a.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f105a;
            }

            public void m859b(dw dwVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    this.f105a.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m860b(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.f105a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m861c(dw dwVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    this.f105a.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m862c(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.f105a.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void clearDefaultAccount() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    this.f105a.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m863d(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.f105a.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m864e(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.f105a.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m865f(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.f105a.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m866f(String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.f105a.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m867g(dw dwVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeStrongBinder(dwVar != null ? dwVar.asBinder() : null);
                    obtain.writeString(str);
                    this.f105a.transact(26, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getAccountName() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    this.f105a.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    String readString = obtain2.readString();
                    return readString;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void removeMoment(String momentId) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.plus.internal.IPlusService");
                    obtain.writeString(momentId);
                    this.f105a.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public static dx m868V(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.plus.internal.IPlusService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof dx)) ? new C1215a(iBinder) : (dx) queryLocalInterface;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            boolean z = false;
            eb ebVar = null;
            dw U;
            switch (code) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m364a(C1214a.m845U(data.readStrongBinder()), data.readString());
                    reply.writeNoException();
                    return true;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m366a(C1214a.m845U(data.readStrongBinder()), data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m372b(C1214a.m845U(data.readStrongBinder()), data.readString());
                    reply.writeNoException();
                    return true;
                case UserListView.TYPE_FAVE /*4*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m358a(data.readInt() != 0 ? at.CREATOR.m150m(data) : null);
                    reply.writeNoException();
                    return true;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    String accountName = getAccountName();
                    reply.writeNoException();
                    reply.writeString(accountName);
                    return true;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    clearDefaultAccount();
                    reply.writeNoException();
                    return true;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m374c(C1214a.m845U(data.readStrongBinder()), data.readString());
                    reply.writeNoException();
                    return true;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m359a(C1214a.m845U(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m363a(C1214a.m845U(data.readStrongBinder()), data.readInt() != 0 ? (Uri) Uri.CREATOR.createFromParcel(data) : null, data.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(data) : null);
                    reply.writeNoException();
                    return true;
                case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m375d(C1214a.m845U(data.readStrongBinder()), data.readString());
                    reply.writeNoException();
                    return true;
                case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m378f(data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case Strings.UPDATE_DIALOG_POSITIVE_BUTTON_ID /*12*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m367a(C1214a.m845U(data.readStrongBinder()), data.readString(), data.readString(), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case Strings.EXPIRY_INFO_TITLE_ID /*13*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m371b(C1214a.m845U(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case Strings.EXPIRY_INFO_TEXT_ID /*14*/:
                    Uri uri;
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    U = C1214a.m845U(data.readStrongBinder());
                    int readInt = data.readInt();
                    String readString = data.readString();
                    if (data.readInt() != 0) {
                        uri = (Uri) Uri.CREATOR.createFromParcel(data);
                    }
                    m362a(U, readInt, readString, uri, data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case TransportMediator.FLAG_KEY_MEDIA_PAUSE /*16*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m360a(C1214a.m845U(data.readStrongBinder()), data.readInt(), data.readInt(), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case StringKeys.SELECT_ALBUM /*17*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    removeMoment(data.readString());
                    reply.writeNoException();
                    return true;
                case StringKeys.SELECT_IMAGES /*18*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m376e(C1214a.m845U(data.readStrongBinder()), data.readString());
                    reply.writeNoException();
                    return true;
                case StringKeys.SELECT_IMAGE /*19*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m373c(C1214a.m845U(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case StringKeys.CAMERA_FOLDER /*20*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m361a(C1214a.m845U(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case C0436R.styleable.View_android_fitsSystemWindows /*21*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    U = C1214a.m845U(data.readStrongBinder());
                    String readString2 = data.readString();
                    if (data.readInt() != 0) {
                        z = true;
                    }
                    m369a(U, readString2, z);
                    reply.writeNoException();
                    return true;
                case C0436R.styleable.View_android_scrollbars /*22*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    dw U2 = C1214a.m845U(data.readStrongBinder());
                    boolean z2 = data.readInt() != 0;
                    if (data.readInt() != 0) {
                        z = true;
                    }
                    m370a(U2, z2, z);
                    reply.writeNoException();
                    return true;
                case C0436R.styleable.View_android_fadingEdge /*23*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m368a(C1214a.m845U(data.readStrongBinder()), data.readString(), data.createStringArrayList(), data.createStringArrayList(), data.createStringArrayList());
                    reply.writeNoException();
                    return true;
                case C0436R.styleable.View_android_fadingEdgeLength /*24*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m377f(C1214a.m845U(data.readStrongBinder()), data.readString());
                    reply.writeNoException();
                    return true;
                case C0436R.styleable.View_android_nextFocusLeft /*25*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    dw U3 = C1214a.m845U(data.readStrongBinder());
                    String readString3 = data.readString();
                    if (data.readInt() != 0) {
                        ebVar = eb.CREATOR.m384u(data);
                    }
                    m365a(U3, readString3, ebVar);
                    reply.writeNoException();
                    return true;
                case C0436R.styleable.View_android_nextFocusRight /*26*/:
                    data.enforceInterface("com.google.android.gms.plus.internal.IPlusService");
                    m379g(C1214a.m845U(data.readStrongBinder()), data.readString());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.plus.internal.IPlusService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void m358a(at atVar) throws RemoteException;

    void m359a(dw dwVar) throws RemoteException;

    void m360a(dw dwVar, int i, int i2, int i3, String str) throws RemoteException;

    void m361a(dw dwVar, int i, String str) throws RemoteException;

    void m362a(dw dwVar, int i, String str, Uri uri, String str2, String str3) throws RemoteException;

    void m363a(dw dwVar, Uri uri, Bundle bundle) throws RemoteException;

    void m364a(dw dwVar, String str) throws RemoteException;

    void m365a(dw dwVar, String str, eb ebVar) throws RemoteException;

    void m366a(dw dwVar, String str, String str2) throws RemoteException;

    void m367a(dw dwVar, String str, String str2, int i, String str3) throws RemoteException;

    void m368a(dw dwVar, String str, List<String> list, List<String> list2, List<String> list3) throws RemoteException;

    void m369a(dw dwVar, String str, boolean z) throws RemoteException;

    void m370a(dw dwVar, boolean z, boolean z2) throws RemoteException;

    void m371b(dw dwVar) throws RemoteException;

    void m372b(dw dwVar, String str) throws RemoteException;

    void m373c(dw dwVar) throws RemoteException;

    void m374c(dw dwVar, String str) throws RemoteException;

    void clearDefaultAccount() throws RemoteException;

    void m375d(dw dwVar, String str) throws RemoteException;

    void m376e(dw dwVar, String str) throws RemoteException;

    void m377f(dw dwVar, String str) throws RemoteException;

    void m378f(String str, String str2) throws RemoteException;

    void m379g(dw dwVar, String str) throws RemoteException;

    String getAccountName() throws RemoteException;

    void removeMoment(String str) throws RemoteException;
}
