package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.LatLngBounds;

public class db {
    public static void m321a(LatLngBounds latLngBounds, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, latLngBounds.m1002u());
        ad.m95a(parcel, 2, latLngBounds.southwest, i, false);
        ad.m95a(parcel, 3, latLngBounds.northeast, i, false);
        ad.m87C(parcel, d);
    }
}
