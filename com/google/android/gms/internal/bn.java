package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import java.lang.ref.WeakReference;

public class bn {
    protected bj cs;
    protected C0112a dh;

    /* renamed from: com.google.android.gms.internal.bn.a */
    public static final class C0112a {
        public int bottom;
        public IBinder di;
        public int dj;
        public int gravity;
        public int left;
        public int right;
        public int top;

        private C0112a(int i) {
            this.di = null;
            this.dj = -1;
            this.left = 0;
            this.top = 0;
            this.right = 0;
            this.bottom = 0;
            this.gravity = i;
            this.di = new Binder();
        }

        public Bundle ap() {
            Bundle bundle = new Bundle();
            bundle.putInt("popupLocationInfo.gravity", this.gravity);
            bundle.putInt("popupLocationInfo.displayId", this.dj);
            bundle.putInt("popupLocationInfo.left", this.left);
            bundle.putInt("popupLocationInfo.top", this.top);
            bundle.putInt("popupLocationInfo.right", this.right);
            bundle.putInt("popupLocationInfo.bottom", this.bottom);
            return bundle;
        }
    }

    /* renamed from: com.google.android.gms.internal.bn.b */
    static final class C1161b extends bn implements OnAttachStateChangeListener, OnGlobalLayoutListener {
        private boolean cC;
        private WeakReference<View> dk;

        protected C1161b(bj bjVar, int i) {
            super(i, null);
            this.cC = false;
        }

        private void m746b(View view) {
            int i = -1;
            if (ba.af()) {
                i = view.getDisplay().getDisplayId();
            }
            IBinder windowToken = view.getWindowToken();
            int[] iArr = new int[2];
            view.getLocationInWindow(iArr);
            int width = view.getWidth();
            int height = view.getHeight();
            this.dh.dj = i;
            this.dh.di = windowToken;
            this.dh.left = iArr[0];
            this.dh.top = iArr[1];
            this.dh.right = iArr[0] + width;
            this.dh.bottom = iArr[1] + height;
            if (this.cC) {
                am();
                this.cC = false;
            }
        }

        public void m747a(View view) {
            View view2;
            Context context;
            if (this.dk != null) {
                view2 = (View) this.dk.get();
                context = this.cs.getContext();
                if (view2 == null && (context instanceof Activity)) {
                    view2 = ((Activity) context).getWindow().getDecorView();
                }
                if (view2 != null) {
                    ViewTreeObserver viewTreeObserver = view2.getViewTreeObserver();
                    if (ba.ae()) {
                        viewTreeObserver.removeOnGlobalLayoutListener(this);
                    } else {
                        viewTreeObserver.removeGlobalOnLayoutListener(this);
                    }
                }
            }
            this.dk = null;
            context = this.cs.getContext();
            if (view == null && (context instanceof Activity)) {
                view2 = ((Activity) context).findViewById(16908290);
                if (view2 == null) {
                    view2 = ((Activity) context).getWindow().getDecorView();
                }
                bk.m178b("PopupManager", "You have not specified a View to use as content view for popups. Falling back to the Activity content view which may not work properly in future versions of the API. Use setViewForPopups() to set your content view.");
                view = view2;
            }
            if (view != null) {
                this.dk = new WeakReference(view);
                view.addOnAttachStateChangeListener(this);
                view.getViewTreeObserver().addOnGlobalLayoutListener(this);
                return;
            }
            bk.m179c("PopupManager", "No content view usable to display popups. Popups will not be displayed in response to this client's calls. Use setViewForPopups() to set your content view.");
        }

        public void am() {
            if (this.dh.di != null) {
                super.am();
            } else {
                this.cC = this.dk != null;
            }
        }

        public void onGlobalLayout() {
            if (this.dk != null) {
                View view = (View) this.dk.get();
                if (view != null) {
                    m746b(view);
                }
            }
        }

        public void onViewAttachedToWindow(View v) {
            m746b(v);
        }

        public void onViewDetachedFromWindow(View v) {
            this.cs.aj();
            v.removeOnAttachStateChangeListener(this);
        }
    }

    private bn(bj bjVar, int i) {
        this.cs = bjVar;
        this.dh = new C0112a(null);
    }

    public static bn m277a(bj bjVar, int i) {
        return ba.ab() ? new C1161b(bjVar, i) : new bn(bjVar, i);
    }

    public void m278a(View view) {
    }

    public void am() {
        this.cs.m1108a(this.dh.di, this.dh.ap());
    }

    public Bundle an() {
        return this.dh.ap();
    }

    public IBinder ao() {
        return this.dh.di;
    }

    public void setGravity(int gravity) {
        this.dh.gravity = gravity;
    }
}
