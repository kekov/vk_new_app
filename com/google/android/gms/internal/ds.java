package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.C1234p.C0129b;
import com.google.android.gms.internal.C1234p.C1606d;
import com.google.android.gms.internal.dq.C1208a;
import com.google.android.gms.internal.dr.C1210a;
import com.google.android.gms.panorama.PanoramaClient.C0146a;
import com.google.android.gms.panorama.PanoramaClient.OnPanoramaInfoLoadedListener;

public class ds extends C1234p<dr> {

    /* renamed from: com.google.android.gms.internal.ds.a */
    final class C1211a extends C0129b<C0146a> {
        public final ConnectionResult gD;
        public final Intent gE;
        final /* synthetic */ ds gF;
        public final int type;

        public C1211a(ds dsVar, C0146a c0146a, ConnectionResult connectionResult, int i, Intent intent) {
            this.gF = dsVar;
            super(dsVar, c0146a);
            this.gD = connectionResult;
            this.type = i;
            this.gE = intent;
        }

        protected void m821a(C0146a c0146a) {
            if (c0146a != null) {
                c0146a.m518a(this.gD, this.type, this.gE);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.ds.c */
    final class C1212c extends C0129b<OnPanoramaInfoLoadedListener> {
        private final ConnectionResult gD;
        private final Intent gE;
        final /* synthetic */ ds gF;

        public C1212c(ds dsVar, OnPanoramaInfoLoadedListener onPanoramaInfoLoadedListener, ConnectionResult connectionResult, Intent intent) {
            this.gF = dsVar;
            super(dsVar, onPanoramaInfoLoadedListener);
            this.gD = connectionResult;
            this.gE = intent;
        }

        protected void m823a(OnPanoramaInfoLoadedListener onPanoramaInfoLoadedListener) {
            if (onPanoramaInfoLoadedListener != null) {
                onPanoramaInfoLoadedListener.onPanoramaInfoLoaded(this.gD, this.gE);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.ds.b */
    final class C1589b extends C1208a {
        final /* synthetic */ ds gF;
        private final C0146a gG;
        private final OnPanoramaInfoLoadedListener gH;
        private final Uri gI;

        public C1589b(ds dsVar, OnPanoramaInfoLoadedListener onPanoramaInfoLoadedListener, Uri uri) {
            this.gF = dsVar;
            this.gG = null;
            this.gH = onPanoramaInfoLoadedListener;
            this.gI = uri;
        }

        public void m1141a(int i, Bundle bundle, int i2, Intent intent) {
            if (this.gI != null) {
                this.gF.getContext().revokeUriPermission(this.gI, 1);
            }
            PendingIntent pendingIntent = null;
            if (bundle != null) {
                pendingIntent = (PendingIntent) bundle.getParcelable("pendingIntent");
            }
            ConnectionResult connectionResult = new ConnectionResult(i, pendingIntent);
            if (this.gG != null) {
                this.gF.m944a(new C1211a(this.gF, this.gG, connectionResult, i2, intent));
            } else {
                this.gF.m944a(new C1212c(this.gF, this.gH, connectionResult, intent));
            }
        }
    }

    public ds(Context context, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        super(context, connectionCallbacks, onConnectionFailedListener, (String[]) null);
    }

    public dr m1142T(IBinder iBinder) {
        return C1210a.m820S(iBinder);
    }

    public void m1143a(C1589b c1589b, Uri uri, Bundle bundle, boolean z) {
        m955n();
        if (z) {
            getContext().grantUriPermission(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, uri, 1);
        }
        try {
            ((dr) m956o()).m343a(c1589b, uri, bundle, z);
        } catch (RemoteException e) {
            c1589b.m1141a(8, null, 0, null);
        }
    }

    protected void m1144a(C0137u c0137u, C1606d c1606d) throws RemoteException {
        c0137u.m476a(c1606d, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), new Bundle());
    }

    public void m1145a(OnPanoramaInfoLoadedListener onPanoramaInfoLoadedListener, Uri uri, boolean z) {
        m1143a(new C1589b(this, onPanoramaInfoLoadedListener, z ? uri : null), uri, null, z);
    }

    protected String m1146b() {
        return "com.google.android.gms.panorama.service.START";
    }

    public /* synthetic */ IInterface m1147c(IBinder iBinder) {
        return m1142T(iBinder);
    }

    protected String m1148c() {
        return "com.google.android.gms.panorama.internal.IPanoramaService";
    }
}
