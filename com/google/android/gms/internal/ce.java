package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.C1234p.C0129b;
import com.google.android.gms.internal.C1234p.C1606d;
import com.google.android.gms.internal.cb.C1165a;
import com.google.android.gms.internal.cc.C1167a;
import com.google.android.gms.location.LocationClient.OnAddGeofencesResultListener;
import com.google.android.gms.location.LocationClient.OnRemoveGeofencesResultListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.List;

public class ce extends C1234p<cc> {
    private final ch<cc> eE;
    private final cd eJ;
    private final String eK;

    /* renamed from: com.google.android.gms.internal.ce.a */
    final class C1168a extends C0129b<OnAddGeofencesResultListener> {
        private final String[] eL;
        final /* synthetic */ ce eM;
        private final int f81p;

        public C1168a(ce ceVar, OnAddGeofencesResultListener onAddGeofencesResultListener, int i, String[] strArr) {
            this.eM = ceVar;
            super(ceVar, onAddGeofencesResultListener);
            this.f81p = LocationStatusCodes.m502J(i);
            this.eL = strArr;
        }

        protected void m760a(OnAddGeofencesResultListener onAddGeofencesResultListener) {
            if (onAddGeofencesResultListener != null) {
                onAddGeofencesResultListener.onAddGeofencesResult(this.f81p, this.eL);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.ce.c */
    final class C1169c implements ch<cc> {
        final /* synthetic */ ce eM;

        private C1169c(ce ceVar) {
            this.eM = ceVar;
        }

        public cc az() {
            return (cc) this.eM.m956o();
        }

        public void m762n() {
            this.eM.m955n();
        }

        public /* synthetic */ IInterface m763o() {
            return az();
        }
    }

    /* renamed from: com.google.android.gms.internal.ce.d */
    final class C1170d extends C0129b<OnRemoveGeofencesResultListener> {
        private final String[] eL;
        final /* synthetic */ ce eM;
        private final int eP;
        private final PendingIntent mPendingIntent;
        private final int f82p;

        public C1170d(ce ceVar, int i, OnRemoveGeofencesResultListener onRemoveGeofencesResultListener, int i2, PendingIntent pendingIntent) {
            boolean z = true;
            this.eM = ceVar;
            super(ceVar, onRemoveGeofencesResultListener);
            if (i != 1) {
                z = false;
            }
            C0126n.m442a(z);
            this.eP = i;
            this.f82p = LocationStatusCodes.m502J(i2);
            this.mPendingIntent = pendingIntent;
            this.eL = null;
        }

        public C1170d(ce ceVar, int i, OnRemoveGeofencesResultListener onRemoveGeofencesResultListener, int i2, String[] strArr) {
            this.eM = ceVar;
            super(ceVar, onRemoveGeofencesResultListener);
            C0126n.m442a(i == 2);
            this.eP = i;
            this.f82p = LocationStatusCodes.m502J(i2);
            this.eL = strArr;
            this.mPendingIntent = null;
        }

        protected void m764a(OnRemoveGeofencesResultListener onRemoveGeofencesResultListener) {
            if (onRemoveGeofencesResultListener != null) {
                switch (this.eP) {
                    case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                        onRemoveGeofencesResultListener.onRemoveGeofencesByPendingIntentResult(this.f82p, this.mPendingIntent);
                    case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                        onRemoveGeofencesResultListener.onRemoveGeofencesByRequestIdsResult(this.f82p, this.eL);
                    default:
                        Log.wtf("LocationClientImpl", "Unsupported action: " + this.eP);
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.ce.b */
    final class C1587b extends C1165a {
        final /* synthetic */ ce eM;
        private final OnAddGeofencesResultListener eN;
        private final OnRemoveGeofencesResultListener eO;

        public C1587b(ce ceVar, OnAddGeofencesResultListener onAddGeofencesResultListener) {
            this.eM = ceVar;
            this.eN = onAddGeofencesResultListener;
            this.eO = null;
        }

        public C1587b(ce ceVar, OnRemoveGeofencesResultListener onRemoveGeofencesResultListener) {
            this.eM = ceVar;
            this.eO = onRemoveGeofencesResultListener;
            this.eN = null;
        }

        public void onAddGeofencesResult(int statusCode, String[] geofenceRequestIds) throws RemoteException {
            this.eM.m944a(new C1168a(this.eM, this.eN, statusCode, geofenceRequestIds));
        }

        public void onRemoveGeofencesByPendingIntentResult(int statusCode, PendingIntent pendingIntent) {
            this.eM.m944a(new C1170d(this.eM, 1, this.eO, statusCode, pendingIntent));
        }

        public void onRemoveGeofencesByRequestIdsResult(int statusCode, String[] geofenceRequestIds) {
            this.eM.m944a(new C1170d(this.eM, 2, this.eO, statusCode, geofenceRequestIds));
        }
    }

    public ce(Context context, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String str) {
        super(context, connectionCallbacks, onConnectionFailedListener, new String[0]);
        this.eE = new C1169c();
        this.eJ = new cd(context, this.eE);
        this.eK = str;
    }

    protected void m1133a(C0137u c0137u, C1606d c1606d) throws RemoteException {
        Bundle bundle = new Bundle();
        bundle.putString("client_name", this.eK);
        c0137u.m483e(c1606d, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), bundle);
    }

    public void addGeofences(List<cf> geofences, PendingIntent pendingIntent, OnAddGeofencesResultListener listener) {
        m955n();
        boolean z = geofences != null && geofences.size() > 0;
        C0142x.m493b(z, (Object) "At least one geofence must be specified.");
        C0142x.m492b((Object) pendingIntent, (Object) "PendingIntent must be specified.");
        C0142x.m492b((Object) listener, (Object) "OnAddGeofencesResultListener not provided.");
        if (listener == null) {
            cb cbVar = null;
        } else {
            Object c1587b = new C1587b(this, listener);
        }
        try {
            ((cc) m956o()).m288a(geofences, pendingIntent, cbVar, getContext().getPackageName());
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    protected String m1134b() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }

    protected /* synthetic */ IInterface m1135c(IBinder iBinder) {
        return m1137q(iBinder);
    }

    protected String m1136c() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }

    public void disconnect() {
        synchronized (this.eJ) {
            if (isConnected()) {
                this.eJ.removeAllListeners();
            }
            super.disconnect();
        }
    }

    public Location getLastLocation() {
        return this.eJ.getLastLocation();
    }

    protected cc m1137q(IBinder iBinder) {
        return C1167a.m759p(iBinder);
    }

    public void removeActivityUpdates(PendingIntent callbackIntent) {
        m955n();
        C0142x.m495d(callbackIntent);
        try {
            ((cc) m956o()).removeActivityUpdates(callbackIntent);
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeGeofences(PendingIntent pendingIntent, OnRemoveGeofencesResultListener listener) {
        m955n();
        C0142x.m492b((Object) pendingIntent, (Object) "PendingIntent must be specified.");
        C0142x.m492b((Object) listener, (Object) "OnRemoveGeofencesResultListener not provided.");
        if (listener == null) {
            cb cbVar = null;
        } else {
            Object c1587b = new C1587b(this, listener);
        }
        try {
            ((cc) m956o()).m283a(pendingIntent, cbVar, getContext().getPackageName());
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeGeofences(List<String> geofenceRequestIds, OnRemoveGeofencesResultListener listener) {
        m955n();
        boolean z = geofenceRequestIds != null && geofenceRequestIds.size() > 0;
        C0142x.m493b(z, (Object) "geofenceRequestIds can't be null nor empty.");
        C0142x.m492b((Object) listener, (Object) "OnRemoveGeofencesResultListener not provided.");
        String[] strArr = (String[]) geofenceRequestIds.toArray(new String[0]);
        if (listener == null) {
            cb cbVar = null;
        } else {
            Object c1587b = new C1587b(this, listener);
        }
        try {
            ((cc) m956o()).m289a(strArr, cbVar, getContext().getPackageName());
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeLocationUpdates(PendingIntent callbackIntent) {
        this.eJ.removeLocationUpdates(callbackIntent);
    }

    public void removeLocationUpdates(LocationListener listener) {
        this.eJ.removeLocationUpdates(listener);
    }

    public void requestActivityUpdates(long detectionIntervalMillis, PendingIntent callbackIntent) {
        boolean z = true;
        m955n();
        C0142x.m495d(callbackIntent);
        if (detectionIntervalMillis < 0) {
            z = false;
        }
        C0142x.m493b(z, (Object) "detectionIntervalMillis must be >= 0");
        try {
            ((cc) m956o()).m281a(detectionIntervalMillis, true, callbackIntent);
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void requestLocationUpdates(LocationRequest request, PendingIntent callbackIntent) {
        this.eJ.requestLocationUpdates(request, callbackIntent);
    }

    public void requestLocationUpdates(LocationRequest request, LocationListener listener) {
        requestLocationUpdates(request, listener, null);
    }

    public void requestLocationUpdates(LocationRequest request, LocationListener listener, Looper looper) {
        synchronized (this.eJ) {
            this.eJ.requestLocationUpdates(request, listener, looper);
        }
    }
}
