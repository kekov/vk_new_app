package com.google.android.gms.internal;

import android.database.CursorWindow;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

/* renamed from: com.google.android.gms.internal.l */
public class C0125l implements Creator<C1230k> {
    static void m439a(C1230k c1230k, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m102a(parcel, 1, c1230k.f115U, false);
        ad.m106c(parcel, LocationStatusCodes.GEOFENCE_NOT_AVAILABLE, c1230k.f114T);
        ad.m101a(parcel, 2, c1230k.f117W, i, false);
        ad.m106c(parcel, 3, c1230k.f121p);
        ad.m92a(parcel, 4, c1230k.f118X, false);
        ad.m87C(parcel, d);
    }

    public C1230k m440a(Parcel parcel) {
        C1230k c1230k = new C1230k();
        int c = ac.m58c(parcel);
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    c1230k.f115U = ac.m81w(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    c1230k.f117W = (CursorWindow[]) ac.m57b(parcel, b, CursorWindow.CREATOR);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    c1230k.f121p = ac.m63f(parcel, b);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    c1230k.f118X = ac.m72n(parcel, b);
                    break;
                case LocationStatusCodes.GEOFENCE_NOT_AVAILABLE /*1000*/:
                    c1230k.f114T = ac.m63f(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() != c) {
            throw new C0107a("Overread allowed size end=" + c, parcel);
        }
        c1230k.m923g();
        return c1230k;
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m440a(x0);
    }

    public C1230k[] m441f(int i) {
        return new C1230k[i];
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m441f(x0);
    }
}
