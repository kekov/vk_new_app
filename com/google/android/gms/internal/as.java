package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.aq.C1143a;
import com.google.android.gms.internal.aq.C1144b;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.ArrayList;

public class as implements Creator<C1143a> {
    static void m146a(C1143a c1143a, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, c1143a.versionCode);
        ad.m96a(parcel, 2, c1143a.className, false);
        ad.m105b(parcel, 3, c1143a.bH, false);
        ad.m87C(parcel, d);
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m147l(x0);
    }

    public C1143a m147l(Parcel parcel) {
        ArrayList arrayList = null;
        int c = ac.m58c(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    str = ac.m70l(parcel, b);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    arrayList = ac.m59c(parcel, b, C1144b.CREATOR);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new C1143a(i, str, arrayList);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m148r(x0);
    }

    public C1143a[] m148r(int i) {
        return new C1143a[i];
    }
}
