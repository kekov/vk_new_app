package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.eq.C1603g;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashSet;
import java.util.Set;

public class ep implements Creator<C1603g> {
    static void m412a(C1603g c1603g, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        Set by = c1603g.by();
        if (by.contains(Integer.valueOf(1))) {
            ad.m106c(parcel, 1, c1603g.m1244u());
        }
        if (by.contains(Integer.valueOf(2))) {
            ad.m96a(parcel, 2, c1603g.getDepartment(), true);
        }
        if (by.contains(Integer.valueOf(3))) {
            ad.m96a(parcel, 3, c1603g.getDescription(), true);
        }
        if (by.contains(Integer.valueOf(4))) {
            ad.m96a(parcel, 4, c1603g.getEndDate(), true);
        }
        if (by.contains(Integer.valueOf(5))) {
            ad.m96a(parcel, 5, c1603g.getLocation(), true);
        }
        if (by.contains(Integer.valueOf(6))) {
            ad.m96a(parcel, 6, c1603g.getName(), true);
        }
        if (by.contains(Integer.valueOf(7))) {
            ad.m99a(parcel, 7, c1603g.isPrimary());
        }
        if (by.contains(Integer.valueOf(8))) {
            ad.m96a(parcel, 8, c1603g.getStartDate(), true);
        }
        if (by.contains(Integer.valueOf(9))) {
            ad.m96a(parcel, 9, c1603g.getTitle(), true);
        }
        if (by.contains(Integer.valueOf(10))) {
            ad.m106c(parcel, 10, c1603g.getType());
        }
        ad.m87C(parcel, d);
    }

    public C1603g m413E(Parcel parcel) {
        int i = 0;
        String str = null;
        int c = ac.m58c(parcel);
        Set hashSet = new HashSet();
        String str2 = null;
        boolean z = false;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i2 = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    str7 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(2));
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    str6 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(3));
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    str5 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(4));
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    str4 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(5));
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    str3 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(6));
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    z = ac.m60c(parcel, b);
                    hashSet.add(Integer.valueOf(7));
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                    str2 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(8));
                    break;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    str = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(9));
                    break;
                case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                    i = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(10));
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new C1603g(hashSet, i2, str7, str6, str5, str4, str3, z, str2, str, i);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public C1603g[] m414Y(int i) {
        return new C1603g[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m413E(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m414Y(x0);
    }
}
