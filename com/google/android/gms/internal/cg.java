package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.location.LocationStatusCodes;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class cg implements Creator<cf> {
    static void m290a(cf cfVar, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m96a(parcel, 1, cfVar.getRequestId(), false);
        ad.m106c(parcel, LocationStatusCodes.GEOFENCE_NOT_AVAILABLE, cfVar.m772u());
        ad.m91a(parcel, 2, cfVar.getExpirationTime());
        ad.m98a(parcel, 3, cfVar.aA());
        ad.m89a(parcel, 4, cfVar.getLatitude());
        ad.m89a(parcel, 5, cfVar.getLongitude());
        ad.m90a(parcel, 6, cfVar.aB());
        ad.m106c(parcel, 7, cfVar.aC());
        ad.m87C(parcel, d);
    }

    public cf[] m291M(int i) {
        return new cf[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m292t(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m291M(x0);
    }

    public cf m292t(Parcel parcel) {
        double d = 0.0d;
        short s = (short) 0;
        int c = ac.m58c(parcel);
        String str = null;
        float f = 0.0f;
        long j = 0;
        double d2 = 0.0d;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    str = ac.m70l(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    j = ac.m64g(parcel, b);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    s = ac.m62e(parcel, b);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    d2 = ac.m67j(parcel, b);
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    d = ac.m67j(parcel, b);
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    f = ac.m66i(parcel, b);
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    i = ac.m63f(parcel, b);
                    break;
                case LocationStatusCodes.GEOFENCE_NOT_AVAILABLE /*1000*/:
                    i2 = ac.m63f(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new cf(i2, str, i, s, d2, d, f, j);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }
}
