package com.google.android.gms.internal;

import android.os.Parcel;
import java.util.ArrayList;

public class eb implements ae {
    public static final ec CREATOR;
    private final int f111T;
    private final String ch;
    private final ArrayList<ag> hO;
    private final ArrayList<ag> hP;
    private final boolean hQ;

    static {
        CREATOR = new ec();
    }

    public eb(int i, String str, ArrayList<ag> arrayList, ArrayList<ag> arrayList2, boolean z) {
        this.f111T = i;
        this.ch = str;
        this.hO = arrayList;
        this.hP = arrayList2;
        this.hQ = z;
    }

    public ArrayList<ag> bv() {
        return this.hO;
    }

    public ArrayList<ag> bw() {
        return this.hP;
    }

    public boolean bx() {
        return this.hQ;
    }

    public int describeContents() {
        return 0;
    }

    public String getDescription() {
        return this.ch;
    }

    public int m898u() {
        return this.f111T;
    }

    public void writeToParcel(Parcel out, int flags) {
        ec.m382a(this, out, flags);
    }
}
