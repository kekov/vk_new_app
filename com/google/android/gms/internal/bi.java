package com.google.android.gms.internal;

import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.internal.bl.C1158a;

public abstract class bi extends C1158a {
    public void m1050A(int i) {
    }

    public void m1051a(int i, int i2, String str) {
    }

    public void m1052a(int i, String str) {
    }

    public void m1053a(int i, String str, boolean z) {
    }

    public void m1054a(C1230k c1230k, C1230k c1230k2) {
    }

    public void m1055a(C1230k c1230k, String[] strArr) {
    }

    public void m1056b(C1230k c1230k) {
    }

    public void m1057b(C1230k c1230k, String[] strArr) {
    }

    public void m1058c(C1230k c1230k) {
    }

    public void m1059c(C1230k c1230k, String[] strArr) {
    }

    public void m1060d(C1230k c1230k) {
    }

    public void m1061d(C1230k c1230k, String[] strArr) {
    }

    public void m1062e(C1230k c1230k) {
    }

    public void m1063e(C1230k c1230k, String[] strArr) {
    }

    public void m1064f(C1230k c1230k) {
    }

    public void m1065f(C1230k c1230k, String[] strArr) {
    }

    public void m1066g(C1230k c1230k) {
    }

    public void m1067h(C1230k c1230k) {
    }

    public void m1068i(C1230k c1230k) {
    }

    public void m1069j(C1230k c1230k) {
    }

    public void m1070k(C1230k c1230k) {
    }

    public void m1071l(C1230k c1230k) {
    }

    public void m1072m(C1230k c1230k) {
    }

    public void m1073n(C1230k c1230k) {
    }

    public void m1074o(C1230k c1230k) {
    }

    public void onAchievementUpdated(int statusCode, String achievementId) {
    }

    public void onLeftRoom(int statusCode, String roomId) {
    }

    public void onRealTimeMessageReceived(RealTimeMessage message) {
    }

    public void onSignOutComplete() {
    }

    public void m1075p(C1230k c1230k) {
    }

    public void m1076q(C1230k c1230k) {
    }

    public void m1077r(C1230k c1230k) {
    }

    public void m1078s(C1230k c1230k) {
    }

    public void m1079t(C1230k c1230k) {
    }

    public void m1080u(C1230k c1230k) {
    }

    public void m1081v(C1230k c1230k) {
    }

    public void m1082w(C1230k c1230k) {
    }

    public void m1083x(int i) {
    }

    public void m1084y(int i) {
    }

    public void m1085z(int i) {
    }
}
