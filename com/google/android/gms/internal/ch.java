package com.google.android.gms.internal;

import android.os.IInterface;

public interface ch<T extends IInterface> {
    void m293n();

    T m294o();
}
