package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class au implements Creator<at> {
    static void m149a(at atVar, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, atVar.m625u());
        ad.m94a(parcel, 2, atVar.m620Y(), false);
        ad.m95a(parcel, 3, atVar.m621Z(), i, false);
        ad.m87C(parcel, d);
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m150m(x0);
    }

    public at m150m(Parcel parcel) {
        aq aqVar = null;
        int c = ac.m58c(parcel);
        int i = 0;
        Parcel parcel2 = null;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    parcel2 = ac.m83y(parcel, b);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    aqVar = (aq) ac.m52a(parcel, b, aq.CREATOR);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new at(i, parcel2, aqVar);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m151s(x0);
    }

    public at[] m151s(int i) {
        return new at[i];
    }
}
