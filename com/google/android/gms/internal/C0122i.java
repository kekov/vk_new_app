package com.google.android.gms.internal;

import com.google.android.gms.common.data.DataBuffer;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* renamed from: com.google.android.gms.internal.i */
public final class C0122i<T> implements Iterator<T> {
    private final DataBuffer<T> f32P;
    private int f33Q;

    public C0122i(DataBuffer<T> dataBuffer) {
        this.f32P = (DataBuffer) C0142x.m495d(dataBuffer);
        this.f33Q = -1;
    }

    public boolean hasNext() {
        return this.f33Q < this.f32P.getCount() + -1;
    }

    public T next() {
        if (hasNext()) {
            DataBuffer dataBuffer = this.f32P;
            int i = this.f33Q + 1;
            this.f33Q = i;
            return dataBuffer.get(i);
        }
        throw new NoSuchElementException("Cannot advance the iterator beyond " + this.f33Q);
    }

    public void remove() {
        throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
    }
}
