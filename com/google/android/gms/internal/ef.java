package com.google.android.gms.internal;

import android.os.Parcel;
import com.facebook.WebDialog;
import com.google.android.gms.internal.an.C1142a;
import com.google.android.gms.plus.model.moments.ItemScope;
import com.google.android.gms.plus.model.moments.Moment;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public final class ef extends an implements ae, Moment {
    public static final eg CREATOR;
    private static final HashMap<String, C1142a<?, ?>> hR;
    private final int f174T;
    private final Set<Integer> hS;
    private String iH;
    private String iN;
    private ed iQ;
    private ed iR;
    private String iw;

    static {
        CREATOR = new eg();
        hR = new HashMap();
        hR.put("id", C1142a.m579f("id", 2));
        hR.put("result", C1142a.m573a("result", 4, ed.class));
        hR.put("startDate", C1142a.m579f("startDate", 5));
        hR.put("target", C1142a.m573a("target", 6, ed.class));
        hR.put(WebDialog.DIALOG_PARAM_TYPE, C1142a.m579f(WebDialog.DIALOG_PARAM_TYPE, 7));
    }

    public ef() {
        this.f174T = 1;
        this.hS = new HashSet();
    }

    ef(Set<Integer> set, int i, String str, ed edVar, String str2, ed edVar2, String str3) {
        this.hS = set;
        this.f174T = i;
        this.iw = str;
        this.iQ = edVar;
        this.iH = str2;
        this.iR = edVar2;
        this.iN = str3;
    }

    public ef(Set<Integer> set, String str, ed edVar, String str2, ed edVar2, String str3) {
        this.hS = set;
        this.f174T = 1;
        this.iw = str;
        this.iQ = edVar;
        this.iH = str2;
        this.iR = edVar2;
        this.iN = str3;
    }

    public HashMap<String, C1142a<?, ?>> m1191G() {
        return hR;
    }

    protected boolean m1192a(C1142a c1142a) {
        return this.hS.contains(Integer.valueOf(c1142a.m587N()));
    }

    protected Object m1193b(C1142a c1142a) {
        switch (c1142a.m587N()) {
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return this.iw;
            case UserListView.TYPE_FAVE /*4*/:
                return this.iQ;
            case UserListView.TYPE_FOLLOWERS /*5*/:
                return this.iH;
            case UserListView.TYPE_POLL_VOTERS /*6*/:
                return this.iR;
            case UserListView.TYPE_FAVE_LINKS /*7*/:
                return this.iN;
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
        }
    }

    ed bP() {
        return this.iQ;
    }

    ed bQ() {
        return this.iR;
    }

    public ef bR() {
        return this;
    }

    Set<Integer> by() {
        return this.hS;
    }

    public int describeContents() {
        eg egVar = CREATOR;
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ef)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ef efVar = (ef) obj;
        for (C1142a c1142a : hR.values()) {
            if (m1192a(c1142a)) {
                if (!efVar.m1192a(c1142a)) {
                    return false;
                }
                if (!m1193b(c1142a).equals(efVar.m1193b(c1142a))) {
                    return false;
                }
            } else if (efVar.m1192a(c1142a)) {
                return false;
            }
        }
        return true;
    }

    public /* synthetic */ Object freeze() {
        return bR();
    }

    public String getId() {
        return this.iw;
    }

    public ItemScope getResult() {
        return this.iQ;
    }

    public String getStartDate() {
        return this.iH;
    }

    public ItemScope getTarget() {
        return this.iR;
    }

    public String getType() {
        return this.iN;
    }

    public boolean hasId() {
        return this.hS.contains(Integer.valueOf(2));
    }

    public boolean hasResult() {
        return this.hS.contains(Integer.valueOf(4));
    }

    public boolean hasStartDate() {
        return this.hS.contains(Integer.valueOf(5));
    }

    public boolean hasTarget() {
        return this.hS.contains(Integer.valueOf(6));
    }

    public boolean hasType() {
        return this.hS.contains(Integer.valueOf(7));
    }

    public int hashCode() {
        int i = 0;
        for (C1142a c1142a : hR.values()) {
            int hashCode;
            if (m1192a(c1142a)) {
                hashCode = m1193b(c1142a).hashCode() + (i + c1142a.m587N());
            } else {
                hashCode = i;
            }
            i = hashCode;
        }
        return i;
    }

    protected Object m1194j(String str) {
        return null;
    }

    protected boolean m1195k(String str) {
        return false;
    }

    int m1196u() {
        return this.f174T;
    }

    public void writeToParcel(Parcel out, int flags) {
        eg egVar = CREATOR;
        eg.m388a(this, out, flags);
    }
}
