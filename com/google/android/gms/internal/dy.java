package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.C0136t.C1236a;
import com.google.android.gms.internal.C1234p.C0129b;
import com.google.android.gms.internal.C1234p.C1232c;
import com.google.android.gms.internal.C1234p.C1606d;
import com.google.android.gms.internal.dx.C1216a;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusClient.C0147a;
import com.google.android.gms.plus.PlusClient.C0148b;
import com.google.android.gms.plus.PlusClient.OnAccessRevokedListener;
import com.google.android.gms.plus.PlusClient.OnMomentsLoadedListener;
import com.google.android.gms.plus.PlusClient.OnPeopleLoadedListener;
import com.google.android.gms.plus.PlusClient.OnPersonLoadedListener;
import com.google.android.gms.plus.model.moments.Moment;
import com.google.android.gms.plus.model.moments.MomentBuffer;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;

public class dy extends C1234p<dx> {
    private final String f167g;
    private final String[] gN;
    private final String ha;
    private final String hb;
    private Person hc;
    private final String[] hd;

    /* renamed from: com.google.android.gms.internal.dy.d */
    final class C1217d extends C0129b<C0147a> {
        final /* synthetic */ dy hf;
        private final ConnectionResult hg;
        private final ParcelFileDescriptor hk;

        public C1217d(dy dyVar, C0147a c0147a, ConnectionResult connectionResult, ParcelFileDescriptor parcelFileDescriptor) {
            this.hf = dyVar;
            super(dyVar, c0147a);
            this.hg = connectionResult;
            this.hk = parcelFileDescriptor;
        }

        public void m869a(C0147a c0147a) {
            if (c0147a != null) {
                c0147a.m522a(this.hg, this.hk);
                return;
            }
            try {
                this.hk.close();
            } catch (Throwable e) {
                Log.e("PlusClientImpl", "failed close", e);
            }
        }

        public void m871q() {
            super.m447q();
        }
    }

    /* renamed from: com.google.android.gms.internal.dy.h */
    final class C1218h extends C0129b<OnPersonLoadedListener> {
        final /* synthetic */ dy hf;
        private final ConnectionResult hg;
        private final Person hn;

        public C1218h(dy dyVar, OnPersonLoadedListener onPersonLoadedListener, ConnectionResult connectionResult, Person person) {
            this.hf = dyVar;
            super(dyVar, onPersonLoadedListener);
            this.hg = connectionResult;
            this.hn = person;
        }

        protected void m872a(OnPersonLoadedListener onPersonLoadedListener) {
            if (onPersonLoadedListener != null) {
                onPersonLoadedListener.onPersonLoaded(this.hg, this.hn);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.dy.k */
    final class C1219k extends C0129b<C0148b> {
        public final ConnectionResult gD;
        final /* synthetic */ dy hf;
        public final du hp;

        public C1219k(dy dyVar, C0148b c0148b, ConnectionResult connectionResult, du duVar) {
            this.hf = dyVar;
            super(dyVar, c0148b);
            this.gD = connectionResult;
            this.hp = duVar;
        }

        protected void m874a(C0148b c0148b) {
            if (c0148b != null) {
                c0148b.m523a(this.gD, this.hp);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.dy.m */
    final class C1220m extends C0129b<OnAccessRevokedListener> {
        final /* synthetic */ dy hf;
        private final ConnectionResult hg;

        public C1220m(dy dyVar, OnAccessRevokedListener onAccessRevokedListener, ConnectionResult connectionResult) {
            this.hf = dyVar;
            super(dyVar, onAccessRevokedListener);
            this.hg = connectionResult;
        }

        protected void m876a(OnAccessRevokedListener onAccessRevokedListener) {
            this.hf.disconnect();
            if (onAccessRevokedListener != null) {
                onAccessRevokedListener.onAccessRevoked(this.hg);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.dy.b */
    final class C1590b extends C1232c<OnMomentsLoadedListener> {
        final /* synthetic */ dy hf;
        private final ConnectionResult hg;
        private final String hh;
        private final String hi;

        public C1590b(dy dyVar, OnMomentsLoadedListener onMomentsLoadedListener, ConnectionResult connectionResult, C1230k c1230k, String str, String str2) {
            this.hf = dyVar;
            super(dyVar, onMomentsLoadedListener, c1230k);
            this.hg = connectionResult;
            this.hh = str;
            this.hi = str2;
        }

        protected void m1161a(OnMomentsLoadedListener onMomentsLoadedListener) {
            if (onMomentsLoadedListener != null) {
                onMomentsLoadedListener.onMomentsLoaded(this.hg, new MomentBuffer(this.O), this.hh, this.hi);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.dy.f */
    final class C1591f extends C1232c<OnPeopleLoadedListener> {
        final /* synthetic */ dy hf;
        private final ConnectionResult hg;
        private final String hh;

        public C1591f(dy dyVar, OnPeopleLoadedListener onPeopleLoadedListener, ConnectionResult connectionResult, C1230k c1230k, String str) {
            this.hf = dyVar;
            super(dyVar, onPeopleLoadedListener, c1230k);
            this.hg = connectionResult;
            this.hh = str;
        }

        protected void m1163a(OnPeopleLoadedListener onPeopleLoadedListener) {
            if (onPeopleLoadedListener != null) {
                onPeopleLoadedListener.onPeopleLoaded(this.hg, new PersonBuffer(this.O), this.hh);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.dy.i */
    public final class C1592i extends C1236a {
        private C1606d cL;
        final /* synthetic */ dy hf;

        public C1592i(dy dyVar, C1606d c1606d) {
            this.hf = dyVar;
            this.cL = c1606d;
        }

        public void m1165a(int i, IBinder iBinder, Bundle bundle) {
            if (i == 0 && bundle != null && bundle.containsKey("loaded_person")) {
                this.hf.hc = eq.m1257d(bundle.getByteArray("loaded_person"));
            }
            this.cL.m1264a(i, iBinder, bundle);
        }
    }

    /* renamed from: com.google.android.gms.internal.dy.a */
    final class C1635a extends dv {
        private final OnMomentsLoadedListener he;
        final /* synthetic */ dy hf;

        public C1635a(dy dyVar, OnMomentsLoadedListener onMomentsLoadedListener) {
            this.hf = dyVar;
            this.he = onMomentsLoadedListener;
        }

        public void m1304a(C1230k c1230k, String str, String str2) {
            if (c1230k.m925h() != null) {
                c1230k.m925h().getParcelable("pendingIntent");
            }
            this.hf.m944a(new C1590b(this.hf, this.he, new ConnectionResult(c1230k.getStatusCode(), null), c1230k, str, str2));
        }
    }

    /* renamed from: com.google.android.gms.internal.dy.c */
    final class C1636c extends dv {
        final /* synthetic */ dy hf;
        private final C0147a hj;

        public C1636c(dy dyVar, C0147a c0147a) {
            this.hf = dyVar;
            this.hj = c0147a;
        }

        public void m1305a(int i, Bundle bundle, ParcelFileDescriptor parcelFileDescriptor) {
            PendingIntent pendingIntent = null;
            if (bundle != null) {
                pendingIntent = (PendingIntent) bundle.getParcelable("pendingIntent");
            }
            this.hf.m944a(new C1217d(this.hf, this.hj, new ConnectionResult(i, pendingIntent), parcelFileDescriptor));
        }
    }

    /* renamed from: com.google.android.gms.internal.dy.e */
    final class C1637e extends dv {
        final /* synthetic */ dy hf;
        private final OnPeopleLoadedListener hl;

        public C1637e(dy dyVar, OnPeopleLoadedListener onPeopleLoadedListener) {
            this.hf = dyVar;
            this.hl = onPeopleLoadedListener;
        }

        public void m1306a(C1230k c1230k, String str) {
            if (c1230k.m925h() != null) {
                c1230k.m925h().getParcelable("pendingIntent");
            }
            this.hf.m944a(new C1591f(this.hf, this.hl, new ConnectionResult(c1230k.getStatusCode(), null), c1230k, str));
        }
    }

    /* renamed from: com.google.android.gms.internal.dy.g */
    final class C1638g extends dv {
        final /* synthetic */ dy hf;
        private final OnPersonLoadedListener hm;

        public C1638g(dy dyVar, OnPersonLoadedListener onPersonLoadedListener) {
            this.hf = dyVar;
            this.hm = onPersonLoadedListener;
        }

        public void m1307a(int i, Bundle bundle, at atVar) {
            ConnectionResult connectionResult = new ConnectionResult(i, bundle != null ? (PendingIntent) bundle.getParcelable("pendingIntent") : null);
            if (atVar == null) {
                this.hf.m944a(new C1218h(this.hf, this.hm, connectionResult, null));
            } else {
                this.hf.m944a(new C1218h(this.hf, this.hm, connectionResult, (eq) atVar.m622a(eq.CREATOR)));
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.dy.j */
    final class C1639j extends dv {
        final /* synthetic */ dy hf;
        private final C0148b ho;

        public C1639j(dy dyVar, C0148b c0148b) {
            this.hf = dyVar;
            this.ho = c0148b;
        }

        public void m1308a(int i, Bundle bundle, Bundle bundle2) {
            du duVar = null;
            ConnectionResult connectionResult = new ConnectionResult(i, bundle != null ? (PendingIntent) bundle.getParcelable("pendingIntent") : null);
            if (bundle2 != null) {
                duVar = new du(bundle2);
            }
            this.hf.m944a(new C1219k(this.hf, this.ho, connectionResult, duVar));
        }
    }

    /* renamed from: com.google.android.gms.internal.dy.l */
    final class C1640l extends dv {
        final /* synthetic */ dy hf;
        private final OnAccessRevokedListener hq;

        public C1640l(dy dyVar, OnAccessRevokedListener onAccessRevokedListener) {
            this.hf = dyVar;
            this.hq = onAccessRevokedListener;
        }

        public void m1309b(int i, Bundle bundle) {
            PendingIntent pendingIntent = null;
            if (bundle != null) {
                pendingIntent = (PendingIntent) bundle.getParcelable("pendingIntent");
            }
            this.hf.m944a(new C1220m(this.hf, this.hq, new ConnectionResult(i, pendingIntent)));
        }
    }

    public dy(Context context, String str, String str2, String str3, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String[] strArr, String[] strArr2, String[] strArr3) {
        super(context, connectionCallbacks, onConnectionFailedListener, strArr3);
        this.ha = str;
        this.hb = str2;
        this.f167g = str3;
        this.hd = strArr;
        this.gN = strArr2;
    }

    protected dx m1167W(IBinder iBinder) {
        return C1216a.m868V(iBinder);
    }

    protected void m1168a(C0137u c0137u, C1606d c1606d) throws RemoteException {
        Bundle bundle = new Bundle();
        bundle.putBoolean("skip_oob", false);
        bundle.putStringArray(PlusClient.KEY_REQUEST_VISIBLE_ACTIVITIES, this.hd);
        if (this.gN != null) {
            bundle.putStringArray("required_features", this.gN);
        }
        c0137u.m478a(new C1592i(this, c1606d), GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, this.ha, this.hb, m951j(), this.f167g, bundle);
    }

    public void m1169a(C0147a c0147a, Uri uri, int i) {
        m955n();
        Bundle bundle = new Bundle();
        bundle.putInt("bounding_box", i);
        dw c1636c = new C1636c(this, c0147a);
        try {
            ((dx) m956o()).m363a(c1636c, uri, bundle);
        } catch (RemoteException e) {
            c1636c.m1305a(8, null, null);
        }
    }

    public void m1170a(C0148b c0148b, String str) {
        m955n();
        Object c1639j = new C1639j(this, c0148b);
        try {
            ((dx) m956o()).m364a(c1639j, str);
        } catch (RemoteException e) {
            c1639j.m1308a(8, null, null);
        }
    }

    protected String m1171b() {
        return "com.google.android.gms.plus.service.START";
    }

    protected /* synthetic */ IInterface m1172c(IBinder iBinder) {
        return m1167W(iBinder);
    }

    protected String m1173c() {
        return "com.google.android.gms.plus.internal.IPlusService";
    }

    public void clearDefaultAccount() {
        m955n();
        try {
            this.hc = null;
            ((dx) m956o()).clearDefaultAccount();
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public String getAccountName() {
        m955n();
        try {
            return ((dx) m956o()).getAccountName();
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public Person getCurrentPerson() {
        m955n();
        return this.hc;
    }

    public void loadMoments(OnMomentsLoadedListener listener, int maxResults, String pageToken, Uri targetUrl, String type, String userId) {
        m955n();
        Object c1635a = new C1635a(this, listener);
        try {
            ((dx) m956o()).m362a(c1635a, maxResults, pageToken, targetUrl, type, userId);
        } catch (RemoteException e) {
            c1635a.m1304a(C1230k.m914e(8), null, null);
        }
    }

    public void loadPeople(OnPeopleLoadedListener listener, int collection, int orderBy, int maxResults, String pageToken) {
        m955n();
        dw c1637e = new C1637e(this, listener);
        try {
            ((dx) m956o()).m360a(c1637e, collection, orderBy, maxResults, pageToken);
        } catch (RemoteException e) {
            c1637e.m1306a(C1230k.m914e(8), null);
        }
    }

    public void loadPerson(OnPersonLoadedListener listener, String userId) {
        m955n();
        Object c1638g = new C1638g(this, listener);
        try {
            ((dx) m956o()).m376e(c1638g, userId);
        } catch (RemoteException e) {
            c1638g.m1307a(8, null, null);
        }
    }

    public void removeMoment(String momentId) {
        m955n();
        try {
            ((dx) m956o()).removeMoment(momentId);
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void revokeAccessAndDisconnect(OnAccessRevokedListener listener) {
        m955n();
        clearDefaultAccount();
        try {
            ((dx) m956o()).m373c(new C1640l(this, listener));
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void writeMoment(Moment moment) {
        m955n();
        try {
            ((dx) m956o()).m358a(at.m607a((ef) moment));
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }
}
