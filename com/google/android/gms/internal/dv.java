package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import com.google.android.gms.internal.dw.C1214a;
import java.util.List;

public abstract class dv extends C1214a {
    public void m1149B(String str) throws RemoteException {
    }

    public void m1150a(int i, Bundle bundle, Bundle bundle2) throws RemoteException {
    }

    public void m1151a(int i, Bundle bundle, ParcelFileDescriptor parcelFileDescriptor) throws RemoteException {
    }

    public void m1152a(int i, Bundle bundle, at atVar) {
    }

    public void m1153a(int i, Bundle bundle, String str, eb ebVar) {
    }

    public void m1154a(int i, Bundle bundle, String str, List<String> list, List<String> list2, List<String> list3) {
    }

    public void m1155a(C1230k c1230k, String str) {
    }

    public void m1156a(C1230k c1230k, String str, String str2) {
    }

    public void m1157b(int i, Bundle bundle) {
    }

    public void m1158b(int i, Bundle bundle, Bundle bundle2) {
    }

    public void m1159b(C1230k c1230k, String str) {
    }

    public void m1160c(int i, Bundle bundle) {
    }
}
