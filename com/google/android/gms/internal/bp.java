package com.google.android.gms.internal;

import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public final class bp {
    public static String m279B(int i) {
        switch (i) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return "PUBLIC";
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                return "SOCIAL";
            default:
                throw new IllegalArgumentException("Unknown leaderboard collection: " + i);
        }
    }
}
