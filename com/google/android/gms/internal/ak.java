package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.internal.an.C0108b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public final class ak implements ae, C0108b<String, Integer> {
    public static final al CREATOR;
    private final int f67T;
    private final HashMap<String, Integer> bp;
    private final HashMap<Integer, String> bq;
    private final ArrayList<C1141a> br;

    /* renamed from: com.google.android.gms.internal.ak.a */
    public static final class C1141a implements ae {
        public static final am CREATOR;
        final String bs;
        final int bt;
        final int versionCode;

        static {
            CREATOR = new am();
        }

        C1141a(int i, String str, int i2) {
            this.versionCode = i;
            this.bs = str;
            this.bt = i2;
        }

        C1141a(String str, int i) {
            this.versionCode = 1;
            this.bs = str;
            this.bt = i;
        }

        public int describeContents() {
            am amVar = CREATOR;
            return 0;
        }

        public void writeToParcel(Parcel out, int flags) {
            am amVar = CREATOR;
            am.m119a(this, out, flags);
        }
    }

    static {
        CREATOR = new al();
    }

    public ak() {
        this.f67T = 1;
        this.bp = new HashMap();
        this.bq = new HashMap();
        this.br = null;
    }

    ak(int i, ArrayList<C1141a> arrayList) {
        this.f67T = i;
        this.bp = new HashMap();
        this.bq = new HashMap();
        this.br = null;
        m564a((ArrayList) arrayList);
    }

    private void m564a(ArrayList<C1141a> arrayList) {
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            C1141a c1141a = (C1141a) it.next();
            m569b(c1141a.bs, c1141a.bt);
        }
    }

    ArrayList<C1141a> m565D() {
        ArrayList<C1141a> arrayList = new ArrayList();
        for (String str : this.bp.keySet()) {
            arrayList.add(new C1141a(str, ((Integer) this.bp.get(str)).intValue()));
        }
        return arrayList;
    }

    public int m566E() {
        return 7;
    }

    public int m567F() {
        return 0;
    }

    public String m568a(Integer num) {
        return (String) this.bq.get(num);
    }

    public ak m569b(String str, int i) {
        this.bp.put(str, Integer.valueOf(i));
        this.bq.put(Integer.valueOf(i), str);
        return this;
    }

    public int describeContents() {
        al alVar = CREATOR;
        return 0;
    }

    public /* synthetic */ Object m570e(Object obj) {
        return m568a((Integer) obj);
    }

    int m571u() {
        return this.f67T;
    }

    public void writeToParcel(Parcel out, int flags) {
        al alVar = CREATOR;
        al.m116a(this, out, flags);
    }
}
