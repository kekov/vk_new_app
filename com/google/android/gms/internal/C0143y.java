package com.google.android.gms.internal;

import android.net.Uri;
import android.net.Uri.Builder;
import com.google.android.gms.common.GooglePlayServicesUtil;

/* renamed from: com.google.android.gms.internal.y */
public final class C0143y {
    private static final Uri aY;

    static {
        aY = new Builder().scheme("android.resource").authority(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE).appendPath("drawable").build();
    }

    public static Uri m496i(String str) {
        C0142x.m492b((Object) str, (Object) "Resource name must not be null.");
        return aY.buildUpon().appendPath(str).build();
    }
}
