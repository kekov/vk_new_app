package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.TileOverlayOptions;

public class dh {
    public static void m327a(TileOverlayOptions tileOverlayOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, tileOverlayOptions.m1008u());
        ad.m93a(parcel, 2, tileOverlayOptions.ba(), false);
        ad.m99a(parcel, 3, tileOverlayOptions.isVisible());
        ad.m90a(parcel, 4, tileOverlayOptions.getZIndex());
        ad.m87C(parcel, d);
    }
}
