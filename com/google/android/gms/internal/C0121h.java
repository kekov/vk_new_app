package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: com.google.android.gms.internal.h */
public class C0121h implements ServiceConnection {
    boolean f30u;
    private final BlockingQueue<IBinder> f31v;

    public C0121h() {
        this.f30u = false;
        this.f31v = new LinkedBlockingQueue();
    }

    public IBinder m433d() throws InterruptedException {
        if (this.f30u) {
            throw new IllegalStateException();
        }
        this.f30u = true;
        return (IBinder) this.f31v.take();
    }

    public void onServiceConnected(ComponentName name, IBinder service) {
        try {
            this.f31v.put(service);
        } catch (InterruptedException e) {
        }
    }

    public void onServiceDisconnected(ComponentName name) {
    }
}
