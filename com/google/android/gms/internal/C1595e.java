package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.appstate.AppState;
import com.google.android.gms.appstate.AppStateBuffer;
import com.google.android.gms.appstate.AppStateClient;
import com.google.android.gms.appstate.OnSignOutCompleteListener;
import com.google.android.gms.appstate.OnStateDeletedListener;
import com.google.android.gms.appstate.OnStateListLoadedListener;
import com.google.android.gms.appstate.OnStateLoadedListener;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.internal.C0120g.C1228a;
import com.google.android.gms.internal.C1234p.C0129b;
import com.google.android.gms.internal.C1234p.C1232c;
import com.google.android.gms.internal.C1234p.C1606d;

/* renamed from: com.google.android.gms.internal.e */
public final class C1595e extends C1234p<C0120g> {
    private final String f171g;

    /* renamed from: com.google.android.gms.internal.e.b */
    final class C1223b extends C0129b<OnStateDeletedListener> {
        final /* synthetic */ C1595e f107o;
        private final int f108p;
        private final int f109q;

        public C1223b(C1595e c1595e, OnStateDeletedListener onStateDeletedListener, int i, int i2) {
            this.f107o = c1595e;
            super(c1595e, onStateDeletedListener);
            this.f108p = i;
            this.f109q = i2;
        }

        public void m894a(OnStateDeletedListener onStateDeletedListener) {
            onStateDeletedListener.onStateDeleted(this.f108p, this.f109q);
        }
    }

    /* renamed from: com.google.android.gms.internal.e.h */
    final class C1224h extends C0129b<OnSignOutCompleteListener> {
        final /* synthetic */ C1595e f110o;

        public C1224h(C1595e c1595e, OnSignOutCompleteListener onSignOutCompleteListener) {
            this.f110o = c1595e;
            super(c1595e, onSignOutCompleteListener);
        }

        public void m896a(OnSignOutCompleteListener onSignOutCompleteListener) {
            onSignOutCompleteListener.onSignOutComplete();
        }
    }

    /* renamed from: com.google.android.gms.internal.e.d */
    final class C1593d extends C1232c<OnStateListLoadedListener> {
        final /* synthetic */ C1595e f168o;

        public C1593d(C1595e c1595e, OnStateListLoadedListener onStateListLoadedListener, C1230k c1230k) {
            this.f168o = c1595e;
            super(c1595e, onStateListLoadedListener, c1230k);
        }

        public void m1174a(OnStateListLoadedListener onStateListLoadedListener) {
            onStateListLoadedListener.onStateListLoaded(this.O.getStatusCode(), new AppStateBuffer(this.O));
        }
    }

    /* renamed from: com.google.android.gms.internal.e.f */
    final class C1594f extends C1232c<OnStateLoadedListener> {
        final /* synthetic */ C1595e f169o;
        private final int f170q;

        public C1594f(C1595e c1595e, OnStateLoadedListener onStateLoadedListener, int i, C1230k c1230k) {
            this.f169o = c1595e;
            super(c1595e, onStateLoadedListener, c1230k);
            this.f170q = i;
        }

        public void m1176a(OnStateLoadedListener onStateLoadedListener) {
            byte[] bArr = null;
            AppStateBuffer appStateBuffer = new AppStateBuffer(this.O);
            try {
                String conflictVersion;
                byte[] localData;
                if (appStateBuffer.getCount() > 0) {
                    AppState appState = appStateBuffer.get(0);
                    conflictVersion = appState.getConflictVersion();
                    localData = appState.getLocalData();
                    bArr = appState.getConflictData();
                } else {
                    localData = null;
                    conflictVersion = null;
                }
                appStateBuffer.close();
                int statusCode = this.O.getStatusCode();
                if (statusCode == AppStateClient.STATUS_WRITE_OUT_OF_DATE_VERSION) {
                    onStateLoadedListener.onStateConflict(this.f170q, conflictVersion, localData, bArr);
                } else {
                    onStateLoadedListener.onStateLoaded(statusCode, this.f170q, localData);
                }
            } catch (Throwable th) {
                appStateBuffer.close();
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.e.a */
    final class C1641a extends C1588d {
        private final OnStateDeletedListener f188n;
        final /* synthetic */ C1595e f189o;

        public C1641a(C1595e c1595e, OnStateDeletedListener onStateDeletedListener) {
            this.f189o = c1595e;
            this.f188n = (OnStateDeletedListener) C0142x.m492b((Object) onStateDeletedListener, (Object) "Listener must not be null");
        }

        public void onStateDeleted(int statusCode, int stateKey) {
            this.f189o.m944a(new C1223b(this.f189o, this.f188n, statusCode, stateKey));
        }
    }

    /* renamed from: com.google.android.gms.internal.e.c */
    final class C1642c extends C1588d {
        final /* synthetic */ C1595e f190o;
        private final OnStateListLoadedListener f191r;

        public C1642c(C1595e c1595e, OnStateListLoadedListener onStateListLoadedListener) {
            this.f190o = c1595e;
            this.f191r = (OnStateListLoadedListener) C0142x.m492b((Object) onStateListLoadedListener, (Object) "Listener must not be null");
        }

        public void m1310a(C1230k c1230k) {
            this.f190o.m944a(new C1593d(this.f190o, this.f191r, c1230k));
        }
    }

    /* renamed from: com.google.android.gms.internal.e.e */
    final class C1643e extends C1588d {
        final /* synthetic */ C1595e f192o;
        private final OnStateLoadedListener f193s;

        public C1643e(C1595e c1595e, OnStateLoadedListener onStateLoadedListener) {
            this.f192o = c1595e;
            this.f193s = (OnStateLoadedListener) C0142x.m492b((Object) onStateLoadedListener, (Object) "Listener must not be null");
        }

        public void m1311a(int i, C1230k c1230k) {
            this.f192o.m944a(new C1594f(this.f192o, this.f193s, i, c1230k));
        }
    }

    /* renamed from: com.google.android.gms.internal.e.g */
    final class C1644g extends C1588d {
        final /* synthetic */ C1595e f194o;
        private final OnSignOutCompleteListener f195t;

        public C1644g(C1595e c1595e, OnSignOutCompleteListener onSignOutCompleteListener) {
            this.f194o = c1595e;
            this.f195t = (OnSignOutCompleteListener) C0142x.m492b((Object) onSignOutCompleteListener, (Object) "Listener must not be null");
        }

        public void onSignOutComplete() {
            this.f194o.m944a(new C1224h(this.f194o, this.f195t));
        }
    }

    public C1595e(Context context, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String str, String[] strArr) {
        super(context, connectionCallbacks, onConnectionFailedListener, strArr);
        this.f171g = (String) C0142x.m495d(str);
    }

    public void m1178a(OnStateLoadedListener onStateLoadedListener, int i, byte[] bArr) {
        if (onStateLoadedListener == null) {
            C0119f c0119f = null;
        } else {
            Object c1643e = new C1643e(this, onStateLoadedListener);
        }
        try {
            ((C0120g) m956o()).m429a(c0119f, i, bArr);
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
        }
    }

    protected void m1179a(C0137u c0137u, C1606d c1606d) throws RemoteException {
        c0137u.m477a(c1606d, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), this.f171g, m951j());
    }

    protected void m1180a(String... strArr) {
        boolean z = false;
        for (String equals : strArr) {
            if (equals.equals(Scopes.APP_STATE)) {
                z = true;
            }
        }
        C0142x.m490a(z, String.format("AppStateClient requires %s to function.", new Object[]{Scopes.APP_STATE}));
    }

    protected C0120g m1181b(IBinder iBinder) {
        return C1228a.m910e(iBinder);
    }

    protected String m1182b() {
        return "com.google.android.gms.appstate.service.START";
    }

    protected /* synthetic */ IInterface m1183c(IBinder iBinder) {
        return m1181b(iBinder);
    }

    protected String m1184c() {
        return "com.google.android.gms.appstate.internal.IAppStateService";
    }

    public void deleteState(OnStateDeletedListener listener, int stateKey) {
        try {
            ((C0120g) m956o()).m431b(new C1641a(this, listener), stateKey);
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
        }
    }

    public int getMaxNumKeys() {
        try {
            return ((C0120g) m956o()).getMaxNumKeys();
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
            return 2;
        }
    }

    public int getMaxStateSize() {
        try {
            return ((C0120g) m956o()).getMaxStateSize();
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
            return 2;
        }
    }

    public void listStates(OnStateListLoadedListener listener) {
        try {
            ((C0120g) m956o()).m426a(new C1642c(this, listener));
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
        }
    }

    public void loadState(OnStateLoadedListener listener, int stateKey) {
        try {
            ((C0120g) m956o()).m427a(new C1643e(this, listener), stateKey);
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
        }
    }

    public void resolveState(OnStateLoadedListener listener, int stateKey, String resolvedVersion, byte[] resolvedData) {
        try {
            ((C0120g) m956o()).m428a(new C1643e(this, listener), stateKey, resolvedVersion, resolvedData);
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
        }
    }

    public void signOut(OnSignOutCompleteListener listener) {
        if (listener == null) {
            C0119f c0119f = null;
        } else {
            Object c1644g = new C1644g(this, listener);
        }
        try {
            ((C0120g) m956o()).m430b(c0119f);
        } catch (RemoteException e) {
            Log.w("AppStateClient", "service died");
        }
    }
}
