package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.LatLng;

public class dc {
    public static void m322a(LatLng latLng, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, latLng.m995u());
        ad.m89a(parcel, 2, latLng.latitude);
        ad.m89a(parcel, 3, latLng.longitude);
        ad.m87C(parcel, d);
    }
}
