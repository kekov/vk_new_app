package com.google.android.gms.internal;

import android.os.Parcel;
import android.support.v4.media.TransportMediator;
import com.facebook.WebDialog;
import com.google.android.gms.internal.an.C1142a;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.Person.AgeRange;
import com.google.android.gms.plus.model.people.Person.Cover;
import com.google.android.gms.plus.model.people.Person.Cover.CoverInfo;
import com.google.android.gms.plus.model.people.Person.Cover.CoverPhoto;
import com.google.android.gms.plus.model.people.Person.Emails;
import com.google.android.gms.plus.model.people.Person.Image;
import com.google.android.gms.plus.model.people.Person.Name;
import com.google.android.gms.plus.model.people.Person.Organizations;
import com.google.android.gms.plus.model.people.Person.PlacesLived;
import com.google.android.gms.plus.model.people.Person.Urls;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.providers.LangProvider.StringKeys;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.hockeyapp.android.Strings;

public final class eq extends an implements ae, Person {
    public static final er CREATOR;
    private static final HashMap<String, C1142a<?, ?>> hR;
    private final int f185T;
    private String bm;
    private String hE;
    private final Set<Integer> hS;
    private String iT;
    private C1596a iU;
    private String iV;
    private String iW;
    private int iX;
    private C1599b iY;
    private String iZ;
    private String iw;
    private List<C1600c> ja;
    private String jb;
    private int jc;
    private boolean jd;
    private C1601d je;
    private boolean jf;
    private String jg;
    private C1602e jh;
    private String ji;
    private int jj;
    private List<C1603g> jk;
    private List<C1604h> jl;
    private int jm;
    private int jn;
    private String jo;
    private List<C1605i> jp;
    private boolean jq;

    /* renamed from: com.google.android.gms.internal.eq.f */
    public static class C0118f {
        public static int m415C(String str) {
            if (str.equals("person")) {
                return 0;
            }
            if (str.equals("page")) {
                return 1;
            }
            throw new IllegalArgumentException("Unknown objectType string: " + str);
        }
    }

    /* renamed from: com.google.android.gms.internal.eq.a */
    public static final class C1596a extends an implements ae, AgeRange {
        public static final ei CREATOR;
        private static final HashMap<String, C1142a<?, ?>> hR;
        private final int f175T;
        private final Set<Integer> hS;
        private int jr;
        private int js;

        static {
            CREATOR = new ei();
            hR = new HashMap();
            hR.put("max", C1142a.m575c("max", 2));
            hR.put("min", C1142a.m575c("min", 3));
        }

        public C1596a() {
            this.f175T = 1;
            this.hS = new HashSet();
        }

        C1596a(Set<Integer> set, int i, int i2, int i3) {
            this.hS = set;
            this.f175T = i;
            this.jr = i2;
            this.js = i3;
        }

        public HashMap<String, C1142a<?, ?>> m1197G() {
            return hR;
        }

        protected boolean m1198a(C1142a c1142a) {
            return this.hS.contains(Integer.valueOf(c1142a.m587N()));
        }

        protected Object m1199b(C1142a c1142a) {
            switch (c1142a.m587N()) {
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return Integer.valueOf(this.jr);
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return Integer.valueOf(this.js);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
            }
        }

        Set<Integer> by() {
            return this.hS;
        }

        public C1596a cd() {
            return this;
        }

        public int describeContents() {
            ei eiVar = CREATOR;
            return 0;
        }

        public /* synthetic */ Object freeze() {
            return cd();
        }

        public int getMax() {
            return this.jr;
        }

        public int getMin() {
            return this.js;
        }

        public boolean hasMax() {
            return this.hS.contains(Integer.valueOf(2));
        }

        public boolean hasMin() {
            return this.hS.contains(Integer.valueOf(3));
        }

        protected Object m1200j(String str) {
            return null;
        }

        protected boolean m1201k(String str) {
            return false;
        }

        int m1202u() {
            return this.f175T;
        }

        public void writeToParcel(Parcel out, int flags) {
            ei eiVar = CREATOR;
            ei.m391a(this, out, flags);
        }
    }

    /* renamed from: com.google.android.gms.internal.eq.b */
    public static final class C1599b extends an implements ae, Cover {
        public static final ej CREATOR;
        private static final HashMap<String, C1142a<?, ?>> hR;
        private final int f178T;
        private final Set<Integer> hS;
        private C1597a jt;
        private C1598b ju;
        private int jv;

        /* renamed from: com.google.android.gms.internal.eq.b.a */
        public static final class C1597a extends an implements ae, CoverInfo {
            public static final ek CREATOR;
            private static final HashMap<String, C1142a<?, ?>> hR;
            private final int f176T;
            private final Set<Integer> hS;
            private int jw;
            private int jx;

            static {
                CREATOR = new ek();
                hR = new HashMap();
                hR.put("leftImageOffset", C1142a.m575c("leftImageOffset", 2));
                hR.put("topImageOffset", C1142a.m575c("topImageOffset", 3));
            }

            public C1597a() {
                this.f176T = 1;
                this.hS = new HashSet();
            }

            C1597a(Set<Integer> set, int i, int i2, int i3) {
                this.hS = set;
                this.f176T = i;
                this.jw = i2;
                this.jx = i3;
            }

            public HashMap<String, C1142a<?, ?>> m1203G() {
                return hR;
            }

            protected boolean m1204a(C1142a c1142a) {
                return this.hS.contains(Integer.valueOf(c1142a.m587N()));
            }

            protected Object m1205b(C1142a c1142a) {
                switch (c1142a.m587N()) {
                    case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                        return Integer.valueOf(this.jw);
                    case Group.ADMIN_LEVEL_ADMIN /*3*/:
                        return Integer.valueOf(this.jx);
                    default:
                        throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
                }
            }

            Set<Integer> by() {
                return this.hS;
            }

            public C1597a ch() {
                return this;
            }

            public int describeContents() {
                ek ekVar = CREATOR;
                return 0;
            }

            public boolean equals(Object obj) {
                if (!(obj instanceof C1597a)) {
                    return false;
                }
                if (this == obj) {
                    return true;
                }
                C1597a c1597a = (C1597a) obj;
                for (C1142a c1142a : hR.values()) {
                    if (m1204a(c1142a)) {
                        if (!c1597a.m1204a(c1142a)) {
                            return false;
                        }
                        if (!m1205b(c1142a).equals(c1597a.m1205b(c1142a))) {
                            return false;
                        }
                    } else if (c1597a.m1204a(c1142a)) {
                        return false;
                    }
                }
                return true;
            }

            public /* synthetic */ Object freeze() {
                return ch();
            }

            public int getLeftImageOffset() {
                return this.jw;
            }

            public int getTopImageOffset() {
                return this.jx;
            }

            public boolean hasLeftImageOffset() {
                return this.hS.contains(Integer.valueOf(2));
            }

            public boolean hasTopImageOffset() {
                return this.hS.contains(Integer.valueOf(3));
            }

            public int hashCode() {
                int i = 0;
                for (C1142a c1142a : hR.values()) {
                    int hashCode;
                    if (m1204a(c1142a)) {
                        hashCode = m1205b(c1142a).hashCode() + (i + c1142a.m587N());
                    } else {
                        hashCode = i;
                    }
                    i = hashCode;
                }
                return i;
            }

            protected Object m1206j(String str) {
                return null;
            }

            protected boolean m1207k(String str) {
                return false;
            }

            int m1208u() {
                return this.f176T;
            }

            public void writeToParcel(Parcel out, int flags) {
                ek ekVar = CREATOR;
                ek.m397a(this, out, flags);
            }
        }

        /* renamed from: com.google.android.gms.internal.eq.b.b */
        public static final class C1598b extends an implements ae, CoverPhoto {
            public static final el CREATOR;
            private static final HashMap<String, C1142a<?, ?>> hR;
            private final int f177T;
            private int gA;
            private int gB;
            private String hE;
            private final Set<Integer> hS;

            static {
                CREATOR = new el();
                hR = new HashMap();
                hR.put("height", C1142a.m575c("height", 2));
                hR.put(PlusShare.KEY_CALL_TO_ACTION_URL, C1142a.m579f(PlusShare.KEY_CALL_TO_ACTION_URL, 3));
                hR.put("width", C1142a.m575c("width", 4));
            }

            public C1598b() {
                this.f177T = 1;
                this.hS = new HashSet();
            }

            C1598b(Set<Integer> set, int i, int i2, String str, int i3) {
                this.hS = set;
                this.f177T = i;
                this.gB = i2;
                this.hE = str;
                this.gA = i3;
            }

            public HashMap<String, C1142a<?, ?>> m1209G() {
                return hR;
            }

            protected boolean m1210a(C1142a c1142a) {
                return this.hS.contains(Integer.valueOf(c1142a.m587N()));
            }

            protected Object m1211b(C1142a c1142a) {
                switch (c1142a.m587N()) {
                    case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                        return Integer.valueOf(this.gB);
                    case Group.ADMIN_LEVEL_ADMIN /*3*/:
                        return this.hE;
                    case UserListView.TYPE_FAVE /*4*/:
                        return Integer.valueOf(this.gA);
                    default:
                        throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
                }
            }

            Set<Integer> by() {
                return this.hS;
            }

            public C1598b ci() {
                return this;
            }

            public int describeContents() {
                el elVar = CREATOR;
                return 0;
            }

            public boolean equals(Object obj) {
                if (!(obj instanceof C1598b)) {
                    return false;
                }
                if (this == obj) {
                    return true;
                }
                C1598b c1598b = (C1598b) obj;
                for (C1142a c1142a : hR.values()) {
                    if (m1210a(c1142a)) {
                        if (!c1598b.m1210a(c1142a)) {
                            return false;
                        }
                        if (!m1211b(c1142a).equals(c1598b.m1211b(c1142a))) {
                            return false;
                        }
                    } else if (c1598b.m1210a(c1142a)) {
                        return false;
                    }
                }
                return true;
            }

            public /* synthetic */ Object freeze() {
                return ci();
            }

            public int getHeight() {
                return this.gB;
            }

            public String getUrl() {
                return this.hE;
            }

            public int getWidth() {
                return this.gA;
            }

            public boolean hasHeight() {
                return this.hS.contains(Integer.valueOf(2));
            }

            public boolean hasUrl() {
                return this.hS.contains(Integer.valueOf(3));
            }

            public boolean hasWidth() {
                return this.hS.contains(Integer.valueOf(4));
            }

            public int hashCode() {
                int i = 0;
                for (C1142a c1142a : hR.values()) {
                    int hashCode;
                    if (m1210a(c1142a)) {
                        hashCode = m1211b(c1142a).hashCode() + (i + c1142a.m587N());
                    } else {
                        hashCode = i;
                    }
                    i = hashCode;
                }
                return i;
            }

            protected Object m1212j(String str) {
                return null;
            }

            protected boolean m1213k(String str) {
                return false;
            }

            int m1214u() {
                return this.f177T;
            }

            public void writeToParcel(Parcel out, int flags) {
                el elVar = CREATOR;
                el.m400a(this, out, flags);
            }
        }

        static {
            CREATOR = new ej();
            hR = new HashMap();
            hR.put("coverInfo", C1142a.m573a("coverInfo", 2, C1597a.class));
            hR.put("coverPhoto", C1142a.m573a("coverPhoto", 3, C1598b.class));
            hR.put("layout", C1142a.m572a("layout", 4, new ak().m569b("banner", 0), false));
        }

        public C1599b() {
            this.f178T = 1;
            this.hS = new HashSet();
        }

        C1599b(Set<Integer> set, int i, C1597a c1597a, C1598b c1598b, int i2) {
            this.hS = set;
            this.f178T = i;
            this.jt = c1597a;
            this.ju = c1598b;
            this.jv = i2;
        }

        public HashMap<String, C1142a<?, ?>> m1215G() {
            return hR;
        }

        protected boolean m1216a(C1142a c1142a) {
            return this.hS.contains(Integer.valueOf(c1142a.m587N()));
        }

        protected Object m1217b(C1142a c1142a) {
            switch (c1142a.m587N()) {
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return this.jt;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return this.ju;
                case UserListView.TYPE_FAVE /*4*/:
                    return Integer.valueOf(this.jv);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
            }
        }

        Set<Integer> by() {
            return this.hS;
        }

        C1597a ce() {
            return this.jt;
        }

        C1598b cf() {
            return this.ju;
        }

        public C1599b cg() {
            return this;
        }

        public int describeContents() {
            ej ejVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1599b)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            C1599b c1599b = (C1599b) obj;
            for (C1142a c1142a : hR.values()) {
                if (m1216a(c1142a)) {
                    if (!c1599b.m1216a(c1142a)) {
                        return false;
                    }
                    if (!m1217b(c1142a).equals(c1599b.m1217b(c1142a))) {
                        return false;
                    }
                } else if (c1599b.m1216a(c1142a)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return cg();
        }

        public CoverInfo getCoverInfo() {
            return this.jt;
        }

        public CoverPhoto getCoverPhoto() {
            return this.ju;
        }

        public int getLayout() {
            return this.jv;
        }

        public boolean hasCoverInfo() {
            return this.hS.contains(Integer.valueOf(2));
        }

        public boolean hasCoverPhoto() {
            return this.hS.contains(Integer.valueOf(3));
        }

        public boolean hasLayout() {
            return this.hS.contains(Integer.valueOf(4));
        }

        public int hashCode() {
            int i = 0;
            for (C1142a c1142a : hR.values()) {
                int hashCode;
                if (m1216a(c1142a)) {
                    hashCode = m1217b(c1142a).hashCode() + (i + c1142a.m587N());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        protected Object m1218j(String str) {
            return null;
        }

        protected boolean m1219k(String str) {
            return false;
        }

        int m1220u() {
            return this.f178T;
        }

        public void writeToParcel(Parcel out, int flags) {
            ej ejVar = CREATOR;
            ej.m394a(this, out, flags);
        }
    }

    /* renamed from: com.google.android.gms.internal.eq.c */
    public static final class C1600c extends an implements ae, Emails {
        public static final em CREATOR;
        private static final HashMap<String, C1142a<?, ?>> hR;
        private final int f179T;
        private int bi;
        private final Set<Integer> hS;
        private boolean jy;
        private String mValue;

        static {
            CREATOR = new em();
            hR = new HashMap();
            hR.put("primary", C1142a.m578e("primary", 2));
            hR.put(WebDialog.DIALOG_PARAM_TYPE, C1142a.m572a(WebDialog.DIALOG_PARAM_TYPE, 3, new ak().m569b("home", 0).m569b("work", 1).m569b("other", 2), false));
            hR.put("value", C1142a.m579f("value", 4));
        }

        public C1600c() {
            this.f179T = 1;
            this.hS = new HashSet();
        }

        C1600c(Set<Integer> set, int i, boolean z, int i2, String str) {
            this.hS = set;
            this.f179T = i;
            this.jy = z;
            this.bi = i2;
            this.mValue = str;
        }

        public HashMap<String, C1142a<?, ?>> m1221G() {
            return hR;
        }

        protected boolean m1222a(C1142a c1142a) {
            return this.hS.contains(Integer.valueOf(c1142a.m587N()));
        }

        protected Object m1223b(C1142a c1142a) {
            switch (c1142a.m587N()) {
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return Boolean.valueOf(this.jy);
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return Integer.valueOf(this.bi);
                case UserListView.TYPE_FAVE /*4*/:
                    return this.mValue;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
            }
        }

        Set<Integer> by() {
            return this.hS;
        }

        public C1600c cj() {
            return this;
        }

        public int describeContents() {
            em emVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1600c)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            C1600c c1600c = (C1600c) obj;
            for (C1142a c1142a : hR.values()) {
                if (m1222a(c1142a)) {
                    if (!c1600c.m1222a(c1142a)) {
                        return false;
                    }
                    if (!m1223b(c1142a).equals(c1600c.m1223b(c1142a))) {
                        return false;
                    }
                } else if (c1600c.m1222a(c1142a)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return cj();
        }

        public int getType() {
            return this.bi;
        }

        public String getValue() {
            return this.mValue;
        }

        public boolean hasPrimary() {
            return this.hS.contains(Integer.valueOf(2));
        }

        public boolean hasType() {
            return this.hS.contains(Integer.valueOf(3));
        }

        public boolean hasValue() {
            return this.hS.contains(Integer.valueOf(4));
        }

        public int hashCode() {
            int i = 0;
            for (C1142a c1142a : hR.values()) {
                int hashCode;
                if (m1222a(c1142a)) {
                    hashCode = m1223b(c1142a).hashCode() + (i + c1142a.m587N());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        public boolean isPrimary() {
            return this.jy;
        }

        protected Object m1224j(String str) {
            return null;
        }

        protected boolean m1225k(String str) {
            return false;
        }

        int m1226u() {
            return this.f179T;
        }

        public void writeToParcel(Parcel out, int flags) {
            em emVar = CREATOR;
            em.m403a(this, out, flags);
        }
    }

    /* renamed from: com.google.android.gms.internal.eq.d */
    public static final class C1601d extends an implements ae, Image {
        public static final en CREATOR;
        private static final HashMap<String, C1142a<?, ?>> hR;
        private final int f180T;
        private String hE;
        private final Set<Integer> hS;

        static {
            CREATOR = new en();
            hR = new HashMap();
            hR.put(PlusShare.KEY_CALL_TO_ACTION_URL, C1142a.m579f(PlusShare.KEY_CALL_TO_ACTION_URL, 2));
        }

        public C1601d() {
            this.f180T = 1;
            this.hS = new HashSet();
        }

        public C1601d(String str) {
            this.hS = new HashSet();
            this.f180T = 1;
            this.hE = str;
            this.hS.add(Integer.valueOf(2));
        }

        C1601d(Set<Integer> set, int i, String str) {
            this.hS = set;
            this.f180T = i;
            this.hE = str;
        }

        public HashMap<String, C1142a<?, ?>> m1227G() {
            return hR;
        }

        protected boolean m1228a(C1142a c1142a) {
            return this.hS.contains(Integer.valueOf(c1142a.m587N()));
        }

        protected Object m1229b(C1142a c1142a) {
            switch (c1142a.m587N()) {
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return this.hE;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
            }
        }

        Set<Integer> by() {
            return this.hS;
        }

        public C1601d ck() {
            return this;
        }

        public int describeContents() {
            en enVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1601d)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            C1601d c1601d = (C1601d) obj;
            for (C1142a c1142a : hR.values()) {
                if (m1228a(c1142a)) {
                    if (!c1601d.m1228a(c1142a)) {
                        return false;
                    }
                    if (!m1229b(c1142a).equals(c1601d.m1229b(c1142a))) {
                        return false;
                    }
                } else if (c1601d.m1228a(c1142a)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return ck();
        }

        public String getUrl() {
            return this.hE;
        }

        public boolean hasUrl() {
            return this.hS.contains(Integer.valueOf(2));
        }

        public int hashCode() {
            int i = 0;
            for (C1142a c1142a : hR.values()) {
                int hashCode;
                if (m1228a(c1142a)) {
                    hashCode = m1229b(c1142a).hashCode() + (i + c1142a.m587N());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        protected Object m1230j(String str) {
            return null;
        }

        protected boolean m1231k(String str) {
            return false;
        }

        int m1232u() {
            return this.f180T;
        }

        public void writeToParcel(Parcel out, int flags) {
            en enVar = CREATOR;
            en.m406a(this, out, flags);
        }
    }

    /* renamed from: com.google.android.gms.internal.eq.e */
    public static final class C1602e extends an implements ae, Name {
        public static final eo CREATOR;
        private static final HashMap<String, C1142a<?, ?>> hR;
        private final int f181T;
        private final Set<Integer> hS;
        private String ir;
        private String iu;
        private String jA;
        private String jB;
        private String jC;
        private String jz;

        static {
            CREATOR = new eo();
            hR = new HashMap();
            hR.put("familyName", C1142a.m579f("familyName", 2));
            hR.put("formatted", C1142a.m579f("formatted", 3));
            hR.put("givenName", C1142a.m579f("givenName", 4));
            hR.put("honorificPrefix", C1142a.m579f("honorificPrefix", 5));
            hR.put("honorificSuffix", C1142a.m579f("honorificSuffix", 6));
            hR.put("middleName", C1142a.m579f("middleName", 7));
        }

        public C1602e() {
            this.f181T = 1;
            this.hS = new HashSet();
        }

        C1602e(Set<Integer> set, int i, String str, String str2, String str3, String str4, String str5, String str6) {
            this.hS = set;
            this.f181T = i;
            this.ir = str;
            this.jz = str2;
            this.iu = str3;
            this.jA = str4;
            this.jB = str5;
            this.jC = str6;
        }

        public HashMap<String, C1142a<?, ?>> m1233G() {
            return hR;
        }

        protected boolean m1234a(C1142a c1142a) {
            return this.hS.contains(Integer.valueOf(c1142a.m587N()));
        }

        protected Object m1235b(C1142a c1142a) {
            switch (c1142a.m587N()) {
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return this.ir;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return this.jz;
                case UserListView.TYPE_FAVE /*4*/:
                    return this.iu;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    return this.jA;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    return this.jB;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    return this.jC;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
            }
        }

        Set<Integer> by() {
            return this.hS;
        }

        public C1602e cl() {
            return this;
        }

        public int describeContents() {
            eo eoVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1602e)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            C1602e c1602e = (C1602e) obj;
            for (C1142a c1142a : hR.values()) {
                if (m1234a(c1142a)) {
                    if (!c1602e.m1234a(c1142a)) {
                        return false;
                    }
                    if (!m1235b(c1142a).equals(c1602e.m1235b(c1142a))) {
                        return false;
                    }
                } else if (c1602e.m1234a(c1142a)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return cl();
        }

        public String getFamilyName() {
            return this.ir;
        }

        public String getFormatted() {
            return this.jz;
        }

        public String getGivenName() {
            return this.iu;
        }

        public String getHonorificPrefix() {
            return this.jA;
        }

        public String getHonorificSuffix() {
            return this.jB;
        }

        public String getMiddleName() {
            return this.jC;
        }

        public boolean hasFamilyName() {
            return this.hS.contains(Integer.valueOf(2));
        }

        public boolean hasFormatted() {
            return this.hS.contains(Integer.valueOf(3));
        }

        public boolean hasGivenName() {
            return this.hS.contains(Integer.valueOf(4));
        }

        public boolean hasHonorificPrefix() {
            return this.hS.contains(Integer.valueOf(5));
        }

        public boolean hasHonorificSuffix() {
            return this.hS.contains(Integer.valueOf(6));
        }

        public boolean hasMiddleName() {
            return this.hS.contains(Integer.valueOf(7));
        }

        public int hashCode() {
            int i = 0;
            for (C1142a c1142a : hR.values()) {
                int hashCode;
                if (m1234a(c1142a)) {
                    hashCode = m1235b(c1142a).hashCode() + (i + c1142a.m587N());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        protected Object m1236j(String str) {
            return null;
        }

        protected boolean m1237k(String str) {
            return false;
        }

        int m1238u() {
            return this.f181T;
        }

        public void writeToParcel(Parcel out, int flags) {
            eo eoVar = CREATOR;
            eo.m409a(this, out, flags);
        }
    }

    /* renamed from: com.google.android.gms.internal.eq.g */
    public static final class C1603g extends an implements ae, Organizations {
        public static final ep CREATOR;
        private static final HashMap<String, C1142a<?, ?>> hR;
        private final int f182T;
        private int bi;
        private String ch;
        private String gl;
        private final Set<Integer> hS;
        private String iH;
        private String iq;
        private String jD;
        private String jE;
        private boolean jy;
        private String mName;

        static {
            CREATOR = new ep();
            hR = new HashMap();
            hR.put("department", C1142a.m579f("department", 2));
            hR.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, C1142a.m579f(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, 3));
            hR.put("endDate", C1142a.m579f("endDate", 4));
            hR.put("location", C1142a.m579f("location", 5));
            hR.put("name", C1142a.m579f("name", 6));
            hR.put("primary", C1142a.m578e("primary", 7));
            hR.put("startDate", C1142a.m579f("startDate", 8));
            hR.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, C1142a.m579f(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_TITLE, 9));
            hR.put(WebDialog.DIALOG_PARAM_TYPE, C1142a.m572a(WebDialog.DIALOG_PARAM_TYPE, 10, new ak().m569b("work", 0).m569b("school", 1), false));
        }

        public C1603g() {
            this.f182T = 1;
            this.hS = new HashSet();
        }

        C1603g(Set<Integer> set, int i, String str, String str2, String str3, String str4, String str5, boolean z, String str6, String str7, int i2) {
            this.hS = set;
            this.f182T = i;
            this.jD = str;
            this.ch = str2;
            this.iq = str3;
            this.jE = str4;
            this.mName = str5;
            this.jy = z;
            this.iH = str6;
            this.gl = str7;
            this.bi = i2;
        }

        public HashMap<String, C1142a<?, ?>> m1239G() {
            return hR;
        }

        protected boolean m1240a(C1142a c1142a) {
            return this.hS.contains(Integer.valueOf(c1142a.m587N()));
        }

        protected Object m1241b(C1142a c1142a) {
            switch (c1142a.m587N()) {
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return this.jD;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return this.ch;
                case UserListView.TYPE_FAVE /*4*/:
                    return this.iq;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    return this.jE;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    return this.mName;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    return Boolean.valueOf(this.jy);
                case UserListView.TYPE_BLACKLIST /*8*/:
                    return this.iH;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    return this.gl;
                case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                    return Integer.valueOf(this.bi);
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
            }
        }

        Set<Integer> by() {
            return this.hS;
        }

        public C1603g cm() {
            return this;
        }

        public int describeContents() {
            ep epVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1603g)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            C1603g c1603g = (C1603g) obj;
            for (C1142a c1142a : hR.values()) {
                if (m1240a(c1142a)) {
                    if (!c1603g.m1240a(c1142a)) {
                        return false;
                    }
                    if (!m1241b(c1142a).equals(c1603g.m1241b(c1142a))) {
                        return false;
                    }
                } else if (c1603g.m1240a(c1142a)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return cm();
        }

        public String getDepartment() {
            return this.jD;
        }

        public String getDescription() {
            return this.ch;
        }

        public String getEndDate() {
            return this.iq;
        }

        public String getLocation() {
            return this.jE;
        }

        public String getName() {
            return this.mName;
        }

        public String getStartDate() {
            return this.iH;
        }

        public String getTitle() {
            return this.gl;
        }

        public int getType() {
            return this.bi;
        }

        public boolean hasDepartment() {
            return this.hS.contains(Integer.valueOf(2));
        }

        public boolean hasDescription() {
            return this.hS.contains(Integer.valueOf(3));
        }

        public boolean hasEndDate() {
            return this.hS.contains(Integer.valueOf(4));
        }

        public boolean hasLocation() {
            return this.hS.contains(Integer.valueOf(5));
        }

        public boolean hasName() {
            return this.hS.contains(Integer.valueOf(6));
        }

        public boolean hasPrimary() {
            return this.hS.contains(Integer.valueOf(7));
        }

        public boolean hasStartDate() {
            return this.hS.contains(Integer.valueOf(8));
        }

        public boolean hasTitle() {
            return this.hS.contains(Integer.valueOf(9));
        }

        public boolean hasType() {
            return this.hS.contains(Integer.valueOf(10));
        }

        public int hashCode() {
            int i = 0;
            for (C1142a c1142a : hR.values()) {
                int hashCode;
                if (m1240a(c1142a)) {
                    hashCode = m1241b(c1142a).hashCode() + (i + c1142a.m587N());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        public boolean isPrimary() {
            return this.jy;
        }

        protected Object m1242j(String str) {
            return null;
        }

        protected boolean m1243k(String str) {
            return false;
        }

        int m1244u() {
            return this.f182T;
        }

        public void writeToParcel(Parcel out, int flags) {
            ep epVar = CREATOR;
            ep.m412a(this, out, flags);
        }
    }

    /* renamed from: com.google.android.gms.internal.eq.h */
    public static final class C1604h extends an implements ae, PlacesLived {
        public static final et CREATOR;
        private static final HashMap<String, C1142a<?, ?>> hR;
        private final int f183T;
        private final Set<Integer> hS;
        private boolean jy;
        private String mValue;

        static {
            CREATOR = new et();
            hR = new HashMap();
            hR.put("primary", C1142a.m578e("primary", 2));
            hR.put("value", C1142a.m579f("value", 3));
        }

        public C1604h() {
            this.f183T = 1;
            this.hS = new HashSet();
        }

        C1604h(Set<Integer> set, int i, boolean z, String str) {
            this.hS = set;
            this.f183T = i;
            this.jy = z;
            this.mValue = str;
        }

        public HashMap<String, C1142a<?, ?>> m1245G() {
            return hR;
        }

        protected boolean m1246a(C1142a c1142a) {
            return this.hS.contains(Integer.valueOf(c1142a.m587N()));
        }

        protected Object m1247b(C1142a c1142a) {
            switch (c1142a.m587N()) {
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return Boolean.valueOf(this.jy);
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return this.mValue;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
            }
        }

        Set<Integer> by() {
            return this.hS;
        }

        public C1604h cn() {
            return this;
        }

        public int describeContents() {
            et etVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1604h)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            C1604h c1604h = (C1604h) obj;
            for (C1142a c1142a : hR.values()) {
                if (m1246a(c1142a)) {
                    if (!c1604h.m1246a(c1142a)) {
                        return false;
                    }
                    if (!m1247b(c1142a).equals(c1604h.m1247b(c1142a))) {
                        return false;
                    }
                } else if (c1604h.m1246a(c1142a)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return cn();
        }

        public String getValue() {
            return this.mValue;
        }

        public boolean hasPrimary() {
            return this.hS.contains(Integer.valueOf(2));
        }

        public boolean hasValue() {
            return this.hS.contains(Integer.valueOf(3));
        }

        public int hashCode() {
            int i = 0;
            for (C1142a c1142a : hR.values()) {
                int hashCode;
                if (m1246a(c1142a)) {
                    hashCode = m1247b(c1142a).hashCode() + (i + c1142a.m587N());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        public boolean isPrimary() {
            return this.jy;
        }

        protected Object m1248j(String str) {
            return null;
        }

        protected boolean m1249k(String str) {
            return false;
        }

        int m1250u() {
            return this.f183T;
        }

        public void writeToParcel(Parcel out, int flags) {
            et etVar = CREATOR;
            et.m419a(this, out, flags);
        }
    }

    /* renamed from: com.google.android.gms.internal.eq.i */
    public static final class C1605i extends an implements ae, Urls {
        public static final eu CREATOR;
        private static final HashMap<String, C1142a<?, ?>> hR;
        private final int f184T;
        private int bi;
        private final Set<Integer> hS;
        private boolean jy;
        private String mValue;

        static {
            CREATOR = new eu();
            hR = new HashMap();
            hR.put("primary", C1142a.m578e("primary", 2));
            hR.put(WebDialog.DIALOG_PARAM_TYPE, C1142a.m572a(WebDialog.DIALOG_PARAM_TYPE, 3, new ak().m569b("home", 0).m569b("work", 1).m569b("blog", 2).m569b("profile", 3).m569b("other", 4), false));
            hR.put("value", C1142a.m579f("value", 4));
        }

        public C1605i() {
            this.f184T = 1;
            this.hS = new HashSet();
        }

        C1605i(Set<Integer> set, int i, boolean z, int i2, String str) {
            this.hS = set;
            this.f184T = i;
            this.jy = z;
            this.bi = i2;
            this.mValue = str;
        }

        public HashMap<String, C1142a<?, ?>> m1251G() {
            return hR;
        }

        protected boolean m1252a(C1142a c1142a) {
            return this.hS.contains(Integer.valueOf(c1142a.m587N()));
        }

        protected Object m1253b(C1142a c1142a) {
            switch (c1142a.m587N()) {
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    return Boolean.valueOf(this.jy);
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    return Integer.valueOf(this.bi);
                case UserListView.TYPE_FAVE /*4*/:
                    return this.mValue;
                default:
                    throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
            }
        }

        Set<Integer> by() {
            return this.hS;
        }

        public C1605i co() {
            return this;
        }

        public int describeContents() {
            eu euVar = CREATOR;
            return 0;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1605i)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            C1605i c1605i = (C1605i) obj;
            for (C1142a c1142a : hR.values()) {
                if (m1252a(c1142a)) {
                    if (!c1605i.m1252a(c1142a)) {
                        return false;
                    }
                    if (!m1253b(c1142a).equals(c1605i.m1253b(c1142a))) {
                        return false;
                    }
                } else if (c1605i.m1252a(c1142a)) {
                    return false;
                }
            }
            return true;
        }

        public /* synthetic */ Object freeze() {
            return co();
        }

        public int getType() {
            return this.bi;
        }

        public String getValue() {
            return this.mValue;
        }

        public boolean hasPrimary() {
            return this.hS.contains(Integer.valueOf(2));
        }

        public boolean hasType() {
            return this.hS.contains(Integer.valueOf(3));
        }

        public boolean hasValue() {
            return this.hS.contains(Integer.valueOf(4));
        }

        public int hashCode() {
            int i = 0;
            for (C1142a c1142a : hR.values()) {
                int hashCode;
                if (m1252a(c1142a)) {
                    hashCode = m1253b(c1142a).hashCode() + (i + c1142a.m587N());
                } else {
                    hashCode = i;
                }
                i = hashCode;
            }
            return i;
        }

        public boolean isPrimary() {
            return this.jy;
        }

        protected Object m1254j(String str) {
            return null;
        }

        protected boolean m1255k(String str) {
            return false;
        }

        int m1256u() {
            return this.f184T;
        }

        public void writeToParcel(Parcel out, int flags) {
            eu euVar = CREATOR;
            eu.m421a(this, out, flags);
        }
    }

    static {
        CREATOR = new er();
        hR = new HashMap();
        hR.put("aboutMe", C1142a.m579f("aboutMe", 2));
        hR.put("ageRange", C1142a.m573a("ageRange", 3, C1596a.class));
        hR.put("birthday", C1142a.m579f("birthday", 4));
        hR.put("braggingRights", C1142a.m579f("braggingRights", 5));
        hR.put("circledByCount", C1142a.m575c("circledByCount", 6));
        hR.put("cover", C1142a.m573a("cover", 7, C1599b.class));
        hR.put("currentLocation", C1142a.m579f("currentLocation", 8));
        hR.put("displayName", C1142a.m579f("displayName", 9));
        hR.put("emails", C1142a.m574b("emails", 10, C1600c.class));
        hR.put("etag", C1142a.m579f("etag", 11));
        hR.put("gender", C1142a.m572a("gender", 12, new ak().m569b("male", 0).m569b("female", 1).m569b("other", 2), false));
        hR.put("hasApp", C1142a.m578e("hasApp", 13));
        hR.put("id", C1142a.m579f("id", 14));
        hR.put("image", C1142a.m573a("image", 15, C1601d.class));
        hR.put("isPlusUser", C1142a.m578e("isPlusUser", 16));
        hR.put("language", C1142a.m579f("language", 18));
        hR.put("name", C1142a.m573a("name", 19, C1602e.class));
        hR.put("nickname", C1142a.m579f("nickname", 20));
        hR.put("objectType", C1142a.m572a("objectType", 21, new ak().m569b("person", 0).m569b("page", 1), false));
        hR.put("organizations", C1142a.m574b("organizations", 22, C1603g.class));
        hR.put("placesLived", C1142a.m574b("placesLived", 23, C1604h.class));
        hR.put("plusOneCount", C1142a.m575c("plusOneCount", 24));
        hR.put("relationshipStatus", C1142a.m572a("relationshipStatus", 25, new ak().m569b("single", 0).m569b("in_a_relationship", 1).m569b("engaged", 2).m569b("married", 3).m569b("its_complicated", 4).m569b("open_relationship", 5).m569b("widowed", 6).m569b("in_domestic_partnership", 7).m569b("in_civil_union", 8), false));
        hR.put("tagline", C1142a.m579f("tagline", 26));
        hR.put(PlusShare.KEY_CALL_TO_ACTION_URL, C1142a.m579f(PlusShare.KEY_CALL_TO_ACTION_URL, 27));
        hR.put("urls", C1142a.m574b("urls", 28, C1605i.class));
        hR.put("verified", C1142a.m578e("verified", 29));
    }

    public eq() {
        this.f185T = 1;
        this.hS = new HashSet();
    }

    public eq(String str, String str2, C1601d c1601d, int i, String str3) {
        this.f185T = 1;
        this.hS = new HashSet();
        this.bm = str;
        this.hS.add(Integer.valueOf(9));
        this.iw = str2;
        this.hS.add(Integer.valueOf(14));
        this.je = c1601d;
        this.hS.add(Integer.valueOf(15));
        this.jj = i;
        this.hS.add(Integer.valueOf(21));
        this.hE = str3;
        this.hS.add(Integer.valueOf(27));
    }

    eq(Set<Integer> set, int i, String str, C1596a c1596a, String str2, String str3, int i2, C1599b c1599b, String str4, String str5, List<C1600c> list, String str6, int i3, boolean z, String str7, C1601d c1601d, boolean z2, String str8, C1602e c1602e, String str9, int i4, List<C1603g> list2, List<C1604h> list3, int i5, int i6, String str10, String str11, List<C1605i> list4, boolean z3) {
        this.hS = set;
        this.f185T = i;
        this.iT = str;
        this.iU = c1596a;
        this.iV = str2;
        this.iW = str3;
        this.iX = i2;
        this.iY = c1599b;
        this.iZ = str4;
        this.bm = str5;
        this.ja = list;
        this.jb = str6;
        this.jc = i3;
        this.jd = z;
        this.iw = str7;
        this.je = c1601d;
        this.jf = z2;
        this.jg = str8;
        this.jh = c1602e;
        this.ji = str9;
        this.jj = i4;
        this.jk = list2;
        this.jl = list3;
        this.jm = i5;
        this.jn = i6;
        this.jo = str10;
        this.hE = str11;
        this.jp = list4;
        this.jq = z3;
    }

    public static eq m1257d(byte[] bArr) {
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        eq F = CREATOR.m417F(obtain);
        obtain.recycle();
        return F;
    }

    public HashMap<String, C1142a<?, ?>> m1258G() {
        return hR;
    }

    protected boolean m1259a(C1142a c1142a) {
        return this.hS.contains(Integer.valueOf(c1142a.m587N()));
    }

    protected Object m1260b(C1142a c1142a) {
        switch (c1142a.m587N()) {
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return this.iT;
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                return this.iU;
            case UserListView.TYPE_FAVE /*4*/:
                return this.iV;
            case UserListView.TYPE_FOLLOWERS /*5*/:
                return this.iW;
            case UserListView.TYPE_POLL_VOTERS /*6*/:
                return Integer.valueOf(this.iX);
            case UserListView.TYPE_FAVE_LINKS /*7*/:
                return this.iY;
            case UserListView.TYPE_BLACKLIST /*8*/:
                return this.iZ;
            case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                return this.bm;
            case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                return this.ja;
            case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                return this.jb;
            case Strings.UPDATE_DIALOG_POSITIVE_BUTTON_ID /*12*/:
                return Integer.valueOf(this.jc);
            case Strings.EXPIRY_INFO_TITLE_ID /*13*/:
                return Boolean.valueOf(this.jd);
            case Strings.EXPIRY_INFO_TEXT_ID /*14*/:
                return this.iw;
            case Strings.FEEDBACK_FAILED_TITLE_ID /*15*/:
                return this.je;
            case TransportMediator.FLAG_KEY_MEDIA_PAUSE /*16*/:
                return Boolean.valueOf(this.jf);
            case StringKeys.SELECT_IMAGES /*18*/:
                return this.jg;
            case StringKeys.SELECT_IMAGE /*19*/:
                return this.jh;
            case StringKeys.CAMERA_FOLDER /*20*/:
                return this.ji;
            case C0436R.styleable.View_android_fitsSystemWindows /*21*/:
                return Integer.valueOf(this.jj);
            case C0436R.styleable.View_android_scrollbars /*22*/:
                return this.jk;
            case C0436R.styleable.View_android_fadingEdge /*23*/:
                return this.jl;
            case C0436R.styleable.View_android_fadingEdgeLength /*24*/:
                return Integer.valueOf(this.jm);
            case C0436R.styleable.View_android_nextFocusLeft /*25*/:
                return Integer.valueOf(this.jn);
            case C0436R.styleable.View_android_nextFocusRight /*26*/:
                return this.jo;
            case C0436R.styleable.View_android_nextFocusUp /*27*/:
                return this.hE;
            case C0436R.styleable.View_android_nextFocusDown /*28*/:
                return this.jp;
            case C0436R.styleable.View_android_clickable /*29*/:
                return Boolean.valueOf(this.jq);
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
        }
    }

    C1596a bT() {
        return this.iU;
    }

    C1599b bU() {
        return this.iY;
    }

    List<C1600c> bV() {
        return this.ja;
    }

    public String bW() {
        return this.jb;
    }

    C1601d bX() {
        return this.je;
    }

    C1602e bY() {
        return this.jh;
    }

    List<C1603g> bZ() {
        return this.jk;
    }

    Set<Integer> by() {
        return this.hS;
    }

    List<C1604h> ca() {
        return this.jl;
    }

    List<C1605i> cb() {
        return this.jp;
    }

    public eq cc() {
        return this;
    }

    public int describeContents() {
        er erVar = CREATOR;
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof eq)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        eq eqVar = (eq) obj;
        for (C1142a c1142a : hR.values()) {
            if (m1259a(c1142a)) {
                if (!eqVar.m1259a(c1142a)) {
                    return false;
                }
                if (!m1260b(c1142a).equals(eqVar.m1260b(c1142a))) {
                    return false;
                }
            } else if (eqVar.m1259a(c1142a)) {
                return false;
            }
        }
        return true;
    }

    public /* synthetic */ Object freeze() {
        return cc();
    }

    public String getAboutMe() {
        return this.iT;
    }

    public AgeRange getAgeRange() {
        return this.iU;
    }

    public String getBirthday() {
        return this.iV;
    }

    public String getBraggingRights() {
        return this.iW;
    }

    public int getCircledByCount() {
        return this.iX;
    }

    public Cover getCover() {
        return this.iY;
    }

    public String getCurrentLocation() {
        return this.iZ;
    }

    public String getDisplayName() {
        return this.bm;
    }

    public List<Emails> getEmails() {
        return (ArrayList) this.ja;
    }

    public int getGender() {
        return this.jc;
    }

    public String getId() {
        return this.iw;
    }

    public Image getImage() {
        return this.je;
    }

    public String getLanguage() {
        return this.jg;
    }

    public Name getName() {
        return this.jh;
    }

    public String getNickname() {
        return this.ji;
    }

    public int getObjectType() {
        return this.jj;
    }

    public List<Organizations> getOrganizations() {
        return (ArrayList) this.jk;
    }

    public List<PlacesLived> getPlacesLived() {
        return (ArrayList) this.jl;
    }

    public int getPlusOneCount() {
        return this.jm;
    }

    public int getRelationshipStatus() {
        return this.jn;
    }

    public String getTagline() {
        return this.jo;
    }

    public String getUrl() {
        return this.hE;
    }

    public List<Urls> getUrls() {
        return (ArrayList) this.jp;
    }

    public boolean hasAboutMe() {
        return this.hS.contains(Integer.valueOf(2));
    }

    public boolean hasAgeRange() {
        return this.hS.contains(Integer.valueOf(3));
    }

    public boolean hasBirthday() {
        return this.hS.contains(Integer.valueOf(4));
    }

    public boolean hasBraggingRights() {
        return this.hS.contains(Integer.valueOf(5));
    }

    public boolean hasCircledByCount() {
        return this.hS.contains(Integer.valueOf(6));
    }

    public boolean hasCover() {
        return this.hS.contains(Integer.valueOf(7));
    }

    public boolean hasCurrentLocation() {
        return this.hS.contains(Integer.valueOf(8));
    }

    public boolean hasDisplayName() {
        return this.hS.contains(Integer.valueOf(9));
    }

    public boolean hasEmails() {
        return this.hS.contains(Integer.valueOf(10));
    }

    public boolean hasGender() {
        return this.hS.contains(Integer.valueOf(12));
    }

    public boolean hasHasApp() {
        return this.hS.contains(Integer.valueOf(13));
    }

    public boolean hasId() {
        return this.hS.contains(Integer.valueOf(14));
    }

    public boolean hasImage() {
        return this.hS.contains(Integer.valueOf(15));
    }

    public boolean hasIsPlusUser() {
        return this.hS.contains(Integer.valueOf(16));
    }

    public boolean hasLanguage() {
        return this.hS.contains(Integer.valueOf(18));
    }

    public boolean hasName() {
        return this.hS.contains(Integer.valueOf(19));
    }

    public boolean hasNickname() {
        return this.hS.contains(Integer.valueOf(20));
    }

    public boolean hasObjectType() {
        return this.hS.contains(Integer.valueOf(21));
    }

    public boolean hasOrganizations() {
        return this.hS.contains(Integer.valueOf(22));
    }

    public boolean hasPlacesLived() {
        return this.hS.contains(Integer.valueOf(23));
    }

    public boolean hasPlusOneCount() {
        return this.hS.contains(Integer.valueOf(24));
    }

    public boolean hasRelationshipStatus() {
        return this.hS.contains(Integer.valueOf(25));
    }

    public boolean hasTagline() {
        return this.hS.contains(Integer.valueOf(26));
    }

    public boolean hasUrl() {
        return this.hS.contains(Integer.valueOf(27));
    }

    public boolean hasUrls() {
        return this.hS.contains(Integer.valueOf(28));
    }

    public boolean hasVerified() {
        return this.hS.contains(Integer.valueOf(29));
    }

    public int hashCode() {
        int i = 0;
        for (C1142a c1142a : hR.values()) {
            int hashCode;
            if (m1259a(c1142a)) {
                hashCode = m1260b(c1142a).hashCode() + (i + c1142a.m587N());
            } else {
                hashCode = i;
            }
            i = hashCode;
        }
        return i;
    }

    public boolean isHasApp() {
        return this.jd;
    }

    public boolean isPlusUser() {
        return this.jf;
    }

    public boolean isVerified() {
        return this.jq;
    }

    protected Object m1261j(String str) {
        return null;
    }

    protected boolean m1262k(String str) {
        return false;
    }

    int m1263u() {
        return this.f185T;
    }

    public void writeToParcel(Parcel out, int flags) {
        er erVar = CREATOR;
        er.m416a(this, out, flags);
    }
}
