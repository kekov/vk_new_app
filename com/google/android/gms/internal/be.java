package com.google.android.gms.internal;

import com.google.android.gms.dynamic.LifecycleDelegate;

public interface be<T extends LifecycleDelegate> {
    void m175a(T t);
}
