package com.google.android.gms.internal;

import android.content.Context;
import android.content.Intent;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcelable;
import android.os.RemoteException;
import android.view.View;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.games.GameBuffer;
import com.google.android.gms.games.GamesClient;
import com.google.android.gms.games.OnGamesLoadedListener;
import com.google.android.gms.games.OnPlayersLoadedListener;
import com.google.android.gms.games.OnSignOutCompleteListener;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerBuffer;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.games.RealTimeSocket;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.OnAchievementUpdatedListener;
import com.google.android.gms.games.achievement.OnAchievementsLoadedListener;
import com.google.android.gms.games.leaderboard.LeaderboardBuffer;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.OnLeaderboardMetadataLoadedListener;
import com.google.android.gms.games.leaderboard.OnLeaderboardScoresLoadedListener;
import com.google.android.gms.games.leaderboard.OnScoreSubmittedListener;
import com.google.android.gms.games.leaderboard.SubmitScoreResult;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationBuffer;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.OnInvitationsLoadedListener;
import com.google.android.gms.games.multiplayer.ParticipantUtils;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessage;
import com.google.android.gms.games.multiplayer.realtime.RealTimeMessageReceivedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeReliableMessageSentListener;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomStatusUpdateListener;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.internal.C0136t.C1236a;
import com.google.android.gms.internal.C1234p.C0129b;
import com.google.android.gms.internal.C1234p.C1232c;
import com.google.android.gms.internal.C1234p.C1606d;
import com.google.android.gms.internal.bm.C1160a;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class bj extends C1234p<bm> {
    private PlayerEntity cA;
    private final bn cB;
    private boolean cC;
    private final Binder cD;
    private final long cE;
    private final boolean cF;
    private final String cy;
    private final Map<String, bo> cz;
    private final String f165g;

    final class af extends C0129b<RealTimeReliableMessageSentListener> {
        final /* synthetic */ bj cH;
        private final String cX;
        private final int cY;
        private final int f72p;

        af(bj bjVar, RealTimeReliableMessageSentListener realTimeReliableMessageSentListener, int i, int i2, String str) {
            this.cH = bjVar;
            super(bjVar, realTimeReliableMessageSentListener);
            this.f72p = i;
            this.cY = i2;
            this.cX = str;
        }

        public void m632a(RealTimeReliableMessageSentListener realTimeReliableMessageSentListener) {
            if (realTimeReliableMessageSentListener != null) {
                realTimeReliableMessageSentListener.onRealTimeMessageSent(this.f72p, this.cY, this.cX);
            }
        }
    }

    final class an extends C0129b<OnSignOutCompleteListener> {
        final /* synthetic */ bj cH;

        public an(bj bjVar, OnSignOutCompleteListener onSignOutCompleteListener) {
            this.cH = bjVar;
            super(bjVar, onSignOutCompleteListener);
        }

        public void m634a(OnSignOutCompleteListener onSignOutCompleteListener) {
            onSignOutCompleteListener.onSignOutComplete();
        }
    }

    final class ap extends C0129b<OnScoreSubmittedListener> {
        final /* synthetic */ bj cH;
        private final SubmitScoreResult df;

        public ap(bj bjVar, OnScoreSubmittedListener onScoreSubmittedListener, SubmitScoreResult submitScoreResult) {
            this.cH = bjVar;
            super(bjVar, onScoreSubmittedListener);
            this.df = submitScoreResult;
        }

        public void m636a(OnScoreSubmittedListener onScoreSubmittedListener) {
            onScoreSubmittedListener.onScoreSubmitted(this.df.getStatusCode(), this.df);
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.e */
    final class C1152e extends C0129b<OnAchievementUpdatedListener> {
        final /* synthetic */ bj cH;
        private final String cJ;
        private final int f73p;

        C1152e(bj bjVar, OnAchievementUpdatedListener onAchievementUpdatedListener, int i, String str) {
            this.cH = bjVar;
            super(bjVar, onAchievementUpdatedListener);
            this.f73p = i;
            this.cJ = str;
        }

        protected void m638a(OnAchievementUpdatedListener onAchievementUpdatedListener) {
            onAchievementUpdatedListener.onAchievementUpdated(this.f73p, this.cJ);
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.n */
    final class C1153n extends C0129b<OnInvitationReceivedListener> {
        final /* synthetic */ bj cH;
        private final Invitation cO;

        C1153n(bj bjVar, OnInvitationReceivedListener onInvitationReceivedListener, Invitation invitation) {
            this.cH = bjVar;
            super(bjVar, onInvitationReceivedListener);
            this.cO = invitation;
        }

        protected void m640a(OnInvitationReceivedListener onInvitationReceivedListener) {
            onInvitationReceivedListener.onInvitationReceived(this.cO);
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.s */
    final class C1154s extends C0129b<OnLeaderboardScoresLoadedListener> {
        final /* synthetic */ bj cH;
        private final C1230k cR;
        private final C1230k cS;

        C1154s(bj bjVar, OnLeaderboardScoresLoadedListener onLeaderboardScoresLoadedListener, C1230k c1230k, C1230k c1230k2) {
            this.cH = bjVar;
            super(bjVar, onLeaderboardScoresLoadedListener);
            this.cR = c1230k;
            this.cS = c1230k2;
        }

        protected void m642a(OnLeaderboardScoresLoadedListener onLeaderboardScoresLoadedListener) {
            onLeaderboardScoresLoadedListener.onLeaderboardScoresLoaded(this.cS.getStatusCode(), new LeaderboardBuffer(this.cR), new LeaderboardScoreBuffer(this.cS));
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.v */
    final class C1155v extends C0129b<RoomUpdateListener> {
        final /* synthetic */ bj cH;
        private final String cU;
        private final int f74p;

        C1155v(bj bjVar, RoomUpdateListener roomUpdateListener, int i, String str) {
            this.cH = bjVar;
            super(bjVar, roomUpdateListener);
            this.f74p = i;
            this.cU = str;
        }

        public void m644a(RoomUpdateListener roomUpdateListener) {
            roomUpdateListener.onLeftRoom(this.f74p, this.cU);
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.w */
    final class C1156w extends C0129b<RealTimeMessageReceivedListener> {
        final /* synthetic */ bj cH;
        private final RealTimeMessage cV;

        C1156w(bj bjVar, RealTimeMessageReceivedListener realTimeMessageReceivedListener, RealTimeMessage realTimeMessage) {
            this.cH = bjVar;
            super(bjVar, realTimeMessageReceivedListener);
            this.cV = realTimeMessage;
        }

        public void m646a(RealTimeMessageReceivedListener realTimeMessageReceivedListener) {
            bk.m176a("GamesClient", "Deliver Message received callback");
            if (realTimeMessageReceivedListener != null) {
                realTimeMessageReceivedListener.onRealTimeMessageReceived(this.cV);
            }
        }
    }

    final class ae extends C1232c<OnPlayersLoadedListener> {
        final /* synthetic */ bj cH;

        ae(bj bjVar, OnPlayersLoadedListener onPlayersLoadedListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, onPlayersLoadedListener, c1230k);
        }

        protected void m1086a(OnPlayersLoadedListener onPlayersLoadedListener) {
            onPlayersLoadedListener.onPlayersLoaded(this.O.getStatusCode(), new PlayerBuffer(this.O));
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.b */
    abstract class C1578b extends C1232c<RoomUpdateListener> {
        final /* synthetic */ bj cH;

        C1578b(bj bjVar, RoomUpdateListener roomUpdateListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, roomUpdateListener, c1230k);
        }

        protected void m1088a(RoomUpdateListener roomUpdateListener) {
            m1089a(roomUpdateListener, this.cH.m1106x(this.O));
        }

        protected abstract void m1089a(RoomUpdateListener roomUpdateListener, Room room);
    }

    /* renamed from: com.google.android.gms.internal.bj.c */
    abstract class C1579c extends C1232c<RoomStatusUpdateListener> {
        final /* synthetic */ bj cH;

        C1579c(bj bjVar, RoomStatusUpdateListener roomStatusUpdateListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, roomStatusUpdateListener, c1230k);
        }

        protected void m1091a(RoomStatusUpdateListener roomStatusUpdateListener) {
            if (roomStatusUpdateListener != null) {
                m1092a(roomStatusUpdateListener, this.cH.m1106x(this.O));
            }
        }

        protected abstract void m1092a(RoomStatusUpdateListener roomStatusUpdateListener, Room room);
    }

    /* renamed from: com.google.android.gms.internal.bj.g */
    final class C1580g extends C1232c<OnAchievementsLoadedListener> {
        final /* synthetic */ bj cH;

        C1580g(bj bjVar, OnAchievementsLoadedListener onAchievementsLoadedListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, onAchievementsLoadedListener, c1230k);
        }

        protected void m1094a(OnAchievementsLoadedListener onAchievementsLoadedListener) {
            onAchievementsLoadedListener.onAchievementsLoaded(this.O.getStatusCode(), new AchievementBuffer(this.O));
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.j */
    public final class C1581j extends C1236a {
        final /* synthetic */ bj cH;
        private final C1606d cL;

        public C1581j(bj bjVar, C1606d c1606d) {
            this.cH = bjVar;
            this.cL = c1606d;
        }

        public void m1096a(int i, IBinder iBinder, Bundle bundle) {
            this.cL.m1264a(i, iBinder, bundle);
            if (i == 0 && bundle != null) {
                this.cH.cC = bundle.getBoolean("show_welcome_popup");
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.l */
    final class C1582l extends C1232c<OnGamesLoadedListener> {
        final /* synthetic */ bj cH;

        C1582l(bj bjVar, OnGamesLoadedListener onGamesLoadedListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, onGamesLoadedListener, c1230k);
        }

        protected void m1097a(OnGamesLoadedListener onGamesLoadedListener) {
            onGamesLoadedListener.onGamesLoaded(this.O.getStatusCode(), new GameBuffer(this.O));
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.p */
    final class C1583p extends C1232c<OnInvitationsLoadedListener> {
        final /* synthetic */ bj cH;

        C1583p(bj bjVar, OnInvitationsLoadedListener onInvitationsLoadedListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, onInvitationsLoadedListener, c1230k);
        }

        protected void m1099a(OnInvitationsLoadedListener onInvitationsLoadedListener) {
            onInvitationsLoadedListener.onInvitationsLoaded(this.O.getStatusCode(), new InvitationBuffer(this.O));
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.u */
    final class C1584u extends C1232c<OnLeaderboardMetadataLoadedListener> {
        final /* synthetic */ bj cH;

        C1584u(bj bjVar, OnLeaderboardMetadataLoadedListener onLeaderboardMetadataLoadedListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, onLeaderboardMetadataLoadedListener, c1230k);
        }

        protected void m1101a(OnLeaderboardMetadataLoadedListener onLeaderboardMetadataLoadedListener) {
            onLeaderboardMetadataLoadedListener.onLeaderboardMetadataLoaded(this.O.getStatusCode(), new LeaderboardBuffer(this.O));
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.a */
    abstract class C1624a extends C1579c {
        private final ArrayList<String> cG;
        final /* synthetic */ bj cH;

        C1624a(bj bjVar, RoomStatusUpdateListener roomStatusUpdateListener, C1230k c1230k, String[] strArr) {
            this.cH = bjVar;
            super(bjVar, roomStatusUpdateListener, c1230k);
            this.cG = new ArrayList();
            for (Object add : strArr) {
                this.cG.add(add);
            }
        }

        protected void m1273a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            if (roomStatusUpdateListener != null) {
                m1274a(roomStatusUpdateListener, room, this.cG);
            }
        }

        protected abstract void m1274a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList);
    }

    final class ad extends bi {
        final /* synthetic */ bj cH;
        private final OnPlayersLoadedListener cW;

        ad(bj bjVar, OnPlayersLoadedListener onPlayersLoadedListener) {
            this.cH = bjVar;
            this.cW = (OnPlayersLoadedListener) C0142x.m492b((Object) onPlayersLoadedListener, (Object) "Listener must not be null");
        }

        public void m1275e(C1230k c1230k) {
            this.cH.m944a(new ae(this.cH, this.cW, c1230k));
        }
    }

    final class ag extends bi {
        final /* synthetic */ bj cH;
        final RealTimeReliableMessageSentListener cZ;

        public ag(bj bjVar, RealTimeReliableMessageSentListener realTimeReliableMessageSentListener) {
            this.cH = bjVar;
            this.cZ = realTimeReliableMessageSentListener;
        }

        public void m1276a(int i, int i2, String str) {
            this.cH.m944a(new af(this.cH, this.cZ, i, i2, str));
        }
    }

    final class ah extends C1579c {
        final /* synthetic */ bj cH;

        ah(bj bjVar, RoomStatusUpdateListener roomStatusUpdateListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, roomStatusUpdateListener, c1230k);
        }

        public void m1277a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onRoomAutoMatching(room);
            }
        }
    }

    final class ai extends bi {
        final /* synthetic */ bj cH;
        private final RoomUpdateListener da;
        private final RoomStatusUpdateListener db;
        private final RealTimeMessageReceivedListener dc;

        public ai(bj bjVar, RoomUpdateListener roomUpdateListener) {
            this.cH = bjVar;
            this.da = (RoomUpdateListener) C0142x.m492b((Object) roomUpdateListener, (Object) "Callbacks must not be null");
            this.db = null;
            this.dc = null;
        }

        public ai(bj bjVar, RoomUpdateListener roomUpdateListener, RoomStatusUpdateListener roomStatusUpdateListener, RealTimeMessageReceivedListener realTimeMessageReceivedListener) {
            this.cH = bjVar;
            this.da = (RoomUpdateListener) C0142x.m492b((Object) roomUpdateListener, (Object) "Callbacks must not be null");
            this.db = roomStatusUpdateListener;
            this.dc = realTimeMessageReceivedListener;
        }

        public void m1278a(C1230k c1230k, String[] strArr) {
            this.cH.m944a(new aa(this.cH, this.db, c1230k, strArr));
        }

        public void m1279b(C1230k c1230k, String[] strArr) {
            this.cH.m944a(new ab(this.cH, this.db, c1230k, strArr));
        }

        public void m1280c(C1230k c1230k, String[] strArr) {
            this.cH.m944a(new ac(this.cH, this.db, c1230k, strArr));
        }

        public void m1281d(C1230k c1230k, String[] strArr) {
            this.cH.m944a(new C1646y(this.cH, this.db, c1230k, strArr));
        }

        public void m1282e(C1230k c1230k, String[] strArr) {
            this.cH.m944a(new C1645x(this.cH, this.db, c1230k, strArr));
        }

        public void m1283f(C1230k c1230k, String[] strArr) {
            this.cH.m944a(new C1647z(this.cH, this.db, c1230k, strArr));
        }

        public void m1284n(C1230k c1230k) {
            this.cH.m944a(new al(this.cH, this.da, c1230k));
        }

        public void m1285o(C1230k c1230k) {
            this.cH.m944a(new C1632q(this.cH, this.da, c1230k));
        }

        public void onLeftRoom(int statusCode, String externalRoomId) {
            this.cH.m944a(new C1155v(this.cH, this.da, statusCode, externalRoomId));
        }

        public void onRealTimeMessageReceived(RealTimeMessage message) {
            bk.m176a("GamesClient", "RoomBinderCallbacks: onRealTimeMessageReceived");
            this.cH.m944a(new C1156w(this.cH, this.dc, message));
        }

        public void m1286p(C1230k c1230k) {
            this.cH.m944a(new ak(this.cH, this.db, c1230k));
        }

        public void m1287q(C1230k c1230k) {
            this.cH.m944a(new ah(this.cH, this.db, c1230k));
        }

        public void m1288r(C1230k c1230k) {
            this.cH.m944a(new aj(this.cH, this.da, c1230k));
        }

        public void m1289s(C1230k c1230k) {
            this.cH.m944a(new C1627h(this.cH, this.db, c1230k));
        }

        public void m1290t(C1230k c1230k) {
            this.cH.m944a(new C1628i(this.cH, this.db, c1230k));
        }
    }

    final class aj extends C1578b {
        final /* synthetic */ bj cH;

        aj(bj bjVar, RoomUpdateListener roomUpdateListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, roomUpdateListener, c1230k);
        }

        public void m1291a(RoomUpdateListener roomUpdateListener, Room room) {
            if (roomUpdateListener != null) {
                roomUpdateListener.onRoomConnected(this.O.getStatusCode(), room);
            }
        }
    }

    final class ak extends C1579c {
        final /* synthetic */ bj cH;

        ak(bj bjVar, RoomStatusUpdateListener roomStatusUpdateListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, roomStatusUpdateListener, c1230k);
        }

        public void m1292a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onRoomConnecting(room);
            }
        }
    }

    final class al extends C1578b {
        final /* synthetic */ bj cH;

        public al(bj bjVar, RoomUpdateListener roomUpdateListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, roomUpdateListener, c1230k);
        }

        public void m1293a(RoomUpdateListener roomUpdateListener, Room room) {
            roomUpdateListener.onRoomCreated(this.O.getStatusCode(), room);
        }
    }

    final class am extends bi {
        final /* synthetic */ bj cH;
        private final OnSignOutCompleteListener dd;

        public am(bj bjVar, OnSignOutCompleteListener onSignOutCompleteListener) {
            this.cH = bjVar;
            this.dd = (OnSignOutCompleteListener) C0142x.m492b((Object) onSignOutCompleteListener, (Object) "Listener must not be null");
        }

        public void onSignOutComplete() {
            this.cH.m944a(new an(this.cH, this.dd));
        }
    }

    final class ao extends bi {
        final /* synthetic */ bj cH;
        private final OnScoreSubmittedListener de;

        public ao(bj bjVar, OnScoreSubmittedListener onScoreSubmittedListener) {
            this.cH = bjVar;
            this.de = (OnScoreSubmittedListener) C0142x.m492b((Object) onScoreSubmittedListener, (Object) "Listener must not be null");
        }

        public void m1294d(C1230k c1230k) {
            this.cH.m944a(new ap(this.cH, this.de, new SubmitScoreResult(c1230k)));
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.d */
    final class C1625d extends bi {
        final /* synthetic */ bj cH;
        private final OnAchievementUpdatedListener cI;

        C1625d(bj bjVar, OnAchievementUpdatedListener onAchievementUpdatedListener) {
            this.cH = bjVar;
            this.cI = (OnAchievementUpdatedListener) C0142x.m492b((Object) onAchievementUpdatedListener, (Object) "Listener must not be null");
        }

        public void onAchievementUpdated(int statusCode, String achievementId) {
            this.cH.m944a(new C1152e(this.cH, this.cI, statusCode, achievementId));
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.f */
    final class C1626f extends bi {
        final /* synthetic */ bj cH;
        private final OnAchievementsLoadedListener cK;

        C1626f(bj bjVar, OnAchievementsLoadedListener onAchievementsLoadedListener) {
            this.cH = bjVar;
            this.cK = (OnAchievementsLoadedListener) C0142x.m492b((Object) onAchievementsLoadedListener, (Object) "Listener must not be null");
        }

        public void m1295b(C1230k c1230k) {
            this.cH.m944a(new C1580g(this.cH, this.cK, c1230k));
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.h */
    final class C1627h extends C1579c {
        final /* synthetic */ bj cH;

        C1627h(bj bjVar, RoomStatusUpdateListener roomStatusUpdateListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, roomStatusUpdateListener, c1230k);
        }

        public void m1296a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onConnectedToRoom(room);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.i */
    final class C1628i extends C1579c {
        final /* synthetic */ bj cH;

        C1628i(bj bjVar, RoomStatusUpdateListener roomStatusUpdateListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, roomStatusUpdateListener, c1230k);
        }

        public void m1297a(RoomStatusUpdateListener roomStatusUpdateListener, Room room) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onDisconnectedFromRoom(room);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.k */
    final class C1629k extends bi {
        final /* synthetic */ bj cH;
        private final OnGamesLoadedListener cM;

        C1629k(bj bjVar, OnGamesLoadedListener onGamesLoadedListener) {
            this.cH = bjVar;
            this.cM = (OnGamesLoadedListener) C0142x.m492b((Object) onGamesLoadedListener, (Object) "Listener must not be null");
        }

        public void m1298g(C1230k c1230k) {
            this.cH.m944a(new C1582l(this.cH, this.cM, c1230k));
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.m */
    final class C1630m extends bi {
        final /* synthetic */ bj cH;
        private final OnInvitationReceivedListener cN;

        C1630m(bj bjVar, OnInvitationReceivedListener onInvitationReceivedListener) {
            this.cH = bjVar;
            this.cN = onInvitationReceivedListener;
        }

        public void m1299k(C1230k c1230k) {
            InvitationBuffer invitationBuffer = new InvitationBuffer(c1230k);
            Invitation invitation = null;
            try {
                if (invitationBuffer.getCount() > 0) {
                    invitation = (Invitation) ((Invitation) invitationBuffer.get(0)).freeze();
                }
                invitationBuffer.close();
                if (invitation != null) {
                    this.cH.m944a(new C1153n(this.cH, this.cN, invitation));
                }
            } catch (Throwable th) {
                invitationBuffer.close();
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.o */
    final class C1631o extends bi {
        final /* synthetic */ bj cH;
        private final OnInvitationsLoadedListener cP;

        C1631o(bj bjVar, OnInvitationsLoadedListener onInvitationsLoadedListener) {
            this.cH = bjVar;
            this.cP = onInvitationsLoadedListener;
        }

        public void m1300j(C1230k c1230k) {
            this.cH.m944a(new C1583p(this.cH, this.cP, c1230k));
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.q */
    final class C1632q extends C1578b {
        final /* synthetic */ bj cH;

        public C1632q(bj bjVar, RoomUpdateListener roomUpdateListener, C1230k c1230k) {
            this.cH = bjVar;
            super(bjVar, roomUpdateListener, c1230k);
        }

        public void m1301a(RoomUpdateListener roomUpdateListener, Room room) {
            roomUpdateListener.onJoinedRoom(this.O.getStatusCode(), room);
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.r */
    final class C1633r extends bi {
        final /* synthetic */ bj cH;
        private final OnLeaderboardScoresLoadedListener cQ;

        C1633r(bj bjVar, OnLeaderboardScoresLoadedListener onLeaderboardScoresLoadedListener) {
            this.cH = bjVar;
            this.cQ = (OnLeaderboardScoresLoadedListener) C0142x.m492b((Object) onLeaderboardScoresLoadedListener, (Object) "Listener must not be null");
        }

        public void m1302a(C1230k c1230k, C1230k c1230k2) {
            this.cH.m944a(new C1154s(this.cH, this.cQ, c1230k, c1230k2));
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.t */
    final class C1634t extends bi {
        final /* synthetic */ bj cH;
        private final OnLeaderboardMetadataLoadedListener cT;

        C1634t(bj bjVar, OnLeaderboardMetadataLoadedListener onLeaderboardMetadataLoadedListener) {
            this.cH = bjVar;
            this.cT = (OnLeaderboardMetadataLoadedListener) C0142x.m492b((Object) onLeaderboardMetadataLoadedListener, (Object) "Listener must not be null");
        }

        public void m1303c(C1230k c1230k) {
            this.cH.m944a(new C1584u(this.cH, this.cT, c1230k));
        }
    }

    final class aa extends C1624a {
        final /* synthetic */ bj cH;

        aa(bj bjVar, RoomStatusUpdateListener roomStatusUpdateListener, C1230k c1230k, String[] strArr) {
            this.cH = bjVar;
            super(bjVar, roomStatusUpdateListener, c1230k, strArr);
        }

        protected void m1312a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onPeerInvitedToRoom(room, arrayList);
            }
        }
    }

    final class ab extends C1624a {
        final /* synthetic */ bj cH;

        ab(bj bjVar, RoomStatusUpdateListener roomStatusUpdateListener, C1230k c1230k, String[] strArr) {
            this.cH = bjVar;
            super(bjVar, roomStatusUpdateListener, c1230k, strArr);
        }

        protected void m1313a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onPeerJoined(room, arrayList);
            }
        }
    }

    final class ac extends C1624a {
        final /* synthetic */ bj cH;

        ac(bj bjVar, RoomStatusUpdateListener roomStatusUpdateListener, C1230k c1230k, String[] strArr) {
            this.cH = bjVar;
            super(bjVar, roomStatusUpdateListener, c1230k, strArr);
        }

        protected void m1314a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onPeerLeft(room, arrayList);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.x */
    final class C1645x extends C1624a {
        final /* synthetic */ bj cH;

        C1645x(bj bjVar, RoomStatusUpdateListener roomStatusUpdateListener, C1230k c1230k, String[] strArr) {
            this.cH = bjVar;
            super(bjVar, roomStatusUpdateListener, c1230k, strArr);
        }

        protected void m1315a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onPeersConnected(room, arrayList);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.y */
    final class C1646y extends C1624a {
        final /* synthetic */ bj cH;

        C1646y(bj bjVar, RoomStatusUpdateListener roomStatusUpdateListener, C1230k c1230k, String[] strArr) {
            this.cH = bjVar;
            super(bjVar, roomStatusUpdateListener, c1230k, strArr);
        }

        protected void m1316a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onPeerDeclined(room, arrayList);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.bj.z */
    final class C1647z extends C1624a {
        final /* synthetic */ bj cH;

        C1647z(bj bjVar, RoomStatusUpdateListener roomStatusUpdateListener, C1230k c1230k, String[] strArr) {
            this.cH = bjVar;
            super(bjVar, roomStatusUpdateListener, c1230k, strArr);
        }

        protected void m1317a(RoomStatusUpdateListener roomStatusUpdateListener, Room room, ArrayList<String> arrayList) {
            if (roomStatusUpdateListener != null) {
                roomStatusUpdateListener.onPeersDisconnected(room, arrayList);
            }
        }
    }

    public bj(Context context, String str, String str2, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String[] strArr, int i, View view, boolean z) {
        super(context, connectionCallbacks, onConnectionFailedListener, strArr);
        this.cC = false;
        this.cy = str;
        this.f165g = (String) C0142x.m495d(str2);
        this.cD = new Binder();
        this.cz = new HashMap();
        this.cB = bn.m277a(this, i);
        setViewForPopups(view);
        this.cE = (long) hashCode();
        this.cF = z;
    }

    private void ah() {
        this.cA = null;
    }

    private void ai() {
        for (bo close : this.cz.values()) {
            try {
                close.close();
            } catch (Throwable e) {
                bk.m177a("GamesClient", "IOException:", e);
            }
        }
        this.cz.clear();
    }

    private bo m1105p(String str) {
        try {
            String r = ((bm) m956o()).m273r(str);
            if (r == null) {
                return null;
            }
            bk.m180d("GamesClient", "Creating a socket to bind to:" + r);
            LocalSocket localSocket = new LocalSocket();
            try {
                localSocket.connect(new LocalSocketAddress(r));
                bo boVar = new bo(localSocket, str);
                this.cz.put(str, boVar);
                return boVar;
            } catch (IOException e) {
                bk.m179c("GamesClient", "connect() call failed on socket: " + e.getMessage());
                return null;
            }
        } catch (RemoteException e2) {
            bk.m179c("GamesClient", "Unable to create socket. Service died.");
            return null;
        }
    }

    private Room m1106x(C1230k c1230k) {
        by byVar = new by(c1230k);
        Room room = null;
        try {
            if (byVar.getCount() > 0) {
                room = (Room) ((Room) byVar.get(0)).freeze();
            }
            byVar.close();
            return room;
        } catch (Throwable th) {
            byVar.close();
        }
    }

    public int m1107a(byte[] bArr, String str, String[] strArr) {
        C0142x.m492b((Object) strArr, (Object) "Participant IDs must not be null");
        try {
            return ((bm) m956o()).m239b(bArr, str, strArr);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
            return -1;
        }
    }

    public void m1108a(IBinder iBinder, Bundle bundle) {
        if (isConnected()) {
            try {
                ((bm) m956o()).m219a(iBinder, bundle);
            } catch (RemoteException e) {
                bk.m178b("GamesClient", "service died");
            }
        }
    }

    protected void m1109a(ConnectionResult connectionResult) {
        super.m943a(connectionResult);
        this.cC = false;
    }

    public void m1110a(OnPlayersLoadedListener onPlayersLoadedListener, int i, boolean z, boolean z2) {
        try {
            ((bm) m956o()).m222a(new ad(this, onPlayersLoadedListener), i, z, z2);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void m1111a(OnAchievementUpdatedListener onAchievementUpdatedListener, String str) {
        if (onAchievementUpdatedListener == null) {
            bl blVar = null;
        } else {
            Object c1625d = new C1625d(this, onAchievementUpdatedListener);
        }
        try {
            ((bm) m956o()).m232a(blVar, str, this.cB.ao(), this.cB.an());
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void m1112a(OnAchievementUpdatedListener onAchievementUpdatedListener, String str, int i) {
        try {
            ((bm) m956o()).m229a(onAchievementUpdatedListener == null ? null : new C1625d(this, onAchievementUpdatedListener), str, i, this.cB.ao(), this.cB.an());
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void m1113a(OnScoreSubmittedListener onScoreSubmittedListener, String str, long j) {
        if (onScoreSubmittedListener == null) {
            bl blVar = null;
        } else {
            Object aoVar = new ao(this, onScoreSubmittedListener);
        }
        try {
            ((bm) m956o()).m231a(blVar, str, j);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    protected void m1114a(C0137u c0137u, C1606d c1606d) throws RemoteException {
        String locale = getContext().getResources().getConfiguration().locale.toString();
        C0136t c1581j = new C1581j(this, c1606d);
        Bundle bundle = new Bundle();
        bundle.putBoolean("com.google.android.gms.games.key.isHeadless", this.cF);
        c0137u.m479a(c1581j, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), this.f165g, m951j(), this.cy, this.cB.ao(), locale, bundle);
    }

    protected void m1115a(String... strArr) {
        int i = 0;
        boolean z = false;
        for (String str : strArr) {
            if (str.equals(Scopes.GAMES)) {
                z = true;
            } else if (str.equals("https://www.googleapis.com/auth/games.firstparty")) {
                i = 1;
            }
        }
        if (i != 0) {
            C0142x.m490a(!z, String.format("Cannot have both %s and %s!", new Object[]{Scopes.GAMES, "https://www.googleapis.com/auth/games.firstparty"}));
            return;
        }
        C0142x.m490a(z, String.format("GamesClient requires %s to function.", new Object[]{Scopes.GAMES}));
    }

    public void aj() {
        if (isConnected()) {
            try {
                ((bm) m956o()).aj();
            } catch (RemoteException e) {
                bk.m178b("GamesClient", "service died");
            }
        }
    }

    protected String m1116b() {
        return "com.google.android.gms.games.service.START";
    }

    public void m1117b(OnAchievementUpdatedListener onAchievementUpdatedListener, String str) {
        if (onAchievementUpdatedListener == null) {
            bl blVar = null;
        } else {
            Object c1625d = new C1625d(this, onAchievementUpdatedListener);
        }
        try {
            ((bm) m956o()).m245b(blVar, str, this.cB.ao(), this.cB.an());
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    protected /* synthetic */ IInterface m1118c(IBinder iBinder) {
        return m1122k(iBinder);
    }

    protected String m1119c() {
        return "com.google.android.gms.games.internal.IGamesService";
    }

    public void clearNotifications(int notificationTypes) {
        try {
            ((bm) m956o()).clearNotifications(notificationTypes);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void connect() {
        ah();
        super.connect();
    }

    public void createRoom(RoomConfig config) {
        try {
            ((bm) m956o()).m225a(new ai(this, config.getRoomUpdateListener(), config.getRoomStatusUpdateListener(), config.getMessageReceivedListener()), this.cD, config.getVariant(), config.getInvitedPlayerIds(), config.getAutoMatchCriteria(), config.isSocketEnabled(), this.cE);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void disconnect() {
        this.cC = false;
        if (isConnected()) {
            try {
                bm bmVar = (bm) m956o();
                bmVar.aj();
                bmVar.m240b(this.cE);
                bmVar.m218a(this.cE);
            } catch (RemoteException e) {
                bk.m178b("GamesClient", "Failed to notify client disconnect.");
            }
        }
        ai();
        super.disconnect();
    }

    public Intent getAchievementsIntent() {
        m955n();
        Intent intent = new Intent("com.google.android.gms.games.VIEW_ACHIEVEMENTS");
        intent.addFlags(67108864);
        return intent;
    }

    public Intent getAllLeaderboardsIntent() {
        m955n();
        Intent intent = new Intent("com.google.android.gms.games.VIEW_LEADERBOARDS");
        intent.putExtra("com.google.android.gms.games.GAME_PACKAGE_NAME", this.cy);
        intent.addFlags(67108864);
        return intent;
    }

    public String getAppId() {
        try {
            return ((bm) m956o()).getAppId();
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
            return null;
        }
    }

    public String getCurrentAccountName() {
        try {
            return ((bm) m956o()).getCurrentAccountName();
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
            return null;
        }
    }

    public Player getCurrentPlayer() {
        PlayerBuffer playerBuffer;
        m955n();
        synchronized (this) {
            if (this.cA == null) {
                try {
                    playerBuffer = new PlayerBuffer(((bm) m956o()).ak());
                    if (playerBuffer.getCount() > 0) {
                        this.cA = (PlayerEntity) playerBuffer.get(0).freeze();
                    }
                    playerBuffer.close();
                } catch (RemoteException e) {
                    bk.m178b("GamesClient", "service died");
                } catch (Throwable th) {
                    playerBuffer.close();
                }
            }
        }
        return this.cA;
    }

    public String getCurrentPlayerId() {
        try {
            return ((bm) m956o()).getCurrentPlayerId();
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
            return null;
        }
    }

    public Intent getInvitationInboxIntent() {
        m955n();
        Intent intent = new Intent("com.google.android.gms.games.SHOW_INVITATIONS");
        intent.putExtra("com.google.android.gms.games.GAME_PACKAGE_NAME", this.cy);
        return intent;
    }

    public Intent getLeaderboardIntent(String leaderboardId) {
        m955n();
        Intent intent = new Intent("com.google.android.gms.games.VIEW_LEADERBOARD_SCORES");
        intent.putExtra("com.google.android.gms.games.LEADERBOARD_ID", leaderboardId);
        intent.addFlags(67108864);
        return intent;
    }

    public RealTimeSocket getRealTimeSocketForParticipant(String roomId, String participantId) {
        if (participantId == null || !ParticipantUtils.m36v(participantId)) {
            throw new IllegalArgumentException("Bad participant ID");
        }
        bo boVar = (bo) this.cz.get(participantId);
        return (boVar == null || boVar.isClosed()) ? m1105p(participantId) : boVar;
    }

    public Intent getRealTimeWaitingRoomIntent(Room room, int minParticipantsToStart) {
        m955n();
        Intent intent = new Intent("com.google.android.gms.games.SHOW_REAL_TIME_WAITING_ROOM");
        C0142x.m492b((Object) room, (Object) "Room parameter must not be null");
        intent.putExtra(GamesClient.EXTRA_ROOM, (Parcelable) room.freeze());
        C0142x.m490a(minParticipantsToStart >= 0, "minParticipantsToStart must be >= 0");
        intent.putExtra("com.google.android.gms.games.MIN_PARTICIPANTS_TO_START", minParticipantsToStart);
        return intent;
    }

    public Intent getSelectPlayersIntent(int minPlayers, int maxPlayers) {
        m955n();
        Intent intent = new Intent("com.google.android.gms.games.SELECT_PLAYERS");
        intent.putExtra("com.google.android.gms.games.MIN_SELECTIONS", minPlayers);
        intent.putExtra("com.google.android.gms.games.MAX_SELECTIONS", maxPlayers);
        return intent;
    }

    public Intent getSettingsIntent() {
        m955n();
        Intent intent = new Intent("com.google.android.gms.games.SHOW_SETTINGS");
        intent.putExtra("com.google.android.gms.games.GAME_PACKAGE_NAME", this.cy);
        intent.addFlags(67108864);
        return intent;
    }

    public void m1120h(String str, int i) {
        try {
            ((bm) m956o()).m263h(str, i);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void m1121i(String str, int i) {
        try {
            ((bm) m956o()).m266i(str, i);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void joinRoom(RoomConfig config) {
        try {
            ((bm) m956o()).m226a(new ai(this, config.getRoomUpdateListener(), config.getRoomStatusUpdateListener(), config.getMessageReceivedListener()), this.cD, config.getInvitationId(), config.isSocketEnabled(), this.cE);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    protected bm m1122k(IBinder iBinder) {
        return C1160a.m745m(iBinder);
    }

    protected void m1123k() {
        super.m952k();
        if (this.cC) {
            this.cB.am();
            this.cC = false;
        }
    }

    protected Bundle m1124l() {
        try {
            Bundle l = ((bm) m956o()).m270l();
            if (l == null) {
                return l;
            }
            l.setClassLoader(bj.class.getClassLoader());
            return l;
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
            return null;
        }
    }

    public void leaveRoom(RoomUpdateListener listener, String roomId) {
        try {
            ((bm) m956o()).m255e(new ai(this, listener), roomId);
            ai();
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void loadAchievements(OnAchievementsLoadedListener listener) {
        try {
            ((bm) m956o()).m248c(new C1626f(this, listener));
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void loadGame(OnGamesLoadedListener listener) {
        try {
            ((bm) m956o()).m252d(new C1629k(this, listener));
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void loadInvitations(OnInvitationsLoadedListener listener) {
        try {
            ((bm) m956o()).m254e(new C1631o(this, listener));
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void loadLeaderboardMetadata(OnLeaderboardMetadataLoadedListener listener) {
        try {
            ((bm) m956o()).m241b(new C1634t(this, listener));
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void loadLeaderboardMetadata(OnLeaderboardMetadataLoadedListener listener, String leaderboardId) {
        try {
            ((bm) m956o()).m253d(new C1634t(this, listener), leaderboardId);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void loadMoreScores(OnLeaderboardScoresLoadedListener listener, LeaderboardScoreBuffer buffer, int maxResults, int pageDirection) {
        try {
            ((bm) m956o()).m224a(new C1633r(this, listener), buffer.aq().ar(), maxResults, pageDirection);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void loadPlayer(OnPlayersLoadedListener listener, String playerId) {
        try {
            ((bm) m956o()).m250c(new ad(this, listener), playerId);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void loadPlayerCenteredScores(OnLeaderboardScoresLoadedListener listener, String leaderboardId, int span, int leaderboardCollection, int maxResults, boolean forceReload) {
        try {
            ((bm) m956o()).m244b(new C1633r(this, listener), leaderboardId, span, leaderboardCollection, maxResults, forceReload);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void loadTopScores(OnLeaderboardScoresLoadedListener listener, String leaderboardId, int span, int leaderboardCollection, int maxResults, boolean forceReload) {
        try {
            ((bm) m956o()).m228a(new C1633r(this, listener), leaderboardId, span, leaderboardCollection, maxResults, forceReload);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void registerInvitationListener(OnInvitationReceivedListener listener) {
        try {
            ((bm) m956o()).m223a(new C1630m(this, listener), this.cE);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public int sendReliableRealTimeMessage(RealTimeReliableMessageSentListener listener, byte[] messageData, String roomId, String recipientParticipantId) {
        try {
            return ((bm) m956o()).m217a(new ag(this, listener), messageData, roomId, recipientParticipantId);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
            return -1;
        }
    }

    public int sendUnreliableRealTimeMessageToAll(byte[] messageData, String roomId) {
        try {
            return ((bm) m956o()).m239b(messageData, roomId, null);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
            return -1;
        }
    }

    public void setGravityForPopups(int gravity) {
        this.cB.setGravity(gravity);
    }

    public void setUseNewPlayerNotificationsFirstParty(boolean newPlayerStyle) {
        try {
            ((bm) m956o()).setUseNewPlayerNotificationsFirstParty(newPlayerStyle);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void setViewForPopups(View gamesContentView) {
        this.cB.m278a(gamesContentView);
    }

    public void signOut(OnSignOutCompleteListener listener) {
        if (listener == null) {
            bl blVar = null;
        } else {
            Object amVar = new am(this, listener);
        }
        try {
            ((bm) m956o()).m220a(blVar);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }

    public void unregisterInvitationListener() {
        try {
            ((bm) m956o()).m240b(this.cE);
        } catch (RemoteException e) {
            bk.m178b("GamesClient", "service died");
        }
    }
}
