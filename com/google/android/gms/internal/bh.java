package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.facebook.WebDialog;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.internal.C0141w.C0140a;
import com.google.android.gms.plus.PlusShare;

public final class bh extends C0123j implements Achievement {
    public bh(C1230k c1230k, int i) {
        super(c1230k, i);
    }

    public String getAchievementId() {
        return getString("external_achievement_id");
    }

    public int getCurrentSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        C0126n.m442a(z);
        return getInteger("current_steps");
    }

    public String getDescription() {
        return getString(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION);
    }

    public void getDescription(CharArrayBuffer dataOut) {
        m434a(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, dataOut);
    }

    public String getFormattedCurrentSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        C0126n.m442a(z);
        return getString("formatted_current_steps");
    }

    public void getFormattedCurrentSteps(CharArrayBuffer dataOut) {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        C0126n.m442a(z);
        m434a("formatted_current_steps", dataOut);
    }

    public String getFormattedTotalSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        C0126n.m442a(z);
        return getString("formatted_total_steps");
    }

    public void getFormattedTotalSteps(CharArrayBuffer dataOut) {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        C0126n.m442a(z);
        m434a("formatted_total_steps", dataOut);
    }

    public long getLastUpdatedTimestamp() {
        return getLong("last_updated_timestamp");
    }

    public String getName() {
        return getString("name");
    }

    public void getName(CharArrayBuffer dataOut) {
        m434a("name", dataOut);
    }

    public Player getPlayer() {
        return new bg(this.O, this.R);
    }

    public Uri getRevealedImageUri() {
        return m435c("revealed_icon_image_uri");
    }

    public int getState() {
        return getInteger("state");
    }

    public int getTotalSteps() {
        boolean z = true;
        if (getType() != 1) {
            z = false;
        }
        C0126n.m442a(z);
        return getInteger("total_steps");
    }

    public int getType() {
        return getInteger(WebDialog.DIALOG_PARAM_TYPE);
    }

    public Uri getUnlockedImageUri() {
        return m435c("unlocked_icon_image_uri");
    }

    public String toString() {
        C0140a a = C0141w.m488c(this).m486a("id", getAchievementId()).m486a("name", getName()).m486a("state", Integer.valueOf(getState())).m486a(WebDialog.DIALOG_PARAM_TYPE, Integer.valueOf(getType()));
        if (getType() == 1) {
            a.m486a("steps", getCurrentSteps() + "/" + getTotalSteps());
        }
        return a.toString();
    }
}
