package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.VisibleRegion;

public class di {
    public static void m328a(VisibleRegion visibleRegion, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, visibleRegion.m1011u());
        ad.m95a(parcel, 2, visibleRegion.nearLeft, i, false);
        ad.m95a(parcel, 3, visibleRegion.nearRight, i, false);
        ad.m95a(parcel, 4, visibleRegion.farLeft, i, false);
        ad.m95a(parcel, 5, visibleRegion.farRight, i, false);
        ad.m95a(parcel, 6, visibleRegion.latLngBounds, i, false);
        ad.m87C(parcel, d);
    }
}
