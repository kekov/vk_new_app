package com.google.android.gms.internal;

import android.os.Parcel;
import android.support.v4.media.TransportMediator;
import com.facebook.WebDialog;
import com.google.android.gms.internal.an.C1142a;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.model.moments.ItemScope;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.cache.CacheTables;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.providers.LangProvider.StringKeys;
import com.vkontakte.android.mediapicker.ui.GalleryPickerHeaderView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.hockeyapp.android.Strings;

public final class ed extends an implements ae, ItemScope {
    public static final ee CREATOR;
    private static final HashMap<String, C1142a<?, ?>> hR;
    private final int f172T;
    private String ch;
    private double ew;
    private double ex;
    private String hE;
    private final Set<Integer> hS;
    private ed hT;
    private List<String> hU;
    private ed hV;
    private String hW;
    private String hX;
    private String hY;
    private List<ed> hZ;
    private ed iA;
    private List<ed> iB;
    private String iC;
    private String iD;
    private String iE;
    private String iF;
    private ed iG;
    private String iH;
    private String iI;
    private String iJ;
    private ed iK;
    private String iL;
    private String iM;
    private String iN;
    private String iO;
    private String iP;
    private int ia;
    private List<ed> ib;
    private ed ic;
    private List<ed> id;
    private String ie;
    private String f173if;
    private ed ig;
    private String ih;
    private String ii;
    private String ij;
    private List<ed> ik;
    private String il;
    private String im;
    private String in;
    private String io;
    private String ip;
    private String iq;
    private String ir;
    private String is;
    private ed it;
    private String iu;
    private String iv;
    private String iw;
    private String ix;
    private ed iy;
    private ed iz;
    private String mName;

    static {
        CREATOR = new ee();
        hR = new HashMap();
        hR.put("about", C1142a.m573a("about", 2, ed.class));
        hR.put("additionalName", C1142a.m580g("additionalName", 3));
        hR.put("address", C1142a.m573a("address", 4, ed.class));
        hR.put("addressCountry", C1142a.m579f("addressCountry", 5));
        hR.put("addressLocality", C1142a.m579f("addressLocality", 6));
        hR.put("addressRegion", C1142a.m579f("addressRegion", 7));
        hR.put("associated_media", C1142a.m574b("associated_media", 8, ed.class));
        hR.put("attendeeCount", C1142a.m575c("attendeeCount", 9));
        hR.put("attendees", C1142a.m574b("attendees", 10, ed.class));
        hR.put("audio", C1142a.m573a("audio", 11, ed.class));
        hR.put("author", C1142a.m574b("author", 12, ed.class));
        hR.put("bestRating", C1142a.m579f("bestRating", 13));
        hR.put("birthDate", C1142a.m579f("birthDate", 14));
        hR.put("byArtist", C1142a.m573a("byArtist", 15, ed.class));
        hR.put("caption", C1142a.m579f("caption", 16));
        hR.put("contentSize", C1142a.m579f("contentSize", 17));
        hR.put("contentUrl", C1142a.m579f("contentUrl", 18));
        hR.put("contributor", C1142a.m574b("contributor", 19, ed.class));
        hR.put("dateCreated", C1142a.m579f("dateCreated", 20));
        hR.put("dateModified", C1142a.m579f("dateModified", 21));
        hR.put("datePublished", C1142a.m579f("datePublished", 22));
        hR.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, C1142a.m579f(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, 23));
        hR.put("duration", C1142a.m579f("duration", 24));
        hR.put("embedUrl", C1142a.m579f("embedUrl", 25));
        hR.put("endDate", C1142a.m579f("endDate", 26));
        hR.put("familyName", C1142a.m579f("familyName", 27));
        hR.put("gender", C1142a.m579f("gender", 28));
        hR.put("geo", C1142a.m573a("geo", 29, ed.class));
        hR.put("givenName", C1142a.m579f("givenName", 30));
        hR.put("height", C1142a.m579f("height", 31));
        hR.put("id", C1142a.m579f("id", 32));
        hR.put("image", C1142a.m579f("image", 33));
        hR.put("inAlbum", C1142a.m573a("inAlbum", 34, ed.class));
        hR.put("latitude", C1142a.m577d("latitude", 36));
        hR.put("location", C1142a.m573a("location", 37, ed.class));
        hR.put("longitude", C1142a.m577d("longitude", 38));
        hR.put("name", C1142a.m579f("name", 39));
        hR.put("partOfTVSeries", C1142a.m573a("partOfTVSeries", 40, ed.class));
        hR.put("performers", C1142a.m574b("performers", 41, ed.class));
        hR.put("playerType", C1142a.m579f("playerType", 42));
        hR.put("postOfficeBoxNumber", C1142a.m579f("postOfficeBoxNumber", 43));
        hR.put("postalCode", C1142a.m579f("postalCode", 44));
        hR.put("ratingValue", C1142a.m579f("ratingValue", 45));
        hR.put("reviewRating", C1142a.m573a("reviewRating", 46, ed.class));
        hR.put("startDate", C1142a.m579f("startDate", 47));
        hR.put("streetAddress", C1142a.m579f("streetAddress", 48));
        hR.put("text", C1142a.m579f("text", 49));
        hR.put("thumbnail", C1142a.m573a("thumbnail", 50, ed.class));
        hR.put(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_THUMBNAIL_URL, C1142a.m579f(PlusShare.KEY_CONTENT_DEEP_LINK_METADATA_THUMBNAIL_URL, 51));
        hR.put("tickerSymbol", C1142a.m579f("tickerSymbol", 52));
        hR.put(WebDialog.DIALOG_PARAM_TYPE, C1142a.m579f(WebDialog.DIALOG_PARAM_TYPE, 53));
        hR.put(PlusShare.KEY_CALL_TO_ACTION_URL, C1142a.m579f(PlusShare.KEY_CALL_TO_ACTION_URL, 54));
        hR.put("width", C1142a.m579f("width", 55));
        hR.put("worstRating", C1142a.m579f("worstRating", 56));
    }

    public ed() {
        this.f172T = 1;
        this.hS = new HashSet();
    }

    ed(Set<Integer> set, int i, ed edVar, List<String> list, ed edVar2, String str, String str2, String str3, List<ed> list2, int i2, List<ed> list3, ed edVar3, List<ed> list4, String str4, String str5, ed edVar4, String str6, String str7, String str8, List<ed> list5, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, String str17, ed edVar5, String str18, String str19, String str20, String str21, ed edVar6, double d, ed edVar7, double d2, String str22, ed edVar8, List<ed> list6, String str23, String str24, String str25, String str26, ed edVar9, String str27, String str28, String str29, ed edVar10, String str30, String str31, String str32, String str33, String str34, String str35) {
        this.hS = set;
        this.f172T = i;
        this.hT = edVar;
        this.hU = list;
        this.hV = edVar2;
        this.hW = str;
        this.hX = str2;
        this.hY = str3;
        this.hZ = list2;
        this.ia = i2;
        this.ib = list3;
        this.ic = edVar3;
        this.id = list4;
        this.ie = str4;
        this.f173if = str5;
        this.ig = edVar4;
        this.ih = str6;
        this.ii = str7;
        this.ij = str8;
        this.ik = list5;
        this.il = str9;
        this.im = str10;
        this.in = str11;
        this.ch = str12;
        this.io = str13;
        this.ip = str14;
        this.iq = str15;
        this.ir = str16;
        this.is = str17;
        this.it = edVar5;
        this.iu = str18;
        this.iv = str19;
        this.iw = str20;
        this.ix = str21;
        this.iy = edVar6;
        this.ew = d;
        this.iz = edVar7;
        this.ex = d2;
        this.mName = str22;
        this.iA = edVar8;
        this.iB = list6;
        this.iC = str23;
        this.iD = str24;
        this.iE = str25;
        this.iF = str26;
        this.iG = edVar9;
        this.iH = str27;
        this.iI = str28;
        this.iJ = str29;
        this.iK = edVar10;
        this.iL = str30;
        this.iM = str31;
        this.iN = str32;
        this.hE = str33;
        this.iO = str34;
        this.iP = str35;
    }

    public ed(Set<Integer> set, ed edVar, List<String> list, ed edVar2, String str, String str2, String str3, List<ed> list2, int i, List<ed> list3, ed edVar3, List<ed> list4, String str4, String str5, ed edVar4, String str6, String str7, String str8, List<ed> list5, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, String str17, ed edVar5, String str18, String str19, String str20, String str21, ed edVar6, double d, ed edVar7, double d2, String str22, ed edVar8, List<ed> list6, String str23, String str24, String str25, String str26, ed edVar9, String str27, String str28, String str29, ed edVar10, String str30, String str31, String str32, String str33, String str34, String str35) {
        this.hS = set;
        this.f172T = 1;
        this.hT = edVar;
        this.hU = list;
        this.hV = edVar2;
        this.hW = str;
        this.hX = str2;
        this.hY = str3;
        this.hZ = list2;
        this.ia = i;
        this.ib = list3;
        this.ic = edVar3;
        this.id = list4;
        this.ie = str4;
        this.f173if = str5;
        this.ig = edVar4;
        this.ih = str6;
        this.ii = str7;
        this.ij = str8;
        this.ik = list5;
        this.il = str9;
        this.im = str10;
        this.in = str11;
        this.ch = str12;
        this.io = str13;
        this.ip = str14;
        this.iq = str15;
        this.ir = str16;
        this.is = str17;
        this.it = edVar5;
        this.iu = str18;
        this.iv = str19;
        this.iw = str20;
        this.ix = str21;
        this.iy = edVar6;
        this.ew = d;
        this.iz = edVar7;
        this.ex = d2;
        this.mName = str22;
        this.iA = edVar8;
        this.iB = list6;
        this.iC = str23;
        this.iD = str24;
        this.iE = str25;
        this.iF = str26;
        this.iG = edVar9;
        this.iH = str27;
        this.iI = str28;
        this.iJ = str29;
        this.iK = edVar10;
        this.iL = str30;
        this.iM = str31;
        this.iN = str32;
        this.hE = str33;
        this.iO = str34;
        this.iP = str35;
    }

    public HashMap<String, C1142a<?, ?>> m1185G() {
        return hR;
    }

    protected boolean m1186a(C1142a c1142a) {
        return this.hS.contains(Integer.valueOf(c1142a.m587N()));
    }

    protected Object m1187b(C1142a c1142a) {
        switch (c1142a.m587N()) {
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return this.hT;
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                return this.hU;
            case UserListView.TYPE_FAVE /*4*/:
                return this.hV;
            case UserListView.TYPE_FOLLOWERS /*5*/:
                return this.hW;
            case UserListView.TYPE_POLL_VOTERS /*6*/:
                return this.hX;
            case UserListView.TYPE_FAVE_LINKS /*7*/:
                return this.hY;
            case UserListView.TYPE_BLACKLIST /*8*/:
                return this.hZ;
            case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                return Integer.valueOf(this.ia);
            case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                return this.ib;
            case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                return this.ic;
            case Strings.UPDATE_DIALOG_POSITIVE_BUTTON_ID /*12*/:
                return this.id;
            case Strings.EXPIRY_INFO_TITLE_ID /*13*/:
                return this.ie;
            case Strings.EXPIRY_INFO_TEXT_ID /*14*/:
                return this.f173if;
            case Strings.FEEDBACK_FAILED_TITLE_ID /*15*/:
                return this.ig;
            case TransportMediator.FLAG_KEY_MEDIA_PAUSE /*16*/:
                return this.ih;
            case StringKeys.SELECT_ALBUM /*17*/:
                return this.ii;
            case StringKeys.SELECT_IMAGES /*18*/:
                return this.ij;
            case StringKeys.SELECT_IMAGE /*19*/:
                return this.ik;
            case StringKeys.CAMERA_FOLDER /*20*/:
                return this.il;
            case C0436R.styleable.View_android_fitsSystemWindows /*21*/:
                return this.im;
            case C0436R.styleable.View_android_scrollbars /*22*/:
                return this.in;
            case C0436R.styleable.View_android_fadingEdge /*23*/:
                return this.ch;
            case C0436R.styleable.View_android_fadingEdgeLength /*24*/:
                return this.io;
            case C0436R.styleable.View_android_nextFocusLeft /*25*/:
                return this.ip;
            case C0436R.styleable.View_android_nextFocusRight /*26*/:
                return this.iq;
            case C0436R.styleable.View_android_nextFocusUp /*27*/:
                return this.ir;
            case C0436R.styleable.View_android_nextFocusDown /*28*/:
                return this.is;
            case C0436R.styleable.View_android_clickable /*29*/:
                return this.it;
            case C0436R.styleable.View_android_longClickable /*30*/:
                return this.iu;
            case CacheTables.MAIN_DB_VERSION /*31*/:
                return this.iv;
            case TransportMediator.FLAG_KEY_MEDIA_STOP /*32*/:
                return this.iw;
            case StringKeys.NO_SD_CARD /*33*/:
                return this.ix;
            case StringKeys.ERROR /*34*/:
                return this.iy;
            case C0436R.styleable.View_android_soundEffectsEnabled /*36*/:
                return Double.valueOf(this.ew);
            case C0436R.styleable.View_android_keepScreenOn /*37*/:
                return this.iz;
            case C0436R.styleable.View_android_isScrollContainer /*38*/:
                return Double.valueOf(this.ex);
            case C0436R.styleable.View_android_hapticFeedbackEnabled /*39*/:
                return this.mName;
            case C0436R.styleable.View_android_onClick /*40*/:
                return this.iA;
            case C0436R.styleable.View_android_contentDescription /*41*/:
                return this.iB;
            case C0436R.styleable.View_android_scrollbarFadeDuration /*42*/:
                return this.iC;
            case C0436R.styleable.View_android_scrollbarDefaultDelayBeforeFade /*43*/:
                return this.iD;
            case C0436R.styleable.View_android_fadeScrollbars /*44*/:
                return this.iE;
            case C0436R.styleable.SherlockTheme_listPreferredItemPaddingLeft /*45*/:
                return this.iF;
            case C0436R.styleable.SherlockTheme_listPreferredItemPaddingRight /*46*/:
                return this.iG;
            case C0436R.styleable.SherlockTheme_textAppearanceListItemSmall /*47*/:
                return this.iH;
            case GalleryPickerHeaderView.SIZE /*48*/:
                return this.iI;
            case StringKeys.CORRUPTED_IMAGE_1 /*49*/:
                return this.iJ;
            case StringKeys.CORRUPTED_IMAGE_2 /*50*/:
                return this.iK;
            case StringKeys.FILTERS_UNSUPPORTED /*51*/:
                return this.iL;
            case StringKeys.SHARE_PHOTO /*52*/:
                return this.iM;
            case StringKeys.SHARE_PHOTOS /*53*/:
                return this.iN;
            case StringKeys.CLOSE /*54*/:
                return this.hE;
            case C0436R.styleable.SherlockTheme_popupMenuStyle /*55*/:
                return this.iO;
            case C0436R.styleable.SherlockTheme_dropdownListPreferredItemHeight /*56*/:
                return this.iP;
            default:
                throw new IllegalStateException("Unknown safe parcelable id=" + c1142a.m587N());
        }
    }

    ed bA() {
        return this.hV;
    }

    List<ed> bB() {
        return this.hZ;
    }

    List<ed> bC() {
        return this.ib;
    }

    ed bD() {
        return this.ic;
    }

    List<ed> bE() {
        return this.id;
    }

    ed bF() {
        return this.ig;
    }

    List<ed> bG() {
        return this.ik;
    }

    ed bH() {
        return this.it;
    }

    ed bI() {
        return this.iy;
    }

    ed bJ() {
        return this.iz;
    }

    ed bK() {
        return this.iA;
    }

    List<ed> bL() {
        return this.iB;
    }

    ed bM() {
        return this.iG;
    }

    ed bN() {
        return this.iK;
    }

    public ed bO() {
        return this;
    }

    Set<Integer> by() {
        return this.hS;
    }

    ed bz() {
        return this.hT;
    }

    public int describeContents() {
        ee eeVar = CREATOR;
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ed)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ed edVar = (ed) obj;
        for (C1142a c1142a : hR.values()) {
            if (m1186a(c1142a)) {
                if (!edVar.m1186a(c1142a)) {
                    return false;
                }
                if (!m1187b(c1142a).equals(edVar.m1187b(c1142a))) {
                    return false;
                }
            } else if (edVar.m1186a(c1142a)) {
                return false;
            }
        }
        return true;
    }

    public /* synthetic */ Object freeze() {
        return bO();
    }

    public ItemScope getAbout() {
        return this.hT;
    }

    public List<String> getAdditionalName() {
        return this.hU;
    }

    public ItemScope getAddress() {
        return this.hV;
    }

    public String getAddressCountry() {
        return this.hW;
    }

    public String getAddressLocality() {
        return this.hX;
    }

    public String getAddressRegion() {
        return this.hY;
    }

    public List<ItemScope> getAssociated_media() {
        return (ArrayList) this.hZ;
    }

    public int getAttendeeCount() {
        return this.ia;
    }

    public List<ItemScope> getAttendees() {
        return (ArrayList) this.ib;
    }

    public ItemScope getAudio() {
        return this.ic;
    }

    public List<ItemScope> getAuthor() {
        return (ArrayList) this.id;
    }

    public String getBestRating() {
        return this.ie;
    }

    public String getBirthDate() {
        return this.f173if;
    }

    public ItemScope getByArtist() {
        return this.ig;
    }

    public String getCaption() {
        return this.ih;
    }

    public String getContentSize() {
        return this.ii;
    }

    public String getContentUrl() {
        return this.ij;
    }

    public List<ItemScope> getContributor() {
        return (ArrayList) this.ik;
    }

    public String getDateCreated() {
        return this.il;
    }

    public String getDateModified() {
        return this.im;
    }

    public String getDatePublished() {
        return this.in;
    }

    public String getDescription() {
        return this.ch;
    }

    public String getDuration() {
        return this.io;
    }

    public String getEmbedUrl() {
        return this.ip;
    }

    public String getEndDate() {
        return this.iq;
    }

    public String getFamilyName() {
        return this.ir;
    }

    public String getGender() {
        return this.is;
    }

    public ItemScope getGeo() {
        return this.it;
    }

    public String getGivenName() {
        return this.iu;
    }

    public String getHeight() {
        return this.iv;
    }

    public String getId() {
        return this.iw;
    }

    public String getImage() {
        return this.ix;
    }

    public ItemScope getInAlbum() {
        return this.iy;
    }

    public double getLatitude() {
        return this.ew;
    }

    public ItemScope getLocation() {
        return this.iz;
    }

    public double getLongitude() {
        return this.ex;
    }

    public String getName() {
        return this.mName;
    }

    public ItemScope getPartOfTVSeries() {
        return this.iA;
    }

    public List<ItemScope> getPerformers() {
        return (ArrayList) this.iB;
    }

    public String getPlayerType() {
        return this.iC;
    }

    public String getPostOfficeBoxNumber() {
        return this.iD;
    }

    public String getPostalCode() {
        return this.iE;
    }

    public String getRatingValue() {
        return this.iF;
    }

    public ItemScope getReviewRating() {
        return this.iG;
    }

    public String getStartDate() {
        return this.iH;
    }

    public String getStreetAddress() {
        return this.iI;
    }

    public String getText() {
        return this.iJ;
    }

    public ItemScope getThumbnail() {
        return this.iK;
    }

    public String getThumbnailUrl() {
        return this.iL;
    }

    public String getTickerSymbol() {
        return this.iM;
    }

    public String getType() {
        return this.iN;
    }

    public String getUrl() {
        return this.hE;
    }

    public String getWidth() {
        return this.iO;
    }

    public String getWorstRating() {
        return this.iP;
    }

    public boolean hasAbout() {
        return this.hS.contains(Integer.valueOf(2));
    }

    public boolean hasAdditionalName() {
        return this.hS.contains(Integer.valueOf(3));
    }

    public boolean hasAddress() {
        return this.hS.contains(Integer.valueOf(4));
    }

    public boolean hasAddressCountry() {
        return this.hS.contains(Integer.valueOf(5));
    }

    public boolean hasAddressLocality() {
        return this.hS.contains(Integer.valueOf(6));
    }

    public boolean hasAddressRegion() {
        return this.hS.contains(Integer.valueOf(7));
    }

    public boolean hasAssociated_media() {
        return this.hS.contains(Integer.valueOf(8));
    }

    public boolean hasAttendeeCount() {
        return this.hS.contains(Integer.valueOf(9));
    }

    public boolean hasAttendees() {
        return this.hS.contains(Integer.valueOf(10));
    }

    public boolean hasAudio() {
        return this.hS.contains(Integer.valueOf(11));
    }

    public boolean hasAuthor() {
        return this.hS.contains(Integer.valueOf(12));
    }

    public boolean hasBestRating() {
        return this.hS.contains(Integer.valueOf(13));
    }

    public boolean hasBirthDate() {
        return this.hS.contains(Integer.valueOf(14));
    }

    public boolean hasByArtist() {
        return this.hS.contains(Integer.valueOf(15));
    }

    public boolean hasCaption() {
        return this.hS.contains(Integer.valueOf(16));
    }

    public boolean hasContentSize() {
        return this.hS.contains(Integer.valueOf(17));
    }

    public boolean hasContentUrl() {
        return this.hS.contains(Integer.valueOf(18));
    }

    public boolean hasContributor() {
        return this.hS.contains(Integer.valueOf(19));
    }

    public boolean hasDateCreated() {
        return this.hS.contains(Integer.valueOf(20));
    }

    public boolean hasDateModified() {
        return this.hS.contains(Integer.valueOf(21));
    }

    public boolean hasDatePublished() {
        return this.hS.contains(Integer.valueOf(22));
    }

    public boolean hasDescription() {
        return this.hS.contains(Integer.valueOf(23));
    }

    public boolean hasDuration() {
        return this.hS.contains(Integer.valueOf(24));
    }

    public boolean hasEmbedUrl() {
        return this.hS.contains(Integer.valueOf(25));
    }

    public boolean hasEndDate() {
        return this.hS.contains(Integer.valueOf(26));
    }

    public boolean hasFamilyName() {
        return this.hS.contains(Integer.valueOf(27));
    }

    public boolean hasGender() {
        return this.hS.contains(Integer.valueOf(28));
    }

    public boolean hasGeo() {
        return this.hS.contains(Integer.valueOf(29));
    }

    public boolean hasGivenName() {
        return this.hS.contains(Integer.valueOf(30));
    }

    public boolean hasHeight() {
        return this.hS.contains(Integer.valueOf(31));
    }

    public boolean hasId() {
        return this.hS.contains(Integer.valueOf(32));
    }

    public boolean hasImage() {
        return this.hS.contains(Integer.valueOf(33));
    }

    public boolean hasInAlbum() {
        return this.hS.contains(Integer.valueOf(34));
    }

    public boolean hasLatitude() {
        return this.hS.contains(Integer.valueOf(36));
    }

    public boolean hasLocation() {
        return this.hS.contains(Integer.valueOf(37));
    }

    public boolean hasLongitude() {
        return this.hS.contains(Integer.valueOf(38));
    }

    public boolean hasName() {
        return this.hS.contains(Integer.valueOf(39));
    }

    public boolean hasPartOfTVSeries() {
        return this.hS.contains(Integer.valueOf(40));
    }

    public boolean hasPerformers() {
        return this.hS.contains(Integer.valueOf(41));
    }

    public boolean hasPlayerType() {
        return this.hS.contains(Integer.valueOf(42));
    }

    public boolean hasPostOfficeBoxNumber() {
        return this.hS.contains(Integer.valueOf(43));
    }

    public boolean hasPostalCode() {
        return this.hS.contains(Integer.valueOf(44));
    }

    public boolean hasRatingValue() {
        return this.hS.contains(Integer.valueOf(45));
    }

    public boolean hasReviewRating() {
        return this.hS.contains(Integer.valueOf(46));
    }

    public boolean hasStartDate() {
        return this.hS.contains(Integer.valueOf(47));
    }

    public boolean hasStreetAddress() {
        return this.hS.contains(Integer.valueOf(48));
    }

    public boolean hasText() {
        return this.hS.contains(Integer.valueOf(49));
    }

    public boolean hasThumbnail() {
        return this.hS.contains(Integer.valueOf(50));
    }

    public boolean hasThumbnailUrl() {
        return this.hS.contains(Integer.valueOf(51));
    }

    public boolean hasTickerSymbol() {
        return this.hS.contains(Integer.valueOf(52));
    }

    public boolean hasType() {
        return this.hS.contains(Integer.valueOf(53));
    }

    public boolean hasUrl() {
        return this.hS.contains(Integer.valueOf(54));
    }

    public boolean hasWidth() {
        return this.hS.contains(Integer.valueOf(55));
    }

    public boolean hasWorstRating() {
        return this.hS.contains(Integer.valueOf(56));
    }

    public int hashCode() {
        int i = 0;
        for (C1142a c1142a : hR.values()) {
            int hashCode;
            if (m1186a(c1142a)) {
                hashCode = m1187b(c1142a).hashCode() + (i + c1142a.m587N());
            } else {
                hashCode = i;
            }
            i = hashCode;
        }
        return i;
    }

    protected Object m1188j(String str) {
        return null;
    }

    protected boolean m1189k(String str) {
        return false;
    }

    int m1190u() {
        return this.f172T;
    }

    public void writeToParcel(Parcel out, int flags) {
        ee eeVar = CREATOR;
        ee.m385a(this, out, flags);
    }
}
