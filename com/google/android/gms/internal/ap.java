package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.an.C1142a;
import com.google.android.gms.internal.aq.C1144b;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class ap implements Creator<C1144b> {
    static void m140a(C1144b c1144b, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, c1144b.versionCode);
        ad.m96a(parcel, 2, c1144b.bI, false);
        ad.m95a(parcel, 3, c1144b.bJ, i, false);
        ad.m87C(parcel, d);
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m141j(x0);
    }

    public C1144b m141j(Parcel parcel) {
        C1142a c1142a = null;
        int c = ac.m58c(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    str = ac.m70l(parcel, b);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    c1142a = (C1142a) ac.m52a(parcel, b, C1142a.CREATOR);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new C1144b(i, str, c1142a);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m142p(x0);
    }

    public C1144b[] m142p(int i) {
        return new C1144b[i];
    }
}
