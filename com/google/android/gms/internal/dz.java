package com.google.android.gms.internal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusClient.C0148b;
import com.google.android.gms.plus.PlusOneButton.OnPlusOneClickListener;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class dz extends LinearLayout implements ConnectionCallbacks, OnConnectionFailedListener {
    private static final int hr;
    private int f106K;
    protected int bi;
    protected PlusClient gY;
    private int hA;
    private Uri[] hB;
    private String[] hC;
    private String[] hD;
    protected String hE;
    protected du hF;
    protected final Resources hG;
    protected final LayoutInflater hH;
    private C1222b hI;
    protected boolean hs;
    protected int ht;
    protected final LinearLayout hu;
    protected final FrameLayout hv;
    protected final CompoundButton hw;
    private final ProgressBar hx;
    protected final ea hy;
    private final dt[] hz;

    /* renamed from: com.google.android.gms.internal.dz.c */
    class C0117c extends CompoundButton {
        final /* synthetic */ dz hK;

        public C0117c(dz dzVar, Context context) {
            this.hK = dzVar;
            super(context);
        }

        public void toggle() {
            if (this.hK.hs) {
                super.toggle();
                return;
            }
            this.hK.hs = true;
            this.hK.bk();
        }
    }

    /* renamed from: com.google.android.gms.internal.dz.a */
    class C1221a implements OnClickListener, OnPlusOneClickListener {
        private final OnPlusOneClickListener hJ;
        final /* synthetic */ dz hK;

        public C1221a(dz dzVar, OnPlusOneClickListener onPlusOneClickListener) {
            this.hK = dzVar;
            this.hJ = onPlusOneClickListener;
        }

        public void onClick(View view) {
            if (view == this.hK.hw || view == this.hK.hy) {
                Intent intent = this.hK.hF == null ? null : this.hK.hF.getIntent();
                if (this.hJ != null) {
                    this.hJ.onPlusOneClick(intent);
                } else {
                    onPlusOneClick(intent);
                }
            }
        }

        public void onPlusOneClick(Intent intent) {
            Context context = this.hK.getContext();
            if ((context instanceof Activity) && intent != null) {
                ((Activity) context).startActivityForResult(intent, this.hK.ht);
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.dz.b */
    public class C1222b implements C0148b {
        final /* synthetic */ dz hK;

        protected C1222b(dz dzVar) {
            this.hK = dzVar;
        }

        public void m878a(ConnectionResult connectionResult, du duVar) {
            if (this.hK.hs) {
                this.hK.hs = false;
                this.hK.hw.refreshDrawableState();
            }
            if (!connectionResult.isSuccess() || duVar == null) {
                this.hK.bl();
                return;
            }
            this.hK.hF = duVar;
            this.hK.bc();
            this.hK.bi();
        }
    }

    static {
        hr = Color.parseColor("#666666");
    }

    public dz(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.ht = 0;
        this.hz = new dt[4];
        this.bi = 1;
        this.hA = 2;
        this.f106K = 3;
        this.hI = new C1222b(this);
        C0142x.m492b((Object) context, (Object) "Context must not be null.");
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) != 0) {
            this.hG = null;
            this.hH = null;
        } else {
            Context k = m889k(context);
            this.hG = k.getResources();
            this.hH = (LayoutInflater) k.getSystemService("layout_inflater");
        }
        this.f106K = m879a(context, attributeSet);
        this.hA = m882b(context, attributeSet);
        Point point = new Point();
        m880a(point);
        if (isInEditMode()) {
            View textView = new TextView(context);
            textView.setGravity(17);
            textView.setText("[ +1 ]");
            addView(textView, new LayoutParams(point.x, point.y));
            this.hy = null;
            this.hx = null;
            this.hw = null;
            this.hv = null;
            this.hu = null;
            return;
        }
        setFocusable(true);
        this.hu = new LinearLayout(context);
        this.hu.setGravity(17);
        this.hu.setOrientation(0);
        addView(this.hu);
        this.hw = new C0117c(this, context);
        this.hw.setBackgroundDrawable(null);
        this.hy = m892n(context);
        this.hv = m890l(context);
        this.hv.addView(this.hw, new FrameLayout.LayoutParams(point.x, point.y, 17));
        m883b(point);
        this.hx = m891m(context);
        this.hx.setVisibility(4);
        this.hv.addView(this.hx, new FrameLayout.LayoutParams(point.x, point.y, 17));
        int length = this.hz.length;
        for (int i = 0; i < length; i++) {
            this.hz[i] = m893o(getContext());
        }
        bm();
    }

    private int m879a(Context context, AttributeSet attributeSet) {
        String a = ab.m50a("http://schemas.android.com/apk/lib/com.google.android.gms.plus", "size", context, attributeSet, true, false, "PlusOneButton");
        return "SMALL".equalsIgnoreCase(a) ? 0 : "MEDIUM".equalsIgnoreCase(a) ? 1 : "TALL".equalsIgnoreCase(a) ? 2 : "STANDARD".equalsIgnoreCase(a) ? 3 : 3;
    }

    private void m880a(Point point) {
        int i = 24;
        int i2 = 20;
        switch (this.f106K) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                i2 = 14;
                break;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                i = 32;
                break;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                i = 50;
                break;
            default:
                i = 38;
                i2 = 24;
                break;
        }
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float applyDimension = TypedValue.applyDimension(1, (float) i, displayMetrics);
        float applyDimension2 = TypedValue.applyDimension(1, (float) i2, displayMetrics);
        point.x = (int) (((double) applyDimension) + 0.5d);
        point.y = (int) (((double) applyDimension2) + 0.5d);
    }

    private void m881a(Uri[] uriArr) {
        this.hB = uriArr;
        bq();
    }

    private int m882b(Context context, AttributeSet attributeSet) {
        String a = ab.m50a("http://schemas.android.com/apk/lib/com.google.android.gms.plus", "annotation", context, attributeSet, true, false, "PlusOneButton");
        return "INLINE".equalsIgnoreCase(a) ? 2 : "NONE".equalsIgnoreCase(a) ? 0 : "BUBBLE".equalsIgnoreCase(a) ? 1 : 1;
    }

    private void m883b(Point point) {
        point.y = (int) (((float) point.y) - TypedValue.applyDimension(1, 6.0f, getResources().getDisplayMetrics()));
        point.x = point.y;
    }

    private void bh() {
        int i = 1;
        int applyDimension = (int) TypedValue.applyDimension(1, 5.0f, getContext().getResources().getDisplayMetrics());
        int applyDimension2 = (int) TypedValue.applyDimension(1, 1.0f, getContext().getResources().getDisplayMetrics());
        int length = this.hz.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (this.hz[i2].getVisibility() == 0) {
                ViewGroup.LayoutParams layoutParams = new LayoutParams(this.hz[i2].getLayoutParams());
                if (i != 0) {
                    layoutParams.setMargins(applyDimension, 0, applyDimension2, 0);
                    i = 0;
                } else {
                    layoutParams.setMargins(applyDimension2, 0, applyDimension2, 0);
                }
                this.hz[i2].setLayoutParams(layoutParams);
            }
        }
    }

    private LayoutParams bj() {
        LayoutParams layoutParams;
        int i = 0;
        switch (this.hA) {
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                layoutParams = new LayoutParams(-2, -2);
                break;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                layoutParams = new LayoutParams(-2, -1);
                break;
            default:
                layoutParams = new LayoutParams(-2, -2);
                break;
        }
        layoutParams.bottomMargin = this.f106K == 2 ? 1 : 0;
        if (this.f106K != 2) {
            i = 1;
        }
        layoutParams.leftMargin = i;
        return layoutParams;
    }

    private void bp() {
        switch (this.hA) {
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                this.hy.m381f(this.hD);
                this.hy.setVisibility(0);
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                this.hy.m381f(this.hC);
                this.hy.setVisibility(0);
            default:
                this.hy.m381f(null);
                this.hy.setVisibility(8);
        }
    }

    private void bq() {
        int i = 0;
        if (this.hB == null || this.hA != 2) {
            int length = this.hz.length;
            while (i < length) {
                this.hz[i].setVisibility(8);
                i++;
            }
        } else {
            Point point = new Point();
            m880a(point);
            point.x = point.y;
            int length2 = this.hz.length;
            int length3 = this.hB.length;
            int i2 = 0;
            while (i2 < length2) {
                Uri uri = i2 < length3 ? this.hB[i2] : null;
                if (uri == null) {
                    this.hz[i2].setVisibility(8);
                } else {
                    this.hz[i2].setLayoutParams(new LayoutParams(point.x, point.y));
                    this.hz[i2].m830a(uri, point.y);
                    this.hz[i2].setVisibility(0);
                }
                i2++;
            }
        }
        bh();
    }

    private Drawable br() {
        return this.hG == null ? null : this.hG.getDrawable(this.hG.getIdentifier(bs(), "drawable", GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE));
    }

    private String bs() {
        switch (this.f106K) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return "ic_plusone_small";
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                return "ic_plusone_medium";
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return "ic_plusone_tall";
            default:
                return "ic_plusone_standard";
        }
    }

    private Uri bt() {
        return C0143y.m496i(bu());
    }

    private String bu() {
        switch (this.f106K) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return "global_count_bubble_small";
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                return "global_count_bubble_medium";
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return "global_count_bubble_tall";
            default:
                return "global_count_bubble_standard";
        }
    }

    private void m884c(int i, int i2) {
        this.bi = i2;
        this.f106K = i;
        bi();
    }

    private void m885c(View view) {
        int applyDimension = (int) TypedValue.applyDimension(1, 3.0f, getContext().getResources().getDisplayMetrics());
        int applyDimension2 = (int) TypedValue.applyDimension(1, 5.0f, getContext().getResources().getDisplayMetrics());
        if (this.hA != 2) {
            applyDimension2 = 0;
        }
        if (!(this.f106K == 2 && this.hA == 1)) {
            applyDimension = 0;
        }
        view.setPadding(applyDimension2, 0, 0, applyDimension);
    }

    private static int m886d(int i, int i2) {
        switch (i) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return 11;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return i2 != 2 ? 15 : 13;
            default:
                return 13;
        }
    }

    private void m887d(String[] strArr) {
        this.hC = strArr;
        bp();
    }

    private void m888e(String[] strArr) {
        this.hD = strArr;
        bp();
    }

    private Context m889k(Context context) {
        try {
            return getContext().createPackageContext(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, 4);
        } catch (NameNotFoundException e) {
            if (Log.isLoggable("PlusOneButton", 5)) {
                Log.w("PlusOneButton", "Google Play services is not installed");
            }
            return null;
        }
    }

    private FrameLayout m890l(Context context) {
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setFocusable(false);
        return frameLayout;
    }

    private ProgressBar m891m(Context context) {
        ProgressBar progressBar = new ProgressBar(context, null, 16843400);
        progressBar.setFocusable(false);
        progressBar.setIndeterminate(true);
        return progressBar;
    }

    private ea m892n(Context context) {
        ea eaVar = new ea(context);
        eaVar.setFocusable(false);
        eaVar.setGravity(17);
        eaVar.setSingleLine();
        eaVar.setTextSize(0, TypedValue.applyDimension(2, (float) m886d(this.f106K, this.hA), context.getResources().getDisplayMetrics()));
        eaVar.setTextColor(hr);
        eaVar.setVisibility(0);
        return eaVar;
    }

    private dt m893o(Context context) {
        dt dtVar = new dt(context);
        dtVar.setVisibility(8);
        return dtVar;
    }

    protected void bc() {
        if (this.hF != null) {
            m887d(this.hF.bf());
            m888e(new String[]{this.hF.be()});
            m881a(this.hF.bg());
            if (this.hF.bd()) {
                bn();
            } else {
                bm();
            }
        }
    }

    protected void bi() {
        int i = 0;
        if (!isInEditMode()) {
            this.hu.removeAllViews();
            Point point = new Point();
            m880a(point);
            this.hw.setLayoutParams(new FrameLayout.LayoutParams(point.x, point.y, 17));
            m883b(point);
            this.hx.setLayoutParams(new FrameLayout.LayoutParams(point.x, point.y, 17));
            if (this.hA == 1) {
                this.hy.m380a(bt());
            } else {
                this.hy.m380a(null);
            }
            bq();
            this.hy.setLayoutParams(bj());
            this.hy.setTextSize(0, TypedValue.applyDimension(2, (float) m886d(this.f106K, this.hA), getContext().getResources().getDisplayMetrics()));
            m885c(this.hy);
            if (this.f106K == 2 && this.hA == 1) {
                this.hu.setOrientation(1);
                this.hu.addView(this.hy);
                this.hu.addView(this.hv);
            } else {
                this.hu.setOrientation(0);
                this.hu.addView(this.hv);
                int length = this.hz.length;
                while (i < length) {
                    this.hu.addView(this.hz[i]);
                    i++;
                }
                this.hu.addView(this.hy);
            }
            requestLayout();
        }
    }

    public void bk() {
        setType(2);
        this.hx.setVisibility(0);
        bo();
    }

    public void bl() {
        setType(3);
        this.hx.setVisibility(4);
        bo();
    }

    protected void bm() {
        setType(1);
        this.hx.setVisibility(4);
        bo();
    }

    protected void bn() {
        setType(0);
        this.hx.setVisibility(4);
        bo();
    }

    protected void bo() {
        this.hw.setButtonDrawable(br());
        switch (this.bi) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                this.hw.setEnabled(true);
                this.hw.setChecked(true);
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                this.hw.setEnabled(true);
                this.hw.setChecked(false);
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                this.hw.setEnabled(false);
                this.hw.setChecked(true);
            default:
                this.hw.setEnabled(false);
                this.hw.setChecked(false);
        }
    }

    public void initialize(PlusClient plusClient, String url, int activityRequestCode) {
        int i = 0;
        C0142x.m492b((Object) plusClient, (Object) "Plus client must not be null.");
        C0142x.m492b((Object) url, (Object) "URL must not be null.");
        boolean z = activityRequestCode >= 0 && activityRequestCode <= Menu.USER_MASK;
        C0142x.m490a(z, "activityRequestCode must be an unsigned 16 bit integer.");
        this.ht = activityRequestCode;
        this.hE = url;
        if (plusClient != this.gY) {
            if (this.gY != null) {
                this.gY.unregisterConnectionCallbacks(this);
                this.gY.unregisterConnectionFailedListener(this);
            }
            this.gY = plusClient;
            this.gY.registerConnectionCallbacks(this);
            this.gY.registerConnectionFailedListener(this);
            int length = this.hz.length;
            while (i < length) {
                this.hz[i].m832a(plusClient);
                i++;
            }
        } else if (this.gY.isConnected()) {
            onConnected(null);
        }
        bi();
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.gY != null) {
            if (!this.gY.isConnectionCallbacksRegistered(this)) {
                this.gY.registerConnectionCallbacks(this);
            }
            if (!this.gY.isConnectionFailedListenerRegistered(this)) {
                this.gY.registerConnectionFailedListener(this);
            }
        }
    }

    public void onConnected(Bundle connectionHint) {
        if (this.hE != null) {
            this.gY.m1015a(this.hI, this.hE);
        }
    }

    public void onConnectionFailed(ConnectionResult status) {
        bl();
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.gY != null) {
            if (this.gY.isConnectionCallbacksRegistered(this)) {
                this.gY.unregisterConnectionCallbacks(this);
            }
            if (this.gY.isConnectionFailedListenerRegistered(this)) {
                this.gY.unregisterConnectionFailedListener(this);
            }
        }
    }

    public void onDisconnected() {
    }

    public boolean performClick() {
        return this.hw.performClick();
    }

    public void setAnnotation(int annotation) {
        C0142x.m492b(Integer.valueOf(annotation), (Object) "Annotation must not be null.");
        this.hA = annotation;
        bp();
        bi();
    }

    public void setOnClickListener(OnClickListener listener) {
        this.hw.setOnClickListener(listener);
        this.hy.setOnClickListener(listener);
    }

    public void setOnPlusOneClickListener(OnPlusOneClickListener listener) {
        setOnClickListener(new C1221a(this, listener));
    }

    public void setSize(int size) {
        m884c(size, this.bi);
    }

    public void setType(int type) {
        m884c(this.f106K, type);
    }
}
