package com.google.android.gms.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.internal.bc.C1151a;
import com.google.android.gms.internal.dj.C1194a;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate;
import com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate.C1249a;
import com.google.android.gms.maps.internal.IMapFragmentDelegate;
import com.google.android.gms.maps.internal.IMapFragmentDelegate.C1255a;
import com.google.android.gms.maps.internal.IMapViewDelegate;
import com.google.android.gms.maps.internal.IMapViewDelegate.C1257a;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public interface cl extends IInterface {

    /* renamed from: com.google.android.gms.internal.cl.a */
    public static abstract class C1174a extends Binder implements cl {

        /* renamed from: com.google.android.gms.internal.cl.a.a */
        static class C1173a implements cl {
            private IBinder f85a;

            C1173a(IBinder iBinder) {
                this.f85a = iBinder;
            }

            public IMapViewDelegate m774a(bc bcVar, GoogleMapOptions googleMapOptions) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.internal.ICreator");
                    obtain.writeStrongBinder(bcVar != null ? bcVar.asBinder() : null);
                    if (googleMapOptions != null) {
                        obtain.writeInt(1);
                        googleMapOptions.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f85a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    IMapViewDelegate y = C1257a.m988y(obtain2.readStrongBinder());
                    return y;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m775a(bc bcVar, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.internal.ICreator");
                    obtain.writeStrongBinder(bcVar != null ? bcVar.asBinder() : null);
                    obtain.writeInt(i);
                    this.f85a.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public ICameraUpdateFactoryDelegate aR() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.internal.ICreator");
                    this.f85a.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                    ICameraUpdateFactoryDelegate r = C1249a.m984r(obtain2.readStrongBinder());
                    return r;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public dj aS() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.internal.ICreator");
                    this.f85a.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                    dj J = C1194a.m803J(obtain2.readStrongBinder());
                    return J;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f85a;
            }

            public void m776c(bc bcVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.internal.ICreator");
                    obtain.writeStrongBinder(bcVar != null ? bcVar.asBinder() : null);
                    this.f85a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IMapFragmentDelegate m777d(bc bcVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.maps.internal.ICreator");
                    obtain.writeStrongBinder(bcVar != null ? bcVar.asBinder() : null);
                    this.f85a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    IMapFragmentDelegate x = C1255a.m987x(obtain2.readStrongBinder());
                    return x;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public static cl m778t(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.internal.ICreator");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof cl)) ? new C1173a(iBinder) : (cl) queryLocalInterface;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            IBinder iBinder = null;
            switch (code) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    data.enforceInterface("com.google.android.gms.maps.internal.ICreator");
                    m300c(C1151a.m631j(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    data.enforceInterface("com.google.android.gms.maps.internal.ICreator");
                    IMapFragmentDelegate d = m301d(C1151a.m631j(data.readStrongBinder()));
                    reply.writeNoException();
                    if (d != null) {
                        iBinder = d.asBinder();
                    }
                    reply.writeStrongBinder(iBinder);
                    return true;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    data.enforceInterface("com.google.android.gms.maps.internal.ICreator");
                    IMapViewDelegate a = m298a(C1151a.m631j(data.readStrongBinder()), data.readInt() != 0 ? GoogleMapOptions.CREATOR.createFromParcel(data) : null);
                    reply.writeNoException();
                    if (a != null) {
                        iBinder = a.asBinder();
                    }
                    reply.writeStrongBinder(iBinder);
                    return true;
                case UserListView.TYPE_FAVE /*4*/:
                    data.enforceInterface("com.google.android.gms.maps.internal.ICreator");
                    ICameraUpdateFactoryDelegate aR = aR();
                    reply.writeNoException();
                    if (aR != null) {
                        iBinder = aR.asBinder();
                    }
                    reply.writeStrongBinder(iBinder);
                    return true;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    data.enforceInterface("com.google.android.gms.maps.internal.ICreator");
                    dj aS = aS();
                    reply.writeNoException();
                    if (aS != null) {
                        iBinder = aS.asBinder();
                    }
                    reply.writeStrongBinder(iBinder);
                    return true;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    data.enforceInterface("com.google.android.gms.maps.internal.ICreator");
                    m299a(C1151a.m631j(data.readStrongBinder()), data.readInt());
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.maps.internal.ICreator");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    IMapViewDelegate m298a(bc bcVar, GoogleMapOptions googleMapOptions) throws RemoteException;

    void m299a(bc bcVar, int i) throws RemoteException;

    ICameraUpdateFactoryDelegate aR() throws RemoteException;

    dj aS() throws RemoteException;

    void m300c(bc bcVar) throws RemoteException;

    IMapFragmentDelegate m301d(bc bcVar) throws RemoteException;
}
