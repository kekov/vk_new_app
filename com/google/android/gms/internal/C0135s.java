package com.google.android.gms.internal;

import android.util.Log;

/* renamed from: com.google.android.gms.internal.s */
public final class C0135s {
    private final String aV;

    public C0135s(String str) {
        this.aV = (String) C0142x.m495d(str);
    }

    public void m467a(String str, String str2) {
        if (m472i(3)) {
            Log.d(str, str2);
        }
    }

    public void m468a(String str, String str2, Throwable th) {
        if (m472i(6)) {
            Log.e(str, str2, th);
        }
    }

    public void m469b(String str, String str2) {
        if (m472i(5)) {
            Log.w(str, str2);
        }
    }

    public void m470c(String str, String str2) {
        if (m472i(6)) {
            Log.e(str, str2);
        }
    }

    public void m471d(String str, String str2) {
        if (!m472i(4)) {
        }
    }

    public boolean m472i(int i) {
        return Log.isLoggable(this.aV, i);
    }
}
