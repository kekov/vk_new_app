package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantEntity;

public final class bx extends C0123j implements Participant {
    private final bg dQ;

    public bx(C1230k c1230k, int i) {
        super(c1230k, i);
        this.dQ = new bg(c1230k, i);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return ParticipantEntity.m1039a(this, obj);
    }

    public Participant freeze() {
        return new ParticipantEntity(this);
    }

    public String getClientAddress() {
        return getString("client_address");
    }

    public String getDisplayName() {
        return m436d("external_player_id") ? getString("default_display_name") : this.dQ.getDisplayName();
    }

    public void getDisplayName(CharArrayBuffer dataOut) {
        if (m436d("external_player_id")) {
            m434a("default_display_name", dataOut);
        } else {
            this.dQ.getDisplayName(dataOut);
        }
    }

    public Uri getHiResImageUri() {
        return m436d("external_player_id") ? null : this.dQ.getHiResImageUri();
    }

    public Uri getIconImageUri() {
        return m436d("external_player_id") ? m435c("default_display_image_uri") : this.dQ.getIconImageUri();
    }

    public String getParticipantId() {
        return getString("external_participant_id");
    }

    public Player getPlayer() {
        return m436d("external_player_id") ? null : this.dQ;
    }

    public int getStatus() {
        return getInteger("player_status");
    }

    public int hashCode() {
        return ParticipantEntity.m1038a(this);
    }

    public boolean isConnectedToRoom() {
        return getInteger("connected") > 0;
    }

    public String toString() {
        return ParticipantEntity.m1040b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        ((ParticipantEntity) freeze()).writeToParcel(dest, flags);
    }
}
