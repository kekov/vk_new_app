package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.Tile;

public class dg {
    public static void m326a(Tile tile, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, tile.m1006u());
        ad.m106c(parcel, 2, tile.width);
        ad.m106c(parcel, 3, tile.height);
        ad.m100a(parcel, 4, tile.data, false);
        ad.m87C(parcel, d);
    }
}
