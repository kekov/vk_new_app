package com.google.android.gms.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.an.C1142a;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import net.hockeyapp.android.Strings;

public class at extends an implements ae {
    public static final au CREATOR;
    private final int f70T;
    private final aq bC;
    private final Parcel bK;
    private final int bL;
    private int bM;
    private int bN;
    private final String mClassName;

    static {
        CREATOR = new au();
    }

    at(int i, Parcel parcel, aq aqVar) {
        this.f70T = i;
        this.bK = (Parcel) C0142x.m495d(parcel);
        this.bL = 2;
        this.bC = aqVar;
        if (this.bC == null) {
            this.mClassName = null;
        } else {
            this.mClassName = this.bC.m602W();
        }
        this.bM = 2;
    }

    private at(ae aeVar, aq aqVar, String str) {
        this.f70T = 1;
        this.bK = Parcel.obtain();
        aeVar.writeToParcel(this.bK, 0);
        this.bL = 1;
        this.bC = (aq) C0142x.m495d(aqVar);
        this.mClassName = (String) C0142x.m495d(str);
        this.bM = 2;
    }

    public static <T extends an & ae> at m607a(T t) {
        String canonicalName = t.getClass().getCanonicalName();
        return new at((ae) t, m614b((an) t), canonicalName);
    }

    public static HashMap<String, String> m608a(Bundle bundle) {
        HashMap<String, String> hashMap = new HashMap();
        for (String str : bundle.keySet()) {
            hashMap.put(str, bundle.getString(str));
        }
        return hashMap;
    }

    private static void m609a(aq aqVar, an anVar) {
        Class cls = anVar.getClass();
        if (!aqVar.m604a(cls)) {
            HashMap G = anVar.m127G();
            aqVar.m603a(cls, anVar.m127G());
            for (String str : G.keySet()) {
                C1142a c1142a = (C1142a) G.get(str);
                Class O = c1142a.m588O();
                if (O != null) {
                    try {
                        m609a(aqVar, (an) O.newInstance());
                    } catch (Throwable e) {
                        throw new IllegalStateException("Could not instantiate an object of type " + c1142a.m588O().getCanonicalName(), e);
                    } catch (Throwable e2) {
                        throw new IllegalStateException("Could not access object of type " + c1142a.m588O().getCanonicalName(), e2);
                    }
                }
            }
        }
    }

    private void m610a(StringBuilder stringBuilder, int i, Object obj) {
        switch (i) {
            case ValidationActivity.VRESULT_NONE /*0*/:
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
            case UserListView.TYPE_FAVE /*4*/:
            case UserListView.TYPE_FOLLOWERS /*5*/:
            case UserListView.TYPE_POLL_VOTERS /*6*/:
                stringBuilder.append(obj);
            case UserListView.TYPE_FAVE_LINKS /*7*/:
                stringBuilder.append("\"").append(ay.m163o(obj.toString())).append("\"");
            case UserListView.TYPE_BLACKLIST /*8*/:
                stringBuilder.append("\"").append(aw.m160a((byte[]) obj)).append("\"");
            case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                stringBuilder.append("\"").append(aw.m161b((byte[]) obj));
                stringBuilder.append("\"");
            case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                az.m164a(stringBuilder, (HashMap) obj);
            case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                throw new IllegalArgumentException("Unknown type = " + i);
        }
    }

    private void m611a(StringBuilder stringBuilder, C1142a<?, ?> c1142a, Parcel parcel, int i) {
        switch (c1142a.m582F()) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                m617b(stringBuilder, (C1142a) c1142a, m130a(c1142a, Integer.valueOf(ac.m63f(parcel, i))));
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                m617b(stringBuilder, (C1142a) c1142a, m130a(c1142a, ac.m65h(parcel, i)));
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                m617b(stringBuilder, (C1142a) c1142a, m130a(c1142a, Long.valueOf(ac.m64g(parcel, i))));
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                m617b(stringBuilder, (C1142a) c1142a, m130a(c1142a, Float.valueOf(ac.m66i(parcel, i))));
            case UserListView.TYPE_FAVE /*4*/:
                m617b(stringBuilder, (C1142a) c1142a, m130a(c1142a, Double.valueOf(ac.m67j(parcel, i))));
            case UserListView.TYPE_FOLLOWERS /*5*/:
                m617b(stringBuilder, (C1142a) c1142a, m130a(c1142a, ac.m69k(parcel, i)));
            case UserListView.TYPE_POLL_VOTERS /*6*/:
                m617b(stringBuilder, (C1142a) c1142a, m130a(c1142a, Boolean.valueOf(ac.m60c(parcel, i))));
            case UserListView.TYPE_FAVE_LINKS /*7*/:
                m617b(stringBuilder, (C1142a) c1142a, m130a(c1142a, ac.m70l(parcel, i)));
            case UserListView.TYPE_BLACKLIST /*8*/:
            case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                m617b(stringBuilder, (C1142a) c1142a, m130a(c1142a, ac.m73o(parcel, i)));
            case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                m617b(stringBuilder, (C1142a) c1142a, m130a(c1142a, m608a(ac.m72n(parcel, i))));
            case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                throw new IllegalArgumentException("Unknown field out type = " + c1142a.m582F());
        }
    }

    private void m612a(StringBuilder stringBuilder, String str, C1142a<?, ?> c1142a, Parcel parcel, int i) {
        stringBuilder.append("\"").append(str).append("\":");
        if (c1142a.m590Q()) {
            m611a(stringBuilder, c1142a, parcel, i);
        } else {
            m616b(stringBuilder, c1142a, parcel, i);
        }
    }

    private void m613a(StringBuilder stringBuilder, HashMap<String, C1142a<?, ?>> hashMap, Parcel parcel) {
        HashMap b = m615b((HashMap) hashMap);
        stringBuilder.append('{');
        int c = ac.m58c(parcel);
        Object obj = null;
        while (parcel.dataPosition() < c) {
            int b2 = ac.m55b(parcel);
            Entry entry = (Entry) b.get(Integer.valueOf(ac.m68j(b2)));
            if (entry != null) {
                if (obj != null) {
                    stringBuilder.append(",");
                }
                m612a(stringBuilder, (String) entry.getKey(), (C1142a) entry.getValue(), parcel, b2);
                obj = 1;
            }
        }
        if (parcel.dataPosition() != c) {
            throw new C0107a("Overread allowed size end=" + c, parcel);
        }
        stringBuilder.append('}');
    }

    private static aq m614b(an anVar) {
        aq aqVar = new aq(anVar.getClass());
        m609a(aqVar, anVar);
        aqVar.m600U();
        aqVar.m599T();
        return aqVar;
    }

    private static HashMap<Integer, Entry<String, C1142a<?, ?>>> m615b(HashMap<String, C1142a<?, ?>> hashMap) {
        HashMap<Integer, Entry<String, C1142a<?, ?>>> hashMap2 = new HashMap();
        for (Entry entry : hashMap.entrySet()) {
            hashMap2.put(Integer.valueOf(((C1142a) entry.getValue()).m587N()), entry);
        }
        return hashMap2;
    }

    private void m616b(StringBuilder stringBuilder, C1142a<?, ?> c1142a, Parcel parcel, int i) {
        if (c1142a.m585L()) {
            stringBuilder.append("[");
            switch (c1142a.m582F()) {
                case ValidationActivity.VRESULT_NONE /*0*/:
                    av.m154a(stringBuilder, ac.m75q(parcel, i));
                    break;
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    av.m156a(stringBuilder, ac.m77s(parcel, i));
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    av.m155a(stringBuilder, ac.m76r(parcel, i));
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    av.m153a(stringBuilder, ac.m78t(parcel, i));
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    av.m152a(stringBuilder, ac.m79u(parcel, i));
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    av.m156a(stringBuilder, ac.m80v(parcel, i));
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    av.m158a(stringBuilder, ac.m74p(parcel, i));
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    av.m157a(stringBuilder, ac.m81w(parcel, i));
                    break;
                case UserListView.TYPE_BLACKLIST /*8*/:
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                    throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                    Parcel[] z = ac.m84z(parcel, i);
                    int length = z.length;
                    for (int i2 = 0; i2 < length; i2++) {
                        if (i2 > 0) {
                            stringBuilder.append(",");
                        }
                        z[i2].setDataPosition(0);
                        m613a(stringBuilder, c1142a.m592S(), z[i2]);
                    }
                    break;
                default:
                    throw new IllegalStateException("Unknown field type out.");
            }
            stringBuilder.append("]");
            return;
        }
        switch (c1142a.m582F()) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                stringBuilder.append(ac.m63f(parcel, i));
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                stringBuilder.append(ac.m65h(parcel, i));
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                stringBuilder.append(ac.m64g(parcel, i));
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
                stringBuilder.append(ac.m66i(parcel, i));
            case UserListView.TYPE_FAVE /*4*/:
                stringBuilder.append(ac.m67j(parcel, i));
            case UserListView.TYPE_FOLLOWERS /*5*/:
                stringBuilder.append(ac.m69k(parcel, i));
            case UserListView.TYPE_POLL_VOTERS /*6*/:
                stringBuilder.append(ac.m60c(parcel, i));
            case UserListView.TYPE_FAVE_LINKS /*7*/:
                stringBuilder.append("\"").append(ay.m163o(ac.m70l(parcel, i))).append("\"");
            case UserListView.TYPE_BLACKLIST /*8*/:
                stringBuilder.append("\"").append(aw.m160a(ac.m73o(parcel, i))).append("\"");
            case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                stringBuilder.append("\"").append(aw.m161b(ac.m73o(parcel, i)));
                stringBuilder.append("\"");
            case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                Bundle n = ac.m72n(parcel, i);
                Set<String> keySet = n.keySet();
                keySet.size();
                stringBuilder.append("{");
                int i3 = 1;
                for (String str : keySet) {
                    if (i3 == 0) {
                        stringBuilder.append(",");
                    }
                    stringBuilder.append("\"").append(str).append("\"");
                    stringBuilder.append(":");
                    stringBuilder.append("\"").append(ay.m163o(n.getString(str))).append("\"");
                    i3 = 0;
                }
                stringBuilder.append("}");
            case Strings.UPDATE_DIALOG_NEGATIVE_BUTTON_ID /*11*/:
                Parcel y = ac.m83y(parcel, i);
                y.setDataPosition(0);
                m613a(stringBuilder, c1142a.m592S(), y);
            default:
                throw new IllegalStateException("Unknown field type out");
        }
    }

    private void m617b(StringBuilder stringBuilder, C1142a<?, ?> c1142a, Object obj) {
        if (c1142a.m584K()) {
            m618b(stringBuilder, (C1142a) c1142a, (ArrayList) obj);
        } else {
            m610a(stringBuilder, c1142a.m581E(), obj);
        }
    }

    private void m618b(StringBuilder stringBuilder, C1142a<?, ?> c1142a, ArrayList<?> arrayList) {
        stringBuilder.append("[");
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                stringBuilder.append(",");
            }
            m610a(stringBuilder, c1142a.m581E(), arrayList.get(i));
        }
        stringBuilder.append("]");
    }

    public HashMap<String, C1142a<?, ?>> m619G() {
        return this.bC == null ? null : this.bC.m605n(this.mClassName);
    }

    public Parcel m620Y() {
        switch (this.bM) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                this.bN = ad.m108d(this.bK);
                ad.m87C(this.bK, this.bN);
                this.bM = 2;
                break;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                ad.m87C(this.bK, this.bN);
                this.bM = 2;
                break;
        }
        return this.bK;
    }

    aq m621Z() {
        switch (this.bL) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return null;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                return this.bC;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return this.bC;
            default:
                throw new IllegalStateException("Invalid creation type: " + this.bL);
        }
    }

    public <T extends ae> T m622a(Creator<T> creator) {
        Parcel Y = m620Y();
        Y.setDataPosition(0);
        return (ae) creator.createFromParcel(Y);
    }

    public int describeContents() {
        au auVar = CREATOR;
        return 0;
    }

    protected Object m623j(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    protected boolean m624k(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    public String toString() {
        C0142x.m492b(this.bC, (Object) "Cannot convert to JSON on client side.");
        Parcel Y = m620Y();
        Y.setDataPosition(0);
        StringBuilder stringBuilder = new StringBuilder(100);
        m613a(stringBuilder, this.bC.m605n(this.mClassName), Y);
        return stringBuilder.toString();
    }

    public int m625u() {
        return this.f70T;
    }

    public void writeToParcel(Parcel out, int flags) {
        au auVar = CREATOR;
        au.m149a(this, out, flags);
    }
}
