package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.PolygonOptions;

public class de {
    public static void m324a(PolygonOptions polygonOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, polygonOptions.m1004u());
        ad.m105b(parcel, 2, polygonOptions.getPoints(), false);
        ad.m107c(parcel, 3, polygonOptions.aZ(), false);
        ad.m90a(parcel, 4, polygonOptions.getStrokeWidth());
        ad.m106c(parcel, 5, polygonOptions.getStrokeColor());
        ad.m106c(parcel, 6, polygonOptions.getFillColor());
        ad.m90a(parcel, 7, polygonOptions.getZIndex());
        ad.m99a(parcel, 8, polygonOptions.isVisible());
        ad.m99a(parcel, 9, polygonOptions.isGeodesic());
        ad.m87C(parcel, d);
    }
}
