package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.PolylineOptions;

public class df {
    public static void m325a(PolylineOptions polylineOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, polylineOptions.m1005u());
        ad.m105b(parcel, 2, polylineOptions.getPoints(), false);
        ad.m90a(parcel, 3, polylineOptions.getWidth());
        ad.m106c(parcel, 4, polylineOptions.getColor());
        ad.m90a(parcel, 5, polylineOptions.getZIndex());
        ad.m99a(parcel, 6, polylineOptions.isVisible());
        ad.m99a(parcel, 7, polylineOptions.isGeodesic());
        ad.m87C(parcel, d);
    }
}
