package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.C0136t.C1236a;
import com.google.android.gms.internal.C0137u.C1238a;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.ValidationActivity;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.internal.p */
public abstract class C1234p<T extends IInterface> implements GooglePlayServicesClient {
    public static final String[] aE;
    private C0130e aA;
    boolean aB;
    boolean aC;
    private final Object aD;
    private T at;
    private ArrayList<ConnectionCallbacks> au;
    final ArrayList<ConnectionCallbacks> av;
    private boolean aw;
    private ArrayList<OnConnectionFailedListener> ax;
    private boolean ay;
    private final ArrayList<C0129b<?>> az;
    private final String[] f123f;
    private final Context mContext;
    final Handler mHandler;

    /* renamed from: com.google.android.gms.internal.p.a */
    final class C0128a extends Handler {
        final /* synthetic */ C1234p aF;

        public C0128a(C1234p c1234p, Looper looper) {
            this.aF = c1234p;
            super(looper);
        }

        public void handleMessage(Message msg) {
            synchronized (this.aF.aD) {
                this.aF.aC = false;
            }
            if (msg.what == 3) {
                this.aF.m943a(new ConnectionResult(((Integer) msg.obj).intValue(), null));
            } else if (msg.what == 4) {
                synchronized (this.aF.au) {
                    if (this.aF.aB && this.aF.isConnected() && this.aF.au.contains(msg.obj)) {
                        ((ConnectionCallbacks) msg.obj).onConnected(this.aF.m953l());
                    }
                }
            } else if (msg.what == 2 && !this.aF.isConnected()) {
            } else {
                if (msg.what == 2 || msg.what == 1) {
                    ((C0129b) msg.obj).m446p();
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.p.b */
    public abstract class C0129b<TListener> {
        final /* synthetic */ C1234p aF;
        private TListener mListener;

        public C0129b(C1234p c1234p, TListener tListener) {
            this.aF = c1234p;
            this.mListener = tListener;
            synchronized (c1234p.az) {
                c1234p.az.add(this);
            }
        }

        protected abstract void m445a(TListener tListener);

        public void m446p() {
            Object obj;
            synchronized (this) {
                obj = this.mListener;
            }
            m445a(obj);
        }

        public void m447q() {
            synchronized (this) {
                this.mListener = null;
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.p.e */
    final class C0130e implements ServiceConnection {
        final /* synthetic */ C1234p aF;

        C0130e(C1234p c1234p) {
            this.aF = c1234p;
        }

        public void onServiceConnected(ComponentName component, IBinder binder) {
            this.aF.m950f(binder);
        }

        public void onServiceDisconnected(ComponentName component) {
            this.aF.at = null;
            this.aF.m954m();
        }
    }

    /* renamed from: com.google.android.gms.internal.p.c */
    public abstract class C1232c<TListener> extends C0129b<TListener> {
        protected final C1230k f122O;
        final /* synthetic */ C1234p aF;

        public C1232c(C1234p c1234p, TListener tListener, C1230k c1230k) {
            this.aF = c1234p;
            super(c1234p, tListener);
            this.f122O = c1230k;
        }

        protected abstract void m930a(TListener tListener);

        public /* bridge */ /* synthetic */ void m931p() {
            super.m446p();
        }

        public /* bridge */ /* synthetic */ void m932q() {
            super.m447q();
        }
    }

    /* renamed from: com.google.android.gms.internal.p.f */
    public final class C1233f extends C0129b<Boolean> {
        final /* synthetic */ C1234p aF;
        public final Bundle aG;
        public final IBinder aH;
        public final int statusCode;

        public C1233f(C1234p c1234p, int i, IBinder iBinder, Bundle bundle) {
            this.aF = c1234p;
            super(c1234p, Boolean.valueOf(true));
            this.statusCode = i;
            this.aH = iBinder;
            this.aG = bundle;
        }

        protected void m933a(Boolean bool) {
            PendingIntent pendingIntent = null;
            if (bool != null) {
                switch (this.statusCode) {
                    case ValidationActivity.VRESULT_NONE /*0*/:
                        try {
                            if (this.aF.m949c().equals(this.aH.getInterfaceDescriptor())) {
                                this.aF.at = this.aF.m948c(this.aH);
                                if (this.aF.at != null) {
                                    this.aF.m952k();
                                    return;
                                }
                            }
                        } catch (RemoteException e) {
                        }
                        C0133q.m460e(this.aF.mContext).m462b(this.aF.m947b(), this.aF.aA);
                        this.aF.aA = null;
                        this.aF.at = null;
                        this.aF.m943a(new ConnectionResult(8, null));
                    case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                        throw new IllegalStateException("A fatal developer error has occurred. Check the logs for further information.");
                    default:
                        if (this.aG != null) {
                            pendingIntent = (PendingIntent) this.aG.getParcelable("pendingIntent");
                        }
                        this.aF.m943a(new ConnectionResult(this.statusCode, pendingIntent));
                }
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.p.d */
    public final class C1606d extends C1236a {
        final /* synthetic */ C1234p aF;

        protected C1606d(C1234p c1234p) {
            this.aF = c1234p;
        }

        public void m1264a(int i, IBinder iBinder, Bundle bundle) {
            this.aF.mHandler.sendMessage(this.aF.mHandler.obtainMessage(1, new C1233f(this.aF, i, iBinder, bundle)));
        }
    }

    static {
        aE = new String[]{"service_esmobile", "service_googleme"};
    }

    protected C1234p(Context context, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String... strArr) {
        this.av = new ArrayList();
        this.aw = false;
        this.ay = false;
        this.az = new ArrayList();
        this.aB = false;
        this.aC = false;
        this.aD = new Object();
        this.mContext = (Context) C0142x.m495d(context);
        this.au = new ArrayList();
        this.au.add(C0142x.m495d(connectionCallbacks));
        this.ax = new ArrayList();
        this.ax.add(C0142x.m495d(onConnectionFailedListener));
        this.mHandler = new C0128a(this, context.getMainLooper());
        m946a(strArr);
        this.f123f = strArr;
    }

    protected void m943a(ConnectionResult connectionResult) {
        this.mHandler.removeMessages(4);
        synchronized (this.ax) {
            this.ay = true;
            ArrayList arrayList = this.ax;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                if (this.aB) {
                    if (this.ax.contains(arrayList.get(i))) {
                        ((OnConnectionFailedListener) arrayList.get(i)).onConnectionFailed(connectionResult);
                    }
                    i++;
                } else {
                    return;
                }
            }
            this.ay = false;
        }
    }

    public final void m944a(C0129b<?> c0129b) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2, c0129b));
    }

    protected abstract void m945a(C0137u c0137u, C1606d c1606d) throws RemoteException;

    protected void m946a(String... strArr) {
    }

    protected abstract String m947b();

    protected abstract T m948c(IBinder iBinder);

    protected abstract String m949c();

    public void connect() {
        this.aB = true;
        synchronized (this.aD) {
            this.aC = true;
        }
        int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.mContext);
        if (isGooglePlayServicesAvailable != 0) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(3, Integer.valueOf(isGooglePlayServicesAvailable)));
            return;
        }
        if (this.aA != null) {
            Log.e("GmsClient", "Calling connect() while still connected, missing disconnect().");
            this.at = null;
            C0133q.m460e(this.mContext).m462b(m947b(), this.aA);
        }
        this.aA = new C0130e(this);
        if (!C0133q.m460e(this.mContext).m461a(m947b(), this.aA)) {
            Log.e("GmsClient", "unable to connect to service: " + m947b());
            this.mHandler.sendMessage(this.mHandler.obtainMessage(3, Integer.valueOf(9)));
        }
    }

    public void disconnect() {
        this.aB = false;
        synchronized (this.aD) {
            this.aC = false;
        }
        synchronized (this.az) {
            int size = this.az.size();
            for (int i = 0; i < size; i++) {
                ((C0129b) this.az.get(i)).m447q();
            }
            this.az.clear();
        }
        this.at = null;
        if (this.aA != null) {
            C0133q.m460e(this.mContext).m462b(m947b(), this.aA);
            this.aA = null;
        }
    }

    protected final void m950f(IBinder iBinder) {
        try {
            m945a(C1238a.m970h(iBinder), new C1606d(this));
        } catch (RemoteException e) {
            Log.w("GmsClient", "service died");
        }
    }

    public final Context getContext() {
        return this.mContext;
    }

    public boolean isConnected() {
        return this.at != null;
    }

    public boolean isConnecting() {
        boolean z;
        synchronized (this.aD) {
            z = this.aC;
        }
        return z;
    }

    public boolean isConnectionCallbacksRegistered(ConnectionCallbacks listener) {
        boolean contains;
        C0142x.m495d(listener);
        synchronized (this.au) {
            contains = this.au.contains(listener);
        }
        return contains;
    }

    public boolean isConnectionFailedListenerRegistered(OnConnectionFailedListener listener) {
        boolean contains;
        C0142x.m495d(listener);
        synchronized (this.ax) {
            contains = this.ax.contains(listener);
        }
        return contains;
    }

    public final String[] m951j() {
        return this.f123f;
    }

    protected void m952k() {
        boolean z = true;
        synchronized (this.au) {
            C0142x.m489a(!this.aw);
            this.mHandler.removeMessages(4);
            this.aw = true;
            if (this.av.size() != 0) {
                z = false;
            }
            C0142x.m489a(z);
            Bundle l = m953l();
            ArrayList arrayList = this.au;
            int size = arrayList.size();
            for (int i = 0; i < size && this.aB && isConnected(); i++) {
                this.av.size();
                if (!this.av.contains(arrayList.get(i))) {
                    ((ConnectionCallbacks) arrayList.get(i)).onConnected(l);
                }
            }
            this.av.clear();
            this.aw = false;
        }
    }

    protected Bundle m953l() {
        return null;
    }

    protected final void m954m() {
        this.mHandler.removeMessages(4);
        synchronized (this.au) {
            this.aw = true;
            ArrayList arrayList = this.au;
            int size = arrayList.size();
            for (int i = 0; i < size && this.aB; i++) {
                if (this.au.contains(arrayList.get(i))) {
                    ((ConnectionCallbacks) arrayList.get(i)).onDisconnected();
                }
            }
            this.aw = false;
        }
    }

    protected final void m955n() {
        if (!isConnected()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    protected final T m956o() {
        m955n();
        return this.at;
    }

    public void registerConnectionCallbacks(ConnectionCallbacks listener) {
        C0142x.m495d(listener);
        synchronized (this.au) {
            if (this.au.contains(listener)) {
                Log.w("GmsClient", "registerConnectionCallbacks(): listener " + listener + " is already registered");
            } else {
                if (this.aw) {
                    this.au = new ArrayList(this.au);
                }
                this.au.add(listener);
            }
        }
        if (isConnected()) {
            this.mHandler.sendMessage(this.mHandler.obtainMessage(4, listener));
        }
    }

    public void registerConnectionFailedListener(OnConnectionFailedListener listener) {
        C0142x.m495d(listener);
        synchronized (this.ax) {
            if (this.ax.contains(listener)) {
                Log.w("GmsClient", "registerConnectionFailedListener(): listener " + listener + " is already registered");
            } else {
                if (this.ay) {
                    this.ax = new ArrayList(this.ax);
                }
                this.ax.add(listener);
            }
        }
    }

    public void unregisterConnectionCallbacks(ConnectionCallbacks listener) {
        C0142x.m495d(listener);
        synchronized (this.au) {
            if (this.au != null) {
                if (this.aw) {
                    this.au = new ArrayList(this.au);
                }
                if (!this.au.remove(listener)) {
                    Log.w("GmsClient", "unregisterConnectionCallbacks(): listener " + listener + " not found");
                } else if (this.aw && !this.av.contains(listener)) {
                    this.av.add(listener);
                }
            }
        }
    }

    public void unregisterConnectionFailedListener(OnConnectionFailedListener listener) {
        C0142x.m495d(listener);
        synchronized (this.ax) {
            if (this.ax != null) {
                if (this.ay) {
                    this.ax = new ArrayList(this.ax);
                }
                if (!this.ax.remove(listener)) {
                    Log.w("GmsClient", "unregisterConnectionFailedListener(): listener " + listener + " not found");
                }
            }
        }
    }
}
