package com.google.android.gms.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface bc extends IInterface {

    /* renamed from: com.google.android.gms.internal.bc.a */
    public static abstract class C1151a extends Binder implements bc {

        /* renamed from: com.google.android.gms.internal.bc.a.a */
        static class C1150a implements bc {
            private IBinder f71a;

            C1150a(IBinder iBinder) {
                this.f71a = iBinder;
            }

            public IBinder asBinder() {
                return this.f71a;
            }
        }

        public C1151a() {
            attachInterface(this, "com.google.android.gms.dynamic.IObjectWrapper");
        }

        public static bc m631j(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamic.IObjectWrapper");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof bc)) ? new C1150a(iBinder) : (bc) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            switch (code) {
                case 1598968902:
                    reply.writeString("com.google.android.gms.dynamic.IObjectWrapper");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }
}
