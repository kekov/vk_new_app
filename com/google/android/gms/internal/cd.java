package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.internal.ca.C1163a;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashMap;

public class cd {
    private final ch<cc> eE;
    private ContentProviderClient eF;
    private HashMap<LocationListener, C1586b> eG;
    private final ContentResolver mContentResolver;

    /* renamed from: com.google.android.gms.internal.cd.a */
    static class C0113a extends Handler {
        private final LocationListener eH;

        public C0113a(LocationListener locationListener) {
            this.eH = locationListener;
        }

        public C0113a(LocationListener locationListener, Looper looper) {
            super(looper);
            this.eH = locationListener;
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    this.eH.onLocationChanged(new Location((Location) msg.obj));
                default:
                    Log.e("LocationClientHelper", "unknown message in LocationHandler.handleMessage");
            }
        }
    }

    /* renamed from: com.google.android.gms.internal.cd.b */
    static class C1586b extends C1163a {
        private final Handler eI;

        C1586b(LocationListener locationListener, Looper looper) {
            this.eI = looper == null ? new C0113a(locationListener) : new C0113a(locationListener, looper);
        }

        public void onLocationChanged(Location location) {
            Message obtain = Message.obtain();
            obtain.what = 1;
            obtain.obj = location;
            this.eI.sendMessage(obtain);
        }
    }

    public cd(Context context, ch<cc> chVar) {
        this.eF = null;
        this.eG = new HashMap();
        this.eE = chVar;
        this.mContentResolver = context.getContentResolver();
    }

    public Location getLastLocation() {
        this.eE.m293n();
        try {
            return ((cc) this.eE.m294o()).ay();
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeAllListeners() {
        try {
            synchronized (this.eG) {
                for (ca caVar : this.eG.values()) {
                    if (caVar != null) {
                        ((cc) this.eE.m294o()).m284a(caVar);
                    }
                }
                this.eG.clear();
            }
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeLocationUpdates(PendingIntent callbackIntent) {
        this.eE.m293n();
        try {
            ((cc) this.eE.m294o()).m282a(callbackIntent);
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void removeLocationUpdates(LocationListener listener) {
        this.eE.m293n();
        C0142x.m492b((Object) listener, (Object) "Invalid null listener");
        synchronized (this.eG) {
            ca caVar = (C1586b) this.eG.remove(listener);
            if (this.eF != null && this.eG.isEmpty()) {
                this.eF.release();
                this.eF = null;
            }
            if (caVar != null) {
                try {
                    ((cc) this.eE.m294o()).m284a(caVar);
                } catch (Throwable e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }

    public void requestLocationUpdates(LocationRequest request, PendingIntent callbackIntent) {
        this.eE.m293n();
        try {
            ((cc) this.eE.m294o()).m286a(request, callbackIntent);
        } catch (Throwable e) {
            throw new IllegalStateException(e);
        }
    }

    public void requestLocationUpdates(LocationRequest request, LocationListener listener, Looper looper) {
        this.eE.m293n();
        if (looper == null) {
            C0142x.m492b(Looper.myLooper(), (Object) "Can't create handler inside thread that has not called Looper.prepare()");
        }
        synchronized (this.eG) {
            ca c1586b;
            C1586b c1586b2 = (C1586b) this.eG.get(listener);
            if (c1586b2 == null) {
                c1586b = new C1586b(listener, looper);
            } else {
                Object obj = c1586b2;
            }
            this.eG.put(listener, c1586b);
            try {
                ((cc) this.eE.m294o()).m287a(request, c1586b);
            } catch (Throwable e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
