package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.MarkerOptions;

public class dd {
    public static void m323a(MarkerOptions markerOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, markerOptions.m1003u());
        ad.m95a(parcel, 2, markerOptions.getPosition(), i, false);
        ad.m96a(parcel, 3, markerOptions.getTitle(), false);
        ad.m96a(parcel, 4, markerOptions.getSnippet(), false);
        ad.m93a(parcel, 5, markerOptions.aY(), false);
        ad.m90a(parcel, 6, markerOptions.getAnchorU());
        ad.m90a(parcel, 7, markerOptions.getAnchorV());
        ad.m99a(parcel, 8, markerOptions.isDraggable());
        ad.m99a(parcel, 9, markerOptions.isVisible());
        ad.m87C(parcel, d);
    }
}
