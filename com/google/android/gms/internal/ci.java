package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.GoogleMapOptions;

public class ci {
    public static void m295a(GoogleMapOptions googleMapOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, googleMapOptions.m978u());
        ad.m88a(parcel, 2, googleMapOptions.aG());
        ad.m88a(parcel, 3, googleMapOptions.aH());
        ad.m106c(parcel, 4, googleMapOptions.getMapType());
        ad.m95a(parcel, 5, googleMapOptions.getCamera(), i, false);
        ad.m88a(parcel, 6, googleMapOptions.aI());
        ad.m88a(parcel, 7, googleMapOptions.aJ());
        ad.m88a(parcel, 8, googleMapOptions.aK());
        ad.m88a(parcel, 9, googleMapOptions.aL());
        ad.m88a(parcel, 10, googleMapOptions.aM());
        ad.m88a(parcel, 11, googleMapOptions.aN());
        ad.m87C(parcel, d);
    }
}
