package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.net.Uri;

/* renamed from: com.google.android.gms.internal.j */
public abstract class C0123j {
    protected final C1230k f34O;
    protected final int f35R;
    private final int f36S;

    public C0123j(C1230k c1230k, int i) {
        this.f34O = (C1230k) C0142x.m495d(c1230k);
        boolean z = i >= 0 && i < c1230k.getCount();
        C0142x.m489a(z);
        this.f35R = i;
        this.f36S = c1230k.m919d(this.f35R);
    }

    protected void m434a(String str, CharArrayBuffer charArrayBuffer) {
        this.f34O.m916a(str, this.f35R, this.f36S, charArrayBuffer);
    }

    protected Uri m435c(String str) {
        return this.f34O.m922f(str, this.f35R, this.f36S);
    }

    protected boolean m436d(String str) {
        return this.f34O.m924g(str, this.f35R, this.f36S);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C0123j)) {
            return false;
        }
        C0123j c0123j = (C0123j) obj;
        return C0141w.m487a(Integer.valueOf(c0123j.f35R), Integer.valueOf(this.f35R)) && C0141w.m487a(Integer.valueOf(c0123j.f36S), Integer.valueOf(this.f36S)) && c0123j.f34O == this.f34O;
    }

    protected boolean getBoolean(String column) {
        return this.f34O.m920d(column, this.f35R, this.f36S);
    }

    protected byte[] getByteArray(String column) {
        return this.f34O.m921e(column, this.f35R, this.f36S);
    }

    protected int getInteger(String column) {
        return this.f34O.m917b(column, this.f35R, this.f36S);
    }

    protected long getLong(String column) {
        return this.f34O.m915a(column, this.f35R, this.f36S);
    }

    protected String getString(String column) {
        return this.f34O.m918c(column, this.f35R, this.f36S);
    }

    public int hashCode() {
        return C0141w.hashCode(Integer.valueOf(this.f35R), Integer.valueOf(this.f36S), this.f34O);
    }
}
