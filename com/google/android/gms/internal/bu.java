package com.google.android.gms.internal;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.leaderboard.LeaderboardScore;

public final class bu extends C0123j implements LeaderboardScore {
    private final bg dA;

    public bu(C1230k c1230k, int i) {
        super(c1230k, i);
        this.dA = new bg(c1230k, i);
    }

    public LeaderboardScore as() {
        return new bt(this);
    }

    public boolean equals(Object obj) {
        return bt.m1126a(this, obj);
    }

    public /* synthetic */ Object freeze() {
        return as();
    }

    public String getDisplayRank() {
        return getString("display_rank");
    }

    public void getDisplayRank(CharArrayBuffer dataOut) {
        m434a("display_rank", dataOut);
    }

    public String getDisplayScore() {
        return getString("display_score");
    }

    public void getDisplayScore(CharArrayBuffer dataOut) {
        m434a("display_score", dataOut);
    }

    public long getRank() {
        return getLong("rank");
    }

    public long getRawScore() {
        return getLong("raw_score");
    }

    public Player getScoreHolder() {
        return m436d("external_player_id") ? null : this.dA;
    }

    public String getScoreHolderDisplayName() {
        return m436d("external_player_id") ? getString("default_display_name") : this.dA.getDisplayName();
    }

    public void getScoreHolderDisplayName(CharArrayBuffer dataOut) {
        if (m436d("external_player_id")) {
            m434a("default_display_name", dataOut);
        } else {
            this.dA.getDisplayName(dataOut);
        }
    }

    public Uri getScoreHolderHiResImageUri() {
        return m436d("external_player_id") ? null : this.dA.getHiResImageUri();
    }

    public Uri getScoreHolderIconImageUri() {
        return m436d("external_player_id") ? m435c("default_display_image_uri") : this.dA.getIconImageUri();
    }

    public long getTimestampMillis() {
        return getLong("achieved_timestamp");
    }

    public int hashCode() {
        return bt.m1125a(this);
    }

    public String toString() {
        return bt.m1127b(this);
    }
}
