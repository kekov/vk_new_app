package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.eq.C1602e;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashSet;
import java.util.Set;

public class eo implements Creator<C1602e> {
    static void m409a(C1602e c1602e, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        Set by = c1602e.by();
        if (by.contains(Integer.valueOf(1))) {
            ad.m106c(parcel, 1, c1602e.m1238u());
        }
        if (by.contains(Integer.valueOf(2))) {
            ad.m96a(parcel, 2, c1602e.getFamilyName(), true);
        }
        if (by.contains(Integer.valueOf(3))) {
            ad.m96a(parcel, 3, c1602e.getFormatted(), true);
        }
        if (by.contains(Integer.valueOf(4))) {
            ad.m96a(parcel, 4, c1602e.getGivenName(), true);
        }
        if (by.contains(Integer.valueOf(5))) {
            ad.m96a(parcel, 5, c1602e.getHonorificPrefix(), true);
        }
        if (by.contains(Integer.valueOf(6))) {
            ad.m96a(parcel, 6, c1602e.getHonorificSuffix(), true);
        }
        if (by.contains(Integer.valueOf(7))) {
            ad.m96a(parcel, 7, c1602e.getMiddleName(), true);
        }
        ad.m87C(parcel, d);
    }

    public C1602e m410D(Parcel parcel) {
        String str = null;
        int c = ac.m58c(parcel);
        Set hashSet = new HashSet();
        int i = 0;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    str6 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(2));
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    str5 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(3));
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    str4 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(4));
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    str3 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(5));
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    str2 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(6));
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    str = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(7));
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new C1602e(hashSet, i, str6, str5, str4, str3, str2, str);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public C1602e[] m411X(int i) {
        return new C1602e[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m410D(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m411X(x0);
    }
}
