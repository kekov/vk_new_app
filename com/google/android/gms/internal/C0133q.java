package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.Message;
import com.google.android.gms.internal.C1234p.C0130e;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: com.google.android.gms.internal.q */
public final class C0133q implements Callback {
    private static C0133q aI;
    private final Context aJ;
    private final HashMap<String, C0132a> aK;
    private final Handler mHandler;

    /* renamed from: com.google.android.gms.internal.q.a */
    final class C0132a {
        private final String aL;
        private final C0131a aM;
        private final HashSet<C0130e> aN;
        private boolean aO;
        private IBinder aP;
        private ComponentName aQ;
        final /* synthetic */ C0133q aR;
        private int mState;

        /* renamed from: com.google.android.gms.internal.q.a.a */
        public class C0131a implements ServiceConnection {
            final /* synthetic */ C0132a aS;

            public C0131a(C0132a c0132a) {
                this.aS = c0132a;
            }

            public void onServiceConnected(ComponentName component, IBinder binder) {
                synchronized (this.aS.aR.aK) {
                    this.aS.aP = binder;
                    this.aS.aQ = component;
                    Iterator it = this.aS.aN.iterator();
                    while (it.hasNext()) {
                        ((C0130e) it.next()).onServiceConnected(component, binder);
                    }
                    this.aS.mState = 1;
                }
            }

            public void onServiceDisconnected(ComponentName component) {
                synchronized (this.aS.aR.aK) {
                    this.aS.aP = null;
                    this.aS.aQ = component;
                    Iterator it = this.aS.aN.iterator();
                    while (it.hasNext()) {
                        ((C0130e) it.next()).onServiceDisconnected(component);
                    }
                    this.aS.mState = 2;
                }
            }
        }

        public C0132a(C0133q c0133q, String str) {
            this.aR = c0133q;
            this.aL = str;
            this.aM = new C0131a(this);
            this.aN = new HashSet();
            this.mState = 0;
        }

        public void m452a(C0130e c0130e) {
            this.aN.add(c0130e);
        }

        public void m453b(C0130e c0130e) {
            this.aN.remove(c0130e);
        }

        public void m454b(boolean z) {
            this.aO = z;
        }

        public boolean m455c(C0130e c0130e) {
            return this.aN.contains(c0130e);
        }

        public IBinder getBinder() {
            return this.aP;
        }

        public ComponentName getComponentName() {
            return this.aQ;
        }

        public int getState() {
            return this.mState;
        }

        public boolean isBound() {
            return this.aO;
        }

        public C0131a m456r() {
            return this.aM;
        }

        public String m457s() {
            return this.aL;
        }

        public boolean m458t() {
            return this.aN.isEmpty();
        }
    }

    private C0133q(Context context) {
        this.mHandler = new Handler(context.getMainLooper(), this);
        this.aK = new HashMap();
        this.aJ = context.getApplicationContext();
    }

    public static C0133q m460e(Context context) {
        if (aI == null) {
            aI = new C0133q(context.getApplicationContext());
        }
        return aI;
    }

    public boolean m461a(String str, C0130e c0130e) {
        boolean isBound;
        synchronized (this.aK) {
            C0132a c0132a = (C0132a) this.aK.get(str);
            if (c0132a != null) {
                this.mHandler.removeMessages(0, c0132a);
                if (!c0132a.m455c(c0130e)) {
                    c0132a.m452a((C0130e) c0130e);
                    switch (c0132a.getState()) {
                        case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                            c0130e.onServiceConnected(c0132a.getComponentName(), c0132a.getBinder());
                            break;
                        case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                            c0132a.m454b(this.aJ.bindService(new Intent(str), c0132a.m456r(), 129));
                            break;
                        default:
                            break;
                    }
                }
                throw new IllegalStateException("Trying to bind a GmsServiceConnection that was already connected before.  startServiceAction=" + str);
            }
            c0132a = new C0132a(this, str);
            c0132a.m452a((C0130e) c0130e);
            c0132a.m454b(this.aJ.bindService(new Intent(str), c0132a.m456r(), 129));
            this.aK.put(str, c0132a);
            isBound = c0132a.isBound();
        }
        return isBound;
    }

    public void m462b(String str, C0130e c0130e) {
        synchronized (this.aK) {
            C0132a c0132a = (C0132a) this.aK.get(str);
            if (c0132a == null) {
                throw new IllegalStateException("Nonexistent connection status for service action: " + str);
            } else if (c0132a.m455c(c0130e)) {
                c0132a.m453b((C0130e) c0130e);
                if (c0132a.m458t()) {
                    this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(0, c0132a), 5000);
                }
            } else {
                throw new IllegalStateException("Trying to unbind a GmsServiceConnection  that was not bound before.  startServiceAction=" + str);
            }
        }
    }

    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                C0132a c0132a = (C0132a) msg.obj;
                synchronized (this.aK) {
                    if (c0132a.m458t()) {
                        this.aJ.unbindService(c0132a.m456r());
                        this.aK.remove(c0132a.m457s());
                    }
                    break;
                }
                return true;
            default:
                return false;
        }
    }
}
