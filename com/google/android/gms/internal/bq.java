package com.google.android.gms.internal;

import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public final class bq {
    public static String m280B(int i) {
        switch (i) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return "DAILY";
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                return "WEEKLY";
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return "ALL_TIME";
            default:
                throw new IllegalArgumentException("Unknown time span " + i);
        }
    }
}
