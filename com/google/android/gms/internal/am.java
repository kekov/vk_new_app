package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ak.C1141a;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class am implements Creator<C1141a> {
    static void m119a(C1141a c1141a, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, c1141a.versionCode);
        ad.m96a(parcel, 2, c1141a.bs, false);
        ad.m106c(parcel, 3, c1141a.bt);
        ad.m87C(parcel, d);
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m120h(x0);
    }

    public C1141a m120h(Parcel parcel) {
        int i = 0;
        int c = ac.m58c(parcel);
        String str = null;
        int i2 = 0;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i2 = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    str = ac.m70l(parcel, b);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    i = ac.m63f(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new C1141a(i2, str, i);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public C1141a[] m121n(int i) {
        return new C1141a[i];
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m121n(x0);
    }
}
