package com.google.android.gms.internal;

import android.text.TextUtils;
import com.vkontakte.android.C0436R;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.mediapicker.providers.LangProvider.StringKeys;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.hockeyapp.android.Strings;

public final class ay {
    private static final Pattern bO;
    private static final Pattern bP;

    static {
        bO = Pattern.compile("\\\\.");
        bP = Pattern.compile("[\\\\\"/\b\f\n\r\t]");
    }

    public static String m163o(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        Matcher matcher = bP.matcher(str);
        StringBuffer stringBuffer = null;
        while (matcher.find()) {
            if (stringBuffer == null) {
                stringBuffer = new StringBuffer();
            }
            switch (matcher.group().charAt(0)) {
                case UserListView.TYPE_BLACKLIST /*8*/:
                    matcher.appendReplacement(stringBuffer, "\\\\b");
                    break;
                case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                    matcher.appendReplacement(stringBuffer, "\\\\t");
                    break;
                case UserListView.TYPE_USER_SUBSCRIPTIONS /*10*/:
                    matcher.appendReplacement(stringBuffer, "\\\\n");
                    break;
                case Strings.UPDATE_DIALOG_POSITIVE_BUTTON_ID /*12*/:
                    matcher.appendReplacement(stringBuffer, "\\\\f");
                    break;
                case Strings.EXPIRY_INFO_TITLE_ID /*13*/:
                    matcher.appendReplacement(stringBuffer, "\\\\r");
                    break;
                case StringKeys.ERROR /*34*/:
                    matcher.appendReplacement(stringBuffer, "\\\\\\\"");
                    break;
                case C0436R.styleable.SherlockTheme_textAppearanceListItemSmall /*47*/:
                    matcher.appendReplacement(stringBuffer, "\\\\/");
                    break;
                case '\\':
                    matcher.appendReplacement(stringBuffer, "\\\\\\\\");
                    break;
                default:
                    break;
            }
        }
        if (stringBuffer == null) {
            return str;
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }
}
