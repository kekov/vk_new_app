package com.google.android.gms.internal;

import android.os.Parcel;
import com.google.android.gms.maps.model.CircleOptions;

public class cz {
    public static void m319a(CircleOptions circleOptions, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, circleOptions.m992u());
        ad.m95a(parcel, 2, circleOptions.getCenter(), i, false);
        ad.m89a(parcel, 3, circleOptions.getRadius());
        ad.m90a(parcel, 4, circleOptions.getStrokeWidth());
        ad.m106c(parcel, 5, circleOptions.getStrokeColor());
        ad.m106c(parcel, 6, circleOptions.getFillColor());
        ad.m90a(parcel, 7, circleOptions.getZIndex());
        ad.m99a(parcel, 8, circleOptions.isVisible());
        ad.m87C(parcel, d);
    }
}
