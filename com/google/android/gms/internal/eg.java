package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac.C0107a;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.util.HashSet;
import java.util.Set;

public class eg implements Creator<ef> {
    static void m388a(ef efVar, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        Set by = efVar.by();
        if (by.contains(Integer.valueOf(1))) {
            ad.m106c(parcel, 1, efVar.m1196u());
        }
        if (by.contains(Integer.valueOf(2))) {
            ad.m96a(parcel, 2, efVar.getId(), true);
        }
        if (by.contains(Integer.valueOf(4))) {
            ad.m95a(parcel, 4, efVar.bP(), i, true);
        }
        if (by.contains(Integer.valueOf(5))) {
            ad.m96a(parcel, 5, efVar.getStartDate(), true);
        }
        if (by.contains(Integer.valueOf(6))) {
            ad.m95a(parcel, 6, efVar.bQ(), i, true);
        }
        if (by.contains(Integer.valueOf(7))) {
            ad.m96a(parcel, 7, efVar.getType(), true);
        }
        ad.m87C(parcel, d);
    }

    public ef[] m389Q(int i) {
        return new ef[i];
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return m390w(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return m389Q(x0);
    }

    public ef m390w(Parcel parcel) {
        String str = null;
        int c = ac.m58c(parcel);
        Set hashSet = new HashSet();
        int i = 0;
        ed edVar = null;
        String str2 = null;
        ed edVar2 = null;
        String str3 = null;
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            ed edVar3;
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    i = ac.m63f(parcel, b);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    str3 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(2));
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    edVar3 = (ed) ac.m52a(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(4));
                    edVar2 = edVar3;
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    str2 = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(5));
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    edVar3 = (ed) ac.m52a(parcel, b, ed.CREATOR);
                    hashSet.add(Integer.valueOf(6));
                    edVar = edVar3;
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    str = ac.m70l(parcel, b);
                    hashSet.add(Integer.valueOf(7));
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return new ef(hashSet, i, str3, edVar2, str2, edVar, str);
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }
}
