package com.google.android.gms.internal;

import com.google.android.gms.appstate.AppState;

/* renamed from: com.google.android.gms.internal.c */
public final class C1585c extends C0123j implements AppState {
    public C1585c(C1230k c1230k, int i) {
        super(c1230k, i);
    }

    public AppState m1130a() {
        return new C1577b(this);
    }

    public boolean equals(Object obj) {
        return C1577b.m1045a(this, obj);
    }

    public /* synthetic */ Object freeze() {
        return m1130a();
    }

    public byte[] getConflictData() {
        return getByteArray("conflict_data");
    }

    public String getConflictVersion() {
        return getString("conflict_version");
    }

    public int getKey() {
        return getInteger("key");
    }

    public byte[] getLocalData() {
        return getByteArray("local_data");
    }

    public String getLocalVersion() {
        return getString("local_version");
    }

    public boolean hasConflict() {
        return !m436d("conflict_version");
    }

    public int hashCode() {
        return C1577b.m1044a(this);
    }

    public String toString() {
        return C1577b.m1046b(this);
    }
}
