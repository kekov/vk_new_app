package com.google.android.gms.internal;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import com.google.android.gms.R;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.ui.GalleryPickerFooterView;

public final class aa extends Button {
    public aa(Context context) {
        this(context, null);
    }

    public aa(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 16842824);
    }

    private int m45a(int i, int i2, int i3) {
        switch (i) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return i2;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                return i3;
            default:
                throw new IllegalStateException("Unknown color scheme: " + i);
        }
    }

    private void m46b(Resources resources, int i, int i2) {
        int a;
        switch (i) {
            case ValidationActivity.VRESULT_NONE /*0*/:
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                a = m45a(i2, R.drawable.common_signin_btn_text_dark, R.drawable.common_signin_btn_text_light);
                break;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                a = m45a(i2, R.drawable.common_signin_btn_icon_dark, R.drawable.common_signin_btn_icon_light);
                break;
            default:
                throw new IllegalStateException("Unknown button size: " + i);
        }
        if (a == -1) {
            throw new IllegalStateException("Could not find background resource!");
        }
        setBackgroundDrawable(resources.getDrawable(a));
    }

    private void m47c(Resources resources) {
        setTypeface(Typeface.DEFAULT_BOLD);
        setTextSize(14.0f);
        float f = resources.getDisplayMetrics().density;
        setMinHeight((int) ((f * GalleryPickerFooterView.SIZE) + 0.5f));
        setMinWidth((int) ((f * GalleryPickerFooterView.SIZE) + 0.5f));
    }

    private void m48c(Resources resources, int i, int i2) {
        setTextColor(resources.getColorStateList(m45a(i2, R.color.common_signin_btn_text_dark, R.color.common_signin_btn_text_light)));
        switch (i) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                setText(resources.getString(R.string.common_signin_button_text));
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                setText(resources.getString(R.string.common_signin_button_text_long));
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                setText(null);
            default:
                throw new IllegalStateException("Unknown button size: " + i);
        }
    }

    public void m49a(Resources resources, int i, int i2) {
        boolean z = true;
        boolean z2 = i >= 0 && i < 3;
        C0142x.m490a(z2, "Unknown button size " + i);
        if (i2 < 0 || i2 >= 2) {
            z = false;
        }
        C0142x.m490a(z, "Unknown color scheme " + i2);
        m47c(resources);
        m46b(resources, i, i2);
        m48c(resources, i, i2);
    }
}
