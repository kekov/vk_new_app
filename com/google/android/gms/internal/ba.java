package com.google.android.gms.internal;

import android.os.Build.VERSION;

public final class ba {
    public static boolean aa() {
        return m165t(11);
    }

    public static boolean ab() {
        return m165t(12);
    }

    public static boolean ac() {
        return m165t(13);
    }

    public static boolean ad() {
        return m165t(14);
    }

    public static boolean ae() {
        return m165t(16);
    }

    public static boolean af() {
        return m165t(17);
    }

    private static boolean m165t(int i) {
        return VERSION.SDK_INT >= i;
    }
}
