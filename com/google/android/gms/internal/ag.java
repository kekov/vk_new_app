package com.google.android.gms.internal;

import android.os.Parcel;

public final class ag implements ae {
    public static final ah CREATOR;
    private final int f65T;
    private final int bi;
    private final int bj;
    private final String bk;
    private final String bl;
    private final String bm;
    private final String bn;

    static {
        CREATOR = new ah();
    }

    public ag(int i, int i2, int i3, String str, String str2, String str3, String str4) {
        this.f65T = i;
        this.bi = i2;
        this.bj = i3;
        this.bk = str;
        this.bl = str2;
        this.bm = str3;
        this.bn = str4;
    }

    public boolean m553A() {
        return this.bi == 2;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ag)) {
            return false;
        }
        ag agVar = (ag) obj;
        return this.f65T == agVar.f65T && this.bi == agVar.bi && this.bj == agVar.bj && C0141w.m487a(this.bk, agVar.bk) && C0141w.m487a(this.bl, agVar.bl);
    }

    public String getDisplayName() {
        return this.bm;
    }

    public int getType() {
        return this.bi;
    }

    public int hashCode() {
        return C0141w.hashCode(Integer.valueOf(this.f65T), Integer.valueOf(this.bi), Integer.valueOf(this.bj), this.bk, this.bl);
    }

    public String toString() {
        if (m553A()) {
            return String.format("Person [%s] %s", new Object[]{m557x(), getDisplayName()});
        } else if (m559z()) {
            return String.format("Circle [%s] %s", new Object[]{m556w(), getDisplayName()});
        } else {
            return String.format("Group [%s] %s", new Object[]{m556w(), getDisplayName()});
        }
    }

    public int m554u() {
        return this.f65T;
    }

    public int m555v() {
        return this.bj;
    }

    public String m556w() {
        return this.bk;
    }

    public void writeToParcel(Parcel out, int flags) {
        ah.m110a(this, out, flags);
    }

    public String m557x() {
        return this.bl;
    }

    public String m558y() {
        return this.bn;
    }

    public boolean m559z() {
        return this.bi == 1 && this.bj == -1;
    }
}
