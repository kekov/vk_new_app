package com.google.android.gms.plus.model.people;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.C1230k;
import com.google.android.gms.internal.es;

public final class PersonBuffer extends DataBuffer<Person> {
    public PersonBuffer(C1230k dataHolder) {
        super(dataHolder);
    }

    public Person get(int position) {
        return new es(this.O, position);
    }
}
