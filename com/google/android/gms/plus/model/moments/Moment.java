package com.google.android.gms.plus.model.moments;

import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.internal.ed;
import com.google.android.gms.internal.ef;
import java.util.HashSet;
import java.util.Set;

public interface Moment extends Freezable<Moment> {

    public static class Builder {
        private final Set<Integer> hS;
        private String iH;
        private String iN;
        private ed iQ;
        private ed iR;
        private String iw;

        public Builder() {
            this.hS = new HashSet();
        }

        public Moment build() {
            return new ef(this.hS, this.iw, this.iQ, this.iH, this.iR, this.iN);
        }

        public Builder setId(String id) {
            this.iw = id;
            this.hS.add(Integer.valueOf(2));
            return this;
        }

        public Builder setResult(ItemScope result) {
            this.iQ = (ed) result;
            this.hS.add(Integer.valueOf(4));
            return this;
        }

        public Builder setStartDate(String startDate) {
            this.iH = startDate;
            this.hS.add(Integer.valueOf(5));
            return this;
        }

        public Builder setTarget(ItemScope target) {
            this.iR = (ed) target;
            this.hS.add(Integer.valueOf(6));
            return this;
        }

        public Builder setType(String type) {
            this.iN = type;
            this.hS.add(Integer.valueOf(7));
            return this;
        }
    }

    String getId();

    ItemScope getResult();

    String getStartDate();

    ItemScope getTarget();

    String getType();

    boolean hasId();

    boolean hasResult();

    boolean hasStartDate();

    boolean hasTarget();

    boolean hasType();
}
