package com.google.android.gms.plus.model.moments;

import com.google.android.gms.common.data.Freezable;
import com.google.android.gms.internal.ed;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface ItemScope extends Freezable<ItemScope> {

    public static class Builder {
        private String ch;
        private double ew;
        private double ex;
        private String hE;
        private final Set<Integer> hS;
        private ed hT;
        private List<String> hU;
        private ed hV;
        private String hW;
        private String hX;
        private String hY;
        private List<ed> hZ;
        private ed iA;
        private List<ed> iB;
        private String iC;
        private String iD;
        private String iE;
        private String iF;
        private ed iG;
        private String iH;
        private String iI;
        private String iJ;
        private ed iK;
        private String iL;
        private String iM;
        private String iN;
        private String iO;
        private String iP;
        private int ia;
        private List<ed> ib;
        private ed ic;
        private List<ed> id;
        private String ie;
        private String f40if;
        private ed ig;
        private String ih;
        private String ii;
        private String ij;
        private List<ed> ik;
        private String il;
        private String im;
        private String in;
        private String io;
        private String ip;
        private String iq;
        private String ir;
        private String is;
        private ed it;
        private String iu;
        private String iv;
        private String iw;
        private String ix;
        private ed iy;
        private ed iz;
        private String mName;

        public Builder() {
            this.hS = new HashSet();
        }

        public ItemScope build() {
            return new ed(this.hS, this.hT, this.hU, this.hV, this.hW, this.hX, this.hY, this.hZ, this.ia, this.ib, this.ic, this.id, this.ie, this.f40if, this.ig, this.ih, this.ii, this.ij, this.ik, this.il, this.im, this.in, this.ch, this.io, this.ip, this.iq, this.ir, this.is, this.it, this.iu, this.iv, this.iw, this.ix, this.iy, this.ew, this.iz, this.ex, this.mName, this.iA, this.iB, this.iC, this.iD, this.iE, this.iF, this.iG, this.iH, this.iI, this.iJ, this.iK, this.iL, this.iM, this.iN, this.hE, this.iO, this.iP);
        }

        public Builder setAbout(ItemScope about) {
            this.hT = (ed) about;
            this.hS.add(Integer.valueOf(2));
            return this;
        }

        public Builder setAdditionalName(List<String> additionalName) {
            this.hU = additionalName;
            this.hS.add(Integer.valueOf(3));
            return this;
        }

        public Builder setAddress(ItemScope address) {
            this.hV = (ed) address;
            this.hS.add(Integer.valueOf(4));
            return this;
        }

        public Builder setAddressCountry(String addressCountry) {
            this.hW = addressCountry;
            this.hS.add(Integer.valueOf(5));
            return this;
        }

        public Builder setAddressLocality(String addressLocality) {
            this.hX = addressLocality;
            this.hS.add(Integer.valueOf(6));
            return this;
        }

        public Builder setAddressRegion(String addressRegion) {
            this.hY = addressRegion;
            this.hS.add(Integer.valueOf(7));
            return this;
        }

        public Builder setAssociated_media(List<ItemScope> associated_media) {
            this.hZ = associated_media;
            this.hS.add(Integer.valueOf(8));
            return this;
        }

        public Builder setAttendeeCount(int attendeeCount) {
            this.ia = attendeeCount;
            this.hS.add(Integer.valueOf(9));
            return this;
        }

        public Builder setAttendees(List<ItemScope> attendees) {
            this.ib = attendees;
            this.hS.add(Integer.valueOf(10));
            return this;
        }

        public Builder setAudio(ItemScope audio) {
            this.ic = (ed) audio;
            this.hS.add(Integer.valueOf(11));
            return this;
        }

        public Builder setAuthor(List<ItemScope> author) {
            this.id = author;
            this.hS.add(Integer.valueOf(12));
            return this;
        }

        public Builder setBestRating(String bestRating) {
            this.ie = bestRating;
            this.hS.add(Integer.valueOf(13));
            return this;
        }

        public Builder setBirthDate(String birthDate) {
            this.f40if = birthDate;
            this.hS.add(Integer.valueOf(14));
            return this;
        }

        public Builder setByArtist(ItemScope byArtist) {
            this.ig = (ed) byArtist;
            this.hS.add(Integer.valueOf(15));
            return this;
        }

        public Builder setCaption(String caption) {
            this.ih = caption;
            this.hS.add(Integer.valueOf(16));
            return this;
        }

        public Builder setContentSize(String contentSize) {
            this.ii = contentSize;
            this.hS.add(Integer.valueOf(17));
            return this;
        }

        public Builder setContentUrl(String contentUrl) {
            this.ij = contentUrl;
            this.hS.add(Integer.valueOf(18));
            return this;
        }

        public Builder setContributor(List<ItemScope> contributor) {
            this.ik = contributor;
            this.hS.add(Integer.valueOf(19));
            return this;
        }

        public Builder setDateCreated(String dateCreated) {
            this.il = dateCreated;
            this.hS.add(Integer.valueOf(20));
            return this;
        }

        public Builder setDateModified(String dateModified) {
            this.im = dateModified;
            this.hS.add(Integer.valueOf(21));
            return this;
        }

        public Builder setDatePublished(String datePublished) {
            this.in = datePublished;
            this.hS.add(Integer.valueOf(22));
            return this;
        }

        public Builder setDescription(String description) {
            this.ch = description;
            this.hS.add(Integer.valueOf(23));
            return this;
        }

        public Builder setDuration(String duration) {
            this.io = duration;
            this.hS.add(Integer.valueOf(24));
            return this;
        }

        public Builder setEmbedUrl(String embedUrl) {
            this.ip = embedUrl;
            this.hS.add(Integer.valueOf(25));
            return this;
        }

        public Builder setEndDate(String endDate) {
            this.iq = endDate;
            this.hS.add(Integer.valueOf(26));
            return this;
        }

        public Builder setFamilyName(String familyName) {
            this.ir = familyName;
            this.hS.add(Integer.valueOf(27));
            return this;
        }

        public Builder setGender(String gender) {
            this.is = gender;
            this.hS.add(Integer.valueOf(28));
            return this;
        }

        public Builder setGeo(ItemScope geo) {
            this.it = (ed) geo;
            this.hS.add(Integer.valueOf(29));
            return this;
        }

        public Builder setGivenName(String givenName) {
            this.iu = givenName;
            this.hS.add(Integer.valueOf(30));
            return this;
        }

        public Builder setHeight(String height) {
            this.iv = height;
            this.hS.add(Integer.valueOf(31));
            return this;
        }

        public Builder setId(String id) {
            this.iw = id;
            this.hS.add(Integer.valueOf(32));
            return this;
        }

        public Builder setImage(String image) {
            this.ix = image;
            this.hS.add(Integer.valueOf(33));
            return this;
        }

        public Builder setInAlbum(ItemScope inAlbum) {
            this.iy = (ed) inAlbum;
            this.hS.add(Integer.valueOf(34));
            return this;
        }

        public Builder setLatitude(double latitude) {
            this.ew = latitude;
            this.hS.add(Integer.valueOf(36));
            return this;
        }

        public Builder setLocation(ItemScope location) {
            this.iz = (ed) location;
            this.hS.add(Integer.valueOf(37));
            return this;
        }

        public Builder setLongitude(double longitude) {
            this.ex = longitude;
            this.hS.add(Integer.valueOf(38));
            return this;
        }

        public Builder setName(String name) {
            this.mName = name;
            this.hS.add(Integer.valueOf(39));
            return this;
        }

        public Builder setPartOfTVSeries(ItemScope partOfTVSeries) {
            this.iA = (ed) partOfTVSeries;
            this.hS.add(Integer.valueOf(40));
            return this;
        }

        public Builder setPerformers(List<ItemScope> performers) {
            this.iB = performers;
            this.hS.add(Integer.valueOf(41));
            return this;
        }

        public Builder setPlayerType(String playerType) {
            this.iC = playerType;
            this.hS.add(Integer.valueOf(42));
            return this;
        }

        public Builder setPostOfficeBoxNumber(String postOfficeBoxNumber) {
            this.iD = postOfficeBoxNumber;
            this.hS.add(Integer.valueOf(43));
            return this;
        }

        public Builder setPostalCode(String postalCode) {
            this.iE = postalCode;
            this.hS.add(Integer.valueOf(44));
            return this;
        }

        public Builder setRatingValue(String ratingValue) {
            this.iF = ratingValue;
            this.hS.add(Integer.valueOf(45));
            return this;
        }

        public Builder setReviewRating(ItemScope reviewRating) {
            this.iG = (ed) reviewRating;
            this.hS.add(Integer.valueOf(46));
            return this;
        }

        public Builder setStartDate(String startDate) {
            this.iH = startDate;
            this.hS.add(Integer.valueOf(47));
            return this;
        }

        public Builder setStreetAddress(String streetAddress) {
            this.iI = streetAddress;
            this.hS.add(Integer.valueOf(48));
            return this;
        }

        public Builder setText(String text) {
            this.iJ = text;
            this.hS.add(Integer.valueOf(49));
            return this;
        }

        public Builder setThumbnail(ItemScope thumbnail) {
            this.iK = (ed) thumbnail;
            this.hS.add(Integer.valueOf(50));
            return this;
        }

        public Builder setThumbnailUrl(String thumbnailUrl) {
            this.iL = thumbnailUrl;
            this.hS.add(Integer.valueOf(51));
            return this;
        }

        public Builder setTickerSymbol(String tickerSymbol) {
            this.iM = tickerSymbol;
            this.hS.add(Integer.valueOf(52));
            return this;
        }

        public Builder setType(String type) {
            this.iN = type;
            this.hS.add(Integer.valueOf(53));
            return this;
        }

        public Builder setUrl(String url) {
            this.hE = url;
            this.hS.add(Integer.valueOf(54));
            return this;
        }

        public Builder setWidth(String width) {
            this.iO = width;
            this.hS.add(Integer.valueOf(55));
            return this;
        }

        public Builder setWorstRating(String worstRating) {
            this.iP = worstRating;
            this.hS.add(Integer.valueOf(56));
            return this;
        }
    }

    ItemScope getAbout();

    List<String> getAdditionalName();

    ItemScope getAddress();

    String getAddressCountry();

    String getAddressLocality();

    String getAddressRegion();

    List<ItemScope> getAssociated_media();

    int getAttendeeCount();

    List<ItemScope> getAttendees();

    ItemScope getAudio();

    List<ItemScope> getAuthor();

    String getBestRating();

    String getBirthDate();

    ItemScope getByArtist();

    String getCaption();

    String getContentSize();

    String getContentUrl();

    List<ItemScope> getContributor();

    String getDateCreated();

    String getDateModified();

    String getDatePublished();

    String getDescription();

    String getDuration();

    String getEmbedUrl();

    String getEndDate();

    String getFamilyName();

    String getGender();

    ItemScope getGeo();

    String getGivenName();

    String getHeight();

    String getId();

    String getImage();

    ItemScope getInAlbum();

    double getLatitude();

    ItemScope getLocation();

    double getLongitude();

    String getName();

    ItemScope getPartOfTVSeries();

    List<ItemScope> getPerformers();

    String getPlayerType();

    String getPostOfficeBoxNumber();

    String getPostalCode();

    String getRatingValue();

    ItemScope getReviewRating();

    String getStartDate();

    String getStreetAddress();

    String getText();

    ItemScope getThumbnail();

    String getThumbnailUrl();

    String getTickerSymbol();

    String getType();

    String getUrl();

    String getWidth();

    String getWorstRating();

    boolean hasAbout();

    boolean hasAdditionalName();

    boolean hasAddress();

    boolean hasAddressCountry();

    boolean hasAddressLocality();

    boolean hasAddressRegion();

    boolean hasAssociated_media();

    boolean hasAttendeeCount();

    boolean hasAttendees();

    boolean hasAudio();

    boolean hasAuthor();

    boolean hasBestRating();

    boolean hasBirthDate();

    boolean hasByArtist();

    boolean hasCaption();

    boolean hasContentSize();

    boolean hasContentUrl();

    boolean hasContributor();

    boolean hasDateCreated();

    boolean hasDateModified();

    boolean hasDatePublished();

    boolean hasDescription();

    boolean hasDuration();

    boolean hasEmbedUrl();

    boolean hasEndDate();

    boolean hasFamilyName();

    boolean hasGender();

    boolean hasGeo();

    boolean hasGivenName();

    boolean hasHeight();

    boolean hasId();

    boolean hasImage();

    boolean hasInAlbum();

    boolean hasLatitude();

    boolean hasLocation();

    boolean hasLongitude();

    boolean hasName();

    boolean hasPartOfTVSeries();

    boolean hasPerformers();

    boolean hasPlayerType();

    boolean hasPostOfficeBoxNumber();

    boolean hasPostalCode();

    boolean hasRatingValue();

    boolean hasReviewRating();

    boolean hasStartDate();

    boolean hasStreetAddress();

    boolean hasText();

    boolean hasThumbnail();

    boolean hasThumbnailUrl();

    boolean hasTickerSymbol();

    boolean hasType();

    boolean hasUrl();

    boolean hasWidth();

    boolean hasWorstRating();
}
