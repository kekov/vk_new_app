package com.google.android.gms.plus.model.moments;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.C1230k;
import com.google.android.gms.internal.eh;

public final class MomentBuffer extends DataBuffer<Moment> {
    public MomentBuffer(C1230k dataHolder) {
        super(dataHolder);
    }

    public Moment get(int position) {
        return new eh(this.O, position);
    }
}
