package com.google.android.gms.plus;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.C0127o;
import com.google.android.gms.internal.C0134r;
import com.vkontakte.android.ValidationActivity;

public class GooglePlusUtil {
    public static final int APP_DISABLED = 3;
    public static final int APP_MISSING = 1;
    public static final int APP_UPDATE_REQUIRED = 2;
    public static final String GOOGLE_PLUS_PACKAGE = "com.google.android.apps.plus";
    public static final String PLATFORM_LOGGING_TAG = "GooglePlusPlatform";
    public static final int SUCCESS = 0;

    private GooglePlusUtil() {
        throw new AssertionError("Cannot instantiate GooglePlusUtil");
    }

    static Dialog m519a(Builder builder, int i, Activity activity, int i2) {
        if (i2 < 0) {
            throw new IllegalArgumentException("Request code must not be negative.");
        }
        switch (i) {
            case ValidationActivity.VRESULT_NONE /*0*/:
                return null;
            case APP_MISSING /*1*/:
                return builder.setTitle(m520a(activity, "install_google_plus_title", "Get Google+")).setMessage(m520a(activity, "install_google_plus_text", "Download Google+ from Google Play so you can share this?")).setPositiveButton(m520a(activity, "install_google_plus_button", "Get Google+"), new C0127o(activity, C0134r.m465g(GOOGLE_PLUS_PACKAGE), i2)).create();
            case APP_UPDATE_REQUIRED /*2*/:
                return builder.setTitle(m520a(activity, "update_google_plus_title", "Update Google+")).setMessage(m520a(activity, "update_google_plus_text", "Update Google+ from Google Play so you can share this?")).setPositiveButton(m520a(activity, "update_google_plus_button", "Update"), new C0127o(activity, C0134r.m465g(GOOGLE_PLUS_PACKAGE), i2)).create();
            case APP_DISABLED /*3*/:
                return builder.setTitle(m520a(activity, "enable_google_plus_title", "Enable Google+")).setMessage(m520a(activity, "enable_google_plus_text", "Enable Google+ from Google Play so you can share this?")).setPositiveButton(m520a(activity, "enable_google_plus_button", "Enable Google+"), new C0127o(activity, C0134r.m463e(GOOGLE_PLUS_PACKAGE), i2)).create();
            default:
                throw new IllegalArgumentException("Unexpected errorCode " + i);
        }
    }

    static String m520a(Context context, String str, String str2) {
        try {
            Resources resources = context.createPackageContext(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, 0).getResources();
            str2 = resources.getString(resources.getIdentifier(str, "string", GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE));
        } catch (Throwable e) {
            Log.e("GooglePlusUtil", "Unable to load resources from GMS: GMS not installed.", e);
        } catch (Throwable e2) {
            Log.e("GooglePlusUtil", "Unable to load resources from GMS: Resource \"" + str + "\" not found.", e2);
        } catch (Throwable e22) {
            Log.e("GooglePlusUtil", "Unable to load resources from GMS.", e22);
        }
        return str2;
    }

    private static int m521b(Context context, int i) {
        PackageManager packageManager = context.getPackageManager();
        try {
            return packageManager.getPackageInfo(GOOGLE_PLUS_PACKAGE, 0).versionCode < i ? APP_UPDATE_REQUIRED : !packageManager.getApplicationInfo(GOOGLE_PLUS_PACKAGE, 0).enabled ? APP_DISABLED : 0;
        } catch (NameNotFoundException e) {
            return APP_MISSING;
        }
    }

    public static int checkGooglePlusApp(Context context) {
        return m521b(context, 330000000);
    }

    public static Dialog getErrorDialog(int errorCode, Activity activity, int requestCode) {
        return m519a(new Builder(activity), errorCode, activity, requestCode);
    }
}
