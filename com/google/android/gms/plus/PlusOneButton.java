package com.google.android.gms.plus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.dz;

public final class PlusOneButton extends ViewGroup {
    public static final int ANNOTATION_BUBBLE = 1;
    public static final int ANNOTATION_INLINE = 2;
    public static final int ANNOTATION_NONE = 0;
    public static final int SIZE_MEDIUM = 1;
    public static final int SIZE_SMALL = 0;
    public static final int SIZE_STANDARD = 3;
    public static final int SIZE_TALL = 2;
    private final dz gQ;

    public interface OnPlusOneClickListener {
        void onPlusOneClick(Intent intent);
    }

    public PlusOneButton(Context context) {
        this(context, null);
    }

    public PlusOneButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.gQ = new dz(context, attrs);
        addView(this.gQ);
        if (!isInEditMode()) {
            setOnPlusOneClickListener(null);
        }
    }

    public void initialize(PlusClient plusClient, String url, int activityRequestCode) {
        C0142x.m490a(getContext() instanceof Activity, "To use this method, the PlusOneButton must be placed in an Activity. Use initialize(PlusClient, String, OnPlusOneClickListener).");
        this.gQ.initialize(plusClient, url, activityRequestCode);
    }

    public void initialize(PlusClient plusClient, String url, OnPlusOneClickListener plusOneClickListener) {
        this.gQ.initialize(plusClient, url, SIZE_SMALL);
        this.gQ.setOnPlusOneClickListener(plusOneClickListener);
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        this.gQ.layout(SIZE_SMALL, SIZE_SMALL, right - left, bottom - top);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        View view = this.gQ;
        measureChild(view, widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(view.getMeasuredWidth(), view.getMeasuredHeight());
    }

    public void setAnnotation(int annotation) {
        this.gQ.setAnnotation(annotation);
    }

    public void setOnPlusOneClickListener(OnPlusOneClickListener listener) {
        this.gQ.setOnPlusOneClickListener(listener);
    }

    public void setSize(int size) {
        this.gQ.setSize(size);
    }
}
