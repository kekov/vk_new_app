package com.google.android.gms.plus;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.text.TextUtils;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.eq;
import com.google.android.gms.plus.model.people.Person;
import java.util.ArrayList;
import java.util.List;

public final class PlusShare {
    public static final String EXTRA_CALL_TO_ACTION = "com.google.android.apps.plus.CALL_TO_ACTION";
    public static final String EXTRA_CONTENT_DEEP_LINK_ID = "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID";
    public static final String EXTRA_CONTENT_DEEP_LINK_METADATA = "com.google.android.apps.plus.CONTENT_DEEP_LINK_METADATA";
    public static final String EXTRA_CONTENT_URL = "com.google.android.apps.plus.CONTENT_URL";
    public static final String EXTRA_IS_INTERACTIVE_POST = "com.google.android.apps.plus.GOOGLE_INTERACTIVE_POST";
    public static final String EXTRA_SENDER_ID = "com.google.android.apps.plus.SENDER_ID";
    public static final String KEY_CALL_TO_ACTION_DEEP_LINK_ID = "deepLinkId";
    public static final String KEY_CALL_TO_ACTION_LABEL = "label";
    public static final String KEY_CALL_TO_ACTION_URL = "url";
    public static final String KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION = "description";
    public static final String KEY_CONTENT_DEEP_LINK_METADATA_THUMBNAIL_URL = "thumbnailUrl";
    public static final String KEY_CONTENT_DEEP_LINK_METADATA_TITLE = "title";
    public static final String PARAM_CONTENT_DEEP_LINK_ID = "deep_link_id";

    public static class Builder {
        private boolean gR;
        private ArrayList<Uri> gS;
        private boolean gT;
        private final Intent mIntent;

        public Builder() {
            this("android.intent.action.SEND");
        }

        public Builder(Activity launchingActivity) {
            this("android.intent.action.SEND", launchingActivity.getPackageName(), launchingActivity.getComponentName());
        }

        public Builder(Activity launchingActivity, PlusClient plusClient) {
            this(launchingActivity);
            C0142x.m490a(plusClient != null, "Must include PlusClient in PlusShare.Builder constructor to create an interactive post.");
            C0142x.m490a(plusClient.isConnected(), "PlusClient must be connected to create an interactive post.");
            C0142x.m490a(plusClient.m1013A(Scopes.PLUS_LOGIN), "Must request PLUS_LOGIN scope in PlusClient to create an interactive post.");
            this.gT = true;
            Person currentPerson = plusClient.getCurrentPerson();
            this.mIntent.putExtra(PlusShare.EXTRA_SENDER_ID, currentPerson != null ? currentPerson.getId() : "0");
        }

        protected Builder(String intentAction) {
            this.mIntent = new Intent().setAction(intentAction);
        }

        protected Builder(String intentAction, String activityPackageName, ComponentName activityComponentName) {
            this(intentAction);
            this.mIntent.putExtra(ShareCompat.EXTRA_CALLING_PACKAGE, activityPackageName);
            if (activityComponentName != null) {
                this.mIntent.putExtra(ShareCompat.EXTRA_CALLING_ACTIVITY, activityComponentName);
            }
            this.mIntent.addFlags(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END);
            this.gR = true;
        }

        public Builder addCallToAction(String label, Uri uri, String deepLinkId) {
            C0142x.m490a(this.gR, "Must include the launching activity with PlusShare.Builder constructor before setting call-to-action");
            boolean z = (uri == null || TextUtils.isEmpty(uri.toString())) ? false : true;
            C0142x.m493b(z, (Object) "Must provide a call to action URL");
            C0142x.m490a(this.gT, "Must include PlusClient in PlusShare.Builder constructor to create an interactive post");
            Bundle bundle = new Bundle();
            if (!TextUtils.isEmpty(label)) {
                bundle.putString(PlusShare.KEY_CALL_TO_ACTION_LABEL, label);
            }
            bundle.putString(PlusShare.KEY_CALL_TO_ACTION_URL, uri.toString());
            if (!TextUtils.isEmpty(deepLinkId)) {
                bundle.putString(PlusShare.KEY_CALL_TO_ACTION_DEEP_LINK_ID, deepLinkId);
            }
            this.mIntent.putExtra(PlusShare.EXTRA_CALL_TO_ACTION, bundle);
            this.mIntent.setType("text/plain");
            this.mIntent.putExtra(PlusShare.EXTRA_IS_INTERACTIVE_POST, true);
            return this;
        }

        public Builder addStream(Uri streamUri) {
            Uri uri = (Uri) this.mIntent.getParcelableExtra("android.intent.extra.STREAM");
            if (uri == null) {
                return setStream(streamUri);
            }
            if (this.gS == null) {
                this.gS = new ArrayList();
            }
            this.gS.add(uri);
            this.gS.add(streamUri);
            return this;
        }

        public Intent getIntent() {
            boolean z = true;
            boolean z2 = this.gS != null && this.gS.size() > 1;
            boolean equals = this.mIntent.getAction().equals("android.intent.action.SEND_MULTIPLE");
            boolean booleanExtra = this.mIntent.getBooleanExtra(PlusShare.EXTRA_IS_INTERACTIVE_POST, false);
            if (z2 && booleanExtra) {
                z = false;
            }
            C0142x.m490a(z, "Call-to-action buttons are only available for URLs.");
            if (!booleanExtra || this.mIntent.hasExtra(PlusShare.EXTRA_CONTENT_URL)) {
                if (!z2 && equals) {
                    this.mIntent.setAction("android.intent.action.SEND");
                    if (this.gS == null || this.gS.isEmpty()) {
                        this.mIntent.removeExtra("android.intent.extra.STREAM");
                    } else {
                        this.mIntent.putExtra("android.intent.extra.STREAM", (Parcelable) this.gS.get(0));
                    }
                    this.gS = null;
                }
                if (z2 && !equals) {
                    this.mIntent.setAction("android.intent.action.SEND_MULTIPLE");
                    if (this.gS == null || this.gS.isEmpty()) {
                        this.mIntent.removeExtra("android.intent.extra.STREAM");
                    } else {
                        this.mIntent.putParcelableArrayListExtra("android.intent.extra.STREAM", this.gS);
                    }
                }
                if (!booleanExtra || this.mIntent.hasExtra(PlusShare.EXTRA_CONTENT_URL) || this.mIntent.hasExtra(PlusShare.EXTRA_CONTENT_DEEP_LINK_ID)) {
                    this.mIntent.setPackage(GooglePlusUtil.GOOGLE_PLUS_PACKAGE);
                    return this.mIntent;
                }
                throw new IllegalStateException("Must set content URL or content deep-link ID to use a call-to-action button.");
            }
            throw new IllegalStateException("The content URL is required for interactive posts.");
        }

        public Builder setContentDeepLinkId(String deepLinkId) {
            return setContentDeepLinkId(deepLinkId, null, null, null);
        }

        public Builder setContentDeepLinkId(String deepLinkId, String title, String description, Uri thumbnailUri) {
            C0142x.m493b(this.gR, (Object) "Must include the launching activity with PlusShare.Builder constructor before setting deep links");
            C0142x.m493b(!TextUtils.isEmpty(deepLinkId), (Object) "The deepLinkId parameter is required.");
            Bundle a = PlusShare.m524a(title, description, thumbnailUri);
            this.mIntent.putExtra(PlusShare.EXTRA_CONTENT_DEEP_LINK_ID, deepLinkId);
            this.mIntent.putExtra(PlusShare.EXTRA_CONTENT_DEEP_LINK_METADATA, a);
            return this;
        }

        public Builder setContentUrl(Uri uri) {
            Object obj = null;
            if (uri != null) {
                obj = uri.toString();
            }
            if (TextUtils.isEmpty(obj)) {
                this.mIntent.removeExtra(PlusShare.EXTRA_CONTENT_URL);
            } else {
                this.mIntent.putExtra(PlusShare.EXTRA_CONTENT_URL, obj);
            }
            return this;
        }

        public Builder setRecipients(List<Person> recipientList) {
            int size = recipientList != null ? recipientList.size() : 0;
            if (size == 0) {
                this.mIntent.removeExtra("com.google.android.apps.plus.RECIPIENT_IDS");
                this.mIntent.removeExtra("com.google.android.apps.plus.RECIPIENT_DISPLAY_NAMES");
            } else {
                ArrayList arrayList = new ArrayList(size);
                ArrayList arrayList2 = new ArrayList(size);
                for (Person person : recipientList) {
                    arrayList.add(person.getId());
                    arrayList2.add(person.getDisplayName());
                }
                this.mIntent.putStringArrayListExtra("com.google.android.apps.plus.RECIPIENT_IDS", arrayList);
                this.mIntent.putStringArrayListExtra("com.google.android.apps.plus.RECIPIENT_DISPLAY_NAMES", arrayList2);
            }
            return this;
        }

        public Builder setStream(Uri streamUri) {
            this.gS = null;
            this.mIntent.putExtra("android.intent.extra.STREAM", streamUri);
            return this;
        }

        public Builder setText(CharSequence text) {
            this.mIntent.putExtra("android.intent.extra.TEXT", text);
            return this;
        }

        public Builder setType(String mimeType) {
            this.mIntent.setType(mimeType);
            return this;
        }
    }

    protected PlusShare() {
        throw new AssertionError();
    }

    public static Bundle m524a(String str, String str2, Uri uri) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_CONTENT_DEEP_LINK_METADATA_TITLE, str);
        bundle.putString(KEY_CONTENT_DEEP_LINK_METADATA_DESCRIPTION, str2);
        if (uri != null) {
            bundle.putString(KEY_CONTENT_DEEP_LINK_METADATA_THUMBNAIL_URL, uri.toString());
        }
        return bundle;
    }

    public static Person createPerson(String id, String displayName) {
        if (TextUtils.isEmpty(id)) {
            throw new IllegalArgumentException("MinimalPerson ID must not be empty.");
        } else if (!TextUtils.isEmpty(displayName)) {
            return new eq(displayName, id, null, 0, null);
        } else {
            throw new IllegalArgumentException("Display name must not be empty.");
        }
    }

    public static String getDeepLinkId(Intent intent) {
        return (intent == null || intent.getData() == null) ? null : intent.getData().getQueryParameter(PARAM_CONTENT_DEEP_LINK_ID);
    }
}
