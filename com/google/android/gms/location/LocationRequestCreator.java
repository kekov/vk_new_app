package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class LocationRequestCreator implements Creator<LocationRequest> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m501a(LocationRequest locationRequest, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, locationRequest.mPriority);
        ad.m106c(parcel, LocationStatusCodes.GEOFENCE_NOT_AVAILABLE, locationRequest.f129T);
        ad.m91a(parcel, 2, locationRequest.ez);
        ad.m91a(parcel, 3, locationRequest.eA);
        ad.m99a(parcel, 4, locationRequest.eB);
        ad.m91a(parcel, 5, locationRequest.eu);
        ad.m106c(parcel, 6, locationRequest.eC);
        ad.m90a(parcel, 7, locationRequest.eD);
        ad.m87C(parcel, d);
    }

    public LocationRequest createFromParcel(Parcel parcel) {
        LocationRequest locationRequest = new LocationRequest();
        int c = ac.m58c(parcel);
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    locationRequest.mPriority = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    locationRequest.ez = ac.m64g(parcel, b);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    locationRequest.eA = ac.m64g(parcel, b);
                    break;
                case UserListView.TYPE_FAVE /*4*/:
                    locationRequest.eB = ac.m60c(parcel, b);
                    break;
                case UserListView.TYPE_FOLLOWERS /*5*/:
                    locationRequest.eu = ac.m64g(parcel, b);
                    break;
                case UserListView.TYPE_POLL_VOTERS /*6*/:
                    locationRequest.eC = ac.m63f(parcel, b);
                    break;
                case UserListView.TYPE_FAVE_LINKS /*7*/:
                    locationRequest.eD = ac.m66i(parcel, b);
                    break;
                case LocationStatusCodes.GEOFENCE_NOT_AVAILABLE /*1000*/:
                    locationRequest.f129T = ac.m63f(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return locationRequest;
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public LocationRequest[] newArray(int size) {
        return new LocationRequest[size];
    }
}
