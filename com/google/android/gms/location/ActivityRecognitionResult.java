package com.google.android.gms.location;

import android.content.Intent;
import android.os.Parcel;
import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.ae;
import java.util.Collections;
import java.util.List;

public class ActivityRecognitionResult implements ae {
    public static final ActivityRecognitionResultCreator CREATOR;
    public static final String EXTRA_ACTIVITY_RESULT = "com.google.android.location.internal.EXTRA_ACTIVITY_RESULT";
    int f127T;
    List<DetectedActivity> en;
    long eo;
    long ep;

    static {
        CREATOR = new ActivityRecognitionResultCreator();
    }

    public ActivityRecognitionResult() {
        this.f127T = 1;
    }

    public ActivityRecognitionResult(DetectedActivity mostProbableActivity, long time, long elapsedRealtimeMillis) {
        this(Collections.singletonList(mostProbableActivity), time, elapsedRealtimeMillis);
    }

    public ActivityRecognitionResult(List<DetectedActivity> probableActivities, long time, long elapsedRealtimeMillis) {
        this();
        boolean z = probableActivities != null && probableActivities.size() > 0;
        C0142x.m493b(z, (Object) "Must have at least 1 detected activity");
        this.en = probableActivities;
        this.eo = time;
        this.ep = elapsedRealtimeMillis;
    }

    public static ActivityRecognitionResult extractResult(Intent intent) {
        return !hasResult(intent) ? null : (ActivityRecognitionResult) intent.getExtras().get(EXTRA_ACTIVITY_RESULT);
    }

    public static boolean hasResult(Intent intent) {
        return intent == null ? false : intent.hasExtra(EXTRA_ACTIVITY_RESULT);
    }

    public int describeContents() {
        return 0;
    }

    public int getActivityConfidence(int activityType) {
        for (DetectedActivity detectedActivity : this.en) {
            if (detectedActivity.getType() == activityType) {
                return detectedActivity.getConfidence();
            }
        }
        return 0;
    }

    public long getElapsedRealtimeMillis() {
        return this.ep;
    }

    public DetectedActivity getMostProbableActivity() {
        return (DetectedActivity) this.en.get(0);
    }

    public List<DetectedActivity> getProbableActivities() {
        return this.en;
    }

    public long getTime() {
        return this.eo;
    }

    public String toString() {
        return "ActivityRecognitionResult [probableActivities=" + this.en + ", timeMillis=" + this.eo + ", elapsedRealtimeMillis=" + this.ep + "]";
    }

    public void writeToParcel(Parcel out, int flags) {
        ActivityRecognitionResultCreator.m499a(this, out, flags);
    }
}
