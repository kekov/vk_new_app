package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class ActivityRecognitionResultCreator implements Creator<ActivityRecognitionResult> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m499a(ActivityRecognitionResult activityRecognitionResult, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m105b(parcel, 1, activityRecognitionResult.en, false);
        ad.m106c(parcel, LocationStatusCodes.GEOFENCE_NOT_AVAILABLE, activityRecognitionResult.f127T);
        ad.m91a(parcel, 2, activityRecognitionResult.eo);
        ad.m91a(parcel, 3, activityRecognitionResult.ep);
        ad.m87C(parcel, d);
    }

    public ActivityRecognitionResult createFromParcel(Parcel parcel) {
        ActivityRecognitionResult activityRecognitionResult = new ActivityRecognitionResult();
        int c = ac.m58c(parcel);
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    activityRecognitionResult.en = ac.m59c(parcel, b, DetectedActivity.CREATOR);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    activityRecognitionResult.eo = ac.m64g(parcel, b);
                    break;
                case Group.ADMIN_LEVEL_ADMIN /*3*/:
                    activityRecognitionResult.ep = ac.m64g(parcel, b);
                    break;
                case LocationStatusCodes.GEOFENCE_NOT_AVAILABLE /*1000*/:
                    activityRecognitionResult.f127T = ac.m63f(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return activityRecognitionResult;
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public ActivityRecognitionResult[] newArray(int size) {
        return new ActivityRecognitionResult[size];
    }
}
