package com.google.android.gms.location;

import android.os.Parcel;
import com.google.android.gms.internal.ae;

public class DetectedActivity implements ae {
    public static final DetectedActivityCreator CREATOR;
    public static final int IN_VEHICLE = 0;
    public static final int ON_BICYCLE = 1;
    public static final int ON_FOOT = 2;
    public static final int STILL = 3;
    public static final int TILTING = 5;
    public static final int UNKNOWN = 4;
    int f128T;
    int eq;
    int er;

    static {
        CREATOR = new DetectedActivityCreator();
    }

    public DetectedActivity() {
        this.f128T = ON_BICYCLE;
    }

    public DetectedActivity(int activityType, int confidence) {
        this();
        this.eq = activityType;
        this.er = confidence;
    }

    private int m973G(int i) {
        return i > TILTING ? UNKNOWN : i;
    }

    public int describeContents() {
        return IN_VEHICLE;
    }

    public int getConfidence() {
        return this.er;
    }

    public int getType() {
        return m973G(this.eq);
    }

    public String toString() {
        return "DetectedActivity [type=" + getType() + ", confidence=" + this.er + "]";
    }

    public void writeToParcel(Parcel out, int flags) {
        DetectedActivityCreator.m500a(this, out, flags);
    }
}
