package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.ac;
import com.google.android.gms.internal.ac.C0107a;
import com.google.android.gms.internal.ad;
import com.vkontakte.android.fragments.BoardTopicsFragment;

public class DetectedActivityCreator implements Creator<DetectedActivity> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void m500a(DetectedActivity detectedActivity, Parcel parcel, int i) {
        int d = ad.m108d(parcel);
        ad.m106c(parcel, 1, detectedActivity.eq);
        ad.m106c(parcel, LocationStatusCodes.GEOFENCE_NOT_AVAILABLE, detectedActivity.f128T);
        ad.m106c(parcel, 2, detectedActivity.er);
        ad.m87C(parcel, d);
    }

    public DetectedActivity createFromParcel(Parcel parcel) {
        DetectedActivity detectedActivity = new DetectedActivity();
        int c = ac.m58c(parcel);
        while (parcel.dataPosition() < c) {
            int b = ac.m55b(parcel);
            switch (ac.m68j(b)) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    detectedActivity.eq = ac.m63f(parcel, b);
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    detectedActivity.er = ac.m63f(parcel, b);
                    break;
                case LocationStatusCodes.GEOFENCE_NOT_AVAILABLE /*1000*/:
                    detectedActivity.f128T = ac.m63f(parcel, b);
                    break;
                default:
                    ac.m56b(parcel, b);
                    break;
            }
        }
        if (parcel.dataPosition() == c) {
            return detectedActivity;
        }
        throw new C0107a("Overread allowed size end=" + c, parcel);
    }

    public DetectedActivity[] newArray(int size) {
        return new DetectedActivity[size];
    }
}
