package com.google.android.gms.location;

import android.os.Parcel;
import android.os.SystemClock;
import com.google.android.gms.internal.ae;

public final class LocationRequest implements ae {
    public static final LocationRequestCreator CREATOR;
    public static final int PRIORITY_BALANCED_POWER_ACCURACY = 102;
    public static final int PRIORITY_HIGH_ACCURACY = 100;
    public static final int PRIORITY_NO_POWER = 105;
    int f129T;
    long eA;
    boolean eB;
    int eC;
    float eD;
    long eu;
    long ez;
    int mPriority;

    static {
        CREATOR = new LocationRequestCreator();
    }

    public LocationRequest() {
        this.mPriority = PRIORITY_BALANCED_POWER_ACCURACY;
        this.ez = 3600000;
        this.eA = (long) (((double) this.ez) / 6.0d);
        this.eB = false;
        this.eu = Long.MAX_VALUE;
        this.eC = ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        this.eD = 0.0f;
    }

    private static void m974H(int i) {
        switch (i) {
            case PRIORITY_HIGH_ACCURACY /*100*/:
            case PRIORITY_BALANCED_POWER_ACCURACY /*102*/:
            case 104:
            case PRIORITY_NO_POWER /*105*/:
            default:
                throw new IllegalArgumentException("invalid quality: " + i);
        }
    }

    public static String m975I(int i) {
        switch (i) {
            case PRIORITY_HIGH_ACCURACY /*100*/:
                return "PRIORITY_HIGH_ACCURACY";
            case PRIORITY_BALANCED_POWER_ACCURACY /*102*/:
                return "PRIORITY_BALANCED_POWER_ACCURACY";
            case 104:
                return "PRIORITY_LOW_POWER";
            default:
                return "???";
        }
    }

    private static void m976a(float f) {
        if (f < 0.0f) {
            throw new IllegalArgumentException("invalid displacement: " + f);
        }
    }

    private static void m977c(long j) {
        if (j < 0) {
            throw new IllegalArgumentException("invalid interval: " + j);
        }
    }

    public static LocationRequest create() {
        return new LocationRequest();
    }

    public int describeContents() {
        return 0;
    }

    public long getExpirationTime() {
        return this.eu;
    }

    public long getFastestInterval() {
        return this.eA;
    }

    public long getInterval() {
        return this.ez;
    }

    public int getNumUpdates() {
        return this.eC;
    }

    public int getPriority() {
        return this.mPriority;
    }

    public float getSmallestDisplacement() {
        return this.eD;
    }

    public LocationRequest setExpirationDuration(long millis) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (millis > Long.MAX_VALUE - elapsedRealtime) {
            this.eu = Long.MAX_VALUE;
        } else {
            this.eu = elapsedRealtime + millis;
        }
        if (this.eu < 0) {
            this.eu = 0;
        }
        return this;
    }

    public LocationRequest setExpirationTime(long millis) {
        this.eu = millis;
        if (this.eu < 0) {
            this.eu = 0;
        }
        return this;
    }

    public LocationRequest setFastestInterval(long millis) {
        m977c(millis);
        this.eB = true;
        this.eA = millis;
        return this;
    }

    public LocationRequest setInterval(long millis) {
        m977c(millis);
        this.ez = millis;
        if (!this.eB) {
            this.eA = (long) (((double) this.ez) / 6.0d);
        }
        return this;
    }

    public LocationRequest setNumUpdates(int numUpdates) {
        if (numUpdates <= 0) {
            throw new IllegalArgumentException("invalid numUpdates: " + numUpdates);
        }
        this.eC = numUpdates;
        return this;
    }

    public LocationRequest setPriority(int priority) {
        m974H(priority);
        this.mPriority = priority;
        return this;
    }

    public LocationRequest setSmallestDisplacement(float smallestDisplacementMeters) {
        m976a(smallestDisplacementMeters);
        this.eD = smallestDisplacementMeters;
        return this;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Request[").append(m975I(this.mPriority));
        if (this.mPriority != PRIORITY_NO_POWER) {
            stringBuilder.append(" requested=");
            stringBuilder.append(this.ez + "ms");
        }
        stringBuilder.append(" fastest=");
        stringBuilder.append(this.eA + "ms");
        if (this.eu != Long.MAX_VALUE) {
            long elapsedRealtime = this.eu - SystemClock.elapsedRealtime();
            stringBuilder.append(" expireIn=");
            stringBuilder.append(elapsedRealtime + "ms");
        }
        if (this.eC != ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED) {
            stringBuilder.append(" num=").append(this.eC);
        }
        stringBuilder.append(']');
        return stringBuilder.toString();
    }

    public void writeToParcel(Parcel parcel, int flags) {
        LocationRequestCreator locationRequestCreator = CREATOR;
        LocationRequestCreator.m501a(this, parcel, flags);
    }
}
