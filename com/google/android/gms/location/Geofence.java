package com.google.android.gms.location;

import android.os.SystemClock;
import com.google.android.gms.internal.cf;

public interface Geofence {
    public static final int GEOFENCE_TRANSITION_ENTER = 1;
    public static final int GEOFENCE_TRANSITION_EXIT = 2;
    public static final long NEVER_EXPIRE = -1;

    public static final class Builder {
        private String es;
        private int et;
        private long eu;
        private short ev;
        private double ew;
        private double ex;
        private float ey;

        public Builder() {
            this.es = null;
            this.et = 0;
            this.eu = Long.MIN_VALUE;
            this.ev = (short) -1;
        }

        public Geofence build() {
            if (this.es == null) {
                throw new IllegalArgumentException("Request ID not set.");
            } else if (this.et == 0) {
                throw new IllegalArgumentException("Transitions types not set.");
            } else if (this.eu == Long.MIN_VALUE) {
                throw new IllegalArgumentException("Expiration not set.");
            } else if (this.ev != (short) -1) {
                return new cf(this.es, this.et, (short) 1, this.ew, this.ex, this.ey, this.eu);
            } else {
                throw new IllegalArgumentException("Geofence region not set.");
            }
        }

        public Builder setCircularRegion(double latitude, double longitude, float radius) {
            this.ev = (short) 1;
            this.ew = latitude;
            this.ex = longitude;
            this.ey = radius;
            return this;
        }

        public Builder setExpirationDuration(long durationMillis) {
            if (durationMillis < 0) {
                this.eu = Geofence.NEVER_EXPIRE;
            } else {
                this.eu = SystemClock.elapsedRealtime() + durationMillis;
            }
            return this;
        }

        public Builder setRequestId(String requestId) {
            this.es = requestId;
            return this;
        }

        public Builder setTransitionTypes(int transitionTypes) {
            this.et = transitionTypes;
            return this;
        }
    }

    String getRequestId();
}
