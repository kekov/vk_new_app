package com.google.android.gms.games.leaderboard;

import com.google.android.gms.internal.C1230k;
import com.google.android.gms.internal.C1231m;
import com.google.android.gms.internal.br;

public final class LeaderboardBuffer extends C1231m<Leaderboard> {
    public LeaderboardBuffer(C1230k dataHolder) {
        super(dataHolder);
    }

    protected /* synthetic */ Object m1033a(int i, int i2) {
        return getEntry(i, i2);
    }

    protected Leaderboard getEntry(int rowIndex, int numChildren) {
        return new br(this.O, rowIndex, numChildren);
    }

    protected String getPrimaryDataMarkerColumn() {
        return "external_leaderboard_id";
    }
}
