package com.google.android.gms.games.leaderboard;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.C1230k;
import com.google.android.gms.internal.bs;
import com.google.android.gms.internal.bu;

public final class LeaderboardScoreBuffer extends DataBuffer<LeaderboardScore> {
    private final bs dp;

    public LeaderboardScoreBuffer(C1230k dataHolder) {
        super(dataHolder);
        this.dp = new bs(dataHolder.m925h());
    }

    public bs aq() {
        return this.dp;
    }

    public LeaderboardScore get(int position) {
        return new bu(this.O, position);
    }
}
