package com.google.android.gms.games.leaderboard;

import com.google.android.gms.internal.C0141w;
import com.google.android.gms.internal.C0141w.C0140a;
import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.C1230k;
import com.google.android.gms.internal.bq;
import java.util.HashMap;

public final class SubmitScoreResult {
    private static final String[] dB;
    private String cw;
    private String dC;
    private HashMap<Integer, Result> dD;
    private int f29p;

    public static final class Result {
        public final String formattedScore;
        public final boolean newBest;
        public final long rawScore;

        public Result(long rawScore, String formattedScore, boolean newBest) {
            this.rawScore = rawScore;
            this.formattedScore = formattedScore;
            this.newBest = newBest;
        }

        public String toString() {
            return C0141w.m488c(this).m486a("RawScore", Long.valueOf(this.rawScore)).m486a("FormattedScore", this.formattedScore).m486a("NewBest", Boolean.valueOf(this.newBest)).toString();
        }
    }

    static {
        dB = new String[]{"leaderboardId", "playerId", "timeSpan", "hasResult", "rawScore", "formattedScore", "newBest"};
    }

    public SubmitScoreResult(int statusCode, String leaderboardId, String playerId) {
        this(statusCode, leaderboardId, playerId, new HashMap());
    }

    public SubmitScoreResult(int statusCode, String leaderboardId, String playerId, HashMap<Integer, Result> results) {
        this.f29p = statusCode;
        this.dC = leaderboardId;
        this.cw = playerId;
        this.dD = results;
    }

    public SubmitScoreResult(C1230k dataHolder) {
        int i = 0;
        this.f29p = dataHolder.getStatusCode();
        this.dD = new HashMap();
        int count = dataHolder.getCount();
        C0142x.m494c(count == 3);
        while (i < count) {
            int d = dataHolder.m919d(i);
            if (i == 0) {
                this.dC = dataHolder.m918c("leaderboardId", i, d);
                this.cw = dataHolder.m918c("playerId", i, d);
            }
            if (dataHolder.m920d("hasResult", i, d)) {
                m31a(new Result(dataHolder.m915a("rawScore", i, d), dataHolder.m918c("formattedScore", i, d), dataHolder.m920d("newBest", i, d)), dataHolder.m917b("timeSpan", i, d));
            }
            i++;
        }
    }

    private void m31a(Result result, int i) {
        this.dD.put(Integer.valueOf(i), result);
    }

    public String getLeaderboardId() {
        return this.dC;
    }

    public String getPlayerId() {
        return this.cw;
    }

    public Result getScoreResult(int timeSpan) {
        return (Result) this.dD.get(Integer.valueOf(timeSpan));
    }

    public int getStatusCode() {
        return this.f29p;
    }

    public String toString() {
        C0140a a = C0141w.m488c(this).m486a("PlayerId", this.cw).m486a("StatusCode", Integer.valueOf(this.f29p));
        for (int i = 0; i < 3; i++) {
            Result result = (Result) this.dD.get(Integer.valueOf(i));
            a.m486a("TimesSpan", bq.m280B(i));
            a.m486a("Result", result == null ? "null" : result.toString());
        }
        return a.toString();
    }
}
