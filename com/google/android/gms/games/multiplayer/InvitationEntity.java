package com.google.android.gms.games.multiplayer;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.games.Game;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.internal.C0141w;
import com.google.android.gms.internal.C0142x;
import java.util.ArrayList;

public final class InvitationEntity implements Invitation {
    public static final Creator<InvitationEntity> CREATOR;
    private final GameEntity dE;
    private final String dF;
    private final long dG;
    private final int dH;
    private final Participant dI;
    private final ArrayList<Participant> dJ;

    /* renamed from: com.google.android.gms.games.multiplayer.InvitationEntity.1 */
    static class C01001 implements Creator<InvitationEntity> {
        C01001() {
        }

        public InvitationEntity[] m32C(int i) {
            return new InvitationEntity[i];
        }

        public /* synthetic */ Object createFromParcel(Parcel x0) {
            return m33p(x0);
        }

        public /* synthetic */ Object[] newArray(int x0) {
            return m32C(x0);
        }

        public InvitationEntity m33p(Parcel parcel) {
            GameEntity gameEntity = (GameEntity) GameEntity.CREATOR.createFromParcel(parcel);
            String readString = parcel.readString();
            long readLong = parcel.readLong();
            int readInt = parcel.readInt();
            Participant participant = (Participant) ParticipantEntity.CREATOR.createFromParcel(parcel);
            int readInt2 = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt2);
            for (int i = 0; i < readInt2; i++) {
                arrayList.add(ParticipantEntity.CREATOR.createFromParcel(parcel));
            }
            return new InvitationEntity(readString, readLong, readInt, participant, arrayList, null);
        }
    }

    static {
        CREATOR = new C01001();
    }

    private InvitationEntity(GameEntity game, String invitationId, long creationTimestamp, int invitationType, Participant inviter, ArrayList<Participant> participants) {
        this.dE = game;
        this.dF = invitationId;
        this.dG = creationTimestamp;
        this.dH = invitationType;
        this.dI = inviter;
        this.dJ = participants;
    }

    public InvitationEntity(Invitation invitation) {
        this.dE = new GameEntity(invitation.getGame());
        this.dF = invitation.getInvitationId();
        this.dG = invitation.getCreationTimestamp();
        this.dH = invitation.getInvitationType();
        String participantId = invitation.getInviter().getParticipantId();
        Object obj = null;
        ArrayList participants = invitation.getParticipants();
        int size = participants.size();
        this.dJ = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            Participant participant = (Participant) participants.get(i);
            if (participant.getParticipantId().equals(participantId)) {
                obj = participant;
            }
            this.dJ.add(participant.freeze());
        }
        C0142x.m492b(obj, (Object) "Must have a valid inviter!");
        this.dI = (Participant) obj.freeze();
    }

    public static int m1035a(Invitation invitation) {
        return C0141w.hashCode(invitation.getGame(), invitation.getInvitationId(), Long.valueOf(invitation.getCreationTimestamp()), Integer.valueOf(invitation.getInvitationType()), invitation.getInviter(), invitation.getParticipants());
    }

    public static boolean m1036a(Invitation invitation, Object obj) {
        if (!(obj instanceof Invitation)) {
            return false;
        }
        if (invitation == obj) {
            return true;
        }
        Invitation invitation2 = (Invitation) obj;
        return C0141w.m487a(invitation2.getGame(), invitation.getGame()) && C0141w.m487a(invitation2.getInvitationId(), invitation.getInvitationId()) && C0141w.m487a(Long.valueOf(invitation2.getCreationTimestamp()), Long.valueOf(invitation.getCreationTimestamp())) && C0141w.m487a(Integer.valueOf(invitation2.getInvitationType()), Integer.valueOf(invitation.getInvitationType())) && C0141w.m487a(invitation2.getInviter(), invitation.getInviter()) && C0141w.m487a(invitation2.getParticipants(), invitation.getParticipants());
    }

    public static String m1037b(Invitation invitation) {
        return C0141w.m488c(invitation).m486a("Game", invitation.getGame()).m486a("InvitationId", invitation.getInvitationId()).m486a("CreationTimestamp", Long.valueOf(invitation.getCreationTimestamp())).m486a("InvitationType", Integer.valueOf(invitation.getInvitationType())).m486a("Inviter", invitation.getInviter()).m486a("Participants", invitation.getParticipants()).toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return m1036a(this, obj);
    }

    public Invitation freeze() {
        return this;
    }

    public long getCreationTimestamp() {
        return this.dG;
    }

    public Game getGame() {
        return this.dE;
    }

    public String getInvitationId() {
        return this.dF;
    }

    public int getInvitationType() {
        return this.dH;
    }

    public Participant getInviter() {
        return this.dI;
    }

    public ArrayList<Participant> getParticipants() {
        return this.dJ;
    }

    public int hashCode() {
        return m1035a(this);
    }

    public String toString() {
        return m1037b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        this.dE.writeToParcel(dest, flags);
        dest.writeString(this.dF);
        dest.writeLong(this.dG);
        dest.writeInt(this.dH);
        this.dI.writeToParcel(dest, flags);
        int size = this.dJ.size();
        dest.writeInt(size);
        for (int i = 0; i < size; i++) {
            ((Participant) this.dJ.get(i)).writeToParcel(dest, flags);
        }
    }
}
