package com.google.android.gms.games.multiplayer;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.PlayerEntity;
import com.google.android.gms.internal.C0141w;
import com.google.android.gms.internal.ax;

public final class ParticipantEntity implements Parcelable, Participant {
    public static final Creator<ParticipantEntity> CREATOR;
    private final String bm;
    private final Uri cj;
    private final Uri ck;
    private final PlayerEntity dM;
    private final int dN;
    private final String dO;
    private final boolean dP;
    private final String dm;

    /* renamed from: com.google.android.gms.games.multiplayer.ParticipantEntity.1 */
    static class C01011 implements Creator<ParticipantEntity> {
        C01011() {
        }

        public ParticipantEntity[] m34D(int i) {
            return new ParticipantEntity[i];
        }

        public /* synthetic */ Object createFromParcel(Parcel x0) {
            return m35q(x0);
        }

        public /* synthetic */ Object[] newArray(int x0) {
            return m34D(x0);
        }

        public ParticipantEntity m35q(Parcel parcel) {
            Object obj = 1;
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            Uri parse = readString3 == null ? null : Uri.parse(readString3);
            String readString4 = parcel.readString();
            Uri parse2 = readString4 == null ? null : Uri.parse(readString4);
            int readInt = parcel.readInt();
            String readString5 = parcel.readString();
            boolean z = parcel.readInt() > 0;
            if (parcel.readInt() <= 0) {
                obj = null;
            }
            return new ParticipantEntity(readString2, parse, parse2, readInt, readString5, z, obj != null ? (PlayerEntity) PlayerEntity.CREATOR.createFromParcel(parcel) : null, null);
        }
    }

    static {
        CREATOR = new C01011();
    }

    public ParticipantEntity(Participant participant) {
        Player player = participant.getPlayer();
        this.dM = player == null ? null : new PlayerEntity(player);
        this.dm = participant.getParticipantId();
        this.bm = participant.getDisplayName();
        this.cj = participant.getIconImageUri();
        this.ck = participant.getHiResImageUri();
        this.dN = participant.getStatus();
        this.dO = participant.getClientAddress();
        this.dP = participant.isConnectedToRoom();
    }

    private ParticipantEntity(String participantId, String displayName, Uri iconImageUri, Uri hiResImageUri, int status, String clientAddress, boolean connectedToRoom, PlayerEntity player) {
        this.dm = participantId;
        this.bm = displayName;
        this.cj = iconImageUri;
        this.ck = hiResImageUri;
        this.dN = status;
        this.dO = clientAddress;
        this.dP = connectedToRoom;
        this.dM = player;
    }

    public static int m1038a(Participant participant) {
        return C0141w.hashCode(participant.getPlayer(), Integer.valueOf(participant.getStatus()), participant.getClientAddress(), Boolean.valueOf(participant.isConnectedToRoom()), participant.getDisplayName(), participant.getIconImageUri(), participant.getHiResImageUri());
    }

    public static boolean m1039a(Participant participant, Object obj) {
        if (!(obj instanceof Participant)) {
            return false;
        }
        if (participant == obj) {
            return true;
        }
        Participant participant2 = (Participant) obj;
        return C0141w.m487a(participant2.getPlayer(), participant.getPlayer()) && C0141w.m487a(Integer.valueOf(participant2.getStatus()), Integer.valueOf(participant.getStatus())) && C0141w.m487a(participant2.getClientAddress(), participant.getClientAddress()) && C0141w.m487a(Boolean.valueOf(participant2.isConnectedToRoom()), Boolean.valueOf(participant.isConnectedToRoom())) && C0141w.m487a(participant2.getDisplayName(), participant.getDisplayName()) && C0141w.m487a(participant2.getIconImageUri(), participant.getIconImageUri()) && C0141w.m487a(participant2.getHiResImageUri(), participant.getHiResImageUri());
    }

    public static String m1040b(Participant participant) {
        return C0141w.m488c(participant).m486a("Player", participant.getPlayer()).m486a("Status", Integer.valueOf(participant.getStatus())).m486a("ClientAddress", participant.getClientAddress()).m486a("ConnectedToRoom", Boolean.valueOf(participant.isConnectedToRoom())).m486a("DisplayName", participant.getDisplayName()).m486a("IconImage", participant.getIconImageUri()).m486a("HiResImage", participant.getHiResImageUri()).toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return m1039a(this, obj);
    }

    public Participant freeze() {
        return this;
    }

    public String getClientAddress() {
        return this.dO;
    }

    public String getDisplayName() {
        return this.dM == null ? this.bm : this.dM.getDisplayName();
    }

    public void getDisplayName(CharArrayBuffer dataOut) {
        if (this.dM == null) {
            ax.m162b(this.bm, dataOut);
        } else {
            this.dM.getDisplayName(dataOut);
        }
    }

    public Uri getHiResImageUri() {
        return this.dM == null ? this.ck : this.dM.getHiResImageUri();
    }

    public Uri getIconImageUri() {
        return this.dM == null ? this.cj : this.dM.getIconImageUri();
    }

    public String getParticipantId() {
        return this.dm;
    }

    public Player getPlayer() {
        return this.dM;
    }

    public int getStatus() {
        return this.dN;
    }

    public int hashCode() {
        return m1038a(this);
    }

    public boolean isConnectedToRoom() {
        return this.dP;
    }

    public String toString() {
        return m1040b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        String str = null;
        int i = 0;
        dest.writeString(this.dm);
        dest.writeString(this.bm);
        dest.writeString(this.cj == null ? null : this.cj.toString());
        if (this.ck != null) {
            str = this.ck.toString();
        }
        dest.writeString(str);
        dest.writeInt(this.dN);
        dest.writeString(this.dO);
        dest.writeInt(this.dP ? 1 : 0);
        if (this.dM != null) {
            i = 1;
        }
        dest.writeInt(i);
        if (this.dM != null) {
            this.dM.writeToParcel(dest, flags);
        }
    }
}
