package com.google.android.gms.games.multiplayer.realtime;

import android.database.CharArrayBuffer;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.multiplayer.Participant;
import com.google.android.gms.games.multiplayer.ParticipantEntity;
import com.google.android.gms.internal.C0141w;
import com.google.android.gms.internal.ax;
import java.util.ArrayList;

public final class RoomEntity implements Room {
    public static final Creator<RoomEntity> CREATOR;
    private final String cU;
    private final String ch;
    private final long dG;
    private final ArrayList<Participant> dJ;
    private final int dX;
    private final Bundle dZ;
    private final String ed;
    private final int ee;

    /* renamed from: com.google.android.gms.games.multiplayer.realtime.RoomEntity.1 */
    static class C01041 implements Creator<RoomEntity> {
        C01041() {
        }

        public RoomEntity[] m39F(int i) {
            return new RoomEntity[i];
        }

        public /* synthetic */ Object createFromParcel(Parcel x0) {
            return m40s(x0);
        }

        public /* synthetic */ Object[] newArray(int x0) {
            return m39F(x0);
        }

        public RoomEntity m40s(Parcel parcel) {
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            long readLong = parcel.readLong();
            int readInt = parcel.readInt();
            String readString3 = parcel.readString();
            int readInt2 = parcel.readInt();
            Bundle readBundle = parcel.readBundle();
            int readInt3 = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt3);
            for (int i = 0; i < readInt3; i++) {
                arrayList.add(ParticipantEntity.CREATOR.createFromParcel(parcel));
            }
            return new RoomEntity(readString2, readLong, readInt, readString3, readInt2, readBundle, arrayList, null);
        }
    }

    static {
        CREATOR = new C01041();
    }

    public RoomEntity(Room room) {
        this.cU = room.getRoomId();
        this.ed = room.getCreatorId();
        this.dG = room.getCreationTimestamp();
        this.ee = room.getStatus();
        this.ch = room.getDescription();
        this.dX = room.getVariant();
        this.dZ = room.getAutoMatchCriteria();
        ArrayList participants = room.getParticipants();
        int size = participants.size();
        this.dJ = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            this.dJ.add(((Participant) participants.get(i)).freeze());
        }
    }

    private RoomEntity(String roomId, String creatorId, long creationTimestamp, int roomStatus, String description, int variant, Bundle autoMatchCriteria, ArrayList<Participant> participants) {
        this.cU = roomId;
        this.ed = creatorId;
        this.dG = creationTimestamp;
        this.ee = roomStatus;
        this.ch = description;
        this.dX = variant;
        this.dZ = autoMatchCriteria;
        this.dJ = participants;
    }

    public static int m1041a(Room room) {
        return C0141w.hashCode(room.getRoomId(), room.getCreatorId(), Long.valueOf(room.getCreationTimestamp()), Integer.valueOf(room.getStatus()), room.getDescription(), Integer.valueOf(room.getVariant()), room.getAutoMatchCriteria(), room.getParticipants());
    }

    public static boolean m1042a(Room room, Object obj) {
        if (!(obj instanceof Room)) {
            return false;
        }
        if (room == obj) {
            return true;
        }
        Room room2 = (Room) obj;
        return C0141w.m487a(room2.getRoomId(), room.getRoomId()) && C0141w.m487a(room2.getCreatorId(), room.getCreatorId()) && C0141w.m487a(Long.valueOf(room2.getCreationTimestamp()), Long.valueOf(room.getCreationTimestamp())) && C0141w.m487a(Integer.valueOf(room2.getStatus()), Integer.valueOf(room.getStatus())) && C0141w.m487a(room2.getDescription(), room.getDescription()) && C0141w.m487a(Integer.valueOf(room2.getVariant()), Integer.valueOf(room.getVariant())) && C0141w.m487a(room2.getAutoMatchCriteria(), room.getAutoMatchCriteria()) && C0141w.m487a(room2.getParticipants(), room.getParticipants());
    }

    public static String m1043b(Room room) {
        return C0141w.m488c(room).m486a("RoomId", room.getRoomId()).m486a("CreatorId", room.getCreatorId()).m486a("CreationTimestamp", Long.valueOf(room.getCreationTimestamp())).m486a("RoomStatus", Integer.valueOf(room.getStatus())).m486a("Description", room.getDescription()).m486a("Variant", Integer.valueOf(room.getVariant())).m486a("AutoMatchCriteria", room.getAutoMatchCriteria()).m486a("Participants", room.getParticipants()).toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return m1042a(this, obj);
    }

    public Room freeze() {
        return this;
    }

    public Bundle getAutoMatchCriteria() {
        return this.dZ;
    }

    public long getCreationTimestamp() {
        return this.dG;
    }

    public String getCreatorId() {
        return this.ed;
    }

    public String getDescription() {
        return this.ch;
    }

    public void getDescription(CharArrayBuffer dataOut) {
        ax.m162b(this.ch, dataOut);
    }

    public String getParticipantId(String playerId) {
        int size = this.dJ.size();
        for (int i = 0; i < size; i++) {
            Participant participant = (Participant) this.dJ.get(i);
            Player player = participant.getPlayer();
            if (player != null && player.getPlayerId().equals(playerId)) {
                return participant.getParticipantId();
            }
        }
        return null;
    }

    public ArrayList<String> getParticipantIds() {
        int size = this.dJ.size();
        ArrayList<String> arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            arrayList.add(((Participant) this.dJ.get(i)).getParticipantId());
        }
        return arrayList;
    }

    public int getParticipantStatus(String participantId) {
        int size = this.dJ.size();
        for (int i = 0; i < size; i++) {
            Participant participant = (Participant) this.dJ.get(i);
            if (participant.getParticipantId().equals(participantId)) {
                return participant.getStatus();
            }
        }
        throw new IllegalStateException("Participant " + participantId + " is not in room " + getRoomId());
    }

    public ArrayList<Participant> getParticipants() {
        return this.dJ;
    }

    public String getRoomId() {
        return this.cU;
    }

    public int getStatus() {
        return this.ee;
    }

    public int getVariant() {
        return this.dX;
    }

    public int hashCode() {
        return m1041a(this);
    }

    public String toString() {
        return m1043b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.cU);
        dest.writeString(this.ed);
        dest.writeLong(this.dG);
        dest.writeInt(this.ee);
        dest.writeString(this.ch);
        dest.writeInt(this.dX);
        dest.writeBundle(this.dZ);
        int size = this.dJ.size();
        dest.writeInt(size);
        for (int i = 0; i < size; i++) {
            ((Participant) this.dJ.get(i)).writeToParcel(dest, flags);
        }
    }
}
