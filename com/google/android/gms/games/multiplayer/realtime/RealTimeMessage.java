package com.google.android.gms.games.multiplayer.realtime;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.C0142x;

public final class RealTimeMessage implements Parcelable {
    public static final Creator<RealTimeMessage> CREATOR;
    public static final int RELIABLE = 1;
    public static final int UNRELIABLE = 0;
    private final String dR;
    private final byte[] dS;
    private final int dT;

    /* renamed from: com.google.android.gms.games.multiplayer.realtime.RealTimeMessage.1 */
    static class C01021 implements Creator<RealTimeMessage> {
        C01021() {
        }

        public RealTimeMessage[] m37E(int i) {
            return new RealTimeMessage[i];
        }

        public /* synthetic */ Object createFromParcel(Parcel x0) {
            return m38r(x0);
        }

        public /* synthetic */ Object[] newArray(int x0) {
            return m37E(x0);
        }

        public RealTimeMessage m38r(Parcel parcel) {
            return new RealTimeMessage(null);
        }
    }

    static {
        CREATOR = new C01021();
    }

    private RealTimeMessage(Parcel parcel) {
        this(parcel.readString(), parcel.createByteArray(), parcel.readInt());
    }

    public RealTimeMessage(String senderParticipantId, byte[] messageData, int isReliable) {
        this.dR = (String) C0142x.m495d(senderParticipantId);
        this.dS = (byte[]) ((byte[]) C0142x.m495d(messageData)).clone();
        this.dT = isReliable;
    }

    public int describeContents() {
        return 0;
    }

    public byte[] getMessageData() {
        return this.dS;
    }

    public String getSenderParticipantId() {
        return this.dR;
    }

    public boolean isReliable() {
        return this.dT == RELIABLE;
    }

    public void writeToParcel(Parcel parcel, int flag) {
        parcel.writeString(this.dR);
        parcel.writeByteArray(this.dS);
        parcel.writeInt(this.dT);
    }
}
