package com.google.android.gms.games.multiplayer.realtime;

import android.os.Bundle;
import com.google.android.gms.games.GamesClient;
import com.google.android.gms.internal.C0142x;
import java.util.ArrayList;
import java.util.Arrays;

public final class RoomConfig {
    private final String dF;
    private final RoomUpdateListener dU;
    private final RoomStatusUpdateListener dV;
    private final RealTimeMessageReceivedListener dW;
    private final int dX;
    private final String[] dY;
    private final Bundle dZ;
    private final boolean ea;

    public static final class Builder {
        final RoomUpdateListener dU;
        RoomStatusUpdateListener dV;
        RealTimeMessageReceivedListener dW;
        int dX;
        Bundle dZ;
        boolean ea;
        String eb;
        ArrayList<String> ec;

        private Builder(RoomUpdateListener updateListener) {
            this.eb = null;
            this.dX = -1;
            this.ec = new ArrayList();
            this.ea = false;
            this.dU = (RoomUpdateListener) C0142x.m492b((Object) updateListener, (Object) "Must provide a RoomUpdateListener");
        }

        public Builder addPlayersToInvite(ArrayList<String> playerIds) {
            C0142x.m495d(playerIds);
            this.ec.addAll(playerIds);
            return this;
        }

        public Builder addPlayersToInvite(String... playerIds) {
            C0142x.m495d(playerIds);
            this.ec.addAll(Arrays.asList(playerIds));
            return this;
        }

        public RoomConfig build() {
            return new RoomConfig();
        }

        public Builder setAutoMatchCriteria(Bundle autoMatchCriteria) {
            this.dZ = autoMatchCriteria;
            return this;
        }

        public Builder setInvitationIdToAccept(String invitationId) {
            C0142x.m495d(invitationId);
            this.eb = invitationId;
            return this;
        }

        public Builder setMessageReceivedListener(RealTimeMessageReceivedListener listener) {
            this.dW = listener;
            return this;
        }

        public Builder setRoomStatusUpdateListener(RoomStatusUpdateListener listener) {
            this.dV = listener;
            return this;
        }

        public Builder setSocketCommunicationEnabled(boolean enableSockets) {
            this.ea = enableSockets;
            return this;
        }

        public Builder setVariant(int variant) {
            this.dX = variant;
            return this;
        }
    }

    private RoomConfig(Builder builder) {
        this.dU = builder.dU;
        this.dV = builder.dV;
        this.dW = builder.dW;
        this.dF = builder.eb;
        this.dX = builder.dX;
        this.dZ = builder.dZ;
        this.ea = builder.ea;
        this.dY = (String[]) builder.ec.toArray(new String[builder.ec.size()]);
        if (this.dW == null) {
            C0142x.m490a(this.ea, "Must either enable sockets OR specify a message listener");
        }
    }

    public static Builder builder(RoomUpdateListener listener) {
        return new Builder(null);
    }

    public static Bundle createAutoMatchCriteria(int minAutoMatchPlayers, int maxAutoMatchPlayers, long exclusiveBitMask) {
        Bundle bundle = new Bundle();
        bundle.putInt(GamesClient.EXTRA_MIN_AUTOMATCH_PLAYERS, minAutoMatchPlayers);
        bundle.putInt(GamesClient.EXTRA_MAX_AUTOMATCH_PLAYERS, maxAutoMatchPlayers);
        bundle.putLong(GamesClient.EXTRA_EXCLUSIVE_BIT_MASK, exclusiveBitMask);
        return bundle;
    }

    public Bundle getAutoMatchCriteria() {
        return this.dZ;
    }

    public String getInvitationId() {
        return this.dF;
    }

    public String[] getInvitedPlayerIds() {
        return this.dY;
    }

    public RealTimeMessageReceivedListener getMessageReceivedListener() {
        return this.dW;
    }

    public RoomStatusUpdateListener getRoomStatusUpdateListener() {
        return this.dV;
    }

    public RoomUpdateListener getRoomUpdateListener() {
        return this.dU;
    }

    public int getVariant() {
        return this.dX;
    }

    public boolean isSocketEnabled() {
        return this.ea;
    }
}
