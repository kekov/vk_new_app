package com.google.android.gms.games.multiplayer;

import com.google.android.gms.internal.C1230k;
import com.google.android.gms.internal.C1231m;
import com.google.android.gms.internal.bw;

public final class InvitationBuffer extends C1231m<Invitation> {
    public InvitationBuffer(C1230k dataHolder) {
        super(dataHolder);
    }

    protected /* synthetic */ Object m1034a(int i, int i2) {
        return getEntry(i, i2);
    }

    protected Invitation getEntry(int rowIndex, int numChildren) {
        return new bw(this.O, rowIndex, numChildren);
    }

    protected String getPrimaryDataMarkerColumn() {
        return "external_invitation_id";
    }
}
