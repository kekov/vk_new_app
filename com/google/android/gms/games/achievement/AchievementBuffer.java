package com.google.android.gms.games.achievement;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.C1230k;
import com.google.android.gms.internal.bh;

public final class AchievementBuffer extends DataBuffer<Achievement> {
    public AchievementBuffer(C1230k dataHolder) {
        super(dataHolder);
    }

    public Achievement get(int position) {
        return new bh(this.O, position);
    }
}
