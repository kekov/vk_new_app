package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.C0141w;
import com.google.android.gms.internal.ax;

public final class GameEntity implements Game {
    public static final Creator<GameEntity> CREATOR;
    private final String bm;
    private final String ce;
    private final String cf;
    private final String cg;
    private final String ch;
    private final String ci;
    private final Uri cj;
    private final Uri ck;
    private final Uri cl;
    private final boolean cm;
    private final boolean cn;
    private final String co;
    private final int cp;
    private final int cq;
    private final int cr;

    /* renamed from: com.google.android.gms.games.GameEntity.1 */
    static class C00971 implements Creator<GameEntity> {
        C00971() {
        }

        public /* synthetic */ Object createFromParcel(Parcel x0) {
            return m27n(x0);
        }

        public GameEntity m27n(Parcel parcel) {
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            String readString5 = parcel.readString();
            String readString6 = parcel.readString();
            String readString7 = parcel.readString();
            Uri parse = readString7 == null ? null : Uri.parse(readString7);
            readString7 = parcel.readString();
            Uri parse2 = readString7 == null ? null : Uri.parse(readString7);
            readString7 = parcel.readString();
            return new GameEntity(readString2, readString3, readString4, readString5, readString6, parse, parse2, readString7 == null ? null : Uri.parse(readString7), parcel.readInt() > 0, parcel.readInt() > 0, parcel.readString(), parcel.readInt(), parcel.readInt(), parcel.readInt(), null);
        }

        public /* synthetic */ Object[] newArray(int x0) {
            return m28v(x0);
        }

        public GameEntity[] m28v(int i) {
            return new GameEntity[i];
        }
    }

    static {
        CREATOR = new C00971();
    }

    public GameEntity(Game game) {
        this.ce = game.getApplicationId();
        this.cf = game.getPrimaryCategory();
        this.cg = game.getSecondaryCategory();
        this.ch = game.getDescription();
        this.ci = game.getDeveloperName();
        this.bm = game.getDisplayName();
        this.cj = game.getIconImageUri();
        this.ck = game.getHiResImageUri();
        this.cl = game.getFeaturedImageUri();
        this.cm = game.isPlayEnabledGame();
        this.cn = game.isInstanceInstalled();
        this.co = game.getInstancePackageName();
        this.cp = game.getGameplayAclStatus();
        this.cq = game.getAchievementTotalCount();
        this.cr = game.getLeaderboardCount();
    }

    private GameEntity(String applicationId, String displayName, String primaryCategory, String secondaryCategory, String description, String developerName, Uri iconImageUri, Uri hiResImageUri, Uri featuredImageUri, boolean playEnabledGame, boolean instanceInstalled, String instancePackageName, int gameplayAclStatus, int achievementTotalCount, int leaderboardCount) {
        this.ce = applicationId;
        this.bm = displayName;
        this.cf = primaryCategory;
        this.cg = secondaryCategory;
        this.ch = description;
        this.ci = developerName;
        this.cj = iconImageUri;
        this.ck = hiResImageUri;
        this.cl = featuredImageUri;
        this.cm = playEnabledGame;
        this.cn = instanceInstalled;
        this.co = instancePackageName;
        this.cp = gameplayAclStatus;
        this.cq = achievementTotalCount;
        this.cr = leaderboardCount;
    }

    public static int m1027a(Game game) {
        return C0141w.hashCode(game.getApplicationId(), game.getDisplayName(), game.getPrimaryCategory(), game.getSecondaryCategory(), game.getDescription(), game.getDeveloperName(), game.getIconImageUri(), game.getHiResImageUri(), game.getFeaturedImageUri(), Boolean.valueOf(game.isPlayEnabledGame()), Boolean.valueOf(game.isInstanceInstalled()), game.getInstancePackageName(), Integer.valueOf(game.getGameplayAclStatus()), Integer.valueOf(game.getAchievementTotalCount()), Integer.valueOf(game.getLeaderboardCount()));
    }

    public static boolean m1028a(Game game, Object obj) {
        if (!(obj instanceof Game)) {
            return false;
        }
        if (game == obj) {
            return true;
        }
        Game game2 = (Game) obj;
        return C0141w.m487a(game2.getApplicationId(), game.getApplicationId()) && C0141w.m487a(game2.getDisplayName(), game.getDisplayName()) && C0141w.m487a(game2.getPrimaryCategory(), game.getPrimaryCategory()) && C0141w.m487a(game2.getSecondaryCategory(), game.getSecondaryCategory()) && C0141w.m487a(game2.getDescription(), game.getDescription()) && C0141w.m487a(game2.getDeveloperName(), game.getDeveloperName()) && C0141w.m487a(game2.getIconImageUri(), game.getIconImageUri()) && C0141w.m487a(game2.getHiResImageUri(), game.getHiResImageUri()) && C0141w.m487a(game2.getFeaturedImageUri(), game.getFeaturedImageUri()) && C0141w.m487a(Boolean.valueOf(game2.isPlayEnabledGame()), Boolean.valueOf(game.isPlayEnabledGame())) && C0141w.m487a(Boolean.valueOf(game2.isInstanceInstalled()), Boolean.valueOf(game.isInstanceInstalled())) && C0141w.m487a(game2.getInstancePackageName(), game.getInstancePackageName()) && C0141w.m487a(Integer.valueOf(game2.getGameplayAclStatus()), Integer.valueOf(game.getGameplayAclStatus())) && C0141w.m487a(Integer.valueOf(game2.getAchievementTotalCount()), Integer.valueOf(game.getAchievementTotalCount())) && C0141w.m487a(Integer.valueOf(game2.getLeaderboardCount()), Integer.valueOf(game.getLeaderboardCount()));
    }

    public static String m1029b(Game game) {
        return C0141w.m488c(game).m486a("ApplicationId", game.getApplicationId()).m486a("DisplayName", game.getDisplayName()).m486a("PrimaryCategory", game.getPrimaryCategory()).m486a("SecondaryCategory", game.getSecondaryCategory()).m486a("Description", game.getDescription()).m486a("DeveloperName", game.getDeveloperName()).m486a("IconImageUri", game.getIconImageUri()).m486a("HiResImageUri", game.getHiResImageUri()).m486a("FeaturedImageUri", game.getFeaturedImageUri()).m486a("PlayEnabledGame", Boolean.valueOf(game.isPlayEnabledGame())).m486a("InstanceInstalled", Boolean.valueOf(game.isInstanceInstalled())).m486a("InstancePackageName", game.getInstancePackageName()).m486a("GameplayAclStatus", Integer.valueOf(game.getGameplayAclStatus())).m486a("AchievementTotalCount", Integer.valueOf(game.getAchievementTotalCount())).m486a("LeaderboardCount", Integer.valueOf(game.getLeaderboardCount())).toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return m1028a(this, obj);
    }

    public Game freeze() {
        return this;
    }

    public int getAchievementTotalCount() {
        return this.cq;
    }

    public String getApplicationId() {
        return this.ce;
    }

    public String getDescription() {
        return this.ch;
    }

    public void getDescription(CharArrayBuffer dataOut) {
        ax.m162b(this.ch, dataOut);
    }

    public String getDeveloperName() {
        return this.ci;
    }

    public void getDeveloperName(CharArrayBuffer dataOut) {
        ax.m162b(this.ci, dataOut);
    }

    public String getDisplayName() {
        return this.bm;
    }

    public void getDisplayName(CharArrayBuffer dataOut) {
        ax.m162b(this.bm, dataOut);
    }

    public Uri getFeaturedImageUri() {
        return this.cl;
    }

    public int getGameplayAclStatus() {
        return this.cp;
    }

    public Uri getHiResImageUri() {
        return this.ck;
    }

    public Uri getIconImageUri() {
        return this.cj;
    }

    public String getInstancePackageName() {
        return this.co;
    }

    public int getLeaderboardCount() {
        return this.cr;
    }

    public String getPrimaryCategory() {
        return this.cf;
    }

    public String getSecondaryCategory() {
        return this.cg;
    }

    public int hashCode() {
        return m1027a(this);
    }

    public boolean isInstanceInstalled() {
        return this.cn;
    }

    public boolean isPlayEnabledGame() {
        return this.cm;
    }

    public String toString() {
        return m1029b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        int i = 1;
        String str = null;
        dest.writeString(this.ce);
        dest.writeString(this.bm);
        dest.writeString(this.cf);
        dest.writeString(this.cg);
        dest.writeString(this.ch);
        dest.writeString(this.ci);
        dest.writeString(this.cj == null ? null : this.cj.toString());
        dest.writeString(this.ck == null ? null : this.ck.toString());
        if (this.cl != null) {
            str = this.cl.toString();
        }
        dest.writeString(str);
        dest.writeInt(this.cm ? 1 : 0);
        if (!this.cn) {
            i = 0;
        }
        dest.writeInt(i);
        dest.writeString(this.co);
        dest.writeInt(this.cp);
        dest.writeInt(this.cq);
        dest.writeInt(this.cr);
    }
}
