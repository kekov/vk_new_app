package com.google.android.gms.games;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.C1230k;
import com.google.android.gms.internal.bg;

public final class PlayerBuffer extends DataBuffer<Player> {
    public PlayerBuffer(C1230k dataHolder) {
        super(dataHolder);
    }

    public Player get(int position) {
        return new bg(this.O, position);
    }
}
