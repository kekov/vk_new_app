package com.google.android.gms.games;

import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.internal.C1230k;
import com.google.android.gms.internal.bf;

public final class GameBuffer extends DataBuffer<Game> {
    public GameBuffer(C1230k dataHolder) {
        super(dataHolder);
    }

    public Game get(int position) {
        return new bf(this.O, position);
    }
}
