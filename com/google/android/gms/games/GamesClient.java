package com.google.android.gms.games;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.games.achievement.OnAchievementUpdatedListener;
import com.google.android.gms.games.achievement.OnAchievementsLoadedListener;
import com.google.android.gms.games.leaderboard.LeaderboardScoreBuffer;
import com.google.android.gms.games.leaderboard.OnLeaderboardMetadataLoadedListener;
import com.google.android.gms.games.leaderboard.OnLeaderboardScoresLoadedListener;
import com.google.android.gms.games.leaderboard.OnScoreSubmittedListener;
import com.google.android.gms.games.multiplayer.OnInvitationReceivedListener;
import com.google.android.gms.games.multiplayer.OnInvitationsLoadedListener;
import com.google.android.gms.games.multiplayer.realtime.RealTimeReliableMessageSentListener;
import com.google.android.gms.games.multiplayer.realtime.Room;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.realtime.RoomUpdateListener;
import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.bj;
import java.util.List;

public final class GamesClient implements GooglePlayServicesClient {
    public static final String EXTRA_EXCLUSIVE_BIT_MASK = "exclusive_bit_mask";
    public static final String EXTRA_INVITATION = "invitation";
    public static final String EXTRA_MAX_AUTOMATCH_PLAYERS = "max_automatch_players";
    public static final String EXTRA_MIN_AUTOMATCH_PLAYERS = "min_automatch_players";
    public static final String EXTRA_PLAYERS = "players";
    public static final String EXTRA_ROOM = "room";
    public static final int MAX_RELIABLE_MESSAGE_LEN = 1400;
    public static final int MAX_UNRELIABLE_MESSAGE_LEN = 1168;
    public static final int NOTIFICATION_TYPES_ALL = -1;
    public static final int NOTIFICATION_TYPES_MULTIPLAYER = 1;
    public static final int NOTIFICATION_TYPE_INVITATION = 1;
    public static final int STATUS_ACHIEVEMENT_NOT_INCREMENTAL = 3002;
    public static final int STATUS_ACHIEVEMENT_UNKNOWN = 3001;
    public static final int STATUS_ACHIEVEMENT_UNLOCKED = 3003;
    public static final int STATUS_ACHIEVEMENT_UNLOCK_FAILURE = 3000;
    public static final int STATUS_CLIENT_RECONNECT_REQUIRED = 2;
    public static final int STATUS_INTERNAL_ERROR = 1;
    public static final int STATUS_INVALID_REAL_TIME_ROOM_ID = 7002;
    public static final int STATUS_LICENSE_CHECK_FAILED = 7;
    public static final int STATUS_MULTIPLAYER_ERROR_CREATION_NOT_ALLOWED = 6000;
    public static final int STATUS_MULTIPLAYER_ERROR_NOT_TRUSTED_TESTER = 6001;
    public static final int STATUS_NETWORK_ERROR_NO_DATA = 4;
    public static final int STATUS_NETWORK_ERROR_OPERATION_DEFERRED = 5;
    public static final int STATUS_NETWORK_ERROR_OPERATION_FAILED = 6;
    public static final int STATUS_NETWORK_ERROR_STALE_DATA = 3;
    public static final int STATUS_OK = 0;
    public static final int STATUS_PARTICIPANT_NOT_CONNECTED = 7003;
    public static final int STATUS_REAL_TIME_CONNECTION_FAILED = 7000;
    public static final int STATUS_REAL_TIME_INACTIVE_ROOM = 7005;
    public static final int STATUS_REAL_TIME_MESSAGE_FAILED = -1;
    public static final int STATUS_REAL_TIME_MESSAGE_SEND_FAILED = 7001;
    public static final int STATUS_REAL_TIME_ROOM_NOT_JOINED = 7004;
    private final bj cs;

    public static final class Builder {
        private String ct;
        private int cu;
        private View cv;
        private final ConnectionCallbacks f25d;
        private final OnConnectionFailedListener f26e;
        private String[] f27f;
        private String f28g;
        private final Context mContext;

        public Builder(Context context, ConnectionCallbacks connectedListener, OnConnectionFailedListener connectionFailedListener) {
            this.f28g = "<<default account>>";
            String[] strArr = new String[GamesClient.STATUS_INTERNAL_ERROR];
            strArr[GamesClient.STATUS_OK] = Scopes.GAMES;
            this.f27f = strArr;
            this.cu = 49;
            this.mContext = context;
            this.ct = context.getPackageName();
            this.f25d = connectedListener;
            this.f26e = connectionFailedListener;
        }

        public GamesClient create() {
            return new GamesClient(this.ct, this.f28g, this.f25d, this.f26e, this.f27f, this.cu, this.cv, null);
        }

        public Builder setAccountName(String accountName) {
            this.f28g = (String) C0142x.m495d(accountName);
            return this;
        }

        public Builder setGravityForPopups(int gravity) {
            this.cu = gravity;
            return this;
        }

        public Builder setScopes(String... scopes) {
            this.f27f = scopes;
            return this;
        }

        public Builder setViewForPopups(View gamesContentView) {
            this.cv = (View) C0142x.m495d(gamesContentView);
            return this;
        }
    }

    private GamesClient(Context context, String gamePackageName, String accountName, ConnectionCallbacks connectedListener, OnConnectionFailedListener connectionFailedListener, String[] scopes, int gravity, View gamesContentView) {
        this.cs = new bj(context, gamePackageName, accountName, connectedListener, connectionFailedListener, scopes, gravity, gamesContentView, false);
    }

    public void clearAllNotifications() {
        this.cs.clearNotifications(STATUS_REAL_TIME_MESSAGE_FAILED);
    }

    public void clearNotifications(int notificationTypes) {
        this.cs.clearNotifications(notificationTypes);
    }

    public void connect() {
        this.cs.connect();
    }

    public void createRoom(RoomConfig config) {
        this.cs.createRoom(config);
    }

    public void declineRoomInvitation(String invitationId) {
        this.cs.m1121i(invitationId, STATUS_OK);
    }

    public void disconnect() {
        this.cs.disconnect();
    }

    public void dismissRoomInvitation(String invitationId) {
        this.cs.m1120h(invitationId, STATUS_OK);
    }

    public Intent getAchievementsIntent() {
        return this.cs.getAchievementsIntent();
    }

    public Intent getAllLeaderboardsIntent() {
        return this.cs.getAllLeaderboardsIntent();
    }

    public String getAppId() {
        return this.cs.getAppId();
    }

    public String getCurrentAccountName() {
        return this.cs.getCurrentAccountName();
    }

    public Player getCurrentPlayer() {
        return this.cs.getCurrentPlayer();
    }

    public String getCurrentPlayerId() {
        return this.cs.getCurrentPlayerId();
    }

    public Intent getInvitationInboxIntent() {
        return this.cs.getInvitationInboxIntent();
    }

    public Intent getLeaderboardIntent(String leaderboardId) {
        return this.cs.getLeaderboardIntent(leaderboardId);
    }

    public RealTimeSocket getRealTimeSocketForParticipant(String roomId, String participantId) {
        return this.cs.getRealTimeSocketForParticipant(roomId, participantId);
    }

    public Intent getRealTimeWaitingRoomIntent(Room room, int minParticipantsToStart) {
        return this.cs.getRealTimeWaitingRoomIntent(room, minParticipantsToStart);
    }

    public Intent getSelectPlayersIntent(int minPlayers, int maxPlayers) {
        return this.cs.getSelectPlayersIntent(minPlayers, maxPlayers);
    }

    public Intent getSettingsIntent() {
        return this.cs.getSettingsIntent();
    }

    public void incrementAchievement(String id, int numSteps) {
        this.cs.m1112a(null, id, numSteps);
    }

    public void incrementAchievementImmediate(OnAchievementUpdatedListener listener, String id, int numSteps) {
        this.cs.m1112a(listener, id, numSteps);
    }

    public boolean isConnected() {
        return this.cs.isConnected();
    }

    public boolean isConnecting() {
        return this.cs.isConnecting();
    }

    public boolean isConnectionCallbacksRegistered(ConnectionCallbacks listener) {
        return this.cs.isConnectionCallbacksRegistered(listener);
    }

    public boolean isConnectionFailedListenerRegistered(OnConnectionFailedListener listener) {
        return this.cs.isConnectionFailedListenerRegistered(listener);
    }

    public void joinRoom(RoomConfig config) {
        this.cs.joinRoom(config);
    }

    public void leaveRoom(RoomUpdateListener listener, String roomId) {
        this.cs.leaveRoom(listener, roomId);
    }

    public void loadAchievements(OnAchievementsLoadedListener listener) {
        this.cs.loadAchievements(listener);
    }

    public void loadGame(OnGamesLoadedListener listener) {
        this.cs.loadGame(listener);
    }

    public void loadInvitablePlayers(OnPlayersLoadedListener listener, int pageSize, boolean forceReload) {
        this.cs.m1110a(listener, pageSize, false, forceReload);
    }

    public void loadInvitations(OnInvitationsLoadedListener listener) {
        this.cs.loadInvitations(listener);
    }

    public void loadLeaderboardMetadata(OnLeaderboardMetadataLoadedListener listener) {
        this.cs.loadLeaderboardMetadata(listener);
    }

    public void loadLeaderboardMetadata(OnLeaderboardMetadataLoadedListener listener, String leaderboardId) {
        this.cs.loadLeaderboardMetadata(listener, leaderboardId);
    }

    public void loadMoreInvitablePlayers(OnPlayersLoadedListener listener, int pageSize) {
        this.cs.m1110a(listener, pageSize, true, false);
    }

    public void loadMoreScores(OnLeaderboardScoresLoadedListener listener, LeaderboardScoreBuffer buffer, int maxResults, int pageDirection) {
        this.cs.loadMoreScores(listener, buffer, maxResults, pageDirection);
    }

    public void loadPlayer(OnPlayersLoadedListener listener, String playerId) {
        this.cs.loadPlayer(listener, playerId);
    }

    public void loadPlayerCenteredScores(OnLeaderboardScoresLoadedListener listener, String leaderboardId, int span, int leaderboardCollection, int maxResults) {
        this.cs.loadPlayerCenteredScores(listener, leaderboardId, span, leaderboardCollection, maxResults, false);
    }

    public void loadPlayerCenteredScores(OnLeaderboardScoresLoadedListener listener, String leaderboardId, int span, int leaderboardCollection, int maxResults, boolean forceReload) {
        this.cs.loadPlayerCenteredScores(listener, leaderboardId, span, leaderboardCollection, maxResults, forceReload);
    }

    public void loadTopScores(OnLeaderboardScoresLoadedListener listener, String leaderboardId, int span, int leaderboardCollection, int maxResults) {
        this.cs.loadTopScores(listener, leaderboardId, span, leaderboardCollection, maxResults, false);
    }

    public void loadTopScores(OnLeaderboardScoresLoadedListener listener, String leaderboardId, int span, int leaderboardCollection, int maxResults, boolean forceReload) {
        this.cs.loadTopScores(listener, leaderboardId, span, leaderboardCollection, maxResults, forceReload);
    }

    public void reconnect() {
        this.cs.disconnect();
        this.cs.connect();
    }

    public void registerConnectionCallbacks(ConnectionCallbacks listener) {
        this.cs.registerConnectionCallbacks(listener);
    }

    public void registerConnectionFailedListener(OnConnectionFailedListener listener) {
        this.cs.registerConnectionFailedListener(listener);
    }

    public void registerInvitationListener(OnInvitationReceivedListener listener) {
        this.cs.registerInvitationListener(listener);
    }

    public void revealAchievement(String id) {
        this.cs.m1111a(null, id);
    }

    public void revealAchievementImmediate(OnAchievementUpdatedListener listener, String id) {
        this.cs.m1111a(listener, id);
    }

    public int sendReliableRealTimeMessage(RealTimeReliableMessageSentListener listener, byte[] messageData, String roomId, String recipientParticipantId) {
        return this.cs.sendReliableRealTimeMessage(listener, messageData, roomId, recipientParticipantId);
    }

    public int sendUnreliableRealTimeMessage(byte[] messageData, String roomId, String recipientParticipantId) {
        bj bjVar = this.cs;
        String[] strArr = new String[STATUS_INTERNAL_ERROR];
        strArr[STATUS_OK] = recipientParticipantId;
        return bjVar.m1107a(messageData, roomId, strArr);
    }

    public int sendUnreliableRealTimeMessage(byte[] messageData, String roomId, List<String> recipientParticipantIds) {
        return this.cs.m1107a(messageData, roomId, (String[]) recipientParticipantIds.toArray(new String[recipientParticipantIds.size()]));
    }

    public int sendUnreliableRealTimeMessageToAll(byte[] messageData, String roomId) {
        return this.cs.sendUnreliableRealTimeMessageToAll(messageData, roomId);
    }

    public void setGravityForPopups(int gravity) {
        this.cs.setGravityForPopups(gravity);
    }

    public void setUseNewPlayerNotificationsFirstParty(boolean newPlayerStyle) {
        this.cs.setUseNewPlayerNotificationsFirstParty(newPlayerStyle);
    }

    public void setViewForPopups(View gamesContentView) {
        C0142x.m495d(gamesContentView);
        this.cs.setViewForPopups(gamesContentView);
    }

    public void signOut() {
        this.cs.signOut(null);
    }

    public void signOut(OnSignOutCompleteListener listener) {
        this.cs.signOut(listener);
    }

    public void submitScore(String leaderboardId, long score) {
        this.cs.m1113a(null, leaderboardId, score);
    }

    public void submitScoreImmediate(OnScoreSubmittedListener listener, String leaderboardId, long score) {
        this.cs.m1113a(listener, leaderboardId, score);
    }

    public void unlockAchievement(String id) {
        this.cs.m1117b(null, id);
    }

    public void unlockAchievementImmediate(OnAchievementUpdatedListener listener, String id) {
        this.cs.m1117b(listener, id);
    }

    public void unregisterConnectionCallbacks(ConnectionCallbacks listener) {
        this.cs.unregisterConnectionCallbacks(listener);
    }

    public void unregisterConnectionFailedListener(OnConnectionFailedListener listener) {
        this.cs.unregisterConnectionFailedListener(listener);
    }

    public void unregisterInvitationListener() {
        this.cs.unregisterInvitationListener();
    }
}
