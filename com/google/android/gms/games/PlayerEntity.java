package com.google.android.gms.games;

import android.database.CharArrayBuffer;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.internal.C0126n;
import com.google.android.gms.internal.C0141w;
import com.google.android.gms.internal.ax;

public final class PlayerEntity implements Player {
    public static final Creator<PlayerEntity> CREATOR;
    private final String bm;
    private final Uri cj;
    private final Uri ck;
    private final String cw;
    private final long cx;

    /* renamed from: com.google.android.gms.games.PlayerEntity.1 */
    static class C00991 implements Creator<PlayerEntity> {
        C00991() {
        }

        public /* synthetic */ Object createFromParcel(Parcel x0) {
            return m29o(x0);
        }

        public /* synthetic */ Object[] newArray(int x0) {
            return m30w(x0);
        }

        public PlayerEntity m29o(Parcel parcel) {
            String readString = parcel.readString();
            String readString2 = parcel.readString();
            String readString3 = parcel.readString();
            String readString4 = parcel.readString();
            return new PlayerEntity(readString2, readString3 == null ? null : Uri.parse(readString3), readString4 == null ? null : Uri.parse(readString4), parcel.readLong(), null);
        }

        public PlayerEntity[] m30w(int i) {
            return new PlayerEntity[i];
        }
    }

    static {
        CREATOR = new C00991();
    }

    public PlayerEntity(Player player) {
        this.cw = player.getPlayerId();
        this.bm = player.getDisplayName();
        this.cj = player.getIconImageUri();
        this.ck = player.getHiResImageUri();
        this.cx = player.getRetrievedTimestamp();
        C0126n.m444b(this.cw);
        C0126n.m444b(this.bm);
        C0126n.m442a(this.cx > 0);
    }

    private PlayerEntity(String playerId, String displayName, Uri iconImageUri, Uri hiResImageUri, long retrievedTimestamp) {
        this.cw = playerId;
        this.bm = displayName;
        this.cj = iconImageUri;
        this.ck = hiResImageUri;
        this.cx = retrievedTimestamp;
    }

    public static int m1030a(Player player) {
        return C0141w.hashCode(player.getPlayerId(), player.getDisplayName(), player.getIconImageUri(), player.getHiResImageUri(), Long.valueOf(player.getRetrievedTimestamp()));
    }

    public static boolean m1031a(Player player, Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        }
        if (player == obj) {
            return true;
        }
        Player player2 = (Player) obj;
        return C0141w.m487a(player2.getPlayerId(), player.getPlayerId()) && C0141w.m487a(player2.getDisplayName(), player.getDisplayName()) && C0141w.m487a(player2.getIconImageUri(), player.getIconImageUri()) && C0141w.m487a(player2.getHiResImageUri(), player.getHiResImageUri()) && C0141w.m487a(Long.valueOf(player2.getRetrievedTimestamp()), Long.valueOf(player.getRetrievedTimestamp()));
    }

    public static String m1032b(Player player) {
        return C0141w.m488c(player).m486a("PlayerId", player.getPlayerId()).m486a("DisplayName", player.getDisplayName()).m486a("IconImageUri", player.getIconImageUri()).m486a("HiResImageUri", player.getHiResImageUri()).m486a("RetrievedTimestamp", Long.valueOf(player.getRetrievedTimestamp())).toString();
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return m1031a(this, obj);
    }

    public Player freeze() {
        return this;
    }

    public String getDisplayName() {
        return this.bm;
    }

    public void getDisplayName(CharArrayBuffer dataOut) {
        ax.m162b(this.bm, dataOut);
    }

    public Uri getHiResImageUri() {
        return this.ck;
    }

    public Uri getIconImageUri() {
        return this.cj;
    }

    public String getPlayerId() {
        return this.cw;
    }

    public long getRetrievedTimestamp() {
        return this.cx;
    }

    public boolean hasHiResImage() {
        return getHiResImageUri() != null;
    }

    public boolean hasIconImage() {
        return getIconImageUri() != null;
    }

    public int hashCode() {
        return m1030a(this);
    }

    public String toString() {
        return m1032b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        String str = null;
        dest.writeString(this.cw);
        dest.writeString(this.bm);
        dest.writeString(this.cj == null ? null : this.cj.toString());
        if (this.ck != null) {
            str = this.ck.toString();
        }
        dest.writeString(str);
        dest.writeLong(this.cx);
    }
}
