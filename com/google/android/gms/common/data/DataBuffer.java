package com.google.android.gms.common.data;

import com.google.android.gms.internal.C0122i;
import com.google.android.gms.internal.C1230k;
import java.util.Iterator;

public abstract class DataBuffer<T> implements Iterable<T> {
    protected final C1230k f24O;

    public DataBuffer(C1230k dataHolder) {
        this.f24O = dataHolder;
    }

    public void close() {
        this.f24O.close();
    }

    public int describeContents() {
        return 0;
    }

    public abstract T get(int i);

    public int getCount() {
        return this.f24O.getCount();
    }

    public boolean isClosed() {
        return this.f24O.isClosed();
    }

    public Iterator<T> iterator() {
        return new C0122i(this);
    }
}
