package com.google.android.gms.common.images;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.ImageView;
import com.google.android.gms.internal.af;
import com.google.android.gms.internal.ba;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class ImageManager {
    private static ImageManager ai;
    private final af<Uri, WeakReference<ConstantState>> aj;
    private final Map<ListenerHolder, ImageReceiver> ak;
    private final Map<Uri, ImageReceiver> al;
    private final Context mContext;

    public final class ImageReceiver extends ResultReceiver {
        final /* synthetic */ ImageManager an;
        private final ArrayList<ListenerHolder> ao;
        private final Uri mUri;

        ImageReceiver(ImageManager imageManager, Uri uri) {
            this.an = imageManager;
            super(new Handler(Looper.getMainLooper()));
            this.mUri = uri;
            this.ao = new ArrayList();
        }

        public void addOnImageLoadedListenerHolder(ListenerHolder imageViewLoadListener) {
            this.ao.add(imageViewLoadListener);
        }

        public Uri getUri() {
            return this.mUri;
        }

        public void onReceiveResult(int resultCode, Bundle resultData) {
            Drawable drawable = null;
            ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor) resultData.getParcelable("com.google.android.gms.extra.fileDescriptor");
            if (parcelFileDescriptor != null) {
                Bitmap decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
                try {
                    parcelFileDescriptor.close();
                } catch (Throwable e) {
                    Log.e("ImageManager", "closed failed", e);
                }
                Drawable bitmapDrawable = new BitmapDrawable(this.an.mContext.getResources(), decodeFileDescriptor);
                this.an.aj.put(this.mUri, new WeakReference(bitmapDrawable.getConstantState()));
                drawable = bitmapDrawable;
            }
            this.an.al.remove(this.mUri);
            int size = this.ao.size();
            for (int i = 0; i < size; i++) {
                ((ListenerHolder) this.ao.get(i)).onImageLoaded(this.mUri, drawable);
            }
        }

        public void removeOnImageLoadedListenerHolder(ListenerHolder imageViewLoadListener) {
            this.ao.remove(imageViewLoadListener);
        }
    }

    public interface OnImageLoadedListener {
        void onImageLoaded(Uri uri, Drawable drawable);
    }

    /* renamed from: com.google.android.gms.common.images.ImageManager.c */
    static final class C0096c implements ComponentCallbacks2 {
        private final af<Uri, WeakReference<ConstantState>> aq;

        public C0096c(af<Uri, WeakReference<ConstantState>> afVar) {
            this.aq = afVar;
        }

        public void onConfigurationChanged(Configuration newConfig) {
        }

        public void onLowMemory() {
            this.aq.evictAll();
        }

        public void onTrimMemory(int level) {
            if (level >= 60) {
                this.aq.evictAll();
            } else if (level >= 40) {
                this.aq.trimToSize(this.aq.size() / 2);
            }
        }
    }

    abstract class ListenerHolder implements OnImageLoadedListener {
        final /* synthetic */ ImageManager an;
        protected final int mDefaultResId;
        protected final int mHashCode;

        private ListenerHolder(ImageManager imageManager, int hashCode, int defaultResId) {
            this.an = imageManager;
            this.mHashCode = hashCode;
            this.mDefaultResId = defaultResId;
        }

        public abstract void handleCachedDrawable(Uri uri, Drawable drawable);

        public int hashCode() {
            return this.mHashCode;
        }

        public abstract void onImageLoaded(Uri uri, Drawable drawable);

        public abstract boolean shouldLoadImage(Uri uri);
    }

    /* renamed from: com.google.android.gms.common.images.ImageManager.a */
    final class C1575a extends ListenerHolder {
        private final WeakReference<OnImageLoadedListener> am;
        final /* synthetic */ ImageManager an;

        private C1575a(ImageManager imageManager, OnImageLoadedListener onImageLoadedListener, int i) {
            this.an = imageManager;
            super(onImageLoadedListener.hashCode(), i, null);
            this.am = new WeakReference(onImageLoadedListener);
        }

        public boolean equals(Object o) {
            if (!(o instanceof C1575a)) {
                return false;
            }
            C1575a c1575a = (C1575a) o;
            return (this.am == null || c1575a.am == null || this.mHashCode != c1575a.mHashCode) ? false : true;
        }

        public void handleCachedDrawable(Uri uri, Drawable drawable) {
            OnImageLoadedListener onImageLoadedListener = (OnImageLoadedListener) this.am.get();
            if (onImageLoadedListener != null) {
                onImageLoadedListener.onImageLoaded(uri, drawable);
            }
        }

        public void onImageLoaded(Uri uri, Drawable drawable) {
            ImageReceiver imageReceiver = (ImageReceiver) this.an.ak.remove(this);
            OnImageLoadedListener onImageLoadedListener = (OnImageLoadedListener) this.am.get();
            if (onImageLoadedListener != null) {
                onImageLoadedListener.onImageLoaded(uri, drawable);
            }
        }

        public boolean shouldLoadImage(Uri uri) {
            if (uri != null) {
                return true;
            }
            OnImageLoadedListener onImageLoadedListener = (OnImageLoadedListener) this.am.get();
            if (onImageLoadedListener != null) {
                if (this.mDefaultResId == 0) {
                    onImageLoadedListener.onImageLoaded(uri, null);
                } else {
                    onImageLoadedListener.onImageLoaded(uri, this.an.mContext.getResources().getDrawable(this.mDefaultResId));
                }
            }
            return false;
        }
    }

    /* renamed from: com.google.android.gms.common.images.ImageManager.b */
    final class C1576b extends ListenerHolder {
        final /* synthetic */ ImageManager an;
        private final WeakReference<ImageView> ap;

        private C1576b(ImageManager imageManager, ImageView imageView, int i) {
            this.an = imageManager;
            super(imageView.hashCode(), i, null);
            this.ap = new WeakReference(imageView);
        }

        public boolean equals(Object o) {
            if (!(o instanceof C1576b)) {
                return false;
            }
            C1576b c1576b = (C1576b) o;
            return (this.ap == null || c1576b.ap == null || this.mHashCode != c1576b.mHashCode) ? false : true;
        }

        public void handleCachedDrawable(Uri uri, Drawable drawable) {
            ImageView imageView = (ImageView) this.ap.get();
            if (imageView != null) {
                imageView.setImageDrawable(drawable);
            }
            ImageReceiver imageReceiver = (ImageReceiver) this.an.ak.remove(this);
            if (imageReceiver != null) {
                imageReceiver.removeOnImageLoadedListenerHolder(this);
            }
        }

        public void onImageLoaded(Uri uri, Drawable drawable) {
            ImageReceiver imageReceiver = (ImageReceiver) this.an.ak.remove(this);
            ImageView imageView = (ImageView) this.ap.get();
            if (imageView != null && imageReceiver != null && imageReceiver.getUri().equals(uri)) {
                imageView.setImageDrawable(drawable);
            }
        }

        public boolean shouldLoadImage(Uri uri) {
            ImageView imageView = (ImageView) this.ap.get();
            if (imageView != null) {
                if (this.mDefaultResId == 0) {
                    imageView.setImageDrawable(null);
                } else {
                    imageView.setImageResource(this.mDefaultResId);
                }
            }
            if (uri != null) {
                return true;
            }
            ImageReceiver imageReceiver = (ImageReceiver) this.an.ak.remove(this);
            if (imageReceiver != null) {
                imageReceiver.removeOnImageLoadedListenerHolder(this);
            }
            return false;
        }
    }

    private ImageManager(Context context) {
        this.mContext = context.getApplicationContext();
        this.aj = new af(50);
        if (ba.ad()) {
            this.mContext.registerComponentCallbacks(new C0096c(this.aj));
        }
        this.ak = new HashMap();
        this.al = new HashMap();
    }

    private void m23a(ListenerHolder listenerHolder, Uri uri) {
        if (uri != null) {
            WeakReference weakReference = (WeakReference) this.aj.get(uri);
            if (weakReference != null) {
                ConstantState constantState = (ConstantState) weakReference.get();
                if (constantState != null) {
                    listenerHolder.handleCachedDrawable(uri, constantState.newDrawable());
                    return;
                }
            }
        }
        if (listenerHolder.shouldLoadImage(uri)) {
            Parcelable parcelable = (ImageReceiver) this.al.get(uri);
            if (parcelable == null) {
                parcelable = new ImageReceiver(this, uri);
                this.al.put(uri, parcelable);
            }
            parcelable.addOnImageLoadedListenerHolder(listenerHolder);
            this.ak.put(listenerHolder, parcelable);
            Intent intent = new Intent("com.google.android.gms.common.images.LOAD_IMAGE");
            intent.putExtra("com.google.android.gms.extras.uri", uri);
            intent.putExtra("com.google.android.gms.extras.resultReceiver", parcelable);
            intent.putExtra("com.google.android.gms.extras.priority", 3);
            this.mContext.sendBroadcast(intent);
        }
    }

    public static ImageManager create(Context context) {
        if (ai == null) {
            ai = new ImageManager(context);
        }
        return ai;
    }

    public void loadImage(ImageView imageView, int resId) {
        loadImage(imageView, null, resId);
    }

    public void loadImage(ImageView imageView, Uri uri) {
        loadImage(imageView, uri, 0);
    }

    public void loadImage(ImageView imageView, Uri uri, int defaultResId) {
        m23a(new C1576b(imageView, defaultResId, null), uri);
    }

    public void loadImage(OnImageLoadedListener listener, Uri uri) {
        loadImage(listener, uri, 0);
    }

    public void loadImage(OnImageLoadedListener listener, Uri uri, int defaultResId) {
        m23a(new C1575a(listener, defaultResId, null), uri);
    }
}
