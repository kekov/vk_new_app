package com.google.android.gms.common;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import com.google.android.gms.internal.C0142x;
import com.google.android.gms.internal.C0145z;
import com.google.android.gms.internal.C0145z.C0144a;
import com.google.android.gms.internal.aa;

public final class SignInButton extends FrameLayout implements OnClickListener {
    public static final int COLOR_DARK = 0;
    public static final int COLOR_LIGHT = 1;
    public static final int SIZE_ICON_ONLY = 2;
    public static final int SIZE_STANDARD = 0;
    public static final int SIZE_WIDE = 1;
    private int f20K;
    private int f21L;
    private View f22M;
    private OnClickListener f23N;

    public SignInButton(Context context) {
        this(context, null);
    }

    public SignInButton(Context context, AttributeSet attrs) {
        this(context, attrs, SIZE_STANDARD);
    }

    public SignInButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.f23N = null;
        setStyle(SIZE_STANDARD, SIZE_STANDARD);
    }

    private static Button m20c(Context context, int i, int i2) {
        Button aaVar = new aa(context);
        aaVar.m49a(context.getResources(), i, i2);
        return aaVar;
    }

    private void m21d(Context context) {
        if (this.f22M != null) {
            removeView(this.f22M);
        }
        try {
            this.f22M = C0145z.m497d(context, this.f20K, this.f21L);
        } catch (C0144a e) {
            Log.w("SignInButton", "Sign in button not found, using placeholder instead");
            this.f22M = m20c(context, this.f20K, this.f21L);
        }
        addView(this.f22M);
        this.f22M.setEnabled(isEnabled());
        this.f22M.setOnClickListener(this);
    }

    public void onClick(View view) {
        if (this.f23N != null && view == this.f22M) {
            this.f23N.onClick(this);
        }
    }

    public void setColorScheme(int colorScheme) {
        setStyle(this.f20K, colorScheme);
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        this.f22M.setEnabled(enabled);
    }

    public void setOnClickListener(OnClickListener listener) {
        this.f23N = listener;
        if (this.f22M != null) {
            this.f22M.setOnClickListener(this);
        }
    }

    public void setSize(int buttonSize) {
        setStyle(buttonSize, this.f21L);
    }

    public void setStyle(int buttonSize, int colorScheme) {
        boolean z = true;
        boolean z2 = buttonSize >= 0 && buttonSize < 3;
        C0142x.m490a(z2, "Unknown button size " + buttonSize);
        if (colorScheme < 0 || colorScheme >= SIZE_ICON_ONLY) {
            z = false;
        }
        C0142x.m490a(z, "Unknown color scheme " + colorScheme);
        this.f20K = buttonSize;
        this.f21L = colorScheme;
        m21d(getContext());
    }
}
