package com.actionbarsherlock.internal.widget;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

public class IcsColorDrawable extends Drawable {
    private int color;
    private final Paint paint;

    public IcsColorDrawable(ColorDrawable drawable) {
        this.paint = new Paint();
        Bitmap bitmap = Bitmap.createBitmap(1, 1, Config.ARGB_8888);
        drawable.draw(new Canvas(bitmap));
        this.color = bitmap.getPixel(0, 0);
        bitmap.recycle();
    }

    public IcsColorDrawable(int color) {
        this.paint = new Paint();
        this.color = color;
    }

    public void draw(Canvas canvas) {
        if ((this.color >>> 24) != 0) {
            this.paint.setColor(this.color);
            canvas.drawRect(getBounds(), this.paint);
        }
    }

    public void setAlpha(int alpha) {
        if (alpha != (this.color >>> 24)) {
            this.color = (this.color & 16777215) | (alpha << 24);
            invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public int getOpacity() {
        return this.color >>> 24;
    }
}
