package com.actionbarsherlock.internal.widget;

import android.view.View;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;

final class IcsView {
    private IcsView() {
    }

    public static int getMeasuredStateInt(View child) {
        return (child.getMeasuredWidth() & Color.WINDOW_BACKGROUND_COLOR) | ((child.getMeasuredHeight() >> 16) & -256);
    }
}
