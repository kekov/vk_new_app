package com.jakewharton;

import java.nio.charset.Charset;

class Charsets {
    static final Charset US_ASCII;
    static final Charset UTF_8;

    Charsets() {
    }

    static {
        US_ASCII = Charset.forName("US-ASCII");
        UTF_8 = Charset.forName("UTF-8");
    }
}
