package com.facebook;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.google.android.gcm.GCMConstants;
import com.vkontakte.android.C0436R;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.concurrent.CancellationException;
import org.acra.ACRAConstants;

public class WebDialog extends Dialog {
    private static final int BACKGROUND_GRAY = -872415232;
    static final String CANCEL_URI = "fbconnect://cancel";
    public static final int DEFAULT_THEME = 16973840;
    private static final String DIALOG_AUTHORITY_FORMAT = "m.%s";
    public static final String DIALOG_PARAM_ACCESS_TOKEN = "access_token";
    public static final String DIALOG_PARAM_APP_ID = "app_id";
    public static final String DIALOG_PARAM_CLIENT_ID = "client_id";
    public static final String DIALOG_PARAM_DISPLAY = "display";
    public static final String DIALOG_PARAM_E2E = "e2e";
    public static final String DIALOG_PARAM_REDIRECT_URI = "redirect_uri";
    public static final String DIALOG_PARAM_SCOPE = "scope";
    public static final String DIALOG_PARAM_TYPE = "type";
    public static final String DIALOG_PATH = "dialog/";
    static final boolean DISABLE_SSL_CHECK_FOR_TESTING = false;
    private static final String DISPLAY_TOUCH = "touch";
    private static final int MAX_BUFFER_SCREEN_WIDTH = 1024;
    private static final double MIN_SCALE_FACTOR = 0.6d;
    private static final int NO_BUFFER_SCREEN_WIDTH = 512;
    static final String REDIRECT_URI = "fbconnect://success";
    private static final String USER_AGENT = "user_agent";
    private FrameLayout contentFrameLayout;
    private ImageView crossImageView;
    private boolean isDetached;
    private boolean listenerCalled;
    private OnCompleteListener onCompleteListener;
    private ProgressDialog spinner;
    private String url;
    private WebView webView;

    /* renamed from: com.facebook.WebDialog.1 */
    class C00801 implements OnCancelListener {
        C00801() {
        }

        public void onCancel(DialogInterface dialogInterface) {
            WebDialog.this.sendCancelToListener();
        }
    }

    /* renamed from: com.facebook.WebDialog.2 */
    class C00812 implements OnCancelListener {
        C00812() {
        }

        public void onCancel(DialogInterface dialogInterface) {
            WebDialog.this.sendCancelToListener();
            WebDialog.this.dismiss();
        }
    }

    /* renamed from: com.facebook.WebDialog.3 */
    class C00823 implements OnClickListener {
        C00823() {
        }

        public void onClick(View v) {
            WebDialog.this.sendCancelToListener();
            WebDialog.this.dismiss();
        }
    }

    private static class BuilderBase<CONCRETE extends BuilderBase<?>> {
        private String action;
        private String applicationId;
        private Context context;
        private OnCompleteListener listener;
        private Bundle parameters;
        private int theme;

        protected BuilderBase(Context context, String applicationId, String action, Bundle parameters) {
            this.theme = WebDialog.DEFAULT_THEME;
            this.applicationId = applicationId;
            finishInit(context, action, parameters);
        }

        public CONCRETE setTheme(int theme) {
            this.theme = theme;
            return this;
        }

        public CONCRETE setOnCompleteListener(OnCompleteListener listener) {
            this.listener = listener;
            return this;
        }

        public WebDialog build() {
            this.parameters.putString(WebDialog.DIALOG_PARAM_APP_ID, this.applicationId);
            if (!this.parameters.containsKey(WebDialog.DIALOG_PARAM_REDIRECT_URI)) {
                this.parameters.putString(WebDialog.DIALOG_PARAM_REDIRECT_URI, WebDialog.REDIRECT_URI);
            }
            return new WebDialog(this.context, this.action, this.parameters, this.theme, this.listener);
        }

        protected String getApplicationId() {
            return this.applicationId;
        }

        protected Context getContext() {
            return this.context;
        }

        protected int getTheme() {
            return this.theme;
        }

        protected Bundle getParameters() {
            return this.parameters;
        }

        protected OnCompleteListener getListener() {
            return this.listener;
        }

        private void finishInit(Context context, String action, Bundle parameters) {
            this.context = context;
            this.action = action;
            if (parameters != null) {
                this.parameters = parameters;
            } else {
                this.parameters = new Bundle();
            }
        }
    }

    private class DialogWebViewClient extends WebViewClient {
        private DialogWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith(WebDialog.REDIRECT_URI)) {
                Bundle values = WebDialog.parseUrl(url);
                String error = values.getString(GCMConstants.EXTRA_ERROR);
                if (error == null) {
                    error = values.getString("error_type");
                }
                String errorMessage = values.getString("error_msg");
                if (errorMessage == null) {
                    errorMessage = values.getString("error_description");
                }
                String errorCodeString = values.getString("error_code");
                int errorCode = -1;
                if (errorCodeString != null && errorCodeString.length() > 0) {
                    try {
                        errorCode = Integer.parseInt(errorCodeString);
                    } catch (NumberFormatException e) {
                        errorCode = -1;
                    }
                }
                if ((error == null || error.length() == 0) && ((errorMessage == null || errorMessage.length() == 0) && errorCode == -1)) {
                    WebDialog.this.sendSuccessToListener(values);
                } else if (error == null || !(error.equals("access_denied") || error.equals("OAuthAccessDeniedException"))) {
                    WebDialog.this.sendErrorToListener(new FacebookServiceException(new FacebookRequestError(errorCode, error, errorMessage), errorMessage));
                } else {
                    WebDialog.this.sendCancelToListener();
                }
                WebDialog.this.dismiss();
                return true;
            } else if (url.startsWith(WebDialog.CANCEL_URI)) {
                WebDialog.this.sendCancelToListener();
                WebDialog.this.dismiss();
                return true;
            } else if (url.contains(WebDialog.DISPLAY_TOUCH)) {
                return WebDialog.DISABLE_SSL_CHECK_FOR_TESTING;
            } else {
                WebDialog.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                return true;
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            WebDialog.this.sendErrorToListener(new FacebookDialogException(description, errorCode, failingUrl));
            WebDialog.this.dismiss();
        }

        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            WebDialog.this.sendErrorToListener(new FacebookDialogException(null, -11, null));
            handler.cancel();
            WebDialog.this.dismiss();
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if (!WebDialog.this.isDetached) {
                WebDialog.this.spinner.show();
            }
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (!WebDialog.this.isDetached) {
                WebDialog.this.spinner.dismiss();
            }
            WebDialog.this.contentFrameLayout.setBackgroundColor(0);
            WebDialog.this.webView.setVisibility(0);
            WebDialog.this.crossImageView.setVisibility(0);
        }
    }

    public interface OnCompleteListener {
        void onComplete(Bundle bundle, RuntimeException runtimeException);
    }

    public static class Builder extends BuilderBase<Builder> {
        public /* bridge */ /* synthetic */ WebDialog build() {
            return super.build();
        }

        public /* bridge */ /* synthetic */ BuilderBase setOnCompleteListener(OnCompleteListener onCompleteListener) {
            return super.setOnCompleteListener(onCompleteListener);
        }

        public /* bridge */ /* synthetic */ BuilderBase setTheme(int i) {
            return super.setTheme(i);
        }

        public Builder(Context context, String applicationId, String action, Bundle parameters) {
            super(context, applicationId, action, parameters);
        }
    }

    public WebDialog(Context context, String url) {
        this(context, url, DEFAULT_THEME);
    }

    public WebDialog(Context context, String url, int theme) {
        super(context, theme);
        this.listenerCalled = DISABLE_SSL_CHECK_FOR_TESTING;
        this.isDetached = DISABLE_SSL_CHECK_FOR_TESTING;
        this.url = url;
    }

    public WebDialog(Context context, String action, Bundle parameters, int theme, OnCompleteListener listener) {
        super(context, theme);
        this.listenerCalled = DISABLE_SSL_CHECK_FOR_TESTING;
        this.isDetached = DISABLE_SSL_CHECK_FOR_TESTING;
        if (parameters == null) {
            parameters = new Bundle();
        }
        parameters.putString(DIALOG_PARAM_DISPLAY, DISPLAY_TOUCH);
        parameters.putString(DIALOG_PARAM_TYPE, USER_AGENT);
        this.url = buildUri(String.format(DIALOG_AUTHORITY_FORMAT, new Object[]{"facebook.com"}), new StringBuilder(DIALOG_PATH).append(action).toString(), parameters).toString();
        this.onCompleteListener = listener;
    }

    public static Uri buildUri(String authority, String path, Bundle parameters) {
        android.net.Uri.Builder builder = new android.net.Uri.Builder();
        builder.scheme("https");
        builder.authority(authority);
        builder.path(path);
        for (String key : parameters.keySet()) {
            Object parameter = parameters.get(key);
            if (parameter instanceof String) {
                builder.appendQueryParameter(key, (String) parameter);
            }
        }
        return builder.build();
    }

    public void setOnCompleteListener(OnCompleteListener listener) {
        this.onCompleteListener = listener;
    }

    public OnCompleteListener getOnCompleteListener() {
        return this.onCompleteListener;
    }

    public void dismiss() {
        if (this.webView != null) {
            this.webView.stopLoading();
        }
        if (!this.isDetached) {
            if (this.spinner.isShowing()) {
                this.spinner.dismiss();
            }
            super.dismiss();
        }
    }

    public void onDetachedFromWindow() {
        this.isDetached = true;
        super.onDetachedFromWindow();
    }

    public void onAttachedToWindow() {
        this.isDetached = DISABLE_SSL_CHECK_FOR_TESTING;
        super.onAttachedToWindow();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setOnCancelListener(new C00801());
        this.spinner = new ProgressDialog(getContext());
        this.spinner.requestWindowFeature(1);
        this.spinner.setMessage(getContext().getString(C0436R.string.loading));
        this.spinner.setOnCancelListener(new C00812());
        requestWindowFeature(1);
        this.contentFrameLayout = new FrameLayout(getContext());
        Pair<Integer, Integer> margins = getMargins();
        this.contentFrameLayout.setPadding(((Integer) margins.first).intValue(), ((Integer) margins.second).intValue(), ((Integer) margins.first).intValue(), ((Integer) margins.second).intValue());
        createCrossImage();
        setUpWebView((this.crossImageView.getDrawable().getIntrinsicWidth() / 2) + 1);
        this.contentFrameLayout.addView(this.crossImageView, new LayoutParams(-2, -2));
        addContentView(this.contentFrameLayout, new LayoutParams(-1, -1));
    }

    private Pair<Integer, Integer> getMargins() {
        double scaleFactor;
        Display display = ((WindowManager) getContext().getSystemService("window")).getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        int scaledWidth = (int) (((float) width) / metrics.density);
        if (scaledWidth <= NO_BUFFER_SCREEN_WIDTH) {
            scaleFactor = 1.0d;
        } else if (scaledWidth >= MAX_BUFFER_SCREEN_WIDTH) {
            scaleFactor = MIN_SCALE_FACTOR;
        } else {
            scaleFactor = MIN_SCALE_FACTOR + ((((double) (1024 - scaledWidth)) / 512.0d) * 0.4d);
        }
        return new Pair(Integer.valueOf((int) ((((double) width) * (1.0d - scaleFactor)) / 2.0d)), Integer.valueOf((int) ((((double) height) * (1.0d - scaleFactor)) / 2.0d)));
    }

    private void sendSuccessToListener(Bundle values) {
        if (this.onCompleteListener != null && !this.listenerCalled) {
            this.listenerCalled = true;
            this.onCompleteListener.onComplete(values, null);
        }
    }

    private void sendErrorToListener(Throwable error) {
        if (this.onCompleteListener != null && !this.listenerCalled) {
            RuntimeException RuntimeException;
            this.listenerCalled = true;
            if (error instanceof RuntimeException) {
                RuntimeException = (RuntimeException) error;
            } else {
                RuntimeException = new RuntimeException(error);
            }
            this.onCompleteListener.onComplete(null, RuntimeException);
        }
    }

    private void sendCancelToListener() {
        sendErrorToListener(new CancellationException());
    }

    private void createCrossImage() {
        this.crossImageView = new ImageView(getContext());
        this.crossImageView.setOnClickListener(new C00823());
        this.crossImageView.setImageDrawable(getContext().getResources().getDrawable(C0436R.drawable.com_facebook_close));
        this.crossImageView.setVisibility(4);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private void setUpWebView(int margin) {
        LinearLayout webViewContainer = new LinearLayout(getContext());
        this.webView = new WebView(getContext());
        this.webView.setVerticalScrollBarEnabled(DISABLE_SSL_CHECK_FOR_TESTING);
        this.webView.setHorizontalScrollBarEnabled(DISABLE_SSL_CHECK_FOR_TESTING);
        this.webView.setWebViewClient(new DialogWebViewClient());
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.loadUrl(this.url);
        this.webView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.webView.setVisibility(4);
        this.webView.getSettings().setSavePassword(DISABLE_SSL_CHECK_FOR_TESTING);
        webViewContainer.setPadding(margin, margin, margin, margin);
        webViewContainer.addView(this.webView);
        webViewContainer.setBackgroundColor(BACKGROUND_GRAY);
        this.contentFrameLayout.addView(webViewContainer);
    }

    public static Bundle decodeUrl(String s) {
        Bundle params = new Bundle();
        if (s != null) {
            for (String parameter : s.split("&")) {
                String[] v = parameter.split("=");
                try {
                    if (v.length == 2) {
                        params.putString(URLDecoder.decode(v[0], "UTF-8"), URLDecoder.decode(v[1], "UTF-8"));
                    } else if (v.length == 1) {
                        params.putString(URLDecoder.decode(v[0], "UTF-8"), ACRAConstants.DEFAULT_STRING_VALUE);
                    }
                } catch (UnsupportedEncodingException e) {
                }
            }
        }
        return params;
    }

    public static Bundle parseUrl(String url) {
        try {
            URL u = new URL(url.replace("fbconnect", "http"));
            Bundle b = decodeUrl(u.getQuery());
            b.putAll(decodeUrl(u.getRef()));
            return b;
        } catch (MalformedURLException e) {
            return new Bundle();
        }
    }
}
