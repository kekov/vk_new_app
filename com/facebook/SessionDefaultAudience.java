package com.facebook;

public enum SessionDefaultAudience {
    NONE(null),
    ONLY_ME(NativeProtocol.AUDIENCE_ME),
    FRIENDS(NativeProtocol.AUDIENCE_FRIENDS),
    EVERYONE(NativeProtocol.AUDIENCE_EVERYONE);
    
    private final String nativeProtocolAudience;

    private SessionDefaultAudience(String protocol) {
        this.nativeProtocolAudience = protocol;
    }

    String getNativeProtocolAudience() {
        return this.nativeProtocolAudience;
    }
}
