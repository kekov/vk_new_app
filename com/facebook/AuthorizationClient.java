package com.facebook;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import com.facebook.PlatformServiceClient.CompletedListener;
import com.facebook.WebDialog.Builder;
import com.facebook.WebDialog.OnCompleteListener;
import com.google.android.gcm.GCMConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import org.acra.ACRAConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class AuthorizationClient implements Serializable {
    private static final String ACCESS_TOKEN = "com.facebook.platform.extra.ACCESS_TOKEN";
    public static final String BATCHED_REST_METHOD_URL_BASE = "method/";
    private static final String DIALOG_AUTHORITY_FORMAT = "m.%s";
    public static final String DIALOG_PARAM_ACCESS_TOKEN = "access_token";
    public static final String DIALOG_PARAM_APP_ID = "app_id";
    public static final String DIALOG_PARAM_CLIENT_ID = "client_id";
    public static final String DIALOG_PARAM_DISPLAY = "display";
    public static final String DIALOG_PARAM_E2E = "e2e";
    public static final String DIALOG_PARAM_REDIRECT_URI = "redirect_uri";
    public static final String DIALOG_PARAM_SCOPE = "scope";
    public static final String DIALOG_PARAM_TYPE = "type";
    public static final String DIALOG_PATH = "dialog/";
    static final String EVENT_EXTRAS_APP_CALL_ID = "call_id";
    static final String EVENT_EXTRAS_DEFAULT_AUDIENCE = "default_audience";
    static final String EVENT_EXTRAS_IS_LEGACY = "is_legacy";
    static final String EVENT_EXTRAS_LOGIN_BEHAVIOR = "login_behavior";
    static final String EVENT_EXTRAS_MISSING_INTERNET_PERMISSION = "no_internet_permission";
    static final String EVENT_EXTRAS_NEW_PERMISSIONS = "new_permissions";
    static final String EVENT_EXTRAS_NOT_TRIED = "not_tried";
    static final String EVENT_EXTRAS_PERMISSIONS = "permissions";
    static final String EVENT_EXTRAS_PROTOCOL_VERSION = "protocol_version";
    static final String EVENT_EXTRAS_REQUEST_CODE = "request_code";
    static final String EVENT_EXTRAS_SERVICE_DISABLED = "service_disabled";
    static final String EVENT_EXTRAS_TRY_LEGACY = "try_legacy";
    static final String EVENT_EXTRAS_TRY_LOGIN_ACTIVITY = "try_login_activity";
    static final String EVENT_EXTRAS_WRITE_PRIVACY = "write_privacy";
    static final String EVENT_NAME_LOGIN_COMPLETE = "fb_mobile_login_complete";
    private static final String EVENT_NAME_LOGIN_METHOD_COMPLETE = "fb_mobile_login_method_complete";
    private static final String EVENT_NAME_LOGIN_METHOD_START = "fb_mobile_login_method_start";
    static final String EVENT_NAME_LOGIN_START = "fb_mobile_login_start";
    static final String EVENT_PARAM_AUTH_LOGGER_ID = "0_auth_logger_id";
    static final String EVENT_PARAM_ERROR_CODE = "4_error_code";
    static final String EVENT_PARAM_ERROR_MESSAGE = "5_error_message";
    static final String EVENT_PARAM_EXTRAS = "6_extras";
    static final String EVENT_PARAM_LOGIN_RESULT = "2_result";
    static final String EVENT_PARAM_METHOD = "3_method";
    private static final String EVENT_PARAM_METHOD_RESULT_SKIPPED = "skipped";
    static final String EVENT_PARAM_TIMESTAMP = "1_timestamp_ms";
    private static final String GRAPH_URL_FORMAT = "https://graph.%s";
    private static final String REST_URL_FORMAT = "https://api.%s/method";
    private static final String TAG = "Facebook-AuthorizationClient";
    private static final String WEB_VIEW_AUTH_HANDLER_STORE = "com.facebook.AuthorizationClient.WebViewAuthHandler.TOKEN_STORE_KEY";
    private static final String WEB_VIEW_AUTH_HANDLER_TOKEN_KEY = "TOKEN";
    private static final long serialVersionUID = 1;
    transient BackgroundProcessingListener backgroundProcessingListener;
    transient boolean checkedInternetPermission;
    transient Context context;
    AuthHandler currentHandler;
    List<AuthHandler> handlersToTry;
    Map<String, String> loggingExtras;
    transient OnCompletedListener onCompletedListener;
    AuthorizationRequest pendingRequest;
    transient StartActivityDelegate startActivityDelegate;

    abstract class AuthHandler implements Serializable {
        private static final long serialVersionUID = 1;
        Map<String, String> methodLoggingExtras;

        abstract String getNameForLogging();

        abstract boolean tryAuthorize(AuthorizationRequest authorizationRequest);

        AuthHandler() {
        }

        boolean onActivityResult(int requestCode, int resultCode, Intent data) {
            return false;
        }

        boolean needsRestart() {
            return false;
        }

        boolean needsInternetPermission() {
            return false;
        }

        void cancel() {
        }

        protected void addLoggingExtra(String key, String value) {
            if (this.methodLoggingExtras == null) {
                this.methodLoggingExtras = new HashMap();
            }
            this.methodLoggingExtras.put(key, value);
        }
    }

    public static class AuthorizationRequest implements Serializable {
        private static final long serialVersionUID = 1;
        private final String applicationId;
        private final String authId;
        private final SessionDefaultAudience defaultAudience;
        private boolean isLegacy;
        private final SessionLoginBehavior loginBehavior;
        private List<String> permissions;
        private final String previousAccessToken;
        private final int requestCode;
        private final transient StartActivityDelegate startActivityDelegate;

        public AuthorizationRequest(SessionLoginBehavior loginBehavior, int requestCode, boolean isLegacy, List<String> permissions, SessionDefaultAudience defaultAudience, String applicationId, String validateSameFbidAsToken, StartActivityDelegate startActivityDelegate, String authId) {
            this.isLegacy = false;
            this.loginBehavior = loginBehavior;
            this.requestCode = requestCode;
            this.isLegacy = isLegacy;
            this.permissions = permissions;
            this.defaultAudience = defaultAudience;
            this.applicationId = applicationId;
            this.previousAccessToken = validateSameFbidAsToken;
            this.startActivityDelegate = startActivityDelegate;
            this.authId = authId;
        }

        StartActivityDelegate getStartActivityDelegate() {
            return this.startActivityDelegate;
        }

        List<String> getPermissions() {
            return this.permissions;
        }

        void setPermissions(List<String> permissions) {
            this.permissions = permissions;
        }

        SessionLoginBehavior getLoginBehavior() {
            return this.loginBehavior;
        }

        int getRequestCode() {
            return this.requestCode;
        }

        SessionDefaultAudience getDefaultAudience() {
            return this.defaultAudience;
        }

        String getApplicationId() {
            return this.applicationId;
        }

        boolean isLegacy() {
            return this.isLegacy;
        }

        void setIsLegacy(boolean isLegacy) {
            this.isLegacy = isLegacy;
        }

        String getPreviousAccessToken() {
            return this.previousAccessToken;
        }

        boolean needsNewTokenValidation() {
            return (this.previousAccessToken == null || this.isLegacy) ? false : true;
        }

        String getAuthId() {
            return this.authId;
        }
    }

    interface BackgroundProcessingListener {
        void onBackgroundProcessingStarted();

        void onBackgroundProcessingStopped();
    }

    public interface OnCompletedListener {
        void onCompleted(Result result);
    }

    public static class Result implements Serializable {
        private static final long serialVersionUID = 1;
        public final Code code;
        public final String errorCode;
        public final String errorMessage;
        public Map<String, String> loggingExtras;
        public final AuthorizationRequest request;
        public final String token;

        enum Code {
            SUCCESS("success"),
            CANCEL("cancel"),
            ERROR(GCMConstants.EXTRA_ERROR);
            
            private final String loggingValue;

            private Code(String loggingValue) {
                this.loggingValue = loggingValue;
            }

            String getLoggingValue() {
                return this.loggingValue;
            }
        }

        private Result(AuthorizationRequest request, Code code, String token, String errorMessage, String errorCode) {
            this.request = request;
            this.token = token;
            this.errorMessage = errorMessage;
            this.code = code;
            this.errorCode = errorCode;
        }

        static Result createTokenResult(AuthorizationRequest request, String token) {
            return new Result(request, Code.SUCCESS, token, null, null);
        }

        static Result createCancelResult(AuthorizationRequest request, String message) {
            return new Result(request, Code.CANCEL, null, message, null);
        }

        static Result createErrorResult(AuthorizationRequest request, String errorType, String errorDescription) {
            return createErrorResult(request, errorType, errorDescription, null);
        }

        static Result createErrorResult(AuthorizationRequest request, String errorType, String errorDescription, String errorCode) {
            return new Result(request, Code.ERROR, null, TextUtils.join(": ", Arrays.asList(new String[]{errorType, errorDescription})), errorCode);
        }
    }

    public interface StartActivityDelegate {
        Activity getActivityContext();

        void startActivityForResult(Intent intent, int i);
    }

    /* renamed from: com.facebook.AuthorizationClient.1 */
    class C11351 implements StartActivityDelegate {
        private final /* synthetic */ Activity val$activity;

        C11351(Activity activity) {
            this.val$activity = activity;
        }

        public void startActivityForResult(Intent intent, int requestCode) {
            this.val$activity.startActivityForResult(intent, requestCode);
        }

        public Activity getActivityContext() {
            return this.val$activity;
        }
    }

    /* renamed from: com.facebook.AuthorizationClient.2 */
    class C11362 implements StartActivityDelegate {
        C11362() {
        }

        public void startActivityForResult(Intent intent, int requestCode) {
            AuthorizationClient.this.pendingRequest.getStartActivityDelegate().startActivityForResult(intent, requestCode);
        }

        public Activity getActivityContext() {
            return AuthorizationClient.this.pendingRequest.getStartActivityDelegate().getActivityContext();
        }
    }

    class GetTokenAuthHandler extends AuthHandler {
        private static final long serialVersionUID = 1;
        private transient GetTokenClient getTokenClient;

        /* renamed from: com.facebook.AuthorizationClient.GetTokenAuthHandler.1 */
        class C11371 implements CompletedListener {
            private final /* synthetic */ AuthorizationRequest val$request;

            C11371(AuthorizationRequest authorizationRequest) {
                this.val$request = authorizationRequest;
            }

            public void completed(Bundle result) {
                GetTokenAuthHandler.this.getTokenCompleted(this.val$request, result);
            }
        }

        GetTokenAuthHandler() {
            super();
        }

        String getNameForLogging() {
            return "get_token";
        }

        void cancel() {
            if (this.getTokenClient != null) {
                this.getTokenClient.cancel();
                this.getTokenClient = null;
            }
        }

        boolean tryAuthorize(AuthorizationRequest request) {
            this.getTokenClient = new GetTokenClient(AuthorizationClient.this.context, request.getApplicationId());
            if (!this.getTokenClient.start()) {
                return false;
            }
            AuthorizationClient.this.notifyBackgroundProcessingStart();
            this.getTokenClient.setCompletedListener(new C11371(request));
            return true;
        }

        void getTokenCompleted(AuthorizationRequest request, Bundle result) {
            this.getTokenClient = null;
            AuthorizationClient.this.notifyBackgroundProcessingStop();
            if (result != null) {
                ArrayList<String> currentPermissions = result.getStringArrayList(NativeProtocol.EXTRA_PERMISSIONS);
                List<String> permissions = request.getPermissions();
                if (currentPermissions == null || !(permissions == null || currentPermissions.containsAll(permissions))) {
                    List<String> newPermissions = new ArrayList();
                    for (String permission : permissions) {
                        if (!currentPermissions.contains(permission)) {
                            newPermissions.add(permission);
                        }
                    }
                    if (!newPermissions.isEmpty()) {
                        addLoggingExtra(AuthorizationClient.EVENT_EXTRAS_NEW_PERMISSIONS, TextUtils.join(",", newPermissions));
                    }
                    request.setPermissions(newPermissions);
                } else {
                    AuthorizationClient.this.completeAndValidate(Result.createTokenResult(AuthorizationClient.this.pendingRequest, result.getString(AuthorizationClient.ACCESS_TOKEN)));
                    return;
                }
            }
            AuthorizationClient.this.tryNextHandler();
        }
    }

    abstract class KatanaAuthHandler extends AuthHandler {
        private static final long serialVersionUID = 1;

        KatanaAuthHandler() {
            super();
        }

        protected boolean tryIntent(Intent intent, int requestCode) {
            if (intent == null) {
                return false;
            }
            try {
                AuthorizationClient.this.getStartActivityDelegate().startActivityForResult(intent, requestCode);
                return true;
            } catch (ActivityNotFoundException e) {
                return false;
            }
        }
    }

    class WebViewAuthHandler extends AuthHandler {
        private static final long serialVersionUID = 1;
        private String applicationId;
        private String e2e;
        private transient WebDialog loginDialog;

        /* renamed from: com.facebook.AuthorizationClient.WebViewAuthHandler.1 */
        class C11381 implements OnCompleteListener {
            private final /* synthetic */ AuthorizationRequest val$request;

            C11381(AuthorizationRequest authorizationRequest) {
                this.val$request = authorizationRequest;
            }

            public void onComplete(Bundle values, RuntimeException error) {
                WebViewAuthHandler.this.onWebDialogComplete(this.val$request, values, error);
            }
        }

        WebViewAuthHandler() {
            super();
        }

        String getNameForLogging() {
            return "web_view";
        }

        boolean needsRestart() {
            return true;
        }

        boolean needsInternetPermission() {
            return true;
        }

        void cancel() {
            if (this.loginDialog != null) {
                this.loginDialog.dismiss();
                this.loginDialog = null;
            }
        }

        boolean tryAuthorize(AuthorizationRequest request) {
            this.applicationId = request.getApplicationId();
            Bundle parameters = new Bundle();
            if (!AuthorizationClient.isNullOrEmpty(request.getPermissions())) {
                parameters.putString(AuthorizationClient.DIALOG_PARAM_SCOPE, TextUtils.join(",", request.getPermissions()));
            }
            String previousToken = request.getPreviousAccessToken();
            if (!AuthorizationClient.isNullOrEmpty(previousToken) && previousToken.equals(loadCookieToken())) {
                parameters.putString(AuthorizationClient.DIALOG_PARAM_ACCESS_TOKEN, previousToken);
            }
            OnCompleteListener listener = new C11381(request);
            this.e2e = AuthorizationClient.getE2E();
            addLoggingExtra(AuthorizationClient.DIALOG_PARAM_E2E, this.e2e);
            this.loginDialog = ((Builder) new AuthDialogBuilder(AuthorizationClient.this.getStartActivityDelegate().getActivityContext(), this.applicationId, parameters).setE2E(this.e2e).setOnCompleteListener(listener)).build();
            this.loginDialog.show();
            return true;
        }

        void onWebDialogComplete(AuthorizationRequest request, Bundle values, RuntimeException error) {
            Result outcome;
            if (values != null) {
                if (values.containsKey(AuthorizationClient.DIALOG_PARAM_E2E)) {
                    this.e2e = values.getString(AuthorizationClient.DIALOG_PARAM_E2E);
                }
                String token = values.getString(AuthorizationClient.DIALOG_PARAM_ACCESS_TOKEN);
                outcome = Result.createTokenResult(AuthorizationClient.this.pendingRequest, token);
                CookieSyncManager.createInstance(AuthorizationClient.this.context).sync();
                saveCookieToken(token);
            } else if (error instanceof CancellationException) {
                outcome = Result.createCancelResult(AuthorizationClient.this.pendingRequest, "User canceled log in.");
            } else {
                this.e2e = null;
                String errorCode = null;
                String errorMessage = error.getMessage();
                if (error instanceof FacebookServiceException) {
                    errorCode = String.format("%d", new Object[]{Integer.valueOf(((FacebookServiceException) error).getRequestError().getErrorCode())});
                    errorMessage = requestError.toString();
                }
                outcome = Result.createErrorResult(AuthorizationClient.this.pendingRequest, null, errorMessage, errorCode);
            }
            if (!AuthorizationClient.isNullOrEmpty(this.e2e)) {
                AuthorizationClient.this.logWebLoginCompleted(this.applicationId, this.e2e);
            }
            AuthorizationClient.this.completeAndValidate(outcome);
        }

        private void saveCookieToken(String token) {
            AuthorizationClient.this.getStartActivityDelegate().getActivityContext().getSharedPreferences(AuthorizationClient.WEB_VIEW_AUTH_HANDLER_STORE, 0).edit().putString(AuthorizationClient.WEB_VIEW_AUTH_HANDLER_TOKEN_KEY, token);
        }

        private String loadCookieToken() {
            return AuthorizationClient.this.getStartActivityDelegate().getActivityContext().getSharedPreferences(AuthorizationClient.WEB_VIEW_AUTH_HANDLER_STORE, 0).getString(AuthorizationClient.WEB_VIEW_AUTH_HANDLER_TOKEN_KEY, ACRAConstants.DEFAULT_STRING_VALUE);
        }
    }

    static class AuthDialogBuilder extends Builder {
        private static final String OAUTH_DIALOG = "oauth";
        static final String REDIRECT_URI = "fbconnect://success";
        private String e2e;

        public AuthDialogBuilder(Context context, String applicationId, Bundle parameters) {
            super(context, applicationId, OAUTH_DIALOG, parameters);
        }

        public AuthDialogBuilder setE2E(String e2e) {
            this.e2e = e2e;
            return this;
        }

        public WebDialog build() {
            Bundle parameters = getParameters();
            parameters.putString(AuthorizationClient.DIALOG_PARAM_REDIRECT_URI, REDIRECT_URI);
            parameters.putString(AuthorizationClient.DIALOG_PARAM_CLIENT_ID, getApplicationId());
            parameters.putString(AuthorizationClient.DIALOG_PARAM_E2E, this.e2e);
            return new WebDialog(getContext(), OAUTH_DIALOG, parameters, getTheme(), getListener());
        }
    }

    class KatanaLoginDialogAuthHandler extends KatanaAuthHandler {
        private static final long serialVersionUID = 1;
        private String applicationId;
        private String callId;

        KatanaLoginDialogAuthHandler() {
            super();
        }

        String getNameForLogging() {
            return "katana_login_dialog";
        }

        boolean tryAuthorize(AuthorizationRequest request) {
            this.applicationId = request.getApplicationId();
            Intent intent = NativeProtocol.createLoginDialog20121101Intent(AuthorizationClient.this.context, request.getApplicationId(), new ArrayList(request.getPermissions()), request.getDefaultAudience().getNativeProtocolAudience());
            if (intent == null) {
                return false;
            }
            this.callId = intent.getStringExtra(NativeProtocol.EXTRA_PROTOCOL_CALL_ID);
            addLoggingExtra(AuthorizationClient.EVENT_EXTRAS_APP_CALL_ID, this.callId);
            addLoggingExtra(AuthorizationClient.EVENT_EXTRAS_PROTOCOL_VERSION, intent.getStringExtra(NativeProtocol.EXTRA_PROTOCOL_VERSION));
            addLoggingExtra(AuthorizationClient.EVENT_EXTRAS_PERMISSIONS, TextUtils.join(",", intent.getStringArrayListExtra(NativeProtocol.EXTRA_PERMISSIONS)));
            addLoggingExtra(AuthorizationClient.EVENT_EXTRAS_WRITE_PRIVACY, intent.getStringExtra(NativeProtocol.EXTRA_WRITE_PRIVACY));
            return tryIntent(intent, request.getRequestCode());
        }

        public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
            Result outcome;
            if (data == null) {
                outcome = Result.createCancelResult(AuthorizationClient.this.pendingRequest, "Operation canceled");
            } else if (NativeProtocol.isServiceDisabledResult20121101(data)) {
                outcome = null;
            } else if (resultCode == 0) {
                outcome = createCancelOrErrorResult(AuthorizationClient.this.pendingRequest, data);
            } else if (resultCode != -1) {
                outcome = Result.createErrorResult(AuthorizationClient.this.pendingRequest, "Unexpected resultCode from authorization.", null);
            } else {
                outcome = handleResultOk(data);
            }
            if (outcome != null) {
                AuthorizationClient.this.completeAndValidate(outcome);
            } else {
                AuthorizationClient.this.tryNextHandler();
            }
            return true;
        }

        private Result handleResultOk(Intent data) {
            Bundle extras = data.getExtras();
            String errorType = extras.getString(NativeProtocol.STATUS_ERROR_TYPE);
            if (errorType == null) {
                return Result.createTokenResult(AuthorizationClient.this.pendingRequest, extras.getString(AuthorizationClient.ACCESS_TOKEN));
            }
            if (NativeProtocol.ERROR_SERVICE_DISABLED.equals(errorType)) {
                return null;
            }
            return createCancelOrErrorResult(AuthorizationClient.this.pendingRequest, data);
        }

        private Result createCancelOrErrorResult(AuthorizationRequest request, Intent data) {
            Bundle extras = data.getExtras();
            String errorType = extras.getString(NativeProtocol.STATUS_ERROR_TYPE);
            if (NativeProtocol.ERROR_USER_CANCELED.equals(errorType) || NativeProtocol.ERROR_PERMISSION_DENIED.equals(errorType)) {
                return Result.createCancelResult(request, data.getStringExtra(NativeProtocol.STATUS_ERROR_DESCRIPTION));
            }
            String errorJson = extras.getString(NativeProtocol.STATUS_ERROR_JSON);
            String errorCode = null;
            if (errorJson != null) {
                try {
                    errorCode = new JSONObject(errorJson).getString("error_code");
                } catch (JSONException e) {
                }
            }
            return Result.createErrorResult(request, errorType, data.getStringExtra(NativeProtocol.STATUS_ERROR_DESCRIPTION), errorCode);
        }

        private void logEvent(String eventName, String timeParameter, String callId) {
        }
    }

    class KatanaProxyAuthHandler extends KatanaAuthHandler {
        private static final long serialVersionUID = 1;
        private String applicationId;

        KatanaProxyAuthHandler() {
            super();
        }

        String getNameForLogging() {
            return "katana_proxy_auth";
        }

        boolean tryAuthorize(AuthorizationRequest request) {
            this.applicationId = request.getApplicationId();
            String e2e = AuthorizationClient.getE2E();
            Intent intent = NativeProtocol.createProxyAuthIntent(AuthorizationClient.this.context, request.getApplicationId(), request.getPermissions(), e2e);
            addLoggingExtra(AuthorizationClient.DIALOG_PARAM_E2E, e2e);
            return tryIntent(intent, request.getRequestCode());
        }

        boolean onActivityResult(int requestCode, int resultCode, Intent data) {
            Result outcome;
            if (data == null) {
                outcome = Result.createCancelResult(AuthorizationClient.this.pendingRequest, "Operation canceled");
            } else if (resultCode == 0) {
                outcome = Result.createCancelResult(AuthorizationClient.this.pendingRequest, data.getStringExtra(GCMConstants.EXTRA_ERROR));
            } else if (resultCode != -1) {
                outcome = Result.createErrorResult(AuthorizationClient.this.pendingRequest, "Unexpected resultCode from authorization.", null);
            } else {
                outcome = handleResultOk(data);
            }
            if (outcome != null) {
                AuthorizationClient.this.completeAndValidate(outcome);
            } else {
                AuthorizationClient.this.tryNextHandler();
            }
            return true;
        }

        private Result handleResultOk(Intent data) {
            Bundle extras = data.getExtras();
            String error = extras.getString(GCMConstants.EXTRA_ERROR);
            if (error == null) {
                error = extras.getString("error_type");
            }
            String errorCode = extras.getString("error_code");
            String errorMessage = extras.getString("error_message");
            if (errorMessage == null) {
                errorMessage = extras.getString("error_description");
            }
            String e2e = extras.getString(AuthorizationClient.DIALOG_PARAM_E2E);
            if (!AuthorizationClient.isNullOrEmpty(e2e)) {
                AuthorizationClient.this.logWebLoginCompleted(this.applicationId, e2e);
            }
            if (error != null || errorCode != null || errorMessage != null) {
                return Result.createErrorResult(AuthorizationClient.this.pendingRequest, error, errorMessage, errorCode);
            }
            return Result.createTokenResult(AuthorizationClient.this.pendingRequest, extras.getString(AuthorizationClient.ACCESS_TOKEN));
        }
    }

    public void setContext(Context context) {
        this.context = context;
        this.startActivityDelegate = null;
    }

    public void setContext(Activity activity) {
        this.context = activity;
        this.startActivityDelegate = new C11351(activity);
    }

    void startOrContinueAuth(AuthorizationRequest request) {
        if (getInProgress()) {
            continueAuth();
        } else {
            authorize(request);
        }
    }

    public void authorize(AuthorizationRequest request) {
        if (request != null) {
            if (this.pendingRequest != null) {
                throw new RuntimeException("Attempted to authorize while a request is pending.");
            } else if (!request.needsNewTokenValidation() || checkInternetPermission()) {
                this.pendingRequest = request;
                this.handlersToTry = getHandlerTypes(request);
                tryNextHandler();
            }
        }
    }

    void continueAuth() {
        if (this.pendingRequest == null || this.currentHandler == null) {
            throw new RuntimeException("Attempted to continue authorization without a pending request.");
        } else if (this.currentHandler.needsRestart()) {
            this.currentHandler.cancel();
            tryCurrentHandler();
        }
    }

    boolean getInProgress() {
        return (this.pendingRequest == null || this.currentHandler == null) ? false : true;
    }

    void cancelCurrentHandler() {
        if (this.currentHandler != null) {
            this.currentHandler.cancel();
        }
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == this.pendingRequest.getRequestCode()) {
            return this.currentHandler.onActivityResult(requestCode, resultCode, data);
        }
        return false;
    }

    private List<AuthHandler> getHandlerTypes(AuthorizationRequest request) {
        ArrayList<AuthHandler> handlers = new ArrayList();
        if (!request.isLegacy()) {
            handlers.add(new GetTokenAuthHandler());
            handlers.add(new KatanaLoginDialogAuthHandler());
        }
        handlers.add(new KatanaProxyAuthHandler());
        handlers.add(new WebViewAuthHandler());
        return handlers;
    }

    boolean checkInternetPermission() {
        if (this.checkedInternetPermission) {
            return true;
        }
        if (checkPermission("android.permission.INTERNET") != 0) {
            complete(Result.createErrorResult(this.pendingRequest, "Error", "Internet permission"));
            return false;
        }
        this.checkedInternetPermission = true;
        return true;
    }

    void tryNextHandler() {
        if (this.currentHandler != null) {
            logAuthorizationMethodComplete(this.currentHandler.getNameForLogging(), EVENT_PARAM_METHOD_RESULT_SKIPPED, null, null, this.currentHandler.methodLoggingExtras);
        }
        while (this.handlersToTry != null && !this.handlersToTry.isEmpty()) {
            this.currentHandler = (AuthHandler) this.handlersToTry.remove(0);
            if (tryCurrentHandler()) {
                return;
            }
        }
        if (this.pendingRequest != null) {
            completeWithFailure();
        }
    }

    private void completeWithFailure() {
        complete(Result.createErrorResult(this.pendingRequest, "Login attempt failed.", null));
    }

    private void addLoggingExtra(String key, String value, boolean accumulate) {
        if (this.loggingExtras == null) {
            this.loggingExtras = new HashMap();
        }
        if (this.loggingExtras.containsKey(key) && accumulate) {
            value = new StringBuilder(String.valueOf((String) this.loggingExtras.get(key))).append(",").append(value).toString();
        }
        this.loggingExtras.put(key, value);
    }

    boolean tryCurrentHandler() {
        if (this.currentHandler.needsInternetPermission() && !checkInternetPermission()) {
            return false;
        }
        boolean tried = this.currentHandler.tryAuthorize(this.pendingRequest);
        if (tried) {
            logAuthorizationMethodStart(this.currentHandler.getNameForLogging());
            return tried;
        }
        addLoggingExtra(EVENT_EXTRAS_NOT_TRIED, this.currentHandler.getNameForLogging(), true);
        return tried;
    }

    void completeAndValidate(Result outcome) {
        if (outcome.token == null || !this.pendingRequest.needsNewTokenValidation()) {
            complete(outcome);
        } else {
            validateSameFbidAndFinish(outcome);
        }
    }

    void complete(Result outcome) {
        if (this.currentHandler != null) {
            logAuthorizationMethodComplete(this.currentHandler.getNameForLogging(), outcome, this.currentHandler.methodLoggingExtras);
        }
        if (this.loggingExtras != null) {
            outcome.loggingExtras = this.loggingExtras;
        }
        this.handlersToTry = null;
        this.currentHandler = null;
        this.pendingRequest = null;
        this.loggingExtras = null;
        notifyOnCompleteListener(outcome);
    }

    OnCompletedListener getOnCompletedListener() {
        return this.onCompletedListener;
    }

    public void setOnCompletedListener(OnCompletedListener onCompletedListener) {
        this.onCompletedListener = onCompletedListener;
    }

    BackgroundProcessingListener getBackgroundProcessingListener() {
        return this.backgroundProcessingListener;
    }

    void setBackgroundProcessingListener(BackgroundProcessingListener backgroundProcessingListener) {
        this.backgroundProcessingListener = backgroundProcessingListener;
    }

    StartActivityDelegate getStartActivityDelegate() {
        if (this.startActivityDelegate != null) {
            return this.startActivityDelegate;
        }
        return this.pendingRequest != null ? new C11362() : null;
    }

    int checkPermission(String permission) {
        return this.context.checkCallingOrSelfPermission(permission);
    }

    void validateSameFbidAndFinish(Result pendingResult) {
        if (pendingResult.token == null) {
            throw new RuntimeException("Can't validate without a token");
        }
        notifyBackgroundProcessingStart();
    }

    private void notifyOnCompleteListener(Result outcome) {
        if (this.onCompletedListener != null) {
            this.onCompletedListener.onCompleted(outcome);
        }
    }

    private void notifyBackgroundProcessingStart() {
        if (this.backgroundProcessingListener != null) {
            this.backgroundProcessingListener.onBackgroundProcessingStarted();
        }
    }

    private void notifyBackgroundProcessingStop() {
        if (this.backgroundProcessingListener != null) {
            this.backgroundProcessingListener.onBackgroundProcessingStopped();
        }
    }

    private void logAuthorizationMethodStart(String method) {
        Bundle bundle = newAuthorizationLoggingBundle(this.pendingRequest.getAuthId());
        bundle.putLong(EVENT_PARAM_TIMESTAMP, System.currentTimeMillis());
        bundle.putString(EVENT_PARAM_METHOD, method);
    }

    private void logAuthorizationMethodComplete(String method, Result result, Map<String, String> loggingExtras) {
        logAuthorizationMethodComplete(method, result.code.getLoggingValue(), result.errorMessage, result.errorCode, loggingExtras);
    }

    private void logAuthorizationMethodComplete(String method, String result, String errorMessage, String errorCode, Map<String, String> loggingExtras) {
        Bundle bundle;
        if (this.pendingRequest == null) {
            bundle = newAuthorizationLoggingBundle(ACRAConstants.DEFAULT_STRING_VALUE);
            bundle.putString(EVENT_PARAM_LOGIN_RESULT, Code.ERROR.getLoggingValue());
            bundle.putString(EVENT_PARAM_ERROR_MESSAGE, "Unexpected call to logAuthorizationMethodComplete with null pendingRequest.");
        } else {
            bundle = newAuthorizationLoggingBundle(this.pendingRequest.getAuthId());
            if (result != null) {
                bundle.putString(EVENT_PARAM_LOGIN_RESULT, result);
            }
            if (errorMessage != null) {
                bundle.putString(EVENT_PARAM_ERROR_MESSAGE, errorMessage);
            }
            if (errorCode != null) {
                bundle.putString(EVENT_PARAM_ERROR_CODE, errorCode);
            }
            if (!(loggingExtras == null || loggingExtras.isEmpty())) {
                bundle.putString(EVENT_PARAM_EXTRAS, new JSONObject(loggingExtras).toString());
            }
        }
        bundle.putString(EVENT_PARAM_METHOD, method);
        bundle.putLong(EVENT_PARAM_TIMESTAMP, System.currentTimeMillis());
    }

    static Bundle newAuthorizationLoggingBundle(String authLoggerId) {
        Bundle bundle = new Bundle();
        bundle.putLong(EVENT_PARAM_TIMESTAMP, System.currentTimeMillis());
        bundle.putString(EVENT_PARAM_AUTH_LOGGER_ID, authLoggerId);
        bundle.putString(EVENT_PARAM_METHOD, ACRAConstants.DEFAULT_STRING_VALUE);
        bundle.putString(EVENT_PARAM_LOGIN_RESULT, ACRAConstants.DEFAULT_STRING_VALUE);
        bundle.putString(EVENT_PARAM_ERROR_MESSAGE, ACRAConstants.DEFAULT_STRING_VALUE);
        bundle.putString(EVENT_PARAM_ERROR_CODE, ACRAConstants.DEFAULT_STRING_VALUE);
        bundle.putString(EVENT_PARAM_EXTRAS, ACRAConstants.DEFAULT_STRING_VALUE);
        return bundle;
    }

    public static boolean isNullOrEmpty(Object o) {
        if (o == null) {
            return true;
        }
        if (o instanceof List) {
            if (((List) o).size() != 0) {
                return false;
            }
            return true;
        } else if (o.toString().length() != 0) {
            return false;
        } else {
            return true;
        }
    }

    private static String getE2E() {
        JSONObject e2e = new JSONObject();
        try {
            e2e.put("init", System.currentTimeMillis());
        } catch (JSONException e) {
        }
        return e2e.toString();
    }

    private void logWebLoginCompleted(String applicationId, String e2e) {
    }
}
