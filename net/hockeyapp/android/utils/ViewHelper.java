package net.hockeyapp.android.utils;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import com.vkontakte.android.mediapicker.providers.ActivityClassProvider.Color;

public class ViewHelper {
    public static Drawable getGradient() {
        return new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{Color.WINDOW_BACKGROUND_COLOR, 0});
    }
}
