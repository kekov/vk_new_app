package de.ankri.text.method;

public interface TransformationMethodCompat2 extends TransformationMethodCompat {
    void setLengthChangesAllowed(boolean z);
}
