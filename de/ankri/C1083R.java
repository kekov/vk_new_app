package de.ankri;

import com.vkontakte.android.C0436R;

/* renamed from: de.ankri.R */
public final class C1083R {

    /* renamed from: de.ankri.R.attr */
    public static final class attr {
        public static final int fontFamily = 2130771982;
        public static final int switchMinWidth = 2130771975;
        public static final int switchPadding = 2130771976;
        public static final int switchStyle = 2130771968;
        public static final int switchTextAppearance = 2130771974;
        public static final int textAppearance = 2130771977;
        public static final int textColor = 2130771978;
        public static final int textColorHighlight = 2130771983;
        public static final int textColorHint = 2130771984;
        public static final int textColorLink = 2130771985;
        public static final int textOff = 2130771972;
        public static final int textOn = 2130771971;
        public static final int textSize = 2130771979;
        public static final int textStyle = 2130771980;
        public static final int thumb = 2130771969;
        public static final int thumbTextPadding = 2130771973;
        public static final int track = 2130771970;
        public static final int typeface = 2130771981;
    }

    /* renamed from: de.ankri.R.drawable */
    public static final class drawable {
        public static final int switch_bg_disabled_holo_dark = 2130838107;
        public static final int switch_bg_disabled_holo_light = 2130838108;
        public static final int switch_bg_focused_holo_dark = 2130838109;
        public static final int switch_bg_focused_holo_light = 2130838110;
        public static final int switch_bg_holo_dark = 2130838111;
        public static final int switch_bg_holo_light = 2130838112;
        public static final int switch_inner_holo_dark = 2130838113;
        public static final int switch_inner_holo_light = 2130838114;
        public static final int switch_thumb_activated_holo_dark = 2130838118;
        public static final int switch_thumb_activated_holo_light = 2130838119;
        public static final int switch_thumb_disabled_holo_dark = 2130838120;
        public static final int switch_thumb_disabled_holo_light = 2130838121;
        public static final int switch_thumb_holo_dark = 2130838122;
        public static final int switch_thumb_holo_light = 2130838123;
        public static final int switch_thumb_pressed_holo_dark = 2130838124;
        public static final int switch_thumb_pressed_holo_light = 2130838125;
        public static final int switch_track_holo_dark = 2130838127;
        public static final int switch_track_holo_light = 2130838128;
    }

    /* renamed from: de.ankri.R.string */
    public static final class string {
        public static final int textOff = 2131230747;
        public static final int textOn = 2131230746;
    }

    /* renamed from: de.ankri.R.style */
    public static final class style {
        public static final int AppThemeDark = 2131361802;
        public static final int AppThemeLight = 2131361803;
        public static final int TextAppearance = 2131361801;
        public static final int switch_dark = 2131361799;
        public static final int switch_light = 2131361800;
    }

    /* renamed from: de.ankri.R.styleable */
    public static final class styleable {
        public static final int[] Switch;
        public static final int Switch_switchMinWidth = 6;
        public static final int Switch_switchPadding = 7;
        public static final int Switch_switchTextAppearance = 5;
        public static final int Switch_textOff = 3;
        public static final int Switch_textOn = 2;
        public static final int Switch_thumb = 0;
        public static final int Switch_thumbTextPadding = 4;
        public static final int Switch_track = 1;
        public static final int[] TextAppearanceSwitch;
        public static final int TextAppearanceSwitch_fontFamily = 4;
        public static final int TextAppearanceSwitch_textColor = 0;
        public static final int TextAppearanceSwitch_textColorHighlight = 5;
        public static final int TextAppearanceSwitch_textColorHint = 6;
        public static final int TextAppearanceSwitch_textColorLink = 7;
        public static final int TextAppearanceSwitch_textSize = 1;
        public static final int TextAppearanceSwitch_textStyle = 2;
        public static final int TextAppearanceSwitch_typeface = 3;

        static {
            Switch = new int[]{C0436R.attr.thumb, C0436R.attr.track, C0436R.attr.textOn, C0436R.attr.textOff, C0436R.attr.thumbTextPadding, C0436R.attr.switchTextAppearance, C0436R.attr.switchMinWidth, C0436R.attr.switchPadding};
            TextAppearanceSwitch = new int[]{C0436R.attr.textColor, C0436R.attr.textSize, C0436R.attr.textStyle, C0436R.attr.typeface, C0436R.attr.fontFamily, C0436R.attr.textColorHighlight, C0436R.attr.textColorHint, C0436R.attr.textColorLink};
        }
    }
}
