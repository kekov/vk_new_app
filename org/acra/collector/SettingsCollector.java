package org.acra.collector;

import android.content.ContentResolver;
import android.content.Context;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.util.Log;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.acra.ACRA;
import org.acra.ACRAConstants;

final class SettingsCollector {
    SettingsCollector() {
    }

    public static String collectSystemSettings(Context ctx) {
        StringBuilder result = new StringBuilder();
        for (Field key : System.class.getFields()) {
            if (!key.isAnnotationPresent(Deprecated.class) && key.getType() == String.class) {
                try {
                    String value = System.getString(ctx.getContentResolver(), (String) key.get(null));
                    if (value != null) {
                        result.append(key.getName()).append("=").append(value).append("\n");
                    }
                } catch (IllegalArgumentException e) {
                    Log.w(ACRA.LOG_TAG, "Error : ", e);
                } catch (IllegalAccessException e2) {
                    Log.w(ACRA.LOG_TAG, "Error : ", e2);
                }
            }
        }
        return result.toString();
    }

    public static String collectSecureSettings(Context ctx) {
        StringBuilder result = new StringBuilder();
        for (Field key : Secure.class.getFields()) {
            if (!key.isAnnotationPresent(Deprecated.class) && key.getType() == String.class && isAuthorized(key)) {
                try {
                    String value = Secure.getString(ctx.getContentResolver(), (String) key.get(null));
                    if (value != null) {
                        result.append(key.getName()).append("=").append(value).append("\n");
                    }
                } catch (IllegalArgumentException e) {
                    Log.w(ACRA.LOG_TAG, "Error : ", e);
                } catch (IllegalAccessException e2) {
                    Log.w(ACRA.LOG_TAG, "Error : ", e2);
                }
            }
        }
        return result.toString();
    }

    public static String collectGlobalSettings(Context ctx) {
        if (Compatibility.getAPILevel() < 17) {
            return ACRAConstants.DEFAULT_STRING_VALUE;
        }
        StringBuilder result = new StringBuilder();
        try {
            Class<?> globalClass = Class.forName("android.provider.Settings$Global");
            Field[] keys = globalClass.getFields();
            Method getString = globalClass.getMethod("getString", new Class[]{ContentResolver.class, String.class});
            for (Field key : keys) {
                if (!key.isAnnotationPresent(Deprecated.class) && key.getType() == String.class && isAuthorized(key)) {
                    Object value = getString.invoke(null, new Object[]{ctx.getContentResolver(), (String) key.get(null)});
                    if (value != null) {
                        result.append(key.getName()).append("=").append(value).append("\n");
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            Log.w(ACRA.LOG_TAG, "Error : ", e);
        } catch (IllegalAccessException e2) {
            Log.w(ACRA.LOG_TAG, "Error : ", e2);
        } catch (ClassNotFoundException e3) {
            Log.w(ACRA.LOG_TAG, "Error : ", e3);
        } catch (SecurityException e4) {
            Log.w(ACRA.LOG_TAG, "Error : ", e4);
        } catch (NoSuchMethodException e5) {
            Log.w(ACRA.LOG_TAG, "Error : ", e5);
        } catch (InvocationTargetException e6) {
            Log.w(ACRA.LOG_TAG, "Error : ", e6);
        }
        return result.toString();
    }

    private static boolean isAuthorized(Field key) {
        if (key == null || key.getName().startsWith("WIFI_AP")) {
            return false;
        }
        for (String regex : ACRA.getConfig().excludeMatchingSettingsKeys()) {
            if (key.getName().matches(regex)) {
                return false;
            }
        }
        return true;
    }
}
