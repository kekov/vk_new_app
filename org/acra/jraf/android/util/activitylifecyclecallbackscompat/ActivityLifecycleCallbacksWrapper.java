package org.acra.jraf.android.util.activitylifecyclecallbackscompat;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;

class ActivityLifecycleCallbacksWrapper implements ActivityLifecycleCallbacks {
    private ActivityLifecycleCallbacksCompat mCallback;

    public ActivityLifecycleCallbacksWrapper(ActivityLifecycleCallbacksCompat callback) {
        this.mCallback = callback;
    }

    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        this.mCallback.onActivityCreated(activity, savedInstanceState);
    }

    public void onActivityStarted(Activity activity) {
        this.mCallback.onActivityStarted(activity);
    }

    public void onActivityResumed(Activity activity) {
        this.mCallback.onActivityResumed(activity);
    }

    public void onActivityPaused(Activity activity) {
        this.mCallback.onActivityPaused(activity);
    }

    public void onActivityStopped(Activity activity) {
        this.mCallback.onActivityStopped(activity);
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        this.mCallback.onActivitySaveInstanceState(activity, outState);
    }

    public void onActivityDestroyed(Activity activity) {
        this.mCallback.onActivityDestroyed(activity);
    }
}
