package org.acra.log;

import android.util.Log;

public final class AndroidLogDelegate implements ACRALog {
    public int m1022v(String tag, String msg) {
        return Log.v(tag, msg);
    }

    public int m1023v(String tag, String msg, Throwable tr) {
        return Log.v(tag, msg, tr);
    }

    public int m1016d(String tag, String msg) {
        return Log.d(tag, msg);
    }

    public int m1017d(String tag, String msg, Throwable tr) {
        return Log.d(tag, msg, tr);
    }

    public int m1020i(String tag, String msg) {
        return Log.i(tag, msg);
    }

    public int m1021i(String tag, String msg, Throwable tr) {
        return Log.i(tag, msg, tr);
    }

    public int m1024w(String tag, String msg) {
        return Log.w(tag, msg);
    }

    public int m1025w(String tag, String msg, Throwable tr) {
        return Log.w(tag, msg, tr);
    }

    public int m1026w(String tag, Throwable tr) {
        return Log.w(tag, tr);
    }

    public int m1018e(String tag, String msg) {
        return Log.e(tag, msg);
    }

    public int m1019e(String tag, String msg, Throwable tr) {
        return Log.e(tag, msg, tr);
    }

    public String getStackTraceString(Throwable tr) {
        return Log.getStackTraceString(tr);
    }
}
