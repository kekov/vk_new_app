package org.acra.log;

public interface ACRALog {
    int m540d(String str, String str2);

    int m541d(String str, String str2, Throwable th);

    int m542e(String str, String str2);

    int m543e(String str, String str2, Throwable th);

    String getStackTraceString(Throwable th);

    int m544i(String str, String str2);

    int m545i(String str, String str2, Throwable th);

    int m546v(String str, String str2);

    int m547v(String str, String str2, Throwable th);

    int m548w(String str, String str2);

    int m549w(String str, String str2, Throwable th);

    int m550w(String str, Throwable th);
}
