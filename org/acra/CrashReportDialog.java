package org.acra;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.io.IOException;
import org.acra.collector.CrashReportData;
import org.acra.util.ToastSender;

public class CrashReportDialog extends Activity implements OnClickListener, OnDismissListener {
    private static final String STATE_COMMENT = "comment";
    private static final String STATE_EMAIL = "email";
    AlertDialog mDialog;
    String mReportFileName;
    private SharedPreferences prefs;
    private EditText userComment;
    private EditText userEmail;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getBooleanExtra("FORCE_CANCEL", false)) {
            ACRA.log.m540d(ACRA.LOG_TAG, "Forced reports deletion.");
            cancelReports();
            finish();
            return;
        }
        this.mReportFileName = getIntent().getStringExtra("REPORT_FILE_NAME");
        Log.d(ACRA.LOG_TAG, "Opening CrashReportDialog for " + this.mReportFileName);
        if (this.mReportFileName == null) {
            finish();
        }
        Builder dialogBuilder = new Builder(this);
        int resourceId = ACRA.getConfig().resDialogTitle();
        if (resourceId != 0) {
            dialogBuilder.setTitle(resourceId);
        }
        resourceId = ACRA.getConfig().resDialogIcon();
        if (resourceId != 0) {
            dialogBuilder.setIcon(resourceId);
        }
        dialogBuilder.setView(buildCustomView(savedInstanceState));
        dialogBuilder.setPositiveButton(17039370, this);
        dialogBuilder.setNegativeButton(17039360, this);
        cancelNotification();
        this.mDialog = dialogBuilder.create();
        this.mDialog.setCanceledOnTouchOutside(false);
        this.mDialog.setOnDismissListener(this);
        this.mDialog.show();
    }

    private View buildCustomView(Bundle savedInstanceState) {
        String savedValue;
        LinearLayout root = new LinearLayout(this);
        root.setOrientation(1);
        root.setPadding(10, 10, 10, 10);
        root.setLayoutParams(new LayoutParams(-1, -2));
        root.setFocusable(true);
        root.setFocusableInTouchMode(true);
        ScrollView scroll = new ScrollView(this);
        root.addView(scroll, new LinearLayout.LayoutParams(-1, -1, 1.0f));
        LinearLayout scrollable = new LinearLayout(this);
        scrollable.setOrientation(1);
        scroll.addView(scrollable);
        TextView text = new TextView(this);
        int dialogTextId = ACRA.getConfig().resDialogText();
        if (dialogTextId != 0) {
            text.setText(getText(dialogTextId));
        }
        scrollable.addView(text);
        int commentPromptId = ACRA.getConfig().resDialogCommentPrompt();
        if (commentPromptId != 0) {
            TextView label = new TextView(this);
            label.setText(getText(commentPromptId));
            label.setPadding(label.getPaddingLeft(), 10, label.getPaddingRight(), label.getPaddingBottom());
            scrollable.addView(label, new LinearLayout.LayoutParams(-1, -2));
            this.userComment = new EditText(this);
            this.userComment.setLines(2);
            if (savedInstanceState != null) {
                savedValue = savedInstanceState.getString(STATE_COMMENT);
                if (savedValue != null) {
                    this.userComment.setText(savedValue);
                }
            }
            scrollable.addView(this.userComment);
        }
        int emailPromptId = ACRA.getConfig().resDialogEmailPrompt();
        if (emailPromptId != 0) {
            label = new TextView(this);
            label.setText(getText(emailPromptId));
            label.setPadding(label.getPaddingLeft(), 10, label.getPaddingRight(), label.getPaddingBottom());
            scrollable.addView(label);
            this.userEmail = new EditText(this);
            this.userEmail.setSingleLine();
            this.userEmail.setInputType(33);
            this.prefs = getSharedPreferences(ACRA.getConfig().sharedPreferencesName(), ACRA.getConfig().sharedPreferencesMode());
            savedValue = null;
            if (savedInstanceState != null) {
                savedValue = savedInstanceState.getString(STATE_EMAIL);
            }
            if (savedValue != null) {
                this.userEmail.setText(savedValue);
            } else {
                this.userEmail.setText(this.prefs.getString(ACRA.PREF_USER_EMAIL_ADDRESS, ACRAConstants.DEFAULT_STRING_VALUE));
            }
            scrollable.addView(this.userEmail);
        }
        return root;
    }

    protected void cancelNotification() {
        ((NotificationManager) getSystemService("notification")).cancel(666);
    }

    public void onClick(DialogInterface dialog, int which) {
        if (which == -1) {
            sendCrash();
        } else {
            cancelReports();
        }
        finish();
    }

    private void cancelReports() {
        ACRA.getErrorReporter().deletePendingNonApprovedReports(false);
    }

    private void sendCrash() {
        String usrEmail;
        String comment = this.userComment != null ? this.userComment.getText().toString() : ACRAConstants.DEFAULT_STRING_VALUE;
        if (this.prefs == null || this.userEmail == null) {
            usrEmail = ACRAConstants.DEFAULT_STRING_VALUE;
        } else {
            usrEmail = this.userEmail.getText().toString();
            Editor prefEditor = this.prefs.edit();
            prefEditor.putString(ACRA.PREF_USER_EMAIL_ADDRESS, usrEmail);
            prefEditor.commit();
        }
        CrashReportPersister persister = new CrashReportPersister(getApplicationContext());
        try {
            Log.d(ACRA.LOG_TAG, "Add user comment to " + this.mReportFileName);
            CrashReportData crashData = persister.load(this.mReportFileName);
            crashData.put(ReportField.USER_COMMENT, comment);
            crashData.put(ReportField.USER_EMAIL, usrEmail);
            persister.store(crashData, this.mReportFileName);
        } catch (IOException e) {
            Log.w(ACRA.LOG_TAG, "User comment not added: ", e);
        }
        Log.v(ACRA.LOG_TAG, "About to start SenderWorker from CrashReportDialog");
        ACRA.getErrorReporter().startSendingReports(false, true);
        int toastId = ACRA.getConfig().resDialogOkToast();
        if (toastId != 0) {
            ToastSender.sendToast(getApplicationContext(), toastId, 1);
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (!(this.userComment == null || this.userComment.getText() == null)) {
            outState.putString(STATE_COMMENT, this.userComment.getText().toString());
        }
        if (this.userEmail != null && this.userEmail.getText() != null) {
            outState.putString(STATE_EMAIL, this.userEmail.getText().toString());
        }
    }

    public void onDismiss(DialogInterface dialog) {
        finish();
    }
}
