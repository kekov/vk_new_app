package org.acra.sender;

import android.net.Uri;
import android.util.Log;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.acra.ACRA;
import org.acra.ACRAConfiguration;
import org.acra.ACRAConstants;
import org.acra.ReportField;
import org.acra.collector.CrashReportData;
import org.acra.util.HttpRequest;
import org.acra.util.JSONReportBuilder.JSONReportException;

public class HttpSender implements ReportSender {
    private final Uri mFormUri;
    private final Map<ReportField, String> mMapping;
    private final Method mMethod;
    private final Type mType;

    /* renamed from: org.acra.sender.HttpSender.1 */
    static /* synthetic */ class C11201 {
        static final /* synthetic */ int[] $SwitchMap$org$acra$sender$HttpSender$Method;
        static final /* synthetic */ int[] $SwitchMap$org$acra$sender$HttpSender$Type;

        static {
            $SwitchMap$org$acra$sender$HttpSender$Method = new int[Method.values().length];
            try {
                $SwitchMap$org$acra$sender$HttpSender$Method[Method.POST.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$acra$sender$HttpSender$Method[Method.PUT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SwitchMap$org$acra$sender$HttpSender$Type = new int[Type.values().length];
            try {
                $SwitchMap$org$acra$sender$HttpSender$Type[Type.JSON.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$org$acra$sender$HttpSender$Type[Type.FORM.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public enum Method {
        POST,
        PUT
    }

    public enum Type {
        FORM {
            public String getContentType() {
                return "application/x-www-form-urlencoded";
            }
        },
        JSON {
            public String getContentType() {
                return "application/json";
            }
        };

        public abstract String getContentType();
    }

    public HttpSender(Method method, Type type, Map<ReportField, String> mapping) {
        this.mMethod = method;
        this.mFormUri = null;
        this.mMapping = mapping;
        this.mType = type;
    }

    public HttpSender(Method method, Type type, String formUri, Map<ReportField, String> mapping) {
        this.mMethod = method;
        this.mFormUri = Uri.parse(formUri);
        this.mMapping = mapping;
        this.mType = type;
    }

    public void send(CrashReportData report) throws ReportSenderException {
        String password = null;
        try {
            URL reportUrl;
            if (this.mFormUri == null) {
                reportUrl = new URL(ACRA.getConfig().formUri());
            } else {
                reportUrl = new URL(this.mFormUri.toString());
            }
            Log.d(ACRA.LOG_TAG, "Connect to " + reportUrl.toString());
            String login = ACRAConfiguration.isNull(ACRA.getConfig().formUriBasicAuthLogin()) ? null : ACRA.getConfig().formUriBasicAuthLogin();
            if (!ACRAConfiguration.isNull(ACRA.getConfig().formUriBasicAuthPassword())) {
                password = ACRA.getConfig().formUriBasicAuthPassword();
            }
            HttpRequest request = new HttpRequest();
            request.setConnectionTimeOut(ACRA.getConfig().connectionTimeout());
            request.setSocketTimeOut(ACRA.getConfig().socketTimeout());
            request.setMaxNrRetries(ACRA.getConfig().maxNumberOfRequestRetries());
            request.setLogin(login);
            request.setPassword(password);
            request.setHeaders(ACRA.getConfig().getHttpHeaders());
            String reportAsString = ACRAConstants.DEFAULT_STRING_VALUE;
            switch (C11201.$SwitchMap$org$acra$sender$HttpSender$Type[this.mType.ordinal()]) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    reportAsString = report.toJSON().toString();
                    break;
                default:
                    reportAsString = HttpRequest.getParamsAsFormString(remap(report));
                    break;
            }
            switch (C11201.$SwitchMap$org$acra$sender$HttpSender$Method[this.mMethod.ordinal()]) {
                case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                    break;
                case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                    reportUrl = new URL(reportUrl.toString() + '/' + report.getProperty(ReportField.REPORT_ID));
                    break;
                default:
                    throw new UnsupportedOperationException("Unknown method: " + this.mMethod.name());
            }
            request.send(reportUrl, this.mMethod, reportAsString, this.mType);
        } catch (IOException e) {
            throw new ReportSenderException("Error while sending " + ACRA.getConfig().reportType() + " report via Http " + this.mMethod.name(), e);
        } catch (JSONReportException e2) {
            throw new ReportSenderException("Error while sending " + ACRA.getConfig().reportType() + " report via Http " + this.mMethod.name(), e2);
        }
    }

    private Map<String, String> remap(Map<ReportField, String> report) {
        ReportField[] fields = ACRA.getConfig().customReportContent();
        if (fields.length == 0) {
            fields = ACRAConstants.DEFAULT_REPORT_FIELDS;
        }
        Map<String, String> finalReport = new HashMap(report.size());
        for (ReportField field : fields) {
            if (this.mMapping == null || this.mMapping.get(field) == null) {
                finalReport.put(field.toString(), report.get(field));
            } else {
                finalReport.put(this.mMapping.get(field), report.get(field));
            }
        }
        return finalReport;
    }
}
