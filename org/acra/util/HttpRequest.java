package org.acra.util;

import com.google.android.gms.games.GamesClient;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import org.acra.ACRA;
import org.acra.ACRAConstants;
import org.acra.sender.HttpSender.Method;
import org.acra.sender.HttpSender.Type;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

public final class HttpRequest {
    private int connectionTimeOut;
    private Map<String, String> headers;
    private String login;
    private int maxNrRetries;
    private String password;
    private int socketTimeOut;

    /* renamed from: org.acra.util.HttpRequest.1 */
    static /* synthetic */ class C11211 {
        static final /* synthetic */ int[] $SwitchMap$org$acra$sender$HttpSender$Method;

        static {
            $SwitchMap$org$acra$sender$HttpSender$Method = new int[Method.values().length];
            try {
                $SwitchMap$org$acra$sender$HttpSender$Method[Method.POST.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$acra$sender$HttpSender$Method[Method.PUT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    private static class SocketTimeOutRetryHandler implements HttpRequestRetryHandler {
        private final HttpParams httpParams;
        private final int maxNrRetries;

        private SocketTimeOutRetryHandler(HttpParams httpParams, int maxNrRetries) {
            this.httpParams = httpParams;
            this.maxNrRetries = maxNrRetries;
        }

        public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
            if (exception instanceof SocketTimeoutException) {
                if (executionCount <= this.maxNrRetries) {
                    if (this.httpParams != null) {
                        int newSocketTimeOut = HttpConnectionParams.getSoTimeout(this.httpParams) * 2;
                        HttpConnectionParams.setSoTimeout(this.httpParams, newSocketTimeOut);
                        ACRA.log.m540d(ACRA.LOG_TAG, "SocketTimeOut - increasing time out to " + newSocketTimeOut + " millis and trying again");
                    } else {
                        ACRA.log.m540d(ACRA.LOG_TAG, "SocketTimeOut - no HttpParams, cannot increase time out. Trying again with current settings");
                    }
                    return true;
                }
                ACRA.log.m540d(ACRA.LOG_TAG, "SocketTimeOut but exceeded max number of retries : " + this.maxNrRetries);
            }
            return false;
        }
    }

    public HttpRequest() {
        this.connectionTimeOut = GamesClient.STATUS_ACHIEVEMENT_UNLOCK_FAILURE;
        this.socketTimeOut = GamesClient.STATUS_ACHIEVEMENT_UNLOCK_FAILURE;
        this.maxNrRetries = 3;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConnectionTimeOut(int connectionTimeOut) {
        this.connectionTimeOut = connectionTimeOut;
    }

    public void setSocketTimeOut(int socketTimeOut) {
        this.socketTimeOut = socketTimeOut;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public void setMaxNrRetries(int maxNrRetries) {
        this.maxNrRetries = maxNrRetries;
    }

    public void send(URL url, Method method, String content, Type type) throws IOException {
        HttpClient httpClient = getHttpClient();
        HttpEntityEnclosingRequestBase httpRequest = getHttpRequest(url, method, content, type);
        ACRA.log.m540d(ACRA.LOG_TAG, "Sending request to " + url);
        HttpResponse response = null;
        try {
            response = httpClient.execute(httpRequest, new BasicHttpContext());
            if (response != null) {
                if (response.getStatusLine() != null) {
                    String statusCode = Integer.toString(response.getStatusLine().getStatusCode());
                    if (!(statusCode.equals("409") || statusCode.equals("403") || (!statusCode.startsWith("4") && !statusCode.startsWith("5")))) {
                        throw new IOException("Host returned error code " + statusCode);
                    }
                }
                EntityUtils.toString(response.getEntity());
            }
            if (response != null) {
                response.getEntity().consumeContent();
            }
        } catch (Throwable th) {
            if (response != null) {
                response.getEntity().consumeContent();
            }
        }
    }

    private HttpClient getHttpClient() {
        HttpParams httpParams = new BasicHttpParams();
        httpParams.setParameter("http.protocol.cookie-policy", "rfc2109");
        HttpConnectionParams.setConnectionTimeout(httpParams, this.connectionTimeOut);
        HttpConnectionParams.setSoTimeout(httpParams, this.socketTimeOut);
        HttpConnectionParams.setSocketBufferSize(httpParams, ACRAConstants.DEFAULT_BUFFER_SIZE_IN_BYTES);
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("http", new PlainSocketFactory(), 80));
        if (ACRA.getConfig().disableSSLCertValidation()) {
            registry.register(new Scheme("https", new FakeSocketFactory(), 443));
        } else {
            registry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        }
        DefaultHttpClient httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, registry), httpParams);
        httpClient.setHttpRequestRetryHandler(new SocketTimeOutRetryHandler(this.maxNrRetries, null));
        return httpClient;
    }

    private UsernamePasswordCredentials getCredentials() {
        if (this.login == null && this.password == null) {
            return null;
        }
        return new UsernamePasswordCredentials(this.login, this.password);
    }

    private HttpEntityEnclosingRequestBase getHttpRequest(URL url, Method method, String content, Type type) throws UnsupportedEncodingException, UnsupportedOperationException {
        HttpEntityEnclosingRequestBase httpRequest;
        switch (C11211.$SwitchMap$org$acra$sender$HttpSender$Method[method.ordinal()]) {
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
                httpRequest = new HttpPost(url.toString());
                break;
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                httpRequest = new HttpPut(url.toString());
                break;
            default:
                throw new UnsupportedOperationException("Unknown method: " + method.name());
        }
        UsernamePasswordCredentials creds = getCredentials();
        if (creds != null) {
            httpRequest.addHeader(BasicScheme.authenticate(creds, "UTF-8", false));
        }
        httpRequest.setHeader("User-Agent", "Android");
        httpRequest.setHeader("Accept", "text/html,application/xml,application/json,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");
        httpRequest.setHeader("Content-Type", type.getContentType());
        if (this.headers != null) {
            for (String header : this.headers.keySet()) {
                httpRequest.setHeader(header, (String) this.headers.get(header));
            }
        }
        httpRequest.setEntity(new StringEntity(content, "UTF-8"));
        return httpRequest;
    }

    public static String getParamsAsFormString(Map<?, ?> parameters) throws UnsupportedEncodingException {
        StringBuilder dataBfr = new StringBuilder();
        for (Object key : parameters.keySet()) {
            Object value;
            if (dataBfr.length() != 0) {
                dataBfr.append('&');
            }
            Object preliminaryValue = parameters.get(key);
            if (preliminaryValue == null) {
                value = ACRAConstants.DEFAULT_STRING_VALUE;
            } else {
                value = preliminaryValue;
            }
            dataBfr.append(URLEncoder.encode(key.toString(), "UTF-8"));
            dataBfr.append('=');
            dataBfr.append(URLEncoder.encode(value.toString(), "UTF-8"));
        }
        return dataBfr.toString();
    }
}
