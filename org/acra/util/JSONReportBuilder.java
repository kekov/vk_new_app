package org.acra.util;

import com.vkontakte.android.mediapicker.gl.GLRenderBuffer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.collector.CrashReportData;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONReportBuilder {

    public static class JSONReportException extends Exception {
        private static final long serialVersionUID = -694684023635442219L;

        public JSONReportException(String message, Throwable e) {
            super(message, e);
        }
    }

    public static JSONObject buildJSONReport(CrashReportData errorContent) throws JSONReportException {
        JSONObject jsonReport = new JSONObject();
        for (ReportField key : errorContent.keySet()) {
            if (key.containsKeyValuePairs()) {
                JSONObject subObject = new JSONObject();
                BufferedReader reader = new BufferedReader(new StringReader(errorContent.getProperty(key)), GLRenderBuffer.EGL_SURFACE_SIZE);
                while (true) {
                    try {
                        String line = reader.readLine();
                        if (line == null) {
                            break;
                        }
                        addJSONFromProperty(subObject, line);
                    } catch (IOException e) {
                        try {
                            ACRA.log.m543e(ACRA.LOG_TAG, "Error while converting " + key.name() + " to JSON.", e);
                        } catch (JSONException e2) {
                            throw new JSONReportException("Could not create JSON object for key " + key, e2);
                        }
                    }
                }
                jsonReport.accumulate(key.name(), subObject);
            } else {
                jsonReport.accumulate(key.name(), guessType(errorContent.getProperty(key)));
            }
        }
        return jsonReport;
    }

    private static void addJSONFromProperty(JSONObject destination, String propertyString) throws JSONException {
        int equalsIndex = propertyString.indexOf(61);
        if (equalsIndex > 0) {
            JSONObject finalObject = destination;
            String currentKey = propertyString.substring(0, equalsIndex).trim();
            Object guessType = guessType(propertyString.substring(equalsIndex + 1).trim());
            if (guessType instanceof String) {
                guessType = ((String) guessType).replaceAll("\\\\n", "\n");
            }
            String[] splitKey = currentKey.split("\\.");
            if (splitKey.length > 1) {
                addJSONSubTree(finalObject, splitKey, guessType);
                return;
            } else {
                finalObject.accumulate(currentKey, guessType);
                return;
            }
        }
        destination.put(propertyString.trim(), true);
    }

    private static Object guessType(String value) {
        if (value.equalsIgnoreCase("true")) {
            return Boolean.valueOf(true);
        }
        if (value.equalsIgnoreCase("false")) {
            return Boolean.valueOf(false);
        }
        if (value.matches("(?:^|\\s)([1-9](?:\\d*|(?:\\d{0,2})(?:,\\d{3})*)(?:\\.\\d*[1-9])?|0?\\.\\d*[1-9]|0)(?:\\s|$)")) {
            try {
                return NumberFormat.getInstance(Locale.US).parse(value);
            } catch (ParseException e) {
            }
        }
        return value;
    }

    private static void addJSONSubTree(JSONObject destination, String[] keys, Object value) throws JSONException {
        for (int i = 0; i < keys.length; i++) {
            String subKey = keys[i];
            if (i < keys.length - 1) {
                JSONObject intermediate;
                if (destination.isNull(subKey)) {
                    intermediate = new JSONObject();
                    destination.accumulate(subKey, intermediate);
                } else {
                    intermediate = destination.getJSONObject(subKey);
                }
                destination = intermediate;
            } else {
                destination.accumulate(subKey, value);
            }
        }
    }
}
