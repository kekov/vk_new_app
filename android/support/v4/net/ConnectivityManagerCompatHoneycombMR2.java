package android.support.v4.net;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.vkontakte.android.NewPostActivity;
import com.vkontakte.android.UserListView;
import com.vkontakte.android.ValidationActivity;
import com.vkontakte.android.api.Group;
import com.vkontakte.android.fragments.BoardTopicsFragment;

class ConnectivityManagerCompatHoneycombMR2 {
    ConnectivityManagerCompatHoneycombMR2() {
    }

    public static boolean isActiveNetworkMetered(ConnectivityManager cm) {
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) {
            return true;
        }
        switch (info.getType()) {
            case ValidationActivity.VRESULT_NONE /*0*/:
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
            case Group.ADMIN_LEVEL_ADMIN /*3*/:
            case UserListView.TYPE_FAVE /*4*/:
            case UserListView.TYPE_FOLLOWERS /*5*/:
            case UserListView.TYPE_POLL_VOTERS /*6*/:
                return true;
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
            case UserListView.TYPE_FAVE_LINKS /*7*/:
            case NewPostActivity.POLL_EDIT_RESULT /*9*/:
                return false;
            default:
                return true;
        }
    }
}
