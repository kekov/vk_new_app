package android.support.v4.text;

import com.vkontakte.android.C0436R;
import com.vkontakte.android.fragments.BoardTopicsFragment;
import com.vkontakte.android.mediapicker.providers.LangProvider.StringKeys;
import java.util.Locale;
import org.acra.ACRAConstants;

public class TextUtilsCompat {
    private static String ARAB_SCRIPT_SUBTAG;
    private static String HEBR_SCRIPT_SUBTAG;
    public static final Locale ROOT;

    public static String htmlEncode(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (c) {
                case StringKeys.ERROR /*34*/:
                    sb.append("&quot;");
                    break;
                case C0436R.styleable.View_android_isScrollContainer /*38*/:
                    sb.append("&amp;");
                    break;
                case C0436R.styleable.View_android_hapticFeedbackEnabled /*39*/:
                    sb.append("&#39;");
                    break;
                case C0436R.styleable.SherlockTheme_windowActionBarOverlay /*60*/:
                    sb.append("&lt;");
                    break;
                case C0436R.styleable.SherlockTheme_windowSplitActionBar /*62*/:
                    sb.append("&gt;");
                    break;
                default:
                    sb.append(c);
                    break;
            }
        }
        return sb.toString();
    }

    public static int getLayoutDirectionFromLocale(Locale locale) {
        if (!(locale == null || locale.equals(ROOT))) {
            String scriptSubtag = ICUCompat.getScript(ICUCompat.addLikelySubtags(locale.toString()));
            if (scriptSubtag == null) {
                return getLayoutDirectionFromFirstChar(locale);
            }
            if (scriptSubtag.equalsIgnoreCase(ARAB_SCRIPT_SUBTAG) || scriptSubtag.equalsIgnoreCase(HEBR_SCRIPT_SUBTAG)) {
                return 1;
            }
        }
        return 0;
    }

    private static int getLayoutDirectionFromFirstChar(Locale locale) {
        switch (Character.getDirectionality(locale.getDisplayName(locale).charAt(0))) {
            case BoardTopicsFragment.ORDER_UPDATED_DESC /*1*/:
            case BoardTopicsFragment.ORDER_CREATED_DESC /*2*/:
                return 1;
            default:
                return 0;
        }
    }

    static {
        ROOT = new Locale(ACRAConstants.DEFAULT_STRING_VALUE, ACRAConstants.DEFAULT_STRING_VALUE);
        ARAB_SCRIPT_SUBTAG = "Arab";
        HEBR_SCRIPT_SUBTAG = "Hebr";
    }
}
